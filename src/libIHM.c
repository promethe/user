/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \ingroup plugin_list
 \defgroup libIHM libIHM
 \brief Groups for interacting with user of files.
 */

#include <string.h>

#include <libx.h>

#include <group_function_pointers.h>

#include <IHM/Graphics_Component.h>
#include <IHM/Image.h>
#include <IHM/NN_IO.h>
#include <IHM/Text.h>

#include <IHM/libIHM.h>

type_group_function_pointers group_function_pointers[] =
  {
  /* se situe pour l'insant dans IHM_dynamic.h vu qu'il est à la racine du repertoire */
    { "f_interface_controle", function_interface_controle, NULL, NULL, NULL, NULL, -1, -1 },

  /* Graphics_Component */
    { "f_add_button", function_add_button, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_add_button_and_set_val_to_neurons", function_add_button_set_val_to_neurons, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_checkbox", function_checkbox, new_checkbox, destroy_checkbox, NULL, NULL, -1, -1 },
  /*{"f_set_val_to_neurons_from_button", function_set_val_to_neurons_from_button,  NULL, NULL, NULL, NULL, -1, -1},  inutilisée */

  /* Image */
    { "f_affiche_image_conditionnel", function_affiche_image_conditionnel, new_affiche_image_conditionnel, destroy_affiche_image_conditionnel, NULL, NULL, -1, -1 },
    { "f_affiche_image_from_extension", function_affiche_image_from_extension, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_affiche_flux_vecteurs", function_affiche_flux_vecteurs, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_values_counter", function_values_counter, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_affiche_visage", function_affiche_visage, NULL, NULL, NULL, NULL, -1, -1 },
  /*{"f_wait_click", function_wait_click, NULL, NULL, NULL, NULL, -1, -1}, inutilisée */
    { "f_wait_click_robot", function_wait_click_robot, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_select_ROI", function_select_ROI, new_function_select_ROI, destroy_function_select_ROI, NULL, NULL, -1, -1 },
    { "f_new_center", function_new_center, new_function_new_center, destroy_function_new_center, NULL, NULL, -1, -1 },
    { "f_save_caracteristics", function_save_caracteristics, new_function_save_caracteristics, destroy_function_save_caracteristics, NULL, NULL, -1, -1 },
    { "f_load_caracteristics", function_load_caracteristics, new_function_load_caracteristics, destroy_function_load_caracteristics, NULL, NULL, -1, -1 },
    { "f_dessiner_forme", function_dessiner_forme, new_dessiner_forme, NULL, NULL, NULL, -1, -1 },

  /* NN_IO */
    { "f_affiche_pdv_info", function_affiche_pdv_info, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_affiche_pdv_xy", function_affiche_pdv_xy, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_affiche_xy", function_affiche_xy, new_affiche_xy, destroy_affiche_xy, NULL, NULL, -1, -1 },
    { "f_affiche_coeff2D", function_affiche_coeff2D, new_affiche_coeff2D, destroy_affiche_coeff2D, NULL, NULL, -1, -1 },
    { "f_demande_numero0", function_demande_numero0, new_demande_numero0, NULL, NULL, NULL, -1, -1 },
    { "f_display_group", function_display_group, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_display_group_perso", function_display_group_perso, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_diplay_group_txt", function_display_group_txt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_display_image_activity", function_display_image_activity, new_display_image_activity, destroy_display_image_activity, NULL, NULL, -1, -1 },
    { "f_display_group_activity", function_display_group_activity, new_display_group_activity, destroy_display_group_activity, NULL, NULL, -1, -1 },
    { "f_init_var_from_config", function_init_var_from_config, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_oscillo", function_oscillo, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_oscillo_c", function_oscillo_c, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_oscillo2", function_oscillo2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_oscillo_group", function_oscillo_group, NULL, NULL, NULL, NULL, -1, -1 },
  /*{"f_record_image_activity", function_record_image_activity,  NULL, NULL, NULL, NULL, -1, -1},  inutilisee */
  /*{"f_save_activity", function_save_activity, NULL, NULL, NULL, NULL, -1, -1},  inutilisee */
    { "f_save_image_activity", function_save_image_activity, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_pdv", function_save_pdv, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_probas", function_save_probas, new_save_probas, destroy_save_probas, NULL, NULL, -1, -1 },
    { "f_save_weight", function_save_weight, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_write_image_activity_split", function_write_image_activity_split, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_weights", function_save_weights, new_save_weights, destroy_save_weights, NULL, NULL, -1, -1 },
    { "f_speak", function_speak, new_speak, destroy_speak, NULL, NULL, -1, -1 },

  /* Text */
    { "f_ask_for_learn", function_ask_for_learn, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_angle_degres", function_demande_angle_degres, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_decision", function_demande_decision, new_demande_decision, NULL, NULL, NULL, -1, -1 },
    { "f_demande_nb", function_demande_nb, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_vecteur", function_demande_vecteur, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_vecteur2", function_demande_vecteur2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_display_frame_rate", function_display_frame_rate, new_display_frame_rate, destroy_frame_rate, NULL, NULL, -1, -1 },
    { "f_display_phase_space", function_display_phase_space, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_test_prom_getopt", function_test_prom_getopt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_vue_metres", function_vue_metres, new_f_vue_metres, destroy_vue_metres, NULL, NULL, -1, -1 },

    { "f_vue_metres_population", function_vue_metres_population, new_vue_metres_population, destroy_vue_metres_population, NULL, NULL, -1, -1 },

  /* pour indiquer la fin du tableau*/
    { NULL, NULL, NULL, NULL, NULL, NULL, -1, -1 } };

void read_help();

void modify_help();

int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}
