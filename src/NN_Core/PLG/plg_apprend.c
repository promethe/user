/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <NN_Core/plg.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

void apprend_plg(int gpe)
{
   /* type_noeud *ptn;*/
   int n,deb,nbre;
   /*  int i;*/
   float local_vigilence;
   int lien,j=0;
   float my_learn=1.0;

   /*  float dc;*/
   /*  float para1,para2;
       int flag=0;*/

   dprints("entre apprend plg %d \n",gpe);

   deb=def_groupe[gpe].premier_ele;
   nbre=def_groupe[gpe].nbre;
   local_vigilence = vigilence;


   while (	(lien=find_input_link(gpe,j)) != -1)
   {
      j++;
      if ( strstr(liaison[lien].nom,"vigilence")!=NULL )
      {
         local_vigilence = liaison[lien].norme * neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
      }
      if ( strstr(liaison[lien].nom,"learn")!=NULL )
      {
         my_learn = liaison[lien].norme * neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
      }
   }
   dprints("Dans apprend PLG la vigilence est de %f\n",local_vigilence);
   if (local_vigilence<0. || my_learn<=0)
      /*vu qu'on peut faire que apprendre ou s'adapter je rajoute ca pour qu'il n'y ai aucune modification des poids*/
      /*c'est pas top mais ca depanne : en fait la vigilence =0 et learning rate =0 revient au meme en + propre... M.M.*/
   {
      dprints("Gpe %d : vigilence<0 ou my_learn : pas d'apprentissage\n",gpe);
      return;
   }


   for (n=deb; n<deb+nbre; n++)
   {
      apprend_coeff_colonne_cor2(n,neurone[n].s2);
   }

   dprints("sort apprend plg %d \n",gpe);
}


int apprend_coeff_colonne_cor2(int n, float sortie)
{
   int No_voie;
   type_coeff *coeff;

   coeff = neurone[n].coeff;

   if (coeff==NULL)
   {
      EXIT_ON_ERROR("ERROR impossible learning - NO LINK !!! \n");
   }

   No_voie=(int)(coeff->type/2);

   if (coeff->type!=No_voie*2)
   {
      if (sortie>0.999999) /*On ne fait l'apprentissage que pour le gagnant pour gagner en temps de calcul !*/
      {
         return (apprend_coeff_colonne_distance_cor2(n,sortie));   /*version distance*/
      }
      else  return (0); /*Il n'y a pas eu d'apprentissage*/
   }

   dprints("\n----> no lien %d type=%d n=%d\n",coeff->gpe_liaison,liaison[coeff->gpe_liaison].mode,n);
   /* return(apprend_coeff_colonne_produit_cor(n,sortie)); 		version produit non PCR*/
   EXIT_ON_ERROR("Non supporte par PLG\n");
   return 0; /* pas possible mais ca evite le warning de gcc... */
   /* return(apprend_coeff_colonne_produit_max(n,sortie)); 	version produit PCR*/
}


/* apprend en analogique ... */

int apprend_coeff_colonne_distance_cor2(int n, float sortie)
{
   float /*dc,*/Wi,Ej;
   type_coeff *coeff;
   float coeff_avant_modif;
   /*  float hasard;*/
   int flag=0;
   /*  float eps_liaison=0.;*/
   float delta = 0.;
   float confiance;
   int  debug_ap=0, debug_ad=0;

   (void) sortie;

   coeff=neurone[n].coeff;

   delta = def_groupe[neurone[n].groupe].learning_rate; /*delat de la regle d'adaptation*/

   if (neurone[n].flag!=1) /*le neurone considere n'a jamais appris alors apprentissage rapide (un coup)*/
   {
      /*	printf("apprenissage en un coups\n");*/
      while (coeff!=NULL)
      {
         if (coeff->evolution==1)
         {
            coeff_avant_modif=coeff->val;
            Ej=neurone[coeff->entree].s1;;
            coeff->val = Ej;
            coeff->Nbre_S +=1.;
            debug_ap =1;
            if (isdiff(coeff_avant_modif,coeff->val)) flag=1;

         }
         coeff=coeff->s;
      }
      neurone[n].flag = 1;

   }
   else if (neurone[n].s<=0.00001) /*faudrait surement degager cette condition*/
   {
      while (coeff!=NULL)
      {
         if (coeff->evolution==1)
         {
            coeff_avant_modif=coeff->val;
            Wi=coeff->val;
            Ej=neurone[coeff->entree].s1;
            coeff->val = Wi + delta*(Ej - Wi)*(1. - neurone[n].s);
            /*plutot un - devant le delta : on eloigne tres legerement le neurone n'appartenant probablement pas a cette categorie vue sa reponse*/
            debug_ad =1;
            if (isdiff(coeff_avant_modif,coeff->val)) flag=1;
         }
         coeff=coeff->s;
      }
   }
   else
   {
      while (coeff!=NULL)
      {
         confiance = (coeff->Nbre_E*coeff->Nbre_S)/(coeff->Nbre_ES*coeff->Nbre_ES);
         if (coeff->Nbre_S< 10. || confiance<=2.)
            confiance=0.5;
         else
            confiance = 0.;
         if (coeff->evolution==1)
         {
            coeff_avant_modif=coeff->val;
            Wi=coeff->val;
            Ej=neurone[coeff->entree].s1;
            coeff->val = Wi + (delta+confiance)*(Ej - Wi)*(1. - neurone[n].s);
            debug_ad =1;
            coeff->Nbre_S +=1.;
            if (neurone[coeff->entree].s1>neurone[n].seuil)
            {
               coeff->Nbre_E += 1.;
               coeff->Nbre_ES +=1.;
            }
            if (isdiff(coeff_avant_modif,coeff->val)) flag=1;
         }
         coeff=coeff->s;
      }
   }

#ifdef DEBUG
   if (debug_ad) printf("--------adaptation\n");
   if (debug_ap) printf("--------apprentissage\n");
#endif
   return (flag);
}
