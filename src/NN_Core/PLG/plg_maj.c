/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>

#include <NN_Core/plg.h>


#undef PAS_FAIT
#define DEBUG

void mise_a_jour_plg(int i, int gestion_STM, int learn)
{
    type_coeff 	  *coeff;
    float 	  sortie=0.;
    int             deb, increment;
    int             groupe, nbre, nbre2;
    float sigma=0.;
    float alpha=0.;
    
    dprints("entre dans maj plg\n");
    coeff=neurone[i].coeff;

    if(coeff==NULL) neurone[i].s2=neurone[i].s1;   /*    c'est entree   */

    groupe = neurone[i].groupe;
    deb = def_groupe[groupe].premier_ele;
    nbre=def_groupe[groupe].nbre;
    nbre2=def_groupe[groupe].taillex*def_groupe[groupe].tailley;
    increment=nbre/nbre2;
    sigma  = def_groupe[groupe].sigma_f;
    alpha  = def_groupe[groupe].alpha;



    if(neurone[i + (increment-1) - (i-deb)%increment ].flag==0 ) 
        /*Pour un macroneur qui n'a pas appris (on regarde le seuil sur le macro neur correspondant au microneur)*/
    {
        sortie=0.;
    }
    else 
    {
        if(coeff!=NULL && (coeff->type%2==0))
        {
            EXIT_ON_ERROR("Non supporte par PLG\n");
        }
        else
        {
            sortie=calcule_mc_distance2_cor(i,gestion_STM,learn,sigma,alpha);
        }
    }

    neurone[i].s=/*neurone[i].s1=*/sortie;		

    dprints("sorti de plg\n");
}


float calcule_mc_distance2_cor(int i,int gestion_STM,int learn,float sigma,float alpha)
{
    type_coeff *coeff;
    float sortie=0.,somme_1=0./*,somme_entree=0.*/;
    float nbre_poids1=0.;
    /*float div;*/
    float confiance=0.;
    float toto;
    
    UNUSED_PARAM gestion_STM; UNUSED_PARAM learn; UNUSED_PARAM sigma;
    
    /* float  Nbr_E,Nbr_S,Nbr_ES;*/
    coeff=neurone[i].coeff;
    /* ------------------------------------- */
    /* Boucle sur les liens du micro-neurone */
    /* ------------------------------------- */
    while(coeff!=NULL)
    {
        /*1ere fois: on initialise pour eviter les division par 0*/
        if(coeff->Nbre_S<=1.)
        {
            coeff->Nbre_ES = 0.0001;
            coeff->Nbre_E = 0.0001;
            confiance = 1.;
        }
        /*si moins de 10 fois active: la correlation n'a pas de sens: le lien est conserve et il apprendra*/
        else if(coeff->Nbre_S<10.)
        {
            confiance = 1.;
        }
        else/*cas normal*/
        {
            confiance = (coeff->Nbre_E * coeff->Nbre_S)/(coeff->Nbre_ES*coeff->Nbre_ES);
        }

        if(confiance <= 1.2)
            /*le poids n'est pas elague car on a confiance en lui*//*2 1.6 1.1*/ /*1/confiance => pourcentage de ES sur S*/
        {

            nbre_poids1 = nbre_poids1 + 1.;			/*Nombre de poids pris en compte*/

            toto = coeff->val-neurone[coeff->entree].s1;
            if(toto>0.)/*a enlever: une entree negative a pas de sens*/
                somme_1 += toto;
        }

        coeff=coeff->s;
    }
    /* printf ("\t\t\t somme_1=%f nbre_poids1=%f \n",somme_1 ,nbre_poids1); */
    /*  printf("-----------------------------------------------------\n corr %f\n-------------------------------------------\n",somme_1);*/
    printf("_____________nbre confiance %f\n",nbre_poids1);

    nbre_poids1=nbre_poids1/(3.*(1./alpha));
    /*on renorme comme une gaussienne doit etre renormee*/
    /*alpha=0.6*/ 
    /*sigma = nbre/(3*(1/%remplissage)) -> donc si 100 point remplis a 50% en fait 50 points soit sigma = 50/3 <=> sigma = 100/(3*(1/0.5))*/
    printf("_____________apha %f____sigma %f\n",alpha,nbre_poids1);

    if(nbre_poids1 > 0. )
    {
        sortie = exp(-(somme_1*somme_1)/(2.*nbre_poids1*nbre_poids1));

        dprints("-----------------------------------------------------\n corr %f %f\n-------------------------------------------\n",somme_1,sortie);
    }
    else
    {
        sortie = 0.;
    }

    return(sortie);
}


