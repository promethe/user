/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <NN_Core/plg.h>


#define DEBUG

void gestion_groupe_plg(int gpe)
{
    int j=0,n,deb,nbre,nbre2,pos,maxpos;
    float max,mmax,mmoy,norme=0.;
    /* float a,b,a1,b1;*/
    int increment;
    /*  int new=0;*/
    int nbr_imagettes=0;
    int lien=0;
    int neuro_vigi=0;
    float vigi_neuro_mod=0.;

#ifdef DEBUG
    printf("entre dans gestion plg\n");
#endif
    deb=def_groupe[gpe].premier_ele;
    nbre=def_groupe[gpe].nbre;
    nbre2=def_groupe[gpe].taillex*def_groupe[gpe].tailley;
    increment=nbre/nbre2;

    /*on traite la neuromodulation*/
    /*M.M.*/
    /*faudrait les memoriser dans le data par exemple pour aller plus vite*/
    while(	(lien=find_input_link(gpe,j)) != -1)
    {
        j++;
        if( strstr(liaison[lien].nom,"vigilence")!=NULL )
        {
            vigi_neuro_mod = liaison[lien].norme * neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
            neuro_vigi=1;
        }
    }

    if(neuro_vigi!=1)
        vigi_neuro_mod = vigilence;

    /*#ifdef DEBUG*/
    printf("La vigilence dans PLG gpe %d est de %f\n",gpe,vigi_neuro_mod);
    /*#endif*/

#ifdef DEBUG
    printf("competition au niveau des imagettes \n");
#endif


    max	= -9999999.;
    mmoy	= 0.;
    maxpos=pos = deb;/*Et non 0 car le premier coup maxpos sera pas change car y'a pas de max et donc faut qu'il soit bon*/


    for(n=deb;n<deb+nbre;n++)
    {
        if(neurone[n].flag==1)	/*Que parmis les formes qui ont appris*/
        {
            nbr_imagettes++;       /* compte le nombre de formes deja apprises*/
            mmoy = mmoy + neurone[n].s; /*Calcul moyenne du signal en entree*/
            norme = norme + (neurone[n].s*neurone[n].s); /*Calcul de la norme ou deviation standart*/
            if(neurone[n].s>max) 
            { 
                max=neurone[n].s; 
                maxpos=pos=n; 
            }
        }
        /*printf("deb %d increment %d pos %d max %f \n",deb,increment,pos,max);*/
        neurone[n].s2=0.; /*Tout mis a zero*/
        neurone[n].s1=0.;
    }

    mmax=max;

#ifdef DEBUG
    printf("PLG ___nombre de vue deja apprises = %d \n",nbr_imagettes);
    printf("PLG ___valeur du max trouvee = %f \n",max);
    printf("PLG ___Le gagnant est %d \n",(pos-deb)/increment);
#endif

    /*--------------------------------------*/
    /*On ne garde que les maximas >= a la vigilance neuro-modilee ou non*/  
    if(max>=vigi_neuro_mod)	
    {  
        neurone[pos].s1 = max;
        neurone[pos].s2 = 1.;
    }
    else 
    {
        if( nbr_imagettes<nbre2)
        {
#ifdef DEBUG
            printf("PLG Nouvelle vue a apprendre pour groupe %d\n",gpe);
#endif
            /*faudrait faire comme dans SAW*/
            n=(deb+nbr_imagettes); 
            neurone[n].s2 = 1.;
            neurone[n].s1 = 1.;

        }
        else
        {
            printf("PLG ERROR !!!!\n");
            printf("PLG Plus de place pour stocker une nouvelle vue dans le groupe plg %d \n",gpe);
            printf("PLG appuyer sur une touche pour continuer \n");
            getchar();getchar();
        }
    }

#ifdef DEBUG
    printf("sort gestion plg\n");
#endif
}


