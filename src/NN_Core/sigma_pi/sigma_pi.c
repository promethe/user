/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*

 Ca c'est du texte que j'ai rajouter pour verifier le merge sur la branche nico.

 Modif PG 20 janvier 2005:
 l'inhibition ne fait plus perdre un coup en supprimant toute activite sur le groupe.
 Elle n'a un effet que sur la memoire du passe (reset STM).
 Cela evite de faire perdre un "coup" et donc de gaspiller la reconnaissance
 du premier landmark (qui a priori est le plus saillant dans l'image!).

 La modif fonctionne sur le meme principe que celle faite par NC sur
 f_integr.

 */

/*------------------------------------------------------------*/
/*   Modeles de neurones biologiques                          */

/*------------------------------------------------------------*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include "../tools/include/macro.h"
/**differents types de synapses identities liees a des modes de calculs et d'apprntissage differents **/
#include <NN_Core/biol_neurons_kutta.h>
#include <Kernel_Function/find_input_link.h>

#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>
/*--------------------------------------------------*/
/*  GESTION DES NEURONES SIGMA PI                   */
/* les liens vers les micro-neurones
 (voies liaisons doivent etre du TYPE_SIGMA = 7 */
/*--------------------------------------------------*/
typedef struct MyData_sigma_pi {
  int gpe_transition_detect;
  int mode;
  int gpe_vigilence;
  float dicket_mem;
  float seuil_mem;
  int time_mem;
  int gpe_learn;
} MyData_sigma_pi;

/*deja declare dans biol_function.h      pierre.
 float rectification(float x,float seuil)
 {
 if(x <= seuil) return 0.;
 return ( x - seuil);
 }
 */

/*#define DEBUG */
/*#define DEBUG_NAME */
void learn_sigma(int n)
{
  type_coeff *coeff;

  coeff = neurone[n].coeff;
  while (coeff != NULL)
  {
    if (neurone[coeff->entree].s1 > 0.99) coeff->val = 1.;
    coeff = coeff->s; /* Lien suivant */
  }
}

void apprend_sigma_pi(int gpe)
{
  int deb, nbre, nbre2, increment, n, i;
  int mode;
  type_coeff *coeff;
  int coactivation; /* utilise pour savoir si l'apprentissage doit avoir lieu (=1) */

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;
  nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
  increment = nbre / nbre2;
#ifdef DEBUG_NAME
  dprints("~~~~~~~~~~~~~~~~~~~~~~~~~~~~ entee dans %s(%d) \n", __FUNCTION__,
      gpe);
#endif

  /*n: num de macro_neurone */
  for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
  {
    coactivation = neurone[n].flag; /* flag=0 pas d'apprentissage, 1 apprentissage ou -1 inhibition */
    /*i: num de micro_neurone relie au macro_neuron n */
    for (i = n - increment + 1; i < n; i++)
    {
      coeff = neurone[i].coeff;
      mode = liaison[coeff->gpe_liaison].mode;
      switch (mode)
      {
      case MACRO_LINKS: /* produit */
        break;
      case TYPE_SIGMA:
        if (coactivation == 1) learn_sigma(i);
        break;
      case INHIBITION:
        break;
      default:
        dprints("That link type (%d) does not exist for sigma-pi neurons of group %d (learning)\n", mode, gpe);
        exit(1);
      }
    }
  }
}

/*! renvoie le niveau d'activite de l'entree la plus active et actualise l'activite du micro_neurone(i) avec cet activite max */
float micro_neurone_sigma(int i)
{
  type_coeff *coeff, adr;
  float max = 0.;
  float produit, s;
  float max_entree = 0.;
#ifdef DEBUG
  int group_debug = 9;
#endif
  /*on recherche sur le neurone i l'entree la plus active. l'activite est place dans max
   et la sortie du neurone generant cet activite maximal est place dans max_entree */
  coeff = neurone[i].coeff;
  while (coeff != NULL)
  {
    adr = *coeff;
    s = neurone[adr.entree].s1;
#ifdef DEBUG
    if (group_debug == neurone[adr.entree].groupe)
    dprints("activity input neurons for lr = %f \n", s);
#endif
    produit = adr.val * s; /*activite du neurone multiplie par son poid synaptique */
    if (produit > max) max = produit;
    if (max_entree < s) max_entree = s;
    coeff = adr.s; /* Lien suivant */
  }

  /*le neurone i prend l'activite max trouvee si elle est >=0. Sinon, on la met a 0 */
  neurone[i].s = max;
  if (neurone[i].s < 0.) neurone[i].s = 0.;

  neurone[i].s2 = neurone[i].s1 = rectification(neurone[i].s, neurone[i].seuil);
  /* if(i==59449 || i==59450) dprints(" sigma %d  S2= %f, S1= %f , s= %f \n", i,neurone[i].s2, neurone[i].s1,neurone[i].s); */
  return max_entree;
}

/*Actualise la sortie du micro_neurone inhib (somme pondere classique)*/
float micro_neurone_inhib(int i)
{
  type_coeff *coeff;
  float somme = 0.;
  /*float max_entree=0.; */

  coeff = neurone[i].coeff;
  while (coeff != NULL)
  {
    somme = somme + coeff->val * neurone[coeff->entree].s1;
    coeff = coeff->s; /* Lien suivant */
  }

  neurone[i].s = somme;

  neurone[i].s2 = neurone[i].s1 = neurone[i].s;
  /*dprints("   S2= %f, S1= %f , s= %f, ds = %f \n", neurone[i].s2, neurone[i].s1,neurone[i].s,dsortie); */
  return somme;
}

/*! produit entre les voies positive et reset du passe si voies negatives actives.
 Ce micro neurone est le corps du macro neurone. 
 Neurone[i].s contient le passe ou l'activite analogique calculee.*/
void micro_neurone_pi2(int i);
void micro_neurone_pi1(int i);
void micro_neurone_pi(int i)
{
  if (((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->mode == 0) micro_neurone_pi1(i);
  else micro_neurone_pi2(i);
}

void micro_neurone_pi1(int i)
{
  type_coeff *coeff;
  float produit = 1.;
  float val_entree;
  float somme_neg = 0.;
  static float facteur_oubli = 1.0; /*0.9999; */
  int e;

  coeff = neurone[i].coeff;
  while (coeff != NULL)
  {
    e = coeff->entree;
    val_entree = neurone[e].s1;
    if (neurone[e].flag != -1) produit = produit * val_entree; /* si ce n'est pas un micro neurone inhibiteur */
    else somme_neg = somme_neg + val_entree; /* c'est un micro neurone inhibiteur */
    coeff = coeff->s; /* Lien suivant */
  }

  if (somme_neg < 0.) neurone[i].s = 0.; /* l'inhibition du passe qui  n'empeche pas les nouvelles entrees de s'exprimer */

  if (produit > neurone[i].s) neurone[i].s = produit;
  else neurone[i].s = facteur_oubli * neurone[i].s; /* oubli du passe avec fenetre glissante */

  neurone[i].s1 = neurone[i].s2 = rectification(neurone[i].s, neurone[i].seuil);
}
void micro_neurone_pi2(int i)
{
  type_coeff *coeff;
  float produit = 1.;
  float val_entree;
  float somme_neg = 0.;
  float facteur_oubli = 1.0; /*0.9999; */
  int e;
  float local_vigilence = vigilence;

  if (((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->gpe_vigilence != -1)
  {
    local_vigilence = neurone[def_groupe[((MyData_sigma_pi *) def_groupe[neurone[i].groupe].data)->gpe_vigilence].premier_ele].s1;
  }

  coeff = neurone[i].coeff;
  while (coeff != NULL)
  {
    e = coeff->entree;
    val_entree = neurone[e].s1;
    if (neurone[e].flag != -1) produit = produit * val_entree; /* si ce n'est pas un micro neurone inhibiteur */
    else somme_neg = somme_neg + val_entree; /* c'est un micro neurone inhibiteur */
    coeff = coeff->s; /* Lien suivant */
  }

  if (somme_neg < 0.) neurone[i].s = 0.; /* l'inhibition du passe qui  n'empeche pas les nouvelles entrees de s'exprimer */

  /*Si on detecte une  transition, le neurone subit l'oubli (s*dicket_mem) ou est resete en apprentissage (s*0) */
  /*sinon s*1 */

  if ((local_vigilence < 0.5) /*phase d'utilisation et reset */
  && isequal(neurone[def_groupe[((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->gpe_transition_detect].premier_ele].s, 1.)) /* phase de reset */
  {
    if (neurone[i].d > 0.) /* nombre iter avant reset memoire */
    {
      facteur_oubli = ((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->dicket_mem;
      neurone[i].d = neurone[i].d - 1.;
    }
    else facteur_oubli = 0.;
  }
  else if (isequal(neurone[def_groupe[((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->gpe_transition_detect].premier_ele].s, 1.)) /*reset en apprentissage */
  {
    facteur_oubli = 0.;
    neurone[i].d = 0.;
  }
  else facteur_oubli = 1; /* apprentissage (vig haute) sans reset */

  neurone[i].s = neurone[i].s * facteur_oubli;

  if (produit > neurone[i].s)
  {
    neurone[i].s = produit;
    neurone[i].d = (float) (((MyData_sigma_pi *) (def_groupe[neurone[i].groupe].data))->time_mem);
  }

  neurone[i].s1 = neurone[i].s2 = rectification(neurone[i].s, neurone[i].seuil);
}

void mise_a_jour_groupe_sigma_pi(int gpe)
{
  int mode;
  int i, n;
  int nl = 0, mode_reset = 0, time_mem = 1, gpe_transition_detect = -1, gpe_vigilence = -1, l = -1, gpe_learn = -1;
  float seuil_mem = 0.01, dicket_mem = 0., learn = 0.;
  MyData_sigma_pi *my_data;
  int increment, deb, nbre, nbre2; /* increment espace entre chaque macroneurone */
  type_coeff *coeff;
  int nbre_voies_somme = 0; /*nombre de microneurone qui se somme sur le macroneurone */
  float somme_voies_entrees_actives, lr;
  char param[32];

#ifdef DEBUG_NAME
  dprints("~~~~~~~~~~~~~~~~~~~~~~~entree dans %s(%d)\n", __FUNCTION__, gpe);
#endif
  if (def_groupe[gpe].data == NULL)
  {
    nl = 0;
    l = find_input_link(gpe, nl);
    while (l != -1)
    {
      if (strcmp(liaison[l].nom, "transition_detect") == 0)
      {
        mode_reset = 1;
        gpe_transition_detect = liaison[l].depart;
      }

      if (prom_getopt(liaison[l].nom, "d", param) == 2) dicket_mem = atof(param);
      if (prom_getopt(liaison[l].nom, "t", param) == 2) time_mem = atoi(param);
      if (prom_getopt(liaison[l].nom, "s", param) == 2) seuil_mem = atof(param);
      if (strcmp(liaison[l].nom, "vigilence") == 0) gpe_vigilence = liaison[l].depart;
      if (strcmp(liaison[l].nom, "learn") == 0) gpe_learn = liaison[l].depart;

      nl++;
      l = find_input_link(gpe, nl);
    }

    my_data = (MyData_sigma_pi *) malloc(sizeof(MyData_sigma_pi));
    def_groupe[gpe].data = (MyData_sigma_pi *) my_data;
    my_data->gpe_transition_detect = gpe_transition_detect;
    my_data->gpe_vigilence = gpe_vigilence;
    my_data->mode = mode_reset;
    my_data->dicket_mem = dicket_mem;
    my_data->time_mem = time_mem;
    my_data->seuil_mem = seuil_mem;
    my_data->gpe_learn = gpe_learn;
    dprints("param sigma_pi: mode=%d, d=%f, t=%d, s=%f\n", mode_reset, dicket_mem, time_mem, seuil_mem);
  }
  else
  {
  	my_data =  (MyData_sigma_pi *) (def_groupe[gpe].data);
  }

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre; /*120=TAILLEX(nbr_in +1)=30*(4+1) */
  nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley; /*30*1=30 */
  increment = nbre / nbre2; /*120/30=4 */



  if( my_data->gpe_learn == -1 ) learn = 1.0; 
  else
  {
    	learn = neurone[def_groupe[my_data->gpe_learn].premier_ele].s1;
  }

  /*Dans la bnoucle qui suit, n represnete l'indice des macro_neurone */
  for (n = deb + increment - 1; n < deb + nbre; n = n + increment) /*boucle pour les macroneurone */
  {
    somme_voies_entrees_actives = 0.;
    nbre_voies_somme = 0;

    /*Dans cette boucle, i est l'indice des micro_neurone en entree du macro_neurone n */
    for (i = n - increment + 1; i < n; i++) /*boucle pour les microneurone */
    {
      /*#ifdef DEBUG
       dprints("micro neuron %d\n",i-deb);
       #endif*/
      coeff = neurone[i].coeff;
      if (coeff != NULL) mode = liaison[coeff->gpe_liaison].mode;
      else mode = TYPE_SIGMA;
      switch (mode)
      {
      case MACRO_LINKS:
        dprints("%s:error, loop only for micro_neurons\n", __FUNCTION__);
        break;

      case TYPE_SIGMA:
        lr = micro_neurone_sigma(i); /*lr=activite du neurone du groupe precedant tel que lr*poid soit l'entree max du micro-neurone i */
        dprints("  voie %d : micro_neurone relie a : %d -- lr = %f \n", i, neurone[neurone[i].coeff->entree].groupe, lr);

        if (lr >= 0.)
        {
          somme_voies_entrees_actives = somme_voies_entrees_actives + lr;
          nbre_voies_somme = nbre_voies_somme + 1;
        }
        break;
      case INHIBITION:
        neurone[i].flag = -1;
        somme_voies_entrees_actives = somme_voies_entrees_actives + micro_neurone_inhib(i);
        break;
      default:
        dprints("That link type (%d) does not exist for sigma-pi. neurons of group %d \n", mode, gpe);
        exit(1);
      }
    }

    micro_neurone_pi(n);

    dprints("  condition d apprentissage :\n somme_voie_active = %f \n nbr voie somme = %d\n activation globale = %f\n neurone.s2 = %f \n ", somme_voies_entrees_actives, nbre_voies_somme, somme_voies_entrees_actives / ((float) nbre_voies_somme), neurone[n].s2);

    /* si globalement beaucoup d'entrees mais non apprises, le produit ne donne rien */
    if (somme_voies_entrees_actives / ((float) nbre_voies_somme) > 0.999 && neurone[n].s2 < 0.001 && learn > 0.99)
    {
      neurone[n].flag = 1; /* mais on declenche l'apprentissage */

      dprints("apprentissage possible, macro_neurone %d\n", (n - deb + 1) / 4);
      dprints("%f %d \n", somme_voies_entrees_actives, nbre_voies_somme);

      neurone[n].s = neurone[n].s1 = neurone[n].s2 = 1.;
    }
    else neurone[n].flag = 0; /* pas d'apprentissage */
  }

  (*def_groupe[gpe].appel_gestion)(gpe); /* ne fait rien */

}
