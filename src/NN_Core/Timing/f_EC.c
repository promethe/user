/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_EC.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Temporal derivate of the input signal.
 *   binary activity.
 *   return 1 when input activity decrease for the first time.
 *   good for detecting binary input signals.
 
 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>

typedef struct my_data_EC {
  int mode_s1;
} my_data_EC;

void new_function_EC(int gpe)
{
  int l=-1, i = 0, mode_s1 = 0;
  my_data_EC* p_my_data=NULL;

  p_my_data = ALLOCATION(my_data_EC);
  def_groupe[gpe].data = (void*) p_my_data;

  l = find_input_link(gpe, 0);
  while (l != -1)
  {
    if (prom_getopt(liaison[l].nom, "-s1", NULL) > 0)
    {
      mode_s1 = 1;
    }
    i++;
    l = find_input_link(gpe, i);
  }

  p_my_data->mode_s1 = mode_s1;
}

void function_EC(int gpe_sortie)
{
  int i; /*, j; */
  int DebutGpeEC_v2 = def_groupe[gpe_sortie].premier_ele;
  int TailleXEC_v2 = def_groupe[gpe_sortie].taillex;
  int TailleYEC_v2 = def_groupe[gpe_sortie].tailley;
  float Delta;
  float somme_entree;
  type_coeff *CoeffTemp;
  int mode_s1 = ((my_data_EC*) def_groupe[gpe_sortie].data)->mode_s1;

  for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleXEC_v2 * TailleYEC_v2; i++)
  {
    neurone[i].s2 = neurone[i].s; // s2 = passé
    CoeffTemp = neurone[i].coeff;
    somme_entree = 0.0;

    if (mode_s1 == 0)
    {
      while (CoeffTemp != NULL)
      {
        somme_entree += neurone[CoeffTemp->entree].s2;
        CoeffTemp = CoeffTemp->s;
      }
    }
    else
    {
      while (CoeffTemp != NULL)
      {
        somme_entree += neurone[CoeffTemp->entree].s1;
        CoeffTemp = CoeffTemp->s;
      }
    }
    neurone[i].s = somme_entree; // s = present
  }

  for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleXEC_v2 * TailleYEC_v2; i++)
  {
    Delta = neurone[i].s - neurone[i].s2;
    if (Delta > 0.0)
    {
      if (isdiff(neurone[i].s1, 1.0))
      {
        neurone[i].s2 = 1.0;
        neurone[i].s1 = 1.0;
      }
      else
      {
        neurone[i].s1 = 0.0;
        neurone[i].s2 = 0.0;
      }
    }
    else neurone[i].s1 = neurone[i].s2 = 0.;
  }

}

void destroy_function_EC(int gpe)
{
  my_data_EC* p_my_data=(my_data_EC*) def_groupe[gpe].data;;
  if (p_my_data!=NULL) free(p_my_data);
}
