/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libNN_Core
\defgroup DG DG


\section Author
    -name: Philippe Gaussier / Sorin Moga
    -Created: 1999 ? (P.A)
\section Modified
    - author: V. Riabchenko
    - description: on ajoute 'simul_speed' comme l'entree. Si il y a de lien avec le nom '-simul_speed',
      la sortie d'une group(ou ce lien commence) remplace la valeur du champ 'simulation_speed' du groupe DG.
      On a ajoute time_multiplier comme parametre d'entree.
    - date: 01/2014

    - author: J. Hirel
    - description: integration of all the DG models
    - date: 03/2009

    - author: C.Giovannangeli
    - description: specific file creation
    - date: 11/08/2004

    - author: A.Revel
    - description: comments "re"-added
    - date: 17/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description
Version ou l'activite de plusieurs lignes peut etre declenchee, avec reset des activites
    existantes

    Groupe de neurone modelisant les cellules granulaire de la structure hippocampique DG

    - Cette fonction cree un ensemble de tailley lignes de
    taillex neurones de DG

    - Chaque ligne de DG est connectee a une entree (theoriquement
    la sortie d'un groupe EC).

    Le modele est le suivant :

    Si une ou plusieurs entrees sont detectees (activite superieure au seuil du groupe),
    les lignes de neurones correspondantes sont activees, apres competition si le champ
    nbre_de_1 du groupe est renseigne. Les autres batteries de cellules sont remises a
    zero si le parametre reset est specifie en entree. Cette activation
    est basee sur une charge/decharge des neurones la composant ordonnes de la
    plus petite cellule vers la plus grosse (ce qui correspond
    respectivement a une cellule a constante de temps rapide et
    constante de temps lente).

    Ce systeme se comporte donc comme une decomposition temporelle du
    signal d'entree (memoire a court terme). Cette decomposition est
    utilisee pour apprendre une signature temporelle de la transition
    entre deux evenements.

    L'evolution temporelle des cellules de DG suivent une loi
    gaussienne dont la moyenne est 'mean', la variance
    'variance' et l'amplitude 'factor'. Le temps sur une ligne
    s'ecoule depuis un "top" de depart (timing_reset).

    Le decompte du temps se fait grace a une fonction algo
    f_TimeGenerator externe qui doit se trouver a la meme echelle
    de temps et qui met a jour la variable globale SystemTime.
    Un lien en entree nomme time_generator peut aussi specifie explicitement
    la boite TimeGenerator a utiliser.

    Cette version permet un control pousse des equations regissant l'activite
    des cellules granulaires.
    - Le parametre -i permet de specifier un temps de reaction pour la premiere
    cellule de chaque batterie, permettant ainsi de reagir sur des timing tres courts.
    - Le parametre -f permet de regler les equations controlant l'amplitude des gaussiennes
    utilisees pour modeliser les cellules granulaires.
    - Le parametre -v permet de regler les equations controlant la variance des gaussiennes.
    - Le parametre -m permet de regler les equations controlant la moyenne des gaussiennes,
    autrement dit leur repartition sur l'interval de temps specifie sur lequel DG reagit. Cet
    interval est regle par le champ simulation_speed du groupe.


\section Macro
-name

\section Local variables
-name

\section Global variables
-timeval SystemTime
-timeval time0

\section Internal Tools
-name

\section External Tools
-none

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <libx.h>
#include <sys/time.h>
#include <stdlib.h>

#include <Struct/DG_data.h>
#include <Global_Var/NN_Core.h>
#include <Typedef/boolean.h>

#define DEBUG

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

void new_DG(int gpe)
{
  char param_link[255];
  int i, l, ret;
  DG_data_2 *data = NULL;

  int gpe_input = -1;
  int sizex = def_groupe[gpe].taillex;
  int sizey = def_groupe[gpe].tailley;
  int first = def_groupe[gpe].premier_ele;
  float battery_period = def_groupe[gpe].simulation_speed;

  if (def_groupe[gpe].data == NULL)
  {
    data = (DG_data_2 *) malloc(sizeof(DG_data_2));

    if (data == NULL)
    {
      fprintf(stderr, "ERROR in DG(%s): malloc failed for data\n", def_groupe[gpe].no_name);
      exit(1);
    }

    data->cells = NULL;
    data->batteries = NULL;

    data->reset = FALSE;
    data->real_time = FALSE;

    data->factor_type = 'r';
    data->factor_param = 1.0;
    data->mean_type = 'l';
    data->variance_type = 'i';
    data->variance_param = 0.04;
    data->timing_first_cell = -1;
    data->min_activity = 0.;

    data->gpe_time_multiplier = -1;
    data->gpe_time_generator = -1;
    data->gpe_simul_speed = -1;

    /* Cells defining the equations for all the batteries in DG */
    data->cells = (DG_cell *) malloc(sizex * sizeof(DG_cell));

    /* Batteries of cells */
    data->batteries = (DG_battery *) malloc(sizey * sizeof(DG_battery));

    if (data->cells == NULL || data->batteries == NULL)
    {
      fprintf(stderr, "ERROR in DG(%s): Memory allocation failed!\n", def_groupe[gpe].no_name);
      exit(1);
    }

    /* Finds input links */
    l = 0;
    i = find_input_link(gpe, l);
    while (i != -1)
    {
      if (strcmp(liaison[i].nom, "simul_speed") == 0)
      {
        data->gpe_simul_speed = liaison[i].depart;
      }
      else
      if (strcmp(liaison[i].nom, "time_multiplier") == 0)
      {
        data->gpe_time_multiplier = liaison[i].depart;
      }
      else
      if (strcmp(liaison[i].nom, "time_generator") == 0)
      {
        data->gpe_time_generator = liaison[i].depart;
      }
      else
      {
      gpe_input = liaison[i].depart;

      /* Parameter setting the reset of the batteries when a new input is detected */
      if (prom_getopt(liaison[i].nom, "reset", param_link) == 1)
      {
        dprints("DG(%s): reset = TRUE\n", def_groupe[gpe].no_name);
        data->reset = TRUE;
      }

      if (prom_getopt(liaison[i].nom, "rt", param_link) == 1)
      {
        dprints("DG(%s): real_time = TRUE\n", def_groupe[gpe].no_name);
        data->real_time = TRUE;
      }

      if (prom_getopt(liaison[i].nom, "i", param_link) == 2)
      {
        data->timing_first_cell = atof(param_link);
        dprints("new_DG(%s): Timing of the first cell -> %f\n", def_groupe[gpe].no_name, data->timing_first_cell);
      }

      if (prom_getopt(liaison[i].nom, "min", param_link) == 2)
      {
        data->min_activity = atof(param_link);
        dprints("new_DG(%s): Minimum activity -> %f\n", def_groupe[gpe].no_name, data->min_activity);
      }

      if (prom_getopt(liaison[i].nom, "f", param_link) == 2)
      {
        ret = sscanf(param_link, "%c%f", &data->factor_type, &data->factor_param);

        if (ret != 2)
        {
          fprintf(stderr, "ERROR in new_DG(%s): incorrect format for option '-f' -> must be -f[char][float]\n", def_groupe[gpe].no_name);
          exit(1);
        }

        dprints("new_DG(%s): Parameters for the decay of the factors of the gaussians -> type = %c, param = %f\n", def_groupe[gpe].no_name, data->factor_type, data->factor_param);
      }

      if (prom_getopt(liaison[i].nom, "v", param_link) == 2)
      {
        ret = sscanf(param_link, "%c%f", &data->variance_type, &data->variance_param);

        if (ret != 2)
        {
          fprintf(stderr, "ERROR in new_DG(%s): incorrect format for option '-v' -> must be -f[char][float]\n", def_groupe[gpe].no_name);
          exit(1);
        }

        dprints("new_DG(%s): Parameters for the growth of the variances of the gaussians -> type = %c, param = %f\n", def_groupe[gpe].no_name, data->variance_type, data->variance_param);
      }

      if (prom_getopt(liaison[i].nom, "m", param_link) == 2)
      {
        data->mean_type = param_link[0];
        dprints("new_DG(%s): Parameters for the means of the gaussians -> type = %c\n", def_groupe[gpe].no_name, data->mean_type);
      }

      }
      l++;
      i = find_input_link(gpe, l);
    }

    if (gpe_input == -1)
    {
      fprintf(stderr, "ERROR in DG(%s): No input link !\n", def_groupe[gpe].no_name);
      exit(1);
    }

    /* Test for correct sizes */
    if (sizey != def_groupe[gpe_input].tailley)
    {
      fprintf(stderr, "ERROR in DG(%s): The sizes of DG and its input group are different !\n", def_groupe[gpe].no_name);
      exit(1);
    }

    if (data->gpe_simul_speed == -1)
    {
    /* Setting the parameters for the gaussians used to simulate the cells in a battery */
    if (data->timing_first_cell <= 0.)
    data->timing_first_cell = (float) battery_period / sizex;

    /* Setting the means, factors and variances of the gaussians */
    for (i = 0; i < sizex; i++)
    {
      switch (data->mean_type)
      {
        /* Calculates the means with an exponential placing */
        case 'e':
          data->cells[i].mean = pow(battery_period - data->timing_first_cell + 1, (float) i / (sizex - 1)) + data->timing_first_cell - 1;
          break;

        /* Calculates the means linearly */
        case 'l':
          data->cells[i].mean = (float) i * (battery_period - data->timing_first_cell) / (sizex - 1) + data->timing_first_cell;
          break;

        default:
          fprintf(stderr, "ERROR in new_DG(%s): Unknown mean type (parameter -m = %c)\n", def_groupe[gpe].no_name, data->mean_type);
          exit(1);
      }


      switch (data->factor_type)
      {
        /* Calculates the factor using a ratio of the means */
        case 'r':
          data->cells[i].factor = (float) data->cells[0].mean / data->cells[i].mean ;
          break;

        /* Calculates the factor using the inverse of the index of the cell */
        case 'i':
          data->cells[i].factor = 1. / (i + 1);
          break;

        /* Calculates the factor using a linear decay */
        case 'l':
          data->cells[i].factor = 1 - data->cells[i].mean * data->factor_param / battery_period;
          if (data->cells[i].factor < 0.)
            data->cells[i].factor = 0.;
          break;

        default:
          fprintf(stderr, "ERROR in new_DG(%s): Unknown factor type (parameter -f = %c)\n", def_groupe[gpe].no_name, data->factor_type);
          exit(1);
      }

      switch (data->variance_type)
      {
        /* Calculates the variance based on the index of the cell */
        case 'i':
          data->cells[i].variance = (i + 1) * battery_period * data->variance_param;
          break;

        /* Calculates the variance based on the mean of the cell */
        case 'm':
          data->cells[i].variance = data->cells[i].mean * battery_period * data->variance_param;
          break;

        /* Constant variance */
        case 'c':
          data->cells[i].variance = battery_period * data->variance_param;
          break;

        default:
          fprintf(stderr, "ERROR in new_DG(%s): Unknown variance type (parameter -v = %c)\n", def_groupe[gpe].no_name, data->variance_type);
          exit(1);
      }

      data->cells[i].variance = data->cells[i].variance * data->cells[i].variance;
      dprints("new_DG(%s): Parameters for cell %d -> mean = %f, factor = %f, variance = %f\n",
      def_groupe[gpe].no_name, i, data->cells[i].mean, data->cells[i].factor, data->cells[i].variance);
    }
    }

    /* Setting the initial reset times of the batteries */
    for (i = sizey; i--;)
    {
      if (data->real_time == FALSE)
      {
        data->batteries[i].reset_time.tv_sec = 0;
        data->batteries[i].reset_time.tv_usec = 0;
      }
      else
      {
        gettimeofday(&data->batteries[i].reset_time, NULL);
      }
      data->batteries[i].activation = FALSE;
    }

    /* Number of winning inputs allowed to simultaneously activate batteries */
    if (def_groupe[gpe].nbre_de_1 >= 1)
      data->nb_of_1 = def_groupe[gpe].nbre_de_1;
    else
      data->nb_of_1 = sizey;

    data->gpe_input = gpe_input;
    def_groupe[gpe].data = data;

    /* Initializing activities to 0 */
    for (i = def_groupe[gpe].nbre; i--;)
      neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0.0;
  }
}

/* Fonctions used for backward compatibility */
/* They are used to emulate the old functions f_DG_1, f_DG_t2 and f_DG_v3 */
void new_DG_1(int gpe)
{
  new_DG(gpe);
  ((DG_data_2 *) def_groupe[gpe].data)->reset = FALSE;
  ((DG_data_2 *) def_groupe[gpe].data)->nb_of_1 = 1;
}

void new_DG_t2(int gpe)
{
  new_DG(gpe);
  ((DG_data_2 *) def_groupe[gpe].data)->reset = TRUE;
  ((DG_data_2 *) def_groupe[gpe].data)->nb_of_1 = 1;
}

void new_DG_v3(int gpe)
{
  new_DG(gpe);
  ((DG_data_2 *) def_groupe[gpe].data)->reset = TRUE;
  ((DG_data_2 *) def_groupe[gpe].data)->nb_of_1 = def_groupe[gpe].tailley;
}



void function_DG(int gpe)
{

  int gpe_input, gpe_time_generator;
  int i, j, ind_max;
  int nb_of_1 = 1;
  float local_time, max;
  struct timeval StructTime;
  DG_data_2 *data;
  boolean input_activity = FALSE;
  int first = def_groupe[gpe].premier_ele;
  int sizex = def_groupe[gpe].taillex;
  int sizey = def_groupe[gpe].tailley;

  data = (DG_data_2 *) def_groupe[gpe].data;
  gpe_input = data->gpe_input;
  gpe_time_generator = data->gpe_time_generator;
  nb_of_1 = data->nb_of_1;

/* Gets the current time */

  if (data->real_time == TRUE)
  {
    /* if compiled with REAL_TIME we use the clock time */
    gettimeofday(&StructTime, NULL);
  }
  else
  {
    /* If no time_generator is specified as input, we use the global variable SystemTime */
    if (gpe_time_generator == -1)
    {
      StructTime.tv_sec = SystemTime.tv_sec;
      StructTime.tv_usec = SystemTime.tv_usec;
    }
    else
    {
      if (def_groupe[gpe_time_generator].ext == NULL)
      {
        fprintf(stderr, "ERROR in DG(%s) : NULL pointer for ext in time_generator group\n", def_groupe[gpe].no_name);
        exit(1);
      }

      StructTime.tv_sec = ((struct timeval *) def_groupe[gpe_time_generator].ext)->tv_sec;
      StructTime.tv_usec = ((struct timeval *) def_groupe[gpe_time_generator].ext)->tv_usec;
    }
  }



  if (data->gpe_simul_speed != -1)
  {
    float battery_period = neurone[def_groupe[data->gpe_simul_speed].premier_ele].s;
  /* Setting the parameters for the gaussians used to simulate the cells in a battery */
  if (data->timing_first_cell <= 0.)
  data->timing_first_cell = (float) battery_period / sizex;

  /* Setting the means, factors and variances of the gaussians */
  for (i = 0; i < sizex; i++)
  {
    switch (data->mean_type)
    {
      /* Calculates the means with an exponential placing */
      case 'e':
        data->cells[i].mean = pow(battery_period - data->timing_first_cell + 1, (float) i / (sizex - 1)) + data->timing_first_cell - 1;
        break;

      /* Calculates the means linearly */
      case 'l':
        data->cells[i].mean = (float) i * (battery_period - data->timing_first_cell) / (sizex - 1) + data->timing_first_cell;
        break;

      default:
        fprintf(stderr, "ERROR in new_DG(%s): Unknown mean type (parameter -m = %c)\n", def_groupe[gpe].no_name, data->mean_type);
        exit(1);
    }


    switch (data->factor_type)
    {
      /* Calculates the factor using a ratio of the means */
      case 'r':
        data->cells[i].factor = (float) data->cells[0].mean / data->cells[i].mean ;
        break;

      /* Calculates the factor using the inverse of the index of the cell */
      case 'i':
        data->cells[i].factor = 1. / (i + 1);
        break;

      /* Calculates the factor using a linear decay */
      case 'l':
        data->cells[i].factor = 1 - data->cells[i].mean * data->factor_param / battery_period;
        if (data->cells[i].factor < 0.)
          data->cells[i].factor = 0.;
        break;

      default:
        fprintf(stderr, "ERROR in new_DG(%s): Unknown factor type (parameter -f = %c)\n", def_groupe[gpe].no_name, data->factor_type);
        exit(1);
    }

    switch (data->variance_type)
    {
      /* Calculates the variance based on the index of the cell */
      case 'i':
        data->cells[i].variance = (i + 1) * battery_period * data->variance_param;
        break;

      /* Calculates the variance based on the mean of the cell */
      case 'm':
        data->cells[i].variance = data->cells[i].mean * battery_period * data->variance_param;
        break;

      /* Constant variance */
      case 'c':
        data->cells[i].variance = battery_period * data->variance_param;
        break;

      default:
        fprintf(stderr, "ERROR in new_DG(%s): Unknown variance type (parameter -v = %c)\n", def_groupe[gpe].no_name, data->variance_type);
        exit(1);
    }

    data->cells[i].variance = data->cells[i].variance * data->cells[i].variance;
    dprints("new_DG(%s): Parameters for cell %d -> mean = %f, factor = %f, variance = %f\n",
    def_groupe[gpe].no_name, i, data->cells[i].mean, data->cells[i].factor, data->cells[i].variance);
  }
  }





  /* Updates the activites of all the neurons */
  for (i = sizey; i--;)
  {
    /* Time elapsed for this battery since it has been activated */
    local_time = (StructTime.tv_sec - data->batteries[i].reset_time.tv_sec) +
    (StructTime.tv_usec - data->batteries[i].reset_time.tv_usec) * 1e-6;

    if(data->gpe_time_multiplier != -1)
    {
      if(neurone[def_groupe[data->gpe_time_multiplier].premier_ele].s > 0.0001)
        local_time /= neurone[def_groupe[data->gpe_time_multiplier].premier_ele].s;
      else
        local_time = 0;
    }

    /* Only activated batteries can have an output */
    if (data->batteries[i].activation == TRUE)
    {
      for (j = sizex; j--;)
      {
        neurone[first + i * sizex + j].s = neurone[first + i * sizex + j].s1 = neurone[first + i * sizex + j].s2 = data->min_activity + (float) data->cells[j].factor * (1 - data->min_activity) * exp((local_time - data->cells[j].mean) * (data->cells[j].mean - local_time) / data->cells[j].variance);
      }
    }
    else
    {
      for (j = sizex; j--;)
      {
        neurone[first + i * sizex + j].s = neurone[first + i * sizex + j].s1 = neurone[first + i * sizex + j].s2 = 0.0;
      }
    }

    /* Resets information about winners for the next step */
    data->batteries[i].winner = FALSE;
  }

  /* Handles the activation of the batteries by the input group */
  for (i = 0; i < nb_of_1; i++)
  {
    /* Input activity must be above the threshold to activate the corresponding battery */
    max = def_groupe[gpe].seuil;
    ind_max = -1;

    for (j = sizey; j--;)
    {
      if (data->batteries[j].winner == FALSE && neurone[def_groupe[gpe_input].premier_ele + j].s1 > max)
      {
        max = neurone[def_groupe[gpe_input].premier_ele + j].s1;
        ind_max = j;
      }
    }

    /* Reseting the battery correspnding to the winning input */
    if (ind_max >= 0)
    {
      dprints("DG(%s): Winning input neuron, index = %d\n", def_groupe[gpe].no_name, ind_max);
      data->batteries[ind_max].winner = TRUE;

      data->batteries[ind_max].activation = TRUE;
      data->batteries[ind_max].reset_time.tv_sec = StructTime.tv_sec;
      data->batteries[ind_max].reset_time.tv_usec = StructTime.tv_usec;

      input_activity = TRUE;
    }
    else
      break;
  }

  /* if the reset option has been set to true, we reset all the non-activated batteries */
  if (input_activity == TRUE && data->reset == TRUE)
  {
    dprints("DG(%s): Input detected, resetting all batteries\n", def_groupe[gpe].no_name);
    for (i = sizey; i--;)
    {
      if (data->batteries[i].winner == FALSE)
        data->batteries[i].activation = FALSE;
    }
  }
}

void destroy_DG(int gpe)
{
  DG_data_2 *data = (DG_data_2 *) def_groupe[gpe].data;
  if (data != NULL)
  {
    free(data->batteries);
    data->batteries = NULL;

    free(data->cells);
    data->cells = NULL;

    free(data);
    def_groupe[gpe].data = NULL;
  }
  dprints("destroy_DG(%s): Leaving function\n", def_groupe[gpe].no_name);
}
