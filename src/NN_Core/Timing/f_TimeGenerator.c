/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_TimeGenerator.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-timeval SystemTime
-timeval time0

Global variables:
-none

Internal Tools:
-none

External Tools: 
-NN_Core/hippo/function_InitialiseHippo

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <sys/time.h>
#include <stdlib.h>

#include <net_message_debug_dist.h>
#include <Global_Var/NN_Core.h>
#include <NN_Core/Timing.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct t_TimeGenerator_data
{
  float pause_treshold;
	int pause_mode;
	int pause_input_group;
} TimeGenerator_data;

void function_TimeGenerator(int gpe_sortie)
{
  float	temp = 0;
  int i = 0, j = 0;
  long TimeStep;
	char param[256];
  type_coeff *coeff = NULL;
	TimeGenerator_data *data = NULL;

	/* Pause : met le temps en pause si un lien "Pause" est trouve
	 *   et si le groupe en entree a une activite inferieure � 1
	 */
  if(def_groupe[gpe_sortie].data == NULL)
	{
	  data = (TimeGenerator_data *)malloc(sizeof(TimeGenerator_data));
    data->pause_mode = 0;
    data->pause_input_group = -1;
		data->pause_treshold = 1.0;

    while((j = find_input_link(gpe_sortie, i++)) != -1)
    {
	    if(prom_getopt(liaison[j].nom, "-pause", param) == 2)
      {
        sscanf(param, "%f", &(data->pause_treshold));
				data->pause_input_group = liaison[j].depart;
				data->pause_mode = 1;
      }
			else if(prom_getopt(liaison[j].nom, "-pause", param) == 1)
      {
				data->pause_input_group = liaison[j].depart;
        data->pause_mode = 1;
      }
    }

    def_groupe[gpe_sortie].data = (TimeGenerator_data *)data;
	}

  data = (TimeGenerator_data *)def_groupe[gpe_sortie].data;

  if(data->pause_mode)
  {
    temp = 0.0;
    j = def_groupe[data->pause_input_group].premier_ele
		      + def_groupe[data->pause_input_group].nbre;
    for (i = def_groupe[data->pause_input_group].premier_ele ; i < j ; i++)
    {
      temp += neurone[i].s1;
    }

    if(temp > data->pause_treshold)
      return;
	}

  /* defining clock tic, in  microsecondes     */
  /* A la place de 100*1e3 pour avoir une vitesse de simulation donnee
	 * en parametre dans le .script (variable simulation_speed de ce groupe)
	 *   m.hersch juillet 04
	 */
  TimeStep = def_groupe[gpe_sortie].simulation_speed * 1e3;

  coeff = neurone[def_groupe[gpe_sortie].premier_ele].coeff;
  if (coeff != NULL)
  {
    for (temp = 0.0; coeff != NULL; coeff = coeff->s)
    {
      temp += (coeff->val * neurone[coeff->entree].s2);
    }
    TimeStep *= temp;
  }
  /* updating clock acording time environment  */

  /* REAL TIME is not defined */
  #ifndef REAL_TIME
  /* ajout m.hersch pour l�nitialisation */
  if (!def_groupe[gpe_sortie].ext)
    function_InitialiseHippo(gpe_sortie);

  SystemTime.tv_usec += TimeStep;
  if (SystemTime.tv_usec >= 1e+6)
  {
    SystemTime.tv_usec -= 1e+6;
    SystemTime.tv_sec++;
  }
	/* REAL TIME is defined */
  #else
	dprints("*****************\n REAL_TIME \n *****************\n");
  #endif
}
