/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\file
\brief 

Author: Pierre Andry / Arnaud Revel
Created: 26/11/2004
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Fonction permettant de creer des lignes a retard servant
   de memoire a court terme du signal d'entree

- Cette fonction cree un ensemble de tailley lignes de
  taillex neurones 

- Chaque ligne est connectee a une entree (theoriquement
  la sortie d'un groupe EC).

Le modele est le suivant :

Si une entree est detectee, la ligne de neurones
correspondante est activee. Cette activation est basee
sur une charge/decharge des neurones la composant ordonnes de la
plus petite cellule vers la plus grosse (ce qui correspond
respectivement a une cellule a constante de temps rapide et
constante de temps lente).

Ce systeme se comporte donc comme une decomposition temporelle du
signal d'entree (memoire a court terme). Cette decomposition est
utilisee pour apprendre une signature temporelle de la transition
entre deux evenements.

L'evolution temporelle des cellules de la base de temps suivent une loi
lineaire avec un seuile en Theta. Tant que l'entree est activee, les
cellules de la base de temps emmagasinent de l'energie et voient leur
activite s'acroitre. La disparition du signal d'entree engendre une
decroissance lineraire de toutes les cellules (avec une saturation a 0).


Le decompte du temps se fait grace a une fonction algo
f_TimeGenerator externe qui doit se trouver a la meme echelle
de temps et qui met a jour la variable globale SystemTime.

Macro:
-none

Local variables:
-none

Global variables:
-timeval SystemTime

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <sys/time.h>
#include <stdlib.h>

/*#include <Struct/DG_data.h>*/

#include <Typedef/boolean.h>
#include <Global_Var/NN_Core.h>

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/* A METTRE DANS UN STRUCT.H !!!!*/
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
typedef struct st_base_de_temps
{
    float *alpha;               /* Pente des cellules de base de temps */
} base_de_temps;

void function_base_de_temps(int gpe_sortie)
{
    struct timeval temps_courant;

    int i = 0;                  /* Variable d'index ;) */
    static double T0 = 0.;      /* Temps lors de l'appel precedent */
    double T1 = 0.;             /* Temps present */
    double DeltaT = 0.;         /* Temps ecoule entre 2 appels de la boite */
    base_de_temps *bt;          /* Structure contenant les parametres de la base de temps */
    int DebutGroupe = def_groupe[gpe_sortie].premier_ele;
    type_coeff *lien = NULL;
    float activite_entree = 0.; /* Activite du neurone d'entree */
    int signe = 0;              /* Le signe de la varitation de l'activite en entree */
    float nouvelleActivite = 0.;    /* Amplitude de la variation */

    /* Horizon temporel de la base */
    float TimeHorizon = def_groupe[gpe_sortie].simulation_speed;

    /* Seuil des cellules de la base de temps */
    float Theta = def_groupe[gpe_sortie].seuil;

    /* Nombre de lignes dans le groupe */
    int nb_lignes = def_groupe[gpe_sortie].tailley;

    /* Nombre de cellules par lignes */
    int nb_cellules = def_groupe[gpe_sortie].taillex;

  /********************/
    /* Gestion du temps */
  /********************/
#ifdef REAL_TIME
    err = gettimeofday(&temps_courant, (void *) NULL);

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        T0 = (double) temps_courant.tv_sec +
            1e-6 * (double) temps_courant.tv_usec;
    }
#else
    temps_courant.tv_sec = SystemTime.tv_sec;
    temps_courant.tv_usec = SystemTime.tv_usec;
#endif

    T1 = (double) temps_courant.tv_sec +
        1e-6 * (double) temps_courant.tv_usec;

    DeltaT = T1 - T0;

  /*******************************************************/
    /* Initialisation des structures lors du premier appel */

    if (def_groupe[gpe_sortie].ext == NULL)
    {

        /* RAZ des neurones de gestion de la base de temps */
        for (i = DebutGroupe; i < DebutGroupe + (nb_lignes * nb_cellules);
             i++)
        {
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
        }

        /* Initialisation de la structure gerant la base de temps */
        bt = (base_de_temps *) malloc(sizeof(base_de_temps));
        if (bt == NULL)
        {
            perror("Malloc dans f_base_de_temps (bt)");
            exit(1);
        }

        bt->alpha = (float *) malloc((nb_cellules) * sizeof(float));
        if (bt->alpha == NULL)
        {
            perror("Malloc dans f_base_de_temps (bt->alpha)");
            exit(1);
        }

        for (i = 0; i < nb_cellules; i++)
        {
            /* Le alpha est calcule de sorte que la
               derniere cellule atteint le seuil en
               fin de l'horizon de prediction */
            bt->alpha[i] = nb_cellules * Theta / (TimeHorizon * (i + 1));
            /* printf("alpha[%d]=%f\n",i,bt->alpha[i]); */
        }
        def_groupe[gpe_sortie].ext = bt;
        DeltaT = 0;
    }
  /************************************************************************/
    /*       Fin du code execute uniquement la premiere fois                */
  /************************************************************************/


  /***********************************************************************/
    /* Le code jusqu'a la fin est execute a chaque iteration               */
  /***********************************************************************/
    else
        bt = (base_de_temps *) def_groupe[gpe_sortie].ext;

    for (i = DebutGroupe; i < DebutGroupe + (nb_lignes * nb_cellules); i++)
    {
        /* On recupere le lien avec le neurone d'entree */
        lien = neurone[i].coeff;

        if (lien == NULL)
        {
            perror("Lien non initialise");
            exit(1);
        }

        /* On travaille sur l'activite apres competition
           mais sans diffusion */
        activite_entree = neurone[lien->entree].s1;

        /* La pente de la courbe depend de la presence (+)
           ou de l'absence (-) d'entree */

        signe = (activite_entree > 0 ? 1 : -1);

        nouvelleActivite =
            neurone[i].s2 +
            signe * bt->alpha[(i - DebutGroupe) % nb_cellules] * DeltaT;
        nouvelleActivite =
            (nouvelleActivite > Theta ? Theta : nouvelleActivite);
        nouvelleActivite = (nouvelleActivite < 0. ? 0. : nouvelleActivite);

        if (isequal(activite_entree, 0.))
            nouvelleActivite = 0.;

        neurone[i].s = signe;   /* Sauvegarde du sens de variation de l'activite du neurone */
        neurone[i].s1 = neurone[i].s2;  /* On reporte l'activite a l'instant precedent */
        neurone[i].s2 = nouvelleActivite;   /* Activite en cours */
    }

  /************************************************/
    T0 = T1;                    /* Sauvegarde de l'instant precedent */
}
