/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*! \file  macro_colonne.c
 *  \brief Neurons Macro colonne
 *
 *   This kind of macro neurons simulate (in a simplified view)
 *   the behavior of the micro columns of the cortex.
 *   Functionnaly, each macro neuron is linked to 2 types
 *   of micro neurons
 *
 *   - a population of DIST micro neurons behaving as a kohonen
 *     1D network.
 *   - one SUP micro neuron  receives stimulations from an Unconditional
 *     stimulus. The Sup micro neuron's activity supervise the learning
 *    of the DIST population.
 *
 *   The latteral interactions between DIST micro neurons id done according
 *   gaussian factors, initialised in initialise_gaussienne() function
 *   (macro_neurone.h)
 *
 *   The mode of the links between the neurons
 *   indicate the type of the neuron.
 *   The link's type is set in the mode field of the link:
 *   - 0 Macro neuron
 *   - 1 Micro neuron DIST
 *   - 7 Micro neuron SUP
 *
 *   (see macro_colonne.h)
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <NN_Core/macro_colonne.h>


#define Seuil_Sup 0.2



/*! Update of the links of the ith DIST micro neuron.
 * - Learning equation:
 * \f$ \delta W_{ij}= \epsilon \cdot (W_{ij}-S_i) \cdot DOG( d( i , winner)) \f$
 *
 * - \f$ \epsilon \f$ is the leaning rate specified by the user in the 'learning rate' field.
 * - \f$ d(a,b) \f$ ditance value between a and b DIST neurons. This is the usual
 *  competition/diffusion mecanism using Gaussian or DOF factors s defined by the dvp and dvn fields
 *   in the .script file
 */

void apprend_coeffs(int i, float distance, float l_rate)
{
   type_coeff *coeff;
   float Si, Sj, dc;

   coeff = neurone[i].coeff;
   while (coeff != NULL)
   {
      if (coeff->evolution == 1)
      {
         Si = coeff->val;
         Sj = neurone[coeff->entree].s2;
         dc = l_rate * (Sj - Si) * distance;
         if ((coeff->val + dc >= 0.0) && (coeff->val + dc <= 1.0))
            coeff->val = coeff->val + dc;
      }
      coeff = coeff->s;
   }
}


/* Learning the DIST population linked to
 * the nth Macro neuron.
 * The DIST pupulation is a 1D kohonen map
 * (latteral interactions).
 * The position of each micro neuron is in the
 * posx field of the neuron, (calculated in the
 * initialise_gaussiennes() function in macro_neurone.c)
 * tableau_gaussienne[] conains the factors of the
 * gaussian or the DOG function decribing the strengh
 * of the inhibition.
 */
void apprend_micro(int n, int gpe, int increm, float l_rate)
{
   type_coeff *coeff;
   int i, mode, winner = -1;
   float max = -999999;
   double dist;
   float distance;

   /* printf("apprend micro l_rate %f macro neurone %d \n",l_rate,n); */

   /*selection du winner */
   for (i = n - increm; i < n; i++)
   {
      coeff = neurone[i].coeff;
      mode = liaison[coeff->gpe_liaison].mode;
      if (mode == MICRO_DIST)
      {
         if (max < neurone[i].s2)
         {
            max = neurone[i].s2;
            winner = i;
         }

      }
   }

   if (winner == -1)
   {
      printf("ERROR winner= -1 in macro_colonne.c \n");
      exit(0);
   }


   /* calcul des distances*/
   for (i = n - increm; i < n; i++)
   {
      coeff = neurone[i].coeff;
      mode = liaison[coeff->gpe_liaison].mode;
      if (mode == MICRO_DIST) /*selection du winner */
      {
         dist = fabs((double) (neurone[i].posx - neurone[winner].posx));
         distance = def_groupe[gpe].tableau_gaussienne[(int) dist];
         apprend_coeffs(i, distance, l_rate);
      }
   }
}






/*! If an Unconditional Stimulus occurs
 *  (macro neuron's flag field equal to 1)
 *  Then the learning of correponding DIST popullation
 *  is performed.
 */
void apprend_macro_colonne(int gpe)
{
   int n;
   float  l_rate;
   int increment, deb, nbre, nbre2;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;
   l_rate = def_groupe[gpe].learning_rate * eps;   /* modif PG */
   for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
   {

      if (neurone[n].flag)
         apprend_micro(n, gpe, increment - 1, l_rate);

   }
}



void mise_a_jour_groupe_macro_colonne(int gpe, int gestion_STM)
{
   UNUSED_PARAM gpe; UNUSED_PARAM gestion_STM;

   printf("mise_a_jour_groupe_macro_colonne\n");
}


/*! Activation of the SUP micro neuron.
 *  If A unsupervised Stimulus is present at input
 *  (\f$Input_US * Weight> SeuilSup \f$),
 *  Then leaning can begin
 *  i.e. the corresponding Macro neuron's flag is set to 1
 */

void supervision_locale(int i)
{
   type_coeff *coeff;
   int nbre, nbre2, increment, deb;
   int pas;

   deb = def_groupe[neurone[i].groupe].premier_ele;
   nbre = def_groupe[neurone[i].groupe].nbre;
   nbre2 = def_groupe[neurone[i].groupe].taillex * def_groupe[neurone[i].groupe].tailley;
   increment = nbre / nbre2;
   pas = (i - deb) / increment;
   pas = deb + (increment * pas) + increment - 1;

   neurone[pas].flag = 0;

   coeff = neurone[i].coeff;
   if ((neurone[coeff->entree].s2 * coeff->val) > Seuil_Sup)  neurone[pas].flag = 1;

}



/*!Computing the potential (s field) of a DIST neuron
 * Distance betwen weights and an input vector
 */
void distance_locale(int i)
{
   type_coeff *coeff;
   float dist = 0.;

   coeff = neurone[i].coeff;
   while (coeff != NULL)
   {
      dist +=
         (float) fabs((double) (coeff->val - neurone[coeff->entree].s2));
      coeff = coeff->s;
   }
   neurone[i].s = neurone[i].s1 = neurone[i].s2 = 1. / (1. + dist);
}



/* Updating neurons of a macro_column group.
 * The mode of the links between the neurons
 * indicate the type of the neuron.
 * The link's type is set in the mode field of the link:
 * - 0 Macro neuron
 * - 1 Micro neuron DIST
 * - 7 Micro neuron SUP
 */
void mise_a_jour_neurone_macro_colonne(int i, int gestion_STM, int learn)
{
   type_coeff *coeff;
   int mode;

   UNUSED_PARAM learn; UNUSED_PARAM gestion_STM;

   coeff = neurone[i].coeff;
   mode = liaison[coeff->gpe_liaison].mode;
   /*printf(".");     */
   switch (mode)
   {
      case MACRO_MC:
         break;
      case MICRO_DIST:
         distance_locale(i);
         break;
      case MICRO_SUPERVISOR:
         supervision_locale(i);
         break;
      default:
         EXIT_ON_ERROR("That link type (%d) does not exist for macro_colonne. \n", mode);
   }
}



static int maj = 0;


/*! Computes the potential of the Macro neuron.
 *  according to the micro neuron SUP activity (updating
 *  the flag field).
 *
 *  - flag equal to 1 : Potential is equal to 1.
 *  - flag equal to 0 : Potential equal to the max of the micro neurons DIST.
 */
void gestion_groupe_macro_colonne(int gpe, int gestion_STM, int learn)
{
   int i, n, mode, max_ind;
   float max_pot;
   int increment, increm, deb, nbre, nbre2;
   type_coeff *coeff;
   FILE *record;

   UNUSED_PARAM gpe; UNUSED_PARAM gestion_STM; UNUSED_PARAM learn;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;
   increm = increment - 1;

   for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
   {

      if (neurone[n].flag)
         neurone[n].s = (float) neurone[n].flag;
      else                    /*recherche du potentiel max sur les micro_neurones distance */
      {
         max_pot = -1.0;
         for (i = n - increm; i < n; i++)
         {
            coeff = neurone[i].coeff;
            mode = liaison[coeff->gpe_liaison].mode;
            if (mode == MICRO_DIST)
               if (max_pot < neurone[i].s2)
                  max_pot = neurone[i].s2;
         }
         neurone[n].s = max_pot;
      }
   }
   max_pot = -1.0;
   max_ind = -1;


   /* recherche du potentiel max sur les macro neurones */
   for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
   {
      if (max_pot < neurone[n].s)
      {
         max_pot = neurone[n].s;
         max_ind = n;

      }
      neurone[n].s1 = neurone[n].s2 = 0.0;
   }

   if (max_ind < 0)            /* modif PG 6/6/06 */
   {
      EXIT_ON_ERROR("ERROR in  macro_colone.c max_ind= %d <0 \n", max_ind);
   }

   neurone[max_ind].s1 = neurone[max_ind].s;   /* modif PG 6/6/06 */
   neurone[max_ind].s2 = 1.0;

   /*************sauvegarde des poids *********************/
   maj++;
   if ((maj%100) == 1)
   {
      char		filename[256];

      sprintf(filename, "./status_%i.txt", maj);

      record = fopen(filename,"w");

      for (i=deb; i<deb+nbre; i++)
      {
         coeff=neurone[i].coeff;
         mode=liaison[coeff->gpe_liaison].mode;
         if (mode == MICRO_DIST)
         {
            coeff=neurone[i].coeff;
            while (coeff!=NULL)
            {
               fprintf (record,"%f ",coeff->val);
               coeff=coeff->s;
            }
            fprintf(record,"\n");
         }
      }
      fclose(record);
   }
   /*************fin sauvegarde des poids *********************/

   /*************debut chargement des poids *********************/
   /*     if (maj == 1) */
   /*       { */
   /* 	char		filename[256]; */

   /* 	maj = 1; */
   /* 	sprintf(filename, "./status_%i.txt", maj); */

   /* 	record = fopen(filename,"w"); */

   /* 	for (i=deb;i<deb+nbre;i++) */
   /* 	  { */
   /* 	    coeff=neurone[i].coeff;    */
   /* 	    mode=liaison[coeff->gpe_liaison].mode; */
   /* 	    if (mode == MICRO_DIST)   */
   /* 	      {  */
   /* 		coeff=neurone[i].coeff;  */
   /* 		while(coeff!=nil) */
   /* 		  {  */
   /* 		    fscanf(record, "%f", &coeff->val); */
   /* 		    coeff=coeff->s; */
   /* 		  }	 */
   /* 		fscanf(record,"\n"); */
   /* 	      } */
   /* 	  } */
   /* 	fclose(record); */
   /*       } */
   /*************fin chargement des poids *********************/

   /*
   printf("chargement des donnees\n");
   getchar();
   */
   /***************************************************************/
   /*chargement des poids en provenance d'un fichier */
   /* on suppose ici que chaque mocro_n comporte
      6 microneurones DIST*/

   /*     nb_micro_dist =  6;  */
   /*     taille_ligne = 85;  */
   /*     record = fopen("poid85.txt","r"); */
   /*     for (i=0;i<(nb_micro_dist*taille_ligne);i++) */
   /*       { */
   /* 	fscanf (record,"%f %f",pun+i,pdeux+i); */
   /* 	printf ("%f  %f  \n",pun[i],pdeux[i]); */
   /*       }  */
   /*     fclose (record); */
   /*     i=0; */
   /*     cpt =0; */
   /*     ind =-nb_micro_dist; */
   /*     for(n=deb;n<deb+nbre;n++) */
   /*       {   */
   /* 	coeff=neurone[n].coeff;  */
   /* 	mode=liaison[coeff->gpe_liaison].mode; */
   /* 	if (mode == MICRO_DIST)   */
   /* 	  {  */
   /* 	    coeff=neurone[n].coeff;  */
   /* 	    coeff->val = pun[k]; 	 */
   /* 	    coeff=coeff->s; */
   /* 	    coeff->val = pdeux[k]; 	 */
   /* 	    k++; */
   /* 	    printf("%d \n",k); */
   /* 	  } */
   /*      } */
   /*fin du chargement des poids*/
   /**************************************************/
}
