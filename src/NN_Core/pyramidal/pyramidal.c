/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include "../tools/include/macro.h"

#include "tools/include/compute_proximal_hebbian.h"
#include "tools/include/compute_proximal_non_hebbian.h"
#include "tools/include/compute_intermediate_hebbian.h"
#include "tools/include/compute_distal_hebbian.h"
#include "tools/include/compute_distal_non_hebbian.h"
#include "tools/include/compute_inhibition.h"
#include "tools/include/compute_global_cell_an_activity.h"
#include "tools/include/compute_global_cell_activity.h"
#include "tools/include/learn_global_cell.h"
#include "tools/include/learn_proximal_hebbian.h"
#include "tools/include/learn_proximal_non_hebbian.h"
#include "tools/include/learn_intermediate_hebbian.h"
#include "tools/include/learn_distal_hebbian.h"


void mise_a_jour_groupe_pyramidal(int gpe)
{
   int DebutGpe, NbreTotalGpe, IncrementGpe;
   int i, j, mode;
   int link;
   float ph, pnh, ih, dh, dnh, inhib;
   type_coeff *CoeffTemp;

   dprints("mise a jour des neurones du groupe : %d\n", gpe);

   DebutGpe = def_groupe[gpe].premier_ele;
   NbreTotalGpe = def_groupe[gpe].nbre;

   /* Pas d'incrementation des macro-neurones */
   IncrementGpe = NbreTotalGpe / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);

   /* Boucle sur les macro-neurones */
   for (i = DebutGpe + IncrementGpe - 1; i < DebutGpe + NbreTotalGpe; i += IncrementGpe)
   {
      ph = pnh = ih = dh = dnh = inhib = 0.0;
      /* Boucle sur les micro-neurones */
      for (j = i - IncrementGpe + 1; j < i; j++)
      {
         CoeffTemp = neurone[j].coeff;
         link = CoeffTemp->gpe_liaison;
         mode = liaison[link].mode;


         switch (mode)
         {
            case MACRO_LINKS:
               break;
            case PROXIMAL_HEBBIAN:
               ph = ph + compute_proximal_hebbian(j);
               break;

               /* Type de liens provennant de f_base_de_temps */
            case PROXIMAL_NON_HEBBIAN:
               pnh = pnh + compute_proximal_non_hebbian(j);
               break;

            case INTERMEDIATE_HEBBIAN:
               ih = ih + compute_intermediate_hebbian(j);
               break;
            case DISTAL_HEBBIAN:
               dh = dh + compute_distal_hebbian(j);
               break;
            case DISTAL_NON_HEBBIAN:
               dnh = dnh + compute_distal_non_hebbian(j);
               break;
            case INHIBITION:
               inhib = inhib + compute_inhibition(j);
               break;
            default:
               EXIT_ON_ERROR("That link type (%d) does not exist for pyr. neurons of group 	%d \n",mode, gpe);
         }
         neurone[j].cste = dnh;
      }

      if (isdiff(eps, 1.0)) compute_global_cell_an_activity(i, ph, ih, pnh, dh, dnh, inhib,DebutGpe, gpe);
      else                    /*eps==1.0 */
      {
         compute_global_cell_activity(i, ph, ih, pnh, dh, dnh, inhib, DebutGpe, gpe);
      }
   }
   (*def_groupe[gpe].appel_gestion) (gpe);
}


void apprend_Pyramidal(int gpe)
{
   int DebutGpe, NbreTotalGpe, IncrementGpe;
   int Coactivation;
   int i /*, j */ , n, mode;
   float PotMacroN;
   type_coeff *CoeffTemp;
   float learning_speed = 1.;  /* apprentissage en 1 coup de pnh             Valeur par defaut de la vitesse d'apprentissage :         */
   /*float learning_speed=vigilence * eps * def_groupe[gpe].learning_rate;  si aucun lien neuromodulation de type learning_speed      */
   /* n'existe en entree du groupe alors, ces valeurs resteront */
   /* inchangees.                                               */
   char type[20];

   /* ---Calcul de la neuromodulation portant sur le groupe--- */
   strcpy(type, "learning_speed");
   calcule_neuromodulation(gpe, type, &learning_speed);

   dprints("apprend cellule pyramidale gpe %d \n", gpe);


   DebutGpe = def_groupe[gpe].premier_ele;

   NbreTotalGpe = def_groupe[gpe].nbre;

   IncrementGpe =
      NbreTotalGpe / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);

   for (n = DebutGpe + IncrementGpe - 1; n < DebutGpe + NbreTotalGpe; n += IncrementGpe)
   {
      PotMacroN = neurone[n].s;

      /* potential of the macro neuron */

      Coactivation = neurone[n].flag;

      dprints("coactivation : %d \n", Coactivation);

      for (i = n - IncrementGpe + 1; i <= n; i++)
      {
         CoeffTemp = neurone[i].coeff;
         mode = liaison[CoeffTemp->gpe_liaison].mode;
#ifdef TRACE_HIPPO_0
         printf("MODE : %d \n", mode);
#endif
         switch (mode)
         {
            case MACRO_LINKS:
               learn_global_cell(i, PotMacroN, Coactivation);
               break;
            case PROXIMAL_HEBBIAN:
               learn_proximal_hebbian(i, PotMacroN, Coactivation, learning_speed);
               break;
            case PROXIMAL_NON_HEBBIAN:
               learn_proximal_non_hebbian(i, PotMacroN, Coactivation,
                                          learning_speed);
               break;
            case INTERMEDIATE_HEBBIAN:
               learn_intermediate_hebbian(i, PotMacroN);
               break;
            case DISTAL_HEBBIAN:
               learn_distal_hebbian(i, PotMacroN);
               break;
            case DISTAL_NON_HEBBIAN:
               break;
            case INHIBITION:
               break;
            default:
               EXIT_ON_ERROR("That link type (%d) does not exist for pyr. neurons of group %d \n",mode, gpe);
         }
      }
   }
}
