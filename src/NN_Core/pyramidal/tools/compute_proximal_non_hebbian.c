/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <libx.h>
#include <stdlib.h>

#include "./include/macro.h"

double f_gaussienne(double x, double m, double s)
{
  double y = x - m;
  /*	double a = 1./(sqrt(2*M_PI)*s);*/

  /*if (x<m)      return 0.; */

  y *= y;
  y /= 2.;
  y /= s * s;
  y = exp(-y);

  return 1. * y;
}
double f_gaussienne_tronquee(double x, double m, double s)
{
  double y = x - m;
  /*	double a = 1./(sqrt(2*M_PI)*s);*/

  /*if (x<m)      return 0.; */

  y *= y;
  y /= 2.;
  y /= s * s;
  y = exp(-y);

  if (y > exp(-1)) return 1. * y;
  return 0;
}

double f_lineaire(double x, double m, double s)
{
  double y;
  double abbs;
  abbs = (x - m > 0.0) * (x - m) + (m - x > 0.0) * (m - x);

  if (x - m > 0) y = 0; /*(m-x)/s+1.; */
  else y = 1 - abbs / s;

  if (y < 0) return 0;

  return y;
}

float compute_proximal_non_hebbian(int i)
{
  float pot;
  type_coeff *coeff = NULL;
  int n = 0; /* nb de neurones en entree */
  /*-char c;*/
  float activite = 0.;
  float input_value;

  float LargeurGaussienne = def_groupe[neurone[i].groupe].simulation_speed;

  /*#define TRACE_HIPPO_0 */

#ifdef TRACE_HIPPO_0
  printf("compute_proximal_non_hebbian %d \n", i);
#endif

  coeff = neurone[i].coeff;
  pot = 0.0;
  while (coeff != NULL)
  {
    input_value = neurone[coeff->entree].s1;

    pot += (input_value > SEUIL_ENTREE) * f_gaussienne_tronquee(input_value, coeff->val, LargeurGaussienne);

    /* Comptabilise les entrees actives */
    n += (input_value > SEUIL_ENTREE);

    coeff = coeff->s;
  }

  /* Le potentiel est d'autant plus grand que les poids sont proches de l'entree 0<pot<N */
  /* L'activite est d'autant plus grande que les poids sont
   proches de l'entree et varie entre 0 et 1 */
  /* Si l'entree n'est pas active la sortie non plus */

  activite = pot / (n + 1e-20);
  neurone[i].s = neurone[i].s1 = neurone[i].s2 = activite;

  if (neurone[i].s < 0.) neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;

  if (neurone[i].s > 0.01)
  {

#ifdef TRACE_HIPPO_0
    printf("*****************************************\n");
    printf("\t\tN[%d]=%f\tPot=%f\n", i, neurone[i].s, pot);
#endif
  }

  return (neurone[i].s2);
}
