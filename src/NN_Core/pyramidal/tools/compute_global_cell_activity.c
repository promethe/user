/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <Typedef/boolean.h>
#include "include/macro.h"
#include <Global_Var/NN_Core.h>

void compute_global_cell_activity(int i, float ph, float ih, float pnh,
                                  float dh, float dnh, float inhib, int Debut,
                                  int Gpe)
{
    boolean NeuronInhibit;
    float ActivProximal, ActivDistal, ActivIntermediate;
    float ActivTemp;


    ActivProximal = ph + pnh;
    ActivIntermediate = ih;
    ActivDistal = dh + dnh;



#ifdef TRACE_HIPPO
    cprints("compute_global_cell_activity %d \t", i);
    cprints("Prox = %f    Dist = %f\n", ActivProximal, ActivDistal);
#endif



    if (neurone[i].flag != 0)
        NeuronInhibit = FALSE;
    else
        NeuronInhibit = TRUE;



    ActivTemp = 0.0;
    neurone[i].flag = 0;
    /*no learning in the default case */
    if (ActivDistal > DISTAL_activity_threshold)
    {
#ifdef TRACE_HIPPO
        cprints("activite distale suffisamment grande %f pour %d\n",
               ActivDistal, i);
#endif
        ActivTemp = ActivProximal;
        neurone[i].flag = 1;
    }
    /* learning is possible, due to coactivation */
    else if (ActivProximal > PROXIMAL_activity_threshold)
    {
#ifdef TRACE_HIPPO
        cprints("activite proximale suffisamment grande %f pour %d\n",
               ActivProximal, i);
#endif
        ActivTemp = ActivProximal;
    }
/*  printf("%f doit etre superieur a %f pour une prediction\n",ActivTemp, GLOBAL_activity_threshold );*/
    /* up to GLOBAL_activity_threshold, negative gradient id detected      */
    if (ActivTemp > GLOBAL_activity_threshold)
      if ((ActivTemp < neurone[i].s) && (isequal(neurone[i].d, 1.0))
            && (NeuronInhibit))
        {
            /*int SizeXGpe = def_groupe[Gpe].taillex; */
            /*int SizeYGpe = def_groupe[Gpe].tailley; */
            /*int SizeGpe = def_groupe[Gpe].nbre; */
            /*int Increment; */
            /*int ll, cc, pp; */
            /*float TimeLocalCA3; */

            /*Increment = SizeGpe / (SizeXGpe * SizeYGpe); */
            /*pp = i - Debut; */
            /*ll = pp / (SizeXGpe * Increment); */
            /*cc = (pp - ll*SizeXGpe*Increment - 2) / Increment; */

            neurone[i].d = 0.0;
            neurone[i].s2 = 1.0;
            /*TimeLocalCA3 = (float)(SystemTime.tv_sec + 1e-6 * SystemTime.tv_usec); */

        }
        else
            neurone[i].s2 = 0.0;
    else
    {
        neurone[i].d = 1.0;
        neurone[i].s2 = 0.0;
    }

    /*two kind of information:
       s and s1 are the internal analogical activity of the cell up, a proximal activity threshold
       s2 is the binary information of prediction */

    neurone[i].s1 = neurone[i].s = ActivTemp;

(void) inhib;
(void) Debut;
(void) Gpe;
}

/*----------------------------------------------------------------------------------------------*/
