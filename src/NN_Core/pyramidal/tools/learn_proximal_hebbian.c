/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <math.h>
/*-----------------------------------------------------------------------------------------------*/

/*! synapses provenant des mossy fibers (DG vers CA3)
 * la surface de
 * contact avec les cellules pyramidale est limitee et implique  
 * une normalisation des poids synaptiques   */
 /* PotMacroN:  the global potential of the pyramidal cell */
 /* Coactivation=1 : apprentissage possible (coactivation = distal*proximal) */
void learn_proximal_hebbian(int n, float PotMacroN, int Coactivation, float learning_speed)
{
    /*int i, j,alpha; */
    float ActiviteDistale;
    float ConstNormalisation, DeltaW;
    type_coeff *CoeffTemp = NULL;
    int freq = 30;
    ActiviteDistale = 0.0;
    ConstNormalisation = 0.0;
    ActiviteDistale = neurone[n + 1].cste;
    /*  Potmac=  fopen("./pot.txt", "a"); */
    /* calcul de la constante de normalisation */

    learning_speed *= eps;

    if (abs(learning_speed-0.0) < 0.00001) /*test d'egalite*/
      {
	return ;
      }

    if (isdiff(learning_speed, 1.0))             /*algorithme du LMS */
    {
        if (rand() % freq == 1)
        {
        	cprints("apprentissage analogique\n");
            CoeffTemp = neurone[n].coeff;
            while (CoeffTemp != NULL)
            {
                DeltaW =
                    learning_speed * (ActiviteDistale -
                           PotMacroN) * neurone[CoeffTemp->entree].s;
                CoeffTemp->val += DeltaW;
                CoeffTemp = CoeffTemp->s;
            }
        }

    }

    else                        /*algorithme d`apprentissage en un coup */
    {
      if (isdiff(Coactivation, 0.))
        {
            CoeffTemp = neurone[n].coeff;
            while (CoeffTemp != NULL)
            {
                ConstNormalisation +=
                    (neurone[CoeffTemp->entree].s *
                     neurone[CoeffTemp->entree].s);
                CoeffTemp = CoeffTemp->s;
            }

#ifdef TRACE_HIPPO
            cprints("apprentisage proximal hebbian du neurone %d \n", n);

#endif

            /* modification des poids */
            CoeffTemp = neurone[n].coeff;
            while (CoeffTemp != NULL)
            {
	      if (isdiff(ConstNormalisation, 0.))
                    CoeffTemp->val =
                        neurone[CoeffTemp->entree].s / ConstNormalisation;

#ifdef TRACE_HIPPO
                else
                    cprints("Poids proximaux non modifiables \n");
#endif

                CoeffTemp = CoeffTemp->s;
            }
        }
    }

}
