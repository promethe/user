/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_spike_prediction.c 
\brief 

Author: Julien Hirel
Created: 05/02/2009
Modified:
- author: 
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Takes as input an analogical activity (eg. prediction activity with timing from CA3) and outputs a spike
when the differential of the activity goes from positive to negative.
For example with a bell-shaped prediction activity, a spike will be emited at the top of the bell.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

void function_spike_prediction(int gpe)
{
   int i;
   float act;
   type_coeff *coeff;

   for(i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
   {	
      act = 0.;
      coeff = neurone[i].coeff;

      while (coeff != NULL)	
      {
	 act += coeff->val * neurone[coeff->entree].s1;
	 coeff = coeff->s;
      }	

      dprints("f_spike_prediction(%s): neuron %d activity = %f\n", def_groupe[gpe].no_name, i, act);

      if (act <= def_groupe[gpe].seuil)
      {
	 neurone[i].s1 = neurone[i].s2 = 0.;
	 neurone[i].flag = 1;
      }
      else
      {  
	 /* Gestion des spikes de prediction sur s2 */
	 if (act < neurone[i].s && neurone[i].flag == 1)
	 {
	    neurone[i].flag = 0;
	    neurone[i].s1 = neurone[i].s;
	    neurone[i].s2 = 1.;
	    dprints("f_spike_prediction(%s): neuron %d SPIKE\n", def_groupe[gpe].no_name, i);
	 }
	 else
	    neurone[i].s1 = neurone[i].s2 = 0.;
      }
      
      neurone[i].s = act;
   }
}
