/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include "../tools/include/macro.h"

#include <Struct/pyramidal_data.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/compute_proximal_hebbian.h"
#include "tools/include/compute_proximal_non_hebbian.h"
#include "tools/include/compute_intermediate_hebbian.h"
#include "tools/include/compute_distal_hebbian.h"
#include "tools/include/compute_distal_non_hebbian.h"
#include "tools/include/compute_inhibition.h"
#include "tools/include/compute_global_cell_an_activity.h"
#include "tools/include/compute_global_cell_activity.h"
#include "tools/include/learn_global_cell.h"
#include "tools/include/learn_proximal_hebbian.h"
#include "tools/include/learn_proximal_non_hebbian.h"
#include "tools/include/learn_intermediate_hebbian.h"
#include "tools/include/learn_distal_hebbian.h"
#include <NN_Core/Modulation/calcule_neuromodulation.h>
 #include <Kernel_Function/trouver_entree.h>
/* le seuil qui permet de detecter une act venant de ec doit etre inf a Wec-ca3*/
#define DNH_SEUIL 0.0008 /*0.008*/

#define DEBUG_learn

float found_max_dg0(int j);

void mise_a_jour_ca( int i,int gestion_STM, int learn )
{

	(void) i;
	(void) gestion_STM;
	(void) learn;
 /* printf("m a j n p\n"); 
scanf("%d",&i);*/
}



                  /* val=act_macro*Wrec, micro neurone i , offset micro j, macro neurone le plus activ� via dg: ind, macro n,val max venant du micro dg du macro "ind" *max*/

float compute_rec(int i,int j,int * ind,int n,float *max){
float val=0.;
  type_coeff *coeff=NULL;
  coeff = neurone[i].coeff; /*macro neurone*/
  
  while(coeff != NULL)
  {
    if( neurone[coeff->entree-j].s2>=*max)
    {
    	(* max)=neurone[coeff->entree-j].s2; /*recherche le macro qui a le micro venant de dg le plus activ� sur cette ligne...*/
    	if( n!=coeff->entree)
	{ /*on ne regarde pas son propre macro*/
     		val=neurone[coeff->entree].s1*coeff->val; 
     		(*ind)=coeff->entree;
     	}
    }
    coeff=coeff->s;	/* Lien suivant */
    
  }
  return val;
}


float found_max_dg0(int i){
float max=0.0;
  type_coeff *coeff=NULL;


  coeff = neurone[i].coeff;
  
  while(coeff != NULL){
    if( neurone[coeff->entree].s2>max){
    i=coeff->entree;
    max=neurone[coeff->entree].s2;
    }
    coeff=coeff->s;	/* Lien suivant */
    
  }

return max;
}


float found_max0(int i,int *m){
float max=-1;
  int DebutGpe, NbreTotalGpe, IncrementGpe;
  *m=-1;
   DebutGpe=def_groupe[i].premier_ele;
  NbreTotalGpe=def_groupe[i].nbre;
  IncrementGpe=NbreTotalGpe / (def_groupe[i].taillex*def_groupe[i].tailley);
  for(i=DebutGpe;i<DebutGpe+NbreTotalGpe;i+=IncrementGpe){
    if( neurone[i].s1>max){
   *m=i-DebutGpe;
    max=neurone[i].s1;
    }
  
    
  }



return max;
}

int seuilrec(float j){
if(j>=0) return 1;
else return 0;
}
int seuilr(float j){
if(j>0) return 1;
else return 0;
}

void mise_a_jour_groupe_ca(int gpe, int gestion_STM)
{
  int DebutGpe, NbreTotalGpe, IncrementGpe;
  int i, j, mode,argmax=-1,tt;
  static int gpe_ec=-1,gpe_dg=-1;
  float n_mod=-1;
  int mod=-1;
  static float Wrec;
  int link,index_ec=10000;
  int diff,diff_tmp;
 int input_ind;
 float ph, pnh, ih, dh, dnh, rh,rrh,max_dg_from_dh,pd,seuil,inhib,max=0,macro_act=0;
 int act_ec=-1;
 float act_dg=-1;
  type_coeff *CoeffTemp,*coeff;
  pyramidal_data *PY;
/*creation si besoin de la structure pyrammidal_data qui contient le seuil sur l'activite pour l'apprentissage et l'index du macroneurone 
dont le micro neurone de la voie conditionelle doit apprendre*/
 
  DebutGpe = def_groupe[gpe].premier_ele;
 
  NbreTotalGpe = def_groupe[gpe].nbre;
   /* Pas d'incrementation des macro-neurones */
  IncrementGpe = NbreTotalGpe / (def_groupe[gpe].taillex*def_groupe[gpe].tailley);
  
  if(def_groupe[gpe].data==NULL)
  {
		
		PY=(pyramidal_data*) malloc(sizeof(pyramidal_data));
		if(PY==NULL){ EXIT_ON_ERROR("pb!\n");}
		
		PY->seuil_appr=-1;
		PY->argmax_to_learn=-1;
		def_groupe[gpe].data=PY;
		PY->cond_offset=PY->index_micro_ec=PY->index_micro_rec=PY->index_micro_trec=-1;
		/*recherche des indices respectif des differente voies par rapport a cekui de leur macroneurone*/
			for(i=0;i<IncrementGpe;i++){
			coeff = neurone[DebutGpe+i].coeff;
		link=coeff->gpe_liaison ;
		mode=liaison[link].mode;
		if(mode==DG_CA){PY->cond_offset=IncrementGpe-i-1;
		gpe_dg=neurone[coeff->entree].groupe;}
   		if(mode==EC_CA){PY->index_micro_ec=IncrementGpe-i-1;gpe_ec=neurone[coeff->entree].groupe;}
		if(mode==2){PY->index_micro_trec=IncrementGpe-i-1;gpe_ec=neurone[coeff->entree].groupe;}
   		if(mode==REC_CA_CA){PY->index_micro_rec=IncrementGpe-i-1;Wrec=coeff->val;}
	}
		srand48(5l);
  
  }
  else
  	PY=def_groupe[gpe].data;
  
  PY->seuil_appr=-1;

/*nmod = 1- diff = 0 ou 1 si planif ou pas*/  
 mod=  trouver_entree(gpe, (char*)"modulationplan");
if(mod<0)n_mod=0.;
else if(neurone[def_groupe[mod].premier_ele].s2>0)n_mod=1.;
else n_mod=0.;

  
  /*reset du seuil d'apprentissage (valeur des neurones sur DG)*/
   ((pyramidal_data*)def_groupe[gpe].data)->seuil_appr=-1;
/*iteration necessaire � la diffusion des informations sur les liens recurrents*/
/*2 iteration de la boucle en planif*/
   if(n_mod>0)diff=0;
	else diff=1;
   diff_tmp=diff;
   for(diff=diff_tmp;diff<2;diff++)
   {/* Boucle sur les macro-neurones */
	for(i=DebutGpe+IncrementGpe-1; i<DebutGpe+NbreTotalGpe; i+=IncrementGpe)
	{
		ph = pnh = ih = dh = 0.;
		dnh = rrh = inhib = macro_act = pd = dh = max_dg_from_dh = rh = 0.0;
		input_ind=0;
		/* Traitement des micro-neurones */
		for(j=i-IncrementGpe+1; j<i; j++)
		{
			CoeffTemp = neurone[j].coeff;
			link=CoeffTemp->gpe_liaison ;
			mode=liaison[link].mode;			
			switch(mode)
			{
				case MACRO_LINKS:	
				break;
	
				case DG_CA     : 
					ph=ph+compute_proximal_hebbian(j) ;
					act_dg=found_max0(gpe_dg,&tt);			
				break;
				case PROXIMAL_NON_HEBBIAN	:			pd=pd+compute_proximal_hebbian(j) ;	
				break;
				case EC_CA   :  
					dnh=dnh+compute_distal_non_hebbian(j); 
					/*recherche le max sur EC lors de la premiere iteration du cycle*/	
					if(act_ec==-1)
						act_ec=found_max0(gpe_ec,&index_ec); 
					break;
				case REC_CA_CA   : 
						/*pour accelerer on ne calcul que si nmod==1*/
					if(n_mod>0)
					{
						/*attend que tous les trans aient une act stable pour calculer val de la voie recurrente*/
						if(diff==1)
						{
							dh=compute_rec(j,PY->cond_offset,&input_ind,i,&max_dg_from_dh);
							/*seuil_rec(x)= 1 si x>=0*/
							/*exemple:  sur DG: act(A)>act(B)>act(C)
								soit les transitions : AA, BA, CA, AB, CB, CD
								on ne veut conserver que AB et CB						
								*/
							/* supprime(rh<0) les trans BA et CA et AA: dh=-2*seuil_rec(-|index(A)-index(A)|)=1.-2 pour assurer que dnh+ph+rec<0 Finalement pour AB et CB rh=Wrec*dh */	
				rh=
				(dh-
				2*(
				seuilrec(
				-abs(  ((int)neurone[input_ind].cste)  - ((int)neurone[input_ind].posy))
				        )
				  ) 
				);
										
						}
					}
				break;
				/*	case INHIBITION	        :  inhib=inhib+compute_inhibition(j);       break;*/
				default		        :  
					EXIT_ON_ERROR("That link type (%d) does not exist for pyr. neurons of group 	%d \n",mode,gpe);

				
				}
		}
		/* Traitement des MAcro-neurones */
		/*le macro neurone a deja appris*/
		if(neurone[i].flag==1)
		{
			/*calcul la reponse du  macro neurone a ses entrees*/
			macro_act=(ph+dnh+pd+rh*n_mod);
			/*neuro_mod_du_seuil_en_planif*/
			if(n_mod>0&&(diff==1))
			{
				/*seuil=ec+dg + nmod*Wrec*dgmax si l'activit� rec ne vient pas d'une trans conn�ct� � dgmax supprime CD*/
				seuil=ph+dnh+
					n_mod*Wrec*act_dg*seuilr(1-seuilrec(max_dg_from_dh-act_dg));/*=0si pas planif ou */
				if(isequal(neurone[i].d,neurone[i].cste))
					seuil+=1;
						/*ne propose pas de transition type AA*/
							
			}					
			else
				seuil=def_groupe[gpe].seuil;
			if(macro_act<0)
				macro_act=0;
			neurone[i].s=macro_act;
			if(macro_act-seuil<0)
				neurone[i].s1=neurone[i].s2=0;
			else
				neurone[i].s1=neurone[i].s2=	macro_act;

							
			if(dnh>DNH_SEUIL&&ph>=act_dg)  
				if(neurone[i].s>def_groupe[gpe].seuil&&neurone[i].s>max) 
				{
					max=neurone[i].s;
					argmax=-1;
				}
									
		}
		else if(dnh>DNH_SEUIL)
		{
								
			do 
			{
				macro_act=drand48()*def_groupe[gpe].seuil;	
			} while(macro_act<dnh);
			neurone[i].s1=neurone[i].s2=0;
			neurone[i].s=macro_act;
			if(neurone[i].s>max)
			{
					max=neurone[i].s;
					argmax=i;
			}
		} 
		else
			neurone[i].s1=neurone[i].s2=neurone[i].s=0;
	} 
   } 	
   if(act_ec>0&&act_dg>0&&argmax>-1)
   {
	PY->argmax_to_learn=argmax;
	neurone[argmax].s1=neurone[argmax].s2=neurone[argmax].s=2.5;
	neurone[argmax].d=(int)index_ec;				
   } 
   else  
   {
	PY->argmax_to_learn=-1;
   }

 (*def_groupe[gpe].appel_gestion)(gpe);  
 (void) gestion_STM;
}




float seuil1b(float f){
if(f>0.01)return 1;
else return 0.;
	}
	
	float seuil_inv(float a){
	if(a<1)return a;
	else return 0;
	}
	
int fpres(int gp,int indexi){
int i;

for(i=def_groupe[gp].premier_ele;i<def_groupe[gp].nbre+def_groupe[gp].premier_ele;i++){
if(neurone[i].s1>0){

	if(i-def_groupe[gp].premier_ele==indexi)
			return 1;
			}
}
return 0;
}


void apprend_ca(int gpe)
{
  int NbreTotalGpe;
  int index_micro,index_macro,i,dg_learned;
  float Sj,max=-1.;
  type_coeff *coeff, *coeff_tmp=NULL;
  pyramidal_data *PY;
  NbreTotalGpe = def_groupe[gpe].nbre;
  
  PY=def_groupe[gpe].data;
  if(PY==NULL){
	  EXIT_ON_ERROR("ERROR on data of grp:%d\n",gpe);

  }
  
  
 
index_macro=PY->argmax_to_learn;
if(index_macro>-1)
{
     

		
		/*calcul l'index du micro neurone de la voie conditionnelle(provenant de DG) correspondant au macroneurone dont le flag est � 1*/
		index_micro=index_macro-PY->cond_offset;
		coeff=neurone[index_micro].coeff;
		dg_learned=0;
		i=0;
		while(coeff!=NULL)
		{
			if(coeff->evolution==1)                  /* si coeff modifiable */
			{
				Sj=neurone[coeff->entree].s1/*coeff->moy*/;
				/* recherche du neurone le plus active en entree*/
				if(Sj>max)
				{
					max=Sj;
					coeff_tmp=coeff;
					dg_learned=i;
				}
				i++;
				coeff->val=0;
			}
			coeff=coeff->s;
		}
			
		/*on fait l'apprentissage sur le neurone le plus active en entree*/
		if(max>-1)
		{
			if(max>0.)
			{
				coeff_tmp->val=1.;
				/*on marque le neurone comme ayant deja appris, il n'est plus modifiable*/
				neurone[index_macro].flag=1; 
				neurone[index_macro].cste=dg_learned;
				
			} 
			else
						cprints("\n\n\nprobleme apprentiassage ca3 pas de max trouver sur la voie en provenance de Dg pour le macro gagant!max:%f \n\n",max);
		}
		else
			cprints("\n\n\nprobleme apprentiassage ca3 pas de max trouver sur la voie en provenance de Dg pour le macro gagant!\n\n\n");
						
}
}


void function_ca3(int gpe)
{
	int i;
	float s,max,p,dw,eps_ca3;
	type_coeff*coeff;
	int sortie=0;
	/*MAJ*/
	for(i=0;i<def_groupe[gpe].nbre;i++)
	{	s=0;
		coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
		while(coeff!=NULL)	
		{
			
			s=s+coeff->val*neurone[coeff->entree].s1;	
			coeff=coeff->s;
		}	
		s=s-def_groupe[gpe].seuil;
		if(s<0.)
			s=0.;
		neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2 =s;
	}
	
	/*APR*/
	max=0.;
	for(i=0;i<def_groupe[gpe].nbre;i++)
	{	if(neurone[def_groupe[gpe].premier_ele+i].s1>max)
			max=neurone[def_groupe[gpe].premier_ele+i].s1;
	}
	
	if( max<1.) 
		eps_ca3=1.;
	else
		eps_ca3 =0.;

	cprints("recrutement\n");
	
	for(i=0;i<def_groupe[gpe].nbre&&sortie==0;i++)
	{	
		dw=0.;
		p=0.;
		/*recupere l'activite du neurone du present sur le lien non modifiable*/
		coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
		while(coeff!=NULL)	
		{
			if(coeff->evolution==0)
			{
				p=neurone[coeff->entree].s1;
			}
			coeff=coeff->s;
		}
		coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
		while(coeff!=NULL)	
		{
			if(coeff->evolution==1)
			{
				dw=eps_ca3 * (1.- coeff->val)  *p*neurone[coeff->entree].s1;
				coeff->val=coeff->val+dw;
			}
			if (dw>0.)
			{
				coeff=NULL;
				sortie=1;
			}
			else	
				coeff=coeff->s;
		}	
	}
	
	/*REMAJ*/
	for(i=0;i<def_groupe[gpe].nbre;i++)
	{	s=0;
		coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
		while(coeff!=NULL)	
		{
			
			s=s+coeff->val*neurone[coeff->entree].s1;	
			coeff=coeff->s;
		}	
		s=s-def_groupe[gpe].seuil;
		if(s<0)
			s=0.;
		neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2 =s;
	}
}

void new_ca3_2(int gpe)
{
	/*Enleve le bruit*/
	int i;
	type_coeff * coeff;
	if(continue_simulation_status==START)
	    for(i=0;i<def_groupe[gpe].nbre;i++)
	    {
		coeff=neurone[i+def_groupe[gpe].premier_ele].coeff;
		while(coeff!=NULL)
		{
			if(coeff->evolution==1)
				coeff->val=liaison[coeff->gpe_liaison].norme;
			coeff=coeff->s;
		}
	    }
}

void function_ca3_2(int gpe)
{
	int i;
	float s,max,p,dw,eps_ca3;
	type_coeff*coeff;
	int sortie=0;
	/*MAJ*/
	for(i=0;i<def_groupe[gpe].nbre;i++)
	{	s=0;
		coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
		while(coeff!=NULL)	
		{
			
			s=s+coeff->val*neurone[coeff->entree].s1;	
			coeff=coeff->s;
		}	
		s=s-THETAR;
		if(s<0.)
			s=0.;
		neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2 =s;
	}
	
	/*APR*/
	max=0.;
	for(i=0;i<def_groupe[gpe].nbre;i++)
	{	if(neurone[def_groupe[gpe].premier_ele+i].s1>max)
			max=neurone[def_groupe[gpe].premier_ele+i].s1;
	}
	if( max<= 1.-THETAR)
		eps_ca3=1.;
	else
		eps_ca3 =0.;
	if(eps_ca3>0)
	{	
		for(i=0;i<def_groupe[gpe].nbre&&sortie==0;i++)
		{	
			if(neurone[def_groupe[gpe].premier_ele+i].seuil<1.)
			{
				p=0.;
				/*recupere l'activite du neurone du present sur le lien non modifiable*/
				coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
				while(coeff!=NULL)	
				{
					dw=0.;
					p=neurone[coeff->entree].s1;
					if(liaison[coeff->gpe_liaison].mode==EC_CA)
						dw=eps_ca3*p*THETAR;					
					else if(liaison[coeff->gpe_liaison].mode==DG_CA)
						dw=eps_ca3*p;		
					coeff->val=coeff->val+dw;
					coeff=coeff->s;
				}

				/*Verification que le neurone encode une transition: si la somme des coeff est egale � 1+ THETAR, sinon, si la somme vaut THETAR, alors peut de lieu de depart */
				dw=0.;
				coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
				while(coeff!=NULL)	
				{
					dw=dw+coeff->val;					
					coeff=coeff->s;
				}
				if(dw>THETAR)
					neurone[def_groupe[gpe].premier_ele+i].seuil=1.;
							
				/*le break ou sortie=0 casse la boucle car un seul neurone peut etre recruted*/
				sortie=0;
				break;
			}	
		}
		
		/*REMAJ*/
		for(i=0;i<def_groupe[gpe].nbre;i++)
		{	s=0;
			coeff=neurone[def_groupe[gpe].premier_ele+i].coeff;
			while(coeff!=NULL)	
			{
				
				s=s+coeff->val*neurone[coeff->entree].s1;	
				coeff=coeff->s;
			}	
			s=s-THETAR;
			if(s<0)
				s=0.;
			neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2 =s;
		}
	}
}
