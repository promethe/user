#ifndef __CHAOS_H__
#define __CHAOS_H__

#include	<glib.h>

typedef struct chaos
{
  double	beta;
  void		*handle;
  int		idx;
  // parameters
  GList		*q;
  double	*alpha;
  int		P;
  int		n;
  int		k;
  double	g;
  double	learn;

  double	**d;
  double	**w;

  int		w_step;
  int		s_step;
  double	t_s;
  double	t_w;
  double	h_s;
  double	h_w;
  int		f_q;
  double	*theta;
  double	*phi;
  double	*x; /* x(t) : neuron state */
  double	*x1;
  double	*x_; /* x_(t) : next neuron state */
  double	*p;
  double	**h_pq;
  double	*p_;
  double	*xm;/* xm(t) : neuron mean state */
  double	*xm1;
  double	*I;
}		type_chaos;

int		N(int k);
void		prepare_state_vector( type_chaos *chaos_p,  double *x, double *x_g[]);
void		prepare_weight_vector( type_chaos *chaos_p,  type_chaos *chaos_q,
				       double *w, double *w_g[]);

double		potential( type_chaos *chaos_p,  double **x, double **w_p, int i);
void		delta_q( type_chaos *chaos_p,  type_chaos *chaos_q,  double *x_p, int i, int q, double *dw);
int		gpe_to_idx(int gpe);
int		idx_to_gpe(int idx);

#endif /* __CHAOS_H__ */
