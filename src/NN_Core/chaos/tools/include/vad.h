#ifndef __VAD_H__
#define __VAD_H__

#define VAD_SIZE 10

typedef struct
{
  int n;
  int k;
  double x[VAD_SIZE];
  /* double oldM; */
  /* double oldS; */
  /* double newM; */
  /* double newS; */
}		vad_t;

void		vad_init(vad_t *vad);
void		vad_update(vad_t *vad, double x);
double		vad_mean(const vad_t *vad);
double		vad_variance(const vad_t *vad);
double		vad_stddev(const vad_t *vad);

#endif /* __VAD_H__ */
