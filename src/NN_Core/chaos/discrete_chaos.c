/* Reseau de neurones recurrent aleatoire a TEMPS DISCRET */
/* Version de 2000 testee ... */
/* Mathias Quoy */

/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M. Quoy,Nicolas CAZIN
- description: specific file creation
- date: 01/03/2005; 04/07/05, 25/09/2014

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-

Global variables:
-none

Internal Tools:
-none

External Tools:


Links:
- type:
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Differents modes de liens :
 - MACRO_LINKS	     :  valeur 0 : liens entre macro neurone et microneurones

 - RECURRENT_LINKS     :  valeur 1 appelle l'apprentissage sur les liens recurrents : learn_recurrent_links(i, n, learning_speed)
 - INCOMING_LINKS      :  valeur 2 : appelle l'apprentissage sur les liens provenant d'un autre groupe KO : learn_incoming_links(i, n, learning_speed);

 - INPUT_LINKS      :  valeur 3 : liens provenant d'un autre groupe (lecteure d'une sequence par ex. pas d'apprentissage.

Sequencement :
Pour tous les groupes KO :
 - appel gestion
 - appel mise a jour
Puis, pour tous les groupes KO :
 - appel apprentissage

Gestion : on met dans s1 l'etat s precedent
        : on bascule la valeur de s2
Mise a jour : calcul de la nouvelle valeur s des micro et macro neurones

Apprentissage : regle hebbienne dans laquelle on retranche a l'etat courant (ou precedent) la valeur moyenne des etats precedents. On ne modifie ndonc une liaison que si l'etat a change. La valeur de s2 permet de savoir si l'on prend la valeur precedente (d) car s1 a deja ete mis a jour, ou si l'on prend la valeur s1 car s1 n'a pas encore ete mis a jour. Introduit pour resoudre la pb du rebouclage : les liens retour doivent apprendre apres maj de l'entree et du RNRA, et pas au moment de l'appel du groupe d'entree.

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>

#include <libx.h>
#include "tools/include/ff.h"
#include "tools/include/chaos.h"
#include "NN_Core/chaos/common_chaos.h"
#include "NN_Core/chaos/discrete_chaos.h"

extern void	 output_weight_COMMON_CHAOS(int gpe); /* defini dans common_chaos.c */
extern void		weights(const type_chaos *chaos_p, const double *x);

void		new_DISCRETE_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		p = chaos_p->idx;
   int		i;
   int		j;

   new_COMMON_CHAOS(gpe);

   chaos_p->h_s = 1.0;
   chaos_p->h_w = 1.0;
}

void		destroy_DISCRETE_CHAOS(int gpe)
{
   destroy_COMMON_CHAOS(gpe);
}

void		learn_DISCRETE_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;

   int k;
   if (chaos_p->k % chaos_p->w_step == 0)
   {
      weight_DISCRETE_CHAOS(gpe);
      output_weight_COMMON_CHAOS(gpe);
   }
}


void		update_DISCRETE_CHAOS(int gpe)
{
   type_chaos		*chaos_p = def_groupe[gpe].data;
   int k;

   input_COMMON_CHAOS(gpe);
   if (chaos_p->k % chaos_p->s_step == 0)
   {
      state_DISCRETE_CHAOS(gpe);
      handle_COMMON_CHAOS(gpe);
      output_state_COMMON_CHAOS(gpe);
   }
}

void		state_DISCRETE_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		p = chaos_p->idx;
   double	*x_g[chaos_p->P];
   double	u_i;
   int		i;

   prepare_state_vector(chaos_p, chaos_p->x, x_g);

   for (i = 0; i < N(p); i++)
   {
      chaos_p->p[i] = potential(chaos_p, x_g, chaos_p->w, i);
      chaos_p->x[i] = ff(chaos_p->p[i] - chaos_p->theta[i] + chaos_p->I[i], chaos_p->g);//chaos_p->h_s * dxdt[i];
   }

   /* activity(chaos_p, x_g, chaos_p->w); */

   /* memcpy(chaos_p->x, chaos_p->x_, N(p) * sizeof(double)); */
   chaos_p->t_s += chaos_p->h_s;
}

void		weight_DISCRETE_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		p = chaos_p->idx;
   int		i, j;
   GList		*q_list;

   weights(chaos_p, chaos_p->x);
   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      int		q = GPOINTER_TO_INT(q_list->data);
      int k;
      for (k = 0; k < N(p) * N(q); k++)
      {
         chaos_p->w[q][k] += chaos_p->d[q][k];
      }
   }
   chaos_p->t_w += chaos_p->h_w;
}
