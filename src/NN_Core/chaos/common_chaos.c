#include		<math.h>
#include		<glib.h>
#include		<libx.h>
/* #include		<gsl/gsl_cblas.h> */
/* #include <gsl/gsl_rng.h> */
#include		<Kernel_Function/find_input_link.h>
#include		<Kernel_Function/prom_getopt.h>

#include		"tools/include/chaos.h"
#include		"tools/include/ff.h"

#include		"NN_Core/chaos/common_chaos.h"

/* variabel globale .... brrr */

static GHashTable	*pq_direct_table = NULL;
static GHashTable	*pq_inverse_table = NULL;
static GOnce		pq_once = G_ONCE_INIT;
static int		P = 0;
static int		*N_array = NULL;

static double		feedback( type_chaos *chaos_p,  double *x, int i);
static double	local_field( int N,  double *J,  double *x);
static void	delta( type_chaos *chaos_p,  double *x_p, int i);

static void *compute_state(gpointer query, gpointer data);
static void *compute_weight(gpointer query, gpointer data) ;

static void	*pq_hash_new(void *arg)
{
   /* puts(__PRETTY_FUNCTION__); */
   int gpe, k ;
   GList *gpe_list;

   (void)arg;

   /* gsl_rng_env_setup(); */

   pq_direct_table = g_hash_table_new(g_direct_hash, g_direct_equal);
   pq_inverse_table = g_hash_table_new(g_direct_hash, g_direct_equal);

   for (gpe = 0; gpe < nb_max_groupes; gpe++)
   {
      switch (def_groupe[gpe].type)
      {
         case No_KO_Continu:
         case No_KO_Discret:
            printf("gpe %d (%s) is %d\n", gpe, def_groupe[gpe].nom, P);
            g_hash_table_insert(pq_direct_table, GINT_TO_POINTER(gpe), GINT_TO_POINTER(P));
            g_hash_table_insert(pq_inverse_table, GINT_TO_POINTER(P), GINT_TO_POINTER(gpe));
            P++;
            break;

         default :
            break;
      }
   }

   N_array = malloc(sizeof(int) * P);

   for (gpe_list = g_hash_table_get_keys(pq_direct_table); gpe_list != NULL; gpe_list = g_list_next(gpe_list))
   {
      gpe = GPOINTER_TO_INT(gpe_list->data);
      k = gpe_to_idx(gpe);
      N_array[k] = def_groupe[gpe].nbre;
   }

   return NULL;
}

int N(int k)
{
   return N_array[k];
}

int		gpe_to_idx(int gpe)
{
   return GPOINTER_TO_INT(g_hash_table_lookup(pq_direct_table, GINT_TO_POINTER(gpe)));
}

int		idx_to_gpe(int idx)
{
   return GPOINTER_TO_INT(g_hash_table_lookup(pq_inverse_table, GINT_TO_POINTER(idx)));
}

void		new_COMMON_CHAOS(int gpe)
{
   /* puts(__PRETTY_FUNCTION__); */
   GList *q_list;
   int		size;
   int p;
   int		deb ;
   int		nbre ;
   int		i, i_, q;
   int depart ;
   int arrivee ;
   int type_liaison ;
   int type_depart ;

   type_chaos	*chaos_p = ALLOCATION(type_chaos);
   type_coeff	*J_i;

   g_once(&pq_once, pq_hash_new, NULL);
   p = gpe_to_idx(gpe);

   chaos_p->idx = p;
   chaos_p->P = P;
   chaos_p->beta = 0.01;
   chaos_p->t_s = 0;
   chaos_p->h_s = 0;

   chaos_p->t_w = 0;
   chaos_p->h_w = 0;

   chaos_p->s_step = -1;
   chaos_p->w_step = -1;
   chaos_p->g = -1.0;

   chaos_p->n = 0;
   chaos_p->k = -1;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   chaos_p->q = NULL;
   for (i = deb; i < deb + nbre; i++)
   {
      i_ = i - deb;
      for (J_i = neurone[i].coeff; J_i != NULL; J_i = J_i->s)
      {
         depart = liaison[J_i->gpe_liaison].depart;
         arrivee = liaison[J_i->gpe_liaison].arrivee;
         type_liaison = liaison[J_i->gpe_liaison].type;
         type_depart = def_groupe[depart].type;

         /* g_assert(gpe == arrivee); */
         switch (type_depart)
         {
            case No_KO_Continu:
            case No_KO_Discret:
               {
                  if (type_liaison != No_l_1_t)
                  {
                     fprintf(stderr, "1 -> T expected\n");
                     exit(EXIT_FAILURE);
                  }
                  else
                  {
                     q = gpe_to_idx(depart);
                     if (chaos_p->q == NULL || g_list_find(chaos_p->q, GINT_TO_POINTER(q)) == NULL)
                     {
                        /* printf("Link between %d -> %d\n", q, p); */
                        chaos_p->q = g_list_append(chaos_p->q, GINT_TO_POINTER(q));
                     }
                  }
               }
               break;
         }
      }
   }



   /* chaos_p->state_handle = calloc(N_array[p], sizeof(t_state_arg)); */
   /* chaos_p->weight_handle = calloc(N_array[p], sizeof(t_weight_arg)); */
   chaos_p->w = calloc(P, sizeof(double *));
   chaos_p->d = calloc(P, sizeof(double *));
   chaos_p->h_pq = calloc(P, sizeof(double *));
   chaos_p->alpha = calloc(P, sizeof(double));

   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      q = GPOINTER_TO_INT(q_list->data);
      chaos_p->h_pq[q] = calloc(N_array[p], sizeof(double));
      chaos_p->w[q] = calloc(N_array[p] * N_array[q], sizeof(double));
      chaos_p->d[q] = calloc(N_array[p] * N_array[q], sizeof(double));
   }


   /* vad_init(&chaos_p->sh_11); */
   /* vad_init(&chaos_p->sh_01); */
   /* vad_init(&chaos_p->active); */
   /* vad_init(&chaos_p->inactive); */
   /* vad_init(&chaos_p->saturated); */
   /* vad_init(&chaos_p->dynamic); */

   /* chaos_p->hi_01 = calloc(N_array[p], sizeof(vad_t)); */
   /* chaos_p->hi_11 = calloc(N_array[p], sizeof(vad_t)); */

   /* for (i = 0; i < N_array[p]; i++) */
   /*   { */
   /*     vad_init(&chaos_p->hi_11[i]); */
   /*     vad_init(&chaos_p->hi_01[i]); */
   /*   } */


   chaos_p->x = calloc(N_array[p], sizeof(double));

   chaos_p->p = calloc(N_array[p], sizeof(double));
   chaos_p->p_ = calloc(N_array[p], sizeof(double));
   chaos_p->x1 = calloc(N_array[p], sizeof(double));
   chaos_p->x_ = calloc(N_array[p], sizeof(double));
   /* chaos_p->u = calloc(N_array[p], sizeof(double)); */
   chaos_p->phi = calloc(N_array[p], sizeof(double));
   chaos_p->theta = calloc(N_array[p], sizeof(double));
   chaos_p->xm = calloc(N_array[p], sizeof(double));
   chaos_p->xm1 = calloc(N_array[p], sizeof(double));
   chaos_p->I = calloc(N_array[p], sizeof(double));


   chaos_p->f_q = -1;

   def_groupe[gpe].data = chaos_p;

   init_COMMON_CHAOS(gpe);
}

void		init_COMMON_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		deb = def_groupe[gpe].premier_ele;
   int		nbre = def_groupe[gpe].nbre;
   int		i;
   int		p = chaos_p->idx;

   int lien_entrant;
   char param_link[256];
   int l = 0;

   int w_step = 1;
   int s_step = 1;
   int f_q = -1;
   float g = 8.0;
   float beta = 0.1;
   int p_gpe;
   double val;
   int i_;
   int	q, q_gpe;


   /* gsl_rng * r; */

   /*  r = gsl_rng_alloc (gsl_rng_default); */

   for (i = deb; i < deb + nbre; i++)
   {
      /* double val = gsl_rng_uniform(r) * 0.1; */
      val = 0;
      i_ = i - deb;

      chaos_p->x[i_] = val;
      neurone[i].s1 = val;
   }

   /* gsl_rng_free (r); */

   p_gpe = gpe;

   for (i = 0; i < nbre_liaison; i++)
   {
      GList *q_list;

      for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
      {
         q = GPOINTER_TO_INT(q_list->data);
         q_gpe = idx_to_gpe(q);

         if (liaison[i].depart == q_gpe && liaison[i].arrivee == p_gpe)
         {
            chaos_p->alpha[q] = liaison[i].temps;
            printf("alpha %d -> %d = %f\n", q, p, chaos_p->alpha[q]);
         }
      }
   }


   while ((lien_entrant = find_input_link(gpe, l)) != -1)
   {
      prom_getopt_int(liaison[lien_entrant].nom, "-f_q", &f_q);
      prom_getopt_int(liaison[lien_entrant].nom, "-w_step", &w_step);
      prom_getopt_int(liaison[lien_entrant].nom, "-s_step", &s_step);
      prom_getopt_float(liaison[lien_entrant].nom, "-g", &g);
      prom_getopt_float(liaison[lien_entrant].nom, "-beta", &beta);
      l++;
   }


   printf("w_step set to %d\n", w_step);
   printf("s_step set to %d\n", s_step);
   printf("g set to %f\n", g);
   printf("beta set to %f\n", beta);
   printf("Feedback link is (p,q) = (%d,%d)\n", chaos_p->idx, f_q);

   chaos_p->w_step = w_step;
   chaos_p->s_step = s_step;
   chaos_p->f_q = f_q;
   chaos_p->g = g;
   chaos_p->beta = beta;
}

void		destroy_COMMON_CHAOS(int gpe)
{
   /* puts(__PRETTY_FUNCTION__); */
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int q;

   for (q = 0; q < P; q++)
   {
      if (chaos_p->w[q] != NULL)
      {
         free(chaos_p->w[q]);
      }
      if (chaos_p->d[q] != NULL)
      {
         free(chaos_p->d[q]);
      }
      if (chaos_p->h_pq[q] != NULL)
      {
         free(chaos_p->h_pq[q]);
      }
   }

   free(chaos_p->w);
   free(chaos_p->d);
   free(chaos_p->x);
   free(chaos_p->x1);
   free(chaos_p->x_);
   free(chaos_p->h_pq);
   /* free(chaos_p->u); */
   free(chaos_p->phi);
   free(chaos_p->theta);
   free(chaos_p->xm);
   free(chaos_p->xm1);
   free(chaos_p->I);
   free(chaos_p);
}

void		input_COMMON_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   type_coeff	*J_i;
   int		deb = def_groupe[gpe].premier_ele;
   int		nbre = def_groupe[gpe].nbre;
   int		i, i_;
   int		p = chaos_p->idx;
   int depart ;
   int arrivee ;
   int type_liaison ;
   int type_depart ;
   int k ;
   int lien;

   memcpy(chaos_p->x1, chaos_p->x, N_array[p] * sizeof(double));
   /* chaos_p->t_s1 = chaos_p->t_s; */
   for (i = deb; i < deb + nbre; i++)
   {
      i_ = i - deb;
      chaos_p->theta[i_] = neurone[i].seuil;
      for (J_i = neurone[i].coeff; J_i != NULL; J_i = J_i->s)
      {
         depart = liaison[J_i->gpe_liaison].depart;
         arrivee = liaison[J_i->gpe_liaison].arrivee;
         type_liaison = liaison[J_i->gpe_liaison].type;
         type_depart = def_groupe[depart].type;

         switch (type_depart)
         {
            case No_KO_Continu:
            case No_KO_Discret:
               {
                  if (type_liaison != No_l_1_t)
                  {
                     fprintf(stderr, "1 -> T expected\n");
                     exit(EXIT_FAILURE);
                  }
                  else
                  {
                     type_chaos *chaos_q = def_groupe[depart].data;
                     int p = chaos_p->idx;
                     int q = chaos_q->idx;
                     int j_ = J_i->entree - def_groupe[depart].premier_ele;
                     chaos_p->w[q][N_array[q] * i_ + j_] = J_i->val;
                  }
               }
               break;

            default :
               if (type_liaison != No_l_1_1_non_modif)
               {
                  fprintf(stderr, "1 -> 1 expected\n");
                  exit(EXIT_FAILURE);
               }
               else
               {
                  chaos_p->I[i_] = neurone[J_i->entree].s1;
               }
               break;
         }
      }
   }

   k = 0;
   chaos_p->learn  = def_groupe[gpe].learning_rate;

   do
   {
      lien = find_input_link(gpe, k);
      if (lien != -1)
      {
         if (!strcmp(liaison[lien].nom, "g"))
         {
            chaos_p->g = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }


         if (!strcmp(liaison[lien].nom, "f_q"))
         {
            chaos_p->f_q = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }

         if (!strcmp(liaison[lien].nom, "h_s"))
         {
            chaos_p->h_s = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }
         if (!strcmp(liaison[lien].nom, "h_w"))
         {

            chaos_p->h_w = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }
         if (!strcmp(liaison[lien].nom, "w_step"))
         {
            chaos_p->w_step = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }
         if (!strcmp(liaison[lien].nom, "s_step"))
         {
            chaos_p->s_step = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
         }

         if (!strcmp(liaison[lien].nom, "learn"))
         {
            chaos_p->learn  = def_groupe[gpe].learning_rate *
                              neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
            /* printf("%d -> %f\n", gpe, chaos_p->learn); */
         }
         k++;
      }
   }
   while (lien != -1);

   chaos_p->k++;
}

void		output_state_COMMON_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		i, i_;
   int		deb = def_groupe[gpe].premier_ele;
   int		nbre = def_groupe[gpe].nbre;

   for (i = deb; i < deb + nbre; i++)
   {
      // TODO vectorize
      int i_ = i - deb;

      neurone[i].s1 = chaos_p->x[i_];
      neurone[i].s2 = chaos_p->x[i_];

      if (chaos_p->f_q != -1)
      {
         neurone[i].s = ff(chaos_p->h_pq[chaos_p->f_q][i_] - chaos_p->theta[i_], chaos_p->g);
      }
   }
}

void		output_weight_COMMON_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   type_coeff	*J_i;
   int		deb = def_groupe[gpe].premier_ele;
   int		nbre = def_groupe[gpe].nbre;
   int		i, j_, i_;
   int p, q;

   for (i = deb; i < deb + nbre; i++)
   {
      i_ = i - deb;
      for (J_i = neurone[i].coeff; J_i != NULL; J_i = J_i->s)
      {
         int depart = liaison[J_i->gpe_liaison].depart;
         int arrivee = liaison[J_i->gpe_liaison].arrivee;
         int type_liaison = liaison[J_i->gpe_liaison].type;
         int type_depart = def_groupe[depart].type;

         switch (type_depart)
         {
            case No_KO_Continu:
            case No_KO_Discret:
               {
                  if (type_liaison != No_l_1_t)
                  {
                     fprintf(stderr, "1 -> T expected\n");
                     exit(EXIT_FAILURE);
                  }
                  else
                  {
                     type_chaos *chaos_q = def_groupe[depart].data;
                     p = chaos_p->idx;
                     q = chaos_q->idx;
                     j_ = J_i->entree - def_groupe[depart].premier_ele;
                     J_i->val = chaos_p->w[q][N_array[q] * i_ + j_];
                  }
               }
               break;

            default :
               break;
         }
      }
   }

}

/*
  Calcul de moyenne glissante de l'état pour chaque neurone
*/
void		handle_COMMON_CHAOS(int gpe)
{
   type_chaos	*chaos_p = def_groupe[gpe].data;
   int		i;
   int		p = chaos_p->idx;
   double	beta = chaos_p->beta;

   for (i = 0; i < N_array[p]; i++)
   {
      chaos_p->xm[i] = chaos_p->xm[i] * (1 - beta) + chaos_p->x[i] * beta;
      chaos_p->xm1[i] = chaos_p->xm1[i] * (1 - beta) + chaos_p->x1[i] * beta;

      /* chaos_p->xm[i] = (chaos_p->n * chaos_p->xm[i] + chaos_p->x[i]) / (chaos_p->n + 1); */
      /* chaos_p->xm1[i] = (chaos_p->n * chaos_p->xm1[i] + chaos_p->x1[i]) / (chaos_p->n + 1); */
   }
   chaos_p->n++;
}

void		prepare_state_vector( type_chaos *chaos_p,  double *x, double *x_g[])
{
   GList		*q_list;
   int k;

   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      k = GPOINTER_TO_INT(q_list->data);
      if (k == chaos_p->idx)
      {
         x_g[k] = x;
      }
      else
      {
         type_chaos *chaos_q = def_groupe[idx_to_gpe(k)].data;
         x_g[k] = chaos_q->x;
      }
   }
}

void		prepare_weight_vector( type_chaos *chaos_p,  type_chaos *chaos_q,
                                double *w, double *w_g[])
{
   GList		*q_list;
   int k;

   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      k = GPOINTER_TO_INT(q_list->data);

      if (k == chaos_q->idx)
      {
         w_g[k] = w;
      }
      else
      {
         w_g[k] = chaos_p->w[k];
      }
   }
}

/* void		activity(const type_chaos *chaos_p, const double **x, const double **w_p) */
/* { */
/*   int		i, j; */
/*   int		p = chaos_p->idx; */

/*   for (i = 0; i < N_array[p]; i++) */
/*     { */
/*       chaos_p->p[i] = potential(chaos_p, x, w_p, i); */
/*       chaos_p->ff */
/*     } */
/* } */




void		weights(type_chaos *chaos_p,  double *x)
{
   int		p = chaos_p->idx;
   int		i;


   /* printf("learn %f alpha_q %f\n", chaos_p->learn, chaos_p->alpha[q]); */
   for (i = 0; i < N_array[p]; i++)
   {
      delta(chaos_p, x, i);
   }
}

void		recupere_micro_neurone(int gpe)
{
   int		i, deb, nbre;
   type_coeff	*coeff;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   for (i = deb; i < deb + nbre; i++)
   {
      coeff = neurone[i].coeff;
      /* mega beurk, mais ca marche ... */
      /* recupere la valeur du micro-neurone juste avant le macro-neurone */
      neurone[i].s = neurone[(coeff->entree) - 1].s;
   }
}


void		delta_q( type_chaos *chaos_p,  type_chaos *chaos_q,  double *x_p, int i, int q, double *dw)
{
   int	j;
   double phi, learning_rate, K;

   phi = 1.0 - ff(chaos_p->p[i] - chaos_p->theta[i], chaos_p->g);
   learning_rate = chaos_p->learn * chaos_p->alpha[q];
   K = (x_p[i] - chaos_p->xm[i]) * ((learning_rate * phi) / (float)N(q));


   for (j = 0; j < N_array[q]; j++)
   {
      dw[j] = K * (chaos_q->x1[j] - chaos_q->xm1[j]);
   }
}

static double		feedback( type_chaos *chaos_p,
                            double *x, int i)
{
   double u;

   (void) x;
   /* double u = ff_inv(x[i], chaos_p->g) - chaos_p->I[i]; */
   u = chaos_p->p[i] - chaos_p->theta[i];
   return ff(u, chaos_p->g);
}

static void		delta( type_chaos *chaos_p,
                      double *x_p, int i)
{
   int		p = chaos_p->idx;
   GList		*q_list;
   double *dw;

   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      int		q = GPOINTER_TO_INT(q_list->data);

      /* printf("p = %d, N(%d) = %d\n", p, q, N(q)); */
      type_chaos	*chaos_q = def_groupe[idx_to_gpe(q)].data;

      dw = &chaos_p->d[q][i * N_array[q]];
      delta_q(chaos_p, chaos_q, x_p, i, q, dw);
   }
}


static double	local_field( int size, double *J,  double *x)
{
   double	h = 0;
   int		j;

   /* return cblas_ddot(size, x, 1, J, 1); */

   for (j = 0; j < size; j++)
   {
      h += J[j] * x[j];
   }

   return h;
}



double	potential( type_chaos *chaos_p,  double **x, double **w_p, int i)
{
   double	sum_h_pq_i = 0;
   double h_pq_i ;
   int r;
   GList		*q_list;

   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
   {
      r = GPOINTER_TO_INT(q_list->data);
      h_pq_i = local_field(N_array[r], &w_p[r][i * N_array[r]], x[r]);

      chaos_p->h_pq[r][i] = h_pq_i;
      sum_h_pq_i += h_pq_i;
   }

   return sum_h_pq_i;
}

