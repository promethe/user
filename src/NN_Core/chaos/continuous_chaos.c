/* Fonctions d'un reseau de neurones recurrent aleatoire a TEMPS CONTINU */
/* fonctions non validees */
/* Mathias Quoy fevrier 2005 */


/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M. Quoy, Nicolas CAZIN
- description: specific file creation
- date: 01/03/2005, 25/09/2014

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-float ddouleur

Global variables:
-none

Internal Tools:
-none

External Tools:
-apprend_coeff_colonne
-calcule_mc_produit_max()
-calcule_mc_produit()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <assert.h>

#include "tools/include/ff.h"
#include "NN_Core/chaos/common_chaos.h"
#include "NN_Core/chaos/continuous_chaos.h"
#include "tools/include/chaos.h"

/* #include <gsl/gsl_errno.h> */
/* #include <gsl/gsl_matrix.h> */
/* #include <gsl/gsl_odeiv2.h> */
/* #include <gsl/gsl_rng.h> */
/* #include <gsl/gsl_randist.h> */

/* static const double	epsabs = 1e30; */
/* static const double	epsrel = 1e30; */

/* typedef struct */
/* { */
/*   gsl_odeiv2_driver	*driver; */
/*   gsl_odeiv2_system	system; */
/* }			t_ode; */

/* typedef struct		s_internal */
/* { */
/*   t_ode			state; */
/*   t_ode			*weight; */
/* }			t_internal; */

extern void		learn_CONTINUOUS_CHAOS(int gpe);   /* defini dans common_chaos.c */
extern void		weights(const type_chaos *chaos_p, const double *x);
extern void		output_weight_COMMON_CHAOS(int gpe);

static int		potential_time_derivative (double t, const double P[], double dPdt[], void *params)
{
  type_chaos		*chaos_p = params;
  int			p = chaos_p->idx;
  double		*x_g[chaos_p->P];
  int			i;
  UNUSED_PARAM t;
  
  prepare_state_vector(chaos_p, chaos_p->x, x_g);
  /* activity(chaos_p, x_g, chaos_p->w); */

  for (i = 0; i < N(p); i++)
    {
      dPdt[i] = -P[i] + potential(chaos_p, x_g, chaos_p->w, i);
    }
  
  /* return GSL_SUCCESS; */
  return 0;
}


/* static int		state_time_derivative (double t, const double x[], double dxdt[], void *params) */
/* { */
/*   type_chaos		*chaos_p = params; */
/*   int			p = chaos_p->idx; */
/*   double		*x_g[chaos_p->P]; */
/*   int			i; */

  /* prepare_state_vector(chaos_p, x, x_g); */
  /* activity(chaos_p, x_g, chaos_p->w); */

  /* for (i = 0; i < N(p); i++) */
  /*   { */
  /*     dxdt[i] = -x[i] + chaos_p->x_[i]; */
  /*   } */
  
/*   return GSL_SUCCESS; */
/* } */


/* static int		weight_time_derivative(double t, const double w[], double fw[], void *params) */
/* { */
/*   type_chaos		*chaos_p = def_groupe[((int *)params)[0]].data; */
/*   type_chaos		*chaos_q = def_groupe[((int *)params)[1]].data; */
/*   double		*x_g[chaos_p->P]; */
/*   double		*w_g[chaos_p->P]; */
/*   int			p = chaos_p->idx; */
/*   int			q = chaos_q->idx; */

/*   prepare_state_vector(chaos_p, chaos_p->x, x_g); */
/*   prepare_weight_vector(chaos_p, chaos_q, w, w_g); */

  /* activity(chaos_p, x_g, w_g); */

/*   int i; */
/*   for (i = 0; i < N(p); i++) */
/*     { */
/*       delta_q(chaos_p, chaos_q, chaos_p->x_, i, q, &fw[i * N(q)]); */
/*     } */
/*   return GSL_SUCCESS; */
/* } */


void			new_CONTINUOUS_CHAOS(int gpe)
{
  new_COMMON_CHAOS(gpe);
  {
    type_chaos		*chaos_p = def_groupe[gpe].data;
    int			p = chaos_p->idx;

    {
      int lien_entrant;
      char param_link[256];
      int l = 0;

      float h_w = 0.1;
      float h_s = 0.1;

      while ((lien_entrant = find_input_link(gpe, l)) != -1)
	{
	  prom_getopt_float(liaison[lien_entrant].nom, "-h_s", &h_s);
	  prom_getopt_float(liaison[lien_entrant].nom, "-h_w", &h_w);
	  l++;
	}
      
      printf("%d h_s set to %f\n", gpe, h_s);
      printf("%d h_w set to %f\n", gpe, h_w);

      chaos_p->h_s = h_s;
      chaos_p->h_w = h_w;
    }
    /* { */
    /*   t_internal		*internal = ALLOCATION(t_internal); */
    /*   int			k; */

    /*   internal->state.system.function = state_time_derivative; */
    /*   internal->state.system.jacobian = NULL; */
    /*   internal->state.system.dimension = N(p); */
    /*   internal->state.system.params = chaos_p; */
	  

    /*   internal->state.driver = gsl_odeiv2_driver_alloc_y_new(&internal->state.system, */
    /* 							     gsl_odeiv2_step_rk4, */
    /* 							     chaos_p->h_s, epsabs, epsrel); */
    
    /*   gsl_odeiv2_driver_set_hmin (internal->state.driver, chaos_p->h_s); */
    /*   gsl_odeiv2_driver_set_hmax (internal->state.driver, chaos_p->h_s); */
    /*   gsl_odeiv2_driver_set_nmax (internal->state.driver, 1); */
    /*   internal->weight = calloc(chaos_p->P, sizeof(t_ode)); */

    /*   GList *q_list; */

    /*   for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list)) */
    /* 	{ */
    /* 	  int		q = GPOINTER_TO_INT(q_list->data); */
	    
    /* 	  internal->weight[q].system.function = weight_time_derivative; */
    /* 	  internal->weight[q].system.jacobian = NULL; */
    /* 	  internal->weight[q].system.dimension = N(p) * N(q); */
				      
    /* 	  int		*pair = calloc(2, sizeof(int)); */
	    
    /* 	  pair[0] = idx_to_gpe(p); */
    /* 	  pair[1] = idx_to_gpe(q); */
    /* 	  internal->weight[q].system.params = pair; */
	    
    /* 	  internal->weight[q].driver = gsl_odeiv2_driver_alloc_y_new(&internal->weight[q].system, */
    /* 								     gsl_odeiv2_step_rk8pd, */
    /* 								     chaos_p->h_w, */
    /* 								     epsabs, epsrel); */

    /* 	  gsl_odeiv2_driver_set_hmin (internal->weight[q].driver, chaos_p->h_w); */
    /* 	  gsl_odeiv2_driver_set_hmax (internal->weight[q].driver, chaos_p->h_w); */
    /* 	  gsl_odeiv2_driver_set_nmax (internal->weight[q].driver, 1); */
    /* 	} */
      
    /*   chaos_p->handle = internal; */
    /* } */
  }
}
  
void			destroy_CONTINUOUS_CHAOS(int gpe)
{
  /* type_chaos		*chaos_p = def_groupe[gpe].data; */
  /* t_internal		*internal = chaos_p->handle; */
  /* int			p = chaos_p->idx; */
  /* int			k; */
  /* GList			*q_list; */
  
  /* for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list)) */
  /*   { */
  /*     int q = GPOINTER_TO_INT(q_list->data); */
      
  /*     gsl_odeiv2_driver_free(internal->weight[q].driver); */
  /*     free(internal->weight[q].system.params); */
  /*   } */
  /* free(internal->weight); */
  /* gsl_odeiv2_driver_free(internal->state.driver); */

  destroy_COMMON_CHAOS(gpe);
}

void			update_CONTINUOUS_CHAOS(int gpe)
{
  type_chaos		*chaos_p = def_groupe[gpe].data;

  /* int k; */
  /*  for (k = 0; k < chaos_p->s_step; k++) */
  input_COMMON_CHAOS(gpe);
  if (chaos_p->k % chaos_p->s_step == 0)
    {
      state_CONTINUOUS_CHAOS(gpe);
      handle_COMMON_CHAOS(gpe);
      output_state_COMMON_CHAOS(gpe);
    }
  
}

void			learn_CONTINUOUS_CHAOS(int gpe)
{
  type_chaos		*chaos_p = def_groupe[gpe].data;
 /* int k; */
 /*  for (k = 0; k < chaos_p->w_step; k++) */
  if (chaos_p->k % chaos_p->w_step == 0)
    {
      weight_CONTINUOUS_CHAOS(gpe);
      output_weight_COMMON_CHAOS(gpe);
    }
}

void			state_CONTINUOUS_CHAOS(int gpe)
{
  type_chaos	*chaos_p = def_groupe[gpe].data;
  /* t_internal	*internal = chaos_p->handle; */
  int p = chaos_p->idx;
  double dPdt[N(p)];
  int i;
  
  double	*x_g[chaos_p->P];
  
  /* prepare_state_vector(chaos_p, chaos_p->x, x_g); */
  potential_time_derivative (chaos_p->t_s, chaos_p->p, dPdt, chaos_p);
  for (i = 0; i < N(p); i++)
    {
      chaos_p->p[i] += chaos_p->h_s * dPdt[i];
      chaos_p->x[i] = ff(chaos_p->p[i] - chaos_p->theta[i] + chaos_p->I[i], chaos_p->g);//chaos_p->h_s * dxdt[i];
    }

  chaos_p->t_s += chaos_p->h_s;


  /* int status= gsl_odeiv2_driver_apply_fixed_step (internal->state.driver, */
  /* 						  &chaos_p->t_s, chaos_p->h_s,1, */
  /* 						  chaos_p->x); */
  /* if (status != GSL_SUCCESS) */
  /*   { */
  /*     GSL_ERROR(gsl_strerror(status), status); */
  /*   } */
    
}

void			weight_CONTINUOUS_CHAOS(int gpe)
{
  type_chaos		*chaos_p = def_groupe[gpe].data;
  /* t_internal		*internal = chaos_p->handle; */
  GList			*q_list;
  int			p = chaos_p->idx;

  /* printf("%d, T_w = %f\n", gpe, chaos_p->t_w); */
  /* for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list)) */
  /*   { */
  /*     int q = GPOINTER_TO_INT(q_list->data); */

      /* double t = chaos_p->t_w; *//* internal->weight[q].system.params */
      
      /* weight_time_derivative(chaos_p->t_w, chaos_p->w[q], chaos_p->d[q], internal->weight[q].system.params); */


      /* memcpy(chaos_p->d[q], chaos_p->w[q], N(p) * N(q) * sizeof(double)); */


      /* int status = gsl_odeiv2_driver_apply_fixed_step (internal->weight[q].driver, */
      /* 						       &t, chaos_p->h_w, 1, */
      /* 						       chaos_p->d[q]); */

      /* if (status != GSL_SUCCESS) */
      /* 	{ */
      /* 	  GSL_ERROR(gsl_strerror(status), status); */
      /* 	} */
    /* } */

  weights(chaos_p, chaos_p->x);

  for (q_list = chaos_p->q; q_list != NULL; q_list = g_list_next(q_list))
    {
      int q = GPOINTER_TO_INT(q_list->data);
      
      int k;
      
      for (k = 0; k < N(p) * N(q); k++)
  	{
  	  chaos_p->w[q][k] += (chaos_p->h_w * chaos_p->d[q][k]);
  	}

      /* memcpy(chaos_p->w[q], chaos_p->d[q], N(q) * N(p) * sizeof(double)); */
    }

  /* printf("learn HW = %f\n", chaos_p->h_w); */

  chaos_p->t_w += chaos_p->h_w;
}
