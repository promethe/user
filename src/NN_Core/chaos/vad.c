#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include	<math.h>
#include	<assert.h>
#include	"tools/include/vad.h"

void	vad_init(vad_t *vad)
{
   memset(vad, 0, sizeof(vad_t));
}

void vad_update(vad_t *vad, double x)
{
   if (vad->n >= VAD_SIZE || vad->n < 0)
   {
      printf("vad->n = %d\n", vad->n);
   }

   assert(vad != 0);
   assert(vad->n >= 0);
   assert(vad->n < VAD_SIZE);

   vad->x[vad->n] = x;
   vad->n++;
   vad->n %= VAD_SIZE;
   if (vad->k < VAD_SIZE)
   {
      vad->k++;
   }


   /* vad->n++; */


   /* if (vad->n == 1) */
   /*   { */
   /*     vad->oldM = vad->newM = x; */
   /*     vad->oldS = 0; */
   /*   } */
   /* else */
   /*   { */
   /*     vad->newM = vad->oldM + (x - vad->oldM)/vad->n; */
   /*     vad->newS = vad->oldS + (x - vad->oldM)*(x - vad->newM); */

   /*     vad->oldM = vad->newM; */
   /*     vad->oldS = vad->newS; */
   /*   } */
}

double vad_mean(const vad_t *vad)
{
   /* return vad->n > 0 ? vad->newM : 0; */
   int k;
   double sum = 0;
   for (k = 0; k < vad->k; k++)  sum += vad->x[k];

   return sum / vad->k;
}

double vad_variance(const vad_t *vad)
{
   double m;
   int k;
   double sum = 0;
   double v;

   m = vad_mean(vad);

   for (k = 0; k < vad->k; k++)
   {
      v = (vad->x[k] - m);
      sum += v * v;
   }
   /* return vad->n > 1 ? vad->newS/(vad->n - 1) : 0; */
   return sum / vad->k;
}

double vad_stddev(const vad_t *vad)
{
   return sqrt(vad_variance(vad));
}

