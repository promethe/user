/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/* calcul de la moyenne des etats des neurones */
/* sortrie a mettre dans un fichier par exemple */
/* Mathias Quoy */


/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file 
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M. Quoy
- description: specific file creation
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-float ddouleur

Global variables:
-none

Internal Tools:
-none

External Tools:
-apprend_coeff_colonne
-calcule_mc_produit_max()
-calcule_mc_produit()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>


void function_observable_moyen(int gpe)
{
    int i, deb, nbre, nb_coeff;
    type_coeff *coeff;
    float somme = 0.;
    float avg ;

    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;

    for (i = deb; i < deb + nbre; i++)
    {
        coeff = neurone[i].coeff;
        somme = 0.;
        nb_coeff = 0;

        while (coeff != NULL)
        {
            somme += neurone[coeff->entree].s1;
            nb_coeff++;
            coeff = coeff->s;
        }

	avg = (nb_coeff > 0) ? (somme / nb_coeff) : (0.0);
	neurone[i].s = avg;
	neurone[i].s1 = avg;
	neurone[i].s2 = avg;
    }
}
