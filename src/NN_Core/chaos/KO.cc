/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "public.h"




/*float g;*/
float borne_min = 2.;
float borne_max = -1.;
float transitoire = 20.;
float alpha_KO;

float ff(float x, float g)
{
return (1.0/(1.0+exp(-2.*g*x)));
}

float alea_gauss(float moyenne,float ecart_type)
{
     float Xt,r1,r2;

     r1=(float)drand48();
     r2=(float)drand48();

     Xt=(float)sqrt(-2.0*ecart_type*ecart_type*log(r1))*cos(2.0*M_PI*r2)+moyenne;
     return(Xt);
}


void calcule_chaos(int i/*,int gestion_STM,int learn*/)
{
  float somme=0.;
  float g;		/* gain */
  type_coeff *coeff;

  /*printf("attente\n");*/
  /*scanf("%f");*/

  coeff=neurone[i].coeff;
  g= def_groupe[neurone[i].groupe].sigma_f;

  while (coeff!=nil)
	       {
		 if (coeff->evolution ==1)
		   somme += coeff->val*neurone[coeff->entree].s1;
		 else
		   somme += coeff->val*neurone[coeff->entree].s;

               	coeff=coeff->s;
	       }

  /* calcul de ui */
  neurone[i].s = ff(somme - neurone[i].seuil,g);

  /*printf("Dans chaos de %d %f %f\n",i,somme,neurone[i].s);*/
}

int nb_passages_dans_apprend_KO=0;


/* il faut retrancher la valeur moyenne de l'etat en entree et en sortie */
void apprend_KO(int gpe)
{
  int i,deb,nbre,deb_entree;
  type_coeff *coeff;
  float *moyenne,*moyenne_entree;
  float g, alpha_eff;		/* gain */

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  g=def_groupe[gpe].sigma_f;

  moyenne = (float*)def_groupe[gpe].ext;

/*moyenne_entree= (float*)def_groupe[gpe].ext;*/
  nb_passages_dans_apprend_KO++;

  if (nb_passages_dans_apprend_KO<(3*transitoire))
    alpha_eff=0.;
  else
    alpha_eff=alpha_KO;

  printf("alpha_eff = %f\n",alpha_eff);

  printf("debut apprend KO de gpe %d\n",gpe);
  /*printf("donner un nbre:\n");
    scanf("%d",&i);*/
  for(i=deb;i<deb+nbre;i++)
    {
	 coeff=neurone[i].coeff;

     	 while (coeff!=nil)
	       {

	        if ((coeff->entree != i) && (coeff->evolution==1))
		  {
		    /* on accede au no du gpe en entree via le neurone d'entree */
		    /* on pourrait diviser par autre chose que nbre_entree, par exemple le nombre moyen de neurones actifs (soit 2*NB_AMER pour l'interface image et 1 pour l'interface motrice */
		    moyenne_entree= (float*)def_groupe[neurone[coeff->entree].groupe].ext;  
		    deb_entree=def_groupe[neurone[coeff->entree].groupe].premier_ele;
		    nbre_entree=def_groupe[neurone[coeff->entree].groupe].nbre;
		    coeff->val += (alpha_eff/nbre_entree)*(neurone[coeff->entree].s2 - moyenne_entree[coeff->entree - deb_entree])*(neurone[i].s - moyenne[i-deb]);

		    /*		    printf("coeff: %f entree: %f moyenne entree: %f courant: %f moyenne courant: %f\n",coeff->val, neurone[coeff->entree].s1, moyenne_entree[coeff->entree - deb_entree], neurone[i].s, moyenne[i]);*/
		  }

               	coeff=coeff->s;
	       }
    }

  /*printf("dans apprend KOde gpe %d\n",gpe);*/
}


void gestion_groupe_KO(int gpe)
{
  int i,deb,nbre;
  float *moyenne;
  float g;

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  g=def_groupe[gpe].sigma_f;
	 
  moyenne = (float*)def_groupe[gpe].ext;

  for(i=deb;i<deb+nbre;i++)
	{
         neurone[i].s2 = neurone[i].s1;
	 neurone[i].s1 = neurone[i].s;
	 moyenne[i-deb] = 0.9 * moyenne[i-deb] + 0.1 * neurone[i].s;
	 /*printf("%f\n", neurone[i].s1);*/
	}

  /*  printf("Dans GESTION chaos de %d\n",gpe);*/
} 



void observable_moyen(int gpe)
{
  int i,deb,nbre, nb_coeff;
  type_coeff *coeff;
  float somme = 0.;
  float g;

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  g=def_groupe[gpe].sigma_f;

 for(i=deb;i<deb+nbre;i++)
	{
	 coeff=neurone[i].coeff;
	 somme =0.;
	 nb_coeff = 0;

     	 while (coeff!=nil)
	       {
		 somme += neurone[coeff->entree].s;
		/*printf("%d %f\n",i,coeff->val);*/
		 nb_coeff ++;
               	coeff=coeff->s;
	       }
	 neurone[i].s2 = neurone[i].s1 = neurone[i].s;
	 if (nb_coeff != 0)
	   neurone[i].s =  somme/nb_coeff;
	 /*fmnet est une variable globale à déclarer */
	 /*fprintf(f_mnet,"%f\n",neurone[i].s);*/

	}
 /*printf("Dans observable moyen\n");*/
}

float changement_repere(z, b_min, b_max)
float z;
float b_min;
float b_max;
{   
return (480. * (z - b_min) / (b_max - b_min));
}

void espace_des_phases(numero)
 int numero;
 {
#ifndef AVEUGLE
   int taille_entree;
   int groupe_entree=-1;
   int i,p;
   int deb2;
   TxPoint point;


   char chaine_min[10], chaine_max[10];

   for(i=0;i<nbre_liaison;i++)
      if(liaison[i].arrivee==numero) 
        { 
          p=atoi(liaison[i].nom);
          groupe_entree=liaison[i].depart; 
          break; 
        }
   if(groupe_entree<0) return;

   taille_entree=def_groupe[groupe_entree].nbre;

   if(p<0 || p>taille_entree)
     {
       printf("ERROR: the neuron number %d should belong to [0,%d[\n",p,taille_entree);
       exit(0);
     }


   deb2=def_groupe[groupe_entree].premier_ele;



   /* ATTENTION a Temps !!! */
   if (((temps/3.)*10)>transitoire)
     {
       point.x = (int) changement_repere(neurone[p+deb2].s1, borne_min, borne_max);
       point.y = (int) changement_repere(neurone[p+deb2].s, borne_min, borne_max);

       /*printf("x = %d y = %d\n",point.x,point.y);*/

       TxDessinerRectangle(&image2,blanc,TxPlein,point,1,1,1);

       /* graduations */
       
       /* conversion flottant en string */
       sprintf(chaine_min,"%f",borne_min);
       sprintf(chaine_max,"%f",borne_max);

       /* abscisses */
       point.x = 2+ (int) changement_repere(borne_min, borne_min, borne_max);
       point.y= (int) changement_repere(borne_max, borne_min, borne_max);

       TxDessinerRectangle(&image2,vert,TxPlein,point,1,10,1);  
       point.y = point.y + 20;
       TxEcrireChaine(&image2,vert,point,chaine_min,NULL);

       point.x = (int) changement_repere(borne_max, borne_min, borne_max);
       point.y= (int) changement_repere(borne_max, borne_min, borne_max);
       TxDessinerRectangle(&image2,vert,TxPlein,point,1,10,1);
       point.y = point.y + 20;
       TxEcrireChaine(&image2,vert,point,chaine_max,NULL);

       /* ordonnees */
       point.x =(int) changement_repere(borne_min, borne_min, borne_max);
       point.y= 2 + (int) changement_repere(borne_min, borne_min, borne_max);

       TxDessinerRectangle(&image2,vert,TxPlein,point,10,1,1);  
       point.y = point.y + 20;
       TxEcrireChaine(&image2,vert,point,chaine_max,NULL);

       /*point.x = (int) changement_repere(borne_max, borne_min, borne_max);
       point.y= (int) changement_repere(borne_max, borne_min, borne_max);
       TxDessinerRectangle(&image2,vert,TxPlein,point,10,1,1);
       point.y = point.y + 20;
       TxEcrireChaine(&image2,vert,point,chaine_max,NULL);*/
     }
   else
     {
       if (neurone[p+deb2].s>borne_max)
	 borne_max = neurone[p+deb2].s;
       else
	 if (neurone[p+deb2].s<borne_min)
	   borne_min = neurone[p+deb2].s;
       /*borne_min = 0.3;
       borne_max = 0.7;*/
     }
   /*printf("bmin = %f bmax = %f\n", borne_min, borne_max);*/

#endif
}


void calcule_chaos_continu(int i)
{
  float somme=0.;
  float g;		/* gain */
  float h;
  type_coeff *coeff;
  float xi,ui;
  float k1,k2,k3,k4,increment_RK4;

  coeff=neurone[i].coeff;
  g= def_groupe[neurone[i].groupe].sigma_f;

  while (coeff!=nil)
	       {
		 /*		 if (coeff->evolution ==1)
				 somme += coeff->val*neurone[coeff->entree].s1;
				 else
				 somme += coeff->val*neurone[coeff->entree].s;*/

		 somme += coeff->val*neurone[coeff->entree].s;

		 coeff=coeff->s;
	       }

  /* calcul de ui */
  ui = ff(somme - neurone[i].seuil,g);
  xi=neurone[i].s;
  /*printf("Dans chaos de %d %f %f\n",i,somme,neurone[i].s);*/

     h=eps;

     k1= -xi+ui;   
     k2= ui-(xi+0.5*k1);   	        
     k3= ui-(xi+0.5*k2);   	        
     k4= ui-(xi+    k3);   	        

     increment_RK4=0.16666666*h*(k1+2.*k2+2.*k3+k4);
     /*neurone[n].s=neurone[n].s1=neurone[n].s2=ma_f_sigmoide(X_i+increment_RK4,g,seuil);*/

     /* ajouter un calcul différent pour s1 et s */

     neurone[i].s=neurone[i].s1=neurone[i].s2=xi+increment_RK4;
}

void gestion_groupe_KO_continu(int gpe)
{
  int i,deb,nbre;
  float *moyenne;
  float g;

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  /*g=def_groupe[gpe].sigma_f;*/
	 
  /*moyenne = (float*)def_groupe[gpe].ext;*/

  for(i=deb;i<deb+nbre;i++)
	{
         neurone[i].s2 = neurone[i].s1;
	 neurone[i].s1 = neurone[i].s;
	 /* le calcul de la valeur moyenne est fait dans coeff->moy */
	 /*moyenne[i-deb] = 0.9 * moyenne[i-deb] + 0.1 * neurone[i].s;*/



	 /*printf("%f\n", neurone[i].s1);*/
	}

  /*  printf("Dans GESTION chaos de %d\n",gpe);*/
} 


/* il faut retrancher la valeur moyenne de l'etat en entree et en sortie */
void apprend_KO_continu(int gpe)
{
  int i,deb,nbre,deb_entree;
  type_coeff *coeff;
  float alpha_eff;		/* gain */

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;


/*moyenne_entree= (float*)def_groupe[gpe].ext;*/
  nb_passages_dans_apprend_KO++;

  if (nb_passages_dans_apprend_KO<(3*transitoire))
    alpha_eff=0.;
  else
    alpha_eff=alpha_KO;

  printf("alpha_eff = %f\n",alpha_eff);

  printf("debut apprend KO de gpe %d\n",gpe);
  /*printf("donner un nbre:\n");
    scanf("%d",&i);*/
  for(i=deb;i<deb+nbre;i++)
    {
	 coeff=neurone[i].coeff;

     	 while (coeff!=nil)
	       {

	        if ((coeff->entree != i) && (coeff->evolution==1))
		  {
		    /* on accede au no du gpe en entree via le neurone d'entree */
		    /* on pourrait diviser par autre chose que nbre_entree, par exemple le nombre moyen de neurones actifs (soit 2*NB_AMER pour l'interface image et 1 pour l'interface motrice */
		    deb_entree=def_groupe[neurone[coeff->entree].groupe].premier_ele;
		    nbre_entree=def_groupe[neurone[coeff->entree].groupe].nbre;
		    coeff->val += (alpha_eff/nbre_entree)*(neurone[coeff->entree].s2 - coeff->moy)*(neurone[i].s - coeff->smoy);

		    /*		    printf("coeff: %f entree: %f moyenne entree: %f courant: %f moyenne courant: %f\n",coeff->val, neurone[coeff->entree].s1, moyenne_entree[coeff->entree - deb_entree], neurone[i].s, moyenne[i]);*/
		  }

               	coeff=coeff->s;
	       }
    }

  /*printf("dans apprend KOde gpe %d\n",gpe);*/
}
