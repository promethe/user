#include	<glib.h>
#include	<libx.h>
#include	<math.h>

typedef struct
{
   int		gpe_a;
   int		gpe_b;
}		type_learning_error;

void		new_learning_error(int gpe)
{
   int		k;
   type_learning_error *data;
   int gpe_a, gpe_b;
   GList		*gpe_list = NULL;
   
   for (k = 0; k < nbre_liaison; k++)
   {
      if (liaison[k].arrivee == gpe)
      {
         gpe_list = g_list_append(gpe_list, GINT_TO_POINTER(liaison[k].depart));
      }
   }

   if (g_list_length(gpe_list) != 2)
   {
      EXIT_ON_ERROR("two group operands are required\n");
   }

   gpe_a = GPOINTER_TO_INT(g_list_first(gpe_list)->data);
   gpe_b = GPOINTER_TO_INT(g_list_last(gpe_list)->data);

   if (def_groupe[gpe_a].nbre != def_groupe[gpe_b].nbre)
   {
      EXIT_ON_ERROR("groups %d and %d must have the same size\n", gpe_a, gpe_b);
   }

   data = ALLOCATION(type_learning_error);

   data->gpe_a = gpe_a;
   data->gpe_b = gpe_b;

   g_list_free(gpe_list);

   def_groupe[gpe].data = data;
}

void		destroy_learning_error(int gpe)
{
   free(def_groupe[gpe].data);
}

void		function_learning_error(int gpe)
{
   type_learning_error *data = def_groupe[gpe].data;
   double v_a, v_b, dot;
   
   int		gpe_a = data->gpe_a;
   int		gpe_b = data->gpe_b;
   int		deb_a = def_groupe[gpe_a].premier_ele;
   int		deb_b = def_groupe[gpe_b].premier_ele;
   int		nbre_a = def_groupe[gpe_a].nbre;
   int		nbre_b = def_groupe[gpe_b].nbre;

   int		i;

   double s_n_a = 0;
   double s_n_b = 0;
   double s = 0;

   for (i = 0; i < nbre_a; i++)
   {
      v_a = neurone[deb_a + i].s1;
      v_b = neurone[deb_b + i].s1;

      s += (v_a - v_b) * (v_a - v_b);
   }
   dot = s / nbre_a;
   i = def_groupe[gpe].premier_ele;
   neurone[i].s = neurone[i].s1 = neurone[i].s2 = dot;
}

