/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup goal goal
   \ingroup NN_Core
   
   \brief Cognitive map

   \section Description Description

   Propage dans un reseau ayant la meme topologie que les transitions
   apprises une activite liee a une motivation. L'activite peut
   ensuite etre utilisee comme biais pour selectionner les transitions
   a realiser.

   La carte cognitive s'appuie sur deux elements : des connections
   recurrentes entre les neurones de la carte cognitive pour permettre
   de propager les activites et des associations entre certaines
   transitions et les neurones d'une carte de drives lorsque d'une
   recompense est recue.


   \section Activation Activation

   Sortie d'un neurone de la carte cognitive : \f$ S_j =
   [ max( s^{motiv}_j , s^{rec}_j )]^+ \f$ 

   S'appuie sur les calculs des activites pour la recurrence et des
   activites pour les motivations : 
   
   \f[ 
   s^{rec}_j(t) = [ max_i (w_{ij}^{rec} \cdot S_i(t-1)
   )]^+ 
   \f] 
   \f[ s^{motiv}_j = [ max_m (w_{mj}^{motiv}
   \cdot E_m^{motiv} )]^+ 
   \f]

   Rq: \f$ max_i (x_i) \f$ signifie le max des \f$ x_i \f$ pour tout
   \f$ i \f$.

   Voir ci-dessous pour plus de details sur les differentes variables. 

   \section Learning Learning

   \subsection EqRec Equation d'apprentissage de la recurrence

   \f[
   \Delta w_{ij}^{rec} = TD \cdot [ -\lambda_{p} \cdot w_{ij}^{rec} -
   \lambda_{a} \cdot  w_{ij}^{rec} \cdot \delta_{jl} 
   + (\gamma - w_{ij}^{rec})\cdot \delta_{ik} \cdot \delta_{jl} ]
   \f]
   with \f$ TD = 1 \f$ if a transition occurred and \f$ 0 \f$ otherwise.

   "The recurrent synaptic weights \f$ w_{ij} \f$ are updated if a
   change of state (transition) is detected (\f$ TD=1 \f$). The
   learning depends on a passive decay \f$ \lambda_{p} \f$, an active
   decay \f$ \lambda_{a} \f$ and a learning parameter \f$ \gamma <1
   \f$.  The connection between the last performed transition \f$ k
   \f$ and the former one \f$ l \f$ is reinforced (up to \f$ \gamma
   \f$) while connections from the former transition l to any other
   transition endures the active decay \f$ \lambda_{a}. This means
   that the connections to the transitions that were not selected are
   decreased. \f$."

   \subsection EqMotiv Equation d'apprentissage de la motivation

   "The system is considered to be in a motivational state \f$
   E^{motiv}_m \f$.  When a reward is received, the last performed
   transition \f$ k \f$ is associated to the current motivation with
   the following learning rule where \f$ w_{mj}^{motiv} \f$ are the weights of
   the motivation, \f$ \varepsilon \f$ the learning rate, \f$ R \f$ is
   the reward given to the robot and \f$ \lambda \f$ an active decay
   term."
   \f[ 
   \Delta w_{mj}^{motiv} = - \lambda \cdot E_m^{motiv} + R
   \cdot \delta_{jk} \cdot \varepsilon \cdot (1 - w_{mj}^{motiv})
   \cdot E_m^{motiv} 
   \f]

   Actuellement, dans l'apprentissage des connections vers les drives,
   le decay \f$ \lambda \f$ est determine par le seuil du group et et la
   vitesse d'apprentissage \f$ \varepsilon \f$ depend du learning rate du
   groupe.


   \section Links 

   On peut modifier la regle d'apprentissage des connections
   recurrentes par l'ajout des options no_forget (pas d'oubli du
   tout), -gamma, pour preciser l'oubli actif \f$ \lambda_{a} \f$, -lambda, pour l'oubli
   passif \f$ \lambda_{p} \f$.

   \section NB Nota Bene

   Par rapport a l'ancienne version de goal.c, il est possible d'avoir
   extactement le meme type de comportements en parametrant
   correctement le systeme:

   - Configuration 1: Apprentissage et desapprentissage en 1
   coup: dans ce cas, il ne peut jamais y avoir plus d'un drive associe
   a une meme transition.
   Seuil du groupe=1 et learning_rate=2.

   - Configuration 2: Apprentissage en 1 coup, pas de desapprentissage.
   Seuil du groupe=0 et learning_rate=1.

   \file
   \ingroup goal

   \brief  Cognitive map

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: N. Cuperlier
   - description: specific file creation
   - date: 01/09/2004
    
   - author: J. Bonhoure 
   - description: Ajout des options no_forget,
   -gamma, pour preciser l'oubli actif, -lambda, pour l'oubli passif.
   - date: 27/07/2011
  
************************************************************/
/* #define DEBUG */
#include <net_message_debug_dist.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>

typedef struct goalStruct
{
  int rec_inc;
  int n_mod_inc;
  int macro_inc;
  int motiv_inc;
  int trans_inc;
  int gpe_vigilence;
  int gpe_transition_lieux;
  int gpe_reward;
  float oubli;
  float oubli_passif;
  float transition_lieux;
  float learn_rate;
  float decay;
  type_coeff **NM_link_tab; 		/** local NeuroMod link tab*/
} goalStruct;


static float seuil(float value)
{
  if (value < 0.)
  {
    return 0.;
  }
  else if (value > 1.)
  {
    return 1.;
  }
  else 
  {
    return value;
  }
}


void new_goal(int gpe)
{
  int i, mode, l;
  int DebutGpe = def_groupe[gpe].premier_ele;
  int NbreTotalGpe = def_groupe[gpe].nbre;
  int IncrementGpe = NbreTotalGpe / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);
  goalStruct * ps = NULL;
  type_coeff * coeff;
  char param[10];

  dprints("enter %s\n",__FUNCTION__);
	
  if (def_groupe[gpe].data == NULL)
  {	
    ps = ALLOCATION(goalStruct);
	
    ps->gpe_vigilence = trouver_entree(gpe, (char *) "vigilence");
    ps->gpe_transition_lieux = trouver_entree(gpe,(char *) "transition_lieux");
    ps->gpe_reward = trouver_entree(gpe,(char *) "reward");
    ps->oubli = GAMMA1;
    ps->oubli_passif = LAMBDA;
    ps->NM_link_tab=get_local_neuromod_link_tab(gpe);
    /** TODO remove useless inhibMotiv links associated with wrong microN */

    /* temporaire : a realiser correctement */
    kprints("WARNING : modifier la recuperation de alpha et epsilon !!!\n");
    ps->decay = def_groupe[gpe].seuil;
    ps->learn_rate = def_groupe[gpe].learning_rate;

    if (def_groupe[gpe].data == NULL)
    {
      l = 0;
      i = find_input_link(gpe, l);
      while (i != -1)
      {
	if (prom_getopt(liaison[i].nom, "gamma", param) == 2)
	{
	  ps->oubli = atof(param);
	}
			
	if (prom_getopt(liaison[i].nom, "lambda", param) == 2)
	{
	  ps->oubli_passif = atof(param);
	}
			
	if (strcmp(liaison[i].nom, "no_forget") == 0)
	{
	  ps->oubli = 0.;
	  ps->oubli_passif = 0.;
	}
		
	l++;
	i = find_input_link(gpe, l);
      }
    }
	  
    kprints("oubli is %f\t oubli_passif is %f\n",ps->oubli,ps->oubli_passif);
    if (ps->gpe_transition_lieux < 0)
    {
      EXIT_ON_ERROR("Missing input link 'transition_lieux'");
    }

    ps->n_mod_inc = ps->macro_inc = ps->motiv_inc = ps->trans_inc = ps->rec_inc = -1;

    /*recherche des positions (increment) des differentes voies*/
    for(i = 0; i < IncrementGpe; i++)
    {
      coeff = neurone[DebutGpe+i].coeff;
      mode = liaison[coeff->gpe_liaison].mode;
      switch(mode)
      {
      case RECURRENT_HEBBIAN:
	ps->rec_inc = i;
	break;

      case NEURO_MOD_BUT:
	ps->n_mod_inc = i;
	break;
	       
      case MACRO_LINKS:
	ps->macro_inc = i;
	break;
	       
      case MOTIVATIONNAL:
	ps->motiv_inc = i;
	break;

      case CA1:
	ps->trans_inc = i;
	break;
			   
      default:
	EXIT_ON_ERROR("But:(%d) type de lien non attendu...\n",mode);
      }
    }

    dprints("\t new_goal: detection de REC en %d - MAC en %d - MOT en %d - CA1 en %d\n", ps->rec_inc, ps->macro_inc, ps->motiv_inc, ps->trans_inc);

    def_groupe[gpe].data = ps;
  }
}

 
/*i le micro recurrent, n le macro correspondant*/
void learn_rec(int i, type_coeff *coeff, int n, goalStruct * ps)
{
  float dw = 0.;
  
  while (coeff_is_valid(coeff))
  {     
    /*on autorise pas les liens du micro rec vers son macro*/
    if (coeff->entree != n)
    {	
      dw = -ps->oubli_passif * coeff->val - ps->oubli * coeff->val * neurone[coeff->entree].s2 + (GAMMA - coeff->val) * neurone[coeff->entree].s2 * neurone[i].s2 ;
	 
      if (dw > 0.0001) 
      {
	dprints("renfo prevu vers micro %d  de macro %d \n",(i-ps->rec_inc-def_groupe[neurone[i].groupe].premier_ele)/(ps->macro_inc+1),(coeff->entree-ps->macro_inc-def_groupe[neurone[i].groupe].premier_ele)/(ps->macro_inc+1));
	dprints("valeur entree : %f\n",neurone[coeff->entree].s2);
	dprints("renforcement:  dw = %f \n", dw);
      }
      if(dw < -ps->oubli_passif * coeff->val && coeff->val  > 0.01) 
      {
	dprints("oubli prevu vers micro %d  de macro %d \n",(i-ps->rec_inc-def_groupe[neurone[i].groupe].premier_ele)/(ps->macro_inc+1),(coeff->entree-ps->macro_inc-def_groupe[neurone[i].groupe].premier_ele)/(ps->macro_inc+1));
	dprints("valeur entree : %f\n",neurone[coeff->entree].s2);
	dprints("oubli:  dw = %f \n", dw);	
      }

      coeff->val += dw;
    }
    else 
    {
      coeff->val = 0;
    }
	      
    if (coeff->val < 0.01) 
    {
      coeff->val = 0;
    }
    coeff = coeff->s;
  }	
}

/*----------------------------------------------------------------*/
/* la sortie d'un neurone but est le max de ses entrees ponderees */
/* prend le max des entrees (+)  "moins" le min des entrees (-)   */
/*----------------------------------------------------------------*/

float calcule_mc_but(int i, type_coeff *coeff)
{
  float sortiep = 0.;
  float val = 0.;
  (void)i;

  /*on ne calcul la sortie que si le neurone a appris ou si 'lon init les poids...*/
  while (coeff_is_valid(coeff))
  {
    /*dprints("entree[%i] : %f, coeff->val : %f\n",coeff->entree, neurone[coeff->entree].s,coeff->val);*/
    /* on ne calcul pas l'autoactivation...*/
    if (coeff->val > 0.5)
    {
      val = neurone[coeff->entree].s * coeff->val;
    }
						
    if (sortiep < val) 
    {
      sortiep = val;
    } 	
			
    coeff = coeff->s;
  }

  return(seuil(sortiep));
} 

/*Apprentissage hebbien classique sur s2 qui contient l'information de decouverte d'une source (f_envie_et_decouverte)*/
void learn_motiv(int i, type_coeff *coeff, int n, float learn_rate, float decay)
{  
  /*apprentissage entre la transition la plus forte au niveau de ca1 (macro.s2 = 1) et la motiv decouverte (entree.s2 = 1)*/
  /* type_coeff *coeff_tmp = NULL;*/
  float Si, Sj; /*, W = 0., Wmax = 0.;*/
  (void)i;

  while ( coeff_is_valid(coeff) )
  {
    Si = neurone[coeff->entree].s2;
    Sj = neurone[n].s2;

    if (Sj < 0.)
    {
      Si = 0.;
    }
    if (Sj < 0.)
    {
      Sj = 0.;
    }
    /*W = Si * Sj; / remplace par learn_rate* (1-W)*Si*Sj - alpha*Si */
    /* LEARNING RULE */
    coeff->val = coeff->val + learn_rate*(1-coeff->val)*Si*Sj - decay*Si; 
    dprints("W %f - Sj : %f / epsilon : %f - decay %f\n",coeff->val, Sj, learn_rate, decay); 
    if(coeff->val<0) { 
      coeff->val=0; 
    }	
    coeff = coeff->s;
  } 	
}

/*calcule du facteur d'envie sur le s1 de f_envie_et_decouverte*/
float calcule_mc_but_motiv(int i, type_coeff *coeff, goalStruct *ps)
{
  float sortiep = 0.;
  float val;
  (void)i;
  (void)ps;

  while (coeff_is_valid(coeff))
  {
    if(coeff->val >= 0.1) dprints("calcule_mc_but_motiv : Le coeff du neurone %d, de la carte %s, est %f\n",i-def_groupe[neurone[i].groupe].premier_ele,def_groupe[neurone[i].groupe].nom,coeff->val);
    /* on ne calcul pas l'autoactivation...*/
    val = coeff->val * neurone[coeff->entree].s1;
    if (sortiep < val) 
    {
      sortiep = val;
    }
    coeff = coeff->s;
  }

  return(seuil(sortiep));
}



float calcule_mc_neuromod(int i, type_coeff *coeff)
{
  float sortiep = 0.;
  (void)i;

  while (coeff_is_valid(coeff))
  {
    /* on ne calcul pas l'autoactivation...*/
    sortiep += coeff->val * neurone[coeff->entree].s1;
    coeff = coeff->s;
  }

  return(sortiep);
}


float calcule_mc_bloqueur(int i, type_coeff *coeff)
{
  (void)i;

  return(coeff->val * neurone[coeff->entree].s1);
}

void mise_a_jour_groupe_but(int gpe)
{
  type_coeff *coeff;
  float sortie_ca = 0.,sortie_rec = 0.,sortie_motiv = 0., sortie_neuromod = 0., diffusion_act = 0.;
  int DebutGpe, NbreTotalGpe;
  int link, mode, i;
  /*structure du groupe*/
  goalStruct *ps = NULL;
  /*index des neurones selectionnes pour apprentissage*/
  int max_index = -1;
  float max = 0;
  float mod_motiv=0.;

  dprints("mise a jour des neurones du groupe : %d\n", gpe);

  DebutGpe = def_groupe[gpe].premier_ele;
  NbreTotalGpe = def_groupe[gpe].nbre;
	
  /*on recupere la structure courante*/	  
  ps = (goalStruct *) def_groupe[gpe].data;
   
  ps->transition_lieux = neurone[def_groupe[ps->gpe_transition_lieux].premier_ele].s1;

  max = 0.;

  for (i = DebutGpe; i < DebutGpe + NbreTotalGpe; i++)
  { 
    coeff = neurone[i].coeff;
    link = coeff->gpe_liaison;
    mode = liaison[link].mode;
    switch(mode)
    { 
    case MOTIVATIONNAL: 
      coeff = neurone[i].coeff;
			
      /**compute motivational neuromod */
      get_local_neuromod(ps->NM_link_tab[i-DebutGpe],inhibMotiv,&mod_motiv);

      sortie_motiv = calcule_mc_but_motiv(i, coeff, ps);
      if(fabs(mod_motiv)>0.00001) {
	dprints("Neurone %d : mod motiv : %f, sortie motiv : %f\n",i,mod_motiv, sortie_motiv);
      }
      neurone[i].s = neurone[i].s1 = neurone[i].s2 =  sortie_motiv = sortie_motiv + mod_motiv;
      break;

    case CA1:   /*calcul au premier tour*/ 
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = sortie_ca = calcule_mc_bloqueur(i, coeff); 
      /*recherche du max*/
      if (neurone[i].s1 > max)
      {
	max = neurone[i].s1;
	max_index = i;
      }
      break;

    case RECURRENT_HEBBIAN: 
      neurone[i].s2 = 0.;
      neurone[i].s = neurone[i].s1 = sortie_rec = calcule_mc_but(i, coeff);
      break;

    case NEURO_MOD_BUT:
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = sortie_neuromod = calcule_mc_neuromod(i, coeff);
	    
      break;

    case MACRO_LINKS:
      neurone[i].s2 = 0.;

      if (sortie_motiv > sortie_rec) 
      {
	diffusion_act = sortie_motiv;
      }
      else 
      {
	diffusion_act = sortie_rec;
      }

      diffusion_act += sortie_neuromod;

      /*Seuillage diffusion*/
      if (diffusion_act > 1.)
      {
	diffusion_act = 1.;
      }
      else if (diffusion_act < 0.01)
      {
	diffusion_act = 0.;
      }

      if (diffusion_act > 0.)
      {
	dprints("%d  => gradient = %f\n",(i-def_groupe[gpe].premier_ele-ps->macro_inc)/(ps->macro_inc+1),diffusion_act);
      }

      /*L'activite de diffusion est placee dans s*/
      neurone[i].s = diffusion_act;

      /*sur s1, soit on met la diffusion si pas de transition de transition, soit on met ca.*/
      if (ps->transition_lieux < 0.5)
      {
	neurone[i].s1 = diffusion_act;
      }
      else
      {
	neurone[i].s1 = sortie_ca;
      }
		
      /*on regarde la sortie ca*/
      if (sortie_ca > 0.)
      {
	/* RAZ pour prochain macro... (les micro etant avant les macro) */
	neurone[i-ps->macro_inc+ps->rec_inc].s2 = 1.;
      }

      sortie_ca = sortie_rec = sortie_motiv = 0.;	
      break;
			
    default:
      break;
    }
  }

  /* Mise a 1 de s2 pour le macro neurone dont l'activite venant de CA1 est max */
  neurone[max_index-ps->trans_inc+ps->macro_inc].s2 = 1.0;

  dprints("prepare  %d \n",max_index - ps->trans_inc + ps->macro_inc);
}

void apprend_but(int gpe)
{
  int i,n;
  type_coeff *coeff; /*, *coeffTmp;*/
  int deb, nbre;
  int increment;
  int link, mode;
  goalStruct *ps;
  float reward;
  float n_eps=0.;

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;
  increment = nbre / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);
  ps = (goalStruct *) def_groupe[gpe].data;

  if (ps == NULL)
  {
    EXIT_ON_ERROR("impossible de recuperer la structure du but dans la fonction d'apprentissage\n");
  }

  dprints("La reward est : %f\n",neurone[def_groupe[ps->gpe_reward].premier_ele].s1);
   
  /* parcours des macroneurones*/
  for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
  {
    /*parcours des micros*/
    for (i = n - increment + 1; i < n; i++)
    {
      coeff = neurone[i].coeff;
      /*on ne traite que les liens qui peuvent apprendre (pas ceux de CA1 ni ceux de la neuromodulation...)*/
      if (coeff->evolution == 1)
      {
	link = coeff->gpe_liaison;
	mode = liaison[link].mode;
	switch(mode)
	{
	case RECURRENT_HEBBIAN: 
	  if (ps->transition_lieux > 0.5)
	  {			
	    learn_rec(i, coeff, n, ps); 
	  }
	  break;

	case MOTIVATIONNAL: 
	  reward = neurone[def_groupe[ps->gpe_reward].premier_ele].s1;
	  /* Recupere un eventuel signal negatif dans le learn (neuromod locale) */

/* 	  coeffTmp = ps->NM_link_tab[i-deb]; */
/* 	  while(coeffTmp!=NULL) { */
/* 	    cprints("Valeur des coeff : %f  a  %d\n", coeffTmp->val, i-deb); */
/* 	    coeffTmp=coeffTmp->s; */
/* 	  } */

	  get_local_neuromod(ps->NM_link_tab[i-deb],NEUROMOD_learn,&n_eps);
	  if (ps->gpe_reward < 0 || fabs(reward) > 0.5 || n_eps > fabs(epsilon) )
	  {
	    /* AR: prise encompte de la reward (positive ou negative) pour le learning rate : rq : le test sur la valeur de la 			     reward est il toujours pertinent ? */
	    learn_motiv(i, coeff, n, ps->learn_rate*reward, ps->decay + n_eps);
	  }
	  break;
			  
	case MACRO_LINKS: 
	case NEURO_MOD_BUT:
	  break;

	default			: 
	  EXIT_ON_ERROR("ERROR: that link type (%d) does not exist for goal neurons\n",mode);
	}
      }
    }
  }   
}

void destroy_goal(int gpe)
{	
  goalStruct *my_data = (goalStruct*) def_groupe[gpe].data;
  type_coeff **NM_link_tab;
  if (def_groupe[gpe].data != NULL)
  {
    dprints("In destroy_goal (%s) : free NM_link_tab\n",def_groupe[gpe].no_name);
    NM_link_tab=my_data->NM_link_tab;
    if(NM_link_tab!=NULL) /** libere NM_link_tab*/
      free(NM_link_tab);
    NM_link_tab=NULL;
    free(my_data);
    def_groupe[gpe].data = NULL;
  }
  dprints("destroy_goal\n");	
}
