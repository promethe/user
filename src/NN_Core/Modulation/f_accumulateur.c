/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <limits.h>
#include <public_tools/Vision.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*
#define DEBUG
*/

/** Structure contenant les donnees specifiques a PCR.
 */
typedef struct t_accumulateur_Data {
  int pot_tresh_max;
  /* Lien de neuromodulation                                                  */
  noeud_modulation *reset_link;
  noeud_modulation *activation_link;
} accumulateur_Data;

/**
 * Accumulateur. Fait la somme dans le temps des signaux reçus jusqu'à
 * un certain seuil. Est remis à zéro lorsqu'il reçoit un signal de neuromod. 
 * @param num le numero de groupe concerne.
 */
void function_accumulateur(int num)
{
  /* Var - General */
  int i = 0;
  int activated = 0;
  float reinf = 0.0;
  float entree = 0.0;
  
  noeud_modulation *n;
  type_coeff *coeff;

  accumulateur_Data *acc_data = NULL;

  /* Initialisation :
   *   Obtention et stockage du lien de reset
   *   TODO Obtention du seuil max
   */
  if(def_groupe[num].data == NULL)
  {
    acc_data = (accumulateur_Data *)malloc(sizeof(accumulateur_Data));
    
    /* Neuromodulations                                                       */
    acc_data->activation_link = (noeud_modulation *)malloc(sizeof(noeud_modulation));
    acc_data->activation_link = (noeud_modulation *)get_noeud_modulation_ptr(num, "-activation");
    
    if(acc_data->activation_link != NULL)
      dprints("%s : Activation link found\n", __FUNCTION__);
    
    acc_data->reset_link = (noeud_modulation *)malloc(sizeof(noeud_modulation));
    acc_data->reset_link = (noeud_modulation *)get_noeud_modulation_ptr(num, "-reset");

    if(acc_data->reset_link == NULL)
      cprints("%s : No reset link found\n", __FUNCTION__);

    /* Lien entrant                                                           */
    if(!(i = find_input_link(num, 0)))
      EXIT_ON_ERROR("%s : No input link found\n", __FUNCTION__);
    
    def_groupe[num].data = (accumulateur_Data *)malloc(sizeof(accumulateur_Data));
    def_groupe[num].data = acc_data;
  }

  acc_data = (accumulateur_Data *)def_groupe[num].data;
  
  /* A-t-on reçu un signal d'activation                                     */
  n = acc_data->activation_link;
  while (n != NULL)
  {
    entree += neurone[n->num_neurone].s2 * n->poids;
    n = n->suivant;
  }

  if(acc_data->activation_link == NULL || entree > 0.5)
    activated = 1;

  #ifdef DEBUG
  cprints("Activation : %f | Activated = %d\n", entree, activated);
  #endif
  
  /* A-t-on reçu un signal de reset                                           */
  /*n = acc_data->reset_link;*/
  reinf = 0.0;
  if ((n = acc_data->reset_link) != NULL)
  {
    reinf += neurone[n->num_neurone].s2 * n->poids;
    /*
    cprints("w = %f | [%d] s %f, s1 %f, s2 %f\n", n->poids, neurone[n->num_neurone].groupe, neurone[n->num_neurone].s, neurone[n->num_neurone].s1, neurone[n->num_neurone].s2);
     */
  }

  #ifdef DEBUG
  if(reinf > 0.2)
    cprints("f_accumulateur::Reset = %f\n", reinf);
  #endif

  /* Remise a zero si reset demandé                                           */
  if(reinf > 0.2)
  {
    for(i = def_groupe[num].premier_ele ; i < def_groupe[num].premier_ele + def_groupe[num].nbre ; ++i)
    {
      neurone[i].s = 0.0;
      neurone[i].s1 = 0.0;
      neurone[i].s2 = 0.0;
    }
    #ifdef DEBUG
    cprints("f_accumulateur::Reset (%f)\n", reinf);
    #endif
  }
  else
  {
    /* Si pas de reset demandé, on accumule                                   */
    if(activated)
    {
      /* Calcul des W*I                                                       */
      for(i = def_groupe[num].premier_ele ; i < def_groupe[num].premier_ele + def_groupe[num].nbre ; ++i)
      {
        coeff = neurone[i].coeff;

        /* Calcul du potentiel du neurone courant : somme des produits des Wij*/
        while(coeff != NULL)
        {
          if(/*coeff->val*neurone[coeff->entree].s1 > def_groupe[num].seuil &&*/
            activated)
          {
            #ifdef DEBUG
            cprints("f_accumulateur::+%f\n", coeff->val*neurone[coeff->entree].s2);
            #endif
            /* Ajout de la sortie - O_{j} += I_{i}*W_{ij}                     */
            neurone[i].s += coeff->val*neurone[coeff->entree].s1;
            neurone[i].s1 = neurone[i].s;
	    /* TODO pouvoir utiliser un seuil defini dans le lien             */
            neurone[i].s2 = neurone[i].s;
          }

          coeff = coeff->s;
        }
      }
    }
  }
}
