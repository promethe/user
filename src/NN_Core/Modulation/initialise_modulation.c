/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <Kernel_Function/find_input_link.h>
#include <libx.h>
/* #include "tools/include/ajouter_neuromodulation.h" */
#include <Struct/structs_noeudmod_listemod.h>
#include <Global_Var/Limbic.h>

noeud_modulation *cree_noeud_modulation(float norme, int neurone_amont);
type_liste_types_neuromodulations *cree_type_modulation( char *nom_modulation, float norme, int neurone_amont);
void complete_chaine_noeud_modulation(noeud_modulation ** ptr_noeud,float norme, int neurone_amont);
void rechercher_modulation_init(type_liste_types_neuromodulations **ptr_types_neuromod,  char *nom_modulation, float norme, int neurone_amont);


noeud_modulation *cree_noeud_modulation(float norme, int neurone_amont)
{
    noeud_modulation *noeud;
    dprints("creation du noeud en bout de chaine\n");
    noeud = (noeud_modulation *) malloc(sizeof(noeud_modulation));

    if (noeud == NULL)
    {
        /* Erreur allocation memoire */
        EXIT_ON_ERROR("erreur lors de l'initialisation des neuromodulations->cree_noeud_modulation.c :\n  ***** Erreur allocation memoire *****\n");
    }

    noeud->poids = norme;
    noeud->num_neurone = neurone_amont;
    noeud->suivant = NULL;
    return noeud;
}

type_liste_types_neuromodulations *cree_type_modulation( char *nom_modulation, float norme, int neurone_amont)
{
    noeud_modulation *noeud;
    type_liste_types_neuromodulations *liste_type_modulation;
    liste_type_modulation =   (type_liste_types_neuromodulations *)   malloc(sizeof(type_liste_types_neuromodulations));

    if (liste_type_modulation == NULL)
    {
        /* Erreur allocation memoire */
        EXIT_ON_ERROR("erreur lors de l'initialisation des neuromodulations->cree_type_modulation.c :\n  ***** Erreur allocation memoire *****\n");
    }

    dprints("Le type de modulation cree est %s et est adressee par %p \n", nom_modulation, (void*)(liste_type_modulation));


    noeud = cree_noeud_modulation(norme, neurone_amont);    /* creation du noeud necessairement absent etant  */
    liste_type_modulation->type = nom_modulation;   /* donne que le type de modulation n'existait pas. */
    liste_type_modulation->liste_modulation = noeud;
    liste_type_modulation->suivant = NULL;

    /* On renvoit la modulation creee */
    return liste_type_modulation;
}

void complete_chaine_noeud_modulation(noeud_modulation ** ptr_noeud, float norme, int neurone_amont)
{
    /* Recherche du bout de la chaine */
    while (*ptr_noeud != NULL)
    {
        dprints(" Recherche du bout de la chaine des noeuds\n");
        ptr_noeud = &((*ptr_noeud)->suivant);
    }
    /* Ajout du noeud en bout de chaine */
    *ptr_noeud = cree_noeud_modulation(norme, neurone_amont);
    return;
}

void rechercher_modulation_init(type_liste_types_neuromodulations **ptr_types_neuromod,  char *nom_modulation, float norme, int neurone_amont)
{
    while ((*ptr_types_neuromod) != NULL)
    {
        dprints("recherche dans %p si la modulation %s existe \n", (void*)(*ptr_types_neuromod), nom_modulation);
        /* Si la modulation est celle recherchee, on la renvoit */
        if (strcmp((*ptr_types_neuromod)->type, nom_modulation) == 0)
        {
            printf
                ("la modulation %s existe deja : on complete la chaine des noeud correspondant a %s \n", nom_modulation, nom_modulation);
                 complete_chaine_noeud_modulation(&((*ptr_types_neuromod)->liste_modulation), norme, neurone_amont);
            return;
        }
        else                    /* Sinon on recherche dans le reste de la liste */
        {

            ptr_types_neuromod = &((*ptr_types_neuromod)->suivant);
        }
    }
    dprints("l'adresse %p est vide, on cree un nouveau type de modulation\n",(void*)(*ptr_types_neuromod));
    *ptr_types_neuromod =  cree_type_modulation(nom_modulation, norme, neurone_amont);

    return;
}

/* Implementation de la neuromodulation de groupe par
   un tableau statique contenant un pointeur associe a chaque groupe */

void initialise_modulation(int gpe)
{
    int i = 0;
    int link = 0;
    float norme;
    int neurone_amont;
    int gpe_amont;
    char *nom_modulation;
    if (def_groupe[gpe].liste_types_neuromodulations == NULL)
    {
        link = find_input_link(gpe, i);
        while (link != -1)
        {
            /* ---seuls les liens de type modulation (type 8) sont pris en compte--- */
            if (liaison[link].type == No_l_neuro_mod)
            {
                /* ---resuperation des donnes du lien--- */
                norme = liaison[link].norme;
                gpe_amont = liaison[link].depart;
                neurone_amont = def_groupe[gpe_amont].premier_ele;
                nom_modulation = strdup(liaison[link].nom);
                printf("neurone amont dans %s : %d\n", __FUNCTION__,  neurone_amont);
                printf("\najout neuromod %s provenant du groupe %d sur le groupe %d :\n\n",nom_modulation, gpe_amont, gpe);
                rechercher_modulation_init(&(def_groupe[gpe].liste_types_neuromodulations),nom_modulation, norme, neurone_amont);
            }
            i++;
            link = find_input_link(gpe, i);
        }
    }
}
