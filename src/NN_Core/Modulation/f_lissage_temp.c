/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_lissage_temp.c 
\brief 

Author: C.Giovannangeli
Created: 11/08/2004
Modified:
- author: A. de Rengerv�
- description: re-name of runge kutta and adaptation to shading
- date: 23/04/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

 The main effect is shading, due to an important inertia.
 This inertia  depends on the simulation speed parameter defined for the group.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>

/* #define LISSAGE_TEMP_TRACE_PLOT	1 */
/*#define DEBUG		        1 */
/*#define LISSAGE_TEMP_DEBUG		1*/ 

#define	MAX_PATH		256

void function_lissage_temp(int numero)
{
    int first, nr;
    int j;
    int *iters;
    /*
       Coeff :
       ->entree : numero du neurone en entree du lien
       ->val : valeur du lien
       ->s : lien suivant
     */
    type_coeff *lien = NULL;
    float learning_speed = 1.;
    char type[20];


#ifdef DEBUG
    printf("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
#endif

    /* nombre de neurones dans le groupe */
    nr = def_groupe[numero].nbre;

    /* Initialisation de donnees pour le groupe */
    if (def_groupe[numero].data == NULL)
    {
        if ((def_groupe[numero].data = malloc(nr * sizeof(int))) == NULL)
        {
            perror("malloc");
            exit(1);
        }
        memset(def_groupe[numero].data, 0, nr * sizeof(int));
    }
    iters = (int *) def_groupe[numero].data;

    /* 1er neuronne du groupe */
    first = def_groupe[numero].premier_ele;

    /* ---Calcul de la neuromodulation portant sur le groupe--- */
    strcpy(type, "learning_speed");
    calcule_neuromodulation(numero, type, &learning_speed);
    
    /* Parcours des neuronnes du groupe */
    for (j = first; j < (first + nr); j++)
    {
        float potentiel = 0.0;
        int i = j - first;
        float h =
            (def_groupe[numero].simulation_speed) * learning_speed;
#ifdef LISSAGE_TEMP_TRACE_PLOT
        char file_name[MAX_PATH];
        char *file_phase = (char*)"./phase.plot";
        FILE *file = NULL;
#endif

        if (iters[i] == 0)
            neurone[j].s = neurone[j].s1 = neurone[j].s2 = 0.0;

#ifdef LISSAGE_TEMP_DEBUG
        printf("### groupe %i neuronne %i\n", numero, i);
        printf("h = %f\n",h);
#endif


        /* On recupere le potentiel de s sur les liens d'entree */
        for (lien = neurone[j].coeff; lien != NULL; lien = lien->s)
        {
#ifdef LISSAGE_TEMP_DEBUG
            printf("potentiel = %f (%f * %f)\n",
                   lien->val * neurone[lien->entree].s, lien->val,
                   neurone[lien->entree].s);
#endif
            potentiel += (lien->val * neurone[lien->entree].s);
        }

        /* dx/dt = h * (  potentiel       +       -neurone[j].s) */

        neurone[j].s = neurone[j].s + h * (potentiel-neurone[j].s);

        potentiel = 0.0;
        /* On recupere le potentiel de s1 sur les liens d'entree */
        for (lien = neurone[j].coeff; lien != NULL; lien = lien->s)
        {
#ifdef LISSAGE_TEMP_DEBUG
            printf("potentiel = %f (%f * %f)\n",
                   lien->val * neurone[lien->entree].s1, lien->val,
                   neurone[lien->entree].s1);
#endif
            potentiel += (lien->val * neurone[lien->entree].s1);
        }

        /* dx/dt = h * (  potentiel       +       -neurone[j].s) */

        neurone[j].s1 = neurone[j].s1 + h * (potentiel-neurone[j].s1);

        potentiel = 0.0;
        /* On recupere le potentiel de s2 sur les liens d'entree */
        for (lien = neurone[j].coeff; lien != NULL; lien = lien->s)
        {
#ifdef LISSAGE_TEMP_DEBUG
            printf("potentiel = %f (%f * %f)\n",
                   lien->val * neurone[lien->entree].s2, lien->val,
                   neurone[lien->entree].s2);
#endif
            potentiel += (lien->val * neurone[lien->entree].s2);
        }

        /* dx/dt = h * (  potentiel       +       -neurone[j].s) */

        neurone[j].s2 = neurone[j].s2 + h * (potentiel-neurone[j].s2);


#ifdef LISSAGE_TEMP_DEBUG
        printf("s = %f : s1 = %f : s2 = %f\n", neurone[j].s, neurone[j].s1,
               neurone[j].s2);
#endif

#ifdef LISSAGE_TEMP_DEBUG_TRACE_PLOT
        /* ecriture du potentiel dans un fichier gnuplot  */
        sprintf(file_name, "./%i_%i.plot", numero, i);
        if ((file = fopen(file_name, "a")) == NULL)
        {
            perror("fopen");
            exit(1);
        }
        fprintf(file, "%i %f\n", (iters[i])++, neurone[j].s2);
        fclose(file);
        file = NULL;

        if ((file = fopen(file_phase, "a")) == NULL)
        {
            perror("fopen");
            exit(1);
        }
        if (numero == 0)
            fprintf(file, "%f ", neurone[j].s2);
        else if (numero == 1)
            fprintf(file, "%f\n", neurone[j].s2);
        fclose(file);

#endif
        if (iters[i] == 0)
            iters[i]++;
    }
#ifdef DEBUG
    printf("===========sortie de %s\n", __FUNCTION__);
#endif
}

/* #undef DEBUG */
