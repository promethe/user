/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*#define DEBUG*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libx.h>
#include <Struct/structs_noeudmod_listemod.h>
#include <net_message_debug_dist.h>
noeud_modulation *rechercher_noeud_mod(type_liste_types_neuromodulations *  types_neuromod_groupe,  const char *nom_modulation);

/* --------------------------------------------------------------------------------------------------- */

noeud_modulation *rechercher_noeud_mod(type_liste_types_neuromodulations *  types_neuromod_groupe,  const char *nom_modulation)
{
   while (types_neuromod_groupe != NULL)
   {
      dprints("comparaison type neuromod: %s et %s \n", types_neuromod_groupe->type, nom_modulation);

      if (strcmp(types_neuromod_groupe->type, nom_modulation) == 0)
      {
         return (types_neuromod_groupe->liste_modulation);
      }
      else
         types_neuromod_groupe = types_neuromod_groupe->suivant;
   }

   dprints
   ("\n ******* Attention ****** Dans la fonction rechercher_noeud_mod de Limbic : \n "
    " le type de neuromodulation %s n'a pas ete trouve en entree du groupe. \n",
    nom_modulation);

   return NULL;
}

/* Calcul de la neuromodulation "neuromod" de type "type" du groupe "gpe" :                                   */
/* Rque si aucun lien de type "type" n'est connecte en entree du groupe alors "neuromod" ne sera pas modifiee */
/* et donc gardera la valeur qui lui aura ete donnee dans la fonction qui appelle calcule_neuromodulation.    */
/* Ainsi, on peut definir une valeur par defaut de "neuromod" qui est utilise si et seulement si aucun lien   */
/* "type" n'est connecte en entree du groupe "gpe".                                                           */

/* La fonction calcule_neuromodulation prends la valeur par defaut de la modulation puis la reinitialise et la */
/* recalcule le cas echeant (si il y a des liens neuromod du bon type en entree)                               */
/* Elle renvoie maintenant 0 si c'est la valeur par defaut qui est conservee et 1                              */
/* si la neuromodulation a ete recalculee.                                                                     */

int calcule_neuromodulation(int gpe, const char *type, float *neuromod)   /* gpe=numero du groupe, type de neuromodulation devant etre           */
/* calculee (DA, Ach, dRenfocement, etc), variable neuromodulation     */
/* a modifier si il y a des liens du type de cette neuromodulation     */
/* en entree du groupe (sinon elle garde sa valeur, valeur par defaut). */
{
   int flag_neuromod = 0;
   noeud_modulation *noeud_mod;
   noeud_mod =
      rechercher_noeud_mod(def_groupe[gpe].liste_types_neuromodulations,
                           type);

   dprints
   ("calcule de la neuromodulation de type %s sur le groupe numero %d\n",
    type, gpe);

   if (noeud_mod != NULL)
   {
      flag_neuromod = 1;
      /* Si au moins un lien neuromodulation de type "type" existe en entree du groupe alors la  */
      /* variable neuromod est reinitialisee puis calculee sinon elle garde sa valeur par defaut */
      *neuromod = 0;
      /* Parcours de la liste des modulation de type "type" */
      while (noeud_mod != NULL)
      {
         *neuromod +=
            neurone[noeud_mod->num_neurone].s1 * noeud_mod->poids ;
         noeud_mod = noeud_mod->suivant;
      }
   }
   return (flag_neuromod);
}

/**
 \defgroup calcule_neuromod_fast calcule_neuromod_fast
 \ingroup group_tools
 \brief  renvoie la somme des Wk.Xk pour une neuromodulation du type designe par le pointeur
 \details

 Param:
 int gpe, noeud_modulation *noeud_mod, float *neuromod

 Return:
 0 si pas de neuromod
 1 si neuromod calculee

 \section Description
 La neuromodulation correspondant au type pointee
 par noeud_mod est calculee (somme de produit des poids par les
 entrees). La valeur passee en sortie par la reference neuromod.


 * cf fonction  get_noeud_modulation_ptr

 \file
 \ingroup calcule_neuromod_fast
*/
int calcule_neuromodulation_fast(int gpe, noeud_modulation *noeud_mod, float *neuromod)   /* gpe=numero du groupe, type de neuromodulation devant etre           */
/* calculee (DA, Ach, dRenfocement, etc), variable neuromodulation     */
/* a modifier si il y a des liens du type de cette neuromodulation     */
/* en entree du groupe (sinon elle garde sa valeur, valeur par defaut). */
{
   int flag_neuromod = 0;

#ifndef DEBUG
   UNUSED_PARAM gpe;
#endif
   dprints("calcule de la neuromodulation sur le groupe numero %s\n",def_groupe[gpe].no_name);

   if (noeud_mod != NULL)
   {
      flag_neuromod = 1;
      /** variable neuromod est reinitialisee puis calculee sinon elle garde sa valeur par defaut **/
      *neuromod = 0;
      /** Parcours de la liste des modulation du type pointe par noeud_mod **/
      while (noeud_mod != NULL)
      {
         *neuromod +=  neurone[noeud_mod->num_neurone].s1 * noeud_mod->poids ;
         noeud_mod = noeud_mod->suivant;
      }
   }
   return (flag_neuromod);
}

/**
   \defgroup get_noeud_modulation_ptr get_noeud_modulation_ptr
   \ingroup group_tools
   \brief
   renvoie un pointeur sur une liste de neuromodulations de type "type"
   \details
   NULL est renvoye si le type "type" n'existe pas
   \file
   \ingroup get_noeud_modulation_ptr
*/
noeud_modulation *get_noeud_modulation_ptr(int gpe, const char *type)
{
   noeud_modulation *noeud_mod;
   noeud_mod = rechercher_noeud_mod(def_groupe[gpe].liste_types_neuromodulations, type);
   return noeud_mod;
}

/**
   \defgroup get_local_neuromod_link_tab get_local_neuromod_link_tab
   \ingroup group_tools
   \brief renvoie un tableau sur les chaines des coeff de neuromod locale
   \details
   \section Description
   remplit un tableau de type_coeff *. Chaque element du tableau correspond au pointeur sur le premier element de la liste des neuromodulations neurone par neurone aboutissant au neurone de meme rang dans le groupe.
   \section Remarque
   La fonction verifie aussi que coeos a bien trie les connections synaptiques : les coeff de neuromod locale doivent tous etre rassembles a la fin de la liste des coeff.
   \file
   \ingroup get_local_neuromod_link_tab
 */
type_coeff **get_local_neuromod_link_tab(int gp)
{
   int i,neuromodN_ptr_found,mode;
   int nb=def_groupe[gp].nbre;
   int deb=def_groupe[gp].premier_ele;
   type_coeff *coeff;
   type_coeff **NM_link_tab;

   dprints("enter in get_neuromodN_link_tab for group %s\n",def_groupe[gp].no_name);

   /** allocationd du tableau de pointeur sur les coeff de neuromod*/
   NM_link_tab = MANY_ALLOCATIONS(nb, type_coeff *);

   for (i=0; i<nb; i++)
   {
      /** recuperation des coeff pour chaque neurone */
      coeff=neurone[i+deb].coeff;
      neuromodN_ptr_found=0;
      NM_link_tab[i]=NULL;
      while (coeff!=NULL)
      {
         /**recuperation du mode pour chaque coeff*/
         mode = liaison[coeff->gpe_liaison].mode;
         /** si neuromod alors initialisation tab et active ptr_found */
         dprints("Neuron %d : coeff mode is %d\n",i,mode);
         if (mode>=NEUROMOD && !neuromodN_ptr_found)
         {
            neuromodN_ptr_found=1;
            NM_link_tab[i]=coeff;
            /* printf("neurone %d : NM_link_tab %p\n",i,(void*)coeff); */
         }
         else if (mode<NEUROMOD && neuromodN_ptr_found)
         {
            /** erreur si coeff est classic alors que l'on a deja
             * trouve le ptr des neuromod. Les neuromod et les liens
             * classiques sont melanges !!
             */
            EXIT_ON_ERROR("Mixed classic links and neuromodN links for group %s\n",def_groupe[gp].no_name);
         }
         coeff=coeff->s;
      }
   }
   dprints("exit from get_neuromodN_link_tab for group %s\n",def_groupe[gp].no_name);
   return NM_link_tab;
}

/**
   \defgroup get_local_neuromod get_local_neuromod
   \ingroup group_tools
   \brief renvoie la neuromod locale (neurone par neurone) d'un type donne
   \details
   Param:
   chaine de coefficients, type de la neuromod, ptr ou ecrire la valeur de la neuromod locale.
   \section Description
   Parcours de la chaine des coeff en testant leur mode. Si leur mode est du type recherche, on inclut le coeff dans le calcul de la neuromod. Le calcul est sum des poids synaptique fois la valeur d'entree (ici la sortie s1 du neurone en entree). Si aucun coeff du bon mode n'est trouve, la valeur de neuromod pointee n'est pas changee.
   \section Remarque
   En pratique, pour gagner du temps, il vaut mieux donner la chaine des liens de neuromodulation locale obtenue avec la fonction get_local_neuromod_link_tab.
   \file
   \ingroup get_local_neuromod
 */
void get_local_neuromod(type_coeff *NM_link_ptr, int local_neuromod_type, float *neuromod)
{
   type_coeff *coeff=NM_link_ptr;
   int neuromod_found=0,mode;
   float nmod=0.0;
   while (coeff!=NULL)
   {
      /**recuperation du mode pour chaque coeff*/
      mode = liaison[coeff->gpe_liaison].mode;
      /** si neuromod du bon type */
      if (mode==local_neuromod_type)
      {
         neuromod_found=1;
         /** calcul de la neuromodulation*/
         nmod+=coeff->val*neurone[coeff->entree].s1; /** sum des coeff.val*neurone.s1 */
      }
      coeff=coeff->s;
   }
   if (neuromod_found)
      *neuromod=nmod;
}

/**
   \defgroup delete_local_neuromod_link delete_local_neuromod_link
   \ingroup group_tools
   \brief supprime les liens d'un certain type
   \details
   Param:
   chaine de coefficients, type de lien a supprimer
   \section Description
   Parcours de la chaine des coeff en testant leur mode. Si leur mode est du type recherche, on supprime le coeff de la liste.
   \section Remarque
   A utiliser avec precaution. Utile dans la cas particulier des micros neurones. Les liens de neuromod sont alors connectes a tous les micros neurones sans etre utile pour tous. Dans ce cas, chaque micro neurone peut selectionner quelques neuromod il souhaite conserver en supprimant les autres.
   \file
   \ingroup delete_local_neuromod_link
 */
void delete_local_neuromod_link(type_coeff **NM_link_ptr,int local_neuromod_type)
{
   type_coeff *prev_coeff=NULL;
   type_coeff *coeff=*NM_link_ptr;
   int mode;

   while (coeff!=NULL)
   {
      /**recuperation du mode pour chaque coeff*/
      mode = liaison[coeff->gpe_liaison].mode;
      /** si neuromod du bon type */
      if (mode==local_neuromod_type)
      {
         dprints("coeff with mode %d is deleted\n",mode);
         /** delete coeff */
         if (prev_coeff==NULL)
         {
            *NM_link_ptr=coeff->s;
         }
         else
         {
            prev_coeff->s=coeff->s;
         }
      }
      else
         prev_coeff=coeff;
      coeff=coeff->s;
   }
}

/**
   \defgroup coeff_is_valid  coeff_is_valid
   \ingroup group_tools
   \brief test si un coeff est non null et non de la neuromod locale
   \details
   Param:
   un coeff
   \section Description
   Test si le coeff est non null et non de la neuromod locale. Renvoie 1 si vrai et 0 sinon.
   \section Remarque
   Utile pour les tests de parcours des neurones pour calculer l'activite de sortie. Auparavant, il suffisait de tester si le coeff etait null. Pour ne pas prendre les liens de neuromodulation dans le calcul de la sortie, il faut utiliser ce test pour savoir si le coeff doit etre pris ou pas. En fait, c'est un test d'arret pour savoir quand il n'y a plus de coefficient a prendre. En effet, a la compilation, coeos tri les connections en mettant toutes les connections de neuromod locale a la fin de la chaine. Quand on a trouve le premier lien de neuromod locale, on sait que le parcours des coeff classiques est fini.
   \file
   \ingroup coeff_is_valid
*/
int coeff_is_valid(type_coeff *coeff)
{
   if (coeff==NULL || liaison[coeff->gpe_liaison].mode >= NEUROMOD) return 0;
   else return 1;
}
