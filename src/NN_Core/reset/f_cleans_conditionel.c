/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_cleans_conditionel.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Met a zero toutes les sorties des groupes specifie par le lien 
 chaque numero est suivi d'une ,virgule

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

void function_cleans_conditionel(int gpe_sortie)
{
    int gpe, i, n, k, /*g, */ nofind = 1, lasti = 0;
    char linkname[255], name[255];
    int depart;
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_cleans_conditionel (multiple clean) \n");
#endif

  /*-------------------*/
    /*Find the input link */
    for (i = 0; (i < nbre_liaison) && nofind; i++)
        if (liaison[i].arrivee == gpe_sortie)
            nofind = 0;

    strcpy(linkname, liaison[i - 1].nom);
    depart = liaison[i - 1].depart;
  /*------------------------------------*/
    /*Listing all group to be cleaned     */
    /*Find each gp name and clean it      */
    while (linkname[lasti] != '\0')
    {
        k = 0;
        for (i = lasti; (linkname[i] != ',') && (linkname[i] != '\0'); i++)
        {
            name[k] = linkname[i];
            k = k + 1;
        }
        name[k] = '\0';
        lasti = i + 1;
        gpe = atoi(name);

      /*-------------------*/
        /*Cleaning each group */
#ifdef DEBUG
        printf("Clean outputs of block %d\n", gpe);
#endif

        if (neurone[def_groupe[depart].premier_ele].s1 > 0.99)
        {
            for (n = def_groupe[gpe].premier_ele;
                 n < (def_groupe[gpe].premier_ele + def_groupe[gpe].nbre);
                 n++)
                neurone[n].s = neurone[n].s1 = neurone[n].s2 = 0.0;
            printf("clean cond %d\n", gpe);
        }

    }


}
