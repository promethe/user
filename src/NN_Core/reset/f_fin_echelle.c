/******************************
 \ingroup NN_Core
 \defgroup f_fin_echelle f_fin_echelle
 *
 *
 \date May 16, 2014
 \author Ali Karaouzene
 \brief

 Fonction qui fait la même chose qu'un break mais attends que l'iteration en cours de l'echelle de temps se finisse.

 \section description
 ***************************************************************************************************************************/
#include <libx.h>

void function_fin_echelle(int gpe)
{
  int i, deb, nbre, nbre2;
  int increment;

  float temp;

  type_coeff *coeff;

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;
  nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
  increment = nbre / nbre2;

  for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
  {
    temp = 0.;

    coeff = neurone[i].coeff;

    while (coeff != NULL)
    {
      temp = temp + coeff->val * neurone[coeff->entree].s1;
      coeff = coeff->s; /* Lien suivant */
    }

    neurone[i].s = temp;
    neurone[i].s1 = neurone[i].s;
    neurone[i].s2 = neurone[i].s;
  }
  if (neurone[deb].s1 > def_groupe[gpe].seuil)
  {
    fin_echelle[def_groupe[gpe].ech_temps] = 1;
  }
}
