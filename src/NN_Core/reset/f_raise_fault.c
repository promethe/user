/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file	f_raise_fault.c
 \author	D. Bailly
 \date	11/02/2011
 \brief Put the group in fault mode according to the output of the previous group

 Modified:
 - author: xxxxx
 - description: xxxxx
 - date: xx/xx/xxxx

 Theoritical description:
 * The group use the threshold information to compare the output of the previous
 * group. If the output of the first neuron is greater than the threshold the
 * group fault field is put to one disabling the execution of the following
 * groups.



 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / neural / math / stat
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>

/*#define DEBUG
 */

typedef struct data_raise_fault
{
   int gpe_input;
} Data_raise_fault;

void new_raise_fault(int index_of_group)
{
   int i = 0, l;
   int gpe_input = -1;
   Data_raise_fault *data = NULL;

   dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

   /**
    * Looking for input groups
    */
   l = find_input_link(index_of_group, i++);
   if (l == -1) EXIT_ON_ERROR("no input link\n");
   while (l != -1)
   {
      if ((strncmp(liaison[l].nom, "sync", 4)) == 0) continue;
      else
      {
         if (gpe_input != -1)                 EXIT_ON_ERROR("more than one input link\n");
         gpe_input = liaison[l].depart;
      }
      l = find_input_link(index_of_group, i++);
   }
   /* Errors managing */
   if (gpe_input < 0)                         EXIT_ON_ERROR("no input link\n");
   if (def_groupe[index_of_group].nbre > 1)   PRINT_WARNING("there are unused neurons in this group (only one is used)\n");

   /* Creation of the data */
   data = (Data_raise_fault *) ALLOCATION(Data_raise_fault);
   data->gpe_input = gpe_input;
   def_groupe[index_of_group].data = data;

   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

}

void function_raise_fault(int index_of_group)
{
   int index_neuron, seuil;
   Data_raise_fault *data = NULL;

   dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

   data = (Data_raise_fault *) def_groupe[index_of_group].data;
   index_neuron = def_groupe[data->gpe_input].premier_ele;
   seuil = def_groupe[data->gpe_input].seuil;

   if (neurone[index_neuron].s1 > seuil)
   {
      dprints("%f : Enabling fault...\n", neurone[index_neuron].s1);
      def_groupe[index_of_group].fault = 1;
   }
   else
   {
      dprints("%f : Disabling fault...\n", neurone[index_neuron].s1);
      def_groupe[index_of_group].fault = 0;
   }

   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}

void destroy_raise_fault(int index_of_group)
{
   dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

   free(def_groupe[index_of_group].data);

   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}
