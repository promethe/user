/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_clean_next.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Met a zero toutes les sorties des groupes specifie par le lien 
 chaque numero est suivi d'une ,virgule

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include "basic_tools.h"

typedef struct MyData_f_clean_next
{
   int *gpe_to_clean;
   int nbr_gpe_to_clean;

} MyData_f_clean_next;


void function_clean_next(int gpe_sortie)
{
   int nbr_gpe_to_clean = 0;
   int *gpe_to_clean = NULL;
   int i, n;
   MyData_f_clean_next *my_data = NULL;
#ifdef DEBUG
   printf("-------------------------------------------------\n");
   printf("function_cleans (multiple clean) \n");
#endif
   if (def_groupe[gpe_sortie].data == NULL)
   {
      for (i = 0; i < nbre_liaison; i++)
         if (liaison[i].depart == gpe_sortie)
         {
            nbr_gpe_to_clean++;
         }

      gpe_to_clean = MANY_ALLOCATIONS(nbr_gpe_to_clean, int);

      n = 0;
      for (i = 0; i < nbre_liaison; i++)
         if (liaison[i].depart == gpe_sortie)
         {
            gpe_to_clean[n] = liaison[i].arrivee;
            n++;
         }

      my_data = (MyData_f_clean_next *) malloc(sizeof(MyData_f_clean_next));
      if (my_data == NULL)
      {
         printf("erreur malloc f_clean_next\n");
         exit(0);
      }
      my_data->nbr_gpe_to_clean = nbr_gpe_to_clean;
      my_data->gpe_to_clean = (int *) gpe_to_clean;
      def_groupe[gpe_sortie].data = (void *) my_data;

   }
   else
   {
      my_data = (MyData_f_clean_next *) def_groupe[gpe_sortie].data;
      nbr_gpe_to_clean = my_data->nbr_gpe_to_clean;
      gpe_to_clean = my_data->gpe_to_clean;
   }


#ifdef DEBUG
   printf("~~~~~~~~clean next %s\n",def_groupe[gpe_sortie].no_name);
#endif
   for (i = 0; i < nbr_gpe_to_clean; i++)
   {
#ifdef DEBUG
      printf("clean next: %s\n",def_groupe[gpe_to_clean[i]].no_name);
#endif
      n = 0;
      for (n = 0; n < def_groupe[gpe_to_clean[i]].nbre; n++)
      {
         neurone[def_groupe[gpe_to_clean[i]].premier_ele + n].s =
               neurone[def_groupe[gpe_to_clean[i]].premier_ele + n].s1 =
                     neurone[def_groupe[gpe_to_clean[i]].premier_ele + n].s2 = 0.;
      }
   }
#ifdef DEBUG
   printf("=========fin clean next\n");
#endif


}
