/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_even_first_transition_detect f_even_first_transition_detect
ingroup NN_Core

\brief Neurones a 1 si transition detectee (IDENTIQUE A F_TRANSITION_DETECT : A METTRE A JOUR POUR CORRESPONDRE A CETTE FONCTION !)

\details

\section Description
   Le ou les neurones du groupe passent a un des qu'une transition en entree
   est detectee puis repassent a 0 

\file 

\brief Neurones a 1 si transition detectee (IDENTIQUE A F_TRANSITION_DETECT : A METTRE A JOUR POUR CORRESPONDRE A CETTE FONCTION !)

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

************************************************************/
#include <libx.h>
#include <stdlib.h>

void new_even_first_transition_detect(int numero)
{
	int nbre,i,deb;
 	if(continue_simulation_status==START)
 	{
 		deb = def_groupe[numero].premier_ele;   /*premier element du groupe */
		nbre = def_groupe[numero].nbre; /*nbre de neurone */
	
		for (i = deb; i < deb + nbre; i++)
		{
			neurone[i].d=0.;
		}
  	}
}

void function_even_first_transition_detect(int numero)
{
    type_coeff *coeff;
    /*   float dsortie,Si; */
    /*   float bruit; */
    int i, nbre = 0, deb;
    /*   int k; */
    /*   int numero2; */

    float temp = 0.;
    float total;
    
    deb = def_groupe[numero].premier_ele;   /*premier element du groupe */
    nbre = def_groupe[numero].nbre; /*nbre de neurone */

    for (i = deb; i < deb + nbre; i++)
    {
        coeff = neurone[i].coeff;
        total = 0.;
        temp = 0.;
        while (coeff != NULL)
        {
            temp = temp + neurone[coeff->entree].s1;
            coeff = coeff->s;   /* Lien suivant */
            total = total + 1.;
        }
        temp = temp / (float) total;
#ifdef DEBUG
        printf("prev:%f,actuel:%f, passage a 1:%f\n", neurone[i].d, temp,
               fabs(neurone[i].d - temp));
#endif
        if (fabs(neurone[i].d - temp) > 0.0001)
            neurone[i].s2 = neurone[i].s1 = neurone[i].s = 1.;
        else
            neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.;

        neurone[i].d = temp;    /* memoire du passe ... */
    }
}
