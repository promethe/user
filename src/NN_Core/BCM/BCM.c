/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**************************************************************
 \file  BCM.c
 \brief simulates a not exactly BCM  neural network (this is much a kind of Exponential Learning Window as a BCM)


 version 1.1

 Author: Benoît Siri
 Created: 19/05/05
 modified : 23/05/05

 Theoritical description:


 -mise_a_jour_neurone_BCM_maison:
 as a pavlov group
 -apprend_BCM_maison:
 The learning rule is quite strange since the manner the synaptic efficacies are modified is not a continuous function. That's why we must handle it in an algorithmic way. That is :
 - The weights between the neuron and a conditionnal neuron are reinforced if the conditional neuron was active recently before the inconditional stimuli appear on the neuron
 - The weights between the neuron and a conditionnal neuron are weakened if the conditional neuron is active after the stimuli came.
 - The learning rule is based on a hebbian one

 -mise_a_jour_STM_entrees_BCM_maison :
 the use of the STM is different here. Some variables are used in the synaptic link and are used for input neurons only. We use here the type_coeff->Nbre_E field to store the timestep when the current spike train started, the Nbre_ES for the
 current timestep (thus, the last timestep of the spike train), and the Nbre_S to store this activity. These datas allows us to compare activations timesteps between neurons and
 then infere wich learns from wich.


 Description:

 This group can perform prediction of a stimulus by conditioningn, and can also perform second-order conditionning : Some conditional stimuli can be learned so frequently with respect to an inconditional one that they eventually are considered as inconditional too. So others
 conditional stimuli could be conditionned with respect to them. For this reason, the learning rule does not depend to the inconditional anymore.

 Since we need to keep the "inconditional" stimulus received a the former timestep, we use here the neurone.d to save it.

 IMPORTANT : Notice that any leto script that use this neural group MUST use a f_TimeGenerator box

 see the doc.




 Global variables:
 - created: none Should seldomly happen!!!
 - used: none, I think

 Macro:
 none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: xxxxxxxxxxx
 - description: none
 - input expected group: none
 - where are the data?: none



 Comments:



 Known bugs:

 Todo: see if the bugs still happen

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/NN_Core.h> /*pour struct timeval SystemTime */
#include <sys/time.h>

/*#define DEBUG*/

void apprend_BCM(int gpe)
{
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = def_groupe[gpe].nbre + first_neuron;
  int i;
  int neur_time; /* derniere date a laquelle le neurone a commence a etre actif */
  int neur_entree_time; /* date du dernier spike d'un neurone d'entree  */
  float var; /* vitesse d'apprentissage */
  float wtemps; /* fenetre de temps de l'apprentissage/desapprentissage, exprimee ici en millisecondes */
  int dt; /* indique l'ecart entre les date d'activation du neurone et celle du neurone d'entree considere */
  short sign; /* signe de l'apprentissage : renforcement ou affaiblissement */
  /*float normalize;    *//* pour normaliser les poids, on considere la borne comme etant liaison.stemps. on calcule ce coefficient pour eviter l'explosion des poids */
  /*float borne; */
  type_coeff *coeff;

  var = eps * def_groupe[gpe].learning_rate; /* vitesse d'apprentissage */

  /*en fonction des valeurs du signal inconditionnel a l'instant present et l'instant precedent, la politique d'apprentissage peut varier */
  for (i = first_neuron; i < last_neuron; i++)
  {

    /* on apprend pas si le neurone est inactif */
    /*if (act_actuel<=0.001) continue; */

    /*lors de sa premiere activation, le neurone modifie les poids des neurones qui ont ete actifs dans une fenetre de temps. Cette fenetre est definie
     dans le champs "temps" de la liaison */
    neur_time = neurone[i].cste;

    coeff = neurone[i].coeff;
    while (coeff != NULL)
    {
      if (coeff->evolution != 0)
      {

        wtemps = liaison[coeff->gpe_liaison].temps;
        neur_entree_time = coeff->Nbre_E;

        dt = (neur_time - neur_entree_time);

        /*l'apprentissage n'a pas lieu si dt==0 : on ne va rien apprendre et "inhiber" ce neurone */
        /*if (dt==0) coeff->Nbre_S=0.0; */

        /*si on est bien dans la fenetre de temps, il y a apprentissage ou desapprentissage.
         on apprend pas non plus si un neurone conditionnel est actif en meme temps ( du coup il deviendra impossible d'apprendre si la fenetre de temps est de 1 !) */
        /*else */if ((abs(dt) <= wtemps) && (dt != 0))
        {
          /* le signe de dt nous indique si le stimulus conditionnel est arrive avant ou apres l'activation du neurone : et donc, si on apprend ou desapprend */
          /*une fois fait, il faudra tricher et inhiber son activite pour que le neurone ne soit plus pris en compte par la suite */

          if (dt > 0) sign = 1;
          else sign = -1;
          coeff->val += sign * var * coeff->Nbre_S /**neurone[i].s*/;

          coeff->Nbre_S = 0.0;
        }
      }
      coeff = coeff->s;
    }

  }
}

/*  Dans cette fonction on calcule la mise a jour des activite des neurones de ce groupe. Il y a deux phases dans l'execution de cette fonction :
 - sauvegarde de l'activite,
 - mise a jour du neurone.
 */
void mise_a_jour_neurone_BCM(int i, int gestion_STM, int learn)
{
  float s, sf, sum_p;
  /*float tau; */
  type_coeff *synapse;
  float IR; /* inconditional response */
  int flag_IR, time;

  (void) gestion_STM; (void) learn;  /*unused parameters */
  
  time = SystemTime.tv_sec * 1e+3 + SystemTime.tv_usec * 1e-3;

  IR = 0.;
  flag_IR = 0;

  /*sauvegarde de l'activite au commencement du train d'onde. */
  neurone[i].d = neurone[i].s;

  /* calcul de la nouvelle activite */
  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  sum_p = 0.0;
  synapse = neurone[i].coeff;

  while (synapse != NULL)
  {
    if (synapse->evolution != 0) /* nous traitons ici les liens conditionnels CS */
    {
      switch (liaison[synapse->gpe_liaison].mode)
      {
      case 0:
      {
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
        break;
      }
      case 2:
      {
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
        break;
      }
      default:
        printf("Erreur mode liaison(%d) incorrect dans mise_a_jour_neurone_BCM, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
        exit(1);
      }
    }
    else
    {
      IR = IR + synapse->val * neurone[synapse->entree].s;
      flag_IR = 1;
    }
    if ((neurone[synapse->entree].s1 > 0.))
    {
      /*si le neurone a eu une coupure dans son activite, vu qu'il est de nouveau actif c'est qu'il est en train de commencer une nouvelle serie d'activation. Il faut resauvegarder la date dans Nbre_E */
      if (time - synapse->Nbre_ES > 1)
      {
        synapse->Nbre_E = time;
        synapse->Nbre_S = neurone[synapse->entree].s;
      }
      synapse->Nbre_ES = time;
    }
    synapse = synapse->s;
  }
  if (flag_IR == 0)
  {
    printf("Erreur: mise_a_jour_neurone_BCM_maison :%d, pas de lien inconditionnel... ", i);
    exit(1);
  }

  s = IR + sum_p - neurone[i].seuil;
  /* fonction rampe tenant compte du seuil */
  if (s < 0.) sf = 0.;
  else if (s > 1.) sf = 1.;
  else sf = s;

  neurone[i].s = sf;

  if (sf > 0.)
  {
    neurone[i].s1 = neurone[i].s2 = 1.0;
    /*si le neurone n'etait pas active avant, il faut stocker la date de l'activation quelque part (vu qu'il l'est maintenant). */
    if (neurone[i].d <= 0.001)
    {
      neurone[i].cste = time; /*instant activation par le stimulus inconditionnel ou le stimulus considere comme tel */
    }
  }

}

/*************************************************************obsolete****************************************************************************/

/* cette fonction gere de facon un peu speciale la memoire a court terme. La fonction classique utilise une decroissance lineaire de l'activite sur une certaine periode de temps. Ici, nous ne nous en servons pas : a la
 place nous sauvegardons l'instant auquel le neurone d'entree a ete active dans le champs Nbre_E du coefficient, et son activite analogique dans le champs Nbre_S.
 Ce n'est pas tout : il se peut que le neurone d'entree reste actif plusieurs pas de temps : il s'agit donc de ne sauvegarder que le premier instant de son activite. Pour cela on va se servir du champs Nbre_ES pour se
 rappeller si le neurone etait actif au temps precedent ou non*
 Note : si un neurone d'entree est actif au tout premier pas de temps, le champs coeff->Nbre_S ne sera pas sette : du coup il ne sera pas appris*/
void mise_a_jour_STM_entrees_BCM(int i)
{
  int j;
  type_coeff *coeff;
  int deb, nbre, time;

  deb = def_groupe[i].premier_ele;
  nbre = def_groupe[i].nbre;

  /* on va compter le temps en millisecondes */
  time = SystemTime.tv_sec * 1e+3 + SystemTime.tv_usec * 1e-3;

  for (j = deb; j < deb + nbre; j++)
  {

    coeff = neurone[j].coeff;
    if (coeff == NULL)
    {
      printf("Attention gp neurone %d sans entree \n", i);
      printf("pas de mise a jour STM entrees pour ce groupe ! \n");
      return;
    }

    while (coeff != NULL)
    {
      if ((neurone[coeff->entree].s1 > 0.))
      {
        /*si le neurone a eu une coupure dans son activite, vu qu'il est de nouveau actif c'est qu'il est en train de commencer une nouvelle serie d'activation. Il faut resauvegarder la date dans Nbre_E*/
        if (time - coeff->Nbre_ES > 1)
        {
          coeff->Nbre_E = time;
          coeff->Nbre_S = neurone[coeff->entree].s;
        }
        coeff->Nbre_ES = time;
      }
      coeff = coeff->s;
    }
  }

}
