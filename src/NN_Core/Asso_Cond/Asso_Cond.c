/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
 /** ***********************************************************
\file  f_integr.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N. Cuperlier
- description: specific file creation
- date: 11/08/2004

- author: N.Cuperlier
- description: Reset Added
- date: 15/11/2006

Theoritical description:
 - \f$  LaTeX equation:  W_{ij} = ((-1/N).W_{ij}.Si +  (1/N).S_i).H(S_k.Si)  \f$  

Description: 
  association conditionnelle de deux voies.
  l'apprentissage permet de realiser un moyennage des entrees inconditionnelles et stocke le resultat dans les poids le liant a la voie conditionnelle

Attention le champ simulation_speed contient en fait un seuil sur l'activite des neurones...
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <NN_Core/Asso_Cond.h>

float f_seuil(float seuil, float input){
if(input>seuil) return 1.;
else return 0.;
}


void apprend_Asso_Cond(int gpe)
{
  /*!
 * Pour chaque neurone i du groupe on applique �a chacun de ses coeff, la formule suivante:
  W_{ij} = ((-1/N).W_{ij}.Si +  (1/N).S_i).H(S_k.Si) 
 * - i neurone courant du groupe
 * - j neurone d'entree de la liason
 * - \f$ W_{ij} = \f$ coeff->val de la liaison entre i et j
 */
  float dc,Si,Sj;
  type_coeff *coeff;
  int i;
  int deb,nbre;
 
  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
				
	for(i=deb;i<deb+nbre;i++)
	{
			coeff=neurone[i].coeff;
			while(coeff!=NULL)
			{
				if(coeff->evolution==1)
				{
					Si=neurone[i].d;/*l'act venant de la voie mvt*/
					Sj=neurone[coeff->entree].s1; /*l'act du neurone de trans*/
					dc=(-(1./15.)*Sj*coeff->val+(1./15.)*Si)*f_seuil(0,Sj*Si) ; 
					coeff->val=coeff->val+dc;
					if(	coeff->val<0)	coeff->val=0.001;
				}
				coeff=coeff->s;
			}
	}
}


void gestion_Asso_Cond(int gpe_sortie)
{
  int	i=0,N,M, gpe=-1,deb,taille;
  float sortiep,W;
type_coeff *coeff;

/*info of the group*/
N = def_groupe[gpe_sortie].taillex;
M = def_groupe[gpe_sortie].tailley;
deb=def_groupe[gpe_sortie].premier_ele;
/*var locale*/
taille=(N*M)+deb;

for(i=deb;i<taille;i++){
/*initialisation des variables pour chaque neurones*/
sortiep=0;

coeff=neurone[i].coeff;
/*on parcors tous les coeff*/


while(coeff!=NULL)
{
		W=coeff->val;
		gpe=coeff->entree;
				
					if(coeff->type==1){
					  /*les transitions de reconnaissance*/
							sortiep=sortiep+W*neurone[gpe].s1;
       					}					
					else{
							sortiep=sortiep+/*log10(*/W*neurone[gpe].s1/*+1)*/;
						
							/*memorise l'info de mouvement*/
							neurone[i].d=W*neurone[gpe].s1;
					}
	
		coeff=coeff->s;
}


/*seuillage des valeurs trop faible en sortie*/
 if(sortiep-def_groupe[gpe_sortie].simulation_speed<0)sortiep=0.0;
 else sortiep-=def_groupe[gpe_sortie].simulation_speed;
  neurone[i].s = sortiep;
  neurone[i].s1 =sortiep;
  neurone[i].s2 =sortiep;
}
}
