/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*
 \defgroup SAW SAW
 \ingroup NN_Core
 \details
 \section  Links
 - the learn input link is a binary trigger to adapt or not. If it is over 0.5 there will be adaptation.
 - the vigilence input link gives the granularity of the stored info. The average value is 0.95 and it is advised to start with this value  if you don't know what to choose. The closer to one this value is finer will be the description of your input space by the SAW but the
 more neurons you will recruite. For a vigilance of 1.0 the SAW will always recruite and never adapt. For a vigilance below 0.80 the most of the time the SAW will learn only one pattern and always adapt it

 %%% VARIANCE %%%
 - A variance computation is activated if there is a link lr_variance linked to a neuron which will give the learning rate.
 - Change the gaussian size with a neuron connected through tau_variance

 Rmq : 	- No changes in the algo if there is no link lr_variance !
 - Pb : lr_variance need to be very low !! But therefore it can't stop recruiting too much...

 \section LINKS

 Manage type of computation according to type of learning link.

 \section WARNING

 GROUP DOES NOT USE COEFF->MOY for computation optimization as most of the time moy only copy input.s1.
 (previous code commented in case we want to come back to previous version)

 \section TODO

 Sortir tous les traitements du mode product PI pour faire un groupe
 a part.

 Attention la mesure de type distance exponentielle necessite une gestion
 particuliere pour le sigma.

 */

/* #define DEBUG 1  */

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <net_message_debug_dist.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_Core/saw.h>

#define HYSTERESIS 0.1

typedef struct data_saw {
   type_liaison *link_vigilence;
   type_liaison *link_learn;
   type_liaison *link_tau_variance;
   type_liaison *link_lr_variance;
   type_liaison *link_local_neuromod;
   int neuron_max;
   float sigma;
} data_saw;

void new_saw(int gpe)
{
   int link, index = 0/*, n=0*/;
   data_saw *mydata = NULL;
   dprints("New saw %s\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data == NULL)
   {
      mydata = ALLOCATION(data_saw);
      mydata->link_learn = NULL;
      mydata->link_vigilence = NULL;
      mydata->link_tau_variance = NULL;
      mydata->link_lr_variance = NULL;
      mydata->link_local_neuromod = NULL;
      mydata->neuron_max = -1;
      mydata->sigma = 1.;

      while ((link = find_input_link(gpe, index++)) != -1)
      {
         if (strstr(liaison[link].nom, "vigilence") != NULL)
         {
            mydata->link_vigilence = &liaison[link];
         }

         if (strstr(liaison[link].nom, "learn") != NULL)
         {
            mydata->link_learn = &liaison[link];
         }
         if (strstr(liaison[link].nom, "tau_variance") != NULL)
         {
            mydata->link_tau_variance = &liaison[link];
         }
         if (strstr(liaison[link].nom, "lr_variance") != NULL)
         {
            mydata->link_lr_variance = &liaison[link];
         }
         if (strstr(liaison[link].nom, "local_neuromod") != NULL)
         {
            mydata->link_local_neuromod = &liaison[link];
            /** test comparaison taille des groupes */
            if (def_groupe[gpe].nbre != def_groupe[mydata->link_local_neuromod->depart].nbre)
               EXIT_ON_ERROR("Local neuromod group and SAW group must have the same size (%d!=%d)\n", def_groupe[gpe].nbre, def_groupe[mydata->link_local_neuromod->depart].nbre);
         }
      }
      def_groupe[gpe].data = mydata;
   }

   dprints("Sortie new saw %s\n", def_groupe[gpe].no_name);
}

void apprend_saw(int gpe)
{
   int neuron_max, mode, i;
   int recruited = 0, learned = 0;
   int gpe_deb, gpe_nbre, neuromod_gpe_deb;
   float local_vigilence = 0., my_learn = 1.0;
   float alpha, delta;
   float Wi, Ej, coeff_avant_modif;
   data_saw *mydata = (data_saw *) def_groupe[gpe].data;
   type_liaison *link_vigilence;
   type_liaison *link_learn;
   type_liaison *link_tau_variance;
   type_liaison *link_lr_variance;
   type_liaison *link_local_neuromod;
   type_coeff *coeff;
   float /*moy_variance = -1.,*/tau_variance = 0.1, lr_variance = 0.;

   /* ajout pour adaptation */
   float max_proba = 0.;
   type_coeff *max_proba_coeff = NULL;
   type_coeff *curr_active_coeff = NULL;

   dprints("apprend_saw(%s): Entering function \n", def_groupe[gpe].no_name);

   if (mydata == NULL)
   {
      EXIT_ON_ERROR("Error retreiving data");
   }

   link_vigilence = mydata->link_vigilence;
   link_learn = mydata->link_learn;
   link_local_neuromod = mydata->link_local_neuromod;
   neuron_max = mydata->neuron_max;
   link_tau_variance = mydata->link_tau_variance;
   link_lr_variance = mydata->link_lr_variance;
   local_vigilence = vigilence;

   if (link_vigilence != NULL)
   {
      local_vigilence = vigilence * link_vigilence->norme * neurone[def_groupe[link_vigilence->depart].premier_ele].s1;
   }

   if (link_learn != NULL)
   {
      my_learn = link_learn->norme * neurone[def_groupe[link_learn->depart].premier_ele].s1;
   }

   if (link_tau_variance != NULL)
   {
      tau_variance = link_tau_variance->norme * neurone[def_groupe[link_tau_variance->depart].premier_ele].s1;
   }

   if (link_lr_variance != NULL)
   {
      lr_variance = neurone[def_groupe[link_lr_variance->depart].premier_ele].s1;
   }

   dprints("apprend_saw(%s): vigilence = %f\n", def_groupe[gpe].no_name, local_vigilence);

   /** adaptation suivant neuromod locale */
   if (link_local_neuromod != NULL)
   {
      neuromod_gpe_deb = def_groupe[link_local_neuromod->depart].premier_ele;
      gpe_deb = def_groupe[gpe].premier_ele;
      gpe_nbre = def_groupe[gpe].nbre;
      for (i = 0; i < gpe_nbre; i++)
      {

         /* Adaptation of learning */
         if (neurone[gpe_deb + i].seuil >= 0.9)
         {
            coeff = neurone[gpe_deb + i].coeff;
            recruited = 0;
            learned = 0;
            delta = neurone[neuromod_gpe_deb + i].s1;
            alpha = neurone[neuromod_gpe_deb + i].s1;

            max_proba = 0.;
            max_proba_coeff = NULL;
            curr_active_coeff = NULL;

            while (coeff != NULL)
            {
               if (coeff->evolution == 1) /* Evolution si possible */
               {
                  coeff_avant_modif = coeff->val;
                  Wi = coeff->val;
                  Ej = neurone[coeff->entree].s1; /*coeff->moy*/

                  mode = liaison[coeff->gpe_liaison].mode;
                  switch (mode)
                  {
                  case NEUROMOD_sigma: /* sigma */
                     /** Ne rien faire */
                     break;
                  case No_mode_link_produit: /* produit des entrees */
                     if (delta > 0.)
                     {
                        coeff->proba += delta * def_groupe[gpe].learning_rate * (neurone[coeff->entree].s2 - coeff->proba);
                        /*  		printf("adaptation of neurone : %d : proba : %f, curr val %f, input %f\n",i, coeff->proba, coeff->val, neurone[coeff->entree].s2); */
                        if (coeff->proba > max_proba)
                        {
                           max_proba = coeff->proba;
                           max_proba_coeff = coeff;
                        }
                        if (coeff->val > 0.)
                        {
                           curr_active_coeff = coeff;
                        }
                        /* si fin des liens vers le m groupe alors mise a jour
                         * des liens en fonction du max proba trouve */
                        /* 		if(coeff->s!=NULL) */
                        /*  		  printf("neurone[coeff->depart].groupe %d, neurone[(coeff->s)->depart].groupe %d\n",neurone[coeff->entree].groupe,neurone[(coeff->s)->entree].groupe);  */
                        if (coeff->s == NULL || neurone[coeff->entree].groupe != neurone[(coeff->s)->entree].groupe)
                        {
                           if (max_proba_coeff == NULL)
                              PRINT_WARNING("OUPS SHOULD NOT SEE THIS\n");
                           /*		  if(curr_active_coeff!=NULL) {
                   cprints("Neurone (%d) %f %f %p %f %f %p\n", i, max_proba, HYSTERESIS,curr_active_coeff,curr_active_coeff->proba,curr_active_coeff->val,max_proba_coeff);
                   } */
                           if (curr_active_coeff != NULL && max_proba > (curr_active_coeff->proba + HYSTERESIS))
                           {
                              curr_active_coeff->val = 0.;
                              max_proba_coeff->val = 1.;
                           }
                           else if (curr_active_coeff == NULL && max_proba_coeff != NULL)
                           {
                              max_proba_coeff->val = 1.;
                           }

                           max_proba = 0.;
                           max_proba_coeff = NULL;
                           curr_active_coeff = NULL;
                        }
                     }
                     break;
                  case No_mode_link_distance_expo: /* type distance exponentielle */
                     coeff->val += delta * (Ej - Wi);
                     break;
                  case No_mode_link_distance_euclidienne: /* type distance euclidienne */
                     coeff->val += delta * (Ej - Wi) * (1. - neurone[neuron_max].s);
                     break;
                  case No_mode_link_distance_compet: /* type distance */
                  case No_mode_link_distance_analog:
                     coeff->val += delta * (Ej - Wi) * (1. - neurone[neuron_max].s);
                     break;
                  case No_mode_link_product_compet:
                  case No_mode_link_product_analog: /* type product */
                     coeff->val += delta * Ej - alpha * Wi;
                     break;
                  default:
                     EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for adaptation of link with local_neuromod\n", def_groupe[gpe].no_name, mode);
                  }

                  /* Saturation of the weights for product links */
                  if (coeff->type % 2 == 0 && coeff->val > 1.) coeff->val = 1;

                  /* Marks the neuron as having learned for debug */
                  if (isdiff(coeff_avant_modif, coeff->val)) learned = 1;
               }

               coeff = coeff->s;
            }
         }
      }
   }
   if (local_vigilence < 0. || my_learn <= 0. || neuron_max < 0)
   {
      dprints("apprend_saw(%s) : vigilence < 0 or learn <= 0 or group full: no learning\n", def_groupe[gpe].no_name);
      return;
   }

   /* delta de la regle d'adaptation */
   delta = def_groupe[gpe].learning_rate * my_learn;

   /* Decay factor */
   alpha = def_groupe[gpe].alpha;


   /* On ne fait l'apprentissage que pour le gagnant pour gagner en
    * temps de calcul ! */
   coeff = neurone[neuron_max].coeff;
   recruited = 0;
   learned = 0;
   while (coeff != NULL)
   {
      if (coeff->evolution == 1) /* Evolution si possible */
      {
         coeff_avant_modif = coeff->val;
         Wi = coeff->val;
         Ej = neurone[coeff->entree].s1; /*coeff->moy*/

         /* Fast (one-shot) learning for the first learning*/
         if (neurone[neuron_max].seuil < 0.9) /*recruit*/
         {
            mode = liaison[coeff->gpe_liaison].mode;
            switch (mode)
            {
            case NEUROMOD_sigma: /* sigma */
               /** Ne rien faire */
               break;
            case No_mode_link_produit: /* produit des entrees et poids
             * entre eux */
               if (Ej > 0.)
               {
                  coeff->val = 1.;
                  coeff->proba = 1.;
               }
               else
               {
                  coeff->val = 0.;
                  coeff->proba = 0.;
               }
               if (coeff->val > 0) recruited = 1;
               break;
            case No_mode_link_distance_expo: /* type distance exponentielle */
               coeff->val = Ej;
               recruited = 1; /* signals the recruitment */
               break;
            case No_mode_link_distance_euclidienne: /* type distance euclidienne */
               coeff->val = Ej;
               recruited = 1; /* signals the recruitment */
               break;
            case No_mode_link_distance_compet:
            case No_mode_link_distance_analog: /* type distance */
               coeff->val = Ej;
               coeff->Nbre_E = 0;
               recruited = 1; /* signals the recruitment */
               break;
            case No_mode_link_product_compet:
            case No_mode_link_product_analog: /* type product */
               coeff->val = delta * Ej;

               /* For product type links, a neuron can only be
                * recruited if it has some input activity */
               if (coeff->val > 0) recruited = 1;
               break;
            default:
               EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[gpe].no_name, mode);
            }
         }
         else /*Adapt*/
         {
            mode = liaison[coeff->gpe_liaison].mode;
            switch (mode)
            {
            case NEUROMOD_sigma: /* sigma */
               /** Ne rien faire */
               break;
            case No_mode_link_produit: /* produit des entrees */
               if (Ej > 0.) coeff->val = 1;
               else coeff->val = 0;
               break;
            case No_mode_link_distance_expo: /* type distance exponentielle */
               coeff->val += delta * (Ej - Wi);
               break;
            case No_mode_link_distance_euclidienne: /* type distance euclidienne */
               coeff->val += delta * (Ej - Wi) * (1. - neurone[neuron_max].s);
               break;
            case No_mode_link_distance_compet: /* type distance */
            case No_mode_link_distance_analog:
               coeff->val += delta * (Ej - Wi) * (1. - neurone[neuron_max].s);
               if (link_lr_variance != NULL)
               {
                  coeff->Nbre_E = coeff->Nbre_E + lr_variance * (fabs(Ej - coeff->val) - coeff->Nbre_E);
                  coeff->Nbre_S = exp(-(float) (coeff->Nbre_E * coeff->Nbre_E) / (2 * tau_variance * tau_variance)); /*Dynamique entre 0 et 100*/
               }
               break;
            case No_mode_link_product_compet:
            case No_mode_link_product_analog: /* type product */
               coeff->val += delta * Ej - alpha * Wi;
               break;
            default:
               EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[gpe].no_name, mode);
            }
         }

         /* Saturation of the weights for product links */
         if (coeff->type % 2 == 0 && coeff->val > 1.) coeff->val = 1;

         /* Marks the neuron as having learned for debug */
         if (isdiff(coeff_avant_modif, coeff->val)) learned = 1;
      }
      coeff = coeff->s;
   }

   if (learned == 1)
      dprints("apprend_saw(%s): learned or adapted\n", def_groupe[gpe].no_name);

   if (recruited == 1)
   {
      /* marks the neuron as recruited */
      neurone[neuron_max].s1 = 1.;
      neurone[neuron_max].seuil = 0.99999;
      dprints("apprend_saw(%s): Neuron recruited\n", def_groupe[gpe].no_name);
   }

   dprints("apprend_saw(%s): Leaving function \n", def_groupe[gpe].no_name);
}

void mise_a_jour_saw(int i, int gestion_STM, int learn)
{
   type_coeff *coeff;
   float sortie = 0., sortie2 = 0., produit = 1.,/* sortie_ss_pruning = 0.,*/sortie_variance_norm = 0.;
   float sum_distance = 0., sum_product = 0., sum_expo = 0., sum_distance_ss_pruning = 0., sum_variance = 0., sum_distance_variance = 0.;
   int nb_weights_distance = 0, expo_activ = 0, nb_weights_distance_euclidienne = 0, prod_activ = 0;
   float sigma = 1., input = 0.;
   int mode, nb_link_produit = 0;

   type_liaison *link_lr_variance;
   /*type_liaison *link_vigilence;*/
   data_saw *data;

   (void) gestion_STM;
   (void) learn;
   dprints("mise_a_jour_saw(neuron %i): Entering function \n", i);
   data = (data_saw *) def_groupe[neurone[i].groupe].data;
   sigma = data->sigma;
   /*link_vigilence = data->link_vigilence;*/
   link_lr_variance = data->link_lr_variance;

   if (neurone[i].seuil > 0.9)
   {
      coeff = neurone[i].coeff;

      while (coeff != NULL)
      {
         mode = liaison[coeff->gpe_liaison].mode;
         input = neurone[coeff->entree].s1;/*coeff->moy*/
         switch (mode)
         {
         case NEUROMOD_sigma: /* sigma */
            sigma = coeff->val * input;
            data->sigma = sigma;
            if (isequal(sigma, 0.))
               PRINT_WARNING("sigma is null in SAW (%s) ! Sigma is set to 1.\n", def_groupe[neurone[i].groupe].no_name);
            break;
         case No_mode_link_produit: /* produit des entrees */
            if (coeff->val > 0.)
            {
               produit *= coeff->val * input;
               /* calcul specifique pour neuromod locale sur entree - unused */
               /* produit2 *= coeff->val * neurone[coeff->entree].s2; */
               nb_link_produit++;
            }
            break;
         case No_mode_link_distance_expo: /* type distance exponentielle */
            /* modulation par la covariance stockee sur neurone[coeff->entree].s2 */
            /*sum_expo+=(coeff->val - input)*(coeff->val - input)/(neurone[coeff->entree].s2*neurone[coeff->entree].s2);*/
            sum_expo += (coeff->val - input) * (coeff->val - input) / (sigma * sigma);
            if (isequal(sigma, 0.))
               EXIT_ON_ERROR("SAW needs covariance on input.s2 to work with exponential distance. TODO :clean code !\n");
            expo_activ = 1;
            break;
         case No_mode_link_distance_euclidienne: /* type distance euclidienne */
            sum_distance += (coeff->val - input) * (coeff->val - input);
            nb_weights_distance_euclidienne++; /*Nombre de poids pris en compte */
            break;
         case No_mode_link_distance_compet: /* type distance */
         case No_mode_link_distance_analog:
            sum_distance_ss_pruning += fabs(coeff->val - input);
            /*Modif M. Maillard : la somme precedente avec la division apres par
         nbre_poids1 est equivalente a celle ci avec 1 - sum() ca evite de
         faire 1- pour chaque coeff */
            nb_weights_distance++; /*Nombre de poids pris en compte */

            if (link_lr_variance != NULL)
            {
               if (isdiff(coeff->Nbre_E, 0.))
               {
                  sum_distance_variance += (coeff->Nbre_S) * fabs(coeff->val - neurone[coeff->entree].s1 /*coeff->moy*/);
                  sum_variance += (coeff->Nbre_S);
               }
               else sum_distance_variance += (fabs(coeff->val - neurone[coeff->entree].s1 /*coeff->moy*/));
            }
            break;
         case No_mode_link_product_compet:
         case No_mode_link_product_analog: /* type product */
            sum_product += coeff->val * input;
            prod_activ = 1;
            break;
         default:
            EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[neurone[i].groupe].no_name, mode);
         }
         coeff = coeff->s;
      }

      /* If mode_link_produit */
      if (nb_link_produit > 0)
      {
         sortie += produit;
         /* sortie2+=produit2; *//* unused ? */
      }

      /* If mode_link_distance_analog */
      if (link_lr_variance != NULL && isdiff(sum_variance, 0.))
      {
         /* With variance */
         sortie_variance_norm += 1 - sum_distance_variance / sum_variance;
         sortie = sortie_variance_norm;
      }
      else
      {
         /* Without variance */
         if (nb_weights_distance > 0) sortie += 1 - (sum_distance_ss_pruning / nb_weights_distance);
      }
      /* type distance euclidienne */
      if (nb_weights_distance_euclidienne > 0)
      {
         sortie += 1 - (sqrt(sum_distance) / nb_weights_distance_euclidienne);
      }

      /* If mode_link_product_analog */
      if (prod_activ) sortie += sum_product;

      /* If mode_link_distance_expo */
      if (expo_activ)
      {
         /* sortie2 pour pouvoir calculer le max quand expo ne donne plus
          * la precision necessaire (=0)*/
         neurone[i].cste = sortie; /* bias */
         sortie2 = -0.5 * sum_expo /*/ sigma*/;
         sortie += exp(sortie2);
      }
      else sortie2 = sortie;

      dprints("Sigma : %f\n", sigma);
   }

   neurone[i].s = sortie;
   neurone[i].d = sortie2; /* .d utilise pour calculer le max */

   dprints("mise_a_jour_saw(neuron %i): Leaving function \n", i);
}

void gestion_groupe_saw(int gpe)
{
   int i = 0, n, nmax, deb, nbre, posmax, nbre_de_1, nbr_imagettes = 0;
   float max, max_d, mmoy, std, norme = 0., CorSeuil = 0.1;
   float local_vigilence = 0., my_learn = 1.;
   data_saw *mydata = (data_saw *) def_groupe[gpe].data;
   type_liaison *link_vigilence;
   type_liaison *link_learn;

   if (mydata == NULL)
      EXIT_ON_ERROR("Error retreiving data");

   link_vigilence = mydata->link_vigilence;
   link_learn = mydata->link_learn;

   dprints("gestion_groupe_saw(%s): Entering function \n", def_groupe[gpe].no_name);

   local_vigilence = vigilence;
   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre_de_1 = def_groupe[gpe].nbre_de_1;

   if (link_vigilence != NULL)
   {
      local_vigilence = /*global_learn *  */link_vigilence->norme * vigilence * neurone[def_groupe[link_vigilence->depart].premier_ele].s1;
   }

   if (link_learn != NULL)
   {
      my_learn = link_learn->norme * neurone[def_groupe[link_learn->depart].premier_ele].s1;
   }

   dprints("gestion_groupe_saw(%s): vigilence = %f\n", def_groupe[gpe].no_name, local_vigilence);

   max = -9999999.;
   max_d = -9999999.;
   mmoy = 0.;
   posmax = deb; /*Et non 0 car le premier coup posmax sera pas change car y'a pas de max et donc faut qu'il soit bon */

   for (n = deb; n < deb + nbre; n++)
   {
      if (neurone[n].seuil > 0.9) /*Que parmis les formes qui ont appris */
      {
         /* Compte le nombre de formes deja apprises */
         nbr_imagettes++;
         /* Calcul moyenne du signal en entree */
         mmoy = mmoy + neurone[n].s;
         /* Calcul de la norme ou deviation standard */
         norme = norme + (neurone[n].s * neurone[n].s);
         if (neurone[n].d > max_d) /* recherche du max sur .d */
         {
            max = neurone[n].s;
            max_d = neurone[n].d;
            posmax = n;
         }
      }

      /* Tout mis a zero */
      neurone[n].s2 = 0.;
      neurone[n].s1 = 0.;
   }

   /* Si on a encore jamais appris donc pas besoin de ce qui suit,
    * evite les divisions par 0 */
   if (nbr_imagettes != 0)
   {
      mmoy = mmoy / (float) nbr_imagettes;
      std = sqrt(norme / (float) nbr_imagettes - mmoy * mmoy);
      CorSeuil = mmoy + std;
   }

   dprints("gestion_groupe_saw(%s): nombre d'imagettes deja apprises = %d \n", def_groupe[gpe].no_name, nbr_imagettes);
   dprints("gestion_groupe_saw(%s): valeur du max trouvee = %f (%f)\n", def_groupe[gpe].no_name, max, max_d);
   dprints("gestion_groupe_saw(%s): Le gagnant est %d \n", def_groupe[gpe].no_name, posmax - deb);

   /* On ne garde que les maximas >= a la vigilance neuro-modulee ou non */
   if (max >= local_vigilence)
   {
      if (isequal(neurone[posmax].d, neurone[posmax].s))
      {
         if (neurone[posmax].s < 1.) neurone[posmax].s1 = neurone[posmax].s;
         else neurone[posmax].s1 = 1.;
      }
      else
      { /* case exponential distance - normalization (max=1) */
         neurone[posmax].s1 = exp(neurone[posmax].d - max_d) + neurone[posmax].cste;
         if (neurone[posmax].s1 > 1.) neurone[posmax].s1 = 1;
         /* must be = 1 */
      }

      neurone[posmax].s2 = 1.;
      mydata->neuron_max = posmax;

      /** **************************************************************** **/

      if (nbre_de_1 < 0)
      {
         /*Recherche des max secondaires */
         for (n = deb; n < deb + nbre; n++)
         {
            /*Que parmis les formes apprises autres que le max */
            if (neurone[n].s > local_vigilence && neurone[n].s > CorSeuil && isequal(neurone[n].s1, 0.) && neurone[n].seuil > 0.9)
            {
               neurone[n].s1 = neurone[n].s * 0.5; /*faudrait neuromoduler ca */
               neurone[n].s2 = neurone[n].s * 0.5; /*0.1 */
            }
         }
      }
      else if (nbre_de_1 > 1 && nbre_de_1 < nbre) /*nbre_de_1 gagnants supperieurs a la vigilence*/
      {
         /* recherche des max suivants nbre_de_1 donne le nbre de max suivant
          * que l'on veut on peut le changer dans le script */
         for (nmax = 0; nmax < nbre_de_1 - 1; nmax++)
         {
            max = -999999.;
            posmax = -1.;
            /* cherche le max local au neurone n considere */
            for (i = deb; i < deb + nbre; i++)
            {
               if (neurone[i].s > max && neurone[i].s2 <= 0.)
                  /*  test sur .s2 si c est a zero c'est que ce n'est pas un
                   * max que l'on a deja selectionne donc on peut le
                   * selectionner*/
               {
                  max = neurone[i].s;
                  posmax = i;
               }
            }
            if (max >= local_vigilence)
            {
               if (neurone[posmax].s < 1.) neurone[posmax].s2 = neurone[posmax].s1 = neurone[posmax].s;
               else neurone[posmax].s2 = neurone[posmax].s1 = 1;

            }
            else neurone[posmax].s2 = 1; /*pour ne pas revisiter ce max */

            dprints("gestion_groupe_saw(%s): le gagnant %i vaut %f \n", def_groupe[gpe].no_name, posmax - def_groupe[gpe].premier_ele, neurone[posmax].s1);
         }
      }
      else if (nbre_de_1 == 0) /*Tous les gagnants quelque soit leur activite*/
      {
         for (i = deb; i < deb + nbre; i++)
         {
            if (neurone[i].seuil > 0.9 && i != posmax)
            {
               if (isequal(neurone[i].d, neurone[i].s))
               {
                  if (neurone[i].s < 1.) neurone[i].s1 = neurone[i].s;
                  else neurone[i].s1 = 1;
                  neurone[i].s2 = 0; /* s2 contient la position du max (=1) */
               }
               else
               { /* case exponential distance - normalization (max=1) */
                  neurone[i].s1 = exp(neurone[i].d - max_d) + neurone[i].cste;
                  if (neurone[i].s1 > 1.) neurone[i].s1 = 1;
                  neurone[i].s2 = 0;
               }
            }
         }
      }
      /*else
     {
     cas un seul gagnant

     }*/
   }
   else /* if the max is lower than the vigilence */
   {
      if (nbr_imagettes < nbre)
      {
         dprints("gestion_groupe_saw(%s): Nouvelle imagette a apprendre\n", def_groupe[gpe].no_name);
         n = deb + nbr_imagettes;
         neurone[n].s2 = 1.;

         if (my_learn > 0.0) neurone[n].s1 = 1.;
         else neurone[n].s1 = 0.;

         mydata->neuron_max = n;
      }
      else
      {
         PRINT_WARNING("Plus de place pour stocker une nouvelle imagette\nappuyer sur une touche pour continuer");
         /*getchar();
       getchar();*/
         mydata->neuron_max = -1;
      }
   }

   dprints("gestion_groupe_saw(%s): Leaving function \n", def_groupe[gpe].no_name);
}

void destroy_saw(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free((data_saw *) def_groupe[gpe].data);
      def_groupe[gpe].data = NULL;
   }
}
