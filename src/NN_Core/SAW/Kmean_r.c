/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*
 \defgroup Kmean_r Kmean_r
 \ingroup NN_Core

 \author Nils Beaussé
 \date: 13/01/2014@

 \details
 Une version simplifié et optimisé (legerement) du SAW.
 Implémente un catégoriseur simple de type Kmean avec recrutement.

 Si l'une des catégorie (neuronnes) est inférieur à Vigilence, on recrute un nouveau neurone de sorte qu'il soit maximum (=1) pour le motif en entrée.
 Ceci jusqu'a épuisement des catégorie

 L'adaptation est de type Kmean simple : adaptation du gagnant uniquement. (très stable mais très lent).

 \section Options
 \subsection Entrées
 - vigilence : liens d'entrée de la vigilence (modulé par la vigilence globale)
 - learn : liens d'entrée du facteur d'apprentissage (modulé par le learn global)
 - learn_product : Entrée du groupe dont le neurone sera pris en compte en cas d'utilisation de liens de type somme de poids*entrée au lieu des liens distance. Parametre du mécanisme de recrutement
 - N : Entrée du groupe dont le neurone représentera le facteur pour le normage des liens de type somme de poids*entrée. Si égale à la somme de la norme des entrées : donne une réaction très similaire à une distance (mais dont les 0 sont pris en compte)

 Liens de neuromod Sigma : Pour le sigma de la distance expo.
 \brief

 Author: Nils Beaussé
 Created: 13/01/2014
 Modified: XXXXXXXXX

 Une version simplifié et optimisé (legerement) du SAW.
 - date: 13/01/2014

 \section TODO

 */

/* #define DEBUG 1  */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_Core/kmean_r.h>

#define HYSTERESIS 0.1
#define PI  3.1415
#define MAX_CONTIN 5

typedef struct data_kmeanr {
  type_liaison *link_vigilence;
  type_liaison *link_learn;
  type_liaison *link_learn_product;
  int neuron_max;
  float sigma;
  int taille;
  float max_d;
  float max;
  int posmax;
  int nbr_neurone_recrute;
  float mmoy;
  float norme;
  int last_recruted;
  int N_entree;
} data_kmeanr;

inline void seuillage_kmeanr(const int i)
{
  if (neurone[i].seuil > 0.9)
  {
    if (neurone[i].s < 1. && neurone[i].s > 0.) neurone[i].s1 = neurone[i].s;
    else if (neurone[i].s >= 1.0) neurone[i].s1 = 1.0;
    else neurone[i].s1 = 0.0;

    neurone[i].s2 = 0;
  }
  else // autre neuronne
  {
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
  }

}

void new_Kmean_R(const int gpe)
{
  int link, index = 0;
  data_kmeanr *mydata = NULL;
  char string[10] = "";
  if (def_groupe[gpe].data == NULL)
  {
    mydata = ALLOCATION(data_kmeanr);
    mydata->link_learn = NULL;
    mydata->link_vigilence = NULL;
    mydata->link_learn_product = NULL;
    mydata->neuron_max = -1;
    mydata->sigma = 1;
    mydata->taille = -1;
    mydata->max_d = -9999999.;
    mydata->posmax = def_groupe[gpe].premier_ele;
    mydata->max = -9999999.;
    mydata->mmoy = 0;
    mydata->norme = 0;
    mydata->nbr_neurone_recrute = 0;
    mydata->last_recruted = -1;
    mydata->N_entree = -1;

    while ((link = find_input_link(gpe, index++)) != -1)
    {
      if (strstr(liaison[link].nom, "vigilence") != NULL)
      {
        mydata->link_vigilence = &liaison[link];
      }
      if (strstr(liaison[link].nom, "learn") != NULL)
      {
        if (strstr(strstr(liaison[link].nom, "learn"), "_produ") == NULL) mydata->link_learn = &liaison[link];
      }
      if (strstr(liaison[link].nom, "learn_product") != NULL)
      {
        mydata->link_learn_product = &liaison[link];
      }
      if (prom_getopt(liaison[link].nom, "N", string) >= 1)
      {
        mydata->N_entree = liaison[link].depart;
      }
    }

    def_groupe[gpe].data = mydata;
  }

  dprints("Sortie new saw %s\n", def_groupe[gpe].no_name);
}

void apprend_Kmean_R(const int gpe)
{
  int neuron_max, mode, premier_passage_product = 1;
  int recruited = 0, reboucle = 1;
  float my_learn = 1.0;
  float my_learn_product = 0.0, norme_coeff = 0.0;
  float delta, delta_product;
  float Wi, Ej, N = 1.0;
  data_kmeanr *mydata = (data_kmeanr *) def_groupe[gpe].data;
  type_liaison *link_learn = NULL;
  type_liaison *link_learn_product = NULL;
  type_coeff *coeff = NULL;

  if (mydata->N_entree >= 0)
  {
    N = neurone[def_groupe[mydata->N_entree].premier_ele].s1;
  }

  link_learn = mydata->link_learn;
  link_learn_product = mydata->link_learn_product;
  neuron_max = mydata->neuron_max;

  if (link_learn != NULL)
  {
    my_learn = neurone[def_groupe[link_learn->depart].premier_ele].s1;
  }
  if (link_learn_product != NULL)
  {
    my_learn_product = neurone[def_groupe[link_learn_product->depart].premier_ele].s1;
  }

  if (neuron_max < 0) // pas de max (cas du début par exemple)
  {
    dprints("apprend_saw(%s) : vigilence < 0 or learn <= 0 or group full: no learning\n", def_groupe[gpe].no_name);
    return;
  }

  /* delta de la regle d'adaptation */
  delta = def_groupe[gpe].learning_rate * my_learn;
  delta_product = def_groupe[gpe].learning_rate * my_learn_product;
  /* On ne fait l'apprentissage que pour le gagnant pour gagner en
   * temps de calcul ! */

  coeff = neurone[neuron_max].coeff;
  recruited = 0;

  if (neurone[neuron_max].seuil < 0.9) /* Si pas recruté -> recruit*/
  {
    while (coeff != NULL)
    {
      reboucle = 1;

      if (coeff->evolution == 1) /* Evolution si possible */
      {
        Ej = neurone[coeff->entree].s1;
        mode = liaison[coeff->gpe_liaison].mode;

        switch (mode)
        {
        case NEUROMOD_sigma: /* sigma */
          /** Ne rien faire */
          break;
        case No_mode_link_produit: /* produit des entrees et poids
         * entre eux */
          if (Ej > 0.)
          {
            coeff->val = 1.;
            coeff->proba = 1.;
          }
          else
          {
            coeff->val = 0.;
            coeff->proba = 0.;
          }
          if (coeff->val > 0) recruited = 1;
          break;
        case No_mode_link_distance_expo: /* type distance exponentielle */
          coeff->val = Ej;
          recruited = 1; /* signals the recruitment */
          break;
        case No_mode_link_distance_euclidienne: /* type distance euclidienne */
          coeff->val = Ej;
          recruited = 1; /* signals the recruitment */
          break;
        case No_mode_link_distance_compet:
        case No_mode_link_distance_analog: /* type distance */
          coeff->val = Ej;
          coeff->Nbre_E = 0;
          recruited = 1; /* signals the recruitment */
          break;
        case No_mode_link_product_compet:
        case No_mode_link_product_analog: /* type product */

          if (premier_passage_product == 1) // cas du product : on fait deux boucles, une pour calculer la norme des entrées et mettre les coeff à leur valeur d'initialisation, l'autre pour normer les coeffs.
          {
            coeff->val = delta_product * Ej;
            norme_coeff += coeff->val * coeff->val;

            if (coeff->s == NULL)
            {
              coeff = neurone[neuron_max].coeff; // on repart du début !
              premier_passage_product = 0;
              reboucle = 0;
            }
          }
          else
          {
            if (isdiff(norme_coeff, 0.000000)) coeff->val = coeff->val / (N * sqrt(norme_coeff)); // si l'entree est bien normée, met de fait la valeur du neurone à 1 apres cette operation
          }

          if (coeff->val > 0) recruited = 1; // For product type links, a neuron can only be recruited if it has some input activity */
          break;
        default:
          EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[gpe].no_name, mode);
        }
      }
      if (reboucle == 1) coeff = coeff->s;
    }
  }

  if (recruited == 1)
  {
    /* marks the neuron as recruited */
    mydata->last_recruted = neuron_max;
    neurone[neuron_max].seuil = 0.99999;
  }

  if (neurone[neuron_max].seuil >= 0.9 && delta > 0.000000) /*Adapt : adaptation simple type Kmean, uniquement sur le gagnant, si il y a une learning de definis uniquement, sinon on saute le passage, pour optimiser  */
  {
    coeff = neurone[neuron_max].coeff;
    norme_coeff = 0.0;
    reboucle = 1;
    premier_passage_product = 1;
    while (coeff != NULL)
    {
      reboucle = 1;
      if (coeff->evolution == 1) /* Evolution si possible */
      {
        Wi = coeff->val;
        Ej = neurone[coeff->entree].s1;
        if (def_groupe[neurone[coeff->entree].groupe].type != No_SAW || def_groupe[neurone[coeff->entree].groupe].type != No_Kmean_R || neurone[coeff->entree].seuil > 0.9)
        {
          mode = liaison[coeff->gpe_liaison].mode;
          switch (mode)
          {
          case NEUROMOD_sigma: /* sigma */
            /** Ne rien faire */
            break;
          case No_mode_link_produit: /* produit des entrees */
            if (Ej > 0.) coeff->val = 1;
            else coeff->val = 0;
            break;
          case No_mode_link_distance_expo: /* type distance exponentielle */
            coeff->val += delta * (Ej - Wi);
            break;
          case No_mode_link_distance_euclidienne: /* type distance euclidienne */
            coeff->val += delta * (Ej - Wi) * (1. - neurone[neuron_max].s);
            break;
          case No_mode_link_distance_compet: /* type distance */
          case No_mode_link_distance_analog:
            coeff->val += fabs(coeff->Nbre_ES - Ej) * delta * (Ej - Wi);
            break;
          case No_mode_link_product_compet:
          case No_mode_link_product_analog: /* type product */
            if (premier_passage_product == 1) // Dans ce cas on est dans le passage classique : modif des poids ET calculs de la norme
            {
              coeff->val += fabs(coeff->Nbre_ES - Ej) * delta * (1 - neurone[neuron_max].s1);
              norme_coeff += coeff->val * coeff->val; // On met à jour la norme

              // Utilisation de la boucle d'adaptation pour calculer la norme : pratique car évite de faire une boucle dans gestion.
              if (coeff->s == NULL)
              {
                coeff = neurone[neuron_max].coeff; // on repart du début !
                premier_passage_product = 0;
                reboucle = 0;
              }
            }
            else
            {
              if (isdiff(norme_coeff, 0.000000)) coeff->val = coeff->val / (N * sqrt(norme_coeff)); // On norme les coeffiscients
            }
            break;
          default:
            EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[gpe].no_name, mode);
          }
        }
        coeff->Nbre_ES = Ej; //permet de garder une trace de l'ancienne valeur'

      }
      if (reboucle == 1) coeff = coeff->s;
    }
  }
}

void mise_a_jour_Kmean_R(const int i, int gestion_STM, int learn)
{
  type_coeff *coeff;
  float sortie = 0., sortie2 = 0., produit = 1.;
  float sum_distance = 0., sum_product = 0., sum_expo = 0., sum_distance_analog = 0.0;
  int nb_weights_distance = 0, nb_weights_distance_eucli = 0, expo = 0;
  float sigma = 1., input = 0.;
  int mode, nb_link_produit = 0, product = 0;

  data_kmeanr *mydata = (data_kmeanr *) def_groupe[neurone[i].groupe].data;
  sigma = mydata->sigma;

  (void) gestion_STM;
  (void) learn;

  if (neurone[i].seuil > 0.9)
  {
    coeff = neurone[i].coeff;

    while (coeff != NULL)
    {
      if ((def_groupe[neurone[coeff->entree].groupe].type != No_SAW || def_groupe[neurone[coeff->entree].groupe].type != No_Kmean_R || neurone[coeff->entree].seuil > 0.9)) // on ne prends en compte que les neurones interessantes en entree, notemment si ce sont des groupes qui recrute en entree (SAW ou Kmean_R, liste à completer) on ne prends que les neurones déja recruté.
      {
        mode = liaison[coeff->gpe_liaison].mode;
        input = neurone[coeff->entree].s1;
        switch (mode)
        {
        case NEUROMOD_sigma: /* sigma */
          sigma = coeff->val * input;
          mydata->sigma = sigma;
          if (isequal(sigma, 0.))
          PRINT_WARNING("sigma is null in SAW (%s) ! Sigma is set to 1.\n", def_groupe[neurone[i].groupe].no_name);
          break;
        case No_mode_link_produit: /* produit des entrees */
          if (coeff->val > 0.)
          {
            produit *= coeff->val * input;
            nb_link_produit++;
          }
          break;
        case No_mode_link_distance_expo: /* type distance exponentielle */
          /* modulation par la covariance stockee sur neurone[coeff->entree].s2 */
          sum_expo += (coeff->val - input) * (coeff->val - input) / (sigma * sigma);
          expo = 1; // Necessaire car sum_expo = 0 est un cas valide !
          break;
        case No_mode_link_distance_euclidienne: /* type distance euclidienne */
          if (coeff->val > 0.)
          {
            sum_distance += (coeff->val - input) * (coeff->val - input);
            nb_weights_distance_eucli++; /*Nombre de poids pris en compte */
          }
          break;
        case No_mode_link_distance_compet: /* type distance */
        case No_mode_link_distance_analog:
          sum_distance_analog += fabs(coeff->val - input);
          /*Modif M. Maillard : la somme precedente avec la division apres par
           nbre_poids1 est equivalente a celle ci avec 1 - sum() ca evite de
           faire 1- pour chaque coeff */
          nb_weights_distance++; /*Nombre de poids pris en compte */
          break;
        case No_mode_link_product_compet:
        case No_mode_link_product_analog: /* type product */
          sum_product += coeff->val * input;
          product = 1;
          break;
        default:
          EXIT_ON_ERROR("Group %s : Not an authorized mode (%d) for link\n", def_groupe[neurone[i].groupe].no_name, mode);
        }
      }
      coeff = coeff->s;

    }

    /* Cas particulier pour chaque situation */
    if (nb_link_produit > 0)
    {
      sortie += produit;
    }
    if (nb_weights_distance > 0)
    {
      sortie += 1 - (sum_distance_analog / (float) nb_weights_distance);
    }
    if (nb_weights_distance_eucli > 0)
    {
      sortie += 1 - ((sqrt(sum_distance)) / ((float) nb_weights_distance_eucli));
    }
    if (product > 0) sortie += sum_product;

    //default case
    sortie2 = sortie;

    /* If mode_link_distance_expo */
    if (expo > 0)
    {
      /* sortie2 pour pouvoir calculer le max quand expo ne donne plus
       * la precision necessaire (=0)*/
      sortie2 = -0.5 * sum_expo;
      sortie += exp(sortie2);
    }
    dprints("Sigma : %f\n", sigma);
  }

  neurone[i].s = sortie;
  neurone[i].d = sortie2;
  seuillage_kmeanr(i);

// Recherche du max,  On profite de la boucle des i pour aller plus vite (evite de chercher le max dans la gestion)
  if (neurone[i].seuil > 0.9) /*Que parmis les formes qui ont appris */
  {
    /* Compte le nombre de neurone recrute */
    mydata->nbr_neurone_recrute++;
    if (neurone[i].d > mydata->max_d) /* recherche du max sur .d */
    {
      mydata->max = neurone[i].s1;
      mydata->max_d = neurone[i].d;
      mydata->posmax = i;
    }
  }
}

void gestion_groupe_Kmean_R(const int gpe)
{
  int n, deb, nbre, posmax, nbr_neurone_recrute = 0;
  float local_vigilence = 0., max;
  data_kmeanr *mydata = (data_kmeanr *) def_groupe[gpe].data;
  type_liaison *link_vigilence = NULL;

  link_vigilence = mydata->link_vigilence;

  local_vigilence = vigilence;
  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;

  if (link_vigilence != NULL)
  {
    local_vigilence = neurone[def_groupe[link_vigilence->depart].premier_ele].s1; // on ne prends pas en compte la norme du liens de vigilence.
  }

  max = mydata->max;
  posmax = mydata->posmax;
  nbr_neurone_recrute = mydata->nbr_neurone_recrute;
  /** **************************************************************** **/

  neurone[posmax].s2 = 1; // le max a son S2 a 1
  if (max > local_vigilence) // cas classique : envoie du max pour adaptation
  {
    mydata->neuron_max = posmax;
  }
  else /* if the max is lower than the vigilence on va tenter de recruter*/
  {
    if (nbr_neurone_recrute < nbre)
    {
      if (local_vigilence > 0.0) //si on est pas dans un cas ou vigilence = 0 (on ne veut pas recruter), on envoie un signal de recrutement.
      {
        n = deb + nbr_neurone_recrute;
        mydata->neuron_max = n;
      }
      else // Sinon, on envoi aucun signal de recrutement (Unique cas possible : Vigilence=0 et pas de neurone > 0.0, pas de maximum envoyé, pas de recrutement ni adaptation)
      {
        mydata->neuron_max = -1;
      }
    }
    else
    {
      //PRINT_WARNING("Plus de place pour recruter de nouveau\n"); : à remettre si necessaire de le visualiser, enlevé pour question de perfs
      mydata->neuron_max = posmax;
    }
  }

//reinitialisation des valeurs pour que le calculs du max inclus dans mise_a_jour soit juste.
  mydata->max = -9999999.;
  mydata->max_d = -9999999.;
  mydata->mmoy = 0.;
  mydata->nbr_neurone_recrute = 0;
  mydata->posmax = deb;
  mydata->norme = 0;

}
void destroy_Kmean_R(const int gpe)
{
  if (def_groupe[gpe].data != NULL)
  {
    free((data_kmeanr *) def_groupe[gpe].data);
    def_groupe[gpe].data = NULL;
  }
}
