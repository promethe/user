/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*------------------------------------------------------------------------*/
/* groupe de neurone utilisant une competition globale ou locale fixee    */
/* par la valeur de dvn (largeur inhibition                               */
/* lors de l'apprentissage les entrees sont copiees dans les poids        */
/* la selectivite des neurones augmente avec l'apprentissage              */
/*------------------------------------------------------------------------*/

#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
//extern int flag_moy_premier_tour_finished;
//int flag_moy_premier_tour_finished=0;

#define RT 0.875

/* seuil sur D barre a partir duquel on considere au'un neurone a appris
   quelque chose. En dessous de ce seuil le neurone est vierge de toute info apprise */


/* #define seuil_db_apprentissage 0.7 */ /* il faudrait l'asservir sur l'init dans leto des seuils des neurones... */


/* r represente le ratio entre l'activite globale en entree lors de
l'apprentissage et lors du test (r<1) et r=1 si cond identique a l'apprentissage*/

//#define DEBUG
//#define DEBUG_NAME
float fonction_de_sortie_selective(float x,int no_neurone,float r)
{
  float y,a;
  float sigma_f,dvp;
  int gpe;

  gpe=neurone[no_neurone].groupe;
  sigma_f=def_groupe[gpe].sigma_f;
  dvp=def_groupe[gpe].dvp;
  a=neurone[no_neurone].seuil;   			/* Dbarre */

  if(x>=1.) return(a);

  /*rintf("vig= %f , seuil a= %f ,sigma_f= %f\n",vigilence,a,sigma_f);*/
  y=a*r*exp( -(vigilence+0.01)*(1.-x/r)*(1.-x/r)/(sigma_f*(a-1.-0.01)*(a-1.-0.01)) );
  /*printf("x= %f, r = %f y= %f \n",x,r,y);*/

  return(y);
}


/*--------------------------------------------------------------------*/
typedef struct MyData_selective_winner{
	int gpe_land;
	int mode; /*0 : normal,  1 : Place_cell 2:  Land used*/
} MyData_selective_winner;

extern float calcule_produit(); /* se trouve dans macro_neurone.c */

void mise_a_jour_neurone_winner_selectif( int i , int gestion_STM,int learn)
 {
  type_coeff *coeff;
  float sortie,/*r,*/nbre_w,nbre_input,input,sum_input,a/*,sv*/;
  /*char c;*/
  float seuil_db_apprentissage;
  int gpe;
  MyData_selective_winner * my_data;
  int l=-1,i_liaison=0,j=0;
  int mode;
  int gpe_land;
  
  int MAX=5,NMAX,is_max,k;
  float max;
  type_coeff * coeff_max;
  type_coeff * coeff_used[5];
  type_coeff ** coeff_present;
  int neurone_land[5];
  if(def_groupe[neurone[i].groupe].data==NULL)
  {
  	my_data=(MyData_selective_winner*)malloc(sizeof(MyData_selective_winner));
	my_data->mode=0;
	mode=0;
	my_data->gpe_land=gpe_land=-1;
	l=find_input_link(neurone[i].groupe,i_liaison);
	while(l!=-1)
	{
		if(strcmp(liaison[l].nom,"PC")==0)
		{
			my_data->mode=1;
			mode=1;
		}
		if(strcmp(liaison[l].nom,"LAND")==0)
		{
			my_data->mode=2;
			mode=2;
			my_data->gpe_land=liaison[l].depart;
			gpe_land=my_data->gpe_land;
		}
		i_liaison++;
		l=find_input_link(neurone[i].groupe,i_liaison);
	
	}
	def_groupe[neurone[i].groupe].data=(MyData_selective_winner*)my_data;
	  	
  }
  else
  {
  	mode=((MyData_selective_winner*)(def_groupe[neurone[i].groupe].data))->mode;
  	gpe_land=((MyData_selective_winner*)(def_groupe[neurone[i].groupe].data))->gpe_land;
  }
#ifdef DEBUG_NAME
    printf("~~~~~~~~~~~~~~~~~~~~ entree dans %s(%d):mode=%d\n",__FUNCTION__,i,mode);
#endif
  sortie=0.;
  coeff=neurone[i].coeff;
  nbre_w=0.;sum_input=0.;nbre_input=0.;


  gpe=neurone[i].groupe;
  seuil_db_apprentissage=def_groupe[gpe].seuil;

  a=neurone[i].seuil;   			/* Dbarre */
#ifdef DEBUG
  printf("seuil neurone %d = %f\n", i-def_groupe[gpe].premier_ele,a);
 	#endif
  if(a<=seuil_db_apprentissage)
    {
      sortie=a*drand48();	// sa sortie est un nombre al�atoire multipli� par son activit�
      neurone[i].s=sortie;
#ifdef DEBUG
      printf(".....neurone non appris %d \n",i-def_groupe[gpe].premier_ele);
#endif
      return ;
    }
  #ifdef DEBUG
  printf("neurone  %d activite :... \n",i-def_groupe[gpe].premier_ele);
  #endif
    
  if(mode==0)
  {
	while(coeff!=nil)
	{
	#ifdef DEBUG
	printf("coef_val entre neurone %d de sel et neurone de sigma_pi %d = %f\n",i,coeff->entree,coeff->val);
	#endif
	if(coeff->val>0.1)
	{
		input=/*coeff->moy; */ neurone[coeff->entree].s1;
		nbre_input=nbre_input+1.;
		sortie=sortie+fabs(coeff->val-input);// somme des diff�rences entre les poids et les activit�s
		#ifdef DEBUG
		printf("w=%f i=%f \n",coeff->val,input);
		#endif
	}
	
	coeff=coeff->s;
	}
	if(nbre_input!=0)
		sortie=1.-sortie/nbre_input;
	else sortie=0.;   /*antibug car il sort des 'nan'*/
	neurone[i].s=sortie;
	
  }
  else if (mode==1)
  {
  	/*on cherche 5 max*/
  	MAX=5;
		
	/*Pour chaque max cherch� j */
   	for(j=0;j<MAX;j++)
	{
		/*la sortie est d'adord � z�ro*/
		sortie=0.;
		max=10;
		
		/*Pour chaque coef, quelque neurone donne le max*/
		while(coeff!=nil)
		{
			if(coeff->val>0.1)
			{	
				/*Supossons un coef qui serait le max si il n'est pas dej� utilis�*/
				is_max=1;
				for(k=0;k<j;k++)
				{
					if(coeff==coeff_used[k])
						is_max=0;
				}
				/*C'sst le max si il est sup�rieur au pr�cedent max*/
				if(is_max==1)
				{
					input=/*coeff->moy; */ neurone[coeff->entree].s1;
					if(fabs(coeff->val-input)<=max)
					{
						max=fabs(coeff->val-input);
						coeff_max=coeff;
					}
				}
				/*Et on regarde sur les autes neurones*/	
			}
			//printf("%d check : %f\n",i,(coeff->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3;	
			coeff=coeff->s;
		}
		/*Ici, le max a �t� trouv�, on le place dans le tableau pour pas le r�utiliser. On recommence pour le j�me max*/
		coeff_used[j]=coeff_max;
		coeff=neurone[i].coeff;
		
		/*if(neurone[i].groupe==32)*/
		{
		printf("%d:neurone used:%f\n",neurone[i].groupe,(float)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5));
		printf("act used:%f\n",neurone[coeff_max->entree].s1);
		}
		/*
		if(neurone[i].groupe==31)
		{
		printf("------%d:neurone used:%f\n",neurone[i].groupe,(float)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)));
		printf("------act used:%f\n",neurone[coeff_max->entree].s1);
		}*/
		
	}
	
	sortie=0;
	for(j=0;j<MAX;j++)
	{
	input=neurone[coeff_used[j]->entree].s1;
	sortie =sortie+fabs(coeff_used[j]->val-input);
	}
	sortie=1-sortie/MAX;
  }
  else if(mode==2)
  {
  
  	/*on cherche 5 max*/
  	MAX=5;
		
	/*Pour chaque max cherch� j */
   	for(j=0;j<MAX;j++)
	{
		/*la sortie est d'adord � z�ro*/
		sortie=0.;
		max=10;
		
		/*Pour chaque coef, quelque neurone donne le max*/
		while(coeff!=nil)
		{
			if(coeff->val>0.1)
			{	
				/*Supossons un coef qui serait le max si il n'est pas dej� utilis�*/
				is_max=1;
				for(k=0;k<j;k++)
				{
					if(coeff==coeff_used[k])
						is_max=0;
				}
				/*C'sst le max si il est sup�rieur au pr�cedent max*/
				if(is_max==1)
				{
					input=/*coeff->moy; */ neurone[coeff->entree].s1;
					if(fabs(coeff->val-input)<=max)
					{
						max=fabs(coeff->val-input);
						coeff_max=coeff;
					}
				}
				/*Et on regarde sur les autes neurones*/	
			}
			//printf("%d check : %f\n",i,(coeff->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3;	
			coeff=coeff->s;
		}
		/*Ici, le max a �t� trouv�, on le place dans le tableau pour pas le r�utiliser. On recommence pour le j�me max*/
		coeff_used[j]=coeff_max;
		coeff=neurone[i].coeff;
		
		if(neurone[i].groupe==32)
		{
		printf("%d:neurone used:%f\n",neurone[i].groupe,(float)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5));
		printf("act used:%f\n",neurone[coeff_max->entree].s1);
		printf("act land:%f\n",neurone[(int)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5)*3+2+def_groupe[gpe_land].premier_ele].s1);
		printf("diff angle:%f\n",neurone[coeff_max->entree].s1/neurone[(int)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5)*3+2+def_groupe[gpe_land].premier_ele].s1);
		}
		
		
		if(neurone[i].groupe==31)
		{
		printf("------%d:neurone used:%f\n",neurone[i].groupe,(float)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)));
		printf("------act used:%f\n",neurone[coeff_max->entree].s1);
		/*printf("act land:%f\n",neurone[(int) ((float) (((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)*3+2+def_groupe[gpe_land].premier_ele].s1);
		*/}
		/*
		printf("neurone used:%f\n",(float)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5));
		
		printf("neurone used:%f\n",neurone[((int)(((float)(((coeff_max->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3)-1)/5))*3+2].s);
	*/	
	}
	
	sortie=0;
	for(j=0;j<MAX;j++)
	{
	input=neurone[coeff_used[j]->entree].s1;
	sortie =sortie+fabs(coeff_used[j]->val-input);
	}
	sortie=1-sortie/MAX;
  
  /*
  	printf("mode222222\n");
  	MAX=5;
	j=0;
	while(coeff!=nil)
		{
			if(coeff->val>0.1)
			{	
				
				j++;
				
				
			}
			//printf("%d check : %f\n",i,(coeff->entree-def_groupe[neurone[coeff_max->entree].groupe].premier_ele)+1)/3;	
			coeff=coeff->s;
		}
	coeff_present=malloc(sizeof(type_coeff)*j);
		
	coeff=neurone[i].coeff;
	j=0;
	while(coeff!=nil)
		{
			if(coeff->val>0.1)
			{	
				coeff_present[j]=coeff;
				j++;
				
				
			}
		
			coeff=coeff->s;
		}
	int nb_neurone_fusion_used=j;
	int t,u;
	int neurone_max;
	
	for(j=0;j<MAX;j++)
	{
		max=-1;
		for(k=def_groupe[gpe_land].premier_ele;k<def_groupe[gpe_land].nbre+def_groupe[gpe_land].premier_ele;k++)
		{
			for(t=0;t<nb_neurone_fusion_used;t++)
			
				if(
					(k-def_groupe[gpe_land].premier_ele+1)
							==
					(int)(((float)(coeff_present[t]->entree-def_groupe[neurone[coeff_present[t]->entree].groupe].premier_ele)+1)/3/5)
				 )	
					if(neurone[k].s1>=max)
					{
						is_max=1;
						for(u=0;u<j;u++)
							if(t==neurone_land[u])
								is_max=0;
						if(is_max==1)
						{						
							max=neurone[k].s1;
							neurone_max=t;
						}
					}
		}
	
		neurone_land[j]=t;
	}
	printf("passage\n");	
	
	sortie=0;
	for(j=0;j<MAX;j++)
	{
	input=neurone[coeff_present[neurone_land[j]]->entree].s1;
	sortie =sortie+fabs(coeff_present[neurone_land[j]]->val-input);
	}
	sortie=1-sortie/MAX;
	free(coeff_present);*/	
  }
  else
  {
  	printf("error_mode\n");
	exit(0);
  }


#ifdef DEBUG
  printf(".....sortie brute du neurone %d  = %f <? vig= %f \n",i-def_groupe[gpe].premier_ele,sortie,vigilence);
#endif

  /*on conserve desormais la sortie*/
  
  
  if(sortie<vigilence) sortie=0.;
  neurone[i].s=sortie;
  
  
  

  #ifdef DEBUG
  printf(".....sortie brute du neurone %d apres etude vigilance s = %f \n",i-def_groupe[gpe].premier_ele,neurone[i].s);
  #endif

 }


/*--------------------------------------------------------------------*/

void apprend_coeff_winner_selectif(int n)
{
  type_coeff *coeff;
  /*float w;
  float somme_poids=0.;
  float nbre_poids=0.;*/

#ifdef DEBUG
  printf("apprend coeff winner selectif, neurone = %d \n",n);
#endif
  coeff=neurone[n].coeff;
  if(coeff==NULL) {printf("ERROR impossible learning - NO LINK !!! \n"); exit(0);}

  coeff=neurone[n].coeff;
  while(coeff!=nil)
    {
      if(coeff->evolution==1)                  /* si coeff modifiable */
	{
#ifdef DEBUG
	  printf("--\n");
	  printf("old val= %f \n", coeff->val);
	  printf("new val=�%f \n",coeff->moy);
#endif
	  coeff->val=coeff->moy;
/* 	  if(coeff->moy>0.1) w=coeff->val=coeff->moy; / entree / */
/* 	  else w=coeff->val=0.; */
	  /*somme_poids=somme_poids+w*w;*/
	}
      coeff=coeff->s;
    }

/*   if(somme_poids<=0.) return; / rien a normaliser ! (les poids ne peuvent etre negatifs) / */
/*   coeff=neurone[n].coeff; */
/*   while(coeff!=nil) */
/*     { */
/*       if(coeff->evolution==1)                  / si coeff modifiable / */
/* 	{ */
/* 	  coeff->val=coeff->val/somme_poids; / entree / */
/* 	} */
/*       coeff=coeff->s; */
/*     } */
}


/*--------------------------------------------------------------------*/

void apprend_winner_selectif(int gpe)
 {
  int i;
  int deb,nbre;
  /*float Si;*/

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;

#ifdef DEBUG_NAME
  printf("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n",__FUNCTION__,gpe);
#endif
  for(i=deb;i<deb+nbre;i++)       /* modifie les poids en consequence */
   {
     /* if(neurone[i].s2>0.99) printf("sortie s = %f , vig = %f \n",neurone[i].s,vigilence);*/
     if(neurone[i].flag==1)
       	apprend_coeff_winner_selectif(i);
/* seul le gagnant apprend s'il n'avait encore rien appris*/
       /* pas de modification des poids */
	 /*	 printf("apprentissage neurone %d \n",i);*/

   }
}




/*-------------------------------------------------------------------*/
/* au lieu de faire une competition sur une fenetre glissante (max_locaux classiques)
la competition se fait sur des zones figees. Cela permet de mieux controler
le nombre de max locaux et donc d'eviter les problemes de variation de la quantite
d'activation globale de la carte. Evite les problemes lies a l'ordre sequentiel d'analyse
des neurones sur la carte */

void recherche_max_locaux_figes(int gpe, float dvn)
/*int gpe;
float dvn;  */                            /* distance de competition */
{
  int i,n,echec,deb,nbre,nbre2;
  int increment;
  int deb2,fin2,posmax;
  float /*max,dc,Si,Sj,sp,*/xg,yg,zg,/*d,*/total;
  /*type_coeff *coeff;*/

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  nbre2=def_groupe[gpe].taillex*def_groupe[gpe].tailley;
  increment=nbre/nbre2;

#ifndef AVEUGLE
  /*printf("nbre = %d - nbre2 = %d - increment = %d\n",nbre,nbre2,increment);*/
#endif

  if(dvn > nbre/increment/2) dvn=nbre/increment/2;
  for(n=deb+increment-1+dvn*increment;n<deb+nbre;n=n+2*increment*dvn)
    {
      xg=neurone[n].posx;
      yg=neurone[n].posy;
      zg=neurone[n].posz;
      echec=0;
      total=neurone[n].s;
      posmax=n;

      /* cherche le max local au neurone n considere */

      deb2=n-increment*dvn;
      fin2=n+increment*dvn;
      if(deb2<deb) deb2=deb;
      if(fin2>deb+nbre) fin2=deb+nbre;

      for(i=deb2;i<fin2;i+=increment)
	{
	  if(neurone[i].s>total)
	    {
	      total=neurone[i].s; posmax=i;
	    }
	  neurone[i].s1=neurone[i].s2=0.;
	}

      if(total>0. ) {  neurone[posmax].s2=1.;  neurone[posmax].s1=neurone[posmax].s;
#ifndef AVEUGLE
	  /* printf("Neurone gagnant dans la carte: %d, seuil=%f\n", n, neurone[n].seuil);*/
#endif
	}
    }
}


/*-------------------------------------------------------------------*/
/* au lieu de faire une competition sur une fenetre glissante (max_locaux classiques)
la competition se fait sur des zones figees. Cela permet de mieux controler
le nombre de max locaux et donc d'eviter les problemes de variation de la quantite
d'activation globale de la carte. Evite les problemes lies a l'ordre sequentiel d'analyse
des neurones sur la carte */

/*On introduit en plus le fait que sur un voisinage on garde les N neurones les plus actifs
les autres sont mis a 0 */
/* un neurone nul ne peut etre un max => pas de mas si toutes les entrees sont nulles
   (car toutes les sorties sont nulles avec un produit W.E)*/

void recherche_max_locaux_figes2(int gpe, float dvn)
/*int gpe;
float dvn;  */                            /* distance de competition */
{
  int i=0,n,deb,nbre,nbre2,nmax;
  int increment;
  int deb2=0,fin2=0,posmax=-1,premier_posmax_nr=-1;
  float total=-1;//total pas explicite du tout mieux vaut l'appeler val_max


  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  nbre2=def_groupe[gpe].taillex*def_groupe[gpe].tailley;
  increment=nbre/nbre2;
#ifdef DEBUG_NAME
#ifndef AVEUGLE
  printf("~~~~~~~~~~~~~~~~~~~~ %s(%d)\n",__FUNCTION__,gpe);
  /*printf("nbre = %d - nbre2 = %d - increment = %d\n",nbre,nbre2,increment);*/
#endif
#endif
  
  if(dvn > nbre/increment/2) dvn=nbre/increment/2;// au maximum dvn �gal � la moiti� des neurones car une fen�tre
  						  //de comp�tion fait 2 dvn.
  for(n=deb+increment-1+dvn*increment;n<deb+nbre;n=n+2*increment*dvn)
    {
      deb2=n-increment*dvn;
      fin2=n+increment*dvn;
      if(deb2<deb) deb2=deb;
      if(fin2>deb+nbre) fin2=deb+nbre;
      total=-1.;
      posmax=-1.;
      premier_posmax_nr=-1;
      for(i=deb2;i<fin2;i+=increment)  /*reset des sorties et recherche du max */
	{ //fen�tre de comp�tion entre deb2 et fin2 avec deb2 et fin2 qui vont avancer de dvn � chaque fois

	  /* desormais on cherche le max dans le neurone non recrut�s (normal) ou dans le neurone recrut�s
	  	dont la sortie s est sup�rieur � la vigilence. Avant leur sorite valait 0 (dommage car on perd l'information de la sortie juste pour les eliminer de la recherche des max.), maintenant
		leur sortie est conserv� mais il sont exclu ici de la recherche des max.
	      Autre avantage: on sait quand il ne reste plus de neurone et que la vigilence impose d'en utiliser un (posmax=-1). Il faudra recoder (si learning mode) ou pas(use only) et mettre sa sortie � 1 pour signifier sa nouvel utilisation.   Avant mise � zero par mise � jour et la verification de total>0. empechait que les sorties sont mise � 1 ou s, le vaiqueur �tait le neurone 0.
		*/
	    neurone[i].flag=0;
	    neurone[i].s1=neurone[i].s2=0.;	// reset des sorties
	    /*recherche du max dans le bon ensemble*/
	    if( (neurone[i].seuil<=def_groupe[gpe].seuil))  /*neurone non recrute*/
	    {
	       if(premier_posmax_nr==-1)   /*est-ce le premier max non recrute max*/
	       		premier_posmax_nr=i;
	       if(neurone[i].s>total) /*est-ce le max*/
	       {
	       		total=neurone[i].s; posmax=i;
	       }
	    }
	    else if	(neurone[i].s>vigilence)                /*sortie (d'un neurone recrute) suffisament activ�*/
	    {
	      if(neurone[i].s>total)  /*est-ce le max*/
	       {
	       		total=neurone[i].s; posmax=i;
	       }

	    }


        }

	/*plus aucun neurone dispo et aucune reconnaissance ?? (=on n'a pas trouverde max )
	=> Il faut designer un vainqueur

		le meiux, ca serait celui qui a eu la plus basse activit� depuis toujours, mais je sais pas comment faire
		
		sinon je choisirais le plus actif en activit� instantann�e comme ca il recode sur le neurone le plus proche. Le ph�nom�ne simultan� peut se reproduire.....
		
		mais pour pas changer d'avant, je choisis le zero te en plus, 
		cependant il va recoder autre chose (pas comme avant).

		
		*/ 
      /*if(posmax==-1)
	{posmax=def_groupe[gpe].premier_ele; neurone[posmax].seuil=def_groupe[gpe].seuil;total=neurone[posmax].s; printf("RECODAGE du neurone 0");}*/
	
      if(posmax==-1)
	{
		
		for(i=def_groupe[gpe].premier_ele;i<=def_groupe[gpe].premier_ele+def_groupe[gpe].nbre;i++)
		{
			if(neurone[i].s>total)
	       		{     
				total=neurone[i].s; 
				posmax=i;
			}
		}
		premier_posmax_nr=posmax=def_groupe[gpe].premier_ele; neurone[posmax].seuil=def_groupe[gpe].seuil;
		printf("RECODAGE du neurone %d\n",posmax-def_groupe[gpe].premier_ele);
	}

       /*si on  recrute alors on prend le premier*/
      if(neurone[posmax].seuil<=def_groupe[gpe].seuil)
      	posmax=premier_posmax_nr;

      if(total>RT ) {  neurone[posmax].s2=1 ; neurone[posmax].s1=1.0/*total*/ ; } // si le max est sup�rieur � 0 alors le neurone max est mis � 1

      if(neurone[posmax].seuil<=def_groupe[gpe].seuil)
       /* seul le gagnant apprend s'il n'avait encore rien appris*/
       /* pas de modification des poids */
       {
	 /*	 printf("apprentissage neurone %d \n",i);*/
	 neurone[posmax].s1=neurone[posmax].s=1;
	 neurone[posmax].flag=1;
	 neurone[posmax].seuil= 0.95 ;
#ifdef DEBUG
	 printf("!!!!!le neurone %d va apprendre \n",posmax-deb); /* Dbarre, neurone tres selectif */
#endif
       }
#ifdef DEBUG
      printf("\n    apres compet, gagnant: %d --  s = %f -- s1 = %f -- s2= 1 \n ========== \n",posmax-deb,neurone[posmax].s,neurone[posmax].s1);


      printf("neurone %d , activite max = %f , activit� de s2 = %f \n",posmax,total,neurone[posmax].s2);
      printf("nbr de 1 pour selective_winner: %d \n",(int)def_groupe[gpe].nbre_de_1);
      #endif


      for(nmax=1;nmax<def_groupe[gpe].nbre_de_1;nmax++) // recherche des max suivants nbre_de_1 donne le nbre de max suivant que
      							//l'on veut on peut le changer dans le script
	{
	  total=-1;
	  posmax=-1.;
	  /* cherche le max local au neurone n considere */
	  for(i=deb2;i<fin2;i+=increment)
	    {
	      if(neurone[i].s>total && neurone[i].s2==0.) // test sur .s2 si c'est � z�ro c'est que ce n'est pas un max que l'on a d�j�
	      						  // s�lectionn� donc on peut le selectionner
		{
		  total=neurone[i].s; posmax=i;
		}
	    }

	  if(total>RT)
	    {  neurone[posmax].s2=neurone[posmax].s1=1.0;
#ifdef DEBUG
#ifndef AVEUGLE
	printf("le %d gagnant vaut %f \n",posmax-def_groupe[gpe].premier_ele, total);
#endif
#endif
	     }
	  else break; /* pas la peine de rechercher les autres max, ils sont nuls */
	} /* fin de la recherche du nieme max */  /*Avec la nouvelle m�thode, ca n'arrive jamais.*/
    }
}

/* le max (Smax) d'un cluster est mis a 1 et les autres a (S_i/Smax)^n */
/* n un entier grand... */

void recherche_max_locaux_souples(int gpe, float dvn)
/*int gpe;
float dvn;  */                            /* distance de competition */
{
  int i,n,echec,deb,nbre,nbre2;
  int increment;
  int deb2,fin2,posmax;
  float /*max,dc,Si,Sj,sp,*/xg,yg,zg/*,d*/,total,v;
  /*type_coeff *coeff;*/

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  nbre2=def_groupe[gpe].taillex*def_groupe[gpe].tailley;
  increment=nbre/nbre2;

#ifndef AVEUGLE
  /*printf("nbre = %d - nbre2 = %d - increment = %d\n",nbre,nbre2,increment);*/
#endif

  if(dvn > nbre/increment/2) dvn=nbre/increment/2;
  for(n=deb+increment-1+dvn*increment;n<deb+nbre;n=n+2*increment*dvn)
    {
      xg=neurone[n].posx;
      yg=neurone[n].posy;
      zg=neurone[n].posz;
      echec=0;
      total=neurone[n].s;
      posmax=n;

      /* cherche le max local au neurone n considere */

      deb2=n-increment*dvn;
      fin2=n+increment*dvn;
      if(deb2<deb) deb2=deb;
      if(fin2>deb+nbre) fin2=deb+nbre;

      for(i=deb2;i<fin2;i+=increment)
	{
	  if(neurone[i].s>total)
	    {
	      total=neurone[i].s; posmax=i;
	    }
	  neurone[i].s1=neurone[i].s2=0.;
	}

      if(total>0 )
	{
	  for(i=deb2;i<fin2;i+=increment)
	    {
	      v=pow(neurone[i].s/total,10.);
	      if(v>0.01)  neurone[i].s2=neurone[i].s1=v;
	      else neurone[i].s2=neurone[i].s1=0.;

#ifndef AVEUGLE
	  /* printf("Neurone gagnant dans la carte: %d, seuil=%f\n", n, neurone[n].seuil);*/
#endif
	    }
	}
    }
}

/*------------------------------------------------------*/
void recherche_max_locaux_sigma_pi(int gpe)
 {
   float  dvn;/*distance d'inhibition */

   /*printf("recherche max locaux sigma-pi \n");*/
   dvn=(float)def_groupe[gpe].dvn;
   /* recherche_max_locaux_figes(gpe,dvn);*/
   /*recherche_max_locaux_souples(gpe,dvn);*/
 recherche_max_locaux_figes2(gpe,dvn);
   /* dans recherche max s1 passe a 1 si on detecte que le neurone doit apprendre */

 }
