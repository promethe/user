/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*------------------------------------------------------------------------*/
/* groupe de neurone utilisant une competition globale ou locale fixee    */
/* par la valeur de dvn (largeur inhibition                               */
/* lors de l'apprentissage les entrees sont copiees dans les poids        */
/* la selectivite des neurones augmente avec l'apprentissage              */
/*------------------------------------------------------------------------*/

/* #define DEBUG */

#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>
#include <string.h>
#include <libhardware.h>
#include <net_message_debug_dist.h>  /* sockets I/O du kernel */


/* r represente le ratio entre l'activite globale en entree lors de
   l'apprentissage et lors du test (r<1) et r=1 si cond identique a
   l'apprentissage */

#define RT 0.0001
#define RT_input 0.0001


void apprend_winner_selectif(int gpe);
void apprend_winner_selectif_env_simul(int gpe);
void mise_a_jour_neurone_winner_selectif_hedonist(int i, int gestion_STM,int learn);
extern float calcule_produit(void); /* se trouve dans macro_neurone.c */
void mise_a_jour_neurone_winner_selectif(int i, int gestion_STM, int learn);

/*--------------------------------------------------------------------*/
typedef struct MyData_selective_winner
{
   int gpe_vigilence;
   float rhoL;
} MyData_selective_winner;


void new_selective_winner(int gpe)
{
   int l, i_liaison = 0;
   int gpe_vigilence = -1;
   int simul = 0;
   float rhoL = -1.;
   char param[255];
   MyData_selective_winner *my_data;

   l = find_input_link(gpe, i_liaison);
   while (l != -1)
   {
      if (prom_getopt(liaison[l].nom, "r",param) == 2)
      {
         rhoL = atof(param);
         def_groupe[gpe].appel_activite = mise_a_jour_neurone_winner_selectif_hedonist;
      }

      if (strcmp(liaison[l].nom, "PC") == 0)
      {
         def_groupe[gpe].appel_activite = mise_a_jour_neurone_winner_selectif_hedonist;
         rhoL=0.33;
      }

      if (strcmp(liaison[l].nom, "vigilence") == 0)
      {
         gpe_vigilence = liaison[l].depart;
      }

      /* Modif Julien Hirel, 15/05/2008 Ajout de la possibilite
      d'utiliser le selective winner en simul explicitement (si
      on a une simulation en multiscript et que le robot est
      initialise dans un autre script par exemple)*/
      if (strcmp(liaison[l].nom, "simul") == 0)
      {
         simul = 1;
      }

      i_liaison++;
      l = find_input_link(gpe, i_liaison);
   }

   if (simul == 1)
      def_groupe[gpe].appel_apprend = apprend_winner_selectif_env_simul;
   else
      def_groupe[gpe].appel_apprend = apprend_winner_selectif;

   my_data = (MyData_selective_winner *) malloc(sizeof(MyData_selective_winner));
   my_data->gpe_vigilence = gpe_vigilence;

   dprints("rhoL=%f in %s\n",rhoL,def_groupe[gpe].no_name);

   if (rhoL <= 1.)
      my_data->rhoL = rhoL;
   else
   {
      PRINT_WARNING("rhoL (paramter -r) must be less than or equal to 1, setting to 1");
      my_data->rhoL = 1;
   }
   def_groupe[gpe].data = (MyData_selective_winner *) my_data;
}

/* a quoi est ce que ca correspond ??? PG */
#define NBRE_MAX_INPUT 1000
void mise_a_jour_neurone_winner_selectif_hedonist(int i, int gestion_STM, int learn) /* i no du neurone mis a jour */
{
   type_coeff *coeff;
   float sortie, input, a;
   int nbre_input;
   float seuil_db_apprentissage;
   int gpe;
   MyData_selective_winner *my_data;
   int j = 0;

   int is_max, k;
   float max;
   type_coeff *coeff_max = NULL;
   type_coeff *coeff_used[NBRE_MAX_INPUT];

   float local_vigilence = vigilence;
   float RHOL;
   int rholXinput;
   
   (void) gestion_STM; (void) learn;
   
   my_data = (MyData_selective_winner *) def_groupe[neurone[i].groupe].data;

   if (my_data->gpe_vigilence != -1) local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

   dprints("~~~~~~~~~~~~~~~~~~~~ entree dans %s(%d)\n", __FUNCTION__, i);

   sortie = 0.;
   coeff = neurone[i].coeff;
   nbre_input = 0;

   gpe = neurone[i].groupe;
   seuil_db_apprentissage = def_groupe[gpe].seuil;

   a = neurone[i].seuil;       /* Dbarre */

   dprints("seuil neurone %d = %f\n", i - def_groupe[gpe].premier_ele, a);

   if (a <= seuil_db_apprentissage)
   {
      /* sa sortie est un nombre aleatoire multiplie par son
       * activite */
      neurone[i].s = a * drand48();
      return;
   }

   coeff = neurone[i].coeff;
   nbre_input = 0;
   while (coeff != NULL)
   {
      if (coeff->val > 1 - RT_input)
      {
         /*Supposons un coeff qui serait le max si il n'est pas
          * deja utilise */
         nbre_input++;
      }
      coeff = coeff->s;
   }

   dprints("nbre_coef_appris:%f\n", nbre_input);

   if (nbre_input > NBRE_MAX_INPUT)
   {
      EXIT_ON_ERROR("erreur memoire: nbre entrees > nbre max d'entrees");
   }

   coeff = neurone[i].coeff;
   /*Pour chaque max chercher j */
   for (j = 0; j < nbre_input; j++)
   {
      /*la sortie est d'adord reinitialisee  */
      sortie = 0.;
      max = 10.;
      /*Pour chaque coef, quelque neurone donne le max */
      while (coeff != NULL)
      {
         if (coeff->val > 1- RT_input)
         {
            /* Supposons un coef qui serait le max si il n'est pas
             * deja utilise */
            is_max = 1;
            for (k = 0; k < j; k++)
            {
               if (coeff == coeff_used[k])
                  is_max = 0;
            }

            /* C'est le max si il est superieur au precedent
             * max */
            if (is_max == 1)
            {
               input = neurone[coeff->entree].s1;
               if (fabs(coeff->val - input) <= max)
               {
                  max = fabs(coeff->val - input);
                  coeff_max = coeff;
               }
            }
            /* Et on regarde sur les autres neurones */
         }
         coeff = coeff->s;
      }

      /* Ici, le max a ete trouve on le place dans le tableau pour
       * pas le reutiliser */
      coeff_used[j] = coeff_max;
      /* on repart du 1er coeff du neurone */
      coeff = neurone[i].coeff;
   }

   RHOL = my_data->rhoL;

   rholXinput = (int) (RHOL * nbre_input);
   dprints("rholxinput %d\n", rholXinput);
   sortie = 0;
   for (j = 0; j < rholXinput; j++)
   {
      input = neurone[coeff_used[j]->entree].s1;
      sortie = sortie + fabs(coeff_used[j]->val - input);
   }

   dprints("nb_coeff_used:%d\n", nb_coeff_used);

   if ( rholXinput == 0) EXIT_ON_ERROR("division par 0 : pas de poids égaux à 1");

   sortie = 1. - sortie / (float)rholXinput;

   dprints(".....sortie brute du neurone %d  = %f <? vig= %f \n", i - def_groupe[gpe].premier_ele, sortie, local_vigilence);

   if (sortie < local_vigilence) sortie = 0.;
   neurone[i].s = sortie;

   dprints(".....sortie brute du neurone %d apres etude vigilance s = %f \n",i - def_groupe[gpe].premier_ele, neurone[i].s);
}


void mise_a_jour_neurone_winner_selectif(int i, int gestion_STM, int learn)
{
   type_coeff *coeff;
   float sortie, input, a;
   int nbre_input;
   float seuil_db_apprentissage;
   int gpe;
   MyData_selective_winner *my_data;
   float local_vigilence = vigilence;

   (void) gestion_STM; (void) learn;

   my_data = (MyData_selective_winner *) def_groupe[neurone[i].groupe].data;

   if (my_data->gpe_vigilence != -1)  local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

   dprints(" ================= entree dans %s(%d)\n", __FUNCTION__, i);

   sortie = 0.;
   coeff = neurone[i].coeff;

   nbre_input = 0;

   gpe = neurone[i].groupe;
   seuil_db_apprentissage = def_groupe[gpe].seuil;

   a = neurone[i].seuil;       /* Dbarre */

   dprints("seuil neurone %d = %f\n", i - def_groupe[gpe].premier_ele, a);

   if (a <= seuil_db_apprentissage)
   {
      /* sa sortie est un nombre aleatoire multiplie par son
       * activite */
      sortie = a * drand48();
      neurone[i].s = sortie;
      dprints(".....neurone non appris %d \n", i - def_groupe[gpe].premier_ele);
      return;
   }

   dprints("neurone  %d activite :... \n", i - def_groupe[gpe].premier_ele);

   while (coeff != NULL)
   {
      //   dprints("coef_val entre neurone %d de sel et neurone de sigma_pi %d = %f\n", i, coeff->entree, coeff->val);

      if (coeff->val > 0.0001)
      {
         input = neurone[coeff->entree].s1;
         nbre_input++;
         /* somme des differences entre les poids et les
         * activites */
         sortie = sortie + fabs(coeff->val - input);
         //    dprints("w=%f i=%f \n", coeff->val, input);
      }

      coeff = coeff->s;
   }

   if (nbre_input != 0) sortie = 1. - sortie / nbre_input;
   else                 sortie = 0.;

   if (sortie < local_vigilence)  sortie = 0.;

   neurone[i].s = sortie;
   dprints(".....sortie brute du neurone %d  = %f <? vig= %f \n", i - def_groupe[gpe].premier_ele, sortie, local_vigilence);
}


/*-----------------------------------------------------------------*/
void apprend_coeff_winner_selectif(int n)
{
   type_coeff *coeff;

   neurone[n].seuil = 0.95;
   coeff = neurone[n].coeff;

   if (coeff == NULL)
   {
      EXIT_ON_ERROR("impossible learning - NO LINK !!!");
   }

   coeff = neurone[n].coeff;
   while (coeff != NULL)
   {
      if (coeff->evolution == 1)  /* si coeff modifiable */
      {
         dprints("old val= %f \n", coeff->val);
         dprints("new val=%f \n", neurone[coeff->entree].s1);

         coeff->val = neurone[coeff->entree].s1;
      }
      coeff = coeff->s;
   }
}


void apprend_coeff_winner_selectif_env_simul(int n)
{
   type_coeff *coeff;

   neurone[n].seuil = 0.95;
   coeff = neurone[n].coeff;

   if (coeff == NULL)
   {
      EXIT_ON_ERROR("impossible learning - NO LINK !!!");
   }

   coeff = neurone[n].coeff;
   while (coeff != NULL)
   {
      if (coeff->evolution == 1)  /* si coeff modifiable */
      {
         dprints("old val= %f \n", coeff->val);
         dprints("new val=%f \n", neurone[coeff->entree].s1);

         if (neurone[coeff->entree].s1 > 0.98)
            coeff->val = neurone[coeff->entree].s1;
         else
            coeff->val = 0.;
      }
      coeff = coeff->s;
   }
}


/*------------------------------------------------------------------*/
void apprend_winner_selectif(int gpe)
{
   int i;
   int deb, nbre;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   dprints("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n", __FUNCTION__, gpe);

   for (i = deb; i < deb + nbre; i++)
   {
      /* seul le gagnant apprend s'il n'avait encore rien appris */
      if (neurone[i].flag == 1)
         apprend_coeff_winner_selectif(i);
   }
}


void apprend_winner_selectif_env_simul(int gpe)
{
   int i;
   int deb, nbre;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   dprints("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n", __FUNCTION__, gpe);

   for (i = deb; i < deb + nbre; i++)
   {
      /* seul le gagnant apprend s'il n'avait encore rien appris */
      if (neurone[i].flag == 1)
         apprend_coeff_winner_selectif_env_simul(i);
   }
}



/*------------------------------------------------------------------*/
/* au lieu de faire une competition sur une fenetre glissante
   (max_locaux classiques) la competition se fait sur des zones
   figees. Cela permet de mieux controler le nombre de max locaux et
   donc d'eviter les problemes de variation de la quantite
   d'activation globale de la carte. Evite les problemes lies a
   l'ordre sequentiel d'analyse des neurones sur la carte */

void recherche_max_locaux_figes(int gpe, float dvn)
{
   int i, n, deb, nbre, nbre2;
   int increment;
   int deb2, fin2, posmax;
   float  total;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;

   if (dvn > nbre / increment / 2)  dvn = nbre / increment / 2;

   for (n = deb + increment - 1 + dvn * increment; n < deb + nbre; n = n + 2 * increment * dvn)
   {
      total = neurone[n].s;
      posmax = n;

      /* cherche le max local au neurone n considere */
      deb2 = n - increment * dvn;
      fin2 = n + increment * dvn;
      if (deb2 < deb)          deb2 = deb;
      if (fin2 > deb + nbre)  fin2 = deb + nbre;

      for (i = deb2; i < fin2; i += increment)
      {
         if (neurone[i].s > total)
         {
            total = neurone[i].s;
            posmax = i;
         }
         neurone[i].s1 = neurone[i].s2 = 0.;
      }

      if (total > 0)
      {
         neurone[posmax].s2 = 1.;
         neurone[posmax].s1 = neurone[posmax].s;
      }
   }
}


/*------------------------------------------------------------------*/
/* au lieu de faire une competition sur une fenetre glissante
   (max_locaux classiques) la competition se fait sur des zones
   figees. Cela permet de mieux controler le nombre de max locaux et
   donc d'eviter les problemes de variation de la quantite
   d'activation globale de la carte. Evite les problemes lies a
   l'ordre sequentiel d'analyse des neurones sur la carte */

/*On introduit en plus le fait que sur un voisinage on garde les N
  neurones les plus actifs les autres sont mis a 0 */
/* un neurone nul ne peut etre un max => pas de mas si toutes les
   entrees sont nulles (car toutes les sorties sont nulles avec un
   produit W.E)*/

void recherche_max_locaux_figes2(int gpe, float dvn)
{
   int i = 0, deb, nbre, nbre2, nmax;
   int increment;
   int deb2 = 0, fin2 = 0, posmax = -1, premier_posmax_nr = -1;
   float val_max = -1;
   float local_vigilence = vigilence;
   MyData_selective_winner *my_data;

   my_data = (MyData_selective_winner *) def_groupe[gpe].data;

   if (my_data->gpe_vigilence != -1)
      local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;

  (void)dvn;

   deb2 = deb;
   fin2 = deb + nbre;
   val_max = -9999999.;
   posmax = -1.;
   premier_posmax_nr = -1;

   for (i = deb2; i < fin2; i += increment)
   {
      /*fenetre de competition entre deb2 et fin2 avec deb2 et fin2
       * qui vont avancer de dvn a chaque fois */

      /* desormais on cherche le max dans le neurone non recrute
      (normal) ou dans le neurone recrute dont la sortie s est
      superieur a la vigilence. Avant leur sorite valait 0
      (dommage car on perd l'information de la sortie juste pour
      les eliminer de la recherche des max.), maintenant leur
      sortie est conserve mais il sont exclu ici de la recherche
      des max.  Autre avantage: on sait quand il ne reste plus de
      neurone et que la vigilence impose d'en utiliser un
      (posmax=-1). Il faudra recoder (si learning mode) ou
      pas(use only) et mettre sa sortie a 1 pour signifier sa
      nouvel utilisation.  Avant mise a zero par mise a jour et
      la verification de val_max>0. empechait que les sorties
      sont mise a 1 ou s, le vainqueur etait le neurone 0. */

      neurone[i].flag = 0;
      /* reset des sorties */
      neurone[i].s1 = neurone[i].s2 = 0.;

      /* recherche du max dans le bon ensemble */
      if (neurone[i].seuil <= def_groupe[gpe].seuil)    /*neurone non recrute */
      {
         if (premier_posmax_nr == -1)    /* est-ce le premier max non recrute max */
            premier_posmax_nr = i;
         if (neurone[i].s > val_max)   /* est-ce le max */
         {
            val_max = neurone[i].s;
            posmax = i;
         }
      }
      else if (neurone[i].s > local_vigilence)    /* sortie (d'un neurone recrute) suffisament activee */
      {
         if (neurone[i].s > val_max)   /*est-ce le max */
         {
            val_max = neurone[i].s;
            posmax = i;
         }

      }
   }

   /* plus aucun neurone dispo et aucune reconnaissance ?? (=on n'a
      pas trouverde max ) => Il faut designer un vainqueur

      le meiux, ca serait celui qui a eu la plus basse activite depuis
      toujours, mais je sais pas comment faire

      sinon je choisirais le plus actif en activite instantannee comme
      ca il recode sur le neurone le plus proche. Le phenomene
      simultane peut se reproduire.....

      mais pour pas changer d'avant, je choisis le zero te en plus,
      cependant il va recoder autre chose (pas comme avant). */

   if (posmax == -1)
   {
      for (i = def_groupe[gpe].premier_ele; i <= def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
      {
         if (neurone[i].s > val_max)
         {
            val_max = neurone[i].s;
            posmax = i;
         }
      }

      premier_posmax_nr = posmax = def_groupe[gpe].premier_ele;
      neurone[posmax].seuil = def_groupe[gpe].seuil;
      EXIT_ON_ERROR("RECODAGE du neurone %d gpe = %s \n", posmax - def_groupe[gpe].premier_ele,def_groupe[gpe].no_name);
   }
   else if (neurone[posmax].seuil <= def_groupe[gpe].seuil)         /*si on recrute alors on prend le premier */
      posmax = premier_posmax_nr;


   if (neurone[posmax].s > RT)
   {
      /* si le max est > 0 alors le neurone max est mis a 1 */
      neurone[posmax].s2 = 1.;
      neurone[posmax].s1 = neurone[posmax].s;
   }

   if (neurone[posmax].seuil <= def_groupe[gpe].seuil && local_vigilence > def_groupe[gpe].seuil)
      /* seul le gagnant apprend s'il n'avait encore rien appris */
      /* pas de modification des poids */
   {
      if (global_learn == 1)
      {
         neurone[posmax].s1 = neurone[posmax].s=neurone[posmax].s2=1.;
         neurone[posmax].flag = 1;
         dprints("!!!!!le neurone %d va apprendre \n", posmax - deb);
      }
   }

   dprints("apres compet, gagnant: %d --  s = %f -- s1 = %f -- s2= 1 \n ========== \n", posmax - deb, neurone[posmax].s, neurone[posmax].s1);
   dprints("neurone %d , activite max = %f , activite de s2 = %f \n", posmax, val_max, neurone[posmax].s2);
   dprints("nbr de 1 pour selective_winner: %d \n", (int) def_groupe[gpe].nbre_de_1);

   /* recherche des max suivants nbre_de_1 donne le nbre de max */
   if((int)def_groupe[gpe].nbre_de_1 == nbre2)
      for (i = deb2; i < fin2; i += increment)
         neurone[i].s2 = neurone[i].s1 = neurone[i].s;
   else
      for (nmax = 1; nmax < def_groupe[gpe].nbre_de_1; nmax++)
      {
         val_max = -1;
         posmax = -1.;
         /* cherche le max local au neurone n considere */
         for (i = deb2; i < fin2; i += increment)
         {
            /* test sur .s2 si c'est a zero c'est que ce n'est pas un
             * max que l'on a deja selectionne donc on peut le
             * selectionner */
            if (neurone[i].s > val_max && neurone[i].s2 <= 0.)
            {
               val_max = neurone[i].s;
               posmax = i;
            }
         }

         if (val_max > RT)
         {
            neurone[posmax].s2 = neurone[posmax].s1 = val_max;
            dprints("le %d gagnant, num %d vaut %f \n", nmax + 1,posmax , val_max);
         }
         else
         {
            /* pas la peine de rechercher les autres max, ils sont
             * nuls */
            break;
         }
      }
      /* fin de la recherche du nieme max */
      /*Avec la nouvelle methode, ca n'arrive jamais. */
}


/* le max (Smax) d'un cluster est mis a 1 et les autres a
 * (S_i/Smax)^n avec n un entier grand... */
void recherche_max_locaux_souples(int gpe, float dvn)
{
   int i, n, deb, nbre, nbre2;
   int increment;
   int deb2, fin2;
   float  total, v;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;

   if (dvn > nbre / increment / 2)
      dvn = nbre / increment / 2;

   for (n = deb + increment - 1 + dvn * increment; n < deb + nbre; n = n + 2 * increment * dvn)
   {
      total = neurone[n].s;

      /* cherche le max local au neurone n considere */
      deb2 = n - increment * dvn;
      fin2 = n + increment * dvn;

      if (deb2 < deb) deb2 = deb;

      if (fin2 > deb + nbre) fin2 = deb + nbre;

      for (i = deb2; i < fin2; i += increment)
      {
         if (neurone[i].s > total)
         {
            total = neurone[i].s;
         }
         neurone[i].s1 = neurone[i].s2 = 0.;
      }

      if (total > 0)
      {
         for (i = deb2; i < fin2; i += increment)
         {
            v = pow(neurone[i].s / total, 10.);
            if (v > 0.01)  neurone[i].s2 = neurone[i].s1 = v;
            else           neurone[i].s2 = neurone[i].s1 = 0.;
         }
      }
   }
}


/*------------------------------------------------------*/
void recherche_max_locaux_sigma_pi(int gpe)
{
   /* distance d'inhibition */
   float dvn;

   dvn = (float) def_groupe[gpe].dvn;
   /* recherche_max_locaux_figes(gpe,dvn); */
   /* recherche_max_locaux_souples(gpe,dvn); */
   recherche_max_locaux_figes2(gpe, dvn);
}
