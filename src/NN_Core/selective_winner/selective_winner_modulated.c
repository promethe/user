/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*------------------------------------------------------------------------*/
/* groupe de neurone utilisant une competition globale ou locale fixee    */
/* par la valeur de dvn (largeur inhibition                               */
/* lors de l'apprentissage les entrees sont copiees dans les poids        */
/* la selectivite des neurones augmente avec l'apprentissage              */
/*------------------------------------------------------------------------*/

//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/trouver_entree.h>
#include <string.h>
#include <libhardware.h>
#include <net_message_debug_dist.h>  /* sockets I/O du kernel */


void recherche_max_locaux(int gpe, float dvn);                                                                                                                                                                                                                                                                                                                                                                                 /* #define seuil_db_apprentissage 0.7 */

/*************************************************************************************
 *
 * 2 voies
 * MODE 1 : learning (categorisation)
 * MODE 4 : modulation (context)
 *
 */

/* r represente le ratio entre l'activite globale en entree lors de
l'apprentissage et lors du test (r<1) et r=1 si cond identique a l'apprentissage*/

//#define DEBUGtt
//#define DEBUG_NAME
#define SEUIL_ACT_CONTEXT 0.75
#define ALPHA 0.002
#define BETA 0.2
#define RT 0.0001


/*#define MESURE*/

void apprend_winner_selectif_modulated(int gpe);
void apprend_winner_selectif_modulated_env_simul(int gpe);
void mise_a_jour_neurone_winner_selectif_modulated_macroneuron_hedonist(int n, int gestion_STM, int learn);

void apprend_coeff_context(int n,int macro);

/*--------------------------------------------------------------------*/
typedef struct MyData_selective_winner_modulated
{
   int gpe_vigilence;
   int mod_offset; /* index offset pour microneurone de la voie du context (decalage par rapport au macroneurone) */
   int learn_offset; /* index offset pour microneurone de la voie de categorisation (decalage par rapport au macroneurone) */
   float rhoL;
#ifdef MESURE
   FILE * f;
   int counter;
#endif
} MyData_selective_winner_modulated;


void new_winner_selectif_modulated(int gpe)
{
   type_coeff *coeff;
   int l,i,mode,link, i_liaison = 0;
   int gpe_vigilence = -1;
   MyData_selective_winner_modulated *my_data;
   int debutGpe, nbreTotalGpe, incrementGpe;
   float rhoL = -1.;
   char param[255];

   debutGpe= def_groupe[gpe].premier_ele;
   nbreTotalGpe = def_groupe[gpe].nbre;
   /* Pas d'incrementation des macro-neurones */
   incrementGpe = nbreTotalGpe / (def_groupe[gpe].taillex*def_groupe[gpe].tailley);

   l = find_input_link(gpe, i_liaison);
   while (l != -1)
   {
      if (prom_getopt(liaison[l].nom, "r",param) == 2)
      {
         rhoL = atof(param);
         def_groupe[gpe].appel_activite = mise_a_jour_neurone_winner_selectif_modulated_macroneuron_hedonist;
      }

      if (strcmp(liaison[l].nom, "vigilence") == 0)
      {
         gpe_vigilence = liaison[l].depart;
      }

      i_liaison++;
      l = find_input_link(gpe, i_liaison);
   }

   my_data =ALLOCATION(MyData_selective_winner_modulated);
   my_data->gpe_vigilence = gpe_vigilence;
   my_data->mod_offset=my_data->learn_offset=-1;
   /*recherche des indices respectif des differente voies par rapport a celui de leur macroneurone*/
   for (i=0; i<incrementGpe; i++)
   {
      coeff = neurone[debutGpe+i].coeff;
      link=coeff->gpe_liaison ;
      mode=liaison[link].mode;
      /*recherche la voie de neuromodulation*/
      if (mode==DISTAL_HEBBIAN)
         my_data->mod_offset=incrementGpe-i-1;
      /*recherche la voie d'apprentissage*/
      if (mode==No_mode_link_distance_compet)
         my_data->learn_offset=incrementGpe-i-1;
      dprints("mode:%d, inc: %d, i:%d learn_offset:%d , mod_offset: %d\n",mode,incrementGpe,i, my_data->learn_offset,my_data->mod_offset);
   }

   if (rhoL <= 1.)
      my_data->rhoL = rhoL;
   else
   {
      PRINT_WARNING("rhoL (paramter -r) must be less than or equal to 1, setting to 1");
      my_data->rhoL = 1;
   }

#ifdef MESURE
   my_data->f=fopen("context_mesure0.m","w");
   fdprints(my_data->f, "%s","b=[\n	");
   fclose(my_data->f);
   my_data->counter=0;
#endif
   def_groupe[gpe].data = (MyData_selective_winner_modulated *) my_data;

}


void mise_a_jour_neurone_winner_selectif_modulated_macroneuron(int n, int gestion_STM, int learn)
{
   int gpe ;
   MyData_selective_winner_modulated *my_data;
   float local_vigilence = vigilence;
   type_coeff *coeff;
   float sortie, a ;
   int nbre_input;

   (void) gestion_STM; (void) learn;
   gpe = neurone[n].groupe ;
   /*recupere l'info de vigilence si non global mais representee par un groupe*/
   my_data = ((MyData_selective_winner_modulated *) (def_groupe[gpe].data));
   if (my_data->gpe_vigilence != -1)
      local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

   /* Check context activation */
   sortie = 0. ;
   nbre_input = 0;
   coeff = neurone[n-my_data->mod_offset].coeff;
   while (coeff != NULL)
   {
      if (coeff->val*neurone[coeff->entree].s1>0.0001)
      {
         //if(local_vigilence < .5) //printf("+++ (neuron %d) weight = %f, input =%f\n", n ,coeff->val, neurone[coeff->entree].s1) ;
         sortie += coeff->val*neurone[coeff->entree].s1 ;
         nbre_input++;
      }
      coeff = coeff->s;
   }

   /* Get input pattern activation for context activated neurons */
   if(nbre_input>0)
   {
      neurone[n].d = neurone[n-my_data->mod_offset].s = sortie/(float)nbre_input ;
      sortie = 0. ;
      nbre_input = 0;
      coeff = neurone[n-my_data->learn_offset].coeff;
      a = neurone[n-my_data->learn_offset].seuil;
      if (a <=def_groupe[gpe].seuil)
      {
         neurone[n].s = neurone[n-my_data->learn_offset].s = a * drand48();
      }
      else
      {

#ifdef MESURE
         /*incremente le compteur de charge calcul*/
         my_data->counter++;
#endif
         while (coeff != NULL)
         {
            if (coeff->val > 0.0001)
            {
               nbre_input++ ;
               sortie += fabs(coeff->val - neurone[coeff->entree].s1);
            }
            coeff = coeff->s;
         }
         if (nbre_input > 0){
            sortie = 1. - sortie / (float)nbre_input;
            //if(local_vigilence < .5) //printf("*** (neuron %d) micro_mod=%f, micro_learn=%f, macro=%f\n", n, neurone[n-my_data->mod_offset].s, sortie, neurone[n-my_data->mod_offset].s*sortie) ;
         }
         else
            sortie = 0.;

         neurone[n-my_data->learn_offset].s = sortie;

         if (sortie < local_vigilence)
            sortie = 0.;

         neurone[n].s = sortie*neurone[n-my_data->mod_offset].s ;
      }
      dprints("*** local_vigi=%f, micro_mod=%f, micro_learn=%f, macro=%f\n", local_vigilence, neurone[n-my_data->mod_offset].s, neurone[n-my_data->learn_offset].s, neurone[n].s) ;
   }
   else
      neurone[n].s = neurone[n].d = neurone[n-my_data->mod_offset].s = neurone[n-my_data->learn_offset].s = 0.;
}

/**
 * This version should only be used for place cells using sigma-pi activities as inputs
 * This one is essentially the same as in selective_winner.c but taking into account the contextual links
 */
#define NBRE_MAX_INPUT 1000
void mise_a_jour_neurone_winner_selectif_modulated_macroneuron_hedonist(int n, int gestion_STM, int learn)
{
   int gpe ;
   MyData_selective_winner_modulated *my_data;
   float local_vigilence = vigilence;
   type_coeff *coeff;
   float sortie, a ;
   int nbre_input;

   float rhoL;
   int rholXinput;
   int is_max, k, j ;
   float max, input;
   type_coeff *coeff_max = NULL;
   type_coeff *coeff_used[NBRE_MAX_INPUT];


   (void) gestion_STM; (void) learn;
   gpe = neurone[n].groupe ;

   /*recupere l'info de vigilence si non global mais representee par un groupe*/
   my_data = ((MyData_selective_winner_modulated *) (def_groupe[gpe].data));
   if (my_data->gpe_vigilence != -1)
      local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

   /* Check context activation */
   sortie = 0. ;
   nbre_input = 0;
   coeff = neurone[n-my_data->mod_offset].coeff;
   while (coeff != NULL)
   {
      if (coeff->val*neurone[coeff->entree].s1>0.0001)
      {
         sortie += coeff->val*neurone[coeff->entree].s1 ;
         nbre_input++;
      }
      coeff = coeff->s;
   }

   /* Get input pattern activation for context activated neurons */
   if(nbre_input>0)
   {
      neurone[n].d = neurone[n-my_data->mod_offset].s = sortie/(float)nbre_input ;
      sortie = 0. ;
      nbre_input = 0;
      coeff = neurone[n-my_data->learn_offset].coeff;
      a = neurone[n-my_data->learn_offset].seuil;
      if (a <=def_groupe[gpe].seuil)
      {
         neurone[n].s = neurone[n-my_data->learn_offset].s = a * drand48();
      }
      else
      {

#ifdef MESURE
         /*incremente le compteur de charge calcul*/
         my_data->counter++;
#endif
         // MARWEN : first loop to get nbre_input then second loop to fill the ordered lookup table
         // these two should be merged
         // 1st loop
         while (coeff != NULL)
         {
            if (coeff->val > 0.9999) // input from PrPh (sigma-pi) is set to 1 during learning phases so links are learned with val = 1.
            {
               nbre_input++ ;
               //sortie += fabs(coeff->val - neurone[coeff->entree].s1);
            }
            coeff = coeff->s;
         }
         if (nbre_input > NBRE_MAX_INPUT)
            EXIT_ON_ERROR("erreur memoire: nbre entrees > nbre max d'entrees");
         coeff = neurone[n-my_data->learn_offset].coeff;
         // 2nd loop
         for (j = 0; j < nbre_input; j++)
         {
            max = 10.;
            coeff = neurone[n-my_data->learn_offset].coeff;
            while (coeff != NULL)
            {
               if (coeff->val > 0.9999) // input from PrPh (sigma-pi) is set to 1 during learning phases so links are learned with val = 1.
               {
                  is_max = 1;
                  for (k = 0; k < j; k++)
                     if (coeff == coeff_used[k])
                        is_max = 0;
                  if (is_max == 1)
                  {
                     input = neurone[coeff->entree].s1;
                     if (fabs(coeff->val - input) <= max)
                     {
                        max = fabs(coeff->val - input);
                        coeff_max = coeff;
                     }
                  }
               }
               coeff = coeff->s;
            }
            coeff_used[j] = coeff_max;
         }
         // once the inputs ordered in the lookup table, we select the (rhoL x nbre_input) best
         rhoL = my_data->rhoL;
         rholXinput = (int) (rhoL * nbre_input);
         dprints("rhol x input %d\n", rholXinput);
         if (rholXinput == 0)
            EXIT_ON_ERROR("division par 0 : pas de poids égaux à 1");
         sortie = 0;
         for (j = 0; j < rholXinput; j++)
            sortie += fabs(coeff_used[j]->val - neurone[coeff_used[j]->entree].s1);

         /* // test supprimé puisqu'on a déjà verifié 5 lignes plus haut (codes à harmoniser)
          * if (nbre_input > 0){
            sortie = 1. - sortie / (float)nbre_input;
         }
         else
            sortie = 0.; */
         sortie = 1. - sortie / (float)rholXinput;

         neurone[n-my_data->learn_offset].s = sortie;

         if (sortie < local_vigilence)
            sortie = 0.;

         neurone[n].s = sortie; //*neurone[n-my_data->mod_offset].s ;
      }
      dprints("*** local_vigi=%f, micro_mod=%f, micro_learn=%f, macro=%f\n", local_vigilence, neurone[n-my_data->mod_offset].s, neurone[n-my_data->learn_offset].s, neurone[n].s) ;
   }
   else
      neurone[n].s = neurone[n].d = neurone[n-my_data->mod_offset].s = neurone[n-my_data->learn_offset].s = 0.;
}




void apprend_coeff_winner_selectif_modulated(int n,int macro)
{
   type_coeff *coeff;
   neurone[n].seuil= neurone[macro].seuil = 0.95 ;
#ifdef DEBUG_NAME
   dprints("apprend coeff winner selectif, neurone = %d \n",n);
#endif
   coeff=neurone[n].coeff;
   if (coeff==NULL) {dprints("ERROR impossible learning - NO LINK !!! \n"); exit(0);}

   coeff=neurone[n].coeff;
   while (coeff!=NULL)
   {
      if (coeff->evolution==1)                 /* si coeff modifiable */
      {
         coeff->val=neurone[coeff->entree].s1;
      }
      coeff=coeff->s;
   }
}


void apprend_winner_selectif_modulated(int gpe)
{
   int i;
   int deb, nbre,incrementGpe;

   MyData_selective_winner_modulated *my_data;
   my_data= ((MyData_selective_winner_modulated *) (def_groupe[gpe].data));
   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   incrementGpe=nbre/(def_groupe[gpe].taillex*def_groupe[gpe].tailley);
   dprints("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n", __FUNCTION__, gpe);
#ifdef DEBUG_NAME
   dprints("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n", __FUNCTION__, gpe);
#endif
   for (i=deb+incrementGpe-1; i<(deb+nbre); i+=incrementGpe)  /* modifie les poids en consequence */
   {
      if (neurone[i].flag==1) {
         apprend_coeff_winner_selectif_modulated(i-my_data->learn_offset,i);
         apprend_coeff_context(i-my_data->mod_offset,i);
      }
   }
}



void apprend_coeff_winner_selectif_modulated_env_simul(int n,int macro)
{
   type_coeff *coeff;
   neurone[n].seuil = neurone[macro].seuil =0.95;
#ifdef DEBUG_NAME
   /*  dprints("%s : neurone = %d  apprend\n", __FUNCTION__, n);*/
#endif
   coeff = neurone[n].coeff;
   if (coeff == NULL)
   {
      EXIT_ON_ERROR("ERROR impossible learning - NO LINK !!! \n");
   }

   while (coeff != NULL)
   {
      if (coeff->evolution == 1)  /* si coeff modifiable */
      {
#ifdef DEBUGl
         dprints("--\n");
         dprints("old val= %f \n", coeff->val);
         dprints("new val=%f \n", coeff->moy);
#endif
         /*    if (neurone[coeff->entree].s1 > 0.98)*/
         coeff->val = neurone[coeff->entree].s1;
         /* else
              coeff->val = 0.;*/
      }
      coeff = coeff->s;
   }
}


/*renvoie 0 si y>vigilence*/
float seuilp(float y)
{
   float k;
   k=vigilence-y;
   if (k>0)return 0.;
   return 1.;
}

/*renvoie 0 si y>vigilence*/
float seuiln(float y)
{
   float k;
   k=vigilence-y;
   if (k>0)return 1.;
   return 0.;
}

float  seuil_pos(float y)
{
   if (y>0)return 1.;
   return 0.;
}


void apprend_coeff_context(int n,int macro)
{
   type_coeff *coeff;
   float coeff_val;
   float entree;
   int nb_e;
   float act_pc;
#ifdef DEBUG_NAME
   /*  dprints("%s : neurone = %d  apprend context\n", __FUNCTION__, n);*/
#endif
   coeff = neurone[n].coeff;
   if (coeff == NULL)
   {
      dprints("ERROR impossible learning - NO LINK !!! :%d\n",n);
      exit(0);
   }

   while (coeff != NULL)
   {
      if (coeff->evolution == 1)  /* si coeff modifiable */
      {
#ifdef DEBUGl
         dprints("--\n");
         dprints("old val= %f \n", coeff->val);
#endif

         /*verison desapprentissage */

         /*variables locales pour eviter de trop repeter les indirections*/
         nb_e=(coeff->Nbre_E);
         entree=seuil_pos(neurone[coeff->entree].s1);
         coeff_val=coeff->val;
         act_pc=seuilp(neurone[macro].s1);

         /*si son entree est active et la PC aussi -> on marque cette coactivation : nb_e=1*/
         nb_e+=(entree*act_pc);

         dprints(" nb_e_seuille:%d, act_PC_seuille: %f , act_PC: %f ,  entree: %f - neurone: %d entree:%d\n",nb_e,(seuilp(neurone[macro].s1)),neurone[macro].s1,neurone[coeff->entree].s1,n,coeff->entree);

         if (nb_e>1)nb_e=1;
         coeff->Nbre_E=nb_e;
         /*
         modification du W:
          *si si nb_e =1:
          * augmenter (1) si act_pc et entree sont >0
          * inchanger sinon w=w+0
          *sinon
          *diminuer si PC active et pas contexte (la pc ne depend pas ou plus de ce context)
         				  si context actif et pas PC (la pc ne depend pas (encore) de ce context)
          */
         coeff_val=  (coeff_val+(entree*act_pc*nb_e))-(1-nb_e);
         if (coeff_val<0)coeff_val=0.;
         else if (coeff_val>1.)coeff_val=1.;
         coeff->val=coeff_val;
         //printf("new cxt link weight = %f \n", coeff->val);

#ifdef DEBUGl
         dprints("new val= %f \n", coeff->val);
#endif
      }
      coeff = coeff->s;
   }
}


/*--------------------------------------------------------------------*/

void apprend_winner_selectif_modulated_env_simul(int gpe)
{
   int i;
   int deb, nbre,incrementGpe;

   MyData_selective_winner_modulated *my_data;
   my_data= ((MyData_selective_winner_modulated *) (def_groupe[gpe].data));
   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   incrementGpe=nbre/(def_groupe[gpe].taillex*def_groupe[gpe].tailley);
#ifdef DEBUG_NAME
   dprints("~~~~~~~~~~~~~~~~ entre dans %s(%d) \n", __FUNCTION__, gpe);
#endif
   for (i=deb+incrementGpe-1; i<(deb+nbre); i+=incrementGpe)  /* modifie les poids en consequence */
   {
      /* if(neurone[i].s2>0.99) dprints("sortie s = %f , vig = %f \n",neurone[i].s,vigilence); */
      /*apprentissage du motif*/
      if (neurone[i].flag == 1)
      {
         /*		dprints("apprentissage neurone %d- %d \n",i,i-my_data->learn_offset);*/
         apprend_coeff_winner_selectif_modulated_env_simul(i-my_data->learn_offset,i);
      }
      /*apprentissage du context pour ts les pc actives*/
      //if(neurone[i].seuil>def_groupe[neurone[i-my_data->mod_offset].groupe].seuil)
      if (neurone[i].seuil >=0.90)
         apprend_coeff_context(i-my_data->mod_offset,i);
   }
}






void recherche_max_locaux(int gpe, float dvn)
/*int gpe; float dvn;  distance de competition */
{
   int i = 0, deb, fin, nbre, nbre2;
   int increment;
   int  posmax = -1, premier_posmax_nr = -1;
   float val_max = -1;
   float local_vigilence = vigilence;
   MyData_selective_winner_modulated *my_data;

   (void)dvn;

   my_data = ((MyData_selective_winner_modulated *) (def_groupe[gpe].data));
   if (my_data->gpe_vigilence != -1) local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

#ifdef MESURE
   if (my_data->counter>0)
   {
      my_data->f=fopen("context_mesure0.m","a");
      fdprints(my_data->f, "%d ,",my_data->counter);
      fclose(my_data->f);
   }

   /*reinitialisation*/
   my_data->counter=0;
#endif



   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   fin = deb+nbre ;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;
#ifdef DEBUG_NAME
#ifndef AVEUGLE
   dprints("~~~~~~~~~~~~~~~~~~~~ %s(%d)\n", __FUNCTION__, gpe);
   /*dprints("nbre = %d - nbre2 = %d - increment = %d\n",nbre,nbre2,increment); */
#endif
#endif

   val_max = -1.;
   posmax = -1.;
   premier_posmax_nr = -1;
   for (i = deb + increment - 1; i < (nbre+deb); i += increment)    /*reset des sorties et recherche du max */
   {
      neurone[i].flag = 0;
      neurone[i].s1 = neurone[i].s2 = 0.; /* reset des sorties */
      if (neurone[i].d>0)
      {

         /*recherche du max dans le bon ensemble */
         if ((neurone[i-my_data->learn_offset].seuil <= def_groupe[gpe].seuil))    /*neurone non recrute */
         {
            if (premier_posmax_nr == -1)    /*est-ce le premier max non recrute max */
               premier_posmax_nr = i;
            if (neurone[i].s > val_max)   /*est-ce le max */
            {
               val_max = neurone[i].s;
               posmax = i;
            }
            dprints("is not recruited (neurone[%d].s %f )and ", i, neurone[i].s);
         }
         else if (neurone[i].s > local_vigilence)    /*sortie (d'un neurone recrute) suffisament activ� */
         {
            if (neurone[i].s > val_max)   /*est-ce le max */
            {
               val_max = neurone[i].s;
               posmax = i;
            }
            /*on conserve tous les PC > vigilence*/
            neurone[i].s1 =neurone[i-my_data->learn_offset].s1= neurone[i].s;
            dprints("is active and (neurone[%d].s %f )", i, neurone[i].s);
         }
         else
            dprints("not activated (neurone[%d].s %f ) but ", i, neurone[i].s);
         dprints("is context activated\n");
      }
      else
         dprints("is NOT context activated\n");

   }

   /*plus aucun neurone dispo et aucune reconnaissance ?? (=on n'a pas trouverde max */
   if (posmax == -1)
   {
      dprints("RECODAGE du neurone , changement de context %f, %f...\n",neurone[deb + increment - 1].d,neurone[deb + increment - 1].s);
      return;
   }

   dprints("! val intermediaire de posmax = %d\n", posmax - deb);

   /*si on  recrute alors on prend le premier */
   if (neurone[posmax-my_data->learn_offset].seuil <= def_groupe[gpe].seuil)
      posmax = premier_posmax_nr;

   if (val_max > RT)
   {
      /* si le max est superieur  a 0 alors le neurone max est mis a 1 */
      neurone[posmax].s2 =neurone[posmax-my_data->learn_offset].s2= 1;
      neurone[posmax].s1 =neurone[posmax-my_data->learn_offset].s1= val_max;
   }

   if (neurone[posmax-my_data->learn_offset].seuil <= def_groupe[gpe].seuil && local_vigilence > def_groupe[gpe].seuil)
      /* seul le gagnant apprend s'il n'avait encore rien appris */
      /* pas de modification des poids */
   {
      if (global_learn == 1)
      {
         /*      dprints("apprentissage neurone %d \n",i); */
         neurone[posmax].s2 = neurone[posmax].s1 = neurone[posmax].s =neurone[posmax-my_data->learn_offset].s1=neurone[posmax-my_data->learn_offset].s= 1;
         neurone[posmax].flag = 1;
         /*      neurone[posmax].seuil= 0.95 ; */

         dprints("!!!!!le neurone %d va apprendre n.seuil:%f, gp.seuil:%f, vigilence:%f \n", posmax - deb,neurone[posmax].seuil,def_groupe[gpe].seuil,local_vigilence); /* Dbarre, neurone tres selectif */

      }
   }
   dprints("\n    apres compet, gagnant: %d --  s = %f -- s1 = %f -- s2= 1 \n ========== \n", posmax - deb, neurone[posmax].s, neurone[posmax].s1);
   dprints("neurone %d , activite max = %f , activit�de s2 = %f \n", posmax, val_max, neurone[posmax].s2);

   if(def_groupe[gpe].nbre_de_1 < 0.)
   {
      for (i = 0; i < nbre; i++)
         if(i != posmax)
            neurone[i].s2 = neurone[i].s1 = val_max;
   }
   else
      for (i = 1; i < def_groupe[gpe].nbre_de_1; i++)
      {
         val_max = -1;
         posmax = -1.;
         /* cherche le max local au neurone n considere */
         for (i = deb; i < fin; i += increment)
         {
            /* test sur .s2 si c'est a zero c'est que ce n'est pas un
             * max que l'on a deja selectionne donc on peut le
             * selectionner */
            if (neurone[i].s > val_max && neurone[i].s2 <= 0.)
            {
               val_max = neurone[i].s;
               posmax = i;
            }
         }

         if (val_max > RT)
         {
            neurone[posmax].s2 = neurone[posmax].s1 = val_max;
            dprints("le %d gagnant, num %d vaut %f \n", i + 1,posmax , val_max);
         }
         else
         {
            /* pas la peine de rechercher les autres max, ils sont
             * nuls */
            break;
         }
      }


}

void recherche_max_locaux_winner_selectif_modulated(int gpe)
{
   /* distance d'inhibition */
   float dvn;

   dvn = (float) def_groupe[gpe].dvn;
   recherche_max_locaux(gpe, dvn);
}
