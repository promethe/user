/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_min_period.c 
\brief 

Author: A.Patard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <libx.h>
#include <Struct/time_infos.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#ifndef timersub
#define timersub(a, b, result) \
	do {	\
		(result)->tv_sec = (a)->tv_sec - (b)->tv_sec; 		\
		(result)->tv_usec = (a)->tv_usec - (b)->tv_usec;	\
		if ((result)->tv_usec < 0) {				\
			--(result)->tv_sec;				\
			(result)->tv_usec += 1000000;			\
		}							\
	} while (0)
#endif

void function_min_period(int gpe)
{
    struct time_infos *data;
    int gpe_entree;
    struct timeval current, diff;
    struct timespec sleep_time;
    long time_diff;
    char retour[256];

    if (def_groupe[gpe].data == NULL)
    {
        data = (struct time_infos *) malloc(sizeof(struct time_infos));;
        if (data == NULL)
        {
            printf("Error in allocation file %s line %d\n", __FILE__,
                   __LINE__);
            return;
        }
        def_groupe[gpe].data = (void *) data;

        gpe_entree = find_input_link(gpe, 0);

        if (prom_getopt(liaison[gpe_entree].nom, "t", retour) != 2)
        {
            printf
                ("Error: wrong (or no) parameter to f_min_period (gpe=%d)\n",
                 gpe);
            return;
        }

        data->sec = atol(retour) / 1000;
        data->microsec = atol(retour) % 1000;
        gettimeofday(&data->last, NULL);
    }
    else
    {
        data = (struct time_infos *) def_groupe[gpe].data;

        gettimeofday(&current, NULL);
        timersub(&current, &data->last, &diff);
        if ((diff.tv_sec <= data->sec) || (diff.tv_usec < data->microsec))
        {
            time_diff = data->sec * 1000000 + data->microsec;
            time_diff -= (diff.tv_sec * 1000000 + diff.tv_usec);
            sleep_time.tv_sec = time_diff / 1000000;
            sleep_time.tv_nsec = time_diff % 1000000;
            nanosleep(&sleep_time, NULL);
        }
        else
        {
            printf("Warning: Exceeded time in f_min_period (gpe=%d)\n", gpe);
        }
        gettimeofday(&data->last, NULL);
    }
}
