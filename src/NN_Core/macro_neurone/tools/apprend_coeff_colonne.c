/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Selctionne l'apprentissage en fonction du type de lien : produit ou distance

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include "include/local_var.h"
#include <Global_Var/Limbic.h>
#include "include/macro.h"
/*------------------------------------------------------------------*/

#define EPSILON_H	0.016           /*!< Coefficient de Hebb */

/* --------------------------------------------------------------------------------------------------- */
/*#define debug */

int apprend_coeff_colonne_produit_max(int n, float sortie, float neuromod)  /* n=numero du micro-neurone d'une colonne,             */
                                        /* sortie=activation du macro-neurone de  cette colonne */
{                               /* neuromod= neuromodulation portant sur le groupe      */
    /*float               dW, Wi, Ej, Wsat; */
    type_coeff *coeff;
    float hasard;
    /*float               Wtemp; */
    /*float         max, input, entree_max, poids_max, max_pos, max_neg; */
    int flag = 0 /*, pos_pos, pos_neg */ ;
    int  i = 0; /* i=numero du neurone en entree connecte au micro-neurone n */
    /* int         N1=0;                   Nombre de liens a un */
    float Cij;                  /* Mesure de correlation */
    float Cij_prime;            /* Correlation ponderant l'importance du passe/present */
    float signe;         /* Type de lien : negatif = inhibiteur, positif = activateur */
    int Ei, Sj;
    float delta_pij;
    float input_value;

    /* si pas de modif de coefficients le flag restera a 0 sinon 1 */
    coeff = neurone[n].coeff;

/* ------------------------------------------------------------------------------- */
/* ----Parcours de l'ensemble des liens arrivant sur le micro-neurone numero n---- */
    while (coeff != NULL)
    {
        i++;
        input_value = neurone[coeff->entree].s1;
        if ((coeff->evolution == 1) && (isdiff(neuromod, 0.0)))   /* test si il y a un signal de renforcement (neuromod) et si les poids sont modifiables */
        {
            Ei = (input_value >= Integration_Seuil); /* 0 si activation inferieure au seuil d'integration, 1 si activation superieure    */
            Sj = (sortie > 0.95);   /* 0 si activation du macro-neurone < 0.95, 1 si activation du macro-neurone > 0.95 */
            /* Mise a jour des outils de calcul de la correlation entree-sortie du lien */
            if (Sj)
            {
                coeff->Nbre_S = coeff->Nbre_S + 1.; /* Decompte du nombre de fois ou la sortie est activee */
            }
            if (Ei)
            {
                coeff->Nbre_E = coeff->Nbre_E + 1.; /* Decompte du nombre de fois ou l'entree est activee */
            }
            if ((Ei) && (Sj))
            {
                coeff->Nbre_ES = coeff->Nbre_ES + 1.;   /* Decompte du nombre de fois ou entree et sortie sont actives simultanement */
            }

/* ------------------------------- 		 Nota :  Plus la probabilite du poids d'une liaison est forte,      */
/* -------Mise a jour proba------- 		 	   plus le poids sera stable (moins de chances d'apprendre)   */
            /* La proba d'un poids constitue un indice de confiance dans le poids */
            if ((coeff->Nbre_E > 0.) && (coeff->Nbre_S > 0.))
            {
                if (coeff->val < 0.)    /* ----------lien inhibiteur--------- */
                    if (coeff->val < -0.5)
                        signe = -1.;    /* Si Wij=-1 -> renforcer la proba (si neuromod positive) */
                    else
                        signe = 1.; /* Si Wij=0 -> diminuer la proba (si neuromod positive) */

                else /* ----------lien activateur--------- */ if (coeff->val < 0.5)
                    signe = -1.;    /* Si Wij=0 -> renforcer la proba (si neuromod positive) */
                else
                    signe = 1.; /* si Wij=1 -> diminuer la proba  (si neuromod positive) */

                /* -----------Calcul de la correlation entree-sortie Cij---------- */
                Cij =
                    coeff->Nbre_ES / (float) sqrt(coeff->Nbre_E *    coeff->Nbre_S);
                /* --Calcul de Cij_prime : correlation entree-sortie en fonction de la neuromodulation -- */
                /* --"neuromod" et de la correlation en entree et en sortie du lien ij                 -- */
                Cij_prime = (BETA * Cij + fabs(neuromod) * (Ei * Sj)) / (BETA + fabs(neuromod));    /* si entree et sortie du lien ij sont co-activees alors  */
                /* Cij_prime subit fortement l'influence de "neuromod"    */
                /* si entree et sortie du lien ij sont decorrellees alors */
                /* Cij_prime est proportionnel a Cij                      */
                /* --Calcul de la probabilite du poids proba=proba+delta_pij, c'est l'indice de confiance dans le poids du lien-- */
                delta_pij =  -(EPSILON_H * input_value + ALPHA * neuromod) * signe * Cij_prime - LAMBDA * coeff->proba * input_value;
                printf("deltaPij: %f\n", delta_pij);
                coeff->proba = coeff->proba + delta_pij; /* Rque : la probabilite pour que le lien soit appris sera 1-coeff->proba */
                if (coeff->proba > 1.)
                    coeff->proba = 1.;
                if (coeff->proba < 0.)
                    coeff->proba = 0.;
                printf
                    ("neuromodulation = %f ### p[%d,%d]=%f ### Cij = %f ### Cij_prime=%f\n",
                     neuromod, i, n, coeff->proba, Cij, Cij_prime);
            }
            /* Loi probabiliste de changement des poids */
            hasard = (float) drand48();
            if ((hasard > coeff->proba) && (coeff->Nbre_E > 0.)
                && (coeff->Nbre_S > 0.))
            {
                /* RAZ : le poid du lien va changer, la correlation entree-sortie doit etre recalculee */
                coeff->Nbre_E = 0;
                coeff->Nbre_S = 0;
                coeff->Nbre_ES = 0;
                /* le poids du lien va changer en 1-ancien_poids donc la probabilite est inverse */
                coeff->proba = 1. - coeff->proba;
                /* mise a jour du poids du lien */
                if (coeff->val > 0.)
                    coeff->val = 1. - coeff->val;
                else
                    coeff->val = -1. - coeff->val;
                printf("Random draw => T=%d ### W[%d,%d]=%f\n", temps_douleur,  i, n, coeff->val);
            }
        }
        coeff = coeff->s;
    }
    return (flag);
}

/* L'apprentissage ne se fait que si l'activite de la colonne corticale, donc du macro-neurone depasse un certain seuil. 	*/
/* Si l'activite de la colonne corticale depasse ce seuil alors l'apprentissage d'un lien vers un micro-neurone se fait 	*/
/* avec une probabilite de (activation du macro-neurone)/1 tiree pour chaque micro-neurone de la colonne corticale. 		*/
/* Lorsqu'il a lieu, l'apprentissage est radical : si l'activitee du micro-neurone est presque egale a 1 le poids des liens 	*/
/* vers ce micro-neurone est passe a 1 sinon, il est passe a zero.								*/

int apprend_coeff_colonne_distance(int n, float sortie) /* n=numero du micro-neurone d'une colonne,             */
                            /* sortie=activation du macro-neurone de  cette colonne */
{
    float Ej;
    type_coeff *coeff;
    float coeff_avant_modif;
    float hasard;
    float certitude;
    int flag = 0;
    float input_value;

    /* si pas de modif de coefficients le flag restera a 0 sinon 1 */
    coeff = neurone[n].coeff;
    certitude = neurone[n].seuil;   /* Dseuil */

    /* Sortie = Dk = activation du macro-neurone de la colonne corticale */
    /* ------------------------------------- */
    /* Boucle sur les liens du micro-neurone */
    /* ------------------------------------- */
    while (coeff != NULL)
    {
        input_value = neurone[coeff->entree].s1;
        if (coeff->evolution == 1)  /* Evolution si possible */
        {
            coeff_avant_modif = coeff->val;
            Ej = input_value;    /* entree du micro-neurone moyennee sur le temps        */
            /* si max local  ou hasard et pres du max local  */
            hasard = (float) (drand48());   /* 0 < hasard < 1 */

            if (certitude < sortie && hasard < sortie)  /* Dk > Dseuil et Dk > Hasard */
            {
                if (liaison[coeff->gpe_liaison].temps < 0.001)
                {
                    if (Ej > 0.999)
                        Ej = 1.;
                    else
                        Ej = 0.;
                }               /* Saturation entree */

                if (Ej > 0.999) /* Entree a un */
                {
                    if (coeff->val > 0.999)
                        coeff->val = 2.;    /* Modification demandee 2 fois */
                    else
                        coeff->val = 1.;
                }
                else            /* Entree a zero */
                {
                    coeff->val = 0.;
                }
                if (isdiff(coeff_avant_modif, coeff->val))
                    flag = 1;
            }
        }
        coeff = coeff->s;
    }
    return (flag);
}

int apprend_coeff_colonne(int n, float sortie, float neuromod)  /* n=numero du micro-neurone d'une colonne,             */
                                /* sortie=activation du macro-neurone de  cette colonne */
{                               /* neuromod=neuromodulation portant sur le groupe       */
    int No_voie;
    type_coeff *coeff;


    coeff = neurone[n].coeff;
    No_voie = (int) (coeff->type / 2);  /* 2n   :  ei*wij    non modifiable    */
    /* 2n+1 : |ei - wij |    modifiable    */
    /* n numero de la voie                 */


    /* -------------------------------- */
    /* Apprentissage selon type de voie */
    /* -------------------------------- */

    if (coeff->type != No_voie * 2)
    {
#ifdef debug
        printf("apprend_coeff_colonne_distance: %d\n", n);
#endif
        return (apprend_coeff_colonne_distance(n, sortie));
    }
    else
    {
#ifdef debug
        printf("apprend_coeff_colonne_produit_max: %d\n", n);
#endif
        return (apprend_coeff_colonne_produit_max(n, sortie, neuromod));
    }
}
