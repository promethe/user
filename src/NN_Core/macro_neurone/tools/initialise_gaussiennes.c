/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  initialise_gaussiennes.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    Mets a jour le tableau "tableau_gaussienne" des groupes detectes:
 * - PTM 
 * - Kohonen 
 * - Macro Colonne 
 * Les valeurs contenues dans ces
 * tableaux sont des DOG (Differences Of Gaussiennes) calculees selon
 * les parametres dvp (distance au voisinage positif) et dvn (distance
 * au voisinage negatif). Dvp et dvn sont generalement lues dans le
 * fichier .script du reseau (genere par leto).
 * Cette fonction est appelee lors de l'initialisation du reseau seulement.
 
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include "libx.h"

#define MICRO_DIST 1

void initialise_gaussiennes(void)
{
    float d;
    float a1, a2;
    float dvp, dvn, taille;
    int i;


    for (i = 0; i < nbre_groupe; i++)
    {
        /* init par defaut */
        for (d = 0.; d < taille_max_tableau_diff_gaussienne; d = d + 1.)
            def_groupe[i].tableau_gaussienne[(int) d] = 0.;

        if (def_groupe[i].type == No_PTM)
        {
            dvp = (float) def_groupe[i].dvp;    /*!< Voisinage de competition */
	    if(dvp>taille_max_tableau_diff_gaussienne-1) dvp=taille_max_tableau_diff_gaussienne-1;
            printf
                ("....group %d, found \n ..... type : PTM \n ..... dvp = %f  \n",
                 i, dvp);
            a1 = -(dvp) * (dvp) / log(0.5);
            for (d = 0.; d < dvp + 1; d = d + 1.)
                def_groupe[i].tableau_gaussienne[(int) d] =
                    (exp(-d * d / a1) - 0.5) * 2.;
        }

        if (def_groupe[i].type == No_Kohonen)
        {
            dvp = (float) def_groupe[i].dvp;
            dvn = (float) def_groupe[i].dvn;
	    if(dvp<0) dvp=0;
	    if(dvn>taille_max_tableau_diff_gaussienne) dvn=taille_max_tableau_diff_gaussienne;
            taille = dvn - dvp;
            printf
                ("....group %d, found \n ..... type : kohonen \n ..... dvp = %f \n ..... dvn = %f  \n",
                 i, dvp, dvn);
            a2 = -(dvp) * (dvp) / log(0.5);
            for (d = dvp; d < dvn; d = d + 1.)
                def_groupe[i].tableau_gaussienne[(int) d] =
                    -fabs(cos(((d - dvp) / taille * pi) + (pi / 2.)) *
                          ((cos((d - dvp) / taille * (pi / 2.))) / 2.)) / 2.;
            for (d = 0.; d < dvp + 1; d = d + 1.)
                def_groupe[i].tableau_gaussienne[(int) d] =
                    (exp(-d * d / a2) - 0.5) * 2.;
        }

        if (def_groupe[i].type == No_Macro_Colonne)
        {
            int deb, nbre, nbre2, increment, increm, n, mode, cpt;
            type_coeff *coeff;

            dvp = (float) def_groupe[i].dvp;
            dvn = (float) def_groupe[i].dvn;
	    if(dvp<0) dvp=0;
            if(dvp>taille_max_tableau_diff_gaussienne-1) dvp=taille_max_tableau_diff_gaussienne-1;
	    if(dvn>taille_max_tableau_diff_gaussienne) dvn=taille_max_tableau_diff_gaussienne;
            taille = dvn - dvp;
            printf
                ("....group %d, found \n ..... type : Macro_Colonne \n ..... dvp = %f \n ..... dvn = %f  \n",
                 i, dvp, dvn);
            a2 = -(dvp) * (dvp) / log(0.5);
            for (d = dvp; d < dvn; d = d + 1.)
                def_groupe[i].tableau_gaussienne[(int) d] =
                    -fabs(cos(((d - dvp) / taille * pi) + (pi / 2.)) *
                          ((cos((d - dvp) / taille * (pi / 2.))) / 2.)) / 2.;
            for (d = 0.; d < dvp + 1; d = d + 1.)
                def_groupe[i].tableau_gaussienne[(int) d] =
                    (exp(-d * d / a2) - 0.5) * 2.;

            /* initalisation des positions des micro_neurones */
            /* distance sur une dimension (posx) */
            deb = def_groupe[i].premier_ele;
            nbre = def_groupe[i].nbre;
            nbre2 = def_groupe[i].taillex * def_groupe[i].tailley;
            increment = nbre / nbre2;
            increm = increment - 2;

            cpt = deb;
            for (n = deb; n < deb + nbre; n++)
            {
                coeff = neurone[n].coeff;
                mode = liaison[coeff->gpe_liaison].mode;
                if (mode == MICRO_DIST)
                {
                    neurone[n].posx = (cpt - deb) % increm;
                    cpt++;
                    /*         printf("%f ",neurone[n].posx); */
                }

            }
/*	   printf("\n");
           for (n=0;n<=30;n++) printf ("%f \n",def_groupe[i].tableau_gaussienne[n]);*/

        }

    }


}
