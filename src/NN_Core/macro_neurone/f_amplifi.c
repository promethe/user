/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_amplifi.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Utilise avec le groupe produit 
  * garde l'activite la plus grande pour chacune des entrees 
  * aussi bien sur s que sur s1 et s2                        
  * les sorties doivent bien evidemment etre remise a zero   
  * avec la fonction f_clean

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

void function_amplifi(int gpe_sortie)
{
    type_coeff *coeff;
    int n, deb_s, taille_groupe_s;

    deb_s = def_groupe[gpe_sortie].premier_ele;
    taille_groupe_s = def_groupe[gpe_sortie].nbre;

    for (n = 0; n < taille_groupe_s; n++)
    {
        coeff = neurone[deb_s + n].coeff;

        while (coeff != NULL)
        {
            if (neurone[coeff->entree].s > neurone[deb_s + n].s)
                neurone[deb_s + n].s = neurone[coeff->entree].s;
            if (neurone[coeff->entree].s1 > neurone[deb_s + n].s1)
                neurone[deb_s + n].s1 = neurone[coeff->entree].s1;
            if (neurone[coeff->entree].s2 > neurone[deb_s + n].s2)
                neurone[deb_s + n].s2 = neurone[coeff->entree].s2;
             /*NONODEBUG*/
                /*if ((neurone[deb_s + n].s > 1.)||(neurone[deb_s + n].s1 > 1.)||(neurone[deb_s + n].s2 > 1.)) { printf("Depassement de la saturation dans f_amplifi\n"); *//*if (neurone[deb_s + n].s != 0.){ printf("neurone[%d].s=%f, .s1=%f, .s2=%f\n",deb_s + n,neurone[deb_s + n].s,neurone[deb_s + n].s1,neurone[deb_s + n].s2);
                   fflush(stdout);} *//*exit (0);} */
                coeff = coeff->s;
        }
    }
}
