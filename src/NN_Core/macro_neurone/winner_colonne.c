/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:



Macro:
-none

Local variables:
-float ddouleur

Global variables:
-none

Internal Tools:
-none

External Tools:
-apprend_coeff_colonne
-calcule_mc_produit_max()
-calcule_mc_produit()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/


#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Global_Var/Limbic.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>

#include "tools/include/apprend_coeff_colonne.h"
#include "tools/include/local_var.h"


float calcule_mc_produit_max(int i, int gestion_STM, int learn)
{
   /* L'activite des neurones produit
    * est le max EW         */
   type_coeff *coeff;
   float sortie, sortieEx, sortieIn, norme, E; /* sortie = activite calculee du neurone i */
   float bruit;
   int deb, increment;
   int groupe, nbre, nbre2;
   float input_value;
   /*  int		  N_max=0,j=0,done=0;*/

   UNUSED_PARAM gestion_STM; UNUSED_PARAM learn;

   norme = 0.;
   sortieIn = 0.0;             /* sortie inhibitrice */
   sortieEx = 0.0;             /* sortie excitatrice */
   coeff = neurone[i].coeff;
   groupe = neurone[i].groupe;
   deb = def_groupe[groupe].premier_ele;
   nbre = def_groupe[groupe].nbre;
   nbre2 = def_groupe[groupe].taillex * def_groupe[groupe].tailley;
   increment = nbre / nbre2;
   /* --------------------------- */
   /* Liens sur le micro-neurones */
   /* --------------------------- */
   while (coeff != NULL)
   {

      /* --------------------------- */
      /* Gestion integration entrees */
      /* --------------------------- */
      if (liaison[coeff->gpe_liaison].temps > 0.) /*float maintenant */
      {
         if (neurone[coeff->entree].s2 > 0.99)  input_value = 1.0;
         else                                   input_value = input_value / ((float) liaison[coeff->gpe_liaison].temps);
      }
      else  input_value = neurone[coeff->entree].s1;

      /* ------------------------------------------------------------------------------------ */
      /* calcul des valeurs vues de chaque lien : E ~ coeff->moy * coeff->val * coeff->proba  */
      /* ------------------------------------------------------------------------------------ */

      norme = liaison[coeff->gpe_liaison].norme;  /* recuperation de la valeur initiale du lien : definition de excitateur ou inhibiteur */

      if (norme >= 0.0)       /* ---si le lien est excitateur (si norme=0, le lien est par defaut excitateur--- */
      {
         E = input_value * 0.5 * ((2 * coeff->val - 1) * coeff->proba + 1);   /* pour le calcul de l'activation, le poids du lien excitateur est module */
                                                                              /* par son indice de confiance (sa proba) : plus la proba est grande,     */
      }                                                                       /* la modulation est forte, plus la proba est faible, plus le poids       */
      else                    /* ---si le lien est inhibiteur---       probabiliste tend vers 0.5                                             */
      {
         E = input_value * 0.5 * ((2 * coeff->val + 1) * coeff->proba - 1);   /* pour le calcul de l'activation, le poids du lien inhibiteur  */
                                                                              /* est centre en -0.5 en fonction de la probabilite             */
      }

      /* ----------------------------- */
      /* sortie excitatrice max        */
      /* ----------------------------- */
      if (E > sortieEx)      sortieEx = E;

      /* ----------------------------- */
      /* sortie inhibatrice max        */
      /* ----------------------------- */
      if (E < sortieIn)      sortieIn = E;

      coeff = coeff->s;
   }
   /*if(alealearn) */
   bruit = drand48();
   /*else bruit = 0.; */
   /* ----------------------------------------- */
   /* ---Calcul de l'activation du neurone i--- */
   sortie = sortieEx + sortieIn + (1. - 2. * bruit) * (stress);   /* Somme de inhibition et activation et ajout  */
                                                                  /* d'un bruit compris entre -1 et 1 module par */
                                                                  /* stress et inattention                       */
   if (((i - deb) % increment) == (increment - 1)) /* test si macroneurone */
   {

      if (sortie > 1.)  sortie = 1. + (stress) * drand48();
      if (sortie < 0.)  sortie =      (stress) * drand48();
   }
   else
   {
      if (sortie > 1.)   sortie =  1. + (stress) * drand48();
      if (sortie < -1.)  sortie = -1. - (stress) * drand48();
   }
   return sortie;
}

float calcule_mc_produit(int i, int gestion_STM, int learn)
{
   type_coeff *coeff;
   float sortie;
   float bruit;
   int compteur = 0;
   float input_value =0.;

   UNUSED_PARAM gestion_STM; UNUSED_PARAM learn;


   sortie = 0.;
   coeff = neurone[i].coeff;
   /*printf("mc \ n"); */
   /* printf("---neurone %d \n",i);*/
   while (coeff != NULL)        /*<! Boucle sur les liens du micro-neurone */
   {
      /* ------------------------------- */
      /* Gestion moyennage dans le temps */
      /* ------------------------------- */

      compteur++;
      if (liaison[coeff->gpe_liaison].temps > 0.) /*float maintenant */
      {
         input_value = input_value + (neurone[coeff->entree].s1 - input_value) / ((float) liaison[coeff->gpe_liaison].temps);
      }
      else
         input_value = neurone[coeff->entree].s1;

      /* ------------- */
      /* Calcul Ei*Wij */
      /* ------------- */
      sortie = sortie + coeff->val * input_value;
      /*N.C:inutilise... norme=norme+fabs(coeff->val); */
      coeff = coeff->s;
   }
   /*if ((compteur>1)&&(fabs(norme)>0.001)) sortie=sortie/norme; ** Normalisation */
   /*if(sortie>=1.) sortie=.999;
      else {if(sortie<-1.) sortie=-1.;}VB *//* Saturation */
   /* ---------------------------------------------------- */
   /* Ajout du bruit a la sortie (Generateur de diversite) */
   /* ---------------------------------------------------- */
   bruit = drand48();
   sortie = sortie + bruit * (0.02 /*stress+noise_bouffe */ ); /* +0.1*douleur */
   return (sortie);
}

/*          Apprentissage pour une colonne corticale
 *    Les neurones doivent devenir de plus en plus selectifs
 *    modif de tout le voisinage sans restriction
      Les liens vers les micro-neurones sont appris en fonction de l'activation de
      la colonne corticale, c'est a dire, en fonction de l'activation du macro-neurone  */

void apprend_winner_colonne(int gpe)
{
   /*type_noeud *ptn; */
   int n, deb, nbre, nbre2;
   int i, increment;
   /*float dc;
      float para1,para2; */
   int flag;

   float neuromod = 0.;        /* Valeur par defaut de neuromod et dRenf : si aucun lien neuromodulation de ces types n'existe en entree */
   float dRenf = 0.5;          /* du groupe alors, ces valeurs resteront inchangees.                                                     */
   char type[10];

   dprints("apprend winner colonne gpe %d \n", gpe);

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;
   /* ---Calcul de la neuromodulation portant sur le groupe--- */
   strcpy(type, "neuromod");
   calcule_neuromodulation(gpe, type, &neuromod);
   strcpy(type, "dRenf");
   calcule_neuromodulation(gpe, type, &dRenf);

   /* Rque : Si il n'y a pas de lien neuromodulation de type "neuromod" ou "dRenf"  */


   if (increment > 1)
   {
      for (n = deb + increment - 1; n < deb + nbre; n = n + increment)    /* n = macroneurone */
      {
         flag = 0;           /*pour savoir si un des micro neurones a appris PG */
         for (i = n - increment + 1; i < n; i++) /* i = microneurone */
            if (apprend_coeff_colonne(i, neurone[n].s2, neuromod) == 1)
               flag = 1;

         if (flag == 1)
         {
            if (neurone[n].s > neurone[n].seuil)
            {
               neurone[n].seuil = 1.5 * (1 - 0.5 * dRenf) * neurone[n].seuil;  /* amplitude */
               if (neurone[n].seuil > 1.)
                  neurone[n].seuil = 1.;
               if (neurone[n].seuil < 0.1)
                  neurone[n].seuil = 0.1;
            }
         }
      }
   }
   else
   {
      for (n = deb; n < deb + nbre; n++)  /* n = macroneurone */
      {
         if (apprend_coeff_colonne(n, neurone[n].s2, neuromod) == 1)
         {
            if (neurone[n].s > neurone[n].seuil)
            {
               neurone[n].seuil = 1.5 * (1 - 0.5 * dRenf) * neurone[n].seuil;  /* amplitude */
               if (neurone[n].seuil > 1.)
                  neurone[n].seuil = 1.;
               if (neurone[n].seuil < 0.1)
                  neurone[n].seuil = 0.1;
            }
         }
      }
   }
}


/*! colonne corticale etats + topologie Kohonen + Max locaux          */

void gestion_groupe_macro_neurone_recherche_max(int gpe)
{
   int n, deb, nbre, nbre2, pos;
   float max;
   int increment;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;

   max = -99999999.;
   pos = 0;
   for (n = deb + increment - 1; n < deb + nbre; n = n + increment)
   {
      if (neurone[n].s > max)
      {
         max = neurone[n].s;
         pos = n;
      }
      neurone[n].s2 = 0.;
      neurone[n].s1 = 0.;
   }

   dprints("max= %f  et  neurone[pos].seuil = %f \n", max, neurone[pos].seuil);
   if (max > neurone[pos].seuil)   /*Modif Vincent : Gestion du seuil sur WTA colonne, a conserver lors d'un put back !!! */
   {
      neurone[pos].s2 = 1.;
      neurone[pos].s1 = 1.;
   }

   dprints("\t Le Max du groupe %d est %d le %deme: %f, increment=%d\n", gpe, pos, (pos - deb) / increment, max, increment);
}



/*-------------------------------------------------------------------*/
/* Calcule la sortie d'un neurone equivalent a une colonne corticale */
/* calcule la sortie pour chacune des voies de liaisons = correlation*/
/* fait la somme des differents resultats                            */
/*-------------------------------------------------------------------*/

void mise_a_jour_neurone_colonne(int i, int gestion_STM, int learn)
{
   type_coeff *coeff;
   float sortie;
   /*int             deb, increment;
      int             groupe, nbre, nbre2; */
   /* ---Calcul de la neuromodulation portant sur le groupe--- */

   coeff = neurone[i].coeff;

   if (coeff == NULL)  neurone[i].s2 = neurone[i].s1;  /*    c'est entree   */

   if ((coeff != NULL) && (coeff->type % 2 == 0))
   {
      dprints("calcule_mc_produit: %d\n", coeff->type);
      sortie = calcule_mc_produit(i, gestion_STM, learn);
      /*printf("  calcule_mc_produit, neurone[%i] coeef_type: %d, sortie %f\n",i,coeff->type,sortie); */
   }
   else
   {
      sortie = calcule_mc_produit_max(i, gestion_STM, learn);
      /* printf("  calcule_mc_produit_mx, neurone[%i] coeef_type: %d, sortie %f\n",i,coeff->type,sortie); */
      dprints("calcule_mc_produit_max: %d\n", coeff->type);
   }

   neurone[i].s = neurone[i].s1 = sortie;  /* activite passee par la fonction de sortie    */

}
