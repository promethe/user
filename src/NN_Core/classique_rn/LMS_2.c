/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup LMS_2 LMS_2
\ingroup NN_Core

\brief Apprentissage type hebbien avec normalisation explicite des poids

\section Modified
\author Khursheed

\details

\section Activite 
* 
1) Remove (comment) the condition that "il faut que l'activite sur la voie inconditionnelle soit superieure a 0.2 en val abs"

2) Introduce Learning rate 

3) Introduce "forgetting"

4) Latex equations are given below


The activity of neurons in LMS-2 ($X''$)  can be computed using \f$y\rightarrow X''\f$ synapses and the learning of \f$y\rightarrow X''\f$ synaptic weights can be computed by equations given below;{FOA_weight_last}.

\f[
X''_{i}(t)= \sum_{k\epsilon y}  W_{y_k-X''_i} \: \: y_k 
\f]


\f[
W_{y_j-X''_i} = \frac{ W_{y_j-X''_i}^{*}}{\sum_{k\epsilon y}  W_{y_j-X''_i}^{*} }
\f]

\f[
W_{y_j-X''_i}^{*} (t+dt) =  W_{y_i-X''_i} (t) + \epsilon \:\: y_j \: U_i 
\f]

\f[
U_{i} = \sum_{m\epsilon X} W_{X_m-X''_i}  \:\: X_{m}
\f]

Where $X''_{i}$ is the activity of neuron in $X''$ group,  $y_{k}$ is the neuron of conditional group 
 (e.g. Oscillator) and $U_{i}$ is inconditional stimulus (e.g. Image (X)). $X_m$ is the 
 activities in inconditional group (X). $W_{y_j-X''_i}$ represents the synaptic weights from 
 oscillator (y) neuron j to LMS (X'') neuron i, unnormalized weights are shown with `` $*$ ''.

\section old

Calcul de type produit classique (pas de prise en compte de l'entree inconditionnelle).
Mode possible : 
- mode produit apres competition (=0): utilise .s en entree
- mode produit avant competition (=2): utilise .moy soit .s1 en entree.

ATTENTION : Le calcul du produit des entrees inconditionnelles se fait uniquement sur s1 !!

ATTENTION : Il faut utiliser produit apres competition (mode=0) si on veut que l'apprentissage soit possible.

Calcul de l'activite \f$ S \f$ de sortie a partir des entrees \f$ X \f$ conditionnelles:
\f[
S_j = \sum_{i \mbox{ conditionels}} X_i.W_{ij}
\f]

Calcul de l'energie \f$ E \f$ de l'entree (uniquement si mode=0, si mode=2, alors \f$ E_j=0 \f$ ):
\f[
E_j = \sum_{i} X_i
\f]

Calcul de la voie inconditionnelle \f$ U \f$ :
\f[
U_j = \sum_{i \mbox{ inconditionnels}} X_i.W_{ij}
\f]

\section Apprentissage

Cas ou le systeme n'apprend pas:
- Si \f$ |U_j| <0.2 \f$
- Si \f$ E_j  \f$ est tres proche de 0

Regle d'apprentissage des poids \f$ W \f$ :
\f[
\epsilon_j = \frac{\lambda}{E_j}
\f]
\f[
W_{ij} = W_{ij} + \epsilon_j \cdot U_j \cdot X_i
\f]

NB: \f$ \lambda \f$ est le resultat d'une neuromodulation globale (1/3 par defaut) correspondant au produit du poids de la connection par la valeur du premier neurone.

Normalisation des poids:
\f[
W_{ij} = \frac{W_{ij}}{\sum_{i}{W_{ij}}}
\f]

\section Links

- lien de neuromodulation globale \<lambda\>.


\section TODO

- Il y a de fortes incoherences dans la facon de selectionner quelles sont les voies de sortie (.s, .s1) utilisees en entree avec de gros a priori sur les conditions dans lesquels l'apprentissage est possible. A ameliorer.




\section old_doc Old Doc (Incorrecte)

La nouvelle doc a ete realisee apres une lecture rapide du code. Pour le moment, l'ancienne doc est conservee ci dessous.

>>>>>>>>>>>>>>>>>>>>>

 LMS_2 par B. SIRI

	Contrairement a ce que son nom indique, 
il ne s'agit plus vraiment d'un LMS car la formule d'apprentissage 
n'est plus la descente du gradient d'erreur. 
Ici l'apprentissage necessite la co-occurence de deux informations, 
ce qui la rapproche d'une formule hebbienne. 
Toutefois, il s'agit d'un groupe apprenant par conditionnement. 
Le code du LMS a de plus ete largement repris.
	Ce groupe apprend a conditionner les entrees qu'il percoit 
par rapport a un stimulus inconditionnel. Seul le neurone stimule apprend. 
Il est possible a un neurone de
desapprendre en lui faisant apprendre autre chose. 
Il est egalement possible de desapprendre grace a un stimulus inconditionnel 
negatif, mais la convergence de l'activite n'est pas garantie...
	Ce groupe stabilise les poids appris en les normalisant 
apres apprentissage. La quantite d'information qu'apporte la nouvelle entree 
apprise est gere par un parametre lambda. Plus lambda est fort et plus 
l'apprentissage favorisera la nouveaute en oubliant le passe. 
A l'inverse, plus lambda est faible et plus l'apprentissage sera stable. 
Par defaut, lambda vaut 1/3. Cela signifie que la somme des gains 
des poids apres apprentissage d'une nouvelle entree represente 1/3 
de la somme des poids appris (apres normalisation). 
Il est possible de parametrer lambda en ajoutant un lien neuromodulation 
appele lambda.

	*La formule d'activation des neurones est classique :
	
	Xj = SUM(i)( Xi.Wij) + Uj
	
	avec Xj un neurone du LMS_2, Xi un neurone d'entree conditionnel 
et Uj le stimulus inconditionnel du neurone j.
	
	*apprentissage :
	
	Yij(t+1) = Wij(t) + epsilon.Uj.Xj
	Wij(t+1) = Yij(t+1) / SUM(z)(Yzj(t+1))
	
	avec espilon calcule de la maniere suivante :
	
	E = SUM(i)(Xi)			(energie totale de l'activite d'entree) ok si Uj =1 sinon il faut augmenter E par idee: en multipliant par uj -> epsilon augmente (rescale)
	epsilon= lambda / E 
	
			-----------------------------------

Remarques sur l'implementation :

	L'energie E servant a calculer epsilon est calculee dans la mise a jour 
des neurones et stocke dans le champs neurone.last_activation. 

PG: il faut que l'activite sur la voie inconditionnelle soit superieure a 0.2 en val abs 

>>>>>>>>>>>>>>>>>>>>>>

\file
\ingroup LMS_2

*/

/* #define DEBUG */

#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <string.h>


#define DEFAULT_LAMBDA	0.33


typedef struct
{
  noeud_modulation *ptr_lambda;
} LMS2_data;



void new_LMS_2(int gpe)
{
    LMS2_data * my_data = NULL;
    noeud_modulation *ptr_tmp_lambda=NULL;

    dprints("new_LMS_2 %d\n",gpe);
         
    /*obtention et stockage d'un pointeur permettant la neuromodulation de la vitesse d'apprentissage*/
    ptr_tmp_lambda = get_noeud_modulation_ptr(gpe,"lambda");

    my_data = ALLOCATION(LMS2_data);
    my_data->ptr_lambda=ptr_tmp_lambda;

    def_groupe[gpe].data = my_data;
    dprints("end of new_LMS_2\n");
}



void apprend_LMS_2(int gpe)
{
    int size_Y = def_groupe[gpe].tailley;
    int size_X = def_groupe[gpe].taillex;
    int first_neuron = def_groupe[gpe].premier_ele;

    int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
    int i;
    float myeps, sumweigth;
    type_coeff *coeff;
    
    float lambda = DEFAULT_LAMBDA;
    
    if( ((LMS2_data *) (def_groupe[gpe].data))->ptr_lambda != NULL){
      calcule_neuromodulation_fast( gpe ,((LMS2_data *) (def_groupe[gpe].data))->ptr_lambda,&lambda);
    }
    /* dprints("neuromodulation : lambda : %f\n",lambda);  */

    for (i = first_neuron; i < last_neuron; i++)
    {
        coeff = neurone[i].coeff;
        /** condition elimination */
        /* .d correspond au resultat sur la voie inconditionnelle */
    /*  if ((neurone[i].d < 0.2) && (neurone[i].d > -0.2)) 
        {
	   >> |activite voie incond| < 0.2
	  continue;
        }
        if (FLOAT_NEAR(neurone[i].last_activation, 0.))
        {*/
	  /** >> somme activite conditionnelle est (presque) 0*/
	  /* continue;
        } */
	/**fin conditions arret apprentissage */
        /**  myeps = lambda / energie */
        myeps = (float) (def_groupe[gpe].learning_rate * eps * lambda /neurone[i].last_activation);
         dprints("myeps : %f\n",myeps);  
        sumweigth = 0.0;
        /** maj poids */
        if(neurone[i].d>0.) /* unconditional input activated */ 
        {
			while (coeff != NULL)
			{
				if (coeff->evolution == 1)
				{
					coeff->val =
						coeff->val +
						neurone[i].d * myeps * neurone[coeff->entree].s1;
					sumweigth += coeff->val;
				}
				coeff = coeff->s;
			}
			/** normalize */
			if(sumweigth<0.1) continue;
			coeff = neurone[i].coeff;
			while (coeff != NULL)
			{
				if (coeff->evolution == 1)
				{
					coeff->val = (float) (coeff->val / sumweigth);
				}
				coeff = coeff->s;
			}
		}
        else /* forgetting */
        {
            coeff = neurone[i].coeff;
            while (coeff != NULL)
            {
                if (coeff->evolution == 1)
                {
                    coeff->val = coeff->val - 0.01*myeps * neurone[coeff->entree].s1  ;
                }
                coeff = coeff->s;
            }   
        }
    }

}



void mise_a_jour_neurone_LMS_2(int i, int gestion_STM, int learn)
{

    float s, sum_p, Sd;
    type_coeff *synapse;
    float input_value;

    (void) gestion_STM;
    (void)learn;


    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    Sd = 0.;                    /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
    sum_p = 0.0;
    synapse = neurone[i].coeff;
    neurone[i].last_activation = 0.0;
    while (synapse != NULL)
    {
      input_value = neurone[synapse->entree].s1;
        if (synapse->evolution != 0)
        {                       /** nous ne sommons pas le lien UC */
            switch (liaison[synapse->gpe_liaison].mode)
            {
            case 0:
                sum_p = sum_p + synapse->val * input_value;  /* ne devrait pas utiliser le champs s PG !!!! */
                /**calcul de E */
                neurone[i].last_activation += input_value;
                break;          /*s */
            case 2:
	      /** pas d'apprentissage possible avec produit apres competition !!*/
                sum_p = sum_p + synapse->val * input_value;    /*neurone[synapse->entree].s2; */
                break;          /*s2 */
            default:
                EXIT_ON_GROUP_ERROR(i, "Erreur mode liaison(%d) incorrect dans mise_a_jour_neurone_LMS_2, groupe %s\n",  liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
            }
        }
        else
        {
	  /** par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
            Sd = Sd + synapse->val * neurone[synapse->entree].s1;

            /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */
        }

        synapse = synapse->s;
    }
    s = neurone[i].s = sum_p;
    neurone[i].d = Sd;

    /** fonction rampe */
    if (s < 0.)
        neurone[i].s1 = neurone[i].s2 = 0.;
    else if (s > 1.)
        neurone[i].s1 = neurone[i].s2 = 1.;
    else
        neurone[i].s1 = neurone[i].s2 = s;

}
