/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup HEBB HEBB
 \ingroup NN_Core

 \brief Hebbian learning rule

 \section Remarque
 Ce groupe est defini par son type et pas par son nom (menu deroulant dans coeos)

 Calcul des activites :
 \f[
 s = \sum{W_j * S_j}

 s1 = seuil(s)

 s2 = s1
 \f]

 \section Apprentissage

 \f$ W_{ij} = \f$ coeff->val de la liaison entre le neurone d'entree i et le neurone du Hebb note j

 \f[
 dW_{ij} = -W_{ij} \cdot (\lambda_1 + \lambda_2 \cdot E_i + \lambda_3 \cdot S_j) + \epsilon \cdot E_i \cdot S_j
 \f]


 Utilise :

 Learning_rate du groupe, alpha du groupe (utilise uniquement pour le calcul de \f$ \lambda1 \f$)

 Neuromod globale : learn_factor, decay <participe dans le calcul de \f$ \lambda_1 \f$ >, decay2, decay3

 Neuromod locale : NEUROMOD_learn, NEUROMOD_decay <participe dans le calcul de \f$ \lambda_1 \f$ >, NEUROMOD_decay2, NEUROMOD_decay3


 Remarque : voir contenu du fichier pour plus de details sur le calcul des parametres. Le mode de fonctionnement est de prendre ce qui est disponible pour faire le calcul et si ce n'est pas present on considere que cela vaut 1.

 NB : Si ni le decay global ni le decay local sont definis alors il n'y a pas de decay pour le groupe.

 NB : La seule exception est que l'on peut definir un decay passif \f$ \lambda_1 \f$ sans liens de neuromod (globale ou locale) en definissant group->alpha !=0.

 NB : Pour avoir un decay passif ( \f$ \lambda1 != 0 \f$ ) il faut necessairement que group->alpha soit different de 0.



 \defgroup HEBB_threshold_binaire HEBB_threshold_binaire
 \ingroup NN_Core

 \brief Hebbian learning rule binarized
 \details
 Meme calcul d'activite et meme regles d'apprentissage que HEBB classique.
 Seuil du group : en dessous : sortie vaut 0, au dessus sortie vaut 1.

 \section Remarque
 Ce groupe est defini par son type et pas par son nom (menu deroulant dans coeos)
 \section Apprentissage

 Utilise :

 Learning_rate du groupe, alpha du groupe (decay)

 Neuromod globale : learn_factor, decay_factor

 Neuromod locale : NEUROMOD_learn, NEUROMOD_decay

 \defgroup HEBB_threshold HEEB_threshold
 \ingroup NN_Core
 \brief DEPRECATED: use HEBB instead.
 \deprecated use HEBB instead

\file  trad_neuron.c
\ingroup HEBB
\ingroup HEBB_threshold_binaire
\brief simulates kohonen neural network

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
-mise_a_jour_neurone_trad():

-apprend_normal(): NON cela a ete change !!! PG dec 2005 (utilisation alpha pour oubli)
	if(Si>0. && Sj>0.) dc=eps*Si*Sj;
       	else if(Si>0. && Sj<=0.) dc= -eps*Si;
        else if(Si<=0. && Sj>0.) dc= -eps*Sj;



Description:
This file contains the C fonction for traditional neural network.
We can find mise_a_jour_neurone_trad(), and apprend_normal(),
which respectivly enables the network to be updated and to learn.

 Calcul des activites :
 \f[
 s = \sum{W_j * S_j}
 s1 = seuil(s)
 s2 = s1
 \f]

 \section Apprentissage

 \f$ W_{ij} = \f$ coeff->val de la liaison entre le neurone d'entree i et le neurone du Hebb note j

 \f[
 dW_{ij} = -W_{ij} \cdot (\lambda_1 + \lambda_2 \cdot E_i + \lambda_3 \cdot S_j) + \epsilon \cdot E_i \cdot S_j
 \f]


 Utilise :

 Learning_rate du groupe, alpha du groupe (decay)

 Neuromod globale : learn_factor, decay <participe dans le calcul de \f$ \lambda_1 \f$ >, decay2, decay3

 Neuromod locale : NEUROMOD_learn, NEUROMOD_decay <participe dans le calcul de \f$ \lambda_1 \f$ >, NEUROMOD_decay2, NEUROMOD_decay3


 (isdef(XXX) ? val1 : val2) resume le fait que si le lien utilisant/defini par XXX est present, alors on utilise la valeur val1 sinon, on utilise la valeur val2.

 \f$ \epsilon = \f$ learning_rate * (isdef(learn_factor) ? learn_factor : 1) * (isdef(NEUROMOD_learn) ? NEUROMOD_learn : 1)
 \f$ \lambda1 = \f$ alpha * (isdef(decay) ? decay : 1) * (isdef(NEUROMOD_decay) ? NEUROMOD_decay : 1)
 \f$ \lambda2 = \f$ (isdef(decay2) ? decay2*(isdef(NEUROMOD_decay2) ? NEUROMOD_decay2 : 1) : (isdef(NEUROMOD_decay2) ? NEUROMOD_decay2 : 0))
 \f$ \lambda3 = \f$ (isdef(decay3) ? decay3*(isdef(NEUROMOD_decay3) ? NEUROMOD_decay3 : 1) : (isdef(NEUROMOD_decay3) ? NEUROMOD_decay3 : 0))

 Exemple :
Si un lien de neuromod globale decay3 est defini et qu'il n'y a pas de lien de neuromod local decay3 alors le calcul de \f$ \lambda3 \f$ est le suivant : decay*1 i.e. c'est la valeur donnee par le lien de neuromod decay3.


Global variables:
- created: none
- used: none, I think

Macro:
none

Internal Tools:
-calcule_produit.h()
-fonction()

External Tools:
-

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs:
apprend_kohonen: there must be an error, faire attention a ne garder que max
		 pb lorsque liaison non modifiable on ne passe pas dans la boucle

Todo: see if the bugs still happen


http://www.doxygen.org
************************************************************/

/* #define DEBUG 1 */

#include <libx.h>
#include <stdlib.h>

#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <NN_Core/rien.h>

#include "include/calcule_produit.h"
#include "include/fonction.h"


/*--------------------------------------------------------------------*/
/*  Utilise par le hebb_seuil                                         */
/*--------------------------------------------------------------------*/
/* mise a jour s1 rajoutee 8 juin 99       */
/* mise a jour s rajoute  9 decembre 99    */
/* mise a  decembre 1999, s ne represente plus le passe  */

void mise_a_jour_neurone_trad(int i, int gestion_STM, int learn)
{
   /*type_coeff *coeff; */
   float sortie = 0.f, sortie1 = 0.f;

   /*neurone[i].d=neurone[i].s1; *//* sauvegarde de la sortie passee : modif PG pour test (transparent) */
   sortie1 = calcule_produit(i, gestion_STM, learn);
   neurone[i].s = sortie1;
   sortie = rampe(sortie1 - neurone[i].seuil);

   neurone[i].s2 = neurone[i].s1 = sortie;

}

/* utilise par No_Ou ou No_Et ou No_Hebb_Seuil_binaire */
void mise_a_jour_neurone_hebb_seuil_binaire(int i, int gestion_STM, int learn)
{
   float sortie = 0.f, sortie1 = 0.f;

   neurone[i].d = neurone[i].s1;   /* sauvegarde de la sortie passee : modif PG pour test (transparent) */

   sortie1 = calcule_produit(i, gestion_STM, learn);
   neurone[i].s = sortie1;
   sortie = sortie1 - neurone[i].seuil;

   if (sortie > 0.)
   {
      neurone[i].s2 = neurone[i].s1 = 1.;
   }
   else
   {
      neurone[i].s2 = neurone[i].s1 = 0.;
   }
   /* if(neurone[i].s2>0)printf("neurone[%d].s2=%f,s1=%f,s=%f,w=%f,w=%f\n",i-def_groupe[neurone[i].groupe].premier_ele,neurone[i].s2,neurone[i].s1,neurone[i].s,neurone[i].coeff->val,(neurone[i].coeff->s)->val); */

}

void mise_a_jour_neurone_hebb(int i, int gestion_STM, int learn)
{
   float sortie = 0.f, sortie1 = 0.f;

   neurone[i].d = neurone[i].s1;   /* sauvegarde de la sortie passee : modif PG pour test (transparent) */

   sortie1 = calcule_produit(i, gestion_STM, learn);
   neurone[i].s = sortie1;
   sortie = rampe(sortie1 - neurone[i].seuil);

   neurone[i].s2 = neurone[i].s1 = sortie;
}

/* cas appele symetrie */
void mise_a_jour_neurone_hopfield(int i, int gestion_STM, int learn)
{
   float sortie = 0.f, sortie1 = 0.f;

   sortie1 = calcule_produit(i, gestion_STM, learn);
   neurone[i].s = sortie1;
   sortie = sortie1 - neurone[i].seuil;

   if (sortie < 0.)
   {
      neurone[i].s2 = neurone[i].s1 = -1.;
   }
   else
   {
      neurone[i].s2 = neurone[i].s1 = 1.;
   }

}



typedef struct MyData_HEBB
{
   noeud_modulation *speed_rate_ptr;
   noeud_modulation *decay_rate_ptr;
   noeud_modulation *decay2_rate_ptr;
   noeud_modulation *decay3_rate_ptr;
   type_coeff **NM_link_tab;
} MyData_HEBB;


void new_hebb(int gpe)
{
   MyData_HEBB * my_data = NULL;
   noeud_modulation *ptr_tmp_speed=NULL;
   noeud_modulation *ptr_tmp_decay=NULL;
   noeud_modulation *ptr_tmp_decay2=NULL;
   noeud_modulation *ptr_tmp_decay3=NULL;

   dprints("new_Hebb %d\n",gpe);

   /*si pas de learning rate, y'aura pas d'apprentissage*/
   if (def_groupe[gpe].learning_rate<=0.)
   {
      def_groupe[gpe].appel_apprend=rien;
      dprints("\t Learning rate nul -> apprentissage=rien groupe Hebb %d\n",gpe);
   }

   /*obtention et stockage d'un pointeur permettant la neuromodulation de la vitesse d'apprentissage*/
   ptr_tmp_speed = get_noeud_modulation_ptr(gpe,"learn_factor");
   ptr_tmp_decay = get_noeud_modulation_ptr(gpe,"decay_factor");
   if (ptr_tmp_decay != NULL)
      EXIT_ON_ERROR("Deprecated use of decay_factor. Learning rule was changed to enable stability, check the documentation or the code. Possible neuromodulations are now 'decay', 'decay2' and 'decay3'. Settings corresponding to former use of 'decay_factor' is : (1) group.alpha=0 and (2) renaming decay_factor into decay3.");
   ptr_tmp_decay = get_noeud_modulation_ptr(gpe,"decay");
   ptr_tmp_decay2 = get_noeud_modulation_ptr(gpe,"decay2");
   ptr_tmp_decay3 = get_noeud_modulation_ptr(gpe,"decay3");

   my_data = ALLOCATION(MyData_HEBB);
   my_data->speed_rate_ptr = ptr_tmp_speed;
   my_data->decay_rate_ptr = ptr_tmp_decay;
   my_data->decay2_rate_ptr = ptr_tmp_decay2;
   my_data->decay3_rate_ptr = ptr_tmp_decay3;
   /** get neuromodN link tab */
   my_data->NM_link_tab=get_local_neuromod_link_tab(gpe);
   def_groupe[gpe].data = (MyData_HEBB *) my_data;

   /** for each NM link list, delete unused neuromod links - if
    * any
    Example to be uncommented and adapted:
    for(i=0; i<def_groupe[gpe].nbre;i++) {
      delete_local_neuromod_link(&(my_data->NM_link_tab[i]),NEUROMOD_decay);
    }
   ****/

   dprints("end of new_Hebb\n");
}

/*--------------------------------------------------------------------*/
/* nouvelle version : inclus aussi  apprend_hebb_seuil_accumul(int gpe)*/
/* utilise aussi pour hopfield avec des entrees/sorties dans [-1,1].   */

void apprend_hebb(int gpe)
{
   int i;
   float dc, Si, Sj;
   float w;
   type_coeff *coeff;
   int deb, nbre;

   float _speed_rate = 1.;
   float _decay_rate = 1., _decay2_rate = 1., _decay3_rate = 1.;
   float _gpe_decay, _gpe_decay2, _gpe_decay3;
   float decay=1., decay2=0., decay3=0.;
   float n_eps=1., n_decay=1., n_decay2=0., n_decay3=0.;

   float _gpe_eps;
   float learn=1.;

   type_coeff **NM_link_tab;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;


   NM_link_tab=((MyData_HEBB *) (def_groupe[gpe].data))->NM_link_tab;

   if ( ((MyData_HEBB *) (def_groupe[gpe].data))->speed_rate_ptr != NULL)
   {
      if ( ! calcule_neuromodulation_fast( gpe ,((MyData_HEBB *) (def_groupe[gpe].data))->speed_rate_ptr , &_speed_rate ) ) EXIT_ON_ERROR("Erreur de neuromodulation learn_factor dans le Hebb gpe %s\n",def_groupe[gpe].no_name);
   }
   if ( ((MyData_HEBB *) (def_groupe[gpe].data))->decay_rate_ptr != NULL)
   {
      if ( ! calcule_neuromodulation_fast( gpe , ((MyData_HEBB *) (def_groupe[gpe].data))->decay_rate_ptr ,&_decay_rate ))  EXIT_ON_ERROR("Erreur de neuromodulation decay_rate dans le Hebb gpe %s\n",def_groupe[gpe].no_name);
      /** global decay rate is defined. Default local decay is set to 1*/
      n_decay=1.;
   }
   if ( ((MyData_HEBB *) (def_groupe[gpe].data))->decay2_rate_ptr != NULL)
   {
      if ( ! calcule_neuromodulation_fast( gpe , ((MyData_HEBB *) (def_groupe[gpe].data))->decay2_rate_ptr ,&_decay2_rate )) EXIT_ON_ERROR("Erreur de neuromodulation decay2_rate dans le Hebb gpe %s\n",def_groupe[gpe].no_name);
      /** global decay2 rate is defined. Default local decay2 is set to 1*/
      n_decay2=1.;
   }
   if ( ((MyData_HEBB *) (def_groupe[gpe].data))->decay3_rate_ptr != NULL)
   {
      if ( ! calcule_neuromodulation_fast( gpe , ((MyData_HEBB *) (def_groupe[gpe].data))->decay3_rate_ptr ,&_decay3_rate )) EXIT_ON_ERROR("Erreur de neuromodulation decay3_rate dans le Hebb gpe %s\n",def_groupe[gpe].no_name);
      /** global decay3 rate is defined. Default local decay3 is set to 1*/
      n_decay3=1.;
   }



   _gpe_eps = eps * def_groupe[gpe].learning_rate * _speed_rate ;

   _gpe_decay = def_groupe[gpe].alpha * _decay_rate ;
   _gpe_decay2 = _decay2_rate ;
   _gpe_decay3 = _decay3_rate ;

   /* cprints("Global learn factor %f, global decay factor %f, global decay2 factor %f, global decay3 factor %f\n",_gpe_eps,_gpe_decay,_gpe_decay2,_gpe_decay3); */

   dc = 0.;
   for (i = 0; i < nbre; i++)  /* modifie les poids en consequence */
   {
      coeff = neurone[i+deb].coeff;
      get_local_neuromod(NM_link_tab[i],NEUROMOD_learn,&n_eps);
      get_local_neuromod(NM_link_tab[i],NEUROMOD_decay,&n_decay);
      get_local_neuromod(NM_link_tab[i],NEUROMOD_decay2,&n_decay2);
      get_local_neuromod(NM_link_tab[i],NEUROMOD_decay3,&n_decay3);
      learn=n_eps*_gpe_eps;
      decay=n_decay*_gpe_decay;
      decay2=n_decay2*_gpe_decay2;
      decay3=n_decay3*_gpe_decay3;
      /* cprints("(%s):[%d]:eps %f, lambda1 %f, lambda2 %f, lambda3 %f\n",def_groupe[gpe].no_name,i,learn,decay,decay2,decay3); */
      while (coeff_is_valid(coeff))
      {
         if (coeff->evolution == 1)  /* si coeff modifiable */
         {
            Si = neurone[deb+i].s1; /* S_j */
            Sj = neurone[coeff->entree].s1; /* E_i */
            w = coeff->val;
            dc = learn*Si*Sj - w*(decay + decay2*Sj + decay3*Si);
            coeff->val = w + dc;
            /* cprints("(%s):[%d]:Si %f, Sj %f,dc %f\n",def_groupe[gpe].no_name,i,Si,Sj,dc); */
         }
         coeff = coeff->s;
      }
   }
}

void destroy_hebb(int gpe)
{
   type_coeff **NM_link_tab;
   if (def_groupe[gpe].data!=NULL)
   {
      dprints("In destroy_hebb (%s) : free NM_link_tab\n",def_groupe[gpe].no_name);
      NM_link_tab=((MyData_HEBB *)def_groupe[gpe].data)->NM_link_tab;
      if (NM_link_tab!=NULL) free(NM_link_tab); /** libere NM_link_tab*/         
      NM_link_tab=NULL;
   }
   dprints("out of destroy hebb\n");
}
