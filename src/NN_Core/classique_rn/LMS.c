/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup LMS LMS
 \ingroup NN_Core

 \brief Conditioning inspired from widrow & Hoff rule

 \details
 It is a LMS with a cliped output [0,1].
 The output does not use the uncontitional link. (contrary to Pavlov)

 \section Learning

 \f[
 \Delta W = \epsilon . (Sd - S)
\f]
\f[
 Sd = \sum_{i \in unlearning\_link}{s1_i . W_i}
 \f]
\f[
 S = \sum_{i \in learning\_link}{smoy_i . W_i}
 \f]
\f[
 \epsilon = eps . learning\_rate . learn\_factor . local\_neuromodulation
 \f]

 - eps is a global variable defined in \<file\>.config
 - learning_rate is defined in the group.
 - learn_factor is a optional neuro_modulation of the group [global neuro modulation link (virtual)].
 - local_neuromodulation is a optional neuromodulation neuron by neuron [learn_factor [neuromod]].
ATTENTION: Si un LMS n'apprend pas pour une raison non déterminée, il est possible que ce soit un mauvais réglage du .config
			* Dans le .config, la ligne "echelle de temps pour l'apprentissage " permet de régler à partir de quelle echelle le groupe apprend. 
			* Autremment dit, si dans le script 3 echelles sont definient (la 0, 1, 2), le fait de mettre : echelle de temps pour l'apprentissage = 1 fait que les groupes qui sont à l'echelle 2 n'apprennent pas.

 \file
 \ingroup LMS
 */

/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <NN_Core/rien.h>

#include "include/gradient_lms.h"

typedef struct MyData_LMS {
	noeud_modulation *speed_rate_ptr;
	type_coeff **NM_link_tab; /** local NeuroMod link tab*/
} MyData_LMS;

/**Fonction d'initialisation du groupe de type LMS
 * sert actuellement a stocker la neuromodulation "learn_factor"
 * agissant sur la vitesse d'apprentissage*/
void new_LMS(int gpe)
{
	MyData_LMS *my_data = NULL;
	noeud_modulation *ptr_tmp = NULL;

	dprints("new_LMS %d\n", gpe);

	/**si pas de learning rate, y'aura pas d'apprentissage*/
	if (def_groupe[gpe].learning_rate <= 0.)
	{
		def_groupe[gpe].appel_apprend = rien;
		dprints("\t Learning rate nul -> apprentissage=rien groupe LMS %d\n", gpe);
	}

	/**Obtention et stockage d'un pointeur permettant la neuromodulation de la vitesse d'apprentissage*/
	ptr_tmp = get_noeud_modulation_ptr(gpe, "learn_factor");

	my_data = ALLOCATION(MyData_LMS);

	my_data->speed_rate_ptr = ptr_tmp;
	my_data->NM_link_tab = get_local_neuromod_link_tab(gpe);
	def_groupe[gpe].data = (MyData_LMS *) my_data;

	/*
	 if(continue_simulation_status==START)
	 for(i=0;i<def_groupe[gpe].nbre;i++)
	 {
	 coeff=neurone[i+def_groupe[gpe].premier_ele].coeff;
	 while(coeff!=NULL)
	 {
	 if(coeff->evolution==1)
	 coeff->val=liaison[coeff->gpe_liaison].norme;
	 coeff=coeff->s;
	 }
	 }*/
}

/**fonction d'apprentissage du LMS
 * Vitesse d'apprentissage dependant du eps global, du learning_rate du groupe, et d'une 
 * eventuelle neuromodulation appelee "learn_factor"*/
void apprend_LMS(int gpe)
{

	float gradient, _Var;
	int size_Y = def_groupe[gpe].tailley;
	int size_X = def_groupe[gpe].taillex;
	int first_neuron = def_groupe[gpe].premier_ele;
	int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
	int i;
	float _speed_rate = 1;
	float n_eps = 1.;

	type_coeff **NM_link_tab;

	NM_link_tab = ((MyData_LMS *) (def_groupe[gpe].data))->NM_link_tab;

	if (((MyData_LMS*) def_groupe[gpe].data)->speed_rate_ptr != NULL) if (!calcule_neuromodulation_fast(gpe, ((MyData_LMS *) (def_groupe[gpe].data))->speed_rate_ptr, &_speed_rate))
	{
		dprints("Erreur de neuromodulation dans le LMS gpe %d\n", gpe);
	}

	_Var = def_groupe[gpe].learning_rate * eps * _speed_rate;

	for (i = first_neuron; i < last_neuron; i++)
	{
		get_local_neuromod(NM_link_tab[i - first_neuron], NEUROMOD_learn, &n_eps);
		gradient = neurone[i].d;
		/**mise a jour des poids sur les entrees Conditionnelles du neurone i */
		gradient_lms(i, gradient, _Var * n_eps);
	}
}

/**fct de mise a jour de neurones de type LMS*/
void mise_a_jour_neurone_LMS(int i, int gestion_STM, int learn)
{
	float s, sum_p, Sd;
	type_coeff *synapse;
	(void) gestion_STM;
	(void) learn;

	neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
	Sd = 0.; /**activite sur la ou les voie(s) inconditionnelle(s), modif PG */
	sum_p = 0.0;
	synapse = neurone[i].coeff;
	while (coeff_is_valid(synapse))
	{
		if (synapse->evolution != 0)
		{ /** nous ne sommons pas le lien UC */
			switch (liaison[synapse->gpe_liaison].mode)
			{
			case 0:
				/*sum_p = sum_p + synapse->val * synapse->moy;*//*Suppresion du moyennage des entrees. Entree prise directement sur s1*/
				sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
				break;
			case 2:
				sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
				break;
			default:
				EXIT_ON_ERROR("Erreur mode liaison(%d) incorrect dans mise_a_jour_neurone_LMS, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
			}
		}
		else
		{
			/** par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
			Sd = Sd + synapse->val * neurone[synapse->entree].s1;
		}
		synapse = synapse->s;
	}
	 neurone[i].s = sum_p;
	 s = neurone[i].s;

	neurone[i].d = Sd - s;

	/* fonction rampe */
	if (s < 0.) neurone[i].s1 = 0.;
	else if (s > 1.) neurone[i].s1 = 1.;
	else neurone[i].s1 = s;

	/**Mouai ca c'est a change parceque pas vrai LMS (on seuil pas: sinon Delta(W)=epsilon*(Sd-f(Sum(wK*xK)) !! */
	/**bref faut pas du tout seuile (vrai regle je crois) ou au pire on seuil mais pas juste la sortie globale et pas pour l'apprentissage */

}

void destroy_lms(int gpe)
{
	type_coeff **NM_link_tab;
	if (def_groupe[gpe].data != NULL)
	{
		dprints("In destroy_lms (%s) : free NM_link_tab\n", def_groupe[gpe].no_name);
		NM_link_tab = ((MyData_LMS *) def_groupe[gpe].data)->NM_link_tab;
		if (NM_link_tab != NULL) /** libere NM_link_tab*/
		free(NM_link_tab);
	}
	dprints("out of destroy lms\n");
}
