/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  winner.c
\brief simulates winner_takes_all neural network

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
-gestion_groupe_winner:
-apprend_winner:


Description:
This file contains the C fonction for winner_takes_all neural network.
We can find gestion_groupe_winner() and apprend_winner(),
which respectivly enables to be treated as a special group, and to learn.
The updates is done traditonaly. As it is a winner_takes_all neural network, gestion_group_winner finds
the most activated neuron, raises its activity to one and downs the other to zero. Learning is then realised.
-apprend_winner:
 * apprentissage du lien entre l'entree activee et la cellule gagnante
 * desaprentissage des liens reliant des entrees/sorties non correlles
-gestion_groupe_winner:
Generic Winner take all management.
Read the .s field of the neurons of the group.
Simply activate the max (i.e. .s1 and .s2 fields
are set to 1.0). Other neurons are inactive (i.e
.s1 and .s2 fields are set to 0.0).


Global variables:
- created: none Should seldomly happen!!!
- used: none, I think

Macro:
none

Internal Tools:
-none

External Tools:
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs:

Todo: see if the bugs still happen

http://www.doxygen.org
************************************************************/

#include "libx.h"
#include <Kernel_Function/trouver_entree.h>
#include <string.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <stdlib.h>
#include "include/calcule_produit.h"
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <NN_Core/rien.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
/* #define DEBUG*/

typedef struct MyData_Winner
{
   noeud_modulation *nb_winners_ptr;
} MyData_Winner;

void new_winner(int gpe)
{
   MyData_Winner *my_data = NULL;
   noeud_modulation *ptr_tmp_1=NULL;

   if (def_groupe[gpe].learning_rate<=0.)
   {
      def_groupe[gpe].appel_apprend=rien;
      printf("\t Learning rate nul -> apprentissage=rien %d\n",gpe);
   }

   ptr_tmp_1 = get_noeud_modulation_ptr(gpe,"nb_winners");

   if (ptr_tmp_1 == NULL) return;

   my_data = (MyData_Winner *) malloc(sizeof(MyData_Winner));
   if (my_data == NULL)
   {
      printf("echec malloc dans %s\n", __FUNCTION__);
      exit(0);
   }
   my_data->nb_winners_ptr = ptr_tmp_1;
   def_groupe[gpe].data = (MyData_Winner *) my_data;

}


void apprend_winner(int gpe)
{
   int i, pos = 0;
   float max, dc = 0., Si, Sj;
   float input_value;
   type_coeff *coeff;
   int deb, nbre;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   max = -99999999.;

   for (i = deb; i < deb + nbre; i++)  /* cherche le max */
   {
      if (neurone[i].s2 > max)
      {
         max = neurone[i].s2;
         pos = i;
      }
      neurone[i].s2 = 0;
   }

   if (max <= 0.)
      return;             /* modif sorin */
   neurone[pos].s2 = 1.;

   coeff = neurone[pos].coeff;
   Si = neurone[pos].s2;
   while (coeff != NULL)
   {
      input_value = neurone[coeff->entree].s1;
      if (coeff->evolution == 1)  /* si coeff modifiable */
      {
         dprints("Winner %d : le neurone %d apprend \n", gpe, pos - def_groupe[gpe].premier_ele);

         Sj = input_value;
         if (Sj > 0.)
         {
            dc = eps * Si * Sj;
         }
         else  dc = 0.;

         coeff->val = coeff->val + dc;
         /*              if(coeff->val>0.) sp=sp+coeff->val;
            else sm=sm-coeff->val; */

         /*       if(coeff->val>1) coeff->val=1;
            if(coeff->val<-1) coeff->val=-1;  */
      }
      coeff = coeff->s;
   }
   /*coeff = neurone[pos].coeff;
          while(coeff!=nil)
           {
           if(coeff->evolution==1)
           {
           if(coeff->val>0.) coeff->val=coeff->val/sp;
           else coeff->val=coeff->val/sm;
           }
           coeff=coeff->s;
           } */

   /* PG c'est anormal, l'apprentissage ne devrait induire un recalcul de tous les neurones apres coup */
   /* la sortie n'est pas le potentiel */

   def_groupe[gpe].appel_activite(pos,0,0);
   /*  neurone[i].s = neurone[i].s1 = calcule_produit(i, 0, 0);*/
}

void gestion_groupe_multiple_winner(int gpe)
{
   int i;
   int posmax = 0, nmax;
   float total;
   int deb, nbre, nbre_de_1;
   float sortie;
   int retour_neuro_mod;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   dprints("gestion_groupe_mutilple_winner %d  \n",gpe);

   if (def_groupe[gpe].nbre_de_1 <= 1)
      nbre_de_1 = 1;
   else
      nbre_de_1 = def_groupe[gpe].nbre_de_1;

   if ( def_groupe[gpe].data != NULL )
   {
      /* La neuromod 'nb_winners' est analogique */
      retour_neuro_mod =   calcule_neuromodulation_fast( gpe ,((MyData_Winner *) (def_groupe[gpe].data))->nb_winners_ptr, &sortie ) ;
      if ( ! retour_neuro_mod )
      {
         cprints("Erreur de neuromodulation nb_winners dans le Winner gpe %d\n",gpe);
      }
      else if (sortie>=0.)
      {
         /*pour arrondis à l'entier le plus proche*/
         nbre_de_1=(int)(sortie+0.5);
      }
   }

   /*printf("nb_winner(%s) = %d\n",def_groupe[gpe].no_name,nbre_de_1);*/

   for (i = deb; i < deb + nbre; i++)
   {
      neurone[i].s1 = neurone[i].s2 = 0.;
   }

   for (nmax = 0; nmax < nbre_de_1; nmax++)    /* recherche des max suivants nbre_de_1 donne le nbre de max suivant que l'on veut on peut le changer dans le script */
   {
      total = -1;
      posmax = -1.;
      /* cherche le max local au neurone n considere */
      for (i = deb; i < deb + nbre; i++)
      {
         if (neurone[i].s > total && neurone[i].s2 <= 0.)   /*  test sur .s2 si c'est à zero c'est que ce n'est pas un max que l'on a deja */
            /*  sélectionné donc on peut le selectionner*/
         {
            total = neurone[i].s;
            posmax = i;
         }
      }

      if (total > 0.)
      {
         /* PG pb: le winner ne tient pas compte du seuil groupe et son activite peut etre superieure a 1 !!! */
         if (total>1.) total=1.; /* agpeout PG pb... */
         neurone[posmax].s2 = neurone[posmax].s1 = total;
         if (nmax == 0)   neurone[posmax].s2 = 1.;

         dprints("le %d(%d) gagnant vaut %f \n", posmax - def_groupe[gpe].premier_ele, gpe, total);
      }
      else break;
   }
}

