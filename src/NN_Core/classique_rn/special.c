/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  special.c
\brief simulates special neural network

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
-Mise_a_jour_neurone_special:
	deux differents types de synapses :
	non modifiables : Unconditional Stimulus (US) evolution 0
	modifiables : Conditional Stimulus (CS) evolution 1

   	if (sumCS + sumUS > neurone[i].seuil)
    	neurone[i].s=neurone[i].s1=neurone[i].s2=1.0;
   	else neurone[i].s=neurone[i].s1=neurone[i].s2=0.0;

-gestion_groupe_special:
	jamais appelee
 -apprend_special:
 	Type de neurone recevant deux types de voies :
	conditionnnelle et unconditionnelle.
     	La presence de stimulus unconditionnel entraine l`apprentisage
     	des poids des liens conditionels coactifs.

Description:
This file contains the C fonction for special neural network.
We can find mise_a_jour_neurone_special(), gestion_groupe_special() and apprend_special(),
which respectivly enables the network to be updated, to be treated as a group, and to learn.


Global variables:
- created: none
- used: none, I think

Macro:
none

Internal Tools:
-none

External Tools:
-none

Links:
- type: xxxxxxxxxxx
- description: La presence de stimulus unconditionnel entraine l`apprentisage
               des poids des liens conditionels coactifs.
- input expected group: Type de neurone recevant deux types de voies :  conditionnelle et inconditionnelle
- where are the data?: xxxxxxxxxxxxxx

Comments: none

Known bugs: not tested

Todo: see if the bugs still happen
      see if an application uses special NN
      find the original programmer to see the file is well described


http://www.doxygen.org
************************************************************/

#include <libx.h>

#include <stdlib.h>

void apprend_Special(int gpe)
{
   type_coeff *synapse;
   /*float sumCS,sumUS,ep,inputActiv; */
   float Sd, dc /*,amod,act */ ;
   int size_Y = def_groupe[gpe].tailley;
   int size_X = def_groupe[gpe].taillex;
   int first_neuron = def_groupe[gpe].premier_ele;
   int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
   int i, cpt;

   Sd = 0.0;

   for (i = first_neuron; i < last_neuron; i++)
   {
      cpt = 0;
      /*recherche de l'entree UC (synapse non modifiable) */
      synapse = neurone[i].coeff;
      while (synapse != NULL)
      {
         if (synapse->evolution == 0)
         {
            Sd = neurone[synapse->entree].s2;
            cpt++;
         }
         synapse = synapse->s;
      }

      if (cpt > 1)
      {
         EXIT_ON_ERROR("**** aprend special: erreur!! il y a plus d'une sortie deiree cpt = %d ... ",cpt);
      }
      else if (cpt == 0)
      {
         EXIT_ON_ERROR("**** aprend special: erreur!! il y a pas de sortie desiree... "); 
      }


      /*mise a jour des poids sur les entrees C */
      synapse = neurone[i].coeff;
      while (synapse != NULL)
      {
         if ((synapse->type == 2) && (synapse->evolution == 1))  /*liens produit */
         {
            dc = eps * (Sd - neurone[i].s2) * neurone[synapse->entree].s2;
            synapse->val = synapse->val + dc;
         }

         if ((synapse->type == 3) && (synapse->evolution == 1))  /*liens distance */
         {
            dc = eps * (neurone[synapse->entree].s2 - synapse->val) * Sd;
            synapse->val = synapse->val + dc;
         }
         synapse = synapse->s;
      }
   }
}

void gestion_groupe_Special(int gpe)
{
   int i, win  ;
   float  max;
   int deb, nbre  ;


   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   max = -1.0e37;
   win = -1;

   for (i = deb; i < deb + nbre; i++)
   {
      if (neurone[i].s > max)
      {
         max = neurone[i].s;
         win = i;
      }
   }

   for (i = deb; i < deb + nbre; i++)
   {
      if (win == i)
      {
         neurone[i].s = neurone[i].s1 = neurone[i].s2 = 1.;
      }
      else
         neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
   }
}

void mise_a_jour_neurone_special(int i, int gestion_STM, int learn)
{

   float sum;
   type_coeff *synapse;
   (void)gestion_STM;
   (void)learn;

   neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
   sum = 0.0;
   synapse = neurone[i].coeff;
   while (synapse != NULL)
   {
      if ((synapse->type == 2) && (synapse->evolution == 1))
         sum += (synapse->val * neurone[synapse->entree].s2);
      if ((synapse->type == 3) && (synapse->evolution == 1))
         sum += fabs(synapse->val - neurone[synapse->entree].s2);
      synapse = synapse->s;
   }

   sum = 1. / (1. + sum);
   neurone[i].s = sum;
}
