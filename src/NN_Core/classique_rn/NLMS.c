/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** 
 \defgroup NLMS NLMS
 \ingroup NN_Core

 \author Julien Hirel
 \date 2010

 \brief Normalized Least Mean Square (NLMS) learning

 \details

 This group is an implementation of the Normalized Least Mean Square (NLMS) learning. It is basically a standard LMS with a normalization factor depending on the energy of the conditionnal inputs. Be careful, it is not a true LMS (threshold on the neuron activity and weights).

 A neuromodulation called "Ach" can be used to specify cholinergic
 modulation.  The group is then more selective and activates only
 co-activated neurons (with both unconditionnal and conditionnal inputs).

 Learning is modulated at the group level by the "learn_factor" neuromodulatory link and at the neuron level by the algorithmic link "local_modulation" with takes as input a group with the same size as the NLMS group.

 \file
 \ingroup NLMS
 \brief

 Author: Julien Hirel
 Created: 2010
 Modified:
 - author:
 - description:
 - date:

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 This group is an implementation of the Normalized Least Mean Square (NLMS) learning. It is basically a standard LMS with a normalization factor depending on the energy of the conditionnal inputs. Be careful, it is not a true LMS (threshold on the neuron activity and weights).

 A neuromodulation called "Ach" can be used to specify cholinergic
 modulation.  The group is then more selective and activates only
 co-activated neurons (with both unconditionnal and conditionnal inputs).

 Learning is modulated at the group level by the "learn_factor" neuromodulatory link and at the neuron level by the algorithmic link "local_modulation" with takes as input a group with the same size as the NLMS group.

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/find_input_link.h>

typedef struct data_NLMS {
  int gpe_local_mod;
  noeud_modulation *ptr_learn;
  noeud_modulation *ptr_Ach;
} data_NLMS;

/***************** Initialisation function ***************/
void new_NLMS(int gpe)
{
  data_NLMS * mydata;
  noeud_modulation *ptr_learn = NULL, *ptr_Ach = NULL;
  int index, link;
  int gpe_local_mod = -1;

  dprints("new_NLMS(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data == NULL)
  {
    ptr_Ach = get_noeud_modulation_ptr(gpe, "Ach");
    ptr_learn = get_noeud_modulation_ptr(gpe, "learn_factor");

    index = 0;
    link = find_input_link(gpe, index);
    while (link != -1)
    {
      if (strcmp(liaison[link].nom, "local_modulation") == 0)
      {
        gpe_local_mod = liaison[link].depart;
        if (def_groupe[gpe].nbre != def_groupe[gpe_local_mod].nbre)
        {
          EXIT_ON_ERROR("Input local_modulation group size (%i) and NLMS group size (%i) must be the same", def_groupe[gpe].nbre, def_groupe[gpe_local_mod].nbre);
        }
      }

      index++;
      link = find_input_link(gpe, index);
    }

    mydata = ALLOCATION(data_NLMS);

    mydata->ptr_learn = ptr_learn;
    mydata->ptr_Ach = ptr_Ach;
    mydata->gpe_local_mod = gpe_local_mod;

    def_groupe[gpe].data = mydata;
  }
  dprints("new_NLMS(%s): Leaving function\n", def_groupe[gpe].no_name);
}

#ifdef __GNUC__
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif

// -Wno-unused

/***************** Neural activity computation ***************/
void mise_a_jour_NLMS(int i,  int UNUSED(gestion_STM),  int __attribute__((__unused__)) learn )
{
  float output = 0., act_CS = 0., act_US = 0.;
  type_coeff *coeff = neurone[i].coeff;

  dprints("mise_a_jour_NLMS: MAJ neuron %i\n", i);

  while (coeff != NULL)
  {
    if (coeff->evolution == 0)
    {
      act_US += coeff->val * neurone[coeff->entree].s1;
    }
    else
    {
      act_CS += coeff->val * neurone[coeff->entree].s1;
    }

    coeff = coeff->s;
  }

  /* Marks neurons activated by unconditionnal links */
  neurone[i].d = act_US;

  output = act_CS - def_groupe[neurone[i].groupe].seuil;
  if (output < 0)
  {
    output = 0;
  }
  else if (output > 1)
  {
    output = 1;
  }

  /* Raw activity on s, threshold activity on s1 and s2 */
  neurone[i].s = act_CS;
  neurone[i].s1 = neurone[i].s2 = output;
}

/***************** Handling of neuromodulatory signals ***************/
void gestion_NLMS(int gpe)
{
  int i;
  float modulation_Ach = 0.;
  data_NLMS *mydata = (data_NLMS *) def_groupe[gpe].data;

  dprints("gestion_NLMS(%s): Entering function\n", def_groupe[gpe].no_name);

  if (mydata == NULL)
  {
    EXIT_ON_ERROR("Cannot retreive data (NULL pointer)");
  }

  if (mydata->ptr_Ach != NULL)
  {
    calcule_neuromodulation_fast(gpe, mydata->ptr_Ach, &modulation_Ach);
    if (modulation_Ach > 0.)
    {
      dprints("gestion_NLMS(%s): Ach modulation = %f\n", def_groupe[gpe].no_name, modulation_Ach);
    }

  }

  if (modulation_Ach > 0)
  {
    for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
    {
      /* Acetylcholine neuromodulation: inhibition of the non
       * co-activated neurons */
      if (neurone[i].d <= def_groupe[gpe].seuil)
      {
        neurone[i].s -= modulation_Ach;
        if (neurone[i].s < 0) neurone[i].s = 0.;

        neurone[i].s1 -= modulation_Ach;
        if (neurone[i].s1 < 0) neurone[i].s1 = 0.;

        neurone[i].s2 -= modulation_Ach;
        if (neurone[i].s2 < 0) neurone[i].s2 = 0.;
      }
      else
      {
        neurone[i].s1 += neurone[i].d;
        if (neurone[i].s1 > 1)
        {
          neurone[i].s1 = 1;
        }

        neurone[i].s2 += neurone[i].d;
        if (neurone[i].s2 > 1)
        {
          neurone[i].s2 = 1;
        }
      }
    }
  }
}

/***************** Learn function for all neurons ***************/
void apprend_NLMS(int gpe)
{
  int i, first, first_local_mod = -1;
  float sum_CS, dw, act_US = 0;
  float learn_factor = 1., local_learn_factor, sigma;
  type_coeff *coeff;
  data_NLMS *mydata = (data_NLMS *) def_groupe[gpe].data;

  if (mydata == NULL)
  {
    EXIT_ON_ERROR("Cannot retreive data");
  }

  first = def_groupe[gpe].premier_ele;

  /* Local learning modulation */
  if (mydata->gpe_local_mod >= 0)
  {
    first_local_mod = def_groupe[mydata->gpe_local_mod].premier_ele;
  }

  /* Global learning modulation */
  if (mydata->ptr_learn != NULL)
  {
    calcule_neuromodulation_fast(gpe, mydata->ptr_learn, &learn_factor);
  }

  learn_factor *= def_groupe[gpe].learning_rate;

  /* Small number used for normalized LMS to avoid division by zero */
  if (def_groupe[gpe].alpha > 0)
  {
    sigma = def_groupe[gpe].alpha;
  }
  else
  {
    sigma = 0.01;
  }

  /* Learning only occurs if the learning rate is > 0 */
  if (learn_factor > 0.)
  {
    dprints("apprend_NLMS(%s): learn_factor = %f\n", def_groupe[gpe].no_name, learn_factor);

    for (i = 0; i < def_groupe[gpe].nbre; i++)
    {
      sum_CS = 0.;
      act_US = 0.;

      /* Modulation of the learning rate for each neuron */
      if (first_local_mod >= 0)
      {
        local_learn_factor = learn_factor * neurone[first_local_mod + i].s1;
      }
      else
      {
        local_learn_factor = learn_factor;
      }

      /* Computation of the sum of input activities, for normalization
       * purposes */
      coeff = neurone[first + i].coeff;
      while (coeff != NULL)
      {
        if (coeff->evolution == 1)
        {
          sum_CS += neurone[coeff->entree].s1 * neurone[coeff->entree].s1;
        }
        else
        {
          act_US += coeff->val * neurone[coeff->entree].s1;
        }

        coeff = coeff->s;
      }

      /* Updating of modifiable synaptic weights */
      coeff = neurone[first + i].coeff;
      while (coeff != NULL)
      {
        if (coeff->evolution == 1)
        {
          /* Normalized LMS learning rule */
          dw = local_learn_factor * (act_US - neurone[first + i].s) * neurone[coeff->entree].s1 / (sum_CS + sigma);
          coeff->val += dw;

          /* Only excitatory connections are allowed */
          if (coeff->val < 0)
          {
            coeff->val = 0;
          }
        }
        coeff = coeff->s;
      }
    }
  }
}

/******************** Clean up function *******************/
void destroy_NLMS(int gpe)
{
  if (def_groupe[gpe].data != NULL)
  {
    free((data_NLMS *) def_groupe[gpe].data);
    def_groupe[gpe].data = NULL;
  }

  dprints("destroy_NLMS(%s)\n", def_groupe[gpe].no_name);
}
