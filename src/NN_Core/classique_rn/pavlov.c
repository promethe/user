/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ****

 \brief

 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 -Mise_a_jour_neurone_widrow:
 deux differents types de synapses :
 non modifiables : Unconditional Stimulus (US) evolution 0
 modifiables : Conditional Stimulus (CS) evolution 1

 Pour les modes de calcul produit:
 \f[
 s = \sum{W_j * S_j}
 \f]

 Pour les modes de calcul distance:
 \f[
 s = \frac{1}{1+\sum{|W_j - S_j|}}
 \f]
 *
 *
 s1 = s2 =
 0 si s<= 0;
 1 si s>= 1;
 s sinon

 - j neurone d'entree de la liaison quelquesoit le type
 - \f$ W_j = \f$ coeff->val de la liaison reliee a j
 - Sj correspond a s ou s2 (selon le type de liaison) du neurone d'entree j.

 -apprend_widrow:
 - j neurone d'entree de la liason conditionnelle
 - \f$ W_j \f$ coeff->val de la liason du neurone courant avec le neurone j
 - S sortie du neurone courant(s) Comprend l'entree desiree (On devra la soustraire d'ou le S-Sd)
 - Sd sortie desiree (valeur de l'entree inconditionnelle)
 - \f$ S_j \f$: sortie du neurone j (s ou s2)
 - \f$ \epsilon \f$: vitesse d'apprentissage

 Description:
 This file contains the C fonction for widrow neural network.
 We can find mise_a_jour_neurone_widrow() and apprend_widrow(),
 which respectivly enables the network to be updated and to learn.
 Type de neurone recevant deux types de voies :
 conditionnnelle et unconditionnelle.
 La presence de stimulus unconditionnel entraine l`apprentisage
 des poids des liens conditionels coactifs.
 L'apprentissage se fait sur s pour les liasons de type 0 et 1, sur s2 pour les liasons de type 2 et 3.
 Pour les liasons de type 0 et 2 la correction est sous forme de produit:
 \f$ \Delta W_j=\epsilon*(Sd-(S-Sd))*S_j \f$

*/
/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>

#include "include/gradient_lms.h"

void apprend_widrow(int gpe)
{
	float gradient;
	int i;
	float Var; /* vitesse d'apprentissage */
	int size_Y, size_X, first_neuron, last_neuron;

	size_Y = def_groupe[gpe].tailley;
	size_X = def_groupe[gpe].taillex;
	first_neuron = def_groupe[gpe].premier_ele;
	last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);

	Var = eps * def_groupe[gpe].learning_rate; /* vitesse d'apprentissage */

	for (i = first_neuron; i < last_neuron; i++)
	{
		gradient = neurone[i].d;
		/*mise a jour des poids sur les entrees Conditionnelles du neurone i */
		gradient_lms(i, gradient, Var);
	}
}

void mise_a_jour_neurone_widrow(int i, int gestion_STM, int learn)
{
	float s, sf, sum_p;
	/*float tau; */
	type_coeff *synapse;
	float IR; /* inconditional response */
	int flag_IR;

	(void) gestion_STM;
	(void) learn;

	IR = 0.;
	flag_IR = 0;

	neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
	sum_p = 0.0;
	synapse = neurone[i].coeff;
	while (synapse != NULL)
	{
		if (synapse->evolution != 0) /* nous traitons ici les liens conditionnels CS */
		{
			switch (liaison[synapse->gpe_liaison].mode)
			{
			case 0:
				sum_p = sum_p + synapse->val * neurone[synapse->entree].s1; /*Suppresion du moyennage des entrees. Entree prise directement sur s1*/
				break;
			case 2:
				sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
				break;
			default:
				EXIT_ON_ERROR("Erreur mode liaison(%d) incorrect dans mise_a_jour_neurone_widrow, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
				break;
			}
		}
		else
		{
			IR = IR + synapse->val * neurone[synapse->entree].s1;
			flag_IR = 1;
		}
		synapse = synapse->s;
	}

	if (flag_IR == 0) EXIT_ON_ERROR("Erreur: mise_a_jour_neurone_widrow :%d, pas de lien inconditionnel... ", i);

	s = sum_p - neurone[i].seuil;
	if (s < 0.) sf = 0.;
	else if (s > 1.) sf = 1.;
	else sf = s;

	neurone[i].d = IR - sf;
	s = neurone[i].s = IR + sf;

	/* fonction rampe tenant compte du seuil */
	if (s < 0.) sf = 0.;
	else if (s > 1.) sf = 1.;
	else sf = s;

	neurone[i].s1 = neurone[i].s2 = sf;
}
