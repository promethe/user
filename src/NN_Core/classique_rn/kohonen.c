/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  kohonen.c
 \brief simulates kohonen neural network

 Author: xxxxxxxxx
 Created: xxxxxxxxxx
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 -Mise_a_jour_neurone_kohonen:
 * Calcul s du neurone i
 * \f$
 * s= \frac{1}{1+\sum{|W_j-s2_j|}}
 * \f$
 * - j neurone d'entree de la liason
 * - \f$ W_j = \f$ coeff->val de la liaison reliee a j
 -gestion_groupe_kohonen:
 * Pour chaque neurone, on calcul s1 en fonction (tableau_gaussienne: pseudo DOG dependant de dvp et dvn) de la 	distance au gagnant (s max).
 * On binarise s2 avec 1 pour le gagnant, 0 pour les autres.
 -apprend_kohonen:
 * Pour chaque neurone i du groupe on applique a chacun de ses coeff, la formule suivante:
 \f[ W_{ij} = W_{ij} + \varepsilon * (s2_j - W_{ij}*s1_i) \f]
 * - i neurone courant du groupe
 * - j neurone d'entree de la liason
 * - \f$ W_{ij} = \f$ coeff->val de la liaison entre i et j

 Description:
 This file contains the C fonction for kohonen neural network.
 We can find mise_a_jour_neurone_kohonen(), gestion_groupe_kohonen() and apprend_kohonen(),
 which respectivly enables the network to be updated, to be treated as a group, and to learn.

 Global variables:
 - created: none Should seldomly happen!!!
 - used: none, I think

 Macro:
 none

 Internal Tools:
 -none

 External Tools:
 -none


 Links:
 - type: xxxxxxxxxxx
 - description: none
 - input expected group: none
 - where are the data?: none

 Comments: none

 Known bugs:
 apprend_kohonen: there must be an error, faire attention a ne garder que max
 pb lorsque liaison non modifiable on ne passe pas dans la boucle

 Todo: see if the bugs still happen


 http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/

#include <libx.h>

typedef struct my_data_kohonen 
{
  int index_learning_box;
} my_data_kohonen;

void new_kohonen(int gpe)
{
  int index = 0;
  int link = find_input_link(gpe, index);
  char string[255];
  my_data_kohonen* my_data = NULL;
  srand(time(NULL));

  my_data = ALLOCATION(my_data_kohonen);
  my_data->index_learning_box = -1;
  def_groupe[gpe].data = my_data;

  while (link != -1)
  {
    if (prom_getopt(liaison[link].nom, "-learning_rate", string) >= 1)
    {
      printf("Boite learning en entrée du kohonen \n");
      my_data->index_learning_box = liaison[link].depart;
    }
    index++;
    link = find_input_link(gpe, index);
  }
}

void mise_a_jour_neurone_kohonen(int i, int gestion_STM, int learn)
{
  type_coeff *coeff;
  float sortie;
  const int nbre=neurone[i].nbre_coeff;
  int j;

  (void) gestion_STM;
  (void) learn;

#ifdef DEBUG
  /*  printf("Mise_a_jour_neurone_kohonen %d \n",i); */
#endif

  neurone[i].flag = 0;
  neurone[i].s1 = neurone[i].s2 = 0.0;
  neurone[i].max = 0;
  sortie = 0.;
  coeff = neurone[i].coeff;

  for(j=0;j<nbre;++j)
  {
    sortie = sortie + fabs(coeff->val - neurone[coeff->entree].s1);
    coeff++;
  }

  neurone[i].s = 1. / (1. + sortie);
}

void apprend_kohonen(int gpe)
{
  int i; /*!
   * Pour chaque neurone i du groupe on applique a chacun de ses coeff, la formule suivante:
   \f[ W_{ij} = W_{ij} + \varepsilon * (s2_j - W_{ij}*s1_i) \f]
   * - i neurone courant du groupe
   * - j neurone d'entree de la liason
   * - \f$ W_{ij} = \f$ coeff->val de la liaison entre i et j
   */
  float dc, Si, Sj;
  type_coeff *coeff;
  int deb, nbre;
  float speed;
  int nbre_coeff,j;
  my_data_kohonen *my_data = NULL;

  dprints("apprend Kohonen gpe %d \n", gpe);

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;

// On lit my_data si il existe, si c'est le cas on verifie qu'une learning_box est presente,
// si c'est le cas on prends la valeur de la boite comme learning_rate
  if (def_groupe[gpe].data != NULL) my_data = def_groupe[gpe].data;
  if (my_data != NULL)
  {
    if (my_data->index_learning_box != -1)
    {
      def_groupe[gpe].learning_rate = neurone[def_groupe[my_data->index_learning_box].premier_ele].s1;
    }
  }

  speed = def_groupe[gpe].simulation_speed * def_groupe[gpe].learning_rate * eps;

  for (i = deb; i < deb + nbre; i++)
  {
    if (isdiff(neurone[i].s1, 0.) && isdiff(eps, 0.))
    {
      coeff = neurone[i].coeff;
      nbre_coeff=neurone[i].nbre_coeff;
      for(j=0;j<nbre_coeff;++j)
      {
        if (coeff->evolution == 1)
        {
          Si = coeff->val;
          Sj = neurone[coeff->entree].s1; /* changement PG */
          dc = speed * (Sj - Si) * neurone[i].s1;
          coeff->val = Si + dc;
        }
        coeff++;
      }
    }
  }
}
void gestion_groupe_kohonen(int gpe)
{
  int i, win;
  float xg, yg, d, max;
  int deb, nbre /*,j */;

  dprints("gestion_groupe_kohonen %d \n", gpe);

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;

  max = neurone[deb].s;
  win = deb;

  for (i = deb + 1; i < deb + nbre; i++) /* Met dans win le neurone du groupe qui a le s max */
  {
    if (neurone[i].s > max)
    {
      max = neurone[i].s;
      win = i;
    }
    neurone[i].max = 0;
  }
  xg = neurone[win].posx;
  yg = neurone[win].posy;

  /*Updating .s1 and s2 */

  for (i = deb; i < deb + nbre; i++)
  {
    d = sqrt((float) ((neurone[i].posx - xg) * (neurone[i].posx - xg) + (neurone[i].posy - yg) * (neurone[i].posy - yg)));
    if (d >= taille_max_tableau_diff_gaussienne)
    {
      printf("Kohonen (gestion_kohonen, file classique_rn.c):\nerror: tableau_gaussienne[%d] defined in struct type_groupe isn't enough!!!", taille_max_tableau_diff_gaussienne);
    }
    neurone[i].s1 = def_groupe[gpe].tableau_gaussienne[(int) d];

    if (win == i)
    {
      neurone[i].s2 = 1.;
      neurone[i].s1 = 1.;
      neurone[i].s = 1.;
    }
  }
}

void destroy_kohonen(int gpe)
{
  free(def_groupe[gpe].data);
  def_groupe[gpe].data=NULL;
}
