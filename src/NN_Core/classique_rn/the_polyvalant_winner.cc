/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  winner.c
\brief simulates winner_takes_all neural network

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
-gestion_groupe_winner:
-apprend_winner:


Description:
This file contains the C fonction for winner_takes_all neural network.
We can find gestion_groupe_winner() and apprend_winner(),
which respectivly enables to be treated as a special group, and to learn.
The updates is done traditonaly. As it is a winner_takes_all neural network, gestion_group_winner finds
the most activated neuron, raises its activity to one and downs the other to zero. Learning is then realised.
-apprend_winner:
 * apprentissage du lien entre l'entree activee et la cellule gagnante
 * desaprentissage des liens reliant des entrees/sorties non correlles
-gestion_groupe_winner:
Generic Winner take all management.
Read the .s field of the neurons of the group.
Simply activate the max (i.e. .s1 and .s2 fields
are set to 1.0). Other neurons are inactive (i.e
.s1 and .s2 fields are set to 0.0).


Global variables:
- created: none Should seldomly happen!!!
- used: none, I think

Macro:
none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs:

Todo: see if the bugs still happen

http://www.doxygen.org
************************************************************/

#include "libx.h"
#include <Kernel_Function/trouver_entree.h>
#include <string.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <stdlib.h>
#include "include/calcule_produit.h"
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

/* #define DEBUG*/

 /* seul le gagnant apprend, les autres neurones ne sont pas modifies */


void rien(int gpe);
void gestion_groupe_generique_winner(int);
void apprend_winner_lieux_action(int gpe);
void apprend_winner_lieux_action2(int gpe);
void mise_a_jour_lieux_action(int n, int gestion_STM, int learn);
void mise_a_jour_lieux_action2(int n, int gestion_STM, int learn);
void gradient_lieux_action_inst(int i, float gradient, float Var,float var_accumul);
void gradient_lieux_action_accumul(int i, float gradient);
typedef struct MyData_winner
{
    int gpe_vigilence;
    int gpe_lieux;
    int gpe_action;
    int gpe_error;
    int gpe_adaptation;
    int lieux_max_prec;
    float seuil_apprend;

    int gpe_epsilon;
} MyData_winner;


void new_winner(int gpe)
{
    int l, i_liaison = 0, i;
    int gpe_vigilence = -1, gpe_lieux = -1, gpe_action = -1, gpe_error = -1,gpe_epsilon = -1;
    MyData_winner *my_data = NULL;
    prom_images_struct *lieux_action = NULL;
    type_coeff *coeff;
    int mode_la = 0;
    int lieux_max_prec = 0;
    float seuil_apprend = 0.00001;  /*0.01 */
    char param_link[32];
    int gpe_adaptation = -1;

    printf("%s %d\n", __FUNCTION__,gpe);

    l = find_input_link(gpe, i_liaison);
    while (l != -1)
    {
        if (strcmp(liaison[l].nom, "vigilence") == 0)
        {
            gpe_vigilence = liaison[l].depart;
        }
        if (strcmp(liaison[l].nom, "adaptation") == 0)
        {
            gpe_adaptation = liaison[l].depart;
        }
        if (strcmp(liaison[l].nom, "error") == 0)
        {
            gpe_error = liaison[l].depart;
        }
	if (strcmp(liaison[l].nom, "epsilon") == 0)
        {
            gpe_epsilon = liaison[l].depart;
        }
        if (strcmp(liaison[l].nom, "-la") == 0)
        {
            def_groupe[gpe].appel_apprend = apprend_winner_lieux_action;
            def_groupe[gpe].appel_activite = mise_a_jour_lieux_action;
// 	    def_groupe[gpe].appel_gestion=rien;
            mode_la = 1;
        }
	if (strcmp(liaison[l].nom, "-la2") == 0)
        {
            def_groupe[gpe].appel_apprend = apprend_winner_lieux_action2;
	    def_groupe[gpe].appel_activite = mise_a_jour_lieux_action2;
	    def_groupe[gpe].appel_gestion=rien;	
            mode_la = 2;
	    if(continue_simulation_status==START)
	    for(i=0;i<def_groupe[gpe].nbre;i++)
	    {
		coeff=neurone[i+def_groupe[gpe].premier_ele].coeff;
		while(coeff!=NULL)
		{
			if(coeff->evolution==1)
				coeff->val=liaison[coeff->gpe_liaison].norme;
			coeff=coeff->s;
		}
	    }
	    
        }
        if (prom_getopt(liaison[l].nom, "s", param_link) == 2)
        {
            seuil_apprend = atof(param_link);
        }
        if (strcmp(liaison[l].nom, "mod_winner_nb") == 0)
        {
            def_groupe[gpe].appel_gestion = gestion_groupe_generique_winner;
        }
        i_liaison++;
        l = find_input_link(gpe, i_liaison);

    }
    if (mode_la >= 1)
    {
        coeff = neurone[def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
            if (coeff->evolution == 1)
            {
                gpe_lieux = neurone[coeff->entree].groupe;
            }
            else
            {
                gpe_action = neurone[coeff->entree].groupe;
            }
            coeff = coeff->s;
        }
        lieux_action =
            calloc_prom_image(1, def_groupe[gpe_action].nbre,
                              def_groupe[gpe_lieux].nbre, 1);
        def_groupe[gpe].ext = (prom_images_struct *) lieux_action;

	printf("image initialise dans %s\n",__FUNCTION__);
    }

    if (def_groupe[gpe].learning_rate<=0.)
    {
		def_groupe[gpe].appel_apprend=rien;
		printf("\t Learning rate nul -> apprentissage=rien %d\n",gpe);
     }

    my_data = (MyData_winner *) malloc(sizeof(MyData_winner));
    my_data->gpe_vigilence = gpe_vigilence;
    my_data->gpe_lieux = gpe_lieux;
    my_data->gpe_action = gpe_action;
    my_data->gpe_error = gpe_error;
    my_data->gpe_adaptation = gpe_adaptation;
    my_data->lieux_max_prec = lieux_max_prec;
    my_data->seuil_apprend = seuil_apprend;
    my_data->gpe_epsilon = gpe_epsilon;

    def_groupe[gpe].data = (MyData_winner *) my_data;
}

void mise_a_jour_lieux_action(int n, int gestion_STM, int learn)
{
    int *thelieuxmax, *theactionmax;
    int maxfound;
    int gpe;
    int i, j, k, lieux_max = -1, action_max = -1;
    float max = 0.;
    prom_images_struct *lieux_action;
    MyData_winner *my_data;
    int gpe_lieux;
    int gpe_action;
    type_coeff *coeff;

    gpe = neurone[n].groupe;
    my_data = (MyData_winner *) (def_groupe[gpe].data);
    gpe_lieux = my_data->gpe_lieux;
    gpe_action = my_data->gpe_action;
    lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

    /*tableau des lieux max */
    thelieuxmax = malloc(sizeof(int) * def_groupe[gpe].nbre_de_1);
    theactionmax = malloc(sizeof(int) * def_groupe[gpe].nbre_de_1);
    for (i = 0; i < def_groupe[gpe].nbre_de_1; i++)
    {
        thelieuxmax[i] = -1;
        theactionmax[i] = -1;
    }

    /*recuper le contenu des poid */
    for (i = 0; i < def_groupe[gpe_action].nbre; i++)
    {
        coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
        for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
        {
            if (coeff->evolution == 0)
                coeff = coeff->s;
            if (coeff->evolution == 1)
            {
                lieux_action->images_table[0][j *
                                              def_groupe[gpe_action].nbre +
                                              i] =
                    (unsigned char) (int) ((coeff->val * 255));

            }
            coeff = coeff->s;
        }
    }

    /*met les neurone a zero */
    for (i = def_groupe[gpe].premier_ele;
         i < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;

    /*cherche les lieux max */
    for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
    {
        max = 0.;
        lieux_max = -1;
        for (i = 0; i < def_groupe[gpe_lieux].nbre; i++)
        {
            if (max < neurone[i + def_groupe[gpe_lieux].premier_ele].s1)
            {
                maxfound = 0;
                for (k = 0; k < j; k++)
                {

                    if (thelieuxmax[k] == i)
                        maxfound = 1;
                }
                if (maxfound == 0)
                {
                    max = neurone[i + def_groupe[gpe_lieux].premier_ele].s1;
                    lieux_max = i;
                }
            }

        }
        thelieuxmax[j] = lieux_max;
#ifdef DEBUG
        printf("lieux max %d : %d\n", j, thelieuxmax[j]);
#endif
    }
    max = 0.;

    /*cherche action max pour lieux max dans le matrice ou dans l'entree */
    for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
    {
        if (thelieuxmax[j] != -1)
        {
            action_max = -1;
            max = 0.;
            for (i = 0; i < def_groupe[gpe_action].nbre; i++)
            {
                if (max <
                    lieux_action->images_table[0][thelieuxmax[j] *
                                                  def_groupe[gpe_action].
                                                  nbre + i])
                {
                    max =
                        lieux_action->images_table[0][thelieuxmax[j] *
                                                      def_groupe[gpe_action].
                                                      nbre + i];
                    action_max = i;
                }
            }
            /*Si pas de max sur la ligne, on prend le max en entree */
            if (action_max == -1)
            {
                max = 0.;
                for (i = 0; i < def_groupe[gpe_action].nbre; i++)
                {
                    if (max <
                        neurone[i + def_groupe[gpe_action].premier_ele].s1)
                    {
                        max =
                            neurone[i +
                                    def_groupe[gpe_action].premier_ele].s1;
                        action_max = i;
                    }
                }
            }
        }
        else
            action_max = -1;

        theactionmax[j] = action_max;
#ifdef DEBUG
        printf("action max %d: %d\n\n", j, theactionmax[j]);
#endif
    }


    for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
    {
        /*active le neurone gagnant */
        if (theactionmax[j] != -1)
        {
            neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s =
                neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s1 =
                neurone[def_groupe[gpe_lieux].premier_ele +
                        thelieuxmax[j]].s1;
            neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s2 = 1.;
        }
    }
    free(theactionmax);
    free(thelieuxmax);
}

/*#define DEBUG*/
void apprend_winner_lieux_action(int gpe)
{
    int i, j, lieux_max = -1,  lieux_max_prec = 0;
    MyData_winner *my_data = def_groupe[gpe].data;
    int gpe_lieux = my_data->gpe_lieux;
    int gpe_action = my_data->gpe_action;
    int gpe_error = my_data->gpe_error;
    int gpe_adaptation = my_data->gpe_adaptation;
    float seuil_apprend = my_data->seuil_apprend;
    int adapt = 1;
    float max = 0.;
    prom_images_struct *lieux_action = def_groupe[gpe].ext;
    float local_vigilence = 1.;
    unsigned char temp;
    type_coeff *coeff;
    float dec_neur;
    int nb_shift;
    int s;

    if (my_data->gpe_vigilence != -1)
        local_vigilence =
            neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;
    lieux_max_prec = my_data->lieux_max_prec;

    max = 0.;
    for (i = 0; i < def_groupe[gpe_lieux].nbre; i++)
    {
        if (max < neurone[i + def_groupe[gpe_lieux].premier_ele].s1)
        {
            max = neurone[i + def_groupe[gpe_lieux].premier_ele].s1;
            lieux_max = i;
        }
    }
    my_data->lieux_max_prec = lieux_max;
    if (local_vigilence > 0.5)
    {
        for (i = 0; i < def_groupe[gpe_action].nbre; i++)
        {
            lieux_action->images_table[0][lieux_max *
                                          def_groupe[gpe_action].nbre + i] =
                (int) (255 *
                       neurone[def_groupe[gpe_action].premier_ele + i].s1);
        }
#ifdef DEBUG
        printf("apprend lieux %d \n", lieux_max);
#endif

    }
    if (gpe_adaptation != -1)
        if (neurone[def_groupe[gpe_adaptation].premier_ele + lieux_max_prec].
            s1 < 0.5)
        {
            adapt = 0;
#ifdef DEBUG
            printf("No adapt %d\n", gpe);
#endif
        }

    if (gpe_error != -1 && lieux_max_prec != lieux_max && adapt == 1)
    {
#ifdef DEBUG
        printf("PC %d-> PC %d\n", lieux_max_prec, lieux_max);
        printf("error winner %f\n",
               neurone[def_groupe[gpe_error].premier_ele +
                       lieux_max_prec].s1);
#endif
        if (neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1 <
            (-1. * seuil_apprend))
        {

            dec_neur =
                neurone[def_groupe[gpe_error].premier_ele +
                        lieux_max_prec].s1 * def_groupe[gpe_action].nbre / 2.;
            nb_shift = (int) fabs(dec_neur / 2.) + 1.;
#ifdef DEBUG
            printf("decalage gauche de action du lieu %d de %d neurone \n",
                   lieux_max_prec, nb_shift);
#endif
            for (s = 0; s < nb_shift; s++)
            {
                temp =
                    lieux_action->images_table[0][lieux_max_prec *
                                                  def_groupe[gpe_action].
                                                  nbre];
                for (i = 0; i < def_groupe[gpe_action].nbre - 1; i++)
                {
                    lieux_action->images_table[0][lieux_max_prec *
                                                  def_groupe[gpe_action].
                                                  nbre + i] =
                        lieux_action->images_table[0][lieux_max_prec *
                                                      def_groupe[gpe_action].
                                                      nbre + i + 1];
                }
                lieux_action->images_table[0][lieux_max_prec *
                                              def_groupe[gpe_action].nbre +
                                              def_groupe[gpe_action].nbre -
                                              1] = temp;
            }
        }
        else if (neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].
                 s1 > seuil_apprend)
        {
            dec_neur =
                neurone[def_groupe[gpe_error].premier_ele +
                        lieux_max_prec].s1 * def_groupe[gpe_action].nbre / 2.;
            nb_shift = (int) fabs(dec_neur / 2.) + 1.;
#ifdef DEBUG
            printf("decalage droite de action du lieu %d de %d neurone\n",
                   lieux_max_prec, nb_shift);
#endif
            for (s = 0; s < nb_shift; s++)
            {
                temp =
                    lieux_action->images_table[0][lieux_max_prec *
                                                  def_groupe[gpe_action].
                                                  nbre +
                                                  def_groupe[gpe_action].
                                                  nbre - 1];
                for (i = def_groupe[gpe_action].nbre - 1; i > 0; i--)
                {
                    lieux_action->images_table[0][lieux_max_prec *
                                                  def_groupe[gpe_action].
                                                  nbre + i] =
                        lieux_action->images_table[0][lieux_max_prec *
                                                      def_groupe[gpe_action].
                                                      nbre + i - 1];
                }
                lieux_action->images_table[0][lieux_max_prec *
                                              def_groupe[gpe_action].nbre] =
                    temp;
            }
        }

    }

/*On retient le lieux gagnant et l'erreur precedente car c est au changement de leix qu'on va faire la correction*/
    my_data->lieux_max_prec = lieux_max;

/*on remet l image dans les coeff*/
    for (i = 0; i < def_groupe[gpe_action].nbre; i++)
    {
        coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
        for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
        {

            if (coeff->evolution == 0)
            {
                coeff = coeff->s;
            }
            if (coeff->evolution == 1)
            {
                coeff->val =
                    (float) (lieux_action->
                             images_table[0][j * def_groupe[gpe_action].nbre +
                                             i]) / 255.;
#ifdef DEBUG
                if (coeff->val > 0.01)
                {
                  printf("%d im \n",
                           (int) (lieux_action->
                                  images_table[0][j *
                                                  def_groupe[gpe_action].
                                                  nbre + i]));
                    printf("coef_val : %f\n", coeff->val);
                }
#endif
            }
            coeff = coeff->s;
        }
    }
#ifdef DEBUG
    printf("fin %s\n", __FUNCTION__);
#endif
}

void mise_a_jour_lieux_action2(int n, int gestion_STM, int learn)
{
    int gpe= neurone[n].groupe;
    int i;
    float s, sum_p, Sd;
    type_coeff *synapse;
int gpe_vigilence =
        ((MyData_winner *) (def_groupe[gpe].data))->gpe_vigilence;
    int gpe_epsilon = ((MyData_winner *) (def_groupe[gpe].data))->gpe_epsilon;
    float eps_local = eps;
    float vigilence_local = vigilence;

    if (gpe_epsilon != -1)
    {
        eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
    }
    if (gpe_vigilence != -1)
    {
        vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
    }

    i=n;	

    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    Sd = 0.;                    /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
    sum_p = 0.0;
    synapse = neurone[i].coeff;
    while (synapse != NULL)
    {

        if (synapse->evolution != 0)
        {                       /* nous ne sommons pas le lien UC */
            switch (liaison[synapse->gpe_liaison].mode)
            {
            case 0:
                sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
		break;    /*neurone[synapse->entree].s2;  break;s2 */
            case 2:
                sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
                break;          /*s */
            default:
                printf
                    ("Erreur type synapse(%d) incorrect dans mise_a_jour_neurone_LMS\n",
                     synapse->type);
                exit(1);
            }
	}
        else
        {
            /* par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
            Sd = Sd + synapse->val * neurone[synapse->entree].s1;
            /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */
        }

        /*tau=liaison[synapse->gpe_liaison].temps;
           synapse->moy=(synapse->moy*tau+neurone[synapse->entree].s2)/(tau+1); *//*Si on veut gerer le retard */
        /* non il serait plus simple de ce servir de la gestion du kernel... */
        /* tres gros risque de pb du a l'ecrasement des valeurs par le kernel. */
        synapse = synapse->s;
    }

    s = neurone[i].s = sum_p;
    neurone[i].d = Sd - s;
#ifdef DEBUG
    if(neurone[i].d<-0.5)
	printf("desapprend %d car Sd=%f, s=%f donc g=%f\n",i,Sd,s,neurone[i].d);
    if(neurone[i].d>0.5)
	printf("apprend %d\n",i);
#endif    
    s=Sd*vigilence_local+s*(1.0-vigilence_local);	
    
//     printf("sump=%f   Sd=%f  ->>>>d = %f    et \n  \t vigilence = %f ->>>>>>> s = %f\n",sum_p, Sd ,neurone[i].d,vigilence_local,s);
      
    if (s < 0.)
        neurone[i].s1 = neurone[i].s2 = 0.;
    else if (s > 1.)
        neurone[i].s1 = neurone[i].s2 = 1.;
    else
        neurone[i].s1 = neurone[i].s2 = s;

    /*Mouai ca c'est a change parceque pas vrai LMS (on seuil pas: sinon Delta(W)=epsilon*(Sd-f(Sum(wK*xK)) !! */
    /*bref faut pas du tout seuilï¿½(vrai regle je crois) ou au pire on seuil mais pas juste la sortie globale et pas pour l'apprentissage */

     /**/
        /*if(learn==0)
           printf("***Sorties des neurones du LMS : %f\n***",neurone[i].s1); */


}


void apprend_winner_lieux_action2(int gpe)
{
   float gradient, Var_inst,Var_accumul;
    int size_Y = def_groupe[gpe].tailley;
    int size_X = def_groupe[gpe].taillex;
    int first_neuron = def_groupe[gpe].premier_ele;
    int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
    int i,j;
    prom_images_struct *lieux_action;
    MyData_winner *my_data;
    int gpe_lieux,gpe_action;
    type_coeff * coeff;
    int gpe_vigilence =
        ((MyData_winner *) (def_groupe[gpe].data))->gpe_vigilence;
    int gpe_epsilon = ((MyData_winner *) (def_groupe[gpe].data))->gpe_epsilon;
    float eps_local = eps;
    float vigilence_local = vigilence;

    if (gpe_epsilon != -1)
    {
        eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
    }
    if (gpe_vigilence != -1)
    {
        vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
    }

    Var_inst = vigilence_local * eps_local  * def_groupe[gpe].learning_rate;
    Var_accumul = eps_local  * def_groupe[gpe].learning_rate;

    for (i = first_neuron; i < last_neuron; i++)
    {
        gradient = neurone[i].d;
	/*mise a jour des poids sur les entrees Conditionnelles du neurone i */
	gradient_lieux_action_accumul(i, Var_accumul);
        gradient_lieux_action_inst(i, gradient, Var_inst,Var_accumul);
    }

    my_data = (MyData_winner *) (def_groupe[gpe].data);
    gpe_lieux = my_data->gpe_lieux;
    gpe_action = my_data->gpe_action;
    lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

    /*tableau des lieux max */
// printf("debut %s\n", __FUNCTION__);
    /*recuper le contenu des poid */
    printf("update image des poinds\n");
    for (i = 0; i < def_groupe[gpe_action].nbre; i++)
    {
        coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
        for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
        {
            if (coeff->evolution == 0)
                coeff = coeff->s;
            if (coeff->evolution == 1)
            {
                lieux_action->images_table[0][j *
                                              def_groupe[gpe_action].nbre +
                                              i] =
                    (unsigned char) (int) ((coeff->val * 255));

            }
            coeff = coeff->s;
        }
    }
}

void gradient_lieux_action_inst(int i, float gradient, float Var,float Var_accumul)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
    type_coeff *synapse;
    float dc;

    synapse = neurone[i].coeff;
    while (synapse != NULL)
    {
        if (synapse->evolution == 1)
        {

	    switch (liaison[synapse->gpe_liaison].mode)
            {
            case 0:            /*liens produit after compet */
                dc = gradient * neurone[synapse->entree].s1;
                break;

            case 2:            /*liens produit before compet */
                dc = gradient * neurone[synapse->entree].s;
                break;
            default:
                printf
                    ("Erreur ce type de lien n'est pas gere pour un neurone pavlov/widrow neurone=%d \n",
                     i);
                exit(1);
            }
#ifdef DEBUG
	    if(dc<-0.5)
		printf("desapprendrai %d: dc = %f et gradient = %f\n",i,dc,gradient);
    	    if(dc>0.5)
		printf("apprendrai %d: dc = %f\n",i,dc);
#endif 
	    if(Var_accumul==0.)
		    synapse->Nbre_ES = dc;
            else
		    synapse->Nbre_ES =0.;

	    synapse->val = synapse->val + Var * dc;
	    
#ifdef DEBUG
	    if(fabs(dc)>0.5)
		printf("maj val : %f \n",synapse->val);
#endif 

	    /* if(dc>0.) synapse->val=synapse->val+Var*dc; else synapse->val=synapse->val+Var*dc; */
            /*on vire l'histoire du desapprentissage car les poids ne redescendent pas sinon */

            /* dissymetrie apprentissage et desapprentissage facteur 1 a 100 ad hoc, a ne pas remonter PG */		
	}
        synapse = synapse->s;
    }
}

void gradient_lieux_action_accumul(int i, float Var)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
    type_coeff *synapse;
   
    synapse = neurone[i].coeff;
    while (synapse != NULL)
    {
        if (synapse->evolution == 1)
        {
            synapse->val = synapse->val + synapse->Nbre_ES * Var;
        }
        synapse = synapse->s;
    }
}


/* #undef DEBUG*/

void apprend_winner(int gpe)
{
    int i, pos = 0;
    float max, dc = 0., Si, Sj, sp, sm;
    type_coeff *coeff;
    int deb, nbre;
    float local_vigilence = 1.;
    MyData_winner *my_data;

    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;

    my_data = ((MyData_winner *) (def_groupe[gpe].data));
    if (my_data->gpe_vigilence != -1)
        local_vigilence =
            neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;

    if (local_vigilence > 0.5)
    {
        max = -99999999.;

        for (i = deb; i < deb + nbre; i++)  /* cherche le max */
        {
            if (neurone[i].s2 > max)
            {
                max = neurone[i].s2;
                pos = i;
            }
            neurone[i].s2 = 0;
        }

        if (max <= 0.)
            return;             /* modif sorin */
        neurone[pos].s2 = 1.;

        coeff = neurone[pos].coeff;
        sp = sm = epsilon;
        Si = neurone[pos].s2;
        while (coeff != nil)
        {
            if (coeff->evolution == 1)  /* si coeff modifiable */
            {
#ifdef DEBUG
                printf("Winner %d : le neurone %d apprend \n", gpe,
                       pos - def_groupe[gpe].premier_ele);
#endif
                Sj = coeff->moy;
                if (Sj > 0.)
                {
                    dc = eps * Si * Sj;
                }
                else
                    dc = 0.;

                coeff->val = coeff->val + dc;
                /*              if(coeff->val>0.) sp=sp+coeff->val;
                   else sm=sm-coeff->val; */

                /*       if(coeff->val>1) coeff->val=1;
                   if(coeff->val<-1) coeff->val=-1;  */
            }
            coeff = coeff->s;
        }
	/*coeff = neurone[pos].coeff;
          while(coeff!=nil)
           {
           if(coeff->evolution==1)                 
           {
           if(coeff->val>0.) coeff->val=coeff->val/sp;
           else coeff->val=coeff->val/sm;
           }
           coeff=coeff->s;
           } */

	/* PG c'est anorma l, l'apprentissage ne devrait induire un recalcul de tous les neurones apres coup */
	/* la sortie n'est pas le potentiel */
     
	   def_groupe[gpe].appel_activite(pos,0,0);
	   /*  neurone[i].s = neurone[i].s1 = calcule_produit(i, 0, 0);*/

    }
}


void apprend_winner_old(int gpe)
{
    int i, pos = 0;
    float max, dc = 0., Si, Sj, sp, sm;
    type_coeff *coeff;
    int deb, nbre;
    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;

    max = -99999999.;

    for (i = deb; i < deb + nbre; i++)  /* cherche le max */
    {
        if (neurone[i].s2 > max)
        {
            max = neurone[i].s2;
            pos = i;
        }
        neurone[i].s2 = 0;
    }
    if (max <= 0.)
        return;                 /* modif sorin */
    neurone[pos].s2 = 1.;

    for (i = deb; i < deb + nbre; i++)  /* modifie les poids en consequence */
    {
        coeff = neurone[i].coeff;
        sp = sm = epsilon;
        while (coeff != nil)
        {
            if (coeff->evolution == 1)  /* si coeff modifiable */
            {
                Si = neurone[i].s2;
                Sj = coeff->moy;
                if (i == pos)   /* neurone selectionne modif vers meilleure reponse */
                {
                    if (Si > 0. && Sj > 0.)
                        dc = eps * Si * Sj;
                    else if (Si > 0. && Sj <= 0.)
                        dc = -eps * Si;
                    else if (Si <= 0. && Sj > 0.)
                        dc = -eps * Sj;
                }
                else            /*   les autres ne doivent plus repondre */
                {
                    if (Si > 0. && Sj > 0.)
                        dc = -eps * Si * Sj;
                    else if (Si > 0. && Sj <= 0.)
                        dc = eps * Si;
                    else if (Si <= 0. && Sj > 0.)
                        dc = eps * Sj;
                }
                coeff->val = coeff->val + dc;
                if (coeff->val > 0.)
                    sp = sp + coeff->val;
                else
                    sm = sm - coeff->val;
                /*       if(coeff->val>1) coeff->val=1;
                   if(coeff->val<-1) coeff->val=-1;  */
            }
            coeff = coeff->s;
        }
        coeff = neurone[i].coeff;
        while (coeff != nil)
        {
            if (coeff->evolution == 1)
            {
                if (coeff->val > 0)
                    coeff->val = coeff->val / sp;
                else
                    coeff->val = coeff->val / sm;
            }
            coeff = coeff->s;
        }

    }

}


void gestion_groupe_generique_winner(int j)
{
     int i,dvn,nb,lien_entrant;
   int pos=0,nmax;
   float max,max_max=-1;
   int deb,nbre,nb_de_1=-1,gpe_entree=-1,nbre2,increment,fin2,deb2,n;
   int *p;
   char param_link[255];
	int toto;
	
	nbre2=def_groupe[j].taillex*def_groupe[j].tailley;
   deb=def_groupe[j].premier_ele;
	increment=nbre/nbre2;
   nbre=def_groupe[j].nbre;
   max= -99999999.;
	dvn=def_groupe[j].dvn;
#ifdef DEBUG
	printf("dvn:%d\n",dvn);
#endif	
	if(dvn<=0||dvn>nbre/2) dvn=nbre/2;
	
	/*printf("nb de 1:%f\n",def_groupe[j].nbre_de_1);*/
	if(def_groupe[j].nbre_de_1<1)
		nb_de_1=1;
	else
		nb_de_1=def_groupe[j].nbre_de_1;
	
	if(def_groupe[j].data==NULL)
	{
			/*recherche du lien de neuromod pour modifier eventuellement le nombre de gagant*/
			gpe_entree=trouver_entree(j, "mod_winner_nb");
			p=(int*)malloc(sizeof(int));
			*p=	gpe_entree;
			def_groupe[j].data=p;
	}
	p=(int*)(def_groupe[j].data);
	gpe_entree=*p;
	if(gpe_entree>-1)
	{
		max=	neurone[def_groupe[gpe_entree].premier_ele].s2;
		if(max>0.)nb_de_1=(int)def_groupe[j].dvp;
#ifdef DEBUG
				printf("number of winner changed: nb_1:%d\n",nb_de_1);
#endif
	}

	/*mise a zero*/
	  for(i=deb;i<deb+nbre;i++)
    {
     neurone[i].s2=0.;
    }
		  
 
		for(nmax=0;nmax<nb_de_1;nmax++) /* recherche des max suivants nbre_de_1 donne le nbre de max suivant que l'on veut on peut le changer dans le script*/
		{			
					/* cherche le max */
					for(n=deb+dvn;n<deb+nbre;n=n+2*dvn)
					{
						max=-1;
						pos=-1.;	
						/*calcul les indices*/
						deb2=n-dvn;
  					    fin2=n+dvn;
						 /*test debordement eventuel*/
 					    if(deb2<deb) deb2=deb;
 					    if(fin2>deb+nbre) fin2=deb+nbre;
						 #ifdef DEBUG
							/*	 printf ("Winner:%d , deb2:%d,fin2:%d,dvn:%d,increment:%d,n:%d\n",j,deb2-deb,fin2-deb,dvn,increment,n);*/
						#endif			 
						 for(i=deb2;i<fin2;i++)
						 {
								if(dvn==nbre/2)
								{ 	
									if(neurone[i].s>max && neurone[i].s2==0.)/* test sur .s2 si c'est à zéro c'est que ce n'est pas un max que l'on a déjà	sélectionné donc on peut le selectionner*/
									{
													max=neurone[i].s; pos=i;
									#ifdef DEBUG
													printf ("Winner compet nb 1:%d  max : %f  , pos : %d   \n",j,max,pos-deb);
									#endif	
									}
								}
								else
								{
									if(neurone[i].s>=max && neurone[i].s2==0.)/* test sur .s2 si c'est à zéro c'est que ce n'est pas un max que l'on a déjà	sélectionné donc on peut le selectionner*/
									{
													max=neurone[i].s; pos=i;
									#ifdef DEBUG
													printf ("Winner compet dvn:%d  max : %f  , pos : %d   \n",j,max,pos-deb);
									#endif	
									}
								}
								/*On ne prend un gagnant que si son activite est > 0*/
						}
						if(max>0.)
						{ 
								neurone[pos].s2=1.;
								if(max>max_max)max_max=max;
							#ifdef DEBUG
								printf("Winner:%d le %d gagnant, neurone (%d)).s1 vaut %f, .s2:%f \n",j, nmax,pos-deb, max,neurone[pos].s2);
							#endif
						}
					}
		
				
			} 
	/*comme on peut avoir plusieurs gagnant, il faut attendre de les avoirs tous obtenus avant de mettre a zero les champ .s1 des neurones perdants...*/	

	for(i=deb;i<deb+nbre;i++)  
	{
						if( neurone[i].s2<1.)/* test sur .s2 si c'est à zéro c'est que ce n'est pas un des gagnants*/
						{
							neurone[i].s1=0.; 
						}
		/*				printf("max:%f, %d,%d\n",neurone[deb+22].s2,i,deb);*/
	}
	
}

void gestion_groupe_multiple_winner(int j)
{
    int i;
    int posmax = 0, nmax;
    float max, total;
    int deb, nbre, nbre_de_1;

    deb = def_groupe[j].premier_ele;
    nbre = def_groupe[j].nbre;
    max = -99999999.;
#ifdef DEBUG
    printf("gestion_groupe_mutilple_winner %d  \n",j);
#endif
    
    if( def_groupe[gpe].data != NULL )
        if ( ! calcule_neuromodulation_fast( gpe ,
                    ((MyData_LMS *) (def_groupe[gpe].data))->nbre_de_1_ptr , 
                    &nbre_de_1 ) )
        {
            fprintf(stderr, "Erreur de neuromodulation dans le LMS gpe %d\n",gpe);
        }

    if (def_groupe[j].nbre_de_1 <= 1)
        nbre_de_1 = 1;
    else
        nbre_de_1 = def_groupe[j].nbre_de_1;

    for (i = deb; i < deb + nbre; i++)
    {
        neurone[i].s1 = neurone[i].s2 = 0.;
    }


    for (nmax = 0; nmax < nbre_de_1; nmax++)    /* recherche des max suivants nbre_de_1 donne le nbre de max suivant que l'on veut on peut le changer dans le script */
    {
        total = -1;
        posmax = -1.;
        /* cherche le max local au neurone n considere */
        for (i = deb; i < deb + nbre; i++)
        {
            if (neurone[i].s > total && neurone[i].s2 <= 0.)    /*  test sur .s2 si c'est à zéro c'est que ce n'est pas un max que l'on a déjà */
/*	      						   sélectionné donc on peut le selectionner*/
            {
                total = neurone[i].s;
                posmax = i;
            }
        }

        if (total > 0.)
        {
	  /* PG pb: le winner ne tient pas compte du seuil groupe et son activite peut etre superieure a 1 !!! */
	  if(total>1.) total=1.; /* ajout PG pb... */
	  neurone[posmax].s2 = neurone[posmax].s1 = total;
	  if (nmax == 0)
	    neurone[posmax].s2 = 1.;
#ifdef DEBUG
	  printf("le %d(%d) gagnant vaut %f \n",
		 posmax - def_groupe[j].premier_ele, j, total);
#endif
        }
        else
            break;
    }
}
