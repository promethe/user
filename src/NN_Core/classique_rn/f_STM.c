/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_STM.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M; Maillard
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
La taille du groupe entrant doit être égal à la taille du groupe.

Macro:
-none 

Local variables:
-nnoe

Global variables:
-none

Internal Tools:
-tools/include/init_function_product()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/struct_product.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

void function_STM(int gpe)
{
    int i = 0;
    int depart, depart_input;
    int lien, sx, sy, gpe_input, delta_depart;
    char *chaine = NULL, *param = NULL;
    float alpha = 0.;
    lien = find_input_link(gpe, i);
    if (lien == -1)
    {
        EXIT_ON_ERROR("Erreur de lien dans f_STM gpe %d\n", gpe);

    }
    chaine = liaison[lien].nom;
    if ((param = strstr(chaine, "-A")) == NULL)
    {
        EXIT_ON_ERROR("Erreur de parametre de lien dans f_STM gpe %d\n", gpe);
    }

    alpha = (float) atof(&param[2]);

    sx = def_groupe[gpe].taillex;
    sy = def_groupe[gpe].tailley;

    gpe_input = liaison[lien].depart;

    if (sx != def_groupe[gpe_input].taillex  || sy != def_groupe[gpe_input].tailley)
    {
        EXIT_ON_ERROR("Erreur dans f_STM gpe %d : les tailles du groupe entrant et de ce groupe doivent etre identiques\n", gpe);
    }
    
    depart = def_groupe[gpe].premier_ele;
    depart_input = def_groupe[gpe_input].premier_ele;

    delta_depart = depart_input - depart;

    for (i = depart; i < depart + (sx * sy); i++)
    {
        neurone[i].s = (neurone[i].s + (alpha * neurone[i + delta_depart].s)) / (alpha +(float) 1.);
        neurone[i].s1 =(neurone[i].s1 + (alpha * neurone[i + delta_depart].s1)) / (alpha + (float) 1.);
        neurone[i].s2 = (neurone[i].s2 + (alpha * neurone[i + delta_depart].s2)) / (alpha + (float) 1.);
    }
}
