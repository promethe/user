/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <libx.h>

/*#define DEBUG*/
#include <net_message_debug_dist.h>
#include <reseau.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include "include/fonction.h"
#include "include/calcule_produit.h"

#define SEUIL_ACTIVATION 0.5
#define DEFAULT_DECAY_FACTOR 0.8

/*#define DEBUG_WEIGHTS*/
/*#define DEBUG_DW*/
/*#define DEBUG_to_File*/


/**
 * Modifie le 18/08/2009 par N. Garnault :
 *   Ajout de la possibilite d'utiliser un lien activateur, qui indique
 *   au groupe quand il doit apprendre (activation > SEUIL_ACTIVATION) et ne
 *   pas apprendre (activation < SEUIL_ACTIVATION).
 */

typedef struct MyData_Sutton_Barto
{
   /*float forgot_factor;*/
   int decay_gpe;
   int noise_gpe;
   int lr_gpe;
   noeud_modulation *activation; /* lien activateur     */
   noeud_modulation *DR;         /* delta reinforcement */
} MyData_S_B;

/***************************************************/
void new_S_B(int gpe)
{
   int i = 0, links_in = 0;
   int decay_gpe = -1, noise_gpe = -1, lr_gpe = -1;
   /*float forgot_factor = 0.0;*/
   MyData_S_B *my_data = NULL;
   noeud_modulation *dr = NULL;
   noeud_modulation *act = NULL;

   dprints("Entree dans new_S_B\n");

   /* Lien de neuromodulation - obligatoire */
   dr = get_noeud_modulation_ptr(gpe, "D_Reinf");
   if (dr == NULL)
      EXIT_ON_ERROR("Gpe[%d] : Aucun lien de neuromodulation defini\n", gpe);

   /* Lien d'activation - optionnel */
   act = get_noeud_modulation_ptr(gpe, "-activation");
   if (act == NULL)
      cprints("Gpe[%d] : Aucun lien activateur defini\n", gpe);

   /* Options supplementaires
    *   * facteur d'oubli -f
    */
   /*forgot_factor = DEFAULT_DECAY_FACTOR;*/
   //   while((links_in = find_input_link(gpe, i++)) != -1)
   //   {
   //      if(prom_getopt(liaison[links_in].nom, "-f", param) == 2)
   //         sscanf(param, "%f", &forgot_factor);
   //   }

   /* Get Input group */
   while ((links_in = find_input_link (gpe, i)) != -1)
   {
      if (prom_getopt(liaison[links_in].nom, "-decay", NULL) == 1)
      {
         decay_gpe = liaison[links_in].depart ;
         if ( def_groupe[decay_gpe].nbre > 1 )
            PRINT_WARNING("GROUP %s (num coeos %s) : Only the first neuron of input decay group %s will be used by Sutton & Barto", __FUNCTION__, def_groupe[gpe].no_name, def_groupe[decay_gpe].no_name);
      }

      if (prom_getopt(liaison[links_in].nom, "-noise", NULL) == 1)
      {
         noise_gpe = liaison[links_in].depart ;
         if ( def_groupe[noise_gpe].nbre > 1 )
            PRINT_WARNING("GROUP %s (num coeos %s) : Only the first neuron of input noise group %s will be used by Sutton & Barto", __FUNCTION__, def_groupe[gpe].no_name, def_groupe[noise_gpe].no_name);
      }
      if (prom_getopt(liaison[links_in].nom, "-lr", NULL) == 1)
      {
         lr_gpe = liaison[links_in].depart ;
         if ( def_groupe[lr_gpe].nbre > 1 )
            PRINT_WARNING("GROUP %s (num coeos %s) : Only the first neuron of input learning rate group %s will be used by Sutton & Barto", __FUNCTION__, def_groupe[gpe].no_name, def_groupe[lr_gpe].no_name);
      }
      i++ ;
   }




   /* Allocation */
   my_data = (MyData_S_B *) malloc(sizeof(MyData_S_B));
   if (my_data == NULL) EXIT_ON_ERROR("%s : Echec d'allocation memoire\n", __FUNCTION__);

   my_data->DR = dr;
   my_data->activation = act;
   my_data->decay_gpe = decay_gpe;
   my_data->noise_gpe = noise_gpe;
   my_data->lr_gpe = lr_gpe;
   //   my_data->forgot_factor = forgot_factor;

   def_groupe[gpe].data = (MyData_S_B *) my_data;
}

/***************************************************/
void apprend_S_B(int gpe)
{
   int i, deb, nbre;
   float dw = 0.0;
   float D_R = 0.0;
   float input = 0.0, S_prev = 0.0;
   float learning = 0.0;
   float activation = 0.0;
   float decay_factor;
   type_coeff *coeff;
   MyData_S_B *my_data;

#ifdef  DEBUG_to_File
   FILE* fp=NULL;
   FILE* f_w0=NULL;
   FILE* f_w1=NULL;
#endif

   dprints("Entree dans %s: gpe=%s\n", __FUNCTION__, def_groupe[gpe].no_name);

   my_data = ((MyData_S_B *) (def_groupe[gpe].data));

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   if ( my_data->lr_gpe > 0 ) learning = neurone[def_groupe[my_data->lr_gpe].premier_ele].s1;
   else                       learning = def_groupe[gpe].learning_rate;

   if ( my_data->decay_gpe > 0 )  decay_factor = neurone[def_groupe[my_data->decay_gpe].premier_ele].s1;
   else                           decay_factor = DEFAULT_DECAY_FACTOR;


   /* Lecture du signal d'activation */
   if (NULL != my_data->activation)
   {
      calcule_neuromodulation_fast(gpe, my_data->activation, &activation);
      dprints("Signal d'activation calcule : %f\n", activation);

      if (activation < SEUIL_ACTIVATION) return;
   }

   /* Lecture du signal de renforcement */
   calcule_neuromodulation_fast(gpe, my_data->DR, &D_R);

   cprints("DR : %f\n", D_R);

#ifdef DEBUG_to_File
   if (NULL == (fp = fopen("S_B_data.txt", "a"))) EXIT_ON_ERROR("%s : Can't open file S_B_data.txt\n", __FUNCTION__);
   if (NULL == (f_w0 = fopen("f_w0.SAVE", "a")))  EXIT_ON_ERROR("%s : Can't open file f_w0.SAVE\n", __FUNCTION__);
   if (NULL == (f_w1 = fopen("f_w1.SAVE", "a")))  EXIT_ON_ERROR("%s : Can't open file f_w1.SAVE\n", __FUNCTION__);

   fprintf(fp,"-------------------------- \n");
   fprintf(fp,"gpe: %d \n",gpe);
   fprintf(fp,"   W   |   dw   |   D_R   |   Out   |   IN\n");
   fprintf(fp,"---------------------------------------------\n");
#endif

#ifdef DEBUG_DW
   cprints("\nDebug Sutton_Barto : dw groupe [%d]\n", gpe);
#endif
   for (i = deb; i < deb+nbre; i++)
   {
      coeff = neurone[i].coeff;
      while (coeff != NULL)
      {
         /* si coeff modifiable */
         if (coeff->evolution == 1)
         {
            input = coeff->Nbre_ES;
            coeff->Nbre_ES = neurone[coeff->entree].s1;

            S_prev = coeff->Nbre_S;
            coeff->Nbre_S = neurone[i].s2;

            /*dw = learning * D_R *  neurone[i].s2  * input;*/
            dw = learning * D_R *  S_prev  * input;

            //printf("\t\t\t W:%.9f = W:%9f * decay:%9f  + dw:%.9f:(lr:%9f * S:S_prev:%9f * input:%.9f * D_R:%.9f)\n",coeff->val, coeff->val, decay_factor, dw, learning, S_prev, input, D_R);

#ifdef DEBUG_DW
            cprints("%f ", dw);
#endif
            coeff->val = decay_factor * coeff->val + dw;
            //printf("\t\t\t NEW W = %.9f",coeff->val);
            coeff->val = coeff->val > 1.0  ? 1.0  : coeff->val;
            coeff->val = coeff->val < -1.0 ? -1.0 : coeff->val;
            //printf("  ->(rampe)->  %.9f\n",coeff->val);
         }

#ifdef DEBUG_to_File
         fprintf(fp,"%.3f    %.2f     %.2f      %.2f      %.2f     \n",coeff->val,dw,D_R,neurone[i].s2,input);
         if (!(i-deb))  fprintf(f_w0,"%.8f\n",coeff->val);
         else           fprintf(f_w1,"%.8f\n",coeff->val);
#endif

         dprints("W=%.9f, dw=%.9f, D_R=%.9f, neurone.s2=%.9f, input=%.9f\n",coeff->val,dw,D_R,neurone[i].s2,input);
         coeff = coeff->s;
      }
#ifdef DEBUG_DW
      cprints("\n");
#endif
   }

#ifdef DEBUG_to_File
   fclose(fp);
   fclose(f_w0);
   fclose(f_w1);
#endif

   //   /*regle complete de sutton et barto*/
   //   for (i = deb; i < deb + nbre; i++)
   //   {
   //      neurone[i].d = neurone[i].last_activation;
   //      neurone[i].last_activation = neurone[i].s2;
   //   }

#ifdef DEBUG_WEIGHTS
   cprints("\nDebug Sutton_Barto : poids groupe [%d]\n", gpe);
   for (i = deb ; i < deb + nbre ; i++)
   {
      coeff = neurone[i].coeff;
      while (coeff != NULL)
      {
         cprints("%f ", coeff->val);
         coeff = coeff->s;
      }
      cprints("\n");
   }
#endif

   dprints("Sortie de %s: gpe=%s\n", __FUNCTION__, def_groupe[gpe].no_name);
}

/***************************************************/
void gestion_groupe_S_B(int gpe)
{
   int i, pos=-1;
   int deb, nbre;
   float max;
   float activation = 0.0;
   MyData_S_B *my_data = ((MyData_S_B *) (def_groupe[gpe].data));

   dprints("Entree dans %s: gpe=%s\n", __FUNCTION__, def_groupe[gpe].no_name);


   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   /*
    *  Lecture du signal d'activation :
    *    Si aucune activation n'est définie, la compétition a lieu.
    *    Si un lien d'activation est défini, la compétition est conditionnée
    *      par le fait que le signal d'activation est supérieur à SEUIL_ACTIVATION
    */
   if (NULL != my_data->activation)
   {
      calcule_neuromodulation_fast(gpe, my_data->activation, &activation);
      dprints("Signal d'activation calcule : %f\n", activation);

      if (activation < SEUIL_ACTIVATION)
      {
         for (i = deb; i < deb + nbre; i++)
         {
            neurone[i].s1 = 0.0;
            neurone[i].s2 = 0.0;
         }

         return;
      }
   }

   /* competition */
   max = -999999.9;
   for (i = deb; i < deb + nbre; i++)
   {
      if (neurone[i].s1 > max)
      {
         max = neurone[i].s1;
         pos = i;
      }
      neurone[i].s2 = 0.0;
      neurone[i].s1 = 0.0;
   }
   neurone[pos].s2 = 1.0;
   neurone[pos].s1 = 1.;//max;

   //   for (i = deb; i < deb + nbre; i++)
   //   {
   //      neurone[i].s1 = neurone[i].s2;
   //      dprints("neurone[%d].s1=%f\n",i,neurone[i].s1);
   //      dprints("neurone[%d].s2=%f\n",i,neurone[i].s2);
   //   }

   dprints("Sortie de %s: gpe=%s\n", __FUNCTION__, def_groupe[gpe].no_name);
}

/***************************************************/

void mise_a_jour_S_B(int i, int gestion_STM, int learn)
{
   float sortie = 0.0;
   float amplitude_bruit = 0.0;
   int gpe;
   MyData_S_B *my_data=NULL;

   gpe = neurone[i].groupe;
   my_data = ((MyData_S_B *) (def_groupe[gpe].data));

   dprints("Entree dans %s: gpe=%s\n", __FUNCTION__, def_groupe[neurone[i].groupe].no_name);
   dprints("gpe=%s, amplitude bruit=%f\n", def_groupe[neurone[i].groupe].no_name, def_groupe[neurone[i].groupe].noise_level);

   if ( my_data->noise_gpe > 0 )  amplitude_bruit = neurone[def_groupe[my_data->noise_gpe].premier_ele].s1;
   else                           amplitude_bruit = def_groupe[gpe].noise_level;


   sortie = calcule_produit(i, gestion_STM, learn);
   neurone[i].s = sortie;
   sortie = rampe(neurone[i].s - neurone[i].seuil);

   /* ajouter les bruit apres les calcule de produit pour eviter la saturation */
   neurone[i].s1 = sortie + amplitude_bruit*(((float)(rand())/RAND_MAX)-0.5);
   neurone[i].s2 = neurone[i].s1; /* avant sortie;*/

   //printf("\t s[%d] = %9f  \t  s1[%d] = %9f\n", i, neurone[i].s, i, neurone[i].s1);

   dprints("neurone[%d].s1=%f\n", i, neurone[i].s1);
   dprints("neurone[%d].s2=%f\n", i, neurone[i].s2);
   dprints("Sortie de %s: gpe=%s\n", __FUNCTION__, def_groupe[neurone[i].groupe].no_name);
}
