/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_qlearning.c
    \brief 

    Author: J. Hirel
    Created: 27/03/2009
    Modified:
    - author: 
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$

    Description:


    Macro:
    -none

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools:
    -Kernel_Function/find_input_link()
    -Kernel_Function/prom_getopt()

    Links:
    -

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*#define DEBUG*/

#include <Kernel_Function/find_input_link.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

typedef struct mydata_qlearning
{
      noeud_modulation *ptr_learn;
      noeud_modulation *ptr_reward;
      noeud_modulation *ptr_discount;
} mydata_qlearning;

void new_qlearning(int gpe)
{
   mydata_qlearning *my_data = NULL;
   noeud_modulation *ptr_learn = NULL, *ptr_reward = NULL, *ptr_discount = NULL;

   dprints("new_qlearning(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {
      ptr_learn = get_noeud_modulation_ptr(gpe,"learn_factor");
      ptr_reward = get_noeud_modulation_ptr(gpe,"reward");
      ptr_discount = get_noeud_modulation_ptr(gpe,"discount_factor");

      if (ptr_learn == NULL || ptr_reward == NULL || ptr_discount == NULL)
      {
	 fprintf(stderr, "ERROR in new_qlearning(%s): Missing neuromodulation links\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data = (mydata_qlearning *) malloc(sizeof(mydata_qlearning));

      if (my_data == NULL)
      {
	 fprintf(stderr, "ERROR in new_qlearning(%s): malloc failed for data\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data->ptr_learn = ptr_learn;
      my_data->ptr_reward = ptr_reward;
      my_data->ptr_discount = ptr_discount;
      def_groupe[gpe].data = (mydata_qlearning *) my_data;
   }

   dprints("new_qlearning(%s): Leaving function\n", def_groupe[gpe].no_name);
}

void function_qlearning(int gpe)
{
   int i;
   float act, Qmax, p;
   float learning_rate = 0., reward = 0., discount_factor = 1.;
   type_coeff *coeff;
   mydata_qlearning *my_data = NULL;
   
   my_data = (mydata_qlearning *) def_groupe[gpe].data;
   if (my_data == NULL)
   {
      fprintf(stderr, "ERROR in f_qlearning(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
      exit(1);
   }

   /* Computes the activity of the neurons */
   Qmax = 0.;

   for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
   {
      act = 0.;
      coeff = neurone[i].coeff;
      
      while (coeff != NULL)
      {
	 p = coeff->val * neurone[coeff->entree].s1;

	 if (coeff->evolution == 1)  /* si coeff modifiable */
	    act += p;
	 else
	 {
	    if (p > def_groupe[gpe].seuil)
	    {
/*	       dprints("f_qlearning(%s): neuron %d has unconditionnal activity = %f\n", def_groupe[gpe].no_name, i - def_groupe[gpe].premier_ele, p);*/
	       neurone[i].d = 1;
	    }
	    else
	       neurone[i].d = 0;
	 }

	 coeff = coeff->s;
      }

      neurone[i].s = neurone[i].s1 = neurone[i].s2 = act;
      if (act > Qmax)
	 Qmax = act;
   }

/*   dprints("f_qlearning(%s): Qmax = %f\n", def_groupe[gpe].no_name, Qmax);*/

   /* Learning */
   calcule_neuromodulation_fast(gpe, my_data->ptr_learn, &learning_rate);
   calcule_neuromodulation_fast(gpe, my_data->ptr_reward, &reward);
   calcule_neuromodulation_fast(gpe, my_data->ptr_discount, &discount_factor);

/*   if (reward > 0.)
     dprints("f_qlearning(%s): Reward obtained ! value = %f\n", def_groupe[gpe].no_name, reward);*/

   for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
   {
      if (neurone[i].d > 0.5)
      {
	 if (learning_rate > 0.)
	    dprints("f_qlearning(%s): Learning => learning_rate = %f, reward = %f, discount_factor = %f, Qmax = %f\n", 
		    def_groupe[gpe].no_name, learning_rate, reward, discount_factor);
	 coeff = neurone[i].coeff;
      
	 while (coeff != NULL)
	 {
	    if (coeff->evolution == 1)
	       coeff->val = (1 - learning_rate) * coeff->val + learning_rate * (reward + discount_factor * Qmax);

	    coeff = coeff->s;
	 }
      }
   }
}

void destroy_qlearning(int gpe)
{
   dprints("destroy_qlearning(%s): Entering function\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
      free(((mydata_qlearning*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_qlearning(%s): Leaving function\n", def_groupe[gpe].no_name);
}
