/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** Rank-Coding Order Algorithm.
\defgroup RCO RCO
\ingroup NN_Core
\brief Rank-Coding Order Algorithm. This file contains the C fonction for Simon Thorpe's Rank-Coding Order algorithm, which uses the rank information of each input for updating the synaptic weights.We can find mise_a_jour_RCO_neuron(), and apprend_RCO_neuron(), which respectivly enables the network to be updated and to learn.
Input: the rank-order of the input values within the input array in S1. It is given directly with the function f_qsort (-dec)
Output: s is the activation value of the neurons, S1 is their discrete values  (0 or 1): the winner neuron takes all.
\file
\ingroup RCO
\brief Rank-Coding Order Algorithm. This file contains the C fonction for Simon Thorpe's Rank-Coding Order algorithm, 
which uses the rank information of each input for updating the synaptic weights.We can find mise_a_jour_RCO_neuron(), and apprend_RCO_neuron(),
*  which respectivly enables the network to be updated and to learn.
Input: the rank-order of the input values within the input array in S1. It is given directly with the function f_qsort (-dec)
Output: s is the activation value of the neurons, S1 is their discrete values  (0 or 1): 
* the winner neuron takes all. S2 corresponds to the summation-saturation curve during integration, for the winner neuron only.

Author: Alex Pitti
Created: 17/10/2011
Modified: XXX
- author: XXX
- description: XXX
- date: XXX

Theoritical description:
-mise_a_jour_RCO_neuron():
process the neuron's activation

-apprend_RCO_neuron():
modify the weights of the best neuron only 

Description:
This file contains the C fonction for Thorpe's Spikenet neural network.
We can find mise_a_jour_RCO_neuron(), and apprend_RCO_neuron(),
which respectivly enables the network to be updated and to learn.

   
Global variables:
- created: none
- used: none, I think

Macro:
none

Internal Tools:

External Tools: 
-

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: 
24/10/2011 added if (neurone[i].s > winner_val) &&   (i != winner_old)
to not permit to have the same winner at each iteration

TODO a reinforcement learning algorithm à la Sutton & Barto
Known bugs: none

Todo:  Add a threshold, in order to have several winners


http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <time.h> 

typedef struct
{
  float threshold;
  float alpha;
  int best_neuron;
  float *modulation;
  int nb_links;
  
  type_liaison *link_learn;
  
  int gpe_local_mod; /* new */
} MyData_RCO; 

/** create and initialize the network with random weights */
void new_RCO_neuron(int gpe)
{   
    MyData_RCO *my_data = NULL;
    int nb_links, i;
    int first_neuron = def_groupe[gpe].premier_ele;
    int last_neuron = first_neuron + def_groupe[gpe].nbre;
    type_coeff *coeff;
    
    int index, link; /* new */
    int gpe_local_mod = -1; /* new */

    srand(time(NULL)); /* seed initialization */

    printf("new_RCO_neuron(%s): Entering function\n", def_groupe[gpe].no_name);

	my_data = (MyData_RCO *) malloc(sizeof(MyData_RCO));
    if (my_data == NULL)
    {
        printf("echec malloc dans %s\n", __FUNCTION__);
        exit(0);
    }

	my_data->link_learn = NULL;

    /* new */
    if (def_groupe[gpe].data == NULL){	
		index = 0;
		link = find_input_link(gpe, index);
		while (link != -1) {
	    
			if (strcmp(liaison[link].nom, "local_modulation") == 0){
				gpe_local_mod = liaison[link].depart;
				if (def_groupe[gpe].nbre != def_groupe[gpe_local_mod].nbre){
					EXIT_ON_ERROR("Input local_modulation group size (%i) and RCO group size (%i) must be the same", def_groupe[gpe].nbre, def_groupe[gpe_local_mod].nbre);
				}
			}
		    if (strstr(liaison[link].nom, "learn") != NULL){
					   printf("INIT === LINK LEARN NOT NULL\n");
				my_data->link_learn = &liaison[link];
			}
				    
			index++;
			link = find_input_link(gpe, index);
		}
	}

    printf("new_RCO_neuron(%s): gpe=%d, gpe_local_mod=%d, def_groupe[gpe_local_mod].nbre=%d\n", def_groupe[gpe].no_name, gpe, gpe_local_mod, def_groupe[gpe_local_mod].nbre);

    /* weights initialization */
    for (i = first_neuron; i < last_neuron; i++) 
      {
	coeff = neurone[i].coeff;
	while (coeff != NULL)
	  {
	    coeff->val = drand48();	    
	    coeff = coeff->s;
	  }
      }

    nb_links=0;
    coeff = neurone[first_neuron].coeff;
    while (coeff != NULL)
      {
	nb_links=nb_links+1;
	coeff = coeff->s;
      }

    /* nombre d'entrees */
    /*nb_neurons = def_groupe[gpe].nbre;*/

    my_data->alpha = def_groupe[gpe].learning_rate;
    my_data->threshold = 0;
    my_data->best_neuron = 0;
    my_data->nb_links = nb_links;
    my_data->modulation =  (float *) malloc(nb_links*sizeof(float));
    
    my_data->gpe_local_mod = gpe_local_mod; /* new */
    
    for (i = 0; i < nb_links; i++)
    {
      my_data->modulation[i] = (float)(1/(float)(i+1));
    }

    def_groupe[gpe].data = (MyData_RCO *) my_data;

}

/** destroy all the network */
void destroy_RCO_neuron(int gpe)
{
  FILE * pFile;
  int i;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = first_neuron + def_groupe[gpe].nbre;
  type_coeff *coeff;
  char filename[32]="";

  printf("destroy_RCO_neuron(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data != NULL)
    {
      sprintf(filename,"RCO_Weights_%s.dat",def_groupe[gpe].no_name);
      pFile = fopen (filename,"w");

      /* weights copy to one file */
      for (i = first_neuron; i < last_neuron; i++) 
	{
	  coeff = neurone[i].coeff;
	  while (coeff != NULL)
	    {
	      fprintf(pFile,"%f ", coeff->val);
	      coeff = coeff->s;
	    }
	  fprintf(pFile,"\n");
	}
      /* weights copy to one file */
      
      fclose (pFile);
      
      free(((MyData_RCO *) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
    }
  printf("destroy_RCO_neuron(%s): Leaving function\n", def_groupe[gpe].no_name);
}

/** update/activate each neuron and define its output S0 */
void mise_a_jour_RCO_neuron(int i, int gestion_STM, int learn)
{
  type_coeff *coeff;
  float output;
  MyData_RCO *my_data = (MyData_RCO *) def_groupe[neurone[i].groupe].data; /* new */
  int first_neuron = def_groupe[neurone[i].groupe].premier_ele;
  int current_neuron = i - first_neuron;
  int current_modulation = def_groupe[my_data->gpe_local_mod].premier_ele + current_neuron;

  (void)gestion_STM; /* pour gerer les warning */
  (void)learn; /* pour gerer les warning */

  /*output = 0.; old */
  output = neurone[current_modulation].s; /* new */

  coeff = neurone[i].coeff;
  
  if (my_data == NULL)
    {
      EXIT_ON_ERROR("Cannot retrieve data");
    } 

  /* update S0 */
  while (coeff != NULL)
    {
      output = output + (coeff->val * my_data->modulation[(int)(neurone[coeff->entree].s1)]);
      coeff = coeff->s;
    }
  neurone[i].s = output; 
  neurone[i].s1 = neurone[i].s2 = 0.;

  /*printf("mise_a_jour_RCO_neuron2(%s): %d, %d, %f, %d, %d, %d, %d, %f\n", def_groupe[neurone[i].groupe].no_name, i, neurone[i].groupe, neurone[i].s, def_groupe[neurone[i].groupe].premier_ele, def_groupe[my_data->gpe_local_mod].premier_ele, current_neuron, current_modulation, neurone[current_modulation].s);*/

}

/** compute the winner neuron and define its output S1 & S2 */
void gestion_groupe_RCO_neuron(int gpe)
{
  MyData_RCO *my_data = (MyData_RCO *) def_groupe[gpe].data;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = first_neuron + def_groupe[gpe].nbre;
  float winner_val;
  int i, winner_old;
  int winner_idx=0;

  (void) winner_old; // (unused)

  winner_old=my_data->best_neuron;

  winner_val = 0;
  for (i = first_neuron; i < last_neuron; i++)
    {
  	if (neurone[i].s > winner_val)
      /*if ((neurone[i].s > winner_val) && (i != winner_old)) */ /*to not permit to have the same winner at each iteration */
	{
	  winner_val = neurone[i].s;
	  winner_idx = i;
	}
      neurone[i].max = 0;
    }
  
  my_data->best_neuron=winner_idx;
  
  /* update S1 */
  neurone[winner_idx].s1 = 1.;

  /*printf("gestion_groupe_RCO_neuron(%s): winner_id %d, winner_val %f\n", def_groupe[neurone[i].groupe].no_name, winner_idx, winner_val);*/

}

/** update the weights for the winner neuron only */
void apprend_RCO_neuron(int gpe)
{
  type_coeff *coeff;
  float my_learn;
  /*int first_local_mod = -1; new */ 

  MyData_RCO *my_data = (MyData_RCO *) def_groupe[gpe].data;

  if (my_data == NULL)
    {
      EXIT_ON_ERROR("Cannot retrieve data");
    }
   
   //Learn only if link learn is either unset or superior to 0.5
   if(my_data->link_learn != NULL){
		my_learn = neurone[def_groupe[my_data->link_learn->depart].premier_ele].s1;
   }else{
	   my_learn = 1.;
   }
   
   if(my_learn <= 0.) return ;
   
  coeff = neurone[my_data->best_neuron].coeff;

  while (coeff != NULL)
    {
      if (coeff->evolution == 1) /* adaptive weight */
	{
	  if( neurone[coeff->entree].s1<0 ||  neurone[coeff->entree].s1> my_data->nb_links) cprints("ERROR %s indice mod =%f, nb_links: %d\n",__FUNCTION__, neurone[coeff->entree].s1, my_data->nb_links);
	  coeff->val = coeff->val + my_learn * my_data->alpha * (my_data->modulation[ (int)(neurone[coeff->entree].s1) ] - coeff->val);
	}

      coeff = coeff->s; /* next link */
    }
}
