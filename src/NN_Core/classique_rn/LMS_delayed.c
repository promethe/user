/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  winner.c
 \brief simulates winner_takes_all neural network

 Author: xxxxxxxxx
 Created: xxxxxxxxxx
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 -gestion_groupe_winner:
 -apprend_winner:


 Description:
 This file contains the C fonction for winner_takes_all neural network.
 We can find gestion_groupe_winner() and apprend_winner(),
 which respectivly enables to be treated as a special group, and to learn.
 The updates is done traditonaly. As it is a winner_takes_all neural network, gestion_group_winner finds
 the most activated neuron, raises its activity to one and downs the other to zero. Learning is then realised.
 -apprend_winner:
 * apprentissage du lien entre l'entree activee et la cellule gagnante
 * desaprentissage des liens reliant des entrees/sorties non correlles
 -gestion_groupe_winner:
 Generic Winner take all management.
 Read the .s field of the neurons of the group.
 Simply activate the max (i.e. .s1 and .s2 fields
 are set to 1.0). Other neurons are inactive (i.e
 .s1 and .s2 fields are set to 0.0).


 Global variables:
 - created: none Should seldomly happen!!!
 - used: none, I think

 Macro:
 none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: xxxxxxxxxxx
 - description: none
 - input expected group: none
 - where are the data?: none

 Comments: none

 Known bugs:

 Todo: see if the bugs still happen

 http://www.doxygen.org
 ************************************************************/
/* #define DEBUG*/

#include "libx.h"
#include <Kernel_Function/trouver_entree.h>
#include <string.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <stdlib.h>
#include "include/calcule_produit.h"
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <NN_Core/rien.h>

void gestion_groupe_generique_winner(int);
void apprend_winner_lieux_action(int gpe);
void apprend_winner_lieux_action2(int gpe);
void mise_a_jour_lieux_action(int n, int gestion_STM, int learn);
void mise_a_jour_lieux_action2(int n, int gestion_STM, int learn);
void mise_a_jour_lieux_action3(int n, int gestion_STM, int learn);
void gradient_lieux_action_inst(int i, float gradient, float Var, float var_accumul);
void gradient_lieux_action_accumul(int i, float gradient);
void apprend_winner_lieux_action3(int gpe);
void gradient_lieux_action_inst3(int i, float pondere, float eps_local, float vigilence_local);
void mise_a_jour_lieux_action4(int n, int gestion_STM, int learn);
void mise_a_jour_lieux_action4_3(int n, int gestion_STM, int learn);
void apprend_winner_lieux_action4_2(int gpe);
void gradient_lieux_action_inst_4_2(int i, float gradient, float Var, float var_accumul);
void gradient_lieux_action_accumul_4_2(int i, float gradient);

float heaviside(float min, float arg)
{
  if (arg > min) return 1.;
  else return 0.;
}

typedef struct MyData_LMS_delayed {
  int gpe_vigilence;
  int gpe_lieux;
  int gpe_action;
  int gpe_error;
  int gpe_adaptation;
  int lieux_max_prec;
  float seuil_apprend;

  int gpe_epsilon;
} MyData_LMS_delayed;

void new_LMS_delayed(int gpe)
{
  int l, j, i_liaison = 0, i;
  int gpe_vigilence = -1, gpe_lieux = -1, gpe_action = -1, gpe_error = -1, gpe_epsilon = -1;
  MyData_LMS_delayed *my_data = NULL;
  prom_images_struct *lieux_action = NULL;
  type_coeff *coeff;
  int mode_la = 0;
  int lieux_max_prec = 0;
  float seuil_apprend = 0.00001; /*0.01 */
  char param_link[32];
  int gpe_adaptation = -1;

  dprints("new_LMS_delayed(%d)\n",gpe);

  l = find_input_link(gpe, i_liaison);
  while (l != -1)
  {
    if (strcmp(liaison[l].nom, "vigilence") == 0)
    {
      gpe_vigilence = liaison[l].depart;
    }
    if (strcmp(liaison[l].nom, "adaptation") == 0)
    {
      gpe_adaptation = liaison[l].depart;
    }
    if (strcmp(liaison[l].nom, "error") == 0)
    {
      gpe_error = liaison[l].depart;
    }
    if (strcmp(liaison[l].nom, "epsilon") == 0)
    {
      gpe_epsilon = liaison[l].depart;
    }

    /*Mode de depart: apprend sur le winner avec decalage temporel a chaque transition
     Possibilite de plusieurs winner : les max pour achque lieux.
     Mais la, il buge*/
    if (strcmp(liaison[l].nom, "-la") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action;
      mode_la = 1;
    }
    /*LMS_delayed: voir roman pour equation LMS_delayed: Utilisation d'une decalage temporel*/
    if (strcmp(liaison[l].nom, "-la2") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action2;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action2;
      def_groupe[gpe].appel_gestion = rien;
      mode_la = 2;
      if (continue_simulation_status == START) for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
        coeff = neurone[i + def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
          if (coeff->evolution == 1) coeff->val = liaison[coeff->gpe_liaison].norme;
          coeff = coeff->s;
        }
      }
    }
    /*Heeb_delayed: comme LMS, mais avec une regle d'apprentissage du type Hebb (pas de decalage temporel)*/
    if (strcmp(liaison[l].nom, "-la3") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action3;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action2;
      def_groupe[gpe].appel_gestion = rien;
      mode_la = 3;
      if (continue_simulation_status == START) for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
        coeff = neurone[i + def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
          if (coeff->evolution == 1) coeff->val = liaison[coeff->gpe_liaison].norme;
          coeff = coeff->s;
        }
      }
    }
    /*Heeb_delayed: comme LMS, mais avec une regle d'apprentissage du type Hebb (pas de decalage temporel)*/
    if (strcmp(liaison[l].nom, "-la3.2") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action2;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action3;
      def_groupe[gpe].appel_gestion = rien;
      mode_la = 3;
      if (continue_simulation_status == START) for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
        coeff = neurone[i + def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
          if (coeff->evolution == 1) coeff->val = liaison[coeff->gpe_liaison].norme;
          coeff = coeff->s;
        }
      }
    }
    /* Apprend sur le gagnant. Act sur toutes les entrees sensoriels Apprend LMS, Decalage temporel. S2 en entree doit donner le gagnant. Loi LMS*/
    if (strcmp(liaison[l].nom, "-la4.2") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action4_2;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action4;
      def_groupe[gpe].appel_gestion = rien;
      mode_la = 4;
      if (continue_simulation_status == START) for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
        coeff = neurone[i + def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
          if (coeff->evolution == 1) coeff->val = liaison[coeff->gpe_liaison].norme;
          coeff = coeff->s;
        }
      }
    }
    /* Apprend sur le gagnant. Act sur toutes les entrees sensoriels Apprend LMS, Decalage temporel. S2 en entree doit donner le gagnant. Loi Hebb*/
    if (strcmp(liaison[l].nom, "-la4.3") == 0)
    {
      def_groupe[gpe].appel_apprend = apprend_winner_lieux_action4_2;
      def_groupe[gpe].appel_activite = mise_a_jour_lieux_action4_3;
      def_groupe[gpe].appel_gestion = rien;
      mode_la = 4;
      if (continue_simulation_status == START) for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
        coeff = neurone[i + def_groupe[gpe].premier_ele].coeff;
        while (coeff != NULL)
        {
          if (coeff->evolution == 1) coeff->val = liaison[coeff->gpe_liaison].norme;
          coeff = coeff->s;
        }
      }
    }
    if (prom_getopt(liaison[l].nom, "s", param_link) == 2)
    {
      seuil_apprend = atof(param_link);
    }
    i_liaison++;
    l = find_input_link(gpe, i_liaison);

  }
  if (mode_la >= 1)
  {
    coeff = neurone[def_groupe[gpe].premier_ele].coeff;
    while (coeff != NULL)
    {
      if (coeff->evolution == 1)
      {
        gpe_lieux = neurone[coeff->entree].groupe;
      }
      else
      {
        gpe_action = neurone[coeff->entree].groupe;
      }
      coeff = coeff->s;
    }
    lieux_action = calloc_prom_image(1, def_groupe[gpe_action].nbre, def_groupe[gpe_lieux].nbre, 1);
    def_groupe[gpe].ext = (prom_images_struct *) lieux_action;

    printf("\t image initialise dans %s\n", __FUNCTION__);

  }
  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {
      if (coeff->evolution == 0) coeff = coeff->s;
      if (coeff->evolution == 1)
      {
        lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i] = (unsigned char) (int) ((coeff->val * 255));

      }
      coeff = coeff->s;

    }
  }

  /*     if(continue_simulation_status!=CONTINUE)
   //     {
   // 	printf("no continue_learning ... reset des poids du groupe %s\n",def_groupe[gpe].no_name);
   // 	for(i=0;i<def_groupe[gpe].nbre;i++)
   // 	{
   // 	    coeff = neurone[def_groupe[gpe].premier_ele+i].coeff;
   // 	    while (coeff != NULL)
   // 	    {
   // 	      if (coeff->evolution == 1)
   // 	      {
   // 		  coeff->val=0.;
   // 	      }
   // 	      coeff = coeff->s;
   // 	  }
   // 	}
   // //
   //     }
   */

  if (def_groupe[gpe].learning_rate <= 0.)
  {
    def_groupe[gpe].appel_apprend = rien;
    printf("\t Learning rate nul -> apprentissage=rien %d\n", gpe);
  }

  my_data = (MyData_LMS_delayed *) malloc(sizeof(MyData_LMS_delayed));
  my_data->gpe_vigilence = gpe_vigilence;
  my_data->gpe_lieux = gpe_lieux;
  my_data->gpe_action = gpe_action;
  my_data->gpe_error = gpe_error;
  my_data->gpe_adaptation = gpe_adaptation;
  my_data->lieux_max_prec = lieux_max_prec;
  my_data->seuil_apprend = seuil_apprend;
  my_data->gpe_epsilon = gpe_epsilon;

  def_groupe[gpe].data = (MyData_LMS_delayed *) my_data;
}

void mise_a_jour_lieux_action(int n, int gestion_STM, int learn)
{
  int *thelieuxmax, *theactionmax;
  int maxfound;
  int gpe;
  int i, j, k, lieux_max = -1, action_max = -1;
  float max = 0.;
  prom_images_struct *lieux_action;
  MyData_LMS_delayed *my_data;
  int gpe_lieux;
  int gpe_action;
  type_coeff *coeff;

  (void) gestion_STM;
  (void) learn;

  gpe = neurone[n].groupe;
  my_data = (MyData_LMS_delayed *) (def_groupe[gpe].data);
  gpe_lieux = my_data->gpe_lieux;
  gpe_action = my_data->gpe_action;
  lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

  /*tableau des lieux max */
  thelieuxmax = malloc(sizeof(int) * def_groupe[gpe].nbre_de_1);
  theactionmax = malloc(sizeof(int) * def_groupe[gpe].nbre_de_1);
  for (i = 0; i < def_groupe[gpe].nbre_de_1; i++)
  {
    thelieuxmax[i] = -1;
    theactionmax[i] = -1;
  }

  /*recuper le contenu des poid */
  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {
      if (coeff->evolution == 0) coeff = coeff->s;
      if (coeff->evolution == 1)
      {
        lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i] = (unsigned char) (int) ((coeff->val * 255));

      }
      coeff = coeff->s;
    }
  }

  /*met les neurone a zero */
  for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;

  /*cherche les lieux max */
  for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
  {
    max = 0.;
    lieux_max = -1;
    for (i = 0; i < def_groupe[gpe_lieux].nbre; i++)
    {
      if (max < neurone[i + def_groupe[gpe_lieux].premier_ele].s1)
      {
        maxfound = 0;
        for (k = 0; k < j; k++)
        {

          if (thelieuxmax[k] == i) maxfound = 1;
        }
        if (maxfound == 0)
        {
          max = neurone[i + def_groupe[gpe_lieux].premier_ele].s1;
          lieux_max = i;
        }
      }

    }
    thelieuxmax[j] = lieux_max;
    dprints("lieux max %d : %d\n", j, thelieuxmax[j]);
  }
  max = 0.;

  /*cherche action max pour lieux max dans le matrice ou dans l'entree */
  for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
  {
    if (thelieuxmax[j] != -1)
    {
      action_max = -1;
      max = 0.;
      for (i = 0; i < def_groupe[gpe_action].nbre; i++)
      {
        if (max < lieux_action->images_table[0][thelieuxmax[j] * def_groupe[gpe_action].nbre + i])
        {
          max = lieux_action->images_table[0][thelieuxmax[j] * def_groupe[gpe_action].nbre + i];
          action_max = i;
        }
      }
      /*Si pas de max sur la ligne, on prend le max en entree */
      if (action_max == -1)
      {
        max = 0.;
        for (i = 0; i < def_groupe[gpe_action].nbre; i++)
        {
          if (max < neurone[i + def_groupe[gpe_action].premier_ele].s1)
          {
            max = neurone[i + def_groupe[gpe_action].premier_ele].s1;
            action_max = i;
          }
        }
      }
    }
    else action_max = -1;

    theactionmax[j] = action_max;
    dprints("action max %d: %d\n\n", j, theactionmax[j]);
  }

  for (j = 0; j < def_groupe[gpe].nbre_de_1; j++)
  {
    /*active le neurone gagnant */
    if (theactionmax[j] != -1)
    {
      neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s = neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s1 = neurone[def_groupe[gpe_lieux].premier_ele + thelieuxmax[j]].s1;
      neurone[def_groupe[gpe].premier_ele + theactionmax[j]].s2 = 1.;
    }
  }
  free(theactionmax);
  free(thelieuxmax);
}

/*#define DEBUG*/
void apprend_winner_lieux_action(int gpe)
{
  int i, j, lieux_max = -1, lieux_max_prec = 0;
  MyData_LMS_delayed *my_data = def_groupe[gpe].data;
  int gpe_lieux = my_data->gpe_lieux;
  int gpe_action = my_data->gpe_action;
  int gpe_error = my_data->gpe_error;
  int gpe_adaptation = my_data->gpe_adaptation;
  float seuil_apprend = my_data->seuil_apprend;
  int adapt = 1;
  float max = 0.;
  prom_images_struct *lieux_action = def_groupe[gpe].ext;
  float local_vigilence = 1.;
  unsigned char temp;
  type_coeff *coeff;
  float dec_neur;
  int nb_shift;
  int s;

  if (my_data->gpe_vigilence != -1) local_vigilence = neurone[def_groupe[my_data->gpe_vigilence].premier_ele].s1;
  lieux_max_prec = my_data->lieux_max_prec;

  max = 0.;
  for (i = 0; i < def_groupe[gpe_lieux].nbre; i++)
  {
    if (max < neurone[i + def_groupe[gpe_lieux].premier_ele].s1)
    {
      max = neurone[i + def_groupe[gpe_lieux].premier_ele].s1;
      lieux_max = i;
    }
  }
  my_data->lieux_max_prec = lieux_max;
  if (local_vigilence > 0.5)
  {
    for (i = 0; i < def_groupe[gpe_action].nbre; i++)
    {
      lieux_action->images_table[0][lieux_max * def_groupe[gpe_action].nbre + i] = (int) (255 * neurone[def_groupe[gpe_action].premier_ele + i].s1);
    }
    dprints("apprend lieux %d \n", lieux_max);

  }
  if (gpe_adaptation != -1) if (neurone[def_groupe[gpe_adaptation].premier_ele + lieux_max_prec].s1 < 0.5)
  {
    adapt = 0;
    dprints("No adapt %d\n", gpe);
  }

  if (gpe_error != -1 && lieux_max_prec != lieux_max && adapt == 1)
  {
    dprints("PC %d-> PC %d\n", lieux_max_prec, lieux_max);
    dprints("error winner %f\n", neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1);

    if (neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1 < (-1. * seuil_apprend))
    {

      dec_neur = neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1 * def_groupe[gpe_action].nbre / 2.;
      nb_shift = (int) fabs(dec_neur / 2.) + 1.;
      dprints("decalage gauche de action du lieu %d de %d neurone \n", lieux_max_prec, nb_shift);

      for (s = 0; s < nb_shift; s++)
      {
        temp = lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre];
        for (i = 0; i < def_groupe[gpe_action].nbre - 1; i++)
        {
          lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + i] = lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + i + 1];
        }
        lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + def_groupe[gpe_action].nbre - 1] = temp;
      }
    }
    else if (neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1 > seuil_apprend)
    {
      dec_neur = neurone[def_groupe[gpe_error].premier_ele + lieux_max_prec].s1 * def_groupe[gpe_action].nbre / 2.;
      nb_shift = (int) fabs(dec_neur / 2.) + 1.;
      dprints("decalage droite de action du lieu %d de %d neurone\n", lieux_max_prec, nb_shift);
      
      for (s = 0; s < nb_shift; s++)
      {
        temp = lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + def_groupe[gpe_action].nbre - 1];
        for (i = def_groupe[gpe_action].nbre - 1; i > 0; i--)
        {
          lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + i] = lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre + i - 1];
        }
        lieux_action->images_table[0][lieux_max_prec * def_groupe[gpe_action].nbre] = temp;
      }
    }

  }

  /*On retient le lieux gagnant et l'erreur precedente car c est au changement de leix qu'on va faire la correction*/
  my_data->lieux_max_prec = lieux_max;

  /*on remet l image dans les coeff*/
  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {

      if (coeff->evolution == 0)
      {
        coeff = coeff->s;
      }
      if (coeff->evolution == 1)
      {
        coeff->val = (float) (lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i]) / 255.;
#ifdef DEBUG
        if (coeff->val > 0.01)
        {
          printf("%d im \n", (int) (lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i]));
          printf("coef_val : %f\n", coeff->val);
        }
#endif
      }
      coeff = coeff->s;
    }
  }
  dprints("fin %s\n", __FUNCTION__);
}

void mise_a_jour_lieux_action2(int n, int gestion_STM, int learn)
{
  int gpe = neurone[n].groupe;
  int i;
  float s, sum_p, Sd;
  type_coeff *synapse;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  (void) gestion_STM;
  (void) learn;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  i = n;

  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  Sd = 0.; /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
  sum_p = 0.0;
  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {

    if (synapse->evolution != 0)
    { /* nous ne sommons pas le lien UC */
      switch (liaison[synapse->gpe_liaison].mode)
      {
      case 0:
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
        break; /*neurone[synapse->entree].s2;  break;s2 */
      case 2:
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
        break; /*s */
      default:
        EXIT_ON_ERROR("Erreur mode liaison(%d) incorrect dans mise_a_jour_lieux_action2, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
      }
    }
    else
    {
      /* par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
      Sd = Sd + synapse->val * neurone[synapse->entree].s1;
      /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */
    }

    /*tau=liaison[synapse->gpe_liaison].temps;
     synapse->moy=(synapse->moy*tau+neurone[synapse->entree].s2)/(tau+1); *//*Si on veut gerer le retard */
    /* non il serait plus simple de ce servir de la gestion du kernel... */
    /* tres gros risque de pb du a l'ecrasement des valeurs par le kernel. */
    synapse = synapse->s;
  }

  s = neurone[i].s = sum_p;
  neurone[i].d = Sd - s;
#ifdef DEBUG
  if(neurone[i].d<-0.5)  printf("desapprend %d car Sd=%f, s=%f donc g=%f\n",i,Sd,s,neurone[i].d);
  if(neurone[i].d>0.5)   printf("apprend %d\n",i);
#endif    
  s = Sd * vigilence_local + s * (1.0 - vigilence_local);

  neurone[i].s1 = neurone[i].s2 = s;
  (void) eps_local;
}

void apprend_winner_lieux_action2(int gpe)
{
  float gradient, Var_inst, Var_accumul;
  int size_Y = def_groupe[gpe].tailley;
  int size_X = def_groupe[gpe].taillex;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
  int i, j;
  prom_images_struct *lieux_action;
  MyData_LMS_delayed *my_data;
  int gpe_lieux, gpe_action;
  type_coeff * coeff;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  Var_inst = vigilence_local * eps_local * def_groupe[gpe].learning_rate;
  Var_accumul = eps_local * def_groupe[gpe].learning_rate;

  for (i = first_neuron; i < last_neuron; i++)
  {
    gradient = neurone[i].d;
    /*mise a jour des poids sur les entrees Conditionnelles du neurone i */
    gradient_lieux_action_accumul(i, Var_accumul);
    gradient_lieux_action_inst(i, gradient, Var_inst, Var_accumul);
  }

  my_data = (MyData_LMS_delayed *) (def_groupe[gpe].data);
  gpe_lieux = my_data->gpe_lieux;
  gpe_action = my_data->gpe_action;
  lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

  /*tableau des lieux max */
  /*recuper le contenu des poid */
  /*printf("update image des poinds\n");*/

  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {
      if (coeff->evolution == 0) coeff = coeff->s;
      if (coeff->evolution == 1)
      {
        lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i] = (unsigned char) (int) ((coeff->val * 255));

      }
      coeff = coeff->s;
    }
  }
  (void) eps_local;
}

void gradient_lieux_action_inst(int i, float gradient, float Var, float Var_accumul)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
  type_coeff *synapse;
  float dc;

  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    if (synapse->evolution == 1)
    {

      switch (liaison[synapse->gpe_liaison].mode)
      {
      case 0: /*liens produit after compet */
        dc = gradient * neurone[synapse->entree].s1;
        break;

      case 2: /*liens produit before compet */
        dc = gradient * neurone[synapse->entree].s;
        break;
      default:
        EXIT_ON_ERROR("Erreur mode liaison(%d) incorrect dans mise_a_jour_neurone_lieux_action_inst, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
      }
#ifdef DEBUG
      if(dc<-0.5) printf("desapprendrai %d: dc = %f et gradient = %f\n",i,dc,gradient);
      if(dc>0.5)  printf("apprendrai %d: dc = %f\n",i,dc);
#endif 
      if (isequal(Var_accumul, 0.)) synapse->Nbre_ES = dc;
      else synapse->Nbre_ES = 0.;

      synapse->val = synapse->val + Var * dc;

#ifdef DEBUG
      if(fabs(dc)>0.5) printf("maj val : %f \n",synapse->val);
#endif 

      /* if(dc>0.) synapse->val=synapse->val+Var*dc; else synapse->val=synapse->val+Var*dc; */
      /*on vire l'histoire du desapprentissage car les poids ne redescendent pas sinon */

      /* dissymetrie apprentissage et desapprentissage facteur 1 a 100 ad hoc, a ne pas remonter PG */
    }
    synapse = synapse->s;
  }
}

void gradient_lieux_action_accumul(int i, float Var)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
  type_coeff *synapse;

  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    if (synapse->evolution == 1)
    {
      synapse->val = synapse->val + synapse->Nbre_ES * Var;
    }
    synapse = synapse->s;
  }
}
/****************************************************************************/

/***************************************************************************/

void mise_a_jour_lieux_action3(int n, int gestion_STM, int learn)
{
  int gpe = neurone[n].groupe;
  int i;
  float s, sum_p, Sd;
  type_coeff *synapse;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  (void) gestion_STM;
  (void) learn;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  i = n;

  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  Sd = 0.; /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
  sum_p = 0.0;
  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {

    if (synapse->evolution != 0)
    { /* nous ne sommons pas le lien UC */
      switch (liaison[synapse->gpe_liaison].mode)
      {
      case 0:
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
        break; /*neurone[synapse->entree].s2;  break;s2 */
      case 2:
        sum_p = sum_p + synapse->val * neurone[synapse->entree].s;
        break; /*s */
      default:
        EXIT_ON_ERROR("Erreur mode liaison(%d) incorrect dans mise_a_jour_lieux_action3, groupe %s\n", liaison[synapse->gpe_liaison].mode, def_groupe[neurone[i].groupe].no_name);
      }
    }
    else
    {
      /* par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
      Sd = Sd + synapse->val * neurone[synapse->entree].s1;
      /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */
    }

    /*tau=liaison[synapse->gpe_liaison].temps;
     synapse->moy=(synapse->moy*tau+neurone[synapse->entree].s2)/(tau+1); *//*Si on veut gerer le retard */
    /* non il serait plus simple de ce servir de la gestion du kernel... */
    /* tres gros risque de pb du a l'ecrasement des valeurs par le kernel. */
    synapse = synapse->s;
  }

  s = neurone[i].s = sum_p;
  neurone[i].d = Sd;
#ifdef DEBUG
  if(neurone[i].d<-0.5)  printf("desapprend %d car Sd=%f, s=%f donc g=%f\n",i,Sd,s,neurone[i].d);
  if(neurone[i].d>0.5)   printf("apprend %d\n",i);
#endif    
  s = Sd * vigilence_local + s * (1.0 - vigilence_local);

  neurone[i].s1 = neurone[i].s2 = s;
  (void) eps_local;
}

void apprend_winner_lieux_action3(int gpe)
{
  type_coeff *synapse;
  int size_Y = def_groupe[gpe].tailley;
  int size_X = def_groupe[gpe].taillex;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
  int i, j;
  prom_images_struct *lieux_action;
  MyData_LMS_delayed *my_data;
  int gpe_lieux, gpe_action;
  type_coeff * coeff;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  for (i = first_neuron; i < last_neuron; i++)
  {
    synapse = neurone[i].coeff;
    while (synapse != NULL)
    {
      if (synapse->evolution == 0) if (neurone[synapse->entree].s1 > 0.) gradient_lieux_action_inst3(i, neurone[synapse->entree].s1, eps_local, vigilence_local);
      synapse = synapse->s;

    }
  }

  my_data = (MyData_LMS_delayed *) (def_groupe[gpe].data);
  gpe_lieux = my_data->gpe_lieux;
  gpe_action = my_data->gpe_action;
  lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

  /*tableau des lieux max */
  /*recuper le contenu des poid */
  /*printf("update image des poinds\n");*/
  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {
      if (coeff->evolution == 0) coeff = coeff->s;
      if (coeff->evolution == 1)
      {
        lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i] = (unsigned char) (int) ((coeff->val * 255));

      }
      coeff = coeff->s;

    }
  }
}

void gradient_lieux_action_inst3(int i, float pondere, float eps_local, float vigilence_local)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
  type_coeff *synapse;
  synapse = neurone[i].coeff;

  while (synapse != NULL)
  {
    if (synapse->evolution == 1)
    {
      synapse->val = synapse->val + neurone[synapse->entree].s1 * pondere * eps_local * vigilence_local;
    }
    synapse = synapse->s;
  }
}

/***********************************************************************************************/
/***********************************************************************************************/

void mise_a_jour_lieux_action4(int n, int gestion_STM, int learn)
{
  int gpe = neurone[n].groupe;
  int i;
  float s, sum_p, Sd, sum_c;
  type_coeff *synapse;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  (void) gestion_STM;
  (void) learn;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  i = n;

  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  Sd = 0.; /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
  sum_p = 0.0;
  sum_c = 0.;
  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    /*Calcul de la sortie predite sum_p et de la sortie en fonction du gagnant : sum_c*/
    if (synapse->evolution != 0)
    {
      /* nous ne sommons pas le lien UC */
      sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
      sum_c = sum_c + synapse->val * (1. - heaviside(0., 1. - neurone[synapse->entree].s2));
    }
    else
    /* par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
    Sd = Sd + synapse->val * neurone[synapse->entree].s1;
    /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */

    /*tau=liaison[synapse->gpe_liaison].temps;
     synapse->moy=(synapse->moy*tau+neurone[synapse->entree].s2)/(tau+1); *//*Si on veut gerer le retard */
    /* non il serait plus simple de ce servir de la gestion du kernel... */
    /* tres gros risque de pb du a l'ecrasement des valeurs par le kernel. */
    synapse = synapse->s;
  }

  s = neurone[i].s = sum_p;
  neurone[i].d = Sd - sum_c;
  
#ifdef DEBUG
  if(neurone[i].d<-0.5)  printf("desapprend %d car Sd=%f, s=%f donc g=%f\n",i,Sd,s,neurone[i].d);
  if(neurone[i].d>0.5)   printf("apprend %d\n",i);
#endif    

  s = Sd * vigilence_local + s * (1.0 - vigilence_local);
  neurone[i].s1 = neurone[i].s2 = s;
  (void) eps_local;
}

void mise_a_jour_lieux_action4_3(int n, int gestion_STM, int learn)
{
  int gpe = neurone[n].groupe;
  int i;
  float s, sum_p, Sd, sum_c;
  type_coeff *synapse;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon   = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  (void) gestion_STM;
  (void) learn;
  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  i = n;

  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  Sd = 0.; /*activite sur la ou les voie(s) inconditionnelle(s), modif PG */
  sum_p = 0.0;
  sum_c = 0.;
  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    /*Calcul de la sortie predite sum_p et de la sortie en fonction du gagnant : sum_c*/
    if (synapse->evolution != 0)
    {
      /* nous ne sommons pas le lien UC */
      sum_p = sum_p + synapse->val * neurone[synapse->entree].s1;
      sum_c = sum_c + synapse->val * (1. - heaviside(0., 1. - neurone[synapse->entree].s2));
    }
    else
    /* par contre on sauvegarde la valeur du lien UC pour l'apprentissage */
    Sd = Sd + synapse->val * neurone[synapse->entree].s1;
    /* modif pour les cas ou l'on aurait plusieurs liens inconditionnels, modif PG */

    /*tau=liaison[synapse->gpe_liaison].temps;
     synapse->moy=(synapse->moy*tau+neurone[synapse->entree].s2)/(tau+1); *//*Si on veut gerer le retard */
    /* non il serait plus simple de ce servir de la gestion du kernel... */
    /* tres gros risque de pb du a l'ecrasement des valeurs par le kernel. */
    synapse = synapse->s;
  }

  s = neurone[i].s = sum_p;
  neurone[i].d = Sd /*- sum_c*/;
#ifdef DEBUG
  if(neurone[i].d<-0.5)  printf("desapprend %d car Sd=%f, s=%f donc g=%f\n",i,Sd,s,neurone[i].d);
  if(neurone[i].d>0.5)   printf("apprend %d\n",i);
#endif    
  s = Sd * vigilence_local + s * (1.0 - vigilence_local);

  neurone[i].s1 = neurone[i].s2 = s;
  (void) eps_local;
}

void apprend_winner_lieux_action4_2(int gpe)
{
  float gradient, Var_inst, Var_accumul;
  int size_Y = def_groupe[gpe].tailley;
  int size_X = def_groupe[gpe].taillex;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
  int i, j;
  prom_images_struct *lieux_action;
  MyData_LMS_delayed *my_data;
  int gpe_lieux, gpe_action;
  type_coeff * coeff;
  int gpe_vigilence = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_vigilence;
  int gpe_epsilon = ((MyData_LMS_delayed *) (def_groupe[gpe].data))->gpe_epsilon;
  float eps_local = eps;
  float vigilence_local = vigilence;

  if (gpe_epsilon != -1)
  {
    eps_local = neurone[def_groupe[gpe_epsilon].premier_ele].s1;
  }
  if (gpe_vigilence != -1)
  {
    vigilence_local = neurone[def_groupe[gpe_vigilence].premier_ele].s1;
  }

  Var_inst = vigilence_local * eps_local * def_groupe[gpe].learning_rate;
  Var_accumul = eps_local * def_groupe[gpe].learning_rate;

  for (i = first_neuron; i < last_neuron; i++)
  {
    gradient = neurone[i].d;
    /*mise a jour des poids sur les entrees Conditionnelles du neurone i */
    gradient_lieux_action_accumul_4_2(i, Var_accumul);
    gradient_lieux_action_inst_4_2(i, gradient, Var_inst, Var_accumul);
  }

  my_data = (MyData_LMS_delayed *) (def_groupe[gpe].data);
  gpe_lieux = my_data->gpe_lieux;
  gpe_action = my_data->gpe_action;
  lieux_action = (prom_images_struct *) (def_groupe[gpe].ext);

  /*tableau des lieux max */
  /*recuper le contenu des poid */
  /*printf("update image des poinds\n");*/
  for (i = 0; i < def_groupe[gpe_action].nbre; i++)
  {
    coeff = neurone[def_groupe[gpe].premier_ele + i].coeff;
    for (j = 0; j < def_groupe[gpe_lieux].nbre; j++)
    {
      if (coeff->evolution == 0) coeff = coeff->s;
      if (coeff->evolution == 1)
      {
        lieux_action->images_table[0][j * def_groupe[gpe_action].nbre + i] = (unsigned char) (int) ((coeff->val * 255));

      }
      coeff = coeff->s;
    }
  }
}

void gradient_lieux_action_inst_4_2(int i, float gradient, float Var, float Var_accumul)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
  type_coeff *synapse;
  float dc;

  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    if (synapse->evolution == 1)
    {
      dc = gradient * (1. - heaviside(0., 1. - neurone[synapse->entree].s2));

#ifdef DEBUG
      if(dc<-0.5)  printf("desapprendrai %d: dc = %f et gradient = %f\n",i,dc,gradient);
      if(dc>0.5)   printf("apprendrai %d: dc = %f\n",i,dc);
#endif 
      if (isequal(Var_accumul, 0.)) synapse->Nbre_ES = dc;
      else synapse->Nbre_ES = 0.;

      synapse->val = synapse->val + Var * dc;

#ifdef DEBUG
      if(fabs(dc)>0.5) printf("maj val : %f \n",synapse->val);
#endif 

      /* if(dc>0.) synapse->val=synapse->val+Var*dc; else synapse->val=synapse->val+Var*dc; */
      /*on vire l'histoire du desapprentissage car les poids ne redescendent pas sinon */

      /* dissymetrie apprentissage et desapprentissage facteur 1 a 100 ad hoc, a ne pas remonter PG */
    }
    synapse = synapse->s;
  }
}

void gradient_lieux_action_accumul_4_2(int i, float Var)
/* gradient: terme d'erreur, Var: vitesse d'apprentissage */
{
  type_coeff *synapse;

  synapse = neurone[i].coeff;
  while (synapse != NULL)
  {
    if (synapse->evolution == 1)
    {
      synapse->val = synapse->val + synapse->Nbre_ES * Var;
    }
    synapse = synapse->s;
  }
}
