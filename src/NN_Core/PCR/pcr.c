/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <limits.h>
#include <public_tools/Vision.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define DEBUG

#define MAXIMUM_SIZE_OF_OPTION 128

#define PCR_EPSILON 0.01
#define PCR_XI      0.01
#define PCR_LAMBDA  0.1

typedef struct my_data
{
    float tau;
    int size_of_reward, size_of_sensation, size_of_output, index_of_link_of_sensation;
    int number_of_iterations;
    noeud_modulation *neuromodulation_of_delta_reward, *neuromodulation_of_enable;
    type_neurone *neurons_of_sensation, *output_neurons;

}My_data;


/* Calcul local (pour ce type de my_data, d'ou le static) du potentiel */
static void saturate(float *value)
{
    if (value[0] > 1.0) value[0] = 1.0;
    else if (value[0] < 0.0) value[0] = 0.0;
}

static void compute_Nbres(My_data *my_data)
{
    type_coeff *coeff;
    type_neurone *current_neuron;
    int i;

    my_data->number_of_iterations++;

    for (i = 0 ; i < my_data->size_of_output; i++)
    {
        current_neuron = &my_data->output_neurons[i];
        coeff = current_neuron->coeff;

        while (coeff != NULL)
        {
            coeff->Nbre_S += (current_neuron->s1 - coeff->Nbre_S) / my_data->number_of_iterations; /* Equivalent a calculer la moyenne des s1 pour toutes les itérations ... */
            coeff->Nbre_E += (neurone[coeff->entree].s1 - coeff->Nbre_E) / my_data->number_of_iterations;
            coeff->Nbre_ES += (neurone[coeff->entree].s1 * current_neuron->s1 - coeff->Nbre_ES) / my_data->number_of_iterations;
            coeff = coeff->s;
        }
    }
}




/******************* Main functions *************************/
void new_PCR(int index_of_group)
{
    My_data *my_data;
    type_groupe  *group;
    my_data = ALLOCATION(My_data);

    my_data = ALLOCATION(My_data);
    group = &def_groupe[index_of_group];
    group->data = my_data;

    my_data->output_neurons = &neurone[group->premier_ele];
    my_data->size_of_output = group->nbre;
    my_data->number_of_iterations = 0;

    my_data->neuromodulation_of_delta_reward = get_noeud_modulation_ptr(index_of_group, "delta_reward");
    if ( my_data->neuromodulation_of_delta_reward == NULL ) EXIT_ON_ERROR ("Neuromodulation 'delta_reward' not found in group: %s .", def_groupe[index_of_group].no_name);

    my_data->neuromodulation_of_enable = get_noeud_modulation_ptr(index_of_group, "enable");
}



void update_neural_activity_PCR(int index_of_neuron, int gestion_STM, int learn)
{
    My_data *my_data;
    float noise_level, potential;
    type_coeff *current_coeff;
    type_groupe *group;
    (void)gestion_STM;
    (void)learn;

    group = &def_groupe[neurone[index_of_neuron].groupe];

    noise_level = group->noise_level;

    my_data = group->data;
    potential = 0.0;

    for (current_coeff = neurone[index_of_neuron].coeff; current_coeff != NULL; current_coeff = current_coeff->s)
    {
        /* Ajout de la sortie - ((2*Wij-1)*Pij + 1)*Ij + bruit(Wij)             */
        potential += ((2.0*current_coeff->val - 1.0) * current_coeff->proba + 1.0) * neurone[current_coeff->entree].s1 + drand48() * noise_level;
    }
    neurone[index_of_neuron].s = potential;
}


void update_group_PCR(int index_of_group)
{
    int i;
    float  maximal_potential, enable;

    My_data *my_data;
    type_groupe *group;
    type_neurone *neurons_of_sensation, *winning_neuron, *current_neuron;

    group = &def_groupe[index_of_group];
    my_data = (My_data*)group->data;

    if (my_data->neuromodulation_of_enable != NULL) calcule_neuromodulation_fast(index_of_group, my_data->neuromodulation_of_enable, &enable);
    else enable = 1;

    if (enable > 0.5)
    {
        neurons_of_sensation = my_data->neurons_of_sensation;

        /* Calcul de la sortie et competition, forcement un gagnant. */
        current_neuron = &my_data->output_neurons[0];
        winning_neuron = current_neuron;
        maximal_potential = current_neuron->s;
        current_neuron->s1 = 1.0;
        current_neuron->s2 = 1.0;
#ifdef DEBUG
        printf("\nVote %d: %f\n", 0, current_neuron->s);
#endif

        /* On regarde si les suivants peuvent gagner */
        for (i = 1; i < my_data->size_of_output; i++)
        {
            current_neuron = &my_data->output_neurons[i];

            #ifdef DEBUG
               printf("Vote %d: %f\n", i, current_neuron->s);
            #endif

            if (current_neuron->s > maximal_potential)
            {
                /*  On efface le precedent gagnant s'il y en a eu un .*/
                if (winning_neuron != NULL)
                {
                    winning_neuron->s1 = 0.0;
                    winning_neuron->s2 = 0.0;
                }

                /* On active le nouveau gagnant */
                current_neuron->s1 = 1.0;
                current_neuron->s2 = 1.0;
                winning_neuron = current_neuron;
                maximal_potential = current_neuron->s;
            }
            else
            {
                current_neuron->s1 = 0.0;
                current_neuron->s2 = 0.0;
            }
        }
    }
}

void learn_PCR(int index_of_group)
{
    My_data *my_data;
    float correlation, delta_reward, temp, previous_proba, enable;
    int i;
    type_groupe *group;

    type_coeff *coeff;
    type_neurone *current_neuron;

    group = &def_groupe[index_of_group];
    my_data = (My_data*)group->data;
    calcule_neuromodulation_fast(index_of_group, my_data->neuromodulation_of_delta_reward, &delta_reward);

    if (my_data->neuromodulation_of_enable != NULL) calcule_neuromodulation_fast(index_of_group, my_data->neuromodulation_of_enable, &enable);
    else enable = 1;

    /** Pour tous les neurones */
    if (enable > 0.5)
    {
        printf("\n");
        for (i = 0 ; i < my_data->size_of_output; i++)
        {
            current_neuron = &my_data->output_neurons[i];
            coeff = current_neuron->coeff;
            /** Pour tous les coeff d'entrees */
            while (coeff != NULL )
            {
                /** S'il y a eu des evenements sur le lien depuis la derniere fois */
              /*  if (coeff->Nbre_E > 0.0 && coeff->Nbre_S > 0.0) */
                {
                  if (coeff->Nbre_E * coeff->Nbre_S > 0) correlation = coeff->Nbre_ES / sqrt(coeff->Nbre_E * coeff->Nbre_S);
                  else correlation = 0;

                  previous_proba = coeff->proba;
                  coeff->proba = previous_proba + (PCR_EPSILON*coeff->Nbre_E + group->learning_rate * delta_reward) * (2*coeff->val - 1) * correlation - PCR_LAMBDA * coeff->Nbre_E * (2*coeff->val - 1);

                  saturate(&coeff->proba);

            /*      if (fabs(delta_reward) > PCR_XI)  S'il y a un evenement dans la recompense  */
                    {
                        temp = drand48();
                        if (temp > coeff->proba)
                        {
                            printf("Changement des poids\n");
                            coeff->val = 1 - coeff->val;
                            coeff->proba = 1 - coeff->proba;
                        }
#ifdef DEBUG
                        printf("rand:%f\n", temp);
                        printf("coeff:%d->%d, delta_reward:%f, old confiance:%f, new confiance:%f, Nb_E:%f, NB_S:%f, NB_ES:%f, correlation:%f, coeff_val:%f \n", coeff->entree, i, delta_reward, previous_proba, coeff->proba, coeff->Nbre_E, coeff->Nbre_S, coeff->Nbre_ES, correlation, coeff->val);
#endif
                        my_data->number_of_iterations = 0;
                        coeff->Nbre_E = 0.0;
                        coeff->Nbre_S = 0.0;
                        coeff->Nbre_ES = 0.0;
                    }
                }
                coeff = coeff->s;
            }
        }
        compute_Nbres(my_data);
    }
}
