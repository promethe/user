/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\file 
\brief 

Author: Arnaud Revel
Created: 18/03/2005
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Le but de cette fonction est de permettre la detection du
   du passage au dessus d'un seuil d'une des entree.
   Dans ce cas, cette entree est mise a 1 et maintenue.
   Les autres neurones sont mis a 0

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: un a un non modifiable
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>

void function_seuil_et_memoire(int gpe_sortie)
{
    int DebutGroupe = def_groupe[gpe_sortie].premier_ele;
    int i = 0;
    type_coeff *lien = NULL;
    double entree = 0.;

    /* Nombre de lignes dans le groupe */
    int nb_lignes = def_groupe[gpe_sortie].tailley;

    /* Nombre de cellules par lignes */
    int nb_cellules = def_groupe[gpe_sortie].taillex;

    double maxvalue = -1.;
    int posvalue = -1;
    int valtokeep = -1;

    static int first = 1;

    /* POUR DEBUG */
    static FILE *debug = NULL;

    if (first)
    {
        first = 0;

        debug = fopen("./WTASeuille", "w");

        for (i = DebutGroupe; i < DebutGroupe + (nb_lignes * nb_cellules);
             i++)
        {
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
        }
    }

    for (i = DebutGroupe; i < DebutGroupe + (nb_lignes * nb_cellules); i++)
    {
        /*neurone[i].s=neurone[i].s1=neurone[i].s2= 0.; */

        /* On recupere le lien avec le neurone d'entree */
        lien = neurone[i].coeff;

        if (lien == NULL)
        {
            EXIT_ON_ERROR("Lien non initialise");
        }
        /* Liens un a un */
        if (lien->s != NULL)
        {
            EXIT_ON_ERROR("Vous devez utiliser des liens un a un");
        }
        entree = neurone[lien->entree].s;

        neurone[i].s = entree;

        if (entree > maxvalue)
        {
            maxvalue = entree;
            posvalue = i;
        }

        /* On teste la nouvelle entree */
        if (entree > def_groupe[gpe_sortie].seuil)  /* L'entree est au dessus du seuil */
        {
            /* Le neurone est peut etre le gagnant */

            neurone[i].s1 = neurone[i].s2 = 1.;
            valtokeep = i;

        }

        /* Si le neurone etait le gagnant il peut peut etre le rester */
        if (valtokeep == -1)
        {
	  if (isequal(neurone[i].s2, 1.))
            {
                posvalue = i;
                valtokeep = i;
            }
        }
        /*printf("\tMax=%f\tpos=%d\tE=%f\tN=%f\n",maxvalue,posvalue,entree,neurone[i].s2); */

    }

    for (i = DebutGroupe; i < DebutGroupe + (nb_lignes * nb_cellules); i++)
    {
        if (i != valtokeep)
            neurone[i].s1 = neurone[i].s2 = 0.;
        fprintf(debug, "%f\t", neurone[i].s2);
        if (((i + 1 - DebutGroupe) % nb_lignes) == 0)
            fprintf(debug, "\n");
    }

}
