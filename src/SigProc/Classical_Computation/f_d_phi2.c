/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_d_phi2 f_d_phi2
ingroup libSigProc


description :
fonction de derivation utilisee avec le NF.s
Le FacteurAmplitude peut etre choisi sur le Lien
em meme temps que l'amplitude du lissage de la derivee, et la
methode de fusion de donnees (Moyenne ou Max)...
EX:  F23.0,5,1

calcule la derivee en prenant la difference du neurone i+1 et du
neurone i-1 pour le calcul du neurone i
cette difference est multipliee par FacteurAmplitude

Un changement de procedure permet de choisir l'amplitude de la
pente choisie: Par exemple, si elle est de 5, on va calculer la
pente de la courbe en faisant la moyenne des pentes calculee
avec les neurones directement adjacent, puis les neurones
eloignes de deux, puis de trois puis de 4 puis de 5

author = Nico

To be completed

\file
ingroup f_d_phi2
*/
#include <stdlib.h>
#include <libx.h>
void function_d_phi2(int gpe)
{
    int groupe_entree = -1;
    int i, j, k;
    int deb1, increment1, nbre11, nbre12;   /* 1: gpe */
    int deb2, increment2, nbre21, nbre22;   /* 2: groupe_entree */
    int r_plus, r_moins, r_pente_amp, r_flag;
    float r_pente, r_temp;
    float FacteurAmplitude = 25.0;  /* valeur originale= 15 */
    /*int nofind; */

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == gpe)
        {
            groupe_entree = liaison[i].depart;
            if (liaison[i].nom[0] == 'F')
            {
                sscanf(liaison[i].nom, "F%f,%d,%d",
                       &FacteurAmplitude, &r_pente_amp, &r_flag);
                /*printf("Function_d_phi2 : FacteurAmplitude = %f",FacteurAmplitude); */
            }
            break;
        }
    if (groupe_entree < 0)
    {
        printf("\nFunction_d_phi2 : Pas d entrees detectees! \n");
        return;
    }

    /* Etude du groupe d_phi2 */
    deb1 = def_groupe[gpe].premier_ele;
    nbre11 = def_groupe[gpe].nbre;
    nbre12 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
    increment1 = nbre11 / nbre12;

    /* Etude du groupe a deriver */
    deb2 = def_groupe[groupe_entree].premier_ele;
    nbre21 = def_groupe[groupe_entree].nbre;
    nbre22 =
        def_groupe[groupe_entree].taillex * def_groupe[groupe_entree].tailley;
    increment2 = nbre21 / nbre22;

    /* Tests de validite */
    if (increment1 != 1)
    {
        printf
            ("Function_d_phi2: Pourquoi y a t-il des Macro Neurones? (Raoul)\n");
        printf
            ("\t\tNormalement, cette function ne devrait pas en avoir...\n");
    }
    if (nbre12 != nbre22)
    {
        printf
            ("Function_d_phi2: Difference de taille entre la fonction et sa derivee \n");
        exit(1);
    }

    /* calcul de la derivee sur les n neurones */
    for (i = deb1 + increment1 - 1, j = deb2 + increment2 - 1;
         (i < deb1 + nbre11) && (j < deb2 + nbre21);
         i += increment1, j += increment2)
    {
        r_pente = 0.0;
        for (k = 1; k <= r_pente_amp; k++)
        {
            /* Choix des neurones */
            r_plus = j + k * increment2;
            r_moins = j - k * increment2;
            /* Modulo: Si un neurone depasse du groupe, on retourne au debut... */
            while ((r_plus >= deb2 + nbre21) || (r_moins < deb2))
            {
                if (r_plus >= deb2 + nbre21)
                    r_plus = r_plus - nbre21;
                if (r_moins < deb2)
                    r_moins = r_moins + nbre21;
            }
            /* calcul de la pente a cet endroit */
            /* Flag=1: Moyenne */
            /* Flag=2: Max */
            r_temp =
                ((float) (neurone[r_plus].s1 - neurone[r_moins].s1) /
                 ((float) 2 * k));
            if (r_flag == 1)
                r_pente = r_pente + r_temp / ((float) r_pente_amp); /* Moyenne */
            if (r_flag == 2)
            {
                if ((fabs((float) r_pente)) < (fabs((float) r_temp)))
                    r_pente = r_temp;   /* Maximum */
            }

            /*printf("f_d_phi2 : neurone[%d]=%f\n",i,neurone[i].s); */
        }
        neurone[i].s = neurone[i].s1 = neurone[i].s2 =
            FacteurAmplitude * r_pente;
    }

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Time in function_d_phi2\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}
