/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \ingroup libSigProc
 \defgroup f_shift f_shift

 \section Description
 Copie les activit��es du groupe avec la liaison 'copy', les d��cale et les redimentionnes suivant la position du max du groupe de la liasion 'shift' et les param��tres des liens s'il y en a. S'il n'y pas de shift un d��calage et un redimentionnement fixe est effectu��.

 - Si l'espace d'arriv��e est plus grand, le neurone d'entr�� sera recopi�� au centre de l'espace d'arriv��e, zero ailleurs (On pourra imaginer une option qui l'��tale sur tous les neurones d'arriv��e correpspondant ).
 - Si l'espace d'arriv��e est plus petit le max des neurones d'entr��s correspondant sera recopi�� sur le neurone d'arriv��e ( On pourra imaginer une option pour prendre la moyenne) .


 \section copy Liens copy

 Groupe qui sera recopi�� dans le groupe de sortie.

 \subsection Options
 -           -xm, -ym: minimum des valeurs represent��es par le champ. (e.g. -180). 0 par d��faut.
 -           -xM, -yM: maximum des valeurs repr��sent��es par le champ. (e.g. +90). Taille de la ligne, respectivement colonne par d��faut.
 -           -ring: indique si en cas de d��passement dans le groupe d'entr��, le r��sultat sera reboucl��. Il est tronqu�� par d��faut.

 \section Lien shift

 groupes qui indiquent les d��calages qui seront fait. Il peut y en avoir plusieurs.

 \subsection Options
 -           -xm, -ym: minimum des valeurs represent��es par le champ. (e.g. -180). -taille/2  par d��faut.
 -           -xM, -yM: maximum des valeurs repr��sent��es par le champ. (e.g. +180). + taille/2  par d��faut.
 -           -inv    : indique si le shift doit ��tre invers��.
 -           -valx,-valy : le d��calage est cod�� en valeur sur un seul neurone. 0 correspond �� un shift de xm (ym) et 1 �� un shift de xM (yM).

 \section options Autres options
 -           -oxm, -oym, -oxM -oxM: Comme xm, xM, ym, yM mais appliqu��s au groupe de sortie. 0 pour oxm, oym et taille(x|y) pour oxM et oyM comme valeurs par d��fauts.

 \remark
 Voir les exemples dans applications/unitary_tests/f_shift.

 \file
 \ingroup f_shift

 */

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <stdlib.h>
#include "basic_tools.h"

/*#define DEBUG*/

typedef struct MyData_f_neural_shift_compass {
  int Gpe_Compass;
  int Gpe_Input;
} MyData_f_neural_shift_compass;

void function_neural_shift_compass(int Gpe)
{

  int Gpe_Compass = -1;
  int Gpe_Input = -1;
  int i, l, j;
  int decalage, indice_compass;
  MyData_f_neural_shift_compass *my_data = NULL;
#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  /*printf("enter f_neural_shitf_compass\n");*/
  if (def_groupe[Gpe].data == NULL)
  {
    i = 0;
    l = find_input_link(Gpe, i);
    while (l != -1)
    {
      /*printf("lien %d: %s--\n",i,liaison[l].nom); */
      if (strcmp(liaison[l].nom, "compass") == 0)
      {
        Gpe_Compass = liaison[l].depart;
      }
      else if (strcmp(liaison[l].nom, "input") == 0)
      {
        Gpe_Input = liaison[l].depart;
      }

      i++;
      l = find_input_link(Gpe, i);
    }
    if (Gpe_Compass == -1 || Gpe_Input == -1)
    {
      EXIT_ON_ERROR("manque un groupe dans f_neural_shift_compass\n");
    }

    if (def_groupe[Gpe].nbre != def_groupe[Gpe_Compass].nbre || def_groupe[Gpe].nbre != def_groupe[Gpe_Input].nbre)
    {
      EXIT_ON_ERROR("les trois groupe relatif a f_neural_shift_compass doivent avoir le meme nombre de neurone\n");
    }

    my_data = (MyData_f_neural_shift_compass *) malloc(sizeof(MyData_f_neural_shift_compass));
    if (my_data == NULL)
    {
      EXIT_ON_ERROR("erreur malloc dans f_neural_shift_compass\n");
    }
    my_data->Gpe_Compass = Gpe_Compass;
    my_data->Gpe_Input = Gpe_Input;
    def_groupe[Gpe].data = (MyData_f_neural_shift_compass *) my_data;

  }
  else
  {
    my_data = (MyData_f_neural_shift_compass *) (def_groupe[Gpe].data);
    Gpe_Compass = my_data->Gpe_Compass;
    Gpe_Input = my_data->Gpe_Input;
  }
  indice_compass = 0;

  for (i = def_groupe[Gpe_Compass].premier_ele; i < def_groupe[Gpe_Compass].nbre + def_groupe[Gpe_Compass].premier_ele; i++)
  {
    if (isequal(neurone[i].s1, 1.0)) indice_compass = i - def_groupe[Gpe_Compass].premier_ele;
  }
  /*printf("indice_compass:%d\n",indice_compass);*/
  decalage = (int) (def_groupe[Gpe].nbre / 2) - indice_compass;
  /*printf("decalage:%d\n",decalage);*/
  for (i = 0; i < def_groupe[Gpe].nbre; i++)
  {
    j = i - decalage;
    if (j < 0) j = j + def_groupe[Gpe].nbre;
    else if (j >= def_groupe[Gpe].nbre) j = j - def_groupe[Gpe].nbre;

    neurone[i + def_groupe[Gpe].premier_ele].s = neurone[i + def_groupe[Gpe].premier_ele].s1 = neurone[i + def_groupe[Gpe].premier_ele].s2 = neurone[j + def_groupe[Gpe_Input].premier_ele].s1;

  }


#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
    1;
    MicroSecondesFunctionTimeTrace =
    1000000 + OutputFunctionTimeTrace.tv_usec -
    InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace,
      "Time in function_shift_compass\t%4ld.%06d\n",
      SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif
}

void function_shift_compass(int Gpe)
{

  int NbNeuronsGpe, NbNeuronsInputGpe;
  int DebutGpe, DebutInputGpe, DebutAngleGpe;
  int InputGpe, AngleGpe;
  int Orientation, LocalOrientation;
  int Index;

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  InputGpe = AngleGpe = -1;
  for (Index = 0; Index < nbre_liaison; Index++)
  {
    if (liaison[Index].arrivee == Gpe && strcmp(liaison[Index].nom, "input") == 0) InputGpe = liaison[Index].depart;
    if (liaison[Index].arrivee == Gpe && strcmp(liaison[Index].nom, "compass") == 0) AngleGpe = liaison[Index].depart;
  }
  if (InputGpe == -1 || AngleGpe == -1)
  {
    EXIT_ON_ERROR("Je ne trouve pas a l'entre du groupe %d le lien <compass> ou/et le lien <input>\n", Gpe);
    exit(-1);
  }

  NbNeuronsGpe = def_groupe[Gpe].nbre;
  DebutGpe = def_groupe[Gpe].premier_ele;
  NbNeuronsInputGpe = def_groupe[InputGpe].nbre;
  DebutInputGpe = def_groupe[InputGpe].premier_ele;
  DebutAngleGpe = def_groupe[AngleGpe].premier_ele;
  if (NbNeuronsGpe != NbNeuronsInputGpe)
  {
    EXIT_ON_ERROR("Le nombre de neurones pour ce groupe et le groupe input doit etre le meme\n");
    dprints("Exit  in 'function_shift_compass' gpe: %d\n", Gpe);
    exit(-1);
  }
  for (Index = DebutGpe; Index < DebutGpe + NbNeuronsGpe; Index++)
    neurone[Index].s = neurone[Index].s1 = neurone[Index].s2 = 0.0;

  Orientation = (int) (neurone[DebutAngleGpe].s * (float) NbNeuronsInputGpe) + (int) (.5 * (float) NbNeuronsInputGpe);
  if (Orientation >= NbNeuronsGpe) Orientation -= NbNeuronsGpe;

  if ((neurone[DebutAngleGpe].s > 1.0) || (neurone[DebutAngleGpe].s < 0.0))
  {
    kprints("Attention, la valeur (.s) du premiere neurone du groupe %d", Gpe);
    kprints("doit etre entre entre 0.0 et 1.0, non %f\n\n", neurone[DebutAngleGpe].s);
    EXIT_ON_ERROR("Exit in 'function_shift_compass' gpe: %d\n", Gpe);
  }

  /*printf("O=%f\n",Orientation); */
  for (Index = 0; Index < NbNeuronsInputGpe; Index++)
  {
    LocalOrientation = Index + Orientation;
    if (LocalOrientation >= NbNeuronsGpe) LocalOrientation -= NbNeuronsGpe;
    if (LocalOrientation < 0.0) LocalOrientation += NbNeuronsGpe;
    neurone[DebutGpe + LocalOrientation].s = neurone[DebutGpe + LocalOrientation].s1 = neurone[DebutGpe + LocalOrientation].s2 = neurone[DebutInputGpe + Index].s1;
    /*     printf("function_shit_compass neurone[%d].s=%f\n",DebutGpe+LocalOrientation ,neurone[DebutGpe+LocalOrientation].s);*/
  }

#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
    1;
    MicroSecondesFunctionTimeTrace =
    1000000 + OutputFunctionTimeTrace.tv_usec -
    InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace,
      "Time in function_shift_compass\t%4ld.%06d\n",
      SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif
}

#define SHIFT_GROUPS_MAX 16

enum {
  VAL_POPULATION = 0, VAL_X, VAL_Y
};

typedef struct shift {
  int ring;
  float copy_x_min, copy_x_max, copy_y_min, copy_y_max, copy_i_to_x, copy_j_to_y;
  float output_x_min, output_x_max, output_y_min, output_y_max, output_i_to_x, output_j_to_y, output_x_to_i, output_y_to_j;
  float x_offset, y_offset;
  type_groupe *copy_group;

  float shift_x_mins[SHIFT_GROUPS_MAX], shift_y_mins[SHIFT_GROUPS_MAX], shift_i_to_xs[SHIFT_GROUPS_MAX], shift_j_to_ys[SHIFT_GROUPS_MAX];
  int shift_invs[SHIFT_GROUPS_MAX];
  int shift_vals[SHIFT_GROUPS_MAX];
  type_groupe *shift_groups[SHIFT_GROUPS_MAX];
  int shift_groups_nb;
} type_shift;

void new_shift(int gpe)
{
  char* link_name;
  float shift_x_max, shift_y_max;
  int link_id, nb_links = 0, shift_id;

  type_shift * my_data;
  type_groupe *shift_group;

  my_data = ALLOCATION(type_shift);

  my_data->shift_groups_nb = 0;
  my_data->copy_group = NULL;

  /* default values */
  my_data->ring = 0;
  my_data->output_x_min = 0;
  my_data->output_x_max = def_groupe[gpe].taillex;
  my_data->output_y_min = 0;
  my_data->output_y_max = def_groupe[gpe].tailley;

  for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, nb_links))
  {
    link_name = liaison[link_id].nom;
    if (strstr(link_name, "copy") == link_name) /* start with */
    {
      if (my_data->copy_group != NULL) EXIT_ON_GROUP_ERROR(gpe, "You cannot have more than one `copy` link.");
      my_data->copy_group = &def_groupe[liaison[link_id].depart];

      if (prom_getopt_float(link_name, "-xm", &my_data->copy_x_min) != 2) my_data->copy_x_min = 0;
      if (prom_getopt_float(link_name, "-xM", &my_data->copy_x_max) != 2) my_data->copy_x_max = my_data->copy_group->taillex;
      if (prom_getopt_float(link_name, "-ym", &my_data->copy_y_min) != 2) my_data->copy_y_min = 0;
      if (prom_getopt_float(link_name, "-yM", &my_data->copy_y_max) != 2) my_data->copy_y_max = my_data->copy_group->tailley;

      my_data->copy_i_to_x = (my_data->copy_x_max - my_data->copy_x_min) / my_data->copy_group->taillex;
      my_data->copy_j_to_y = (my_data->copy_y_max - my_data->copy_y_min) / my_data->copy_group->tailley;

      if (prom_getopt(link_name, "-inv", NULL) == 1) EXIT_ON_GROUP_ERROR(gpe, "Option `inv` must be on the shift link");

    }
    else
    {
      if (strstr(link_name, "shift") == link_name)
      {
        shift_group = &def_groupe[liaison[link_id].depart];
        shift_id = my_data->shift_groups_nb;

        my_data->shift_groups[shift_id] = shift_group;


        if (prom_getopt(link_name, "-inv", NULL) == 1) my_data->shift_invs[shift_id] = 1;
        else my_data->shift_invs[shift_id] = 0;

        if (prom_getopt(link_name, "-valx", NULL) == 1)
        {
          if (prom_getopt_float(link_name, "-xm", &my_data->shift_x_mins[shift_id]) != 2) my_data->shift_x_mins[shift_id] = 0;
          if (prom_getopt_float(link_name, "-xM", &shift_x_max) != 2) shift_x_max = 1;
          my_data->shift_vals[shift_id] = VAL_X;
          my_data->shift_i_to_xs[shift_id] = shift_x_max - my_data->shift_x_mins[shift_id];
        }
        else if (prom_getopt(link_name, "-valy", NULL) == 1)
        {
          if (prom_getopt_float(link_name, "-ym", &my_data->shift_y_mins[shift_id]) != 2) my_data->shift_y_mins[shift_id] = 0;
          if (prom_getopt_float(link_name, "-yM", &shift_y_max) != 2) shift_y_max = 1;
          my_data->shift_vals[shift_id] = VAL_Y;
          my_data->shift_j_to_ys[shift_id] = shift_y_max - my_data->shift_y_mins[shift_id];
        }
        else
        {
           if (prom_getopt_float(link_name, "-xm", &my_data->shift_x_mins[shift_id]) != 2) my_data->shift_x_mins[shift_id] = - (shift_group->taillex-1) / 2.;
           if (prom_getopt_float(link_name, "-xM", &shift_x_max) != 2) shift_x_max =  ((shift_group->taillex-1) / 2.);
           if (prom_getopt_float(link_name, "-ym", &my_data->shift_y_mins[shift_id]) != 2) my_data->shift_y_mins[shift_id] = - (shift_group->tailley-1) / 2.;
           if (prom_getopt_float(link_name, "-yM", &shift_y_max) != 2) shift_y_max = ((shift_group->tailley-1) / 2.);


          my_data->shift_vals[shift_id] = VAL_POPULATION;
          my_data->shift_i_to_xs[shift_id] = (shift_x_max - my_data->shift_x_mins[shift_id]) / shift_group->taillex;
          my_data->shift_j_to_ys[shift_id] = (shift_y_max - my_data->shift_y_mins[shift_id]) / shift_group->tailley;
        }

        if (prom_getopt(link_name, "-ring", NULL) == 1) EXIT_ON_GROUP_ERROR(gpe, "Option `ring` must not be on the shift link but on `copy` or other.");

        if (my_data->shift_groups_nb < SHIFT_GROUPS_MAX) my_data->shift_groups_nb++;
        else EXIT_ON_GROUP_ERROR(gpe, "You cannot have more than %d shift groups.", SHIFT_GROUPS_MAX);
      }
    }

    if (prom_getopt(link_name, "-ring", NULL) == 1) my_data->ring = 1;

    prom_getopt_float(link_name, "-oxm", &my_data->output_x_min);
    prom_getopt_float(link_name, "-oxM", &my_data->output_x_max);
    prom_getopt_float(link_name, "-oym", &my_data->output_y_min);
    prom_getopt_float(link_name, "-oyM", &my_data->output_y_max);

    if (prom_getopt(link_name, "-m", NULL) == 1) EXIT_ON_GROUP_ERROR(gpe, "`-m` on link `%s` is not a valid option. You probably mean `-xm`, `-oxm`,  `-ym` or `-oym`.", link_name);
    if (prom_getopt(link_name, "-M", NULL) == 1) EXIT_ON_GROUP_ERROR(gpe, "`-M` on link `%s` is not a valid option. You probably mean `-xM`, `-oxM`,  `-yM` or `-oyM`.", link_name);

    nb_links++;
  }

  my_data->output_i_to_x = (my_data->output_x_max - my_data->output_x_min) / def_groupe[gpe].taillex;
  my_data->output_j_to_y = (my_data->output_y_max - my_data->output_y_min) / def_groupe[gpe].tailley;
  my_data->output_x_to_i = def_groupe[gpe].taillex / (my_data->output_x_max - my_data->output_x_min);
  my_data->output_y_to_j = def_groupe[gpe].tailley / (my_data->output_y_max - my_data->output_y_min);

  my_data->x_offset = my_data->copy_x_min - my_data->output_x_min; /* Pour centrer les valeurs */
  my_data->y_offset = my_data->copy_y_min - my_data->output_y_min;

  if (my_data->copy_group == NULL) EXIT_ON_GROUP_ERROR(gpe, "You need a `copy` link.");

  def_groupe[gpe].data = my_data;
}

void function_shift(int numero)
{
  int copy_j_id, output_j_id, output_j, output_i, copy_i_size, copy_j_size;
  int max_id, id;
  int i, j, shift_id;
  int output_id, copy_id, deb, copy_deb, shift_deb;
  int i_size, j_size, shift_i_size;
  float shift_x, shift_y, local_shift_x=0, local_shift_y=0;
  int shift_max_i = 0, shift_max_j = 0;
  float tmp, max = 1.0;

  type_shift *my_data = def_groupe[numero].data;

  shift_x = 0;
  shift_y = 0;

  /*----------------------------*/
  /*Taille des groupes et debuts */
  i_size = def_groupe[numero].taillex;
  j_size = def_groupe[numero].tailley;
  deb = def_groupe[numero].premier_ele;

  copy_i_size = my_data->copy_group->taillex;
  copy_j_size = my_data->copy_group->tailley;
  copy_deb = my_data->copy_group->premier_ele;

  for (shift_id = 0; shift_id < my_data->shift_groups_nb; shift_id++)
  {
    shift_deb = my_data->shift_groups[shift_id]->premier_ele;

    if (my_data->shift_vals[shift_id] == VAL_POPULATION)
    {

      shift_i_size = my_data->shift_groups[shift_id]->taillex;

      max = neurone[shift_deb].s1;
      max_id = 0;

      for (id = 1; id < my_data->shift_groups[shift_id]->nbre; id++)
      {
        tmp = neurone[shift_deb + id].s1;
        if (tmp > max)
        {
          max = tmp;
          max_id = id;
        }
      }
      /* Evite d'avoir 2 boucles imbriquees */
      shift_max_i = (max_id % shift_i_size);
      shift_max_j = (max_id / shift_i_size);

      local_shift_x = (shift_max_i * my_data->shift_i_to_xs[shift_id]) + my_data->shift_x_mins[shift_id];
      local_shift_y = (shift_max_j * my_data->shift_j_to_ys[shift_id]) + my_data->shift_y_mins[shift_id];

    }
    else if (my_data->shift_vals[shift_id] == VAL_X)
    {
      local_shift_x = my_data->shift_x_mins[shift_id] + neurone[shift_deb].s1 * my_data->shift_i_to_xs[shift_id];
      local_shift_y = 0;
    }
    else if (my_data->shift_vals[shift_id] == VAL_Y)
    {
      local_shift_x = 0;
      local_shift_y = my_data->shift_y_mins[shift_id] + neurone[shift_deb].s1 * my_data->shift_j_to_ys[shift_id];
    }

    if (my_data->shift_invs[shift_id])
    {
      shift_x -= local_shift_x;
      shift_y -= local_shift_y;
    }
    else
    {
      shift_x += local_shift_x;
      shift_y += local_shift_y;
    }
  }

  /*--------------------------------------------------------------*/
  /*copie et decalage des activites du groupe sur la liaison "copy" */

  for (id = 0; id < i_size * j_size; id++)
  {
     neurone[deb + id].s = neurone[deb + id].s1 = neurone[deb + id].s2 = 0.;
  }


  if (max > 0. || my_data->shift_groups_nb == 0) /*S' il y a de l'activite ou pas de lien shift*/
  {
    for (j = 0; j < copy_j_size; j++)
    {
      output_j = ((j + 0.5) * my_data->copy_j_to_y + my_data->y_offset - shift_y) * my_data->output_y_to_j;

      if (my_data->ring) output_j = MOD(output_j, j_size);
      else if (output_j < 0 || output_j >= j_size) continue;

      copy_j_id = copy_deb + j * copy_i_size;
      output_j_id = deb + output_j * i_size;

      for (i = 0; i < copy_i_size; i++)
      {
        output_i = ((i + 0.5) * my_data->copy_i_to_x + my_data->x_offset - shift_x) * my_data->output_x_to_i;

        if (my_data->ring) output_i = MOD(output_i, i_size);
        else if (output_i < 0 || output_i >= i_size) continue;

        copy_id = copy_j_id + i;
   
        output_id = output_i + output_j_id;

        if (neurone[copy_id].s > neurone[output_id].s) neurone[output_id].s = neurone[copy_id].s;
        if (neurone[copy_id].s1 > neurone[output_id].s1) neurone[output_id].s1 = neurone[copy_id].s1;
        if (neurone[copy_id].s2 > neurone[output_id].s2) neurone[output_id].s2 = neurone[copy_id].s2;
	/*	if (output_id < def_groupe[numero].premier_ele) EXIT_ON_GROUP_ERROR(numero, "out of group %d :%d", def_groupe[numero].premier_ele,  output_id );
	 *     if (output_id > def_groupe[numero].premier_ele+def_groupe[numero].nbre) EXIT_ON_GROUP_ERROR(numero, "out of group deb: %d output_id: %d", def_groupe[numero].premier_ele, output_id);*/
      }
    }
  }
}

typedef struct MyData_f_neural_shift_compass_inv {
  int Gpe_Compass;
  int Gpe_Input;
} MyData_f_neural_shift_compass_inv;

void function_neural_shift_compass_inv(int Gpe)
{

  int Gpe_Compass = -1;
  int Gpe_Input = -1;
  int i, l, j;
  int decalage, indice_compass;
  MyData_f_neural_shift_compass_inv *my_data = NULL;
#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  /*printf("enter f_neural_shitf_compass\n");*/
  if (def_groupe[Gpe].data == NULL)
  {
    i = 0;
    l = find_input_link(Gpe, i);
    while (l != -1)
    {
      /*printf("lien %d: %s--\n",i,liaison[l].nom); */
      if (strcmp(liaison[l].nom, "compass") == 0)
      {
        Gpe_Compass = liaison[l].depart;
      }
      else if (strcmp(liaison[l].nom, "input") == 0)
      {
        Gpe_Input = liaison[l].depart;
      }

      i++;
      l = find_input_link(Gpe, i);
    }
    if (Gpe_Compass == -1 || Gpe_Input == -1)
    {
      EXIT_ON_ERROR("manque un groupe dans f_neural_shift_compass_inv\n");
      exit(0);
    }

    if (def_groupe[Gpe].nbre != def_groupe[Gpe_Compass].nbre || def_groupe[Gpe].nbre != def_groupe[Gpe_Input].nbre)
    {
      EXIT_ON_ERROR("les trois groupe relatif a f_neural_shift_compass_inv doivent avoior le meme nombre de neurone\n");
      exit(0);
    }

    my_data = (MyData_f_neural_shift_compass_inv *) malloc(sizeof(MyData_f_neural_shift_compass_inv));
    if (my_data == NULL)
    {
      EXIT_ON_ERROR("erreur malloc dans f_neural_shift_compass_inv\n");
      exit(0);
    }
    my_data->Gpe_Compass = Gpe_Compass;
    my_data->Gpe_Input = Gpe_Input;
    def_groupe[Gpe].data = (MyData_f_neural_shift_compass_inv *) my_data;

  }
  else
  {
    my_data = (MyData_f_neural_shift_compass_inv *) (def_groupe[Gpe].data);
    Gpe_Compass = my_data->Gpe_Compass;
    Gpe_Input = my_data->Gpe_Input;
  }
  indice_compass = 0;

  for (i = def_groupe[Gpe_Compass].premier_ele; i < def_groupe[Gpe_Compass].nbre + def_groupe[Gpe_Compass].premier_ele; i++)
  {
    if (isequal(neurone[i].s1, 1.0)) indice_compass = i - def_groupe[Gpe_Compass].premier_ele;
  }
  /*printf("indice_compass:%d\n",indice_compass);*/
  decalage = 0 - (int) (def_groupe[Gpe].nbre / 2) + indice_compass;
  /*printf("decalage:%d\n",decalage);*/
  for (i = 0; i < def_groupe[Gpe].nbre; i++)
  {
    j = i - decalage;
    if (j < 0) j = j + def_groupe[Gpe].nbre;
    else if (j >= def_groupe[Gpe].nbre) j = j - def_groupe[Gpe].nbre;

    neurone[i + def_groupe[Gpe].premier_ele].s = neurone[i + def_groupe[Gpe].premier_ele].s1 = neurone[i + def_groupe[Gpe].premier_ele].s2 = neurone[j + def_groupe[Gpe_Input].premier_ele].s1;

  }

#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
    1;
    MicroSecondesFunctionTimeTrace =
    1000000 + OutputFunctionTimeTrace.tv_usec -
    InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace,
      "Time in function_shift_compass\t%4ld.%06d\n",
      SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif
}
