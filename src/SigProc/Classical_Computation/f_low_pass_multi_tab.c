/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** 
 \ingroup libSigProc
 \defgroup f_low_pass_multi_tab f_low_pass_multi_tab

 \author Syed Khursheed HASNAIN
 \date 15/11/2012

 \details
 It is a simple low pass filter (working in Z-domain) e.g.  y(n) = 0.235*x[n] -0.125*x[n-1]+0.25*x[n-2]+0.425*x[n-3] .....
 Tabs (coefficient) of low pass equation (as above) can be calculated by very simple commands of MATLAB. Example is given below;
 You just have to provide following parameters
 Sampling Frequency (fsamp) : of your choice (in the example it is 10 Hz)
 Cut-off frequency for the pass band (fcuts) : of your choice   (in the example it is 0.06172 Hz)
 Cut-off frequency for the stop band (fcuts) : for your choice   (in the example it is 0.555 Hz)
 clc;
 clear all;
 fsamp = 10;
 fcuts = [0.06172 0.555];
 mags = [1 0];
 devs = [0.05 0.01];
 [n,Wn,beta,ftype] = kaiserord(fcuts,mags,devs,fsamp)
 hh = fir1(n,Wn,ftype,kaiser(n+1,beta),'noscale')
 freqz(hh)
 After providing these parameters, run the small script in the matlab, it will give you the tabs. Save these tab in the text file (as in the example), and use it as mask.

 \section Options
 \subsection Sorties
 -		-S0		: (s0, s1 ou S2)
 -		Mask 	: (txt file that contains tab of the filter)
 -		Data	: (input data to be filtered)



 ************************************************************/

#include <stdlib.h>
#include <string.h>

#include <libx.h>
#include <net_message_debug_dist.h> 

/* #define FILTER_SIZE 46 */

/** Find the low frequency component by using simple z-transform */
typedef struct my_data {

  float **previous;
  float *mask;
  int beginning; /* to allow circular functioning of the regression vector */
  type_groupe *Mask_group;
  type_groupe *Data_group;
} type_my_data;

/* Initialization */

void new_low_pass_multi_tab(int index_of_group)
{
  int number_of_links, index_of_link, i, j;
  type_liaison *link = NULL;
  type_groupe *group;
  type_my_data *my_data;
  int FILTER_SIZE = 0;

  /* Memory allocation for type_my_data    */

  my_data = ALLOCATION(type_my_data);

  group = &def_groupe[index_of_group];
  group->data = my_data;

  for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];
    if (strcmp(link->nom, "Mask") == 0)
    {
      my_data->Mask_group = &def_groupe[link->depart];
      FILTER_SIZE = my_data->Mask_group->nbre;

      printf("FILTER_SIZE FILTER_SIZE = %d\n", FILTER_SIZE);
    }
    else if (strcmp(link->nom, "Data") == 0) my_data->Data_group = &def_groupe[link->depart];
  }

  if (link == NULL)
  {
    EXIT_ON_ERROR("ERROR %s: the function should have at least one input group \n", __FUNCTION__);

  }

  /* memory allocation for previous values and mask values */
  my_data->previous = MANY_ALLOCATIONS(FILTER_SIZE, float *);

  for (i = 0; i < FILTER_SIZE; i++)
  {
    my_data->previous[i] = MANY_ALLOCATIONS(group->nbre, float);
  }

  my_data->mask = MANY_ALLOCATIONS(FILTER_SIZE, float);

  /* Set the initial value to zero at the start of program */

  for (i = 0; i < group->nbre; i++)
  {
    for (j = 0; j < FILTER_SIZE; j++)
    {
      my_data->previous[j][i] = 0.;
    }
  }
  my_data->beginning = 0;

  /*printf("entrer un caractere \n");
   scanf("%c",&c);
   printf("val=%c \n",c); */
}

void function_low_pass_multi_tab(int index_of_group)
{
  type_my_data *my_data;
  type_groupe *group;
  int i, j, deb;
  int FILTER_SIZE;
  int p, local_beg, future_local_beg;
  float sum;

  /*  __asm__ __volatile__ ("int3");*/

  group = &def_groupe[index_of_group];
  my_data = group->data;
  deb = group->premier_ele;

  FILTER_SIZE = my_data->Mask_group->nbre;

  for (i = 0; i < FILTER_SIZE; i++)
  {
    my_data->mask[i] = neurone[my_data->Mask_group->premier_ele + i].s1;
  }

  local_beg = my_data->beginning;
  future_local_beg = (local_beg + 1) % FILTER_SIZE;

  /* Calculate the current value */

  for (i = 0; i < group->nbre; i++)
  {
    sum = 0;
    for (j = 0; j < FILTER_SIZE; j++)
    {
      p = (local_beg + FILTER_SIZE - j) % FILTER_SIZE;
      /* printf("*** j=%d p=%d local_beg = %d my_data= %f \n",j,p,local_beg,my_data->previous[p][i]);*/
      sum = sum + my_data->mask[j] * my_data->previous[p][i];
    }
    neurone[deb + i].s1 = -0.0020 * neurone[my_data->Data_group->premier_ele + i].s1 + sum;
    neurone[deb + i].s = neurone[deb + i].s2 = neurone[deb + i].s1;

    /* Save the previous values to the corresponding variables     */

    /*	for(j=FILTER_SIZE;j>0;j--)
     {
     my_data->previous[j][i] = my_data->previous[j-1][i];
     }*/

    my_data->previous[future_local_beg][i] = neurone[my_data->Data_group->premier_ele + i].s1;

    /* printf("data= %f \n",neurone[my_data->Data_group->premier_ele + i].s1); */
  }
  my_data->beginning = future_local_beg;
}

void destroy_low_pass_multi_tab(int index_of_group)
{
  type_my_data *my_data;
  type_groupe *group;
  int i, FILTER_SIZE;

  group = &def_groupe[index_of_group];
  my_data = group->data;
  FILTER_SIZE = my_data->Mask_group->nbre;

  for (i = 0; i < FILTER_SIZE; i++)
  {
    free(my_data->previous[i]);
    free(my_data->mask);
  }
}

/*
 void function_low_pass_multi_tab_old (int index_of_group)
 {
 type_my_data *my_data;
 type_groupe *group;
 int i,j, deb;
 int FILTER_SIZE;
 float sum;

 group = &def_groupe[index_of_group];
 my_data = group->data;
 deb = group->premier_ele;

 FILTER_SIZE = my_data->Mask_group->nbre;

 for(i=0;i<FILTER_SIZE;i++)
 {
 my_data->mask[i]=neurone[my_data->Mask_group->premier_ele+i].s1;
 }


*/ /* Calculate the current value */

/*   
 for (i=0; i<group->nbre; i++)
 {
 sum = 0;
 for (j=0;j<FILTER_SIZE;j++)
 {
 sum+=my_data->mask[j]*my_data->previous[j][i];
 }
 neurone[deb+i].s1 = -0.002*neurone[my_data->Data_group->premier_ele + i].s1+sum;

*/
/* Save the previous values to the corresponding variables */

/*	for(j=FILTER_SIZE-1;j>0;j--)
 {
 my_data->previous[j][i] = my_data->previous[j-1][i];
 }

 my_data->previous[0][i] = neurone[my_data->Data_group->premier_ele + i].s1;
 *//*printf("\n"); */
/*	}
 }
 */

