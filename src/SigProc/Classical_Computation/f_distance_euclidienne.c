/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
defgroup f_distance_euclidienne f_distance_euclidienne
ingroup libSigProc

2 modes :
-X1
-X2
-Y1
-Y2
4 neurons to compute the distance

ou

-X
-Y
As many neurons (=dimension) in each groups to compute the distance


To be completed

\file
ingroup f_distance_euclidienne
 **/

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <stdlib.h>

#define DEBUG

typedef struct My_Data_f_distance
{
   int GpeX1;
   int GpeY1;
   int GpeX2;
   int GpeY2;
   int vect;
   int GpeX;
   int GpeY;
} My_Data_f_distance;


void function_distance_euclidienne(int gpe)
{
   My_Data_f_distance *my_data;
   int i = 0, l, GpeX1 = -1, GpeY1 = -1, GpeX2 = -1, GpeY2 = -1, GpeX = -1, GpeY = -1;
   float distance = -1;
   int x1 = -1;
   int x2 = -1;
   int y1 = -1;
   int y2 = -1;
   int vect = 1;

#ifdef DEBUG
   dprints("entree dans function_distance_euclidienne.\n");
#endif

   if (def_groupe[gpe].data == NULL)
   {
      l = find_input_link(gpe, i);
      while (l != -1)
      {
         if (strcmp(liaison[l].nom, "X1") == 0)
         {
            GpeX1 = liaison[l].depart;
#ifdef DEBUG
            dprints("je rentre dans X1\n");
#endif
         }
         if (strcmp(liaison[l].nom, "Y1") == 0)
         {
            GpeY1 = liaison[l].depart;
#ifdef DEBUG
            dprints("je rentre dans Y1\n");
#endif
         }
         if (strcmp(liaison[l].nom, "X2") == 0)
         {
            GpeX2 = liaison[l].depart;
#ifdef DEBUG
            dprints("je rentre dans X2\n");
#endif
         }
         if (strcmp(liaison[l].nom, "Y2") == 0)
         {
            GpeY2 = liaison[l].depart;
#ifdef DEBUG
            dprints("je rentre dans Y2\n");
#endif
         }



         if (strcmp(liaison[l].nom, "X") == 0)
         {
            GpeX = liaison[l].depart;
            vect = 0;
#ifdef DEBUG
            dprints("je rentre dans X\n");
#endif
         }
         if (strcmp(liaison[l].nom, "Y") == 0)
         {
            GpeY = liaison[l].depart;
            vect = 0;
#ifdef DEBUG
            dprints("je rentre dans Y\n");
#endif
         }
         l = find_input_link(gpe, i);
         i++;
      }
      if( vect )
      {

         if( GpeX1 == -1 || GpeX2 == -1 || GpeY1 == -1 || GpeY2 == -1 )
         {
            EXIT_ON_ERROR("GROUP %s (num coeos %s) : There must be links X1 X2 Y1 Y2   (OR   X and Y)  \n", __FUNCTION__, def_groupe[gpe].no_name);
         }
      }
      else
      {

         if( GpeX == -1 || GpeY == -1 )
         {
            EXIT_ON_ERROR("GROUP %s (num coeos %s) : There must be links  X and Y  (OR    X1 X2 Y1 Y2)  \n", __FUNCTION__, def_groupe[gpe].no_name);
         }
         if( def_groupe[GpeX].nbre != def_groupe[GpeY].nbre )
            EXIT_ON_ERROR("GROUP %s (num coeos %s) : There must be the same number of neurons (=dimensions) in gpe X and Y\n", __FUNCTION__, def_groupe[gpe].no_name);
      }

      my_data = (My_Data_f_distance *) malloc(sizeof(My_Data_f_distance));

      if (my_data == NULL)
      {
         EXIT_ON_ERROR("error malloc %s\n", __FUNCTION__);

      }

      my_data->GpeX1 = GpeX1;
      my_data->GpeY1 = GpeY1;
      my_data->GpeX2 = GpeX2;
      my_data->GpeY2 = GpeY2;

      my_data->GpeX = GpeX;
      my_data->GpeY = GpeY;

      my_data->vect = vect;

      def_groupe[gpe].data = (My_Data_f_distance *) my_data;
   }
   else
   {
      my_data = (My_Data_f_distance *)def_groupe[gpe].data;
#ifdef DEBUG
      dprints("GpeX1=%s\n",def_groupe[my_data->GpeX1].no_name);
      dprints("GpeY1=%s\n",def_groupe[my_data->GpeY1].no_name);
      dprints("GpeX2=%s\n",def_groupe[my_data->GpeX2].no_name);
      dprints("GpeY2=%s\n",def_groupe[my_data->GpeY2].no_name);
#endif

   }

#ifdef DEBUG
   dprints("GpeX1=%s\n",def_groupe[my_data->GpeX1].no_name);
   dprints("GpeY1=%s\n",def_groupe[my_data->GpeY1].no_name);
   dprints("GpeX2=%s\n",def_groupe[my_data->GpeX2].no_name);
   dprints("GpeY2=%s\n",def_groupe[my_data->GpeY2].no_name);
#endif


   if ( my_data->vect )
   {
      for(i = def_groupe[my_data->GpeX1].premier_ele ; i < def_groupe[my_data->GpeX1].premier_ele + def_groupe[my_data->GpeX1].nbre ; i++)
      {

         if(neurone[i].s1>0.5)
         {
            x1 = i - def_groupe[my_data->GpeX1].premier_ele;
            break;
         }
      }

      for(i = def_groupe[my_data->GpeX2].premier_ele ; i < def_groupe[my_data->GpeX2].premier_ele + def_groupe[my_data->GpeX2].nbre ; i++)
      {

         if(neurone[i].s1>0.5)
         {
            x2 = i - def_groupe[my_data->GpeX2].premier_ele;
            break;
         }
      }

      for(i = def_groupe[my_data->GpeY1].premier_ele ; i < def_groupe[my_data->GpeY1].premier_ele + def_groupe[my_data->GpeY1].nbre ; i++)
      {

         if(neurone[i].s1>0.5)
         {
            y1 = i - def_groupe[my_data->GpeY1].premier_ele;
            break;
         }
      }

      for(i = def_groupe[my_data->GpeY2].premier_ele ; i < def_groupe[my_data->GpeY2].premier_ele + def_groupe[my_data->GpeY2].nbre ; i++)
      {

         if(neurone[i].s1>0.5)
         {
            y2 = i - def_groupe[my_data->GpeY2].premier_ele;
            break;
         }
      }

      /*calcul de la distance euclidienne*/
      /*distance = sqrt((neurone[def_groupe[my_data->GpeX1].premier_ele].s - neurone[def_groupe[my_data->GpeY1].premier_ele].s)
       *      *(neurone[def_groupe[my_data->GpeX1].premier_ele].s - neurone[def_groupe[my_data->GpeY1].premier_ele].s)
       *      +(neurone[def_groupe[my_data->GpeX1].premier_ele + 1].s - neurone[def_groupe[my_data->GpeY1].premier_ele + 1].s)
       *      *(neurone[def_groupe[my_data->GpeX1].premier_ele + 1].s - neurone[def_groupe[my_data->GpeY1].premier_ele + 1].s)
       *      );
       */
      distance = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

#ifdef DEBUG
      dprints("x1=%d\n",x1);
      dprints("x2=%d\n",x2);
      dprints("y1=%d\n",y1);
      dprints("y2=%d\n",y2);
      dprints("distance=%f\n",distance);
#endif
      neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
                  neurone[def_groupe[gpe].premier_ele].s2 = distance/200;
   }
   else
   {

      distance = 0.;
      for( i = 0 ; i < def_groupe[my_data->GpeX].nbre ; i++)
      {
         distance += (  (neurone[def_groupe[my_data->GpeX].premier_ele + i].s1 - neurone[def_groupe[my_data->GpeY].premier_ele + i].s1)
               * (neurone[def_groupe[my_data->GpeX].premier_ele + i].s1 - neurone[def_groupe[my_data->GpeY].premier_ele + i].s1) );

#ifdef DEBUG

         dprints("X%d = %f\n", i, neurone[def_groupe[my_data->GpeX].premier_ele + i] );
         dprints("Y%d = %f\n", i, neurone[def_groupe[my_data->GpeY].premier_ele + i] );
#endif
      }

      distance = sqrt(distance);

#ifdef DEBUG
      dprints("distance euclidienne = %f\n",distance);
#endif

      neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
                  neurone[def_groupe[gpe].premier_ele].s2 = distance;
   }



#ifdef DEBUG
   dprints("distance=%f\n",distance);
   dprints("neurone.s=%f\n",neurone[def_groupe[gpe].premier_ele].s);
   dprints("sortie dans function_distance_euclidienne.\n");
#endif
}
