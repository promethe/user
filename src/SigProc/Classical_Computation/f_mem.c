/** ***********************************************************
 \file  f_mem.c
 \brief

 Author: Nils Beaussé
 Created: 19/06/2014

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Modèle ultra simple de cellule à mémoire, peut etre utilisé comme simple mémoire ou comme intégrateur à la place d'un assemblage de f_multiply etc... ou dans des applications plus complexe
 3 modes de fonctionnement :
 - intégrateur (surement plus économe en ressource qu'un hebb qui reboucle, les parametres d'intégrations peuvent etre definis dynamiquement)
 - memoire (garde la valeur en cours,  cas particulier de l'integrateur)
 - reset (laisse tout passer, cas particulier également)
 
 Le modèle de fonctionnement est inspiré de celui de François Rivest (these 2010, canada).
 Input : équivaut à un multiply avec un gain en entrée
 Forget : il est plus cohérent de l'appeler "integre" : gain sur le rebouclage.
 Output : gain sur la sortie.

 Quelques combinaisons :
 Pour faire un simple gain -> I:1 F:0 O:Gain ou I:gain F:0 O:1
 Pour faire un integrateur -> I:gain1 F:gain2 O:1
 Pour une mémoire -> I:0 F:1 O:1

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_mem {
  type_groupe* g_output;
  type_groupe* g_input;
  type_groupe* g_forget;
  int increment_gpe;
} data_mem;

void new_mem(int gpe)
{
  char string[255];
  int index, link;
  type_groupe *g_output = NULL, *g_input = NULL, *g_forget = NULL;
  data_mem *my_data = NULL;

  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);

    while (link != -1)
    {
      if (prom_getopt(liaison[link].nom, "g_output", string) >= 1)
      {
        g_output = &(def_groupe[liaison[link].depart]);
      }
      if (prom_getopt(liaison[link].nom, "g_input", string) >= 1)
      {
        g_input = &(def_groupe[liaison[link].depart]);
      }
      if (prom_getopt(liaison[link].nom, "g_forget", string) >= 1)
      {
        g_forget = &(def_groupe[liaison[link].depart]);
      }
      index++;
      link = find_input_link(gpe, index);
    }

    if (g_output == NULL || g_input == NULL || g_forget == NULL) EXIT_ON_ERROR("En entrée de mem : pas assez de liens \n");
    my_data = ALLOCATION(data_mem);
    my_data->g_input = g_input;
    my_data->g_output = g_output;
    my_data->g_forget = g_forget;
    my_data->increment_gpe = def_groupe[gpe].nbre / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);

    def_groupe[gpe].data = (void*) my_data;
  }
  else
  {
    PRINT_WARNING("L'init du gpe :%d mem ne s'est pas faite\n", gpe);
  }

}

void function_mem(int gpe)
{
  int i ;
  float temp;
  data_mem *my_data = (data_mem *) def_groupe[gpe].data;
  int deb = def_groupe[gpe].premier_ele;
  int nbre = def_groupe[gpe].nbre;
  int increment_gpe = my_data->increment_gpe;
  float g_input = neurone[my_data->g_input->premier_ele].s1;
  float g_output = neurone[my_data->g_output->premier_ele].s1;
  float g_forget = neurone[my_data->g_forget->premier_ele].s1;
  type_coeff *coeff;

  for (i = deb + increment_gpe - 1; i < deb + nbre; i = i + increment_gpe)
  {
    temp = 0.;

    coeff = neurone[i].coeff;

    while (coeff != NULL)
    {
      temp = temp + coeff->val * neurone[coeff->entree].s1;
      coeff = coeff->s; /* Lien suivant */
    }

    temp = temp * g_input; // application du gain d'entrée
    temp += neurone[i].s * g_forget; // neurone.s est considere comme le milieu qui reboucle dans la cellule à memoire
    neurone[i].s = temp;

    neurone[i].s1 = neurone[i].s2 = temp * g_output; // application du gain de sortie
    /* printf("function_sum_no_seuil neurone[%d].s=%f \n",i,neurone[i].s); */
  }

}

void destroy_mem(int gpe)
{
  data_mem *my_data = (data_mem *) def_groupe[gpe].data;
  if (my_data != NULL) free(my_data);
  def_groupe[gpe].data = NULL;
}
