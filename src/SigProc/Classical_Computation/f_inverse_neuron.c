/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_inverse_neurons f_inverse_neurons
\ingroup libSigProc

\brief output =  1/(input)

description :

Calcule le potentiel d'entree de maniere classique ( \f$ sum w_i x_i \f$ ) avant 
de prendre l'inverse.
 Le potentiel de sortie du neurone est donc \f$ frac{1}{ sum w_i x_i} \f$ .

author = A. de Rengerve
date = 12/01/2010

To be completed

\file
\ingroup f_inverse_neurons

author = "A. de Rengerve"
date = "12/01/2010"
description = "specific file creation"


*/

#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

#include <net_message_debug_dist.h>

void function_inverse_neuron(int gpe)
{  
    int i, deb, nbre, nbre2;
    int increment;
    float temp;

    type_coeff *coeff;
    dprints("entree dans inverse_neuron (%d)\n",gpe);

    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
    nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
    increment = nbre / nbre2;

    for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
    {
        temp = 0.;

        coeff = neurone[i].coeff;
        while (coeff != NULL)
        {
            temp = temp + coeff->val * neurone[coeff->entree].s1;
            coeff = coeff->s;   /* Lien suivant */
        }
			if((temp > -0.0000001) && (temp < 0.0000001))
			{
				if(temp > 0.0000000)
						temp = 0.0000001;
				else
						temp = -0.0000001;
				PRINT_WARNING("The value is too small (=0.0); value set at %.7f \n ATTENTION : La valeur sortant de cette fonction est totalement errone dans ce cas : \nPensez a utiliser f_diff_time a la place pour le calcul de vitesse ou f_a_sur_b pour a/b", temp);
			} 
        neurone[i].s = temp;
        neurone[i].s1 = neurone[i].s2 = 1.0 / temp;

	/*
	if(neurone[i].s2 >1.) 
	  neurone[i].s1 = 1.; 
	else if(neurone[i].s2 <0.) 
	  neurone[i].s1 = 0.;
	else 
	  neurone[i].s1 = neurone[i].s2; 
	*/

        /* printf("function_inverse_neuron neurone[%d].s=%f \n",i,neurone[i].s); */
    }

   
   dprints("sortie de inverse_neuron (%d)\n",gpe);   
}

