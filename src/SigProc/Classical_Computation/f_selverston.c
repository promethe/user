/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/***********************************************************
* @author Alex Mesnil
* @date 19/04/2011
************************************************************/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <math.h>

/* #define DEBUG		1 */


typedef struct selverston
{
	float v, q, ss ;
}type_selverston;

/*typedef struct my_data
	{
		float ss, Af;
	}type_my_data;*/


float fS(float q, float v, float ss, float Iinj, float tm, float lambda)
{
      (void) q;(void) ss; (void) tm;
	 return lambda*Iinj*v;

}

float fv(float q, float v, float Iinj, float tm, float ts, float ss, float sf, float Af)
{
  (void) ts; (void) ss;
	 return 1/tm*(-v-q+Af*tanh(sf/Af*v)+Iinj);

}

float fq(float q, float v, float Iinj, float tm, float ts, float ss, float sf, float Af)
{
  (void) Iinj; (void) tm; (void) sf; (void) Af;
	return 1/ts*(-q+ss*v);
}

void function_selverstone(int numero)
{
    int first, nr;
    int i, neuron_index;
    type_coeff *lien = NULL;
    float h, potentiel = 0;
    type_selverston *selverston;
    
    float lambda=0.5;
    float tm=0.2;
	float ts=2.8;
	float sf=2;
	float ss;
	float v,q;
	float k1v,k1q,k2v,k2q,k3v,k3q,k4v,k4q,k1S,k2S,k3S,k4S;
	float Af=2;
	float potentiel2;
	h = def_groupe[numero].simulation_speed * eps;


	/*type_my_data my_data;




    l = 0;
    m = find_input_link(numero, l);
    while (m != -1)
    {
   	 if (strcmp(liaison[m].nom, "ss") == 0)
   	 {
   	    my_data.ss = liaison[m].depart;
   	 }
   	if (strcmp(liaison[m].nom, "Af") == 0)
   	       	 {
   	       	    my_data.Af = liaison[m].depart;
   	       	 }
	 l++;
	 m = find_input_link(numero, l);
    }
*/

    /* nombre de neurones dans le groupe */
    nr = def_groupe[numero].nbre;

    /* Initialisation de donnees pour le groupe */
    if (def_groupe[numero].data == NULL)
    {
        selverston = malloc(sizeof(type_selverston));
        def_groupe[numero].data = selverston;

        selverston->v = 1;
        selverston->q = 0.2;
        selverston->ss=3;
/*        memset(def_groupe[numero].data, 0, nr * sizeof(type_selverston)); */
    }
    else


  {
    	selverston = def_groupe[numero].data;
    }

    v = (*selverston).v;
    q = (*selverston).q;
   ss= (*selverston).ss;
    /* 1er neuronne du groupe */
    first = def_groupe[numero].premier_ele;

    /* Parcours des neuronnes du groupe */
    
    for (neuron_index = first; neuron_index < (first + nr); neuron_index++)
    {
        i = neuron_index - first;
        
        /* On recupere le potentiel sur les liens d'entree */
        for (lien = neurone[neuron_index].coeff; lien != NULL; lien = lien->s)
		{
#ifdef CTRNN_DEBUG
            printf("potentiel = %f (%f * %f)\n", lien->val * neurone[lien->entree].s2, lien->val, neurone[lien->entree].s2);
#endif
			potentiel += (lien->val * neurone[lien->entree].s1);
		}

        potentiel2=potentiel;
        k1v=fv(q,v,potentiel2,tm,ts,ss,sf,Af);
		k1q=fq(q,v,potentiel2,tm,ts,ss,sf,Af);
		k1S=fS(q,v,ss,potentiel2,tm,lambda);
		
		k2v=fv(q+ h*k1q / 2.0,v+ h*k1v / 2.0,potentiel2,tm,ts,ss+h*k1S/2.0,sf,Af);
		k2q=fq(q+ h*k1q / 2.0,v+ h*k1v / 2.0,potentiel,tm,ts,ss+h*k1S/2.0,sf,Af);
		k2S=fS(q+ h*k1q / 2.0,v+ h*k1v / 2.0,ss+h*k1S/2.0,potentiel2,tm,lambda);
		
		k3v=fv(q+ h*k2q / 2.0,v+ h*k2v / 2.0,potentiel2,tm,ts,ss+h*k2S/2.0,sf,Af);
		k3q=fq(q+ h*k2q / 2.0,v+ h*k2v / 2.0,potentiel2,tm,ts,ss+h*k2S/2.0,sf,Af);
		k3S=fS(q+ h*k2q / 2.0,v+ h*k2v / 2.0,ss+h*k2S/2.0,potentiel2,tm,lambda);
		
		k4v=fv(q+ h*k3q,v+ h*k3v,potentiel2,tm,ts,ss+h*k3S,sf,Af);
		k4q=fq(q+ h*k3q,v+ h*k3v,potentiel2,tm,ts,ss+h*k3S,sf,Af);
        k4S=fS(q+ h*k3q,v+ h*k3v,ss+h*k3S,potentiel2,tm,lambda);

		v = v + h * 0.1666666 * (k1v + 2 * k2v + 2*k3v + k4v);
		q = q + h * 0.1666666 * (k1q + 2 * k2q + 2*k3q + k4q);
		ss = ss + h * 0.1666666 * (k1S + 2 * k2S + 2*k3S + k4S);

        neurone[neuron_index].s2 =ss;
        neurone[neuron_index].s1 = neurone[neuron_index].s = v;

        selverston->v = v;
        selverston->q = q;
        selverston->ss =ss;

#ifdef CTRNN_DEBUG
        printf("s = %f : s1 = %f : s2 = %f\n", neurone[neuron_index].s, neurone[neuron_index].s1, neurone[neuron_index].s2);
#endif
	
    }
    (void) i;
}

