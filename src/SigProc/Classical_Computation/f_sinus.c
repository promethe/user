/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sinus.c
\brief 

Author: LAGARDE Matthieu
Created: 10/11/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

/*#define DEBUG 1*/

#define ANGLE_RADIANS 0
#define ANGLE_DEGREES 1

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

typedef struct data_sinus
{
   int angle_units;
} data_sinus;

void new_sinus(int gpe)
{
   int index, link;
   char string[255];
   int angle_units = ANGLE_RADIANS;
   data_sinus *my_data = NULL;
   
   if (def_groupe[gpe].data == NULL)
   {
      index = 0;
      link = find_input_link(gpe, index);

      while (link != -1)
      {
	 if (prom_getopt(liaison[link].nom, "d", string) >= 1 || prom_getopt(liaison[link].nom, "D", string) >= 1)
	 {
	    angle_units = ANGLE_DEGREES;
	 }
	 
   	 index++;
   	 link = find_input_link(gpe, index);
      }

      my_data = ALLOCATION(data_sinus);

      my_data->angle_units = angle_units;
      def_groupe[gpe].data = my_data;
   }

}

void function_sinus(int gpe)
{
   int		i;
   int		first;
   float		potentiel = 0.0;
   type_coeff	*lien = NULL;
   data_sinus *my_data = (data_sinus *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retrieve data");
   }

   first = def_groupe[gpe].premier_ele;

   dprints("f_sinus(%s) : entree\n", def_groupe[gpe].no_name);

   for (i = 0; i < def_groupe[gpe].nbre; i++)
   {
      dprints("f_sinus(%s) : neurone %i\n", def_groupe[gpe].no_name, i);
      potentiel = 0;

      for (lien = neurone[first + i].coeff; lien != NULL; lien = lien->s)
      {
	 dprints("f_sinus(%s) : lien trouve\n", def_groupe[gpe].no_name);
	 potentiel += lien->val * neurone[lien->entree].s1;
      }
      
      if (my_data->angle_units == ANGLE_DEGREES)
      {
	 potentiel *= M_PI / 180;
      }
      neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = sin(potentiel);
   }

   dprints("f_sinus(%s) : sortie\n", def_groupe[gpe].no_name);
}

void destroy_sinus(int gpe)
{
   dprints("destroy_sinus(%s): Entering function\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_sinus *) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_sinus(%s): Leaving function\n", def_groupe[gpe].no_name);
}
