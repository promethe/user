/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_sum_bearing f_sum_bearing
ingroup libSigProc

To be completed

\file
ingroup f_sum_bearing
*/

#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>

#include <Kernel_Function/prom_getopt.h>
typedef struct MyData_f_sum_bearing
{
    int gpe_bearing;
    float facteur_influence;
} MyData_f_sum_bearing;

void function_sum_bearing(int numero)
{

    int i = 0, l;
    int gpe_bearing = -1, pos_final;
    float facteur_influence = 1.;
    char param[32];
    float the_max = -1.;
    MyData_f_sum_bearing *my_data;
    float Fx, Fy, angle_final, val;

    if (def_groupe[numero].data == NULL)
    {
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "b", param) >= 1)
            {
                facteur_influence = atof(param);
                gpe_bearing = liaison[l].depart;
            }
            i++;
            l = find_input_link(numero, i);

        }
        my_data = malloc(sizeof(MyData_f_sum_bearing));
        my_data->gpe_bearing = gpe_bearing;
        my_data->facteur_influence = facteur_influence;
        def_groupe[numero].data = (void *) my_data;

        if (gpe_bearing == -1)
        {
            printf("pb, pas de groupe bearing dans %s \n", __FUNCTION__);
            exit(0);
        }
        if (def_groupe[gpe_bearing].nbre != def_groupe[numero].nbre)
        {

            printf
                ("%s doit avoir le meme nombre de neurone que le groupe d entree \n",
                 __FUNCTION__);
            exit(0);
        }
    }
    else
    {
        my_data = (MyData_f_sum_bearing *) def_groupe[numero].data;
        facteur_influence = my_data->facteur_influence;
        gpe_bearing = my_data->gpe_bearing;
    }

    for (i = def_groupe[gpe_bearing].premier_ele;
         i <
         def_groupe[gpe_bearing].premier_ele + def_groupe[gpe_bearing].nbre;
         i++)
    {
        val = neurone[i].s1;
        if (the_max < val)
            the_max = val;
    }

    Fx = Fy = 0.;
    if (the_max > 0)
    {
        for (i = 0; i < def_groupe[numero].nbre; i++)
        {
            neurone[def_groupe[numero].premier_ele + i].s =
                pow((neurone[def_groupe[gpe_bearing].premier_ele + i].s1) /
                    the_max, facteur_influence);

            Fx = Fx + neurone[def_groupe[numero].premier_ele +
                              i].s * cos(((float) (i * 2. * M_PI)) /
                                         ((float) (def_groupe[numero].nbre)));
            Fy = Fy + neurone[def_groupe[numero].premier_ele +
                              i].s * sin(((float) (i * 2. * M_PI)) /
                                         ((float) (def_groupe[numero].nbre)));

        }

        angle_final = atan2(Fy, Fx);
        if (angle_final < 0.)
            angle_final = angle_final + 2 / M_PI;

        pos_final =
            (int) (angle_final * def_groupe[numero].nbre / 2 / M_PI +
                   def_groupe[numero].premier_ele);
        /*neurone[pos_final].s=neurone[pos_final].s1=neurone[pos_final].s2=1.0; */

        for (i = def_groupe[numero].premier_ele;
             i < def_groupe[numero].premier_ele + def_groupe[numero].nbre;
             i++)
        {
            if (i == pos_final)
                neurone[i].s1 = neurone[i].s2 = 1.0 /*sqrt(Fx*Fx+Fy*Fy) */ ;
            else
                neurone[i].s1 = neurone[i].s2 = 0.;
        }
    }
    else
    {
        for (i = def_groupe[numero].premier_ele;
             i < def_groupe[numero].premier_ele + def_groupe[numero].nbre;
             i++)
        {
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
        }
    }
}
