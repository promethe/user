/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_ressort.c
 \brief

 Author: Nils Beaussé
 Created: 2015
 Modified: 2015
 - author: Nils Beaussé
 - description: Ajout lien init
 - date: 2015

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 - Permet l'emission de pic (valeur de neurone à 1) tout les 't'. 't' étant compris entre "bmin" et "bmax". La valeur de t est piochée de façon aléatoire entre ces bornes de façon à suivre une distribution uniforme.
 t est en micro-seconde. Le pic dure une itération. Il est de la responsabilité de l'utilisateur d'utiliser des systemes à retard si il désirent faire durer le pic plus longtemps.


 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo
 - description: -bminXXX (XXX en micro-seconde) -bmaxXXX (XXX en micro-seconde) -initX (X booleen : 1 -> premier pic à l'itération 1 / 0 -> le premier pic est aprés le temps d'attente initial)
 - input expected group: none
 - where are the data?: none


 Known bugs: none found yet...

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_random_time {
  int borne_moins;
  int borne_plus;
  int premier_passage;
  int random_suivant;
  int vient_de_passer;
  int init;
  int input;
  int sync;
  int autorisation_depart_pour_cette_boucle;
  struct timeval time_prece;
  int type_sync;

} data_random_time;

void new_random_time(int gpe)
{
  data_random_time *my_data = NULL;
  int i, l, borne_moins = -1, borne_plus = -1, init = 0;
  int param;
  int retour_sync=1;

  my_data = ALLOCATION(data_random_time);

  my_data->premier_passage = 1;
  my_data->input = -1;
  my_data->sync = -1;
  my_data->type_sync = 1;

  i = 0;
  l = find_input_link(gpe, i);
  while (l != -1)
  {
    if (prom_getopt_int(liaison[l].nom, "bmin", &param) == 2)
    {
      borne_moins = param;
    }
    if (prom_getopt_int(liaison[l].nom, "bmax", &param) == 2)
    {
      borne_plus = param;
    }
    if (prom_getopt_int(liaison[l].nom, "init", &param) == 2)
    {
      init = param;
    }
    if (prom_getopt(liaison[l].nom, "input", NULL) > 0)
    {
      my_data->input = liaison[l].depart;
    }
    if (prom_getopt_int(liaison[l].nom, "sync", &retour_sync) > 0)
    {
      my_data->sync = liaison[l].depart;
      my_data->type_sync=retour_sync;
    }

    i++;
    l = find_input_link(gpe, i);
  }
  if ((borne_moins == -1 || borne_plus == -1) && my_data->input == -1)
  {
    EXIT_ON_ERROR("ERROR in new_random_time(%s): No bmin or bmax or input\n", def_groupe[gpe].no_name);
  }

  my_data->init = init;
  my_data->borne_moins = borne_moins;
  my_data->borne_plus = borne_plus;
  my_data->random_suivant = (int) frand_a_b(my_data->borne_moins, my_data->borne_plus);
  my_data->vient_de_passer = 0;

  def_groupe[gpe].data = (void*) my_data;

}

void function_random_time(int gpe)
{
  struct timeval time_actu;
  data_random_time *my_data = (data_random_time*) def_groupe[gpe].data;
  long int delta_time;
  int i;

  gettimeofday(&time_actu, (void *) NULL);
  if (my_data->premier_passage == 1)
  {
    my_data->time_prece = time_actu;
    my_data->premier_passage = 0;
  }

  if (my_data->input != -1)
  {
    my_data->borne_plus = my_data->borne_moins = (int) (neurone[def_groupe[my_data->input].premier_ele].s1);
  }


  if (my_data->sync != -1)
  {
    if (my_data->autorisation_depart_pour_cette_boucle == 0)
      {
        my_data->autorisation_depart_pour_cette_boucle = neurone[def_groupe[my_data->sync].premier_ele].s1;
        if(my_data->type_sync==2)  my_data->time_prece = time_actu;
      }
  }
  else
  {
    my_data->autorisation_depart_pour_cette_boucle = 1;
  }

  if (my_data->autorisation_depart_pour_cette_boucle == 0)
  {
    delta_time=0.0;
  }
  else
  {
    delta_time = (time_actu.tv_sec - my_data->time_prece.tv_sec) * 1000000 + (time_actu.tv_usec - my_data->time_prece.tv_usec);
  }


  if (delta_time >= my_data->random_suivant || my_data->init == 1)
  {

      for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
      {
        neurone[i].s1 = 1;
      }

    my_data->vient_de_passer = 1;
    my_data->random_suivant = (int) frand_a_b(my_data->borne_moins, my_data->borne_plus);
    my_data->time_prece = time_actu;
    my_data->autorisation_depart_pour_cette_boucle = 0;
    if (my_data->init == 1)
    {
      my_data->init = 0;
    }
  }
  else if (my_data->vient_de_passer == 1)
  {
    for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
    {
      neurone[i].s1 = 0;
    }
    my_data->vient_de_passer = 0;
  }

}

void destroy_random_time(int gpe)
{

  if (def_groupe[gpe].data != NULL)
  {
    free(def_groupe[gpe].data);
  }

}
