/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_alea_select f_alea_select
\ingroup libSigProc

\brief 	Select n neurons randomly in the input group at the begining of the
simulation

\section Modified
- author	D. Bailly
- description: creation
- date	25/02/2010

\details
	Cette fonction choisit aléatoirement n neurones dans le groupe en entrée (n
	le nombre de neurones dans le groupe) et recopiela sortie de chacun de ces
	neurones sur la sortie du neurone correspondant dans le présent groupe.
	
	Allows to select n neurons randomly in the input group where n is
	the number of neurons in this group. The selection is made the initialization
	of this function. The output values of those chosen neurons are then copied on 
	the respectiv output of this group at each step.

\section Options
	Le groupe a besoin d'un groupe en entré. Le lien n'a aucune option
*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_alea_select
{
  int nbr;
  int *indices;
} Data_alea_select;



void new_alea_select(int index_of_group)
{
  Data_alea_select *data;
  int i, l, nbr, inputGpe;

  
#ifdef DEBUG
    printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  l = find_input_link(index_of_group, 0);
  if(l==-1) EXIT_ON_ERROR("no input link\n");
  inputGpe = liaison[l].depart;
  
  data = (Data_alea_select*)ALLOCATION(Data_alea_select);
  data->indices = MANY_ALLOCATIONS(def_groupe[index_of_group].nbre, int);
  nbr = def_groupe[index_of_group].nbre;
  data->nbr = nbr;
  def_groupe[index_of_group].data = data;
  
  /* a little message to help find bugs */
  if(find_input_link(index_of_group, 1) != -1) PRINT_WARNING("there is an unused input link\n");
  for(i=0; i<nbr; i++)
  {
    data->indices[i] = def_groupe[inputGpe].premier_ele + rand()%def_groupe[inputGpe].nbre;
  }

#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


void function_alea_select(int index_of_group)
{
  int i, nbr, premier_ele;
  int *indices;
  Data_alea_select *data;

    
#ifdef DEBUG
    printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  data = (Data_alea_select *)def_groupe[index_of_group].data;
  nbr = data->nbr;
  indices = data->indices;
  premier_ele = def_groupe[index_of_group].premier_ele;
  
  for(i=0; i<nbr; i++)
  {
    neurone[premier_ele + i].s = neurone[indices[i]].s;
    neurone[premier_ele + i].s1 = neurone[indices[i]].s1; 
    neurone[premier_ele + i].s2 = neurone[indices[i]].s2; 
  }
  
  
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


void destroy_alea_select(int index_of_group)
{
  
#ifdef DEBUG
  printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  if(def_groupe[index_of_group].data != NULL)
  {
    free(((Data_alea_select *)def_groupe[index_of_group].data)->indices);
    free(def_groupe[index_of_group].data);
  }
  
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}
