/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_bille2D.c
 \brief

 Author: BEAUSSÉ Nils
 Created: 10/11/2006
 Modified:

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:
 Fonctions qui simule le déplacement d'un poids dans un espace 2D, l'energie dépensée étant fonction de la hauteur parcourue.
 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

/*#define DEBUG 1*/

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

#define g 9.89
#define Dt 0.03
#define m 0.5

typedef struct data_environnement_2D {
  int taille_x;
  int taille_y;
  int no_group;
  float celx;
  float cely;
  float posx;
  float posy;
  float Epp;
  float Ec;
  float W;
  float W2;
  float pos_recompense;
  int pret_a_repositionner;
  float penalite_force;
  int no_group_sum_force;
  float accely;
  float accelx;

} data_environnement_2D;

double frand_a_b(double a, double b)
{
  return ((rand() / (double) RAND_MAX) * (b - a) + a);
}

void new_bille2D(int gpe)
{
  int index, link, taille_x = 1, taille_y = 1, no_group = -1, no_group_sum_force = -1;

  char string[255];
  data_environnement_2D *my_data = NULL;

  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);

    while (link != -1)
    {
      if (prom_getopt(liaison[link].nom, "taille_x", string) >= 1)
      {
        taille_x = atoi(string);
      }
      if (prom_getopt(liaison[link].nom, "taille_y", string) >= 1)
      {
        taille_y = atoi(string);
      }
      if (prom_getopt(liaison[link].nom, "XY", string) >= 1)
      {
        no_group = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "sumforce", string) >= 1)
      {
        no_group_sum_force = liaison[link].depart;
      }
      index++;
      link = find_input_link(gpe, index);
    }

    if (no_group == -1) EXIT_ON_ERROR("En entrée de bille2D, groupe de commande non trouve");
    my_data = ALLOCATION(data_environnement_2D);
    printf("taillex = %d\n", taille_x);
    printf("tailley = %d\n", taille_y);
    printf("no_group = %d\n", taille_y);
    my_data->taille_x = taille_x;
    my_data->taille_y = taille_y;
    my_data->no_group = no_group;
    my_data->posx = (float) taille_x / 2;
    my_data->posy = (float) taille_y / 2;
    my_data->celx = 0.0;
    my_data->cely = 0.0;
    my_data->W = 0.0;
    my_data->W2 = 0.0;
    my_data->pos_recompense = 0.75;
    my_data->pret_a_repositionner = 0;
    my_data->penalite_force = 0;
    my_data->no_group_sum_force = no_group_sum_force;
    def_groupe[gpe].data = (void*) my_data;
  }

}

void function_bille2D(int gpe)
{
  int first;
  float ForceN = 0.0;
  float ForceE = 0.0;
  float oldEpp = 0.0, pos_0_1_x = 0.0, pos_0_1_y = 0.0;
  float oldEc = 0.0;
  float oldy = 0.0;
  float oldx = 0.0;
  float recompense = 0.0, D = 0.0, F = 0.0;
  float pos_recompense;
  float raz_apprentissage = 0.0;
  float lancer_transition = 0.0;
  float sigma = 0;
  float jerkx = 0.0, jerky = 0.0;
  data_environnement_2D *my_data = (data_environnement_2D*) def_groupe[gpe].data;

  srand(time(NULL));
  pos_recompense = my_data->pos_recompense;
  if (my_data == NULL)
  {
    EXIT_ON_ERROR("Cannot retrieve data");
  }
  //  my_data->W=0.0;
  my_data->W2 = 0.0;
  first = def_groupe[gpe].premier_ele;

  // On interprete les potentiels arrivant sur une neuronne comme des forces

  //anciennement 20*

  ForceE = 12 * neurone[def_groupe[my_data->no_group].premier_ele].s1;
  if (ForceE >= 20.0) ForceE = 10.0;

  //	ForceE=ForceE-my_data->penalite_force;

  ForceN = 12 * neurone[def_groupe[my_data->no_group].premier_ele + 1].s1;
  if (ForceN >= 20.0) ForceN = 10.0;

  //NOTA : on revient à force = vitesse pour le moment !

  //penalité si on bourine les limites

  if ((my_data->posx <= 0.0 && ForceE < 0.0) || (my_data->posy <= 0.0 && ForceN < 0.0) || (my_data->posx >= my_data->taille_x && ForceE > 0) || (my_data->posy >= my_data->taille_y && (ForceN) > 0))
  {
    my_data->W2 = -0.1;
    //my_data->W=0.000;
  }

  oldx = my_data->posx;
  oldy = my_data->posy;

  //prise en compte de la gravité.

  my_data->cely = (my_data->cely) + (((ForceN / m) * Dt) - (g * Dt));
  my_data->celx = (my_data->celx) + (((ForceE / m) * Dt));

  jerkx = (((ForceE / m))) - my_data->accelx; // Dt est fixe, donc inutile de diviser
  jerky = (((ForceN / m)) - (g)) - my_data->accely;
  my_data->accely = (((ForceN / m)) - (g));
  my_data->accelx = (((ForceE / m)));
  //   my_data->cely=ForceN;
  // my_data->celx=ForceE;

  /*
   if(((float)my_data->posy/(float)my_data->taille_y)<0.01)
   {
   //   my_data->cely=my_data->cely/6.0;
   //   my_data->celx=my_data->celx/6.0;
   my_data->W=my_data->W-0.02;
   }
   */

  my_data->posx = (my_data->posx) + (my_data->celx) * Dt;
  my_data->posy = (my_data->posy) + (my_data->cely) * Dt;

  if (my_data->posx < 0.0)
  {
    my_data->posx = 0.0;
    my_data->celx = 0.0;
  }
  if (my_data->posy < 0.0)
  {
    my_data->posy = 0.0;
    my_data->cely = 0.0;
  }
  if (my_data->posx > my_data->taille_x)
  {
    my_data->posx = my_data->taille_x;
    my_data->celx = 0.0;
  }
  if (my_data->posy > my_data->taille_y)
  {
    my_data->posy = my_data->taille_y;
    my_data->cely = 0.0;
  }
  if ((my_data->celx > -0.001 && my_data->celx < 0.001) && (my_data->cely > -0.001 && my_data->cely < 0.001)) my_data->W -= 0.001;
  else my_data->W = 0.0;

  if (my_data->posy <= 0.001 && oldy <= 0.001)
  {
    D = fabs(oldx - my_data->posx);
    F = m * 20 * copysignf(1.0, my_data->celx);
    my_data->W2 = my_data->W2 - ((fabs(F) * D) / 4);
    // printf("oldx =%f posx = %f m=%f my_data->celx = %f, D=%f, F=%f \n",oldx,my_data->posx,m,my_data->celx,D,F);
  }
  else
  {
    F = 0;
    D = 0;
  }
  //  my_data->penalite_force=fabs(3*my_data->celx);

  //my_data->W2=abs(F)*D;
  // my_data->W=0;
  //printf("my_data->W2=%f \n",my_data->W2);

  oldEpp = my_data->Epp;
  oldEc = my_data->Ec;
  my_data->Epp = m * g * my_data->posy;
  my_data->Ec = (1.0 / 2.0) * m * (pow(my_data->celx, 2) + pow(my_data->cely, 2));
  // OldEM=oldEpp+oldEc;
  // EM=my_data->Epp+my_data->Ec;
  // my_data->W=EM-OldEM; //energie mecanique rajoutée
  // if(my_data->W<0)

  pos_0_1_x = (float) ((float) (my_data->posx)) / ((float) (my_data->taille_x));
  pos_0_1_y = (float) ((float) (my_data->posy)) / ((float) (my_data->taille_y));
  sigma = 0.1;
  //   if(((int)copysignf(1,pos_recompense-pos_0_1_x)==(int)copysignf(1,my_data->celx))&&((int)copysignf(1,0-pos_0_1_y)==(int)copysignf(1,my_data->cely)))
  //  recompense=2*(float)exp(-(pow(pos_0_1_x-pos_recompense, 2.0) + pow(pos_0_1_y-0, 2.0)) / (2*sigma*sigma));

  if ((((float) (my_data->posx) / (float) (my_data->taille_x)) > pos_recompense - 0.085) && (((float) (my_data->posx) / (float) (my_data->taille_x)) < (pos_recompense + 0.085)) && (((float) (my_data->posy) / (float) (my_data->taille_y)) < 0.01))
  {
    recompense = 2.0;
    lancer_transition = 1.0;
    my_data->posx = 0.2;
    my_data->posy = 0.0;
    my_data->cely = 0.0;
    my_data->celx = 0.0;
    //pos_recompense=0.75+frand_a_b(-0.2,0.2);
  }
  else
  {
    lancer_transition = 0;
  }

  //   if(my_data->no_group_sum_force!=-1) my_data->W-=fabs(((neurone[def_groupe[my_data->no_group_sum_force].premier_ele].s1)/30.0));
  //    my_data->W-=sqrt(jerkx*jerkx+jerky*jerky)/4.0;
  // printf("jerk = %f",sqrt(jerkx*jerkx+jerky*jerky));
  neurone[first].s1 = neurone[first].s = neurone[first].s2 = ((float) my_data->posx / (float) my_data->taille_x);
  neurone[first + 1].s1 = neurone[first + 1].s = neurone[first + 1].s2 = ((float) my_data->posy / (float) my_data->taille_y);
  neurone[first + 2].s1 = neurone[first + 2].s = neurone[first + 2].s2 = ((float) my_data->W);
  neurone[first + 3].s1 = neurone[first + 3].s = neurone[first + 3].s2 = recompense;
  neurone[first + 4].s1 = neurone[first + 4].s = neurone[first + 4].s2 = ((float) my_data->W2);
  neurone[first + 5].s1 = neurone[first + 5].s = neurone[first + 5].s2 = ((float) my_data->celx);
  neurone[first + 6].s1 = neurone[first + 6].s = neurone[first + 6].s2 = ((float) my_data->cely);
  neurone[first + 7].s1 = neurone[first + 7].s = neurone[first + 7].s2 = raz_apprentissage;
  neurone[first + 8].s1 = neurone[first + 8].s = neurone[first + 8].s2 = lancer_transition;
  (void) oldEc;
  (void) oldEpp;
}

void destroy_bille2D(int gpe)
{
  dprints("destroy_sinus(%s): Entering function\n", def_groupe[gpe].no_name);
  if (def_groupe[gpe].data != NULL)
  {
    free(((data_environnement_2D *) def_groupe[gpe].data));
    def_groupe[gpe].data = NULL;
  }
  dprints("destroy_sinus(%s): Leaving function\n", def_groupe[gpe].no_name);
}
