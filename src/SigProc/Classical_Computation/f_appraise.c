/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


#define MAXIMUM_SIZE_OF_OPTION 32
#define MAXIMUM_NUMBER_OF_DIMS 3
#define MAXIMUM_NUMBER_OF_CHANNELS 8

/**
author: Arnaud Blanchard
date: 29/10/2009
*/

typedef struct my_data
{
   float sensibility[MAXIMUM_NUMBER_OF_CHANNELS];
   int number_of_neurons_of_sensation, number_of_neurons_of_desired_sensation;
   int number_of_channels, number_of_channels_per_sensation, number_of_channels_per_desired_sensation;
   int width_of_sensation, height_of_sensation, width_of_desired_sensation, height_of_desired_sensation;


   type_neurone *neurons_of_desired_sensations[MAXIMUM_NUMBER_OF_CHANNELS], *neurons_of_sensations[MAXIMUM_NUMBER_OF_CHANNELS], *neurons;

}My_data;


int link_start_with_name(type_liaison *link, const char *name)
{
   if (strncmp(link->nom, name, strlen(name)) == 0) return 1;
   else return 0;
}

float link_try_to_get_float_of_option(type_liaison *link, const char *option)
{
   char text_of_option[MAXIMUM_SIZE_OF_OPTION];
   int success;

   success = prom_getopt(link->nom, option, text_of_option);
   if (success == 1) EXIT_ON_ERROR("The content of option %s in link %s is not valid.",  option, link->nom);
   else if (success == 0) 	return -1.0;

   return atof(text_of_option);
}

float link_get_float_of_option(type_liaison *link, const char *option)
{
   float result;

   result = link_try_to_get_float_of_option(link, option);
   if (result < 0.0) EXIT_ON_ERROR("Option %s is not defined in link %s.",  option, link->nom);

   return result;
}

int link_try_to_get_int_of_option(type_liaison *link, const char *option)
{
   char text_of_option[MAXIMUM_SIZE_OF_OPTION];
   int success;

   success = prom_getopt(link->nom, option, text_of_option);
   if (success == 1) EXIT_ON_ERROR("The content of option %s in link %s is not valid.", option, link->nom);
   else	if (success == 0) return -1;

   return atoi(text_of_option);
}

int link_get_int_of_option(type_liaison *link, const char *option)
{
   int result;

   result = link_try_to_get_int_of_option(link, option);
   if (result < 0) EXIT_ON_ERROR("Option %s is not defined in link %s.", option, link->nom);

   return result;
}


void new_appraise(int index_of_group)
{
   float sensibility;
   int i;
   int number_of_links, index_of_link, channel;
   type_liaison *link;
   type_groupe *group;
   My_data *my_data;

   group = &def_groupe[index_of_group];
   my_data = ALLOCATION(My_data);
   group->data = my_data;

   my_data->neurons = &neurone[group->premier_ele];

   /** Default values which may change with the options on the links */
   my_data->width_of_sensation = -1;
   my_data->height_of_sensation = -1;

   sensibility = -1;

   for (i=0; i<MAXIMUM_NUMBER_OF_CHANNELS; i++)
   {
      my_data->sensibility[i] = -1;

   }

   my_data->number_of_channels_per_desired_sensation = -1;
   my_data->number_of_channels_per_sensation = -1;

   for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
   {
      link = &liaison[index_of_link];

      channel = link_try_to_get_int_of_option(link, "channel=");
      sensibility = link_try_to_get_float_of_option(link, "sensibility=");

      /** DESIRED SENSATIONS */
      if (link_start_with_name(link, "desired_sensation"))
      {
         if (channel != -1) /** Un canal est precise */
         {
            my_data->neurons_of_desired_sensations[channel] = &neurone[def_groupe[link->depart].premier_ele];


            if (sensibility >= 0) my_data->sensibility[channel] = sensibility;

            my_data->width_of_desired_sensation = def_groupe[link->depart].taillex;
            my_data->height_of_desired_sensation = def_groupe[link->depart].tailley;

            my_data->number_of_channels_per_desired_sensation = 1;
         }
         else /** Chaque neurone correspond à un canal different*/
         {
            my_data->number_of_channels_per_desired_sensation = def_groupe[link->depart].nbre;

            if (my_data->number_of_channels_per_desired_sensation>MAXIMUM_NUMBER_OF_CHANNELS) EXIT_ON_ERROR("group %s: The number of values of desired_sensations (%d) cannot exceed %d.", group->no_name, my_data->number_of_channels_per_desired_sensation, MAXIMUM_NUMBER_OF_CHANNELS);

            for (i=0; i<my_data->number_of_channels_per_desired_sensation; i++)
            {
               if (sensibility >= 0)
               {
                  if (my_data->sensibility[i] < 0)  my_data->sensibility[i] = sensibility;
                  else EXIT_ON_ERROR("Group %s: sensivbility of channel %d have been defined many times.", group->no_name, i);
               }

               my_data->neurons_of_desired_sensations[i] = &neurone[def_groupe[link->depart].premier_ele+i];
            }

            my_data->width_of_desired_sensation = 1;
            my_data->height_of_desired_sensation = 1;
         }
      }
      /** SENSATION */
      if (link_start_with_name(link, "sensation"))
      {
         if (channel != -1) /** Un canal est precise */
         {
            my_data->neurons_of_sensations[channel] = &neurone[def_groupe[link->depart].premier_ele];

            if (sensibility >= 0)
            {
                  if (my_data->sensibility[channel] < 0)  my_data->sensibility[channel] = sensibility;
                  else EXIT_ON_ERROR("Group %s: sensivbility of channel %d have been defined many times.", group->no_name, channel);
            }

            if (my_data->width_of_sensation < 0)  my_data->width_of_sensation = def_groupe[link->depart].taillex;
            else if (my_data->width_of_sensation != def_groupe[link->depart].taillex) EXIT_ON_ERROR("Group %s: different channels of sensations do not have the same width (%d and %d).", group->no_name, my_data->width_of_sensation, def_groupe[link->depart].taillex);

            if (my_data->height_of_sensation < 0) my_data->height_of_sensation = def_groupe[link->depart].tailley;
            else if (my_data->height_of_sensation != def_groupe[link->depart].tailley) EXIT_ON_ERROR("Group %s: different channels of sensations do not have the same height (%d and %d).", group->no_name, my_data->height_of_sensation, def_groupe[link->depart].tailley);

            if (channel > my_data->number_of_channels_per_sensation - 1) my_data->number_of_channels_per_sensation = channel+1;

         }
         else /** Chaque neurone correspond à un canal different*/
         {
            my_data->number_of_channels_per_sensation = def_groupe[link->depart].nbre;
            if (my_data->number_of_channels_per_sensation>MAXIMUM_NUMBER_OF_CHANNELS) EXIT_ON_ERROR("group %s: The number of neurons of sensations (%d) cannot exceed MAXIMUM_NUMBER_OF_CHANNELS (%d). Maybe you wanted to use a specific channel (-channel=...).", group->no_name, my_data->number_of_channels_per_sensation, MAXIMUM_NUMBER_OF_CHANNELS);

            for (i=0; i<my_data->number_of_channels_per_sensation; i++)
            {
               if (sensibility >= 0) my_data->sensibility[i] = sensibility;
               my_data->neurons_of_sensations[i] = &neurone[def_groupe[link->depart].premier_ele+i];
            }

            my_data->width_of_sensation = 1;
            my_data->height_of_sensation = 1;
         }
      }
   }

   my_data->number_of_neurons_of_desired_sensation = my_data->width_of_desired_sensation * my_data->height_of_desired_sensation;
   my_data->number_of_neurons_of_sensation = my_data->width_of_sensation*my_data->height_of_sensation;

   my_data->number_of_channels = 1;

   if (my_data->number_of_channels < my_data->number_of_channels_per_desired_sensation) my_data->number_of_channels = my_data->number_of_channels_per_desired_sensation;
   else if (my_data->number_of_channels < my_data->number_of_channels_per_sensation) my_data->number_of_channels= my_data->number_of_channels_per_sensation;

   /** By default sensibility is 0 */
   for (i=0; i<my_data->number_of_channels; i++)
   {
      if (my_data->sensibility[i] < 0) my_data->sensibility[i] = 0;
   }

}

void function_appraise(int index_of_group)
{
   My_data *my_data;
   float familiarity;
   int i, channel;
   type_groupe *group;

   group = &def_groupe[index_of_group];
   my_data = (My_data*)group->data;

   for (i=0; i<group->nbre; i++)
   {
      familiarity = 1.0;

      for (channel=0; channel < my_data->number_of_channels;channel ++)
      {
         familiarity *=exp(- my_data->sensibility[channel] * pow( my_data->neurons_of_sensations[channel%my_data->number_of_channels_per_sensation][i%my_data->number_of_neurons_of_sensation].s1 - my_data->neurons_of_desired_sensations[channel%my_data->number_of_channels_per_desired_sensation][i%my_data->number_of_neurons_of_desired_sensation].s1, 2));
      }

      my_data->neurons[i].s = familiarity;
      my_data->neurons[i].s1 = familiarity;
      my_data->neurons[i].s2 = familiarity;
   }
}


void destroy_appraise(int index_of_group)
{
   My_data *my_data;
   type_groupe *group;

   group = &def_groupe[index_of_group];
   my_data = (My_data*)group->data;

   free(def_groupe[index_of_group].data);
}
