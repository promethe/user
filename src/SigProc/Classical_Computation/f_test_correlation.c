/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: BLANCHARD Benjamin
Created: 2010

Theoritical description:
 - \f$  LaTeX equation: c[x,y] = \max_{0<tau<n} \displaystyle {\sum_{t=0}^{n}} x(t)*y(t-tau)*\frac{t}{n}  \f$ 

Description: 	Calcule l'intercorrelation entre chaque neurone issu du lien "ext" avec le premier neurone issu du lien "osc"


************************************************************/

#include <stdlib.h>
#include <string.h>

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/** calculer l'intercorrelation entre l'oscillateur actuel et les mouvements externes */
typedef struct my_data
{
	type_groupe *input_groupExt;
	type_groupe *input_groupOsc;
	/* sauvegarde des instants précédents pour calculer la corrélation
	on enregistre les "nbEtats" derniers instants des deux signaux
	*/
	float *neuprevExt;	/*accumulation brute des signaux*/
	float *neuprevOsc;
	float *neutravExt;  /* signaux normalise centre */
	float *neutravOsc;
	float *correl;
	int nbEtats;
	int tailleExt;
	int tailleOsc;
	int indiceEnCours;
	int choixOsc;
	int neuPlein;	/* 0 ou 1 : A-t-on suffisamment d'etats pour calculer la correlation */
	
} My_data;


void new_test_correlation(int index_of_group)
{
    int number_of_links, index_of_link;
    type_liaison *link;
    type_groupe *group;
    /*type_groupe *group, *A_group = NULL, *B_group = NULL;*/
    My_data *my_data;
	
    my_data = ALLOCATION(My_data);

    group = &def_groupe[index_of_group];
    group->data = my_data;
    
	for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
	   {
		link = &liaison[index_of_link];
		if (strcmp(link->nom, "ext")==0) my_data->input_groupExt = &def_groupe[link->depart];
		else if (strcmp(link->nom, "osc")==0) my_data->input_groupOsc = &def_groupe[link->depart];
		else EXIT_ON_ERROR("Group: %d, type of link unknowm: %s", index_of_group, link->nom);
	   }
	   
	/* valeurs initiales*/
	my_data -> nbEtats = 40;
	my_data -> tailleExt = my_data -> input_groupExt -> nbre;
	my_data -> tailleOsc = my_data -> input_groupOsc -> nbre;
	my_data -> indiceEnCours = 0;
	my_data -> choixOsc = 0;
	/*if (my_data -> tailleOsc <= g_referenceOscilloDefaut) my_data -> choixOsc = 0;*/
	my_data -> neuPlein = 0;
	my_data -> neuprevExt = (float*) malloc(sizeof(float) * my_data -> tailleExt * my_data -> nbEtats);
	my_data -> neuprevOsc = (float*) malloc(sizeof(float) * my_data -> tailleOsc * my_data -> nbEtats);
	my_data -> neutravExt = (float*) malloc(sizeof(float) * my_data -> tailleExt * my_data -> nbEtats);
	my_data -> neutravOsc = (float*) malloc(sizeof(float) * my_data -> tailleOsc * my_data -> nbEtats);
	my_data -> correl = (float*) malloc(sizeof(float) * my_data -> tailleExt * my_data -> nbEtats );

   
}

void function_test_correlation(int index_of_group)
{
	My_data *my_data;
    type_groupe *group;
    int i, t, tau, tt, deb;
	float sum, moyenne, correlmax, sigmax, foubli;
	
    group = &def_groupe[index_of_group];
    my_data = group->data;
   
    deb = group->premier_ele;

	/*
	 * osc = link->osc
	 * key = link->key
	 * 
	 * */
	
	/* ajouter l'etat en cours aux signaux */
	
	for (i=0; i< my_data -> tailleExt; i++)
	{
		my_data->neuprevExt[my_data->indiceEnCours * my_data->tailleExt +i] = neurone[ my_data->input_groupExt->premier_ele + i ].s1;
	}
	for (i=0; i< my_data -> tailleOsc; i++)
	{
		my_data->neuprevOsc[my_data->indiceEnCours * my_data->tailleOsc +i] = neurone[ my_data->input_groupOsc->premier_ele + i ].s1;
	}
	my_data->indiceEnCours++;
	if (my_data->indiceEnCours >= my_data->nbEtats)
	{
		my_data->indiceEnCours = 0;
		my_data->neuPlein = 1;
	}
	/* Pour calculer le taux de corrélation,
	 * on recentre les signaux
	 * on normalise
	 * on calcule la correlation pour chaque translation
	 * on choisit de comparer l'oscillateur numero "oscChoix" a chaque pixel*/
	/* calcul de distance */
	
	if (my_data->neuPlein)
	{
		/* recentrage : calcul de la moyenne puis soustraction*/
		
		/* pour le signal oscillation interne, on ne calcule que celui choisi (choixOsc) 
		 * calcul de la moyenne : sum / nbEtats */
		sum = 0;
		for (t=0; t<my_data->nbEtats; t++)
		{
			sum += my_data->neuprevOsc[t * my_data->tailleOsc + my_data->choixOsc];
		}
		/* on enregistre le signal - moyenne et on prepare la normalisation */
		moyenne = sum / my_data->nbEtats;
		sigmax = 0;
		for (t=0; t<my_data->nbEtats; t++)
		{
			my_data->neutravOsc[t * my_data->tailleOsc + my_data->choixOsc] = my_data->neuprevOsc[t * my_data->tailleOsc + my_data->choixOsc] - moyenne;
			if (sigmax < my_data->neutravOsc[t * my_data->tailleOsc + my_data->choixOsc])
				sigmax = my_data->neutravOsc[t * my_data->tailleOsc + my_data->choixOsc];
		}
		
		/* normaliser le signal : reduire de façon a ramener le max a 1 */
		for (t=0; t<my_data->nbEtats; t++)
		{
			my_data->neutravOsc[t * my_data->tailleOsc + my_data->choixOsc] /= sigmax;		
		}
		
		/* on refait la meme chose avec chaque pixel (neurone) externe */
		for (i=0; i< my_data -> tailleExt; i++)
		{
		 /* calcul de la moyenne : sum / nbEtats */
			sum = 0;
			for (t=0; t<my_data->nbEtats; t++)
			{
				sum += my_data->neuprevExt[t * my_data->tailleExt + i];
			}
			/* on enregistre le signal - moyenne et on prepare la normalisation */
			moyenne = sum / my_data->nbEtats;
			sigmax = 0;
			for (t=0; t<my_data->nbEtats; t++)
			{
				my_data->neutravExt[t * my_data->tailleExt + i] = my_data->neuprevExt[t * my_data->tailleExt + i] - moyenne;
				if (sigmax < my_data->neutravExt[t * my_data->tailleExt + i])
					sigmax = my_data->neutravExt[t * my_data->tailleExt + i];
			}
			
			/* normaliser le signal : reduire de façon a ramener le max a 1 */
			if (sigmax>0)
			{	
				for (t=0; t<my_data->nbEtats; t++)
				{
					my_data->neutravExt[t * my_data->tailleExt + i] /= sigmax;		
				}
			}
		}
		
		
		/* calcul de l'intercorrelation entre chaque signal externe et le signal oscillo choisi
		 * 
		 * */
		sigmax = 0;
		for (i=0; i< my_data -> tailleExt; i++)	/* pour chaque neurone */
		{
			neurone[deb+i].s1 = 0;
			correlmax =0;
			for (tau=0; tau<my_data -> nbEtats ; tau++) /* pour chaque translation my_data -> nbEtats*/
			{ 
				my_data -> correl[ i * my_data -> nbEtats + tau ] = 0;
				for (t=0; t< my_data -> nbEtats; t++) /* pour chaque instant */
				{
					tt = t-tau;
					if (t < my_data->indiceEnCours)	
						foubli = 1. - (float)(my_data->indiceEnCours-1-t) / (float)my_data -> nbEtats ;
					else foubli =  (float)(t-my_data->indiceEnCours-1) / (float)my_data -> nbEtats ;
					if (tt<0) tt += my_data -> nbEtats;
					/*neurone[deb+i].s1 += my_data->neutravExt[ t * my_data->tailleExt +i] * my_data->neutravOsc[ tt * my_data->tailleOsc + my_data->choixOsc ];*/
					my_data -> correl[ i * my_data -> nbEtats + tau ] += 
						my_data->neutravExt[ t * my_data->tailleExt +i] * my_data->neutravOsc[ tt * my_data->tailleOsc + my_data->choixOsc ] * foubli;/* */
				}
				/* on prend parmi toutes les translations le maximum */
				if (correlmax < my_data -> correl[ i * my_data -> nbEtats + tau ])
					correlmax = my_data -> correl[ i * my_data -> nbEtats + tau ];
			}
			neurone[deb+i].s1 = correlmax / (float)my_data -> nbEtats;
			if (sigmax < neurone[deb+i].s1)
				sigmax = neurone[deb+i].s1;
		}
		/* normaliser le signal : reduire de façon a ramener le max a 1 */
		if (sigmax > group->seuil)
		{
			for (i=0; i< my_data -> tailleExt; i++)
			{
				neurone[deb+i].s1 /= sigmax;
				neurone[deb+i].s = neurone[deb+i].s1;
				neurone[deb+i].s2 =neurone[deb+i].s1;		
			}
		}
		else 
		{
			for (i=0; i< my_data -> tailleExt; i++)
			{
				neurone[deb+i].s1 = 0;
				neurone[deb+i].s = neurone[deb+i].s1;
				neurone[deb+i].s2 =neurone[deb+i].s1;		
			}
		}
	}
	/* cas ou le buffer n'est pas rempli et le calcul impossible */
	else 
	{
		for (i=0; i< my_data -> tailleExt; i++)
		{
			neurone[deb+i].s1 =0 ;
			neurone[deb+i].s = neurone[deb+i].s1;
			neurone[deb+i].s2 =neurone[deb+i].s1;		
		}
	}
	
	      
}

void destroy_test_correlation(int index_of_group)
{
	My_data *my_data;
    type_groupe *group;
	group = &def_groupe[index_of_group];
    my_data = group->data;
	free(my_data->neuprevExt);
	free(my_data->neuprevOsc);
	free(my_data->correl);
    /*
	* segfault ! */
    free( my_data );
	
}
