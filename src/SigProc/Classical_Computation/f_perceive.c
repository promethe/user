/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <libx.h>

#include <outils.h>

#define MAXIMUM_SIZE_OF_OPTION 32
#define MAXIMUM_NUMBER_OF_DIMS 3
#define MAXIMUM_NUMBER_OF_CHANNELS 8

/**
author: Arnaud Blanchard
date: 29/10/2009
*/

typedef struct box
{
	float alpha, consolidation, reward, sensibility;
	int  size_of_dims[MAXIMUM_NUMBER_OF_DIMS], step_of_dims[MAXIMUM_NUMBER_OF_DIMS];
	type_neurone *neurons_of_reward , *neurons, *neurons_of_sensibility, *neurons_of_alpha;
	type_neurone *neurons_of_sensation;
}Box;


static int link_start_with_name(type_liaison *link, const char *name)
{
	if (strncmp(link->nom, name, strlen(name)) == 0) return 1;
	else return 0;
}

static float link_try_to_get_float_of_option(type_liaison *link,const char *option)
{
	char text_of_option[MAXIMUM_SIZE_OF_OPTION];
	int success;

	success = prom_getopt(link->nom, option, text_of_option);
	if (success == 1) EXIT_ON_ERROR("The content of option %s in link %s is not valid.",  option, link->nom);
	else if (success == 0) 	return -1.0;

	return atof(text_of_option);
}

/*
static float link_get_float_of_option(type_liaison *link, const char *option)
{
	float result;

	result = link_try_to_get_float_of_option(link, option);
	if (result < 0.0) EXIT_ON_ERROR("Option %s is not defined in link %s.",  option, link->nom);

	return result;
}

static int link_get_int_of_option(type_liaison *link, char *option)
{
	char text_of_option[MAXIMUM_SIZE_OF_OPTION];
	int success;

	success = prom_getopt(link->nom, option, text_of_option);
	if (success == 0) EXIT_ON_ERROR("Option %s is not defined in link %s.", option, link->nom);
	else if (success == 1) EXIT_ON_ERROR("The content of option %s in link %s is not valid.", option, link->nom);

	return atoi(text_of_option);
}
*/

void new_perceive(int index_of_group)
{
   int number_of_links, index_of_link;
   type_liaison *link;
   type_groupe *group;
   Box *box;

   (void) box; // (unused)

   group = &def_groupe[index_of_group];
	box = ALLOCATION(Box);
	group->data = box;

	box->neurons = &neurone[group->premier_ele];

	box->neurons_of_reward = NULL;
	box->neurons_of_sensation = NULL;

   box->neurons_of_sensibility = NULL;
   box->neurons_of_alpha = NULL;

   box->sensibility = -1;
   box->alpha = -1;

   for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
   {
   	link = &liaison[index_of_link];
  		if (link_start_with_name(link, "reward"))
      {
         box->neurons_of_reward = &neurone[def_groupe[link->depart].premier_ele];

         box->sensibility = link_try_to_get_float_of_option(link, "sensibility=");
         box->alpha = link_try_to_get_float_of_option(link, "alpha=");

         box->size_of_dims[0] = def_groupe[link->depart].taillex;
         box->size_of_dims[1] = def_groupe[link->depart].tailley;
      }
      else if (link_start_with_name(link, "sensation"))
      {
         box->neurons_of_sensation = &neurone[def_groupe[link->depart].premier_ele];

         box->alpha = link_try_to_get_float_of_option(link, "alpha=");

         box->size_of_dims[0] = def_groupe[link->depart].taillex;
         box->size_of_dims[1] = def_groupe[link->depart].tailley;
      }
      else if (link_start_with_name(link, "sensibility"))
      {
         box->neurons_of_sensibility = &neurone[def_groupe[link->depart].premier_ele];
      }
      else if (link_start_with_name(link, "alpha"))
      {
         box->neurons_of_alpha = &neurone[def_groupe[link->depart].premier_ele];
      }
      else if (!link_start_with_name(link, "sync")) EXIT_ON_ERROR("Group %s: the name of link '%s' is unknown, the possibilities are: 'reward', 'sensibility', 'alpha' and 'sync'.", group->no_name, link->nom);

      box->step_of_dims[0] = 1;
      box->step_of_dims[1] = box->size_of_dims[0] * box->step_of_dims[0];
      box->consolidation = 0;
 	}
 	if (box->sensibility < 0) box->sensibility = 0;
      else if ( box->neurons_of_sensibility != NULL) EXIT_ON_ERROR("You cannot have a link sensibility and a parameter sensibility");

   if (box->alpha < 0) box->alpha = 0;
      else if (box->neurons_of_alpha != NULL) EXIT_ON_ERROR("You cannot have a link alpha and a parameter alpha");

}


void function_perceive(int index_of_group)
{
	Box *box;
	float eta, alpha, sensibility, reward, focus, desired_i =0, desired_j = 0, consolidation, s, s_;

   int i, j, j_step;
   type_groupe *group;

   group = &def_groupe[index_of_group];
   box = (Box*)group->data;

   j_step = box->step_of_dims[1];

   if ( box->neurons_of_sensibility == NULL) sensibility = box->sensibility;
   else sensibility = box->neurons_of_sensibility[0].s1;

   if ( box->neurons_of_alpha == NULL) alpha = box->alpha;
   else alpha = box->neurons_of_alpha[0].s1;

	/** On calcule pour la dimension 0 (x)  et 1 (y) */

	consolidation = box->consolidation*alpha;


   if (box->neurons_of_sensation == NULL)
   {


      for (j = 0; j < box->size_of_dims[1]; j++)
      {
         for (i = 0; i < box->size_of_dims[0]; i++)
         {
            reward = box->neurons_of_reward[i + j*j_step].s1;

            focus = exp(reward*sensibility);
            consolidation = consolidation + focus;
            eta = focus / consolidation;

            desired_i = desired_i + eta *(i - desired_i);
            desired_j = desired_j + eta * (j - desired_j);
         }
      }
     	box->neurons[0].s1 = desired_i/box->size_of_dims[0]; /** normalisation */
      box->neurons[0].s = box->neurons[0].s1;
      box->neurons[0].s2 = box->neurons[0].s1;

      box->neurons[1].s1 = desired_j/box->size_of_dims[1]; /** normalisation */
      box->neurons[1].s = box->neurons[1].s1;
      box->neurons[1].s2 = box->neurons[1].s1;

   }
   else if (box->neurons_of_reward == NULL)
   {
      focus = 1;
      consolidation = consolidation + focus;
      eta = focus / consolidation;

      for (j = 0; j < box->size_of_dims[1]; j++)
      {
         for (i = 0; i < box->size_of_dims[0]; i++)
         {
            s = box->neurons_of_sensation[i + j*j_step].s1;

            s_ =  box->neurons[i + j*j_step].s1;

            s_= s_ + eta * (s - s_);

            box->neurons[i + j*j_step].s1 = s_;
            box->neurons[i + j*j_step].s = s_;
            box->neurons[i + j*j_step].s2 = s_;
         }
      }
   }
   else EXIT_ON_ERROR("sensation and reward are used.");
	box->consolidation = consolidation;

}


void destroy_perceive(int index_of_group)
{
	Box *box;
	type_groupe *group;

	(void) box; // (unused)

	group = &def_groupe[index_of_group];
    box = (Box*)group->data;

    free(def_groupe[index_of_group].data);
}

