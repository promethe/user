/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_modulo.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>

void function_modulo(int gpe)
{
    int i, deb, nbre, nbre2;
    int increment;

    float temp;
    float modulo_x;
    type_coeff *coeff;

    modulo_x = def_groupe[gpe].seuil;
    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
    nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
    increment = nbre / nbre2;
    if(modulo_x<=0.)
    {
	printf("modulo 0 imp dans %s \n",__FUNCTION__);
	exit(0);
    }

	dprints("enter %s\n",__FUNCTION__);


    for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
    {
        temp = 0.;

        coeff = neurone[i].coeff;

        while (coeff != NULL)
        {
            temp = temp + coeff->val * neurone[coeff->entree].s1;
            coeff = coeff->s;   /* Lien suivant */
        }

        if (temp < 0.)         temp = modulo_x + (temp - modulo_x* ((int)(temp / modulo_x))); /* temp = temp + modulo_x; */

        if (temp >= modulo_x)  temp = temp - modulo_x* ((int)(temp / modulo_x));  /*temp = temp - modulo_x;*/

        neurone[i].s = temp;
        neurone[i].s1 = temp;
        neurone[i].s2 = temp;

        /* printf("function_modulo_x neurone[%d].s=%f \n",i,neurone[i].s); */
    }

    dprints("fin %s\n",__FUNCTION__);

}
