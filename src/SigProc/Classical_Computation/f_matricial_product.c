/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 * @ingroup libSigProc
 * @defgroup f_matricial_product f_matricial_product
 *
 * @author A. Blanchard
 * @date 07/2009
 *
 * @brief Compute a matricial product
 *
 * \section Description
 * Realize the product of A x B where A and B are input links. The input S1 is used for the calculation.
 * @todo Optimize the code. Not suitable for big groups.
 *
 * \section Options
 * - A First operand of the product
 * - B Second operand of the product
 * */


#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct matricial_product
{
    type_groupe *A_group, *B_group;
    int number_of_lines, number_of_columns, n;

} type_of_matricial_product;

void new_matricial_product(int index_of_group)
{
    int number_of_links, index_of_link;
    type_liaison *link;
    type_groupe *group, *A_group = NULL, *B_group = NULL;
    type_of_matricial_product *matricial_product;

    group = &def_groupe[index_of_group];

    for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
   	{
   		link = &liaison[index_of_link];
   		if (strcmp(link->nom, "A")==0) A_group = &def_groupe[link->depart];
   		else if (strcmp(link->nom, "B")==0) B_group = &def_groupe[link->depart];
   		/*else if (strstr(link->nom, "sync")!=NULL) EXIT_ON_ERROR("Group: %s, type of link unknowm: %s. The possibilities are : 'A', 'B' and 'sync'.", group->no_name, link->nom);
   	*/}

   	if (A_group ==NULL  || B_group ==NULL) EXIT_ON_ERROR("There is no input link A or B for the group %d", index_of_group);

   if (A_group->nbre != A_group->taillex * A_group->tailley) EXIT_ON_ERROR("In A group, macro neurons are not managed");
   if (B_group->nbre != B_group->taillex * B_group->tailley) EXIT_ON_ERROR("In B group, macro neurons are not managed");

   /* Check the compatibility of the sizes */
   if (group->tailley != A_group->tailley) EXIT_ON_ERROR("Number of lines %d of A group is different from the group named: '%s'.", A_group->tailley, group->no_name);
   if (group->taillex != B_group->taillex) EXIT_ON_ERROR("Number of columns %d of B group is different from current group named: '%s'.", B_group->taillex, group->no_name);
   if (A_group->taillex != B_group->tailley) EXIT_ON_ERROR("The number of columns of A group is different the number of lines of B group in the group with name: '%s'.", group->no_name);


    matricial_product = ALLOCATION(type_of_matricial_product);
    matricial_product->number_of_lines = group->tailley;
    matricial_product->number_of_columns = group->taillex;
    matricial_product->n = A_group->taillex;
    matricial_product->A_group = A_group;
    matricial_product->B_group = B_group;

    group->data = (void*)matricial_product;
}

void function_matricial_product(int index_of_group)
{
    int line, column, r;
    type_groupe *A_group, *B_group, *group;

    float sum;

    type_of_matricial_product *matricial_product;

    group = &def_groupe[index_of_group];
    matricial_product = group->data;
    A_group = matricial_product->A_group;
    B_group = matricial_product->B_group;

    for (line = 0; line < matricial_product->number_of_lines; line ++)
    {
        for (column = 0; column < matricial_product->number_of_columns; column ++)
        {
            sum = 0;

            for (r = 0; r < matricial_product->n; r++)
            {
                    sum += neurone[A_group->premier_ele + r + line * A_group->taillex].s1 * neurone[B_group->premier_ele + column + r * B_group->taillex].s1;
            }
            neurone[group->premier_ele + column + line * group->taillex].s = sum;
            neurone[group->premier_ele + column + line * group->taillex].s1 = sum;
            neurone[group->premier_ele + column + line * group->taillex].s2 = sum;
        }
    }
}

void destroy_matricial_product(int index_of_group)
{
    free(def_groupe[index_of_group].data);
}

