/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_distance f_distance
ingroup libSigProc
To be completed
\file
ingroup f_distance
**/

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <stdlib.h>
typedef struct My_Data_f_distance
{
    int GpeRef;
    int GpeInput;
} My_Data_f_distance;


void function_distance(int gpe)
{
    My_Data_f_distance *my_data;
    int i = 0, l, GpeInput = -1, GpeRef = -1;
    float maxin, maxref;
    int imax_in=0, imax_ref=0;
    float temp;

    if (def_groupe[gpe].data == NULL)
    {
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "input") == 0)
                GpeInput = liaison[l].depart;

            if (strcmp(liaison[l].nom, "ref") == 0)
                GpeRef = liaison[l].depart;

            l = find_input_link(gpe, i);
            i++;
        }
        if (def_groupe[GpeInput].nbre != def_groupe[GpeInput].nbre)
        {
            printf
                ("eeror %s: les groupe d entree doivent avoir le meme nombre de neurone\n",
                 __FUNCTION__);
            exit(0);
        }
        my_data = (My_Data_f_distance *) malloc(sizeof(My_Data_f_distance));
        if (my_data == NULL)
        {
            printf("eeror malloc %s\n", __FUNCTION__);
            exit(0);
        }
        my_data->GpeInput = GpeInput;
        my_data->GpeRef = GpeRef;
        def_groupe[gpe].data = (My_Data_f_distance *) my_data;
    }
    else
    {
        my_data = def_groupe[gpe].data;
        GpeInput = my_data->GpeInput;
        GpeRef = my_data->GpeRef;
    }
    maxin = -1000.;
    maxref = -1000.;
    for (i = 0; i < def_groupe[GpeInput].nbre; i++)
    {
        if (maxin < neurone[def_groupe[GpeInput].premier_ele + i].s1)
        {
            maxin = neurone[def_groupe[GpeInput].premier_ele + i].s1;
            imax_in = i;
        }
        if (maxref < neurone[def_groupe[GpeRef].premier_ele + i].s1)
        {
            maxref = neurone[def_groupe[GpeRef].premier_ele + i].s1;
            imax_ref = i;
        }
    }

    temp = (imax_in - imax_ref) / (float) def_groupe[GpeInput].nbre;

    neurone[def_groupe[gpe].premier_ele].s = temp;
    neurone[def_groupe[gpe].premier_ele].s1 =
        neurone[def_groupe[gpe].premier_ele].s;
    neurone[def_groupe[gpe].premier_ele].s2 =
        neurone[def_groupe[gpe].premier_ele].s;
}
