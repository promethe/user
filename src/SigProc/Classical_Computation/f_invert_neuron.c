/**
\defgroup f_invert_neurons f_invert_neurons
\ingroup libSigProc

\brief output =  1/(input)

description :

Calcule le potentiel d'entree de maniere classique ( \f$ sum w_i x_i \f$ ) avant
de prendre l'invert.
 Le potentiel de sortie du neurone est donc \f$ frac{1}{ sum w_i x_i} \f$ .

author = A. de Rengerve
date = 12/01/2010

To be completed

\file
\ingroup f_invert_neurons

author = "A. de Rengerve"
date = "12/01/2010"
description = "specific file creation"


*/

#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

#include <net_message_debug_dist.h>


void function_invert_neuron(int gpe)
{
   int i, deb, nbre, nbre2;
   int increment;

   float temp;

   type_coeff *coeff;

   dprints("entree dans invert_neuron (%d)\n",gpe);

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;
   nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
   increment = nbre / nbre2;

   for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
   {
      temp = 0.;

      coeff = neurone[i].coeff;

      while (coeff != NULL)
      {
         temp = temp + coeff->val * neurone[coeff->entree].s1;
         coeff = coeff->s;   /* Lien suivant */
      }
      if (isequal(temp, 0.))
      {
         temp = 1;
      }
      else
      {
         temp = 0;
      }

      neurone[i].s = temp;
      neurone[i].s1 = neurone[i].s2 = temp;

      /*
      if(neurone[i].s2 >1.)
        neurone[i].s1 = 1.;
      else if(neurone[i].s2 <0.)
        neurone[i].s1 = 0.;
      else
        neurone[i].s1 = neurone[i].s2;
      */

      /* printf("function_invert_neuron neurone[%d].s=%f \n",i,neurone[i].s); */
   }
   dprints("sortie de invert_neuron (%d)\n",gpe);
}

