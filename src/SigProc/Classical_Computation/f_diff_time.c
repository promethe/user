/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup f_diff_time f_diff_time
 \ingroup libSigProc

 \brief output =  (a-(a-1))/Delta(t)

 description :
 Calcul (a-(a-1))/Delta(t), avec Delta(t) en usec

 author = Nils Beaussé
 date = 12/01/2010

 To be completed

 \file
 \ingroup f_diff_time

 author = "Nils Beaussé"
 date = "11/04/2014"
 description = "compute (a-(a-1))/Dt, Dt en usec"


 */

#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

#include <net_message_debug_dist.h>



typedef struct data_a_sur_b
{
  int no_group_a;
  double norme_grp_a;
  struct timeval time_prece;
  int premier_passage;
} data_diff_time;

void new_diff_time(int gpe)
{
  data_diff_time *my_data = NULL;

  if (def_groupe[gpe].data == NULL)
  {
    my_data = ALLOCATION(data_diff_time);
    my_data->premier_passage = 1;
    def_groupe[gpe].data = (void*) my_data;
  }

}

void function_diff_time(int gpe)
{
  struct timeval time_actu, time_prece;
  double Delta_signal = 0.0;
  long int Delta_time = 0.0;
  long double resultat = 0.0;
  float signal_actu;
  int i;
  type_coeff* coeff = NULL;

  data_diff_time *my_data = (data_diff_time *) def_groupe[gpe].data;
  gettimeofday(&time_actu, (void *) NULL);

  /* Premier passage dans la fonction*/
  if (my_data->premier_passage == 1)
  {
    my_data->time_prece = time_actu;
  }

  time_prece = my_data->time_prece;
  Delta_time = (time_actu.tv_sec - time_prece.tv_sec) * 1000000 + (time_actu.tv_usec - time_prece.tv_usec);

  for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
  {
    coeff = neurone[i].coeff;
    signal_actu = 0.0;

    while (coeff != NULL)
    {
      signal_actu = signal_actu + coeff->val * neurone[coeff->entree].s1;
      coeff = coeff->s;
    }

    Delta_signal = signal_actu - neurone[i].d;

    my_data->time_prece = time_actu; //temps actu devient temps precedent.
    neurone[i].d = signal_actu; // signal actu devient signal precedent.


    if (my_data->premier_passage == 1)
    {
     neurone[i].s1 = 0.0;
    }
    else
    {
      if(Delta_time!=0) resultat = (long double) Delta_signal / (long double) Delta_time;
      else resultat=0.0;
      neurone[i].s1 = (float) resultat;
    }
  }

  /* Premier passage dans la fonction*/

  if (my_data->premier_passage == 1)
  {
    my_data->premier_passage = 0;
  }

  dprints("sortie de diff_time (%d)\n", gpe);
}

void destroy_diff_time(int gpe)
{
  dprints("destroy_diff_time(%s): Entering function\n", def_groupe[gpe].no_name);
  if (def_groupe[gpe].data != NULL)
  {
    free(((data_diff_time *) def_groupe[gpe].data));
    def_groupe[gpe].data = NULL;
  }
  dprints("destroy_diff_time(%s): Leaving function\n", def_groupe[gpe].no_name);
}
