/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_double_threshold f_double_threshold
 \ingroup libSigProc
 To be completed. This is a simple function which compares the input data with the two given threshold limits (lower and upper threshold). If the input is within threshold limits, the output will be 1, otherwise 0.
 \file
 \ingroup f_double_threshold
 */

/* ------------------------------------*/

/* #define DEBUG */

#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

typedef struct MyData_f_double_threshold {
  float max;
  float min;
  int linear;
  type_groupe *input_group;
  type_groupe *link;
} MyData_f_double_threshold;

void new_double_threshold(int Gpe)
{
  float max;
  float min;
  int linear = 0;
  int i, l,retour;
  MyData_f_double_threshold *my_data;

  my_data = (MyData_f_double_threshold *) malloc(sizeof(MyData_f_double_threshold));
  if (my_data == NULL)
  {
    EXIT_ON_ERROR("erreur malloc dans f_double_threshold\n");
  }
  /* Recherche des deux gpes d'entreee  */
  i = 0;
  l = find_input_link(Gpe, i);
  my_data->input_group = &def_groupe[liaison[l].depart];
  while (l != -1)
  {
    if (prom_getopt_int(liaison[l].nom, "-linear", &retour) > 0) linear = 1;
    if (prom_getopt_float(liaison[l].nom, "-max", &max) == 1) EXIT_ON_ERROR("Option -max on");
    if (prom_getopt_float(liaison[l].nom, "-min", &min) == 1) EXIT_ON_ERROR("Option -min on");
    i++;
    l = find_input_link(Gpe, i);
  }
  my_data->max = max;
  my_data->min = min;
  my_data->linear = linear;
  def_groupe[Gpe].data=my_data;
}

void function_double_threshold(int Gpe)
{
  float max;
  float min;
  int linear = 0;
  int i, deb;
  MyData_f_double_threshold *my_data=NULL;

  deb = def_groupe[Gpe].premier_ele;

  my_data = (MyData_f_double_threshold *) (def_groupe[Gpe].data);

  max = my_data->max;
  min = my_data->min;
  linear = my_data->linear;

  for (i = 0; i < def_groupe[Gpe].nbre; i++)
  {
    if (linear == 0)
    {
      if ((neurone[my_data->input_group->premier_ele + i].s1 > min) && (neurone[my_data->input_group->premier_ele + i].s1 < max))
      {
        neurone[deb + i].s1 = 1;
      }
      else
      {
        neurone[deb + i].s1 = 0;
      }
    }
    else
    {
      if ((neurone[my_data->input_group->premier_ele + i].s1 > min) && (neurone[my_data->input_group->premier_ele + i].s1 < max))
      {
        neurone[deb + i].s1 = neurone[my_data->input_group->premier_ele + i].s1;
      }
      else if (neurone[my_data->input_group->premier_ele + i].s1 >= max)
      {
        neurone[deb + i].s1 = max;
      }
      else if (neurone[my_data->input_group->premier_ele + i].s1 <= min)
      {
        neurone[deb + i].s1 = min;
      }
    }
  }
}


void destroy_double_threshold(int Gpe)
{
  MyData_f_double_threshold *my_data;
  my_data = (MyData_f_double_threshold *) (def_groupe[Gpe].data);
  if (my_data != NULL)
    {
      free(my_data);
      def_groupe[Gpe].data=NULL;
    }
}

