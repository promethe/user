/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_d_phi  f_d_phi
ingroup libSigProc

\details
\section link
name = "from"
type_input_group = "Image of real point"
description = "where are the data? in the image to convert"

To be completed

\file
ingroup f_d_phi
*/

#include <libx.h>
#include <stdlib.h>

void function_d_phi(int Gpe)
{
    int taille_entree, debut_entre, debut, taille;
    int groupe_entree;
    int i, j, k;
    /*float FacteurCedric = 15.0; */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        def_groupe[Gpe].ext = (void *) malloc(sizeof(int));
        groupe_entree = -1;
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                groupe_entree = liaison[i].depart;
                break;
            }
        if (groupe_entree < 0)
            return;

        *(int *) def_groupe[Gpe].ext = groupe_entree;
    }
    else
        groupe_entree = *(int *) def_groupe[Gpe].ext;


    taille_entree = def_groupe[groupe_entree].nbre;
    debut_entre = def_groupe[groupe_entree].premier_ele;

    debut = def_groupe[Gpe].premier_ele;
    taille = def_groupe[Gpe].nbre;

    if (taille != taille_entree)
    {
        fprintf(stderr,
                "f_d_phi: Le groupe d'entree n'a pas la bonne taille\n");
        exit(1);
    }


    for (i = debut, j = debut_entre, k = 0; k < taille - 1; i++, j++, k++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 =
            /*FacteurCedric */
            (neurone[j + 1].s1 - neurone[j].s1);
    neurone[i].s = neurone[i].s1 = neurone[i].s2 =
        neurone[j].s1 - neurone[debut_entre].s1;

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Time in function_d_phi\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}
