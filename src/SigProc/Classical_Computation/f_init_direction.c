/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_init_direction.c
\brief 

Author: BEAUSS������ Nils
Created: 10/11/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
Fonction qui init les liens de deux neuronnes en fonctions d'un tableau de neuronne en entrée de sorte que la sortie de ces neuronnes soit interprété comme une force XY
exemple : Activation de la neuronne en haut à gauche = aller en haut à gauche -> Ax = -1 Ay=-1
Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

/*#define DEBUG 1*/

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <NN_Core/Modulation/calcule_neuromodulation.h>
#include <NN_Core/rien.h>

float calcule_produit2(int i)
{
    type_coeff *coeff;
    float sortie;

    sortie = 0.;
    coeff = neurone[i].coeff;
    if (!coeff_is_valid(coeff))           /* c'est entree */
    {
        neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.;
        return (0.);
    }

    while (coeff_is_valid(coeff))
    {
        sortie = sortie + coeff->val * neurone[coeff->entree].s1;

        coeff = coeff->s;
    }
    return(sortie);
}

void new_init_direction(int gpe)
{
   int premier_ele=def_groupe[gpe].premier_ele;
   type_coeff* s;
   int premier_ele_entree=def_groupe[neurone[(neurone[premier_ele].coeff->entree)].groupe].premier_ele;

     s=neurone[premier_ele].coeff;
      while (s!=NULL)
      {
        if (((s->entree-premier_ele_entree)%3) == 0)
        {
          s->val=-1;
        }
        if (((s->entree-premier_ele_entree)%3) == 1)
        {
          s->val=0;
        }
        if (((s->entree-premier_ele_entree)%3) == 2)
        {
          s->val=1;
        }
        s=s->s;
      }

      s=neurone[premier_ele+1].coeff;
      while (s!=NULL)
      {
        if ((s->entree-premier_ele_entree)<3)
        {
          s->val=1;
        }
        else if ((s->entree-premier_ele_entree)>=3 && (s->entree-premier_ele_entree)<6)
        {
          s->val=0;
        }
        else
        {
          s->val=-1;
        }
        s=s->s;
      }
}

void function_init_direction(int gpe)
{
   int first= def_groupe[gpe].premier_ele;

	   neurone[first].s1=neurone[first].s=neurone[first].s2= calcule_produit2(first);
	   neurone[first+1].s1=neurone[first+1].s=neurone[first+1].s2= calcule_produit2(first+1);
}



void destroy_init_direction(int gpe)
{
(void)gpe;

}
