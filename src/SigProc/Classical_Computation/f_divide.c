/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\defgroup f_divide f_divide
\ingroup libSigProc

\brief output =  a/b

description :
Calcul a/b, permet de s'affranchir de f_inverse_neuron pour calculer a/b (1/b * a) dont la valeur en sortie (1/b) peut depasser ce qui est autorise pour un float alors que
a/b ne l'aurais pas forcement depasse. Ceci est notemment le cas pour l'ensemble des calculs de vitesse en neuronnal qui etait donc auparavant faux.
Il est conseillé d'utiliser la nouvelle fonction f_diff_time pour calculer en interne de façon plus précise une variation temporelle

author = Nils Beaussé
date = 12/01/2010

To be completed

\file
\ingroup f_divide

author = "Nils Beaussé"
date = "11/04/2014"
description = "compute a/b"


 */

#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

#include <net_message_debug_dist.h>



typedef struct data_a_sur_b
{
   int no_group_deno;
   int no_group_nume;
   double norme_grp_deno;
   double norme_grp_nume;
} data_a_sur_b;

void new_divide(int gpe)
{
   int index=0,link=0,no_group_nume=-1,no_group_deno=-1;
   double norme_grp_deno=0.0,norme_grp_nume=0.0;
   char string[255];
   data_a_sur_b *my_data = NULL;

   if (def_groupe[gpe].data == NULL)
   {
      index = 0;
      link = find_input_link(gpe, index);
      // Pas propre, changer pour une prise en compte de parametre passer directement dans la config de la boite.
      while (link != -1)
      {
         if (prom_getopt(liaison[link].nom, "deno", string) >= 1)
         {
            no_group_deno=liaison[link].depart;
            norme_grp_deno=liaison[link].norme;
         }
         if (prom_getopt(liaison[link].nom, "nume", string) >= 1)
         {
            no_group_nume=liaison[link].depart;
            norme_grp_nume=liaison[link].norme;
         }
         index++;
         link = find_input_link(gpe, index);
      }

      if (no_group_nume==-1 || no_group_deno==-1) EXIT_ON_ERROR("En entrée de f_divide, pas de groupe 'deno' ou 'nume'");


      if ( (def_groupe[no_group_nume].nbre != def_groupe[no_group_deno].nbre)
            || (def_groupe[no_group_nume].nbre != def_groupe[gpe].nbre) )
         EXIT_ON_ERROR("Groupes 'deno', 'nume' et f_divide n'ont pas le même nombre de neurones");

      my_data = ALLOCATION(data_a_sur_b);
      my_data->no_group_deno= no_group_deno;
      my_data->no_group_nume= no_group_nume;
      my_data->norme_grp_deno= norme_grp_deno;
      my_data->norme_grp_nume= norme_grp_nume;
      def_groupe[gpe].data = (void*)my_data;
   }

}

void function_divide(int gpe)
{  
   double a=0.0,b=0.0,/*linka=0.0,linkb=0.0,*/resultat=0.0;
   int i;

   data_a_sur_b *my_data = (data_a_sur_b *) def_groupe[gpe].data;

   for( i = 0; i < def_groupe[gpe].nbre; i++)
   {
      a=(double)(neurone[def_groupe[my_data->no_group_nume].premier_ele + i].s1);
      b=(double)(neurone[def_groupe[my_data->no_group_deno].premier_ele + i].s1);

      resultat = a/b;

      neurone[def_groupe[gpe].premier_ele + i].s=neurone[def_groupe[gpe].premier_ele + i].s1 = neurone[def_groupe[gpe].premier_ele + i].s2 = (float)resultat;
   }

   dprints("sortie de inverse_neuron (%d)\n",gpe);   
}

void destroy_divide(int gpe)
{
   dprints("destroy_divide(%s): Entering function\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_a_sur_b *) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_divide(%s): Leaving function\n", def_groupe[gpe].no_name);
}
