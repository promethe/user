/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\defgroup f_sum_no_seuil f_sum_no_seuil
\ingroup libSigProc
To be completed
\file
\ingroup f_sum_no_seuil

    -nXXX : Permet d'avoir en sortie la moyenne des XXX itérations précédentes (utilisé pour l'habituation pour la reconnaissance d'objet).


 */
/*#define DEBUG*/
#include <libx.h>


typedef struct data_sum_no_seuil
{
   int deb_gpe;
   int nbre_gpe;
   int increment_gpe;
   int iteAccu;
   int gpeAccu;
   int cptAccu;
   int gpeRaz;
   int copie_d;
   float** tabAccu;
} Data_sum_no_seuil;

// TODO : moderniser et simplifier
void function_sum_no_seuil(int gpe)
{
   int i = 0, j=0, l, deb = 0, nbre = 0, gpeRaz = -1;
   int increment = 1, nonGere = 0, iteAccu = -1, gpeAccu = -1, inc = 0;
   float** tabAccu = NULL;
   float temp3;
   int copie_d=0;

   float temp1, temp0, temp2;
   char param[255];

   Data_sum_no_seuil *myData = NULL;

   type_coeff *coeff;


   dprints("Enter in f_sum_no_seuil (%s)\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {
      i = 0;
      nonGere = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {
         if(prom_getopt(liaison[l].nom, "-n", param) == 2)
         {
            iteAccu = atoi(param);
            iteAccu = ((iteAccu > 1) ? iteAccu : 1);
            gpeAccu = liaison[l].depart;
            dprints("Iteration accumule = %d, liaisonAccu = %d, gpe = %d\n", iteAccu, l, gpe);
            nonGere++;
         }
         if(prom_getopt(liaison[l].nom, "-d", NULL) == 1)
         {
           // on permet la copie de d
           copie_d=1;
         }
         if(prom_getopt(liaison[l].nom, "-raz", NULL) == 1)
         {
            gpeRaz = liaison[l].depart ;
            if( def_groupe[gpeRaz].nbre > 1 )
               PRINT_WARNING("GROUP %s (num coeos %s) : Only the first neuron of raz sensor group %s will be used by f_sum_no_seuil", __FUNCTION__, def_groupe[gpe].no_name, def_groupe[gpeRaz].no_name);
         }
         i++;
         l = find_input_link(gpe, i);
      }

      if( nonGere > 1 )
      {
         EXIT_ON_ERROR("%s : Ne gere pas 2 lien avec -n !\n", __FUNCTION__);
      }

      if(iteAccu != -1)
      {
         tabAccu = MANY_ALLOCATIONS( (def_groupe[gpe].nbre), float* );
         for(j=0; j<def_groupe[gpe].nbre ; j++)
            tabAccu[j] = MANY_ALLOCATIONS(iteAccu, float);
      }

      /* Allocation */
      myData = ALLOCATION(Data_sum_no_seuil);

      myData->deb_gpe = def_groupe[gpe].premier_ele;
      myData->nbre_gpe = def_groupe[gpe].nbre;
      myData->increment_gpe = def_groupe[gpe].nbre / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);
      myData->iteAccu = iteAccu;
      myData->gpeAccu = gpeAccu;
      myData->cptAccu = 0;
      myData->tabAccu = tabAccu;
      myData->gpeRaz = gpeRaz;
      myData->copie_d = copie_d;
      def_groupe[gpe].data = (void *)myData;
   }
   else
   {
      myData = (Data_sum_no_seuil *) def_groupe[gpe].data;
      iteAccu = myData->iteAccu;
      gpeAccu = myData->gpeAccu;
      gpeRaz = myData->gpeRaz;
      copie_d = myData->copie_d;
   }
   deb = myData->deb_gpe;
   nbre = myData->nbre_gpe;
   increment = myData->increment_gpe;


   for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
   {
      if( gpeRaz > 0 && (neurone[def_groupe[gpeRaz].premier_ele].s1 > 0.5) )
      {
         neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
      }
      else
      {

         temp0 = 0.;
         temp1 = 0.;
         temp2 = 0.;
         if(copie_d==1) temp3 = 0;
         coeff = neurone[i].coeff;

         while (coeff != NULL)
         {
            temp0 = temp0 + coeff->val * neurone[coeff->entree].s;
            temp1 = temp1 + coeff->val * neurone[coeff->entree].s1;
            temp2 = temp2 + coeff->val * neurone[coeff->entree].s2;
            if(copie_d==1)
            {
              temp3 = temp3 + coeff->val * neurone[coeff->entree].d;
            }

            coeff = coeff->s;   /* Lien suivant */
         }

         if( gpeAccu != -1 )
         {
            myData->tabAccu[inc][myData->cptAccu] = neurone[def_groupe[gpeAccu].premier_ele+(inc%(def_groupe[gpeAccu].nbre))].s1;
            for(j=0; j<iteAccu ; j++)
               temp1 += myData->tabAccu[inc][j];
            temp1 = temp1 / (float)iteAccu;
            inc++;
         }

         neurone[i].s =  temp0;
         neurone[i].s1 = temp1;
         neurone[i].s2 = temp2;
         if(copie_d==1)
         {
           neurone[i].d=temp3;
         }
      }
   }

   if( gpeAccu != -1 )  myData->cptAccu = (myData->cptAccu+1) % iteAccu;
}
