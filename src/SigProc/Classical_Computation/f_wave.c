/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \ingroup libSigProc
 \defgroup f_wave f_wave
 \author A.Pitti
 \date 14/02/2012
 \brief 	wave generator
 \section Modified
 - author	Nils Beaussé
 - description   Rationalisation et modernisation de l'écriture de la fonction
 - date	24/02/2015

 \section Description

 Cette fonction genere une fonction sinus, tres pratique pour simuler un signal d'entree!
 L'amplitude ainsi que la fréquence et la phase peuvent etre précisés par un groupe en entrée (lien algorithmique avec "amp" ou "freq" ou "phi")
 On peut également passer ces paramètres à l'aide des options -f[freq] et -a[amp] -p[phi]
 Pour plusieurs neurones, chaque neurone subit un dephasage dependant de sa position.
 \file
 \ingroup f_wave
 **/

#define ANGLE_RADIANS 0
#define ANGLE_DEGREES 1

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#define MAXIMUM_SIZE_OF_OPTION 128

typedef struct data_wave {
  struct timeval time_depart;
  float freq;
  float phi;
  float amp;
  int freq_gpe;
  int amp_gpe;
  int phi_gpe;
  float time;
  float step;
  int rt;
} data_wave;

void new_wave(int gpe)
{
  int index_of_link, success, number_of_links;
  char buffer[MAXIMUM_SIZE_OF_OPTION];
  type_liaison *link = NULL;
  data_wave *my_data = NULL;

  my_data = ALLOCATION(data_wave);

  my_data->step = 0.01;
  my_data->time = 0;
  my_data->freq = 1.0;
  my_data->phi = 0;
  my_data->amp = 1;
  my_data->freq_gpe = -1;
  my_data->amp_gpe = -1;
  my_data->phi_gpe = -1;
  my_data->rt = 0;

  for (number_of_links = 0; (index_of_link = find_input_link(gpe, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];
    if (strncmp(link->nom, "phi", 3*sizeof(char)) == 0)
    {
      my_data->phi_gpe = link->depart;
    }
    if (strncmp(link->nom, "amp", 3*sizeof(char)) == 0)
    {
      my_data->amp_gpe = link->depart;
    }
    if (strncmp(link->nom, "freq", 3*sizeof(char)) == 0)
    {
      my_data->freq_gpe = link->depart;
    }
  }
  // my_data->phi_gpe = trouver_entree(gpe, (char*) "phi");
  // my_data->freq_gpe = trouver_entree(gpe, (char*) "freq");
  // my_data->amp_gpe = trouver_entree(gpe, (char*) "amp");

  gettimeofday(&(my_data->time_depart), (void *) NULL);

  for (number_of_links = 0; (index_of_link = find_input_link(gpe, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];

    if (my_data->freq_gpe == -1)
    {
      success = prom_getopt_float(link->nom, "-f", &(my_data->freq));
      if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'f' is not valid on the link :'%s'.", gpe, link->nom);
    }

    if (my_data->amp_gpe == -1)
    {
      success = prom_getopt_float(link->nom, "-a", &(my_data->amp));
      if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'a' is not valid on the link :'%s'.", gpe, link->nom);
    }

    if (my_data->phi_gpe == -1)
    {
      success = prom_getopt_float(link->nom, "-p", &(my_data->phi));
      if (success == 1)
      {
        EXIT_ON_ERROR("Group %d, the parameter of 'p' is not valid on the link :'%s'.", gpe, link->nom);
      }
    }
    success = prom_getopt(link->nom, "rt", buffer);
    if (success > 0) my_data->rt = 1;
  }
  def_groupe[gpe].data = my_data;
}

void function_wave(int gpe)
{
  int i;
  int first;
  float amp;
  float freq;
  float phi;
  struct timeval time_actu;
  double time, time_depart;
  float potential = 0.0;
  data_wave *my_data = (data_wave *) def_groupe[gpe].data;

  if (my_data == NULL)
  {
    EXIT_ON_ERROR("Cannot retrieve data");
  }

  first = def_groupe[gpe].premier_ele;

  gettimeofday(&time_actu, (void *) NULL);
  //time_actu.tv_sec = (time_actu.tv_sec - my_data->time_depart.tv_sec);
  //time_actu.tv_usec = (time_actu.tv_usec - my_data->time_depart.tv_usec);

  if (my_data->rt)
  {
    time = (double) (time_actu.tv_sec) + (((double) (time_actu.tv_usec)) / ((double) (1000000)));
    time_depart = (double) (my_data->time_depart.tv_sec) + (((double) (my_data->time_depart.tv_usec)) / ((double) (1000000)));
    time = time - time_depart;
  }
  else
  {
    my_data->time += my_data->step;
    time = my_data->time;
  }

  if (my_data->freq_gpe != -1) freq = neurone[def_groupe[my_data->freq_gpe].premier_ele].s1;
  else freq = my_data->freq;
  if (my_data->amp_gpe != -1) amp = neurone[def_groupe[my_data->amp_gpe].premier_ele].s1;
  else amp = my_data->amp;
  if (my_data->phi_gpe != -1) phi = neurone[def_groupe[my_data->phi_gpe].premier_ele].s1;
  else phi = my_data->phi;

  for (i = 0; i < def_groupe[gpe].nbre; i++)
  {
    potential = (2 * M_PI * freq * time) + (2 * M_PI * ((float) i / def_groupe[gpe].nbre)) + phi;
    neurone[first + i].s1 = 0.5 * amp * sin(potential);
  }
}

void destroy_wave(int gpe)
{
  if (def_groupe[gpe].data != NULL)
  {
    free(((data_wave *) def_groupe[gpe].data));
    def_groupe[gpe].data = NULL;
  }
}
