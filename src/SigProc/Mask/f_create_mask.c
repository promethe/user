/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_create_mask f_create_mask
ingroup libSigProc

name = "f_create_mask"
comment = "useful fonction for popout package" >

To be completed

\file
ingroup f_create_mask
*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/prom_images_struct.h>

void function_create_mask(int Gpe)
{
    int n;
    /*int InputGpe; */
    float *masque, i, j, nx = -1.0, ny = -1.0, SigO = -1.0, SigD =
        -1.0, orientation = -1.0, dist = -1.0, pola = -1.0;
    /* rajout 24/11/2003 Olivier Ledoux */
    float teta = 0.0;


    char *ST, *chaine = NULL;


    if (def_groupe[Gpe].ext == NULL)
    {
#ifdef DEBUG
        printf("%s: alloc de mem pour le groupe\n", __FUNCTION__);
#endif
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s: ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }
        /* on cherche les groupes precedents */

        for (i = 0.; i < nbre_liaison; i++)
            if (liaison[(int) i].arrivee == Gpe)
            {
                /*InputGpe = liaison[i].depart; */
                chaine = liaison[(int) i].nom;
                break;
            }

        /* recuperation des parametres */
        /*chaine = liaison[i].nom; */
        ST = NULL;
        ST = strstr(chaine, "-X");
        if (ST != NULL)
            nx = atof(&ST[2]);

        ST = NULL;
        ST = strstr(chaine, "-Y");
        if (ST != NULL)
            ny = atof(&ST[2]);

        ST = NULL;
        ST = strstr(chaine, "-O");
        if (ST != NULL)
            orientation = atof(&ST[2]);

        ST = NULL;
        ST = strstr(chaine, "-D");
        if (ST != NULL)
            dist = atof(&ST[2]);

        ST = NULL;
        ST = strstr(chaine, "-SO");
        if (ST != NULL)
            SigO = atof(&ST[3]);

        ST = NULL;
        ST = strstr(chaine, "-SD");
        if (ST != NULL)
            SigD = atof(&ST[3]);

        ST = NULL;
        ST = strstr(chaine, "-P");
        if (ST != NULL)
            pola = atof(&ST[2]);

        /* rajout du 24/11/2003 Olivier Ledoux */
        ST = NULL;
        ST = strstr(chaine, "-T");
        if (ST != NULL)
            teta = atof(&ST[2]);

        if (isequal(SigD, -1) || isequal(SigO, -1) || isequal(dist, -1)
            || isequal(orientation, -1) || isequal(nx, -1) || isequal(ny, -1)
            || isequal(pola, -1))
        {
            printf("%s(groupe %d): erreur, manque au moins un parametre\n",
                   __FUNCTION__, Gpe);
            exit(0);
        }
#ifdef DEBUG
        printf
            ("%s(groupe %d): taille : %fx%f, orientation : %f, distance : %f, sigma orientation : %f, sigma distance : %f, pola : %f\n",
             __FUNCTION__, Gpe, nx, ny, orientation, dist, SigO, SigD, pola);
#endif
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = (int) nx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = (int) ny;
        n = (int) nx *ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        /* pour le filtre resultat */

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(n * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s: ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }
        masque =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];

        /* determination de la taille */
        /*longueur = (int) floor(max(2*dist+1.0,dist+2*SigmaDist+1.0)); */
        /* largeur = (int) floor(SigmaOrient*4+1); */
        /*printf("%s: dimension reelle du filtre : %dx%d\n",__FUNCTION__,longueur,largeur); */

        SigD = 2 * SigD * SigD;
        SigO = 2 * SigO * SigO;




        /* les filtres */
        if (isequal(orientation, 0.))
        {
            /* filtres horizontaux */
	  if (isequal(pola, 0.))
                for (i = 0.; i < ny; i++)
                    for (j = 0.; j < nx; j++)
                    {
                        /* horizontal droit */
                        masque[(int) (i * nx + j)] =
                            exp(-(j - nx / 2. + dist / 2.) *
                                (j - nx / 2. + dist / 2.) / SigD - (i -
                                                                    ny /
                                                                    2.) *
                                (i - ny / 2.) / SigO);
                    }
            else
                for (i = 0.; i < ny; i++)
                    for (j = 0.; j < nx; j++)
                    {
                        /* horizontal gauche */
                        masque[(int) (i * nx + j)] =
                            exp(-(j - nx / 2. - dist / 2.) *
                                (j - nx / 2. - dist / 2.) / SigD - (i -
                                                                    ny /
                                                                    2.) *
                                (i - ny / 2.) / SigO);
                    }
        }
        if (isequal(orientation, 1))
        {
            /* filtres verticaux */
	  if (isequal(pola, 0))
                for (i = 0.; i < ny; i++)
                    for (j = 0.; j < nx; j++)
                    {
                        /* vertical haut */
                        masque[(int) (i * nx + j)] =
                            exp(-(i - ny / 2. + dist / 2.) *
                                (i - ny / 2. + dist / 2.) / SigD - (j -
                                                                    nx /
                                                                    2.) *
                                (j - nx / 2.) / SigO);
                    }
            else
                for (i = 0.; i < ny; i++)
                    for (j = 0.; j < nx; j++)
                    {
                        /* vertical bas */
                        masque[(int) (i * nx + j)] =
                            exp(-(i - ny / 2. - dist / 2.) *
                                (i - ny / 2. - dist / 2.) / SigD - (j -
                                                                    nx /
                                                                    2.) *
                                (j - nx / 2.) / SigO);
                    }
        }
        if (isequal(orientation, 2))
        {
            /* filtres diagonaux */
            for (i = 0.; i < ny; i++)
                for (j = 0.; j < nx; j++)
                {
                    masque[(int) (i * nx + j)] =
                        exp(-(i - ny / 2. + cos(teta) * dist / 2.) *
                            (i - ny / 2. + cos(teta) * dist / 2.) / SigD -
                            (j - nx / 2. + sin(teta) * dist / 2.) * (j -
                                                                     nx / 2. +
                                                                     sin(teta)
                                                                     * dist /
                                                                     2.) /
                            SigO);
                }
        }

#ifdef DEBUG
#ifndef AVEUGLE
        affichage2(2, masque, (int) nx, (int) ny, 0, 0);
#endif
        if (orientation)
        {
            if (pola)
            {
                printf("filtre decale vertical bas\n");
            }
            else
            {
                printf("filtre decale vertical haut\n");
            }
        }
        else
        {
            if (pola)
            {
                printf("filtre decale horizontal gauche\n");
            }
            else
            {
                printf("filtre decale horizontal droit\n");
            }
        }
        /*printf("orientation : %d (0:horizontal,1:vertical) pola : %d (0/1 droite/gauche haut/bas)\n",(int)orientation,(int)pola); */
        /*getchar(); */
#endif


    }

    else
    {
        /* rien */

    }


}
