/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\defgroup f_load_mask_sequential f_load_mask_sequential
\ingroup libSigProc

\brief Cette fonction lit sequentialement dans un fichier les valeur a donner aux neurones

\details

\section Description

Cette fonction lit sequentialement dans un fichier les valeur a donner aux neurones  

\section Option
Option sur le lien et valeur par defaut :

-enable : enable if input neuron at 1

-rewind : rewind file if input neuron at 1

-ffile

-ignXXX

-c0 (0,1,2) : continue when EOF : 

0= stop and kill (default)

1= keep last no kill

2= replay whole file

\file
\ingroup f_load_mask_sequential

\details
modification:
author = "N.Beaussé"
date = "16/06/2014"
description = "Ajout possibilité d'ignorer "ign" valeurs en entrée"

modification:
author = "C.Giovannangeli"
date = "01/09/2004"
description = "specific file creation"
external_tool:
"Kernel_Function/find_input_link()"
"Kernel_Function/prom_getopt()"


 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
/* #define DEBUG */

#include <net_message_debug_dist.h>

typedef struct MyData_f_load_mask_sequential
{
   char *file_mask_name;
   FILE *file_mask;
   int continuer;
   int rewind_gpe;
   int enable_gpe;
   int value_to_ignore;
} MyData_f_load_mask_sequential;


void function_load_mask_sequential(int Gpe)
{
   char *file_mask_name = NULL;
   char param[256];
   FILE *file_mask;
   MyData_f_load_mask_sequential *my_data = NULL;
   int i, l, ret, enable = 1;
   int continuer=0, rewind_gpe = -1, enable_gpe = -1;
   float input;
   int eof=-1,value_to_ignore=-1,j=0;
   dprints("\nfunction_load_mask_sequential\n");
   if (def_groupe[Gpe].data == NULL)
   {
      i = 0;
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         if (prom_getopt(liaison[l].nom, "f", param) == 2)
         {
            file_mask_name = (char *) malloc(256);
            strcpy(file_mask_name, param);
         }
         if (prom_getopt(liaison[l].nom, "c", param) == 2)
         {
            continuer=atoi(param);
         }
         if (prom_getopt(liaison[l].nom, "ign", param) == 2)
         {
            value_to_ignore=atoi(param);
         }
         if (prom_getopt(liaison[l].nom, "-rewind", NULL) == 1)
         {
            rewind_gpe=liaison[l].depart;
            if( def_groupe[rewind_gpe].nbre != 1 )
               EXIT_ON_ERROR("GROUP %s (num coeos %s) : Input -rewind (group %s) needs just 1 neurons to rewind the file", __FUNCTION__, def_groupe[Gpe].no_name, def_groupe[rewind_gpe].no_name);
         }
         if (prom_getopt(liaison[l].nom, "-enable", NULL) == 1)
         {
            enable_gpe=liaison[l].depart;
            if( def_groupe[enable_gpe].nbre != 1 )
               EXIT_ON_ERROR("GROUP %s (num coeos %s) : Input -enable (group %s) needs just 1 neurons to rewind the file", __FUNCTION__, def_groupe[Gpe].no_name, def_groupe[enable_gpe].no_name);
         }
         i = i + 1;
         l = find_input_link(Gpe, i);
      }
      if (file_mask_name == NULL)
      {
         EXIT_ON_ERROR("f_load_mask_sequential (%s): pas de parametre donnant le nom du fichier\n",def_groupe[Gpe].no_name);
      }

      cprints("load_mask_sequential(%s):%s",def_groupe[Gpe].no_name, file_mask_name);
      switch(continuer) {
      case 0:
         cprints(", stop&kill");
         break;
      case 1:
         cprints(", keep last no kill");
         break;
      case 2:
         cprints(", replay");
         break;
      case 3:
         cprints(", stop&kill now");
         break;
      default:
         EXIT_ON_ERROR("f_load_mask_sequential (%s): Unexpected situation\n",def_groupe[Gpe].no_name);
         break;
      }
      cprints(" when EOF\n");


      file_mask = fopen(file_mask_name, "r");
      if (file_mask == NULL)
         EXIT_ON_ERROR("impossible d'ouvrir '%s'\n", file_mask_name);


      /* ouverture du fichier liste d'image */
      dprints("ouverture du mask sequential '%s'\n", file_mask_name);

      my_data = (MyData_f_load_mask_sequential *) malloc (sizeof(MyData_f_load_mask_sequential));
      if (my_data == NULL)
         EXIT_ON_ERROR("f_load_mask_sequential:malloc failed\n");


      my_data->file_mask_name = file_mask_name;
      my_data->file_mask = file_mask;
      my_data->continuer = continuer;
      my_data->enable_gpe = enable_gpe;
      my_data->rewind_gpe = rewind_gpe;
      my_data->value_to_ignore = value_to_ignore;
      printf("value_to_ignore %d \n",value_to_ignore);
      def_groupe[Gpe].data = (MyData_f_load_mask_sequential *) my_data;

   }
   else
   {
      my_data = (MyData_f_load_mask_sequential *) def_groupe[Gpe].data;
      file_mask_name = my_data->file_mask_name;
      file_mask = my_data->file_mask;
      continuer = my_data->continuer;
      enable_gpe = my_data->enable_gpe;
      rewind_gpe = my_data->rewind_gpe;
      value_to_ignore=my_data->value_to_ignore;
   }


   if( rewind_gpe != -1 )
   {
      if( neurone[def_groupe[rewind_gpe].premier_ele].s1 > 0.5 )
         rewind(file_mask);
   }

   if( enable_gpe != -1 )
      enable = neurone[def_groupe[enable_gpe].premier_ele].s1 > 0.5 ? 1 : 0;

   if( enable )
   {
      if (feof(file_mask))
      {
         switch(continuer) {
         case 0:
            cprints("In (%s), fin du fichier mask '%s'", def_groupe[Gpe].no_name,file_mask_name);
            fclose(file_mask);
            EXIT_ON_ERROR("Promethe va s'arreter\n");
            break;
         case 1:
            cprints("EOF (%s in %s) : Keeping last values\n", file_mask_name, def_groupe[Gpe].no_name);
            eof=1;; /* pas de mise a jour des valeurs */
            break;
         case 2:
            dprints("EOF (%s in %s) : Replay now\n", file_mask_name, def_groupe[Gpe].no_name);
            rewind(file_mask);
            break;
         case 3:
            cprints("In (%s), fin du fichier mask '%s'", def_groupe[Gpe].no_name,file_mask_name);
            fclose(file_mask);
            EXIT_ON_ERROR("Promethe va s'arreter\n");
            break;
         default:
            EXIT_ON_ERROR("f_load_mask_sequential (%s): Unexpected situation\n",def_groupe[Gpe].no_name);
            break;
         }
      }

      for (i = 0; i < def_groupe[Gpe].nbre && eof < 1; i++)
      {
         if (feof(file_mask))
         {
            switch(continuer) {
            case 0:
               dprints("In (%s), fin du fichier mask '%s'", def_groupe[Gpe].no_name,file_mask_name);
               getchar();
               getchar();
               fclose(file_mask);
               EXIT_ON_ERROR("Promethe va s'arreter\n");
               break;
            case 1:
               dprints("EOF (%s in %s) : Keeping last values\n", file_mask_name, def_groupe[Gpe].no_name);
               i=def_groupe[Gpe].nbre; /* pas de mise a jour des valeurs */
               break;
            case 2:
               dprints("EOF (%s in %s) : Replay now\n", file_mask_name, def_groupe[Gpe].no_name);
               rewind(file_mask);
               i--;
               break;
            case 3:
               dprints("In (%s), fin du fichier mask '%s'", def_groupe[Gpe].no_name,file_mask_name);
               fclose(file_mask);
               EXIT_ON_ERROR("Promethe va s'arreter\n");
               break;
            default:
               EXIT_ON_ERROR("f_load_mask_sequential (%s): Unexpected situation\n",def_groupe[Gpe].no_name);
               break;
            }
         }
         else
         {

            if (value_to_ignore>0)
            {
               for(j=0;j<=value_to_ignore;j++)
               {
                  ret = fscanf(file_mask, "%f", &input);
               }
               value_to_ignore=-1; // mise à -1 en local pour n'ignorer qu'une fois par ligne
            }
            else
            {
               ret = fscanf(file_mask, "%f", &input);
            }
            if(ret==1) {
               neurone[def_groupe[Gpe].premier_ele + i].s2 =
                     neurone[def_groupe[Gpe].premier_ele + i].s1 =
                           neurone[def_groupe[Gpe].premier_ele + i].s = input;
               dprints("i= %d, val = %f\n", i, input);
            }
            else{
               /* si on ne trouve pas de float reessayer la lecture pour le meme i*/
               i--;
            }
         }
      }
   }

   dprints("sortie de function_load_mask_sequential\n");
}
