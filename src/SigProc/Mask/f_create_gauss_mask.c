/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_create_gauss_mask f_create_gauss_mask
ingroup libSigProc

name = "f_create_gauss_mask" 
author = "Maickael Maillard"
date = "01/04/2004"
description = "Fonction creant un mask de convolution gaussien pour effectuer par exemple une fft.
 - Rem : le masque est normalise
 - Taille de la prom_image_struct : indiquee prioritairement sur le lien,
 sinon c'est la taille de l'image du groupe en amont qui est prise
 - le parametre sigma de la gaussienne est passe en parametre
 - Rem : le remplissage du mask est effectue jusqu'a 99% de l'energie de la gaussienne
 le reste etant complete avec des 0 (coeff non nul pour une demi taille de 3*sigma_de_la_gaussienne)"
comment = "Creation d'un mask de convolution gaussien" >

To be completed

\file
ingroup f_create_gauss_mask
author = "C.Giovannangeli"
date = "23/08/2004"
description = "specific file creation" 

*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#undef DEBUG
/*#define DEBUG*/

void function_create_gauss_mask(int Gpe)
{
	float sigma;
	float *outputImage;
	int i, row, column;
	int sx, sy, halfsize_x, halfsize_y, nsx, nsy;
	char *chaine = NULL, *param = NULL, *param2;
	float sigmasqr, NormCoeff;
	int inputGpe = -1;
	int parity_x, parity_y;

	float temp = (float) 0.0;
#ifdef DEBUG
	printf("Debut Gpe %d gaussmask\n", Gpe);
#endif

	if (def_groupe[Gpe].ext == NULL)
	{
		/*recherche du numero de groupe entrant et gestion des erreurs si existantes */
		for (i = 0; i < nbre_liaison; i++)
		{
			if (liaison[i].arrivee == Gpe)
			{
				chaine = liaison[i].nom;
				inputGpe = liaison[i].depart;
				break;
			}
		}
		if (chaine == NULL)
		{
			printf("%s : pas de liaison entrante\n", __FUNCTION__);
			exit(0);
		}
		param = strstr(chaine, "-SI");
		if (param != NULL)
			sigma = (float) atof(&param[3]);
		else
		{
			printf("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
			exit(0);
		}

		/*sur le lien et le echeant dans le groupe amont */
		param = strstr(chaine, "-X");
		param2 = strstr(chaine, "-Y");
		if (param != NULL && param2 != NULL)
		{
			sx = (int) atoi(&param[2]);
			sy = (int) atoi(&param2[2]);
		}
		else
		{
			if (def_groupe[inputGpe].ext == NULL)
			{
				printf
					("%s : les parametres du lien sont incorrects et le pointeur du lien entrant n'a pas ete initialise\n",
					 __FUNCTION__);
				exit(0);
			}
			sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
			sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;
		}


		sigmasqr = sigma * sigma;

		/*allocation de memoire pour la structure resulante */
		def_groupe[Gpe].ext =
			(prom_images_struct *) malloc(sizeof(prom_images_struct));
		if (def_groupe[Gpe].ext == NULL)
		{
			printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
				   __LINE__);
			exit(0);
		}

		/*halfsize_x=(int)(sx/2);
		   halfsize_y=(int)(sy/2); *//*code remplisant tout le masque a ne plsu utilise */

		/*mieux : code remplissant juste ce qui est necessaire : ~99% de l'energie */
		halfsize_x = (int) (3 * sigma);
		halfsize_y = (int) (3 * sigma);	/*attention a bien faire un calloc et attention <= */

		NormCoeff = 1 / (2 * (float) M_PI * sigmasqr);	/*et c'est bien 1/(2*pi*sigma**2) en 2D discret !!!!! il suffit de remettre le define debug pour s'en convaincre */

		/*taille et nb_band de la map resultat */
		((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
		((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
		((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
		((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

		/*allocation de memoire pour la map resultat */
		(((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
			calloc(sx * sy, sizeof(float));
		if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
			NULL)
		{
			printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
				   __LINE__);
			exit(0);
		}

		/*recuperation de pointeur pour alleger les expressions...et recup des parametres */
		outputImage =
			(float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
					   images_table[0]);


		/*calcul de la map resultante dans le cas d'un remplissage complet du masque */
		/*for(row=-halfsize_y;row<(sy-halfsize_y);row++)
		   {
		   for(column=-halfsize_x;column<(sx-halfsize_x);column++)
		   {
		   *(outputImage + (row+halfsize_y)*sx + column+halfsize_x ) = (float)( NormCoeff * exp(-(row*row+column*column)/(2*sigmasqr)) );
		   #ifdef DEBUG
		   temp=temp+ *(outputImage + (row+halfsize_y)*sx + column+halfsize_x);
		   #endif
		   }
		   } */


		nsx = (int) (sx / 2);
		nsy = (int) (sy / 2);

		/*on calcul precisement pour centrer le mask, que la taille soit paire ou impaire */
		parity_x = 1 - sx % 2;	/*1 si pair, 0 si impair */
		parity_y = 1 - sy % 2;

		/*calcul dans le cas du remplissage partiel 99% de l'energie */
		for (row = -halfsize_y - parity_y; row <= halfsize_y; row++)
		{
			for (column = -halfsize_x - parity_x; column <= halfsize_x;
				 column++)
			{
				if ((row + nsy) >= 0 && (row + nsy) < sy
					&& (column + nsx) >= 0 && (column + nsx) < sx)
				{
					*(outputImage + (row + nsy) * sx + column + nsx) = (float) (	/*NormCoeff * */
																				   exp
																				   (-
																					(((float) row + 0.5 * (float) parity_y) * ((float) row + 0.5 * (float) parity_y) + ((float) column + 0.5 * (float) parity_x) * ((float) column + 0.5 * (float) parity_x)) / (2 * sigmasqr)));

					temp =
						temp + *(outputImage + (row + nsy) * sx + column +
								 nsx);
				}
			}
		}

		/*on renormalise */
		for (row = -halfsize_y - parity_y; row <= halfsize_y; row++)
		{
			for (column = -halfsize_x - parity_x; column <= halfsize_x;
				 column++)
			{
				*(outputImage + (row + nsy) * sx + column + nsx) =
					*(outputImage + (row + nsy) * sx + column + nsx) / temp;
			}
		}

#ifdef DEBUG
/*    for (row=0; row<sy; row++)
    {
        for(column=0; column<sx; column++)
        {
            printf("%f  ", *(outputImage+row*sx+column) );
        }
        printf("\n");
    }*/

		printf
			("Gpe : %d ,mask gauss sigmasqr : %f cree , image number : %d, taille %dx%d,normalisation %f\n",
			 Gpe, sigmasqr,
			 ((prom_images_struct *) def_groupe[Gpe].ext)->image_number, sx,
			 sy, (float) temp);
#endif

	}
	else						/*autre appel du groupe : on ne recalcul pas le filtre */
	{;							/*instruction vide quand y'a pas de debug */
#ifdef DEBUG
		printf("Gpe %d mask deja cree\n", Gpe);
#endif
	}
#ifdef DEBUG
	printf("fin Gpe %d\n gaussmask", Gpe);
#endif
	return;
}
