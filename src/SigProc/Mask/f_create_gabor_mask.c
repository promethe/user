/**
\ingroup libSig_Prog
\defgroup f_create_gabor_mask f_create_gabor_mask


\section Author
- author: Maickael Maillard
- description: specific file creation
- date: 01/04/2004


 \section Modification
 author = C.Giovannangeli
 date = 23/08/2004
 description = specific file creation


\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description
Fonction creant un mask de gabor pour effectuer par exemple un filtrage de gabor.
 Le mask est normalise.

 Le remplissage du mask est effectue jusqu'a� 99% de l'energie de la gaussienne, le reste etant complete par des 0.

 Parametre : -SI, valeur du sigma du mask de gabor.
 -GA, valeur du gamma du mask de gabor.
 -Th, valeur du theta du mask de gabor.

 -X --> sx
 -Y --> sy
 La taille de la structure prom_image_struct ( sx et sy) est indique prioritairement sur le lien, sinon c'est la taille de l'image amont qui est prise."
 comment = "Creation d'un mask pour un filtrage de gabor



\section Macro
-none
\section Local variables
-none


\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
 */



/* #define DEBUG 1 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

void function_create_gabor_mask(int Gpe)
{
   float sigma, gamma, theta, cos_th, sin_th, gamma_sigma, ratio_spatial;
   float *outputImage_reel, *outputImage_imag;
   int i, row, column;
   int sx = -1, sy = -1, halfsize_x, halfsize_y, nsx, nsy;
   char *chaine = NULL, *param = NULL, *param2;
   float sigmasqr, NormCoeff;
   int inputGpe = -1;
   float reel, imag;
   FILE* FilePtr = NULL;
   int number = 0;
   char chaine2[100] = "";
   float temp = (float) 0.0;
   dprints("Debut Gpe %d gabormask\n", Gpe);
   sigma = 0;
   gamma = 0;
   theta = 0;
   ratio_spatial = 0.5 ; // TODO : prendre comme parametre sur le lien d'entree
   if (def_groupe[Gpe].ext == NULL)
   {
      /*recherche du numero de groupe entrant et gestion des erreurs si existantes */
      for (i = 0; i < nbre_liaison; i++)
      {
         if (liaison[i].arrivee == Gpe)
         {
            chaine = liaison[i].nom;
            inputGpe = liaison[i].depart;
            break;
         }
      }
      if (chaine == NULL)
      {
         printf("%s : pas de liaison entrante\n", __FUNCTION__);
         exit(0);
      }

      param = strstr(chaine, "-N");

      if (param != NULL) number = (int) atof(&param[2]);
      else
      {

         param = strstr(chaine, "-SI");
         if (param != NULL) sigma = (float) atof(&param[3]);
         else
         {
            EXIT_ON_ERROR("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
         }

         param = strstr(chaine, "-GA");
         if (param != NULL) gamma = (float) atof(&param[3]);
         else
         {
            EXIT_ON_ERROR("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
         }

         param = strstr(chaine, "-Th");
         if (param != NULL) theta = (float) atof(&param[3]);
         else
         {
            printf("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
            exit(0);
         }

         param = strstr(chaine, "-X");
         param2 = strstr(chaine, "-Y");
         if (param != NULL && param2 != NULL)
         {
            sx = (int) atoi(&param[2]);
            sy = (int) atoi(&param2[2]);
         }
         else
         {
            if (def_groupe[inputGpe].ext == NULL)
            {
               EXIT_ON_ERROR
               ("%s : les parametres du lien sont incorrects et le pointeur du lien entrant n'a pas ete initialise\n",
                     __FUNCTION__);
            }
            sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
            sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;
         }
      }

      FilePtr = fopen("gabor", "r");

      if (FilePtr != NULL)
      {
         sprintf(chaine2, "%d:", number);

         while (fgets(chaine, 100, FilePtr) != NULL)
         {
            if ((param = strstr(chaine, chaine2)) != NULL)
            {

               if ((param = strstr(chaine, "-SI")) != NULL) sigma = (float) atof(&param[3]);
               else
               {
                  EXIT_ON_ERROR("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
               }

               if ((param = strstr(chaine, "-GA")) != NULL) gamma = (float) atof(&param[3]);
               else
               {
                  EXIT_ON_ERROR("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
               }

               if ((param = strstr(chaine, "-Th")) != NULL) theta = (float) atof(&param[3]);
               else
               {
                  EXIT_ON_ERROR("%s : Parametre sigma incorrect -SI##\n", __FUNCTION__);
               }

               if ((param = strstr(chaine, "-X")) != NULL && (param2 = strstr(chaine, "-Y")) != NULL)
               {
                  sx = (int) atoi(&param[2]);
                  sy = (int) atoi(&param2[2]);
               }
               else
               {
                  EXIT_ON_ERROR("%s : Parametre taille image incorrect -X## -Y## \n", __FUNCTION__);
               }
               break;
            }
         }
      }
      else
      {
         dprints("%s : Pas de fichier de config gabor\n", __FUNCTION__);
      }

      //fclose(FilePtr);

      dprints("%d:\t sigma:%f, gamma:%f, theta:%f,sx:%d,sy:%d\n",number,sigma,gamma,theta,sx,sy);

      sigmasqr = sigma * sigma;

      /*allocation de memoire pour la structure resulante */
      def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)
      {
         EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
               __LINE__);
      }

      /*halfsize_x=(int)(sx/2);
     halfsize_y=(int)(sy/2); *//*code remplisant tout le masque a ne plsu utilise */

      /*mieux : code remplissant juste ce qui est necessaire : ~99% de l'energie */
      halfsize_x = (int) (4 * sigma);
      halfsize_y = (int) (4 * sigma); /*attention a bien faire un calloc et attention <= */

      /*taille et nb_band de la map resultat */
      ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
      ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
      ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 2;

      /*allocation de memoire pour la map resultat */
      (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) = calloc(sx * sy, sizeof(float));
      if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
      {
         EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
               __LINE__);
      }
      (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1]) = calloc(sx * sy, sizeof(float));
      if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] == NULL)
      {
         EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
               __LINE__);
      }
      /*recuperation de pointeur pour alleger les expressions...et recup des parametres */
      outputImage_reel = (float *) (((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[0]);
      outputImage_imag = (float *) (((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[1]);

      nsx = (int) (sx / 2);
      nsy = (int) (sy / 2);
      gamma_sigma = gamma * sigma;
      sin_th = sin(theta);
      cos_th = cos(theta);
      NormCoeff = 0.;

      /*calcul dans le cas du remplissage partiel */
      for (row = -halfsize_y; row <= halfsize_y; row++)
      {
         for (column = -halfsize_x; column <= halfsize_x; column++)
         {
            if ((row + nsy) >= 0 && (row + nsy) < sy && (column + nsx) >= 0 && (column + nsx) < sx)
            {
               reel = (float) (exp(-((row * cos_th + column * sin_th)*(row * cos_th + column * sin_th) + ratio_spatial * (column * cos_th - row * sin_th)*(column * cos_th - row * sin_th)) / (2 * sigmasqr)) * cos(2. * M_PI * (row * cos_th + column * sin_th) / (gamma_sigma)));
               // OLD : reel = (float) (exp(-(row * row + column * column) / (2 * sigmasqr)) * cos(2. * M_PI * (row * cos_th + column * sin_th) / (gamma_sigma)));

               *(outputImage_reel + (row + nsy) * sx + column + nsx) = reel;

               imag = (float) (exp(-((row * cos_th + column * sin_th)*(row * cos_th + column * sin_th) + ratio_spatial * (column * cos_th - row * sin_th)*(column * cos_th - row * sin_th)) / (2 * sigmasqr)) * sin(2. * M_PI * (row * cos_th + column * sin_th) / (gamma_sigma)));
               //OLD : imag = (float) (exp(-(row * row + column * column) / (2 * sigmasqr)) * sin(2. * M_PI * (row * cos_th + column * sin_th) / (gamma_sigma)));

               *(outputImage_imag + (row + nsy) * sx + column + nsx) = imag;

               NormCoeff += reel * reel + imag * imag;

            }
         }
      }

      NormCoeff = sqrt(NormCoeff);
      NormCoeff = 1. / NormCoeff;
      for (row = -halfsize_y; row <= halfsize_y; row++)
      {
         for (column = -halfsize_x; column <= halfsize_x; column++)
         {
            if ((row + nsy) >= 0 && (row + nsy) < sy && (column + nsx) >= 0 && (column + nsx) < sx)
            {
               *(outputImage_reel + (row + nsy) * sx + column + nsx) *= NormCoeff;
               *(outputImage_imag + (row + nsy) * sx + column + nsx) *= NormCoeff;
            }
         }
      }

      dprints
      ("Gpe : %d ,mask gabor sigmasqr : %f cree , image number : %d, taille %dx%d,normalisation %f\n",
            Gpe, sigmasqr,
            ((prom_images_struct *) def_groupe[Gpe].ext)->image_number, sx,
            sy, (float) temp);

   }
   else /*autre appel du groupe : on ne recalcul pas le filtre */
   {
      ; /*instruction vide quand y'a pas de debug */

      dprints("Gpe %d mask deja cree\n", Gpe);

   }
   dprints("fin Gpe %d\n gabor", Gpe);

   (void) temp;
   return;
}
