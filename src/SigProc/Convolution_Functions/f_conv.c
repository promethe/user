/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup f_conv  f_conv
 \ingroup libSigProc

 \brief To be completed

 description :

 Convolution spatio-temporelle avec la methode d approximation runge-kunta

 To be completed

 \file
 \ingroup f_conv
 name = ERR_FLOAT
 external_tool
 name = SigProc/Convolution_Functions/function_normal_convolution()

 **/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/conv_data.h>

#include <SigProc/Convolution_Functions.h>

#include "tools/include/macro.h"

void new_convolution(int gpe)
{
  char *link_name;
  conv_data *my_data;
  int link_id, links_nb;
  type_groupe *my_group;
  char param[256];

  my_group = &def_groupe[gpe];
  my_data = ALLOCATION(conv_data);

  my_data->cste_moins_facteur_gpe=-1;
  my_data->borne=0;
  for (links_nb = 0; (link_id = find_input_link(gpe, links_nb)) != -1; links_nb++)
  {
    link_name = liaison[link_id].nom;

    if (prom_getopt(link_name, "-borne", param) == 2)
    {
      my_data->borne=1;
    }
    if (strstr(link_name, "mask") == link_name)
    {
      my_data->mask_group = &def_groupe[liaison[link_id].depart];
    }
    if (strstr(link_name, "cste_moins") == link_name)
    {
      my_data->cste_moins_facteur_gpe = liaison[link_id].depart;
      my_data->sum_total=0.0;
    }
    else if (strstr(link_name, "input") == link_name)
    {
      my_data->input_group = &def_groupe[liaison[link_id].depart];
      my_data->time_float = liaison[link_id].stemps;
      if (my_data->time_float <= ERR_FLOAT)
      {
        free(my_data);
        my_data = NULL;
        function_normal_convolution(gpe);
        return; /* AB: Tres mal fichu car il n'ya pas de new dans  function_normal_convolution De toute façon les fonction devraient être differentes e.g. f_dnf */

      }
    }
  }

  if (my_group->taillex != my_data->input_group->taillex || my_group->tailley != my_data->input_group->tailley) EXIT_ON_GROUP_ERROR(gpe, "error the size of the input group (%d, %d) is not the same as the convolution groupe (%d,%d)\n", my_data->input_group->taillex, my_data->input_group->tailley, my_group->taillex, my_group->tailley);

  my_group->data = my_data;
}

/*       if(trace>0) printf("create extention data for the convolution \n");

 for (i = 0; i < nbre_liaison; i++)
 {
 if (liaison[i].arrivee == gpe && strcmp(liaison[i].nom, "mask") == 0)
 {
 no_input = liaison[i].depart;
 mask_begin = def_groupe[no_input].premier_ele;
 mask_lenght = def_groupe[no_input].nbre;
 mask_size_x = def_groupe[no_input].taillex;
 mask_size_y = def_groupe[no_input].tailley;

 mask = (float *) malloc(sizeof(float) * mask_lenght);

 }
 if (liaison[i].arrivee == numero && strcmp(liaison[i].nom, "input") == 0)
 {
 no_input = liaison[i].depart;
 input_begin = def_groupe[no_input].premier_ele;
 input_lenght = def_groupe[no_input].nbre;
 input_size_x = def_groupe[no_input].taillex;
 input_size_y = def_groupe[no_input].tailley;
 time_float = liaison[i].stemps;
 }
 }
 if (size_x != input_size_x || size_y != input_size_y)
 {
 EXIT_ON_GROUP_ERROR(gpe, "error the size of the input group (%d, %d) is not the same as the convolution groupe (%d,%d)\n", input_size_x, input_size_y, size_x, size_y);
 }

 j = 0;
 for (i = mask_begin; i < mask_begin + mask_lenght; i++)
 {
 mask[j++] = neurone[i].s;
 }

 def_groupe[gpe].data = (void *) malloc(sizeof(conv_data));

 (*((conv_data *) def_groupe[gpe].data)).mask_begin = mask_begin;
 (*((conv_data *) def_groupe[gpe].data)).mask_lenght = mask_lenght;
 (*((conv_data *) def_groupe[gpe].data)).mask_size_x = mask_size_x;
 (*((conv_data *) def_groupe[gpe].data)).mask_size_y = mask_size_y;
 (*((conv_data *) def_groupe[gpe].data)).input_begin = input_begin;
 (*((conv_data *) def_groupe[gpe].data)).input_lenght = input_lenght;
 (*((conv_data *) def_groupe[gpe].data)).input_size_x = input_size_x;
 (*((conv_data *) def_groupe[gpe].data)).input_size_y = input_size_y;
 (*((conv_data *) def_groupe[numero].data)).time_float = time_float;
 (*((conv_data *) def_groupe[numero].data)).mask = mask;

 }
 */
void function_convolution(int gpe)
{
  int deb, length;
  int mask_deb, mask_half_size_x, mask_half_size_y;
  int i;
  int mask_size_x = 0, mask_size_y = 0;
  int input_deb;
  /*int input_size_x, input_size_y;*/
  int size_x, size_y;
  int mask_id, p2, x, y;
  int a, b, a1, b1;
  /*float k1, k2, k3, k4;*/
  float /*Si, temp,*/ val;
  float u, neighbor, input,facteur_minus=0.0, sum_total=0.0;
  conv_data *my_data;

  /*float A0=1.;
   FILE * conv; */

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  my_data = def_groupe[gpe].data;

  if (my_data->time_float <= ERR_FLOAT) function_normal_convolution(gpe);
  else
  {
    deb = def_groupe[gpe].premier_ele;
    length = def_groupe[gpe].nbre;
    size_x = def_groupe[gpe].taillex;
    size_y = def_groupe[gpe].tailley;

    input_deb = my_data->input_group->premier_ele;
    /*input_size_x = my_data->input_group->taillex;
    input_size_y = my_data->input_group->tailley;*/

    mask_deb = my_data->mask_group->premier_ele;
    mask_size_x = my_data->mask_group->taillex;
    mask_size_y = my_data->mask_group->tailley;

    mask_half_size_x = mask_size_x / 2;
    mask_half_size_y = mask_size_y / 2;

  // for (i = deb; i < deb + length; i++)
   //   neurone[i].d = 0.0; /* to store the sum of the local interaction effect */

    for (a = 0; a < size_x; a++)
    {
      for (b = 0; b < size_y; b++)
      {
        i = deb + a + size_x * b;

        val = neurone[i].s1;

        if (val > 0.0001)
        {
          for (x = 0; x < mask_size_x; x++)
          {
            a1 = a + x - mask_half_size_x;
            a1 = MOD(a1, size_x);

            for (y = 0; y < mask_size_y; y++)
            {
              mask_id = mask_deb + x + y * mask_size_x;

              b1 = b + y - mask_half_size_y;
              b1 = MOD(b1, size_y);

              p2 = deb + a1 + b1 * size_x;

              neurone[p2].d += val * neurone[mask_id].s1; /* lateral interactions */
            }
          }
        }
      }
    }

    if (my_data->cste_moins_facteur_gpe!=-1)
      {
        facteur_minus=neurone[def_groupe[my_data->cste_moins_facteur_gpe].premier_ele].s1;
      }
    for (i = 0; i < length; i++)
      {
        u = neurone[deb + i].s;
        neighbor = neurone[deb + i].d;
        input = neurone[input_deb + i].s1;

        neurone[deb + i].s += my_data->time_float * (-u + neighbor + input - (facteur_minus*(my_data->sum_total - neurone[deb+i].s1)));

        if(my_data->borne==1)
        {
          if (neurone[deb + i].s < 0) neurone[deb + i].s = 0;
          else if (neurone[deb + i].s > 1) neurone[deb + i].s = 1;
          neurone[deb + i].s1=neurone[deb + i].s;
        }
        else
        {
          if (neurone[deb + i].s < 0) neurone[deb + i].s1 = 0;
          else if (neurone[deb + i].s > 1) neurone[deb + i].s1 = 1;
          else neurone[deb + i].s1 = neurone[deb + i].s;
        }


        sum_total+=neurone[deb+i].s1;
        neurone[deb + i].d=0;
      //  neurone[deb + i].s2 = neurone[deb + i].s1;
      }
    my_data->sum_total=sum_total;
  }



  dprints("f_conv\n");
  /*potentiel u */
  /*
   for (i = deb; i < deb + length; i++)
   {
   Si = neurone[i].s;
   temp = neurone[i].d + neurone[input_begin + (i - deb) * input_step].s1;

   k1 = lambda * (-Si + temp);
   k2 = lambda * (-Si - 0.5 * k1 * lambda + temp + .5 * lambda);
   k3 = lambda * (-Si - 0.5 * k2 * lambda + temp + .5 * lambda);
   k4 = lambda * (-Si - k3 * lambda + temp + lambda);
   Si = Si + 0.16666666 * (k1 + 2 * k2 + 2 * k3 + k4);

   neurone[i].s = Si;

   if (Si < 0.) neurone[i].s1 = neurone[i].s2 = 0.;
   else if (Si > 1.) neurone[i].s1 = neurone[i].s2 = 1.;
   else neurone[i].s1 = neurone[i].s2 = Si;

   }*/
  /*  }*/
}
