/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_conv_non_circular f_conv_non_circular
 \ingroup libSigProc


 To be completed

 description :

 Convolution spatio-temporelle avec la methode d approximation runge-kunta
 Version non ciculaire

 \file
 \ingroup f_conv_non_circular

 name = ERR_FLOAT

 external_tool
 name = SigProc/Convolution_Functions/function_n_conv_non_circular()

 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/conv_data.h>

#include <SigProc/Convolution_Functions.h>

#include "tools/include/macro.h"

/* #define DEBUG */

#include <net_message_debug_dist.h>

void new_conv_non_circular(const int numero)
{
  int no_input;
  int i;
  float *mask = NULL;
  int mask_begin = 0, mask_lenght = 0;
  int mask_size_x = 0, mask_size_y = 0;
  int input_begin = 0, input_lenght = 0;
  int input_size_x = 0, input_size_y = 0;
  int size_x, size_y;
  float time_float = 0.; /*Modif le 9/4/99 Sacha, temps devient float au lieu de int  time constant for the group memory stored in stemps field of the link */
  /*float A0=1.;
   FILE * conv; */

  dprints("entree dans f_conv_non_circular (%d) \n", numero);

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  /*
   if(trace>0) printf("convolution group : %d \n",numero);
   */
  size_x = def_groupe[numero].taillex;
  size_y = def_groupe[numero].tailley;

  if (def_groupe[numero].data == NULL)
  {
    /*       if(trace>0) printf("create extention data for the convolution \n"); */
    /* recupere sur le lien l'information sur la periode de la sequence */

    for (i = 0; i < nbre_liaison; ++i)
    {
      if (liaison[i].arrivee == numero && strcmp(liaison[i].nom, "mask") == 0)
      {
        no_input = liaison[i].depart;
        mask_begin = def_groupe[no_input].premier_ele;
        mask_lenght = def_groupe[no_input].nbre;
        mask_size_x = def_groupe[no_input].taillex;
        mask_size_y = def_groupe[no_input].tailley;


      }
      if (liaison[i].arrivee == numero && strcmp(liaison[i].nom, "input") == 0)
      {
        no_input = liaison[i].depart;
        input_begin = def_groupe[no_input].premier_ele;
        input_lenght = def_groupe[no_input].nbre;
        input_size_x = def_groupe[no_input].taillex;
        input_size_y = def_groupe[no_input].tailley;
        time_float = liaison[i].stemps;
      }
    }

    if (size_x != input_size_x || size_y != input_size_y)
    EXIT_ON_ERROR("error the size of the input group (%d, %d) is not the same as the convolution groupe (%d,%d)\n", input_size_x, input_size_y, size_x, size_y);



    def_groupe[numero].data = (void *) malloc(sizeof(conv_data));

    (*((conv_data *) def_groupe[numero].data)).mask_begin = mask_begin;
    (*((conv_data *) def_groupe[numero].data)).mask_lenght = mask_lenght;
    (*((conv_data *) def_groupe[numero].data)).mask_size_x = mask_size_x;
    (*((conv_data *) def_groupe[numero].data)).mask_size_y = mask_size_y;
    (*((conv_data *) def_groupe[numero].data)).input_begin = input_begin;
    (*((conv_data *) def_groupe[numero].data)).input_lenght = input_lenght;
    (*((conv_data *) def_groupe[numero].data)).input_size_x = input_size_x;
    (*((conv_data *) def_groupe[numero].data)).input_size_y = input_size_y;
    (*((conv_data *) def_groupe[numero].data)).time_float = time_float;
    (*((conv_data *) def_groupe[numero].data)).mask = mask;


  }
  else
  {
    PRINT_WARNING("Erreur dans la fonction f_conv_non_circular au niveau de l'init\n");
  }

}

void function_conv_non_circular(const int numero)
{
  int i;
  int inc;
  int pas,j;
  int p, p2, x, y;
  int a, b, a1, b1;
  float lambda; /* decay coeff for the potential */
  float k1, k2, k3, k4;
  float Si, temp = 0.;
  const int mask_begin = (*((conv_data *) def_groupe[numero].data)).mask_begin;
  const int mask_lenght = (*((conv_data *) def_groupe[numero].data)).mask_lenght;
  const int mask_size_x = (*((conv_data *) def_groupe[numero].data)).mask_size_x;
  const int mask_size_y = (*((conv_data *) def_groupe[numero].data)).mask_size_y;
  const int input_begin = (*((conv_data *) def_groupe[numero].data)).input_begin;
  const int input_lenght = (*((conv_data *) def_groupe[numero].data)).input_lenght;
  const int input_size_x = (*((conv_data *) def_groupe[numero].data)).input_size_x;
  const int input_size_y = (*((conv_data *) def_groupe[numero].data)).input_size_y;
  const float time_float = (*((conv_data *) def_groupe[numero].data)).time_float; /*Modif le 9/4/99 Sacha, temps devient float au lieu de int  time constant for the group memory stored in stemps field of the link */
  float *mask = (*((conv_data *) def_groupe[numero].data)).mask;
  const int longueur = def_groupe[numero].nbre;
  const int deb = def_groupe[numero].premier_ele;
  const int size_x = def_groupe[numero].taillex;
  const int size_y = def_groupe[numero].tailley;


  // Attention : initialisation du masque forcement dans cette fonction car pas d'activité des neurones d'entrée dans le new !
  if(mask == NULL)
  {
    (*((conv_data *) def_groupe[numero].data)).mask = (float *) malloc(sizeof(float) * mask_lenght);
    mask=(*((conv_data *) def_groupe[numero].data)).mask ;
    j = 0;
    pas = input_lenght / (input_size_x * input_size_y);
    for (i = mask_begin; i < mask_begin + mask_lenght; i=i+pas)
      {
       mask[j] = neurone[i].s1;
       j++;
      }
  }

  if (fabs(time_float) <= ERR_FLOAT)
    {
      function_n_conv_non_circular(numero);
    }
  else
  {
    dprints("passage dans la conv temporelle\n");
    lambda = time_float;
    for (i = deb; i < deb + longueur; ++i)
      neurone[i].d = 0.0; /* to store the sum of the local interaction effect */

    pas = input_lenght / (input_size_x * input_size_y);
    //j = deb;
    inc = mask_size_x / 2 + mask_size_y / 2 * mask_size_x;
    for (a = 0; a < size_x; ++a)
      for (b = 0; b < size_y; ++b)
      {
        i = deb + a + size_x * b;
        if (fabs(neurone[i].s2) > 0.0001)
        {
          for (x = -mask_size_x / 2; x < (1 + mask_size_x) / 2; ++x)
            for (y = -mask_size_y / 2; y < (1 + mask_size_y) / 2; ++y)
            {
              p = inc + x + y * mask_size_x;
              a1 = a + x;
              b1 = b + y;
              if (a1 >= 0 && a1 < size_x && b1 >= 0 && b1 < size_y)
              {
                p2 = pas - 1 + pas * (a1 + b1 * size_x) + deb;
                neurone[p2].d += neurone[i].s2 * mask[p]; /* lateral interactions */
              }
            }
        }
      }

    for (i = deb; i < deb + longueur; i++)
    {
      Si = neurone[i].s;
      temp = neurone[i].d + neurone[input_begin + (i - deb) * pas].s2;

      k1 = lambda * (-Si + temp);
      k2 = lambda * (-Si - 0.5 * k1 * lambda + temp + .5 * lambda);
      k3 = lambda * (-Si - 0.5 * k2 * lambda + temp + .5 * lambda);
      k4 = lambda * (-Si - k3 * lambda + temp + lambda);
      Si = Si + 0.16666666 * (k1 + 2 * k2 + 2 * k3 + k4);
      /*if(Si<0.) neurone[i].s1=neurone[i].s2=neurone[i].s=0.; *//* modifie par Jerome Damelincourt */
      /* else */
      neurone[i].s1 = neurone[i].s2 = neurone[i].s = Si;
      /*       printf("conv runge-kutta neurone[%d].s=%f\n",i,neurone[i].s); */

    }

  }
#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace =
    OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
    1;
    MicroSecondesFunctionTimeTrace =
    1000000 + OutputFunctionTimeTrace.tv_usec -
    InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace,
      "Time in function_convolution\t%4ld.%06d\n",
      SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif

  dprints("sortie de f_conv_non_circular (%d) \n", numero);
}

void destroy_conv_non_circular(const int numero)
{
  if (def_groupe[numero].data != NULL)
  {
    if ((*((conv_data *) def_groupe[numero].data)).mask != NULL)
    {
      free((float*) ((*((conv_data *) def_groupe[numero].data)).mask));
    }
    else
    {
      PRINT_WARNING("Erreur dans la libération mémoire de mask dans f_conv_non_circular\n");
    }

    free((conv_data *) def_groupe[numero].data);
  }
  else
  {
    PRINT_WARNING("Erreur dans la libération mémoire de f_conv_non_circular\n");
  }

}
