/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
\defgroup f_create_dog f_create_dog
\ingroup libSigProc

author = A. Karaouzene
date = 24/04/2013
description = specific file creation

\brief Centered Difference of Gaussian.

\section Description

Cree une dog sur la taille du groupe centree sur le milieu de la carte (prendre un nombre de neurones impairs.
(utile pour les convolutions par exemple)
\f[

act(x)  = \frac{1}{s_1*sqrt(2*pi)} * e^{ - \frac{x*x}{2*s_1*s_1}} - \frac{1}{s_2*sqrt(2*pi)} * e^{ - \frac{x*x}{2*s_2*s_2}}

\f]

L'equation en 2D n'est pas l'equation classique pour une fonction
gaussienne. Il manque le produit x par y.

\section Links

-R : valeur de sigma1 de la premiere gaussienne
-T : valeur de sigma2 de la deuxieme gaussienne

 */


/* #define DEBUG */
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <math.h>

/*#define R_PI sqrt(2*M_PI)*/

typedef struct my_data {
	float theta2;
	float theta1;
	float *tableau;
} Data_create_dog;


/**
 * see f_create_gaussian_map in modules
 * @param num group number
 */
void new_create_dog(int gpe)
{
	int i,j,l;
	char * string = NULL;
	char param_link[256];
	float theta1 = 0.,theta2 = 0.;
	int taille_gpe = 0, debut_gpe = 0,mi_taille = 0, nx = 0, ny = 0, mi_nx = 0, mi_ny = 0,ind = 0;
	float a1, a2, d, d1, d2, a3, a4;
	float R_PI = 0.;
	float *tableau;
	float amp1, amp2;
	Data_create_dog * mydata;
	/*Recuperation des liens*/
	mydata = (Data_create_dog * ) malloc (sizeof(Data_create_dog));
	if (mydata == NULL)
		EXIT_ON_ERROR("Cannot allocate the data in (%s)\n",__FUNCTION__);
	theta2 = 7.0;
	theta1 = 2.0;
	amp1 = amp2 = 1.0;
	R_PI = sqrt(2*M_PI);
	i = 0;
	l = find_input_link(gpe, i);
	while (l != -1)
	{
		string = liaison[l].nom;
		if (strstr(string,"sync") != NULL )
			dprints("\nlien sync ");
		else
		{
			if (prom_getopt(string, "-T", param_link) == 2 || prom_getopt(string, "-t", param_link) == 2)
			{
				theta2 = atof(param_link);	dprints("theta2 = %f\n", theta2);
			}

			if (prom_getopt(string, "-R", param_link) == 2 || prom_getopt(string, "-r", param_link) == 2)
			{
				theta1 = atof(param_link);        dprints("theta1 = %f\n", theta1);
			}
			if (prom_getopt(string, "-AT", param_link) == 2 || prom_getopt(string, "-t", param_link) == 2)
                        {
                                amp2 = atof(param_link);      dprints("amp2 = %f\n", theta2);
                        }

                        if (prom_getopt(string, "-AR", param_link) == 2 || prom_getopt(string, "-r", param_link) == 2)
                        {
                                amp1 = atof(param_link);        dprints("amp1 = %f\n", theta1);
                        }

		}
		i++;
		l = find_input_link(gpe, i);
	}
	if (theta1>theta2)
		PRINT_WARNING ("Le parametre R(theta1) doit etre plus grand que T (theta2) sinon ce n'est plus une cellule centre-oon in %s",__FUNCTION__);


	taille_gpe = def_groupe[gpe].nbre;
	nx =  def_groupe[gpe].taillex;
	ny = def_groupe[gpe].tailley;
	debut_gpe = def_groupe[gpe].premier_ele;
	mi_taille = taille_gpe/2;
	mi_nx = nx/2;
	mi_ny =ny/2;
	tableau = (float *) malloc (taille_gpe*sizeof(float));
	if (tableau == NULL)
		EXIT_ON_ERROR("Cannot allocate tableau in (%s)\n",__FUNCTION__);


	if (nx == 1 || ny == 1 )
	{
		dprints("en UNE seule dimension \n");
		a3 = (2. * theta1 * theta1);
		a4 = (2. * theta2 * theta2);
		a1 = amp1 / (R_PI * theta1);
		a2 = amp2 / (R_PI * theta2);
		for (i = 0 ; i < taille_gpe; i++)
		{
			d = (float) (i-mi_taille)*(i-mi_taille);
			d1 = exp(-d / a3);
			d2 = exp(-d / a4);
			neurone[debut_gpe+i].s
			= neurone[debut_gpe+i].s1
			= neurone[debut_gpe+i].s2 = (a1 * d1) - (a2 * d2);
			//fprintf(stderr,"%f %f %f\n",neurone[debut_gpe+i].s2,a1 * d1,a2 * d2);
			tableau[i] = (a1 * d1) - (a2 * d2);
			//fprintf(stderr,"%f %d %d\n",tableau[i],debut_gpe+i,def_groupe[gpe].nbre);
		}
	}
	else
	{
		dprints("en DEUX seule dimension \n");
		a3 = (2. * theta1 * theta1);
		a4 = (2. * theta2 * theta2);
		a1 = amp1 / (2. * M_PI * theta1 * theta1);
		a2 = amp2 / (2. * M_PI * theta2 * theta2);
		for (j = 0; j < ny; j++)
		{
			for (i = 0 ; i < nx; i++)
			{
				d = (float) (((i - mi_nx) * (i-mi_nx)) + ((j - mi_ny) * (j-mi_ny)));
				d1 = exp(-d / a3);
				d2 = exp(-d / a4);
				ind = j*nx+i;
				neurone[debut_gpe+ind].s
				= neurone[debut_gpe+ind].s1
				= neurone[debut_gpe+ind].s2 = (a1 * d1) - (a2 * d2);
				tableau[ind] = (a1 * d1) - (a2 * d2);
			}
		}

	}
	mydata->tableau = tableau;
	def_groupe[gpe].data = mydata;
}


void function_create_dog(int gpe) {
	Data_create_dog * mydata;
	float * tableau;
	int i=0,j=0;
	int debut_gpe = 0, nx = 0, ny = 0,ind = 0;
	mydata = (Data_create_dog * )def_groupe[gpe].data;
	tableau = (float*) mydata->tableau;

	nx =  def_groupe[gpe].taillex;
	ny = def_groupe[gpe].tailley;
	debut_gpe = def_groupe[gpe].premier_ele;

	for (j = 0; j < ny; j++)
	{
		for (i = 0 ; i < nx; i++)
		{
			ind = j*nx+i;
			neurone[debut_gpe+ind].s = neurone[debut_gpe+ind].s1 = neurone[debut_gpe+ind].s2 = tableau[i];
		}
	}

}

