/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_n_conv_non_circular f_n_conv_non_circular
\ingroup libSigProc

description :
 Convolution spatio-temporelle avec la methode d approximation runge-kunta
  Version non ciculaire

To be completed

\file
\ingroup f_n_conv_non_circular
*/

#include <libx.h>


#include <stdlib.h>
#include <string.h>
#include <Struct/conv_data.h>
#define debug
void function_n_conv_non_circular(int numero)
{
    int deb, longueur;
    int no_input;
    int i, j;
    float *mask = NULL;
    int mask_begin = 0, mask_lenght = 0;
    int mask_size_x = 0, mask_size_y = 0;
    int input_begin = 0, input_lenght = 0;
    int input_size_x = 0, input_size_y = 0;
    int size_x, size_y;
    int inc;
    int pas;
    int p, p2, x, y;
    int a, b, a1, b1;
    float time_float = 0;       /* time constant for the group memory stored in stemps field of the link */
/*   float Si;*/
    float store;
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    longueur = def_groupe[numero].nbre;
    deb = def_groupe[numero].premier_ele;
    size_x = def_groupe[numero].taillex;
    size_y = def_groupe[numero].tailley;
#ifdef DEBUG
    printf("Entrer in f_n_conv_non_circular: ");
#endif

    if (def_groupe[numero].data == NULL)
    {
        /*       if(trace>0) printf("create extention data for the normal convolution \n"); */
        /* recupere sur le lien l'information sur la periode de la sequence */

        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == numero
                && strcmp(liaison[i].nom, "mask") == 0)
            {
                no_input = liaison[i].depart;
                mask_begin = def_groupe[no_input].premier_ele;
                mask_lenght = def_groupe[no_input].nbre;
                mask_size_x = def_groupe[no_input].taillex;
                mask_size_y = def_groupe[no_input].tailley;
                mask = (float *) malloc(sizeof(float) * mask_lenght);
            }
            if (liaison[i].arrivee == numero
                && strcmp(liaison[i].nom, "input") == 0)
            {
                no_input = liaison[i].depart;
                input_begin = def_groupe[no_input].premier_ele;
                input_lenght = def_groupe[no_input].nbre;
                input_size_x = def_groupe[no_input].taillex;
                input_size_y = def_groupe[no_input].tailley;
                time_float = liaison[i].stemps;
            }
        }

        if (size_x != input_size_x || size_y != input_size_y)
            EXIT_ON_ERROR
                ("error the size of the input group (%d, %d) is not the same as the normal convolution non circulargroupe (%d,%d)\n",
                 input_size_x, input_size_y, size_x, size_y);


        for (i = mask_begin, j = 0; i < mask_begin + mask_lenght; i++, j++)
        {
            mask[j] = neurone[i].s1;
        }


        def_groupe[numero].data = (void *) malloc(sizeof(conv_data));

        (*((conv_data *) def_groupe[numero].data)).mask_begin = mask_begin;
        (*((conv_data *) def_groupe[numero].data)).mask_lenght = mask_lenght;
        (*((conv_data *) def_groupe[numero].data)).mask_size_x = mask_size_x;
        (*((conv_data *) def_groupe[numero].data)).mask_size_y = mask_size_y;
        (*((conv_data *) def_groupe[numero].data)).input_begin = input_begin;
        (*((conv_data *) def_groupe[numero].data)).input_lenght =
            input_lenght;
        (*((conv_data *) def_groupe[numero].data)).input_size_x =
            input_size_x;
        (*((conv_data *) def_groupe[numero].data)).input_size_y =
            input_size_y;
        (*((conv_data *) def_groupe[numero].data)).time_float = time_float;
        (*((conv_data *) def_groupe[numero].data)).mask = mask;

    }
    else
    {
        /*       if(trace>0) printf("instantiate local variables from the group extention \n"); */
        mask_begin = (*((conv_data *) def_groupe[numero].data)).mask_begin;
        mask_lenght = (*((conv_data *) def_groupe[numero].data)).mask_lenght;
        mask_size_x = (*((conv_data *) def_groupe[numero].data)).mask_size_x;
        mask_size_y = (*((conv_data *) def_groupe[numero].data)).mask_size_y;
        input_begin = (*((conv_data *) def_groupe[numero].data)).input_begin;
        input_lenght =
            (*((conv_data *) def_groupe[numero].data)).input_lenght;
        input_size_x =
            (*((conv_data *) def_groupe[numero].data)).input_size_x;
        input_size_y =
            (*((conv_data *) def_groupe[numero].data)).input_size_y;
        time_float = (*((conv_data *) def_groupe[numero].data)).time_float;

        mask = (*((conv_data *) def_groupe[numero].data)).mask;
    }

/*   for(i=deb;i<deb+longueur;i++)
   {
     neurone[i].d = 0.0;
   }*/

    pas = input_lenght / (input_size_x * input_size_y);
    j = deb;
    inc = mask_size_x / 2 + mask_size_y / 2 * mask_size_x;
    for (a = 0; a < size_x; a++)
        for (b = 0; b < size_y; b++)
        {
            /*i=input_begin+(a+size_x*b)*pas; */
            i = deb + a + size_x * b;   /* index in convolution map */
            store = 0.;
            for (x = -mask_size_x / 2; x < (1 + mask_size_x) / 2; x++)  /* x_index in mask */
                for (y = -mask_size_y / 2; y < (1 + mask_size_y) / 2; y++)  /* y_index in mask */
                {
                    p = inc + x + y * mask_size_x;  /* index in mask map */
                    a1 = a + x;
                    b1 = b + y;
                    if (a1 >= 0 && a1 < size_x && b1 >= 0 && b1 < size_y)
                    {
                        p2 = input_begin + (a1 + b1 * size_x) * pas + pas - 1;
                        store += neurone[p2].s1 * mask[p];  /* lateral interactions */
                    }
                    neurone[i].s1 = neurone[i].s = store;   /* rajout pour avoir une version non seuille dans s1 24/04/2003 Olivier Ledoux */
                    if (store < -1.)
                        neurone[i].s2 = -1.;    /* modif pour avoir une version non seuille dans s1 24/04/2003 Olivier Ledoux */
                    else if (store < 1.)
                        neurone[i].s2 = store;  /* modif pour avoir une version non seuille dans s1 24/04/2003 Olivier Ledoux */
                    else
                        neurone[i].s2 = 1.; /* modif pour avoir une version non seuille dans s1 24/04/2003 Olivier Ledoux */
                }
        }

/*
   for(i=deb;i<deb+longueur;i++){
     Si = neurone[i].d;
     neurone[i].s1=Si;

     if(Si<-1.) neurone[i].s2=neurone[i].s=-1.;
     else

       if(Si<1.) neurone[i].s2=neurone[i].s=Si;
       else

	 neurone[i].s=neurone[i].s2=1.;


   }*/

#ifdef DEBUG
    printf("Exit from f_n_conv_non_circular: ");
#endif
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_n_conv_non_circular\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
(void) longueur;
}
