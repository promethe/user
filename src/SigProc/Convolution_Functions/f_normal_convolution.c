/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSigProc
\defgroup f_normal_convolution f_normal_convolution

\section Modified
- author: A. Karaouzene
- description: Parallelisation
- date: 02/01/2012


\details
 Cette fonction permet de calculer la convolution entre les signaux en entree.
 Les convolution en 2D sont threadees. L'image est decoupee en plusieures bandes suivant les lignes.
 Un thread compute la convulution par bande.

\section Entree
  liens Algos nommes -mask ou -input
 * Deux liens input : convolution entre deux signaux (images).
 * Deux liens mask  : convolution entre deux mask.
 * Lien input et lien mask : Convolultion entre un signal et un mask.

\section Sortie
  Resultat de la convolution sur s1.

\file
\ingroup f_normal_convolution
 **/


/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/conv_data.h>
#include <pthread.h>
#include "net_message_debug_dist.h"

#define NB_MAX_THREADS 1024

typedef struct type_arg_convol
{
	int b;
	int mask_size_x;
	int mask_size_y;
	int input_begin;
	int input_lenght;
	int input_size_x;
	int input_size_y;
	float* mask;
	int size_x;
	int size_y;
	int deb;
	int longueur;
	int nligne ;
	int no_thread;
} type_arg_convol;



void process_convol (void *arg)
{
	int inc;
	int pas;
	int p1, p, p2, x, y;
	int i/*,j*/;
	int a, /*b,*/ a1, b1;
	float Si;
	type_arg_convol *my_arg;

	my_arg = (type_arg_convol *) arg;

	pas =  my_arg->input_lenght / (my_arg->input_size_x * my_arg->input_size_y);
	/*j = my_arg->deb;*/
	inc = my_arg->mask_size_x / 2 + my_arg->mask_size_y / 2 * my_arg->mask_size_x;
	p1= my_arg->deb + my_arg->size_x * my_arg->b;
	for (a = 0; a < my_arg->size_x; a++)
	{
		i = p1 + a ;   /* index in convolution map */
		for (x = -my_arg->mask_size_x / 2; x < (1 + my_arg->mask_size_x) / 2; x++)  /* x_index in mask */
			for (y = -my_arg->mask_size_y / 2; y < (1 + my_arg->mask_size_y) / 2; y++)  /* y_index in mask */
			{
				p = inc + x + y * my_arg->mask_size_x;  /* index in mask map */
				a1 = a + x;
				b1 = my_arg->b + y;
				if (a1 < 0)                     a1 = my_arg->size_x + a1;   /* test bords, convolution circulaire */
				else if (a1 >= my_arg->size_x)  a1 = a1 - my_arg->size_x;
				if (b1 < 0)                     b1 = my_arg->size_y + b1;   /* test bords, convolution circulaire */
				else if (b1 >= my_arg->size_y)  b1 = b1 - my_arg->size_y;
				p2 = my_arg->input_begin + (a1 + b1 * my_arg->size_x) * pas + pas - 1;  /* index in input map */

				neurone[i].d += neurone[p2].s1 * my_arg->mask[p];   /* lateral interactions */
			}
	}


	for (i = my_arg->deb; i < my_arg->deb + my_arg->longueur; i++)
	{
		Si = neurone[i].d;

		neurone[i].s1 = Si;     /* rajout pour avoir une version non seuille dans s1 24/04/2003 Olivier Ledoux */
		if(Si<-1.) neurone[i].s2=neurone[i].s=-1.;
		else  if(Si<1.) neurone[i].s2=neurone[i].s=Si;
		else  neurone[i].s=neurone[i].s2=1.;

		/* neurone[i].s=neurone[i].s1=neurone[i].s2=Si; */
	}

}

void *convol_nlignes(void *arg)
{
	type_arg_convol *my_arg;
	int k;
	int depart;
	my_arg=( type_arg_convol*) arg;
	depart = my_arg->b;

	for(k=my_arg->b;k<depart+my_arg->nligne;k++)
	{
		dprints("\n ligne traitee = %d pour le type %d",k,my_arg->deb);
		my_arg->b=k; /* attention y est maintenant le No de la ligne courante */
		process_convol(my_arg);  /* lance le calcul a partir de la ligne k */
	}
	return NULL;
}

void convolution_thread (int mask_size_x,int mask_size_y,int input_begin,int input_lenght,int input_size_x,int input_size_y,float* mask,int size_x,int size_y,int deb,int longueur)
{
	int b, n, j;

	type_arg_convol arg[NB_MAX_THREADS];
	pthread_t un_thread[NB_MAX_THREADS];
	void *resultat;
	int res, nb_threads;
	int nligne;

	nb_threads = input_size_y/mask_size_y; /*nombre de thread*/
	if (mask_size_x == 1 || mask_size_y == 1 || input_size_x == 1 || input_size_y == 1)
		nb_threads = 1;

	nligne = input_size_y/nb_threads ; /*nbre de lignes par bande*/


	dprints("nombre de threads a lancer = %d pour le type %d\n",nb_threads,deb);


	/*for(n=nb_threads-1;n--;)*/ /*boucle inversee dernier thread premier a demarrer ***plus safe!!!!****/
	for(n=0;n<nb_threads;n++)
	{
		b = n * nligne;

		arg[n].b=b;
		arg[n].mask_size_x = mask_size_x;   arg[n].mask_size_y=mask_size_y;
		arg[n].input_begin=input_begin;
		arg[n].input_lenght=input_lenght;
		arg[n].input_size_x=input_size_x;   arg[n].input_size_y=input_size_y;
		arg[n].mask=mask;
		arg[n].size_x=size_x;               arg[n].size_y=size_y;
		arg[n].deb=deb;arg[n].longueur=longueur;
		arg[n].nligne= nligne ;          /*nb_bandes est le nombre de ligne par bande*//*nbre*2*l*/ /*+1*/
		arg[n].no_thread=n;

		dprints("lance thread %d , traitant  %d  ligne de (%d) a (%d)\n ",arg[n].no_thread,arg[n].nligne,arg[n].b,arg[n].b+arg[n].nligne-1);

		res = pthread_create(&(un_thread[n]), NULL, convol_nlignes, (void *) &(arg[n]));

		if (res != 0)
		{
			kprints("fealure on thread creation \n");
			exit(1);
		}
	}

	dprints("---- %d threads on ete lances ---\n",n);

	for (j = 0; j < nb_threads; j++)
	{
		res = pthread_join(un_thread[j], &resultat);
		if (res == 0)
		{
			dprints("thread %d recueilli \n",j);
		}
		else
		{
			kprints("echec de pthread_join %d pour le thread %d\n", res, j);
			exit(0);
		}
	}
	dprints("execution THREADS terminee \n");
}
void function_normal_convolution(int numero)
{
	int deb, longueur;
	int no_input;
	int i, j;
	float *mask = NULL;
	int mask_begin = 0, mask_lenght = 0;
	int mask_size_x = 0, mask_size_y = 0;
	int input_begin = 0, input_lenght = 0;
	int input_size_x = 0, input_size_y = 0;
	int size_x, size_y;
	/* type_arg_convol my_arg;*/
	float time_float = 0;       /* time constant for the group memory stored in stemps field of the link */
	int dynamic_mask = 0 ;
#ifdef TIME_TRACE
	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

	longueur = def_groupe[numero].nbre;
	deb = def_groupe[numero].premier_ele;
	size_x = def_groupe[numero].taillex;
	size_y = def_groupe[numero].tailley;

	dprints("Entrer in f_n_convol: ");


	if (def_groupe[numero].data == NULL)
	{
		/*       if(trace>0) printf("create extention data for the normal convolution \n"); */
		/* recupere sur le lien l'information sur la periode de la sequence */

		for (i = 0; i < nbre_liaison; i++)
		{
			if (liaison[i].arrivee == numero
					&& strstr(liaison[i].nom, "mask") != NULL)
			{
				no_input = liaison[i].depart;
				mask_begin = def_groupe[no_input].premier_ele;
				mask_lenght = def_groupe[no_input].nbre;
				mask_size_x = def_groupe[no_input].taillex;
				mask_size_y = def_groupe[no_input].tailley;
				mask = (float *) malloc(sizeof(float) * mask_lenght);

				if( strstr(liaison[i].nom,"dyn") != NULL)
				{
					dynamic_mask = 1;
				}
			}
			if (liaison[i].arrivee == numero
					&& strcmp(liaison[i].nom, "input") == 0)
			{
				no_input = liaison[i].depart;
				input_begin = def_groupe[no_input].premier_ele;
				input_lenght = def_groupe[no_input].nbre;
				input_size_x = def_groupe[no_input].taillex;
				input_size_y = def_groupe[no_input].tailley;
				time_float = liaison[i].stemps;
			}
		}

		if (size_x != input_size_x || size_y != input_size_y)
			EXIT_ON_ERROR("error the size of the input group (%d, %d) is not the same as the normal convolution groupe (%d,%d),   GPE = %d\n",input_size_x, input_size_y, size_x, size_y, numero);
			

		for (i = mask_begin, j = 0; i < mask_begin + mask_lenght; i++, j++)
		{
			mask[j] = neurone[i].s;
		}


		def_groupe[numero].data = (void *) malloc(sizeof(conv_data));

		(*((conv_data *) def_groupe[numero].data)).mask_begin = mask_begin;
		(*((conv_data *) def_groupe[numero].data)).mask_lenght = mask_lenght;
		(*((conv_data *) def_groupe[numero].data)).mask_size_x = mask_size_x;
		(*((conv_data *) def_groupe[numero].data)).mask_size_y = mask_size_y;
		(*((conv_data *) def_groupe[numero].data)).input_begin = input_begin;
		(*((conv_data *) def_groupe[numero].data)).input_lenght =
				input_lenght;
		(*((conv_data *) def_groupe[numero].data)).input_size_x =
				input_size_x;
		(*((conv_data *) def_groupe[numero].data)).input_size_y =
				input_size_y;
		(*((conv_data *) def_groupe[numero].data)).time_float = time_float;
		(*((conv_data *) def_groupe[numero].data)).mask = mask;
		(*((conv_data *) def_groupe[numero].data)).dynamic_mask = dynamic_mask;

	}
	else
	{
		/*       if(trace>0) printf("instantiate local variables from the group extention \n"); */
		mask_begin = (*((conv_data *) def_groupe[numero].data)).mask_begin;
		mask_lenght = (*((conv_data *) def_groupe[numero].data)).mask_lenght;
		mask_size_x = (*((conv_data *) def_groupe[numero].data)).mask_size_x;
		mask_size_y = (*((conv_data *) def_groupe[numero].data)).mask_size_y;
		input_begin = (*((conv_data *) def_groupe[numero].data)).input_begin;
		input_lenght =
				(*((conv_data *) def_groupe[numero].data)).input_lenght;
		input_size_x =
				(*((conv_data *) def_groupe[numero].data)).input_size_x;
		input_size_y =
				(*((conv_data *) def_groupe[numero].data)).input_size_y;
		time_float = (*((conv_data *) def_groupe[numero].data)).time_float;

		mask = (*((conv_data *) def_groupe[numero].data)).mask;
		dynamic_mask = (*((conv_data *) def_groupe[numero].data)).dynamic_mask;

		/*RECOPIE DU MASK : la recopie ne s'effectue uniquement dans le cas ou l'option -dyn est ajout�� dans le lien mask */
		if( dynamic_mask == 1 )
		{
			for (i = mask_begin, j = 0; i < mask_begin + mask_lenght; i++, j++)
			{
				mask[j] = neurone[i].s;
			}
		}
	}

	for (i = deb; i < deb + longueur; i++)
		neurone[i].d = 0.0;     /* to store the sum of the local interaction effect */

	dprints("\ndebut = %d, taille = %d",deb,longueur);
	convolution_thread(mask_size_x,mask_size_y,input_begin,input_lenght,input_size_x,input_size_y,mask,size_x,size_y,deb,longueur);

	dprints("Exit from f_n_convol: ");

#ifdef TIME_TRACE
	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
	if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	else
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
				1;
		MicroSecondesFunctionTimeTrace =
				1000000 + OutputFunctionTimeTrace.tv_usec -
				InputFunctionTimeTrace.tv_usec;
	}
	sprintf(MessageFunctionTimeTrace,
			"Time in function_normal_convolution\t%4ld.%06d\n",
			SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
	affiche_message(MessageFunctionTimeTrace);
#endif
}
