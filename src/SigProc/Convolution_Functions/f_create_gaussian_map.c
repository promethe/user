/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
\defgroup f_create_gaussian_map f_create_gaussian_map
\ingroup libSigProc

\brief Centered Gaussian map usefull for convolution

\section Description

Cree une carte dont les neurones encode un profil de type gaussien centree sur le milieu de la carte (prendre un nombre de neurones impairs. (utile pour les convolutions par exemple)
dx : distance selon l'axe x dans la carte neuronale
dy : distance selon y.
\f[

act = N * e^{ - \frac{dx*dx + dy*dy}{2*s*s}}

\f]

L'equation en 2D n'est pas l'equation classique pour une fonction
gaussienne. Il manque le produit x par y.

\section Links

-N : un reel flottant : la valeur max de la gaussienne
Si aucune valeur n'est fournie, alors la gaussienne est normalisee par \f$ \frac{1}{\sqrt(2\pi\sigma^2)} \f$
Si l'option n'est pas presente alors le coefficient de normalisation est 1.

-s : valeur du sigma de la gaussienne

-g : un flottant : ratio entre la valeur max  et  la valeur min sur les neurones les plus eloignes dans la carte (valide si pas de sigma passe en parametre), revient a un facteur de proportionalite entre la valeur la plus grande et la valeur plus petite.

-p : un flottant compris entre 0 et 1 : permet l'evaluation de la taille du champ par rapport a une precision donnee. Des messages d'info donne la taille suffisante pour avoir au moins 100*p % de la Gaussienne qui soit encodee sur le champ. En reduisant a la valeur suggeree on peut maitriser un compromis precision - taille du champ.

NB: L'option -p n'est disponible que pour les champs a une dimension.

-w : with modulation , peut modifier le sigma en fonction du neurones d'entrée


\file
\ingroup f_create_gaussian_map
modification
author = A. de Rengerve
date = 12/08/2009
description = specific file creation 

*/


/* #define DEBUG */
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <math.h>


typedef struct my_data {
  int gpe_deb;
  int sizeX;
  int sizeY; 
  int with_modulation;
} myData;


/**
 * see f_create_gaussian_map in modules
 * @param num group number
 */
void function_create_gaussian_map(int num) {
  myData *mydata=NULL;
  int i=0,j,ret=-1,x=-1,y=-1;
  float N = 1.;
  float p = 1.;
  float sigma = -1.;
  float gamma = 100.;
  int sizeX= -1;
  int sizeY= -1;
  char param[256];
  float centerX=-1.;
  float centerY=-1.;
  float tmp_f = 0.;
  float d_x, d_y;
  float accu=0, integ=0;
  

   
   
  dprints ("execution de create gaussian map\n");
  if(def_groupe[num].data==NULL) {
    mydata = ALLOCATION(myData);
    
    mydata->gpe_deb = def_groupe[num].premier_ele;
    mydata->sizeX = sizeX = def_groupe[num].taillex;
    mydata->sizeY = sizeY = def_groupe[num].tailley;
    mydata->with_modulation=0;
    def_groupe[num].data = mydata;
  }
  else
  {
	mydata = def_groupe[num].data;
	sizeX =  mydata->sizeX;
	sizeY = mydata->sizeY;
	// Le champ de neuronne n'est rempli qu'au premier appel de la fonction
	// si le champ n'est pas modulé, il n'y a pas de raison de recalculer sa valeur
	if( mydata->with_modulation == 0 ) return;
  }

    j=find_input_link(num,0);
    if(j!=-1) {
      ret=prom_getopt(liaison[j].nom,"-N",param);
      if(ret==2)
      {
	N = atof(param);
      }
      if(prom_getopt(liaison[j].nom,"-g",param)==2)
      {
	gamma = atof(param);
      }
      if(prom_getopt(liaison[j].nom,"-s",param)==2)
      {
	sigma = atof(param);
      }
      if(prom_getopt(liaison[j].nom,"-p",param)==2)
      {
	p = atof(param); /* only useful if 1D field */
      }
      if(prom_getopt(liaison[j].nom,"-w",param)==1)
      {
	mydata->with_modulation=1;
      }
    }
    
    /* calcul du centre de la gaussienne */
    centerX = (float)(sizeX-1) / 2.;
    centerY = (float)(sizeY-1) / 2.;
            
    /* calcul de sigma */
    if( mydata->with_modulation == 1)
    {
	sigma = sigma * neurone[ neurone[mydata->gpe_deb].coeff->entree ].s * neurone[mydata->gpe_deb].coeff->val;
    }	
    if(sigma<0.) {
      sigma = sqrt((centerX*centerX + centerY*centerY)/(2*log(gamma)));
      dprints("gamma %f \n",gamma);
    }
    
    if(isequal(sigma,0.)) sigma =1;

    /** definition of N if normalized Gaussian */
    if(ret==1)
      N=1/(sqrt(2*M_PI)*sigma);
    
    dprints("centerX %f / centerY %f \n", centerX, centerY);
    
    dprints("N %f sigma %f\n",N,sigma);
    for(i = 0; i<sizeX; i++) 
      for(j=0; j<sizeY; j++) {
	d_x = (float)i - centerX;
	d_y = (float)j - centerY;
/* 	 dprints("d_x %f / d_y %f \n", d_x, d_y); */
/* 	 dprints("res int %f \n",(d_x*d_x + d_y*d_y)); */
	
	/** Particular 2 dimensional gaussian function !! */
	
	tmp_f = N * exp( - (d_x*d_x + d_y*d_y) / (2.*sigma*sigma));
	
	neurone[mydata->gpe_deb+j*sizeX+i].s=neurone[mydata->gpe_deb+j*sizeX+i].s1=
	neurone[mydata->gpe_deb+j*sizeX+i].s2=tmp_f;

	accu+=tmp_f;
      }

    /** calcul de la proportion a garder */
    /** only for 1D fields */
    if((sizeX-1)*(sizeY-1)==0) {
    
      /** verif improvement is possible */
    //  cprints("Create_gaussian_map (%s)\n\t>> La precision atteinte avec tout le champ est de %f:\n\t>> Valeur min actuelle : %f\n",def_groupe[num].no_name,accu/(N*(sqrt(2*M_PI)*sigma)),neurone[mydata->gpe_deb].s1);
	integ = 1. - accu/(N*(sqrt(2*M_PI)*sigma));
/* 	if(accu/(N*(sqrt(2*M_PI)*sigma))>=p) { */
	if(integ<(1-p)/2) {
	  for(i = 0; i<mydata->sizeX && integ<(1-p)/2; i++) 
	    for(j=0; j<mydata->sizeY && integ<(1-p)/2; j++) {
	      integ+=neurone[mydata->gpe_deb+j*mydata->sizeX+i].s1/(N*(sqrt(2*M_PI)*sigma));      
	      x=j;
	      y=i;
	    }
	 // cprints("Create_gaussian_map (%s) \n\t>> La taille correspondant a une precision de %f est size_x :%d, size_y:%d\n\t>> Valeur min correspondante : %f\n",def_groupe[num].no_name,p,mydata->sizeX-2*(y),mydata->sizeY-2*(x),neurone[mydata->gpe_deb+(x+(y+1)/sizeX)*mydata->sizeX+(y+(1%sizeX))].s1);
	}
    }
 /* }
   mydata = def_groupe[num].data;
*/

  dprints("sortie de create gaussian map (%d)\n",num);   
}

