/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>

void function_file_mem_oscilo(int gpe_sortie)
{
    char file_name[255];
    int gpe_entre, debut_entre, taille_entre, lien_entree = -1;
    int i;
    int *j;
    FILE *f;

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        def_groupe[gpe_sortie].ext = (void *) malloc(sizeof(int));
        j = (int *) def_groupe[gpe_sortie].ext;

        /* recupere sur le lien l'information sur le groupe de depart */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == gpe_sortie)
            {
                strcpy(file_name, liaison[i].nom);
                gpe_entre = liaison[i].depart;
                break;
            }

        /* on stocke la liasion */
        *j = lien_entree = i;

        /* on efface le fichier */
        f = fopen(file_name, "w");
        if (f == NULL)
        {
            printf("ERROR unable to open file %s \n", file_name);
            exit(-1);
        }
        fclose(f);
    }
    else
    {
        j = (int *) def_groupe[gpe_sortie].ext;
        lien_entree = *j;
    }


    strcpy(file_name, liaison[lien_entree].nom);
    gpe_entre = liaison[lien_entree].depart;
    debut_entre = def_groupe[gpe_entre].premier_ele;
    taille_entre = def_groupe[gpe_entre].nbre;

    f = fopen(file_name, "a+");
    if (f == NULL)
    {
        int try = 0;
        do
        {
            printf("try to open %s %d\n", file_name, try);
            f = fopen(file_name, "a+");
            try++;
        }
        while (try < 10 && f == NULL);
        printf("ERROR unable to open file %s \n", file_name);
        exit(-1);
    }

    for (i = debut_entre; i < debut_entre + taille_entre; i++)
        fprintf(f, "%f ", neurone[i].s);
    fprintf(f, "\n");
    fclose(f);

}
