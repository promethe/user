/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
defgroup f_interpolation_lineaire f_interpolation_lineaire
ingroup libSigProc
Calcule une interpolation lineaire entre deux entrees.

\file
\brief

Author: G. Merle
Created: 02/05/2011
Modified:
- author: xxxxxxxx
- description: xxxxxxxx
- date: XX/XX/XXXX

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
-----------------------------------------------------------------------
Calcule une interpolation linéaire entre deux entrées.
Entrées :
    'sel' : signal de sélection, détermine quelle entrée est activée
    '0' : entrée active lorsque sel = 0
    '1' : entrée active lorsque sel = 1
Si sel n'a qu'un neurone, alors il sélectionne a lui seul tous les neurones des
entrées. Sinon, chaque neurone de sel sélectionne uniquement le neurone 
correspondant des entrées.
-----------------------------------------------------------------------

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/xxx
- input expected group: sel, 0, 1
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <stdio.h>
#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>
#include <NN_Core/macro_colonne.h>


typedef struct data_interpolation {
    int gpe_0, gpe_1, gpe_sel;
} data_interpolation;

void new_interpolation_lineaire(int gpe) {
    int l, i;
    data_interpolation* data = NULL;
    if (def_groupe[gpe].data == NULL) {
        def_groupe[gpe].data = data = (data_interpolation*) malloc(sizeof(data_interpolation));
        if (data == NULL) EXIT_ON_ERROR("error: malloc fail, in new_interpolation_lineaire(gpe = %i)\n", gpe);
        else {
            /* zero-ing everything */
            data->gpe_sel = data->gpe_0 = data->gpe_1 = -1;
            
            /* then we examine input links */
            for (i = 0; (l = find_input_link(gpe,i)) != -1; ++i) {
                if (0 == strcmp(liaison[l].nom,"sel")) {
                    data->gpe_sel = liaison[l].depart;
                    if (def_groupe[data->gpe_sel].nbre != 1 && 
                        def_groupe[data->gpe_sel].nbre != def_groupe[gpe].nbre)
                    EXIT_ON_ERROR("error: 'sel' input link size mismatch, expected 1 or %i, got %i, in new_interpolation_lineaire(gpe = %i)\n", def_groupe[gpe].nbre, def_groupe[data->gpe_sel].nbre, gpe);
                    continue;
                }
                if (0 == strcmp(liaison[l].nom,"0")) {
                    data->gpe_0 = liaison[l].depart;
                    if (def_groupe[data->gpe_0].nbre != def_groupe[gpe].nbre)
                    EXIT_ON_ERROR("error: '0' input link size mismatch, expected %i, got %i, in new_interpolation_lineaire(gpe = %i)\n", def_groupe[gpe].nbre, def_groupe[data->gpe_0].nbre, gpe);
                    continue;
                }
                if (0 == strcmp(liaison[l].nom,"1")) {
                    data->gpe_1 = liaison[l].depart;
                    if (def_groupe[data->gpe_1].nbre != def_groupe[gpe].nbre)
                    EXIT_ON_ERROR("warning: '1' input link size mismatch, expected %i, got %i, in new_interpolation_lineaire(gpe = %i)\n", def_groupe[gpe].nbre, def_groupe[data->gpe_1].nbre, gpe);
                    continue;
                }
            }
            if (data->gpe_sel == -1)
            fprintf(stderr, "error: 'sel' input link absent, in new_interpolation_lineaire(gpe = %i)\n", gpe);
            if (data->gpe_0 == -1)
            fprintf(stderr, "error: '0' input link absent, in new_interpolation_lineaire(gpe = %i)\n", gpe);
            if (data->gpe_1 == -1)
            fprintf(stderr, "error: '1' input link absent, in new_interpolation_lineaire(gpe = %i)\n", gpe);
        }
    }
}

void destroy_interpolation_lineaire(int gpe) {
    if (def_groupe[gpe].data) {
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

void function_interpolation_lineaire(int gpe) {
    int i, nbr_gpe, deb_gpe, nbr_sel, deb_sel, deb_0, deb_1;
    float tmp;
    
    data_interpolation * data = (data_interpolation *) def_groupe[gpe].data;
    
    if (data == NULL) {
        return;
    }
    
    if (data->gpe_sel == -1 || data->gpe_0 == -1 || data->gpe_1 == -1) {
        return;
    }
    
    nbr_gpe = def_groupe[gpe].nbre;
    deb_gpe = def_groupe[gpe].premier_ele;
    nbr_sel = def_groupe[data->gpe_sel].nbre;
    deb_sel = def_groupe[data->gpe_sel].premier_ele;
    deb_0 = def_groupe[data->gpe_0].premier_ele;
    deb_1 = def_groupe[data->gpe_1].premier_ele;
    
    if (nbr_sel == 1) {
        tmp = neurone[deb_sel].s1;
        for (i = 0; i < nbr_gpe; ++i)
        neurone[deb_gpe+i].s = neurone[deb_gpe+i].s1 = neurone[deb_gpe+i].s2 = 
            neurone[deb_0+i].s1 + (neurone[deb_1+i].s1 - neurone[deb_0+i].s1) * tmp;
    } else if (nbr_sel == nbr_gpe) {
        for (i = 0; i < nbr_gpe; ++i) {
        tmp = neurone[deb_sel+i].s1;
        neurone[deb_gpe+i].s = neurone[deb_gpe+i].s1 = neurone[deb_gpe+i].s2 = 
            neurone[deb_0+i].s1 + (neurone[deb_1+i].s1 - neurone[deb_0+i].s1) * tmp;
        }
    } else {
        for (i = 0; i < nbr_gpe; ++i)
        neurone[deb_gpe+i].s = neurone[deb_gpe+i].s1 = neurone[deb_gpe+i].s2 = 
            0.;
    }
}

