
/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/******************************
\ingroup libSigProc
\defgroup f_stats.c f_stats.c
 *
 *
\date Oct 16, 2012
\author Ali Karaouzene
\brief

Cette fonction calcul :
- L'erreur quadratique moyenne entre deux boites.
EQM = sum (X_i -Y_i)^2 / (taille_gpe * nb_iterations)

- Taux de succes
 Récupère le gagnants des deux boites et compare les positions
 * Si les positions sont les mêmes "success ++" 
 * Sinon "echec ++"
-

Options sur les liens :
-"-S" : Groupe de supervision (sorties desiree)
-"-T" : Groupe de test (ex. Sortie LMS)
-"-L" : Seuil de decision
-"-M" : Calculer uniquement la moyenne quadratique.
-"-ALL" : Calculer la moyenne et les autres statistiques
-"-reset" : Remise à zero des : moyenne, stats, iterations, si le 1er neurone du groupe est egal a 1

\section description
 ***************************************************************************************************************************/

/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


#define CORRECT 0
#define FALSE_POSITIVE 1
#define FALSE_NEGATIVE 2
/*#define SEUIL 0.8*/
#define TAILLETAB 6

typedef struct Mydata_stats
{
	int iterations;             /* nombre d'iterations */
	float erreur_quad;			/*erreur quadratique*/
	float erreur_quad_moy;		/*erreur quadratique moyenne*/
	float sum_erreur_quad;
	float seuil;                /* seuil de la bonne reponse */
	int nb_result;
	int moyenneOnly;
	int allOutput;
	float * tabResult[TAILLETAB];
	float success;              /* nombre de prototypes correctement reconnus */
	float fail;
	float false_positive;       /* indique le nombre de fois que la sortie est plus grand que le seuil erronement */
	float false_negative;       /* indique le nombre de fois que la sortie est moins grand que le seuil erronement */
	int nb_success;
	int nb_fail;
	int nb_false_negative;
	int nb_false_positive;
	int pourcent;
	int gpe_supervision;        /* numero du groupe d'entree correspondant a l'apprentissage */
	int gpe_test;               /* numero du groupe d'entree de supervision */
	int gpe_transition_detect;
	int trans_detect;
	FILE * statdf;
	char statfilename[255];
	int infile;
} Mydata_stats;


void new_stats(int Gpe)
{
	int 	n, link,i = 0,j = 0;
	int 	longueur_n_test;        /* numero de neurones du groupe de test */
	char 	resultat[256];
	char 	*string   = NULL;
	char    param_link[256];
	int		taille_sup;
	Mydata_stats *mydata;

	mydata = (Mydata_stats *) malloc(sizeof(Mydata_stats));
	if (mydata== NULL)
		EXIT_ON_ERROR("Erreur d'allocation de mydata %s",__FUNCTION__);



	/* initialisation de la structure */

	dprints("initialisation structure mydata dans (%s) groupe %s\n", __FUNCTION__,def_groupe[Gpe].no_name);

	mydata->gpe_supervision =0;
	mydata->gpe_test = 0;
	mydata->iterations = 0;
	mydata->erreur_quad_moy = 0.;
	mydata->erreur_quad = 0.;
	mydata->sum_erreur_quad = 0.;
	mydata->trans_detect = 0;
	mydata->nb_result = 1;
	mydata->success = 0;
	mydata->fail = 0;
	mydata->false_positive = 0;
	mydata->false_negative = 0;
	mydata->nb_success = 0;
	mydata->nb_fail = 0;
	mydata->nb_false_negative = 0;
	mydata->nb_false_positive = 0;
	mydata->pourcent = 0;
	mydata->moyenneOnly = 0;
	/*mydata->seuil = SEUIL;*/
	mydata->infile = 0;


	dprints("initialisation structure mydata (%s) iterations = %d success = %g, seuil = %f\n",__FUNCTION__, mydata->iterations, mydata->success, mydata->seuil);

	/* Le groupe doit obligatoirement avoir deux entrees :
	 * On compare la sortie du reseau avec la sortie desiree. */

	n = 0;
	link = find_input_link(Gpe, n);
	while (link != -1)
	{
		string = liaison[link].nom;
		/* Groupe de sortie desirees (supervision )*/
		if (prom_getopt(string, "-S", param_link) == 1 || prom_getopt(string, "-s", param_link) == 1)
		{
			mydata->gpe_supervision = liaison[link].depart;
			dprints("supervision input group : %s (%s %s) \n",def_groupe[mydata->gpe_supervision].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
		}
		/* Groupe de 'test' : Sortie apprise (ex : sortie du LMS) */
		if (prom_getopt(string, "-T", param_link) == 1 || prom_getopt(string, "-t", param_link) == 1)
		{
			mydata->gpe_test = liaison[link].depart;
			dprints("test input group : %s (%s %s) \n",def_groupe[mydata->gpe_test].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
		}
		/* Sortie des taux en pourcentage ou entre 0 et 1 */
		if (prom_getopt(string, "-P", param_link) == 1 || prom_getopt(string, "-p", param_link) == 1)
		{
			mydata->pourcent = 1;
			dprints("Sortie en pourcentage (%s %s) \n",__FUNCTION__, def_groupe[Gpe].no_name);
		}
		/* Pour specifier le seuil */
		if (prom_getopt(liaison[link].nom, "-L", resultat) == 2 || prom_getopt(liaison[link].nom, "-l", resultat) == 2)
		{
			mydata->seuil = atof(resultat);
			dprints("mydata->seuil = %g (%s %s) \n", mydata->seuil,__FUNCTION__,def_groupe[Gpe].no_name );
		}
		/* Specifier les stats a calculer */
		if (prom_getopt(liaison[link].nom, "-M", resultat) == 1 || prom_getopt(liaison[link].nom, "-m", resultat) == 1)
		{
			mydata->moyenneOnly = 1;
			dprints("mydata->nb_result = %d (%s %s) \n", mydata->nb_result,__FUNCTION__,def_groupe[Gpe].no_name );
		}
		/* Specifier les stats a calculer */
		/*if (prom_getopt(liaison[link].nom, "-MULTI", resultat) == 1 || prom_getopt(liaison[link].nom, "-multi", resultat) == 1)
		{
			mydata->multi = 1;
			dprints("mydata->multi = %d (%s %s) \n", mydata->multi,__FUNCTION__,def_groupe[Gpe].no_name );
		}*/
		/*fichier enregistrer les stats*/
		if (prom_getopt(liaison[link].nom, "-f", resultat) == 2 || prom_getopt(liaison[link].nom, "-F", resultat) == 1)
		{
			strcpy(mydata->statfilename,resultat);
			mydata->infile = 1;
			dprints("fichier des statistiques = %s\n", mydata->statfilename);
		}

		/* Pour remise a zero a la demande */
		/*if (prom_getopt(liaison[link].nom, "transition_detect", resultat) == 1 || prom_getopt(liaison[link].nom, "trans_detect", resultat) == 1)*//*NE MARCHE PAS ????*/
		if (prom_getopt(liaison[link].nom, "-reset", resultat) == 1 || prom_getopt(liaison[link].nom, "-RESET", resultat) == 1)
		{
			mydata->trans_detect = 1;
			mydata->gpe_transition_detect = liaison[link].depart;
			dprints("Transition detect link (group %s) found in (%s %s) \n",def_groupe[mydata->gpe_transition_detect].no_name, __FUNCTION__,def_groupe[Gpe].no_name );
		}

		n++;
		link = find_input_link(Gpe, n);
	}
	
	
	if (mydata->gpe_supervision == 0 || mydata->gpe_test == 0)
		EXIT_ON_ERROR ("Le groupe %s (%s) doit avoir au moins deux liens en entree. Sortie desiree (-s), sortie du reseau (-t) et (optionel) transition_detect",
				def_groupe[Gpe].no_name, __FUNCTION__);

	/*if (def_groupe[Gpe].nbre != mydata->nb_result)
		EXIT_ON_ERROR ("Le nombre de neurones du groupe (%s) est different du nombre de resultats attendus (%d,%d). Verifiez les options sur les liens",
				def_groupe[Gpe].no_name,def_groupe[Gpe].nbre, mydata->nb_result);*/

	longueur_n_test = def_groupe[mydata->gpe_test].nbre;
	taille_sup = def_groupe[mydata->gpe_supervision].nbre;
	/* verification que les donnees d'entree sont comparables */
	if (taille_sup != longueur_n_test)
		EXIT_ON_ERROR ("the input groups in group %s (%s) must have the same number of neurons\n",def_groupe[Gpe].no_name, __FUNCTION__);

	/*Creer un tableau de resultat par categorie, 4 nombre de stats (Vrai,Faux) positif (Vrai,Faux) Negatif*/
	for( i = 0; i < TAILLETAB; i++)
	{
		mydata->tabResult[i] = (float *) malloc ( taille_sup * sizeof(float));
		if (mydata->tabResult[i] == NULL)
			EXIT_ON_ERROR("Erreur d'allocation de tabResult[%d] %s",i,__FUNCTION__);
	}
	for (i = 0; i<TAILLETAB; i++)
		for(j=0;j<taille_sup;j++)
			mydata->tabResult[i][j] = 0;
	/*ouverture du fichier des stats*/
	if (mydata->infile)
	{
		mydata->statdf = fopen(mydata->statfilename,"w+");
		if(mydata->statdf==NULL)
			EXIT_ON_ERROR("Cannot open file %s in %s",mydata->statfilename,__FUNCTION__);
	}

	def_groupe[Gpe].data=mydata;
	dprints("END %s\n",__FUNCTION__);
}



void function_stats(int Gpe)
{
	int i = 0/*,j = 0,index_sup,index_test*/;
	Mydata_stats *mydata = NULL;
	/*float temp = 0., rate, false_negative, false_positive,tmp,erreur_quad;*/
	float tmp = 0.0/*,max = 0.0, max_sup = 0.0*/;
	/* donnees sur le groupe d'apprentissage */
	int debut = 0, first_neuron_sup = 0, taille_sup = 0, first_neuron_test = 0, taille_test = 0;
	int nb_result;
	float seuil;
	FILE *statdf;


	mydata = (Mydata_stats *) def_groupe[Gpe].data;
	if (mydata == NULL)
		EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name,__FUNCTION__);

	debut = def_groupe[Gpe].premier_ele;
	first_neuron_sup = def_groupe[mydata->gpe_supervision].premier_ele;
	taille_sup = def_groupe[mydata->gpe_supervision].nbre;
	taille_test = def_groupe[mydata->gpe_test].nbre;
	first_neuron_test = def_groupe[mydata->gpe_test].premier_ele;
	seuil = mydata->seuil;
	nb_result = mydata->nb_result;
	statdf = (FILE *) mydata->statdf;


	/*Calcul de l'erreur quadratique moyenne (Calcul en utilisant les champs non seuilles*/
	/*Remise a zero de l'erreur quad et du nombre d'itérations*/
	if(mydata->trans_detect == 1)
	{
		if (neurone[def_groupe[mydata->gpe_transition_detect].premier_ele].s1 > 0.5)
		{
			mydata->erreur_quad = 0.0;
			mydata->iterations = 0;
			mydata->nb_success = 0;
			mydata->nb_fail=0;
			mydata->nb_false_negative = 0;
			mydata->erreur_quad = 0.;
			mydata->sum_erreur_quad = 0.;
		}
	}

	mydata->iterations ++;
	dprints("ITERATION =  %d\n",mydata->iterations);

	/*Calcul de l'erreur quadratique moyenne*/
	mydata->erreur_quad =0.;
	tmp = 0;
	for (i = 0; i < taille_sup; i++)
	{
		tmp = (neurone[first_neuron_sup+i].s1 -  neurone[first_neuron_test+i].s);
		tmp=sqrt(tmp*tmp);

		mydata->erreur_quad = mydata->erreur_quad + tmp;
	}
	mydata->sum_erreur_quad += (mydata->erreur_quad/taille_sup);
	mydata->erreur_quad_moy = mydata->sum_erreur_quad /  mydata->iterations ;
	/*mydata->erreur_quad_moy /= mydata->iterations;*/
    
	dprints("erreur quad  = (%f - %f) %f\n",neurone[first_neuron_sup].s1,neurone[first_neuron_test].s,mydata->erreur_quad);
	dprints("somme erreur quad  = %f\n",mydata->sum_erreur_quad);				
	dprints("erreur quad moy  =  %f\n",mydata->erreur_quad_moy);
	/*Le calcul de l'erreur peut se faire seul pour gagner du temps*/
	if (mydata->moyenneOnly == 1)
	{
		neurone[debut].s = neurone[debut].s1 = neurone[debut].s2 =  mydata->erreur_quad_moy;
		return;
	}

	/*****STATS******/

	/*Parcourir les deux vecteurs vecteurs en entier et les comparer*/

	for (i = 0; i < taille_sup; i++)
	{
		//Le NN répond et il devait repondre => Vrai Positif
		if(neurone[first_neuron_sup+i].s1 > seuil)
		 mydata->tabResult[4][i] = mydata->tabResult[4][i] +1.; /*la categorie est rencontree*/
		else mydata->tabResult[5][i] = mydata->tabResult[5][i] +1.; /*categorie non vu*/

		//Le NN répond correctement => Vrai Positif
		if(neurone[first_neuron_test+i].s1 > seuil && neurone[first_neuron_sup+i].s1 > seuil)
		{
			mydata->tabResult[0][i]  = (mydata->tabResult[0][i] + 1.);
			mydata->nb_success ++;
		}
		//Le NN répond et il NE devait PAS repondre => Faux Positif
		else if (neurone[first_neuron_test+i].s1 > seuil && neurone[first_neuron_sup+i].s1 <= seuil)
		{
			mydata->tabResult[1][i] = (mydata->tabResult[1][i] + 1.) ;
			mydata->nb_fail ++;
		}
		//Le NN NE répond PAS et il NE devait PAS repondre => Vrai negatif
		else if (neurone[first_neuron_test+i].s1 <= seuil && neurone[first_neuron_sup+i].s1 <= seuil)
		{
			mydata->tabResult[2][i] = (mydata->tabResult[2][i] + 1.) ;
			mydata->nb_success ++;
		}
		//Le NN NE répond PAS et il devait repondre => Faux negatif
		else if (neurone[first_neuron_test+i].s1 <= seuil && neurone[first_neuron_sup+i].s1 > seuil)
		{
			mydata->tabResult[3][i] = (mydata->tabResult[3][i] + 1.) ;
			mydata->nb_fail ++;
		}
		else
			PRINT_WARNING("UNKNOWN CASE \n");
	}


	/*TAUX*/
/*	mydata->success = (float)mydata->nb_success / ((float)mydata->iterations *taille_sup ) ;  */
	mydata->success = (float)(mydata->nb_success-mydata->nb_fail) / ((float)mydata->iterations *taille_sup ) ;
	mydata->fail = (float)mydata->nb_fail / ((float)mydata->iterations * taille_sup);
	if (mydata->pourcent == 1)
	{
		mydata->success = mydata->success * 100;
		mydata->fail =  mydata->success *100;
	}

	//printf("nb_success = %d, taux_succes = %f nb_fail = %d, taux_fail = %f,  nb_iteration = %d\n",
	//mydata->nb_success,mydata->success,mydata->nb_fail,mydata->fail,mydata->iterations);
	/*********END STAT******/


	/*Recopier les resultats dans les neurones de sortie*/
	/*Premier neurone : Erreur Quadratique Moyenne*/
	neurone[debut].s = neurone[debut].s1 = neurone[debut].s2 =  mydata->erreur_quad_moy;
	neurone[debut+1].s = neurone[debut+1].s1 = neurone[debut+1].s2 =  mydata->success;
	/*neurone[debut+2].s = neurone[debut+2].s1 = neurone[debut+2].s2 =  mydata->false_negative;
	neurone[debut+3].s = neurone[debut+3].s1 = neurone[debut+3].s2 =  mydata->false_positive;*/

	/********************************************************************************************************************/
	/********************************************************************************************************************/




}

void destroy_stats(int Gpe)
{
	Mydata_stats *mydata = NULL;
	int i;
	FILE *statdf;
	int taille_sup = 0;

	mydata = (Mydata_stats *) def_groupe[Gpe].data;
	if (mydata == NULL)  EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name,__FUNCTION__);
	if (mydata->infile)
	{
		statdf = mydata->statdf;
		taille_sup  = def_groupe[mydata->gpe_supervision].nbre;
		/*Affichage dans un fichier*/
		/*for (j = 0; j<4; j++)
		{*/
		fprintf(statdf,"\nVrai Positif | ");
		for (i=0 ; i<taille_sup; i++)
		{
			if(mydata->tabResult[4][i]>0) fprintf(statdf, "%0.3f | ",mydata->tabResult[0][i] /mydata->tabResult[4][i] );
			else fprintf(statdf, " NaN  | ");
		}
		fprintf(statdf,"\nFaux Negatif | ");
		for (i=0 ; i<taille_sup; i++)
		{
			if(mydata->tabResult[4][i]>0) fprintf(statdf, "%0.3f | ",mydata->tabResult[3][i] /mydata->tabResult[4][i] );
			else fprintf(statdf, " NaN  | ");
		}
		fprintf(statdf,"\nFaux Positif | ");
		for (i=0 ; i<taille_sup; i++)
		{
			if(mydata->tabResult[5][i]>0) fprintf(statdf, "%0.3f | ",mydata->tabResult[1][i] /mydata->tabResult[5][i] );
			else fprintf(statdf, " NaN   | ");
		}
		fprintf(statdf,"\nVrai Negatif | ");
		for (i=0 ; i<taille_sup; i++)
		{
			if(mydata->tabResult[5][i]>0) fprintf(statdf, "%0.3f | ",mydata->tabResult[2][i] /mydata->tabResult[5][i] );
			else fprintf(statdf, " NaN  | ");
		}
		fprintf(statdf,"\n");
		fclose(((Mydata_stats *) def_groupe[Gpe].data)->statdf);
	}
	def_groupe[Gpe].data=NULL;
	free(def_groupe[Gpe].data);

}
