/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_erreur_prediction.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:  Computes the error between 2 groups (same size is expected but not necessary)

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/


void function_erreur_prediction(int numero)
{

	int i = 0;
	int Gpe_ref = -1, Gpe_pred = -1;
	float error = 0., temp = 0.;
	int l = -1;
	float L_ref_val=0.0;
	float L_pred_val=0.0;
	float* my_data2=NULL;
	int* my_data1=NULL;

	if (def_groupe[numero].data == NULL)
	{

		if (l == -1)
		{

			l = find_input_link(numero, i);

			while (l != -1)
			{
				if (strcmp(liaison[l].nom, "-ref") == 0)
				{
					Gpe_ref = liaison[l].depart;
					L_ref_val=liaison[l].norme;
					printf("norme de la liaison ref = %f\n",L_ref_val);
				}

				if (strcmp(liaison[l].nom, "-pred") == 0)
				{
					Gpe_pred = liaison[l].depart;
					L_pred_val=liaison[l].norme;
					printf("norme de la liaison pred = %f\n",L_pred_val);
				}

				i++;
				l = find_input_link(numero, i);

			}
		}

		if (Gpe_ref == -1)
		{
			printf("groupe  ref non trouve dans groupe %d\n", numero);
			exit(0);
		}

		if (Gpe_pred == -1)
		{
			printf("groupe  pred non trouve dans groupe %d\n", numero);
			exit(0);
		}

		def_groupe[numero].data = MANY_ALLOCATIONS(2,int);
		def_groupe[numero].ext = MANY_ALLOCATIONS(2,float);
		my_data1=(int*)def_groupe[numero].data;
		my_data2=(float*)def_groupe[numero].ext;
		my_data1[0]= Gpe_ref;
		my_data1[1]= Gpe_pred;
		my_data2[0]= L_ref_val;
		my_data2[1]= L_pred_val;




	}
	else
	{
	    my_data1=(int*)def_groupe[numero].data;
	    my_data2=(float*)def_groupe[numero].ext;
		Gpe_ref= my_data1[0];
		Gpe_pred= my_data1[1];
		L_ref_val=  my_data2[0];
		L_pred_val=  my_data2[1];

	}

	temp = 0.;
	for (i = 0; i < def_groupe[Gpe_pred].nbre; i++)
	{

		temp = (neurone[def_groupe[Gpe_ref].premier_ele + i].s1*L_ref_val -
				neurone[def_groupe[Gpe_pred].premier_ele + i].s1*L_pred_val);
		error += temp;
	}
	if (error<=0.000005 && error>=-0.000005)
	{
	  error=0.0;
	}
	

	neurone[def_groupe[numero].premier_ele].s =
			neurone[def_groupe[numero].premier_ele].s2 =
					neurone[def_groupe[numero].premier_ele].s1= error;

}
