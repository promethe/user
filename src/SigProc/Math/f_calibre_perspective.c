/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include "reseau.h"

typedef struct MyData_f_calibre_perspective
{
  int gpe_image;
  int size_grid;
  int init_drag;
#ifndef AVEUGLE
  TxPoint origin;
  TxPoint * pt_calib;
#endif
  int largeur;
} MyData_f_calibre_perspective;

int invert_mat3x3  (float ** matin, float ** matout);
int display_mat3x3  (float ** matin);
int mult_mat3x3_vec(float ** matin, float * vecin,float * vecout);
int display_vec3x1(float * vecin);

int display_mat3x3(float ** matin)
{
  printf("%f\t%f\t%f\n%f\t%f\t%f\n%f\t%f\t%f\n",matin[0][0],matin[0][1],matin[0][2],matin[1][0],matin[1][1],matin[1][2],matin[2][0],matin[2][1],matin[2][2]);
  return 1;
}

int display_vec3x1(float * vecin)
{
  printf("%f\n%f\n%f\n",vecin[0],vecin[1],vecin[2]);
  return 1;
}

int invert_mat3x3(float ** matin, float ** matout)
{
  float a,b,c,d,e,f,g,h,i;
  float af,bf,cf,df,ef,ff,gf,hf,fi;
  float det;

  a=matin[0][0];
  b=matin[0][1];
  c=matin[0][2];
  d=matin[1][0];
  e=matin[1][1];
  f=matin[1][2];
  g=matin[2][0];
  h=matin[2][1];
  i=matin[2][2];

  det=a*( e*i-f*h) - b *(d*i -f*g) + c*(d*h-e*g);
  if (isequal(det,0.))
    return -1;
	
  af=(e*i-f*h)/det;
  bf=(c*h-b*i)/det;
  cf=(b*f-c*e)/det;
  df=(f*g-d*i)/det;
  ef=(a*i-c*g)/det;
  ff=(c*d-a*f)/det;
  gf=(d*h-e*g)/det;
  hf=(b*g-a*h)/det;
  fi=(a*e-b*d)/det;

  matout[0][0]=af;
  matout[0][1]=bf;
  matout[0][2]=cf;
  matout[1][0]=df;
  matout[1][1]=ef;
  matout[1][2]=ff;
  matout[2][0]=gf;
  matout[2][1]=hf;
  matout[2][2]=fi;
	
  return 1;
}

int mult_mat3x3_vec(float ** matin, float * vecin, float * vecout)
{
  int i,j;
  for(i=0; i<3; i++)
  {	
    vecout[i] = 0.;
    for(j=0; j<3; j++)
      vecout[i] = vecout[i] + matin[i][j]*vecin[j];
  }
  return 1;
}


void function_calibre_perspective(int gpe)
{
#ifndef AVEUGLE
  int gpe_image = -1;
  int i, l, j,sx,sy,cpt,nb_band;
  prom_images_struct *pt_in;
  MyData_f_calibre_perspective *my_data = NULL;
    
  TxPoint * pt_calib = (TxPoint*)malloc(4*sizeof(TxPoint));
  TxPoint origin;
  char param_link[255];
  int size_grid=20;
  TxPoint point1,point2;
  int init_drag=-1;
  int largeur=100;
/*****************Pour calcul matrice*****************/
  float xa=100;
  float ya=100;
	
  float 	xb=700;
  float 	yb=200;
	
  float 	xc=700;
  float 	yc=400;
	
  float 	xd=100;
  float 	yd=500;
	
  float 	x1=0;
  float 	y1=0;
  float 	x2=768;
  float 	y2=0;
  float 	x3=768;
  float 	y3=576;
  float 	x4=0;
  float 	y4=576;

  float R1,R2,R3;
  float Xd[3];
  float X4[3];
  float **A,**iA,**B,**iB;
  float kA[3],kB[3];	
  float **C,**iC;	
  float Rx[3],Ry[3],R[3];
  float A1[3],A2[3],A3[3];
  float **Af,**iAf;

  float pt_orig[3];
  float pt_image[3];
	
  A=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    A[i]=(float*)malloc(sizeof(float)*3);
  iA=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    iA[i]=(float*)malloc(sizeof(float)*3);
  B=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    B[i]=(float*)malloc(sizeof(float)*3);
  iB=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    iB[i]=(float*)malloc(sizeof(float)*3);
  C=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    C[i]=(float*)malloc(sizeof(float)*3);
  iC=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    iC[i]=(float*)malloc(sizeof(float)*3);
  Af=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    Af[i]=(float*)malloc(sizeof(float)*3);
  iAf=(float**)malloc(sizeof(float*)*3);
  for(i=0;i<3;i++)	
    iAf[i]=(float*)malloc(sizeof(float)*3);
/***********************************************************/
  origin.x=0;
  origin.y=0;
  if (def_groupe[gpe].data == NULL)
  {
    cpt = 0;
    l = find_input_link(gpe, cpt);
    while (l != -1)
    {
      if (strcmp(liaison[l].nom, "image") == 0)
      {
	gpe_image = liaison[l].depart;
      }
      if (prom_getopt(liaison[l].nom, "s",param_link) ==2)
      {
	size_grid=atoi(param_link);
      }
      if (prom_getopt(liaison[l].nom, "x",param_link) ==2)
      {
	origin.x=atoi(param_link);
      }
      if (prom_getopt(liaison[l].nom, "y",param_link) ==2)
      {
	origin.y=atoi(param_link);
      }
      if (prom_getopt(liaison[l].nom, "l",param_link) ==2)
      {
	largeur=atoi(param_link);
      } 
		
	    

      cpt++;
      l = find_input_link(gpe, cpt);
    }
    if (def_groupe[gpe_image].ext == NULL || gpe_image == -1)
    {
      printf
	("pas d'image ds l'ext du groupe image de f_calibre_perspective(%d)\n",
	 gpe);
      exit(0);
    }

    pt_in = ((prom_images_struct *) (def_groupe[gpe_image].ext));

    if (pt_in->nb_band > 3)
    {
      printf("bande naturel SVP dans  %s(%d)\n",
	     __FUNCTION__,gpe);
    }

    pt_calib[0].x=0+origin.x;
    pt_calib[0].y=0+origin.y;
    pt_calib[1].x=pt_in->sx+origin.x;
    pt_calib[1].y=0+origin.y;
    pt_calib[2].x=pt_in->sx+origin.x;
    pt_calib[2].y=pt_in->sy+origin.y;
    pt_calib[3].x=0+origin.x;
    pt_calib[3].y=pt_in->sy+origin.y;
	
    my_data =
      (MyData_f_calibre_perspective *)
      malloc(sizeof(MyData_f_calibre_perspective));
    if (my_data == NULL)
    {
      printf("erreur malloc dans f_calibre_perspective\n");
      exit(0);
    }
    my_data->gpe_image = gpe_image;
    my_data->pt_calib = pt_calib;
    my_data->size_grid = size_grid;
    my_data->init_drag = init_drag;
    my_data->origin = origin;
    my_data->largeur=largeur;
    def_groupe[gpe].data = (MyData_f_calibre_perspective *) my_data;

    point1.x=0;
    point1.y=0;
    TxDessinerRectangle(&image1, blanc, TxPlein, point1,
			4000,2000, 1);
  }
  else
  {
    my_data = (MyData_f_calibre_perspective *) def_groupe[gpe].data;
    gpe_image = my_data->gpe_image;
    pt_calib = my_data->pt_calib;
    size_grid = my_data->size_grid ;
    init_drag = my_data->init_drag;
    origin = my_data->origin;
    largeur=my_data->largeur;
    pt_in = ((prom_images_struct *) (def_groupe[gpe_image].ext));
	
  }
  sx=pt_in->sx;
  sy=pt_in->sy;
  nb_band=pt_in->nb_band;

/*redraw*/

  point1.x=0;
  point1.y=0;
  TxDessinerRectangle(&image1, blanc, TxPlein, point1,
		      4000,2000, 1);
  if (pt_in->nb_band == 3)
    TxAfficheImageCouleur(&image1,pt_in->images_table[0],sx,sy, origin.x,origin.y);
  else if (pt_in->nb_band == 1)
    TxAfficheImageNB(&image1,pt_in->images_table[0],sx,sy, origin.x,origin.y);   


/************* Calcule matrice ********************************/
/*point dans l'image normal*/	
  xa=pt_calib[0].x-origin.x;
  ya=pt_calib[0].y-origin.y;
  xb=pt_calib[1].x-origin.x;
  yb=pt_calib[1].y-origin.y;
  xc=pt_calib[2].x-origin.x;
  yc=pt_calib[2].y-origin.y;
  xd=pt_calib[3].x-origin.x;
  yd=pt_calib[3].y-origin.y;

/*point dans l'image finale*/
  x1=0;
  y1=0;

  x2=largeur;
  y2=0;

  x3=largeur;
  y3=largeur;

  x4=0;
  y4=largeur;
/*Creation des vecteur pour le calcul*/
  Xd[0]=xd; 
  Xd[1]=yd;
  Xd[2]=1;

  /*printf("====Xd===\n");
    display_vec3x1(Xd);*/

  X4[0]=x4;
  X4[1]=y4;
  X4[2]=1;

/* 	printf("====X4===\n");
   display_vec3x1(X4);*/
		
  A[0][0]=xa; A[0][1]=xb; A[0][2]=xc;
  A[1][0]=ya; A[1][1]=yb; A[1][2]=yc;
  A[2][0]=1.; A[2][1]=1.; A[2][2]=1.;

  B[0][0]=x1; B[0][1]=x2; B[0][2]=x3;
  B[1][0]=y1; B[1][1]=y2; B[1][2]=y3;
  B[2][0]=1.; B[2][1]=1.; B[2][2]=1.;

/*	printf("====A===\n");
  display_mat3x3(A);
  printf("====B===\n");
  display_mat3x3(B);*/

  invert_mat3x3(A,iA);
  invert_mat3x3(B,iB);

  /*printf("====iA===\n");
    display_mat3x3(iA);
    printf("====iB===\n");
    display_mat3x3(iB);*/

  mult_mat3x3_vec(iA,Xd,kA);
  mult_mat3x3_vec(iB,X4,kB);

  /*printf("====kA===\n");
    display_vec3x1(kA);
    printf("====kB===\n");
    display_vec3x1(kB);*/
	
  R1=kB[0]/kA[0];
  R2=kB[1]/kA[1];
  R3=kB[2]/kA[2];
	
  C[0][0]=xa; C[0][1]=ya; C[0][2]=1;
  C[1][0]=xb; C[1][1]=yb; C[1][2]=1;
  C[2][0]=xc; C[2][1]=yc; C[2][2]=1;

  invert_mat3x3(C,iC);
	
  /*printf("====C===\n");
    display_mat3x3(C);
    printf("====iC===\n");
    display_mat3x3(iC);*/

  Rx[0]=R1*x1;		Ry[0]=R1*y1;		R[0]=R1;
  Rx[1]=R2*x2;		Ry[1]=R2*y2;		R[1]=R2;
  Rx[2]=R3*x3;		Ry[2]=R3*y3;		R[2]=R3;
	
/*	printf("====Rx===\n");
  display_vec3x1(Rx);
  printf("====Ry===\n");
  display_vec3x1(Ry);
  printf("====R===\n");
  display_vec3x1(R);*/
	
  mult_mat3x3_vec((float**)iC,Rx,A1);
  mult_mat3x3_vec((float**)iC,Ry,A2);
  mult_mat3x3_vec((float**)iC,R ,A3);
	
  Af[0][0]=A1[0] ;Af[0][1]=A1[1]; Af[0][2]=A1[2];
  Af[1][0]=A2[0] ;Af[1][1]=A2[1]; Af[1][2]=A2[2];
  Af[2][0]=A3[0] ;Af[2][1]=A3[1]; Af[2][2]=A3[2];
		
  invert_mat3x3(Af,iAf);



  printf("====Af===\n");
  display_mat3x3(Af);
  printf("====iAF===\n");
  display_mat3x3(iAf);

/*dessine grille*/	
	
/*
  TxDessinerSegment(&image1, rouge, pt_calib[0],
  pt_calib[1], 2);
  TxDessinerSegment(&image1, rouge, pt_calib[1],
  pt_calib[2], 2);
  TxDessinerSegment(&image1, rouge, pt_calib[2],
  pt_calib[3], 2);
  TxDessinerSegment(&image1, rouge, pt_calib[3],
  pt_calib[0], 2);

  for(i=1;i<size_grid;i++)
  {	
  point1.x=ceil (  ( i*(pt_calib[3].x) + (size_grid - i)*pt_calib[0].x)/size_grid);
  point1.y=ceil(( i*(pt_calib[3].y)+(size_grid - i)*pt_calib[0].y)/size_grid);
  point2.x=ceil(( i*(pt_calib[2].x)+(size_grid - i)*pt_calib[1].x)/size_grid);
  point2.y=ceil(( i*(pt_calib[2].y)+(size_grid - i)*pt_calib[1].y)/size_grid);
		
  TxDessinerSegment(&image1, rouge, point1,
  point2, 1);
		
  point1.x=ceil (  ( i*(pt_calib[1].x) + (size_grid - i)*pt_calib[0].x)/size_grid);
  point1.y=ceil(( i*(pt_calib[1].y)+(size_grid - i)*pt_calib[0].y)/size_grid);
  point2.x=ceil(( i*(pt_calib[2].x)+(size_grid - i)*pt_calib[3].x)/size_grid);
  point2.y=ceil(( i*(pt_calib[2].y)+(size_grid - i)*pt_calib[3].y)/size_grid);
		
  TxDessinerSegment(&image1, rouge, point1,
  point2, 1);
  }	*/
  TxDessinerSegment(&image1, rouge, pt_calib[0],
		    pt_calib[1], 1);
  TxDessinerSegment(&image1, rouge, pt_calib[1],
		    pt_calib[2], 1);
  TxDessinerSegment(&image1, rouge, pt_calib[2],
		    pt_calib[3], 1);
  TxDessinerSegment(&image1, rouge, pt_calib[3],
		    pt_calib[0], 1);
  for(i=1;i<size_grid;i++)
  {
    pt_orig[0]=i*largeur/size_grid;		
    pt_orig[1]=0;	
    pt_orig[2]=1;
    mult_mat3x3_vec(iAf,pt_orig,pt_image);
    point1.x=pt_image[0]/pt_image[2]+origin.x;
    point1.y=pt_image[1]/pt_image[2]+origin.y;

    pt_orig[0]=i*largeur/size_grid;		
    pt_orig[1]=largeur;	
    pt_orig[2]=1;
    mult_mat3x3_vec(iAf,pt_orig,pt_image);
    point2.x=pt_image[0]/pt_image[2]+origin.x;
    point2.y=pt_image[1]/pt_image[2]+origin.y;
		
    TxDessinerSegment(&image1, rouge, point1,
		      point2, 1);

    pt_orig[0]=0;		
    pt_orig[1]=i*largeur/size_grid;	
    pt_orig[2]=1;
    mult_mat3x3_vec(iAf,pt_orig,pt_image);
    point1.x=pt_image[0]/pt_image[2]+origin.x;
    point1.y=pt_image[1]/pt_image[2]+origin.y;

    pt_orig[0]=largeur;		
    pt_orig[1]=i*largeur/size_grid;	
    pt_orig[2]=1;
    mult_mat3x3_vec(iAf,pt_orig,pt_image);
    point2.x=pt_image[0]/pt_image[2]+origin.x;
    point2.y=pt_image[1]/pt_image[2]+origin.y;
		
    TxDessinerSegment(&image1, rouge, point1,
		      point2, 1);	
  }


/* recalcule des points de calib avec le drag and drop */
  if (dragging_image1==1)
  {
    /*cherche le pt qui bouge*/
    if(image1_posx<pt_calib[0].x+10 && image1_posx>pt_calib[0].x-10 && image1_posy<pt_calib[0].y+10 && image1_posy>pt_calib[0].y-10 )
    {
      init_drag=0;
    }	
    else if(image1_posx<pt_calib[1].x+10 && image1_posx>pt_calib[1].x-10 && image1_posy<pt_calib[1].y+10 && image1_posy>pt_calib[1].y-10)
    {
      init_drag=1;
    }
    else if(image1_posx<pt_calib[2].x+10 && image1_posx>pt_calib[2].x-10 && image1_posy<pt_calib[2].y+10 && image1_posy>pt_calib[2].y-10)
    {
      init_drag=2;
    }
    else if(image1_posx<pt_calib[3].x+10 && image1_posx>pt_calib[3].x-10 && image1_posy<pt_calib[3].y+10 && image1_posy>pt_calib[3].y-10)
    {
      init_drag=3;
    }
    /*reaffecte le point qui a boug�*/
    if(init_drag!=-1)
    {
      pt_calib[init_drag].x=image1_posx;
      pt_calib[init_drag].y=image1_posy;
    }
  }	
  else
  {
    init_drag=-1;
  }
/*******************************************************************************/

  my_data->init_drag = init_drag;
  TxFlush(&image1);


/*Ecriture de la matrice des les neurones du groupe*/
  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      neurone[def_groupe[gpe].premier_ele+i*3+j].s=neurone[def_groupe[gpe].premier_ele+i*3+j].s1=neurone[def_groupe[gpe].premier_ele+i*3+j].s2= iAf[i][j];
#else
  (void)gpe;
#endif
}
