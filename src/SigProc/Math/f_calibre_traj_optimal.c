/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

typedef struct MyData_f_calibre_traj_optimal
{
  int gpe_pt_restant;
} MyData_f_calibre_traj_optimal;

void function_calibre_traj_optimal(int gpe)
{
#ifndef AVEUGLE
  TxPoint pt_traj,pt_rect;
  int l,cpt;
  MyData_f_calibre_traj_optimal *my_data = NULL;
  int gpe_pt_restant=-1,nb_pt_restant;
  struct timespec duree_nanosleep, res;

  printf("enter function_calibre_traj_optimal\n");	
	
  if (def_groupe[gpe].data == NULL)
  {
    if(def_groupe[gpe].nbre!=2)
    {
      printf("il faut 2 neurones dans f_calibre_traj_optimal\n");
      exit(-1);
    }
    cpt = 0;
    l = find_input_link(gpe, cpt);
    while (l != -1)
    {
      if (strcmp(liaison[l].nom, "pt_restant") == 0)
      {
	gpe_pt_restant = liaison[l].depart;
      }
      cpt++;
      l = find_input_link(gpe, cpt);
    }

    if(gpe_pt_restant==-1)
    {
      printf("il manque le groupe des point restant dans f_calibre_traj_optimal\n");
      exit(-1);
    }

    my_data =
      (MyData_f_calibre_traj_optimal *)
      malloc(sizeof(MyData_f_calibre_traj_optimal));
    if (my_data == NULL)
    {
      printf("erreur malloc dans f_calibre_traj_optimal\n");
      exit(0);
    }
    my_data->gpe_pt_restant = gpe_pt_restant;
    def_groupe[gpe].data = (MyData_f_calibre_traj_optimal *) my_data;
  }
  else
  {
    my_data = (MyData_f_calibre_traj_optimal *) def_groupe[gpe].data;
    gpe_pt_restant=my_data->gpe_pt_restant;
	
  }

  nb_pt_restant=ceil(neurone[def_groupe[gpe_pt_restant].premier_ele].s1);
  printf("===================================\n\tf_calibre_traj_optimal\nIl reste %d point � rentrer\n",nb_pt_restant+1);

  duree_nanosleep.tv_sec = 0;
  duree_nanosleep.tv_nsec = 10000000;
  while(dragging_image2!=1)
  {

    nanosleep(&duree_nanosleep, &res);
  }
  while(dragging_image2!=0)
  {
    nanosleep(&duree_nanosleep, &res);
  }
  pt_traj.x=image2_posx;
  pt_traj.y=image2_posy;
	
  pt_rect.x=pt_traj.x-1;
  pt_rect.y=pt_traj.y-1;
	
  TxDessinerRectangle(&image2, rouge, TxPlein, pt_rect,
		      2,2, 1);
	
  TxFlush(&image2);

  printf("Le point %d %d appartient a la traj optimal\n",pt_traj.x,pt_traj.y);
	

  neurone[def_groupe[gpe].premier_ele].s=
    neurone[def_groupe[gpe].premier_ele].s1=
    neurone[def_groupe[gpe].premier_ele].s2=pt_traj.x;

  neurone[def_groupe[gpe].premier_ele+1].s=
    neurone[def_groupe[gpe].premier_ele+1].s1=
    neurone[def_groupe[gpe].premier_ele+1].s2=pt_traj.y;
#else
  (void)gpe;
#endif
}
