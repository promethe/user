/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
\ingroup libSigProc
\defgroup  f_statistics f_statistics
\brief 

Author: AM.Tousch
Created: 01/03/2006
Modified:
Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction permettant de calculer le pourcentage de r�ssite lors d'un test de reconnaissance (d'expressions faciales �l'origine).
Permet aussi d'afficher quelques donn�s sur le groupe d'entr� (entr� test) telles que learning rate et gaussienne.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: affichage du taux de ressite dans la fenetre image2 fait des hi�oglyphes...

Todo: see author for testing and commenting the function

************************************************************/
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <NN_Core/macro_colonne.h>


#define NBLINKS 5
#define SEUIL 0.05
#define SEUILMAC 0.01

typedef struct
{
    int time;                   /* nombre d'iterations */
    int success;                /* nombre de prototypes correctement reconnus */
    int failure;                /* nombre d'echecs */
    int *success_by_class;      /* nombre de prototypes correctement reconnus pour chaque classe */
    int *time_by_class;         /* nombre de prototypes rencontres pour chaque classe */
    int gpe_supervision;        /* numero du groupe d'entree correspondant a l'apprentissage */
    int gpe_test;               /* numero du groupe d'entree de supervision */
    int size;                   /* nombre de classes */
    int is_macro;               /* 1 si gpe_test est un macro_colonne, 0 sinon */
    int with_data;              /* demande ou non l'affichage des donnees sur l'apprentissage (learning rate, tableau_gaussienne, poids du r�eau)
                                   correspond a l'option -d0 (sans) ou -d1 (avec) */
    int answer;
    int *answer_by_class;
    int *failure_by_class;
} statistics;

void new_statistics(int Gpe)
{
    int n, link;
    int taille_test, taille_sup;
    char resultat[256];

    int i;

    statistics *stats;
    stats = (statistics *) malloc(sizeof(statistics));
    if (stats == NULL)
    {
      EXIT_ON_ERROR("pb malloc dans %s\n",def_groupe[Gpe].no_name);
    }

    /* initialisation de la structure */
    dprints("initialisation structure stat dans new_statistics groupe %d\n", def_groupe[Gpe].no_name);
    stats->time = 0;
    stats->success = 0;
    stats->failure = 0;
    stats->is_macro = 0;
    stats->with_data = 0;
    stats->answer = 0;

    /* Le groupe doit obligatoirement avoir deux entrees :
     * typiquement, on compare le resultat obtenu par le reseau avec celui attendu pour chaque prototype. */
    n = -1;
    do
    {
        n++;
        link = find_input_link(Gpe, n);
        if (link != -1)
        {
            if (liaison[link].nom[0] == '-')
            {
                switch (liaison[link].nom[1])
                {
                    /* entree de supervision donne le neurone correspondant au prototype */
                case 's':
                    stats->gpe_supervision = liaison[link].depart;

                    	dprints("supervision input group : %d (fstatistic %s) \n", liaison[link].depart, def_groupe[Gpe].no_name);
                    break;

                    /* entr� de 'test' - r�ultat de l'apprentissage */
                    /* dans le cas du macro-colonne, le groupe en entr� sur ce lien peut subir des modifications
                     * qui ne sont pas associ�s aux statistiques, mais �l'�olution g��ale du syst�e */
                case 't':
                    stats->gpe_test = liaison[link].depart;

                    dprints("neuron input group : %d (fstatistic %s) \n", liaison[link].depart, def_groupe[Gpe].no_name);

                    /* Pour afficher les donnees (l_rate, gaussienne) */
                    if (prom_getopt(liaison[link].nom, "-d", resultat) == 2)
                        stats->with_data = atoi(resultat);
                    else
                        stats->with_data = 0;
                    break;
                default:
                    printf(" link %s not recognized (f_statistic) \n", liaison[link].nom);
                }
            }
            else
                printf(" link %s not recognized (f_statistic) \n", liaison[link].nom);
        }
    }
    while (link != -1);


    taille_test = def_groupe[stats->gpe_test].nbre;
    taille_sup = def_groupe[stats->gpe_supervision].nbre;

    /* cas particulier ou le groupe d'entree d'apprentissage est un macro-colonne */
    /* A ameliorer pour pouvoir vraiment tenir compte du nombre de classes (pas seulement tailley en theorie */
    if (def_groupe[stats->gpe_test].type == No_Macro_Colonne)
    {
        taille_test = def_groupe[stats->gpe_test].tailley;
        stats->is_macro = 1;
    }

    /* veification que les donnees d'entrees sont comparables */
    if (taille_test != taille_sup)
    {
        EXIT_ON_ERROR("the input groups in group f_statistics %s must have the same size\n",  def_groupe[Gpe].no_name);
    }

    /* taille du reseau de supervision = nombre de classes */
    stats->size = taille_sup;

    /* allocation memoire pour les tableaux de quotas de reussite et initialisation */
    stats->success_by_class = (int *) malloc(stats->size * sizeof(int));
    stats->time_by_class = (int *) malloc(stats->size * sizeof(int));
    stats->answer_by_class = (int *) malloc(stats->size * sizeof(int));
    stats->failure_by_class = (int *) malloc(stats->size * sizeof(int));

    for (i = 0; i < stats->size; i++)
    {
        stats->success_by_class[i] = 0;
        stats->time_by_class[i] = 0;
        stats->answer_by_class[i] = 0;
        stats->failure_by_class[i] = 0;
    }

    def_groupe[Gpe].data = stats;

}

/********************************** fin new_statistics **********************************/



/****************************************************************************************/
void function_statistics(int Gpe)
{
    int gpe_test, gpe_sup;
    float diff, diff1, diff2;
    int deb_gpe_test, deb_gpe_sup;
    int size;
    int nberrors;
    statistics *my_stats = NULL;
    char message[256];
    int n, i, im;
    float rate, answer_rate, global_rate;
    int nbre = 0, nbre2 = 0, increment = 0;
    int class_number;
    float max_act, max_act2;
    int max_act_class, max_act2_class;
    int not_sure;

    /* donnees sur le groupe d'apprentissage */
    float l_rate;
    float distance;

    static int maj = 0;

#ifndef AVEUGLE
    char font;
    TxPoint where_message;
    where_message.x = 10;
    where_message.y = 50;
    font = 'a';
#endif

    /* getting data from current group structure */
    my_stats = (statistics *) def_groupe[Gpe].data;
    if (my_stats == NULL)
    {
        EXIT_ON_ERROR("error while loading data in group %s f_statistics \n", def_groupe[Gpe].no_name);
    }

    /* information about input groups */
    gpe_test = my_stats->gpe_test;
    gpe_sup = my_stats->gpe_supervision;
    size = my_stats->size;

    deb_gpe_test = def_groupe[gpe_test].premier_ele;
    deb_gpe_sup = def_groupe[gpe_sup].premier_ele;

    class_number = 0;

  /*************** STATISTICS *********************/

    /* Comparing inputs activities 
     * *****************************/
    nberrors = 0;
    max_act = -1;
    max_act2 = -1;
    max_act_class = -1;
    max_act2_class = -1;
    not_sure = 0;
    /* if input is a macro-column */
    if (my_stats->is_macro)
    {

        nbre = def_groupe[gpe_test].nbre;
        nbre2 = def_groupe[gpe_test].taillex * def_groupe[gpe_test].tailley;
        increment = nbre / nbre2;

        i = 0;                  /* index in supervised entry */
        for (im = deb_gpe_test + increment - 1; im < deb_gpe_test + nbre;
             im += increment)
        {
            /* if macro-neuron */
            if (i < size)
            {
                /* which prototype is activated? */
                if (fabs(neurone[deb_gpe_sup + i].s - 1) < SEUIL)
                    class_number = i;

                if (neurone[im].s > max_act)
                {
                    max_act = neurone[im].s;
                    max_act_class = i;
                }

                /* input comparisons */
                diff = fabs(neurone[im].s - neurone[deb_gpe_sup + i].s);
                diff1 = fabs(neurone[im].s1 - neurone[deb_gpe_sup + i].s1);
                diff2 = fabs(neurone[im].s2 - neurone[deb_gpe_sup + i].s2);

                dprints("neurons activity %i:\n app%d=%f  sup%d=%f   diff = %f\n", i, im, neurone[im].s, deb_gpe_sup + i, neurone[deb_gpe_sup + i].s, diff);

                i++;

                if (diff > SEUIL && diff1 > SEUIL && diff2 > SEUIL)
                    nberrors++;
            }
        }
    }
    /* in other case */
    else
    {
        /* both networks are scanned in parallel */
        for (i = 0; i < size; i++)
        {
            /* which prototype is activated? */
            if (fabs(neurone[deb_gpe_sup + i].s - 1) < SEUIL)
                class_number = i;


            if (neurone[deb_gpe_test + i].s > max_act)
            {
                /* former max is kept as second max */
                max_act2 = max_act;
                max_act2_class = max_act_class;
                /* new max */
                max_act = neurone[deb_gpe_test + i].s;
                max_act_class = i;
            }
            else if (neurone[deb_gpe_test + i].s > max_act2)
            {
                max_act2 = neurone[deb_gpe_test + i].s;
                max_act2_class = i;
            }
        }
    }


    /* Counting number of iterations */
    my_stats->time++;
    my_stats->time_by_class[class_number]++;
    maj++;
/*  printf("total of %d iterations (%d)\n----------*--***--*--------------\n",maj, my_stats->time); */

    /* Testing whether answer is sure or not */
    if (my_stats->is_macro)
    {
        if (max_act < SEUILMAC)
        {
            not_sure = 1;
        }

        dprints("neurone d'activitee max = %d, sup=%d\n", max_act_class, class_number);
        dprints("activitee : %f\n", max_act);

    }
    else
    {
        /* neurons of two max activities have similar activities */
        if (fabs(max_act - max_act2) < SEUIL)
        {
            not_sure = 1;
        }

        /* None of the two max is the right answer - error added */
        if (max_act_class != class_number && max_act2_class != class_number)
            nberrors++;

#ifdef DEBUG
        printf("neurone d'activite max = %d, max2 = %d, sup=%d\n", max_act_class, max_act2_class, class_number);
        printf("activites respectives : %f %f\n", max_act, max_act2);
#endif
    }


#ifdef DEBUG
    printf(" groupe %d r�onse %s\n", gpe_test, (not_sure ? "not sure" : "sure"));
#endif

    /* if an answer is given */
    if (not_sure == 0)
    {
        my_stats->answer++;
        my_stats->answer_by_class[class_number]++;


        /* Computing total succes rate */
        if (nberrors == 0)
        {
            my_stats->success++;
            my_stats->success_by_class[class_number]++;
        }
    }

    /* global rate (sure or not) */
    if (nberrors > 0)
    {
        my_stats->failure++;
        my_stats->failure_by_class[class_number]++;
    }

    /* display rates */
    if ((my_stats->time % 50) == 0)
    {
        if (my_stats->answer > 0)
            rate = (float) my_stats->success / (float) my_stats->answer * 100;
        else
            rate = 0.;
        answer_rate = (float) my_stats->answer / (float) my_stats->time * 100;

        global_rate = ((float) my_stats->time - (float) my_stats->failure) / (float) my_stats->time * 100;

        n = sprintf(message, "success rate: %f", rate);
#ifdef DEBUG
        printf("%s\n", message);
#endif

        if (n < 0)
        {
            printf("string error in group %d (f_statistics)\n", Gpe);
            exit(EXIT_FAILURE);
        }
        printf
            ("*--------------------------------------------------------------*\n* group %s - success rate: %f in %d tries\t*\n",
             def_groupe[gpe_test].nom, rate, my_stats->time);
        printf("answer rate : %f\nglobal rate : %f\n", answer_rate, global_rate);


        /* Computing success rate for each class */
        for (i = 0; i < my_stats->size; i++)
        {
            if (my_stats->answer > 0)
                rate = (float) my_stats->success_by_class[i] / (float) my_stats->answer_by_class[i] * 100;
            else
                rate = 0.;
            answer_rate = (float) my_stats->answer_by_class[i] / (float) my_stats->time_by_class[i] * 100;

            global_rate = ((float) my_stats->time_by_class[i] - (float) my_stats->failure_by_class[i]) / (float) my_stats->time_by_class[i] * 100;

            /*      n = sprintf(message,"success rate: %f",rate); */

            if (n < 0)
            {
                printf("string error in group %d (f_statistics)\n", Gpe);
                exit(EXIT_FAILURE);
            }
            printf("* group %s - class %d success rate: %f in %d tries\t*\n", def_groupe[gpe_test].nom, i, rate, my_stats->time_by_class[i]);
            printf("class %d answer rate : %f\nglobal rate : %f\n", i, answer_rate, global_rate);
        }
        printf
            ("*--------------------------------------------------------------*\n");
    }


#ifndef AVEUGLE
    TxDessinerRectangle(&image2, blanc, 1, where_message, 200, 50, 2);
    TxFlush(&image2);
    TxEcrireChaine(&image2, noir, where_message, message, &font);
    TxFlush(&image2);
#endif

  /******************** END OF STATISTICS *********************/


  /*********** Affichage des donn�s : learning rate, gaussienne *****************/
    if (my_stats->time % 1000 == 0)
    {
        l_rate = def_groupe[gpe_test].learning_rate;
        /* print learning rate */
        printf(" ==> l_rate = %f\n----------------\n", l_rate);

        if (my_stats->with_data)
        {
            /* learning rate evolution */
            def_groupe[gpe_test].learning_rate = l_rate / 2;
        }

        /* printf neighbourhood */
        if (def_groupe[gpe_test].type == No_PTM
            || def_groupe[gpe_test].type == No_Kohonen
            || def_groupe[gpe_test].type == No_Macro_Colonne)
        {
            printf("gaussienne :\n");
            for (i = 0; i < 10; i++)
            {
                distance = def_groupe[gpe_test].tableau_gaussienne[i];
                printf("d = %f\t", distance);
            }
        }
        printf("\n****************************\n");
    }

}
