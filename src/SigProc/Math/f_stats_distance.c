/*
 * stat_distance.c
 *
 * Copyright 2015 AliaaMoualla <aliaamoualla@cyber104>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


#define CORRECT 0
#define FALSE_POSITIVE 1
#define FALSE_NEGATIVE 2
#define TAILLETAB 7
#define TAILLETABLEAU 2000

typedef struct Mydata_stats
{
   int iterations;             /* nombre d'iterations */
   float erreur_quad;			/*erreur quadratique*/
   float erreur_quad_moy;		/*erreur quadratique moyenne*/
   float sum_erreur_quad;
   float seuil;                /* seuil de la bonne reponse */
   int nb_result;
   int moyenneOnly;
   int allOutput;
   float * tabResult[TAILLETAB];
   float tab_dist[TAILLETABLEAU];
   float tab_tab[TAILLETAB];
   int tab_dist_deux[TAILLETABLEAU];
   float tab_d[320];
   float tab_ecart[TAILLETABLEAU];
   float success;              /* nombre de prototypes correctement reconnus */
   float fail;
   float false_positive;       /* indique le nombre de fois que la sortie est plus grand que le seuil erronement */
   float false_negative;       /* indique le nombre de fois que la sortie est moins grand que le seuil erronement */
   float sum_dist;
   int dd;
   float ecartype;
   float moy_dist;
   int nb_success;
   int nb_success_pos;
   int nb_fail;
   int nb_false_negative;
   int nb_false_positive;
   int pourcent;
   int gpe_supervision;        /* numero du groupe d'entree correspondant a l'apprentissage */
   int gpe_test; /* numero du groupe d'entree de supervision *//*???????????????????????*/
   int gpe_pos_supervision; /* la position de l'objet en corrigee AAAAAAAAAA*/
   int gpe_pos_test;        /*la position comme reponse de reseau AAAAAAAAAAA */
   int gpe_transition_detect;
   int trans_detect;
   FILE * statdf;
   FILE * statdf_pos;
   char statfilename[255];
   int infile;
} Mydata_stats;


#define MAX_ERROR 0.0001
inline int f_egal(float a, float b)
{
   float relativeError;

   if (fabs(b) > fabs(a))   relativeError = fabs((a - b) / b);
   else  relativeError = fabs((a - b) / a);

   if (relativeError <= MAX_ERROR)  return 1;

   return 0;
}

void new_stats_distance(int Gpe)
{
   int 	n, link,i = 0,j = 0;
   int 	longueur_n_test;        /* numero de neurones du groupe de test */
   char 	resultat[256];
   char 	*string   = NULL;
   char    param_link[256];
   int	taille_sup;
   Mydata_stats *mydata;


   mydata = (Mydata_stats *) malloc(sizeof(Mydata_stats));
   if (mydata== NULL)
      EXIT_ON_ERROR("Erreur d'allocation de mydata %s",__FUNCTION__);

   /* initialisation de la structure */

   dprints("initialisation structure mydata dans (%s) groupe %s\n", __FUNCTION__,def_groupe[Gpe].no_name);

   mydata->gpe_supervision =0;
   mydata->gpe_test = 0;
   mydata->gpe_pos_supervision=0;
   mydata->gpe_pos_test=0;
   mydata->iterations = 0;
   mydata->erreur_quad_moy = 0.;
   mydata->erreur_quad = 0.;
   mydata->sum_erreur_quad = 0.;
   mydata->trans_detect = 0;
   mydata->nb_result = 1;
   mydata->success = 0;
   mydata->sum_dist=0.0;
   mydata->ecartype=0;
   mydata->dd=0;
   mydata->seuil=0;
   mydata-> moy_dist=0;
   mydata->fail = 0;
   mydata->false_positive = 0;
   mydata->false_negative = 0;
   mydata->nb_success = 0;
   mydata->nb_success_pos= 0;
   mydata->nb_fail = 0;
   mydata->nb_false_negative = 0;
   mydata->nb_false_positive = 0;
   mydata->pourcent = 0;
   mydata->moyenneOnly = 0;
   mydata->infile = 0;

   dprints("initialisation structure mydata (%s) iterations = %d success = %g, seuil = %f\n",__FUNCTION__, mydata->iterations, mydata->success, mydata->seuil);

   n = 0;
   link = find_input_link(Gpe, n);
   while (link != -1)
   {
      string = liaison[link].nom;
      /* Groupe de sortie desirees (supervision )*/
      if (prom_getopt(string, "-sss", param_link) == 1)
      {
         mydata->gpe_supervision = liaison[link].depart;
         dprints("supervision input group : %s (%s %s) \n",def_groupe[mydata->gpe_supervision].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
      }
      /* Groupe de 'test' : Sortie apprise (ex : sortie du LMS) */
      if (prom_getopt(string, "-ttt", param_link) == 1)
      {
         mydata->gpe_test = liaison[link].depart;
         dprints("test input group : %s (%s %s) \n",def_groupe[mydata->gpe_test].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
      }
      if (prom_getopt(string, "-ps", param_link) == 1)
      {
         mydata->gpe_pos_supervision = liaison[link].depart;
         dprints("supervision position input group : %s (%s %s) \n",def_groupe[mydata->gpe_pos_supervision].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
      }
      if (prom_getopt(string, "-pt", param_link) == 1)
      {
         mydata->gpe_pos_test = liaison[link].depart;
         dprints("test position input group : %s (%s %s) \n",def_groupe[mydata->gpe_pos_test].no_name,__FUNCTION__, def_groupe[Gpe].no_name);
      }
      /* Pour specifier le seuil */
      if (prom_getopt(liaison[link].nom, "-L", resultat) == 2 || prom_getopt(liaison[link].nom, "-l", resultat) == 2)
      {
         mydata->seuil = atof(resultat);
         dprints("mydata->seuil = %g (%s %s) \n", mydata->seuil,__FUNCTION__,def_groupe[Gpe].no_name );
      }
      if (prom_getopt(liaison[link].nom, "-f", resultat) == 2 || prom_getopt(liaison[link].nom, "-F", resultat) == 1)
      {
         strcpy(mydata->statfilename,resultat);
         mydata->infile = 1;
         dprints("fichier des statistiques = %s\n", mydata->statfilename);
      }
      if (prom_getopt(liaison[link].nom, "-M", resultat) == 1 || prom_getopt(liaison[link].nom, "-m", resultat) == 1)
      {
         mydata->moyenneOnly = 1;
         dprints("mydata->nb_result = %d (%s %s) \n", mydata->nb_result,__FUNCTION__,def_groupe[Gpe].no_name );
      }

      /* Pour remise a zero a la demande */
      /*if (prom_getopt(liaison[link].nom, "transition_detect", resultat) == 1 || prom_getopt(liaison[link].nom, "trans_detect", resultat) == 1)*//*NE MARCHE PAS ????*/
      if (prom_getopt(liaison[link].nom, "-reset", resultat) == 1 || prom_getopt(liaison[link].nom, "-RESET", resultat) == 1)
      {
         mydata->trans_detect = 1;
         mydata->gpe_transition_detect = liaison[link].depart;
         dprints("Transition detect link (group %s) found in (%s %s) \n",def_groupe[mydata->gpe_transition_detect].no_name, __FUNCTION__,def_groupe[Gpe].no_name );
      }
      n++;
      link = find_input_link(Gpe, n);
   }

   if (mydata->gpe_supervision == 0 || mydata->gpe_test == 0)
      EXIT_ON_ERROR ("Le groupe %s (%s) doit avoir au moins deux liens en entree. Sortie desiree (-s), sortie du reseau (-t) et (optionel) transition_detect",
                     def_groupe[Gpe].no_name, __FUNCTION__);

   if (mydata->gpe_pos_supervision == 0 || mydata->gpe_pos_test == 0)
      EXIT_ON_ERROR ("Le groupe %s (%s) doit avoir au moins deux liens en entree. Sortie desiree (-sss), sortie du reseau (-ttt) et (optionel) transition_detect",
                     def_groupe[Gpe].no_name, __FUNCTION__);

   longueur_n_test = def_groupe[mydata->gpe_test].nbre;
   taille_sup = def_groupe[mydata->gpe_supervision].nbre;
   /* verification que les donnees d'entree sont comparables */
   if (taille_sup != longueur_n_test)
      EXIT_ON_ERROR ("the input groups in group %s (%s) must have the same number of neurons\n",def_groupe[Gpe].no_name, __FUNCTION__);

   /*Creer un tableau de resultat par categorie, 4 nombre de stats (Vrai,Faux) positif (Vrai,Faux) Negatif*/
   for ( i = 0; i < TAILLETAB; i++)
   {

      mydata->tabResult[i] = (float *) malloc ( taille_sup * sizeof(float));
      if (mydata->tabResult[i] == NULL)
         EXIT_ON_ERROR("Erreur d'allocation de tabResult[%d] %s",i,__FUNCTION__);
   }
   ///////////////////////ALiaa/INITIALISATION DES TABLEAUX////////////////////////////
   for (i=0; i<TAILLETABLEAU; i++)
   {
      mydata->tab_dist[i]=-1;
      mydata->tab_ecart[i]=-1;
   }
   ///////////////////////////////////////////////////////////
   for (i=0; i<30; i++)
   {
      mydata->tab_d[i]=0;

   }

   ///////////////////////////////////////////////////////////
   for (i = 0; i<TAILLETAB; i++)
      for (j=0; j<taille_sup; j++)
         mydata->tabResult[i][j] = 0;
   /*ouverture du fichier des stats*/
   if (mydata->infile)
   {
      mydata->statdf = fopen(mydata->statfilename,"w+");
      if (mydata->statdf==NULL)
         EXIT_ON_ERROR("Cannot open file %s in %s",mydata->statfilename,__FUNCTION__);
   }
   ///////////////QQ I FAIT CETTE LIGNE ////////////////////////////
   def_groupe[Gpe].data=mydata;
   dprints("END %s\n",__FUNCTION__);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void function_stats_distance(int Gpe)
{
   int i = 0,j = 0,objet=0/*index_sup,index_test*/;
   /* int min,m=0 ; */
   Mydata_stats *mydata = NULL;
   /*float temp = 0., rate, false_negative, false_positive,tmp,erreur_quad;*/
   float tmp = 0.0/*,max = 0.0, max_sup = 0.0*/;
   float tmp_distance = 0.0;
   //float tmpd = 0.0;
   float pos_corrige=0.0;
   float pos_test=0.0;

   /* donnees sur le groupe d'apprentissage */
   int debut = 0, first_neuron_sup = 0, taille_sup = 0, first_neuron_test = 0;
   int first_neuron_sup_pos=0 , first_neuron_test_pos =0;
   //int nb_result;
   float seuil;
   //FILE *statdf;

   ////////////////////////////pourquoi////////////////////////////////////////

   mydata = (Mydata_stats *) def_groupe[Gpe].data;
   if (mydata == NULL)
      EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name,__FUNCTION__);
   ////////////////////////////////////////////////////////////////////////////

   debut = def_groupe[Gpe].premier_ele;
   first_neuron_sup = def_groupe[mydata->gpe_supervision].premier_ele;
   taille_sup = def_groupe[mydata->gpe_supervision].nbre;
   //taille_test = def_groupe[mydata->gpe_test].nbre;
   first_neuron_test = def_groupe[mydata->gpe_test].premier_ele;

   /////////////////////////////////Aliaa//////////////////////////////////////
   first_neuron_sup_pos = def_groupe[mydata->gpe_pos_supervision].premier_ele;
   //taille_sup_pos = def_groupe[mydata->gpe_pos_supervision].nbre;
   //taille_test_pos = def_groupe[mydata->gpe_pos_test].nbre;
   first_neuron_test_pos = def_groupe[mydata->gpe_pos_test].premier_ele;

   ////////////////////////////////////////////////////////////////////////////

   seuil = mydata->seuil;

   /*Calcul de l'erreur quadratique moyenne (Calcul en utilisant les champs non seuilles*/
   /*Remise a zero de l'erreur quad et du nombre d'itérations*/
   if (mydata->trans_detect == 1)
   {
      if (neurone[def_groupe[mydata->gpe_transition_detect].premier_ele].s1 > 0.5)
      {
         mydata->erreur_quad = 0.0;
         mydata->iterations = 0;
         mydata->nb_success = 0;
         mydata->nb_fail=0;
         mydata->nb_false_negative = 0;
         mydata->erreur_quad = 0.;
         mydata->sum_erreur_quad = 0.;
         //mydata->sum_dist=0.0;
      }
   }

   mydata->iterations ++;
   dprints("ITERATION =  %d\n",mydata->iterations);

   /*Calcul de l'erreur quadratique moyenne*/
   mydata->erreur_quad =0.;
   tmp = 0;

   tmp_distance=0;
   for (i = 0; i < taille_sup; i++)
   {
      tmp = (neurone[first_neuron_sup+i].s1 -  neurone[first_neuron_test+i].s1);

      tmp=sqrt(tmp*tmp);
      mydata->erreur_quad = mydata->erreur_quad + tmp;
   }


   mydata->sum_erreur_quad += (mydata->erreur_quad/taille_sup);
   mydata->erreur_quad_moy = mydata->sum_erreur_quad /  mydata->iterations ;
   /*mydata->erreur_quad_moy /= mydata->iterations;*/

   dprints("erreur quad  = (%f - %f) %f\n",neurone[first_neuron_sup].s1,neurone[first_neuron_test].s,mydata->erreur_quad);
   dprints("somme erreur quad  = %f\n",mydata->sum_erreur_quad);
   dprints("erreur quad moy  =  %f\n",mydata->erreur_quad_moy);
   /*Le calcul de l'erreur peut se faire seul pour gagner du temps*/

   if (mydata->moyenneOnly == 1)
   {
      neurone[debut].s = neurone[debut].s1 = neurone[debut].s2 =  mydata->erreur_quad_moy;
      return;
   }


   /*****STATS******/
   //////////////////////STATS POSITIONS ////////////////////////////////

   /*Parcourir les deux vecteurs vecteurs en entier et les comparer*/

   // printf("mon debut \n");
   for (i = 0; i < taille_sup; i++) /* pour les 25 objets */
   {
      //Le NN répond et il devait repondre => Vrai Positif
      if (neurone[first_neuron_sup+i].s1 > seuil)     mydata->tabResult[4][i] = mydata->tabResult[4][i] +1.; /*la categorie est rencontree*/
      else mydata->tabResult[5][i] = mydata->tabResult[5][i] +1.; /*categorie non vu*/

      //Le NN répond correctement => Vrai Positif
      if (neurone[first_neuron_test+i].s1 > seuil && neurone[first_neuron_sup+i].s1 > seuil)
      {
         objet=i+1;
         tmp=neurone[first_neuron_sup+i].s1;
         pos_test=neurone[first_neuron_test_pos].s1;
         tmp=neurone[first_neuron_sup_pos].s1;
         for (j=0; j<4; j++) /* pour les 4 positions possibles de l'objet i dans l'image */
         {
            if (f_egal(neurone[first_neuron_sup_pos+j].s1,objet))
            {
               pos_corrige=neurone[first_neuron_sup_pos+j+4].s1;
               pos_test=pos_test*320;
               tmp_distance=fabs(pos_corrige-pos_test);
               mydata->sum_dist=mydata->sum_dist+tmp_distance;
               mydata->tab_dist[mydata->iterations]=tmp_distance;
               
               if (tmp_distance<30)
               {
                  mydata->tabResult[6][i]  = (mydata->tabResult[6][i] + 1.);
                  mydata->nb_success_pos ++;
               }
            }
         }

         mydata->tabResult[0][i]  = (mydata->tabResult[0][i] + 1.);
         mydata->nb_success ++;
         mydata->dd++;
         //mydata->sum_dist=mydata->sum_dist+tmp_distance;
      }

      //Le NN répond et il NE devait PAS repondre => Faux Positif
      else if (neurone[first_neuron_test+i].s1 > seuil && neurone[first_neuron_sup+i].s1 <= seuil)
      {
         mydata->tabResult[1][i] = (mydata->tabResult[1][i] + 1.) ;
         mydata->nb_fail ++;

      }
      //Le NN NE répond PAS et il NE devait PAS repondre => Vrai negatif
      else if (neurone[first_neuron_test+i].s1 <= seuil && neurone[first_neuron_sup+i].s1 <= seuil)
      {
         mydata->tabResult[2][i] = (mydata->tabResult[2][i] + 1.) ;
         mydata->nb_success ++;

      }
      //Le NN NE répond PAS et il devait repondre => Faux negatif
      else if (neurone[first_neuron_test+i].s1 <= seuil && neurone[first_neuron_sup+i].s1 > seuil)
      {
         mydata->tabResult[3][i] = (mydata->tabResult[3][i] + 1.) ;
         mydata->nb_fail ++;

      }
      else { PRINT_WARNING("----> UNKNOWN CASE \n"); fflush(stdout);}
   }

   //tmp_distance=(neurone[first_neuron_sup_pos+dd+taille_sup].s1 - neurone[first_neuron_test_pos].s1);
   //    fprintf(mydata->statdf,"difference_de_distance %d ___ il y a ((%d ))\n\n",dd+1);

   mydata-> moy_dist=(float)mydata->sum_dist/(float)mydata->dd;
   mydata->success = (float)(mydata->nb_success-mydata->nb_fail) / ((float)mydata->iterations *taille_sup ) ;
   mydata->fail = (float)mydata->nb_fail / ((float)mydata->iterations * taille_sup);

   if (mydata->pourcent == 1)
   {
      mydata->success = mydata->success * 100;
      mydata->fail =  mydata->success *100;
   }

   neurone[debut].s = neurone[debut].s1 = neurone[debut].s2 =  mydata->erreur_quad_moy;
   neurone[debut+1].s = neurone[debut+1].s1 = neurone[debut+1].s2 =  mydata->success;
   //neurone[debut+2].s = neurone[debut+2].s1 = neurone[debut+2].s2 = tmp_distance;
}

void destroy_stats_distance(int Gpe)
{
   Mydata_stats *mydata = NULL;
   int i,m,mim=0;
   int taille_sup = 0;
   float a=0.0,ecart;
   mydata = (Mydata_stats *) def_groupe[Gpe].data;

   //mydata-> moy_dist=(float)mydata->sum_dist/(float)mydata->dd;
   ///////////////////Aliaa/////////////////////////////////////////
   m=0;
   for (i=0; i<2000; i++)
   {
      if (mydata->tab_dist[i]<30)
      {
         mim++;
      }
      if (mydata->tab_dist[i]>=0.)
      {
         ecart=fabs(mydata->tab_dist[i]-mydata-> moy_dist);
         a=a+(ecart*ecart);
         m++;
      }
   }
   mydata->ecartype=sqrt(a/m);
   if (m!=mydata->dd) dprints("pb sur m=%d et dd=%d \n",m,mydata->dd) ;
   ////////////////////////////////////////////////////////////////
   if (mydata == NULL)  EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name,__FUNCTION__);
   if (mydata->infile)
   {


      //statdf = mydata->statdf;
      taille_sup  = def_groupe[mydata->gpe_supervision].nbre;
      /*Affichage dans un fichier*/
      ///////////////////////////////////ALiaa//////////////////////////////
      for (i=0 ; i<TAILLETABLEAU; i++)
      {

         if (mydata->tab_dist[i]>=0.)
            fprintf( mydata->statdf, "%f \n ",mydata->tab_dist[i]);

         for (m=0; m<320; m++)
         {
            //if (f_egal(mydata->tab_dist[i],m) )
            if ((int)mydata->tab_dist[i]==m)
            {
               mydata->tab_d[m] ++;
            }
         }
      }

////////////////////////////////////////////
      fprintf( mydata->statdf, "************************************ ");
      for (i=0 ; i<320; i++)
      {
         fprintf( mydata->statdf, "%f \n ",mydata->tab_d[i]);
      }
      fprintf( mydata->statdf, "************************************ ");

////////////////////////////////////////////
      fprintf( mydata->statdf,"moyenne_distance= %f \n ",mydata-> moy_dist);
      fprintf( mydata->statdf,"ecartype= %f \n ",  mydata->ecartype);
      fprintf( mydata->statdf,"success_avec_pos= %d \n ",mim);
      fprintf( mydata->statdf,"success_sans_pos= %d \n ",mydata->dd);
      fprintf( mydata->statdf,"SUM= %f \n ",mydata->sum_dist);
      fprintf( mydata->statdf,"DD= %d \n ",mydata->dd);
      fprintf( mydata->statdf,"a= %f \n ",a);
      //fprintf( mydata->statdf,"La faute ERRRRREUUUUUUUUUUERRRRRRRRR %f \n ",mydata->tab_tab[0]);
      //fprintf( mydata->statdf,"La faute ERRRRREUUUUUUUUUUERRRRRRRRR  POS_TEST %f \n ",mydata->tab_tab[1]);
      //fprintf( mydata->statdf,"La faute ERRRRREUUUUUUUUUUERRRRRRRRR POS_CORRIGE %f \n ",mydata->tab_tab[2]);
      //////////////////////////////////////////////////////////////////////
      fprintf( mydata->statdf,"\nVrai Positif position | ");
      for (i=0 ; i<taille_sup; i++)
      {
         if (mydata->tabResult[4][i]>0) fprintf( mydata->statdf, "%0.3f | ",mydata->tabResult[6][i] /mydata->tabResult[4][i] );
         else fprintf( mydata->statdf, " NaN  | ");
      }
/////////////////////////////////////////////////////////////////////
      fprintf( mydata->statdf,"\nVrai Positif | ");
      for (i=0 ; i<taille_sup; i++)
      {
         if (mydata->tabResult[4][i]>0) fprintf( mydata->statdf, "%0.3f | ",mydata->tabResult[0][i] /mydata->tabResult[4][i] );
         else fprintf( mydata->statdf, " NaN  | ");
      }
      fprintf( mydata->statdf,"\nFaux Negatif | ");
      for (i=0 ; i<taille_sup; i++)
      {
         if (mydata->tabResult[4][i]>0) fprintf( mydata->statdf, "%0.3f | ",mydata->tabResult[3][i] /mydata->tabResult[4][i] );
         else fprintf( mydata->statdf, " NaN  | ");
      }
      fprintf( mydata->statdf,"\nFaux Positif | ");
      for (i=0 ; i<taille_sup; i++)
      {
         if (mydata->tabResult[5][i]>0) fprintf( mydata->statdf, "%0.3f | ",mydata->tabResult[1][i] /mydata->tabResult[5][i] );
         else fprintf(mydata->statdf, " NaN   | ");
      }
      fprintf(mydata->statdf,"\nVrai Negatif | ");
      for (i=0 ; i<taille_sup; i++)
      {
         if (mydata->tabResult[5][i]>0) fprintf( mydata->statdf, "%0.3f | ",mydata->tabResult[2][i] /mydata->tabResult[5][i] );
         else fprintf( mydata->statdf, " NaN  | ");
      }
      fprintf( mydata-> statdf,"\n");

      //fclose(((Mydata_stats *) def_groupe[Gpe].data)->statdf);
      fclose(mydata->statdf);
   }
   def_groupe[Gpe].data=NULL;
   free(def_groupe[Gpe].data);
}
















