/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   libSigProc
   f_deriv_erreur f_deriv_erreur

   \brief to be completed

   @author A.Hiolle
   @date 11/08/2004
*/

/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

static int tps;

void function_deriv_erreur(int numero)
{
  int i = 0;
  int Gpe_E2 = -1, Gpe_E1 = -1, Gpe_X = -1;
  float r = -1;
  float tau = 0., val = -1., min = 0.000001;
  int l = -1;
  FILE *fx = NULL;
  float var = -1.;

  cprints
    ("-----------------------deriv Erreur----------------------------\n");

  if (def_groupe[numero].data == NULL)
  {


    tps = 0;
    if (l == -1)
    {

      l = find_input_link(numero, i);

      while (l != -1)
      {
	if (strcmp(liaison[l].nom, "-X") == 0)
	{

	  Gpe_X = liaison[l].depart;

	}
	if (strcmp(liaison[l].nom, "-E1") == 0)
	{
	  Gpe_E1 = liaison[l].depart;
	}

	if (strcmp(liaison[l].nom, "-E2") == 0)
	{
	  Gpe_E2 = liaison[l].depart;
	}

	i++;
	l = find_input_link(numero, i);

      }
    }

    if (Gpe_E1 == -1)
    {
      printf("groupe  ref non trouve dans groupe %d\n", numero);
      exit(0);
    }

    if (Gpe_E2 == -1)
    {
      printf("groupe  pred non trouve dans groupe %d\n", numero);

    }

    def_groupe[numero].data = MANY_ALLOCATIONS(3,int);


    ((int *) def_groupe[numero].data)[0] = Gpe_E1;
    ((int *) def_groupe[numero].data)[1] = Gpe_E2;
    ((int *) def_groupe[numero].data)[2] = Gpe_X;

    if (Gpe_X != -1)
    {

      for (i = 0; i < def_groupe[Gpe_X].nbre; i++)
      {

	r = 1. + (10. * rand() / (RAND_MAX + 1.0));
	/*neurone[def_groupe[numero].premier_ele+i].s1=0.5+0.002*r; */

	neurone[def_groupe[numero].premier_ele + i].s1 = 0.001;
	neurone[def_groupe[numero].premier_ele + i].s =
	  neurone[def_groupe[numero].premier_ele + i].s2 =
	  neurone[def_groupe[numero].premier_ele + i].s1;

	cprints("Init deriv[%d]=%f rand=%f \n", i,
		neurone[def_groupe[numero].premier_ele + i].s1, r);
      }
    }
    else
    {

      neurone[def_groupe[numero].premier_ele].s2 =
	neurone[def_groupe[numero].premier_ele].s =
	neurone[def_groupe[numero].premier_ele].s1 = 0.001;

      neurone[def_groupe[numero].premier_ele].d = 0.;

    }

  }
  else
  {
    tps++;

    Gpe_E1 = ((int *) def_groupe[numero].data)[0];

    Gpe_E2 = ((int *) def_groupe[numero].data)[1];

    Gpe_X = ((int *) def_groupe[numero].data)[2];

  }

  if (Gpe_E2 != -1)
  {
    var =
      (neurone[def_groupe[Gpe_E1].premier_ele].s2 -
       neurone[def_groupe[Gpe_E2].premier_ele].s2);
    /*var=var>=0.?var:0. -var; */
    var = var >= 0. ? min : 0. - var;

  }
  if (Gpe_X == -1)
  {

    tau = def_groupe[numero].learning_rate;
    if (tau >= 0.)
    {
      if (isequal(neurone[def_groupe[numero].premier_ele].d,0.))
      {

	if (Gpe_E2 == -1)
	{

	  /***Calcul de la moyenne de la derivee***/



	  val =
	    neurone[def_groupe[Gpe_E1].premier_ele].s1 >
	    0. ? neurone[def_groupe[Gpe_E1].premier_ele].s1 : 0. -
	    neurone[def_groupe[Gpe_E1].premier_ele].s1;

	  printf("tau deriv=%f ,entree=%f\n", tau,
		 neurone[def_groupe[Gpe_E1].premier_ele].s1);
	  /* moyenne de la derivee sur fenetre tau */

	  cprints("val=%f\n", val);    /*
					 neurone[def_groupe[numero].premier_ele].s2=((tau*neurone[def_groupe[numero].premier_ele].s1)+val)/(tau+1);
				       */

	  neurone[def_groupe[numero].premier_ele].s =
	    neurone[def_groupe[numero].premier_ele].s1 =
	    neurone[def_groupe[numero].premier_ele].s2 = val;


	  cprints("In deriv error i=%d E1=%f E1-E2=%f \n", i,
		  neurone[def_groupe[Gpe_E1].premier_ele].s1,
		  neurone[def_groupe[numero].premier_ele].s2);

	}
	else
	{
	  /*renforcement =1 si >seuil */
	  if (def_groupe[numero].seuil > 0.)
	    var = var > def_groupe[numero].seuil ? 1. : min;

	  neurone[def_groupe[numero].premier_ele].s = var;




	  neurone[def_groupe[numero].premier_ele].s1 =
	    neurone[def_groupe[numero].premier_ele].s2 =
	    neurone[def_groupe[numero].premier_ele].s;
	}
	fx = fopen("positionX", "a");
	fprintf(fx, " %f\n", var);
	neurone[def_groupe[numero].premier_ele].d = 1.0;
	fclose(fx);

      }
      else
      {
	neurone[def_groupe[numero].premier_ele].d = 0.;
	neurone[def_groupe[numero].premier_ele].s1 =
	  neurone[def_groupe[numero].premier_ele].s2 =
	  neurone[def_groupe[numero].premier_ele].s = -1.;


      }
    }
    else
    {
      /*renforcement =1 si >seuil */
      if (def_groupe[numero].seuil > 0.)
	var = var > def_groupe[numero].seuil ? 1. : min;

      neurone[def_groupe[numero].premier_ele].s = var;




      neurone[def_groupe[numero].premier_ele].s1 =
	neurone[def_groupe[numero].premier_ele].s2 =
	neurone[def_groupe[numero].premier_ele].s;

      fx = fopen("positionX", "a");
      fprintf(fx, " %f\n", var);
      neurone[def_groupe[numero].premier_ele].d = 1.0;
      fclose(fx);



    }
    cprints("Deriv[%d]=%f  \n", i,
	    neurone[def_groupe[numero].premier_ele].s);
  }
  else
  {
    /*      
	    memset(file,0,50);
	    memset(num,0,4);
	    strcpy(file,"erreur");

    */ for (i = 0; i < def_groupe[Gpe_X].nbre; i++)
    {

      if (neurone[def_groupe[Gpe_X].premier_ele + i].s1 > 0.)
	break;
    }

    /*      sprintf(num,"%d",i);
	    strcat(file,num);
	    printf("gpe=%d synapse[%d] fichier %s \n",numero,i,file);

	    fd=fopen(file,"a");
    */
    if (Gpe_E2 != -1)
    {
      /***Calcul de la variation ****/



      /*neurone[def_groupe[numero].premier_ele+i].s2=var>min?var:min; */
      /*** la valeur est rendue positive***/
      neurone[def_groupe[numero].premier_ele + i].s2 = var;
      neurone[def_groupe[numero].premier_ele + i].s2 =
	neurone[def_groupe[numero].premier_ele + i].s2 >
	0. ? neurone[def_groupe[numero].premier_ele + i].s2 : 0. -
	neurone[def_groupe[numero].premier_ele + i].s2;

      /****seuillage inferieur a  0.001***/
      neurone[def_groupe[numero].premier_ele + i].s2 =
	neurone[def_groupe[numero].premier_ele + i].s2 >
	def_groupe[numero].seuil ? neurone[def_groupe[numero].
					   premier_ele + i].s2 : 0.;


      /****seuillage superieur a  1.***/
      /*      
	      neurone[def_groupe[numero].premier_ele+i].s2=neurone[def_groupe[numero].premier_ele+i].s2<1.?neurone[def_groupe[numero].premier_ele+i].s2:1.;
      */

      neurone[def_groupe[numero].premier_ele + i].s =
	neurone[def_groupe[numero].premier_ele + i].s1 =
	neurone[def_groupe[numero].premier_ele + i].s2;

      cprints("In deriv error i=%d E1=%f E2=%f E1-E2=%f \n", i,
	      neurone[def_groupe[Gpe_E1].premier_ele].s1,
	      neurone[def_groupe[Gpe_E2].premier_ele].s1,
	      neurone[def_groupe[numero].premier_ele + i].s2);

      /*} */
      /*fprintf(fd," %f\n",neurone[def_groupe[numero].premier_ele+i].s2); */
      fx = fopen("positionX", "a");
      fprintf(fx, " %f\n", var);

      fclose(fx);

    }
    else
    {
      /***Calcul de la moyenne de la derivee***/

      tau = def_groupe[numero].learning_rate;
      cprints("tau deriv=%f ,entree=%f\n", tau,
	      neurone[def_groupe[Gpe_E1].premier_ele].s1);
      val =
	neurone[def_groupe[Gpe_E1].premier_ele].s1 >
	0. ? neurone[def_groupe[Gpe_E1].premier_ele].
	s1 : -neurone[def_groupe[Gpe_E1].premier_ele].s1;

      /* moyenne de la derivee sur fenetre tau */

      cprints("val=%f\n", val);
      neurone[def_groupe[numero].premier_ele].s2 =
	((tau * neurone[def_groupe[numero].premier_ele].s1) +
	 val) / (tau + 1);


      neurone[def_groupe[numero].premier_ele + i].s =
	neurone[def_groupe[numero].premier_ele + i].s1 =
	neurone[def_groupe[numero].premier_ele + i].s2;


      cprints("In deriv error i=%d E1=%f E1-E2=%f \n", i,
	      neurone[def_groupe[Gpe_E1].premier_ele].s1,
	      neurone[def_groupe[numero].premier_ele + i].s2);


    }

    cprints("Deriv[%d]=%f  \n", i,
	    neurone[def_groupe[numero].premier_ele + i].s);
  }                           
/*
  fclose(fd);
*/
}
