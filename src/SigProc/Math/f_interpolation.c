/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_interpolation.c 
\brief 

Author: VILLEMIN Jean-Baptiste
Created: 2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
- Cette fonction calcule le barycentre des valeurs des neurones.
Utile pour connaitre la valeur analogique interpolee d'une primitive pour un visage.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* A ENLEVER SI ON VEUT UNE INTERPOLATION LINEAIRE */
#define SIGMOIDE

typedef struct {
  int gpe_data; /* gpe du groupe precedent contenant les donnees */
  float pas; /* pas de ponderation */
} data;

void new_interpolation (int gpe)
{

  data *my_data;
  int nbre;
  int link = -1;
  
#ifdef DEBUG
  printf(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);
#endif
  
/**
 * on cree la structure qui contiendra les donnees
 */
  
  my_data = (data *) malloc(sizeof(data));
  
  if (my_data == NULL)
    {
      printf("error in memory allocation in group %d %s\n", gpe,__FUNCTION__);
      exit(EXIT_FAILURE);
    }
  /* On initialise cette structure */
  my_data->gpe_data = -1;
  my_data->pas = 0;
  
  
/**
* On recherche les liens et les numéros des boites entrantes
*/
  link = find_input_link(gpe, 0);
  
  if (link == -1)
    {
      printf("Il n'y a pas de lien en entree du Gpe: %d fonction :%s\n",gpe,__FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  my_data->gpe_data = liaison[link].depart;
  
#ifdef DEBUG
  printf("GpeData = %d\n",my_data->gpe_data );
#endif
  
/**
 * On recupere le nombre de neurone du groupe gpe_data pour calculer le pas discretisant
 */
  nbre = def_groupe[my_data->gpe_data].nbre;
#ifdef DEBUG
  printf("Nombre de neurone = %d\n",nbre);
#endif
  if(nbre!=1)
    my_data->pas = (float)(1.0 / ( (float)(nbre)-1.0));
  else
    my_data->pas = 1.0;
  
#ifdef DEBUG
  printf("Pas = %f\n",my_data->pas);
#endif
  
  /**
   * Passe les donnes a la fonction
   */
  def_groupe[gpe].data = my_data;
  
#ifdef DEBUG
  printf(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);
#endif
  
}


/**
*	------------------- FIN DU CONSTRUCTEUR ------------------------
*/

void function_interpolation(int gpe)
{
  data *my_data = NULL;
  int nbre;
  int i,first,firstdata;
  float valeur = 0;
  float activite =0;
  
#ifdef DEBUG
  printf("---------------------- Begin of %s --------------------\n", __FUNCTION__);
#endif
  
  /**
   * Recuperation donnees data
   */
  
  my_data = (data *) def_groupe[gpe].data;   
  if (my_data == NULL)
    {
      printf("error while loading data in group %d %s\n", gpe,__FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  /**
   * Calcul de l'interpolation
   */
  nbre = def_groupe[my_data->gpe_data].nbre;
  firstdata = def_groupe[my_data->gpe_data].premier_ele;
  first = def_groupe[gpe].premier_ele;
  
  for (i=0;i<nbre;i++)
    {
      activite += neurone[firstdata + i].s;
      valeur +=  i* my_data->pas* neurone[firstdata + i].s;
    }
  if (activite > 0.001)
    valeur = valeur / activite;

#ifdef SIGMOIDE
  valeur = 0.5 + 0.5 * sin((valeur * M_PI) - (M_PI / 2));
#endif
  
  /**
   * MAJ neurone boite
   */
  neurone[first].s = neurone[first].s1 =neurone[first].s2 = valeur;
  
#ifdef DEBUG
  printf(" Valeur analogique interpolee : %f \n", valeur);
  printf("----------------------End of %s --------------------\n", __FUNCTION__);
#endif
  
}

