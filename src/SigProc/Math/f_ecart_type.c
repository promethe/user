/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSigProc
\defgroup f_ecart_type f_ecart_type
\author	D. Bailly
\date	25/02/2010
\brief 	Calculate the standard deviation (ecart type) of the potentials of the input group

\section description
Calculate the mathematical standard deviation (ecart type) of the potentials of the input group.
*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <net_message_debug_dist.h>
#include <libx.h>

#include <Kernel_Function/find_input_link.h>

typedef struct data_ecart_type
{
  int debut;
  int fin;
} Data_ecart_type;



void new_ecart_type(int index_of_group)
{
  Data_ecart_type *data;
  int l;

  
#ifdef DEBUG
    printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  l = find_input_link(index_of_group, 0);
  if(l==-1) EXIT_ON_ERROR("no input link\n");
  
  data = (Data_ecart_type*)ALLOCATION(Data_ecart_type);
  data->debut = def_groupe[liaison[l].depart].premier_ele;
  data->fin = data->debut + def_groupe[liaison[l].depart].nbre;
  def_groupe[index_of_group].data = data;
  
  /* a little message to help find bugs */
  if(find_input_link(index_of_group, 1) != -1) PRINT_WARNING("there is an unused input link\n");

#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


void function_ecart_type(int index_of_group)
{
  int i, debut, fin, nbr, premier_ele;
  float moyenne, moyenne1, moyenne2, variance, variance1, variance2;
  Data_ecart_type *data;
/*   type_coeff *synapse=NULL; */

    
#ifdef DEBUG
    printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif

  data = (Data_ecart_type *)def_groupe[index_of_group].data;
  debut = data->debut;
  fin = data->fin;
  nbr = fin - debut;
  premier_ele = def_groupe[index_of_group].premier_ele;
  
  moyenne = 0.0;
  moyenne1 = 0.0;
  moyenne2 = 0.0;
  for(i=debut; i<fin; i++)
  {
      moyenne += neurone[i].s;
      moyenne1 += neurone[i].s1;
      moyenne2 += neurone[i].s2;
    /*synapse = neurone[i].coeff;
    while(synapse != NULL)
    {
      moyenne += synapse->val * neurone[synapse->entree].s;
      moyenne1 += synapse->val * neurone[synapse->entree].s1;
      moyenne2 += synapse->val * neurone[synapse->entree].s2;
      
      synapse = synapse->s;
    }*/
  }
  moyenne /= (float)nbr;
  moyenne1 /= (float)nbr;
  moyenne2 /= (float)nbr;
  
  variance = 0.0;
  variance1 = 0.0;
  variance2 = 0.0;
  for(i=debut; i<fin; i++)
  {
    variance += (neurone[i].s - moyenne) * (neurone[i].s - moyenne);
    variance1 += (neurone[i].s1 - moyenne1) * (neurone[i].s1 - moyenne1);
    variance2 += (neurone[i].s2 - moyenne2) * (neurone[i].s2 - moyenne2);
  }
  variance /= (float)nbr;
  variance1 /= (float)nbr;
  variance2 /= (float)nbr;
  
  neurone[premier_ele].s = sqrt(variance);
  neurone[premier_ele].s1 = sqrt(variance1);
  neurone[premier_ele].s2 = sqrt(variance2);
  
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


void destroy_ecart_type(int index_of_group)
{
  
#ifdef DEBUG
  printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  free(def_groupe[index_of_group].data);
  
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}

#undef DEBUG
