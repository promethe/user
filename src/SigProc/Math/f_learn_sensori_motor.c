/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: Maickael Maillard
Created: 01/04/2004
Modified:
- author: M. Maillard
- description: specific file creation
- date: 19/09/2005

Theoritical description:

Description: 
Fonction entierement ad-hoc gerant les durees d'apprentissage des groupes.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/



#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

#undef DEBUG

typedef struct TAG_SENSORI_MOTOR_DATA
{
    char learn_imagettes;
    char boussole;
    char learn_per;

    int neurone_input;
} sensori_motor_data;

void function_learn_sensori_motor(int Gpe)
{
    int lien;

    int deb_gpe = -1;
    int neurone_input;
    int input_gpe = -1;
    int longueur;
    sensori_motor_data *my_data = NULL;
    char learn_imagettes, boussole, learn_per;

#ifdef DEBUG
    printf("entree dans fonction add_local_to_global Gpe %d \n", Gpe);
#endif
    if (def_groupe[Gpe].data == NULL)
    {

        if ((lien = find_input_link(Gpe, 0)) == -1)
        {
            printf
                ("Erreur de lien entrant dans function_learn_sensori_motor : gpe %d\n",
                 Gpe);
            exit(1);
        }
        input_gpe = liaison[lien].depart;
        neurone_input = def_groupe[input_gpe].premier_ele;


        deb_gpe = def_groupe[Gpe].premier_ele;
        longueur = def_groupe[Gpe].nbre;

        if (4 != longueur)
        {
            printf("Longueur mismatch in f_learn_sensori_motor gpe %d\n",
                   Gpe);
            exit(1);
        }

        my_data = malloc(sizeof(sensori_motor_data));
        if (my_data == NULL)
        {
            printf("Ereur malloc dans Gpe %d f_learn_sensori_motor\n", Gpe);
            exit(1);
        }

        learn_imagettes = my_data->learn_imagettes = 0;
        boussole = my_data->boussole = 0;
        learn_per = my_data->learn_per = 0;
        my_data->neurone_input = neurone_input;

        def_groupe[Gpe].data = (void *) my_data;
    }
    else
    {
        my_data = (sensori_motor_data *) def_groupe[Gpe].data;
        learn_imagettes = my_data->learn_imagettes;
        boussole = my_data->boussole;
        learn_per = my_data->learn_per;
        neurone_input = my_data->neurone_input;

        deb_gpe = def_groupe[Gpe].premier_ele;
    }

    if (neurone[neurone_input].s1 > 0.5 && (!learn_per))
    {
        learn_imagettes = 2;
        boussole = 2;
        learn_per = 25;
    }

    if (learn_imagettes == 2)
    {
        neurone[deb_gpe].s = neurone[deb_gpe].s1 = neurone[deb_gpe].s2 = 1.0000000; /*learn_imagette */
        neurone[deb_gpe + 1].s = neurone[deb_gpe + 1].s1 = neurone[deb_gpe + 1].s2 = 0.9200000; /*vig */
        neurone[deb_gpe + 2].s = neurone[deb_gpe + 2].s1 = neurone[deb_gpe + 2].s2 = 1.0000000; /*boussole */
        neurone[deb_gpe + 3].s = neurone[deb_gpe + 3].s1 = neurone[deb_gpe + 3].s2 = 1.000000;  /*vig2 */
        learn_imagettes--;
        boussole--;
        learn_per--;
    }
    else if (learn_imagettes == 1)
    {
        neurone[deb_gpe].s = neurone[deb_gpe].s1 = neurone[deb_gpe].s2 =
            1.0000000;
        neurone[deb_gpe + 1].s = neurone[deb_gpe + 1].s1 =
            neurone[deb_gpe + 1].s2 = 0.9200000;
        neurone[deb_gpe + 2].s = neurone[deb_gpe + 2].s1 =
            neurone[deb_gpe + 2].s2 = 1.000000;
        neurone[deb_gpe + 3].s = neurone[deb_gpe + 3].s1 =
            neurone[deb_gpe + 3].s2 = 0.0000001;
        learn_imagettes--;
        boussole--;
        learn_per--;
    }
    else if (learn_per)
    {
        neurone[deb_gpe].s = neurone[deb_gpe].s1 = neurone[deb_gpe].s2 =
            0.0000000;
        neurone[deb_gpe + 1].s = neurone[deb_gpe + 1].s1 =
            neurone[deb_gpe + 1].s2 = 0.9100000;
        neurone[deb_gpe + 2].s = neurone[deb_gpe + 2].s1 =
            neurone[deb_gpe + 2].s2 = 1.0000000;
        neurone[deb_gpe + 3].s = neurone[deb_gpe + 3].s1 =
            neurone[deb_gpe + 3].s2 = 0.0000001;
        boussole--;
        learn_per--;
    }
    else
    {
        neurone[deb_gpe].s = neurone[deb_gpe].s1 = neurone[deb_gpe].s2 =
            0.0000000;
        neurone[deb_gpe + 1].s = neurone[deb_gpe + 1].s1 =
            neurone[deb_gpe + 1].s2 = 0.8900000;
        neurone[deb_gpe + 2].s = neurone[deb_gpe + 2].s1 =
            neurone[deb_gpe + 2].s2 = 0.0000000;
        neurone[deb_gpe + 3].s = neurone[deb_gpe + 3].s1 =
            neurone[deb_gpe + 3].s2 = -1.0000000;
    }

    my_data->learn_imagettes = learn_imagettes;
    my_data->boussole = boussole;
    my_data->learn_per = learn_per;

    return;
}
