/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_find_scale_space_keypoints.c 
\brief 

Author: Mickael Maillard
Created: 01/07/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 *fonction recherchant des keypoint a travers les echelles
 *recherche par les extremums
 *voisinage de recherche cubique 3x3x3
 *les points trouves sont mis sur une carte de 0 avec la valeur 255
 *3 echelles doivent etre entres en lien (si DOG alors extremums du laplacien)


Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-QuickSortAbs()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Typedef/boolean.h>
#include <Struct/prom_images_struct.h>

#undef VISU
#undef DEBUG
/*#define DEBUG*/

#include <math.h>
void QuickSortAbs(float **arr, int lidx, int ridx);

void QuickSortAbs(float **arr, int lidx, int ridx)
{

    int k;
    float *buffer;
    int e;
    int mid;

    if (lidx >= ridx)
        return;

    mid = (int) ((lidx + ridx) / 2);
    buffer = arr[lidx];
    arr[lidx] = arr[mid];
    arr[mid] = buffer;

    e = lidx;
    for (k = lidx + 1; k <= ridx; k++)

        if (fabs(*(*(arr + k) + 0)) > fabs(*(*(arr + lidx) + 0)))
        {
            e++;
            buffer = arr[e];
            arr[e] = arr[k];
            arr[k] = buffer;
        }

    buffer = arr[lidx];
    arr[lidx] = arr[e];
    arr[e] = buffer;

    QuickSortAbs(arr, lidx, e - 1);
    QuickSortAbs(arr, e + 1, ridx);
}


void function_find_scale_space_keypoints(int Gpe)
{

    int Gpe_Up = -1, Gpe_Down = -1, Gpe_Cur = -1;
    float *outputImage, *up_image, *down_image, *cur_image;
    int i, row, column;
    int sx, sy, nb_band;
    float trace_carre, det, curvRatio;
    float hessien[2][2];
    char *chaine = NULL;
    boolean isMax = FALSE;
    boolean isMin = FALSE;
    float temp;
    int compt = 0;

    float **tri_table;

    /*definition des seuils pas Touche ils sont valides */
    int MAXKEYPOINTS = 100;     /*on prend que les MAXKEYPOINTS meilleurs */
    float RATIOTHRESHOLD = (float) 12.1;    /*rapport entre les valeurs propres pour avoir un point bien localise */
    float NOISETHRESHOLD = (float) 4.;  /*4.55 */

    if (def_groupe[Gpe].ext == NULL)
    {
        /*recherche du numero de groupe entrant et gestion des erreurs */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                if (strstr(chaine, "-UP") != NULL)
                    Gpe_Up = liaison[i].depart;
                else if (strstr(chaine, "-DOWN") != NULL)
                    Gpe_Down = liaison[i].depart;
                else if (strstr(chaine, "-CUR") != NULL)
                {
                    Gpe_Cur = liaison[i].depart;

                    /*si on veut mettre des parametres sur le lien : */
                    /*if( (param=strstr(chaine,"-RH"))!=NULL )
                       RATIOTHRESHOLD=(float)atof(&param[3]);
                       if( (param=strstr(chaine,"-NT"))!=NULL )
                       NOISETHRESHOLD=(float)atof(&param[3]); */
                }
                else
                {
                    printf
                        ("%s : parametre des liens entrants incorrects (-UP, -CUR et -DOWN)\n",
                         __FUNCTION__);
                    exit(0);
                }
            }
        }

        if (Gpe_Up == -1 || Gpe_Down == -1 || Gpe_Cur == -1)
        {
            printf
                ("%s : il manque une liaison entrante ou un parametre est incorrect\n",
                 __FUNCTION__);
            exit(0);
        }

        if (def_groupe[Gpe_Up].ext == NULL || def_groupe[Gpe_Down].ext == NULL
            || def_groupe[Gpe_Cur].ext == NULL)
        {
            printf
                ("%s : un des pointeurs des liens entrants n'a pas ete initialise\n",
                 __FUNCTION__);
            /*exit(0); */
            return;
        }

        /*verification du format des images */
        if (((prom_images_struct *) def_groupe[Gpe_Up].ext)->nb_band != 4 ||
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->nb_band != 4 ||
            ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->nb_band != 4)
        {
            printf("%s les images doivent etre en floatant N&B...\n",
                   __FUNCTION__);
            exit(0);
        }
        nb_band = ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->nb_band;

        /*verification de la compatibilite des tailles d'images */
        if (((prom_images_struct *) def_groupe[Gpe_Up].ext)->sx !=
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->sx
            || ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sy !=
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->sy
            || ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->sx !=
            ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sx
            || ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->sy !=
            ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sy)
        {
            printf("%s : les 3 images doivent etre de meme taille...\n",
                   __FUNCTION__);
            exit(0);
        }
        sx = ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->sy;


        /*allocation de memoire pour la structure resulante */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /*stockage des pointeurs entrants */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            ((prom_images_struct *) def_groupe[Gpe_Up].ext)->images_table[0];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            ((prom_images_struct *) def_groupe[Gpe_Cur].ext)->images_table[0];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->
            images_table[0];

        /*stockage des infos dans la structure */
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nb_band;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        /*allocation de l'image */
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
            malloc(sx * sy * nb_band);
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("%s : Gpe : %d ALLOCATION DE MEMOIRE IMPOSSIBLE (%d)...\n",
                   __FUNCTION__, Gpe, __LINE__);
            exit(0);
        }
    }
    else
    {
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    }

    outputImage =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    up_image =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[3]);
    cur_image =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[2]);
    down_image =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[1]);

    compt = 0;
    for (row = 0; row < sy; row++)
        for (column = 0; column < sx; column++)
            *(outputImage + row * sx + column) = 0;

    /*on regarde si le points est un extremum local dans un voisinage 3x3x3 */
    for (row = 1; row < (sy - 1); row++)
        for (column = 1; column < (sx - 1); column++)
        {
            temp = *(cur_image + row * sx + column);
            if (temp > NOISETHRESHOLD || temp < (-NOISETHRESHOLD))  /*valeurs du point plus importante qu'un seuil de bruit */
            {                   /*reherche d'un maximum local */
                isMax = temp > *(cur_image + (row - 1) * sx + column - 1) &&
                    temp > *(cur_image + (row - 1) * sx + column) &&
                    temp > *(cur_image + (row - 1) * sx + column + 1) &&
                    temp > *(cur_image + row * sx + column - 1) &&
                    temp > *(cur_image + row * sx + column + 1) &&
                    temp > *(cur_image + (row + 1) * sx + column - 1) &&
                    temp > *(cur_image + (row + 1) * sx + column) &&
                    temp > *(cur_image + (row + 1) * sx + column + 1);
                if (isMax)
                {
                    isMax = temp > *(down_image + (row - 1) * sx + column - 1)
                        && temp > *(down_image + (row - 1) * sx + column)
                        && temp > *(down_image + (row - 1) * sx + column + 1)
                        && temp > *(down_image + row * sx + column - 1)
                        && temp > *(down_image + row * sx + column)
                        && temp > *(down_image + row * sx + column + 1)
                        && temp > *(down_image + (row + 1) * sx + column - 1)
                        && temp > *(down_image + (row + 1) * sx + column)
                        && temp > *(down_image + (row + 1) * sx + column + 1);

                    if (isMax)
                    {
                        isMax =
                            temp > *(up_image + (row - 1) * sx + column - 1)
                            && temp > *(up_image + (row - 1) * sx + column)
                            && temp >
                            *(up_image + (row - 1) * sx + column + 1)
                            && temp > *(up_image + row * sx + column - 1)
                            && temp > *(up_image + row * sx + column)
                            && temp > *(up_image + row * sx + column + 1)
                            && temp >
                            *(up_image + (row + 1) * sx + column - 1)
                            && temp > *(up_image + (row + 1) * sx + column)
                            && temp >
                            *(up_image + (row + 1) * sx + column + 1);
                    }
                }
                if (!isMax)     /*recherche d'un minimum local */
                {

                    isMin = temp < *(cur_image + (row - 1) * sx + column - 1)
                        && temp < *(cur_image + (row - 1) * sx + column)
                        && temp < *(cur_image + (row - 1) * sx + column + 1)
                        && temp < *(cur_image + row * sx + column - 1)
                        && temp < *(cur_image + row * sx + column + 1)
                        && temp < *(cur_image + (row + 1) * sx + column - 1)
                        && temp < *(cur_image + (row + 1) * sx + column)
                        && temp < *(cur_image + (row + 1) * sx + column + 1);
                    if (isMin)
                    {
                        isMin =
                            temp < *(down_image + (row - 1) * sx + column - 1)
                            && temp < *(down_image + (row - 1) * sx + column)
                            && temp <
                            *(down_image + (row - 1) * sx + column + 1)
                            && temp < *(down_image + row * sx + column - 1)
                            && temp < *(down_image + row * sx + column)
                            && temp < *(down_image + row * sx + column + 1)
                            && temp <
                            *(down_image + (row + 1) * sx + column - 1)
                            && temp < *(down_image + (row + 1) * sx + column)
                            && temp <
                            *(down_image + (row + 1) * sx + column + 1);

                        if (isMin)
                        {
                            isMin =
                                temp <
                                *(up_image + (row - 1) * sx + column - 1)
                                && temp <
                                *(up_image + (row - 1) * sx + column)
                                && temp <
                                *(up_image + (row - 1) * sx + column + 1)
                                && temp < *(up_image + row * sx + column - 1)
                                && temp < *(up_image + row * sx + column)
                                && temp < *(up_image + row * sx + column + 1)
                                && temp <
                                *(up_image + (row + 1) * sx + column - 1)
                                && temp <
                                *(up_image + (row + 1) * sx + column)
                                && temp <
                                *(up_image + (row + 1) * sx + column + 1);
                        }
                    }
                }
                if (isMax || isMin) /*si on a un extremum on regarde si la localisation est correcte */
                {
                    /*calcul du hessien pour avoir les courbures du signal */
                    hessien[0][0] = *(cur_image + (row + 1) * sx + column)
                        - 2 * *(cur_image + row * sx + column)
                        + *(cur_image + (row - 1) * sx + column);

                    hessien[0][1] =
                        (float) (0.25) *
                        (*(cur_image + (row + 1) * sx + column + 1) -
                         *(cur_image + (row + 1) * sx + column - 1) -
                         *(cur_image + (row - 1) * sx + column + 1) +
                         *(cur_image + (row - 1) * sx + column - 1));

                    hessien[1][0] = hessien[0][1];

                    hessien[1][1] = *(cur_image + row * sx + column + 1)
                        - 2 * *(cur_image + row * sx + column)
                        + *(cur_image + row * sx + column - 1);

                    trace_carre =
                        (hessien[0][0] + hessien[1][1]) * (hessien[0][0] +
                                                           hessien[1][1]);
                    det =
                        (hessien[0][0] * hessien[1][1]) -
                        (hessien[1][0] * hessien[0][1]);

                    if (fabs(det) > 0.000000001)    /*sinon le point est mal localise */
                    {
                        curvRatio = (float) (trace_carre / det);
                        if (curvRatio < RATIOTHRESHOLD) /*ou fabs(curvRatio) */
                        {
                            /*il s'agit bien d'un point caracteristique */
                            *(outputImage + row * sx + column) =
                                /*(float)255; */ *(cur_image + row * sx +
                                                   column);
                            compt++;
#ifdef DEBUG
                            printf
                                ("keypoint trouve par le groupe %d en row:%d column:%d\n",
                                 Gpe, row, column);
#endif
                        }

                    }

                }
            }
        }                       /*fin parcours de l'image */

    if (compt != 0)             /*Test ki dur... on garde les MAXKEYPOINTS meilleurs au sens de la dog */
    {
        tri_table = (float **) malloc(compt * sizeof(float *));
        if (tri_table == NULL)
        {
            printf("%s : Gpe : %d ALLOCATION DE MEMOIRE IMPOSSIBLE (%d)...\n",
                   __FUNCTION__, Gpe, __LINE__);
            exit(0);
        }
        for (i = 0; i < compt; i++)
        {
            *(tri_table + i) = (float *) malloc(3 * sizeof(float));
            if (*(tri_table + i) == NULL)
            {
                printf
                    ("%s : Gpe : %d ALLOCATION DE MEMOIRE IMPOSSIBLE (%d)...\n",
                     __FUNCTION__, Gpe, __LINE__);
                exit(0);
            }
        }
        i = 0;
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
            {
                if (fabs(*(outputImage + row * sx + column)) > 0.00000001)
                {
                    *(*(tri_table + i) + 1) = (float) row;
                    *(*(tri_table + i) + 2) = (float) column;
                    *(*(tri_table + i) + 0) =
                        (float) (*(cur_image + row * sx + column));
                    i++;
                }
            }


        QuickSortAbs(tri_table, 0, compt - 1);

        for (i = MAXKEYPOINTS - 1; i < compt; i++)
            *(outputImage + (int) (*(*(tri_table + i) + 1)) * sx +
              (int) (*(*(tri_table + i) + 2))) = 0;

        for (i = 0; i < compt; i++)
            free(*(tri_table + i));
        free(tri_table);
    }
    return;
}
