/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSig_proc
\defgroup f_perspective_to_2D f_perspective_to_2D


\section Modified
- author: XXXXX
- description: specific file creation
- date: XX/XX/XXXX

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description


\section Macro
-none
\section Local variables
-int i, l, j,gpe_image, sx,sy,cpt,k,nb_band;
-prom_images_struct *pt_out=NULL, *pt_in;
-MyData_f_perspective_to_2D *my_data = NULL;
-float ** A;
-float * X,*Y;
-int theX,theY;
-int gpe_matrice=-1;
-int largeur=-1;
-char param_link[255];

\section Global variables
-none

\section Internal Tools
-MyData_f_perspective_to_2D()


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/



#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <public_tools/Vision.h>

typedef struct MyData_f_perspective_to_2D
{
	int gpe_matrice;
    	int gpe_image;
    	float ** A;
	int largeur;
} MyData_f_perspective_to_2D;

void function_perspective_to_2D(int gpe)
{
    int gpe_image = -1;
    int i, l, j,sx,sy,cpt,k,nb_band;
    prom_images_struct *pt_out=NULL, *pt_in;
    MyData_f_perspective_to_2D *my_data = NULL;
    float ** A; 
    float * X,*Y;
    int theX,theY;
    int gpe_matrice=-1;
    int largeur=-1;
    char param_link[255];

	A=(float**)malloc(sizeof(float*)*3);
	for(i=0;i<3;i++)	
		A[i]=(float*)malloc(sizeof(float)*3);
	X=malloc(sizeof(float)*3);
	Y=malloc(sizeof(float)*3);
	cprints("function_perspective_to_2D\n");

    if (def_groupe[gpe].data == NULL)
    {
        cpt = 0;
        l = find_input_link(gpe, cpt);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "matrice") == 0)
            {
		if(def_groupe[liaison[l].depart].nbre!=9)
		{
			EXIT_ON_ERROR("matride in de %s(%d) n est pas 3 3\n",__FUNCTION__,gpe);
		}
		gpe_matrice=liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "image") == 0)
            {
                gpe_image = liaison[l].depart;
            }
            if (prom_getopt(liaison[l].nom, "l",param_link) ==2)
            {
               largeur=atoi(param_link);
            } 

            cpt++;
            l = find_input_link(gpe, cpt);
        }
        if (def_groupe[gpe_image].ext == NULL || gpe_image == -1)
        {
          EXIT_ON_ERROR("pas d'image ds l'ext du groupe image de f_perspective_to_2D(%d)\n",gpe);
        }

        pt_in = ((prom_images_struct *) (def_groupe[gpe_image].ext));

        if (pt_in->nb_band == 3)
        {
            pt_out = calloc_prom_image(2, largeur, largeur, 3);
        }
        else if (pt_in->nb_band == 1)
        {
            pt_out = calloc_prom_image(2, largeur, largeur, 1);
        }
	else
	{
		cprints("bande naturel SVP dans  f_perspective_to_2D(%d)\n",
                 gpe);
	}

        my_data =
            (MyData_f_perspective_to_2D *)
            malloc(sizeof(MyData_f_perspective_to_2D));
        if (my_data == NULL)
        {
          EXIT_ON_ERROR("erreur malloc dans f_perspective_to_2D\n");
        }
        my_data->gpe_image = gpe_image;
        my_data->A = A;
        my_data->gpe_matrice=gpe_matrice;
	my_data->largeur=largeur;
	def_groupe[gpe].data = (MyData_f_perspective_to_2D *) my_data;
        def_groupe[gpe].ext = (prom_images_struct *) pt_out;
    }
    else
    {
        my_data = (MyData_f_perspective_to_2D *) def_groupe[gpe].data;
        gpe_image = my_data->gpe_image;
	gpe_matrice = my_data->gpe_matrice;
    	A=my_data->A;
	largeur=my_data->largeur;
	pt_in = ((prom_images_struct *) (def_groupe[gpe_image].ext));
	pt_out = ((prom_images_struct *) (def_groupe[gpe].ext));
    }

	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
			A[i][j]=neurone[def_groupe[gpe_matrice].premier_ele+i*3+j].s1;
	sx=pt_in->sx;
	sy=pt_in->sy;
	nb_band=pt_in->nb_band;
	for(i=0;i<3;i++)
	{	
		for(j=0;j<3;j++)
			cprints("%f \t",A[i][j]);
		cprints("\n");
	}	
	cprints("code effectif de %s: %d %d pts, %d nb_band\n",__FUNCTION__,sx,sy,nb_band);
	for(j=0;j<largeur;j++)
	for(i=0;i<largeur;i++)
	{	
		/*formation du point traited*/
		X[0]=i;X[1]=j;X[2]=1;

		for(k=0;k<3;k++)
		{
			Y[k]=X[0]*A[k][0]+X[1]*A[k][1]+X[2]*A[k][2];
		}
		Y[0]=Y[0]/Y[2];
		Y[1]=Y[1]/Y[2];

		/*simple recopie*/
		/*if(nb_band==3)
		{
			pt_out->images_table[0][(int)(j*sx+i)*3]  = pt_in->images_table[0][(int)(j*sx+i)*3];
			pt_out->images_table[0][(int)(j*sx+i)*3+1]=pt_in->images_table[0][(int)(j*sx+i)*3+1];
			pt_out->images_table[0][(int)(j*sx+i)*3+2]=pt_in->images_table[0][(int)(j*sx+i)*3+2];
		}
		else if(nb_band==1)
		{ 
			pt_out->images_table[0][(int)(Y[1]*sx+Y[0])]=pt_out->images_table[0][(int)(j*sx+i)];
		}*/
		if(Y[0]>0 && Y[0]<sx && Y[1]>0 && Y[1]<sy )
		{
			theX=(int)(Y[0]);
			theY=(int)(Y[1]);
/* 			crintf("xyd = %f %f \t xyf %f %f\n",X[0],X[1],Y[0],Y[1]);*/
 			if(nb_band==3)
			{
 				pt_out->images_table[0][(j*largeur+i)*3]  = pt_in->images_table[0][(int)(theY*sx+theX)*3];
 				pt_out->images_table[0][(j*largeur+i)*3+1]= pt_in->images_table[0][(int)(theY*sx+theX)*3+1];
 				pt_out->images_table[0][(j*largeur+i)*3+2]= pt_in->images_table[0][(int)(theY*sx+theX)*3+2];
 				/* Dessin du cadre
 					pt_out->images_table[0][(int)((theY*sx+theX)*3)]  = (unsigned char) 250;
 					pt_out->images_table[0][(int)((theY*sx+theX)*3+1)]=(unsigned char)250;
 					pt_out->images_table[0][(int)((theY*sx+theX)*3+2)]=(unsigned char)250;
	#ifndef AVEUGLE*/
/*					TxDessinerCercle(&image2,couleur, TxPlein,point1, 5, 2);
					point1.x=Y[0];
					point1.y=Y[1]; 
					TxFlush(&image2);
	#endif*/
			}
			else if(nb_band==1)
			{ 
				pt_out->images_table[0][(int)(j*largeur+i)]=255;
			}
		}
/*
 		if(Y[0]>0 && Y[0]<sx && Y[1]>0 && Y[1]<sy )
 		{	
 			if(nb_band==3)
 			{
 				pt_out->images_table[0][(j*sx+i)*3]  = pt_in->images_table[0][(int)(Y[1]*sx+Y[0])*3];
 				pt_out->images_table[0][(j*sx+i)*3+1]= pt_in->images_table[0][(int)(Y[1]*sx+Y[0])*3+1];
 				pt_out->images_table[0][(j*sx+i)*3+2]= pt_in->images_table[0][(int)(Y[1]*sx+Y[0])*3+2];
 			}
 			else if(nb_band==1)
 			{n
 				pt_out->images_table[0][(j*sx+i)]  = pt_in->images_table[0][(int)(Y[1]*sx+Y[0])];
 				
 			}
 		}
 		else
 		{
 		if(nb_band==3)
 		{
 			pt_out->images_table[0][(j*sx+i)*3]  =(unsigned char) 255;
 			pt_out->images_table[0][(j*sx+i)*3+1]=(unsigned char) 0;
 			pt_out->images_table[0][(j*sx+i)*3+2]=(unsigned char)0;
 		}
 		else if(nb_band==1)
 		{
 				pt_out->images_table[0][(j*sx+i)]  =(unsigned char) 255;
 		
 		}
  		}       */
	}	
cprints("fin function_perspective_to_2D\n");
}
