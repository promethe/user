/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\ingroup libSigProc
\defgroup  f_statistics_one f_statistics_one
\brief 

Author: R.Shirakawa
Created: 25/04/2006

Modified:10/08/2006
Author: R.Shirakawa
Modification: Maintenant c'est possible de l'utiliser pour faire une statistic d'un reseau de plus d'un neurone.

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction permettant de calculer le pourcentage de reussite lors d'un test de reseau de neuronnes 
en comparant les sorties du group d'apprentissage (0 ou 1) et du group de test (float entre 0 et 1)

comme entree, il doit avoir "-s" pour l'entree de supervision et "-t" pour l'entree de test et sur la liasion de test,
on peut metre l'optionnel "-l" suivi d'un numero, que sera le Seuil pour le neurone de test etre considere allume.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()


Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none yet..

Todo: see author for testing and commenting the function

 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define CORRECT 0
#define FALSE_POSITIVE 1
#define FALSE_NEGATIVE 2
#define TAILLE_HISTORIQUE 100
#define SEUIL 0.8

typedef struct
{
	int iterations;             /* nombre d'iterations */
	float seuil;                /* seuil de la bonne reponse */
	float success;              /* nombre de prototypes correctement reconnus */
	float false_positive;       /* indique le nombre de fois que la sortie est plus grand que le seuil erronement */
	float false_negative;       /* indique le nombre de fois que la sortie est moins grand que le seuil erronement */
	int gpe_supervision;        /* numero du groupe d'entree correspondant a l'apprentissage */
	int gpe_test;               /* numero du groupe d'entree de supervision */
	int historique[TAILLE_HISTORIQUE];  /* vecteur avec les 100 dernieres reponses */
	int premier_n_sup;          /* numero du premier neurone du groupe de supervision */
	int longueur_n;             /* numero de neurones du groupe de supervision */
	int premier_n_test;         /* numero du premier neurone du groupe de test */
	int *neurones_sup;
	int *neurones_test;
	int *matrix_neurones;
	float *temp_neur_test;
} statistics;


void new_statistics_one(int Gpe)
{
	int n, link;
	int longueur_n_test;        /* numero de neurones du groupe de test */
	char resultat[256];
	char *string   = NULL;
	char    param_link[256];
	statistics *stats;

	stats = (statistics *) malloc(sizeof(statistics));


	/* initialisation de la structure */

	dprints("initialisation structure stat dans new_statistics groupe %d\n",Gpe);

	stats->iterations = 0;
	stats->success = 0;
	stats->false_positive = 0;
	stats->false_negative = 0;
	stats->seuil = SEUIL;

	dprints("initialisation structure stat stats->iterations = %d stats->success = %g, stats->seuil = %f\n",
			stats->iterations, stats->success, stats->seuil);

	/* Le groupe doit obligatoirement avoir deux entrees :
	 * typiquement, on compare le resultat obtenu par le reseau avec celui attendu pour chaque prototype. */

    /*if (prom_getopt(string, "-T", param_link) == 2 || prom_getopt(string, "-t", param_link) == 2)
      {
          mydata->theta2 = atof(param_link);printf("theta2 = %f\n", mydata->theta2);
      }*/

	n = -1;
	do
	{
		n++;
		link = find_input_link(Gpe, n);
		string = liaison[link].nom;

		if (link != -1)
		{
			/* entree de supervision donne le neurone correspondant au prototype */
			if (prom_getopt(string, "-S", param_link) == 1 || prom_getopt(string, "-s", param_link) == 1)
			{
				stats->gpe_supervision = liaison[link].depart;
				dprints("supervision input group : %d (f_statistic_one %s) \n",liaison[link].depart, def_groupe[Gpe].no_name);
			}
			/* entree de 'test' - resultat de l'apprentissage */
			if (prom_getopt(string, "-T", param_link) == 1 || prom_getopt(string, "-t", param_link) == 1)
			{
				stats->gpe_test = liaison[link].depart;
				dprints("test input group : %d (f_statistic_one %s) \n",liaison[link].depart, def_groupe[Gpe].no_name);
			}
			/* Pour specifier le seuil */
			if (prom_getopt(liaison[link].nom, "-L", resultat) == 2 || prom_getopt(liaison[link].nom, "-l", resultat) == 2)
			{
				stats->seuil = atof(resultat);
				dprints("stats->seuil = %g (f_statistic_one %s) \n", stats->seuil,def_groupe[Gpe].no_name );
			}
		}

	}
	while (link != -1);

	if (n != 1) EXIT_ON_ERROR ("Le groupe %s (%s) doit avoir deux liens en entree", def_groupe[Gpe].no_name, __FUNCTION__);

	stats->premier_n_sup = def_groupe[stats->gpe_supervision].premier_ele;
	stats->longueur_n = def_groupe[stats->gpe_supervision].nbre;
	stats->premier_n_test = def_groupe[stats->gpe_test].premier_ele;
	longueur_n_test = def_groupe[stats->gpe_test].nbre;

	/* verification que les donnees d'entree sont comparables */
	if (stats->longueur_n != longueur_n_test)
		EXIT_ON_ERROR ("the input groups in group %s (%s) must have the same number of neurons\n",def_groupe[Gpe].no_name, __FUNCTION__);


	stats->neurones_sup = (int *) malloc(TAILLE_HISTORIQUE * (stats->longueur_n) * sizeof(int));
	stats->neurones_test = (int *) malloc(TAILLE_HISTORIQUE * (stats->longueur_n) * sizeof(int));
	stats->matrix_neurones = (int *) malloc((stats->longueur_n) * (stats->longueur_n) * sizeof(int));
	stats->temp_neur_test = (float *) malloc(stats->longueur_n * sizeof(float));
	def_groupe[Gpe].data = stats;

}

/********************************** fin new_statistics **********************************/

/****************************************************************************************/
void function_statistics_one(int Gpe)
{
	int i, j, k, iterations, pos_temp, reponse_type = 0;
	statistics *my_stats = NULL;
	float temp = 0., rate, false_negative, false_positive;
	/* donnees sur le groupe d'apprentissage */
	int h_correct = 0, h_false_neg = 0, h_false_pos = 0;

	/* getting data from current group structure */
	my_stats = (statistics *) def_groupe[Gpe].data;
	if (my_stats == NULL)
		EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name, __FUNCTION__);

	/*************** STATISTICS *********************/

	iterations = my_stats->iterations % TAILLE_HISTORIQUE;

	/* si seulement un neurone */
	if (my_stats->longueur_n == 1)
	{
		dprints
		("\nmy_stats->premier_n_test = %f \t my_stats->premier_n_sup = %f (in group %s %s)\n",
				neurone[my_stats->premier_n_test].s, neurone[my_stats->premier_n_sup].s, def_groupe[Gpe].no_name, __FUNCTION__);

		if (neurone[my_stats->premier_n_test].s >= my_stats->seuil
				&& neurone[my_stats->premier_n_sup].s >= 1.)
		{
			my_stats->success++;
			reponse_type = CORRECT;
		}
		else if (neurone[my_stats->premier_n_test].s <= my_stats->seuil
				&& neurone[my_stats->premier_n_sup].s <= 0.)
		{
			my_stats->success++;
			reponse_type = CORRECT;
		}
		else if (neurone[my_stats->premier_n_test].s >= my_stats->seuil
				&& neurone[my_stats->premier_n_sup].s <= 0.)
		{
			my_stats->false_positive++;
			reponse_type = FALSE_POSITIVE;
		}
		else if (neurone[my_stats->premier_n_test].s <= my_stats->seuil
				&& neurone[my_stats->premier_n_sup].s >= 1.)
		{
			my_stats->false_negative++;
			reponse_type = FALSE_NEGATIVE;
		}

		/* moving historic's array of answers and inserting last answer */
		my_stats->historique[iterations] = reponse_type;

		if ((my_stats->iterations != 0) && iterations == 0)
		{
			for (i = 0; i < TAILLE_HISTORIQUE; i++)
				switch (my_stats->historique[i])
				{
				case CORRECT:
					h_correct++;
					break;
				case FALSE_NEGATIVE:
					h_false_neg++;
					break;
				case FALSE_POSITIVE:
					h_false_pos++;
					break;
				default:
					break;
				}

			rate = (my_stats->success / my_stats->iterations) * TAILLE_HISTORIQUE;
			false_positive = (my_stats->false_positive / my_stats->iterations) * TAILLE_HISTORIQUE;
			false_negative = (my_stats->false_negative / my_stats->iterations) * TAILLE_HISTORIQUE;
			printf("\n----------*--***--*--------------\n");
			printf ("total iterations: %d \tcorrects: %.3g%%   \tfalse positives: %.3g%%   \tfalse negatives: %.3g%%\n",
					my_stats->iterations, rate, false_positive, false_negative);
			printf ("last 100 ---- \tcorrects: %d   \tfalse positives: %d   \tfalse negatives: %d\n",
					h_correct, h_false_pos, h_false_neg);
		}

		my_stats->iterations++;

	}
	/* si il y a plus qu'un neurone */
	else
	{
		if ((my_stats->iterations != 0) && (iterations == 0))
		{
			for (i = 0; i < TAILLE_HISTORIQUE * my_stats->longueur_n;i = i + my_stats->longueur_n)
				for (j = 0; j < my_stats->longueur_n; j++)  /* ici j est l'aixe de supervision */
					if (my_stats->neurones_sup[j + i] == 1)
						for (k = 0; k < my_stats->longueur_n; k++)  /* ici k est l'aixe de test */
							if (my_stats->neurones_test[k + i] == 1)
								my_stats->matrix_neurones[j +(k *my_stats->longueur_n)]++;

			printf("---------------------------------------------------------------\n");
			printf("\t\tsupervision \n\t");

			for (i = 0; i < my_stats->longueur_n; i++)
				printf("\t%d", i);
			printf("\n");

			for (k = 0; k < (my_stats->longueur_n * my_stats->longueur_n); k = k + my_stats->longueur_n)    /* ici k est l'aixe de test */
			{
				if (k / my_stats->longueur_n == ((my_stats->longueur_n * my_stats->longueur_n) /(2 * my_stats->longueur_n)))
					printf("test");
				printf("\t%d", (k / my_stats->longueur_n));
				for (j = 0; j < my_stats->longueur_n; j++)  /* ici j est l'aixe de supervision */
					printf("\t%d", my_stats->matrix_neurones[j + k]);
				printf("\n");
			}

			for (i = 0; (i < my_stats->longueur_n * my_stats->longueur_n);i++)
				my_stats->matrix_neurones[i] = 0;
		}


		for (i = 0; i < my_stats->longueur_n; i++)
		{
			my_stats->neurones_sup[i + (iterations * my_stats->longueur_n)]
			                       = neurone[my_stats->premier_n_sup + i].s2;
			my_stats->neurones_test[i + (iterations * my_stats->longueur_n)] =
					0;
			my_stats->temp_neur_test[i] =
					neurone[my_stats->premier_n_test + i].s2;
		}

		for (i = 0, pos_temp = 1; i < my_stats->longueur_n; i++)
			if (my_stats->temp_neur_test[i] >
		my_stats->temp_neur_test[pos_temp])
			{
				temp = my_stats->temp_neur_test[i];
				pos_temp = i;
			}
			else
				temp = my_stats->temp_neur_test[pos_temp];

		my_stats->neurones_test[pos_temp +
		                        (iterations * my_stats->longueur_n)] = 1.;

		my_stats->iterations++;
	}

	/******************** END OF STATISTICS *********************/

}
