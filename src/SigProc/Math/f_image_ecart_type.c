/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSig_Prog
\defgroup f_image_ecart_type f_image_ecart_type


\section Author
- author: D. Bailly
- description: specific file creation
- date: 30/08/2010

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description
Calculate the mathematical standard deviation (ecart type) on the pixels of the
* image of the input group.



\section Macro
-none
\section Local variables
-none


\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <libx.h>

typedef struct data_image_ecart_type
{
  int index_gpe_ext;
} Data_image_ecart_type;



void new_image_ecart_type(int index_of_group)
{
  Data_image_ecart_type *data;
  int l;



    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);


  l = find_input_link(index_of_group, 0);
  if(l==-1) EXIT_ON_ERROR("no input link\n");

  data = (Data_image_ecart_type*)ALLOCATION(Data_image_ecart_type);
  data->index_gpe_ext = liaison[l].depart;
  def_groupe[index_of_group].data = data;

  /* a little message to help find bugs */
  if(find_input_link(index_of_group, 1) != -1) PRINT_WARNING("there is an unused input link\n");


  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

}


void function_image_ecart_type(int index_of_group)
{
  Data_image_ecart_type *data;
  int i, nbr, premier_ele;
  float moyenne, variance;
  prom_images_struct *input_ext=NULL;
  unsigned char *input_image=NULL;
  type_coeff *synapse=NULL;



    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);


  data = (Data_image_ecart_type *)def_groupe[index_of_group].data;
  input_ext = def_groupe[data->index_gpe_ext].ext;
  if(input_ext == NULL) EXIT_ON_ERROR("no image on the input group (.ext == NULL)\n");
  input_image = input_ext->images_table[0];
  nbr = input_ext->sx*input_ext->sx;
  premier_ele = def_groupe[index_of_group].premier_ele;

  moyenne = 0.0;
  for(i=0; i<nbr; i++)
  {
      moyenne += input_image[i];
  }
  moyenne /= (float)nbr;

  variance = 0.0;
  for(i=0; i<nbr; i++)
  {
    variance += (input_image[i] - moyenne) * (input_image[i] - moyenne);
  }
  variance /= (float)nbr;

  neurone[premier_ele].s = sqrt(variance);


  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

  (void) synapse;
}


void destroy_image_ecart_type(int index_of_group)
{


  dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

  if(def_groupe[index_of_group].data != NULL)
    free(def_groupe[index_of_group].data);


  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

}

