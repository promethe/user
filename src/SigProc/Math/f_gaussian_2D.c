/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_gaussian2D.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: Nils Beaussé
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:  Renvois la valeur d'une gaussienne 2D d'un point de vue algo, les parametres X Y et sigma sont reglables. Utile pour simulation 2D, permet de ne pas passer par la creation de masque de gaussienne (lourde).

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/

typedef struct my_data {
  double sigma;
  type_neurone* X;
  type_neurone* Y;
  type_neurone* POSX;
  type_neurone* POSY;
} my_data;

void new_gaussian2D(int numero)
{
  int Gpe_X = -1, Gpe_Y = -1, i = 0, Gpe_POSX = -1, Gpe_POSY = -1;
  float sigma = -1;
  int l = -1;
  char param[16];
  my_data* hop = NULL;

  if (def_groupe[numero].data == NULL)
  {
    printf("l=%d\n", l);
    if (l == -1)
    {
      printf("entree boucle\n");
      l = find_input_link(numero, 0);
      printf("l init boucle=%d\n", l);
      while (l != -1)
      {
        if (prom_getopt(liaison[l].nom, "-X", param) > 0)
        {
          Gpe_X = liaison[l].depart;
        }
        if (prom_getopt(liaison[l].nom, "-Y", param) > 0)
        {
          Gpe_Y = liaison[l].depart;
        }
        if (prom_getopt(liaison[l].nom, "-POSX", param) > 0)
        {
          Gpe_POSX = liaison[l].depart;
        }
        if (prom_getopt(liaison[l].nom, "-POSY", param) > 0)
        {
          Gpe_POSY = liaison[l].depart;
        }

        printf("nom de la liaison=%s\n", liaison[l].nom);
        if (prom_getopt(liaison[l].nom, "sigma", param) > 0)
        {
          printf("detection sigma, param renvoyé=%s\n", param);
          sigma = atof(param);
          printf("Sigma=%f", sigma);
        }
        i++;
        l = find_input_link(numero, i);
      }
    }

    if (Gpe_X == -1)
    {
      EXIT_ON_ERROR("groupe  X non trouve dans groupe %d\n", numero);
    }
    if (Gpe_Y == -1)
    {
      PRINT_WARNING("groupe  Y non trouve dans groupe, switch to 1D mode %d\n", numero);
    }
    if (Gpe_POSX == -1)
    {
      EXIT_ON_ERROR("groupe  POS X non trouve dans groupe %d\n", numero);
    }
    if (Gpe_POSY == -1)
    {
      PRINT_WARNING("groupe  POS Y non trouve dans groupe, switch to 1D mode %d\n", numero);
    }
    if (sigma < 0.0)
    {
      EXIT_ON_ERROR("sigma non trouve dans groupe %d\n", numero);
    }

    def_groupe[numero].data = malloc(sizeof(my_data));
    hop = (my_data*) def_groupe[numero].data;
    hop->sigma = sigma;

    hop->X = &(neurone[def_groupe[Gpe_X].premier_ele]);

    if (Gpe_Y == -1) hop->Y = NULL;
    else hop->Y = &(neurone[def_groupe[Gpe_Y].premier_ele]);

    hop->POSX = &(neurone[def_groupe[Gpe_POSX].premier_ele]);

    if (Gpe_POSY == -1) hop->POSY = NULL;
    else hop->POSY = &(neurone[def_groupe[Gpe_POSY].premier_ele]);
  }
}

void function_gaussian2D(int numero)
{
  float POSY, Y;
  my_data* hop = (my_data*) def_groupe[numero].data;

  if (hop->POSY != NULL && hop->POSY != NULL)
  {
    POSY = hop->POSY->s1;
    Y = hop->Y->s1;
  }
  else
  {
    POSY = 0;
    Y = 0;
  }
  neurone[def_groupe[numero].premier_ele].s1 = (float) exp(-(pow(hop->X->s1 - hop->POSX->s1, 2.0) + pow(Y - POSY, 2.0)) / (float) (2 * hop->sigma * hop->sigma));

}

void destroy_gaussian2D(int numero)
{
  free(((my_data*) def_groupe[numero].data));
  def_groupe[numero].data = NULL;
}
