/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

ingroup libSigProc
defgroup f_derivation_t f_derivation_t

\brief To be completed

\author N Cuperlier
\date 01/09/2004

\section Description
Fonction VB (Sorin a l'origine): j'en ai besoin pour la planification.
 je veux deriver le signal pour detecter la nouveaute.
 Par derivation, on entend ici que si le signal en entree a varie, alors c'est
 le nouveau signal qui apparait. Si le signal n'a pas change, alors tous les
 neurones ont une activite nulle.
 Remarque: Le signal est calcule a partir de s2 !
*/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/trouver_entree.h>
#define DEBUG
void function_derivation_t(int gpe_sortie)
{
    int i, gpe_planif = -1;
    int DebutGpeEC_v2 = def_groupe[gpe_sortie].premier_ele;
    int TailleYEC_v2 = def_groupe[gpe_sortie].tailley;
    float Delta;
    type_coeff *CoeffTemp = NULL;

    /*ajout pour supprimer le dervitateur si on est en planification */
    gpe_planif = trouver_entree(gpe_sortie, (char*)"plan");

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        def_groupe[gpe_sortie].ext = (void *) malloc(1);
        for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleYEC_v2; i++)
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    }
    if (gpe_planif == -1
        || isequal(neurone[def_groupe[gpe_planif].premier_ele].s2, 0))
    {
        /* calcul dans .s2 des entrees */
        for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleYEC_v2; i++)
        {
            Delta = 0.0;
            CoeffTemp = neurone[i].coeff;
            while (CoeffTemp != NULL)
            {
                Delta += CoeffTemp->val * neurone[CoeffTemp->entree].s2;
                CoeffTemp = CoeffTemp->s;
            }
            neurone[i].s = Delta;
        }


        for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleYEC_v2; i++)
        {

            Delta = neurone[i].s - neurone[i].d;


            /*  printf("                                                f_derivation: ap:%f ,av:%f, delta: %f\n",neurone[i].s,neurone[i].s2,Delta);
             */
            if (Delta > 0.)
                Delta = 1.0;
            if (Delta < 0.0)
                Delta = 0.0;
            neurone[i].d = neurone[i].s;
            if (Delta > 0)
                neurone[i].s1 = neurone[i].s2 = Delta;
            else
                neurone[i].s1 = neurone[i].s2 = 0;
            /*                    
               printf("                                           f_derivation: .s:%f ,.s2:%f, delta: %f\n",neurone[i].s,neurone[i].s2,Delta);
             */
        }
    }
    else
    {
        printf("	f_derivation: planif=========\n");
        for (i = DebutGpeEC_v2; i < DebutGpeEC_v2 + TailleYEC_v2; i++)
        {
            Delta = 0.0;
            CoeffTemp = neurone[i].coeff;
            while (CoeffTemp != NULL)
            {
                Delta += CoeffTemp->val * neurone[CoeffTemp->entree].s2;
                CoeffTemp = CoeffTemp->s;
            }
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = Delta;
        }
    }
}
