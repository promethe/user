/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
ingroup libSigProc
defgroup f_moyenne f_moyenne
\brief Computes and store a mean value of input on the val field of the link.
    the window for the mean is the temps field of the link.

Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

\file

************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_moyenne
{
    int gpe_transition;
    int gpe_vigilence;
    int gpe_reset_value;
} MyData_f_moyenne;
void function_moyenne(int numero)
{
    float vigilence_local = 1.;

    int neur_win = -1, i, l, reset = 0;

    int first_neuron = def_groupe[numero].premier_ele;

    float tau = 0., val = -1., var = 0., reset_value = 0.;

    type_coeff *synapse, *synX, *synE;

    MyData_f_moyenne *my_data;
    int gpe_transition = -1, gpe_vigilence = -1, gpe_reset_value = -1;

    if (def_groupe[numero].data == NULL)
    {
        i = 0;
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "transition_detect") == 0
                || strcmp(liaison[l].nom, "reset") == 0)
            {
                gpe_transition = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "vigilence") == 0)
            {
                gpe_vigilence = liaison[l].depart;
            }

            if (strcmp(liaison[l].nom, "reset_value") == 0)
            {
                gpe_reset_value = liaison[l].depart;
            }

            i++;
            l = find_input_link(numero, i);
        }
        my_data = (MyData_f_moyenne *) malloc(sizeof(MyData_f_moyenne));
        if (my_data == NULL)
        {
            printf("error malloc %s\n", __FUNCTION__);
            exit(0);
        }
        my_data->gpe_transition = gpe_transition;
        my_data->gpe_vigilence = gpe_vigilence;
        my_data->gpe_reset_value = gpe_reset_value;
        def_groupe[numero].data = (MyData_f_moyenne *) my_data;
    }
    else
    {
        my_data = (MyData_f_moyenne *) def_groupe[numero].data;
        gpe_transition = my_data->gpe_transition;
        gpe_vigilence = my_data->gpe_vigilence;
        gpe_reset_value = my_data->gpe_reset_value;
    }

    if (gpe_transition != -1)
    {
        if (neurone[def_groupe[gpe_transition].premier_ele].s1 > 0.5)
        {
            reset = 1;
            if (gpe_reset_value != -1)
                reset_value =
                    neurone[def_groupe[gpe_reset_value].premier_ele].s1;

        }
    }

    for (i = 0; i < def_groupe[numero].nbre; i++)
    {
        val = -1.;
        synX = NULL;
        synapse = neurone[first_neuron + i].coeff;

        /*Pour chaque neurone, on cherche dans le groupe d'etat (modifible) le neurone que l'on moyenne. Generalament, il n'y en a qu'un mais sinon c'est le max positif qui est choisis */
        while (synapse != NULL)
        {
            if (synapse->evolution == 1)
            {
                if (neurone[synapse->entree].s1 > val
                    && neurone[synapse->entree].s1 > 0.)
                {
                    val = neurone[synapse->entree].s1;
                    synX = synapse;
                    neur_win = i;
                }
            }
            synapse = synapse->s;
        }
        if (synX != NULL)
        {
            /*Puis on cherche le neurone dont on veut la valeur moyenne pour l'etat actif: il ne doit y en avoir qu'une */
            synapse = neurone[first_neuron + neur_win].coeff;
            synE = NULL;
            while (synapse != NULL)
            {
                if (synapse->evolution == 0)
                {
                    if (synE == NULL)
                        synE = synapse;
                    else
                    {
                        printf
                            ("%s %d: plusieurs neurones peut etre moyenn� condition impossible\n",
                             __FUNCTION__, numero);
                        exit(0);
                    }
                }
                synapse = synapse->s;
            }



            if (gpe_vigilence != -1)
            {
                /*Dec 2005 CG : Apprentissage neuro-modul�par le terme de vigilence exclusivement: cela evite de faire un strcmp sur le type de la neuromodulation */
                vigilence_local =
                    neurone[def_groupe[gpe_vigilence].premier_ele].s1;
                printf("vigilence_local_used\n");

            }

            if (synE != NULL)
            {
                if (reset == 1)
                {
                    synX->val = reset_value;
                }
                if (vigilence_local > 0.5)
                {
                    tau = liaison[synX->gpe_liaison].temps;
                    var = neurone[synE->entree].s2 * synE->val;
                    synX->val = (tau * synX->val + var) / (tau + 1);

                }
                neurone[first_neuron + neur_win].s2 =
                    neurone[first_neuron + neur_win].s =
                    neurone[first_neuron + neur_win].s1 = synX->val;
            }
            else
            {
                printf
                    ("%s %d: aucun neurone peut etre moyenn� condition impossible\n",
                     __FUNCTION__, numero);
                exit(0);
            }

        }

    }


}
