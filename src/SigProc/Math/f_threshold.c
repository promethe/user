/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_threshold f_threshold
 \ingroup libSigProc

 \brief applique un seuil a la valeur en entree

 \details

 \section Description

 Fonction seuil S: Sortie, I : Entree, T : Seuil
 S = f(I) => S = I si I >= T
 S = 0 si I < T

 Author: Ali Karaouzene
 Created: 29/01/2013

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct Mydata_threshold {
  int gpeInput;
  int mode_seuil;
  float borne_min;
  float borne_max;
  int infini_max;
  int infini_min;

} Mydata_threshold;

void new_threshold(int Gpe)
{
  int n, link;
  Mydata_threshold *mydata;
  char *string = NULL;
  float borne_moins = 0.0;
  float borne_plus = 0.0;
  int borne_donnee_min = 0;
  int borne_donnee_max = 0;
  int mode_seuil = 0;
  int infini_min = 0;
  int infini_max = 0;
  int plop;
  float param;

  mydata = (Mydata_threshold *) malloc(sizeof(Mydata_threshold));
  if (mydata == NULL)
  EXIT_ON_ERROR("Erreur d'allocation de mydata %s", __FUNCTION__);

  mydata->gpeInput = -1;

  n = 0;
  link = find_input_link(Gpe, n);
  while (link != -1)
  {
    string = liaison[link].nom;
    if (strncmp(string, "sync", 4) != 0)
    {
      mydata->gpeInput = liaison[link].depart;
    }
    plop = prom_getopt_float(liaison[link].nom, "bmin", &param);
    if (plop > 0)
    {
      if (plop == 2) borne_moins = param;
      else infini_min = 1;
      borne_donnee_min = 1;

    }
    plop = prom_getopt_float(liaison[link].nom, "bmax", &param);

    if (plop > 0)
    {
      if (plop == 2) borne_plus = param;
      else infini_max = 1;
      borne_donnee_max = 1;
    }



    n++;
    link = find_input_link(Gpe, n);
  }
  if (mydata->gpeInput == -1)
  EXIT_ON_ERROR("Il faut un groupe et uniquement un seul (hors lien sync) en entree %s (%s)", def_groupe[Gpe].no_name, __FUNCTION__);
  if (def_groupe[mydata->gpeInput].nbre != def_groupe[Gpe].nbre)
  EXIT_ON_ERROR("Groups must be same length in %s ( %s ) ", def_groupe[Gpe].no_name, __FUNCTION__);
  if (borne_donnee_min == 1 && borne_donnee_max == 1)
  {
    mode_seuil = 1;
  }
  else if (borne_donnee_min == 1 || borne_donnee_max == 1)
  {
    EXIT_ON_ERROR("bmin et bmax doivent etre définis ensemble ou pas du tout (dans ce cas c'est seuil qui sera pris comme valeur min) (bmin ou bmax peuvent etre definis SANS valeur, dans ce cas cela designe une borne infinie)\n");
  }

  mydata->mode_seuil = mode_seuil;
  mydata->infini_max = infini_max;
  mydata->infini_min = infini_min;
  mydata->borne_max = borne_plus;
  mydata->borne_min = borne_moins;
  def_groupe[Gpe].data = mydata;

  dprints("END %s\n", __FUNCTION__);
}

void function_threshold(int Gpe)
{
  int deb, nbre, debInput, i, gpeInput, infini_max, infini_min;
  float T;
  float borne_min;
  float borne_max;
  Mydata_threshold *mydata = NULL;
  int mode_seuil;

  mydata = (Mydata_threshold *) def_groupe[Gpe].data;
  if (mydata == NULL)
  EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name, __FUNCTION__);

  mode_seuil = mydata->mode_seuil;
  gpeInput = mydata->gpeInput;
  borne_min = mydata->borne_min;
  borne_max = mydata->borne_max;
  infini_max = mydata->infini_max;
  infini_min = mydata->infini_min;
  debInput = def_groupe[gpeInput].premier_ele;
  deb = def_groupe[Gpe].premier_ele;
  nbre = def_groupe[Gpe].nbre;
  T = def_groupe[Gpe].seuil;
  if (mode_seuil == 0) // conservé pour retrocompatibilité
  {
    for (i = 0; i < nbre; i++)
    {
      if (neurone[debInput + i].s1 < T) neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0;
      else neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = neurone[debInput + i].s1;
    }
  }
  else // nouveau mode plus complet
  {
    for (i = 0; i < nbre; i++)
    {
      if (neurone[debInput + i].s1 < borne_min && infini_min == 0) neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = borne_min;
      else if (neurone[debInput + i].s1 > borne_max && infini_max == 0) neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = borne_max;
      else
      {
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = neurone[debInput + i].s1;
      }
    }

  }

}

void destroy_threshold(int Gpe)
{
  free(def_groupe[Gpe].data);
}
