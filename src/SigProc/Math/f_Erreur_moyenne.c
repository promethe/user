/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 ingroup libSigProc
 defgroup f_Erreur_moyenne f_Erreur_moyenne

 \brief To be completed


 \section Description
 computes and store a mean value of input on the val field of the link the window for the mean is the temps field of the link.

 \deprecated NOT DEFINED IN PROMETHE !!

 \file
 ingroup f_Erreur_moyenne

 */
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*
 #define DEBUG*/
/*
 #define SAVE*/

void function_erreur_moyenne_antoine(int numero)
{

  int mode = 1, l = -1, Gpe_1 = -1;

  int first_neuron = def_groupe[numero].premier_ele;

  float tau = 0., val = -1, var = 0.;

  type_coeff *synapse, *synX, *synE;

  int i = 0, nsyn = -1;

  printf("Erreur moyenne de %d\n", numero);
#ifdef DEBUG
  printf("first = %d , last=%d  ...\n", first_neuron, last_neuron);
#endif

  if (def_groupe[numero].data == NULL)
  {

    def_groupe[numero].data = malloc(sizeof(int));
    if (l == -1)
    {

      l = find_input_link(numero, i);
      /*
       printf("liaison %s\n",liaison[l].nom);
       */
      while (l != -1)
      {

        if (strcmp(liaison[l].nom, "-2") == 0)
        {
          mode = 2;

          def_groupe[numero].ext = malloc(sizeof(int));

          Gpe_1 = *(int *) def_groupe[numero].ext = liaison[l].depart;

          break;
        }

        i++;
        l = find_input_link(numero, i);
        if (l != -1) printf("liaison %s\n", liaison[l].nom);

      }
    }

    def_groupe[numero].data = malloc(sizeof(int));
    *(int *) def_groupe[numero].data = mode;

  }
  else
  {
    if (def_groupe[numero].ext != NULL) Gpe_1 = *(int *) def_groupe[numero].ext;

    mode = *(int *) def_groupe[numero].data;
  }

  if ( /*neurone[first_neuron].d==0. */1)
  {

    synX = NULL;

    synE = NULL;

    synapse = neurone[first_neuron].coeff;

    i = 0;

    if (synapse == NULL)
    {

      printf("Erreur synapse NUL \n");
      exit(0);
    }
    while (synapse != NULL)
    {

      if (synapse->evolution == 0)
      {

        synE = synapse;
      }
      synapse = synapse->s;
    }

    synapse = neurone[first_neuron].coeff;

    while (synapse != NULL)
    {

      if (synapse->evolution == 1)
      {

        if (neurone[synapse->entree].s2 > val && neurone[synapse->entree].s2 > 0.)
        {

          val = neurone[synapse->entree].s2;

          synX = synapse;

          if (isdiff(def_groupe[numero].seuil, -1.)) neurone[synX->entree].s1 += 1.0; /*compte le nbre d'experiences* */
          nsyn = i;

        }
      }
      synapse = synapse->s;
      i++;
    }
#ifdef DEBUG
    printf("nsyn=%d  \n", nsyn);
#endif
    synapse = neurone[first_neuron].coeff;
    i = 0;
    /*#ifdef SAVE
     while (synapse != NULL){

     if(synapse->evolution==1 && i!=nsyn){


     if(Gpe_1!=-1||def_groupe[numero].seuil==-1){

     memset(file,0,50);

     memset(line,0,1000);

     memset(num,0,4);

     memset(buf,0,1000);

     strcpy(file,"erreur");

     sprintf(num,"%d",i);

     strcat(file,num);

     fd=fopen(file,"a");

     fr=fopen(file,"r");

     fgets(line,1024,fr);

     if(strlen(line)>4){

     while(strlen(line)>4 &&!feof(fr)) {

     memset(buf,0,1000);

     strncpy(buf,line,strlen(line));

     memset(line,0,1000);

     fgets(line,1024,fr);

     };

     #ifdef DEBUG
     printf(" line=%d buf=%d buf : %s \n",strlen(line),strlen(buf),buf);
     #endif
     fprintf(fd,"%s",buf);

     memset(buf,0,1000);

     }else{

     if(def_groupe[numero].seuil!=-1.)
     fprintf(fd,"0. 0. 0. 0. 0. \n ");
     else{


     fprintf(fd,"0. 0. 0. 0. \n");

     }

     }
     fclose(fr);

     fclose(fd);
     }

     }
     i++;

     synapse=synapse->s;
     }

     #endif		*/
    if (synX != NULL)
    {

      tau = liaison[synX->gpe_liaison].temps;

      if (isequal(def_groupe[numero].seuil, -1.))
      {

        var = neurone[synE->entree].s2 - neurone[synX->entree].s1 >= 0. ? neurone[synE->entree].s2 - neurone[synX->entree].s1 : 0. - (neurone[synE->entree].s2 - neurone[synX->entree].s1);

        synX->val = (tau * synX->val + var) / (tau + 1);

        printf("Deriv_erreur instantanee : %f, ancienne %f, erreur %f\n", synX->val, neurone[synX->entree].s1, neurone[synE->entree].s2);

        neurone[synX->entree].s1 = neurone[synE->entree].s2;

        neurone[first_neuron].s2 = neurone[first_neuron].s = neurone[first_neuron].s1 = synX->val;
      }
      else
      {
        if (neurone[synX->entree].s1 >= def_groupe[numero].seuil)
        {

          if (neurone[synX->entree].s1 - def_groupe[numero].seuil < tau)
          {

            tau = neurone[synX->entree].s1;

            synX->val = (neurone[synE->entree].s2 + tau * synX->val) / (tau + 1);

            neurone[first_neuron].s2 = neurone[first_neuron].s = neurone[first_neuron].s1 = 0.;

          }
          else
          {
            synX->val = (neurone[synE->entree].s2 + tau * synX->val) / (tau + 1);

            neurone[first_neuron].s2 = neurone[first_neuron].s = neurone[first_neuron].s1 = synX->val;

          }

        }
      }

      printf("val[%d] Erreur : val %f    \n", nsyn, synX->val);

    }
    else
    {

      if (isequal(def_groupe[numero].seuil, -1.))
      {

        printf("transition non apprise  \n");

      }

    }
    neurone[first_neuron].d = 1.;

  }
  else
  {
    neurone[first_neuron].d = 0.;
  }
}
