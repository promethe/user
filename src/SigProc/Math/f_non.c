/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/******************************
\ingroup libSigProc
\defgroup f_non.c f_non.c
 *
 *
\date Jan 09, 2012
\author Ali Karaouzene
\brief

Cette fonction fait un non logique. Les neurones en entrée supérieur au seuil sont mis à zero le reste à un.

\section description
 ***************************************************************************************************************************/

/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>



typedef struct Mydata_non
{
	int gpeInput;
} Mydata_non;


void new_non(int Gpe)
{
	int 	n, link/*,i,j*/;
	Mydata_non *mydata;
	/*char 	*string   = NULL;*/

	mydata = (Mydata_non *) malloc(sizeof(Mydata_non));
	if (mydata== NULL)
		EXIT_ON_ERROR("Erreur d'allocation de mydata %s",__FUNCTION__);

	mydata->gpeInput = -1;


	n = 0;
	link = find_input_link(Gpe, n);
	while (link != -1)
	{
		/*string = liaison[link].nom;*/
		if (strncmp(liaison[link].nom, "sync", 4) != 0)
		{
			mydata->gpeInput = liaison[link].depart;
		}

		n++;
		link = find_input_link(Gpe, n);
	}
	if (mydata->gpeInput == -1 || n > 1)
		EXIT_ON_ERROR ("Il faut un groupe et uniquement un seul (hors lien sync) en entree %s (%s)",def_groupe[Gpe].no_name, __FUNCTION__);
	if (def_groupe[mydata->gpeInput].nbre != def_groupe[Gpe].nbre)
		EXIT_ON_ERROR ("Groups must be same length in %s ( %s ) ", def_groupe[Gpe].no_name, __FUNCTION__);
	def_groupe[Gpe].data=mydata;
	dprints("END %s\n",__FUNCTION__);
}



void function_non(int Gpe)
{
	int i/*,j*/,deb,nbre;
	Mydata_non *mydata = NULL;
	int gpeInput,debInput;

	mydata = (Mydata_non *) def_groupe[Gpe].data;
	if (mydata == NULL)
		EXIT_ON_ERROR("error while loading data in group %s (%s)\n", def_groupe[Gpe].no_name,__FUNCTION__);

	gpeInput = mydata->gpeInput;
	debInput = def_groupe[gpeInput].premier_ele;
	deb =  def_groupe[Gpe].premier_ele;
	nbre =  def_groupe[Gpe].nbre;
	for (i = 0; i < nbre; i++)
	{
		if (neurone[debInput+i].s1 >  def_groupe[Gpe].seuil )
			neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = 0;
		else
			neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = 1;
	}

}

void destroy_non(int Gpe)
{
	free(def_groupe[Gpe].data);
}
