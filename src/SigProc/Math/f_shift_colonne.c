/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

#undef DEBUG

/*shift des colonnes d'une matrice comme le fait la fonction f_shift pour des vecteurs*/

typedef struct TAG_STRUCT_SHIFT_COLONNE
{
    int deb_gpe_cpy;
    int deb_gpe_shift;
    int deb_gpe;
    int longueur;
    int longueurx_gpe_cpy;
    int longueury_gpe_cpy;
    int longueur_gpe_shift;
} struct_shift_colonne;


void function_shift_colonne(int Gpe)
{
    int deb_gpe = -1;
    int deb_gpe_shift, deb_gpe_cpy = -1;
    int longueur, longueurx_gpe_cpy,longueury_gpe_cpy, longueur_gpe_shift;
    float max;
    int pos_max, shift;
    struct_shift_colonne *this_data;
    int i, j;
    int old_posx, new_posx;

    if (def_groupe[Gpe].data == NULL)
    {
        int lien;
        if ((lien = find_input_link(Gpe, 0)) == -1)
        {
            printf
                ("Erreur de lien entrant dans function_shift_colonne : gpe %d\n",
                 Gpe);
            exit(1);
        }

        if (strstr(liaison[lien].nom, "shift") != NULL)
        {
            deb_gpe_shift = def_groupe[liaison[lien].depart].premier_ele;
            longueur_gpe_shift = def_groupe[liaison[lien].depart].nbre;
            if ((lien = find_input_link(Gpe, 1)) == -1)
            {
                printf
                    ("Erreur de lien entrant dans function_shift_colonne : gpe %d\n",
                     Gpe);
                exit(1);
            }
            deb_gpe_cpy = def_groupe[liaison[lien].depart].premier_ele;
            longueury_gpe_cpy = def_groupe[liaison[lien].depart].tailley;
	    longueurx_gpe_cpy = def_groupe[liaison[lien].depart].taillex;

        }
        else
        {
            deb_gpe_cpy = def_groupe[liaison[lien].depart].premier_ele;
            longueury_gpe_cpy = def_groupe[liaison[lien].depart].tailley;
	    longueurx_gpe_cpy = def_groupe[liaison[lien].depart].taillex;
            if ((lien = find_input_link(Gpe, 1)) == -1)
            {
                printf
                    ("Erreur de lien entrant dans function_shift_colonne : gpe %d\n",
                     Gpe);
                exit(1);
            }
            deb_gpe_shift = def_groupe[liaison[lien].depart].premier_ele;
            longueur_gpe_shift = def_groupe[liaison[lien].depart].nbre;

        }


        deb_gpe = def_groupe[Gpe].premier_ele;
        longueur = def_groupe[Gpe].nbre;

        if(longueur != (longueurx_gpe_cpy*longueury_gpe_cpy))
           {
           printf("Longueur mismatch in function_shift_colonne gpe %d\n",Gpe);
           exit(1);
           }

        /*if (longueur != (longueur_gpe_cpy + 2 * (longueur_gpe_shift / 2)))
        {
            printf("Longueur mismatch in function_shift_bords gpe %d\n", Gpe);
            exit(1);
        }*/

        this_data = malloc(sizeof(struct_shift_colonne));
        if (this_data == NULL)
        {
            printf("Erreur allocation memoire dans function_shift_colonne gpe %d\n",
                   Gpe);
            exit(1);
        }

        def_groupe[Gpe].data = (void *) this_data;

        this_data->deb_gpe = deb_gpe;
        this_data->deb_gpe_shift = deb_gpe_shift;
        this_data->deb_gpe_cpy = deb_gpe_cpy;
        this_data->longueur = longueur;
        this_data->longueur_gpe_shift = longueur_gpe_shift;
        this_data->longueurx_gpe_cpy = longueurx_gpe_cpy;
	this_data->longueury_gpe_cpy = longueury_gpe_cpy;
    }
    else
    {
        this_data = (struct_shift_colonne *) def_groupe[Gpe].data;
        deb_gpe = this_data->deb_gpe;
        deb_gpe_shift = this_data->deb_gpe_shift;
        deb_gpe_cpy = this_data->deb_gpe_cpy;
        longueur = this_data->longueur;
        longueurx_gpe_cpy = this_data->longueurx_gpe_cpy;
        longueury_gpe_cpy = this_data->longueury_gpe_cpy;
        longueur_gpe_shift = this_data->longueur_gpe_shift;
    }

    /*recherche de la position max sur shift */
    /*pos_max = deb_gpe_shift + longueur_gpe_shift / 2;*/
    pos_max = 0;
    max = 0.;
    for (i = deb_gpe_shift; i < deb_gpe_shift + longueur_gpe_shift; i++)
    {
        if (neurone[i].s1 > max)
        {
            max = neurone[i].s1;
            pos_max = i;
        }
    }
    pos_max = pos_max - deb_gpe_shift;
    /*printf("pos_max %d\n",pos_max); */
    /*valeur du shift */
    shift = longueur_gpe_shift / 2 - pos_max;
    printf(" shift %d\n",shift);
    /*decalage de la cpy */
    for (j = deb_gpe; j < deb_gpe + longueur; j++)
        neurone[j].s = neurone[j].s1 = neurone[j].s2 = 0;

    if (max <= 0.)
        shift = 0;

    for (i = 0; i < longueury_gpe_cpy; i++)
    {
        for (j = 0; j < longueurx_gpe_cpy; j++)
        {
            old_posx = j + deb_gpe_cpy;
            new_posx = j + deb_gpe + shift;
            if( (j+shift) >= 0 && (j+shift) < longueurx_gpe_cpy )
            {
                neurone[i*longueurx_gpe_cpy + new_posx].s2 = neurone[i*longueurx_gpe_cpy + old_posx].s2;
                neurone[i*longueurx_gpe_cpy + new_posx].s1 = neurone[i*longueurx_gpe_cpy + old_posx].s1;
                neurone[i*longueurx_gpe_cpy + new_posx].s = neurone[i*longueurx_gpe_cpy + old_posx].s;

            }
            /*printf(" i %d old_pos %d new_pos %d shift %d value %f\n",i,old_pos,new_pos,shift,neurone[new_pos].s1); */
        }
    }
}
