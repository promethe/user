/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup plugin_list
\defgroup libIO_Robot libIO_Robot
\brief Groups for controlling robots.
*/

#include <string.h>

#include <IO_Robot/Simulateur_2D.h>

#include <libx.h>

#include <libhardware.h>

#include <group_function_pointers.h>

#include <IO_Robot/Aibo.h>
#include <IO_Robot/Arm.h>
#include <IO_Robot/Expressive_Head.h>
#include <IO_Robot/Robot_Mvt.h>
#include <IO_Robot/Joystick.h>
#include <IO_Robot/Pan_Tilt.h>
#include <IO_Robot/Env_Simul.h>
#include <IO_Robot/Complex_Action.h>
#include <IO_Robot/SystemCall.h>
#include <IO_Robot/Serial.h>
#include <IO_Robot/Sensor.h>
#include <IO_Robot/pressure.h>
#include <IO_Robot/Mouse.h>
#include <IO_Robot/GenericTools.h>
#include <IO_Robot/libIO_Robot.h>






type_group_function_pointers group_function_pointers[]=
{
  /* AIBO */
  {"f_aibocam_to_ext", function_aibocam_to_ext, NULL, NULL, NULL, NULL, -1, -1},
  {"f_aibomicro_to_ext", function_aibomicro_to_ext, NULL, NULL, NULL, NULL, -1, -1},
  {"f_close_aibo_connection", function_close_aibo_connection, NULL, NULL, NULL, NULL, -1, -1},
  {"f_device_to_neurone", function_device_to_neurone, NULL, NULL, NULL, NULL, -1, -1},
  {"f_joint_to_neurone", function_device_to_neurone, NULL, NULL, NULL, NULL, -1, -1},
  {"f_send_command", function_send_command, NULL, NULL, NULL, NULL, -1,-1},
  {"f_sensor_to_neurone", function_sensor_to_neurone, NULL, NULL, NULL, NULL, -1, -1},
  {"f_smooth_command", function_smooth_command, NULL, NULL, NULL, NULL, -1, -1},
  {"f_update_aibo", function_update_aibo, NULL, NULL, NULL, NULL, -1, -1},
  
  /* Arm */
  {"f_arm", function_arm, new_arm, NULL, NULL, NULL, -1, -1},
  {"f_ressort", function_ressort, new_ressort, destroy_ressort, NULL, NULL, -1, -1},
  {"f_arm_simulation", function_arm_simulation, new_arm_simulation, NULL, NULL, NULL, -1, -1},
  {"f_arm_simu_katana", function_arm_simu_katana, new_arm_simu_katana, destroy_arm_simu_katana, NULL, NULL, -1, -1},
  {"f_control_vitesse_arm", function_control_vitesse_arm, new_control_vitesse_arm, NULL, NULL, NULL, -1, -1},
  {"f_control_vitesse_arm_by_moteur", function_control_vitesse_arm_by_moteur, new_control_vitesse_arm_by_moteur, NULL, NULL, NULL, -1, -1},
  {"f_get_arm_position", function_get_arm_position, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_get_value", function_get_value, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_object", function_object, new_object, NULL, NULL, NULL, -1, -1},
  {"f_phi_arm", function_phi_arm, NULL, NULL, NULL, NULL, -1,- 1},
  {"f_proprio_arm", function_proprio_arm, new_proprio_arm, NULL, NULL, NULL, -1, -1},
  {"f_sensor_arm", function_sensor_arm, new_sensor_arm, NULL, NULL, NULL, -1, -1},
  {"f_test_arm", function_test_arm, NULL, NULL, NULL, NULL, -1, -1},
  {"f_yuragi", function_yuragi, NULL, NULL, NULL, NULL, -1, -1},
  
  /* Complex Action */
  {"f_diffusion_angle", function_diffusion_angle, NULL, NULL, NULL, NULL, -1, -1},
  {"f_feeding_actions", function_feeding_actions, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_gestion_motivations", function_gestion_motivations, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_get_panorama", function_get_panorama, NULL, NULL, NULL, NULL, -1, -1},
  {"f_paste_image", function_paste_image, NULL, NULL, NULL, NULL, -1, -1},
  {"f_get_pano_alt", function_get_panorama_alternate, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_info_sortie_neurones", function_info_sortie_neurones, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_landmarks_sequential", function_landmarks_sequential, new_landmarks_sequential, NULL, NULL, NULL, -1, -1},
  {"f_load_sercom", function_load_sercom, NULL, NULL, NULL, NULL, -1, -1},
  {"f_panoramique", function_panoramique, NULL, NULL, NULL, NULL, -1, -1},
  {"f_reach_destination", function_reach_destination, new_reach_destination, destroy_reach_destination, NULL, NULL, -1, -1},
  {"f_renorm_vect", function_renorm_vect, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_robot_reflex_elem", function_robot_reflex_elem, NULL, NULL, NULL, NULL, -1, -1}, non utilisee */
  {"f_robot_apprend_lieu", function_robot_apprend_lieu, NULL, NULL, NULL, NULL, -1, -1},
  {"f_robot_limbic", function_robot_limbic, NULL, NULL, NULL, NULL, -1, -1},
  {"f_robot_manage_exploration", function_robot_manage_exploration, NULL, NULL, NULL, NULL, -1, -1},
  {"f_spice", function_spice, NULL, NULL, NULL, NULL, -1, -1},
  {"f_theta_adapt", function_theta_adapt, NULL, NULL, NULL, NULL, -1,- 1},
  {"f_theta_generate", function_theta_generate, NULL, NULL, NULL, NULL, -1, -1},
   
  /* Env Simul */
  {"f_drive_detected_simule", function_drive_detected_simule, NULL, NULL, NULL, NULL, -1, -1},
  {"f_envie_et_decouverte", function_envie_et_decouverte, NULL, NULL, NULL, NULL, -1, -1},
  {"f_env_init", function_env_init, NULL, NULL, NULL, NULL, -1, -1},
  {"f_env_init2", function_env_init2, NULL, NULL, NULL, NULL, -1, -1},
  {"f_explo", function_explo, NULL, NULL, NULL, NULL, -1, -1},
  {"f_explo_alea", function_explo_alea, NULL, NULL, NULL, NULL, -1, -1},

  /* Should be moved to NN_IO/Tools but need the global var discr
   *
   */
  {"f_extract_val_to_vector_circular",function_extract_val_to_vector_circular, NULL, NULL, NULL, NULL, -1, -1},
  {"f_extract_val_to_vector_non_circular", function_extract_val_to_vector_non_circular, NULL, NULL, NULL, NULL, -1, -1},
  {"f_extract_vector_to_val", function_extract_vector_to_val, new_extract_vector_to_val, NULL, NULL, NULL, -1, -1},
  {"f_extract_multi_vectors_to_vals", function_extract_multi_vectors_to_vals, NULL, NULL, NULL, NULL, -1, -1},
  {"f_extract_multi_vals_to_vectors", function_extract_multi_vals_to_vectors, NULL, NULL, NULL, NULL, -1, -1},
  {"f_extract_multi_vals_to_vectors_circular", function_extract_multi_vals_to_vectors_circular, NULL, NULL, NULL, NULL, -1, -1},
  {"f_extract_multi_vals_to_vectors_non_circular", function_extract_multi_vals_to_vectors_non_circular, NULL, NULL, NULL, NULL, -1, -1},

  {"f_fin_planif", function_fin_planif, NULL, NULL, NULL, NULL, -1, -1},
  {"f_fin_planif_real", function_fin_planif_real, NULL, NULL, NULL, NULL, -1, -1},
  {"f_fin_simul", function_fin_simul, NULL, NULL, NULL, NULL, -1, -1},
  {"f_fin_simul_trajrecord", function_fin_simul_trajrecord, NULL, NULL, NULL, NULL, -1, -1},
  {"f_mesure_planif", function_mesure_planif, NULL, NULL, NULL, NULL, -1, -1},
  {"f_monitor_planif", function_monitor_planif, new_monitor_planif, destroy_monitor_planif, save_monitor_planif, NULL, -1, -1},
  {"f_monitor_planif_real", function_monitor_planif, new_monitor_planif_real, destroy_monitor_planif, save_monitor_planif, NULL, -1, -1},
  {"f_move_robot", function_move_robot, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_move_robot_alea", function_move_robot_alea, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  /*{"f_move_robot_info", function_move_robot_info, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_nav_a_vue", function_nav_a_vue, NULL, NULL, NULL, NULL, -1, -1},
  {"f_pres_obstacle", function_pres_obstacle, NULL, NULL, NULL, NULL, -1, -1},
  {"f_print_infos", function_print_infos, NULL, NULL, NULL, NULL, -1, -1},
  {"f_print_one_place", function_print_one_place, new_print_one_place, fin_print_one_place, NULL, NULL, -1, -1},
  {"f_print_place", function_print_place, NULL, NULL, NULL, NULL, -1, -1},
  {"f_print_place_elegant", function_print_place_elegant, NULL, NULL, NULL, NULL, -1, -1},
  {"f_print_place_from_file", function_print_place_from_file, NULL, NULL, NULL, NULL, -1, -1},
  {"f_print_place_world", function_print_place_world, new_print_place_world, destroy_print_place_world, NULL, NULL, -1, -1},
  /*{"f_print_pos", function_print_pos, NULL, NULL, NULL, NULL, -1, -1}, initulisee */
  {"f_print_vect_print_info", function_print_vector_explo_from_print_info, NULL, NULL, NULL, NULL, -1, -1},
  {"f_rebond", function_rebond, NULL, NULL, NULL, NULL, -1, -1},
  {"f_reco_info", function_reco_info, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sequential_perception", function_sequential_perception, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sequential_perception_break", function_sequential_perception_break, NULL, NULL, NULL, NULL, -1, -1},
  {"f_show_env_and_robot", function_show_env_and_robot, NULL, NULL, NULL, NULL, -1, -1},
  {"f_simulation_lumiere", function_simulation_lumiere, NULL, NULL, NULL, NULL, -1, -1},
  {"f_simul_drive_expected", function_simul_drive_expected, NULL, NULL, NULL, NULL, -1, -1},
  {"f_simul_real", function_simul_real, NULL, NULL, NULL, NULL, -1, -1},
  {"f_simul_robot_perception", function_simul_robot_perception, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sonar_simule", function_sonar_simule, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sonar_simule2", function_sonar_simule2, NULL, NULL, NULL, NULL, -1, -1},
  {"f_stat_on_position_error", function_stat_on_position_error, new_stat_on_position_error, destroy_stat_on_position_error, NULL, NULL, -1, -1},
  /*{"f_timing_reward", function_timing_reward, new_timing_reward, destroy_timing_reward, NULL, -1, -1}, inutilisee */
  {"f_trajectory_XY_teacher", function_trajectory_XY_teacher, NULL, NULL, NULL, NULL, -1, -1},
  {"f_trajectory_XY_teacher_2", function_trajectory_XY_teacher_2, NULL, NULL, NULL, NULL, -1, -1},
  {"f_trajectory_XY_teacher_3", function_trajectory_XY_teacher_3, NULL, NULL, NULL, NULL, -1, -1},
  {"f_trajectory_XY_teacher_4", function_trajectory_XY_teacher_4, NULL, NULL, NULL, NULL, -1, -1},
  {"f_variables_essentielles", function_variables_essentielles, new_variables_essentielles, NULL, NULL, NULL, -1, -1},
  {"f_vigilance", function_vigilance, NULL, NULL, NULL, NULL, -1, -1},
  {"f_vigilance_up_down", function_vigilance_up_down, NULL, NULL, NULL, NULL, -1, -1},
  
        /* SystemCall */
  {"f_systemCall_exec", function_systemCall_exec, new_systemCall_exec, destroy_systemCall_exec, NULL, NULL, -1, -1},

  /* Expressive Head */
  {"f_discret_to_analog_expression", function_discret_to_analog_expression, new_discret_to_analog_expression, NULL, NULL, NULL, -1, -1},
  {"f_expressions", function_expressions, new_expressions, NULL, NULL, NULL, -1, -1},
  {"f_expression_to_servos", function_expression_to_servos, new_expression_to_servos, NULL, NULL, NULL, -1, -1},
  {"f_primitives_to_servos", function_primitives_to_servos, new_primitives_to_servos, NULL, NULL, NULL, -1, -1},
  {"f_servo_tete", function_servo_tete, new_servo_tete, NULL, NULL, NULL, -1, -1}, 
  {"f_test_expression", function_test_expression, NULL, NULL, NULL, NULL, -1, -1},
  
  /* Joystick */
  {"f_control_robot_by_joystick", function_control_robot_by_joystick, NULL, NULL, NULL, NULL, -1, -1},
  {"f_init_joystick", function_init_joystick, NULL, NULL, NULL, NULL, -1, -1},
  {"f_joystick", function_joystick, NULL, NULL, NULL, NULL, -1, -1},
  {"f_joystick_axe_to_neurons", function_joystick_axe_to_neurons, new_joystick_axe_to_neurons, destroy_joystick_axe_to_neurons, NULL, NULL, -1, -1},
  {"f_joystick_axe_to_single_neuron", function_joystick_axe_to_single_neuron, new_joystick_axe_to_single_neuron, destroy_joystick_axe_to_single_neuron, NULL, NULL, -1, -1}, 
  {"f_joystick_buttons_to_neurons", function_joystick_buttons_to_neurons, new_joystick_buttons_to_neurons, destroy_joystick_buttons_to_neurons, NULL, NULL, -1, -1},
  
  /*Mouse*/
#ifndef Darwin
  {"f_get_mouse_event", function_get_mouse_event, new_get_mouse_event, NULL, NULL, NULL, -1, -1},
 #endif
  {"f_mouse_buttons_to_neurons",function_mouse_buttons_to_neurons, new_mouse_buttons_to_neurons, destroy_mouse_buttons_to_neurons, NULL, NULL, -1, -1},
  {"f_mouse_axe_to_single_neuron", function_mouse_axe_to_single_neuron, new_mouse_axe_to_single_neuron, destroy_mouse_axe_to_single_neuron, NULL, NULL, -1, -1},

  /*pressure*/
  {"f_pressure_to_single_neuron", function_pressure_to_single_neuron, new_pressure_to_single_neuron, destroy_pressure_to_single_neuron, NULL, NULL, -1, -1},

  /* Pan Tilt */
  {"f_activ_joint", function_activ_joint, new_activ_joint, NULL, NULL, NULL, -1, -1},
  {"f_dphi_to_joint", function_dphi_to_joint, NULL, NULL, NULL, NULL, -1, -1},
  {"f_global_x", function_global_x, NULL, NULL, NULL, NULL, -1, -1},
  {"f_global_y", function_global_y, NULL, NULL, NULL, NULL, -1, -1},
  {"f_joint", function_joint, new_joint, destroy_joint, NULL, NULL, -1, -1},
  {"f_joint_get_proprio", function_joint_get_proprio, NULL, NULL, NULL, NULL, -1, -1},
  {"f_joint_population", function_joint_population, NULL, NULL, NULL, NULL, -1, -1},
  {"f_merge", function_merge, NULL, NULL, NULL, NULL, -1, -1},
  {"f_motor_get_proprio", function_motor_get_proprio, NULL, NULL, NULL, NULL, -1, -1},
  {"f_motor_position", function_motor_position, new_motor_position, destroy_motor_position, NULL, NULL, -1, -1},
  {"f_pantilt_get_proprio", function_pantilt_get_proprio , new_pantilt_get_proprio, destroy_pantilt_get_proprio, NULL, NULL, -1, -1},
  {"f_pantilt_move_position", function_pantilt_move_position, new_pantilt_move_position, destroy_pantilt_move_position, NULL, NULL, -1, -1},
  {"f_phi_pan", function_phi_pan, NULL, NULL, NULL, NULL, -1, -1},
  {"f_phi_pan_tilt", function_phi_pan_tilt, NULL, NULL, NULL, NULL, -1, -1},
  {"f_project_on_global_space", function_project_on_global_space, NULL, NULL, NULL, NULL, -1, -1},
  {"f_readout", function_readout, NULL, NULL, NULL, NULL, -1, -1},
  {"f_speedo_joint", function_speedo_joint, NULL, NULL, NULL, NULL, -1, -1},
  {"f_motor_is_moving", function_motor_is_moving, new_motor_is_moving, destroy_motor_is_moving, NULL, NULL, -1,- 1},
  {"f_motor_get_position", function_motor_get_position, new_motor_get_position, destroy_motor_get_position, NULL, NULL, -1, -1},
  {"f_motor_get_torque", function_motor_get_torque, new_motor_get_torque, destroy_motor_get_torque, NULL, NULL, -1, -1},
  {"f_motor_set_position", function_motor_set_position, new_motor_set_position, destroy_motor_set_position, NULL, NULL, -1, -1},
  {"f_motor_get_3D_pos_of_joint", function_motor_get_3D_pos_of_joint, new_motor_get_3D_pos_of_joint, destroy_motor_get_3D_pos_of_joint, NULL, NULL, -1, -1},
  {"f_motor_set_speed", function_motor_set_speed, new_motor_set_speed, destroy_motor_set_speed, NULL, NULL, -1, -1},
  {"f_motor_set_torque", function_motor_set_torque, new_motor_set_torque, destroy_motor_set_torque, NULL, NULL, -1,- 1},
  {"f_motor_set_activation", function_motor_set_activation, new_motor_set_activation, destroy_motor_set_activation, NULL, NULL, -1,- 1},
  {"f_handle_panorama", function_handle_panorama, new_handle_panorama, destroy_handle_panorama, NULL, NULL, -1,- 1},
  {"f_detect_end_panorama", function_detect_end_panorama, new_detect_end_panorama, destroy_detect_end_panorama, NULL, NULL, -1,- 1},
  
   
  /* Robot Mvt */
  {"f_action_selection", function_action_selection, NULL, NULL, NULL, NULL, -1, -1},
  {"f_algowinner", function_algowinner, NULL, NULL, NULL, NULL, -1, -1},
  {"f_epsilon_greedy_action", function_epsilon_greedy_action, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_feaod_load_trajvector", function_feaod_load_trajvector, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  /*{"f_explo_alea_classique", function_explo_alea_classique, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  /*{"f_set_direction", function_set_direction, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  /*{"f_explore_et_nid", function_explore_et_nid, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_explo_alea_ou_dirige", function_explo_alea_ou_dirige, NULL, NULL, NULL, NULL, -1, -1},
  {"f_generate_movement", function_generate_movement, NULL, NULL, NULL, NULL, -1, -1},
  {"f_generate_mvt", function_generate_movement, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_get_h", function_get_h, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_location_abs", function_location_abs, new_location_abs, NULL, NULL, NULL, -1, -1},
  {"f_set_robot_location", function_set_robot_location, new_set_robot_location, destroy_set_robot_location, NULL, NULL, -1, -1},
  {"f_location_odo", function_location_odo, new_location_odo, destroy_location_odo, NULL, NULL, -1, -1},
  /*{"f_move_alea", function_move_alea, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_mvt_simule", function_movement_simule, NULL, NULL, NULL, NULL, -1, -1},
  {"f_navigation_movement", function_navigation_movement, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt", function_navigation_movement, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt_robot", function_navigation_movement_robot, NULL, NULL, NULL, NULL, -1, -1},
  {"f_navigation_movement_simple", function_navigation_movement_simple, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt_simple", function_navigation_movement_simple, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt_simule", function_navigation_movement_simule, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt_simule2", function_navigation_movement_simule2, NULL, NULL, NULL, NULL, -1, -1},
  {"f_odometrie", function_odometrie, new_odometrie, NULL, NULL, NULL, -1, -1},
  {"f_mrpt_send_odo",function_mrpt_send_odo,new_mrpt_send_odo, destroy_mrpt_send_odo, NULL, NULL, -1, -1},
  {"f_odo_roburoc", function_odo_roburoc, new_odo_roburoc, NULL, NULL, NULL, -1, -1},
  {"f_orientation", function_orientation, NULL, NULL, NULL, NULL, -1, -1},
  {"f_positionX", function_positionX, NULL, NULL, NULL, NULL, -1, -1},
  {"f_pseudo_movement_simple", function_pseudo_movement_simple, NULL, NULL, NULL, NULL, -1, -1},
  {"f_nav_mvt_simple", function_pseudo_movement_simple, NULL, NULL, NULL, NULL, -1, -1},
  {"f_random_mvt", function_random_mvt, NULL, NULL, NULL, NULL, -1, -1},
  {"f_readout_nms", function_readout_nms, NULL, NULL, NULL, NULL, -1, -1},
  {"f_set_velocity", function_set_velocity, NULL, NULL, NULL, NULL, -1, -1},
  {"f_speedo_angular_robot", function_speedo_angular_robot, NULL, NULL, NULL, NULL, -1, -1},
  {"f_stop_robot", function_stop_robot, NULL, NULL, NULL, NULL, -1, -1},
  {"f_store", function_store, NULL, NULL, NULL, NULL, -1, -1},
  {"f_test_robot", function_test_robot, NULL, NULL, NULL, NULL, -1, -1},
  {"f_test_robubox", function_test_robubox, NULL, NULL, NULL, NULL, -1, -1},
  {"f_true_orientation", function_true_orientation, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_true_orientation_acc", function_true_orientation_acc, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  /*{"f_turn_angle_robot", function_turn_angle_robot, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  
  /* Sensor */
 
 {"f_accelero", function_accelero, NULL, NULL, NULL, NULL, -1, -1},
 {"f_accelero3D", function_accelero3D, NULL, NULL, NULL, NULL, -1, -1},
 {"f_accelero_bras", function_accelero_bras, NULL, NULL, NULL, NULL, -1, -1},  
 {"f_calibre_plateforme", function_calibre_plateforme,  NULL, NULL, NULL, NULL, -1, -1},
 {"f_correction_plateforme", function_correction_plateforme,  NULL, NULL, NULL, NULL, -1, -1},
 {"f_color_detector", function_color_detector, new_color_detector, NULL, NULL, NULL, -1, -1},
 {"f_compass", function_compass, NULL, NULL, NULL, NULL, -1, -1},
 {"f_battery_level", function_battery_level, NULL, NULL, NULL, -1, -1},
 /*{"f_contrast_detector", function_contrast_detector, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
 {"f_gps", function_gps, new_gps, destroy_gps, NULL, NULL, -1, -1},
 {"f_air_sensor", function_air_sensor, NULL, NULL, NULL, NULL, -1, -1},
 {"f_led_rgb", function_led_rgb, NULL, NULL, NULL, NULL, -1, -1},
 {"f_cmd", function_cmd, NULL, NULL, NULL, NULL, -1, -1},
 {"f_IR_circle", function_IR_circle, NULL, NULL, NULL, NULL, -1, -1},
 {"f_IR_circle_target", function_IR_circle_target, NULL, NULL, NULL, NULL, -1, -1},
 {"f_IR_localisation", function_IR_localisation, new_IR_localisation, destroy_IR_localisation, NULL, NULL, -1, -1},
 {"f_IR_simul", function_IR_simul, NULL, NULL, NULL, NULL, -1, -1},
 {"f_laser", function_laser, NULL, NULL, NULL, NULL, -1, -1},
 {"f_laser_LMS", function_laser_LMS, NULL, NULL, NULL, NULL, -1, -1},
 {"f_LED", function_LED, NULL, NULL, NULL, NULL, -1, -1},
 {"f_sensor", function_sensor, NULL, NULL, NULL, NULL, -1, -1},
 {"f_sensor2", function_sensor2, new_sensor2, NULL, NULL, NULL, -1, -1},
 {"f_distance_sensor_get_distance", function_distance_sensor_get_distance, new_distance_sensor_get_distance, destroy_distance_sensor_get_distance, NULL, NULL, -1, -1},
 {"f_distance_sensor_get_matrix", function_distance_sensor_get_matrix, new_distance_sensor_get_matrix, destroy_distance_sensor_get_matrix, NULL, NULL, -1, -1},
 //{"f_distance_sensor_get_matrix_AC", function_distance_sensor_get_matrix_AC, new_distance_sensor_get_matrix_AC, destroy_distance_sensor_get_matrix_AC, NULL, NULL, -1, -1},
 {"f_temperature_sensor_get_matrix", function_temperature_sensor_get_matrix, new_temperature_sensor_get_matrix, destroy_temperature_sensor_get_matrix, NULL, NULL, -1, -1},
 /*{"f_test_neck", function_test_neck, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
 {"f_gripperSensor_get_sensor", function_gripperSensor_get_sensor, new_gripperSensor_get_sensor, destroy_gripperSensor_get_sensor, NULL, NULL, -1,- 1},



  
   /* Serial */
  {"f_AX12_test", function_AX12_test, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_MD23_test", function_MD23_test, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_serial_init", function_serial_init, NULL, NULL, NULL, NULL, -1, -1},

  /* bus tool */
  {"f_bus_get_values", function_bus_get_values, new_bus_get_values, destroy_bus_get_values, NULL, NULL, -1, -1},
  {"f_bus_set_values", function_bus_set_values, new_bus_set_values, destroy_bus_set_values, NULL, NULL, -1, -1},

#ifdef SIM2D
  /* Simulator 2D */
  {"f_sim2d_init", function_sim2d_init, new_function_sim2d_init, NULL, NULL, NULL, -1, -1}, 
  {"f_sim2d_flushimage", function_sim2d_flushimage, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_display", function_sim2d_display, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_sequential_landmarks_perception", function_sim2d_sequential_landmarks_perception, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_landmarks_perception", function_sim2d_landmarks_perception, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_move_agent_abs", function_sim2d_move_agent_abs, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_move_agent_rel",function_sim2d_move_agent_rel, NULL, NULL, NULL, NULL, -1, -1},
  /*{"f_sim2d_move_agent_rel_non_smooth", function_sim2d_move_agent_rel_non_smooth, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
  {"f_sim2d_display_agent", function_sim2d_display_agent, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_agent_proximity", function_sim2d_agent_proximity, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_agent_position", function_sim2d_agent_position, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_place_cell_simul", function_sim2d_place_cell_simul, NULL, NULL, NULL, NULL, -1, -1},
  {"f_sim2d_otheragents_perception", function_sim2d_otheragents_perception, NULL, NULL, NULL, NULL, -1, -1},
  {"f_get_motor_power", function_get_motor_power , new_get_motor_power , NULL, NULL, NULL, -1, -1},
  {"f_set_actuator_value", function_set_actuator_value, new_set_actuator_value, destroy_set_actuator_value, NULL, NULL, -1, -1},

#endif 

  /* pour indiquer la fin du tableau*/
  {NULL, NULL, NULL, NULL, NULL, NULL, -1, -1}
};

void read_help();

void modify_help();


int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}

