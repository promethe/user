/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 @ingroup plugin_list
 @defgroup libSigProc libSigProc
 @brief Groups for signal processing.
 */

#include <stdio.h>
#include <string.h>

//#include <libx.h>

#include <group_function_pointers.h>

#include <SigProc/Convolution_Functions.h>
#include <SigProc/Test_Dynamic.h>
#include <SigProc/Classical_Computation.h>
#include <SigProc/Math.h>
#include <SigProc/Mask.h>

#include <SigProc/libSigProc.h>

type_group_function_pointers group_function_pointers[] =
  {
  /*{-1, -1, "f_file_mem_oscillo", function_file_mem_oscilo, NULL, NULL, NULL, NULL},*/

    { "f_file_mem_oscillo", function_file_mem_oscilo, NULL, NULL, NULL, NULL, -1, -1 },

  /* Classical Computation */
    { "f_angular_distance", function_angular_distance, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_bias", function_bias, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cosinus", function_cosinus, new_cosinus, destroy_cosinus, NULL, NULL, -1, -1 },
    { "f_wave", function_wave, new_wave, destroy_wave, NULL, NULL, -1, -1 },
    { "f_distance", function_distance, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_distance_euclidienne", function_distance_euclidienne, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_d_phi", function_d_phi, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_d_phi2", function_d_phi, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_inverse_neuron", function_inverse_neuron, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_divide", function_divide, new_divide, destroy_divide, NULL, NULL, -1, -1 },
    { "f_diff_time", function_diff_time, new_diff_time, destroy_diff_time, NULL, NULL, -1, -1 },
    { "f_modulo", function_modulo, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_multiply", function_multiply, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_NONOU", function_NONOU, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_product_term_to_term", function_product_term_to_term, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_shift", function_shift, new_shift, NULL, NULL, NULL, -1, -1 },
    { "f_neural_shift_compass", function_neural_shift_compass, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_neural_shift_compass_inv", function_neural_shift_compass_inv, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_shift_bords", function_shift_bords, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sinus", function_sinus, new_sinus, destroy_sinus, NULL, NULL, -1, -1 },
    { "f_bille2D", function_bille2D, new_bille2D, destroy_bille2D, NULL, NULL, -1, -1 },
    { "f_init_direction", function_init_direction, new_init_direction, destroy_init_direction, NULL, NULL, -1, -1 },
    { "f_sqrt", function_sqrt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sum", function_sum, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sum_bearing", function_sum_bearing, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sum_no_seuil", function_sum_no_seuil, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_mem", function_mem, new_mem, destroy_mem, NULL, NULL, -1, -1 },
    { "f_double_threshold", function_double_threshold, new_double_threshold, destroy_double_threshold, NULL, NULL, -1, -1 },
    { "f_double_threshold2", function_double_threshold2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_digital_freq_divider", function_digital_freq_divider, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_band_limit", function_band_limit, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_saturation", function_saturation, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_minimum_limit", function_minimum_limit, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_valeur_absolue", function_valeur_absolue, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_appraise", function_appraise, new_appraise, destroy_appraise, NULL, NULL, -1, -1 },
    { "f_matricial_product", function_matricial_product, new_matricial_product, destroy_matricial_product, NULL, NULL, -1, -1 },
    { "f_perceive", function_perceive, new_perceive, destroy_perceive, NULL, NULL, -1, -1 },
    { "f_random_time", function_random_time, new_random_time, destroy_random_time, NULL, NULL, -1, -1 },

  /* Convolution Functions */
    { "f_conv", function_convolution, new_convolution, NULL, NULL, NULL, -1, -1 },
    { "f_conv_non_circular", function_conv_non_circular, new_conv_non_circular, destroy_conv_non_circular, NULL, NULL, -1, -1 },
    { "f_create_gaussian_map", function_create_gaussian_map, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_n_conv_non_circular", function_n_conv_non_circular, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_n_conv", function_normal_convolution, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_n_convol", function_normal_convolution, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_create_dog", function_create_dog, new_create_dog, NULL, NULL, NULL, -1, -1 },

  /* Math */
    { "f_calibre_perspective", function_calibre_perspective, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_calibre_traj_optimal", function_calibre_traj_optimal, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_derivation_t", function_derivation_t, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_deriv_erreur", function_deriv_erreur, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ecart_type", function_ecart_type, new_ecart_type, destroy_ecart_type, NULL, NULL, -1, -1 },
  /*{"f_erreur_moyenne_antoine", function_erreur_moyenne_antoine, NULL, NULL, NULL, NULL, -1, -1},*/
    { "f_erreur_prediction", function_erreur_prediction, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_find_scale_space_keypoints", function_find_scale_space_keypoints, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_interpolation", function_interpolation, new_interpolation, NULL, NULL, NULL, -1, -1 },
    { "f_interpolation_lineaire", function_interpolation_lineaire, new_interpolation_lineaire, destroy_interpolation_lineaire, NULL, NULL, -1, -1 },
    { "f_learn_sensori_motor", function_learn_sensori_motor, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_minimum", function_minimum, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_moyenne", function_moyenne, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_perspective_to_2D", function_perspective_to_2D, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_runge_kutta", function_runge_kutta, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_shift_colonne", function_shift_colonne, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_statistics", function_statistics, new_statistics, NULL, NULL, NULL, -1, -1 },
    { "f_statistics_one", function_statistics_one, new_statistics_one, NULL, NULL, NULL, -1, -1 },
    { "f_stats", function_stats, new_stats, destroy_stats, NULL, NULL, -1, -1 },
    { "f_non", function_non, new_non, destroy_non, NULL, NULL, -1, -1 },
    { "f_threshold", function_threshold, new_threshold, destroy_threshold, NULL, NULL, -1, -1 },
    { "f_gaussian_2D", function_gaussian2D, new_gaussian2D, destroy_gaussian2D, NULL, NULL, -1, -1 },
    { "f_norm_vecteur", function_norm_vecteur, NULL, NULL, NULL, NULL, -1, -1 },

  /* Mask */
    { "f_create_gabor_mask", function_create_gabor_mask, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_create_gauss_mask", function_create_gauss_mask, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_create_mask", function_create_mask, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_mask", function_load_mask, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_mask_sequential", function_load_mask_sequential, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_test_correlation", function_test_correlation, new_test_correlation, destroy_test_correlation, NULL, NULL, -1, -1 },
    { "f_low_pass_multi_tab", function_low_pass_multi_tab, new_low_pass_multi_tab, destroy_low_pass_multi_tab, NULL, NULL, -1, -1 },
    { "f_selverstone", function_selverstone, NULL, NULL, NULL, NULL, -1, -1 },
  /* pour indiquer la fin du tableau*/
    { NULL, NULL, NULL, NULL, NULL, NULL, -1, -1 } };

void read_help();

void modify_help();

int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}

