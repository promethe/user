/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_seconds_to_break.c 
\brief 

Author: R. Shirakawa
Created: 03/08/2006

Description: A lier avec la boite BREAK, cette fonction active son neurone quand le temps
de attendre est arrive. l'utilisateur peut selectionner le temps d'attend en mettant une liaison
avec "-t" et les seconds a attendre.

Macro:
-none

Local variables:
-unsigned int i;

Global variables:
-InputFunctionTimeTrace
-OutputFunctionTimeTrace
-SecondesFunctionTimeTrace
-MicroSecondesFunctionTimeTrace

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <libx.h>
#include <stdlib.h>
#include <time.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <sys/time.h>

#define SECONDS_TO_WAIT 2

typedef struct
{
    struct timeval timer;       /* variable pour faire le temps d'attend minimum entre une e autre expression */
    struct timeval actual_time; /* idem */
    time_t cur_time;
    float wait;
    int neurone;
} time_break;

void new_seconds_to_break(int Gpe)
{
    int gpe_amounte;            /* parametres de la ligne d'entree de la boite */
    time_break *init_time;  
    char param_link[10];    
    init_time = (time_break *) malloc(sizeof(time_break));
            /* nom de la ligne d'entree de la boite */

#ifdef DEBUG
    printf("begin of %s\n", __FUNCTION__);
#endif

    init_time->wait = SECONDS_TO_WAIT;
    init_time->neurone = def_groupe[Gpe].premier_ele;
    gpe_amounte = find_input_link(Gpe, 0);

    /* secondes pour attendre avant envoyer le siganl de break */
    if (prom_getopt(liaison[gpe_amounte].nom, "-t", param_link) == 2)
    {
        init_time->wait = atof(param_link);
    }
    gettimeofday(&init_time->timer, (void *) NULL);

    /* passer les donnees a la fonction */
    def_groupe[Gpe].data = init_time;

#ifdef DEBUG
    printf("init_time->wait = %g, group %d, %s\n", init_time->wait, Gpe,
           __FUNCTION__);
    printf("end of %s\n", __FUNCTION__);
#endif

}

void function_seconds_to_break(int Gpe)
{
    time_break *my_time;
    my_time = (time_break *) def_groupe[Gpe].data;

#ifdef DEBUG
    printf("begin of %s\n", __FUNCTION__);
#endif

    /* temps pour break. */
    gettimeofday(&my_time->actual_time, (void *) NULL);

    neurone[my_time->neurone].s2 = neurone[my_time->neurone].s1 =
        neurone[my_time->neurone].s = 0.;

#ifdef DEBUG
    printf
        ("my_time->timer.tv_sec = %d, group %d, neurone = %f, function : %s \n",
         my_time->timer.tv_sec, Gpe, neurone[my_time->neurone].s,
         __FUNCTION__);
#endif

    if ((int) (my_time->timer.tv_sec + my_time->wait) >
        (int) (my_time->actual_time.tv_sec))
        return;

#ifdef DEBUG
    printf("*************PASSEE!!!************, group %d %s\n", Gpe,
           __FUNCTION__);
#endif

    gettimeofday(&my_time->timer, (void *) NULL);
    neurone[my_time->neurone].s2 = neurone[my_time->neurone].s1 =
        neurone[my_time->neurone].s = 1.;

    printf("Groupe %d break activated ( time = %d )\n", Gpe,
          (int) (my_time->timer.tv_sec));

#ifdef DEBUG
    printf("neurone = %d\n", neurone[my_time->neurone].s);
    printf("end of %s\n", __FUNCTION__);
#endif

}
