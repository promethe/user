/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_delay.c
    \brief 

    Author: P. Gaussier
    Created: 01/12/2001
    Modified:
    - author: P. Gaussier
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 



    Cette fonction doit etre mise en sortie d'un groupe de meme taille.	
    Les neurones de cette fonctions ont la meme activite que celle que les neurones du   
    groupe en entree avaient a l'iteration precedente. 

    Remarque 1: Cette fonction, c est  le Gyrus Dentele !							    
    Remarque 2: Si tous les neurones du groupe en entree avaient une activite nulle,       
    l'activite qui apparait est la derniere activitee non nulle constatee.
    C'est ce qui fait la difference avec la fonction z-1 representant un retard pur.	     


    Macro:
    -none 

    Local variables:
    - l'allocation dynamique d'une structure data_function_delay a la premiere execution
    permet de sauvegrader le contexte de la fonction (accelerer l'execution) et
    stocker dans un champs *retard le tableau des anciennes activites des neurones.
    Cette structure est stockee dans le champs data du groupe.

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools:
    -Kernel_Function/find_input_link()

    Links:
    - type: algo
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>



#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

/** uncomment to remove delay - CAUTION ! */
/** only for particular checking tests */
/* #define NO_DELAY 1 */
/* #define DEBUG */

typedef struct data_function_delay
{
      int gpe_reset;
      int gpe_entree;
      int local;
      float *retards2;
      float *retards1;
} data_function_delay;

void new_delay(int gpe)
{
   int gpe_reset = -1, gpe_entree = -1;
   int local = 0;
   int i, l, size;
   data_function_delay *my_data = NULL;

   dprints("new_delay(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {
      
      l = 0;
      i = find_input_link(gpe, l);
      while (i != -1)
      {
	 if (strcmp(liaison[i].nom, "reset") == 0)
	    gpe_reset = liaison[i].depart;
	 else
	 {
	    gpe_entree = liaison[i].depart;
	    
	    if (strcmp(liaison[i].nom, "local") == 0)
	       local = 1;
	 }	       
	 
	 l++;
	 i = find_input_link(gpe, l);	 
      }
      
      if (gpe_entree < 0)
      {
	 fprintf(stderr, "ERROR in new_delay(%s): Missing input group\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      size = def_groupe[gpe].nbre;
      if (size != def_groupe[gpe_entree].nbre)
      {
	 fprintf(stderr, "ERROR in new_delay(%s): Sizes of current and input groups are different (%d != %d)\n",
		 def_groupe[gpe].no_name, size, def_groupe[gpe_entree].nbre);
	 exit(1);
      }
      
      my_data = (data_function_delay *) malloc(sizeof(data_function_delay));
      if (my_data == NULL)
      {
	 fprintf(stderr, "ERROR in new_delay(%s): malloc failed for data\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data->gpe_entree = gpe_entree;
      my_data->gpe_reset = gpe_reset;
      my_data->local = local;

      my_data->retards2 = (float *) malloc(size * sizeof(float));
      my_data->retards1 = (float *) malloc(size * sizeof(float));
      if (my_data->retards1 == NULL || my_data->retards2 == NULL)
      {
	 fprintf(stderr, "ERROR in new_delay(%s): malloc failed for retards\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      for (i = 0; i < size; i++)
      {
	 my_data->retards2[i] = 0.;
	 my_data->retards1[i] = 0.;
      }
      def_groupe[gpe].data = my_data;
   }   

   dprints("new_delay(%s): Leaving function\n", def_groupe[gpe].no_name);
}
 
void function_delay(int gpe)
{
   int gpe_entree, gpe_reset;
   int activity = 0;
   int i;
   int input_first;
   data_function_delay *my_data;
   int first = def_groupe[gpe].premier_ele;
   int size = def_groupe[gpe].nbre;

   my_data = (data_function_delay *) def_groupe[gpe].data;
   if (my_data == NULL)
   {
      fprintf(stderr, "ERROR in f_delay(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
      exit(1);
   }

   gpe_reset = my_data->gpe_reset;
   gpe_entree = my_data->gpe_entree;
   input_first = def_groupe[gpe_entree].premier_ele;

   if (gpe_reset >= 0 && neurone[def_groupe[gpe_reset].premier_ele].s2 > 0.)
   {
      for (i = 0; i < size; i++)
      {
	 my_data->retards2[i] = 0.;
	 my_data->retards1[i] = 0.;
      }
   }
   /* 1) uncomment to have delay */
#ifndef NO_DELAY
#ifdef DEBUG
   cprints("EC delay\n");
#endif
   for (i = 0; i < size; i++)
   {
      neurone[first + i].s = neurone[first + i].s1 = my_data->retards1[i];
      neurone[first + i].s2 = my_data->retards2[i];
   }
#endif

   for (i = input_first; i < input_first + size; i++)
   {
      if (isdiff(neurone[i].s2, 0.))
      {
	 activity = 1;
	 break;
      }
   }

   if (activity == 1)
   {
      for (i = 0; i < size; i++)
      {
	 if (my_data->local == 0 || isdiff(neurone[input_first + i].s2, 0.))
	 {
	    my_data->retards2[i] = neurone[input_first + i].s2;
	    my_data->retards1[i] = neurone[input_first + i].s1;
	 }
      }
   }

#ifdef NO_DELAY
#ifdef DEBUG
   cprints("EC no delay\n");
#endif
   for (i = 0; i < size; i++)
   {
      neurone[first + i].s = neurone[first + i].s1 = my_data->retards1[i];
      neurone[first + i].s2 = my_data->retards2[i];
   }
#endif
}

void destroy_delay(int gpe)
{
   data_function_delay *my_data = NULL;

   dprints("destroy_delay(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data != NULL)
   {
      my_data = (data_function_delay *) def_groupe[gpe].data;

      free(my_data->retards1);
      my_data->retards1 = NULL;

      free(my_data->retards2);
      my_data->retards2 = NULL;

      free(my_data);
      def_groupe[gpe].data = NULL;
   }

   dprints("destroy_delay(%s): Entering function\n", def_groupe[gpe].no_name);
}
