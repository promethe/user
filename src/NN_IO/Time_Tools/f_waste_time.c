/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_waste_time.c 
\brief 

Author: P. Gaussier
Created: 01/01/20001


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: Fonciton qui utilise du temps de calcul: option -t1000 par exemple

Macro:
-none

Local variables:
-none
 
Global variables:
-temps

Internal Tools:
-none

External Tools: 
-non

Links:
-none

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
double waste_time(long n)
{
	double res = 0;
	long i = 0;
	while (i <n * 200000) {
		i++;
		res += sqrt(i);
	}
	return res;
}

void function_waste_time(int gpe)
{
    long temps_calcul=10;
    int i,l;
    char param[32];
    if(def_groupe[gpe].data==NULL)
    {
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "t", param) == 2)
            {
                    temps_calcul = atol(param);
            }
            i++;
            l = find_input_link(gpe, i);
         }
	 def_groupe[gpe].data=(long*)malloc(sizeof(long));
	*((long*)(def_groupe[gpe].data))=temps_calcul;
    }
    else
    {
  	temps_calcul=*((long*)(def_groupe[gpe].data));
    }
	
    printf("\t waste_time %d\n",(int)temps_calcul);
    waste_time(temps_calcul);
    

}
