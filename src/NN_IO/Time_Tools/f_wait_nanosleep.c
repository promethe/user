/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_wait_nanosleep f_wait_nanosleep
\ingroup libNN_IO

\brief Attend pendant une certaine quantite de temps

\details

\section Options
Option on input link gives the amount of time to wait.
-s seconds
-n nanoseconds
If a value is given after the option, it is used as wait time.
If there is no value after the option, then the group look for the first neuron in the input group of the link. The rounded output value s1 of this neuron is used as the wait time following the rule : -n takes nano seconds, -s takes seconds.

\section WARNING 
WARNING : If someone uses -n-s on a link, the value of the first neuron is used for seconds and nanoseconds. This may not be the best way of doing this.
One should uses two different links to do that.

\file  f_wait_nanosleep.c
\ingroup f_wait_nanosleep
\brief  Attend pendant une certaine quantite de temps

Author: Raoul
Created: 20/04/2000
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Option on input link gives the amount of time to wait.
-s seconds
-n nanoseconds
If a value is given after the option, it is used as wait time.
If there is no value after the option, then the group look for the first neuron in the input group of the link. The rounded output value s1 of this neuron is used as the wait time following the rule : -n takes nano seconds, -s takes seconds.

WARNING : If someone uses -n-s on a link, the value of the first neuron is used for seconds and nanoseconds. This may not be the best way of doing this.
One should uses two different links to do that.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

typedef struct MyData_f_wait_nanosleep
{
  struct timespec duree_nanosleep;
  int gpe;
  int no_gpe_entree;
} MyData_f_wait_nanosleep;


void function_wait_nanosleep(int numero)
{
  int i, l;
  int duree_seconde = 0, duree_nano = 0;
  char param_link[255];
  struct timespec duree_nanosleep, res;
  MyData_f_wait_nanosleep *my_data = NULL;
  float signal = 0;
  int gpe = -1;
int no_gpe_entree=-1;
  
  if (def_groupe[numero].data == NULL)
    {
      
      l = 0;
      i = find_input_link(numero, l);
      while (i != -1)
      {
	  
	  if (prom_getopt(liaison[i].nom, "s", param_link) == 2)
            {
	      duree_seconde = atoi(param_link);
            }
	  if (prom_getopt(liaison[i].nom, "s", param_link) == 1)
            {
	      duree_seconde =(int)round(neurone[def_groupe[liaison[i].depart].premier_ele].s1);
            }
	  if (prom_getopt(liaison[i].nom, "n", param_link) == 2)
            {
	      duree_nano = atoi(param_link);
            }
	  if (prom_getopt(liaison[i].nom, "n", param_link) == 1)
            {
	      duree_nano = (int)round(neurone[def_groupe[liaison[i].depart].premier_ele].s1);
		no_gpe_entree=liaison[i].depart;
            }
	  if (strcmp(liaison[i].nom, "learn") == 0)
	    {
	      gpe = liaison[i].depart;
	    }
	  i = find_input_link(numero, l);
	  l++;
        }
      duree_nanosleep.tv_sec = duree_seconde;
      duree_nanosleep.tv_nsec = duree_nano;
      
      my_data = (MyData_f_wait_nanosleep *) malloc(sizeof(MyData_f_wait_nanosleep));
      if (my_data == NULL)
	{
	  printf("error malloc in %s\n", __FUNCTION__);
	  exit(0);
	}
      my_data->duree_nanosleep = duree_nanosleep;
      my_data->gpe = gpe;
my_data->no_gpe_entree=no_gpe_entree;
      def_groupe[numero].data = (MyData_f_wait_nanosleep *) my_data;
    }
  else
    {
      my_data = (MyData_f_wait_nanosleep *) (def_groupe[numero].data);
	if(my_data->no_gpe_entree!=-1)
	{
		duree_nano = (int)round(neurone[def_groupe[my_data->no_gpe_entree].premier_ele].s1);
		(my_data->duree_nanosleep).tv_nsec=duree_nano;
	}
      duree_nanosleep = my_data->duree_nanosleep;
      gpe = my_data->gpe;
    }
  
  
  if(gpe >= 0)
    signal = neurone[def_groupe[gpe].premier_ele].s1;
  else
    signal = 0;
  
  
  
  if(signal < 0.5)
    {
      dprints("sleep during %ld sec %ld nsec\n", duree_nanosleep.tv_sec,
	     duree_nanosleep.tv_nsec);
      nanosleep(&duree_nanosleep, &res);
    }
  
}
