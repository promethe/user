/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/***
 * Description : 
 * Compute time between 2 execution : put on output neuron, can also be printed on standard output
 * -quiet : no standard output printing
 * -m choose the static part of the message to be printed on the standard output
 */

#include <Global_Var/SigProc.h>
#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <Kernel_Function/find_input_link.h>

typedef struct MyData
{
  struct timeval LastInputFunctionTimeTrace;
  char message[255];
  int quiet;
} MyData;


void function_delta_time(int gpe)
{
	struct timeval InputFunctionTimeTrace,LastInputFunctionTimeTrace;
	struct MyData *mydata=NULL;
	long MicroSecondesFunctionTimeTrace;
	char *message=NULL;
	
	mydata=def_groupe[gpe].data;
	if ( mydata == NULL ) {
		printf("Oups, pas normal ça .. function_delta_time()");
		exit(1);
	}
	
	LastInputFunctionTimeTrace=mydata->LastInputFunctionTimeTrace;
	message=mydata->message;
	
	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
	
	/* Premier passage dans la fonction*/
	if ( LastInputFunctionTimeTrace.tv_sec==0 && LastInputFunctionTimeTrace.tv_usec==0 ){
		mydata->LastInputFunctionTimeTrace=InputFunctionTimeTrace;
		
		return;
	}
	
	MicroSecondesFunctionTimeTrace=(InputFunctionTimeTrace.tv_sec - LastInputFunctionTimeTrace.tv_sec)*1000000+(InputFunctionTimeTrace.tv_usec - LastInputFunctionTimeTrace.tv_usec);
	
	if (!(mydata->quiet) && message != NULL) {
		printf("%s (%d) :\t\t %ld microsecondes\n",mydata->message,gpe,MicroSecondesFunctionTimeTrace);
	}

	neurone[def_groupe[gpe].premier_ele].s=neurone[def_groupe[gpe].premier_ele].s1=	neurone[def_groupe[gpe].premier_ele].s2=MicroSecondesFunctionTimeTrace;
		
	mydata->LastInputFunctionTimeTrace=InputFunctionTimeTrace;
	
}


void new_f_delta_time(int gpe)
{
  MyData *mydata = NULL;
  int lien;
  char param_link[255];
  
  mydata=(MyData*)malloc(sizeof(MyData));
  if(mydata==NULL) {
    printf("pb malloc dans %d\n",gpe);
    exit(0);
  }
  def_groupe[gpe].data=mydata;
  
  /* On regarde s'il y un message a afficher*/
  lien=find_input_link(gpe,0);
  if (prom_getopt(liaison[lien].nom, "quiet", param_link) >= 1)
    {
      mydata->quiet=1;
    }
  else {
    mydata->quiet=0;
  }
  if (prom_getopt(liaison[lien].nom, "m", param_link) == 2){
    sprintf(mydata->message, "%s", param_link);
  }
  else {
    sprintf(mydata->message, "%s", "## Delta time");
  }
  (mydata->LastInputFunctionTimeTrace).tv_sec=(mydata->LastInputFunctionTimeTrace).tv_usec=0;
  
  /* RAJOUTER la possibilite de mettre une chaine de caractere sur le lien en entree du groupe ..*/
}
