/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_delay_timing.c 
    \brief 

    Author: Julien Hirel
    Created: 01/12/2009
    Modified:
    - author: 
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
    - Activate a neuron when it has been receiving activity for a certain amount of time.

    Macro:

    Local varirables:

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    - delay : link to the group whose activity specifies the delay in second
    - time_generator : f_TimeGenerator group in case the time is simulated

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
/*#define DEBUG*/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <Kernel_Function/find_input_link.h>

typedef struct data_delay_timing
{
      int gpe_delay;
      int gpe_time_generator;
      struct timeval *activation_times;
      
} data_delay_timing;


void new_delay_timing(int gpe)
{ 
   int i;
   int gpe_delay = -1;
   int gpe_time_generator = -1;
   int index, link;
   data_delay_timing *my_data = NULL;

   if (def_groupe[gpe].data == NULL)
   {
      index = 0;
      link = find_input_link(gpe, index);
      while (link != -1)
      {
	 if (strcmp(liaison[link].nom, "delay") == 0)
	    gpe_delay = liaison[link].depart;

	 if (strcmp(liaison[link].nom, "time_generator") == 0)
	    gpe_time_generator = liaison[link].depart;

	 index++;
	 link = find_input_link(gpe, index);
      }

      if (gpe_delay == -1)
      {
	 EXIT_ON_ERROR("Missing 'delay' input link");
      }

      my_data = ALLOCATION(data_delay_timing);

      my_data->activation_times = MANY_ALLOCATIONS(def_groupe[gpe].nbre, struct timeval);
      for (i = 0; i < def_groupe[gpe].nbre; i++)
      {
	 my_data->activation_times[i].tv_sec = -1;
	 my_data->activation_times[i].tv_usec = -1;
      }
      my_data->gpe_delay = gpe_delay;
      my_data->gpe_time_generator = gpe_time_generator;
 
      def_groupe[gpe].data = my_data;
   }
}

 
void function_delay_timing(int gpe)
{
   int i, first;
   float act = 0.;
   double time_elapsed = 0.;
   type_coeff *coeff = NULL;
   struct timeval current_time;
   struct timeval *ref_time = NULL;
   data_delay_timing *my_data = NULL;
   
   first = def_groupe[gpe].premier_ele;

   my_data = (data_delay_timing *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retreive data");
   }
   
   if (my_data->gpe_time_generator >= 0)
   {
      ref_time = (struct timeval *) def_groupe[my_data->gpe_time_generator].ext;
      
      if (ref_time == NULL)
      {
	 EXIT_ON_ERROR("NULL pointer for ext in time_generator group");
      }
      
      current_time.tv_sec = ref_time->tv_sec;
      current_time.tv_usec = ref_time->tv_usec;
   }
   else 
   {
      gettimeofday(&current_time, NULL);
   }

   dprints("f_delay_timing(%s): current time is %i sec %i usec\n", def_groupe[gpe].no_name, current_time.tv_sec, current_time.tv_usec);

   for (i = 0; i <  + def_groupe[gpe].nbre; i++)
   {
      neurone[first + i].s1 = neurone[first + i].s2 = 0;
      act = 0.;
      coeff = neurone[first + i].coeff;
      
      while (coeff != NULL)
      {
	 act += coeff->val * neurone[coeff->entree].s1;
	 
	 coeff = coeff->s;
      }

      if (act > def_groupe[gpe].seuil)
      {
	 dprints("f_delay_timing(%s): neuron %i excited\n", def_groupe[gpe].no_name, i);

	 /* First activation */
	 if (neurone[first + i].s < def_groupe[gpe].seuil)
	 {
	    my_data->activation_times[i].tv_sec = current_time.tv_sec;
	    my_data->activation_times[i].tv_usec = current_time.tv_usec;
	    dprints("f_delay_timing(%s): First excitation for neuron %i, activation time = %i sec, %i usec\n", def_groupe[gpe].no_name, i, my_data->activation_times[i].tv_sec, my_data->activation_times[i].tv_usec);
	 }
	 /* Neuron was already activated */
	 else 
	 {
	    time_elapsed = (double) current_time.tv_sec + (double) current_time.tv_usec / 1000000. - (double) my_data->activation_times[i].tv_sec - (double) my_data->activation_times[i].tv_usec / 1000000.;

	    dprints("f_delay_timing(%s): time passed excited for neuron %i = %f sec (%f + %f - %f - %f), delay is %f\n", def_groupe[gpe].no_name, i, time_elapsed, (double) current_time.tv_sec, current_time.tv_usec / 1000000., (double) my_data->activation_times[i].tv_sec, my_data->activation_times[i].tv_usec / 1000000., neurone[def_groupe[my_data->gpe_delay].premier_ele].s1);
	    
	    /* The delay has passed */
	    if (time_elapsed > neurone[def_groupe[my_data->gpe_delay].premier_ele].s1)
	    {
	       dprints("f_delay_timing(%s): Activation for neuron %i\n", def_groupe[gpe].no_name, i);
	       neurone[first + i].s1 = act;
	       neurone[first + i].s2 = 1;
	    }	       
	 }
      }	    
      else
      {
	 my_data->activation_times[i].tv_sec = -1;
	 my_data->activation_times[i].tv_usec = -1;	 
      }

      neurone[first + i].s = act;
   }
}


void destroy_delay_timing(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_delay_timing*)def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_delay_timing(%s): Leaving function\n", def_groupe[gpe].no_name);
}
