/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: R. Shirakawa
Created: 18/05/2006

Description: montre le temps pour faire un numero fixe d'iteractions(ITERACTIONS_NUMBER).

Macro:
-none

Local variables:
-unsigned int i;

Global variables:
-InputFunctionTimeTrace
-OutputFunctionTimeTrace
-SecondesFunctionTimeTrace
-MicroSecondesFunctionTimeTrace

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <libx.h>
#include <stdlib.h>
#include <sys/time.h>


#define ITERATIONS_NUMBER 100

typedef struct
{
    struct timeval start_TimeLocal;
    struct timeval TimeLocal;
    unsigned int secondes;
    unsigned int microsecondes;
    int iterations;
} time_it;

void new_time_iterations(int Gpe)
{
    time_it *init_time;
    init_time = (time_it *) malloc(sizeof(time_it));
    init_time->iterations = 0;
    init_time->secondes = 0;
    init_time->microsecondes = 0;
    gettimeofday(&init_time->start_TimeLocal, (void *) NULL);
    def_groupe[Gpe].data = init_time;
}

void function_time_iterations(int Gpe)
{
    time_it *my_time;
    my_time = (time_it *) def_groupe[Gpe].data;
    if (my_time->iterations % ITERATIONS_NUMBER == 0)
    {
        gettimeofday(&my_time->TimeLocal, (void *) NULL);
        if (my_time->TimeLocal.tv_usec >= my_time->start_TimeLocal.tv_usec)
        {
            my_time->secondes =
                my_time->TimeLocal.tv_sec - my_time->start_TimeLocal.tv_sec;
            my_time->microsecondes =
                my_time->TimeLocal.tv_usec - my_time->start_TimeLocal.tv_usec;
        }
        else
        {
            my_time->secondes =
                my_time->TimeLocal.tv_sec - my_time->start_TimeLocal.tv_sec -
                1;
            my_time->microsecondes =
                1000000 + my_time->TimeLocal.tv_usec -
                my_time->start_TimeLocal.tv_usec;
        }
        gettimeofday(&my_time->start_TimeLocal, (void *) NULL);
        printf("temps pour %d itertions: %d.%d\n", ITERATIONS_NUMBER,
               my_time->secondes, my_time->microsecondes);
    }
    my_time->iterations++;
}
