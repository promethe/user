/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_delay.c
\brief 

Author: P. Gaussier
Created: 01/12/2001
Modified:
- author: P. Gaussier
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 



Cette fonction doit etre mise en sortie d'un groupe de meme taille.	
Les neurones de cette fonctions ont la meme activite que celle que les neurones du   
groupe en entree avaient a l'iteration precedente. 

Remarque 1: Cette fonction, c est  le Gyrus Dentele !							    
Remarque 2: Si tous les neurones du groupe en entree avaient une activite nulle,       
l'activite qui apparait est la derniere activitee non nulle constatee.
C'est ce qui fait la difference avec la fonction z-1 representant un retard pur.	     


Macro:
-none 

Local variables:
- l'allocation dynamique d'une structure data_function_delay � la premiere ex�cution
permet de sauvegrader le contexte de la fonction (acc�l�rer l'ex�cution) et
stocker dans un champs *retard le tableau des anciennes activit�s des neurones.
Cette structure est stock�e dans le champs data du groupe.

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/trouver_entree.h>
typedef struct data_function_delay{
  int gpe_entree;
  int taille_groupe_e;
  int deb_e ;
  int tailleX_e;
  int tailleY_e;
  int increment;
  float *retard;
  char	nom_liaison[256];
} data_function_delay;

void function_delay(int gpe_sortie)
{
  int		gpe_entree;
  int		taille_groupe_e;	
  int		taille_groupe_s;		
  int		deb_e, deb_s;
  int		increment;
  char		*nom_liaison;
  int 		i,reset_grp;
  int		tailleX_e, tailleY_e,nul;
  float	*retard;
  data_function_delay *data;
  static int reset=0;
  
#ifdef DEBUG 
 printf("-----------  Function delay -----------\n");
#endif
  
  
  deb_s = def_groupe[gpe_sortie].premier_ele;
  taille_groupe_s=def_groupe[gpe_sortie].nbre;
  data=def_groupe[ gpe_sortie].data;
 reset_grp=trouver_entree(gpe_sortie,"reset");
 if(reset_grp>-1)
 if(neurone[def_groupe[reset_grp].premier_ele].s2>0)
 if(reset==0){data=NULL;reset=1;printf("reseted f_delay done!\n");}
  if(data==NULL)   /*premier passage initialisation */
    {
      data=def_groupe[ gpe_sortie].data=( data_function_delay*)malloc(sizeof(data_function_delay));
      i=find_input_link( gpe_sortie,0);
      if(i<0) {printf("ERROR Function f_delay must have one input group\n"); exit(1);}

      data->gpe_entree      = gpe_entree      = liaison[i].depart;
      strcpy(data->nom_liaison,liaison[i].nom);
      nom_liaison           = data->nom_liaison;
      data->taille_groupe_e = taille_groupe_e = def_groupe[gpe_entree].nbre;
      data->deb_e 	    = deb_e           = def_groupe[gpe_entree].premier_ele;
      data->tailleX_e       = tailleX_e       = def_groupe[gpe_entree].taillex;
      data->tailleY_e       = tailleY_e       = def_groupe[gpe_entree].tailley;
      data->increment       = increment       = taille_groupe_e/(tailleX_e*tailleY_e);

      if (taille_groupe_s!=taille_groupe_e)
	{
	  printf("Taille incoherente avec la taille de la carte d'entree\n");
	  exit(1);
	}

      data->retard          =  (float *)malloc(taille_groupe_s*sizeof(float));
		 printf("retard alloue\n");
      retard =data->retard;
      for(i=0;i<taille_groupe_s;i++)
	{
	  neurone[deb_s+i].s=
	  neurone[deb_s+i].s1=
	  neurone[deb_s+i].s2=0.;
	   
		    printf("new act to delayed:%f, %d\n",neurone[deb_e+i].s2,i);
	  retard[i]=neurone[deb_e+i].s2;
	}
	 scanf("%d",&nul);
    }
  else
    {

      gpe_entree      = data->gpe_entree ;   
      nom_liaison     = data->nom_liaison ;
      taille_groupe_e = data->taille_groupe_e ;
      deb_e           = data->deb_e ;
      tailleX_e       =data->tailleX_e ;
      tailleY_e       = data->tailleY_e ;
      increment       = data->increment ;
      retard          =data->retard;
      for(i=0;i<taille_groupe_s;i++)
	{
	  neurone[deb_s+i].s=
	  neurone[deb_s+i].s1=
     neurone[deb_s+i].s2=retard[i];
	   printf("act:%f, %d\n",retard[i],i);
	}
  /*  scanf("%d",&nul);*/
      nul=1;
		/*si il y a une activite sur le groupe d'entree alors on la copie*/
      for (i=0;i<taille_groupe_e;i++){
	if (neurone[deb_e+i].s2!=0){
	  nul=0;
     break; /*nc: inutile de continuer...*/
  }
}
      if (!nul){
	for(i=0;i<taille_groupe_e;i++)
	  {	 
		    printf("new act to delayed:%f, %d\n",neurone[deb_e+i].s2,i);
	    retard[i]=neurone[deb_e+i].s2;
	  }
	 scanf("%d",&nul);
    }
	 
	 
	 
	 
	 }
}	
