/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_gettime f_gettime
\ingroup libNN_IO

\brief Affiche le temps ecoule depuis le premier passage dans la fonction

\details

\section Description

Affiche le temps ecoule depuis le premier passage dans la fonction

\file f_gettime.c
\ingroup f_gettime
\brief Affiche le temps ecoule depuis le premier passage dans la fonction

Author: XXX
Created: XXX
Modified:
- author: XXX
- description: XXX
- date: XX/XX/XXXX

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Affiche le temps ecoule depuis le premier passage dans la fonction

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
-none

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <Global_Var/SigProc.h>
#include <Global_Var/NN_IO.h>
#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <Kernel_Function/find_input_link.h>

typedef struct MyData_f_gettime
{
	struct timeval FirstInputFunctionTimeTrace;
} MyData_f_gettime;

void new_f_gettime(int gpe)
{
	MyData_f_gettime *mydata = NULL;
	
	mydata=(MyData_f_gettime*)malloc(sizeof(MyData_f_gettime));
	if(mydata==NULL) {
		printf("pb malloc dans %d\n",gpe);
		exit(0);
	}
	def_groupe[gpe].data=mydata;
        mydata->FirstInputFunctionTimeTrace=prom_started_date;

}


void function_gettime(int gpe)
{
	struct timeval InputFunctionTimeTrace,FirstInputFunctionTimeTrace;
	struct MyData_f_gettime *mydata=NULL;
	long MicroSecondesFunctionTimeTrace;
	
	mydata=def_groupe[gpe].data;
	if ( mydata == NULL ) 	
		new_f_gettime(gpe);

	FirstInputFunctionTimeTrace=mydata->FirstInputFunctionTimeTrace;
	
	
	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
	
	/* Premier passage dans la fonction*/
	
	MicroSecondesFunctionTimeTrace=(InputFunctionTimeTrace.tv_sec - FirstInputFunctionTimeTrace.tv_sec)*1000000+(InputFunctionTimeTrace.tv_usec - FirstInputFunctionTimeTrace.tv_usec);
	
	printf("temps dans %s: %d\n",def_groupe[gpe].no_name,(int)MicroSecondesFunctionTimeTrace);
	
	neurone[def_groupe[gpe].premier_ele].s=neurone[def_groupe[gpe].premier_ele].s1=	neurone[def_groupe[gpe].premier_ele].s2=MicroSecondesFunctionTimeTrace;
	
}

