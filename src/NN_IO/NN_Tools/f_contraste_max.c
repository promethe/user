/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_contraste_max.c 
\brief 

Author: P. Gaussier
Created: 17/05/15


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   On fait l'hypothese que les neurones sont sur un champts 1D.
   Les neurones de la boite une valeur liee au "contraste" entre le max et les activites des neurones distants du max. 
   Cette fonction est utiisee pour fabriquer un signal de renforcement liee a la precision de la reconstruction d'une position/


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <net_message_debug_dist.h>


void function_contraste_max(int gpe_sortie)
{
	int deb = def_groupe[gpe_sortie].premier_ele;
	int i, pos_max;
	type_coeff *coeff;
	float max, val;
   int dist_min=4;
   float somme=0;

	dprints("function_contraste_max gpe %s \n", def_groupe[gpe_sortie].no_name);

	for (i = deb; i < deb + def_groupe[gpe_sortie].nbre; i++)
	{
		max = -999999;
		coeff = neurone[i].coeff;
		while (coeff != NULL)
		{
			val = coeff->val * neurone[coeff->entree].s1;
			if (max < val)
			{
				max = val;
            pos_max=coeff->entree;
			}
			coeff = coeff->s;
		}
      
      somme=0.;
      coeff = neurone[i].coeff;
		while (coeff != NULL)
		{
         val = coeff->val * neurone[coeff->entree].s1;
			if (fabs(coeff->entree-pos_max)> dist_min && val>1e-8)
			{
				somme=somme + (1.+val)*fabs(coeff->entree-pos_max); /* l'entree est d'autant plus importante dans la somme quelle est loin du max */
            dprints("(%f, %1.3f) ",val,(1.+val)*fabs(coeff->entree-pos_max));

			}
         else { dprints("(%1.3f, %1.3f) ",val,0.);}
			coeff = coeff->s;
		}
      dprints("\n");   
      if(somme<=0.) somme=1.;
		neurone[i].s = neurone[i].s1 = neurone[i].s2 = max/somme;

	}
}

