/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_pos_offset f_pos_offset
\ingroup libNN_IO

\brief Introduit un offset de position dans la carte neuronale

\details
\section Description

Introduit un offset de position dans la carte neuronale
  x offset en x (0 par defaut)
  y offset en y (0 par defaut)
En sortie, le neurone (0,0) correspond au neurone (x,y) en entree. 

\section Options
 recuperation sous la forme : x%d,y%d


\file  f_pos_offset.c 
\ingroup f_pos_offset

\brief Introduit un offset de position dans la carte neuronale

Author: A. de Rengerve
Created: 21/04/2009
Modified:
- author: A. de Rengerve
- description: specific file creation
- date: 21/04/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: Introduit un offset de position dans la carte neuronale
  x offset en x (0 par defaut)
  y offset en y (0 par defaut)

 recuperation sous la forme : x%d,y%d

En sortie, le neurone (0,0) correspond au neurone (x,y) en entree. 



Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
 Groupe non teste pour le cas ou le nombre de neurone est plus grand 
que le produit taillex*tailley.
 Normalement, ce cas doit etre gere par ce groupe.

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org

************************************************************/
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */
#include<net_message_debug_dist.h>

typedef struct my_data {
  int gpe_entree;
  int gpe_entree_deb;
  int gpe_deb;
  int x_offset;
  int y_offset;
} myData;

void function_pos_offset(int num)
{  
   myData *mydata=NULL;
   int i=0,j,k;
   int x=0, y=0;
   int entree_deb, deb;
   int increment, nbre, nbre2;
   int index_entree, index;
   int taillex, tailley;
   int ret;


  dprints("execution de f_pos_offset\n");

   if(def_groupe[num].data==NULL) {
        mydata = (myData*)malloc(sizeof(myData));

	mydata->gpe_deb = def_groupe[num].premier_ele;

        i = find_input_link(num, 0);
        if (i == -1)
        {
            printf
                ("f_pos_offset (%d): ERROR must have a link \n",num);
            exit(0);
        }

        ret=sscanf(liaison[i].nom, "x%d,y%d", &x, &y);
	if(ret!=2) {
	  EXIT_ON_ERROR("In (%s), string on input link doesn t match format : x_,y_\n",def_groupe[num].no_name);
	}

	dprints("x %d, y %d\n",x,y);



	mydata->gpe_entree=liaison[i].depart;
	mydata->gpe_entree_deb=def_groupe[mydata->gpe_entree].premier_ele;
	mydata->x_offset = x;
	mydata->y_offset = y;
	
	def_groupe[num].data = mydata;
   }
   else {
     mydata = def_groupe[num].data;
   }
   
   x = mydata->x_offset;
   y = mydata->y_offset;

#ifdef DEBUG
   printf("x %d, y %d\n", x, y);
#endif

   deb = mydata->gpe_deb;
   entree_deb = mydata->gpe_entree_deb;
   nbre = def_groupe[mydata->gpe_entree].nbre;
   nbre2 = def_groupe[mydata->gpe_entree].taillex * def_groupe[mydata->gpe_entree].tailley;

   taillex = def_groupe[mydata->gpe_entree].taillex;
   tailley =def_groupe[mydata->gpe_entree].tailley;

   /* controle d'erreur de taille */
   if(nbre != def_groupe[num].nbre || nbre2!=def_groupe[num].taillex * def_groupe[num].tailley) {
     cprints("f_pos_offset (%d) : ERROR lengths doesn't match!\n",num);
     exit(1);
   }

   increment = nbre / nbre2; 
   
 
   for(i=0; i<tailley*increment; i=i+increment)
     for(j=0; j<taillex*increment; j=j+increment)	
       for(k=0; k<increment; k++) {
	 index = deb +j +i*taillex*increment +k;
	 index_entree = entree_deb+((j+x)%taillex)+((i+y)%tailley)*taillex*increment+k;

/*  	 dprints("index %d, index entree %d\n",index, index_entree); */

	 neurone[index].s2 =  neurone[index_entree].s2;
	 neurone[index].s1 =  neurone[index_entree].s1;
	 neurone[index].s = neurone[index_entree].s;
       }

   dprints("sortie de f_pos_offset\n");
   
}



