/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\defgroup  f_concatenation f_concatenation
\ingroup libNN_IO

\section Description

Cette fonction concatene les neurones de plusieurs groupes en
recopiant pour chacun les trois sorties (s, s1 et s2).  Elle met les
neurones d'un groupe d'entree sur un groupe de sortie de taille
differente.  Les neurones sont concatenes dans l'ordre correspondant
au numero du lien connectant le groupe d'entree au
f_concatenation. Les valeurs peuvent aller de 0 a 99.

Ajouter '-image' sur un lien permet de concatener les images presentes
sur le .ext dans le .ext de f_concatenation.  Les images doivent
toutes avoir la meme taille et avoir le meme nombre de bandes. Un
groupe d'entree peut avoir plusieurs images. Elles seront toutes
recuperees.

Le groupe gere dynamiquement les images. Si au demarrage les images ne
sont pas encore toutes recues, le groupe reserve la place pour une
image (on peut demander de reserver plus de place en passant en
parametre le nombre d'image apres -image). Lorsque l'image sera alors
recue, le systeme la prendra en compte dans la mise a jour du .ext. Si
le nombre d'image effectivement recu differe du nombre prevu, le
groupe rejette la situation et fait un EXIT_ON_ERROR.

NB: toutes les images doivent respecter : meme nombre de pixel (sx,
sy) et meme nombre de bandes (1 ou 3).

NB: Si aucune des images a transferer n'a ete recue a l'init du
groupe, l'init du .ext est reporte a la reception de la premiere image
(.ext reste NULL en attendant). C'est elle qui determinera le nombre
de bande et les dimensions (sx,sy). D'autre part, un changement du
nombre d'image par rapport a ce qui est annonce est tolere tant que le
.ext n'a pas encore ete alloue.

NB: Si le .ext a ete alloue, alors tant que l'image reelle n'a pas ete
recue tous les pixels correspondant dans la memoire reservee seront a
0 (noir).

\section TODO

On pourrait ameliorer la gestion du nombre d'image a reserver en
choisissant s'il faut etre toujours stricte ou pas.


\file
\ingroup f_concatenation
\brief concatenation de neurones et d'images

Author: Mickael Maillard
Created: 09/07/04
Modified:
- author: Maillard
- description: specific file creation
- date: 23/08/2004
Modified:
- author: Rengerve
- description: can concatenate images
- date: 19/03/2012

************************************************************/

/* #define DEBUG */

#include <net_message_debug_dist.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

struct data_concatenation
{
   int Ninput;
   /**
      table content:
      0 - premier_ele Neurons
      1 - nbre of Neurons
      2 - position of first image in ext
      3 - nb images in ext
      4 - gpe_index if ext
   **/
   int input_Gpe_table[100][5];
   void *input_Gpe_ext[100];
   int this_deb;
   int n_images;
   int n_max_images;
};


void function_concatenation(int Gpe)
{
   int i, j;
   int inputGpe = -1;
   char param[255];
   int this_deb, this_nbre, Ninput = 0;
   /*  int **p_input_Gpe_table_size = NULL;*/
   struct data_concatenation *this_data = NULL;
   int deb_prev_gpe, nbre_prev_gpe, indice;
   int idx,ret;
   prom_images_struct *p_images_ext, *p_images_dest;
   unsigned char *pt;
   int sx=-1, sy=-1;
   int nbImages=0;
   int nb_band=-1;

   /* initialisation */
   dprints("groupe %s : %s\n", def_groupe[Gpe].no_name, __FUNCTION__);

   if (def_groupe[Gpe].data == NULL)
   {
      j = 0;
      def_groupe[Gpe].data = malloc(sizeof(struct data_concatenation));
      if (def_groupe[Gpe].data == NULL)
      {
         EXIT_ON_ERROR("Gpe %s (f_concatenation) : Impossible d'allouer de la memoire\n", def_groupe[Gpe].no_name);
      }
      this_data = (struct data_concatenation *) def_groupe[Gpe].data;
      for (i = 0; i < 100; i++)
      {
         this_data->input_Gpe_table[i][0] = -1;
         this_data->input_Gpe_table[i][1] = -1;
         this_data->input_Gpe_table[i][2] = -1;
         this_data->input_Gpe_table[i][3] = -1;
         this_data->input_Gpe_table[i][4] = -1;
      }
      this_data->n_images=0;
      this_nbre = def_groupe[Gpe].nbre;
      this_deb = def_groupe[Gpe].premier_ele;
      Ninput = 0;
      idx = 0;
      for (i = 0; i < nbre_liaison; i++)
      {
         if (liaison[i].arrivee == Gpe)
         {
            inputGpe = liaison[i].depart;
            dprints("groupe entrant dans f_concatenation%s %d %d \n", def_groupe[inputGpe].no_name, def_groupe[inputGpe].premier_ele, def_groupe[inputGpe].nbre);
            idx = atoi(liaison[i].nom);
            if (idx>=100) EXIT_ON_ERROR("F_concatenation : (%s) : Input link with index %d rejected : 99 is the max value (see code).\n",def_groupe[Gpe].no_name,idx);

            this_data->input_Gpe_table[idx][0] = def_groupe[inputGpe].premier_ele;
            this_data->input_Gpe_table[idx][1] = def_groupe[inputGpe].nbre;

            ret = prom_getopt(liaison[i].nom,"-image",param);
            if (ret>0)
            {
               if (ret==2)
               {
                  ret= sscanf(param,"%d",&(this_data->input_Gpe_table[idx][3]));
                  if (ret!=1) EXIT_ON_ERROR("Param of -image is not a readable integer !\n");
               }
               else
               {
                  this_data->input_Gpe_table[idx][3]=-2; /* no param */
               }
               this_data->input_Gpe_ext[idx] = def_groupe[inputGpe].ext;
               p_images_ext = (prom_images_struct *)this_data->input_Gpe_ext[idx];
               if (p_images_ext==NULL)
               {
                  PRINT_WARNING("F_concatenation : (%s) : Pas de ext sur le groupe precedent (%s)",def_groupe[Gpe].no_name,def_groupe[inputGpe].no_name);
                  this_data->input_Gpe_table[idx][2] = -2; /*flag wait for image*/
                  if (this_data->input_Gpe_table[idx][3]==-2) this_data->input_Gpe_table[idx][3]=1; /* 1 is default */
                  this_data->input_Gpe_table[idx][4] = inputGpe;
                  nbImages+=this_data->input_Gpe_table[idx][3];
               }
               else
               {
                  if (sx==-1)
                     sx=p_images_ext->sx;
                  if (sy==-1)
                     sy=p_images_ext->sy;
                  if (nb_band==-1)
                     nb_band=p_images_ext->nb_band;
                  if (p_images_ext->sx != sx) {EXIT_ON_ERROR("All input images does not have the same size (sx = %d/%d)\n",p_images_ext->sx,sx);}
                  if ( p_images_ext->sy != sy) {EXIT_ON_ERROR("All input images does not have the same size (sy = %d/%d)\n",p_images_ext->sy,sy);}
                  if ( p_images_ext->nb_band != nb_band) {EXIT_ON_ERROR("All input images does not have the same nb_band (nb_band = %d/%d)\n",p_images_ext->nb_band,nb_band);}
                  this_data->input_Gpe_table[idx][2] = nbImages; /* flag image is present */
                  if (this_data->input_Gpe_table[idx][3]!=-2 && this_data->input_Gpe_table[idx][3]!=p_images_ext->image_number)
                     PRINT_WARNING("F_concatenation : (%s)\n At init: Announced number of images is different from the real number of input image. Take real number of images : %d\n",def_groupe[Gpe].no_name, p_images_ext->image_number);
                  this_data->input_Gpe_table[idx][3] = p_images_ext->image_number;
                  this_data->input_Gpe_table[idx][4] = inputGpe;
                  nbImages+=p_images_ext->image_number;
                  this_data->n_images +=p_images_ext->image_number;
               }

            }
            j += def_groupe[inputGpe].nbre;
            Ninput++;
         }
      }
      this_data->Ninput = Ninput;
      this_data->this_deb = this_deb;
      this_data->n_max_images = nbImages;
      if (j != this_nbre)
      {
         EXIT_ON_ERROR("Erreur dans la taille des groupes gpe %s (%d != %d)\n", def_groupe[Gpe].no_name, j, this_nbre);
      }

      /*    p_images_ext = (prom_images_struct *)this_data->input_Gpe_ext[0];*/

      /* check if size and band was initialized */
      if (sx!=-1)
      {
         def_groupe[Gpe].ext = (void *) calloc_prom_image(nbImages, sx, sy,nb_band);
         dprints("F_concatenation : (%s) : Init this .ext with NbImages : %d\n", def_groupe[Gpe].no_name, nbImages);
      }


   }
   else
   {
      this_data = (struct data_concatenation *) def_groupe[Gpe].data;
      /* p_input_Gpe_table_size = this_data->input_Gpe_table_size;*/
      Ninput = this_data->Ninput;
      this_deb = this_data->this_deb;
   }


   p_images_dest = (prom_images_struct *) def_groupe[Gpe].ext;
   if (p_images_dest != NULL) dprints("p_images_dest : %d %d %d %d\n", p_images_dest->image_number, p_images_dest->sx,p_images_dest->sy, p_images_dest->nb_band);

   indice = this_deb;
   nbImages=0;
   for (i = 0; i < 100; i++)
   {
      /* parcours pas optimal */
      if (this_data->input_Gpe_table[i][0] == -1) continue;
      deb_prev_gpe = this_data->input_Gpe_table[i][0];
      nbre_prev_gpe = this_data->input_Gpe_table[i][1];
      for (j = deb_prev_gpe; j < (deb_prev_gpe + nbre_prev_gpe); j++)
      {
         neurone[indice].s2 = neurone[j].s2;
         neurone[indice].s1 = neurone[j].s1;
         neurone[indice].s = neurone[j].s;
         indice++;
      }

      nbImages = this_data->input_Gpe_table[i][2];
      if (nbImages != -1)
      {
         if (nbImages != -2)
         {
            /* cpy of image */
            dprints(" >> Recuperation image from link %d\n", i);
            p_images_ext = (prom_images_struct *)this_data->input_Gpe_ext[i];
            for (j=0; j<p_images_ext->image_number; j++)
            {
               dprints(" copie image local %d dans image globale %d\n", j, nbImages+j);
               pt = p_images_dest->images_table[nbImages+j];
               memcpy(pt, p_images_ext->images_table[j], p_images_ext->sx * p_images_ext->sy * p_images_ext->nb_band);
            }
         }
         else
         {
            /* check if input ext is now defined */
            p_images_ext = (prom_images_struct *)def_groupe[this_data->input_Gpe_table[i][4]].ext;
            if (p_images_ext==NULL)
            {
               cprints(" f_concatenation : (%s) : No ext coming from input link %d\n",def_groupe[Gpe].no_name,i);
            }
            else
            {
               /* check if this .ext was initialized correctly */
               if (p_images_dest!=NULL)
               {
                  if ((int)p_images_dest->sx!= -1)
                  {
                     /* p_images_dest was already initialized */
                     if (p_images_ext->sx != p_images_dest->sx)
                     {
                        EXIT_ON_ERROR("All input images does not have the same size (sx = %d/%d)\n",p_images_ext->sx,p_images_dest->sx);
                     }
                     if ( p_images_ext->sy != p_images_dest->sy)
                     {
                        EXIT_ON_ERROR("All input images does not have the same size (sy = %d/%d)\n",p_images_ext->sy,p_images_dest->sy);
                     }
                     if ( p_images_ext->nb_band != p_images_dest->nb_band)
                     {
                        EXIT_ON_ERROR("All input images does not have the same nb_band (nb_band = %d/%d)\n",p_images_ext->nb_band, p_images_dest->nb_band);
                     }
                  }
                  if ((this_data->input_Gpe_table[i][3]) != p_images_ext->image_number)
                     EXIT_ON_ERROR("Number of images from link %d does not match what was announced (%d vs %d)\n",i,p_images_ext->image_number,this_data->input_Gpe_table[i][3]);
               }
               else
               {
                  if ((this_data->input_Gpe_table[i][3]) != p_images_ext->image_number)
                  {
                     PRINT_WARNING("Number of images from link %d does not match what was expected (%d vs %d)\n Take real number of images : %d\n",i,p_images_ext->image_number,this_data->input_Gpe_table[i][3],p_images_ext->image_number);
                     this_data->n_max_images = this_data->n_max_images - this_data->input_Gpe_table[i][3] + p_images_ext->image_number;
                  }

                  def_groupe[Gpe].ext = (void *) calloc_prom_image(this_data->n_max_images, p_images_ext->sx, p_images_ext->sy,p_images_ext->nb_band);
                  if (def_groupe[Gpe].ext==NULL)
                  {
                     dprints("calloc failed in f_concatenation\n");
                  }
                  p_images_dest=(prom_images_struct *)def_groupe[Gpe].ext;
                  dprints("F_concatenation : (%s) : Init this .ext with NbImages : %d and dim (%d,%d) and bands : %d\n", def_groupe[Gpe].no_name, p_images_dest->image_number, p_images_dest->sx, p_images_dest->sy, p_images_dest->nb_band);
               }

               this_data->input_Gpe_table[i][2] = this_data->n_images; /* flag image is present */
               this_data->input_Gpe_ext[i] = def_groupe[this_data->input_Gpe_table[i][4]].ext;
               this_data->n_images+=p_images_ext->image_number;

               /* cpy of image */
               dprints(" >> Recuperation image from link %d\n", i);
               p_images_ext = (prom_images_struct *)this_data->input_Gpe_ext[i];
               nbImages = this_data->input_Gpe_table[i][2];
               for (j=0; j<p_images_ext->image_number; j++)
               {
                  dprints(" copie image local %d dans image globale %d\n", j, nbImages+j);
                  pt = p_images_dest->images_table[nbImages+j];
                  memcpy(pt, p_images_ext->images_table[j], p_images_ext->sx * p_images_ext->sy * p_images_ext->nb_band);
               }

            }
         }
      }
      Ninput--;
      if (Ninput <= 0)
         break;
   }

   dprints("fin Gpe %s : %s\n", def_groupe[Gpe].no_name, __FUNCTION__);
   return;
}
