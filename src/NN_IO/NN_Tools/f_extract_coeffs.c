/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_extract_coeffs.c
\brief

Author: G. Merle
Created: 28/06/2011
Modified:
- author: xxxxxxxx
- description: xxxxxxxx
- date: xx/xx/xxxx

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Cette fonction extrait d'un groupe de neurone, les valeurs des coeffs d'un de ses neurones. Ce neurone a pour ordonnee et abscisse definis sur le lien par -x"abscisse" et -y"ordonnee". Rque x et y valent par defaut 0.
Comme f_extract_neurons mais extrait les coeffs d'un neurone et non les neurones eux même.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef float (*key_func_t)(type_coeff*);

static float key_val(type_coeff* coeff) {
    return coeff->val;
}
static float key_proba(type_coeff* coeff) {
    return coeff->proba;
}

typedef struct MyData_extract_coeffs {
    int neuron;
    key_func_t key;
} MyData_extract_coeffs;


void new_extract_coeffs(int gpe) {

    int in;
    int l;
    int x = 0, y = 0;
    int increment;
    key_func_t key = key_val;

    MyData_extract_coeffs* data = NULL;

    char chaine[255];

    if (def_groupe[gpe].data == NULL) {
        /* get the input link */
        l = find_input_link(gpe, 0);
        if (l == -1) {
            PRINT_WARNING("gpe #%s '%s': the group should have at least one input link!\n", 
                def_groupe[gpe].no_name, def_groupe[gpe].nom);
            in = -1;
        } else {
            in = liaison[l].depart;
            if (prom_getopt(liaison[l].nom, "x", chaine) == 2) {
                x = atoi(chaine);
            }
            if (prom_getopt(liaison[l].nom, "y", chaine) == 2) {
                y = atoi(chaine);
            }
            if (prom_getopt(liaison[l].nom, "k", chaine) == 2) {
                switch (chaine[0]) {
                    case 'v':
                    key = key_val;
                    break;
                    case 'p':
                    key = key_proba;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid argument for option -k, expected 'v' or 'p', got '%s'\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
            }
            
            if (x >= def_groupe[in].taillex || y >= def_groupe[in].tailley) {
                PRINT_WARNING("gpe #%s '%s': (x,y) out of bounds\n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
                in = -1;
            }
        }
        
        def_groupe[gpe].data = data = malloc(sizeof(MyData_extract_coeffs));
        if (data == NULL) {
            EXIT_ON_ERROR("gpe #%s '%s': malloc failed\n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
        }
        
        if (in != -1) {
            increment = def_groupe[in].nbre / (def_groupe[in].taillex * def_groupe[in].tailley);
            
            data->neuron = def_groupe[in].premier_ele +
                increment * (y * def_groupe[in].taillex +x +1) -1;
        } else {
            data->neuron = in;
        }
        data->key = key;
    }
}

void destroy_extract_coeffs(int gpe) {
    if (def_groupe[gpe].data != NULL) {
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

void function_extract_coeffs(int gpe) {
    MyData_extract_coeffs* data = def_groupe[gpe].data;
    int in = data->neuron;
    int i;
    int n = def_groupe[gpe].nbre;
    int deb = def_groupe[gpe].premier_ele;
    type_coeff* coeff;
    if (in == -1) return;
    for (i = 0, coeff = neurone[in].coeff; i < n && coeff != NULL; ++i, coeff = coeff->s) {
        neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = data->key(coeff);
    }
}

