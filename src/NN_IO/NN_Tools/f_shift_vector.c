/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_shift_vector.c
\brief 

Author: LAGARDE Matthieu
Created: 10/11/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG 1

typedef	struct		s_shift_vector
{
  int			prev_first_dec;

  int			prev_first_vec;
  int			prev_nbre_vec;

  int			first;
  int			nbre;
  
  char			axe;
}			t_shift_vector;

void		f_shift_vector(int numero)
{
  t_shift_vector	*data = NULL;
  int			prev_first_dec;

  int			prev_first_vec;
  int			prev_nbre_vec;

  int			first;
  int			nbre;

  char			axe = '\0';

  int			link = -1;
  int			idx = -1;
  int			i = -1;

#ifdef DEBUG
  fprintf(stdout, "f_shift_vector : entree\n");
#endif /* DEBUG */

  if (def_groupe[numero].ext == NULL)
    {
      if ((data = malloc(sizeof(t_shift_vector))) == NULL)
	{
	  fprintf(stderr, "f_shift_vector : Erreur d'allocation memoire\n");
	  perror("malloc");
	  exit(-1);
	}
      data->first = def_groupe[numero].premier_ele;
      data->nbre = def_groupe[numero].nbre;

      if (def_groupe[numero].taillex > 1)
	data->axe = 'X';
      else if (def_groupe[numero].tailley > 1)
	data->axe = 'Y';

      for (i = 0; (link = find_input_link(numero, i)) >= 0; i++)
	{
	  if (!strcmp(liaison[link].nom, "dec"))
	    {
	      data->prev_first_dec = def_groupe[liaison[link].depart].premier_ele;
	    }
	  if (!strcmp(liaison[link].nom, "vec"))
	    {
	      data->prev_first_vec = def_groupe[liaison[link].depart].premier_ele;
	      data->prev_nbre_vec = def_groupe[liaison[link].depart].nbre;
	    }
	}

      def_groupe[numero].ext = (void *)data;
    }
  else
    data = (t_shift_vector *)def_groupe[numero].ext;

  first = data->first;
  nbre = data->nbre;
  prev_first_dec = data->prev_first_dec;
  prev_first_vec = data->prev_first_vec;
  prev_nbre_vec = data->prev_nbre_vec;
  axe = data->axe;

  for (i = 0; i < nbre; i++)
    neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0;

  /* On recherche l indice du neurone active sur le groupe precedent */
  for (i = 0; i < prev_nbre_vec; i++)
    {
      if (neurone[prev_first_vec + i].s2 > 0)
	break;
    }
  if (i < prev_nbre_vec)
    switch (axe)
      {
      case 'X' :
	idx = i + neurone[prev_first_dec].s2;
	break;
      case 'Y' :
	idx = i - neurone[prev_first_dec].s2;
	break;
      default :
	fprintf(stderr, "f_shift_vector : le groupe doit etre un vecteur horizontal OU vertical\n");
	exit (-1);
      }
  
  if (idx >= nbre)
    idx = nbre - 1;
  else if (idx < 0)
    idx = 0;
    
  neurone[first + idx].s = neurone[first + idx].s1 = neurone[first + idx].s2 = 1;

#ifdef DEBUG
  fprintf(stdout, "f_shift_vector : sortie\n");
#endif /* DEBUG */
}
