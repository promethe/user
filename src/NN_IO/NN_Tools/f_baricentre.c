/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_baricentre.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Fonction VB  :   Met sur le neurone de la boite le max de l'activite  
  analogique ( .s) de la boite precedante.				 
  Cette fonction est utilise dans la planification pour recuperer	 
  l'intensite de la reconnaissance du lieux le mieux reconnu.	

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Kernel_Function/trouver_entree.h>
#include <stdlib.h>

void function_baricentre(int gpe_sortie)
{
   int gpe_entree, debe, nbree, i, nbre = def_groupe[gpe_sortie].nbre;
   int ind_max = -1;
   float som, som_act;

   gpe_entree = trouver_entree(gpe_sortie, (char*)"input");
   debe = def_groupe[gpe_entree].premier_ele;
   nbree = def_groupe[gpe_entree].nbre;
   if (nbre != nbree)
   {
      printf("Error on group norm_ana(%d): group has different size\n", gpe_sortie);
      exit(-1);
   }
   /*recherche du max pour la normalisation */

   som = 0.;
   som_act = 0.;

   for (i = 0; i < nbree; i++)
   {
      som_act = som_act + neurone[i + debe].s1;
      som = som + neurone[i + debe].s1 * i;

   }

   ind_max = som / som_act;

   for (i = def_groupe[gpe_sortie].premier_ele; i < def_groupe[gpe_sortie].premier_ele + nbree; i++)
   {
      neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.;
   }
   neurone[def_groupe[gpe_sortie].premier_ele + ind_max].s = neurone[def_groupe[gpe_sortie].premier_ele + ind_max].s1 = neurone[def_groupe[gpe_sortie].premier_ele + ind_max].s2 = 1.;

}
