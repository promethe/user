/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_extract_neurons f_extract_neurons
\ingroup libNN_IO

\brief Extraction d'un groupe de neurones dans un groupe d'entree

\details

\section Description

Cette fonction extrait d'un groupe de neurone, un sous groupe de taille "taille du groupe qui appelle la fonction". Ce sous groupe a pour coin superieur gauche le neurone dont ordonnee et abscisse sont definis sur le lien par -x"abscisse de depart" et -y"ordonnee de depart". 

\section Option

-x%d-y%d 

Rque x et y valent par default 0.

\section Deprecated option

La notation -d"numero de depart" n'est plus acceptee et donc renvoie un message d'erreur explicatif.


\file  f_extract_neurons.c
\ingroup f_extract_neurons
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Cette fonction extrait d'un groupe de neurone, un sous groupe de taille "taille du groupe qui appelle la fonction". Ce sous groupe a pour coin superieur gauche le neurone dont ordonnee et abscisse sont definis sur le lien par -x"abscisse de depart" et -y"ordonnee de depart". Rque x et y valent par default 0.
La notation -d"numero de depart" n'est plus acceptee et donc renvoie un message d'erreur explicatif.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
/*#define TIME_TRACE*/

#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <Global_Var/Vision.h>
#ifdef TIME_TRACE
#include <Global_Var/SigProc.h>
#endif
#include <Struct/prom_images_struct.h>
#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_extract_neurons
{
	int GpeIn;
	int increment;
	int abscisse_depart;
	int ordonnee_depart;
} MyData_f_extract_neurons;


void function_extract_neurons(int Gpe)
{

	int GpeIn = -1, increment = 1;

	int l = -1, i = 0, j = 0, abscisse_depart = 0, ordonnee_depart = 0;
	int p, q;

	MyData_f_extract_neurons *my_data;

	char param[10];

#ifdef DEBUG
	printf("entree function_extract_neurons\n");
#endif
	if (def_groupe[Gpe].data == NULL)
	{

		l = find_input_link(Gpe, i);
		if (l == -1)
		{
			printf ("Erreur de lien entrant Gpe %s in function_extract_neurons\n", def_groupe[Gpe].no_name);
			exit(1);
		}
		while (l != -1)
		{
			if (prom_getopt(liaison[l].nom, "d", param) == 2) EXIT_ON_ERROR("**Erreur dans la fonction f_extract_neurons (groupe numero %d):\n"
					"l'option -d sur le lien en entree n'est plus acceptee. \n "
					"Elle doit etre remplacee par une abscisse de depart -x et une ordonnee de depart -y\n"
					"(cf src/NN_IO/NN_Tools/f_extract_neurons.c)\n", Gpe);

			if (prom_getopt(liaison[l].nom, "x", param) == 2)
			{
				abscisse_depart = atoi(param);
				GpeIn = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "y", param) == 2)
			{
				ordonnee_depart = atoi(param);
				GpeIn = liaison[l].depart;
			}
			i++;
			l = find_input_link(Gpe, i);
		}

		increment =	def_groupe[GpeIn].nbre / (def_groupe[GpeIn].taillex * def_groupe[GpeIn].tailley);


		dprints("abscisse_depart=%d, ordonnee_depart=%d, GpeIN= %d\n", abscisse_depart, ordonnee_depart, GpeIn);


		/** test de la taille et la position de la matrice a extraire
		 * vs la taille du groupe d'entree */
		if(abscisse_depart<0 || ordonnee_depart<0)
			EXIT_ON_ERROR("In groupe (%s): Position du premier neurone (x%d, y%d) est hors du groupe d'entree. Il faut x dans [0, %d] et y dans [0, %d].\n",def_groupe[Gpe].no_name,abscisse_depart,ordonnee_depart,def_groupe[GpeIn].taillex,def_groupe[GpeIn].tailley);
		if(abscisse_depart+def_groupe[Gpe].taillex>def_groupe[GpeIn].taillex || ordonnee_depart+def_groupe[Gpe].tailley > def_groupe[GpeIn].tailley )
			EXIT_ON_ERROR("In groupe (%s): Position de l'entree du dernier neurone (%d,%d) est hors du groupe d'entree. La position maximale est [%d,%d].\n",def_groupe[Gpe].no_name,abscisse_depart+def_groupe[Gpe].taillex, ordonnee_depart+def_groupe[Gpe].tailley,def_groupe[GpeIn].taillex,def_groupe[GpeIn].tailley);


		my_data = malloc(sizeof(MyData_f_extract_neurons));
		if (my_data == NULL)
			EXIT_ON_ERROR("erreur d'allocation memoire dans f_extract_neurons gpe %s\n",def_groupe[Gpe].no_name);

		my_data->GpeIn = GpeIn;
		my_data->increment = increment;
		my_data->abscisse_depart = abscisse_depart;
		my_data->ordonnee_depart = ordonnee_depart;
		def_groupe[Gpe].data = (MyData_f_extract_neurons *) my_data;

	}

	else
	{
		my_data = (MyData_f_extract_neurons *) def_groupe[Gpe].data;
		increment = my_data->increment;
		abscisse_depart = my_data->abscisse_depart;
		ordonnee_depart = my_data->ordonnee_depart;
		GpeIn = my_data->GpeIn;

	}

	dprints("debut code effectif\n");

	for (i = 0; i < def_groupe[Gpe].taillex; i++)
	{
		for (j = 0; j < def_groupe[Gpe].tailley; j++)
		{
			p = def_groupe[Gpe].premier_ele + i + j * def_groupe[Gpe].taillex;
			q = def_groupe[GpeIn].premier_ele + increment - 1 + increment * (ordonnee_depart + j) * (def_groupe[GpeIn].taillex) + increment * (abscisse_depart + i);
			neurone[p].s = neurone[q].s;
			neurone[p].s1 = neurone[q].s1;
			neurone[p].s2 = neurone[q].s2;
		}
	}


#ifdef TIME_TRACE
	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
	if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	else
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
				1;
		MicroSecondesFunctionTimeTrace =
				1000000 + OutputFunctionTimeTrace.tv_usec -
				InputFunctionTimeTrace.tv_usec;
	}
	printf("Time in function_acquisition\t%4ld.%06d\n",
			SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
	/* printf("%s\n", MessageFunctionTimeTrace);*/
#endif



#ifdef DEBUG
	printf("fin function_extract_neurons\n");
#endif

}
