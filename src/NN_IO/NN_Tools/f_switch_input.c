/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_switch_input f_switch_input
   \ingroup libNN_IO

   \brief Cette fonction permet de bloquer ou laisser passer le ou les groupes en entree, en fonction d'un flag externe.

   \details

   Cette fonction permet de bloquer ou laisser passer le ou les groupes en entree, en fonction d'un
   flag externe. En un sens, cela fonctionne comme un groupe inhibiteur a la difference qu'il est
   possible d'avoir des exceptions au bloquage des boites.

   *Elle necessite au moins un groupe en entree : un f_demande_numero, relie au switch_input
   par un lien algo dont le nom est "switch". Suivant la valeur du neurone de ce f_demande_numero,
   certaines boites passeront et d'autres non.

   *Les boites dont on souhaite filtrer le passage sont egalement reliees au switch_input par
   un lien algo, dont voici les parametres :

   -activ{0,1} : c'est un parametre obligatoire. Le switch_input laissera passer la boite si le
   paramere activ est de meme valeur que le premier neurone du f_demande_numero. Par exemple, 
   -activ1 signifie que le groupe passera si le f_demande_numero est a 1.

   -except{n1,n2,n3...} : le groupe switch_input maintient un compteur incremente a chaque
   passage. La liste de valeur faisant reference a cette variable, un groupe d'entree dont le lien
   porte l'option "-activ1 -except0,2,3" sera actif quand le f_demande_numero sera a 1, et aussi aux
   iterations 0,2,3 quand le f_demande_numero est a 0.

   -listexcept\<nom_fichier\> : meme chose que ci dessus, mais la liste des exceptions est donnee dans 
   un fichier. Le fichier est constitue de une exception par ligne.
	
   Il est possible d'emuler ces options grace a une bonne utilisation du f_demande_numero,
   cette option n'est la que pour simplifier la vie dans certains cas.

   \file
   \ingroup f_switch_input

*/

#include <stdlib.h>
#include <stdio.h>
#include <libx.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define LIST_MAX 	100




typedef struct
{
  int neurone_de_switch;
  int compteur;
  int link0except[LIST_MAX];
  int link1except[LIST_MAX];
  int for_learn0;
  int for_learn1;
} switch_input_datas;




void function_switch_input(int gpe)
{
  int debut, lino, j, res;
  int gpepred, debutpred, nbpred1, nbpred2, incr;
  int i, count;               /*attention : sert a plusieurs choses */
  int *arraypointer;
  FILE *exceptfic;
  int reslecture, exceptnum;
  int flag_modul, flag_entree;

  char retour[100];
  char *parsebuffer;
  switch_input_datas *mydatas;

  if (def_groupe[gpe].data == NULL)
  {

    mydatas = (switch_input_datas *) malloc(sizeof(switch_input_datas));

    /*init */
    for (i = 0; i < LIST_MAX; i++)
    {
      mydatas->link0except[i] = mydatas->link1except[i] = -2;
    }
    mydatas->for_learn0 = -1;
    mydatas->for_learn1 = -1;


    j = 0;
    flag_modul = flag_entree = 0;
    do
    {

      /*recherche de la valeur au moment ou le lien est actif */
      lino = find_input_link(gpe, j);


      /*on cherche s'il s'agit d'un lien d'entree */
      res = prom_getopt(liaison[lino].nom, "activ", retour);
      if (res != 0)
      {

	/*si on entre ici, cette boite est une boite d entree */
	flag_entree = 1;
	i = atoi(retour);
	if (i != 0 && i != 1)
	{
	  printf
	    ("link between %d : %s box and the %d switch_input box must have a parameter \"-activ[0-1]\"\n",
	     liaison[lino].depart,
	     def_groupe[liaison[lino].depart].nom, gpe);
	  exit(0);
	}

	if (i == 0)
	  mydatas->for_learn0 = j;    /*sauvegarde du numero du lien */
	else
	  mydatas->for_learn1 = j;

	/*recherche des exceptions a l'execution */
	res = prom_getopt(liaison[lino].nom, "except", retour);
	if (res != 0)
	{

	  /*il faut parser pour obtenir cette liste */
	  if (i == 0)
	    arraypointer = mydatas->link0except;
	  else
	    arraypointer = mydatas->link1except;

	  i = 0;
	  parsebuffer = strtok(retour, ",");
	  if (parsebuffer != NULL)
	    arraypointer[i++] = atoi(parsebuffer);
	  do
	  {
	    parsebuffer = strtok(NULL, ",");
	    if (parsebuffer != NULL)
	    {
	      arraypointer[i++] = atoi(parsebuffer);
	      if (i == LIST_MAX)
	      {
		printf
		  ("too much exceptions for the link from %d\n",
		   liaison[lino].depart);
		exit(0);
	      }
	    }
	  }
	  while (parsebuffer != NULL);
	}
	else
	{
	  res =
	    prom_getopt(liaison[lino].nom, "listexcept", retour);
	  if (res != 0)
	  {
	    exceptfic = fopen(retour, "r");
	    if (exceptfic == NULL)
	    {
	      printf("switch %d : le fichier %s n'existe pas\n",
		     gpe, retour);
	      j++;
	      continue;
	    }
	    if (i == 0)
	      arraypointer = mydatas->link0except;
	    else
	      arraypointer = mydatas->link1except;
	    i = 0;
	    do
	    {
	      reslecture =
		fscanf(exceptfic, "%d\n", &exceptnum);
	      if (reslecture == EOF)
		break;
	      arraypointer[i++] = exceptnum;
	    }
	    while (reslecture != EOF);
	    fclose(exceptfic);
	  }
	  else
	  {
	    j++;
	    continue;
	  }
	}
      }
      else
      {
	/*il s'agit peut etre alors du lien switch */
	if (strcmp(liaison[lino].nom, "switch") == 0)
	{
	  /*bingo : on va sauvegarder le numero du neurone du f_demande_numero */
	  flag_modul = 1;
	  mydatas->neurone_de_switch =
	    def_groupe[liaison[lino].depart].premier_ele;

	}
      }

      j++;

    }
    while (lino != -1);

    /*tests : manque-t-il une boite ? */
    if (flag_entree == 0 || flag_modul == 0)
    {
      printf("the switch box %d must have at least two input boxes :\n",
	     gpe);
      printf
	("\tone normal input box, and the link must be named <-activ{0,1}>[-except{e1,e2....}] \n");
      printf
	("\tanother one whose first neuron will control the switch (like f_demande_numero, or anything else), and the link must be named <switch>\n");
      exit(0);
    }

    mydatas->compteur = 0;
    def_groupe[gpe].data = mydatas;

  }
  else
    mydatas = def_groupe[gpe].data;

  /*maintenant, activer la couche de neurone avec la bonne entree */
  /*il faut reperer quelle va etre la liaison qui va s'executer, en fonction du neurone switch */
  /*on va regarder si l'autre propose une exception, si c'est le cas elle s'executera a la place de l'autre */
  /*si la liaison n'existe pas et qu'il n'y a pas d'exception, la couche sera a 0 */

  if (isequal(neurone[mydatas->neurone_de_switch].s, 0.))
  {
    j = mydatas->for_learn0;    /*affectation du lien a utiliser */

    for (i = 0; i < LIST_MAX; i++)
    {
      if (mydatas->link1except[i] == mydatas->compteur)
      {
	j = mydatas->for_learn1;
	break;
      }                   /*une des exceptions s'applique ici */
      if (mydatas->link1except[i] == -2)
	break;          /*plus d'exceptions */
    }
  }
  else
  {
    j = mydatas->for_learn1;    /*affectation du lien a utiliser */

    for (i = 0; i < LIST_MAX; i++)
    {
      if (mydatas->link0except[i] == mydatas->compteur)
      {
	j = mydatas->for_learn0;
	break;
      }                   /*une des exceptions s'applique ici */
      if (mydatas->link0except[i] == -2)
	break;          /*plus d'exceptions a regarder */
    }
  }
  if (j == -1)
  {
    debut = def_groupe[gpe].premier_ele;
    nbpred1 = def_groupe[gpe].nbre;
    for (i = debut; i < debut + nbpred1; i++)
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    mydatas->compteur++;
    return;                 /*aucune liaison n'est a effectuer ici */
  }


  /*bon, il ne reste plus qu'a affecter les activites aux neurones */
  debut = def_groupe[gpe].premier_ele;
  lino = find_input_link(gpe, j);
  gpepred = liaison[lino].depart;
  debutpred = def_groupe[gpepred].premier_ele;
  nbpred1 = def_groupe[gpepred].nbre;
  nbpred2 = def_groupe[gpepred].taillex * def_groupe[gpepred].tailley;
  incr = nbpred1 / nbpred2;

  count = debut;
  for (i = debutpred + incr - 1; i < debutpred + nbpred1; i = i + incr)
  {
    neurone[count].s = neurone[i].s;
    neurone[count].s1 = neurone[i].s1;
    neurone[count].s2 = neurone[i].s2;
    count++;
  }


  mydatas->compteur++;

}
