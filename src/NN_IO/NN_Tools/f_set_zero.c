/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_set_zero.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction demandant un angle entre 0 et 360� et active le neurone correspondant.
Codage de l'angle par une poupulation de neurone avec mise a l'echelle. Le nombre de neurone repesente l'inverse du pas de discretisation. on peut specifie la plage code en donnant sur le lien de la boite la borne de debut (0 par defaut) et de fin (360) et un angle de rotation (deafut=180) indiquant la rotation de la plage ex par defaut =180 sortie: [-180;180[ le neurone representant un angle de 0
� est code par le neurone a l'index: longueur/2 au lieu de l'index 0 si [0;360[
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>

/*#define DEBUG*/
void function_set_zero(int numero)
{	 int deb,nbre,i;
int gpe_i;
	
  deb=def_groupe[numero].premier_ele;
  nbre=def_groupe[numero].nbre;
 
    gpe_i=trouver_entree(numero,(char*)"b");
  	if(gpe_i<0){
     /*recherche du max en entree*/
	for(i=deb;i<nbre+deb;i++)
	{
		
		neurone[i].s2=neurone[i].s1=neurone[i].s=0;
		
	}
}
else{
if(neurone[def_groupe[gpe_i].premier_ele].s2>0)
neurone[deb].s2=neurone[deb].s1=neurone[deb].s=1-neurone[deb].s;
}
 
}

