/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_instar_multi_gpe f_instar_multi_gpe
\ingroup libNN_IO

\brief application d'un instar sur des groupes de neurones (d'apres le model de Grosseberg assembles par colonnes ou par ligne

\details

\section Description
Calcul d'une normalisation des images d'apres le modele Instar de Grosseberg).

eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i)

Si A=1 :
A l'equilibre
ni =((bP)/(1+P))*qi

P=sum(pj)(quelquesoit j) !! c'est bien sur tous les pj entrants
et qi=pj/P

L'activite total de la carte vaut ((bP)/(1+P))<=b

b represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc b=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (bp)/(A+p) ici.

Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

\section Options

Parametres : -A down (=1 par defaut)
	     -B up (=sx*sy par defaut)
	     -H/-V (les groupes sont assembles horizontalement or verticalement)

\file
\ingroup f_instar_multi_gpe

\brief application d'un instar sur des groupes de neurones (d'apres le model de Grosseberg assembles par colonnes ou par ligne

Author: A. de RENGERVE
Created: 19/08/2010
Modified:
- author: A. de RENGERVE
- description: specific file creation
- date: 19/08/2010


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Calcul d'une normalisation des images d'apres le modele Instar de Grosseberg).

eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i)

Si A=1 :
A l'equilibre
ni =((bP)/(1+P))*qi

P=sum(pj)(quelquesoit j) !! c'est bien sur tous les pj entrants
et qi=pj/P

L'activite total de la carte vaut ((bP)/(1+P))<=b

b represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc b=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (bp)/(A+p) ici.

Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

Parametres : -A down (=1 par defaut)
	     -B up (=sx*sy par defaut)
	     -H/-V (les groupes sont assembles horizontalement or verticalement)
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/




#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_IO/NN_Tools.h>

#undef DEBUG

#include <net_message_debug_dist.h>

struct instar_multi_gpe_data
{
  int deb_gpe;
  int deb_gpe_input;
  int sx;
  int sy;
  float up;
  float down;
  int incr_neurons;
  int incr_groups;
  int nb_neurons;
  int nb_groups;
};

void function_instar_multi_gpe(int Gpe)
{
  int lien, i, j;
  float f_sum = 0., f_norm = 0.;
  int sx, sy;
  float up = 1.;              /*par defaut on considere que c'est 1 pour faire des sorties de type neurone */
  float down;
  int deb_gpe = -1, deb_gpe_input = -1;
  int pos = -1;
  int input_gpe = -1;
  struct instar_multi_gpe_data *this_instar_gpe_data;
  char *param = NULL, *chaine = NULL;
  int incr_neurons,incr_groups,nb_neurons,nb_groups;
  dprints("entree dans fonction instar_multi_gpe %s \n", def_groupe[Gpe].no_name);
  
  if (def_groupe[Gpe].data == NULL)
    {
      if ((lien = find_input_link(Gpe, 0)) == -1)
        {
	  EXIT_ON_ERROR("Erreur de lien entrant dans function_instar_multi_gpe : gpe %s\n",def_groupe[Gpe].no_name);
        }
      
      chaine = liaison[lien].nom;
      
      if ((param = strstr(chaine, "-A")) != NULL)
	down = (float) atof(&param[2]);
      else
	down = 1.;
      
      input_gpe = liaison[lien].depart;

      sx = def_groupe[Gpe].taillex;
      sy = def_groupe[Gpe].tailley;
      
      if ((sx != def_groupe[input_gpe].taillex)
	  || (sy != def_groupe[input_gpe].tailley))
	{
	  EXIT_ON_ERROR("Gpe : %s f_instar_multi_gpe : le nombre de neurones du groupe ne correspond pas a la taille du groupe entrant\n", def_groupe[Gpe].no_name);
	}
      
      if ((param = strstr(chaine, "-B")) != NULL)
	{
	  up = (float) atof(&param[2]);
	  /*printf("init up=%f \n",up);*/
	}
      else
	up = (float) sx *(float) sy;
      /*comme vu dans les explication on il faut up par rapport a la carte */
      
      if ((param = strstr(chaine, "-H")) != NULL)
	{	  
	  incr_neurons = 1;
	  incr_groups = sx;
	  nb_neurons=sx;
	  nb_groups=sy;
	}
      else if ((param = strstr(chaine, "-V")) != NULL)
	{
	  incr_neurons = sx;
	  incr_groups = 1;
	  nb_neurons=sy;
	  nb_groups=sx;
	}
      else
	{	  
	  incr_neurons = 1;
	  incr_groups = sx*sy;
	  nb_neurons=sx*sy;
	  nb_groups=1;
	}
      
      
      deb_gpe = def_groupe[Gpe].premier_ele;
      deb_gpe_input = def_groupe[input_gpe].premier_ele;
      
      def_groupe[Gpe].data = ALLOCATION(struct instar_multi_gpe_data);
      this_instar_gpe_data = (struct instar_multi_gpe_data *) def_groupe[Gpe].data;
      this_instar_gpe_data->deb_gpe = deb_gpe;
      this_instar_gpe_data->deb_gpe_input = deb_gpe_input;
      this_instar_gpe_data->sx = sx;
      this_instar_gpe_data->sy = sy;
      this_instar_gpe_data->up = up;
      this_instar_gpe_data->down = down;
      this_instar_gpe_data->incr_neurons=incr_neurons;
      this_instar_gpe_data->incr_groups=incr_groups;
      this_instar_gpe_data->nb_neurons=nb_neurons;
      this_instar_gpe_data->nb_groups=nb_groups;
      
      if ((lien = find_input_link(Gpe, 1)) != -1)
        {
	  EXIT_ON_ERROR("Erreur : la fonction instar_multi_gpe (groupe %s) ne prend qu'un lien algo -Axxx-Byyy en entree \n",def_groupe[Gpe].no_name);
        }
    }
  else
    {
      this_instar_gpe_data = (struct instar_multi_gpe_data *) def_groupe[Gpe].data;
      deb_gpe = this_instar_gpe_data->deb_gpe;
      deb_gpe_input = this_instar_gpe_data->deb_gpe_input;
      sx = this_instar_gpe_data->sx;
      sy = this_instar_gpe_data->sy;
      up = this_instar_gpe_data->up;
      down = this_instar_gpe_data->down;
      incr_neurons = this_instar_gpe_data->incr_neurons;
      incr_groups = this_instar_gpe_data->incr_groups;
      nb_neurons = this_instar_gpe_data->nb_neurons;
      nb_groups = this_instar_gpe_data->nb_groups;
    }
  
  for (i = 0; i < nb_groups; i++) { /* pour chaque groupe on effectue le instar*/
    f_sum=0;
    for (j = 0; j < nb_neurons; j++)
      f_sum += neurone[deb_gpe_input + i*incr_groups + j*incr_neurons].s1;
    
    if (f_sum < 0.000001)       /*cas ou les entrees sont nulles */
      {
	for (j = 0; j < nb_neurons; j++)
	  {
	    pos = deb_gpe + i*incr_groups + j*incr_neurons;
	    neurone[pos].s = 0.;
	    neurone[pos].s1 = 0.;
	    neurone[pos].s2 = 1.;   /*pour que le groupe suivant aprenne sur ces neurones quoique dans le 
				      cas ou y'a absolument rien en entree on peut se demander si'il faut l'apprendre... */
	  }
      }
    else {
      f_norm = (up * f_sum) / (down + f_sum);
      /*printf("f_norm = %f , f_sum=%f, up =%f, down=%f \n",f_norm,  f_sum, up, down);*/

      for (j = 0; j < nb_neurons; j++)
	{
	  pos = deb_gpe + i*incr_groups + j*incr_neurons;
	  neurone[pos].s = f_norm * (neurone[deb_gpe_input + i*incr_groups + j*incr_neurons].s1 / f_sum);
	  neurone[pos].s1 = neurone[pos].s;
	  neurone[pos].s2 = 1.;   /*pour que le groupe suivant aprenne sur ces neurones */
	}
    }
  }
}  
