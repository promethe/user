/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_im_to_gpe.c 
\brief Fonction convertissant une image de la prom_image_struct vers des neurones

Author: Mickael Maillard 
Created: 09/07/04
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction permet de mettre une image du groupe entrant sur les neurones du groupe.
L'image doit etre en N&B (unsigned char ou float)
Le nombre de neurones du groupe doit etre egal au nombre de pixels de l'image.

Parametre : -f, facteur par defaut = 10 dans le cas d'une image en floatant.
 
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#include <Kernel_Function/find_input_link.h>

#undef DEBUG

#define DEF_FACTEUR 10.

void function_im_to_gpe(int Gpe)
{
    int i;
    int lien = -1;
    int inputGpe, longueur, nb_band, sx, sy, deb;
    float *float_image = NULL;
    unsigned char *char_image;
    char *chaine = NULL, *param = NULL;
    float facteur;

    lien = find_input_link(Gpe, 0);
    if (lien == -1)
    {
        printf("%s : Gpe : %d : Absence de  lien entrants...\n", __FUNCTION__,
               Gpe);
        exit(0);
    }

    chaine = liaison[lien].nom;
    param = strstr(chaine, "f");
    if (param != NULL)
        facteur = (float) atof(&param[1]);
    else
        facteur = (float) DEF_FACTEUR;
    inputGpe = liaison[lien].depart;
    longueur = def_groupe[Gpe].nbre;
    deb = def_groupe[Gpe].premier_ele;

    if (def_groupe[inputGpe].ext == NULL)
    {
        printf
            ("%s : Gpe : %d : Absence d'extension dans le groupe entrant...\n",
             __FUNCTION__, Gpe);
        /*exit(0); */
        return;
    }

    nb_band = ((prom_images_struct *) def_groupe[inputGpe].ext)->nb_band;
    sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
    sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;

    if ((sx * sy) != longueur)
    {
        printf
            ("%s : Gpe : %d : Nbre de neurones incorrect par rapport a la taille de l'image\n",
             __FUNCTION__, Gpe);
        exit(0);
    }
    /*printf("Gpe %d %d %d %d\n",Gpe,sx,sy,nb_band); */
    switch (nb_band)
    {
    case 1:
        char_image =
            ((prom_images_struct *) def_groupe[inputGpe].ext)->
            images_table[0];
        for (i = 0; i < (sx * sy); i++)
        {
            neurone[i + deb].s = neurone[i + deb].s1 = neurone[i + deb].s2 =
                (*(char_image + i) / (float) 255);
        }
        break;

    case 4:
        float_image =
            (float *) (((prom_images_struct *) def_groupe[inputGpe].ext)->
                       images_table[0]);
        for (i = 0; i < (sx * sy); i++)
        {
            neurone[i + deb].s = neurone[i + deb].s1 = neurone[i + deb].s2 =
                *(float_image + i) * facteur;
        }
        break;

    default:
        printf
            ("%s : Gpe : %d : nb_band incorrect dans le groupe entrant : cette fonction ne gere que le noir et blanc\n",
             __FUNCTION__, Gpe);
        exit(0);

    }
    /*for(i=0;i<(sx*sy);i++)
       {
       printf("%f \n",neurone[i+deb].s1);
       } */

    return;
}
