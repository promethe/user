/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_1D_TO_2D.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M.LAGARDE
- description: specific file creation
- date: 26/07/2006

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

struct s1D_TO_2D_STRUCT
{
    int gpe_x;
    int first_x;
    int size_x;
    int gpe_y;
    int first_y;
    int size_y;
};

void function_1D_TO_2D(int numero)
{
    int nr;
    int first;
    int i = 0, j = 0;
    struct s1D_TO_2D_STRUCT *ptr = NULL;
    int gpe_x = -1, gpe_y = -1;
    int first_x, first_y;
    int size_x, size_y;

    /* nombre de neurones dans le groupe */
    nr = def_groupe[numero].nbre;
    /* premier neurone du groupe */
    first = def_groupe[numero].premier_ele;

    if (def_groupe[numero].ext == NULL)
    {
        /* INIT : on recherche le lien algo en entree et le groupe de depart de ce lien */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == numero)
            {
                if (def_groupe[liaison[i].depart].taillex > 1)
                {
                    if (def_groupe[liaison[i].depart].tailley > 1)
                    {
                        fprintf(stderr,
                                "f_1D_TO_2D : Les groupes precedents ne doivent etre que sur 1 dimension - groupe %i\n",
                                liaison[i].depart);
                        exit(1);
                    }
                    else
                        gpe_x = liaison[i].depart;
                }
                else if (def_groupe[liaison[i].depart].tailley > 1)
                {
                    if (def_groupe[liaison[i].depart].taillex > 1)
                    {
                        fprintf(stderr,
                                "f_1D_TO_2D : Les groupes precedents ne doivent etre que sur 1 dimension - groupe %i\n",
                                liaison[i].depart);
                        exit(1);
                    }
                    else
                        gpe_y = liaison[i].depart;
                }
                else
                {
                    fprintf(stderr,
                            "f_1D_TO_2D : 1 des groupes precedent est un nombre de neurones incorrect - groupe %i\n",
                            liaison[i].depart);
                    exit(1);
                }
            }
        if ((gpe_x == -1) || (gpe_y == -1))
        {
            fprintf(stderr,
                    "f_1D_TO_2D : 1 des groupes precedent est un nombre de neurones incorrect\n");
            exit(2);
        }

        size_x = def_groupe[gpe_x].nbre;
        size_y = def_groupe[gpe_y].nbre;

        if (nr != (size_x * size_y))
        {
            fprintf(stderr,
                    "f_1D_TO_2D : Le nombre de neurones en entree en X et Y ne correspond pas au nombre de neurone N de ce groupe.\n"
                    "f_1D_TO_2D : N = X * Y\n");
            exit(3);
        }

        if ((ptr = malloc(sizeof(struct s1D_TO_2D_STRUCT))) == NULL)
        {
            fprintf(stderr, "f_1D_TO_2D : Erreur d allocation memoire\n");
            exit(4);
        }

        ptr->gpe_x = gpe_x;
        ptr->first_x = first_x = def_groupe[gpe_x].premier_ele;
        ptr->size_x = size_x;
        ptr->gpe_y = gpe_y;
        ptr->first_y = first_y = def_groupe[gpe_y].premier_ele;
        ptr->size_y = size_y;

        def_groupe[numero].ext = (void *) ptr;
    }
    else
    {
        ptr = (struct s1D_TO_2D_STRUCT *) def_groupe[numero].ext;
        gpe_x = ptr->gpe_x;
        first_x = ptr->first_x;
        size_x = ptr->size_x;
        gpe_y = ptr->gpe_y;
        first_y = ptr->first_y;
        size_y = ptr->size_y;
    }


    for (i = 0; i < size_y; i++)
    {
        for (j = 0; j < size_x; j++)
        {
            if (((neurone[first_x + j].s2 < 0)
                 || (neurone[first_x + j].s2 > 0))
                && ((neurone[first_y + i].s2 < 0)
                    || (neurone[first_y + i].s2 > 0)))
            {
                neurone[first + j + (size_x * i)].s =
                    neurone[first + j + (size_x * i)].s1 =
                    neurone[first + j + (size_x * i)].s2 = 1;
            }
            else
                neurone[first + j + (size_x * i)].s =
                    neurone[first + j + (size_x * i)].s1 =
                    neurone[first + j + (size_x * i)].s2 = 0;
        }
    }
}
