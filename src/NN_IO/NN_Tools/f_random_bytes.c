/** ***********************************************************
\file  random_bytes.c
\brief takes random bytes in files with expressions_choices

Author: Oriane Dermy
Created: 06/08/15

Theoritical description:

Description:
* DON'T SVN this specific function !
 This file contains a little function ables to take a random buffer of bytes in a file.
 * It's specific of Oriane programms in wich the files in witch we take bytes is linked to an expression (i.e. if a control group say we want an "a" sound, it will open the "a" files
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Kernel_Function/find_input_link.h>
#include <libx.h>


typedef struct random_bytes{
	int controlGroup, mx, my;
	int16_t *buf;
	int ta, to, ti, tn;
	FILE *fa, *fo, *fi, *fn;
} type_random_bytes;


void new_random_bytes(int Gpe)
{
	int l;
	
	type_random_bytes *my_random_bytes=NULL;
	char param[256];

	dprints("Begining of new_random_bytes\n");
	srand(time(NULL)); // initialisation de rand
	my_random_bytes = (type_random_bytes*)malloc(sizeof(type_random_bytes));

	if(my_random_bytes == NULL)
	{
		dprints("my_random_bytes is NULL!\n");
		EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
	}
	
	/**Initialisation **/
	my_random_bytes->fa = NULL;
	my_random_bytes->fo = NULL;
	my_random_bytes->fi = NULL;
	my_random_bytes->fn = NULL;
	my_random_bytes->ta = -1;
	my_random_bytes->to = -1;
	my_random_bytes->ti = -1;
	my_random_bytes->tn = -1;
	
	my_random_bytes->controlGroup = -1;
	
	my_random_bytes->mx = def_groupe[Gpe].taillex;
	my_random_bytes->my = def_groupe[Gpe].tailley;
	
	my_random_bytes->mx = def_groupe[Gpe].taillex;
	my_random_bytes->buf = (int16_t *) malloc(my_random_bytes->mx*my_random_bytes->my*sizeof(int16_t));
	/******END Init********/
	/******************************************************Links Treatments*************************************/
	dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
		l = find_input_link(Gpe, 0);
		if (prom_getopt(liaison[l].nom, "-neuron", param) != 0)
		{
			my_random_bytes->controlGroup = liaison[l].depart;
		}
	}
	
	if (my_random_bytes->controlGroup == -1)
	{
		dprints("%s : No controlGroup in input !\n", __FUNCTION__);
		kprints("%s : No controlGroup in input !\n", __FUNCTION__);
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}
	dprints("Groupe input is : %s\n", def_groupe[my_random_bytes->controlGroup].no_name);
	
	dprints("End of the reading links\n");
	/****************************************End Links Treatments**********************************/
	
	/****************************************Open files**********************************/
	if((my_random_bytes->fa = fopen("data/sound/soundA", "rb")) == NULL)
	{
		fprintf(stderr, "erreur lors de l'ouverture du fichier soundA\n");
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);

	}
	if((my_random_bytes->fo = fopen("data/sound/soundO", "rb")) == NULL)
	{
		fprintf(stderr, "erreur lors de l'ouverture du fichier soundO\n");
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}
	if((my_random_bytes->fi = fopen("data/sound/soundI", "rb")) == NULL)
	{
		fprintf(stderr, "erreur lors de l'ouverture du fichier soundI\n");
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}
	if((my_random_bytes->fn = fopen("data/sound/soundN", "rb")) == NULL)
	{
		fprintf(stderr, "erreur lors de l'ouverture du fichier soundN\n");
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}
	fseek(my_random_bytes->fa, 0, SEEK_END);
	my_random_bytes->ta = (int)((double) ftell(my_random_bytes->fa) / (double)(sizeof(int16_t)*2048));
	if(my_random_bytes->ta == 0) EXIT_ON_ERROR("erreur il n'y a pas assez de data dans soundA\n");
	fseek(my_random_bytes->fo, 0, SEEK_END);
	my_random_bytes->to = (int)((double) ftell(my_random_bytes->fo) / (double)(sizeof(int16_t)*2048));
	if(my_random_bytes->to == 0) EXIT_ON_ERROR("erreur il n'y a pas assez de data dans soundO\n");
	fseek(my_random_bytes->fi, 0, SEEK_END);
	my_random_bytes->ti = (int)((double) ftell(my_random_bytes->fi) / (double)(sizeof(int16_t)*2048));
	if(my_random_bytes->ti == 0) EXIT_ON_ERROR("erreur il n'y a pas assez de data dans soundi\n");
	fseek(my_random_bytes->fn, 0, SEEK_END);
	my_random_bytes->tn = (int)((double) ftell(my_random_bytes->fn) / (double)(sizeof(int16_t)*2048));
	if(my_random_bytes->tn == 0) EXIT_ON_ERROR("erreur il n'y a pas assez de data dans soundN\n");
	dprints("tailles : a(%d) o(%d) i(%d) n(%d)\n", my_random_bytes->ta, my_random_bytes->ti, my_random_bytes->to, my_random_bytes->tn);
	
	
	/************************************END OF Open files**********************************/

	def_groupe[Gpe].data= my_random_bytes; /*Save of My_Data*/
	if(my_random_bytes == NULL || def_groupe[Gpe].data == NULL) EXIT_ON_ERROR("erreur probleme d'allocation des datas");
	dprints("End of new_random_bytes\n");
}


void destroy_random_bytes(int gpe)
{
	type_random_bytes *my_random_bytes=NULL;
	my_random_bytes = def_groupe[gpe].data;
	
	if(my_random_bytes->fa!=NULL) fclose(my_random_bytes->fa);
	if(my_random_bytes->fo!=NULL) fclose(my_random_bytes->fo);
	if(my_random_bytes->fi!=NULL) fclose(my_random_bytes->fi);
	if(my_random_bytes->fn!=NULL) fclose(my_random_bytes->fn);
	free(def_groupe[gpe].data);
	def_groupe[gpe].data=NULL;
	def_groupe[gpe].ext=NULL;
}


void function_random_bytes(int Gpe)
{
	int debut, debutCtrlGpe, choix, i;
	type_random_bytes *my_random_bytes=NULL;
	float max;
	int random; 
	
	dprints("At the begining of the function random_bytes\n");
	my_random_bytes = def_groupe[Gpe].data;
	if(my_random_bytes ==NULL) EXIT_ON_ERROR("erreur! les datas sont nulles \n");
	
	debut = def_groupe[Gpe].premier_ele;
	debutCtrlGpe = def_groupe[my_random_bytes->controlGroup].premier_ele;

	//récupération du type de son (a, o , i , e) qu'il faut récupérer
	max = neurone[debutCtrlGpe].s1;
	choix = 0;
	for(i= 1; i< 4; i++)
	{
		if(neurone[debutCtrlGpe +i].s1>max)
		{
			max = neurone[debutCtrlGpe +i].s1;
			choix = i;
		}
	}
	
	//récupération du buffer
	switch(choix)
	{
		case 0 : 	random = rand()%(my_random_bytes->ti); 
					dprints("On lit lechantillon %d des datas O\n", random);
					//MODIF : Ajout du *2048 parce que ça me parait plus logique
					fseek(my_random_bytes->fi, random*2048, SEEK_SET);
					if(fread(my_random_bytes->buf, sizeof(int16_t), 2048, my_random_bytes->fi)!= 2048)
					{
						printf("Erreur, on a pas le bon format de lecture ?\n");
					}
					break;
		case 1 : 	random =  (int)(((double)rand() * (double)(my_random_bytes->tn) / (double) RAND_MAX)); //rand() % (my_random_bytes->tn); 
					dprints("random %d data N tn = %d\n", random, my_random_bytes->tn);
					//MODIF : Ajout du *2048 parce que ça me parait plus logique
					fseek(my_random_bytes->fn, random*2048, SEEK_SET);
					if(fread(my_random_bytes->buf, sizeof(int16_t), 2048, my_random_bytes->fn)!= 2048)
					{
						printf("Erreur, on a pas le bon format de lecture ?\n");
					}
					break;
		case 2 : 	random =  (int)(((double)rand() * (double)(my_random_bytes->ta) / (double) RAND_MAX)); //rand() % (my_random_bytes->tn); 
					dprints("random %d data A tn = %d\n", random, my_random_bytes->ta);
					//MODIF : Ajout du *2048 parce que ça me parait plus logique
					fseek(my_random_bytes->fa, random*2048, SEEK_SET);
					if(fread(my_random_bytes->buf, sizeof(int16_t), 2048, my_random_bytes->fa)!= 2048)
					{
						printf("Erreur, on a pas le bon format de lecture ?\n");
					}
					break;
		case 3 : 	random =  (int)(((double)rand() * (double)(my_random_bytes->to) / (double) RAND_MAX)); //rand() % (my_random_bytes->tn); 
					dprints("random %d data O tn = %d\n", random, my_random_bytes->to);
					//MODIF : Ajout du *2048 parce que ça me parait plus logique
					fseek(my_random_bytes->fo, random*2048, SEEK_SET);
					if(fread(my_random_bytes->buf, sizeof(int16_t), 2048, my_random_bytes->fo)!= 2048)
					{
						printf("Erreur, on a pas le bon format de lecture ?\n");
					}
					break;
	}

	//remplissage des neurones
	for(i= 0;i<my_random_bytes->mx;i++)
	{
		//We normalize the data. Max  is 1 << 15
		// Pour optimiser il ne faudrait peut être pas normalise et ne travailler que sur des entiers. Arnaud B.
		neurone[debut + i].s1 = (float)  my_random_bytes->buf[i * 2] / (1 << 15);
		neurone[debut + i + my_random_bytes->mx].s1 =  (float) my_random_bytes->buf[i * 2 + 1] / (1 << 15);	
	}
	
	dprints("end of random_bytes Gpe %d : %s\n", Gpe, __FUNCTION__);
}
