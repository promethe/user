/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_ambigous_states_learning.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define	L_PRODUIT	0
#define	L_DISTANCE	1

#define	OSCILLO_SEUIL	0.8

typedef	struct	s_ambigous_states_learning
{
  int		first;
  int		nr;

  int		cs_gpe;

  float		seuil;
}		t_ambigous_states_learning;

void		function_ambigous_states_learning_produit(int gpe, t_ambigous_states_learning	*data)
{
  int		i = 0;
  int		cs_gpe = -1;
  float		seuil = 0.0;
  int		first = 0;
  int		nr = 0;
  float		pot_max = -1000.0;
  int		max = -1;
  type_coeff	*lien = NULL;
  int		input = 0;

  cs_gpe = data->cs_gpe;
  seuil = data->seuil;
  nr = data->nr;
  first = data->first;

  /* On met tous les neuronnes � 0 */
  for (i = first; i < (first + nr); i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  
  /* On caclul les potentiels d entree */
  for (i = first; i < (first + nr); i++)
    {
      float		potentiel = 0.0;
      
      potentiel = 0.0;
      /* On recupere le potentiel sur les liens d'entree inconditionnels */
      for (lien = neurone[i].coeff; lien != NULL; lien = lien->s)
	{
	  if (liaison[lien->gpe_liaison].depart != cs_gpe)
	    potentiel += (lien->val * neurone[lien->entree].s2);
	}

      if (potentiel >= seuil)
	{
	  type_coeff	*lien_c = NULL;

	  potentiel = 0.0;
	  for (lien_c = neurone[i].coeff; lien_c != NULL; lien_c = lien_c->s)
	    {
	      if (liaison[lien_c->gpe_liaison].depart == cs_gpe)
		potentiel += (lien_c->val * neurone[lien_c->entree].s2);
	    }
	  neurone[i].s = potentiel;
	  input = 1;
	}
    }
  
  /* On deduit le gagnant */
  if (input)
    {
      for (i = first; i < (first + nr); i++)
	{
	  if (neurone[i].s >= pot_max)
	    {
	      max = i;
	      pot_max = neurone[i].s;
	    }
	}
      if (max >= 0)
	{
	  neurone[max].s1 = 1.0;/*  neurone[max].s; */
	  neurone[max].s2 = 1.0;
	}

      /* 
      ** On lance l apprentissage
      */
      if (global_learn) /* Apprentissage */
	{
	  for (lien = neurone[max].coeff; lien != NULL; lien = lien->s)
	    {
	      if ((liaison[lien->gpe_liaison].depart == cs_gpe) && (lien->evolution))
		{
		  lien->val = lien->val + (epsilon * (neurone[max].s - lien->val) * neurone[lien->entree].s2);
		}
	    }
	}
      else /* Utilisation */
	{

	}
    }
}

void		function_ambigous_states_learning_distance(int gpe, t_ambigous_states_learning	*data)
{
  int		i = 0;
  int		cs_gpe = -1;
  float		seuil = 0.0;
  int		first = 0;
  int		nr = 0;
  float		act_min = 1000.0;
  int		min = -1;
  float		act_max = -1000.0;
  int		max = -1;
  type_coeff	*lien = NULL;
  int		input = 0;
  float		learning_speed = 1;

  cs_gpe = data->cs_gpe;
  seuil = data->seuil;
  nr = data->nr;
  first = data->first;

  /* On met tous les neuronnes � 0 */
  for (i = first; i < (first + nr); i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  
  /* On caclul les potentiels d entree */
  for (i = first; i < (first + nr); i++)
    {
      float		potentiel = 0.0;
      
      potentiel = 0.0;
      /* On recupere le potentiel sur les liens d'entree inconditionnels */
      for (lien = neurone[i].coeff; lien != NULL; lien = lien->s)
	{
	  if (liaison[lien->gpe_liaison].depart != cs_gpe)
	    potentiel += (lien->val * neurone[lien->entree].s2);
	}

      if (potentiel >= seuil)
	{
	  type_coeff	*lien_c = NULL;

	  potentiel = 0.0;
	  fprintf(stdout, "########\n");
	  for (lien_c = neurone[i].coeff; lien_c != NULL; lien_c = lien_c->s)
	    {
	      if (liaison[lien_c->gpe_liaison].depart == cs_gpe)
		potentiel += fabs(lien_c->val - neurone[lien_c->entree].s2);
	    }
	  neurone[i].s = 1.0 / (1.0 + (float)potentiel);
	  neurone[i].s1 = 1.0;
	  input = 1;
	  fprintf(stdout, "potentiel %f\n", neurone[i].s);
	}
    }
  
  /* On deduit le perdant */
  if (input)
    {
      for (i = first; i < (first + nr); i++)
	{
	  if (((neurone[i].s1 < 0) || (neurone[i].s1 > 0)) && (neurone[i].s <= act_min))
	    {
	      min = i;
	      act_min = neurone[i].s;
	    }
	  if (((neurone[i].s1 < 0) || (neurone[i].s1 > 0)) && (neurone[i].s >= act_max))
	    {
	      max = i;
	      act_max = neurone[i].s;
	    }
	}
      
      if ((max >= 0) && neurone[max].s > OSCILLO_SEUIL)
	{
	  for (i = first; i < (first + nr); i++)
	    if (i != max)
	      neurone[i].s1 = 0.0;
	  neurone[max].s2 = 1.0;
	  return;
	}
      if (min >= 0)
	{
	  for (i = first; i < (first + nr); i++)
	    if (i != min)
	      neurone[i].s1 = 0.0;
	  neurone[min].s2 = 1.0;
	}


      /* 
      ** On lance l apprentissage
      */
      if (global_learn) /* Apprentissage */
	{
	  for (lien = neurone[min].coeff; lien != NULL; lien = lien->s)
	    {
	      if ((liaison[lien->gpe_liaison].depart == cs_gpe) && (lien->evolution))
		{
		  lien->val = lien->val + (def_groupe[gpe].learning_rate * (neurone[lien->entree].s2 - lien->val));
		}
	    }
	}
      else /* Utilisation */
	{

	}
    }
}

void		function_ambigous_states_learning(int gpe)
{
  int				i = 0;
  int				link = -1;
  t_ambigous_states_learning	*data = NULL;

  if (def_groupe[gpe].ext == NULL)
    {
      if ((data = malloc(sizeof(t_ambigous_states_learning))) == NULL)
	{
	  perror("f_ambigous_states_learning : init : malloc");
	  exit(1);
	}
      for (i = 0; (link = find_input_link(gpe, i)) >= 0; i++)
	{
	  if (!strcmp(def_groupe[liaison[link].depart].nom, "f_ctrnn"))
	    data->cs_gpe = liaison[link].depart;
	}
      data->first = def_groupe[gpe].premier_ele;
      data->nr = def_groupe[gpe].nbre;
      data->seuil = def_groupe[gpe].seuil;

      def_groupe[gpe].ext = (void *)data;
    }
  else
    data = (t_ambigous_states_learning *)def_groupe[gpe].ext;

/*       f_ambigous_states_learning_produit(gpe, data); */

      function_ambigous_states_learning_distance(gpe, data);
}
