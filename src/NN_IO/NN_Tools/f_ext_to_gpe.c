/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_ext_to_gpe f_ext_to_gpe
 \ingroup libNN_IO

 \brief Convert data from ext to neural activity on group.

 \details

 Convert data from ext to neural activity on group.

 \file
 \ingroup f_ext_to_gpe
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: Arnaud Blacnhard
 - description:
 Permet de choisir le cannal (images couleurs ou composites).
 Format moderne (new / funtion / destroy )
 - date: 27/11/2009

 */

/*#define DEBUG*/

#include <string.h>
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>

#define SCALE_NONE 0
#define SCALE_AUTO 1

void image_vers_reseau(int gpe, unsigned char *im, float norm)
{
  int deb, i, longueur;
  deb = def_groupe[gpe].premier_ele;
  longueur = def_groupe[gpe].nbre;
  norm = 1. / norm;
  for (i = deb; i < deb + longueur; i++)
  {
    neurone[i].s = neurone[i].s1 = (((float) im[i - deb]) * norm);
    neurone[i].s2 = 1.;
  }
}

void imagef_vers_reseau(int gpe, unsigned char *im, float norm)
{
  int deb, i, longueur;
  float *im_f = (float*) im;

  deb = def_groupe[gpe].premier_ele;
  longueur = def_groupe[gpe].nbre;

  norm = 1. / norm;
  for (i = deb; i < deb + longueur; i++)
  {
    neurone[i].s = neurone[i].s1 = im_f[i - deb] * norm;
    //neurone[i].s = neurone[i].s1 =  im[i - deb]*norm;  // Ne fonctionne pas
    neurone[i].s2 = neurone[i].s;
  }

}

typedef struct my_data {
  int channel, scale_mode;
  float x_scale, y_scale;
  prom_images_struct *input_ext;
  type_groupe *input_group;
  int no_image; /* no de l'image a selectionner en entree (si plusieurs images) */
  float norm; /* coeff utiliser pour la conv image -> neurone */
} My_data;

void new_ext_to_gpe(int gpe)
{
  My_data *my_data;
  int Index, GpeExt;
  char param_link[256];
  type_groupe *group;

  my_data = ALLOCATION(My_data);
  group = &def_groupe[gpe];
  group->data = my_data;

  dprints("begin %s \n", __FUNCTION__);
  my_data->channel = 0;
  my_data->scale_mode = SCALE_NONE;
  my_data->norm = -1.;
  my_data->no_image = 0;

  GpeExt = -1;
  /* recuperations des donnees */
  for (Index = 0; Index < nbre_liaison; Index++)
  {
    if (liaison[Index].arrivee == gpe)
    {
      GpeExt = liaison[Index].depart;

      if (prom_getopt(liaison[Index].nom, "-scale=", param_link) == 2)
      {
        if (strcmp(param_link, "none") == 0)
        {
          my_data->scale_mode = SCALE_NONE;
        }
        else if (strcmp(param_link, "auto") == 0)
        {
          my_data->scale_mode = SCALE_AUTO;
        }
        else EXIT_ON_ERROR("The scale mode (%s) for option '-scale=' is unknown. The possible scale mode are: 'none', 'auto'.", param_link);
      }

      if (prom_getopt(liaison[Index].nom, "-channel=", param_link) == 2)
      {
        my_data->channel = atoi(param_link);
      }
      if (prom_getopt(liaison[Index].nom, "-I=", param_link) == 2)
      {
        my_data->no_image = atoi(param_link);
        printf("%s input image %d \n", __FUNCTION__, my_data->no_image);
      }
      if (prom_getopt(liaison[Index].nom, "-norm=", param_link) == 2)
      {
        my_data->norm = atof(param_link);
        my_data->norm = 1. / my_data->norm;
      }
      break;
    }
  }

  if (GpeExt == -1) EXIT_ON_ERROR("Le group %s doit etre connecte a un groupe \t", def_groupe[gpe].no_name);

  my_data->input_group = &def_groupe[GpeExt];
  my_data->input_ext = NULL;
  dprints("end %s \n", __FUNCTION__);
}

void function_ext_to_gpe(int gpe)
{
  My_data *my_data;
  float x_scale, y_scale;
  int i, j, nj, ni;
  type_neurone *my_neuron;
  float *sub_image_f;
  unsigned char *sub_image_c;
  int channel/*, number_of_channels*/;
  int deb, nb_neurones, deb_line, offset, taillex, im_width, im_height;
  type_groupe *group;
  unsigned char *image;
  float *image_f = NULL, result;
  int no_image;
  int nb_band;
  float norm;

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  dprints("begin %s \n", __FUNCTION__);
  group = &def_groupe[gpe];
  my_data = (My_data*) group->data;
  if (my_data->input_ext == NULL) /* premiere lecture live d'une image - il faut faire les init que new ne pouvait pas faire*/
  {
    if (my_data->input_group->ext == NULL) /* toujours pas d'image. La prochaine fois peut etre que le groupe en entree aura une image. */
    {
      PRINT_WARNING("Gpe %s : le pointeur du groupe amont a son ext NULL\n", my_data->input_group->no_name);
      return;
    }
    else my_data->input_ext = my_data->input_group->ext;

    if (my_data->input_ext->image_number <= my_data->no_image)
    {
      EXIT_ON_ERROR("ERROR: %s, nb max images totale du groupe d'entrée moins grand que l'image désirée, spécifiée sur le lien -I=xxx (nb Image : %d <= %d :I)\n", __FUNCTION__, my_data->input_ext->image_number, my_data->no_image);
    }

    if (my_data->scale_mode == SCALE_NONE)
    {
      if (group->taillex != (int) my_data->input_ext->sx)
      EXIT_ON_ERROR("La taille x (%d) du groupe %s et la taille sx (%d) d'ext du groupe %s doit etre la meme. \
														 Otherwise use the option: '-scale=auto' (careful, no filtering).\n", group->taillex, group->no_name, my_data->input_ext->sx, my_data->input_group->no_name);
      if (group->tailley != (int) my_data->input_ext->sy)
      EXIT_ON_ERROR("La taille y (%d) du groupe %s et la taille sy (%d) d'ext du groupe %s doit etre la meme. \
															Otherwise use the option: '-scale=auto' (careful, no filtering).\n", group->tailley, group->no_name, my_data->input_ext->sy, my_data->input_group->no_name);
    }
    else
    {
      /** Utile pour le mode de scaling automatique */
      if (my_data->channel >= (int) my_data->input_ext->nb_band)
      EXIT_ON_ERROR("The channel %d is superieur to the number of channels %d", my_data->channel, my_data->input_ext->nb_band);

      if (group->taillex >= (int) my_data->input_ext->sx)
      EXIT_ON_ERROR("Pas VRAI PG !!! With -scale=auto, the size x (%d) of the group %s must be strictly smaller than the width (%d) \
												 of the incoming image.", group->taillex, group->no_name, my_data->input_ext->sx);
      if (group->tailley >= (int) my_data->input_ext->sy)
      EXIT_ON_ERROR("Pas VRAI PG !!! With -scale=auto, the size y (%d) of the group %s must be strictly smaller than the height (%d) \
				 									of the incomining image.", group->tailley, group->no_name, my_data->input_ext->sy);

      my_data->x_scale = ((float) group->taillex) / ((float) my_data->input_ext->sx);
      my_data->y_scale = ((float) group->tailley) / ((float) my_data->input_ext->sy);
    }
    if (fabs(my_data->norm + 1.) < 1e-8)
    {
      if (my_data->input_ext->nb_band == 4) my_data->norm = 1.; /* par defaut image float entre 0 et 1 */
      else my_data->norm = 1. / 255.; /* par defaut dans une image char les valeurs sont entre 0 et 255 */
    }
  }

  nb_neurones = def_groupe[gpe].nbre;
  channel = my_data->channel;
  no_image = my_data->no_image;
  image = my_data->input_ext->images_table[no_image];
  nb_band = my_data->input_ext->nb_band;
  norm = my_data->norm;

  if (nb_band == 4)
  {
    dprints("image %d float en entree \n", no_image);
    image_f = (float*) my_data->input_ext->images_table[no_image];
  }

  if (my_data->scale_mode == SCALE_AUTO)
  {
    dprints("%s autoscale mode \n", __FUNCTION__);
    deb = group->premier_ele;
    /*number_of_channels = my_data->input_ext->nb_band;*/

    taillex = group->taillex;
    im_width = my_data->input_ext->sx;
    im_height = my_data->input_ext->sy;
    x_scale = my_data->x_scale;
    y_scale = my_data->y_scale;

    if (my_data->x_scale > 1.) /* pas d'optim pour l'instant : pas d'exemple pour tester... pas sur que ca serve.  */
    {
      dprints("dilatation ???\n");
      for (j = 0; j < group->tailley; j++)
      {
        deb_line = deb + j * taillex;
        offset = ((int) ((float) j * y_scale)) * im_width;

        for (i = 0; i < taillex; i++)
        {
          if (nb_band == 4) result = image_f[((int) (i * x_scale) + offset)]; /* vrai que pour char : *0.003921569; a modifier PG*//** 1 / 255 */
          else result = image[((int) (i * x_scale) + offset)];
          neurone[deb_line + i].s = neurone[deb_line + i].s1 = result * norm;
          neurone[deb_line + i].s2 = 1.;
        }
      }
    }
    else
    {
      /* init du groupe de neurone pour sommer et moyenner les contributions des differents pixels de l'image */
      for (j = deb; j < deb + nb_neurones; j++)
      {
        neurone[j].s1 = neurone[j].s = 0.;
        neurone[j].d = 0.;
        neurone[j].s2 = 1.;
      }

      if (nb_band == 4) /* image de float */
      {
        for (j = im_height; j--;)
        {
          nj = (int) (j * y_scale);
          deb_line = deb + nj * taillex;
          offset = (j * im_width);
          my_neuron = &neurone[deb_line];
          sub_image_f = &image_f[offset]; /* on recupere une ligne */

          for (i = im_width; i--;)
          {
            ni = (int) (i * x_scale);
            my_neuron[ni].s += sub_image_f[i];
            my_neuron[ni].d++;
          }
        }
      }
      else /* image de char */
      {
        /*im_width=im_width*nb_band;*/
        for (j = im_height; j--;)
        {
          nj = (int) (j * y_scale);
          deb_line = deb + nj * taillex;
          offset = (j * im_width) * nb_band;
          my_neuron = &neurone[deb_line];
          sub_image_c = &image[offset]; /* on recupere une ligne */

          for (i = im_width; i--;)
          {
            ni = (int) (i * x_scale);
            my_neuron[ni].s += sub_image_c[i * nb_band + channel];
            my_neuron[ni].d++;
          }
        }
      }

      for (j = deb; j < deb + nb_neurones; j++)
      {
        if (neurone[j].d > 0) neurone[j].s1 = neurone[j].s2 = neurone[j].s / neurone[j].d * norm;
      }
    }
  } /* l'image et le RN on la meme taille */
  else if (nb_band != 4) image_vers_reseau(gpe, image, norm);
  else imagef_vers_reseau(gpe, image, norm);

  dprints("Fin %s \n", __FUNCTION__);
}

void destroy_ext_to_gpe(int gpe)
{
  free(def_groupe[gpe].data);
}
