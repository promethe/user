/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_supervision.c 
\brief 

Author: R. Shirakawa
Created: 03/04/2006

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Cette fonction doit etre utilisee avec la fonction f_load_list.

Elle lit une ligne avec le nom des images suivi par les valeurs du neurone a activer et sa valeur d'activation, en mettant tous les autres neurones a zero.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: function find_input_link don't work well in new_supervision - using old method to find input links...

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>

#define MAX_ACTIVITE 1
#define FILE_NAME_SIZE 255
/* ne changer pas le MESSAGE_TAILLE (lie a la structure de groupe de reseau.h */
#define MESSAGE_SIZE 255

typedef struct
{
    int deb;                    /* variable que indique la valeur du premier neurone */
    int longueur;               /* variable que indique la valeur du dernier neurone */
    int neurone;                /* neurone a active */
    float param;                /* parametre indiquant la valeur d'activation du neurone */
    int gpe_amounte;            /* groupe de liaison en arrier */
    char nom_image[FILE_NAME_SIZE]; /* nom des images a utiliser pour l'apprentissage */
    char message[MESSAGE_SIZE];
} file_data;

void new_supervision(int Gpe)
{
    file_data *data;            /* initialisation de la structure */
    int i;                      /* variable d'iteration */

#ifdef DEBUG
    printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    /*on cree la structure qui contiendra le pointeur vers ce fichier et on init */
    data = (file_data *) malloc(sizeof(file_data));

    data->deb = def_groupe[Gpe].premier_ele;
    data->longueur = def_groupe[Gpe].nbre;

    /* cherche le groupe de liaison en arrier */
  /** data->gpe_amounte = find_input_link ( Gpe , 0 );*/

    data->gpe_amounte = -1;

    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == Gpe)
        {
            data->gpe_amounte = liaison[i].depart;
            break;
        }

#ifdef DEBUG
    printf("%s\t  find_input_link = %i\t gpe_amounte = %i \n", __FUNCTION__,
           find_input_link(Gpe, 0), data->gpe_amounte);
#endif

    if (data->gpe_amounte == -1)
    {
        printf(" Error, %s group %i don't have an input\n", __FUNCTION__,
               Gpe);
        exit(EXIT_FAILURE);
    }

    /* passer les donnees a la fonction suivante */
    def_groupe[Gpe].data = data;

#ifdef DEBUG
    printf("~~~~~~~~~~~end of %s\n", __FUNCTION__);
#endif

}


void function_supervision(int Gpe)
{
    /* initialisation des variables */
    file_data *my_data = NULL;  /* structure des parametres regles par la fonction de supervision */
    int i;                      /* variable d'iteration */
    /* fin de l'initialisation des variables */

#ifdef DEBUG
    printf("~~~~~~~~~~~enter in %s\t group %d\n", __FUNCTION__, Gpe);
#endif

    /* recuperer les donnees de la dernier passage */
    my_data = (file_data *) def_groupe[Gpe].data;
    strcpy(my_data->message, def_groupe[my_data->gpe_amounte].message);

#ifdef DEBUG
    printf("my_data->message = %s in %s\n ", my_data->message, __FUNCTION__);
#endif

    if (my_data == NULL)
    {
        printf("error while loading data in group %d %s\n", Gpe,
               __FUNCTION__);
        exit(EXIT_FAILURE);
    }

    /* recuperer les donnees de la ligne lue */
    sscanf(my_data->message, "%s %d %g", my_data->nom_image,
           &my_data->neurone, &my_data->param);

    /* mettre a zero tous les neurones */
    for (i = my_data->deb; i <= my_data->deb + my_data->longueur; i++)
    {
        neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;
    }

    /* si OK, activer neurone */
    if (my_data->neurone <= my_data->longueur &&
        my_data->param <= MAX_ACTIVITE && my_data->param >= -MAX_ACTIVITE)
    {
        neurone[my_data->deb + my_data->neurone].s2 = MAX_ACTIVITE;
        neurone[my_data->deb + my_data->neurone].s1 =
            neurone[my_data->deb + my_data->neurone].s = my_data->param;
#ifdef DEBUG
        printf("fichier %s \tactive neurone : %d\t avec la valeur: %g\n",
               my_data->nom_image, my_data->neurone,
               neurone[my_data->deb + my_data->neurone].s);
#endif
    }
    /* s'il y a un erreur, montre une message d'erreur et continue */
    else
        printf
            ("error with:\n\tfile %s \n\tneurone to activate: %d \t (min 0 max %d)\n\tchosen activity: %g \t(max %d) \n ignoring this file... \n)",
             my_data->nom_image, my_data->neurone, my_data->longueur,
             my_data->param, MAX_ACTIVITE);


#ifdef DEBUG
    printf("===========exit of %s\t group %d\n", __FUNCTION__, Gpe);
#endif
}
