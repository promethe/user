/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file
\brief 

Author: S. Boucenna
Created: 15/04/2009

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: retourne une valeur emotionnelle positive (+1) ou negative (-1)
Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct MyData {
  int gpe_prec;
}MyData;

void function_valeur_emotionnelle(int gpe)
{
  int deb;                    /* le premier neurone */
  int i=0,j=0;                    /* variable de boucles */
  MyData *mydata=NULL;        /* structure de sauvegarde*/ 
  float max=-9999;
  int i_max=0;
#ifdef DEBUG
    printf("------------>enter in %s\n", __FUNCTION__);
#endif

    if(def_groupe[gpe].data == NULL)
      {
	mydata=(MyData *)malloc(sizeof(MyData));
	if(mydata==NULL)
	  {
	    printf("pb malloc pour mydata");
	    exit(0);
	  }
	/*recuperation des parametres d'entree*/
	while((j=find_input_link(gpe,i))!=-1)
	  {
	    if (strcmp(liaison[j].nom, "expression") == 0)
	      {
		mydata->gpe_prec = liaison[j].depart;
#ifdef DEBUG
		printf("je rentre dans expression\n");
#endif
	      }
	    i++;
	  }
	def_groupe[gpe].data=mydata;	 /*sauvegarde de Mydata*/
      }
    else
      {
	mydata= def_groupe[gpe].data;
      }
    

    deb = def_groupe[gpe].premier_ele;
    
    for(i = def_groupe[mydata->gpe_prec].premier_ele ; i<def_groupe[mydata->gpe_prec].premier_ele + def_groupe[mydata->gpe_prec].nbre ; i++)
      {
	if(neurone[i].s1>max)
	  {
	    max=neurone[i].s1;
	    i_max=i - def_groupe[mydata->gpe_prec].premier_ele;
	  }
      }
    

    if(i_max == 2)
      {
	neurone[deb].s= neurone[deb].s1= neurone[deb].s2=1;
      }
    else
      {
	if(i_max == 3)
	  {
	    neurone[deb].s= neurone[deb].s1= neurone[deb].s2=-1;
	  }
	else
	  {
	    neurone[deb].s= neurone[deb].s1= neurone[deb].s2=0;
	  }
      }

#ifdef DEBUG
    printf("--------------end of %s\n", __FUNCTION__);
#endif
}
