/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef	struct	s_write_conditionnal
{
  int		first;
  int		nr;

  int		pas;
  int		cs_gpe;

}		t_write_conditionnal;

void		function_write_conditionnal(int gpe)
{
  FILE				*fd = NULL;
  int				i = 0;
  int				link = -1;
  int				first;
  int				nr;
  int				cs_gpe;
  type_coeff			*lien = NULL;
  char				filename[64];
  t_write_conditionnal		*data = NULL;
  

  if (def_groupe[gpe].data == NULL)
    {
      if ((data = malloc(sizeof(t_write_conditionnal))) == NULL)
	{
	  perror("function_write_conditionnal : init : malloc");
	  exit(1);
	}
      for (i = 0; (link = find_input_link(gpe, i)) >= 0; i++)
	{
	  if (!strcmp(def_groupe[liaison[link].depart].nom, "f_ctrnn"))
	    data->cs_gpe = liaison[link].depart;
	}
      data->first = def_groupe[gpe].premier_ele;
      data->nr = def_groupe[gpe].nbre;
      data->pas = 0;
      def_groupe[gpe].data = (void *)data;
    }
  else
    data = (t_write_conditionnal *)def_groupe[gpe].data;
  first = data->first;
  nr = data->nr;
  cs_gpe = data->cs_gpe;

  sprintf(filename, "%s.txt", def_groupe[gpe].no_name);
  if ((fd = fopen(filename, "a")) == NULL)
    {
      perror("function_write_conditionnal : fopen");
      exit(-1);
    }
  
  /* On met tous les neuronnes � 0 */
  for (i = first; i < (first + nr); i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
  
  /* On caclul les potentiels d entree */
  for (i = first; i < (first + nr); i++)
    {
      float		potentiel = 0.0;
      type_coeff	*lien_c = NULL;
      
      potentiel = 0.0;
      /* On recupere le potentiel sur les liens d'entree inconditionnels */
      for (lien = neurone[i].coeff; lien != NULL; lien = lien->s)
	{
	  if (liaison[lien->gpe_liaison].depart != cs_gpe)
	    potentiel += (lien->val * neurone[lien->entree].s2);
	}
      
      fprintf(fd, "%i", data->pas);
      if (potentiel > 0)
	{
	  neurone[i].s = neurone[i].s1 = neurone[i].s2 = potentiel;
	  fprintf(fd, " %i", i - first);
	  
	  
	  for (lien_c = neurone[i].coeff; lien_c != NULL; lien_c = lien_c->s)
	    {
	      if (liaison[lien_c->gpe_liaison].depart == cs_gpe)
		{
		  fprintf(fd, " %f", neurone[lien_c->entree].s2);
		}
	    }
	}
      fprintf(fd, "\n");
    }
  fclose(fd);
  data->pas++;
}
