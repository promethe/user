/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_get_angle_from_orientation.c
\brief 

Author: LAGARDE Matthieu
Created: 08/11/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG 1

typedef	struct	s_get_angle_from_orientation
{
  int		first;

  int		prev_first;
  int		prev_nbre;
}		t_get_angle_from_orientation;


void		f_get_angle_from_orientation(int numero)
{
  t_get_angle_from_orientation	*data = NULL;
  int					first = -1;
  int					link = -1;
  int					prev_first = -1;
  int					prev_nbre = -1;
  int					i = -1;

#ifdef DEBUG
  fprintf(stdout, "f_get_angle_from_orientation : entree\n");
#endif /* DEBUG */

  if (def_groupe[numero].ext == NULL)
    {
      if ((data = malloc(sizeof(t_get_angle_from_orientation))) == NULL)
	{
	  fprintf(stderr, "f_get_angle_from_orientation : Erreur d allocation de donnees\n");
	  perror("malloc");
	  exit(-1);
	}
      
      data->first = def_groupe[numero].premier_ele;


      for (i = 0; (link = find_input_link(numero, i)) >= 0; i++)
	{
	  if (!strcmp(liaison[link].nom, "ori"))
	    {
	      data->prev_first = def_groupe[liaison[link].depart].premier_ele;
	      data->prev_nbre = def_groupe[liaison[link].depart].nbre;
	    }
	}

      def_groupe[numero].ext = (void *)data;
    }
  else
    data = (t_get_angle_from_orientation *)def_groupe[numero].ext;

  first = data->first;
  prev_first = data->prev_first;
  prev_nbre = data->prev_nbre;

  neurone[first].s = neurone[first].s1 = neurone[first].s2 = 0.0;

  /* On recherche l indice du neurone active sur le groupe d orientation */
  for (i = 0; i < prev_nbre; i++)
    {
      if (neurone[prev_first + i].s2 > 0)
	break;
    }
  if (i == prev_nbre)
    neurone[data->first].s = neurone[data->first].s1 = neurone[data->first].s2 = -1;
  else
    neurone[data->first].s = neurone[data->first].s1 = neurone[data->first].s2 = 2 * i * 3.14 / prev_nbre;

#ifdef DEBUG
  fprintf(stdout, "f_get_position_from_orientation : sortie\n");
#endif /* DEBUG */

}
