/** ***********************************************************
\file  nelder_mead.c
\brief create a simplex of degree

Author: Oriane Dermy
Created: 03/03/14

Theoritical description:

Description:
This file contains the Nelder function wich allow to find wich degree the robot have to take to 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
#define DEBUG 1
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#define epsilonn  0.0001 //TODO param ?

typedef struct nelder_mead{
	int distGroup, commandGroup, resetGpe;
	float **points, *distances; //points contains angles
	int dimension, needed_points_nb;
	int flagBar, flagEtir, flagRef, flagFinAlgo, flagContrac, flagRemplissage;
	int nbIter, points_nb;
} type_nelder_mead;

void init_nelder_mead(type_nelder_mead *my_nelder_mead)
{
	//this point_nb allow to recompute all the simplex with new points
	my_nelder_mead->points_nb= 0;
	my_nelder_mead->nbIter = 0;
	my_nelder_mead->flagBar = 0;
	my_nelder_mead->flagEtir = 0;
	my_nelder_mead->flagRef = 0;
	my_nelder_mead->flagFinAlgo = 0;
	my_nelder_mead->flagContrac=0;
	my_nelder_mead->flagRemplissage=0;
}

void new_nelder_mead(int Gpe)
{
	int l, i;
	char chaine[256];

	type_nelder_mead *my_nelder_mead = NULL;

	dprints("Begining of new_nelder_mead\n");

	my_nelder_mead = (type_nelder_mead *)malloc(sizeof(type_nelder_mead));

	if(my_nelder_mead == NULL)
	{
		dprints("type_nelder_mead is NULL!\n");
		EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
	}
	my_nelder_mead->dimension = 7*1;
	my_nelder_mead->needed_points_nb = my_nelder_mead->dimension + 1;	
	my_nelder_mead->points = MANY_ALLOCATIONS((my_nelder_mead->needed_points_nb + 4), float*); // +5 for barycentre, extension et reflexion, tild
	for(i=0;i<(my_nelder_mead->needed_points_nb + 4);i++)
	{
		my_nelder_mead->points[i] = MANY_ALLOCATIONS((my_nelder_mead->dimension), float);
	}
	my_nelder_mead->distances = MANY_ALLOCATIONS((my_nelder_mead->needed_points_nb + 4), float); // idem
	init_nelder_mead(my_nelder_mead);
	/******************************************************Links Treatments*************************************/
	//dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
		i=0;
		l = find_input_link(Gpe, i);
		while (l != -1)
		{
			if (prom_getopt(liaison[l].nom, "distance", chaine) != 0)
			{
				my_nelder_mead->distGroup = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "command", chaine) != 0)
			{
				my_nelder_mead->commandGroup = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "reset", chaine) != 0)
			{
				my_nelder_mead->resetGpe = liaison[l].depart;
			}
			i++;
			l = find_input_link(Gpe, i);
		}
	}
	//dprints("End of the reading links\n");
	/****************************************End Links Treatments**********************************/
	def_groupe[Gpe].data=my_nelder_mead; /*Save of My_Data*/

	dprints("End of new_nelder_mead\n");
}


void destroy_nelder_mead(int gpe)
{
	free(def_groupe[gpe].data);
	def_groupe[gpe].data=NULL;
	def_groupe[gpe].ext=NULL;
}

float aleatAngleSimplex()
{
	return (rand()/(float)RAND_MAX );
}


void calculxtild(float *xtild, float *taba, float *tabb, int dimension)
{
	int degre;
	float beta=0.5;
	for(degre=0;degre<dimension;degre++)
	{
		//normaly xtild = beta*taba + (1-beta)*tabb, here beta = 1/2
		xtild[degre] = beta*(taba[degre]) + (1-beta)*(tabb[degre]);
		if(xtild[degre]>1)
		{
			 dprints("calcul xtild depasse 1!\n");
			 xtild[degre] = 1.0;
		}
	}
}


void copy(float *xe, float *x, int dimension)
{
	int degre;
	for(degre=0;degre<dimension;degre++)
	{
		x[degre]=xe[degre];
	}
}

void echange (float *tab, float **nuage, int i, int j) //distance au point, position(x,y,z), 7angles
{
	float tampon = tab[i], tampon2[7];
	int k;

	//on classe les fn
	tab[i] = tab[j];
	tab[j] = tampon;

	//on classe la matrice nuage
	for(k=0;k<7;k++)
	{
		tampon2[k] = nuage[i][k];
		nuage[i][k] = nuage[j][k];
		nuage[j][k] = tampon2[k];
	}


}


int partitionner(float *T, float **nuage, int premier, int dernier, int pivot)
{
	int i,j;
    echange( T, nuage, pivot, dernier);
    j = premier;
    for(i=premier;i<dernier; i++)
    {
		if (T[i] <= T[dernier])
		{
            echange(T, nuage,i, j);
            j++;
		}
    }
    echange(T, nuage, dernier, j);
    return j;
}
 
 void tri_rapide(float *t, float **nuage, int premier, int dernier)
 {
	int pivot=premier;
	if(premier<dernier)
	{
       pivot = partitionner(t, nuage, premier, dernier, pivot);
       tri_rapide(t , nuage, premier, pivot-1);
       tri_rapide(t, nuage, pivot+1, dernier);
    }
 }

void calculBarycentre(float *barycentre, float **nuage, int dimension)
{
	int point,degre;

	for(degre=0;degre<dimension;degre++)
	{
		barycentre[degre]=0;
		//on ne prend pas en compte le dernier point
		for(point=0;point<dimension;point++)
		{
			barycentre[degre]+= nuage[point][degre];
		}
		barycentre[degre]= barycentre[degre] / ((float) dimension);
		if(barycentre[degre]>1.0)
		{
			barycentre[degre]=1.0;
			dprints("barycentre depasse 1?\n");
		}
	}
}

int calculDk(float *barycentre, float *xn1, int dimension)
{

	int degre;
	float sum= 0.0;
	for(degre=0;degre<dimension;degre++)
	{
		//ici alpha = 1 (formule générale (1+ alpha)xb - alpha*xn1
		sum += (barycentre[degre] - xn1[degre])*(barycentre[degre] - xn1[degre]);
	}
	sum = sqrt(sum);
	//dprints("dk = %f\n", sum);
	if(sum <= epsilonn) return 1;
	return 0;
}

void calculReflexion(float *reflexion, float *barycentre, float *xn1, int dimension)
{
	int degre;
	for(degre=0;degre<dimension;degre++)
	{
		//ici alpha = 1 (formule générale (1+ alpha)xb - alpha*xn1
		reflexion[degre] = 2*(barycentre[degre]) - xn1[degre];
		//TODO changement ici : risque que lalgo soit plus bon
		if(reflexion[degre]<0.0)
		{
			reflexion[degre]=0.0;
		}else if(reflexion[degre]>1.0)
		{
			dprints("reflection depasse 1?\n");
			reflexion[degre]=1.0;
		}
	}
}


void calculEtirement(float *xe, float *xr, float *xc, int dimension)
{
	int degre;
	for(degre=0;degre<dimension;degre++)
	{
		//ici gamma=2, normalement xe = gamma*xr + (1 - gamma)*xb
		xe[degre] = 2*xr[degre] - xc[degre];
		//TODO changement ici : risque que lalgo soit plus bon
		if(xe[degre]<0.0)
		{
			xe[degre]=0.0;
		}else if(xe[degre]>1.0)
		{
			dprints("xe depasse 1?\n");
			xe[degre]=1.0;
		}
	}
}

void function_nelder_mead(int Gpe)
{
	int i, j, indexprecedent, finAlgo,debut, debutdistGpe, debutcommandGpe, positionBar, positionRef, positionEtir, dimension, nbPoint, positionTild;
	type_nelder_mead *my_nelder_mead=NULL;
	float *xtild; 
	
	//dprints("At the begining of the function nelder_mead\n");
	my_nelder_mead = def_groupe[Gpe].data; //the output group

	debut = def_groupe[Gpe].premier_ele;
	debutdistGpe = def_groupe[my_nelder_mead->distGroup].premier_ele;
	debutcommandGpe = def_groupe[my_nelder_mead->commandGroup].premier_ele;

	dimension = my_nelder_mead->dimension;
	nbPoint = my_nelder_mead->needed_points_nb;
	xtild = (float *)malloc(dimension*sizeof(float));
	
	//if the reset group is activate, we compute new points and their distance
	if (my_nelder_mead->resetGpe != -1 && isequal(neurone[def_groupe[my_nelder_mead->resetGpe].premier_ele].s1, 1.))
	{
		init_nelder_mead(my_nelder_mead);
	}	
	
	//first part
	if ((my_nelder_mead->flagRemplissage == 0) && (my_nelder_mead->points_nb < my_nelder_mead->needed_points_nb))
	{
		
		//deux premiers points
		if(my_nelder_mead->points_nb ==0)
		{
			//on recupere la distance par rapport à la position de base (000)
			my_nelder_mead->distances[0] = neurone[debutdistGpe].s1;

			//ajout de l'exeption "les angles sont deja correct"
			if(my_nelder_mead->distances[0]< epsilonn)
			{
				dprints("le point est deja correct des l'init\n");
				for(i=0;i< (my_nelder_mead->dimension);i++)
				{
					my_nelder_mead->flagFinAlgo = 1;
					my_nelder_mead->points_nb = my_nelder_mead->needed_points_nb; //on triche pour qu'il s'arrete dans l'init
					neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[0][i];
				}
			}
							
			else //cas sans exceptions
			{
				// on donne les angles de bases correspondant a la distance recupérée
				for(i=0;i<my_nelder_mead->dimension;i++){
					my_nelder_mead->points[0][i] = neurone[debutcommandGpe+i].s1; //aleatAngleSimplex(); 
					my_nelder_mead->points[1][i] = neurone[debutcommandGpe+i].s1; //le deuxieme point est init pareil...
				}
			
				//on ajoute une valeur entre 0 et 1 dans l'une des dimension pour assuré linéairement indep
				my_nelder_mead->points[1][0] += 0.5;//(rand()/(float)RAND_MAX ) ;
				if(my_nelder_mead->points[1][0] >1) my_nelder_mead->points[1][0] -= 1;
				
				for(i=0;i< (my_nelder_mead->dimension);i++)
				{
					neurone[debut].s1 = neurone[debut].s2 = neurone[debut].s = my_nelder_mead->points[1][i];
				}
				my_nelder_mead->points_nb = 2; //on a ainsi initialisé les deux premiers points
			}
		}
		else //pas deux premiers points
		{
			// On n’a pas assez de points mais plus de deux
			indexprecedent = my_nelder_mead->points_nb -1;
			//on récupere la distance du point précédemment crée
			my_nelder_mead->distances[indexprecedent] = neurone[debutdistGpe].s1; 
			
			
			//cas exception : si la distance est deja convenable on arrête l'algorithme
			if(neurone[debutdistGpe].s1<epsilonn)
			{
				dprints("On est dans un cas exceptionnel d'angles deja correct\n");
				my_nelder_mead->flagFinAlgo = 1;//on precise que l'algo est fini
				my_nelder_mead->points_nb = my_nelder_mead->needed_points_nb; //on triche pour arret init
				for(i=0;i< (my_nelder_mead->dimension);i++)
				{	
					//on met sur x0 les angles du point correct
					my_nelder_mead->points[0][i] = my_nelder_mead->points[indexprecedent][i];
					neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[0][i];
				}
			}
			
			else //cas sans exception
			{
				//premiere etape on copie les valeurs de "x0" (sauf pour une dimension)
				for(j=0;j<(my_nelder_mead->dimension);j++)
				{
					my_nelder_mead->points[indexprecedent+1][j] = my_nelder_mead->points[0][j]; 
					neurone[debut+ j].s1 = neurone[debut + j].s2 = neurone[debut + j].s = my_nelder_mead->points[(my_nelder_mead->points_nb)][j];
				}
				//on ajoute "+90" dans l'une des dimension pour assuré linéairement indep
				my_nelder_mead->points[indexprecedent+1][indexprecedent] += 0.5;//(rand()/(float)RAND_MAX );
				if(my_nelder_mead->points[indexprecedent+1][indexprecedent] >1) my_nelder_mead->points[indexprecedent+1][indexprecedent] -= 1;
				
				neurone[debut+ indexprecedent].s1 = neurone[debut + indexprecedent].s2 = neurone[debut + indexprecedent].s = my_nelder_mead->points[indexprecedent+1][indexprecedent];			
				my_nelder_mead->points_nb++;
				if(my_nelder_mead->points_nb==my_nelder_mead->needed_points_nb)
				{
					dprints("fin init:\n");
					for(i=0;i<my_nelder_mead->points_nb;i++)
					{
						for(j=0;j<my_nelder_mead->dimension;j++)
						{
							dprints("%f ", my_nelder_mead->points[i][j]);
						}
						dprints("\n");
					}
					dprints("\n");
				}
			}

		}

	}
	else //si on a tous les points
	{
		//pour facilité la compréhension quelques variables
		positionBar = nbPoint;
		positionRef = nbPoint + 1;
		positionEtir = nbPoint + 2;
		positionTild = nbPoint + 3;
		//EtapeBarycentre
		if(my_nelder_mead->flagBar == 1)
		{	
			//on récupère la distance du barycentre précédemment calculé au point voulu 
			my_nelder_mead->distances[positionBar] = neurone[debutdistGpe].s1;
			//dprints("distance barycentre : %f position %d\n", my_nelder_mead->distances[positionBar], positionBar);
			
			//calcul de dk
			finAlgo= calculDk(my_nelder_mead->points[positionBar],my_nelder_mead->points[nbPoint -1],dimension);
			if(finAlgo==1)
			{
				
				//dprints("Fin algo : distance %f, points : \n");
				for(i=0;i< (my_nelder_mead->dimension);i++)
				{
					dprints("%f ", my_nelder_mead->points[0][i]);
					neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[0][i];
				}
				dprints("\n");
				my_nelder_mead->flagFinAlgo=1;
			}
			else{		
				my_nelder_mead->nbIter++;	
				//calcul de la position de la reflexion
				calculReflexion(my_nelder_mead->points[positionRef], my_nelder_mead->points[positionBar], my_nelder_mead->points[nbPoint-1]/*xn+1*/, dimension);

				//on rempli le neuronne de sortie
				for(i=0;i< my_nelder_mead->dimension;i++)
				{
					neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[positionRef][i];
				}
			
				//on passe à l'étapeReflextion
				my_nelder_mead->flagRef=1;

			}
			my_nelder_mead->flagBar=0;
		}
		//etapeFinAlgo
		else if(my_nelder_mead->flagFinAlgo == 1)
		{
			//dprints("fin algo : distance x1 : %f\t Nombre d'itérations : %d\n", my_nelder_mead->distances[0], my_nelder_mead->nbIter);
			//on a trouvé les meilleurs points pour se rapprocher suffisemment
			//dprints("Angles : ");
			for(i=0;i< (my_nelder_mead->dimension);i++)
			{
				//dprints("%f, ", my_nelder_mead->points[0][i]);
				neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[0][i];
			}
			//dprints("\n");
		}
		//EtapeReflection
		else if((my_nelder_mead->flagRef) == 1)
		{
			//on récupère la distance de la reflexion précédement calculé au point voulu
			my_nelder_mead->distances[positionRef] = neurone[debutdistGpe].s1;
			//dprints("distance reflection : %f position %d\n", my_nelder_mead->distances[positionRef], positionRef);
	
			//dprints("distance xr = %f x1 = %f xn = %f xn+1 = %f\n", my_nelder_mead->distances[positionRef], my_nelder_mead->distances[0],my_nelder_mead->distances[nbPoint-2],my_nelder_mead->distances[nbPoint-1]);
			//etape 4
			//si f(xr)<f(x1) alors on cherche au dela de xr
			if((my_nelder_mead->distances[positionRef]) < (my_nelder_mead->distances[0]))
			{
				//dprints("fr<f(x1)\n");
				calculEtirement(my_nelder_mead->points[positionEtir],my_nelder_mead->points[positionRef], my_nelder_mead->points[positionBar], dimension);				
				/*dprints("\n\nappel a un etirement :\n\n");
				for(j=0;j<dimension;j++)
				{
					dprints("%f\t", my_nelder_mead->points[positionEtir][j]);
				}  
				dprints("\n");
				*/
				my_nelder_mead->flagEtir=1;
			}
			//etape5
			else if((my_nelder_mead->distances[nbPoint-2]/*xn*/) > (my_nelder_mead->distances[positionRef])) //ici on compare avec xn
			{
				//dprints("fxn>fr>=fx1 avec fxn = %.10f fr = %.10f\n", my_nelder_mead->distances[nbPoint-2], (my_nelder_mead->distances[positionRef]));
				copy(my_nelder_mead->points[positionRef], my_nelder_mead->points[nbPoint-1]/*xn+1*/, dimension);//copie dans xn+1
			}
			
			//etape 6 : contraction : on recherche avant xr
			else//(fr >= fxn )
			{
				//si dxr < dxn+1 on calcul la contraction par rapport a xr et xc
				if(my_nelder_mead->distances[nbPoint-1]/*xn+1*/ > my_nelder_mead->distances[positionRef])
				{
					//dprints("dxr < dxn+1\n");
					calculxtild(my_nelder_mead->points[positionTild], my_nelder_mead->points[positionRef], my_nelder_mead->points[positionBar], dimension);
				}else //sinon par rapport a xn+1 et xc
				{
					//dprints("dxr >= xn+1");
					calculxtild(my_nelder_mead->points[positionTild], my_nelder_mead->points[nbPoint-1], my_nelder_mead->points[positionBar], dimension);
				}
				
				//on remplit le neuronne de sortie
				for(i=0;i< (my_nelder_mead->dimension);i++)
				{
					neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[positionTild][i];
				}
				
				//et on passe au calcul de distance de la contraction
				my_nelder_mead->flagContrac = 1;
				my_nelder_mead->flagRef = 0;
				
				return; //car on veut pas faire la suite
			}
		
			//on remplit le neuronne de sortie
			for(i=0;i< (my_nelder_mead->dimension);i++)
			{
				neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[nbPoint-1][i];
			}
			my_nelder_mead->flagRef=0;
			//dprints("fin etape reflexion\n");
		}
		//etape pi = (pi + p1) /2
		else if(my_nelder_mead->flagRemplissage == 1)
		{
			//lecture distance du point en cours
			my_nelder_mead->distances[my_nelder_mead->points_nb-1] = neurone[debutdistGpe].s1;
			
			//si il reste des points à remplir
			if(my_nelder_mead->points_nb < my_nelder_mead->needed_points_nb) 
			{
				for(i=0;i<my_nelder_mead->dimension;i++)
				{
					//xi = (xi + x1) /2
					my_nelder_mead->points[my_nelder_mead->points_nb][i] = (my_nelder_mead->points[my_nelder_mead->points_nb][i] + my_nelder_mead->points[0][i])/2;
				}
				my_nelder_mead->points_nb += 1;
			}
			else{
				my_nelder_mead->flagRemplissage =0;
			}
		}
		//etape de contraction
		else if(my_nelder_mead->flagContrac == 1)
		{
			//on calcul la distance de xtild
			my_nelder_mead->distances[positionTild] = neurone[debutdistGpe].s1; 
			//dprints("distance tild : %f position %d\n", my_nelder_mead->distances[positionTild], positionTild);
			
			if(my_nelder_mead->distances[positionTild] >= my_nelder_mead->distances[nbPoint -1])//xtild > fn+1
			{
				//dprints("On a pas trouvé de points interessant (dxc >= dxn+1) : on se rapproche de x1\n");
				//remettre pour tout xi : xi = (x1+xi)/2
				my_nelder_mead->points_nb = 1;
				for(i=0;i<my_nelder_mead->dimension;i++)
				{
					// xi = (xi + x1) /2
					my_nelder_mead->points[my_nelder_mead->points_nb][i] = (my_nelder_mead->points[my_nelder_mead->points_nb][i] + my_nelder_mead->points[0][i])/2;
				}
				my_nelder_mead->points_nb += 1;
				
				my_nelder_mead->flagRemplissage = 1;
			}
			else //xtild<= <xn+1 => xn+1 =  xtild
			{
				 //dprints("dxc < dxn+1  : xn+1 <- xc\n");
				 my_nelder_mead->distances[nbPoint -1] = my_nelder_mead->distances[positionTild] ;
				 for(i=0;i<my_nelder_mead->dimension;i++)
				 {
					my_nelder_mead->points[nbPoint - 1][i] = my_nelder_mead->points[positionTild][i];
				  }
			}
			my_nelder_mead->flagContrac = 0;
			
		}
		//Etape Etirement
		else if((my_nelder_mead->flagEtir)==1)
		{

			//on calcul la distance de l'etirement par rapport au but
			my_nelder_mead->distances[positionEtir] = neurone[debutdistGpe].s1; 
			//dprints("distance etirement : %f position %d\n", my_nelder_mead->distances[positionEtir], positionEtir);
			
			if((my_nelder_mead->distances[positionEtir])<(my_nelder_mead->distances[positionRef]))
			{
				copy(my_nelder_mead->points[positionEtir], xtild, dimension); //ici xn+1
			}else
			{
				copy(my_nelder_mead->points[positionRef], xtild, dimension);
			}
			copy(xtild, my_nelder_mead->points[nbPoint-1], dimension); //copie dans xn+1

			//on rempli le neuronne de sortie
			for(i=0;i< (my_nelder_mead->dimension);i++)
			{
				neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[nbPoint-1][i];
			}

			my_nelder_mead->flagEtir = 0;
			//dprints("fin calcul etirement\n");
		}
		//Debut algo : 
		else
		{	
			//on récupere ici le nouveau calcul de xn+1 à partir de la position précédement calculé
			my_nelder_mead->distances[nbPoint-1]= neurone[debutdistGpe].s1; 
			
			//dprints("distance xn+1 : %f position %d\n", my_nelder_mead->distances[nbPoint-1], nbPoint -1);
			
			tri_rapide(my_nelder_mead->distances, my_nelder_mead->points, 0, nbPoint-1); //derniers arguments : extremités du tableau
			
			/*dprints("apres tri\n");
			for(i=0;i<nbPoint;i++)
			{
				dprints("x[%d]%f\t", i, my_nelder_mead->distances[i]);
			}*/
			
			//calcul de xo centre de gravité des xi (ie 7 angles moyen puis on fait distance ?)
			calculBarycentre(my_nelder_mead->points[positionBar], my_nelder_mead->points, dimension);
			//on rempli le neuronne de sortie
			for(i=0;i< (my_nelder_mead->dimension);i++)
			{
				neurone[debut+i].s1 = neurone[debut+i].s2 = neurone[debut+i].s = my_nelder_mead->points[positionBar][i];
			}
			my_nelder_mead->flagBar = 1;
			//dprints("fin debut algo\n");
		}	
	}
}
