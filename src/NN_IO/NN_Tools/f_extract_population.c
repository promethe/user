/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_extract_population.c
\brief 

Author:  de Rengerv� Antoine
Created: 26-08-2010
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Extraction d'un groupe de neurones a l'interieur d'une population de neurones.
Les liens sont : 
    population : la population de neurones
    extract : le premier neurone de ce groupe est le decalage en x, et le deuxieme neurone de ce groupe est le decalage en y. Ce decalage est normalise par rapport a la taille en x et la taille en y du groupe population.
 Attention : ces valeurs doivent rester en dessous de valeurs critiques (<1) qui correspondent a conserver la fenetre d'extraction a l'interieur du groupe population.


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo 
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG 1
#include <net_message_debug_dist.h>

typedef	struct		s_extract_population
{
	int			prev_first_dec;

	int			prev_first_pop;
	int prev_taillex_pop;
	int prev_tailley_pop;

	int			first;
	int			nbre;

}			t_extract_population;

void function_extract_population(int numero)
{
	t_extract_population	*data = NULL;
	int			prev_first_dec;

	int			prev_first_pop;
	int prev_taillex_pop;
	int prev_tailley_pop;
	int size_x, size_y;
	float x_offset, y_offset;
	int x_dec = 0, y_dec = 0;

	int			first;
	int			nbre;


	int			link = -1;
	int			i = -1, j=-1, pos;

	dprints("f_extract_population (%s) : entree\n",def_groupe[numero].no_name);

	if (def_groupe[numero].data == NULL)
	{
		data = ALLOCATION(t_extract_population);

		data->first = def_groupe[numero].premier_ele;
		data->nbre = def_groupe[numero].nbre;



		for (i = 0; (link = find_input_link(numero, i)) >= 0; i++)
		{
			if (!strcmp(liaison[link].nom, "extract"))
			{
				data->prev_first_dec = def_groupe[liaison[link].depart].premier_ele;
				if (def_groupe[liaison[link].depart].nbre < 2) {
					EXIT_ON_ERROR("In (%s): Check number of neurons on extract : there must be at least 2 neurons : first one is x offset and second is y offset for extraction\n",def_groupe[numero].no_name);
				}
			}
			if (!strcmp(liaison[link].nom, "population"))
			{
				data->prev_first_pop = def_groupe[liaison[link].depart].premier_ele;
				data->prev_taillex_pop = def_groupe[liaison[link].depart].taillex;
				data->prev_tailley_pop = def_groupe[liaison[link].depart].tailley;
			}
		}



		def_groupe[numero].data = (void *)data;
	}
	else
		data = (t_extract_population *)def_groupe[numero].data;

	first = data->first;
	nbre = data->nbre;
	prev_first_dec = data->prev_first_dec;
	prev_first_pop = data->prev_first_pop;
	prev_taillex_pop = data->prev_taillex_pop;
	prev_tailley_pop = data->prev_tailley_pop;

	size_x = def_groupe[numero].taillex;
	size_y = def_groupe[numero].tailley;
	x_offset = neurone[prev_first_dec].s2;
	y_offset = neurone[prev_first_dec+1].s2;
	if (x_offset<0 || y_offset < 0) /*Pas de position en entree. Tous les neurones mis a zero*/
	{
		for(i=0; i< size_x;i++)
			for(j=0; j<size_y;j++)
			{
				neurone[first + j*size_x+i].s = 0;
				neurone[first + j*size_x+i].s1 = 0;
				neurone[first + j*size_x+i].s2 = 0;
			}
		return;
	}
	x_dec = (int)(x_offset*prev_taillex_pop);
	y_dec = (int)(y_offset*prev_tailley_pop);

	dprints("Offsets are : x=%f (%d) and y=%f(%d)\n",x_offset, x_dec, y_offset, y_dec);

	if(x_dec<0 || y_dec < 0) {
		EXIT_ON_ERROR("Extract_population (%s) : selected shift cannot be negative!! (Shifts are : x=%d and y=%d)\n",def_groupe[numero].no_name,x_dec, y_dec);
	}
	if(x_dec+size_x > prev_taillex_pop+1 || y_dec+size_y > prev_tailley_pop+1) /*ajout du +1 : si x_offset=1 x_dec = prev_taillex_pop, ce qui generait une erreur*/
	{
		EXIT_ON_ERROR("Extract_population (%s) : selected shift is out of range in population group!! (Shifts are : x=%d>%d and y=%d>%d)\n",def_groupe[numero].no_name,x_dec+size_x,prev_taillex_pop, y_dec+size_y,prev_tailley_pop);
	}

	/* initialisation */
	for (i = 0; i < nbre; i++)
		neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0;


	for(i=0; i< size_x;i++)
		for(j=0; j<size_y;j++) {
			pos = prev_first_pop + (y_dec+j)*prev_taillex_pop+(x_dec+i);
			neurone[first + j*size_x+i].s = neurone[pos].s;
			neurone[first + j*size_x+i].s1 = neurone[pos].s1;
			neurone[first + j*size_x+i].s2 = neurone[pos].s2;
		}

	dprints("f_extract_population : sortie\n",def_groupe[numero].no_name);
}
