/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_choose_input.c
\brief enable to choose the one output we want to use in the next group

Author: Oriane Dermy
Created: 13/06/13

Theoritical description:

Description:
 This file contains the C fonction for let the choice of the output we want to use in the next group. Normally, it works for all types of functions

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options
- O : (int) index of the previous "images_table's cell" we want to use in the next group

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
//#define DEBUG 1
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>

typedef struct MyData{
  int inputGpe; //input group number
  int output; //the output table we choose
  prom_images_struct *gpeIn; //struct in input
} MyData; 


void new_choose_input(int Gpe)
{  
    int l, i;
  	MyData *mydata=NULL;
	char param[256];
 
    dprints("Begining of new_choose_input\n");
 		
    mydata=(MyData*)malloc(sizeof(MyData));

    if(mydata==NULL) 
    {
      dprints("mydata is NULL!\n");
      EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
    }
		
    mydata->inputGpe=-1; //initialisation of the input group
    mydata->output = 0; //by default we take the first cell
    mydata->gpeIn=NULL;
		
/******************************************************Links Treatments*************************************/		
	dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
       i=0;
       l = find_input_link(Gpe, i);
       while (l != -1)
		{
			mydata->inputGpe = liaison[l].depart;
			mydata->gpeIn = (prom_images_struct *) def_groupe[(mydata->inputGpe)].ext;
	
			if (prom_getopt(liaison[l].nom, "-O", param) !=2 ) //not int
			{
				dprints("Warning in choose_input : Option -O is expecting an integer. By default it will be cell 0.\n");
				break;
			}
            else
			{
				mydata->output= atoi(param);				
				dprints("We choose the output number %d\n", mydata->output);			
				break;
			}
			i++;
			l = find_input_link(Gpe, i);
		}

		if (mydata->inputGpe == -1)
		{
			dprints("%s : No group in input !\n", __FUNCTION__);
			kprints("%s : No group in input !\n", __FUNCTION__);
            EXIT_ON_ERROR("xxxxx %d\n",Gpe);
		}
		dprints("Groupe input is : %s\n", def_groupe[mydata->inputGpe].no_name);
		if(mydata->gpeIn == NULL)
		{
			dprints("Gpe %s : the input group has his ext NULL\n", def_groupe[Gpe].no_name);
			kprints("Gpe %s : the input group has his ext NULL\n", def_groupe[Gpe].no_name);
			//return;
		}
	}
	dprints("End of the reading links\n");		
/****************************************End Links Treatments**********************************/ 
  	def_groupe[Gpe].data=mydata; /*Save of My_Data*/
  	
  	dprints("End of new_choose_input\n");
}


void destroy_choose_input(int gpe)
{
    free(def_groupe[gpe].data);
    def_groupe[gpe].data=NULL;
    def_groupe[gpe].ext=NULL;
}

void function_choose_input(int Gpe)
{
	unsigned int nx, ny, nb_band;
  	MyData *mydata=NULL;
  	prom_images_struct *InputGpe=NULL;
  	
	dprints("At the begining of the function choose input\n");

	if (def_groupe[Gpe].ext != NULL) return;

  	mydata = def_groupe[Gpe].data; //the output group
	
	if((def_groupe[mydata->inputGpe].ext)==NULL)
	{
		dprints("error : the input is null ?\n");
        return;
	}   
	
	InputGpe = (prom_images_struct *) (def_groupe[mydata->inputGpe].ext);  //the input group
	
	//Verify if the selected table exist
	if(mydata->output < 0 || mydata->output >= (int) InputGpe->image_number)
    {
		dprints("Error the table you selected doesn't exist. By default : 0\n");
		mydata->output=0;
	}
	
	//values of the struct_prom_image
	nx = InputGpe->sx;		
	ny = InputGpe->sy;
	nb_band=InputGpe->nb_band;

    //Begin of the def_groupe[Gpe].ext's fill
    def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
	((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
	((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nb_band;
	((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1; //because we take only the selected cell of the table
	((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = InputGpe->images_table[mydata->output]; //the selected output
    if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
	{
	   dprints("%s: IMPOSSIBLE ALLOCATION ...! \n", __FUNCTION__);
       kprints("%s:%d : IMPOSSIBLE ALLOCATION ...! \n", __FUNCTION__, __LINE__);
       EXIT_ON_ERROR("xxxxx %d\n",Gpe);
    }
    	
	dprints("end of choose_input Gpe %d : %s\n", Gpe, __FUNCTION__);
}
