/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: Maickael Maillard
Created: 01/04/2004
Modified:
- author: M. Maillard
- description: specific file creation
- date: 19/09/2005

Theoritical description:

Description: 
Cette fonction permet d'inverser les sorties d'un groupe de neurones.
Les groupes doivent avoir le meme nombre de neurones.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/



#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

#undef DEBUG

void function_inverse_gpe_population(int Gpe)
{
    int lien;
    int deb_gpe = -1;
    int deb_gpe_input = -1;
    int input_gpe = -1;
    int longueur2, longueur, i, long_inv, tmp, tmp2;

    if ((lien = find_input_link(Gpe, 0)) == -1)
    {
        printf
            ("Erreur de lien entrant dans function_add_local_to_global : gpe %d\n",
             Gpe);
        exit(1);
    }
    input_gpe = liaison[lien].depart;
    deb_gpe_input = def_groupe[input_gpe].premier_ele;
    longueur2 = def_groupe[input_gpe].nbre;

    deb_gpe = def_groupe[Gpe].premier_ele;
    longueur = def_groupe[Gpe].nbre;

    if (longueur2 != longueur)
    {
        printf("Longueur mismatch in f_inverse_gpe_population gpe %d\n", Gpe);
        exit(1);
    }

    long_inv = deb_gpe_input + longueur2 - 1;

    for (i = 0; i < longueur; i++)
    {
        tmp = i + deb_gpe;
        tmp2 = long_inv - i;
        neurone[tmp].s2 = neurone[tmp2].s2;
        neurone[tmp].s1 = neurone[tmp2].s1;
        neurone[tmp].s = neurone[tmp2].s;
    }

}
