/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

\defgroup f_ext_plus_neurone f_ext_plus_neurone
\ingroup libNN_IO


\brief Rassemble des neurones (lien neuron) et un ext (lien ext)

\file  
\ingroup f_ext_plus_neurone

\brief Rassemble des neurones (lien neuron) et un ext (lien ext)

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004


************************************************************/
/* #define DEBUG */
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


#define	MAX_PATH		256

typedef	struct		s_ext_plus_neurone
{
  int			first;
  int			nr;

  int			gpe_ext;

  int			gpe_neuron;
  int			gpe_neuron_first;
  int			gpe_neuron_nr;
}			t_ext_plus_neurone;

void		function_ext_plus_neurone(int numero)
{
  int		i = 0;
  int		l = 0;
  int		gpe_ext = 0;
  int		gpe_neuron = 0;
  int		gpe_neuron_first;
  int		gpe_neuron_nr;
  int		first = 0;
  int		nr = 0;
  t_ext_plus_neurone	*data = NULL;

  if (def_groupe[numero].data == NULL)
  {

    data = ALLOCATION(t_ext_plus_neurone);
    memset(data, 0, sizeof(t_ext_plus_neurone));
    data->gpe_ext = -1;
    data->gpe_neuron = -1;

    while ((l = find_input_link(numero, i)) != -1)
    {
      if (strcmp(liaison[l].nom, "ext") == 0)
      {
	data->gpe_ext = liaison[l].depart;
      }
      else if (strcmp(liaison[l].nom, "neuron") == 0)
      {
	data->gpe_neuron = liaison[l].depart;
	data->gpe_neuron_first = def_groupe[liaison[l].depart].premier_ele;
	data->gpe_neuron_nr = def_groupe[liaison[l].depart].nbre;
      }
      i++;
    }
    if ((data->gpe_ext == 0) || (data->gpe_neuron == 0))
    {
      PRINT_WARNING("f_ext_plus_neurone : lien entrant \"ext\" ou \"neuron\" manquant.\n"
		    "si \"ext\" est manquant il sera mis � NULL.\n"
		    "si \"neuron\" est manquant il seront mis a NULL.\n");
    }
    data->first = def_groupe[numero].premier_ele;
    data->nr = def_groupe[numero].nbre;

    if (data->nr != data->gpe_neuron_nr)
    {
      PRINT_WARNING("f_ext_plus_neurone : le nombre de neurones est incoherent :\n"
		    "moi : %i\ngroupe \"neuron\" : %i\ncertains neurones seront mis a 0 ou ignores.\n",
		    data->nr, data->gpe_neuron_nr);
    }

    def_groupe[numero].data = (void *)data;
  }
  else
    data = (t_ext_plus_neurone *)def_groupe[numero].data;
  gpe_ext = data->gpe_ext;
  gpe_neuron = data->gpe_neuron;
  gpe_neuron_first = data->gpe_neuron_first;
  gpe_neuron_nr = data->gpe_neuron_nr;
  nr = data->nr;
  first = data->first;

  /**
   ** On remplit le ext. (en fait ptr .ext sur le .ext du groupe precedent)
   */
  if (gpe_ext >= 0)
  {
    def_groupe[numero].ext = def_groupe[gpe_ext].ext;
  }

  /**
   ** On remplit les neurones
   */
  if (gpe_neuron >= 0)
  {
    for (i = 0; (i < nr) && (i < gpe_neuron_nr); i++)
    {
	  
      dprints("neurone %i = %f\n", i, neurone[gpe_neuron_first + i].s2);

      neurone[first + i].s = neurone[gpe_neuron_first + i].s;
      neurone[first + i].s1 = neurone[gpe_neuron_first + i].s1;
      neurone[first + i].s2 = neurone[gpe_neuron_first + i].s2;
    }
  }
}

