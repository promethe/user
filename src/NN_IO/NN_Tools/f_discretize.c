/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: P. Gaussier
Created: 01/04/2005
Modified:
- author: G. Merle
- description: ajout des fonctions new_discretize() et destroy_discretize(), ajout d'une option de discrétisation douce
- date: 09/05/2011

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction qui lit une valeur sur le groupe d'entrée et active le neurone correspondant du groupe de sortie.
Codage par une population de neurones avec mise à l'échelle. Le nombre de neurone représente l'inverse du pas de discretisation. On peut spécifier un min et un max sur le lien + le type de projection: lineaire ou logarithmique.
La fonction reprend du code de f_read_and_map_angle_ana.c Elles pourraient etre fusionnées.

Cette fonction est aussi tres proche de f_extract_val_to_vector (elles pourraient etre fusionnées).

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo 
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo: ajout gestion de plusieurs entrees (creation matrice?)

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct discretize_data {
    int debe;                   /* position neurone entree */
    float scale;                /* echelle pour la discretisation */
    float offset;               /* offset de la discretisation */
    int logs;                   /* 0 échelle proportionnelle, 1 échelle logarithmique */
    int soft;                   /* 0 discrétisation dure, 1 discrétisation douce */
    int warn;                   /* 0 pas de warnings, 1 warnings si entrée out of range */
} discretize_data;

void new_discretize(int gpe) {
    int debe;
    int l;
    float vmin = 0, vmax = 0, scale = 0;
    float offset = 0.;
    int flag = 0;
    int logs = 0;
    int soft = 0;
    int warn = 0;
    char chaine[255];
    discretize_data *data = NULL;


#ifdef DEBUG
    printf("New_discretize(%d)\n", gpe);
#endif

    if (def_groupe[gpe].data == NULL) {
        /* get the input link */
        l = find_input_link(gpe, 0);
        if (l == -1) {
            fprintf(stderr,"error: in new_discretize(gpe = %i): the group should have one input group!\n", gpe);
            exit(-1);
        }

        if (prom_getopt(liaison[l].nom, "m", chaine) == 2) { /* valeur analogique min */
            flag = flag + 1;
            vmin = -atof(chaine);
#ifdef DEBUG
            printf("m= %s\n", chaine);
#endif
        }
        if (prom_getopt(liaison[l].nom, "M", chaine) == 2) { /* valeur analogique max */
            flag = flag + 1;
            vmax = atof(chaine);
#ifdef DEBUG
            printf("M= %s\n", chaine);
#endif
        }
        if (prom_getopt(liaison[l].nom, "l", chaine) != 0) { /* type d'echelle : linéaire (default) ou log */
            logs = 1;
        }
        if (prom_getopt(liaison[l].nom, "s", chaine) != 0) { /* type de discrétisation, dure (default) ou douce */
            soft = 1;
        }
        if (prom_getopt(liaison[l].nom, "w", chaine) != 0) { /* warnings ou non (default) */
            warn = 1;
        }
        if (flag < 2) {
            fprintf(stderr,"error: in new_discretize(gpe = %i): you must specify -m -M arguments on the link ex: -m0.0-M1.0 (-min and +max values)\n", gpe);
            fprintf(stderr,"note: vmin = %f, vmax = %f, ech_log = %d, soft = %d \n", vmin, vmax, logs, soft);
            exit(1);
        }

        debe = def_groupe[liaison[l].depart].premier_ele;

        if (!logs) {
            scale = def_groupe[gpe].nbre / (vmax - vmin);
            offset = -vmin * scale;
        } else {
            scale = log((float) def_groupe[gpe].nbre) / log(vmax - vmin);
            /* attention au signe de vmin quel est le bon calcul??? */
            offset = -(log(vmin) * scale);
        }

        def_groupe[gpe].data = data = (discretize_data *) malloc(sizeof(discretize_data));
        if (data == NULL) {
            fprintf(stderr,"error: in new_discretize(gpe = %i): malloc failed\n", gpe);
            exit(1);
        }
        data->debe = debe;
        data->scale = scale;
        data->offset = offset;
        data->logs = logs;
        data->soft = soft;
        data->warn = warn;
    }
}

void destroy_discretize(int gpe) {
    if (def_groupe[gpe].data != NULL) {
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

void function_discretize(int gpe) {
    int deb, debe, nbr;
    int i, index;
    float scale, offset, val, pos;
    discretize_data *data = (discretize_data *) def_groupe[gpe].data;
    
    if (data == NULL) return;
    
    deb = def_groupe[gpe].premier_ele;
    nbr = def_groupe[gpe].nbre;
    debe = data->debe;
    scale = data->scale;
    offset = data->offset;
    
    /* zeroing everything */
    for (i = deb; i < deb + nbr; ++i) {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    }

    val = neurone[debe].s1;
    
    if (data->logs) /* logarithmic scale */
    val = log(val);
    
    /* compute the index */
    pos = (val * scale) + offset;
    index = floor(pos);
    
    if (pos < 0) {
        if (data->warn) {
            fprintf(stderr, "warning: in f_discretize(gpe = %i), the index is negative = %f\n",
                gpe, pos);
            fprintf(stderr, "note: val = %f, scale = %f, offset = %f\n", val, scale, offset);
            fprintf(stderr, "note: decrease the -m value of the link \n");
        }
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 1.;
    } else if (index >= nbr-1) {
        if (index >= nbr && data->warn) {
            fprintf(stderr, "warning: f_discretize(gpe = %i), the index is too high = %i\n",
                gpe, index);
            fprintf(stderr, "note: val = %f, scale = %f, offset = %f\n", val, scale, offset);
            fprintf(stderr, "note: increase the -M value of the link \n");
        }
        neurone[deb+nbr-1].s = neurone[deb+nbr-1].s1 = neurone[deb+nbr-1].s2 = 1.;
    } else if (!data->soft) { /* hard discretisation */
        neurone[deb+index].s = neurone[deb+index].s1 = neurone[deb+index].s2 = 1.;
    } else { /* soft discretisation */
        neurone[deb+index].s = neurone[deb+index].s1 = neurone[deb+index].s2 = 1. - (pos - index);
        neurone[deb+index+1].s = neurone[deb+index+1].s1 = neurone[deb+index+1].s2 = (pos - index);
    } 
    
}

