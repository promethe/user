/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_merge_potentials f_merge_potentials
\ingroup libNN_IO

\brief Set s0,s1,s2 from different input layers given by algorithmic links

\details

\section Description
 
 Recupere le potentiel s1 sur les groupes pointes par des liens algo s ou s1 ou s2 (nom sur le lien d'entree) et les mets sur le niveau d'activite correspondant du groupe merge_potential

\section Notes

 Utilisation : permet de gerer des informations specifiques non
 connues a priori pour les mettre sur un niveau de sortie du neurone
 arbitraire. Interet particulier : initialiser un attribut de neurone
 pour permettre ensuite une modulation locale liee a l'entree.

\file  
\ingroup f_merge_potentials
\brief 

Author: A. de Rengerve
Created: 27/11/2012

**/

/* #define DEBUG */
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

typedef struct data_mp {
  int gpe_deb;
  int gpe_nbre;
  int gpe_entree_deb_s;
  int gpe_entree_deb_s1;
  int gpe_entree_deb_s2;
} type_data_mp;

/* private */
void check_layer_size(int size1, int size2, int gpe_1, int gpe_2) {
  if(size1 != size2)
    EXIT_ON_ERROR("f_merge_potentials (%s) : erreur : nb neurones differents dans (%s) et (%s) \n",def_groupe[gpe_1].no_name, def_groupe[gpe_2].no_name,def_groupe[gpe_1].no_name);
}

void function_merge_potentials(int num)
{  
   type_data_mp *mydata=NULL;
   int i=0,j;
   int tmp_gpe_id;
   int gpe_entree_deb_s=-1;
   int gpe_entree_deb_s1=-1;
   int gpe_entree_deb_s2=-1;
   int default_entree_deb=-1;
   
   dprints ("execution de merge potential (%s)\n",def_groupe[num].no_name);
   if(def_groupe[num].data==NULL) {
     mydata = ALLOCATION(type_data_mp);
     
     mydata->gpe_deb = def_groupe[num].premier_ele;
     mydata->gpe_nbre = def_groupe[num].nbre;
     
     i=0;
     j=find_input_link(num,i);
     if(j==-1) {
       EXIT_ON_ERROR("merge potential (%s) : You need at least 1 link !\n",def_groupe[num].no_name);
     }
     while(j!=-1) {
       if(strcmp(liaison[j].nom,"s1")==0)
       {
	 /* recuperation de s1*/
	 tmp_gpe_id=liaison[j].depart;
	 gpe_entree_deb_s1=def_groupe[tmp_gpe_id].premier_ele;
	 mydata->gpe_entree_deb_s1=gpe_entree_deb_s1;
	 check_layer_size(mydata->gpe_nbre, def_groupe[tmp_gpe_id].nbre, num, tmp_gpe_id);
	 default_entree_deb=mydata->gpe_entree_deb_s1;
       }
       else if(strcmp(liaison[j].nom,"s")==0)
       {
	 /* recuperation de s*/
	 tmp_gpe_id=liaison[j].depart;
	 gpe_entree_deb_s=def_groupe[tmp_gpe_id].premier_ele;
	 mydata->gpe_entree_deb_s=gpe_entree_deb_s;
	 check_layer_size(mydata->gpe_nbre, def_groupe[tmp_gpe_id].nbre, num, tmp_gpe_id);
	 if(default_entree_deb==-1)
	   default_entree_deb=mydata->gpe_entree_deb_s;
       }
       else if(strcmp(liaison[j].nom,"s2")==0)
       {
	 /* recuperation de s2*/
	 tmp_gpe_id=liaison[j].depart;
	 gpe_entree_deb_s2=def_groupe[tmp_gpe_id].premier_ele;
	 mydata->gpe_entree_deb_s2=gpe_entree_deb_s2;
	 check_layer_size(mydata->gpe_nbre, def_groupe[tmp_gpe_id].nbre, num, tmp_gpe_id);
	 if(gpe_entree_deb_s1==-1)
	   default_entree_deb=gpe_entree_deb_s2;
       }
       else {
	 EXIT_ON_ERROR("merge potential (%s) : link error : should be s, s1 or s2 (%s) !\n",def_groupe[num].no_name,liaison[j].nom);
       }
       /* TODO : allow sync links */
       i++;
       j=find_input_link(num,i);
     }
     /* default init if not defined */
     if(gpe_entree_deb_s1==-1)
	mydata->gpe_entree_deb_s1=default_entree_deb;
     if(gpe_entree_deb_s==-1)
	mydata->gpe_entree_deb_s=default_entree_deb;
     if(gpe_entree_deb_s2==-1)
	mydata->gpe_entree_deb_s2=default_entree_deb;

     def_groupe[num].data = mydata;
   }
   else {
      mydata = def_groupe[num].data;
   }
   
   for(i=0; i<mydata->gpe_nbre; i++) {
     neurone[mydata->gpe_deb+i].s = neurone[mydata->gpe_entree_deb_s+i].s1;
     neurone[mydata->gpe_deb+i].s1 = neurone[mydata->gpe_entree_deb_s1+i].s1;
     neurone[mydata->gpe_deb+i].s2 = neurone[mydata->gpe_entree_deb_s2+i].s1;
   }
   
   dprints("sortie de merge potentials (%s)\n",def_groupe[num].no_name);
}

