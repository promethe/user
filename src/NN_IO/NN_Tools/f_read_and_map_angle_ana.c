/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_read_and_map_angle_ana.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction qui lit un angle entre -Pi et Pi code sur le groupe d'entree et active le neurone correspondant.
Codage de l'angle par une poupulation de neurone avec mise a l'echelle. Le nombre de neurone repesente l'inverse du pas de discretisation. On peut specifie un angle de rotation (defaut=180) indiquant la rotation de la plage ex par defaut =180 sortie: [-180;180[ le neurone representant un angle de 0� est code par le neurone a l'index: longueur/2 au lieu de l'index 0 si [0;360[
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#define PI 3.1415927
#define DEBUG
void function_read_and_map_angle_ana(int numero)
{
    int deb, debe, longueur;
    int i, index;
    int nlink;
    float scale = 0;
    float offset = 0., val = -999.;


#ifdef DEBUG
    printf("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
#endif
    printf("Function_read_and_map_angle_ana(%d)\n", numero);


    /*get the string of the input link */
    nlink = find_input_link(numero, 0);
    if (nlink == -1)
    {
        printf("Error the input group should have one input group!\n");
        exit(-1);
    }

    deb = def_groupe[numero].premier_ele;
    longueur = def_groupe[numero].nbre;
    debe = def_groupe[liaison[nlink].depart].premier_ele;
    if (def_groupe[numero].data == NULL)
    {
        /*val= */
        val = neurone[debe].s2;
                            /*-Pi Pi*/
        def_groupe[numero].data = (void*)1;
    }
    else
        val = neurone[debe].s2;
               /*-Pi Pi*/

    offset = longueur / 2;


    printf("Output range [-180;180[, meanning angle=0� is neurone n�: %f\n",
           offset);

/*Get the input from user*/


    scale = 2 * PI / (float) longueur;
    /*R.A.Z */
    for (i = deb; i < deb + longueur; i++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    }


    /*Compute the index */
    index = (int) (((float) val / scale) + offset);
    /*Set the neuron at the specified index */
    if (index >= 0)
        neurone[index + deb].s = neurone[index + deb].s1 =
            neurone[index + deb].s2 = 1.;
#ifdef DEBUG
    printf
        ("Angle:%f degre soit %f radians, longueur:%d, neurone set:%d, offset:%f\n",
         val, val * 2 * 3.1415927 / 360, longueur, index, offset);
#endif

#ifdef DEBUG
    printf("===========sortie de %s\n", __FUNCTION__);
#endif
}
