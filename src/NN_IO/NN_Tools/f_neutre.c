/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_neutre.c 
\brief 

Author: Boucenna Sofiane
Created: 03/04/2006

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: cette fonction sert juste a� revenir a� l'expression neutre si il n y a pas de lien en entre.
             Ou fait l'expression de tristesse ou de joie si sur le lien on a EXPRESSION

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/*#define DEBUG*/

void function_neutre(int Gpe)
{

    
    int deb;                    /* le premier neurone */
    int gpe_precedent = -1;     /* groupe precedent*/
    int deb_precedent;          /* debut du groupe precedent*/
    int expression;             /* l'expression que le robot va faire*/
    int neutre = 1;             /* neurone neutre */
    int joie = 2;               /* neurone de la joie */ 
    int tristesse = 0;          /* neurone de la tristesse*/
    int i=0;
    int j=0;
    expression = 0;
#ifdef DEBUG
    printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    /*gpe_precedent = find_input_link(Gpe, 0);*/
    deb_precedent = def_groupe[gpe_precedent].premier_ele;

    while( (j=find_input_link(Gpe,i) ) != -1){
      
      if(strcmp(liaison[j].nom,"expression")==0){
	gpe_precedent=liaison[j].depart;
#ifdef DEBUG
	printf("groupe=%d",liaison[j].depart);
#endif
      }

      if(strcmp(liaison[j].nom,"neutre")==0){
	gpe_precedent = -1;
#ifdef DEBUG
	printf("groupe precedent ne donne pas d information\n");
#endif
      }

      i++;
      
#ifdef DEBUG
      printf("on a fini de recuperer les parametres\n");
#endif
    }

    deb_precedent = def_groupe[gpe_precedent].premier_ele;
    deb = def_groupe[Gpe].premier_ele;

    if(gpe_precedent == -1)
      {
	expression = 0;
      }
    else
      {
	expression =(int)neurone[deb_precedent].s; 
#ifdef DEBUG
	printf("je suis dans le else\n");
	printf("expression=  %d\n", expression);
#endif
      }

#ifdef DEBUG
    printf("groupe precedent= %d\n",gpe_precedent);
    printf("groupe precedent= %s\n",def_groupe[gpe_precedent].no_name);
    /*printf("expression= %d\n",expression);*/
#endif

    /**********************************************************************/
    /*********************   activer le neurone gagnant  ******************/
    /**********************************************************************/
    for(i=0;i<5;i++)
      {
	neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2=0.0;
      }

    if( expression == 1)
      {

	neurone[deb + tristesse].s2 =
	  neurone[deb + tristesse].s1 = neurone[deb + tristesse].s = 1.;
#ifdef DEBUG	
	printf("TRISTESSE\n");
#endif
      }

    else
      {
	if( expression == 2)
	  {

	    neurone[deb + joie].s2 =
	      neurone[deb + joie].s1 = neurone[deb + joie].s = 1.;
#ifdef DEBUG
	    printf("JOIE\n");
#endif
	  }
	
	else
	  {

	    neurone[deb + neutre].s2 =
	      neurone[deb + neutre].s1 = neurone[deb + neutre].s = 1.;
#ifdef DEBUG
	    printf("NEUTRE\n");
#endif
	  }
      }


	
#ifdef DEBUG
    printf("--------------end of %s\n", __FUNCTION__);
#endif
}
