/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_orientation.c
\brief 

Author: LAGARDE Matthieu
Created: 07/11/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG 1

typedef	struct	s_orientation
{
  int		prev_gpe_X;
  int		prev_gpe_Y;

  int		prev_nbre_X;
  int		prev_nbre_Y;

  int		prev_first_X;
  int		prev_first_Y;

  int		old_idx_X;
  int		old_idx_Y;

  int		first;
  int		nbre;

  float		pas;
}		t_orientation;

void		function_get_orientation(int numero)
{
  t_orientation	*data = NULL;
  int		link = 0;
  int		i = 0;
  int		idx = -1;

  int		cur_idx_X = -1;
  int		cur_idx_Y = -1;
  int		prev_nbre_X = 0;
  int		prev_gpe_X = -1;
  int		prev_first_X = -1;
  int		prev_first_Y = -1;
  int		prev_nbre_Y = 0;
  int		prev_gpe_Y = -1;
  int		old_idx_X = -1;
  int		old_idx_Y = -1;
  float		pas = 0;
  int		first = -1;
  int		nbre = -1;
  float		angle = 0;


#ifdef DEBUG
  fprintf(stdout, "f_get_orientation : entree\n");
#endif /* DEBUG */

  if (def_groupe[numero].ext == NULL)
    {
      if ((data = malloc(sizeof(t_orientation))) == NULL)
	{
	  fprintf(stderr, "f_get_orientation : Erreur d allocation de donnees\n");
	  perror("malloc");
	  exit(-1);
	}
      data->old_idx_X = -1;
      data->old_idx_Y = -1;

      data->first = def_groupe[numero].premier_ele;
      data->nbre = def_groupe[numero].nbre;
      
      /* Nombre de neurone par degres : 360 degres */
      data->pas = (float)((float)data->nbre / 360.0);

      /* Recherche des 2 vecteurs en entree */
      for (i = 0; (link = find_input_link(numero, i)) >= 0; i++)
	{
	  if (!strcmp(liaison[link].nom, "X"))
	    data->prev_gpe_X = liaison[link].depart;
	  else if (!strcmp(liaison[link].nom, "Y"))
	    data->prev_gpe_Y = liaison[link].depart;
	}

      data->prev_nbre_X = def_groupe[data->prev_gpe_X].nbre;
      data->prev_nbre_Y = def_groupe[data->prev_gpe_Y].nbre;

      data->prev_first_X = def_groupe[data->prev_gpe_X].premier_ele;
      data->prev_first_Y = def_groupe[data->prev_gpe_Y].premier_ele;

      def_groupe[numero].ext = (void *)data;
    }
  else
    data = (t_orientation *)def_groupe[numero].ext;

  prev_gpe_X = data->prev_gpe_X;
  prev_gpe_Y = data->prev_gpe_Y;
  prev_first_X = data->prev_first_X;
  prev_first_Y = data->prev_first_Y;
  prev_nbre_X = data->prev_nbre_X;
  prev_nbre_Y = data->prev_nbre_Y;
  old_idx_X = data->old_idx_X;
  old_idx_Y = data->old_idx_Y;
  pas = data->pas;
  first = data->first;
  nbre = data->nbre;

#ifdef DEBUG
  fprintf(stdout, "f_get_orientation : groupe X = %i - groupe Y = %i - pas = %f\n", prev_gpe_X, prev_gpe_Y, pas);
#endif /* DEBUG */

  /* On recherche le neurone actif sur le vecteur horizontal d entree */
  for (i = 0; i < prev_nbre_X; i++)
    if (neurone[prev_first_X + i].s2 > 0)
      cur_idx_X = i;
  /* On recherche le neurone actif sur le vecteur vertical d entree */
  for (i = 0; i < prev_nbre_Y; i++)
    if (neurone[prev_first_Y + i].s2 > 0)
      cur_idx_Y = i;

#ifdef DEBUG
  fprintf(stdout, "f_get_orientation : cur_idx_X = %i, cur_idx_Y = %i\n", cur_idx_X, cur_idx_Y);
#endif /* DEBUG */

  /* Si on a des valeur anterieures, alors on calcul l angle */
  if ((old_idx_X > -1) && (old_idx_Y > -1))
    {
      int	dist_X = 0;
      int	dist_Y = 0;

      dist_X = cur_idx_X - old_idx_X;
      dist_Y = (cur_idx_Y - old_idx_Y) * -1;	/* * -1 car l axe verticale est inverse dans les groupes de neurones */
      if ((dist_X == 0) && (dist_Y == 0))
	{
	  /* On remet a 0 les neurone */
	  for (i = 0; i < nbre; i++)
	    neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0.0;
	}
      else
	{
	  angle = (180.0 * atan2f((float)(dist_Y), (float)(dist_X))) / M_PI;
	  
#ifdef DEBUG
	  fprintf(stdout, "f_get_orientation : dist_X = %i - dist_Y = %i\n", dist_X, dist_Y);
#endif /* DEBUG */
	  
	  /* Calcul de l index du neurone d apres l angle */
	  if (angle >= 0)
	    idx = (int)(pas * angle);
	  else
	    idx = (int)(pas * (angle + 360.));
	  
	  
	  /* On remet a 0 les neurone */
	  for (i = 0; i < nbre; i++)
	    neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0.0;
	  
	  /* On active le neurone correspondant a l orientation */
	  neurone[first + idx].s = neurone[first + idx].s1 = neurone[first + idx].s2 = 1.0;
	}
#ifdef DEBUG
      fprintf(stdout, "f_get_orientation : angle = %f\n", angle);
#endif /* DEBUG */
	  
    }

  /* On sauvegarde les nouveaux indices pour le calcul suivant */
  data->old_idx_X = cur_idx_X;
  data->old_idx_Y = cur_idx_Y;

#ifdef DEBUG
  fprintf(stdout, "f_get_orientation : sortie\n");
#endif /* DEBUG */

}
