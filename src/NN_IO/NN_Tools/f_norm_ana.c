/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libNN_IO
\defgroup f_norm_ana f_norm_ana
\brief Normalise les activites (.s1) du groupe d'entree de telle sorte que le max
du groupe en sortie soit egale a un parametre de norme (=1 par defaut).

\details

\section Description
  Fonction VB  :   Normalise les activites (.s1) du groupe d'entree de telle sorte que le max
du groupe en sortie vale la norme (1 par defaut).
  Cette fonction est utilise dans la planification pour recuperer	 
  l'intensite de la reconnaissance du lieux le mieux reconnu.	

 Attention: si le max est inferieur au seuil du groupe, il n'y a pas de normalisation de la sortie !

\section Links

 - -norm : si absent, norme vaut 1 par defaut (le max sera egal a 1). Si present avec une valeur : cette valeur sera utilisee comme norme. Si present sans valeur, la sortie s1 du premier neurone du groupe d'entree de ce lien sera prise comme norme.

\file  f_norm_ana.c 
\ingroup f_norm_ana
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author:N.Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 	
	voir module libNN_IO/f_norm_ana

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

/* #define DEBUG */
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/trouver_entree.h>


typedef struct norm_ana_data_s
{
  int deb_gpe;
  int deb_gpe_input;
  int nbre;
  int deb_gpe_norm;
  float norm;
}norm_ana_data;


void function_norm_ana(int gpe_sortie)
{
  int deb,res;
  float max = -9999, val;
  float norm=1.0;
  int gpe_entree, deb_gpe_norm=-1, debe, nbree, i, nbre;
  int link;
  char param[256];
  type_liaison *lien;  
  norm_ana_data *mydata=NULL;
  
  if (def_groupe[gpe_sortie].data == NULL)
  {
    mydata = ALLOCATION(norm_ana_data);
    i=0;
    link = find_input_link(gpe_sortie,i);
    while(link != -1) {
      lien = &liaison[link];
      
      /** On recherche le lien norm */
      res = prom_getopt(lien->nom, "norm", param);
      if ( res == 2) {
	norm=atof(param);
      }
      else if (res ==1) {
	deb_gpe_norm = def_groupe[lien->depart].premier_ele;
	norm=neurone[deb_gpe_norm].s1; 
      }
      i++;
      link = find_input_link(gpe_sortie,i);
    }    
    deb = def_groupe[gpe_sortie].premier_ele;
    nbre = def_groupe[gpe_sortie].nbre;
    gpe_entree = trouver_entree(gpe_sortie, (char*)"to_normalize");
    debe = def_groupe[gpe_entree].premier_ele;
    nbree = def_groupe[gpe_entree].nbre;
    if (nbre != nbree)
    {
      EXIT_ON_ERROR("Error on group norm_ana(%d): group has different size\n",
		    gpe_sortie);
    }
    
    mydata->deb_gpe_norm=deb_gpe_norm;
    mydata->norm=norm;
    mydata->deb_gpe=deb;
    mydata->deb_gpe_input=debe;
    mydata->nbre=nbre;
    def_groupe[gpe_sortie].data=mydata;
  }
  else {
    mydata=def_groupe[gpe_sortie].data;
    debe = mydata->deb_gpe_input;
    nbre = mydata->nbre;
    nbree = nbre;
    deb = mydata->deb_gpe;
    deb_gpe_norm = mydata->deb_gpe_norm;
    if(deb_gpe_norm==-1) {
      norm=mydata->norm;
    }
    else {
      /** on ne tient pas compte de la norme du lien*/
      norm=neurone[deb_gpe_norm].s1; 
    }
  }
  
  /*recherche du max pour la normalisation */
  dprints("function_norm_analogique: %d,entree:%d,norm:%d (Neurones)\n", mydata->deb_gpe,mydata->deb_gpe_input,mydata->deb_gpe_norm);
  for (i = debe; i < debe + nbree; i++)
  {
    neurone[deb + i - debe].s = neurone[deb - debe + i].s1 =
      neurone[deb + i - debe].s2 = 0.;
    val = neurone[i].s1;
    if (max < val)
      max = val;
#ifdef DEBUG
    if (val > 0)
      printf("%f\n", val);
#endif    
  }
  dprints(" max norm_analogique: %f, norm %f\n", max,norm);
  /*normalisation */
  
  for (i = 0; i < nbre; i++)
  {
    if (max > def_groupe[gpe_sortie].seuil) /*       if(max>1.0) */
      neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 =
	norm*neurone[debe + i].s1 / max;
    else
      neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 =
	neurone[debe + i].s1;
/* if(neurone[debe+i].s1>0) printf(" val neurone%d: %f\n",i,neurone[deb+i].s1);*/
  }
}
