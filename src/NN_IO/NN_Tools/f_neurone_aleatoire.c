/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_neurone_aleatoire.c 
\brief 

Author: R. Shirakawa
Created: 03/04/2006

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: randomly choses one neuron every time this function is called. The function uses a gaussian-distributed curve.

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <stdbool.h>


void function_neurone_aleatoire(int Gpe)
{
    int deb;                    /* le premier neurone */
    int longueur;               /* le numero de neurones dans le goupe */
    int i;                      /* variable de boucles */
    int num_aleatoire;          /* la valeur aleatoire entre 0 et longueur-1 */

#ifdef DEBUG
    printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    deb = def_groupe[Gpe].premier_ele;
    longueur = def_groupe[Gpe].nbre;

    /* metre a zero tous les neurones */
    for (i = deb; i < deb + longueur; i++)
    {
        neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;
    }

    /* choisir le neurone gagnant */
    num_aleatoire=rand()%def_groupe[Gpe].nbre;

#ifdef DEBUG
    printf("chosen neuron (in group %s): %d \n", __FUNCTION__, bruit);
#endif

    /* activer le neurone gagnant */
    neurone[deb + num_aleatoire].s2 =
        neurone[deb + num_aleatoire].s1 = neurone[deb + num_aleatoire].s = 1.;

#ifdef DEBUG
    printf("--------------end of %s\n", __FUNCTION__);
#endif
}
