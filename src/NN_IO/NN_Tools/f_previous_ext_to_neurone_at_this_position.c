/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <stdio.h>

/* #define DEBUG 1 */

void f_previous_ext_to_neurone_at_this_position(int num)
{
    int lien = 0;
    int prev_gpe = 0;
    int val = 0;
    int i = 0;
int * my_data;
#ifdef DEBUG
    printf("f_get_pos : entree\n");
#endif
 
    if (def_groupe[num].data == NULL)
    {
        for (lien = 0; lien < nbre_liaison; lien++)
            if (liaison[lien].arrivee == num)
            {
                my_data=(int*)malloc(sizeof(int));
		*my_data=liaison[lien].depart;
		def_groupe[num].data=my_data;
                break;
            }
    }
	my_data=def_groupe[num].data;
    prev_gpe = *my_data;

    if (def_groupe[prev_gpe].ext == NULL)
      return;

    if ((def_groupe[num].taillex > 1) && (def_groupe[num].tailley == 1))
      val = ((int *)def_groupe[prev_gpe].ext)[0];
    else if ((def_groupe[num].tailley > 1) && (def_groupe[num].taillex == 1))
      val = ((int *)def_groupe[prev_gpe].ext)[1];
    else
      return;

    for (i = 0; i < def_groupe[num].nbre; i++)
        if (i == val)
            neurone[def_groupe[num].premier_ele + i].s =
                neurone[def_groupe[num].premier_ele + i].s1 =
                neurone[def_groupe[num].premier_ele + i].s2 = 1;
        else
            neurone[def_groupe[num].premier_ele + i].s =
                neurone[def_groupe[num].premier_ele + i].s1 =
                neurone[def_groupe[num].premier_ele + i].s2 = 0;

#ifdef DEBUG
    printf("f_get_pos : fin\n");
#endif
}
