/**
 \defgroup f_alpha_cell f_alpha_cell
 \ingroup libNN_IO

 \brief réponse phasique d'une cellule ganglionnaire, modele simple.

 \details

 \section Description

 \section Options

 \file
 \ingroup f_alpha_cell

 Author: Nils Beaussé
 Created: 21/04/2015

 Theoritical description:

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

/* #define DEBUG  */

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <NN_IO/NN_Tools.h>

typedef struct alpha_cell_data {
int gpe_dt_entree;
int gpe_A_entree;
int gpe_B_entree;

} alpha_cell_data;

void new_alpha_cell(int gpe)
{
  int link=-1,index=-1;
  alpha_cell_data* my_data;

  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);

    my_data = ALLOCATION(alpha_cell_data);

    def_groupe[gpe].data = (void*) my_data;

  }


}

void function_alpha_cell(int gpe)
{
int i,deb,nbre;
float A=1,temp=0.0;
float activite,DT=0.2;

type_coeff* coeff=NULL;

deb=def_groupe[gpe].premier_ele;
nbre=def_groupe[gpe].nbre;

  for (i = deb; i < deb + nbre; i++)
    {
      temp = 0.;

      coeff = neurone[i].coeff;

      while (coeff != NULL)
      {
        temp = temp + coeff->val * neurone[coeff->entree].s1;
        coeff = coeff->s; /* Lien suivant */
      }

      activite=(fabs(neurone[i].d-temp)/DT);
      neurone[i].s1 = neurone[i].s1 + DT * (-A*neurone[i].s1 + activite);
      neurone[i].s=temp;
      neurone[i].d=temp;

    }

}

void destroy_alpha_cell(int gpe)
{
  if(def_groupe[gpe].data!=NULL)
  {
    free((alpha_cell_data*)def_groupe[gpe].data);
    def_groupe[gpe].data=NULL;

  }
}
