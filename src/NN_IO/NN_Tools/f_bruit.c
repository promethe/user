/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** 
 \defgroup f_bruit f_bruit
 \ingroup libNN_IO

 \brief Genere un signal aleatoire

 \details

 \section Description

 Bruit blanc centre sur 0 dont l'amplitude est egale a la valeur de la variable threshold du groupe.
 Pour un simple bruit blanc entre 0 et 1 utiliser: f_matrice_alea_entre_0_1

 \section Options
 -seed$-count$

 Selection de la graine de random = seed (-> selection dependante de current time par defaut).

 Maintien d'une valeur pendant count iterations(=10 par defaut).


 \file f_bruit.c
 \ingroup f_bruit
 \brief Genere un signal aleatoire

 Author: C. Hasson
 Created: 10/09/2007
 Modified:
 - author: XXX
 - description: XXX
 - date: XX/XX/XXXX

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Bruit blanc centre sur 0 dont l'amplitude est egale a la valeur de la variable threshold du groupe.
 Lien entree (optionnel): -seed$-count$
 Selection de la graine de random = seed (-> selection dependante de current time par defaut).
 Maintien d'une valeur pendant count iterations(=10 par defaut).

 Macro:
 -none

 Local variables:
 -boolean flag_init_seed = false

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()

 Links:
 -none

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

/* #define DEBUG 1 */

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>
#include <Kernel_Function/prom_getopt.h>
#include <NN_Core/macro_colonne.h>
#include <net_message_debug_dist.h>

typedef struct My_Data_bruit {
  int cpt;
  int count;
  int graine;
} My_Data_bruit;

void function_bruit(int numero)
{
  int i, taille_gpe, deb_gpe, cpt = 0;
  float bruit;
  float coef_bruit;
  char param[50];

  My_Data_bruit * my_data;

  dprints("Enter in function f_bruit (%s)\n", def_groupe[numero].no_name);

  if (def_groupe[numero].data == NULL)
  {
    my_data = ALLOCATION(My_Data_bruit);
    my_data->cpt = 0;
    def_groupe[numero].data = (My_Data_bruit *) my_data;

    i = find_input_link(numero, 0);
    if (i != -1)
    {
      if (prom_getopt(liaison[i].nom, "-seed", param) == 2)
      {
        if (sscanf(param, "%d", &(my_data->graine)) != 1) my_data->graine = -1;
      }
      else my_data->graine = -1;
      if (prom_getopt(liaison[i].nom, "-count", param) == 2)
      {
        if (sscanf(param, "%d", &(my_data->count)) != 1) my_data->count = 10;
      }
      else my_data->count = 10;
    }
    else my_data->count = 10;

    dprints("Graine est %d\n", my_data->graine);
  }
  else
  {
    my_data = (My_Data_bruit *) def_groupe[numero].data;
    cpt = my_data->cpt;
  }

  taille_gpe = def_groupe[numero].nbre;
  deb_gpe = def_groupe[numero].premier_ele;
  coef_bruit = def_groupe[numero].seuil;

  if (!flag_init_seed)
  {
    if (my_data->graine == -1) srand48(time(NULL));
    else srand(my_data->graine);

    flag_init_seed = TRUE;
  }

  if (cpt < my_data->count)
  {
    cpt++;
  }
  else
  {
    cpt = 0;

    for (i = deb_gpe; i < deb_gpe + taille_gpe; i++)
    {
      bruit = coef_bruit * ((float) drand48() - 0.5);
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = bruit;

     // if (neurone[i].s1 > 1) neurone[i].s1 = 1;
     // else if (neurone[i].s1 < -1) neurone[i].s1 = -1;
    }
  }
  my_data->cpt = cpt;
}
