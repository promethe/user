/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_seqN_WTA f_seqN_WTA
\ingroup libNN_IO

\brief Winner sequentiels

\details

Winner sequentiels permettant par exemple des focalisations sequentielles sur une carte d'entree (competition locale)

\section Description
Les 2 neurones de sortie donnent les coord.. du point de focal.
ils ont -1 si y'a pas de point
Faut fournir sur le lien la taille du voisinage d'inhib des competitions locales et le nombre de focalisations demandees
Fonction best optimized (ou presque...)

\file 
\ingroup f_seqN_WTA

\brief Winner sequentiels permettant par exemple des focalisations sequentielles sur une carte d'entree (competition locale)

Author: Mickael Maillard 
Created: 09/07/04
Modified:
- author: M. Maillard
- description: specific file creation
- date: 23/08/2004

 ************************************************************/
#undef DEBUG
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#include <Kernel_Function/find_input_link.h>

#define VER2

#ifdef VER1
struct data_f_seqN_WTA
{
    int index;

    int x_max;
    int x_min;
    int y_max;
    int y_min;
    int sx;
    int sy;

    float *image;
    char *Tag_image;
    float **sort_Table_keypoint_ref;
    float *sort_Table_keypoint;

    int neurone_offset_x;
    int neurone_offset_y;
    int neurone_gpe_reset_offset;

    int inhib_size;

    int Nb_focalisations;
    float seuil;
};

void find_sorted_keypoints(float *image, char *Tag_image,
        float **sort_Table_keypoint_ref, int inhib_size,
        int x_min, int x_max, int y_min, int y_max,
        int Nb_focalisations, int sx, int sy, float seuil);

void insert_in_sort_table(float **sort_Table_keypoint_ref,
        int index_insertion, float valeur, int pos_x,
        int pos_y);

void function_seqN_WTA(int Gpe)
{
    int i;
    int lien = -1;
    int gpe_im = -1, gpe_reset = -1;
    char *chaine = NULL, *param = NULL;

    int index;
    int x_max, x_min, y_max, y_min;
    int sx, sy;
    float *image = NULL;
    char *Tag_image = NULL;
    float **sort_Table_keypoint_ref = NULL;
    float *sort_Table_keypoint = NULL;
    int neurone_offset_y, neurone_offset_x, neurone_gpe_reset_offset;
    int inhib_size = 3;
    int Nb_focalisations = 1;
    float seuil = 0.;

    struct data_f_seqN_WTA *my_data_f_seqN_WTA;
    float pos_x, pos_y;
    char isfirst_time = 0;

    /*	float hessien[2][2];
        float det,trace_carre,curvRatio,RATIOTHRESHOLD=12.1;*/

    if (def_groupe[Gpe].ext == NULL)
    {
        for (i = 0; i < 2; i++)
        {
            lien = find_input_link(Gpe, i);
            chaine = liaison[lien].nom;	/*mal gere : on test pas le retour de find_input_link */
            param = strstr(chaine, "reset");
            if (param != NULL)
                gpe_reset = liaison[lien].depart;
            else
            {
                if ((param = strstr(chaine, "Inh")) != NULL)
                {
                    gpe_im = liaison[lien].depart;
                    inhib_size = atoi(&param[3]);	/*mal fait si y'a pas de param... */
                }
                else
                {
                    printf
                        ("f_seqN_WTA : Gpe %d  : un des liens est mal formate : Inh\n",
                         Gpe);
                    exit(1);
                }
                if ((param = strstr(chaine, "-N")) != NULL)
                {
                    Nb_focalisations = atoi(&param[2]);	/*mal fait si y'a pas de param... */
                }
                else
                {
                    printf
                        ("f_seqN_WTA : Gpe %d  : un des liens est mal formate : -N\n",
                         Gpe);
                    exit(1);
                }
            }
        }
        if (gpe_reset == -1 || gpe_im == -1)
        {
            printf("%s : Gpe : %d : Absence de  lien entrants...\n",
                    __FUNCTION__, Gpe);
            exit(0);
        }

        /*allocation de la prom_images_struct */
        def_groupe[Gpe].ext = malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire\n",
                    Gpe);
            exit(1);
        }

        /*verif du groupe entrant */
        if (def_groupe[gpe_im].ext == NULL)
        {
            printf
                ("%s : Gpe : %d : Absence d'extension dans le groupe entrant...\n",
                 __FUNCTION__, Gpe);
            /*exit(0); */
            return;
        }
        if ((((prom_images_struct *) def_groupe[Gpe].ext)->nb_band
                    = ((prom_images_struct *) def_groupe[gpe_im].ext)->nb_band) != 4)
        {
            printf
                ("%s : Gpe : %d : Le groupe amont a un nb_band incorrect : cette fonction ne prend que du floatant n&b...\n",
                 __FUNCTION__, Gpe);
            exit(1);
        }

        /*attribution des tailles d'image */
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx
            = ((prom_images_struct *) def_groupe[gpe_im].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy
            = ((prom_images_struct *) def_groupe[gpe_im].ext)->sy;

        /*verif du nombre de neurone du groupee */
        if (def_groupe[Gpe].nbre < 2)
        {
            printf
                ("%s : Gpe : %s : Le groupe a un nombre de neurones < 2...%d\n",
                 __FUNCTION__, def_groupe[Gpe].no_name,def_groupe[Gpe].nbre);
            exit(1);
        }

        /*Init de la Tag_image */
        Tag_image = malloc(sx * sy * sizeof(char));
        if (Tag_image == NULL)
        {
            printf("Gpe %d : Erreur d'allocation memoire image\n", Gpe);
            exit(1);
        }

        /*on complete la prom_images_struct */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) Tag_image;
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[gpe_im].ext)->images_table[0];
        image =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];

        /*init de la table des keypoints */
        sort_Table_keypoint_ref = malloc(Nb_focalisations * sizeof(float **));
        sort_Table_keypoint = malloc(Nb_focalisations * 3 * sizeof(float *));
        if (sort_Table_keypoint == NULL || sort_Table_keypoint_ref == NULL)
        {
            printf
                ("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire sort_table_keypoints\n",
                 Gpe);
            exit(1);
        }
        for (i = 0; i < Nb_focalisations; i++)
        {
            *(sort_Table_keypoint_ref + i) = sort_Table_keypoint + 3 * i;
        }


        /*recip de l'offset du reset */
        neurone_gpe_reset_offset = def_groupe[gpe_reset].premier_ele;

        /*il est possible de fournir un seuil */
        seuil = def_groupe[Gpe].seuil;

        /*MAZ des neurones du groupe et recup des offset */
        neurone_offset_x = def_groupe[Gpe].premier_ele;
        neurone_offset_y = neurone_offset_x + 1;
        neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
            neurone[neurone_offset_x].s = -1.;
        neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
            neurone[neurone_offset_y].s = -1.;

        /*Init de la structure data et des derniers champs */
        my_data_f_seqN_WTA = def_groupe[Gpe].data =
            malloc(sizeof(struct data_f_seqN_WTA));
        if (my_data_f_seqN_WTA == NULL)
        {
            printf("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire data\n",
                    Gpe);
            exit(1);
        }

        index = my_data_f_seqN_WTA->index = 0;

        x_max = my_data_f_seqN_WTA->x_max = sx - inhib_size;
        x_min = my_data_f_seqN_WTA->x_min = inhib_size;
        y_max = my_data_f_seqN_WTA->y_max = sy - inhib_size;
        y_min = my_data_f_seqN_WTA->y_min = inhib_size;
        my_data_f_seqN_WTA->sx = sx;
        my_data_f_seqN_WTA->sy = sy;

        my_data_f_seqN_WTA->image = image;
        my_data_f_seqN_WTA->Tag_image = Tag_image;
        my_data_f_seqN_WTA->sort_Table_keypoint_ref = sort_Table_keypoint_ref;
        my_data_f_seqN_WTA->sort_Table_keypoint = sort_Table_keypoint;

        my_data_f_seqN_WTA->neurone_offset_x = neurone_offset_x;
        my_data_f_seqN_WTA->neurone_offset_y = neurone_offset_y;
        my_data_f_seqN_WTA->neurone_gpe_reset_offset =
            neurone_gpe_reset_offset;

        my_data_f_seqN_WTA->inhib_size = inhib_size;

        my_data_f_seqN_WTA->Nb_focalisations = Nb_focalisations;
        my_data_f_seqN_WTA->seuil = seuil;

        /*on traite le premier passage car il n'y a pas de signel de reset avec f_flip_flop et f_transition_detect a la premiere execution */
        isfirst_time = 1;
    }
    else
    {
        my_data_f_seqN_WTA = (struct data_f_seqN_WTA *) def_groupe[Gpe].data;

        index = my_data_f_seqN_WTA->index;

        x_max = my_data_f_seqN_WTA->x_max;
        x_min = my_data_f_seqN_WTA->x_min;
        y_max = my_data_f_seqN_WTA->y_max;
        y_min = my_data_f_seqN_WTA->y_min;
        sx = my_data_f_seqN_WTA->sx;
        sy = my_data_f_seqN_WTA->sy;

        image = my_data_f_seqN_WTA->image;
        Tag_image = my_data_f_seqN_WTA->Tag_image;
        sort_Table_keypoint_ref = my_data_f_seqN_WTA->sort_Table_keypoint_ref;
        sort_Table_keypoint = my_data_f_seqN_WTA->sort_Table_keypoint;

        neurone_offset_x = my_data_f_seqN_WTA->neurone_offset_x;
        neurone_offset_y = my_data_f_seqN_WTA->neurone_offset_y;
        neurone_gpe_reset_offset =
            my_data_f_seqN_WTA->neurone_gpe_reset_offset;

        inhib_size = my_data_f_seqN_WTA->inhib_size;

        Nb_focalisations = my_data_f_seqN_WTA->Nb_focalisations;
        seuil = my_data_f_seqN_WTA->seuil;

    }

    if (neurone[neurone_gpe_reset_offset].s1 == 1. || isfirst_time)	/*si signal de reset */
    {
#ifdef DEBUG
        printf("Signal reset\n");
#endif
        memset(Tag_image, (int) 0, sx * sy);	/*on remet a 0 l'image des tag */
        /*memset(sort_Table_keypoint,0,Nb_focalisations*3*sizeof(float)); */
        for (i = 0; i < 3 * Nb_focalisations; i++)
            *(sort_Table_keypoint + i) = -1.;
        index = 0;
        find_sorted_keypoints(image, Tag_image, sort_Table_keypoint_ref,
                inhib_size, x_min, x_max, y_min, y_max,
                Nb_focalisations, sx, sy, seuil);
#ifdef DEBUG
        for (i = 0; i < Nb_focalisations; i++)
            printf("res de max %f row %d column %d\n",
                    *(*(sort_Table_keypoint_ref + i) + 0),
                    (int) (*(*(sort_Table_keypoint_ref + i) + 1)),
                    (int) (*(*(sort_Table_keypoint_ref + i) + 0)));
        printf("--------------- fin Signal reset\n");
#endif

    }

    if (index < Nb_focalisations)
    {
        pos_x = (*(*(sort_Table_keypoint_ref + index) + 1));
        pos_y = (*(*(sort_Table_keypoint_ref + index) + 2));

        neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
            neurone[neurone_offset_x].s = pos_x / sx;
        neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
            neurone[neurone_offset_y].s = pos_y / sy;

        my_data_f_seqN_WTA->index = index + 1;	/*pour le coup suivant faut stocker */
    }
    else
    {
        /*neurone[neurone_offset_x].s2=neurone[neurone_offset_x].s1=neurone[neurone_offset_x].s = 0.;
          neurone[neurone_offset_y].s2=neurone[neurone_offset_y].s1=neurone[neurone_offset_y].s = 0.; */
        neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
            neurone[neurone_offset_x].s = -1.;
        neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
            neurone[neurone_offset_y].s = -1.;
    }
    return;
}


void find_sorted_keypoints(float *image, char *Tag_image,
        float **sort_Table_keypoint_ref, int inhib_size,
        int x_min, int x_max, int y_min, int y_max,
        int Nb_focalisations, int sx, int sy, float seuil)
{
    int row, column, local_row, local_column;
    float min_finded = 1000000000.;
    int nb_finded = 0;
    float this_point_value;
    int radiussqr = inhib_size * inhib_size;
    int distsqr;

    for (row = y_min; row < y_max; row++)
    {
        for (column = x_min; column < x_max; column++)
        {
            if (*(Tag_image + row * sx + column) == (char) 0)
            {
                this_point_value = *(image + row * sx + column);
                if (this_point_value < seuil)
                    goto end_point;
                for (local_row = -inhib_size; local_row <= inhib_size;
                        local_row++)
                    for (local_column = -inhib_size;
                            local_column <= inhib_size; local_column++)
                    {
                        distsqr =
                            local_column * local_column +
                            local_row * local_row;
                        if ((distsqr <= radiussqr) && distsqr)
                            /*if( (distsqr<=radiussqr)) */
                        {
                            if (*(image + (row + local_row) * sx + column + local_column) >= this_point_value)	/*> */
                            {
                                goto end_point;
                            }
                        }
                    }
                /*ici : donc max_local */
                if (nb_finded < Nb_focalisations)
                {
                    if (this_point_value < min_finded)
                        min_finded = this_point_value;
                    insert_in_sort_table(sort_Table_keypoint_ref, nb_finded,
                            this_point_value, column, row);
                    nb_finded++;
                }
                else
                {
                    if (this_point_value > min_finded)
                    {
                        insert_in_sort_table(sort_Table_keypoint_ref,
                                nb_finded - 1, this_point_value,
                                column, row);
                        /*min_finded = this_point_value; */
                        min_finded =
                            *(*(sort_Table_keypoint_ref + nb_finded - 1));
                    }
                    /*sinon on est plus petit que le petit on a suffisament de point donc on fait rien; */
                }
                /*dans tous les cas peut pas y'avoir d'autres max locaux dans le vosinage donc je tag */
                for (local_row = -inhib_size; local_row <= inhib_size;
                        local_row++)
                    for (local_column = -inhib_size;
                            local_column <= inhib_size; local_column++)
                        if ((local_column * local_column +
                                    local_row * local_row) <= radiussqr)
                            *(Tag_image + (row + local_row) * sx + column +
                                    local_column) = (char) -1;
                *(Tag_image + row * sx + column) = (char) 1;
            }
end_point:;
        }
    }
}

void insert_in_sort_table(float **sort_Table_keypoint_ref,
        int index_insertion, float valeur, int pos_x,
        int pos_y)
{
    float *temp;
    int i;

    *(*(sort_Table_keypoint_ref + index_insertion) + 0) = valeur;
    *(*(sort_Table_keypoint_ref + index_insertion) + 1) = (float) pos_x;
    *(*(sort_Table_keypoint_ref + index_insertion) + 2) = (float) pos_y;
    for (i = index_insertion; i > 0; i--)
    {
        if (*(*(sort_Table_keypoint_ref + i)) >
                *(*(sort_Table_keypoint_ref + i - 1)))
        {
            temp = *(sort_Table_keypoint_ref + i);
            *(sort_Table_keypoint_ref + i) =
                *(sort_Table_keypoint_ref + i - 1);
            *(sort_Table_keypoint_ref + i - 1) = temp;
        }
    }
}

#endif
/***********************************************
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 *
 *
 *
 */
#ifdef VER2
struct data_f_seqN_WTA
{
    int index;

    int x_max;
    int x_min;
    int y_max;
    int y_min;
    int sx;
    int sy;

    float *image;
    char *Tag_image;
    float **sort_Table_keypoint_ref;
    float *sort_Table_keypoint;

    int neurone_offset_x;
    int neurone_offset_y;
    int neurone_gpe_reset_offset;

    int inhib_size;

    int Nb_focalisations;
    float seuil;

    int neurone_inhib;
    int detect_transition;
};

void find_sorted_keypoints(float *image, char *Tag_image,
        float **sort_Table_keypoint_ref, int inhib_size,
        int x_min, int x_max, int y_min, int y_max,
        int Nb_focalisations, int sx, float seuil);

void insert_in_sort_table(float **sort_Table_keypoint_ref,
        int index_insertion, float valeur, int pos_x,
        int pos_y);

void function_seqN_WTA(int Gpe)
{
    int i;
    int lien = 0;
    int gpe_im = -1, gpe_reset = -1;
    char *chaine = NULL, *param = NULL;

    int index;
    int x_max, x_min, y_max, y_min;
    int sx, sy;
    float *image = NULL;
    char *Tag_image = NULL;
    float **sort_Table_keypoint_ref = NULL;
    float *sort_Table_keypoint = NULL;
    int neurone_offset_y, neurone_offset_x, neurone_gpe_reset_offset;
    int inhib_size = 3;
    int Nb_focalisations = 1;
    float seuil = 0.;

    struct data_f_seqN_WTA *my_data_f_seqN_WTA;
    float pos_x, pos_y;
    char isfirst_time = 0;

    int neurone_inhib = -1;
    int detect_transition = 0;

    /*	float hessien[2][2];
        float det,trace_carre,curvRatio,RATIOTHRESHOLD=12.1;*/

    if (def_groupe[Gpe].ext == NULL)
    {
        i = 0;
        while ((lien = find_input_link(Gpe, i)) != -1)
        {
            i++;
            chaine = liaison[lien].nom;	/*mal gere : on test pas le retour de find_input_link */
            param = strstr(chaine, "reset");
            if (param != NULL)
                gpe_reset = liaison[lien].depart;
            else if ((param = strstr(chaine, "gpe_inhib")) != NULL)
            {
                neurone_inhib = def_groupe[liaison[lien].depart].premier_ele;
            }
            else
            {
                if ((param = strstr(chaine, "Inh")) != NULL)
                {
                    gpe_im = liaison[lien].depart;
                    inhib_size = atoi(&param[3]);	/*mal fait si y'a pas de param... */
                }
                else
                {
                    printf
                        ("f_seqN_WTA : Gpe %d  : un des liens est mal formate : Inh\n",
                         Gpe);
                    exit(1);
                }
                if ((param = strstr(chaine, "-N")) != NULL)
                {
                    Nb_focalisations = atoi(&param[2]);	/*mal fait si y'a pas de param... */
                }
                else
                {
                    printf
                        ("f_seqN_WTA : Gpe %d  : un des liens est mal formate : -N\n",
                         Gpe);
                    exit(1);
                }
            }
        }
        if (gpe_reset == -1 || gpe_im == -1)
        {
            printf("%s : Gpe : %d : Absence de  lien entrants...\n",
                    __FUNCTION__, Gpe);
            exit(0);
        }

        /*verif du groupe entrant */
        if (def_groupe[gpe_im].ext == NULL)
        {
            printf
                ("%s : Gpe : %d : Absence d'extension dans le groupe entrant...\n",
                 __FUNCTION__, Gpe);
            /*exit(0); */
            return;
        }
        /*allocation de la prom_images_struct */
        def_groupe[Gpe].ext = malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire\n",
                    Gpe);
            exit(1);
        }

        if ((((prom_images_struct *) def_groupe[Gpe].ext)->nb_band
                    = ((prom_images_struct *) def_groupe[gpe_im].ext)->nb_band) != 4)
        {
            printf
                ("%s : Gpe : %d : Le groupe amont a un nb_band incorrect : cette fonction ne prend que du floatant n&b...\n",
                 __FUNCTION__, Gpe);
            exit(1);
        }

        /*attribution des tailles d'image */
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx
            = ((prom_images_struct *) def_groupe[gpe_im].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy
            = ((prom_images_struct *) def_groupe[gpe_im].ext)->sy;

        /*verif du nombre de neurone du groupee */
        if (def_groupe[Gpe].nbre < 2)
        {
            printf("%s : Gpe : %d : Le groupe a un nombre de neurone < 2\n",
                    __FUNCTION__, Gpe);
            exit(1);
        }

        /*Init de la Tag_image */
        Tag_image = malloc(sx * sy * sizeof(char));
        if (Tag_image == NULL)
        {
            printf("Gpe %d : Erreur d'allocation memoire image\n", Gpe);
            exit(1);
        }

        /*on complete la prom_images_struct */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) Tag_image;
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[gpe_im].ext)->images_table[0];
        image =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];

        /*init de la table des keypoints */
        sort_Table_keypoint_ref = malloc(Nb_focalisations * sizeof(float **));
        sort_Table_keypoint = malloc(Nb_focalisations * 3 * sizeof(float *));
        if (sort_Table_keypoint == NULL || sort_Table_keypoint_ref == NULL)
        {
            printf
                ("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire sort_table_keypoints\n",
                 Gpe);
            exit(1);
        }
        for (i = 0; i < Nb_focalisations; i++)
        {
            *(sort_Table_keypoint_ref + i) = sort_Table_keypoint + 3 * i;
        }


        /*recip de l'offset du reset */
        neurone_gpe_reset_offset = def_groupe[gpe_reset].premier_ele;

        /*il est possible de fournir un seuil */
        seuil = def_groupe[Gpe].seuil;

        /*MAZ des neurones du groupe et recup des offset */
        neurone_offset_x = def_groupe[Gpe].premier_ele;
        neurone_offset_y = neurone_offset_x + 1;
        neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
            neurone[neurone_offset_x].s = -1.;
        neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
            neurone[neurone_offset_y].s = -1.;

        /*Init de la structure data et des derniers champs */
        my_data_f_seqN_WTA = def_groupe[Gpe].data =
            malloc(sizeof(struct data_f_seqN_WTA));
        if (my_data_f_seqN_WTA == NULL)
        {
            printf("f_seqN_WTA : Gpe %d : Erreur d'allocation memoire data\n",
                    Gpe);
            exit(1);
        }

        index = my_data_f_seqN_WTA->index = 0;

        x_max = my_data_f_seqN_WTA->x_max = sx - inhib_size;
        x_min = my_data_f_seqN_WTA->x_min = inhib_size;
        y_max = my_data_f_seqN_WTA->y_max = sy - inhib_size;
        y_min = my_data_f_seqN_WTA->y_min = inhib_size;
        my_data_f_seqN_WTA->sx = sx;
        my_data_f_seqN_WTA->sy = sy;

        my_data_f_seqN_WTA->image = image;
        my_data_f_seqN_WTA->Tag_image = Tag_image;
        my_data_f_seqN_WTA->sort_Table_keypoint_ref = sort_Table_keypoint_ref;
        my_data_f_seqN_WTA->sort_Table_keypoint = sort_Table_keypoint;

        my_data_f_seqN_WTA->neurone_offset_x = neurone_offset_x;
        my_data_f_seqN_WTA->neurone_offset_y = neurone_offset_y;
        my_data_f_seqN_WTA->neurone_gpe_reset_offset =
            neurone_gpe_reset_offset;

        my_data_f_seqN_WTA->inhib_size = inhib_size;

        my_data_f_seqN_WTA->Nb_focalisations = Nb_focalisations;
        my_data_f_seqN_WTA->seuil = seuil;

        my_data_f_seqN_WTA->neurone_inhib = neurone_inhib;
        my_data_f_seqN_WTA->detect_transition = detect_transition;

        /*on traite le premier passage car il n'y a pas de signel de reset avec f_flip_flop et f_transition_detect a la premiere execution */
        isfirst_time = 1;
    }
    else
    {
        my_data_f_seqN_WTA = (struct data_f_seqN_WTA *) def_groupe[Gpe].data;

        index = my_data_f_seqN_WTA->index;

        x_max = my_data_f_seqN_WTA->x_max;
        x_min = my_data_f_seqN_WTA->x_min;
        y_max = my_data_f_seqN_WTA->y_max;
        y_min = my_data_f_seqN_WTA->y_min;
        sx = my_data_f_seqN_WTA->sx;
        sy = my_data_f_seqN_WTA->sy;

        image = my_data_f_seqN_WTA->image;
        Tag_image = my_data_f_seqN_WTA->Tag_image;
        sort_Table_keypoint_ref = my_data_f_seqN_WTA->sort_Table_keypoint_ref;
        sort_Table_keypoint = my_data_f_seqN_WTA->sort_Table_keypoint;

        neurone_offset_x = my_data_f_seqN_WTA->neurone_offset_x;
        neurone_offset_y = my_data_f_seqN_WTA->neurone_offset_y;
        neurone_gpe_reset_offset =
            my_data_f_seqN_WTA->neurone_gpe_reset_offset;

        inhib_size = my_data_f_seqN_WTA->inhib_size;

        Nb_focalisations = my_data_f_seqN_WTA->Nb_focalisations;
        seuil = my_data_f_seqN_WTA->seuil;

        neurone_inhib = my_data_f_seqN_WTA->neurone_inhib;
        detect_transition = my_data_f_seqN_WTA->detect_transition;

    }

    if (neurone_inhib != -1)	/*cas ou le gpe est inhibe par un autre , il ne sort rien... */
    {
        if (neurone[neurone_inhib].s1 < 0.0000001)
        {
            neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
                neurone[neurone_offset_x].s = -1.;
            neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
                neurone[neurone_offset_y].s = -1.;
            my_data_f_seqN_WTA->detect_transition = 0;
            return;
        }
        else
        {
            if (detect_transition < 0.00000001)
                isfirst_time = 1;
            my_data_f_seqN_WTA->detect_transition = 1;
        }
    }

    if (neurone[neurone_gpe_reset_offset].s1 > 0.99999999999 || isfirst_time)	/*si signal de reset */
    {
#ifdef DEBUG
        printf("Signal reset\n");
#endif
        memset(Tag_image, (int) 0, sx * sy);	/*on remet a 0 l'image des tag */
        /*memset(sort_Table_keypoint,0,Nb_focalisations*3*sizeof(float)); */
        for (i = 0; i < 3 * Nb_focalisations; i++)
            *(sort_Table_keypoint + i) = -1.;
        index = 0;
        find_sorted_keypoints(image, Tag_image, sort_Table_keypoint_ref,
                inhib_size, x_min, x_max, y_min, y_max,
                Nb_focalisations, sx, seuil);
#ifdef DEBUG
        for (i = 0; i < Nb_focalisations; i++)
            printf("res de max %f row %d column %d\n",
                    *(*(sort_Table_keypoint_ref + i) + 0),
                    (int) (*(*(sort_Table_keypoint_ref + i) + 1)),
                    (int) (*(*(sort_Table_keypoint_ref + i) + 0)));
        printf("--------------- fin Signal reset\n");
#endif

    }

    if (index < Nb_focalisations)
    {
        pos_x = (*(*(sort_Table_keypoint_ref + index) + 1));
        pos_y = (*(*(sort_Table_keypoint_ref + index) + 2));

        if (pos_x < -0.99999 || pos_y < -0.999999)
        {
            neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
                neurone[neurone_offset_x].s = -1.;
            neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
                neurone[neurone_offset_y].s = -1.;
        }
        else
        {
            neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
                neurone[neurone_offset_x].s = pos_x / sx;
            neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
                neurone[neurone_offset_y].s = pos_y / sy;
        }
        my_data_f_seqN_WTA->index = index + 1;	/*pour le coup suivant faut stocker */
    }
    else
    {
        /*neurone[neurone_offset_x].s2=neurone[neurone_offset_x].s1=neurone[neurone_offset_x].s = 0.;
          neurone[neurone_offset_y].s2=neurone[neurone_offset_y].s1=neurone[neurone_offset_y].s = 0.; */
        neurone[neurone_offset_x].s2 = neurone[neurone_offset_x].s1 =
            neurone[neurone_offset_x].s = -1.;
        neurone[neurone_offset_y].s2 = neurone[neurone_offset_y].s1 =
            neurone[neurone_offset_y].s = -1.;
    }
    /*toto:
      if(index<Nb_focalisations)
      {
      pos_x = (*(*(sort_Table_keypoint_ref+index)+1));
      pos_y = (*(*(sort_Table_keypoint_ref+index)+2));

      hessien[0][0]=*(image + ((int)pos_y+1)*sx + (int)pos_x) 
      - 2 * *(image + (int)pos_y*sx + (int)pos_x) 
      + *(image + ((int)pos_y-1)*sx + (int)pos_x);

      hessien[0][1]=(float)(0.25) * (*(image + ((int)pos_y+1)*sx + (int)pos_x+1)
      - *(image + ((int)pos_y+1)*sx + (int)pos_x-1) 
      - *(image + ((int)pos_y-1)*sx + (int)pos_x+1) 
      + *(image + ((int)pos_y-1)*sx + (int)pos_x-1) );

      hessien[1][0]=hessien[0][1];

      hessien[1][1]=*(image + (int)pos_y*sx + (int)pos_x+1)
      -2* *(image + (int)pos_y*sx + (int)pos_x) 
      + *(image + (int)pos_y*sx + (int)pos_x-1);

      trace_carre = (hessien[0][0] + hessien[1][1]) * ( hessien[0][0] + hessien[1][1]);
      det = (hessien[0][0]*hessien[1][1]) - (hessien[1][0]*hessien[0][1]);

      if(det!=0)
      {
      curvRatio = (float)(trace_carre/det);
      if(curvRatio<RATIOTHRESHOLD)
      {
      neurone[neurone_offset_x].s2=neurone[neurone_offset_x].s1=neurone[neurone_offset_x].s = pos_x/sx;
      neurone[neurone_offset_y].s2=neurone[neurone_offset_y].s1=neurone[neurone_offset_y].s = pos_y/sy;
      }
      else
      {
      printf("******keypoint rejected*******sx %d sy %d\n",sx,sy);
      index++;
      goto toto;
      }

      }

      my_data_f_seqN_WTA->index = index + 1;
      }
      else
      {
      neurone[neurone_offset_x].s2=neurone[neurone_offset_x].s1=neurone[neurone_offset_x].s = 0;
      neurone[neurone_offset_y].s2=neurone[neurone_offset_y].s1=neurone[neurone_offset_y].s = 0;
      }*/
    return;
}


void find_sorted_keypoints(float *image, char *Tag_image,
        float **sort_Table_keypoint_ref, int inhib_size,
        int x_min, int x_max, int y_min, int y_max,
        int Nb_focalisations, int sx, float seuil)
{
    int row, column, local_row, local_column;
    float min_finded = 1000000000.;
    int nb_finded = 0;
    float this_point_value;
    int radiussqr = inhib_size * inhib_size;
    int distsqr;

    for (row = y_min; row < y_max; row++)
    {
        for (column = x_min; column < x_max; column++)
        {
            if (*(Tag_image + row * sx + column) == (char) 0)
            {
                this_point_value = *(image + row * sx + column);
                if (this_point_value < seuil)
                    goto end_point;
                for (local_row = -inhib_size; local_row <= inhib_size;
                        local_row++)
                    for (local_column = -inhib_size;
                            local_column <= inhib_size; local_column++)
                    {
                        distsqr =
                            local_column * local_column +
                            local_row * local_row;
                        if ((distsqr <= radiussqr) && distsqr)
                            /*if( (distsqr<=radiussqr)) */
                        {
                            if (*(image + (row + local_row) * sx + column + local_column) >= this_point_value)	/*> */
                            {
                                goto end_point;
                            }
                        }
                    }
                /*ici : donc max_local */
                if (nb_finded < Nb_focalisations)
                {
                    if (this_point_value < min_finded)
                        min_finded = this_point_value;
                    insert_in_sort_table(sort_Table_keypoint_ref, nb_finded,
                            this_point_value, column, row);
                    nb_finded++;
                }
                else
                {
                    if (this_point_value > min_finded)
                    {
                        insert_in_sort_table(sort_Table_keypoint_ref,
                                nb_finded - 1, this_point_value,
                                column, row);
                        /*min_finded = this_point_value; */
                        min_finded =
                            *(*(sort_Table_keypoint_ref + nb_finded - 1));
                    }
                    /*sinon on est plus petit que le petit on a suffisament de point donc on fait rien; */
                }
                /*dans tous les cas peut pas y'avoir d'autres max locaux dans le vosinage donc je tag */
                for (local_row = -inhib_size; local_row <= inhib_size;
                        local_row++)
                    for (local_column = -inhib_size;
                            local_column <= inhib_size; local_column++)
                        if ((local_column * local_column +
                                    local_row * local_row) <= radiussqr)
                            *(Tag_image + (row + local_row) * sx + column +
                                    local_column) = (char) -1;
                *(Tag_image + row * sx + column) = (char) 1;
            }
end_point:;
        }
    }
}

void insert_in_sort_table(float **sort_Table_keypoint_ref,
        int index_insertion, float valeur, int pos_x,
        int pos_y)
{
    float *temp;
    int i;

    *(*(sort_Table_keypoint_ref + index_insertion) + 0) = valeur;
    *(*(sort_Table_keypoint_ref + index_insertion) + 1) = (float) pos_x;
    *(*(sort_Table_keypoint_ref + index_insertion) + 2) = (float) pos_y;
    for (i = index_insertion; i > 0; i--)
    {
        if (*(*(sort_Table_keypoint_ref + i)) >
                *(*(sort_Table_keypoint_ref + i - 1)))
        {
            temp = *(sort_Table_keypoint_ref + i);
            *(sort_Table_keypoint_ref + i) =
                *(sort_Table_keypoint_ref + i - 1);
            *(sort_Table_keypoint_ref + i - 1) = temp;
        }
    }
}
#endif
