/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>

/** ***********************************************************
\file  f_s_winner_to_s1.c 
\brief 

Author: B.Fouque
Created: 01/12/08

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
- recopie exclusivement la plus grande sortie de s sur s1 et s2, les autres sont mises a 0 sur s1 et s2.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

/*!
 * \brief recopie exclusivement la plus grande sortie de s sur s1 et s2, les autres sont mises a 0 sur s1 et s2.
 *
 * Entree: Groupe de neurones
 * Sortie: Seul le winner en entree a une valeur non nulle en sortie: sa valeur analogique est mise en s1 et s2.
 */
void function_s_winner_to_s1(int gpe)
{
    int i, deb, nbre, nbre2;
    int increment;

    float temp;

    type_coeff *coeff;

    float   max = 0.0;
    int     argmax = 0;

    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
    nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
    increment = nbre / nbre2;

    for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
    {
        temp = 0.;

        coeff = neurone[i].coeff;

        while (coeff != NULL)
        {
            temp = temp + coeff->val * neurone[coeff->entree].s;
            coeff = coeff->s;   /* Lien suivant */
        }

        neurone[i].s = temp;
        neurone[i].s1 = neurone[i].s;
        neurone[i].s2 = neurone[i].s;

	if (neurone[i].s > max)
	  {
	    max = neurone[i].s;
	    argmax = i;
	  }
    }

    for (i = deb + increment - 1; i < deb + nbre; i = i + increment)
      {
	neurone[i].s
	  = neurone[i].s1
	  = neurone[i].s2
	  = 0.0;
      }
    neurone[argmax].s
      = neurone[argmax].s1
      = neurone[argmax].s2
/*       = 1.0; */
      = max;

    if (max > 0.0 && max < 1.0)
      {
	printf ("s_winner_to_s1: %i:%f\n", argmax - deb, max);
      }
}
