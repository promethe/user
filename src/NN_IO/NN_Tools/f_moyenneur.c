/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Simulation of the sequential perception of distant landmarks             
   possible problem when the system cannot analyze the whole set
   of landmarks. Not enough iterations... 

   Problem solved. The function refuses to take into account more
landmarks than the number of iterations at the function time scale.
Yet, this functioning mode induces a particular bias in the case of hidden landmarks
(the first landmarks are prefered  but it is not a big issue in a simple simulation).
PG.

Macro:
-none 

Local variables:
-float posx
-float posy
-int NBLAND
-float ** table_land

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include "tools/include/local_var.h"

/*#define DEBUG*/

void function_moyenneur(int gpe)
{

    float total = 0, sortien = 0, sortiep = 0, W, totalp = 0, totaln =
        0, sortie = 0, val;
    int deb, nbre, i, gp;

    type_coeff *coeff;
    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;


    /*somme pondere par act: angle+=new_angle*W*intensite */
    /*moitie inf */
    for (i = deb; i < nbre / 2 + deb; i++)
    {
#ifdef DEBUG
        printf("I:%d\n", i);
#endif
        coeff = neurone[i].coeff;
/*on parcors tous les coeff*/
        while (coeff != NULL)
        {
            W = coeff->val;
            gp = coeff->entree -
                def_groupe[neurone[coeff->entree].groupe].premier_ele;
            if (W > 0)
            {
                sortien = sortien + W * neurone[coeff->entree].s2 * gp;
                totaln += W * neurone[coeff->entree].s2;

#ifdef DEBUG
                printf
                    ("sortien: %f, totaln: %f, W: %f,neurone[gpe].s2: %f,gpe:%d, coeff->entree %d\n",
                     sortien, totaln, W, neurone[coeff->entree].s2, gp,
                     coeff->entree);
#endif
            }
            else
            {
                printf("PPPPPPPPPPPPPP\n");
            }
            coeff = coeff->s;
        }
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
    }


/*moy positif*/

    for (i = deb + nbre / 2; i < nbre + deb; i++)
    {
#ifdef DEBUG
        printf("I:%d\n", i);
#endif
        coeff = neurone[i].coeff;
/*on parcors tous les coeff*/
        while (coeff != NULL)
        {
            W = coeff->val;
            gp = coeff->entree - def_groupe[neurone[coeff->entree].groupe].premier_ele;

            if (W > 0)
            {
                sortiep = sortiep + W * neurone[coeff->entree].s2 * gp;
                totalp += W * neurone[coeff->entree].s2;

#ifdef DEBUG
                printf
                    ("sortiep: %f, totalp: %f, W: %f,neurone[gpe].s2: %f,gpe:%d, coeff->entree %d\n",
                     sortiep, total, W, neurone[coeff->entree].s2, gp,
                     coeff->entree);
#endif
            }
            else
            {
                printf("PROBLEM avec les liens vers  F_moy!\n");
            }
            coeff = coeff->s;
        }
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
    }

    if (totaln > -1 && totalp > -1)
    {
        /*si ecart sup a PI */
        if ((sortiep / totalp) - (sortien / totaln) > nbre / 2)
        {
            if (totalp > totaln)
                sortien = sortien + (nbre * totaln);
            else if (totalp < totaln)
                sortiep = sortiep - (nbre * totalp);
            /*egalite en poids, on va vers le plus pres... */
            else
            {
#ifdef DEBUG
                printf("p:%f,n:%f\n", nbre - 1 - (sortiep / totalp),
                       (sortien / totaln));
#endif
                if (nbre - 1 - (sortiep / totalp) <= (sortien / totaln))
                {
                    sortiep = sortiep - (nbre * totalp);
                }
                else
                {
                    sortien = sortien + (nbre * totaln);
                }
            }
        }
        /*moy std */

        sortie = sortiep + sortien;
        total = totalp + totaln;

    }
    else if (totaln > 0)
    {
        total = totaln;
        sortie = sortien;
    }
    else
    {
        total = totalp;
        sortie = sortiep;
    }

    /*si entrees inhibitrices suffisantes (> seuil) alors inhib=1 et on ne parcourrera plus les lien negatifs */
/*if(sortien<SEUIL) inhib=1;*/
/*si pas d'inhibition on conserve la memoire*/
    /*if(inhib==0){ */
    /*moyenne: angle=angle/totale */
    val = (sortie / total);
    i = (int) val;
#ifdef DEBUG
    printf
        ("i: %d,  totalp:%f, sortiep:%f,totaln:%f,sortien:%f, total:%f,sortie:%f\n",
         i, totalp, sortiep, totaln, sortien, total, sortie);
#endif
    if (i < 0)
        i += nbre;
    else if (i >= nbre)
        i -= nbre;
#ifdef DEBUG
    printf("i: %d\n", i);
#endif
    neurone[i + deb].s = 1;
    neurone[i + deb].s1 = 1;
    neurone[i + deb].s2 = 1;
    /*} */
    /*sinon on ne prend en compte que les nouvelles entrees */
/*  else{
neurone[i].s = sortiep;
  neurone[i].s1 =sortiep;
  neurone[i].s2 =sortiep;
   #ifdef DEBUG
  printf("reset asked of the path_integration... val:%f\n",sortien);
#endif
  }*/
}
