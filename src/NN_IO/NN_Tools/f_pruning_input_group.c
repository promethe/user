/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_pruning_input_group.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Met a zero toutes les sorties des groupes specifie par le lien 
 chaque numero est suivi d'une ,virgule

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct MyData_f_pruning_input_group
{
    int gpe_to_prune;
    float coeff_min;
    float coeff_max;
} MyData_f_pruning_input_group;


void function_pruning_input_group(int gpe_sortie)
{
    int i, l, gpe_to_prune = -1;
    char param[32];
    float coeff_min = 0., coeff_max = 0.;
    MyData_f_pruning_input_group *my_data = NULL;
    type_coeff *coeff = NULL, *coeff_prec = NULL, *coeff_prems = NULL;
    /*#ifdef DEBUG */
    printf("-------------------------------------------------\n");
    printf("function_pruning (multiple clean) \n");
    /*#endif */

    if (def_groupe[gpe_sortie].data == NULL)
    {
        i = 0;
        l = find_input_link(gpe_sortie, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "m", param) == 2)
                coeff_min = atof(param);

            if (prom_getopt(liaison[l].nom, "M", param) == 2)
            {
                coeff_max = atof(param);
                gpe_to_prune = liaison[l].depart;
            }
            i++;
            l = find_input_link(gpe_sortie, i);

        }


        my_data =
            (MyData_f_pruning_input_group *)
            malloc(sizeof(MyData_f_pruning_input_group));
        if (my_data == NULL)
        {
            printf("erreur malloc f_pruning_input_group\n");
            exit(0);
        }
        my_data->gpe_to_prune = gpe_to_prune;
        my_data->coeff_min = coeff_min;
        my_data->coeff_min = coeff_max;
        def_groupe[gpe_sortie].data = (void *) my_data;

    }
    else
    {
        my_data =
            (MyData_f_pruning_input_group *) def_groupe[gpe_sortie].data;
        gpe_to_prune = my_data->gpe_to_prune;
        coeff_min = my_data->coeff_min;
        coeff_max = my_data->coeff_max;
    }

    if (vigilence < 0.5)
    {
        printf("pruning init done: %d, min %f max %f\n", gpe_to_prune,
               coeff_min, coeff_max);
        for (i = 0; i < def_groupe[gpe_to_prune].nbre; i++)
        {
            coeff_prems =
                neurone[def_groupe[gpe_to_prune].premier_ele + i].coeff;
            coeff = neurone[def_groupe[gpe_to_prune].premier_ele + i].coeff;
            coeff_prec = NULL;
            while (coeff != NULL)
            {                   /*    >0. et <= 0.49 */
                /*printf("i: %d coeff val %f",i,coeff->val); */
                if (coeff->val >= coeff_min && coeff->val < coeff_max)
                {
/* 				printf("saute\n");*/
                    if (coeff == coeff_prems)
                    {
                        coeff_prems = coeff->s;
                    }

                    else
                    {
                        coeff_prec->s = coeff->s;
                    }
                }
                else
                {
/* 				printf("conserve\n");*/
                    coeff_prec = coeff;

                }
                coeff = coeff->s;
            }
            neurone[def_groupe[gpe_to_prune].premier_ele + i].coeff =
                coeff_prems;
        }
    }

#ifdef DEBUG
    printf("FIN PRUNING\n");
    getchar();
    getchar();
#endif
}
