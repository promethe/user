/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct
{
   FILE *f;
   int continuer;
   char *file;
   float *val;
   int input_size;
   float precision;            /*precision du calcul pour l'etude de l'activite */
} file_data;

int lecture_ligne_res(int n, FILE * f, float *val)
{
   int i, j;

   /*for(j=0;j<3;j++) chp .s, .s1, .s2 */
   for (i = 0; i < n; i++)     /*longueur d'une ligne =longueur du groupe */
   {
      j = fscanf(f, "%f", &val[i]);
      if (j != 1)
      {
         cprints("sortie de lireligne de test_result prematurement\n");
         return j;
      }
   }
   return 1;
}

void function_test_results(int numero)
{

   int deb, longueur, deb_e, longueur_e, increment_e;
   int i, x, gpe_entree;
   int nlink;
   char param_link[255], pre[255];
   FILE *f;
   file_data *data = NULL;
   longueur = def_groupe[numero].nbre;

   /*get the string of the input link */
   nlink = find_input_link(numero, 0);
   /*get info on input */
   gpe_entree = liaison[nlink].depart;
   deb_e = def_groupe[gpe_entree].premier_ele;
   longueur_e = def_groupe[gpe_entree].nbre;
   increment_e = longueur_e / (def_groupe[gpe_entree].taillex * def_groupe[gpe_entree].tailley);
   dprints("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
   deb = def_groupe[numero].premier_ele;


   /*data*/
   data = def_groupe[numero].data;
   /*s'il y a un lien en entree*/
   if (nlink > -1)
   {
      /*debut initialisation */
      if (data == NULL)
      {

         /*recherche si le lien contient un nom de fichier */
         if (prom_getopt(liaison[nlink].nom, "-f", param_link) == 2)
         {
            /*si oui, tente de l'ouvrir */
            f = fopen(param_link, "r");
            if (f == NULL)
            {
               EXIT_ON_ERROR("Cannot open input file: %s\n", param_link);
            }
            else
            {
               /*on creer la structure qui contiendra le pointeur vers ce fichier et on init */
               data = (file_data *) malloc(sizeof(file_data));
               if (data == NULL)
               {
                  EXIT_ON_ERROR("Erreur: impossible d'allouer la structure data_file du groupe:%d\n",numero);
               }
               data->f = f;
               /*neurone[deb].flag=-1; */
               data->file = (char *) malloc(255);
               strcpy(data->file, param_link);
               if (prom_getopt(liaison[nlink].nom, "-p", pre) == 2) sscanf(pre, "%f", &data->precision);
               def_groupe[numero].data = data;
               TRY_FSCANF(1,f, "group size: %d", &(data->input_size));
               if (data->input_size != def_groupe[gpe_entree].taillex * def_groupe[gpe_entree].tailley)
               {
                  EXIT_ON_ERROR("Erreur: la taille du groupe en entree n'est pas la meme que celle du fichier!:%d\n",numero);
               }
               else  data->val = malloc( /*3 */ data->input_size * sizeof(float));
            }
         }
         else  cprints("No input file found on %d\n", numero);
      }
   }

   /*affiche la liason */
   if (strcmp(liaison[nlink].nom, "") != 0  && strcmp(liaison[nlink].nom, ".") != 0  && strcmp(liaison[nlink].nom, "?") != 0)
      cprints("%s\n", liaison[nlink].nom);
   /*recupere les data */
   f = data->f;


   if (f != NULL)
   {
      /*on lit son contenu pour obtenir l'index du neurone a mettre a 1 */
      if (lecture_ligne_res(data->input_size, f, data->val) == EOF)
      {
         cprints("Plus d'entree a lire: %d \n", numero);
         cprints("Voulez vous: \n 1) n'affecter aucune sortie (-1)\n 2) relancer la lecture depuis le debut \n 3) quitter\n");
         cscans("%d", &i);
         if (i == 1)
         {
            neurone[deb].flag = 1;
            x = -1;
         }
         else if (i == 2)
         {
            fclose(f);
            data->f = fopen(data->file, "r");
            f = data->f;
            lecture_ligne_res(data->input_size, f, data->val);
         }
         else EXIT_ON_ERROR("la valeur lue n'est ni 1 ni 2\n");
      }
   }



   /*RAZ*/ for (i = deb; i < deb + longueur; i++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
   }
   x = 0;
   /*comparaison entre les valeurs du groupe en entree et le fichier */
   for (i = deb_e + increment_e - 1; i < deb_e + longueur_e; i += increment_e)
   {
      if (fabs(neurone[i].s2 - data->val[x]) > data->precision)
      {
         cprints ("ATTENTION! 1 activite differente de celle enregistree a ete detecte: %f au lieu de %f\n", neurone[i].s2, data->val[x]);
         neurone[deb + x].s = neurone[deb + x].s1 = neurone[i].s2 - data->val[x];   neurone[deb + x].s2 = 1.;
      }
      x++;
   }
   cprints("to continue,enter a number...\n");
   cscans("%d", &x);
   dprints("===========sortie de %s\n", __FUNCTION__);
}
