/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_instar_gpe f_instar_gpe
\ingroup libNN_IO

\brief Application d'un instar sur un groupe de neurones (d'apres le model de Grosseberg)

Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

\details

\section Description

Calcul d'une normalisation des images d'apres le modele Instar de Grosseberg.

\f[
eps*d(ni)/dt = -Ani + (b - ni)*pi -ni * \sum(pj)(j!=i)
\f]

Si A=1 :
A l'equilibre
ni =((bP)/(1+P))*qi

\f$ P=sum(pj) \f$ (quelquesoit j) !! c'est bien sur tous les pj entrants
et qi=pj/P

L'activite total de la carte vaut ((bP)/(1+P))<=b

b represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc b=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (bp)/(A+p) ici.


\section Options
Passage par option (prom_get_opt) des differentes entrees :
-input : groupe contenant le signal de stimulation
         Le nombre de neurones du groupe doit correspondre a la taille 
   du groupe entrant.
-A < down > : si pas d'option -A sur un des liens : valeur par defaut =1
              si option avec une valeur donnee : cette valeur est utilisee
        si option sans valeur donnee : la sortie s1 du premier neurone 
        du groupe d'entree est utilisee
-B < up > : fonctionnement similaire a -A (valeur par defaut =sx*sy)

\file 
\ingroup f_instar_gpe

\brief Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

Author: Maickael Maillard
Created: 01/04/2004
Modified:
- author: M. Maillard
- description: specific file creation
- date: 19/09/2005

- author: A. de RENGERVE
- description: correction d'une division par 0 potentielle
- date: 08/12/2009

Theoritical description:
 - \f$  LaTeX equation: eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i) \f$  
Si A=1 :
A l'equilibre
\f$ ni =((bP)/(1+P))*qi \f$


Description: 
Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i)

Si A=1 :
A l'equilibre
ni =((bP)/(1+P))*qi

P=sum(pj)(quelquesoit j) !! c'est bien sur tous les pj entrants
et qi=pj/P

L'activite total de la carte vaut ((bP)/(1+P))<=b

b represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc b=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (bp)/(A+p) ici.

Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

Parametres : -A down (=1 par defaut)
       -B up (=sx*sy par defaut)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/


/* #define DEBUG  */

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

struct instar_gpe_data
{
  int deb_gpe;
  int deb_gpe_input;
  int sx;
  int sy;
  float up;
  int up_gpe;
  float down;
  int down_gpe;
};

void function_instar_gpe(int Gpe)
{
  int index_of_link, success, number_of_links;
  int i, j, k, l, pos;
  int sx=-1, sy=-1;
  int deb_gpe = -1, deb_gpe_input = -1;
  int input_gpe = -1, up_gpe=-1, down_gpe=-1;
  int init_up=-1, init_down=-1;

  float f_sum = 0., f_norm = 0.;
  float down=-1;
  float up = 1.;              /*par defaut on considere que c'est 1
      * pour faire des sorties de type
      * neurone */
  char param[256];

  struct instar_gpe_data *this_instar_gpe_data;
  type_liaison *lien;   

  dprints("entree dans fonction instar_gpe %s \n", def_groupe[Gpe].no_name);

  if (def_groupe[Gpe].data == NULL)
  {
    /** On recherche les liens entrants */
    for (number_of_links = 0; (index_of_link = find_input_link(Gpe, number_of_links)) != -1; number_of_links++)
    {
      lien = &liaison[index_of_link];

      /** On recherche le lien input */
      if (prom_getopt(lien->nom, "input", param) > 0) 
      {
        input_gpe = lien->depart;
        sx = def_groupe[Gpe].taillex;
        sy = def_groupe[Gpe].tailley;
        if ((sx != def_groupe[input_gpe].taillex) || (sy != def_groupe[input_gpe].tailley))
        {
           EXIT_ON_ERROR("Gpe : %s f_instar_gpe : le nombre de neurones du groupe ne correspond pas a la taille du groupe entrant\n", def_groupe[Gpe].no_name);
        }
      }

      /** Looking for coeff A */
      success = prom_getopt(lien->nom, "A", param);
      if(success ==2)
      {
        down = (float) atof(param);
        init_down=1;
      }
      else if(success==1)
      {
        down_gpe = lien->depart;
        init_down=1;
        down=neurone[def_groupe[down_gpe].premier_ele].s1;
      }

      /** Looking for coeff B */
      success = prom_getopt(lien->nom, "B", param);
      if(success ==2)
      {
        up = (float) atof(param);
        init_up=1;
      }
      else if(success==1)
      {
        up_gpe = lien->depart;
        init_up=1;
        up=neurone[def_groupe[up_gpe].premier_ele].s1;
      }  
    }
    if (input_gpe == -1)
    {
      index_of_link = find_input_link(Gpe, 0);
      if(index_of_link==-1)
        EXIT_ON_ERROR("No input link to instar_gpe (%s)\n",def_groupe[Gpe].no_name);

      lien = &liaison[index_of_link];
      input_gpe = lien->depart;
      sx = def_groupe[Gpe].taillex;
      sy = def_groupe[Gpe].tailley;
      if ((sx != def_groupe[input_gpe].taillex) || (sy != def_groupe[input_gpe].tailley))
        EXIT_ON_ERROR("Gpe : f_instar_gpe %s : le nombre de neurones du groupe ne correspond pas a la taille du groupe entrant\n", def_groupe[Gpe].no_name);
      
      if(find_input_link(Gpe, 1)!=-1)
        EXIT_ON_ERROR("Gpe : f_instar_gpe %s : too many input links without option -input defined\n", def_groupe[Gpe].no_name);
      
    }
    if(init_up==-1)
    {
      up = (float) sx *(float) sy;
      /*comme vu dans les explication on il faut up par rapport
      * a la carte */
    }
    if(init_down==-1)
      down = 1.;
      
    deb_gpe = def_groupe[Gpe].premier_ele;
    deb_gpe_input = def_groupe[input_gpe].premier_ele;
    
    def_groupe[Gpe].data = ALLOCATION(struct instar_gpe_data);
    this_instar_gpe_data = (struct instar_gpe_data *) def_groupe[Gpe].data;
    this_instar_gpe_data->deb_gpe = deb_gpe;
    this_instar_gpe_data->deb_gpe_input = deb_gpe_input;
    this_instar_gpe_data->sx = sx;
    this_instar_gpe_data->sy = sy;
    this_instar_gpe_data->up = up;
    this_instar_gpe_data->down = down;
    this_instar_gpe_data->up_gpe = up_gpe;
    this_instar_gpe_data->down_gpe = down_gpe;
    dprints("init : up = %f, down = %f\n",up, down);
      
  }
  else
  {
    this_instar_gpe_data = (struct instar_gpe_data *) def_groupe[Gpe].data;
    deb_gpe = this_instar_gpe_data->deb_gpe;
    deb_gpe_input = this_instar_gpe_data->deb_gpe_input;
    sx = this_instar_gpe_data->sx;
    sy = this_instar_gpe_data->sy;
    up_gpe = this_instar_gpe_data->up_gpe;
    down_gpe = this_instar_gpe_data->down_gpe;
    
    /* recuperation up and down */
    if(up_gpe==-1)
      up = this_instar_gpe_data->up;
    else /* get up from input gpe */
      up=neurone[def_groupe[up_gpe].premier_ele].s1;
      
    if(this_instar_gpe_data->down_gpe==-1)
      down = this_instar_gpe_data->down;
    else
     /* get down from input gpe*/
     down=neurone[def_groupe[down_gpe].premier_ele].s1;
  }

  dprints("up = %f, down = %f\n",up, down);

  for (i = 0; i < sy; i++)
    for (j = 0; j < sx; j++)
      f_sum += neurone[deb_gpe_input + i * sx + j].s1;

  if (isequal(f_sum,0.))       /*cas ou les entrees sont nulles */
  {
    for (k = 0; k < sy; k++)
      for (l = 0; l < sx; l++)
      {
        pos = deb_gpe + k * sx + l;
        neurone[pos].s = 0.;
        neurone[pos].s1 = 0.;
        neurone[pos].s2 = 1.;   /*pour que le groupe suivant aprenne sur ces neurones quoique dans le 
                                           cas ou y'a absolument rien en entree on peut se demander si'il faut l'apprendre... */
      }
  }
  else
  {
    f_norm = up  / (down + f_sum); /* It was (up * f_sum) / ...  but f_norm was divide by f_sum to compute .s -> simplification */
    /*printf("f_norm = %f , f_sum=%f, up =%f, down=%f \n",f_norm,  f_sum, up, down);*/
    /* modif AR : ici pour eviter /0 si down =0 et f_sum=0*/
    for (k = 0; k < sy; k++)
      for (l = 0; l < sx; l++)
      {
        pos = deb_gpe + k * sx + l;
        neurone[pos].s = f_norm * (neurone[deb_gpe_input + k * sx + l].s1 ); /* It was divide by f_sum but f_norm is already divided by _f_sum (simplification ) */
        neurone[pos].s1 = neurone[pos].s;
        neurone[pos].s2 = 1.;   /*pour que le groupe suivant apprenne sur ces neurones */
      }
  }
    
}
