/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_all_n_link.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: M.LAGARDE
 - description: specific file creation
 - date: 05/05/2006

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define		MAX_LEN_INT	12

int **get_args(char *line_args)
{
  int i = 0, j = 0, k = 0, l = 0;
  char tmp[MAX_LEN_INT];
  int **args = NULL;

  memset(tmp, 0, MAX_LEN_INT);

  if ((args = (int **) malloc(5 * sizeof(int *))) == NULL)
  {
    perror("malloc");
    fprintf(stderr, "f_all_n_link : Erreur d'allocation memoire pour les aguments\n");
    exit(1);
  }
  memset(args, 0, 5 * sizeof(int *));

  if ((args[l] = (int *) malloc(4 * sizeof(int))) == NULL)
  {
    perror("malloc");
    fprintf(stderr, "f_all_n_link : Erreur d'allocation memoire pour les aguments\n");
    exit(1);
  }
  memset(args[l], 0, 4 * sizeof(int));

  for (i = 0, j = 0, k = 0, l = 0; line_args[i]; i++)
  {
    if ((line_args[i] >= '0') && (line_args[i] <= '9')) tmp[j++] = line_args[i];
    else
    {
      switch (line_args[i])
      {
      case 'x':
      {
        args[l][k++] = (int) 'x';
        tmp[j] = 0;
        args[l][k++] = atoi(tmp);
      }
        break;
      case 'y':
      {
        args[l][k++] = (int) 'y';
        tmp[j] = 0;
        args[l][k++] = atoi(tmp);
      }
        break;
      case '=':
      {
        j = 0;
      }
        break;
      case ',':
      {
        l++;
        if ((args[l] = (int *) malloc(4 * sizeof(int))) == NULL)
        {
          perror("malloc");
          fprintf(stderr, "f_all_n_link : Erreur d'allocation memoire pour les aguments\n");
          exit(1);
        }
        memset(args[l], 0, 4 * sizeof(int));
        k = 0;
        j = 0;
      }
        break;
      default:
      {
        fprintf(stderr, "f_all_n_link : Erreur dans les parametres du lien virtuel\n");
        exit(2);
      }
      }
    }
  }
  l++;
  args[l] = NULL;
  return args;
}

void x_to_x(int in_x, int out_x, int numero, int num_entry_grp)
{
  int first, first_dep;
  int i_sg, j_sg_dep;
  int inc_dep;
  int first_sg, first_sg_dep;
  int nr_sg, nr_sg_dep;
  int i, j;
  int nbre_sg, nbre_sg_dep;

  /* PREMIERS NEURONES DES GROUPES */
  first = def_groupe[numero].premier_ele;
  first_dep = def_groupe[num_entry_grp].premier_ele;

  /* INCREMENTS A APPLIQUER */
  inc_dep = def_groupe[num_entry_grp].nbre / (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley);

  /* NOMBRE DE SOUS GROUPES */
  nr_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) / out_x) + (((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_x) ? 1 : 0);

  nr_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) / in_x) + (((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_x) ? 1 : 0);

  /* PARCOURS DES SOUS GROUPES */
  for (i_sg = 0, j_sg_dep = 0; j_sg_dep < nr_sg_dep; i_sg = (i_sg + 1) % nr_sg, j_sg_dep++)
  {
    /* PREMIERS NEURONES DES SOUS GROUPES */
    first_sg = first + (i_sg * out_x);
    first_sg_dep = first_dep + (j_sg_dep * inc_dep * in_x);

    /* NOMBRE DE NEURONES DANS LE SOUS GROUPE COURANT */
    if ((first_sg + out_x) > (first + (def_groupe[numero].taillex * def_groupe[numero].tailley))) nbre_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_x);
    else nbre_sg = out_x;

    if ((first_sg_dep + (in_x * inc_dep)) > (first_dep + (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley * inc_dep))) nbre_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_x) * inc_dep;
    else nbre_sg_dep = in_x * inc_dep;

    /* PARCOURS DES NEURONES DANS LES SOUS GROUPES */
    for (i = 0, j = (inc_dep - 1); j < nbre_sg_dep; i = (i + 1) % out_x, j += inc_dep)
    {
      if (i >= nbre_sg) i = 0;
      neurone[i + first_sg].s += neurone[j + first_sg_dep].s;
      neurone[i + first_sg].s1 += neurone[j + first_sg_dep].s1;
      neurone[i + first_sg].s2 += neurone[j + first_sg_dep].s2;
    }
  }
}

void x_to_y(int in_x, int out_y, int numero, int num_entry_grp)
{
  int first, first_dep;
  int i_sg, j_sg_dep;
  int inc_dep;
  int first_sg, first_sg_dep;
  int nr_sg, nr_sg_dep;
  int i, j;
  int nbre_sg, nbre_sg_dep;

  /* PREMIERS NEURONES DES GROUPES */
  first = def_groupe[numero].premier_ele;
  first_dep = def_groupe[num_entry_grp].premier_ele;

  /* INCREMENTS A APPLIQUER */
  inc_dep = def_groupe[num_entry_grp].nbre / (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley);

  /* NOMBRE DE SOUS GROUPES */
  nr_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) / out_y) + (((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_y) ? 1 : 0);

  nr_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) / in_x) + (((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_x) ? 1 : 0);

  /* PARCOURS DES SOUS GROUPES */
  for (i_sg = 0, j_sg_dep = 0; j_sg_dep < nr_sg_dep; i_sg = (i_sg + 1) % nr_sg, j_sg_dep++)
  {
    /* PREMIERS NEURONES DES SOUS GROUPES */
    first_sg = first + (((i_sg * out_y) % def_groupe[numero].tailley) * def_groupe[numero].taillex) + ((i_sg * out_y) / def_groupe[numero].tailley);
    first_sg_dep = first_dep + (j_sg_dep * inc_dep * in_x);

    /* NOMBRE DE NEURONES DANS LE SOUS GROUPE COURANT */
    if ((first_sg + ((out_y - 1) * def_groupe[numero].taillex)) > (first + (def_groupe[numero].taillex * def_groupe[numero].tailley))) nbre_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_y);
    else nbre_sg = out_y;

    if ((first_sg_dep + (in_x * inc_dep)) > (first_dep + (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley * inc_dep))) nbre_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_x) * inc_dep;
    else nbre_sg_dep = in_x * inc_dep;

    /* PARCOURS DES NEURONES DANS LES SOUS GROUPES */
    for (i = 0, j = (inc_dep - 1); j < nbre_sg_dep; i += def_groupe[numero].taillex, j += inc_dep)
    {
      if ((i / def_groupe[numero].taillex) >= out_y) i = 0;
      neurone[i + first_sg].s += neurone[j + first_sg_dep].s;
      neurone[i + first_sg].s1 += neurone[j + first_sg_dep].s1;
      neurone[i + first_sg].s2 += neurone[j + first_sg_dep].s2;
    }
  }
  (void) nbre_sg;
}

void y_to_x(int in_y, int out_x, int numero, int num_entry_grp)
{
  int first, first_dep;
  int i_sg, j_sg_dep;
  int inc_dep;
  int first_sg, first_sg_dep;
  int nr_sg, nr_sg_dep;
  int i, j;
  int nbre_sg, nbre_sg_dep;

  /* PREMIERS NEURONES DES GROUPES */
  first = def_groupe[numero].premier_ele;
  first_dep = def_groupe[num_entry_grp].premier_ele;

  /* INCREMENTS A APPLIQUER */
  inc_dep = def_groupe[num_entry_grp].nbre / (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley);

  /* NOMBRE DE SOUS GROUPES */
  nr_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) / out_x) + (((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_x) ? 1 : 0);

  nr_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) / in_y) + (((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_y) ? 1 : 0);

  /* PARCOURS DES SOUS GROUPES */
  for (i_sg = 0, j_sg_dep = 0; j_sg_dep < nr_sg_dep; i_sg = (i_sg + 1) % nr_sg, j_sg_dep++)
  {
    /* PREMIERS NEURONES DES SOUS GROUPES */
    first_sg = first + (i_sg * out_x);
    first_sg_dep = first_dep + (((((j_sg_dep * in_y) % def_groupe[num_entry_grp].tailley) * def_groupe[num_entry_grp].taillex) + ((j_sg_dep * in_y) / def_groupe[num_entry_grp].tailley)) * inc_dep);

    /* NOMBRE DE NEURONES DANS LE SOUS GROUPE COURANT */
    if ((first_sg + out_x) > (first + (def_groupe[numero].taillex * def_groupe[numero].tailley))) nbre_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_x);
    else nbre_sg = out_x;

    if ((first_sg_dep + ((in_y - 1) * inc_dep * def_groupe[num_entry_grp].taillex) + (inc_dep - 1)) > (first_dep + (inc_dep * def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley))) nbre_sg_dep = ((def_groupe[num_entry_grp].taillex
        * def_groupe[num_entry_grp].tailley) % in_y) * inc_dep;
    else nbre_sg_dep = in_y * inc_dep;

    /* PARCOURS DES NEURONES DANS LES SOUS GROUPES */
    for (i = 0, j = (inc_dep - 1); j < ((((in_y - 1) * inc_dep * def_groupe[num_entry_grp].taillex)) + inc_dep); i = (i + 1) % out_x, j += (inc_dep * def_groupe[num_entry_grp].taillex))
    {
      if (i >= nbre_sg) i = 0;
      neurone[i + first_sg].s += neurone[j + first_sg_dep].s;
      neurone[i + first_sg].s1 += neurone[j + first_sg_dep].s1;
      neurone[i + first_sg].s2 += neurone[j + first_sg_dep].s2;
    }
  }
  (void) nbre_sg_dep;
}

void y_to_y(int in_y, int out_y, int numero, int num_entry_grp)
{
  int first, first_dep;
  int i_sg, j_sg_dep;
  int inc_dep;
  int first_sg, first_sg_dep;
  int nr_sg, nr_sg_dep;
  int i, j;
  int nbre_sg, nbre_sg_dep;

  /* PREMIERS NEURONES DES GROUPES */
  first = def_groupe[numero].premier_ele;
  first_dep = def_groupe[num_entry_grp].premier_ele;

  /* INCREMENTS A APPLIQUER */
  inc_dep = def_groupe[num_entry_grp].nbre / (def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley);

  /* NOMBRE DE SOUS GROUPES */
  nr_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) / out_y) + (((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_y) ? 1 : 0);

  nr_sg_dep = ((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) / in_y) + (((def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley) % in_y) ? 1 : 0);

  /* PARCOURS DES SOUS GROUPES */
  for (i_sg = 0, j_sg_dep = 0; j_sg_dep < nr_sg_dep; i_sg = (i_sg + 1) % nr_sg, j_sg_dep++)
  {
    /* PREMIERS NEURONES DES SOUS GROUPES */
    first_sg = first + (((i_sg * out_y) % def_groupe[numero].tailley) * def_groupe[numero].taillex) + ((i_sg * out_y) / def_groupe[numero].tailley);
    first_sg_dep = first_dep + (((((j_sg_dep * in_y) % def_groupe[num_entry_grp].tailley) * def_groupe[num_entry_grp].taillex) + ((j_sg_dep * in_y) / def_groupe[num_entry_grp].tailley)) * inc_dep);

    /* NOMBRE DE NEURONES DANS LE SOUS GROUPE COURANT */
    if ((first_sg + ((out_y - 1) * def_groupe[numero].taillex)) > (first + (def_groupe[numero].taillex * def_groupe[numero].tailley))) nbre_sg = ((def_groupe[numero].taillex * def_groupe[numero].tailley) % out_y);
    else nbre_sg = out_y;

    if ((first_sg_dep + ((in_y - 1) * inc_dep * def_groupe[num_entry_grp].taillex) + (inc_dep - 1)) > (first_dep + (inc_dep * def_groupe[num_entry_grp].taillex * def_groupe[num_entry_grp].tailley))) nbre_sg_dep = ((def_groupe[num_entry_grp].taillex
        * def_groupe[num_entry_grp].tailley) % in_y) * inc_dep;
    else nbre_sg_dep = in_y * inc_dep;

    /* PARCOURS DES NEURONES DANS LES SOUS GROUPES */
    for (i = 0, j = (inc_dep - 1); j < ((((in_y - 1) * inc_dep * def_groupe[num_entry_grp].taillex)) + inc_dep); i += def_groupe[numero].taillex, j += (inc_dep * def_groupe[num_entry_grp].taillex))
    {
      if ((i / def_groupe[numero].taillex) >= out_y) i = 0;
      neurone[i + first_sg].s += neurone[j + first_sg_dep].s;
      neurone[i + first_sg].s1 += neurone[j + first_sg_dep].s1;
      neurone[i + first_sg].s2 += neurone[j + first_sg_dep].s2;
    }
  }
  (void) nbre_sg_dep;
  (void) nbre_sg;
}

void function_all_n_link(int numero)
{
  int nr;
  int first;
  int i = 0, j = 0;
  int num_entry_grp = -1;
  int **args = NULL;

  /* nombre de neurones dans le groupe */
  nr = def_groupe[numero].nbre;
  /* premier neurone du groupe */
  first = def_groupe[numero].premier_ele;

  /* INIT : on recherche le lien algo en entree et le groupe de depart de ce lien */
  for (i = 0; i < nbre_liaison; i++)
    if (liaison[i].arrivee == numero)
    {
      num_entry_grp = liaison[i].depart;
      /* On recupere les arguments du lien */
      if (def_groupe[numero].ext == NULL) def_groupe[numero].ext = get_args(liaison[i].nom);
      break;
    }
  args = (int **) def_groupe[numero].ext;
  /* si on a pas trouve de lien en entree */
  if (num_entry_grp < 0)
  {
    fprintf(stdout, "Il n'y a pas de lien en entree !\n");
    exit(1);
  }

  /* On remet a 0 les sorties des neurones */
  for (j = 0; j < nr; j++)
    neurone[j + first].s = neurone[j + first].s1 = neurone[j + first].s2 = 0.0;

  for (i = 0; args[i] != NULL; i++)
  {
    switch (((char) args[i][0]))
    {
    case 'x':
    {
      switch (((char) args[i][2]))
      {
      case 'x':
        x_to_x(args[i][1], args[i][3], numero, num_entry_grp);
        break;
      case 'y':
        x_to_y(args[i][1], args[i][3], numero, num_entry_grp);
        break;
      default:
      {
        fprintf(stderr, "f_all_n_link : Erreur dans les parametres du lien virtuel\n");
        exit(2);
      }
      }
    }
      break;
    case 'y':
    {
      switch (((char) args[i][2]))
      {
      case 'x':
        y_to_x(args[i][1], args[i][3], numero, num_entry_grp);
        break;
      case 'y':
        y_to_y(args[i][1], args[i][3], numero, num_entry_grp);
        break;
      default:
      {
        fprintf(stderr, "f_all_n_link : Erreur dans les parametres du lien virtuel\n");
        exit(2);
      }
      }
    }
      break;
    default:
    {
      fprintf(stderr, "f_all_n_link : Erreur dans les parametres du lien virtuel\n");
      exit(2);
    }
    }
  }
}
