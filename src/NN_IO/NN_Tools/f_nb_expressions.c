/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_nb_expressions.c 
\brief 

Author: Sofiane Boucenna
Created: 
Modified: 
- author:
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-sert juste � compter le nombre d expressions que doit faire le robot

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet...

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>

//#define DEBUG



typedef struct
{
  int gpe;
  int nb;
}data;


void new_nb_expressions(int Gpe)
{
  data *my_data;      /* structure de cette fonction */
  int gpe = -1;
  int i = 0;
  int l = -1;
  char param[256];  
  
#ifdef DEBUG
  printf("######enter in %s\n", __FUNCTION__);
#endif
  
  /*
   *on cree la structure qui contiendra 
   *les donnees
   */
  
  my_data = (data *) malloc(sizeof(data));
  
  if (my_data == NULL)
    {
      printf("error in memory allocation in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  
  
  l = find_input_link(Gpe, i);
  while (l != -1)
    {
      if (prom_getopt(liaison[l].nom,"-nb",param)==2)
	{
	  gpe = my_data->gpe = liaison[l].depart;
	  my_data->nb=atoi(param);
	}
      
      l = find_input_link(Gpe, i);
      i++;
    }

  printf("on a recuperer les parametres\n"); 
  
  if (gpe == -1)
    {
      printf(" Error, %s group %i doesn't have an input for the supervision\n", __FUNCTION__,
	     Gpe);
      exit(EXIT_FAILURE);
    }
  
  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = my_data;
  
#ifdef DEBUG
  printf("######end of %s\n", __FUNCTION__);
#endif
  
  
}

/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/


void function_nb_expressions(int Gpe)
{
  
  
  data *my_data = NULL;    
  
  
  

#ifdef DEBUG
  printf("##########enter in %s, group %s\n", __FUNCTION__, def_groupe[Gpe].no_name);
#endif
  
  
  my_data = (data *) def_groupe[Gpe].data; 
  
  if (my_data == NULL)
    {
      printf("error while loading data in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
#ifdef DEBUG 
  printf("groupe precedent=%s\n",def_groupe[my_data->gpe].no_name);
  printf("nombre d expression=%i\n",my_data->nb);
#endif

  if(my_data->nb != 0)
    {
      my_data->nb--;
      neurone[def_groupe[Gpe].premier_ele].s = neurone[def_groupe[Gpe].premier_ele].s1 =neurone[def_groupe[Gpe].premier_ele].s2 = 0;
    }
  else
    {
      neurone[def_groupe[Gpe].premier_ele].s = neurone[def_groupe[Gpe].premier_ele].s1 =neurone[def_groupe[Gpe].premier_ele].s2 = 1;
    }

}
