/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\ingroup libNN_IO
\defgroup f_switch_2groups f_switch_2groups

\brief switch between two groups at each interation

\section Modified
- author: C.Giovannangeli
- description: ???
- date: ???

\details
  La sortie alterne entre l'entrée 1 et l'entrée 2 à chaque entrée dans la
  fonction

\section Options
- first :   à mettre sur le lien venant du premier groupe

\section Todo
  stop using <libx.h> (really ??, maybe <public.h> which has been already done)
 */
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


typedef struct MyData_f_switch_2groups
{
   char flip_flop;
   int gp_switch;
   int gp_1;
   int gp_2;
} MyData_f_switch_2groups;

/*#define DEBUG*/
void function_switch_2groups(int numero)
{
   MyData_f_switch_2groups *my_data = NULL;
   char flip_flop=0;
   int gp_1=-1, gp_2=-1, gpe_to_copy, gp_switch=-1;
   int l, i=0;
#ifdef DEBUG
   printf("-------------------------------------------------\n");
   printf("function_switch_2groups---------------------\n");
#endif

   if (def_groupe[numero].data == NULL)
   {
      /*---------------------------*/
      /*Looking for the input group */
      l = find_input_link(numero, i);
      while(l!=-1)
      {
         /*---------------------------------*/
         /*Getting option on the input link */

         if (strcmp(liaison[l].nom,"first") == 0)
            gp_1 = liaison[l].depart;
         else if(strcmp(liaison[l].nom,"switch") == 0)
            gp_switch = liaison[l].depart;
         else if(strcmp(liaison[l].nom,"sync") != 0)
            gp_2 = liaison[l].depart;

         i++;
         l = find_input_link(numero, i);
      }

      if(gp_1==-1 || gp_2==-1)
         EXIT_ON_ERROR("Manque un groupe en entree");
      if(gp_switch == -1)
         PRINT_WARNING("mode flip flop activated");

      if(def_groupe[gp_1].nbre != def_groupe[numero].nbre || def_groupe[gp_2].nbre != def_groupe[numero].nbre)
         EXIT_ON_ERROR("Taille neuronale des entree et sorties differentes");

      my_data = malloc(sizeof(MyData_f_switch_2groups));
      if (my_data == NULL)
         EXIT_ON_ERROR("erreur d'allocation memoire");

      my_data->gp_1 = gp_1;
      my_data->gp_2 = gp_2;
      my_data->gp_switch = gp_switch;
      my_data->flip_flop = flip_flop;
      def_groupe[numero].data = (void *) my_data;
   }
   else
   {
      my_data = (MyData_f_switch_2groups *) def_groupe[numero].data;
      gp_1 = my_data->gp_1;
      gp_2 = my_data->gp_2;
      gp_switch = my_data->gp_switch;
      flip_flop = my_data->flip_flop;
   }
   if(gp_switch != -1)
   {
      if(neurone[def_groupe[gp_switch].premier_ele].s1 > 0.5)
         gpe_to_copy=gp_2;
      else
         gpe_to_copy=gp_1;
   }
   else
   {
      if(flip_flop==0)
      {
         gpe_to_copy=gp_1;
         my_data->flip_flop=1;
      }
      else
      {
         gpe_to_copy=gp_2;
         my_data->flip_flop=0;
      }
   }

   for(i=0;i<def_groupe[numero].nbre;i++)
   {
      neurone[def_groupe[numero].premier_ele+i].s=neurone[def_groupe[gpe_to_copy].premier_ele+i].s;
      neurone[def_groupe[numero].premier_ele+i].s1=neurone[def_groupe[gpe_to_copy].premier_ele+i].s1;
      neurone[def_groupe[numero].premier_ele+i].s2=neurone[def_groupe[gpe_to_copy].premier_ele+i].s2;
   }
   def_groupe[numero].ext=def_groupe[gpe_to_copy].ext;

}
