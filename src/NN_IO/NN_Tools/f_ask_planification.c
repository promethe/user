/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_ask_planification.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction VB: Cette fonction fixe le mode d'utilisation (planification ou
exploration) grace a la variable globale "planification" (1=planif,0=explo)
 Cette fonction doit avoir un seul neurone : sa valeur sera celle de la
 variable planification.
 De plus, la variable globale "vigilence" est initialisee (vigilence est,
 pour le moment en tout cas, toujours basse en planification, et haute ou
 basse en exploration)
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include "tools/include/local_var.h"
void function_ask_planification(int gpe_sortie)
{
   int rep = 0;
   static int deb_s;
   static int first = 1;
   float VIGILANCE_MIN = 0.1, VIGILANCE_MAX = 0.9999;

   if (first)
   {
      first = 0;
      deb_s = def_groupe[gpe_sortie].premier_ele;
   }

   printf("Planification (0/1) ? ");
   fflush(stdin);
   cscans("%d", &rep);
   printf("\nVIGILANCE_MIN=%f & VIGILANCE_MAX=%f, rep: %d\n", VIGILANCE_MIN, VIGILANCE_MAX, rep);
   if (rep)
   {
      planification = 1;
      neurone[deb_s].s2 = neurone[deb_s].s1 = neurone[deb_s].s = 1;
      printf("PLAN Old vigilance=%f =>", vigilence);
      /*  vigilence=VIGILANCE_MIN; */
      /*        vigilence=0.99; */
      printf(" New vigilance=%f\n", vigilence);
   }
   else
   {
      planification = 0;
      neurone[deb_s].s2 = neurone[deb_s].s1 = neurone[deb_s].s = 0;
      printf("Old Vigilance=%f =>", vigilence);
      /*  vigilence=VIGILANCE_MAX;  Demo sans visions */
      /*vigilence=VIGILANCE_MIN;        Demo pavage de la piece */
      /*        vigilence=0.99; */
      printf(" New vigilance=%f\n", vigilence);
   }
}
