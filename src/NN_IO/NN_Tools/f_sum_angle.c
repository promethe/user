/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sum_angle.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction qui somme les angles represent�es par les neurones des groupes en entr�es.
Exemple d'usage: obtenir un angle en absolue en sommant un angle relatif+ la boussole
ou angle relatif =angle absolue -angle boussole....
pour le moment  -180 180;
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
 #include <Kernel_Function/trouver_entree.h>
 #include <math.h>
#define SEUIL 0
#define nb_max 2
/*#define DEBUG
#define DEBUGu*/
void function_sum_angle (int numero)
{
  int deb,debe,longueur,nbe=0,pos=-1,index;
  int i=0,j=0,taille_groupe_e,signe=0;
  int gpe_entree=-1;
  float scale,max=0.,offset;
 int offset_pos=-1,angle_pos=-1;

#ifdef DEBUG
  printf("~~~~~~~~~~~entree dans fonction sum_angle %s\n",__FUNCTION__);
#endif


 deb=def_groupe[numero].premier_ele;
 longueur=def_groupe[numero].nbre;
 offset=longueur/2;
 scale=2*M_PI/(float)longueur;

    /*R.A.Z*/
  for(i=deb;i<deb+longueur;i++)
    {
      neurone[i].s=neurone[i].s1=neurone[i].s2=0.;
    }



	/*traitement de l'offset*/
 gpe_entree=trouver_entree(numero, (char*)"offset");
debe=def_groupe[(gpe_entree)].premier_ele;
nbe=def_groupe[(gpe_entree)].nbre;
taille_groupe_e = def_groupe[(gpe_entree)].taillex*def_groupe[(gpe_entree)].tailley;
scale=2*M_PI/(float)taille_groupe_e; /*echelle, depend du nombre de neurone du groupe en entr�e*/ 
/*recherche du max*/
	for(j=0;j<nbe;j++)
	{
		if(neurone[debe+j].s2>max)
		{
			max=neurone[debe+j].s2;
			pos=j;
		}
	}
	if(pos!=-1)
	{
		offset_pos=pos;
	}
	else
	{	
		printf("defaut:milieu\n");
		neurone[(longueur/2)+deb].s=neurone[(longueur/2)+deb].s1=neurone[(longueur/2)+deb].s2=1;
		printf("f_sum_angle: pas d'activite sur le groupe: %d\n",gpe_entree);/*	exit(-1); */ 
			/*scanf("%d",&j);*/
		return;
	}
		/*max=0;
		pos=-1;*/

	/*reecherche des entrees*/
	gpe_entree=trouver_entree(numero, (char*)"inputs");
	debe=def_groupe[(gpe_entree)].premier_ele;
	nbe=def_groupe[(gpe_entree)].nbre;
	taille_groupe_e = def_groupe[(gpe_entree)].taillex*def_groupe[(gpe_entree)].tailley;
	scale=2*M_PI/(float)taille_groupe_e; 
	for(j=0;j<nbe;j++)
	{
		#ifdef DEBUG
			printf("i:%d,val:%f,max:%f\n",j,neurone[debe+j].s2,max);
		#endif
		if(fabs(neurone[debe+j].s2)>0)
		{
			pos=debe+j;
			angle_pos=j;
			
			#ifdef DEBUG
			pos=debe+j;
			max=neurone[debe+j].s2;;
			angle_tmp_p=(pos-debe)*scale -M_PI;
			#endif
			
			if(angle_pos-offset_pos>0)
				signe=-1;
			else if(angle_pos-offset_pos<0)
				signe=1;
			index=angle_pos-offset_pos;
	
			if(abs(index)>abs(signe*longueur-offset_pos+angle_pos))
				index=signe*longueur-offset_pos+angle_pos;
			index=index+longueur/2;
			if(index>=longueur)
				index-=longueur;
			if(index<0)
				index+=longueur;
			
			/*Compute the index*/
			
			if(index>=longueur)
			{
				printf("f_sum_angle: pb mod 2 pi index > longueur: %d\n",index);	
				exit(-1);  
			}
						/*Set the neuron at the specified index*/
			neurone[index+deb].s=neurone[index+deb].s1=neurone[index+deb].s2=neurone[debe+j].s2;
	
	#ifdef DEBUG
			printf("Angle:%f degre soit %f radians, longueur:%d, neurone set:%d, offset:%f,maxmax:%f\n",angle,angle*2*3.1415927/360,longueur,index,offset,maxmax);
	#endif
		}
							
	}

#ifdef DEBUG
  printf("===========sortie de %s\n",__FUNCTION__);
#endif
}



