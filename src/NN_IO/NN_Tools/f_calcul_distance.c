/** ***********************************************************
\file  calcul_distance.c
\brief calculate distance between two points

Author: Oriane Dermy
Created: 03/03/14

Theoritical description:

Description:
 This file contains a little function able to calculate distance between the goal point and the actual point used by nelder_mead function
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
//#define DEBUG 1
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>


typedef struct calcul_distance{
	int finx,finy, finz, finxyz, actualGroup;
	float positionGoal[3], positionActual[3], distance;
} type_calcul_distance;


void new_calcul_distance(int Gpe)
{
	int l, i;
	
	type_calcul_distance *my_calcul_distance=NULL;
	char param[256];

	dprints("Begining of new_calcul_distance\n");

	my_calcul_distance=(type_calcul_distance*)malloc(sizeof(type_calcul_distance));

	if(my_calcul_distance==NULL)
	{
		dprints("my_calcul_distance is NULL!\n");
		EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
	}
	
	/**Initialisation **/
	//my_calcul_distance->goalGroup=-1;
	my_calcul_distance->finx=-1;
	my_calcul_distance->finy=-1;
	my_calcul_distance->finz=-1;
	my_calcul_distance->finxyz = -1;
	my_calcul_distance->actualGroup=-1;
	my_calcul_distance->distance= -1;
	/******END Init********/
	/******************************************************Links Treatments*************************************/
	dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
		i=0;
		l = find_input_link(Gpe, i);
		while (l != -1)
		{
			/*if (prom_getopt(liaison[l].nom, "-goal", param) != 0){
				my_calcul_distance->goalGroup = liaison[l].depart;
			}*/
			if (prom_getopt(liaison[l].nom, "-x", param) != 0){
				my_calcul_distance->finx = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "-y", param) != 0){
				my_calcul_distance->finy = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "-z", param) != 0){
				my_calcul_distance->finz = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "-xyz", param)!=0){
				my_calcul_distance->finxyz = liaison[l].depart;
			}
			if (prom_getopt(liaison[l].nom, "-actual", param) != 0){
				my_calcul_distance->actualGroup = liaison[l].depart;
			}
			i++;
			l = find_input_link(Gpe, i);
		}

	}
	/*
	if (my_calcul_distance->goalGroup == -1)
	{
		dprints("%s : No goalgroup in input !\n", __FUNCTION__);
		kprints("%s : No goalgroup in input !\n", __FUNCTION__);
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}*/

	if (my_calcul_distance->finxyz == -1)
	{
		if (my_calcul_distance->finx == -1)
		{
			dprints("%s : No finxgroup in input !\n", __FUNCTION__);
			kprints("%s : No finxgroup in input !\n", __FUNCTION__);
			EXIT_ON_ERROR("xxxxx %d\n",Gpe);
		}
		if (my_calcul_distance->finy == -1)
		{
			dprints("%s : No finygroup in input !\n", __FUNCTION__);
			kprints("%s : No finygroup in input !\n", __FUNCTION__);
			EXIT_ON_ERROR("xxxxx %d\n",Gpe);
		}
		if (my_calcul_distance->finz == -1)
		{
			dprints("%s : No finzgroup in input !\n", __FUNCTION__);
			kprints("%s : No finzgroup in input !\n", __FUNCTION__);
			EXIT_ON_ERROR("xxxxx %d\n",Gpe);
		}	
	}	
	if (my_calcul_distance->actualGroup == -1)
	{
		dprints("%s : No actualgroup in input !\n", __FUNCTION__);
		kprints("%s : No actualgroup in input !\n", __FUNCTION__);
        EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}
	dprints("Groupe input is : %s\n", def_groupe[my_calcul_distance->actualGroup].no_name);

	
	dprints("End of the reading links\n");
	/****************************************End Links Treatments**********************************/
	def_groupe[Gpe].data= my_calcul_distance; /*Save of My_Data*/

	dprints("End of new_calcul_distance\n");
}


void destroy_calcul_distance(int gpe)
{
	free(def_groupe[gpe].data);
	def_groupe[gpe].data=NULL;
	def_groupe[gpe].ext=NULL;
}


void function_calcul_distance(int Gpe)
{
	float x, y, z, xg, yg, zg, distx, disty, distz, result;
	int debut, debutActGpe, debutfinx,debutfiny, debutfinz, debutfinxyz;//debutGoalGpe;
	type_calcul_distance *my_calcul_distance=NULL;
	
	dprints("At the begining of the function calcul_distance\n");

	my_calcul_distance = def_groupe[Gpe].data;
	
	debut = def_groupe[Gpe].premier_ele;
	debutActGpe = def_groupe[my_calcul_distance->actualGroup].premier_ele;
	//debutGoalGpe = def_groupe[my_calcul_distance->goalGroup].premier_ele;
	
	if(my_calcul_distance->finxyz !=-1)
	{
		if(def_groupe[my_calcul_distance->finxyz].nbre != 3)
		{
			dprints("%s :We need 3 neurons in input !\n", __FUNCTION__);
			kprints("%s : We need 3 neurons in input !\n", __FUNCTION__);
	        EXIT_ON_ERROR("xxxxx %d\n",Gpe);
		}
		debutfinxyz = def_groupe[my_calcul_distance->finxyz].premier_ele;
		xg = neurone[debutfinxyz].s1;
		yg = neurone[debutfinxyz+1].s1;
		zg = neurone[debutfinxyz+2].s1;
	}else
	{	
		debutfinx = def_groupe[my_calcul_distance->finx].premier_ele;
		debutfiny = def_groupe[my_calcul_distance->finy].premier_ele;
		debutfinz = def_groupe[my_calcul_distance->finz].premier_ele;	
		xg = neurone[debutfinx].s1;
		yg = neurone[debutfiny].s1;
		zg = neurone[debutfinz].s1;
	}
	x = neurone[debutActGpe].s1;
	y = neurone[debutActGpe+1].s1;
	z = neurone[debutActGpe+2].s1;

	
	distx = (xg - x);
	disty = (yg-y);
	distz = (zg -z);
	
	result = sqrt(distx*distx + disty*disty + distz*distz);
	
	dprints("x %f y %f z%f xg %f yg %f zg %f>>%f \n", x, y, z, xg, yg, zg, result);
	
	
	neurone[debut].s1 = neurone[debut].s = neurone[debut].s2= result;
	dprints("sortie : %f\n", neurone[debut].s1);
	dprints("end of calcul_distance Gpe %d : %s\n", Gpe, __FUNCTION__);
}
