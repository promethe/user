/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: K. PREPIN
Created: 10/09/2004
Modified:
- author: XXX
- description: XXX
- date: XX/XX/XXXX

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

La fonction suivante (f_bruit_sur_signal) doit avoir un unique groupe en entree,
et le groupe qui l'appelle doit avoir la meme taille que ce groupe en entree.
Le lien entre ces deux groupes doit etre de "un vers un" "inconditionnel".
Cette fonction activera alors les neurones du groupe qui l'appelle avec les activites des neurones du
groupe en entree modifiees par un bruit additif.
Il s'agit d'un bruit blanc dont l'amplitude est egale au poids de la liaison entre les deux groupes (poids choisi lors dela creation du lien).

Macro:
-none

Local variables:
-boolean flag_init_seed = false

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo 5
- description: none/ XXX
- input expected group: same size group without macro-neurons
- where are the data?: they are the activations of the neurons of the input group

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <time.h>
#include <libx.h>
#include <stdlib.h>

#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>

void function_bruit_sur_signal(int numero)
{
    int j;
    int lien, gpe_entree;
    int deb_entree, taille_entree, taille_entree2, increment,
        neurone_gpe_entree;
    int deb_gpe_f_bruit, taille_gpe_f_bruit, neurone_gpe_bruit;
    float bruit;
    float coef_bruit;

    lien = find_input_link(numero, 0);
    taille_gpe_f_bruit = def_groupe[numero].nbre;

    if (!flag_init_seed)
    {
        srand48(time(NULL));
        flag_init_seed = TRUE;
    }

    if (lien < 0)
    {
        printf
            ("le groupe %d appelle fonction_bruit_sur_signal alors qu'il n'a pas de lien en entree ! \n",
             numero);
    }

    else
    {
        coef_bruit = liaison[lien].norme;
        gpe_entree = liaison[lien].depart;
        deb_gpe_f_bruit = def_groupe[numero].premier_ele;
        deb_entree = def_groupe[gpe_entree].premier_ele;
        taille_entree2 =
            def_groupe[gpe_entree].taillex * def_groupe[gpe_entree].tailley;
        if (taille_entree2 != taille_gpe_f_bruit)
        {
            printf
                ("le groupe %d (fonction_bruit_sur_signal) n'a pas le meme nombre de neurones que le groupe en entree !",
                 numero);
        }
        else
        {
            taille_entree = def_groupe[gpe_entree].nbre;
            increment = taille_entree / taille_entree2;
            for (j = 0; j < taille_gpe_f_bruit; j++)    /*(j=deb+increment-1; j<(deb+taille_entree); j+=increment) */
            {
                bruit = coef_bruit * ((float) drand48() - 0.5);
                neurone_gpe_bruit = deb_gpe_f_bruit + j * increment;
                neurone_gpe_entree = deb_entree + j * increment;
                neurone[neurone_gpe_bruit].s1 =
                    neurone[neurone_gpe_entree].s1 + bruit;
                if (neurone[neurone_gpe_bruit].s1 > 1)
                    neurone[neurone_gpe_bruit].s1 = 1;
                else if (neurone[neurone_gpe_bruit].s1 < -1)
                    neurone[neurone_gpe_bruit].s1 = -1;
		neurone[neurone_gpe_bruit].s2 = neurone[neurone_gpe_bruit].s = neurone[neurone_gpe_bruit].s1;
            }
        }
    }

}
