/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: VILLEMIN Jean-Baptiste
Created: 2009

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 	Active les neurones, les uns après les autres (de façon séquentielle).
		L'activation se fait du début à la fin, puis de la fin au début, etc...
		Le premier neurone activé est le neurone médian.
		Récupère un lien "learn" pour connaitre la phase (apprentissage ou test) qui permet le fonctionnement ou non de la boite.

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct mydata
{
	int compteur;
	int gpe;
	int decompte;
	int milieu;
} mydata;

void new_neurone_seq_AR(int Gpe)
{
	mydata *data=NULL;
	int i,l;
	int nombre;



	data=(mydata *)malloc(sizeof(mydata));
	if(data == NULL)
		{
		printf("Probleme allocation malloc pour mydata dans %s\n",__FUNCTION__);
		exit(0);
		}
	l = 0;
	i = find_input_link(Gpe, l);
	while (i != -1)
		{
		if (strcmp(liaison[i].nom, "learn") == 0)
			data->gpe = liaison[i].depart;
		i = find_input_link(Gpe, l);
		l++;
		}

	data->decompte = 0;
	nombre = def_groupe[Gpe].nbre;

	if (nombre <= 1)
		data->milieu = 0;
	else
		data->milieu = round(nombre/2);

	data->compteur = data->milieu;

#ifdef DEBUG
printf("Nombre: %d Milieu: %d\n",nombre,data->milieu);
#endif

/**
* Passage donnee data
*/
	def_groupe[Gpe].data=data;



}
void function_neurone_seq_AR(int Gpe)
{
	mydata *data=NULL;
	int deb;		/* le premier neurone */
	int nombre;		/* le nombre de neurone */
	int i=0;		/* variable de boucles */

#ifdef DEBUG
printf("~~~~~~~~~~~ enter in %s ~~~~~~~~~~~\n", __FUNCTION__);
#endif
    

if(def_groupe[Gpe].data == NULL)
	{
	printf("Probleme dans la recuperation des valeurs du constructeur new_neurone_seq_AR\n");
	exit(0);
	}
else
	data=def_groupe[Gpe].data;

/* ------ */

deb = def_groupe[Gpe].premier_ele;
nombre = def_groupe[Gpe].nbre;

if(neurone[def_groupe[data->gpe].premier_ele].s < 0.5)
	{

	for (i = deb; i < deb + nombre; i++)
		neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;

	neurone[deb+data->milieu].s2 = neurone[deb+data->milieu].s1 = neurone[deb+data->milieu].s = 1.0;

	data->decompte=0;
	data->compteur=data->milieu;	

	}
else
	{

#ifdef DEBUG
printf("%d\n",data->compteur);
#endif

	/* reset des neurones */
	for (i = deb; i < deb + nombre; i++)
		neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;

	/* active le neurone gagnant */
	neurone[deb + data->compteur].s2 = neurone[deb + data->compteur].s1 = neurone[deb + data->compteur].s = 1.;

	if(data->decompte == 0) 
		data->compteur++;
	else
		data->compteur--;

	if(data->compteur >= (nombre-1))                                                
		data->decompte=1;
	if(data->compteur <= 0)                                                
		data->decompte=0;		
	}

#ifdef DEBUG
printf("-------------- End of %s ------------ \n", __FUNCTION__);
#endif
}
