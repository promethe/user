/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_moyenne_etat.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:   computes and store a mean value of input on the val field of the link.
		the window for the mean is the temps field of the link.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
typedef struct MyData_f_trajectory_XY_teacher
{
    int gpe_teaching;
    char file_name[255];
    FILE *file_desc;
    int pt[56000][2];
    int nb_pt;
    float d_min_prec;
    float Rmax;

} MyData_f_trajectory_XY_teacher;
void function_moyenne_etat(int numero)
{
    float vigilence_local = 1.;

    int neur_win = -1, i;

    int first_neuron = def_groupe[numero].premier_ele;

    float tau = 0., val = -1., var = 0.;

    type_coeff *synapse, *synX, *synE;

    for (i = 0; i < def_groupe[numero].nbre; i++)
    {
        val = -1.;
        synX = NULL;
        synapse = neurone[first_neuron + i].coeff;

        /*Pour chaque neurone, on cherche dans le groupe d'etat (modifible) le neurone que l'on moyenne. Generalament, il n'y en a qu'un mais sinon c'est le max positif qui est choisis */
        /*Un lien transition_detect remet les coef a leur norme de depart */
        while (synapse != NULL)
        {
            if (synapse->evolution == 1)
            {
                if (neurone[synapse->entree].s1 > val
                    && neurone[synapse->entree].s1 > 0.)
                {
                    val = neurone[synapse->entree].s1;
                    synX = synapse;
                    neur_win = i;
                }
            }
            synapse = synapse->s;
        }
        if (synX != NULL)
        {
            /*Puis on cherche le neurone dont on veut la valeur moyenne pour l'etat actif: il ne doit y en avoir qu'une */
            synapse = neurone[first_neuron + neur_win].coeff;
            synE = NULL;
            while (synapse != NULL)
            {
                if (synapse->evolution == 0)
                {
                    if (synE == NULL)
                        synE = synapse;
                    else
                    {
                        printf
                            ("%s %d: plusieurs neurones peut etre moyenne: condition impossible\n",
                             __FUNCTION__, numero);
                        exit(0);
                    }
                }
                synapse = synapse->s;
            }



            if (def_groupe[numero].liste_types_neuromodulations != NULL)
            {
                /*Dec 2005 CG : Apprentissage neuro-module par le terme de vigilence exclusivement: cela evite de faire un strcmp sur le type de la neuromodulation */
                vigilence_local =
                    def_groupe[numero].liste_types_neuromodulations->
                    liste_modulation->poids *
                    neurone[def_groupe[numero].liste_types_neuromodulations->
                            liste_modulation->num_neurone].s;


            }

            if (synE != NULL)
            {
                if (vigilence_local > 0.5)
                {
                    tau = liaison[synX->gpe_liaison].temps;
                    var = neurone[synE->entree].s2;
                    synX->val = (tau * synX->val + var) / (tau + 1);
                }
                neurone[first_neuron + neur_win].s2 =
                    neurone[first_neuron + neur_win].s =
                    neurone[first_neuron + neur_win].s1 = synX->val;
            }
            else
            {
                printf
                    ("%s %d: aucun neurone peut etre moyenne: condition impossible\n",
                     __FUNCTION__, numero);
                exit(0);
            }

        }

    }


}
