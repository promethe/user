/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \ingroup libNN_IO
 \defgroup  f_extract_val_to_vector  f_extract_val_to_vector

 \author A. Blanchard
 \date 11/08/2012

 \details
 Activate the neurone of a population of neurone where the position depends on the analogic entry of the input neuron.

 \section Links
 -    input : group of input (useless if there is only one link.

 \section Options
 -    -c         : the vector is circular
 -    -M[max]	   : max of the analogic value
 -    -x[column] : column in the input group
 -    -y[line]   : line in the input group
 -	  -l : Allow to linearise between two neurons the activation if the value selected is between them.

 \remark contrary to f_extract_val_to_vector_(non)_circular where the last neuron only is activated for an input neuron exactly equal to the max. Here it is symmetrical. [0, max] instead of [0, max+max/n[

 \file
 \ingroup f_extract_val_to_vector

 */

#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include "outils.h"

const double PI = 3.14159265358979323846;

typedef struct extract_val_to_vector {
  char circular;
  float max, min, gain;
  int input_neuron, isLinear;
  int last_index;
  int isGaussian;
  int isPopulation;
  float sigma;
  double r2pi;
  int sigma_entree;
} type_extract_val_to_vector;

void new_extract_val_to_vector(int gpe)
{
  const char *args;
  int link, input_group = -1, default_input_group, increment, nb_links = 0;
  int x, y;
  type_extract_val_to_vector *my_data = NULL;

  my_data = ALLOCATION(type_extract_val_to_vector);
  def_groupe[gpe].data = my_data;

  /* default */
  my_data->circular = 0;
  x = 0;
  y = 0;
  my_data->max = 1.;
  my_data->min = 0.;
  my_data->isLinear = 0;
  my_data->isGaussian = 0;
  my_data->sigma = 0.0;
  my_data->r2pi = sqrt(2 * PI);
  my_data->sigma_entree = -1;
  my_data->isPopulation = 0;

  //todo ATTENTION, cette façon de faire est particulierement chelou : dans ce cas là il vaut mieux mettre un while plutot que d'utiliser un for dont on ne connais pas le nombre de tour de façon pre-definie a l'entree du for : c'est plus clair
  for (link = find_input_link(gpe, 0); link != -1; link = find_input_link(gpe, nb_links))
  {
    args = liaison[link].nom;

    default_input_group = liaison[link].depart;
    if (prom_getopt(args, "input", NULL) == 1) input_group = default_input_group;
    if (prom_getopt_int(args, "-x", &x) == 1) EXIT_ON_ERROR("Option -x on %s is waiting for a column number", args);
    if (prom_getopt_int(args, "-y", &y) == 1) EXIT_ON_ERROR("Option -y on %s is waiting for a line number", args);
    if (prom_getopt_float(args, "-M", &(my_data->max)) == 1) EXIT_ON_ERROR("You must give a positive float following -M to specify the max in %s \n", args);
    if (prom_getopt_float(args, "-m", &(my_data->min)) == 1) PRINT_WARNING("No minimum : set minimum at 0.0 \n");
    if (prom_getopt(args, "-c", NULL) == 1) my_data->circular = 1;
    if (prom_getopt(args, "-l", NULL) == 1) my_data->isLinear = 1;
    if (prom_getopt(args, "-p", NULL) == 1) my_data->isPopulation = 1;
    if (prom_getopt_float(args, "-s", &(my_data->sigma)) == 2)
    {
      my_data->isGaussian = 1;
    }
    else if (prom_getopt_float(args, "-s", &(my_data->sigma)) == 1)
    {
      EXIT_ON_ERROR("Erreur de conversion sur le float dans l'option -s . Pour donner un sigma variable vous pouvez utiliser -S . Sinon veuillez utiliser un format float correct", args);
    }
    if (prom_getopt(args, "-S", NULL) == 1)
    {
      my_data->isGaussian = 1;
      my_data->sigma = 0.0;
      my_data->sigma_entree = default_input_group;
    }

    nb_links++;
  }

  if (nb_links == 1)
  {
    input_group = default_input_group;
  }
  else if (input_group == -1) EXIT_ON_ERROR("in %s When there is no exactly one link you need to specify the 'input'", def_groupe[gpe].no_name);

  increment = def_groupe[input_group].nbre / (def_groupe[input_group].taillex * def_groupe[input_group].tailley);

  if (x >= def_groupe[input_group].taillex) EXIT_ON_ERROR("x is >= taillex (%d) of input group", def_groupe[input_group].taillex);
  if (y >= def_groupe[input_group].tailley) EXIT_ON_ERROR("y is >= tailley (%d) of input group", def_groupe[input_group].tailley);

  if (my_data->isLinear && my_data->circular) EXIT_ON_ERROR("Circular AND linear options are not yet implemented together.\nYou have to choose or implement them.");
  if (my_data->isGaussian && my_data->circular) EXIT_ON_ERROR("Circular AND gaussian options are not yet implemented together.\nYou have to choose or implement them.");
  if (my_data->isGaussian && my_data->isLinear) PRINT_WARNING("Extraction to a gaussian is linear by default : the both options are not needed");
  if (my_data->isPopulation && (my_data->isGaussian == 0)) EXIT_ON_ERROR("Extraction to a population is only implemented witho gaussian option, please activate it or implemente Population in other case your self");

  my_data->gain = (def_groupe[gpe].nbre) / (my_data->max - my_data->min); // 0 à 10
  my_data->input_neuron = def_groupe[input_group].premier_ele + x * increment + y * increment * def_groupe[input_group].taillex;
  my_data->last_index = 0;
}

void function_extract_val_to_vector(int gpe)
{
  float value;
  float index_float;
  float temp;
  int deb, index, i;
  type_extract_val_to_vector *my_data;

  my_data = def_groupe[gpe].data;
  value = neurone[my_data->input_neuron].s1;
  deb = def_groupe[gpe].premier_ele;
  index_float = (value - my_data->min) * my_data->gain; // 0 à 10
  index = (int) index_float; //0 à 10.0

  neurone[my_data->last_index].s1 = 0.0;
  if (!(my_data->isGaussian))
  {
    if (!(my_data->isLinear))
    {
      if (my_data->circular) index = index % (def_groupe[gpe].nbre); // 0 à 9
      else
      {
        if (index >= def_groupe[gpe].nbre) index = def_groupe[gpe].nbre - 1;
        if (index < 0) index = 0;
      }
      neurone[deb + index].s1 = 1.0;
    }
    else
    {
      //implémentation à tester de l'option circular dans le cas l1
      if (my_data->circular)
      {
        if (index >= def_groupe[gpe].nbre)
        {
          index_float = index_float - ((index / def_groupe[gpe].nbre) * def_groupe[gpe].nbre);
          index = index - ((index / def_groupe[gpe].nbre) * def_groupe[gpe].nbre);
        }
      }
      else
      {
        if (index >= def_groupe[gpe].nbre) index = def_groupe[gpe].nbre - 1; // 0 à 9 [0.0 -> 9.99 avec 10 inclus]
      }
      if (index < 0) index = 0; // 0 à 9 [0.0 -> 9.99 avec 10 inclus]

      temp = 1.0 - (index_float - index);

      if (temp < 0.0) temp = 0.0;
      else if (temp > 1.0) temp = 1.0;

      if (temp < 0.5) neurone[deb + index].s1 = temp;
    }

    my_data->last_index = index + deb;
  }
  else //cas gaussien
  {

    if (my_data->sigma_entree != -1) my_data->sigma = neurone[def_groupe[my_data->sigma_entree].premier_ele].s1;
    for (i = deb; i < def_groupe[gpe].nbre + deb; i++)
    {
      //max gaussien à index_float
      neurone[i].s1 = (1.0 / (my_data->sigma * my_data->r2pi)) * exp(-0.5 * pow(((((i - deb) - index_float) / my_data->sigma)), 2));
      if (my_data->isPopulation && ((i - deb) < index_float)) neurone[i].s1 = 1.0;
    }
  }

}

void destroy_extract_val_to_vector(int gpe)
{
  if (def_groupe[gpe].data != NULL)
  {
    free((type_extract_val_to_vector*) def_groupe[gpe].data);
  }
}

