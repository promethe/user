/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_norm_euclidienne f_norm_euclidienne
\ingroup libNN_IO

\brief Normalisation euclidienne

\details

\section Description
 Fonction de normalisation des activites d'un groupe par la norme euclidenne (norme carree) de ces activites.

\section Options

 Entree definie par le lien "to_normalize"


\file
\ingroup f_norm_euclidienne

\brief Normalisation euclidienne

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author:A. de Rengerve
- description: specific file creation
- date: 17-08-2010

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Fonction de normalisation des activites d'un groupe par la norme euclidenne (norme carree) de ces activites.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/trouver_entree.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

void function_norm_euclidienne(int gpe_sortie)
{
	int deb = def_groupe[gpe_sortie].premier_ele;
	float val =0, val1=0, val2=0;
	int gpe_entree, debe, nbree, i, nbre = def_groupe[gpe_sortie].nbre;
	gpe_entree = trouver_entree(gpe_sortie, (char*)"to_normalize");
	debe = def_groupe[gpe_entree].premier_ele;
	nbree = def_groupe[gpe_entree].nbre;

	if (nbre != nbree)
	{
		EXIT_ON_ERROR("Error on group norm_euclidienne(%s): group has different size\n",def_groupe[gpe_sortie].no_name);
	}

	/*calcul de la norme euclidienne */
	dprints("function_norm_euclidienne: %s,entree:%s\n", def_groupe[gpe_sortie].no_name,def_groupe[gpe_entree].no_name);

	val =0;
	val1 =0;
	val2 =0;
	for (i = debe; i < debe + nbree; i++)
	{
		neurone[deb + i - debe].s = neurone[deb - debe + i].s1 = neurone[deb + i - debe].s2 = 0.;
		val = val + neurone[i].s1*neurone[i].s;
		val1 += neurone[i].s1*neurone[i].s1;
		val2 += neurone[i].s1*neurone[i].s2;
	}
	val= sqrt(val);
	val1= sqrt(val1);
	val2= sqrt(val2);
	dprints("norme euclidienne: val %f, val1 %f, val2 %f\n", val, val1, val2);

	/*normalisation selon chaque niveau d'activite*/

	if(val>0.0000001) {
		for (i = 0; i < nbre; i++)
		{
			neurone[deb + i].s= neurone[debe + i].s / val;
		}
	}
	if(val1>0.0000001) {
		for (i = 0; i < nbre; i++)
		{
			neurone[deb + i].s1= neurone[debe + i].s1 / val1;
		}
	}
	if(val2>0.0000001) {
		for (i = 0; i < nbre; i++)
		{
			neurone[deb + i].s2= neurone[debe + i].s2 / val2;
		}
	}

	dprints("Sortie de norm_euclidienne %s\n", def_groupe[gpe_sortie]);
}
