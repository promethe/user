/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_gpe_to_ext.c 
\brief 

Author: Maillard
Created: 02/05
Modified:
- author: Maillard
- description: specific file creation
- date: 02/05

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / naural
- description: convertit un groupe de neurone en une image en niveaux de gris/XXX
- input expected group: un groupe de neurones entre 0 et 1/xxx
- where are the data?: none/xxx

Comments:
pour le moment pas de flottant
Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>

#include <string.h>

void function_gpe_to_ext(int gpe)
{
    unsigned char *ImageLocale;
    int Index, GpeExt;
    int deb, i, longueur;
    int rescale = 1;
    char *param;
    float *ImageLocale_float;

    if (def_groupe[gpe].ext == NULL)
    {
        GpeExt = -1;
        /* recuperations des donnees */
        for (Index = 0; Index < nbre_liaison; Index++)
            if (liaison[Index].arrivee == gpe)
            {
                GpeExt = liaison[Index].depart;
                break;
            }
        if (GpeExt == -1)
        {
            printf("Le group %d doit etre connecte a un groupe \t", gpe);
            printf("Exit in function_gpe_to_ext\n");
            exit(-1);
        }

        if ((param = strstr(liaison[Index].nom, "-N")) != NULL)
        {
            rescale = atoi(&param[2]);
            /*     def_groupe[gpe].data = (void*)malloc(sizeof(int));
               if(def_groupe[gpe].data == NULL){
               printf("Not enought memory!\n");
               printf("Exit in function_gpe_to_ext\n");
               exit(-1);
               } */
        }


        def_groupe[gpe].ext = (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[gpe].ext == NULL)
        {
            printf("Not enought memory!\n");
            printf("Exit in function_gpe_to_ext\n");
            exit(-1);
        }

        ((prom_images_struct *) def_groupe[gpe].ext)->sx =
            def_groupe[GpeExt].taillex;
        ((prom_images_struct *) def_groupe[gpe].ext)->sy =
            def_groupe[GpeExt].tailley;
        ((prom_images_struct *) def_groupe[gpe].ext)->image_number = 1;


        if (rescale == 1)
        {
            ((prom_images_struct *) def_groupe[gpe].ext)->nb_band = 1;
            ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] =
                (unsigned char *) malloc(def_groupe[GpeExt].taillex *
                                         def_groupe[GpeExt].tailley *
                                         sizeof(char));
        }
        else if (rescale == 0)
        {
            ((prom_images_struct *) def_groupe[gpe].ext)->nb_band = 4;
            ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] =
                (unsigned char *) malloc(def_groupe[GpeExt].taillex *
                                         def_groupe[GpeExt].tailley *
                                         sizeof(float));
        }
        else
        {
            printf("rescale non reconnue\n");
            exit(1);
        }

        if (((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("Not enought memory!\n");
            printf("Exit in function_gpe_to_ext\n");
            exit(-1);
        }
        ((prom_images_struct *) def_groupe[gpe].ext)->images_table[1] =
            (unsigned char *) malloc(2 * sizeof(int));
        *(int *) (((prom_images_struct *) def_groupe[gpe].ext)->
                  images_table[1]) = GpeExt;
        *((int *) (((prom_images_struct *) def_groupe[gpe].ext)->
                   images_table[1]) + 1) = rescale;

        ImageLocale =
            ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0];
    }
    else
    {
        ImageLocale =
            ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0];
        GpeExt =
            *(int *) (((prom_images_struct *) def_groupe[gpe].ext)->
                      images_table[1]);
        rescale =
            *((int *) (((prom_images_struct *) def_groupe[gpe].ext)->
                       images_table[1]) + 1);
    }

/*
  reseau_vers_image(GpeExt, ImageLocale);
*/
    deb = def_groupe[GpeExt].premier_ele;
    longueur = def_groupe[GpeExt].nbre;

    switch (rescale)
    {
    case 1:
        for (i = deb; i < deb + longueur; i++)
            ImageLocale[i - deb] = (unsigned char) (neurone[i].s1 * 255.);
        break;

    case 0:
/*		min = 99999999999;
			max = -9999999999;
		for(i=deb;i<deb+longueur;i++)
		{
			if(ImageLocale[i-deb].s1>max)
				max = ImageLocale[i-deb];
			if(ImageLocale[i-deb].s1<min)
				max = ImageLocale[i-deb];
		}
		pente=(b-a)/(max-min);
		for(i=deb;i<deb+longueur;i++)
		{
			neurone[i].s1 = neurone[i].s = neurone[i].s2 = pente * ImageLocale[i-deb]+
		}*/
        ImageLocale_float = (float *) ImageLocale;
        for (i = deb; i < deb + longueur; i++)
            ImageLocale_float[i - deb] = neurone[i].s1;
        break;

    }

}
