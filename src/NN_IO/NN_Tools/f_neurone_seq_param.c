/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \ingroup libIHM
 \defgroup f_neurone_seq_param.c f_neurone_seq_param.c

 Author: Ali K.
 Created: 2015

 Description: Active les neurones sequentiellement.
 Laisse chaque neurone actif pendant "N" iterations à définir sur le lien d'entrée.
 Fait la sequence  "S" fois à définir sur le lien. (Optional)
 Ou bien les sequences sont répetées à l'infini jusqu'à ce que "enable" passe à zero.
     enable = valeur (binarisé) du neurone en entrée

 http://www.doxygen.org
 ************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/local_var.h"

typedef struct mydata {
  int neuractif;
  int enable;
  int iteration;
  int nbiteration;
  int nbseq;
  int seq;
  int alea;
} mydata;

void new_neurone_seq_param(int Gpe)
{
  mydata *data = NULL;

  int i = 0, l;
  char param_link[16];

  data = (mydata *) malloc(sizeof(mydata));
  if (data == NULL)
    EXIT_ON_ERROR("pb malloc pour mydata %s", __FUNCTION__);
  data->nbiteration= 1;
  data->nbseq= 1;
  data->alea = 0;
  l = 0;
  i = find_input_link(Gpe, l);
  while (i != -1)
  {
    if (strstr(liaison[i].nom, "enable") != NULL)
    {
      data->enable = liaison[i].depart;
    }
    if (prom_getopt(liaison[i].nom, "N", param_link) == 2 || prom_getopt(liaison[i].nom, "n", param_link) == 2)
      data->nbiteration= atoi(param_link);
    if (prom_getopt(liaison[i].nom, "A", param_link) == 1 || prom_getopt(liaison[i].nom, "a", param_link) == 1)
      data->alea= 1;
    i = find_input_link(Gpe, l);
    l++;
  }
  data->neuractif = 0;
  data->iteration = 0;
  def_groupe[Gpe].data = data;
  for (i = def_groupe[Gpe].premier_ele; i < def_groupe[Gpe].premier_ele + def_groupe[Gpe].nbre; i++)
    neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;

}
void function_neurone_seq_param(int Gpe)
{
  mydata *data = NULL;
  int deb,taille,i,alea;
  float enable;

  data = (mydata *) def_groupe[Gpe].data;
  deb = def_groupe[Gpe].premier_ele;
  taille = def_groupe[Gpe].nbre;
  enable = neurone[def_groupe[data->enable].premier_ele].s1;
  alea = data->alea;
  for (i = deb; i < deb + taille; i++)
    neurone[i].s1= 0.0;

  if ( enable > 0.5)
  {
    data->iteration = (data->iteration + 1 ) % data->nbiteration;
    if (!data->iteration )
    {
      if(alea)
      {
        if (!flag_init_seed)
        {
          srand(time(NULL));
          flag_init_seed = TRUE;
        }
        data->neuractif = rand()% taille;
      }
      else
        data->neuractif = (data->neuractif + 1) % taille;
    }
    neurone[deb + data->neuractif ].s1 = 1;
  }
  else
    data->iteration = data->neuractif = 0;

  dprints("--------------end of %s\n", __FUNCTION__);
}
void destroy_neurone_seq_param(int Gpe)
{

  free(def_groupe[Gpe].data);
  def_groupe[Gpe].data = NULL;
}
