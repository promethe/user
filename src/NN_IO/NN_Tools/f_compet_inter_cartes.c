/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\ingroup libNNIO
\defgroup f_compet_inter_cartes f_compet_inter_cartes

\author Ali K.
\date 03/10/2013

\section Description
La fonction permet de faire une competition entre plusieurs cartes d'une seule dimension (1D).
La competition peut se faire entre les lignes ou entre les colonnes.


 ** Le max est calculé entre toutes les valeurs des colonnes (lignes) sauf à la position de la ligne (colonne) en cours

Out(i,j) = In(i,j) - max(In(k,j))  k =[0,i[,]i,ny]    ** Competition entre les lignes.
Out(i,j) = In(i,j) - max(In(i,k))	k =[0,j[,]j,nx]    **Competition entre les colonnes

nx : nombre de colonnes
ny : nombre de lignes


\section Options :
 -lin : Competition entre les lignes.
 -col : Competition entre les colonnes.

 \section Test Unitaire :
 Le test unitaire se trouve dans le dossier :
Simulateur/applications/unitary_tests/compet_inter_carte

Lancer le run.sh. Ce script compilera le symb et le res et lancera promethe.
Pour verifier que la boite est ok lire les fichiers lignes.SAVE et colonnes.SAVE.
Si les fichiers lignes.SAVE et colonnes.SAVE ne contiennent que des zeros TOUT EST OK.

\section Type :
Cette boite est une boite algo (pour le moment). Elle n'accepte que les liens algo en entrée.

 ************************************************************/
#include <libx.h>
#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define LIN 0
#define COL 1

typedef struct data {
	int compet;
	int gpe_input;
} Data_compet;
void new_compet_inter_cartes(int gpe)
{
	int i = 0, link_num = 0;
	char *link_name = NULL;
	char param_link[256];
	Data_compet * mydata;
	mydata = (Data_compet *) malloc (sizeof(Data_compet));
	if (mydata == NULL)
		EXIT_ON_ERROR ("Cannot allocate the data in %s",__FUNCTION__);
	mydata->gpe_input = -1;
	link_num = find_input_link(gpe,i);
	while(link_num!=-1)
	{
		link_name = liaison[link_num].nom;
		if (strstr(link_name,"sync")!=NULL)
		{}
		else
		{
			if (prom_getopt(link_name,"-col",param_link)==1) mydata->compet = COL;
			if (prom_getopt(link_name,"-lin",param_link)==1) mydata->compet = LIN;
			mydata->gpe_input = liaison[link_num].depart;
		}
		i++;
		link_num = find_input_link(gpe,i);
	}
	if(mydata->gpe_input == -1)
		EXIT_ON_ERROR("Le groupe %s (c:%d) doit avoir un groupe en entree\n",def_groupe[gpe].no_name,__LINE__);
	if(def_groupe[mydata->gpe_input].taillex != def_groupe[gpe].taillex || def_groupe[mydata->gpe_input].tailley != def_groupe[gpe].tailley)
		EXIT_ON_ERROR("Les groupes %s et %s doivent avoir la meme taille en x et en y",def_groupe[mydata->gpe_input].no_name,def_groupe[gpe].no_name);
	def_groupe[gpe].data = mydata;
}
void function_compet_inter_cartes(int gpe)
{
	int i,j,k;
	int deb = -1, nx = -1, ny = -1;
	float max;
	int compet = -1;
	int gpe_input = -1,  deb_input = -1;
	int index_input, index;
	Data_compet *mydata;

	mydata = (Data_compet *) (def_groupe[gpe].data);
	compet = mydata->compet;
	/*BOITE ALGO POUR LE MOMENT*/
	dprints("%s gpe %s \n", __FUNCTION__,def_groupe[gpe_sortie].no_name);

	deb = def_groupe[gpe].premier_ele;
	nx = def_groupe[gpe].taillex;
	ny = def_groupe[gpe].tailley;
	gpe_input = mydata->gpe_input;
	deb_input = def_groupe[gpe_input].premier_ele;

	if (compet == LIN) /*Competition entre lignes*/
	{
		dprints("Competition entre lignes\n");
		for (j = 0; j < ny ; j++ ) /* a la ligne de la sortie*/
		{
			for(i = 0; i < nx ; i++) /* a la colonne de la sortie*/
			{
				index = j*nx + i; /*en chaque point du gpe de sortie*/
				dprints("calcul du max entre les valeurs de la colonne: %d\n",i);
				max = -9999;
				for (k = 0; k < ny; k++)/*faire toutes les lignes de l'entree sauf la ligne en cours*/
				{
					if (k!=j)
					{
						index_input = k*nx+i;
						dprints("Val neurone a pos : %d = %f comparer avec %f\n",index_input,neurone[index_input+deb_input].s1,max);
						if(max < neurone[index_input+deb_input].s1)
							max = neurone[index_input+deb_input].s1;
					}
				}
				dprints("max (%d,%d) : %f\n",j,i,max);
				neurone[index+deb].s =  neurone[index+deb_input].s1 - max;
				if (neurone[index+deb].s > 1)
					neurone[index+deb].s1  = neurone[index+deb].s2 = 1;
				else if (neurone[index+deb].s < 0)
					neurone[index+deb].s1  = neurone[index+deb].s2 = 0;
				else
					neurone[index+deb].s1  = neurone[index+deb].s2 = neurone[index+deb].s;
				//neurone[index+deb].s = neurone[index+deb].s1 = neurone[index+deb].s2 =  max;
			}
		}
	}
	/*Competition entre colonnes*/
	else if (compet == COL)
	{
		dprints("Competition entre colonnes\n");
		for (j = 0; j < ny ; j++ ) /* a la ligne de la sortie*/
		{
			for(i = 0; i < nx ; i++) /* a la colonne de la sortie*/
			{
				index = j * nx + i; /*en chaque point du gpe de sortie*/

				dprints("calcul du max entre les valeurs de la ligne : %d\n",j);
				max = -9999;
				for (k = 0; k < nx; k++)/*faire toutes les colonnes de l'entree sauf la colonne en cours*/
				{
					if (k!=i)
					{
						index_input = j*nx+k;
						dprints("Val neurone a pos : %d = %f comparer avec %f\n",index_input,neurone[index_input+deb_input].s1,max);
						if(max < neurone[index_input+deb_input].s1)
							max = neurone[index_input+deb_input].s1;
					}
				}
				dprints("max (%d,%d) index %d : %f\n",j,i,index,max);
				neurone[index+deb].s = neurone[index+deb_input].s1 - max;
				/*bloquer les resultats entre 0 et 1 ???*/
				if (neurone[index+deb].s > 1)
					neurone[index+deb].s1  = neurone[index+deb].s2 = 1;
				else if (neurone[index+deb].s < 0)
					neurone[index+deb].s1  = neurone[index+deb].s2 = 0;
				else
					neurone[index+deb].s1  = neurone[index+deb].s2 = neurone[index+deb].s;
				//neurone[index+deb].s = neurone[index+deb].s1 = neurone[index+deb].s2 =  max;
			}
		}
	}
}
void destroy_compet_inter_cartes(int gpe)
{
	(void) gpe;

}
