/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_random_variable.c 
\brief 

Author: J. Hirel
Created: 02/09/2009

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: randomly choses one neuron every time this function is called. The probability mass function of the random variable is given as input.

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdlib.h>

/*#define DEBUG*/

#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

typedef struct data_random_variable
{
	int gpe_mass;
} data_random_variable;


void new_random_variable(int gpe)
{
	int index, link;
	int gpe_mass = -1;
	data_random_variable *my_data = NULL;

	dprints("new_random_variable(%s): Entering function\n", def_groupe[gpe].no_name);

	if (def_groupe[gpe].data == NULL)
	{
		my_data = ALLOCATION(data_random_variable);
		memset(my_data, 0, sizeof(data_random_variable));

		index = 0;
		link = find_input_link(gpe, index);
		while (link != -1)
		{
			if (strcmp(liaison[link].nom, "probability_mass") == 0)
			{
				gpe_mass = liaison[link].depart;
			}

			index++;
			link = find_input_link(gpe, index);
		}

		if (gpe_mass == -1)
		{
			EXIT_ON_ERROR("Missing input group 'probability_mass'");
		}

		if (def_groupe[gpe_mass].nbre != def_groupe[gpe].nbre)
		{
			EXIT_ON_ERROR("Sizes of current group and probability_mass group should be the same");
		}

		my_data->gpe_mass = gpe_mass;
		def_groupe[gpe].data = my_data;
	}

}


void function_random_variable(int gpe)
{
	data_random_variable *my_data = NULL;
	int first, first_mass;
	int i;
	double random_value;
	double sum_input = 0, sum_proba = 0;

	my_data = (data_random_variable *) def_groupe[gpe].data;

	if (my_data == NULL)
	{
		EXIT_ON_ERROR("Cannot retreive data");
	}

	first = def_groupe[gpe].premier_ele;
	first_mass = def_groupe[my_data->gpe_mass].premier_ele;

	for (i = 0; i < def_groupe[gpe].nbre; i++)
	{
		neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 0;
		sum_input += neurone[first_mass + i].s1;
	}

	random_value = drand48();

	dprints("f_random_variable(%s): random_value = %f, sum_input = %f\n", def_groupe[gpe].no_name, random_value, sum_input);

	for (i = 0; i < def_groupe[gpe].nbre; i++)
	{
		if (random_value >= sum_proba && random_value <= (sum_proba += neurone[first_mass + i].s1 / sum_input))
		{
			dprints("f_random_variable(%s): %f < %f, neurone %i activated\n", def_groupe[gpe].no_name, random_value, sum_proba, i);
			neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = 1;
			break;
		}
	}
}

void destroy_random_variable(int gpe)
{
	data_random_variable *my_data = NULL;

	dprints("destroy_random_variable(%s): Entering function\n", def_groupe[gpe].no_name);

	if (def_groupe[gpe].data != NULL)
	{
		my_data = (data_random_variable *) def_groupe[gpe].data;

		free(my_data);
		def_groupe[gpe].data = NULL;
	}

	dprints("destroy_random_variable(%s): Entering function\n", def_groupe[gpe].no_name);
}
