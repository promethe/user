/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  teleguide.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Pour teleguider le robot

Macro:
-none

Local variables:
-float posx
-float posy
-float pas
-float temps
-int mode

Global variables:
-none

Internal Tools:
-rencontre_tout()
-arrange_echelle()
-affiche_simul()
-checker_landmarks_visibles()
-reconnaissance()
-affiche_robot()
-affiche_obstacle()
-affiche_temps()

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <stdio.h>
typedef struct telecommande_struct
{
   float dx;
   float dy;
   float dx_mem;
   float dy_mem;
   float posx_prec;
   float posy_prec;
   float dx_m_prec;
   float dy_m_prec;
   float posx;
   float posy;
   float pas;
} telecommande_struct;

void function_telecommande(int gpe)
{

   int choix;
   telecommande_struct *t;
   dprints("%s = entree\n", __FUNCTION__);

   if (def_groupe[gpe].data == NULL)
   {
      t = (telecommande_struct *) malloc(sizeof(telecommande_struct));
      t->posx = 50.;
      t->posy = 50.;
      t->dx = 1.;
      t->dy = 0.;
      t->dx_mem = 1.;
      t->dy_mem = 1.;
      t->posx_prec = 1.;
      t->posy_prec = 1.;
      t->dx_m_prec = 1.;
      t->dy_m_prec = 0.;
      t->pas = 12.;
      def_groupe[gpe].data = t;
   }
   t = def_groupe[gpe].data;
   t->posx_prec = t->posx;
   t->posy_prec = t->posy;
   cprints("Entrez la direction: 1(gauche),  6(droite), 2(bas), 8(haut), diag(7,9,1,3)\n");
   cscans("%d", &choix);
   switch (choix)
   {
      case 0:
         /*printf("\n");
            printf("* exit remote  *\n");
            printf("\n");
            printf("\a");fflush(stdout);
            printf("\n");return; */ break;
      case 1:
         t->dx = -1.;
         t->dy = 1.;
         break;
      case 2:
         t->dx = 0.;
         t->dy = 1.;
         break;
      case 3:
         t->dx = 1.;
         t->dy = 1.;
         break;
      case 4:
         t->dx = -1.;
         t->dy = 0.;
         break;
      case 6:
         t->dx = 1.;
         t->dy = 0.;
         break;
      case 7:
         t->dx = -1.;
         t->dy = -1.;
         break;
      case 8:
         t->dx = 0.;
         t->dy = -1.;
         break;
      case 9:
         t->dx = 1.;
         t->dy = -1.;
         break;
   }

   /*Bouger le robot     */
   t->dx_mem = t->dx;
   t->dy_mem = t->dy;

   t->posx = t->posx + t->pas * t->dx_mem;
   t->posy = t->posy + t->pas * t->dy_mem;

   /*  posx=arrange_echelle(posx);
      posy=arrange_echelle(posy);

    */
   neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = t->posx;
   neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = t->posx;
   t->posx_prec = t->posx;
   t->posy_prec = t->posy;
   t->dx_m_prec = t->dx_mem;
   t->dy_m_prec = t->dy_mem;

}
