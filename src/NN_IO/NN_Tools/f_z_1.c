/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Function used to introcude a delay of exactly one step (one synaptic transmission delay) 
 the difference with the delay function is that information are stored for one iteration only 
 and not until another input is proposed 
 Function_z1 computes exactly a z-1  delay  (PG dec 2001) 

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -tools/erreur/message();

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

typedef struct type_data_z_1 {
  int deb_e;
  type_neurone *retard;
} type_data_z_1;

void function_z_1(int gpe_sortie)
{
  int gpe_entree;
  int taille_groupe_e = 0;
  int taille_groupe_s;
  int deb_e = 0, deb_s;
  char nom_liaison[32];
  int i, p;
  type_data_z_1 *my_data;
  type_neurone *retard;

#ifdef DEBUG
  printf("entering %s (%s line %i)", __FUNCTION__, __FILE__, __LINE__);
#endif

  deb_s = def_groupe[gpe_sortie].premier_ele;
  taille_groupe_s = def_groupe[gpe_sortie].nbre;

  if (def_groupe[gpe_sortie].data == NULL)
  {
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == gpe_sortie)
      {
        gpe_entree = liaison[i].depart;
        strcpy(nom_liaison, liaison[i].nom);
        taille_groupe_e = def_groupe[gpe_entree].nbre;
        deb_e = def_groupe[gpe_entree].premier_ele;
        break;
      }

    if (taille_groupe_s != taille_groupe_e)
    {
      EXIT_ON_ERROR("Taille incoherente avec la taille de la carte d'entree in group %s", def_groupe[gpe_sortie].no_name);
    }

    my_data = (type_data_z_1*) malloc(sizeof(type_data_z_1));
    retard = (type_neurone *) malloc(taille_groupe_s * sizeof(type_neurone));
    my_data->retard = retard;
    my_data->deb_e = deb_e;
    def_groupe[gpe_sortie].data = (void*) my_data;

    for (i = 0; i < taille_groupe_s; i++)
    {
      neurone[deb_s + i].s = neurone[deb_s + i].s1 = neurone[deb_s + i].s2 = 0.;

      retard[i].s = neurone[deb_e + i].s;
      retard[i].s1 = neurone[deb_e + i].s1;
      retard[i].s2 = neurone[deb_e + i].s2;
    }
  }
  else /* le groupe a deja ete initialise */
  {
    retard = ((type_data_z_1*) def_groupe[gpe_sortie].data)->retard;
    deb_e = ((type_data_z_1*) def_groupe[gpe_sortie].data)->deb_e;
    for (i = 0; i < taille_groupe_s; i++)
    {
      p = deb_s + i;
      neurone[p].s = retard[i].s;
      neurone[p].s1 = retard[i].s1;
      neurone[p].s2 = retard[i].s2;
      p = deb_e + i;
      retard[i].s = neurone[p].s;
      retard[i].s1 = neurone[p].s1;
      retard[i].s2 = neurone[p].s2;
    }

  }
#ifdef DEBUG
  printf("exiting %s (%s line %i)", __FUNCTION__, __FILE__, __LINE__);
#endif
}
