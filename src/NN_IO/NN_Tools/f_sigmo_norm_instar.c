/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief application d'un instar normalise a feedback sigmoidal sur un groupe de neurones (Grossberg)

Author: Adrien Jauffret
Created: 10/01/2011
Modified:
- author: Original instar code from M. Maillard
- description: specific file creation
- date: 19/09/2005

- author: A. de RENGERVE
- description: correction d'un bug potentiel
- date: 08/12/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Calcul de la normalisation du groupe d'entree d'apres la version "Sigmoid Feedback" du modele Instar de Grossberg + Suppression d'offset par la moyenne + Normalisation. 

"Nonlinear Neural Networks: Principles, Mechanisms, and Architectures". Stephen Grossberg, Boston University. Neural Networks 1987.
Section 15. The importance of Sigmoid Feedback signals: Shunting Feedback Networks.
 
d(xi)/dt = -Axi + (B*nbr_actifs(x) - xi)*[Ii + f(xi)] - xi *sum(Ik) (k!=i)

Avec f() une fonction sigmoidale.
et nbr_actifs() le nombre de neurones vraiment actifs et pertinents (>0.2 noise)

B represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc B=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (Bp)/(A+p) ici.

Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

Parametres : -A down (=1 par defaut)
			 -B up (=1 par defaut)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/




#include <stdlib.h>
#include <libx.h>
#include <math.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

#undef DEBUG
struct instar_gpe_data
{
    int deb_gpe;
    int deb_gpe_input;
    int sx;
    int sy;
    float up;
    float down;

};

static float sigmoid(float input)					/* Fonction sigmoide */
{
	float output=0.0;
	
	output = 1 / (1 + exp(-input));
	return output;	
}

void function_sigmo_norm_instar(int Gpe)
{
    int lien, i, j, k, l;
    float f_sum = 0., f_sum2 = 0., moy = 0.;
    float max = -9999, max1= -9999;
    float noise=0.3;
    int sx, sy;
    float up = 1.;             				/*par defaut on considere que c'est 1 pour faire des sorties de type neurone */
    float down;
    int deb_gpe = -1, deb_gpe_input = -1;
    int pos = -1;
    int input_gpe = -1;
    int nbr = 0;
    struct instar_gpe_data *this_instar_gpe_data;
    char *param = NULL, *chaine = NULL;
#ifdef DEBUG
    printf("entree dans donction instar_gpe %s \n", def_groupe[Gpe].no_name);
#endif
    if (def_groupe[Gpe].data == NULL)
    {
        if ((lien = find_input_link(Gpe, 0)) == -1)
        {
            printf("Erreur de lien entrant dans function_instar_gpe : gpe %s\n",def_groupe[Gpe].no_name);
            exit(1);
        }

        chaine = liaison[lien].nom;
        if ((param = strstr(chaine, "-A")) != NULL)
            down = (float) atof(&param[2]);
        else
            down = 1.;

        input_gpe = liaison[lien].depart;

        sx = def_groupe[Gpe].taillex;
        sy = def_groupe[Gpe].tailley;

        if ((sx != def_groupe[input_gpe].taillex)
            || (sy != def_groupe[input_gpe].tailley))
        {
            printf("Gpe : %s f_instar_gpe : le nombre de neurones du groupe ne correspond pas a la taille du groupe entrant\n", def_groupe[Gpe].no_name);
            exit(1);
        }

        if ((param = strstr(chaine, "-B")) != NULL)
		{
			up = (float) atof(&param[2]);	    /*printf("init up=%f \n",up);*/
		}
        else
            up = 1;    /*up est définit à 1 par defaut, pour ne prendre en compte que le nombre de neurones actifs */


        deb_gpe = def_groupe[Gpe].premier_ele;
        deb_gpe_input = def_groupe[input_gpe].premier_ele;

        def_groupe[Gpe].data = malloc(sizeof(struct instar_gpe_data));
        this_instar_gpe_data = (struct instar_gpe_data *) def_groupe[Gpe].data;
        this_instar_gpe_data->deb_gpe = deb_gpe;
        this_instar_gpe_data->deb_gpe_input = deb_gpe_input;
        this_instar_gpe_data->sx = sx;
        this_instar_gpe_data->sy = sy;
        this_instar_gpe_data->up = up;
        this_instar_gpe_data->down = down;

        if ((lien = find_input_link(Gpe, 1)) != -1)
        {
            printf("Erreur : la fonction instar_gpe (groupe %s) ne prend qu'un lien algo -Axxx-Byyy en entree \n",def_groupe[Gpe].no_name);
            exit(1);
        }
    }
    else
    {
        this_instar_gpe_data = (struct instar_gpe_data *) def_groupe[Gpe].data;
        deb_gpe = this_instar_gpe_data->deb_gpe;
        deb_gpe_input = this_instar_gpe_data->deb_gpe_input;
        sx = this_instar_gpe_data->sx;
        sy = this_instar_gpe_data->sy;
        up = this_instar_gpe_data->up;
        down = this_instar_gpe_data->down;
    }

    for (i = 0; i < sy; i++)
        for (j = 0; j < sx; j++)
		{
			f_sum += neurone[deb_gpe_input + i * sx + j].s;	

			if( neurone[deb_gpe_input + i * sx + j].s > noise)
				nbr += 1;				
		
			/* calcul du max en entree */
			if (max1 < neurone[deb_gpe_input + i * sx + j].s)
				max1 = neurone[deb_gpe_input + i * sx + j].s;	
		}

	if (nbr > 1)	/* on vérifie qu'il n'y ai pas qu'un seul neurone actif */
	{	
		if (f_sum < 0.000001)      			/*cas ou les entrees sont nulles */
		{
			for (k = 0; k < sy; k++)
				for (l = 0; l < sx; l++)
				{
					pos = deb_gpe + k * sx + l;
					neurone[pos].s = 0.;
					neurone[pos].s1 = 0.;
					neurone[pos].s2 = 1.;   /*pour que le groupe suivant aprenne sur ces neurones quoique dans le cas ou y'a absolument rien en entree on peut se demander s'il faut l'apprendre... */
				}
		}
		else
		{
		for (k = 0; k < sy; k++)
			for (l = 0; l < sx; l++)
			{
				if( neurone[deb_gpe_input + k * sx + l].s > noise) 	/* Filtrage du bruit: On ne prend pas en compte les neurones qui ont ete recrutes */
				{
					pos = deb_gpe + k * sx + l;
					neurone[pos].s = up * nbr * (neurone[deb_gpe_input + k * sx + l].s + sigmoid(neurone[pos].s)) / (down + neurone[deb_gpe_input + k * sx + l].s + sigmoid(neurone[pos].s) + f_sum);
					/* Instar & Sigmoid Feedback selon Grossberg : Shunting Feedback Networks */
					
					f_sum2 += neurone[pos].s;	
										
				}
			}
	   
		moy = f_sum2/(nbr+0.01); 			/*calcul de la moyenne en évitant une eventuelle division par zero */
			
			
		for (k = 0; k < sy; k++)
			for (l = 0; l < sx; l++)
			{
				pos = deb_gpe + k * sx + l;		
				neurone[pos].s = neurone[pos].s - 0.9*moy;	/* On supprime l'offset en supprimant les valeurs inférieures à la moyenne (non-pertinentes) */
				
				if(neurone[pos].s < 0)					/* seuiller a zero les valeurs negatives */
					neurone[pos].s = 0;

				/* calcul du max pour la normalisation */
				if (max < neurone[pos].s)
					max = neurone[pos].s;
				
			}
		for (k = 0; k < sy; k++)
			for (l = 0; l < sx; l++)
			{
				pos = deb_gpe + k * sx + l;	
				if(max > 0.0) /* != */
					neurone[pos].s1 = neurone[pos].s = (neurone[pos].s/max)*max1;	
					/* On normalise en divisant par le max puis en multipliant par le max de l'entree pour conserver le niveau absolu*/					
					
				else
					neurone[pos].s1 = neurone[pos].s;
					
				neurone[pos].s2 = 1.;   				/* pour que le groupe suivant aprenne sur ces neurones */				
			}
		}
	}
	else
	{												/* Cas particulier ou on a qu'un seul neurone actif (on recopie alors le groupe d'entree)*/
		for (k = 0; k < sy; k++)
				for (l = 0; l < sx; l++)
				{
					pos = deb_gpe + k * sx + l;
					
					if(neurone[deb_gpe_input + k * sx + l].s >= noise)
						neurone[pos].s = neurone[pos].s1 = neurone[deb_gpe_input + k * sx + l].s;
					else
						neurone[pos].s = neurone[pos].s1 = 0;
						
					neurone[pos].s2 = 1.;   /*pour que le groupe suivant aprenne sur ces neurones */
				}
	}
	
		
    
}
