/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_detection_nouveaute.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction VB : detecte la nouveaute a partir de CA3
 La detection de nouveaute signifie ici la detection d'une nouvelle
 transition. Pour detecter une nouvelle transition, on regarde sur CA3 si
 un neurone a une activite superieure a 0.4 . Si oui alors il y a nouveaute
 Cette fonction doit avoir UN neurone qui prendra les valeurs suivantes:
 En planification, 0
 En exploration, 1 si pas nouveaute
		   0 si nouveaute
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Global_Var/Limbic.h>
#include "tools/include/local_var.h"
/*------------------------------------------------------------------------------*/
/* 					*/
/*------------------------------------------------------------------------------*/
void function_detection_nouveaute(int gpe_sortie)
{
    static int first = 1;
    static int deb_s;
    static int gpe_entre, deb_e, inc_e, taille_e;
    int i, j, eq;

    printf("\n----------Detection   Nouveaute---------------\n");
    if (first)
    {
        first = 0;
        deb_s = def_groupe[gpe_sortie].premier_ele;

        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == gpe_sortie)
            {
                gpe_entre = liaison[i].depart;
                deb_e = def_groupe[gpe_entre].premier_ele;
                taille_e = def_groupe[gpe_entre].nbre;
                j = def_groupe[gpe_entre].taillex *
                    def_groupe[gpe_entre].tailley;
                inc_e = taille_e / j;
                break;
            }
    }
    /*printf("Neurone nouveaute n�%d",deb_s); Modif Cyril 28/05/02 */


    eq = 1;
    for (i = 0; i < taille_e / inc_e; i++)
        if (neurone[deb_e + i * inc_e].s2 > 0.4)
            eq = 0;

    if (!eq)
    {
        neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = 0;
        if (!planification)
            dP = -10000;
        else
            dP = 0;
    }
    else
    {
        neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = 1;
        dP = 0;
    }


    if (planification)
    {
        neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = 0;
    }
}
