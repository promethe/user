/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M.LAGARDE
- description: specific file creation
- date: 02/11/2006

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct	s_transpose_data
{
   int	first;
   int	prev_first;
   int	size_x;
   int	size_y;
   int	prev_increment;
   int	last;
   int	prev_last;
   int rotation;
}		t_transpose_data;

void f_transpose(int numero)
{
   t_transpose_data	*data = NULL;

   int	prev_gpe = -1;
   int	first = -1;
   int	prev_first = -1;
   int	idx_link = -1;
   int	size_x = -1;
   int	size_y = -1;
   int	prev_increment = -1;
   int	size = -1;
   int	prev_size = -1;
   int	last = -1;
   int	prev_last = -1;

   int	x = 0;
   int	y = 0;

   int	j = 0;

   int rotation = 0;
   int l, i = 0;

   l = find_input_link(numero, i);
   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "rotation") == 0)
         rotation = liaison[l].depart;
      i++;
      l = find_input_link(numero, i);
   }


   if (def_groupe[numero].ext == NULL)
   {
      if ((idx_link = find_input_link(numero, 0)) == -1)
      {
         fprintf(stderr, "f_transpose : lien manquant en entree\n");
         exit(-1);
      }
      if ((prev_gpe = liaison[idx_link].depart) == -1)
      {
         fprintf(stderr, "f_transpose : erreur du groupe de depart du lien d'entree\n");
         exit(-1);
      }

      if (def_groupe[numero].taillex != def_groupe[prev_gpe].tailley)
         fprintf(stderr, "f_transpose : !!! ATTENTION !!! la taille horizontale ne correspond pas a la taille verticale du groupe precedent\n");

      if (def_groupe[numero].tailley != def_groupe[prev_gpe].taillex)
         fprintf(stderr, "f_transpose : !!! ATTENTION !!! la taille verticale ne correspond pas a la taille horizontale du groupe precedent\n");

      first = def_groupe[numero].premier_ele;
      prev_first = def_groupe[prev_gpe].premier_ele;

      size_x = def_groupe[numero].taillex;
      size_y = def_groupe[numero].tailley;

      size = def_groupe[numero].taillex * def_groupe[numero].tailley;
      prev_size = def_groupe[prev_gpe].taillex * def_groupe[prev_gpe].tailley;

      prev_increment = def_groupe[prev_gpe].nbre / prev_size;

      if (size != prev_size)
         fprintf(stderr, "f_transpose : !!! ATTENTION !!! Il est preferable que les 2 groupes aient le meme nombre de neurones ou macro neurones\n");

      last = first + (size - 1);
      prev_last = prev_first + (def_groupe[prev_gpe].nbre);

      if ((data = malloc(sizeof(t_transpose_data))) == NULL)
      {
         perror("f_transpose : malloc");
         fprintf(stderr, "f_transpose : erreur d'allocation memoire\n");
         exit(-1);
      }
      def_groupe[numero].ext = (void *)data;

      data->first = first;
      data->prev_first = prev_first;
      data->size_x = size_x;
      data->size_y = size_y;
      data->prev_increment = prev_increment;
      data->last = last;
      data->prev_last = prev_last;
      data->rotation = rotation;
   }
   else
   {
      data = (t_transpose_data *)def_groupe[numero].ext;
      first = data->first;
      prev_first = data->prev_first;
      size_x = data->size_x;
      size_y = data->size_y;
      prev_increment = data->prev_increment;
      last = data->last;
      prev_last = data->prev_last;
      rotation = data->rotation;
   }

   if( rotation == 0)
      for (x = 0, j = (prev_first + prev_increment - 1); (x < size_x) && (j < prev_last); x++)
         for (y = size_y - 1; (y >= 0) && (j < prev_last); y--)
         {
            neurone[first + (y * size_x) + x].s = neurone[j].s;
            neurone[first + (y * size_x) + x].s1 = neurone[j].s1;
            neurone[first + (y * size_x) + x].s2 = neurone[j].s2;
            j += prev_increment;
         }
   else
      for (y=0; y<size_y; y++)
         for(x=0; x<size_x; x++)
         {
            for(i=0; i<prev_increment; i++)
            {
               neurone[first + (y*size_x+x)*prev_increment + i].s = neurone[prev_first + (x*size_y+y)*prev_increment + i].s;
               neurone[first + (y*size_x+x)*prev_increment + i].s1 = neurone[prev_first + (x*size_y+y)*prev_increment + i].s1;
               neurone[first + (y*size_x+x)*prev_increment + i].s2 = neurone[prev_first + (x*size_y+y)*prev_increment + i].s2;
            }
         }
}
