/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_product.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-nnoe

Global variables:
-none

Internal Tools:
-tools/include/init_function_product()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/struct_product.h>
#include <stdlib.h>
void permute_int(int *n, int *m);
struct_product *init_function_product(int numero);

void permute_int(int *n, int *m)
{
    int a;
    a = *n;
    (*n) = (*m);
    (*m) = a;
}

struct_product *init_function_product(int numero)
{
    int gpe_entree1 = 0, gpe_entree2 = 0;   /* Groupes d'entree de la carte */
    int deb, deb1, deb2;        /* Debut des entrees des differents groupes */
    int nbre, nbre1, nbre2;     /* Nombre de neurones */
    int inc, inc1, inc2;        /* Increment */
    int n_macro, n_macro1, n_macro2;    /* Nombre de macro neurones */
    int i /*,j */ , F;
    int N, M;                   /* Taille des groupes d'entrees */
    int analog1 = 0, analog2 = 0;   /* vallent 1 si on prend le .s1 au lieu de .s2 */
    /*   int p,q; */
     /**/ struct_product * ptr = NULL;

    ptr = ALLOCATION(struct_product);

    N = def_groupe[numero].taillex; /* Taille entree 1 */
    M = def_groupe[numero].tailley; /* Taille entree 2 */

    /* ---------------------- */
    /* Determine les liaisons */
    /* ---------------------- */
    F = 0;                      /* Flag pour premier groupe trouve */
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == numero)
        {
            if (F == 0)
            {
                gpe_entree1 = liaison[i].depart;
                if (liaison[i].mode > 1)
                    analog1 = 1;
                F = 1;
            }
            else
            {
                gpe_entree2 = liaison[i].depart;
                if (liaison[i].mode > 1)
                    analog2 = 1;
                break;
            }
        }
    }

    deb = def_groupe[numero].premier_ele;
    deb1 = def_groupe[gpe_entree1].premier_ele;
    deb2 = def_groupe[gpe_entree2].premier_ele;

    nbre = def_groupe[numero].nbre;
    nbre1 = def_groupe[gpe_entree1].nbre;
    nbre2 = def_groupe[gpe_entree2].nbre;

    n_macro = def_groupe[numero].taillex * def_groupe[numero].tailley;
    n_macro1 =
        def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley;
    n_macro2 =
        def_groupe[gpe_entree2].taillex * def_groupe[gpe_entree2].tailley;

    inc = nbre / n_macro;
    inc1 = nbre1 / n_macro1;
    inc2 = nbre2 / n_macro2;

#ifdef DEBUG
    printf("La premiere entree est : %s\n", def_groupe[gpe_entree1].nom);
    printf("Elle contient %d neurones\n", n_macro1);
    printf("La deuxieme entree est : %s\n", def_groupe[gpe_entree2].nom);
    printf("Elle contient %d neurones\n", n_macro2);
#endif

    if (n_macro1 == M && n_macro2 == N)
    {
        permute_int(&deb1, &deb2);
        permute_int(&nbre1, &nbre2);
        permute_int(&n_macro1, &n_macro2);
        permute_int(&analog1, &analog2);
    }


    if (n_macro1 != N) EXIT_ON_ERROR("Erreur de dimension N\n");
    if (n_macro2 != M) EXIT_ON_ERROR("Erreur de dimension M\n");

    ptr->deb1 = deb1;
    ptr->deb2 = deb2;
    ptr->N = N;
    ptr->M = M;
    ptr->inc1 = inc1;
    ptr->inc2 = inc2;

    return (ptr);
}

void function_product(int numero)
{

    int p, q, r;
    int i, j;
    int N, M;
    int deb, deb1, deb2, inc1, inc2;

#ifdef DEBUG
    printf("----------------------------------------------------------\n");
    printf("Fonction produit\n");
#endif

    if (def_groupe[numero].ext == NULL)
        def_groupe[numero].ext = (void *) init_function_product(numero);

    deb1 = (*(struct_product *) def_groupe[numero].ext).deb1;
    deb2 = (*(struct_product *) def_groupe[numero].ext).deb2;
    N = (*(struct_product *) def_groupe[numero].ext).N;
    M = (*(struct_product *) def_groupe[numero].ext).M;
    inc1 = (*(struct_product *) def_groupe[numero].ext).inc1;
    inc2 = (*(struct_product *) def_groupe[numero].ext).inc2;

    deb = def_groupe[numero].premier_ele;



    /* ----------------- */
    /* Calcul du produit */
    /* ----------------- */
     /**/ for (i = 0; i < N; i++)
    {
        p = deb1 + (i + 1) * inc1 - 1;
        for (j = 0; j < M; j++)
        {
            q = deb2 + (j + 1) * inc2 - 1;
            r = deb + i + j * N;
            neurone[r].s = (neurone[p].s) * (neurone[q].s);
            neurone[r].s1 = (neurone[p].s1) * (neurone[q].s1);
            neurone[r].s2 = (neurone[p].s2) * (neurone[q].s2);
#ifdef DEBUG
            if ((neurone[r].s > 1.) || (neurone[r].s1 > 1.)
                || (neurone[r].s2 > 1.))
            {
                printf
                    ("WARNING: Depassement de la saturation dans f_product\n");
                printf("neurone[%d].s=%f, .s1=%f, .s2=%f\n", r, neurone[r].s,
                       neurone[r].s1, neurone[r].s2);
                printf("des neurones : %f et %f \n", neurone[p].s,
                       neurone[q].s);
                /*fflush(stdout); */
                /* exit (0); */
            }
#endif
        }
    }

#ifdef DEBUG
    printf("----------------------------------------------------------\n");
#endif
}
