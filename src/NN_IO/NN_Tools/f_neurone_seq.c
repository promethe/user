/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_neurone_seq.c 
\brief 

Author: Boucenna Sofiane
Created: 2007

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: fait des expressions faciales dans l ordre
             tristesse, neutre, joir, colere, surprise, tistesse, neutre ....

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct mydata
{
	int expression;
	int gpe;
	int iteration;
	int nbiteration;
} mydata;

void function_neurone_seq(int Gpe)
{
	mydata *data=NULL;
	int deb;                      /* le premier neurone */
	int longueur;                 /* le numero de neurones dans le goupe */
	int i=0;                      /* variable de boucles */
	int l;
	char param_link[16];


	dprints("~~~~~~~~~~~enter in %s\n", __FUNCTION__);


	if(def_groupe[Gpe].data == NULL)
	{
		data=(mydata *)malloc(sizeof(mydata));

		if(data == NULL) EXIT_ON_ERROR("pb malloc pour mydata %s",__FUNCTION__);
		data->nbiteration = 0;
		l = 0;
		i = find_input_link(Gpe, l);
		while (i != -1)
		{                       /* Recherche de f_pano_focus ou get_pano_alt ou f_get_pano ou load_image_from_disk ou f_load_baccon pour recuperer r_diff ou/et field */

			if (strcmp(liaison[i].nom, "learn") == 0)
			{
				data->gpe = liaison[i].depart;
			}
			if (prom_getopt(liaison[i].nom, "N", param_link) == 2 || prom_getopt(liaison[i].nom, "n", param_link) == 2)
				data->nbiteration = atoi(param_link);


			i = find_input_link(Gpe, l);
			l++;
		}
		data->expression = 0;
		data->iteration = 0;

		def_groupe[Gpe].data=data;
	}

	else
	{
		data=def_groupe[Gpe].data;
	}

	if(neurone[def_groupe[data->gpe].premier_ele].s1 < 0.5)
	{
		deb = def_groupe[Gpe].premier_ele;
		longueur = def_groupe[Gpe].nbre;

		/* metre a zero tous les neurones */
		for (i = deb; i < deb + longueur; i++)
		{
			neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;
		}
		data->iteration ++;
		if (data->iteration > data->nbiteration)
		{
			data->expression++;
			data->iteration = 0;
		}
		if(data->expression==longueur)
			data->expression=0;

		/* activer le neurone gagnant */
		neurone[deb + data->expression].s2 = neurone[deb + data->expression].s1 = neurone[deb + data->expression].s = 1.;

	}
	else
	{

		deb = def_groupe[Gpe].premier_ele;
		longueur = def_groupe[Gpe].nbre;
		for (i = deb; i < deb + longueur; i++)
		{
			neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;
		}
		data->iteration = 0;
		data->expression=0;
	}
	dprints("--------------end of %s\n", __FUNCTION__);
}
