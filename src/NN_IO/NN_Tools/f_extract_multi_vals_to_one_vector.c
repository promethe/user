/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \ingroup libNN_IO
 \defgroup  f_extract_multi_vals_to_one_vector  f_extract_multi_vals_to_one_vector

 \author A. Blanchard
 \date 11/08/2012

 \details
 Activate the neurone of a population of neurone where the position depends on the analogic entry of the input neuron.

 \section Links
 -    input : group of input (useless if there is only one link.

 \section Options
 -    -c         : the vector is circular
 -    -M[max]	   : max of the analogic value
 -    -x[column] : column in the input group
 -    -y[line]   : line in the input group

 \remark contrary to f_extract_multi_vals_to_one_vector_(non)_circular where the last neuron only is activated for an input neuron exactly equal to the max. Here it is symmetrical. [0, max] instead of [0, max+max/n[

 \file
 \ingroup f_extract_multi_vals_to_one_vector

 */
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>

#include "outils.h"

typedef struct extract_multi_vals_to_one_vector {
	char circular;
	float max;
	int input_neuron;
	int gpe_input;
} type_extract_multi_vals_to_one_vector;

void new_extract_multi_vals_to_one_vector(int gpe)
{
	const char *args;
	int link, default_input_group, nb_links = 0;
	type_extract_multi_vals_to_one_vector *this;

	this = ALLOCATION(type_extract_multi_vals_to_one_vector);
	def_groupe[gpe].data = this;

	/* default */
	this->circular = 0;
	this->max = 1.;
	this->gpe_input = -1;

	for (link = find_input_link(gpe, 0); link != -1; link = find_input_link(gpe, nb_links))
	{
		args = liaison[link].nom;

		default_input_group = liaison[link].depart;
		if (prom_getopt(args, "input", NULL) == 1) this->gpe_input = default_input_group;
		if (prom_getopt_float(args, "-M", &this->max) == 1) EXIT_ON_ERROR("You must give a positive float following -M to specify the max in %s ", args);
		/*if (prom_getopt(args, "-c", NULL) == 1) this->circular = 1;*/
		nb_links++;
	}

	if (nb_links == 1)
	{
		this->gpe_input = default_input_group;
	}
	else if (this->gpe_input == -1) EXIT_ON_ERROR("When there is no exactly one link you need to specify the 'input'");

}

void function_extract_multi_vals_to_one_vector(int gpe)
{
	float max;
	int value;
	int i, deb,deb_input,nbre,nbre_input,gpe_input,pos;
	type_extract_multi_vals_to_one_vector *this;


	this = (type_extract_multi_vals_to_one_vector *) def_groupe[gpe].data;
	gpe_input = this->gpe_input;
	deb = def_groupe[gpe].premier_ele;
	deb_input = def_groupe[gpe_input].premier_ele;
	nbre = def_groupe[gpe].nbre;
	nbre_input = def_groupe[gpe_input].nbre;
	max = this->max;


	/*RAZ du groupe*/
	for (i = 0; i < nbre; i++)
		neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

	for (i = 0; i < nbre_input; i++)
	{
		pos = deb_input + i;
		/*value = (int) round ((float) (nbre - 1) * (neurone[pos].s1 / max));*/
		value = (int) neurone[pos].s1;
		dprints("\nvalue in extract = %d (%s)",value,def_groupe[gpe].no_name);
		if (value < 0 || value >= nbre)
		{
			continue;
		}
		neurone[deb+value].s = neurone[deb+value].s1 = neurone[deb+value].s2 = 1.;
	}
}

void destroy_extract_multi_vals_to_one_vector(int gpe)
{
	free(def_groupe[gpe].data);
}
