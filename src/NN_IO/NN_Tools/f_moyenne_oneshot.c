/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_moyenne_oneshot.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:   computes and store a mean value of input on the val field of the link.
		the window for the mean is the temps field of the link.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_moyenne_oneshot
{
    int gpe_transition;
} MyData_f_moyenne_oneshot;
void function_moyenne_oneshot(int numero)
{

    int i, l, reset = 0;

    int first_neuron = def_groupe[numero].premier_ele;

    float tau = 0./*,  reset_value = 0.*/;

    type_coeff *synapse;

    MyData_f_moyenne_oneshot *my_data;
    int gpe_transition = -1/*, gpe_reset_value = -1*/;
    
    if (def_groupe[numero].data == NULL)
    {
        i = 0;
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "transition_detect") == 0
                || strcmp(liaison[l].nom, "reset") == 0)
            {
                gpe_transition = liaison[l].depart;
            }
       
            i++;
            l = find_input_link(numero, i);
        }
        my_data = (MyData_f_moyenne_oneshot *) malloc(sizeof(MyData_f_moyenne_oneshot));
        if (my_data == NULL)
        {
            printf("error malloc %s\n", __FUNCTION__);
            exit(0);
        }
        my_data->gpe_transition = gpe_transition;
        def_groupe[numero].data = (MyData_f_moyenne_oneshot *) my_data;
    }
    else
    {
        my_data = (MyData_f_moyenne_oneshot *) def_groupe[numero].data;
        gpe_transition = my_data->gpe_transition;
       
    }

    tau=liaison[(neurone[def_groupe[numero].premier_ele].coeff)->gpe_liaison].temps;
    printf("================tau=%f\n",tau);
    if (gpe_transition != -1)
    {
        if (neurone[def_groupe[gpe_transition].premier_ele].s1 > 0.5)
        {
            reset = 1;
            /*if (gpe_reset_value != -1)
                reset_value =
                    neurone[def_groupe[gpe_reset_value].premier_ele].s1;*/

        }
    }
    if(reset==0)
    {
	for (i = first_neuron; i < first_neuron+def_groupe[numero].nbre; i++)
	{
		synapse = neurone[i].coeff;
		neurone[i].s=neurone[i].s1=neurone[i].s2=(neurone[synapse->entree].s1+ tau*neurone[i].s1 )/(tau+1);
	}
    }
    else
    {	
	for (i = first_neuron; i < first_neuron+def_groupe[numero].nbre; i++)
    	{
        	synapse = neurone[i].coeff;
		neurone[i].s=neurone[i].s1=neurone[i].s2=neurone[synapse->entree].s1;
    	}
    }
}
