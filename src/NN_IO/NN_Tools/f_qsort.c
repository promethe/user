/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
\defgroup QSORT QSORT
\ingroup NN_Tools
\file  f_qsort.c
\brief QSORT returns the indices of an array of values in (as/de)cending order. QSORT effectue un tri. Les neurones sont réordonnés par ordre croissant (par défaut) ou décroissant d'activité, selon le champ s (par défaut), s1 ou s2, en fonction des options -dec (décroissant) et -sX. La sortie s renvoie la valeur de depart inchangee et la sortie s1 son index

Author: A. Pitti
Created: 18/10/2011
Modified:
- author: A. Pitti
- description: xxxxxxxx
- date: 14/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:



Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef float (*key_func_t)(int neuron);
typedef int (*cmp_func_t)(float l, float r);

static float key_s(int neuron) {
    return neurone[neuron].s;
}
static float key_s1(int neuron) {
    return neurone[neuron].s1;
}
static float key_s2(int neuron) {
    return neurone[neuron].s2;
}

static int cmp_lt(float l, float r) {
    return l < r;
}
static int cmp_gt(float l, float r) {
    return l > r;
}

typedef struct MyData_qsort {
    int in;
    key_func_t key;
    cmp_func_t cmp;

    float *table;
    int *table1;
} MyData_qsort;

void new_qsort(int gpe) {
    int in;
    int l;
    
    key_func_t key = key_s;
    cmp_func_t cmp = cmp_gt;

    MyData_qsort* data = NULL;

    char chaine[255];

    if (def_groupe[gpe].data == NULL) {
        /* get the input link */
        l = find_input_link(gpe, 0);
        if (l == -1) {
            PRINT_WARNING("gpe #%s '%s': the group should have at least one input link!\n", 
                def_groupe[gpe].no_name, def_groupe[gpe].nom);
            in = -1;
        } else {
            in = liaison[l].depart;
            if (prom_getopt(liaison[l].nom, "s", chaine) == 2) {
                switch (atoi(chaine)) {
                    case 1:
                    key = key_s1;
                    break;
                    case 2:
                    key = key_s2;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid argument for option -s, expected '1' or '2', got '%s'\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
            }
            if (prom_getopt(liaison[l].nom, "dec", chaine) != 0) {
                cmp = cmp_lt;
            }
        }
        
        def_groupe[gpe].data = data = malloc(sizeof(MyData_qsort));
        if (data == NULL) {
            EXIT_ON_ERROR("gpe #%s '%s': malloc failed\n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
        }
        
        data->key = key;
        data->cmp = cmp;
        data->in = in;

	data->table  = (float *) malloc(def_groupe[gpe].nbre*sizeof(float));
	data->table1 = (int *) malloc(def_groupe[gpe].nbre*sizeof(int));
    }
}

void destroy_qsort(int gpe) {
    if (def_groupe[gpe].data != NULL) {
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

void function_qsort(int gpe) {
    MyData_qsort* data = def_groupe[gpe].data;
    int in = data->in;
    int i, k, p;
    int n = def_groupe[gpe].nbre;
    int deb = def_groupe[gpe].premier_ele;
    int deb_in;
    float swap_tmp;
    key_func_t key = data->key;
    cmp_func_t cmp = data->cmp;
    
    if (data->in == -1) return;
    
    deb_in = def_groupe[in].premier_ele;
    
    /* heap sort */
    
    #define swap(x,y,S) do { \
        swap_tmp = neurone[x].S; \
        neurone[x].S = neurone[y].S; \
        neurone[y].S = swap_tmp; \
    } while (0)
    #define swap_neurons(x,y) do { \
        swap(x,y,s); swap(x,y,s1); swap(x,y,s2); \
    } while (0)
    
    /* 1) copy inputs */
    
    for (i = 0; i < n && i < def_groupe[in].nbre; ++i) {
        data->table[i] = neurone[deb_in+i].s;

        neurone[deb+i].s  = neurone[deb_in+i].s;
        neurone[deb+i].s1 = i; /* copy the index in S1*/
        neurone[deb+i].s2 = neurone[deb_in+i].s2;
    }
    for (i = def_groupe[in].nbre; i < n; ++i) {
        neurone[deb+i].s  = neurone[deb+i].s1 = neurone[deb+i].s2 = 0;
    }
    
    /* 2) insert elements into the heap */
    
    for (i = 1; i < n; ++i) {
        p = (i-1) / 2;
        /*  parent key must always be better than its childs, 
            so we have to swap them from the newly inserted leaf to the root 
            if it is not the case */
        while (cmp(key(deb+i),key(deb+p))) {
            swap_neurons(deb+i,deb+p);
            i = p;
            p = (i-1) / 2;
        }
    }
    
    /* 3) remove elements from the heap */
    
    for (i = n-1; i > 0; --i) {
        swap_neurons(deb, deb+i);
        p = 0;
        /*  parent key must always be better than its childs, 
            so we have to swap them from the newly swapped root to the leafs
            until it is the case */
        while ((2*p+1 < i && cmp(key(deb+2*p+1),key(deb+p))) || (2*p+2 < i && cmp(key(deb+2*p+2),key(deb+p)))) {
            if (2*p+2 < i && cmp(key(deb+2*p+2),key(deb+2*p+1))) k = 2*p+2;
            else k = 2*p+1;
            swap_neurons(deb+p,deb+k);
            p = k;
        }
    }

    /* 4) backup tmp */
    for (i = 0; i < n && i < def_groupe[in].nbre; ++i) {
      data->table1[i] = (int)neurone[deb+i].s1;
   }

    /* 5) write output */
    for (i = 0; i < n && i < def_groupe[in].nbre; ++i) {
      neurone[deb+i].s =data->table[i];
      neurone[deb+data->table1[i]].s1=i;
    }
    
    /* voila! */
}

