/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_get_potential f_get_potential
\ingroup libNN_IO

\brief get s,s1 or s2 input on s,s1 and s2 output

\details

\section Description
 
 Recupere le potentiel sur s ou s1 ou s2 (nom sur le lien d'entree)
des neurones du groupe precedent et le reproduit sur ses neurones de sortie
sur s, s1 et s2.

\file  
\ingroup f_get_potential
\brief 

Description: 
 Recupere le potentiel sur s ou s1 ou s2 (nom sur le lien d'entree)
des neurones du groupe precedent et le reproduit sur ses neurones de sortie
sur s, s1 et s2.

Author: A. de Rengerve
Created: 18/06/2009
Modified:
- author: A. de Rengerve
- description: specific file creation
- date: 18/06/2009

**/

/* #define DEBUG */
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

typedef struct data_get_potential {
  int gpe_deb;
  int gpe_nbre;
  int gpe_entree;
  int gpe_entree_deb;
  int potential_type;
  int increment;
} Data_get_potential;


void function_get_potential(int num)
{  
   Data_get_potential *mydata=NULL;
   int i=0,j;
   float tmp=0.;
   int nbre, nbre2, increment;

   
   dprints ("execution de get potential (%s)\n",def_groupe[num].no_name);
   if(def_groupe[num].data==NULL) {
      mydata = ALLOCATION(Data_get_potential);
      
      mydata->gpe_deb = def_groupe[num].premier_ele;
      mydata->gpe_nbre = def_groupe[num].nbre;
      
      
      j=find_input_link(num,0);
      if(j==-1) {
	 EXIT_ON_ERROR("get potential (%s) : link error !\n",def_groupe[num].no_name);
      }
      
      if(strcmp(liaison[j].nom,"s")==0)
      {
	 /* recuperation de s*/
	 mydata->gpe_entree=liaison[j].depart;
	 mydata->gpe_entree_deb=def_groupe[mydata->gpe_entree].premier_ele;
	 mydata->potential_type = 0;
      }
      else if(strcmp(liaison[j].nom,"s1")==0)
      {
	 /* recuperation de s*/
	 mydata->gpe_entree=liaison[j].depart;
	 mydata->gpe_entree_deb=def_groupe[mydata->gpe_entree].premier_ele;
	 mydata->potential_type = 1;
      }
      else if(strcmp(liaison[j].nom,"s2")==0)
      {
	 /* recuperation de s*/
	 mydata->gpe_entree=liaison[j].depart;
	 mydata->gpe_entree_deb=def_groupe[mydata->gpe_entree].premier_ele;
	 mydata->potential_type = 2;
      }
      else {
	 EXIT_ON_ERROR("get potential (%s) : link error !\n",def_groupe[num].no_name);
      }
      
      nbre = def_groupe[mydata->gpe_entree].nbre;
      nbre2 = def_groupe[mydata->gpe_entree].taillex 
	 * def_groupe[mydata->gpe_entree].tailley;
      mydata->increment = nbre / nbre2;
      
      if(mydata->gpe_nbre!= nbre2) 
      {
	 EXIT_ON_ERROR("f_get_potential (%s) : erreur : nb neurones differents dans (%d) et (%d) \n",def_groupe[num].no_name, def_groupe[mydata->gpe_entree].no_name,def_groupe[num].no_name);
      }
      
      def_groupe[num].data = mydata;
   }
   else {
      mydata = def_groupe[num].data;
   }
   
   for(i=0; i<mydata->gpe_nbre; i++) {
      
      increment = mydata->increment;    
      
      switch(mydata->potential_type) {
	 case 0:
	    tmp = neurone[mydata->gpe_entree_deb+(i+1)*increment-1].s;
	    break;
	 case 1:
	    tmp = neurone[mydata->gpe_entree_deb+(i+1)*increment-1].s1;	    
	    break;
	 case 2:
	    tmp = neurone[mydata->gpe_entree_deb+(i+1)*increment-1].s2;
	    break;
	 default:
	    EXIT_ON_ERROR("get_potential (%s) : oups ! je ne devrais pas voir ce message !\n", def_groupe[num].no_name);
      }
      
      neurone[mydata->gpe_deb+i].s = neurone[mydata->gpe_deb+i].s1 = neurone[mydata->gpe_deb+i].s2 = tmp;
   }
   
   dprints("sortie de get potential (%s)\n",def_groupe[num].no_name);
}

