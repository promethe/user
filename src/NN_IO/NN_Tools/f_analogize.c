/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_analogize.c
\brief

Author: G. Merle
Created: 16/05/2011
Modified:
- author: xxxx
- description: xxxx
- date: xx/xx/xxxx

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction qui lit un vecteur sur le groupe d'entrée et active analogiquement le neurone du groupe de sortie selon les valeurs du vecteur d'entrée.
Codage par une population de neurones avec mise à l'échelle. Le nombre de neurone représente l'inverse du pas de discretisation. On peut spécifier un min et un max sur le lien + le type de projection: lineaire ou logarithmique.
C'est la fonction inverse de function_discretize(). Elle prend exactement les mêmes options, et fait
exactement le calcul inverse. Il serait bon de maintenir cette symétrie entre ces deux fonctions.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo 
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Tout comme pour f_discretize(), le comportement de l'échelle logarithmique n'est pas du tout clair.

Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct analogize_data {
    int in;                     /* groupe neurone entree */
    float scale;                /* echelle pour la discretisation */
    float offset;               /* offset de la discretisation */
    int logs;                   /* 0 échelle proportionnelle, 1 échelle logarithmique */
    int soft;                   /* 0 discrétisation dure, 1 discrétisation douce */
} analogize_data;

void new_analogize(int gpe) {
    int in;
    int l;
    float vmin = 0, vmax = 0, scale = 0;
    float offset = 0.;
    int flag = 0;
    int logs = 0;
    int soft = 0;
    char chaine[255];
    analogize_data *data = NULL;


#ifdef DEBUG
    printf("New_analogize(%d)\n", gpe);
#endif

    if (def_groupe[gpe].data == NULL) {
        /* get the input link */
        l = find_input_link(gpe, 0);
        if (l == -1)           EXIT_ON_GROUP_ERROR(gpe, "the group should have one input group!\n");

        if (prom_getopt(liaison[l].nom, "m", chaine) == 2) { /* valeur analogique min */
            flag = flag + 1;
            vmin = -atof(chaine);
#ifdef DEBUG
            printf("m= %s\n", chaine);
#endif
        }
        if (prom_getopt(liaison[l].nom, "M", chaine) == 2) { /* valeur analogique max */
            flag = flag + 1;
            vmax = atof(chaine);
#ifdef DEBUG
            printf("M= %s\n", chaine);
#endif
        }
        if (prom_getopt(liaison[l].nom, "l", chaine) != 0) { /* type d'echelle : linéaire (default) ou log */
            logs = 1;
        }
        if (prom_getopt(liaison[l].nom, "s", chaine) != 0) { /* type de discrétisation, dure (default) ou douce */
            soft = 1;
        }
        if (prom_getopt(liaison[l].nom, "w", chaine) != 0) {
            /* warnings ou non (inutilisé) */
        }
        if (flag < 2) {
            fprintf(stderr,"error: in new_analogize(gpe = %i): you must specify -m -M arguments on the link ex: -m0.0-M1.0 (-min and +max values)\n", gpe);
            fprintf(stderr,"note: vmin = %f, vmax = %f, ech_log = %d, soft = %d \n", vmin, vmax, logs, soft);
            exit(1);
        }

        in = liaison[l].depart;

        if (!logs) {
            scale = def_groupe[in].nbre / (vmax - vmin);
            offset = -vmin * scale;
        } else {
            scale = log((float) def_groupe[in].nbre) / log(vmax - vmin);
            /* attention au signe de vmin quel est le bon calcul??? */
            offset = -(log(vmin) * scale);
        }

        def_groupe[gpe].data = data = (analogize_data *) malloc(sizeof(analogize_data));
        if (data == NULL) {
            fprintf(stderr,"error: in new_analogize(gpe = %i): malloc failed\n", gpe);
            exit(1);
        }
        data->in = in;
        data->scale = scale;
        data->offset = offset;
        data->logs = logs;
        data->soft = soft;
    }
}

void destroy_analogize(int gpe) {
    if (def_groupe[gpe].data != NULL) {
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

void function_analogize(int gpe) {
    int deb, debe, nbr;
    int i, index;
    float scale, offset, val, pos;
    analogize_data *data = (analogize_data *) def_groupe[gpe].data;
    
    if (data == NULL) return;
    
    deb = def_groupe[data->in].premier_ele;
    nbr = def_groupe[data->in].nbre;
    debe = def_groupe[gpe].premier_ele;
    scale = data->scale;
    offset = data->offset;
    
    /* zeroing everything */
    pos = 0;
    val = 0;
    index = 0;
    
    /* compute the index */
    if (!data->soft) { /* discrétisation dure */
        for (i = 0; i < nbr; ++i) {
            if (neurone[deb+i].s1 > val) {
                val = neurone[deb+i].s1;
                index = i;
            }
        }
        pos = index;
    } else { /* discrétisation douce */
        for (i = 0; i < nbr; ++i) {
            val += neurone[deb+i].s1;
            pos += neurone[deb+i].s1 * i;
        }
        if (val <= 0.) pos = (nbr-1) / 2.;
        else pos /= val;
    }
    
    /* compute the value */
    val = (pos - offset) / scale;
    
    if (data->logs) /* logarithmic scale */
    val = exp(val);
    
    neurone[debe].s = neurone[debe].s1 = neurone[debe].s2 = val;
}

