/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_esn.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygenambigous_states_.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #define DEBUG 1 */

typedef	struct		s_esn
{
  int			first;
  int			nbre;
  int			pas;
  FILE			*f;
  int			gpe_teacher;
  long			time;
}			t_esn;

void			new_esn(int gpe)
{
  t_esn			*data = NULL;
  int			i = 0;
  int			first = -1;
  int			nbre = -1;
  type_coeff		*lien = NULL;
  FILE			*g = NULL;
  int			prev_gpe = -1;
  int			l = 0;
  char			file_name[128];
  char			time[256];

  if (def_groupe[gpe].data == NULL)
    {
      if ((data = malloc(sizeof(t_esn))) == NULL)
	{
	  perror("f_esn : malloc");
	  exit(1);
	}
      first = data->first = def_groupe[gpe].premier_ele;
      nbre = data->nbre = def_groupe[gpe].nbre;
      data->pas = 0;
      data->gpe_teacher = -1;

/*       sprintf(file_name, "w_esn_%s.dat", def_groupe[gpe].no_name); */
/*       if ((data->f = fopen(file_name, "w+")) == NULL) */
/* 	{ */
/* 	  fprintf(stderr, "%s :", def_groupe[gpe].no_name); */
/* 	  fprintf(stdout, "Pas de sauvegarde des poids des liens recurrent !\n"); */
/* 	} */

      def_groupe[gpe].data = (void *)data;


      for (l = 0; l < nbre_liaison; l++)
	{
	  if ((liaison[l].arrivee == gpe) && (!strncmp(liaison[l].nom, "teacher", 7)))
	    {
	      data->gpe_teacher = liaison[l].depart;
	      if (prom_getopt(liaison[l].nom, "t", time) == 2)
		{
		  data->time = atol(time);
		}
	    }
	  else
	    if ((liaison[l].arrivee == gpe) && (!strncmp(liaison[l].nom, "sync", 4)))
	      {
		;
	      }
	    else
	      if ((liaison[l].arrivee == gpe) && (liaison[l].depart != gpe))
		{
		  prev_gpe = liaison[l].depart;
		}
	}

      sprintf(file_name, "w_esn_in_%s.dat", def_groupe[gpe].no_name);
      fprintf(stdout, "input weights load from %s\n", file_name);
      if ((g = fopen(file_name, "r")) == NULL)
	{
	  fprintf(stderr, "%s :", def_groupe[gpe].no_name);
	  perror("f_esn : pas de chargement des poids des connexion entrantes");
	}
      else
	{
/* 	  fprintf(stdout, "f_esn : lecture des poids des connexions entrantes du groupe %s %i\n", def_groupe[gpe].no_name, nbre); */
	  for (i = 0; (g != NULL) && (i < nbre); i++)
	    {
/* 	      fprintf(stdout, "DEBUG : lien = neurone[first + i].coeff = %p\n", neurone[first + i].coeff); */
	      for (lien = neurone[first + i].coeff; lien != NULL; lien = lien->s)
		{
/* 		  fprintf(stdout, "DEBUG : neurone[lien->entree].groupe %i == prev_gpe %i \n", neurone[lien->entree].groupe, prev_gpe); */
		  if (neurone[lien->entree].groupe == prev_gpe)
		    {
		      char sep;
		      if ((fscanf(g, "%f%c", &lien->val, &sep)) != 2)
		      {
			fprintf(stderr, "%s :", def_groupe[gpe].no_name);
			fprintf(stderr, "erreur de chargement des poids des liens entrant\n");
			exit(1);
		      }
/* 		    fprintf(stdout, "%f\n", lien->val); */
		    }
		}
	    }
	  fclose(g);
	  g = NULL;
	}

      sprintf(file_name, "w_esn_rec_%s.dat", def_groupe[gpe].no_name);
      fprintf(stdout, "recursive weights load from %s\n", file_name);
      if ((g = fopen(file_name, "r")) == NULL)
	{
	  fprintf(stderr, "%s :", def_groupe[gpe].no_name);
	  perror("f_esn : pas de chargement des poids des connexion recurrentes");
	}
      else
	{
/* 	  fprintf(stdout, "f_esn : lecture des poids des connexions reccurentes du groupe %s %i\n", def_groupe[gpe].no_name, nbre); */
	  for (i = 0; (g != NULL) && (i < nbre); i++)
	    {
/* 	      fprintf(stdout, "DEBUG : lien = neurone[first + i].coeff = %p\n", neurone[first + i].coeff); */
	      for (lien = neurone[first + i].coeff; lien != NULL; lien = lien->s)
		{
/* 		  fprintf(stdout, "DEBUG : neurone[lien->entree].groupe %i == gpe %i \n", neurone[lien->entree].groupe, gpe); */
		  if (neurone[lien->entree].groupe == gpe)
		    {
		      if ((fscanf(g, "%f", &lien->val)) != 1)
			{
			  fprintf(stderr, "%s :", def_groupe[gpe].no_name);
			  fprintf(stderr, "erreur de chargement des poids des liens recurrents\n");
			  exit(1);
			}
/* 		      fprintf(stdout, "%f\n", lien->val); */
		    }
		}
	    }
	  fclose(g);
	}
    }
}

void			function_esn(int gpe)
{
  t_esn			*data = NULL;
  int			i = 0;
  int			first = -1;
  int			nbre = -1;
  type_coeff		*lien = NULL;
  float			potentiel = 0.0;
  FILE			*f = NULL;
  int			gpe_teacher = -1;

  (void) f; // (unused)

  data = (t_esn *)def_groupe[gpe].data;
  nbre = data->nbre;
  first = data->first;
  f = data->f;
  gpe_teacher = data->gpe_teacher;

#ifdef DEBUG
  printf("[%i] : ######################################################\n", gpe);
#endif

  for (i = 0; i < nbre; i++)
    {
      potentiel = 0.0;
      if (data->gpe_teacher >= 0)
	{
	  if (global_learn || data->pas < data->time)
	    potentiel = neurone[def_groupe[gpe_teacher].premier_ele + i].s2;

	  if (!global_learn && data->pas >= data->time)
	    {
	      for (lien = neurone[first + i].coeff; lien != NULL; lien = lien->s)
		{
		  potentiel += (lien->val * neurone[lien->entree].s2);
#ifdef DEBUG
		  printf("[%i] : (%i) ->val=%f ->entree=%i\n", gpe, i, lien->val, lien->entree);
#endif
		}
	    }
	  neurone[first + i].s = potentiel;
	}
      else
	{
	  for (lien = neurone[first + i].coeff; lien != NULL; lien = lien->s)
	    {
	      potentiel += (lien->val * neurone[lien->entree].s2);
#ifdef DEBUG
	      printf("[%i] : (%i) ->val=%f ->entree=%i\n", gpe, i, lien->val, lien->entree);
#endif	    
	    }
	  neurone[first + i].s = potentiel;
	}

#ifdef DEBUG
      printf("[%i] : potentiel = %f\n", gpe, potentiel);
#endif

/*       if (f != NULL) */
/* 	{ */
/* 	  if (i != (nbre - 1)) */
/* 	    fprintf(f, "%f ", neurone[first + i].s1); */
/* 	  else */
/* 	    fprintf(f, "%f\n", neurone[first + i].s1); */
/* 	  fflush(f); */
/* 	} */
    }
  for (i = 0; i < nbre; i++)
    neurone[first + i].s1 = neurone[first + i].s2 = neurone[first + i].s;

  data->pas++;
}
