/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_z_n f_z_n
 \ingroup libNN_IO
 \brief Introduce a delay of n iterations

 \author Matthieu CARDINAUX
 \date 12/07/2012


 Description:
 Function (based on f_z_1.c) used to introduce a delay of n step (n synaptic transmission delay) 
 If a link with -n[NumberOfDelaySteps] is found, computes a delay of [NumberOfDelaySteps]. By default (i.e. without -n link), computes a delay of one iteration.
 \file
 \ingroup f_z_n

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

typedef struct type_data_z_n {
   int deb_e;
   type_neurone **retard;
   int taille_retard;
   int nbre_bloc_retard;
   int taille_bloc_s;
} type_data_z_n;

void new_z_n(int gpe_sortie)
{
   int gpe_entree,nbre_retard=1;
   int taille_groupe_e = 0;
   int taille_groupe_s;
   int deb_e = 0, deb_s,taille_bloc_s=0;
   char param[50];
   char nom_liaison[32];
   int i, j, temps_t;
   type_data_z_n *my_data;
   type_neurone **retard;
   int taille_retard;
#ifdef DEBUG
   printf("entering %s (%s line %i)", __FUNCTION__, __FILE__, __LINE__);
#endif

   deb_s = def_groupe[gpe_sortie].premier_ele;
   taille_groupe_s = def_groupe[gpe_sortie].nbre;

   taille_retard = 1;
   for (i = 0; i < nbre_liaison; i++)
   {
      if (liaison[i].arrivee == gpe_sortie)
      {
         if (prom_getopt(liaison[i].nom, "-n", param) == 2)
         {
            taille_retard = atoi(param);
         }

         gpe_entree = liaison[i].depart;
         strcpy(nom_liaison, liaison[i].nom);
         taille_groupe_e = def_groupe[gpe_entree].nbre;
         deb_e = def_groupe[gpe_entree].premier_ele;
         break;
      }
   }

   if (taille_groupe_s != taille_groupe_e)
   {
      if(taille_groupe_s%taille_groupe_e!=0)
      {
         EXIT_ON_ERROR("Taille incoherente avec la taille de la carte d'entree in group %s", def_groupe[gpe_sortie].no_name);
      }
      else
      {
         nbre_retard=taille_groupe_s/taille_groupe_e;
         taille_bloc_s=taille_groupe_e;

         if(taille_retard<nbre_retard)
            EXIT_ON_ERROR("Dans cette version de z-n le nombre de bloc de retard affiche est forcement en lien avec le retard max (on va forcement de present jusqu'a retard), verifiez la coherence de la taille du groupe %s vis a vis de -n", def_groupe[gpe_sortie].no_name);
      }
   }
   else
   {
      taille_bloc_s=taille_groupe_s;
   }

   my_data = (type_data_z_n*) malloc(sizeof(type_data_z_n));
   retard = (type_neurone **) malloc(taille_retard * sizeof(type_neurone*));
   for (i = 0; i < taille_retard; i++)
   {
      retard[i] = (type_neurone *) malloc(taille_bloc_s * sizeof(type_neurone));
   }
   my_data->taille_retard = taille_retard;
   my_data->retard = retard;
   my_data->nbre_bloc_retard = nbre_retard;
   my_data->taille_bloc_s = taille_bloc_s;
   my_data->deb_e = deb_e;
   def_groupe[gpe_sortie].data = (void*) my_data;

   //init des neurones
   for (j = 0; j < taille_groupe_s; j++)
   {
      neurone[deb_s + j].s = neurone[deb_s + j].s1 = neurone[deb_s + j].s2 = 0.;
   }

   //init des neurones
   for (temps_t = 0; temps_t < taille_retard - 1; temps_t++)
   {
      for (j = 0; j < taille_bloc_s; j++)
      {
         retard[temps_t][j].s = 0;
         retard[temps_t][j].s1 = 0;
         retard[temps_t][j].s2 = 0;
      }
   }

   for (j = 0; j < taille_bloc_s; j++)
   {
      retard[0][j].s = neurone[deb_e + j].s;
      retard[0][j].s1 = neurone[deb_e + j].s1;
      retard[0][j].s2 = neurone[deb_e + j].s2;
   }
}

void function_z_n(int gpe_sortie)
{
   type_neurone **retard;
   int taille_retard;
   //int taille_groupe_s;
   int deb_e, deb_s,taille_bloc_s;
   int j,i, temps_t,nbre_bloc_retard;

   taille_retard = ((type_data_z_n*) def_groupe[gpe_sortie].data)->taille_retard;
   retard = ((type_data_z_n*) def_groupe[gpe_sortie].data)->retard;
   deb_e = ((type_data_z_n*) def_groupe[gpe_sortie].data)->deb_e;
   taille_bloc_s=((type_data_z_n*) def_groupe[gpe_sortie].data)->taille_bloc_s;
   deb_s = def_groupe[gpe_sortie].premier_ele;
   //taille_groupe_s = def_groupe[gpe_sortie].nbre;
   nbre_bloc_retard=((type_data_z_n*) def_groupe[gpe_sortie].data)->nbre_bloc_retard;

   for(i = 0; i < nbre_bloc_retard; i++)
   {
      for (j = 0; j < taille_bloc_s; j++)
      {
         neurone[deb_s + j + i*taille_bloc_s].s = retard[taille_retard - 1 - i][j].s;
         neurone[deb_s + j + i*taille_bloc_s].s1 = retard[taille_retard - 1 - i][j].s1;
         neurone[deb_s + j + i*taille_bloc_s].s2 = retard[taille_retard - 1 - i][j].s2;
      }
   }

   for (temps_t = taille_retard - 1; temps_t > 0; temps_t--)
   {
      for (j = 0; j < taille_bloc_s; j++)
      {
         retard[temps_t][j].s = retard[temps_t - 1][j].s;
         retard[temps_t][j].s1 = retard[temps_t - 1][j].s1;
         retard[temps_t][j].s2 = retard[temps_t - 1][j].s;
      }
   }

   for (j = 0; j < taille_bloc_s; j++)
   {
      retard[0][j].s = neurone[deb_e + j].s;
      retard[0][j].s1 = neurone[deb_e + j].s1;
      retard[0][j].s2 = neurone[deb_e + j].s2;
   }

#ifdef DEBUG
   printf("exiting %s (%s line %i)", __FUNCTION__, __FILE__, __LINE__);
#endif
}

void destroy_z_n(int gpe)
{

   int i = 0;
   type_data_z_n* my_data = (type_data_z_n*) def_groupe[gpe].data;
   int t_ret = my_data->taille_retard;
   type_neurone** retard = my_data->retard;
   for (i = 0; i < t_ret; i++)
   {
      free(retard[i]);
   }
   free(retard);
   free(my_data);
}
