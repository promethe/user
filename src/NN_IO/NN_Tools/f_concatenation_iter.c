/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************

\defgroup f_concatenation_iter f_concatenation_iter
\ingroup NN_Tools
\file  f_concatenation_iter.c

\brief
Description:
Concatenates the input group neurons (values) over n iterations in the current group. It is possible to reset the neurons, the define an offset and/or to flip the concatenation direction from "left to right" (regular) to "right to left" and vice versa.


Author: Marwen Belkaid
Created: 07/05/2015




\section Input Links
Mandatory :
- -input: the input group of which we want to iteratively concatenate the values
Optional :
- -n: number of iterations we want to concatenate before looping back. If not define, it is equal to gpe_size/input_gpe_size
- -reset: define an input group that resets the current group neurons when equal to 1
- -flip: defines an input group that flips the concatenation direction (e.g. L_TO_R to R_TO_L) when equal to 1
- -offset: the concatenation starts with an offset instead of starting at zero. (consistent when flip=1)
- -pos: allows to bypass the regular stepbystep concatenation process and get position from an input group

http://www.doxygen.org
 ************************************************************/

//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>



typedef enum concat_iter_dir
{
   CONCAT_ITER_L_TO_R,
   CONCAT_ITER_R_TO_L,
} concat_iter_dir ;

typedef struct data_concatenation_iter
{
   int input_gpe ;
   int reset_gpe ;
   int flip_gpe ;
   int pos_gpe ;
   concat_iter_dir direction ;
   int nb_iter;
   int **lookup_start_iter;
   int count;
} data_concatenation_iter;


void new_concatenation_iter(int gpe)
{

   data_concatenation_iter* mydata ;
   int tmp_input_gpe = -1, tmp_reset_gpe = -1, tmp_flip_gpe = -1, tmp_pos_gpe = -1, tmp_nb_iter = -1, tmp_offset = 0;
   int **tmp_lookup;
   int i = 0, links_in, n ;
   char tmp_input_value[5];

   /* Get Input group */
   while((links_in = find_input_link (gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "-input", NULL) == 1)
         tmp_input_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-reset", NULL) == 1)
         tmp_reset_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-flip", NULL) == 1)
         tmp_flip_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-pos", NULL) == 1)
         tmp_pos_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-n", tmp_input_value) == 2)
         tmp_nb_iter = atoi(tmp_input_value);
      if(prom_getopt(liaison[links_in].nom, "-offset", tmp_input_value) == 2)
         tmp_offset = atoi(tmp_input_value);

      i++ ;
   }

   /* Check inputs */
   if(tmp_input_gpe < 0)
      EXIT_ON_ERROR("%s needs an input link named \"-input\" \n", __FUNCTION__) ;
   n = (int) def_groupe[gpe].nbre/def_groupe[tmp_input_gpe].nbre ;
   if(tmp_nb_iter < 0)
   {
      PRINT_WARNING("%s : you did not specify the number of iterations (-n[nb_iter] link). Default value = group_size/input_group_size will be considered \n", __FUNCTION__) ;
      tmp_nb_iter = n;
   }
   else if(tmp_nb_iter > n)
      EXIT_ON_ERROR("%s : this group is too small to memorize the required number of iterations\n", __FUNCTION__) ;
   else if(tmp_offset > def_groupe[gpe].nbre - tmp_nb_iter*def_groupe[tmp_input_gpe].nbre)
      EXIT_ON_ERROR("%s : the given offset is too big \n", __FUNCTION__) ;

   /* allocate lookup table  */
   if(tmp_flip_gpe < 0)
   {
      tmp_lookup = ALLOCATION(int*) ;
      *tmp_lookup = MANY_ALLOCATIONS(tmp_nb_iter, int) ;
      for(i = 0 ; i < tmp_nb_iter ; i++)
      {
         tmp_lookup[0][i] = tmp_offset + i * def_groupe[tmp_input_gpe].nbre ; // L_TO_R
         dprints("lookup[%d] = %d", i, tmp_lookup[i]) ;
      }
   }
   else
   {
      tmp_lookup = MANY_ALLOCATIONS(2,int*) ;
      *tmp_lookup = MANY_ALLOCATIONS(tmp_nb_iter, int) ;
      *(tmp_lookup+1) = MANY_ALLOCATIONS(tmp_nb_iter, int) ;
      for(i = 0 ; i < tmp_nb_iter ; i++)
      {
         tmp_lookup[0][i] = tmp_offset + i * def_groupe[tmp_input_gpe].nbre ; // L_TO_R
         tmp_lookup[1][i] = def_groupe[gpe].nbre - tmp_offset - (i+1) * def_groupe[tmp_input_gpe].nbre ; // R_TO_L
         dprints("lookup[%d] = %d ", i, tmp_lookup[0][i]) ;
         dprints("revert lookup[%d] = %d \n", i, tmp_lookup[1][i]) ;
      }
   }

   /* Create object */
   mydata = ALLOCATION(data_concatenation_iter);
   mydata->input_gpe = tmp_input_gpe ;
   mydata->reset_gpe = tmp_reset_gpe ;
   mydata->flip_gpe = tmp_flip_gpe ;
   mydata->pos_gpe = tmp_pos_gpe ;
   mydata->direction = CONCAT_ITER_L_TO_R ;
   mydata->nb_iter = tmp_nb_iter;
   mydata->lookup_start_iter = tmp_lookup ;
   mydata->count = 0 ;
   def_groupe[gpe].data = (data_concatenation_iter *) mydata;
}


void function_concatenation_iter(int gpe)
{
   int **tmp_lookup ;
   int tmp_input_gpe, tmp_reset_gpe, tmp_flip_gpe, tmp_pos_gpe, tmp_nb_iter, tmp_count;
   data_concatenation_iter *mydata ;
   int i, gpe_size, gpe_start, input_gpe_start, bloc_size, bloc_start;

   /* get params from local structure */
   mydata = (data_concatenation_iter *) def_groupe[gpe].data ;
   tmp_input_gpe = mydata->input_gpe ;
   tmp_reset_gpe = mydata->reset_gpe ;
   tmp_flip_gpe = mydata->flip_gpe ;
   tmp_pos_gpe = mydata->pos_gpe ;
   tmp_nb_iter = mydata->nb_iter ;
   tmp_lookup = mydata->lookup_start_iter ;
   tmp_count = mydata->count ;

   gpe_size = def_groupe[gpe].nbre ;
   gpe_start = def_groupe[gpe].premier_ele ;
   input_gpe_start = def_groupe[tmp_input_gpe].premier_ele ;
   bloc_size = def_groupe[tmp_input_gpe].nbre ;
   bloc_start = -1 ;

   /* reset neurons if needed */
   if(tmp_reset_gpe >= 0 && neurone[def_groupe[tmp_reset_gpe].premier_ele].s1 > 0.999999)
      for(i = gpe_size ; i-- ; )
      {
         neurone[gpe_start+i].s1 = 0. ;
         tmp_count = 0 ;
      }

   /* flip direction if needed */
   if(tmp_flip_gpe >= 0 && neurone[def_groupe[tmp_flip_gpe].premier_ele].s1 > 0.999999)
      mydata->direction = abs(1-mydata->direction) ;

   /* get position from input group (instead of memorized counter) if specified */
   if(tmp_pos_gpe >= 0)
         tmp_count = (int)neurone[def_groupe[tmp_pos_gpe].premier_ele].s1 ;

   /* set next start position */
   bloc_start = tmp_lookup[mydata->direction][tmp_count] ;
   if(bloc_start < 0 || bloc_start >= gpe_size)
      EXIT_ON_ERROR("%s something is wrong with the lookup table ! ", __FUNCTION__);
   dprints("==== count = %d, bloc_start = %d, direction = %d", tmp_count, bloc_start, mydata->direction) ;

   /* concatenate new input bloc */
   for(i = bloc_size ; i-- ; )
      neurone[gpe_start+bloc_start+i].s1 = neurone[input_gpe_start+i].s1 ;

   /* update bloc counter value */
   mydata->count = (tmp_count < tmp_nb_iter-1) ? tmp_count+1 : 0 ;

}



void destroy_concatenation_iter(int gpe)
{
   if (((data_concatenation_iter *) def_groupe[gpe].data)->lookup_start_iter != NULL)
   {
      free(((int *) ((data_concatenation_iter *) def_groupe[gpe].data)->lookup_start_iter));
      ((data_concatenation_iter *) def_groupe[gpe].data)->lookup_start_iter = NULL;
   }
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_concatenation_iter *) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }

}

