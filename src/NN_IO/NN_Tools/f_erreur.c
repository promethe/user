/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_erreur.c 
\brief 

Author: Boucenna Sofiane
Created: 03/04/2006

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: cette fonction calcule l'erreur 

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/*#define DEBUG*/
typedef struct MyData {
  int gpe_prediction;
  int gpe_evenement;
  int temps;
  int compteur;
  int seuil;
  int nbre_tour;
  int nbre_tour_courant;
  float erreur_accu;
}MyData;


void function_erreur(int Gpe)
{

    
    int deb;                    /* le premier neurone */
    
    int deb_prediction;         /* debut du groupe prediction*/
    
    int deb_evenement;          /* debut du groupe evenemt*/

    int i=0;
    int j=0;
    int N,M;
    int inc;
    float erreur;               /* erreur calcule l erreur instantannee*/
    MyData *mydata;
    char param[255];    

#ifdef DEBUG
    printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    if(def_groupe[Gpe].data == NULL){
      mydata=(MyData *)malloc(sizeof(MyData));
      if(mydata==NULL){
	printf("pb malloc pour mydata dans la fonction %s",__FUNCTION__);
	exit(0);
      }
      /*recuperation des parametres d'entree*/
      
      while( (j=find_input_link(Gpe,i) ) != -1){
	
	if(strcmp(liaison[j].nom,"prediction")==0){
	  mydata->gpe_prediction = liaison[j].depart;
	}
	
	if(strcmp(liaison[j].nom,"evenement")==0){
	  mydata->gpe_evenement = liaison[j].depart;
	}

	if(prom_getopt(liaison[j].nom,"nb",param)==2){
	  mydata->nbre_tour_courant=mydata->nbre_tour=atoi(param);  
	} 
	if(prom_getopt(liaison[j].nom,"seuil",param)==2){
	  mydata->seuil = atoi(param);
#ifdef DEBUG
	  printf("seuil=%d\n",mydata->seuil);
#endif
	} 
	
	i++;
	
#ifdef DEBUG
	printf("on a fini de recuperer les parametres\n");
#endif
      }
      
      
      if(mydata->gpe_prediction == -1 || mydata->gpe_evenement == -1)
	{
	  printf("ATTENTION: erreur dans la fonction %s\n",__FUNCTION__);
	  printf("la fonction doit avoir deux groupes en entree \n");
	}
      mydata->erreur_accu=0;
    
      def_groupe[Gpe].data=mydata;	 /*sauvegarde de Mydata*/
    }
    else{
      mydata = def_groupe[Gpe].data;
    }
    
    deb = def_groupe[Gpe].premier_ele;
    deb_prediction = def_groupe[mydata->gpe_prediction].premier_ele;
    deb_evenement = def_groupe[mydata->gpe_evenement].premier_ele;
    N = def_groupe[mydata->gpe_prediction].taillex;
    M = def_groupe[mydata->gpe_prediction].tailley;
    inc = def_groupe[mydata->gpe_prediction].nbre / (N * M); ;

#ifdef DEBUG
   
    printf("groupe precedent prediction= %s\n",def_groupe[mydata->gpe_prediction].no_name);
    printf("groupe precedent evenement= %s\n",def_groupe[mydata->gpe_evenement].no_name);
    
#endif

    /**********************************************************************/
    /*********************      calcule de l erreur      ******************/
    /**********************************************************************/
    
#ifdef DEBUG
    
    printf("neurone_evenement.s= %f\n",neurone[deb_evenement].s);
    printf("neurone_evenement.s1= %f\n",neurone[deb_evenement].s1);
    printf("neurone_evenement.s2= %f\n",neurone[deb_evenement].s2);
    
    printf("neurone_prediction.s= %f\n",neurone[deb_prediction+inc-1].s);
    printf("neurone_prediction.s1= %f\n",neurone[deb_prediction+inc-1].s1);
    printf("neurone_prediction.s2= %f\n",neurone[deb_prediction+inc-1].s2);
#endif


    /*neurone[deb].s=neurone[deb].s1=neurone[deb].s2=0;*/
    


    if(mydata->nbre_tour_courant == 0){
      mydata->nbre_tour_courant = mydata->nbre_tour;
      mydata->temps = 0;
      mydata->erreur_accu = 0;
    }
    else{
      mydata->nbre_tour_courant--;
    }
      
    if(neurone[deb_evenement].s1 > 0.99 )
      {
#ifdef DEBUG
	printf("neurone_evenement.s= %f\n",neurone[deb_evenement].s);
	printf("neurone_evenement.s1= %f\n",neurone[deb_evenement].s1);
	printf("neurone_evenement.s2= %f\n",neurone[deb_evenement].s2);
	
	printf("neurone_prediction.s= %f\n",neurone[deb_prediction+inc-1].s);
	printf("neurone_prediction.s1= %f\n",neurone[deb_prediction+inc-1].s1);
	printf("neurone_prediction.s2= %f\n",neurone[deb_prediction+inc-1].s2);
#endif
	if(neurone[deb_evenement].s1 - neurone[deb_prediction+inc-1].s1 < 0)
	  {
	    
	    /*neurone[deb].s = neurone[deb].s1=neurone[deb].s2*/
	    erreur = neurone[deb_prediction+inc-1].s1 - neurone[deb_evenement].s1 ;
	    mydata->temps++;
	    mydata->erreur_accu = mydata->erreur_accu + erreur; /*+ neurone[deb].s;*/
#ifdef DEBUG
	    printf("CAS evenement<prediction \n");
	    printf("ERREUR = %f\n", erreur);
	    printf("ERREUR ACCU=%f\n",mydata->erreur_accu);
	    printf("TEMPS = %d\n",mydata->temps);
#endif
	  }
	else
	  {
	    
	    /*neurone[deb].s=neurone[deb].s1=neurone[deb].s2*/
	    erreur =  neurone[deb_evenement].s1 - neurone[deb_prediction+inc-1].s1;
	    mydata->temps++;
	    mydata->erreur_accu =mydata->erreur_accu + erreur; /*+ neurone[deb].s;*/
#ifdef DEBUG
	    printf("CAS evenement>prediction \n");
	    printf("ERREUR = %f\n", erreur);
	    printf("ERREUR ACCU=%f\n",mydata->erreur_accu);
	    printf("TEMPS = %d\n", mydata->temps);
#endif
	  }
	
      }

    if(mydata->nbre_tour_courant == 0){

      printf("######################################## ERREUR=%f\n",mydata->erreur_accu);
      
      if(mydata->erreur_accu > mydata->seuil || mydata->erreur_accu <0.000001){
	neurone[deb].s=neurone[deb].s1=neurone[deb].s2=0;
	printf("PAS DANS LE RYHTME\n");
      }
      else{
	printf("RYTHME\n");
	neurone[deb].s=neurone[deb].s1=neurone[deb].s2=1;
      }
      
    }
    
    else{
      /*on affiche rien*/
    }
    
    

#ifdef DEBUG

    printf("VALEUR_NEURONE=%f\n",neurone[deb].s);
    
#endif
    
#ifdef DEBUG
    printf("--------------end of %s\n", __FUNCTION__);
#endif
}
