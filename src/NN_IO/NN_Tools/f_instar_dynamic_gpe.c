/**
 \defgroup f_instar_gpe f_instar_gpe
 \ingroup libNN_IO

 \brief Application d'un instar sur un groupe de neurones (d'apres le model de Grosseberg)

 Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

 \details

 \section Description

 Calcul d'une normalisation des images d'apres le modele Instar de Grosseberg.

 \f[
 eps*d(ni)/dt = -Ani + (b - ni)*pi -ni * \sum(pj)(j!=i)
 \f]

 Si A=1 :
 A l'equilibre
 ni =((bP)/(1+P))*qi

 \f$ P=sum(pj) \f$ (quelquesoit j) !! c'est bien sur tous les pj entrants
 et qi=pj/P

 L'activite total de la carte vaut ((bP)/(1+P))<=b

 b represente le max de la sum des activites de la carte desire
 ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
 ramene a un neurone (donc b=sx*sy*ma).
 ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
 en fait c'est (bp)/(A+p) ici.


 \section Options
 Passage par option (prom_get_opt) des differentes entrees :
 -input : groupe contenant le signal de stimulation
 Le nombre de neurones du groupe doit correspondre a la taille
 du groupe entrant.
 -A < down > : si pas d'option -A sur un des liens : valeur par defaut =1
 si option avec une valeur donnee : cette valeur est utilisee
 si option sans valeur donnee : la sortie s1 du premier neurone
 du groupe d'entree est utilisee
 -B < up > : fonctionnement similaire a -A (valeur par defaut =sx*sy)

 \file
 \ingroup f_instar_gpe

 \brief Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

 Author: Nils Beaussé
 Created: 01/04/2004

 Theoritical description:
 - \f$  LaTeX equation: eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i) \f$  
 Si A=1 :
 A l'equilibre
 \f$ ni =((bP)/(1+P))*qi \f$


 Description:
 Calcul d'une normalisation des images d'apres le modele Instar de Grossberg.

 eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i)

 Si A=1 :
 A l'equilibre
 ni =((bP)/(1+P))*qi

 P=sum(pj)(quelquesoit j) !! c'est bien sur tous les pj entrants
 et qi=pj/P

 L'activite total de la carte vaut ((bP)/(1+P))<=b

 b represente le max de la sum des activites de la carte desire
 ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
 ramene a un neurone (donc b=sx*sy*ma).
 ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
 en fait c'est (bp)/(A+p) ici.

 Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

 Parametres : -A down (=1 par defaut)
 -B up (=sx*sy par defaut)

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

/* #define DEBUG  */

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct instar_gpe_dynamic_data {
  int entree;
  int debut_entree;
  int DT_entree;
  int A_entree;
  int B_entree;
  int C_entree;
  int D_entree;
  int ligne;
  int dvp_entree;
  int dvn_entree;
  float dvp;
  float dvn;

} instar_gpe_dynamic_data;

void new_instar_dynamic_gpe(int gpe)
{
  int index, link, entree = -1, DT_entree = -1, A_entree = -1, B_entree = -1, ligne = -1, C_entree = -1, D_entree = -1, dvn_entree = -1, dvp_entree = -1;
  char string[255];
  instar_gpe_dynamic_data* my_data;

  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);

    while (link != -1)
    {
      if (prom_getopt(liaison[link].nom, "input", string) >= 1)
      {
        entree = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "dvn", string) >= 1)
      {
        dvp_entree = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "dvp", string) >= 1)
      {
        dvn_entree = liaison[link].depart;
      }

      if (prom_getopt(liaison[link].nom, "DT", string) >= 1)
      {
        DT_entree = liaison[link].depart;
      }
      else
      {
        if (prom_getopt(liaison[link].nom, "D", string) >= 1)
        {
          D_entree = liaison[link].depart;
        }
      }
      if (prom_getopt(liaison[link].nom, "A", string) >= 1)
      {
        A_entree = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "B", string) >= 1)
      {
        B_entree = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "C", string) >= 1)
      {
        C_entree = liaison[link].depart;
      }
      if (prom_getopt(liaison[link].nom, "ligne", string) >= 1)
      {
        ligne = 1;
      }

      index++;
      link = find_input_link(gpe, index);
    }

    my_data = ALLOCATION(instar_gpe_dynamic_data);

    if (entree == -1)
    {
      EXIT_ON_ERROR("Pas d'entree à instar dyn '-input'");
    }
    if (DT_entree == -1)
    {
      EXIT_ON_ERROR("Pas d'entree à instar dyn '-DT'");
    }
    if (A_entree == -1)
    {
      EXIT_ON_ERROR("Pas d'entree à instar dyn '-A'");
    }
    if (B_entree == -1)
    {
      EXIT_ON_ERROR("Pas d'entree à instar dyn '-B'");
    }

    my_data->entree = entree;
    my_data->ligne = ligne;
    my_data->A_entree = A_entree;
    my_data->B_entree = B_entree;
    my_data->C_entree = C_entree;
    my_data->D_entree = D_entree;
    my_data->DT_entree = DT_entree;
    my_data->debut_entree = def_groupe[entree].premier_ele;
    my_data->dvn_entree = dvn_entree;
    my_data->dvp_entree = dvp_entree;
    my_data->dvp = -1;
    my_data->dvn = -1;

    def_groupe[gpe].data = (void*) my_data;

  }

}

void function_instar_dynamic_gpe(int gpe)
{
  int i = 0, j = 0, k = 0;
  float sum = 0, sum2 = 0, dvp = 0, dvn = 0, taille = 0, a2, d = 0.0;
  instar_gpe_dynamic_data* my_data = (instar_gpe_dynamic_data*) def_groupe[gpe].data;
  int debut_entree = my_data->debut_entree;
  int debut = def_groupe[gpe].premier_ele;
  int nbre = def_groupe[gpe].nbre;
  float DT = neurone[def_groupe[my_data->DT_entree].premier_ele].s1;
  float A = neurone[def_groupe[my_data->A_entree].premier_ele].s1;
  float B = neurone[def_groupe[my_data->B_entree].premier_ele].s1;
  float C = 1;
  float D = 0;

  if (my_data->C_entree != -1)
  {
    C = neurone[def_groupe[my_data->C_entree].premier_ele].s1;
  }
  if (my_data->D_entree != -1 && my_data->dvp_entree != -1 && my_data->dvn_entree != -1)
  {
    D = neurone[def_groupe[my_data->D_entree].premier_ele].s1;
    if ((fabs(my_data->dvp - neurone[def_groupe[my_data->dvp_entree].premier_ele].s1) > 0.01) || fabs(my_data->dvn - neurone[def_groupe[my_data->dvn_entree].premier_ele].s1) > 0.01)
    {
      my_data->dvp = neurone[def_groupe[my_data->dvp_entree].premier_ele].s1;
      my_data->dvn = neurone[def_groupe[my_data->dvn_entree].premier_ele].s1;
      dvp = my_data->dvp;
      dvn = my_data->dvn;

      taille = dvn - dvp;
      a2 = -(dvp) * (dvp) / log(0.5);
      for (d = dvp; d < dvn; d = d + 1.)
        def_groupe[gpe].tableau_gaussienne[(int) d] = -fabs(cos(((d - dvp) / taille * pi) + (pi / 2.)) * ((cos((d - dvp) / taille * (pi / 2.))) / 2.)) / 2.;
      for (d = 0.; d < dvp + 1; d = d + 1.)
        def_groupe[gpe].tableau_gaussienne[(int) d] = (exp(-d * d / a2) - 0.5) * 2.;
    }
  }
  if (my_data->ligne != 1)
  {
    for (i = debut; i < debut + nbre; i++)
    {
      sum += neurone[i].s1;
    }
    for (i = debut; i < debut + nbre; i++)
    {
      if (neurone[i].s > B) B = neurone[i].s;
      sum2 = 0;

      if (my_data->D_entree != -1)
      {
        for (k = debut; k < debut + nbre; k++)
        {
          if (k != i) sum2 += def_groupe[gpe].tableau_gaussienne[abs(i - k)] * neurone[i].s1;
        }
      }

      neurone[i].s = neurone[i].s + DT * ((-A * neurone[i].s) + ((B - neurone[i].s) * neurone[i].s1) - (C * neurone[i].s * (sum - neurone[i].s1)) + (D * neurone[i].s * sum2) + neurone[i - debut + debut_entree].s1);
      if (neurone[i].s<0) neurone[i].s=0.0;
    }
    for (i = debut; i < debut + nbre; i++)
    {
      neurone[i].s2 = neurone[i].s1 = tanh(neurone[i].s);
      //if (neurone[i].s1>1.0) neurone[i].s1=1.0;
      if (neurone[i].s1<0.0)
        {
          neurone[i].s1=0.0;
        }
    }
  }
  else
  {
    for (j = 0; j < def_groupe[gpe].tailley; j++)
    {
      sum = 0;
      for (i = 0; i < def_groupe[gpe].taillex; i++)
      {
        sum += neurone[debut + i + j * def_groupe[gpe].taillex].s1;
      }

      for (i = 0; i < def_groupe[gpe].taillex; i++)
      {
        if (neurone[debut + i + j * def_groupe[gpe].taillex].s > B) B = neurone[debut + i + j * def_groupe[gpe].taillex].s;
        sum2 = 0;

        for (k = 0; k < def_groupe[gpe].taillex; k++)
        {
          if (k != i) sum2 += def_groupe[gpe].tableau_gaussienne[abs(i - k)] * neurone[debut + k + j * def_groupe[gpe].taillex].s1;
        }

        neurone[debut + i + j * def_groupe[gpe].taillex].s = neurone[debut + i + j * def_groupe[gpe].taillex].s
            + DT
                * ((-A * neurone[debut + i + j * def_groupe[gpe].taillex].s) + ((B - neurone[debut + i + j * def_groupe[gpe].taillex].s) * neurone[debut + i + j * def_groupe[gpe].taillex].s1)
                    - (C * neurone[debut + i + j * def_groupe[gpe].taillex].s * (sum - neurone[debut + i + j * def_groupe[gpe].taillex].s1)) + (D * neurone[debut + i + j * def_groupe[gpe].taillex].s * sum2)
                    + neurone[i + j * def_groupe[gpe].taillex + debut_entree].s1);
      }

      for (i = 0; i < def_groupe[gpe].taillex; i++)
      {
        neurone[debut + i + j * def_groupe[gpe].taillex].s2 = neurone[debut + i + j * def_groupe[gpe].taillex].s1 = tanh(neurone[debut + i + j * def_groupe[gpe].taillex].s);
      }

    }
  }

}

void destroy_instar_dynamic_gpe(int gpe)
{
  instar_gpe_dynamic_data* my_data = (instar_gpe_dynamic_data*) def_groupe[gpe].data;
  if (my_data != NULL)
  {
    free(my_data);
    def_groupe[gpe].data = NULL;
  }
}
