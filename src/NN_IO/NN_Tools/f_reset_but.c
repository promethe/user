/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_reset_but.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Fonction VB :						
 Cette fonction fait un reset du groupe dont le numero est 	
 sur le lien entrant. Cela doit normalement etre la carte BUT 
 c est a dire la carte cognitive.				
 Mode exploration :						
  Systematiquement, on ne laisse				
  sur le niveau BUT que l'activite la plus forte		
  (correspondante a la derniere transition reconnue)		
 Mode Planification :						
  Reset de tout le niveau but					

Macro:
-none 

Local variables:
-int planification

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include "tools/include/local_var.h"

void function_reset_but(int gpe_sortie)
{
    static int first = 1;
    static int gpe_but, deb_but, inc_but, taille_but;
    int i, j, pos, pos2;
    float max, max2;

    if (first)
    {
        first = 0;
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == gpe_sortie)
            {
                gpe_but = atoi(liaison[i].nom);
                deb_but = def_groupe[gpe_but].premier_ele;
                taille_but = def_groupe[gpe_but].nbre;
                j = def_groupe[gpe_but].taillex * def_groupe[gpe_but].tailley;
                inc_but = taille_but / j;
                break;
            }
    }


    printf("\nfunction_reset_but\n");
    max = max2 = -9999;
    pos = pos2 = -1;
    for (i = deb_but + inc_but - 1; i < deb_but + taille_but; i += inc_but)
    {
        if (max < neurone[i].s2)
        {
            max2 = max;
            pos2 = pos;
            max = neurone[i].s2;
            pos = i;
        }
        else if (max2 < neurone[i].s2)
        {
            max2 = neurone[i].s2;
            pos2 = i;
        }
    }
    printf("Max=%f et Max2=%f\n", max, max2);

    for (i = deb_but + inc_but - 1; i < deb_but + taille_but; i += inc_but)
    {
        if (planification)
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
        else if ((i != pos) && (i != pos2))
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
    }
}
