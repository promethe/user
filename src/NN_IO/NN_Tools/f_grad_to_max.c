/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_grad_to_max.c 
\brief 

Author: A. de Rengerve
Created: 18/06/2009
Modified:
- author: A. de Rengerve
- description: specific file creation
- date: 18/06/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
- liens :
          -energy (vecteur horizontal ou vertical)
          -center_x/ou center_y
 Determine le max sur l'entree appelee energy et donne la direction (le grad) normee {-1; 0;1}
pour que le centre (x/y) recupere dans f_project_on_global_space se deplace 
vers ce max.
 Seuillage : Si la distance au max est inferieure au seuil du gpe, alors la valeur est 0.
 Sens positif : sens croissant des (x/y)


Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */

#include <net_message_debug_dist.h>

#define MIN_DETECT_ENERGY 0.01

typedef struct my_data {
  int gpe_deb;
  int gpe_nbre;
  int gpe_energy;
  int gpe_energy_deb;
  int gpe_energy_nbre;
  int gpe_center;
  int gpe_center_deb;
  int gpe_center_nbre;
} myData;


void function_grad_to_max(int num)
{  
   myData *mydata=NULL;
   float max;
   int i=0,j;
   int i_max_energy, i_max_center;
   int valeur;


   dprints ("execution de grad to max\n");
   if(def_groupe[num].data==NULL) {
     mydata = (myData*)malloc(sizeof(myData));
     
     mydata->gpe_deb = def_groupe[num].premier_ele;
     mydata->gpe_nbre = def_groupe[num].nbre;
     
     while((j=find_input_link(num,i))!=-1){
       if(strcmp(liaison[j].nom,"energy")==0)
	 {
	   /* recuperation des donnees du groupe energy*/
	   mydata->gpe_energy=liaison[j].depart;
	   mydata->gpe_energy_deb=def_groupe[mydata->gpe_energy].premier_ele;
	   mydata->gpe_energy_nbre=def_groupe[mydata->gpe_energy].nbre;
	 }
       if(strcmp(liaison[j].nom,"center")==0)
	 {
	   /* recuperation des donnees du groupe de center*/
	   mydata->gpe_center=liaison[j].depart;
	   mydata->gpe_center_deb=def_groupe[mydata->gpe_center].premier_ele;
	   mydata->gpe_center_nbre=def_groupe[mydata->gpe_center].nbre;
	 }
       i++;
     }

     if(mydata->gpe_center_nbre != mydata->gpe_energy_nbre) {
       EXIT_ON_ERROR("Tailles des groupes energy et center incompatibles !!\n");
     }
     def_groupe[num].data = mydata;
   }
   else {
     mydata = def_groupe[num].data;
   }


   /* trouve le max sur energy */
   max = MIN_DETECT_ENERGY;
   i_max_energy = -1;
   for(i = 0; i < mydata->gpe_energy_nbre; i++) {
     dprints("neurone energy :  %f\n", neurone[mydata->gpe_energy_deb + i].s2);
     if(neurone[mydata->gpe_energy_deb + i].s2> max) {
       max = neurone[mydata->gpe_energy_deb + i].s2;
       i_max_energy = i;
     }
   }

   dprints("i_max %d / max energy %f \n", i_max_energy,max);

   if(i_max_energy != -1) {
          
     /* trouve le centre (max sur center) */
     max = 0.5;
     i_max_center = -1;
     for(i = 0; i < mydata->gpe_center_nbre; i++) {
       if(neurone[mydata->gpe_center_deb + i].s2> max) {
	 max = neurone[mydata->gpe_center_deb + i].s2;
	 i_max_center = i;
       }
     }

     dprints("i_max %d / max center %f \n", i_max_center,max);

     if( i_max_center == -1) {
       EXIT_ON_ERROR("pas de center de l'image dans l'espace visuel ?!!\n");
     }

     valeur = i_max_energy-i_max_center;

     dprints("valeur : %d\n",valeur);

     if(valeur>def_groupe[num].seuil)
       neurone[mydata->gpe_deb].s=neurone[mydata->gpe_deb].s1=
	 neurone[mydata->gpe_deb].s2=1.;
     else if(valeur < - def_groupe[num].seuil)        
       neurone[mydata->gpe_deb].s=neurone[mydata->gpe_deb].s1=
	 neurone[mydata->gpe_deb].s2=-1.;
     else
       neurone[mydata->gpe_deb].s=neurone[mydata->gpe_deb].s1=
	 neurone[mydata->gpe_deb].s2=0.;
       
     
   }
   else {
     neurone[mydata->gpe_deb].s=neurone[mydata->gpe_deb].s1=
       neurone[mydata->gpe_deb].s2=0.;
     dprints("pas de max d'energy <%f\n",MIN_DETECT_ENERGY);   
   }

   dprints("sortie du neurone : %f\n",neurone[mydata->gpe_deb].s2);

   dprints("sortie de grad to max (%d)\n",num);
}

