/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define MIN_WEIGHT 0.01
#define MIN_VAL 1e-10


typedef struct sos_ext
{
    int *tab;                   /* tableau pour ordonner les neurones par activite */
    int *last_tab;              /* idem pour l'act precedente */
    type_coeff **coeffs;
    int rangex;
    int rangey;
    int enablegroup;
    int xposgroup;
    int yposgroup;
    float xvar;
    float yvar;
} sos_ext;



/****************************************************************************************
 *
 * function_self_organizing_sequences
 *
 * author: micha hersch
 * created: mai 2004
 * 
 * Description:
 * Cette fonction apprend et generalise une sequence. Elle implemente un modele
 * insprie des chaines de Markov mais avec un apprentissage en ligne inspire des cartes
 * de Kohonen. Voir la dox ad hoc pour plus de details
 * 
 * Liens:
 * Ce groupe doit avoir un lien de 1 vers tous sur lui-meme (initialise a 1/(n*n))
 * un lien virtuel nomme 'xpos' et un lien virtuel nomme 'ypos' lui indiquant
 * ou chercher les entrees (la positin en x et la position en y respectivement). Ce sont des sorties de wta qui indiquent la position
 * courante (la variable observee courante) Il peut aussi y avoir
 * un lien virtuel nomme 'enable' dont l'entree indique si il faut mettre a
 * jour le systeme ou pas (mise a jour si le s1 du premier neurone du groupe
 * d'entree superieur a 0.5)

 * Sortie: sur s1 la probabilite, selon le modele d'etre dans l'etat cache correspondant
 *         sur s2 une version binarisee de s1, le max est 1 les autres 0
 *         sur s le s1 du pas de temps precedent
 * 
 * Implementation: Les transitions sont minorees par une constante pour eviter des probleme de valeurs trop
 * petites et ingerables (les calculs ne se font pas en log).  
 * Si les probabilites correpondant a l'instant precedent sont trop petite, on ne tient pas compte du passe
 * (il est trop improbable pour servir de reference) et l'on onsidere uniquement la position actuelle
 * 
 * Comments: la convergence de cet algorithme n'a pas ete demontree (juin 04)
             Il doit y avoir une activation en entree (sauf si l'entree de enable est inactive)
	     * 
 * parametres:
 * simulation_speed: 2*variance des gaussiennes mesuree en ecart initial entre deux etats adjacents 
 * learning_rate: le taux d'apprentissage des position, celui des transition est le meme * 0.4 
 * 
 * TODO: Mettre deux learning_rate differents, un pour l'apprentissage des position,
 * un pour l'apprentissage des poids. Maintenant l'un est fixe a 0.4 fois l'autre (valeur ad hoc)
 * gerer le cas ou il n'ya pas d'activation en entree
 *************************************************************************************/

void function_self_organizing_sequences(int gpe)
{
    int x, y, i, j, n, k, nbx, nby, prem, last, maxi, maxj, tmp, rangex,
        rangey, lien;
    float xint, yint, offsetx, offsety, max, tot, delta, ttemp, val, sum;
    int *temp, *ttab, *llast_tab;
    type_coeff *co;
    sos_ext *ext;
    x = y = 0;
    nbx = def_groupe[gpe].taillex;
    nby = def_groupe[gpe].tailley;
    n = nbx * nby;
    prem = def_groupe[gpe].premier_ele;
    last = prem + n;
    if (!def_groupe[gpe].ext)
    {                           /* initialisation */
        /* allocation memoire (jamais desallouee ) */
        def_groupe[gpe].ext = malloc(sizeof(sos_ext));
        ext = (sos_ext *) def_groupe[gpe].ext;
        ext->tab = (int *) malloc(n * sizeof(int));
        ext->last_tab = (int *) malloc(n * sizeof(int));


        /*classif des inputs */
        ext->enablegroup = ext->xposgroup = ext->yposgroup = -1;
        for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
        {
            if (!strcmp(liaison[lien].nom, "enable"))
            {
                ext->enablegroup = liaison[lien].depart;
            }
            else if (!strcmp(liaison[lien].nom, "xpos"))
            {
                ext->xposgroup = liaison[lien].depart;
            }
            else if (!strcmp(liaison[lien].nom, "ypos"))
            {
                ext->yposgroup = liaison[lien].depart;
            }
        }
        if (ext->xposgroup == -1 && ext->yposgroup == -1)
        {
            fprintf(stderr,
                    "error, function_self_organizing_sequences can't find position group\n");
        }

        if (ext->xposgroup != -1)
        {
            rangex = def_groupe[ext->xposgroup].nbre;
        }
        else
        {
            rangex = 1;
        }
        if (ext->yposgroup != -1)
        {
            rangey = def_groupe[ext->yposgroup].nbre;
        }
        else
        {
            rangey = 1;
        }

        xint = rangex / (float) nbx;
        yint = rangey / (float) nby;
        ext->xvar = def_groupe[gpe].simulation_speed * (xint) * (xint);
        ext->yvar = def_groupe[gpe].simulation_speed * (yint) * (yint);
        offsety = yint / 2;
        offsetx = xint / 2;
        k = prem;
        for (j = 0; j < nby; j++)
        {
            for (i = 0; i < nbx; i++)
            {
                neurone[k].s = 1.0 / n;
                neurone[k].s1 = 1.0 / n;
                neurone[k].s2 = 0;
                neurone[k].posx = (int) (offsetx + i * xint);
                neurone[k].posy = (int) (offsety + j * yint);
                ext->tab[k - prem] = ext->last_tab[k - prem] = k - prem;    /* penser a mieux */
                k++;
            }
        }
        /* initialisation du tableau des liens (pour les avoir sous
           forme matricielle)  elmts numerote en ligne, transition de colonne a ligne */
        ext->coeffs = (type_coeff **) malloc(sizeof(type_coeff *) * n * n);
        k = 0;
        for (i = prem; i < last; i++)
        {
            co = neurone[i].coeff;
            for (j = 0; j < n; j++)
            {
                if (!co)
                {
                    fprintf(stderr,
                            "bad linking in function_self_organizing_sequence %d <- %d\n",
                            i - prem, j);
                }
                ext->coeffs[k] = co;
                ext->coeffs[k]->val = 1.0 / n;
                /*res.coeffs[k].depart = j;
                   res.coeffs[k].arrivee = i;
                   if(j==n-1){
                   res.coeffs[k].s = 0;
                   }
                   else{
                   res.coeffs[k].s = (type_coeff *) &(res.coeffs[k+1]);
                   }   deja fait normalement */
                k++;
                co = co->s;
            }
        }

    }
    ext = (sos_ext *) def_groupe[gpe].ext;

    if (ext->enablegroup == -1
        || neurone[def_groupe[ext->enablegroup].premier_ele].s1 > 0.5)
    {
        /* determination de la position observee (x,y) */
        if (ext->xposgroup == -1)
        {
            x = 1;
        }
        else
        {
            for (i = def_groupe[ext->xposgroup].premier_ele;
                 i < def_groupe[ext->xposgroup].premier_ele +
                 def_groupe[ext->xposgroup].nbre; i++)
            {
	      if (isequal(neurone[i].s2, 1))
                {
                    x = i - def_groupe[ext->xposgroup].premier_ele;
                    break;
                }
            }
        }
        if (ext->yposgroup == -1)
        {
            y = 1;
        }
        else
        {
            for (i = def_groupe[ext->yposgroup].premier_ele;
                 i < def_groupe[ext->yposgroup].premier_ele +
                 def_groupe[ext->yposgroup].nbre; i++)
            {
	      if (isequal(neurone[i].s2, 1))
                {
                    y = i - def_groupe[ext->yposgroup].premier_ele;
                    break;
                }
            }
        }
        /* mise a jour de l'actvite */
        tot = 0;
        max = -1;
        maxi = -1;
        for (i = prem; i < last; i++)
        {
            neurone[i].s = neurone[i].s1;
        }
        for (i = prem; i < last; i++)
        {
            co = neurone[i].coeff;
            sum = 0;
            while (co)
            {
                sum += co->val * neurone[co->entree].s;
                co = co->s;
            }
            neurone[i].s1 =
                sum *
                exp(-
                    ((x - neurone[i].posx) * (x -
                                              neurone[i].posx) / ext->xvar +
                     (y - neurone[i].posy) * (y -
                                              neurone[i].posy) / ext->yvar));
            /* normalisation a la fin */

            tot += neurone[i].s1;
            if (neurone[i].s1 > max)
            {
                max = neurone[i].s1;
                maxi = i;
            }
        }
        if (!(tot > MIN_VAL && tot < 1))
        {                       /* on ne tient compte que du present */
            tot = 0;
            for (i = prem; i < last; i++)
            {
                neurone[i].s1 =
                    exp(-
                        ((x - neurone[i].posx) * (x -
                                                  neurone[i].posx) /
                         ext->xvar + (y - neurone[i].posy) * (y -
                                                              neurone[i].
                                                              posy) /
                         ext->yvar));
                tot += neurone[i].s1;
                if (neurone[i].s1 > max)
                {
                    max = neurone[i].s1;
                    maxi = i;
                }
            }
        }

        /* normalisation */
        for (i = prem; i < last; i++)
        {
            neurone[i].s1 /= tot;
            neurone[i].s2 = 0;
        }
        neurone[maxi].s2 = 1;

        if (global_learn)
        {                       /* apprentissage */
            /* swapping */
            temp = ext->last_tab;
            ext->last_tab = ext->tab;
            ext->tab = temp;

            /* alias */
            llast_tab = ext->last_tab;
            ttab = ext->tab;

            /* tri des neurones par activation s1 (tri non optimal) */
            for (i = 0; i < n - 1; i++)
            {
                max = neurone[ttab[i] + prem].s1;
                maxj = i;
                for (j = i + 1; j < n; j++)
                {
                    if (neurone[ttab[j] + prem].s1 > max)
                    {
                        max = neurone[ttab[j] + prem].s1;
                        maxj = j;
                    }
                }
                tmp = ttab[i];
                ttab[i] = ttab[maxj];
                ttab[maxj] = tmp;
            }
            /* mise a jour des centroides, on rapproche les 2 + probables */
            for (i = 0; i < 2; i++)
            {
                neurone[ttab[i] + prem].posx +=
                    def_groupe[gpe].learning_rate * (x -
                                                     neurone[ttab[i] +
                                                             prem].posx) *
                    (neurone[ttab[i] + prem].s1);
                neurone[ttab[i] + prem].posy +=
                    def_groupe[gpe].learning_rate * (y -
                                                     neurone[ttab[i] +
                                                             prem].posy) *
                    neurone[ttab[i] + prem].s1;
            }
            /* mise a jour des liens */
            /* lien le plus probable */


            val = ext->coeffs[n * ttab[0] + llast_tab[0]]->val;
            /* ampleur de la modif */
            if (ttab[0] == llast_tab[0])
            {
                delta = 0.4 * def_groupe[gpe].learning_rate * neurone[ttab[0] + prem].s1 * neurone[llast_tab[0] + prem].s * (1 - val);  /* 0.4 ad hoc, il faudrait avoir deux learnging_rate un pour les pos et un pour les poids */
            }
            else
            {
                delta =
                    def_groupe[gpe].learning_rate * neurone[ttab[0] +
                                                            prem].s1 *
                    neurone[llast_tab[0] + prem].s * (1 - val);
            }
            /* modif du lien le plus probable */
            if (isdiff(delta, 0.) && isdiff(val, 1.))
            {
                ext->coeffs[n * ttab[0] + llast_tab[0]]->val += delta;

                /* normalisation de la ligne */

                ttemp = delta / (1 - val);
                for (i = 1; i < n; i++)
                {
                    k = n * ttab[0] + llast_tab[i];
                    ext->coeffs[k]->val -= ttemp * ext->coeffs[k]->val;

                    if (ext->coeffs[k]->val < MIN_WEIGHT / n)
                    {
                        ext->coeffs[k]->val = MIN_WEIGHT / n;
                    }
                }


                /*normalisation de la colonne */
                for (i = 1; i < n; i++)
                {
                    k = n * ttab[i] + llast_tab[0];
                    ext->coeffs[k]->val -= ttemp * ext->coeffs[k]->val;
                    if (ext->coeffs[k]->val < MIN_WEIGHT / n)
                    {
                        ext->coeffs[k]->val = MIN_WEIGHT / n;   /* pas de poids null */
                    }
                }

                ttemp = delta / ((1 - val) * (1 - val));
                /* normalisation des autres elemts */
                for (i = 1; i < n; i++)
                {
                    for (j = 1; j < n; j++)
                    {
                        co = ext->coeffs[n * ttab[i] + llast_tab[j]];
                        co->val +=
                            ttemp * ext->coeffs[n * ttab[0] +
                                                llast_tab[j]]->val *
                            ext->coeffs[n * ttab[i] + llast_tab[0]]->val;
                    }
                }
                /* impression des poids */
#ifdef DEBUG
                k = 0;
                printf("sos-weights\n");
                for (j = 0; j < n; j++)
                {
                    for (i = 0; i < n; i++)
                    {
                        printf("%f ", ext->coeffs[k]->val);
                        k++;
                    }
                    printf("\n");
                }
#endif
            }
        }
    }
    else
    {                           /* pas de perpetuation de l'activite pour laisser lap lace aux prediction */
        for (i = prem; i < last; i++)
        {
            neurone[i].s2 = 0;
        }
    }
}
