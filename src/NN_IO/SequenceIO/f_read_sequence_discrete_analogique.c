/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_read_sequence_discrete_analogique.c
\brief

Author: 
Created: 
Modified:
- author: 
- description: 
- date: 

Description:
 
Liens:
  
 
  Variables utilisees:
 
  Comment:

Macro:

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:

Links:

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <sys/time.h>

typedef	struct		s_read_sequence_discrete_analogique
{
  FILE			*f;
  int			pas;
  int			first;
  int			cycle;
}			t_read_sequence_discrete_analogique;

void new_read_sequence_discrete_analogique(int gpe)
{
  t_read_sequence_discrete_analogique	*data = NULL;
  int					l = -1;
  char					fname[256];

  if (def_groupe[gpe].data == NULL)
    {
      if ((data = malloc(sizeof(t_read_sequence_discrete_analogique))) == NULL)
	{
	  perror("new_read_sequence_discrete_analogique : malloc");
	  exit(1);
	}
      data->cycle = 0;
      l = find_input_link(gpe, 0);
      if (prom_getopt(liaison[l].nom, "f", fname) == 2)
	{
	  fprintf(stdout, "fname = %s\n", fname);
	  if ((data->f = fopen(fname, "r")) == NULL)
	    {
	      perror("new_read_sequence_discrete_analogique : fopen");
	      exit(2);
	    }
	}
      if (prom_getopt(liaison[l].nom, "c", fname) == 2)
	{
	  data->cycle = 1;
	}

      data->first = def_groupe[gpe].premier_ele;
      def_groupe[gpe].data = (void *)data;
    }
}

void function_read_sequence_discrete_analogique(int gpe)
{
  t_read_sequence_discrete_analogique	*data = NULL;
  FILE					*f = NULL;
  char					sep = 0;
  int					i = 0;

  data = (t_read_sequence_discrete_analogique *)def_groupe[gpe].data;

  f = data->f;

  i = data->first;
  while (sep != '\n')
    {
      neurone[i].s1 = neurone[i].s2 = neurone[i].s = 0.000000;
      if(feof(f))
	{
	  if (data->cycle)
	    rewind(f);
	  break;
	}
      if ((fscanf(f, "%f%c", &(neurone[i].s), &sep)) != 2)
	{
	  break;
	}
      neurone[i].s1 = neurone[i].s2 = neurone[i].s;
      i++;
      if(feof(f))
	{
	  if (data->cycle)
	    rewind(f);
	  break;
	}
    }
}
