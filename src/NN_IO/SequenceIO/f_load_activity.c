/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_load_activity f_load_activity
 \ingroup libNN_IO

 Description:
 Reads sequentially the activities given in an input file. Loops back to the beginning when the end of the file is reached
 An optionnal input link can specify a group whose activity controls the reading sequence.

 Links:
 - read_next: when present, the link specifies a group whose activity controls the reading sequence
 (ie: when s1 > 0.5, the next activities are read)

 \file
 \ingroup f_load_activity
 \brief

 Author: J. Hirel
 Created: 18/02/2008
 Modified:
 - author: Nils Beaussé
 - description: Ajout de différentes options, reecriture et documentation
 - date: 2015


 Description:
 Reads sequentially the activities given in an input file. Loops back to the beginning when the end of the file is reached
 An optionnal input link can specify a group whose activity controls the reading sequence.
 Liens :
 -tabs : rejette la premiere valeur de chaque ligne
 -random : va chercher de façon aleatoire une ligne dans le fichier
 -exclude_circle :permet d'exclure les lignes explorées jusqu'a que toute les lignes soit explorées. (non compatible avec interpolate, compatible avec random)
 -interpolate : va chercher une seconde ligne dans le fichier pour creer une valeur comprise entre la premiere ligne et la seconde ligne. Le facteur d'interpolation étant lui meme aleatoire. Ideal pour definir des bornes en plusieurs dimensions à explorer.
 -fXXX : nom du fichier à charger
 -select_next: permet de definir le no de ligne (groupe d'entree) à lire dans le fichier
 "read_next" : permet de definir un groupe d'entrée commandant la lecture de la prochaine ligne.

 External Tools:
 -Kernel_Function/find_input_link()
 -Kernel_Function/prom_getopt()

 Links:
 - read_next: when present, the link specifies a group whose activity controls the reading sequence
 (ie: when s1 > 0.5, the next activities are read)

 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <basic_tools.h>

typedef struct mydata_load_activity {
  int gpe_next;
  FILE *fdd;
  int tabs;
  int random;
  int nb_lignes;
  int interpolate;
  int exclude;
  int* exclude_tab;
  int nbre_exclus;
  int entree_choix;
} mydata_load_activity;

void init_exclude_tab(int* exclude_tab, int taille)
{
  int i;
  for (i = 0; i < taille; i++)
  {
    exclude_tab[i] = 0;
  }
}
void new_load_activity(int gpe)
{
  char file_name[256] = "\0";
  char param[256];
  FILE *fdd = NULL;
  mydata_load_activity *my_data = NULL;
  int i, l, interpolate = 0;
  int gpe_next = -1;
  int tabs = 0;
  int random = 0;
  int exclude = 0;
  char buffer[100];
  int nb_lignes = 0;
  char* temp = NULL;
  int* exclude_tab = NULL;
  int nbre_exclus = 0,byte_now=0;
  char * pointeur;
  float temporary_float;
  int entree_choix = -1,read=-1,nbre_valeur=0;

  if (def_groupe[gpe].data == NULL)
  {
    i = 0;
    l = find_input_link(gpe, i);
    while (l != -1)
    {
      if (prom_getopt(liaison[l].nom, "tabs", param) > 0)
      {
        tabs = 1;
      }
      if (prom_getopt(liaison[l].nom, "random", param) > 0)
      {
        random = 1;
      }
      if (prom_getopt(liaison[l].nom, "exclude_circle", param) > 0)
      {
        exclude = 1;
      }
      if (prom_getopt(liaison[l].nom, "interpolate", param) > 0)
      {
        interpolate = 1;
      }
      if (prom_getopt(liaison[l].nom, "f", param) == 2)
      {
        sprintf(file_name, "%s", param);
      }
      if (prom_getopt(liaison[l].nom, "select_next", param) > 0)
      {
        entree_choix = def_groupe[liaison[l].depart].premier_ele;
      }
      if (strcmp(liaison[l].nom, "read_next") == 0) gpe_next = liaison[l].depart;
      i++;
      l = find_input_link(gpe, i);
    }

    if (strlen(file_name) == 0)
    {
      EXIT_ON_ERROR("ERROR in new_load_activity(%s): No filename parameter (-f) was given\n", def_groupe[gpe].no_name);
    }

    fdd = fopen(file_name, "r");
    if (fdd == NULL)
    {
      EXIT_ON_ERROR("ERROR in new_load_activity(%s): Cannot open file %s\n", def_groupe[gpe].no_name, file_name);
    }

    my_data = ALLOCATION(mydata_load_activity);
    if (my_data == NULL)
    {
      EXIT_ON_ERROR("ERROR in new_load_activity(%s): malloc failed for data\n", def_groupe[gpe].no_name);
    }

    my_data->fdd = fdd;
    my_data->exclude = exclude;
    my_data->gpe_next = gpe_next;
    my_data->tabs = tabs;
    my_data->random = random;
    my_data->interpolate = interpolate;
    my_data->nbre_exclus = nbre_exclus;
    my_data->entree_choix = entree_choix;

    printf("exclude = %d, nbre_exclus=%d\n", exclude, nbre_exclus);

    if (exclude && interpolate) EXIT_ON_ERROR("Exclude n'est pas compatible avec interpolate dans la fonction f_load_activity\n");

    temp = fgets(buffer, 100, fdd);
    if (temp==NULL) EXIT_ON_ERROR("erreur de lecture du fichier d'entree dans f_load_activity\n");

    nbre_valeur=0;
    pointeur=buffer;
    do
    {
      read = sscanf(pointeur, "%f%n", &temporary_float, &byte_now);
      pointeur+=byte_now;
      if (read==1) nbre_valeur++;

    }while(read==1);

    if(nbre_valeur != def_groupe[gpe].taillex) PRINT_WARNING("ATTENTION : La taillex de votre groupe n'est pas cohérente avec la taille des donnees de la premiere ligne du fichier (fichier = %d groupe = %d)\n",nbre_valeur,def_groupe[gpe].taillex);

    // Decompte des lignes du fichier fournis en parametre. 100 est une borne purement ad hoc
    if (random == 1 || entree_choix != -1)
    {
      rewind(fdd);
      do
      {
        temp = fgets(buffer, 100, fdd);
        nb_lignes++;
        if (temp == NULL) PRINT_WARNING("compte des lignes du fichier : %d, verifiez que c'est bon \n", (nb_lignes - 1));
      } while (!feof(fdd) && temp != NULL);
    }
    else
    {
      nb_lignes = 0;
    }

    my_data->nb_lignes = nb_lignes - 1;

    if (my_data->exclude == 1)
    {
      exclude_tab = MANY_ALLOCATIONS(my_data->nb_lignes, int);
      init_exclude_tab(exclude_tab, my_data->nb_lignes);
    }
    my_data->exclude_tab = exclude_tab;
    rewind(fdd);
    def_groupe[gpe].data = (mydata_load_activity *) my_data;

  }
}

void function_load_activity(int gpe)
{
  int i, j, read, nb_lignes, nbre_alea1 = 0, nbre_alea2 = 0, nbre_exlus, exclude, k = 0, no_ligne_utile;
  int gpe_next, random;
  float input;
  double nbre_alea3 = 0.0;
  FILE *fdd = NULL;
  mydata_load_activity *my_data = NULL;
  char buffer[100];
  char* hop = NULL;
  int* exclude_tab = NULL;
  int entree_choix = -1;

  my_data = (mydata_load_activity *) def_groupe[gpe].data;
  if (my_data == NULL)
  {
    EXIT_ON_ERROR("ERROR in f_load_activity(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
  }

  gpe_next = my_data->gpe_next;
  fdd = my_data->fdd;
  random = my_data->random;
  nb_lignes = my_data->nb_lignes;
  nbre_exlus = my_data->nbre_exclus;
  exclude_tab = my_data->exclude_tab;
  exclude = my_data->exclude;
  entree_choix = my_data->entree_choix;

  if (feof(fdd))
  {
    rewind(fdd);
  }

  if (gpe_next == -1 || neurone[def_groupe[gpe_next].premier_ele].s1 > 0.5)
  {

    if (my_data->nbre_exclus == my_data->nb_lignes)
    {
      init_exclude_tab(exclude_tab, nb_lignes);
      my_data->nbre_exclus = 0;
      nbre_exlus = my_data->nbre_exclus;
    }

    if (my_data->tabs)
    {
      /** reject first val if tabs */
      read = fscanf(fdd, "%f", &input);
      if (read != 1)
      EXIT_ON_ERROR("ERROR in f_load_activity(%s): Activity reading failed\n", def_groupe[gpe].no_name);
    }

    // comportement standart : pas d'entree et pas de comportement random : on lis la ligne en cours simplement
    if (random == 0 && entree_choix == -1)
    {
      for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
      {
        read = fscanf(fdd, "%f", &input);
        if (read == 1) neurone[i].s2 = neurone[i].s1 = neurone[i].s = input;
        else
        {
          EXIT_ON_ERROR("ERROR in f_load_activity(%s): Activity reading failed, 1\n", def_groupe[gpe].no_name);
        }
      }
      read = fscanf(fdd, "\n");
    }
    else
    { //Autres cas :

      rewind(fdd); // car tout les autres modes ne necessite pas
      // Si il y a un random on cherche le premier nombre aleatoire, sinon on prends celle de l'entree imposée
      if (entree_choix != -1) nbre_alea1 = (int) (neurone[entree_choix].s1);
      if (random == 1)
      {
        nbre_alea1 =rand() % (nb_lignes - nbre_exlus);
        // Si il y a l'option interpolate (compatible avec random uniquement) on va chercher un second nombre aleatoire
        if (my_data->interpolate == 1)
        {
          do
          {
            nbre_alea2 = rand() % (nb_lignes);
          } while ((nbre_alea2 == nbre_alea1) && (nb_lignes != 1));
        }
      }
      k = -1;
      //on va à la ligne correspondant au premier
      no_ligne_utile = 0;

      // On va à la ligne considéré
      do
      {
        k++;
        no_ligne_utile++;
        if (exclude == 1)
        {
          if (exclude_tab[k] == 1) no_ligne_utile--;
        }

        if (no_ligne_utile != nbre_alea1 + 1)
        {
          hop = fgets(buffer, 100, fdd);
          if (hop == NULL) EXIT_ON_ERROR("Erreur dans le compte des lignes de f_load_activity : 1");
        }
      } while (no_ligne_utile != (nbre_alea1 + 1));


//on vire la ligne déja utilisé
    if (exclude == 1)
    {
      exclude_tab[k] = 1;
      (my_data->nbre_exclus)++;
    }

    // on stoque les valeurs dans d
    for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
    {
      read = fscanf(fdd, "%f", &input);
      //  printf("input = %f \n",input);
      if (read == 1)
      {
        neurone[i].d = input;
      }
      else
      {
        EXIT_ON_ERROR("ERROR in f_load_activity(%s): Activity reading failed, 2, nbre_alea1=%d: \n", def_groupe[gpe].no_name, nbre_alea1);
      }
    }

    rewind(fdd); // on repart au début et on va à la ligne nbre_alea2

    if (my_data->interpolate == 1)
    {
      for (j = 0; j < nbre_alea2; j++)
      {
        hop = fgets(buffer, 100, fdd);
        if (hop == NULL) EXIT_ON_ERROR("Erreur dans le compte des lignes de f_load_activity : 2");
      }
    }
    nbre_alea3 = drand48();

    if (my_data->interpolate == 1)
    {
      for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
      {
        read = fscanf(fdd, "%f", &input);
        if (read == 1) neurone[i].s1 = neurone[i].s = neurone[i].s2 = ((neurone[i].d - input) * ((float) nbre_alea3)) + input;
        else
        {
          EXIT_ON_ERROR("ERROR in f_load_activity(%s): Activity reading failed, 3,  nbre_alea2=%d:\n", def_groupe[gpe].no_name, nbre_alea2);
        }
      }
    }
    else
    {
      for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
      {
        neurone[i].s1 = neurone[i].s = neurone[i].s2 = neurone[i].d;
      }
    }

  }
  }

}

void destroy_load_activity(int gpe)
{
  if (def_groupe[gpe].data != NULL)
  {
    if (((mydata_load_activity*) def_groupe[gpe].data)->exclude_tab != NULL) free(((mydata_load_activity*) def_groupe[gpe].data)->exclude_tab);

    fclose(((mydata_load_activity*) def_groupe[gpe].data)->fdd);

    free(((mydata_load_activity*) def_groupe[gpe].data));
    def_groupe[gpe].data = NULL;
  }
  dprints("destroy_load_activity(%s): Leaving function\n", def_groupe[gpe].no_name);
}
