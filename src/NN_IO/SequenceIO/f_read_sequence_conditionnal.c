/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_read_sequence_conditionnal.c
\brief

Author: Micha Hersch
Created: 28/01/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Modified:

Description:


Liens:


Macro:

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:

Links:

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <sys/time.h>

#define	MAX_PATH	256

typedef struct	s_read_sequence_conditionnal
{
   int	prev_gpe;
   int	idx;
   int	clean_gpe;
}	t_read_sequence_conditionnal;

void function_read_sequence_conditionnal(int gpe)
{
   t_read_sequence_conditionnal	*data = NULL;
   int				lien = 0;
   int				i = 0;
   int				premier_ele;
   int				prev_premier_ele;
   int				prev_nbre;
   int				potentiel = 0.0;
   int				clean_gpe = -1;

   if (def_groupe[gpe].data == NULL)
   {
      if ((data = malloc(sizeof(t_read_sequence_conditionnal))) == NULL)
      {
         EXIT_ON_ERROR("function_read_sequence_conditionnal : malloc probleme d'allocation memoire");
      }

      data->clean_gpe = -1;
      data->prev_gpe = -1;
      while ((lien = find_input_link(gpe, i++)) != -1)
      {
         if (!strcmp(liaison[lien].nom, "reset"))  data->clean_gpe = liaison[lien].depart;
         else
         {
            data->prev_gpe = liaison[lien].depart;
         }
      }
      data->idx = 0;
      def_groupe[gpe].data = (void *)data;
   }
   else data = (t_read_sequence_conditionnal *)def_groupe[gpe].data;

   clean_gpe = data->clean_gpe;
   premier_ele = def_groupe[gpe].premier_ele;
   prev_premier_ele = def_groupe[data->prev_gpe].premier_ele;
   prev_nbre = def_groupe[data->prev_gpe].nbre;

   if (data->idx > 0)
   {
      neurone[premier_ele + (data->idx - 1)].s = 0;
      neurone[premier_ele + (data->idx - 1)].s1 = 0;
      neurone[premier_ele + (data->idx - 1 )].s2 = 0;
   }

   for (i = 0; i < prev_nbre; i++)
   {
      potentiel += neurone[prev_premier_ele + i].s2;
   }

   if ((clean_gpe != -1) && ( isdiff(neurone[def_groupe[clean_gpe].premier_ele].s2, 0.0)))
   {
      data->idx = 0;
   }

   if ((potentiel < -0.000001) || (potentiel > 0.000001))
   {
      neurone[premier_ele + data->idx].s = 1.000000;
      neurone[premier_ele + data->idx].s1 = 1.000000;
      neurone[premier_ele + data->idx].s2 = 1.000000;
      data->idx++;
   }
}
