/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file 
\brief

Author: Mathias Quoy
Created: 24/06/2005


Description:
  Evite l'usine a gaz de f_read_sequence pour lire un element d'une sequence a t, l'element suivant a t+1 etc jusqu'au dernier element, puis bouclage.
La sequence est dans un fichier comprenant les numeros des neurones a activer a 1.

Liens:
  
  Ce groupe comprend 1 lien qui correspond au nom du fichier a lire (le type du groupe precedant est 
  indifferent) :
  -f(fichier) nom du fichier ou est lue la sequence (format: "float int \n"
  pour chaque element de la sequence)
 
  Par defaut le fichier lu est ./files/sequences/seq(xx) ou (xx) est
  le 1e chiffre indique dans le fichier ./files/seqrange. seqrange contient
  deux int separes par un espace, l'un indiquant le no du fichier a lire et l'autre
  indiquant le no max. Lorsque Cette fonction ouvre un fichier en lecture, elle incremente
  le premier int de seqrange et ainsi lors de la prochaine utilisation et lira le prochain
  fichier. Ce no n'est pas incremente au dela du second numero du fichier seqrange. Ceci est utile
  pour lire dans plusieurs fichiers sans avoir a changer le nom de lien
  a chaque fois.
 
  Variables utilisees:

 
  Comment:

  - La boite doit contenir plus de neurones que le plus grand
  element (int) de la sequence lue (pour pouvoir tous les exprimer)

Macro:
-ILENAME_SIZE
-FRS_NBFILES
-gettime
-fullfname
-fno

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: 2 input link, with -fsequin (in order to read the sequin file) and -Timeref for clock ticks reference.
- description: none/ XXX
- input expected group:  Time_generator
- where are the data?: on the link's names

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


void function_read_sequence_discrete(int gpe)
{
    char fullfname[FILENAME_SIZE];  /* nom de fichier avec no */
    int nbread;

    int val, i, deb, longueur, foundfile, lien;
    read_sequence_t *dge;


    deb = def_groupe[gpe].premier_ele;

    longueur = def_groupe[gpe].nbre;



    dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */
    if (!dge)
    {
    /** 1e iteration de la simulation, initialisation des variables **/
        def_groupe[gpe].ext = (void *) malloc(sizeof(read_sequence_t)); /* libere dans function_end_read_sequence */
        dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */
        /* recherche du nom du fichier de la sequence sur le lien */
        i = 0;
        foundfile = 0;
        while ((lien = find_input_link(gpe, i++)) != -1)
        {
            if (prom_getopt(liaison[lien].nom, "f", fullfname))
            {
                foundfile = 1;
                break;
            }
        }

        if (!foundfile)
        {
            fprintf(stderr, "Impossible d'ouvrir le fichier %s\n", fullfname);
            return;
        }

        /* le fichier existe */

        /* ouverture, lecture du fichier contenant la sequence */
        if (!(dge->fp = fopen(fullfname, "r")))
        {
            fprintf(stderr, "Impossible d'ouvrir le fichier %s\n", fullfname);
        }
    }


    /* dge existe */

    nbread = fscanf(dge->fp, "%d\n", &val);
    if (nbread == EOF)
    {
        rewind(dge->fp);
        nbread = fscanf(dge->fp, "%d\n", &val);
        if (nbread == 1)
        {
            dge->val = val;
        }
        else
            fprintf(stderr,
                    "Impossible de lire le fichier %s, mauvais format\n",
                    fullfname);
    }
    else if (nbread == 1)
    {
        dge->val = val;
    }
    else
        fprintf(stderr, "Impossible de lire le fichier %s, mauvais format\n",
                fullfname);

    if (dge->val < longueur)
    {
        /*printf("num neurone %d\n",dge->val); */
        for (i = deb; i < deb + longueur; i++)
        {
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
        }

        if (dge->val != -1)
            neurone[deb + dge->val].s = neurone[deb + dge->val].s1 =
                neurone[deb + dge->val].s2 = 1.0;

    }
    else
        fprintf(stderr, "Neurone %d inexistant\n", dge->val);
}
