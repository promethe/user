/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>



typedef struct read_pos_t
{
    int sosgpe;                 /* le gpe ou lire posx ou posy */
    int stategpe;               /*le gpe indiquant le neurone ou chercher */
    int toread;
} read_pos_t;


/** *******************************************************************************
 *
 * function_read_pos
 *
 *
 * Description:
 * lit les valeurs posx ou posy du groupe precedent et active les neurones
 * correspondant a la valeur du neurone d'etat. Permet la reconversion apres function_self_organizing_sequences
 * dans l'espace des observations
 *
 * Liens:
 * Il y a au plus 3 lien entrants
 * -hash: l'origine de ce lien est le groupe ou lire les valeurs posx et posy (le groupe selforganizing sequence)
 * -x : il faut lire la valeur de posx des neurones actifs a l'origine du lien ainsi marque
 * -y : il faut lire la valeur de posy des neurones actifs a l'origine du lien ainsi marque
 * Les options -x et -y sont mutuellement exclusives
 * 
 * Entree:
 * Le groupe en entree du lien marque -x ou -y doit avoir au plus autant de neurone que le groupe a l'entree du lien marque -hash
 * Les valeurs lues sur les liens -x et -y sont s1.
 *
 * Sortie:
 *  La sortie s=s1=s2 est l'activation des neurones coorespondant aux etat active du groupe precedent
 * 
 * Comment:
 * Cette fonction deroge au pricnipe de plausibilite biologique. La reconversion pourrait etre l'objet d'un apprentissage
 *********************************************************************************/


void function_read_pos(int gpe)
{
    int lien, i, deb, nbre, pos;
    float act;
    char val[50];
    read_pos_t *ext;
    if (!def_groupe[gpe].ext)
    {                           /* initialisation */
        def_groupe[gpe].ext = malloc(sizeof(read_pos_t));
        ext = (read_pos_t *) def_groupe[gpe].ext;
        ext->toread = 0;
        ext->sosgpe = -1;
        ext->stategpe = -1;
        for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
        {
            if (prom_getopt(liaison[lien].nom, "x", val))
            {
                ext->stategpe = liaison[lien].depart;
                ext->toread += 1;
            }
            if (prom_getopt(liaison[lien].nom, "y", val))
            {
                ext->stategpe = liaison[lien].depart;
                ext->toread += 2;
            }
            if (prom_getopt(liaison[lien].nom, "hash", val))
            {
                ext->sosgpe = liaison[lien].depart;
            }
        }
        if (ext->toread == 0 || ext->sosgpe == -1 || ext->stategpe == -1)
        {
            fprintf(stderr, "missing arguments on function_read_pos links\n");
        }
        if (ext->toread == 3)
        {
            fprintf(stderr,
                    "cannot read both posx and poxy values in function_read_pos\n");
        }
        if (def_groupe[ext->stategpe].nbre > def_groupe[ext->sosgpe].nbre)
        {
            fprintf(stderr,
                    "function_read_pos (%d): incompatible preceding group sizes\n",
                    gpe);
        }
    }
    else
    {
        ext = (read_pos_t *) def_groupe[gpe].ext;
    }
    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
    for (i = deb; i < deb + nbre; i++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
    }
    if (ext->toread == 1)
    {
        for (i = 0; i < def_groupe[ext->stategpe].nbre; i++)
        {
            act = neurone[def_groupe[ext->stategpe].premier_ele + i].s1;
            pos = (int) neurone[def_groupe[ext->sosgpe].premier_ele + i].posx;
            if (pos >= 0 && pos < nbre)
            {
                neurone[deb + pos].s = neurone[deb + pos].s1 =
                    neurone[deb + pos].s2 = act;
            }
            else
            {
                fprintf(stderr,
                        "function_read_pos (%d): read pos value (%d) incompatible with group size (%d)\n",
                        gpe, pos, nbre);
            }
        }
    }
    else if (ext->toread == 2)
    {
        for (i = 0; i < def_groupe[ext->stategpe].nbre; i++)
        {
            act = neurone[def_groupe[ext->stategpe].premier_ele + i].s1;
            pos = (int) neurone[def_groupe[ext->sosgpe].premier_ele + i].posy;
            if (pos >= 0 && pos < nbre)
            {
                neurone[deb + pos].s = neurone[deb + pos].s1 =
                    neurone[deb + pos].s2 = act;
            }
            else
            {
                fprintf(stderr,
                        "function_read_pos (%d): read pos value (%d) incompatible with group size (%d)\n",
                        gpe, pos, nbre);
            }
        }
    }
}
