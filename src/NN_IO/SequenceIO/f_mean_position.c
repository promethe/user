/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define MAX_FRAMES 100
/*#define MEASURE_TRAJ*/


typedef struct mean_position_t
{
    int n;
    float last_pos[MAX_FRAMES];
    float tot;
    int prec;
    int cursor;
    int nb;
#ifdef MEASURE_TRAJ
    char c;                     /* v: postion verticale, h: horizontale */
#endif
} mean_position_t;



/**********************************************************************
 *
 * function_mean_position  
 *
 * 
 * author:  micha hersch 
 * created: mai 2004
 *
 * Description theorique: Cette fonction calcule la moyenne spatiale de
 * l'activation des neurones sur les <n> derniers cycles. Si le groupe
 * entree represente une trajectoire, cette trajectoire est spatialement
 * lissee. Plus <n> est grand, plus le lissage est important.
 *
 * 
 * Liens:
 * Entree: le groupe (d'habitide un wta) indiquant la position courante. 
 * Il n'y a qu'un seul lien entrant; Si le groupe n'est pas un wta (plusieurs
 * neurones sont actifs) c'est le lieu de la moyenne ponderee de l'activite
 * qui est pris en compte, c'est a dire
 * \f$ \frac{\sum_{i}i\cdot Act(i)}{\sum_{i}Act(i)} \f$
 * ou i indice les neurones et Act(i) represente l'activite (.s2 dans notre cas)
 *
 *  Option: -n<n> est donne sur le lien. <n> indique le nombre de  positions
 * prises dans la moyenne. Vaut 2 par defaut.
 *
 * Sorties: sortie binarisee sur s1=s2. Au plus un neurone actif a la fois
 * en cas d'inactivite en entree pas de sortie.
 *  
 * Comments: 
 *  - Cette fonction alloue de la memoire en ext, et qui doit etre liberee
 *  - Le groupe d'entree ne peut etre plus grand que ce groupe
 *  - Pas d'output lors des <n>-1  premiers cycles 
 *


 ***********************************************************************/

#include <Kernel_Function/prom_getopt.h>

#include <Kernel_Function/find_input_link.h>

void function_mean_position(int gpe)
{
    char val[256];
    int act, premprec, i, lien, meanpos, prem;
    float sum, w, sumw;
    mean_position_t *rec;
    /*   void *ext; */
    if (!def_groupe[gpe].ext)
    {                           /* initialisation de la structure */
        def_groupe[gpe].ext = malloc(sizeof(mean_position_t));
        rec = (mean_position_t *) def_groupe[gpe].ext;
        if ((lien = find_input_link(gpe, 0)) != -1)
        {
            if (prom_getopt(liaison[lien].nom, "n", val))
            {
                rec->n = atoi(val);
            }
            else
            {
                rec->n = 2;     /* valeur par defaut */
            }
#ifdef MEASURE_TRAJ
            if (prom_getopt(liaison[lien].nom, "p", val))
            {
                rec->c = val[0];
            }
#endif
            rec->tot = 0;
            rec->cursor = 0;
            rec->nb = 1;
            rec->prec = liaison[lien].depart;
            for (i = 0; i < MAX_FRAMES; i++)
            {
                rec->last_pos[i] = 0.0;
            }
            if (def_groupe[rec->prec].nbre > def_groupe[gpe].nbre)
            {
                fprintf(stderr,
                        " ERROR, entry group of function_mean_position cannot be larger than function_mean_position\n");
            }
        }
        else
        {
            fprintf(stderr,
                    "ERROR, cannot find input to function_mean_position\n");
        }
    }
    else
    {
        rec = (mean_position_t *) def_groupe[gpe].ext;
    }

    premprec = def_groupe[rec->prec].premier_ele;

    act = 0;
    sumw = 0;
    sum = 0;
    /* recherche du lieu moyen de l'activite au temps t */
    for (i = 0; i < def_groupe[rec->prec].nbre; i++)
    {
        w = neurone[premprec + i].s2;
        if (w > 0)
        {
            act = 1;
        }
        sum += (i + 1) * w;     /* i+1 pour differencier l'act en i = 0, de l'act nulle */
        sumw += w;
    }

    if (act)
    {                           /* activite dans le groupe d'entree, on decale la fenetre */
        rec->tot -= rec->last_pos[rec->cursor];
        rec->last_pos[rec->cursor] = sum / sumw;    /* lieu moyen de l'act */
#ifdef MEASURE_TRAJ
        printf("mvnt in point %c: %f\n", rec->c, rec->last_pos[rec->cursor]);
#endif

        rec->tot += rec->last_pos[rec->cursor];
        rec->cursor = (rec->cursor + 1) % (rec->n);
        prem = def_groupe[gpe].premier_ele;
        for (i = 0; i < def_groupe[gpe].nbre; i++)
        {
            neurone[prem + i].s2 = neurone[prem + i].s1 = 0;
        }
        if (rec->nb < rec->n)
        {
            rec->nb++;
        }
        else
        {
            meanpos = (int) ((rec->tot + 0.5) / rec->n);    /* + 0.5 pour un arrondi */
            if (meanpos >= 1)
            {
                neurone[prem + meanpos - 1].s2 =
                    neurone[prem + meanpos - 1].s1 = 1;
            }
        }
    }
    else
    {                           /* pas d'activite dans le groupe d'entree, sortie nulle, mais on conserve les position precedentes */
        for (i = 0; i < def_groupe[gpe].nbre; i++)
        {
            neurone[def_groupe[gpe].premier_ele + i].s2 =
                neurone[def_groupe[gpe].premier_ele + i].s1 =
                neurone[def_groupe[gpe].premier_ele + i].s = 0;
        }
    }
}
