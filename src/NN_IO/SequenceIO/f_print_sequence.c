/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***************************************************************
defgroup f_print_sequence f_print_sequence
ingroup libNN_IO

 This function prints a sequence from the previous box.

 To be completed

\file 
\brief

Author: Mischa Hersch
Created: 01/02/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  This function prints a sequence from the previous box. It prints
 * the activation (of s1) time intervals (in seconds) of each neuron
 * of the previous box. Time starts at the first call of this function
 * If the output file already exists, the output is appended to the file
Liens:
 * Ce groupe comprend 2 liens, un intitule 'timeref' qui la relie au groupe f_timeGenerator
 * Ce grope lui donne acces a la base de temps du systeme (variable SystemTime dans hippo.c, referencee
 * dans le ext du time genrator).
 * Le second lien indique le nom du ficher a ecrire. Le groupe predecesseur est celui dont la sequence doit etre ecrite
 * et doit etre different du  f_timeGeenratore
 *
Options: (sur ce second lien)
 * -f(fichier) filename to be written
 * -l(l) is 1 if this function is called during learning, 0 otherwise
 * -u(u) is 1 if this function is called during use, 0 otherwise
 * -t(t) is 1 when the same elemt is not printed twice consecutively (if
 * a neurons fires twice in a row, it is only recorded once)
 * by default, (u) and (t) are set to 1, (l) is set to 0.

Macro:
-FILENAME_SIZE
-FRS_NBFILES
-gettime
-fullfname
-fno
-min

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>

#include "tools/include/macro.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

void function_print_sequence(int gpe)
{
    int deb, prec, premprec, lprec, elmt, lien, i, u, l, t;
    int currentfile, maxfile, argsnotfound, foundfile;
    double time = 0.0;
    read_sequence_t *dge;
    char outfilename[] = "./files/seqrange";
    char filename[] = "./files/results/res";
    char fullfname[FILENAME_SIZE];  /* nom de fichier avec no */
    char fno[FRS_NBFILES];
    char val[50];
    FILE *fpout;                /* fichier indiquant le no de la sequence */
#ifdef REAL_TIME
    struct timeval tm1;
#endif

    dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */

    if (!dge)
    {                           /* initialiser la structure */
        def_groupe[gpe].ext = malloc(sizeof(read_sequence_t));  /* libere ds end_read_sequence */
        dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */
        /* mode fixing */
        l = 0;                  /* default arg values */
        u = t = 1;              /* default arg value */

        i = 0;
        foundfile = 0;
        argsnotfound = 1;
        while (argsnotfound && (lien = find_input_link(gpe, i++)) != -1)
        {
            if (strcmp(liaison[lien].nom, "timeref"))
            {
                if (prom_getopt(liaison[lien].nom, "f", fullfname))
                {
                    foundfile = 1;
                    argsnotfound = 0;
                }
                if (prom_getopt(liaison[lien].nom, "l", val))
                {
                    l = atoi(val);
                    argsnotfound = 0;
                }
                if (prom_getopt(liaison[lien].nom, "u", val))
                {
                    u = atoi(val);
                    argsnotfound = 0;
                }
                if (prom_getopt(liaison[lien].nom, "t", val))
                {
                    t = atoi(val);
                    argsnotfound = 0;
                }
            }
        }
        if (u * (1 - u) || l * (1 - l) || t * (1 - t))
        {
            fprintf(stderr,
                    "ERROR, arg values to function_print_sequence must be 0 or 1\n");
        }
        dge->mode = t + 2 * u + 4 * l;


        if ((dge->mode / 4 && global_learn)
            || ((dge->mode % 4) / 2 && !global_learn))
        {
            dge->val = -1;      /* un elmt hors de la liste */
            dge->time = time;
            if (!foundfile)
            {                   /* par defaut, ouverture, lecture du fichier indiquant ou trouver la sequence */
                if (!(fpout = fopen(outfilename, "r+")))
                    fprintf(stderr, "Impossible d'ouvrir le fichier %s\n",
                            outfilename);
                else
                {
                    if (fscanf(fpout, "%d %d", &currentfile, &maxfile) != 2)
                        fprintf(stderr, "Mauvais format fichier %s\n",
                                outfilename);
                    else
                    {
                        rewind(fpout);
                        fprintf(fpout, "%d %d\n",
                                min(currentfile + 1, maxfile), maxfile);
                        fclose(fpout);
                    }
                    strcpy(fullfname, filename);
                    sprintf(fno, "%d", currentfile);    /* incremente par read_sequence */
                    strcat(fullfname, fno);
                }
            }
            /* ouverture en ecriture du fichier ou imprimer la sequence */
            if (!(dge->fp = fopen(fullfname, "a")))
                fprintf(stderr, "Impossible d'ouvrir le fichier %s\n",
                        fullfname);

#ifndef REAL_TIME               /* recherche de la base de temps du systeme (TimeGenerator) */
            for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
            {
                if (!strcmp(liaison[lien].nom, "timeref"))
                {
                    break;
                }
            }
            if (lien == -1)
            {
                fprintf(stderr,
                        "print_sequence cannot find time reference link\n");
            }
            dge->tm = (struct timeval *) def_groupe[liaison[lien].depart].ext;
#else
            dge->tm = &tm1;
#endif
            gettime(dge->tm, time);
        }
    }
    if ((dge->mode / 4 && global_learn)
        || ((dge->mode % 4) / 2 && !global_learn))
    {
#ifdef REAL_TIME
        dge->tm = &tm1;
#endif
        for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
        {
            if (strcmp(liaison[lien].nom, "timeref"))
            {
                break;
            }
        }
        if (lien == -1)
        {
            fprintf(stderr,
                    "ERROR print_sequence cannot find box to print sequence from\n");
        }
        prec = liaison[lien].depart;
        premprec = def_groupe[prec].premier_ele;
        lprec = def_groupe[prec].nbre;
        deb = def_groupe[gpe].premier_ele;
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 0;

        for (i = premprec; i < premprec + lprec; i++)
        {                       /* examine l'elmt rememore */
            elmt = i - premprec;
            if (isequal(neurone[i].s1, 1) && (elmt != dge->val || !(dge->mode % 2)))
            {                   /* nouvel elmt  ou <t>=0 */
                printf("new.\n");
                gettime(dge->tm, time);
                fprintf(dge->fp, "%f %d\n", time - dge->time, elmt);
                dge->time = time;
                dge->val = elmt;
                neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 1;
            }
        }
    }
}
