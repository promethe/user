/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>


/*#define MEASURE_TRAJ 1 utile pour imprimer les points parcourus*/

typedef struct detect_traj_pt_t
{
    int mvdir_h;                /* direction du mvnt en horisontal */
    int lastpos_h;              /* derniere position enregistree en horizontal */
    int mvdir_v;                /* direction du mvnt en vertical */
    int lastpos_v;              /* derniere position enregistree en vertical */

} detect_traj_pt_t;



/** ***************************************************************
 * function_detect_traj_pt
 *
 * Author: Micha Hersch
 * Created: june 2004
 *
 * Description theorique: 
 * Cette fonction detecte les points interessants d'une trajectoire
 * Il s'agit des points qui seront appris par le reseau. Les points
 * releve sont ceux qui pour lesquels on observe un changement de
 * direction (dans au moins une des deux dimensions), c'est a dire
 * que la derivee passe par zero
 * 
 * Liens:
 * groupes en entrees: les projections de la trajectoire (un wta indiquant
 * la position courante). Il ne peut y a voir plus de deux groupes en entree
 * un lien marque v indiquant la projection sur y (axe vertical) et un lien
 * marque h indiquant la projection sur x (axe horizontal).
 * type: les liens sont de type algoritmique

 * Sortie: Les valeurs s1=S2 sont mise a lorsqu'un changement de direction
 * est detect 
 *
 * TODO: Permettre de gerer des trajectoires a 3 dimensions
 ****************************************************************/

void function_detect_traj_pt(int gpe)
{
    int lien, prec, i, j, precdeb, precnb, pos, deb;
    detect_traj_pt_t *rec = NULL;
    int *lastpos = NULL, *mvdir = NULL;
    if (!def_groupe[gpe].ext)
    {
        def_groupe[gpe].ext = malloc(sizeof(detect_traj_pt_t));
        rec = (detect_traj_pt_t *) def_groupe[gpe].ext;
        rec->mvdir_h = 1;
        rec->lastpos_h = 0;
        rec->mvdir_v = 1;
        rec->lastpos_v = 0;
    }
    else
    {
        rec = (detect_traj_pt_t *) def_groupe[gpe].ext;
    }
    deb = def_groupe[gpe].premier_ele;
    neurone[deb].s1 = neurone[deb].s2 = 0;
    for (j = 0; j < 2; j++)
    {                           /* pour la boite horizontale est vericale */
        pos = -1;
        if ((lien = find_input_link(gpe, j)) != -1)
        {
            prec = liaison[lien].depart;
            if (!strcmp(liaison[lien].nom, "h"))
            {
                lastpos = &(rec->lastpos_h);
                mvdir = &(rec->mvdir_h);
            }
            else
            {
                if (!strcmp(liaison[lien].nom, "v"))
                {
                    lastpos = &(rec->lastpos_v);
                    mvdir = &(rec->mvdir_v);
                }
                else
                {
                    fprintf(stderr,
                            "ERREUR, le lien sur detect_traj_pt doit etre nomme v ou h");
                }
            }
            precnb = def_groupe[prec].nbre;
            precdeb = def_groupe[prec].premier_ele;
            for (i = 0; i < precnb; i++)
            {
	      if (isequal(neurone[precdeb + i].s2, 1.))
                {
                    pos = i;
                    break;
                }
            }
            if (pos != -1)
            {
                if (((pos - (*lastpos)) * (*mvdir)) < 0)
                {               /* chgment de direction */
                    neurone[deb].s1 = neurone[deb].s2 = 1;
                    if (pos > *lastpos)
                    {
                        *mvdir = 1;
                    }
                    else
                    {
                        *mvdir = -1;
                    }
                }
                *lastpos = pos;
            }
            else
            {                   /* pas d'activite en entree */
                neurone[deb].s1 = neurone[deb].s2 = 0;
                return;
            }
        }
    }
#ifdef MEASURE_TRAJ
    if (neurone[deb].s1 == 1)
    {
        printf("record pt h: %d; v: %d\n", rec->lastpos_h, rec->lastpos_v);
    }
#endif
}
