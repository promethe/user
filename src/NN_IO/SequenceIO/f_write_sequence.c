/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file
\brief

Author: Micha Hersch
Created: 28/01/2004
Modified:
- author: LAGARDE Matthieu
- description: specific file creation
- date: 08/01/2007

Modified:

Description:
 
Liens:
 
  Variables utilisees:
 
  Comment:

Macro:

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type:
- description: none/ XXX
- input expected group:
- where are the data?:

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <Global_Var/NN_Core.h>

typedef	struct		s_write_sequence
{
  float			time;
  float			prev_time;
  FILE			*file;

  int			prevgpe;
  int			deb_prevgpe;
  int			taille_prevgpe;
}			t_write_sequence;

#define	MAX_FILE_NAME	256

void			function_write_sequence(int gpe)
{
  t_write_sequence	*data = NULL;
  FILE			*file = NULL;
  int			prevgpe = -1;
  int			deb_prevgpe = -1;
  int			taille_prevgpe = -1;
  int			lien = 0;
  int			i = 0;
  char			fullname[MAX_FILE_NAME];

  memset(fullname, 0, MAX_FILE_NAME * sizeof(char));

  if (def_groupe[gpe].ext == NULL)
    {
      if ((def_groupe[gpe].ext = malloc(sizeof(t_write_sequence))) == NULL)
	{
	  perror("function_write_sequence : malloc");
	  exit(1);
	}
      memset(def_groupe[gpe].ext, 0, sizeof(t_write_sequence));
      data = (t_write_sequence *)def_groupe[gpe].ext;

      while ((lien = find_input_link(gpe, i++)) != -1)
        {
	  if (prom_getopt(liaison[lien].nom, "f", fullname))
	    {
	      data->prevgpe = liaison[lien].depart;
	      data->deb_prevgpe = def_groupe[data->prevgpe].premier_ele;
	      data->taille_prevgpe = def_groupe[data->prevgpe].nbre;
	      break;
	    }
        }


      if ((data->file = fopen(fullname, "a+")) == NULL)
	{
	  perror("function_write_sequence : fopen");
	  exit(1);	  
	}
      file = data->file;
      data->prev_time = (SystemTime.tv_sec + (1e-6 * SystemTime.tv_usec));
    }
  else
    {
      data = (t_write_sequence *)def_groupe[gpe].ext;
      file = data->file;
      prevgpe = data->prevgpe;
      deb_prevgpe = data->deb_prevgpe;
      taille_prevgpe = data->taille_prevgpe;
    }

  data->time = (SystemTime.tv_sec + (1e-6 * SystemTime.tv_usec));
  
  for (i = 0; i < taille_prevgpe; i++)
    {
      if ((neurone[deb_prevgpe + i].s2 < 0) || (neurone[deb_prevgpe + i].s2 > 0))
	{
	  fprintf(file, "%f %i\n", data->time - data->prev_time, i);
	  fflush(file);
	  data->prev_time = data->time;
	}
    }
}
