/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file
\brief

Author: Micha Hersch
Created: 28/01/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Modified:
- author: P.Andry
- deleted a condition that made this group function in learning condition only.
- date: 21/09/2004

- author: A.Revel
- .s output added
- date: 23/09/2004

- author: A.Revel
- une entree a -1 inhibe toutes les entrees
- date: 02/08/2005

Description:
  Cette fonction lit un fichier contenant une sequence d'entiers (int)
  espaces par des intervalles de temps (float) et execute cette
  sequence : activation pendant l'interavalle de temps d'un seul neurone, le neurone 
  numero 'int'(donc deux colonnes dans le fichier quel que soit le nombre de neurone
  a activer au long de la sequence).
 
Liens:
  
  Ce groupe comprend 2 liens, un intitule 'timeref' provenant du groupe f_timeGenerator :
  acces a la base de temps du systeme (variable SystemTime dans hippo.c, referencee dans le ext du time genrator).
  
  Le second lien indique le nom du ficher a lire (le type du groupe precedant est 
  indifferent mais doit etre different du f_timeGenerator) :
  -f[fichier] nom du fichier ou est lue la sequence (format: "float int \n"
  pour chaque element de la sequence)
 
  Par defaut le fichier lu est ./files/sequences/seq[xx] ou [xx] est
  le 1e chiffre indique dans le fichier ./files/seqrange. seqrange contient
  deux int separes par un espace, l'un indiquant le no du fichier a lire et l'autre
  indiquant le no max. Lorsque Cette fonction ouvre un fichier en lecture, elle incremente
  le premier int de seqrange et ainsi lors de la prochaine utilisation et lira le prochain
  fichier. Ce no n'est pas incremente au dela du second numero du fichier seqrange. Ceci est utile
  pour lire dans plusieurs fichiers sans avoir a changer le nom de lien
  a chaque fois.
 
  Variables utilisees:
  Cette fonction utililse l'extension 'ext' du timeGenerator qui reference la variable SystemTime dans hippo.c
 
  Comment:
  - Le dernier element de la sequence n'est pas lu. Il n'est la que pour
  indiquer le temps de fin d'activation du groupe.
  - La boite doit contenir plus de neurones que le plus grand
  element (int) de la sequence lue (pour pouvoir tous les exprimer)

Macro:
-ILENAME_SIZE
-FRS_NBFILES
-gettime
-fullfname
-fno

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: 2 input link, with -fsequin (in order to read the sequin file) and -Timeref for clock ticks reference.
- description: none/ XXX
- input expected group:  Time_generator
- where are the data?: on the link's names

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/read_sequence_t.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <sys/time.h>

void function_read_sequence(int gpe)
{
    char outfilename[] = "./files/seqrange";
    char filename[] = "./files/sequences/seq";
    char fullfname[FILENAME_SIZE];  /* nom de fichier avec no */
    char fno[FRS_NBFILES];
    int maxfile, currentfile, nbread, limmax;

    double mt1;                 /* temps courant */
    float delay;
    int val, i, deb, longueur, foundfile, lien;
    read_sequence_t *dge;
    FILE *fpout;
#ifdef REAL_TIME
    struct timeval tm1;
#endif

    deb = def_groupe[gpe].premier_ele;

    longueur = def_groupe[gpe].nbre;

    dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */
    if (!dge)
    {
    /** 1e iteration de la simulation, initialisation des variables **/
        def_groupe[gpe].ext = (void *) malloc(sizeof(read_sequence_t)); /* libere dans function_end_read_sequence */
        dge = (read_sequence_t *) def_groupe[gpe].ext;  /* alias */
        /* recherche du nom du fichier de la sequence sur le lien */
        i = 0;
        foundfile = 0;
        while ((lien = find_input_link(gpe, i++)) != -1)
        {
            if (strcmp(liaison[lien].nom, "timeref"))
            {
                if (prom_getopt(liaison[lien].nom, "f", fullfname))
                {
                    foundfile = 1;
                    break;
                }
            }
        }

        if (!foundfile)
        {                       /* par defaut, ouverture, lecture du fichier indiquant ou trouver la sequence */
            if (!(fpout = fopen(outfilename, "r+")))
                fprintf(stderr, "Impossible d'ouvrir le fichier %s\n",
                        outfilename);
            else
            {
                if (fscanf(fpout, "%d %d", &currentfile, &maxfile) != 2)
                    fprintf(stderr, "Mauvais format fichier %s\n",
                            outfilename);
                else
                {
                    printf("\tcurrentfile=%d\n", currentfile);
                    limmax = pow(10, FRS_NBFILES);
                    if (maxfile >= limmax)
                    {
                        fprintf(stdout,
                                "WARNING --- Il ne peut y avoir plus de 10e%d fichiers %s",
                                FRS_NBFILES, filename);
                        maxfile = limmax - 1;
                    }
                }
                fclose(fpout);
            }
            strcpy(fullfname, filename);
            sprintf(fno, "%d", currentfile);
            strcat(fullfname, fno);
        }

        /* ouverture, lecture du fichier contenant la sequence */
        if (!(dge->fp = fopen(fullfname, "r")))
        {
            fprintf(stderr, "Impossible d'ouvrir le fichier %s\n", fullfname);

        }
        else
        {
            nbread = fscanf(dge->fp, "%f %d\n", &delay, &val);
            if (nbread == 2)
            {
                dge->delay = delay;
                dge->val = val;
            }
            else
                fprintf(stderr,
                        "Impossible de lire le fichier %s, mauvais format\n",
                        fullfname);
        }
#ifndef REAL_TIME               /* recherche de la base de temps du systeme (TimeGenerator) */
        for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
        {
            if (!strcmp(liaison[lien].nom, "timeref"))
            {
                break;
            }
        }
        if (lien == -1)
        {
            fprintf(stderr,
                    "read_sequence cannot find time reference link\n");
        }
        dge->tm = (struct timeval *) def_groupe[liaison[lien].depart].ext;
#endif
    }
#ifdef REAL_TIME
    dge->tm = &tm1;
#endif
    /* lecture du temps */
    gettime(dge->tm, mt1);

    /* output par defaut */
#ifdef PONCT_STATES
    for (i = deb; i < deb + longueur; i++)
    {                           /* Les etats sont ponctuels, ils ne durent  */
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0; /*  qu'un pas de temps : ici, remise a zero */
    }                           /* systematique si PONCT_STATES             */
#endif
    if (dge->delay >= 0)
    {                           /* intervalle inter-elmt ecoule             */
        if (mt1 - dge->time >= dge->delay)
        {                       /* ceci n'a d'effet que si le mode          */
            dge->time = mt1;    /* PONCT_STATES n'est pas defini            */
#ifndef PONCT_STATES
            for (i = deb; i < deb + longueur; i++)
            {                   /* remise a zero de toutes les activations */
                neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0; /*  apres le delais specifie              */
            }
#endif

            /* activation de l'element choisi           */
            if (isequal(neurone[deb + dge->val].s1, 0) && (dge->val != -1))  /* -1 permet d'inhiber toutes les entrees */
                neurone[deb + dge->val].s = neurone[deb + dge->val].s1 =
                    neurone[deb + dge->val].s2 = 1;

            /* lecture du prochain elmt de la sequence */
            nbread = fscanf(dge->fp, "%f %d", &delay, &val);

            if (nbread == 2)
            {
                dge->delay = delay;
                dge->val = val;
            }
            else if (nbread == EOF)
            {                   /* fin de la sequence */
                dge->delay = -1;
#ifndef PONCT_STATES            /* sequence terminee, on remet tout  0 */
                for (i = deb; i < deb + longueur; i++)
                {
                    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
                }
#endif
            }
            else
            {
                fprintf(stderr, "error while reading sequence file\n");
            }
        }
    }

}
