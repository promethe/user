/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_drives.c
\brief 

Author: Cyril Hasson
Created: 
Modified:
Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction calculant le niveau des drives (faim,soif, ...) en fonction du niveau des variables essentielles (groupe necessaire en entree: f_variables_essentielles).

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()
-tools/erreur/gestion_erreur()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs:

Todo: see author for testing and commenting the function

************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <math.h>
/*#include "../../Vision/Algo/popout/tools/include/local_var.h"*/
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*#include <NN_Core/macro_colonne.h>*/



typedef struct
{
  int gpe;/*sauvegarde le numero du groupe precedent*/
} data;


/*fonction d initialisation*/
/*recuperation des parametres*/

void new_drives(int Gpe)
{
  int n, link;
  data *my_data;
  my_data = (data *) malloc(sizeof(data));
  
  /* initialisation de la structure */

#ifdef DEBUG
  printf("initialisation structure %s %d\n",__FUNCTION__,Gpe);
#endif

  /* Le groupe doit obligatoirement avoir une entree :*/
  n = -1;
  do
    {
      n++;
      link = find_input_link(Gpe, n);
      if (link != -1)
        {
	  if (liaison[link].nom[0] == '-')
            {
	      switch (liaison[link].nom[1])
                {
		  /* definie le numero du groupe en entree */
                case 'c':
		  my_data->gpe = liaison[link].depart;

		  #ifdef DEBUG
		  printf("supervision input group : %d \n", liaison[link].depart);
		  #endif

		  break;
	
                default:
		  printf(" link %s not recognized %s \n", liaison[link].nom,__FUNCTION__);
                }
            }
	  else
	    printf(" link %s not recognized %s \n", liaison[link].nom,__FUNCTION__);
        }
    }
  while (link != -1);
  
  /*on sauvegarde les donnees dans le champ data*/
  def_groupe[Gpe].data = my_data;
  
}

/********************************** fin new_drives **********************************/


void function_drives(int Gpe)
{
  data *my_data = NULL;
  int deb_gpe=-1;
  int taille_gpe=-1;
  int i=0;
  int deb;
  int taille;


  
  /* getting data from current group structure */
  my_data = (data *) def_groupe[Gpe].data;

  if (my_data == NULL)
  {
  	printf("error while loading data in group %d (f_drives.c)\n", Gpe);
  	exit(EXIT_FAILURE);
  }
  /*debut et taille du groupe precedent*/
  deb_gpe = def_groupe[my_data->gpe].premier_ele;
  taille_gpe = def_groupe[my_data->gpe].nbre;
  /*debut du groupe courant*/
  deb = def_groupe[Gpe].premier_ele;
  taille = def_groupe[Gpe].nbre;

  /*	INSERER VERIF COMPATIBILITE TAILLE GP ENTREE ET SORTIE	*/

  /*initialisation des neurones du groupe courant*/
  for(i=0 ; i<taille ; i++)
  {
	neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2=0;
  }

  /*calcul du niveau de chaque drive*/
  for(i=0 ; i<taille ; i++)
  {

	neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = 1-neurone[deb_gpe+i].s;
  }

}
