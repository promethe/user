/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_drive_expected.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Fonction VB : On demande a l'utilisateur de rentrer la motivation devant
 etre satisfaite. D'un point de vue pratique, le fait que l'animat ai deja
 rencontre un lieu ou cette motivation puisse etre satisfaite peut beaucoup
 l'aider a resoudre le probleme.
 Bon, serieusement, la fonction doit avoir autant de neurones que de motivations
 possibles. Quand on cherche a satisfaire une motivation (donc mode
planification), le neurone correspondant est active.
 Cette fonction ne fait RIEN en exploration.
 A terme, cette fonction sera remplacee par une autre qui gerera
 automatiquement les motivation (avec une decroissance dans le temps des
variables.
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <Kernel_Function/trouver_entree.h>
void function_drive_expected(int gpe_sortie)
{

   static int deb_s, taille_s;
   int i;
   int deb_e, gpe;
   gpe = trouver_entree(gpe_sortie, (char*)"planif");
   deb_e = def_groupe[gpe].premier_ele;
   deb_s = def_groupe[gpe_sortie].premier_ele;
   taille_s = def_groupe[gpe_sortie].nbre;
   dprints("\n%d\n", deb_e);

   if (isequal(neurone[deb_e].s2, 1.))
   {
      for (i = deb_s; i < deb_s + taille_s; i++) neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;

      do
      {
         printf("\nEntrer le numeros de la motivation recherchee (1 a %d) = ",taille_s);
         cscans("%d", &i);
      }
      while ((i < 1) || (i > taille_s));
      neurone[deb_s + i - 1].s = neurone[deb_s + i - 1].s1 = neurone[deb_s + i - 1].s2 = 1;
   }
   else for (i = deb_s; i < deb_s + taille_s; i++)  neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
}
