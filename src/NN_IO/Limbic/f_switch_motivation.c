/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/

int tps;
void function_switch_motivation(int numero)
{
    int i = 0;
    int Gpe_1 = -1;
    int Gpe_2 = -1;
    int l = -1;

    /*      printf("f_store  de %d\n",numero); */

    if (def_groupe[numero].data == NULL)
    {

        tps = 1;
        if (l == -1)
        {

            l = find_input_link(numero, i);

            while (l != -1)
            {
                if (strcmp(liaison[l].nom, "-1") == 0)
                {
                    Gpe_1 = liaison[l].depart;


                }
                if (strcmp(liaison[l].nom, "-2") == 0)
                {
                    Gpe_2 = liaison[l].depart;

                }


                i++;
                l = find_input_link(numero, i);

            }
        }

        if (Gpe_1 == -1 || Gpe_2 == -1)
        {
            printf("groupe  1 ou 2  non trouve dans groupe %d\n", numero);
            exit(0);
        }




        def_groupe[numero].data = malloc(sizeof(int));
        /*memcpy(def_groupe[numero].data,groupes,3*sizeof(int)); */
        *(int *) def_groupe[numero].data = Gpe_1;

        def_groupe[numero].ext = malloc(sizeof(int));
        /*memcpy(def_groupe[numero].ext,groupes,3*sizeof(int)); */
        *(int *) def_groupe[numero].ext = Gpe_2;




    }
    else
    {

        Gpe_1 = *(int *) def_groupe[numero].data;
        Gpe_2 = *(int *) def_groupe[numero].ext;

    }
    if (tps < def_groupe[numero].seuil && global_learn == 1)
    {
        for (i = 0; i < def_groupe[numero].nbre; i++)
        {

            neurone[def_groupe[numero].premier_ele + i].s1 =
                neurone[def_groupe[Gpe_1].premier_ele + i].s1;
            neurone[def_groupe[numero].premier_ele + i].s =
                neurone[def_groupe[numero].premier_ele + i].s2 =
                neurone[def_groupe[numero].premier_ele + i].s1;

        }

    }
    else
    {

        for (i = 0; i < def_groupe[numero].nbre; i++)
        {

            neurone[def_groupe[numero].premier_ele + i].s1 =
                neurone[def_groupe[Gpe_2].premier_ele + i].s1;
            neurone[def_groupe[numero].premier_ele + i].s =
                neurone[def_groupe[numero].premier_ele + i].s2 =
                neurone[def_groupe[numero].premier_ele + i].s1;

        }
    }
    tps++;
}
