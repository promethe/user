/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_smooth_command.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
/*#define DEBUG*/

/*#define CIRC*/
void function_real_action(int numero)
{
    int l = -1;

    int i = 0;
    float var = -99999., max = -1;
    int Gpe_moteur = -1, Gpe_action = -1;


    printf("-------------------real action--------------------------\n");

    if (def_groupe[numero].data == NULL)
    {


        if (l == -1)
        {
            l = find_input_link(numero, i);
            while (l != -1)
            {
                if (strcmp(liaison[l].nom, "-X") == 0)
                {
                    Gpe_moteur = liaison[l].depart;
                }
                if (strcmp(liaison[l].nom, "-action") == 0)
                {
                    Gpe_action = liaison[l].depart;
                }
                i++;
                l = find_input_link(numero, i);
            }
        }

        if (Gpe_moteur == -1)
        {
            printf("groupe X non trouvee\n");
            exit(0);
        }

        def_groupe[numero].data = malloc(sizeof(int));

        *(int *) def_groupe[numero].data = Gpe_moteur;

        def_groupe[numero].ext = malloc(sizeof(int));

        *(int *) def_groupe[numero].ext = Gpe_action;
    }
    else
    {

        Gpe_moteur = *(int *) def_groupe[numero].data;
        Gpe_action = *(int *) def_groupe[numero].ext;

    }
    var =
        neurone[def_groupe[Gpe_moteur].premier_ele].s1 -
        neurone[def_groupe[Gpe_moteur].premier_ele].s;

    neurone[def_groupe[numero].premier_ele].s =
        neurone[def_groupe[numero].premier_ele].s1 =
        neurone[def_groupe[numero].premier_ele].s2 = 0.;
    neurone[def_groupe[numero].premier_ele + 1].s =
        neurone[def_groupe[numero].premier_ele + 1].s1 =
        neurone[def_groupe[numero].premier_ele + 1].s2 = 0.;
    for (i = 0; i < def_groupe[Gpe_action].nbre; i++)
    {

        if (neurone[def_groupe[Gpe_action].premier_ele + i].s > max)
            max = neurone[def_groupe[Gpe_action].premier_ele + i].s;

    }

    printf("max=%f\n", max);

    if (var > 0.)
    {

        neurone[def_groupe[numero].premier_ele].s =
            neurone[def_groupe[numero].premier_ele].s1 =
            neurone[def_groupe[numero].premier_ele].s2 = max;


    }
    else
    {
        neurone[def_groupe[numero].premier_ele + 1].s =
            neurone[def_groupe[numero].premier_ele + 1].s1 =
            neurone[def_groupe[numero].premier_ele + 1].s2 = max;

    }


}
