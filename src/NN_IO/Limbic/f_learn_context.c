/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_learn_context.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define	L_PRODUIT	0
#define	L_DISTANCE	1

#define	OSCILLO_SEUIL	0.8

typedef	struct	s_learn_context
{
   int		first;
   int		nr;
   int		next_gpe;
   float		seuil;
}	t_learn_context;


void function_learn_context_distance(int gpe, t_learn_context	*data)
{
   int		i = 0;
   int		first = 0;
   int		nr = 0;
   type_coeff	*lien = NULL;
   int		next_gpe;

   nr = data->nr;
   first = data->first;

   next_gpe = data->next_gpe;

   /* On caclul les potentiels d entree */
   for (i = first; i < (first + nr); i++)
   {
      float		potentiel = 0.0;
      float		potentiel_o = 0.0;
      type_coeff	*lien_c = NULL;

      /* On met tous les neuronnes à 0 */
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;

      potentiel = 0.0;
      potentiel_o = 0.0;
      /* On recupere le potentiel sur les liens d'entree inconditionnels */
      for (lien = neurone[i].coeff; lien != NULL; lien = lien->s)
      {
         if (lien->evolution == 0)
         {
            potentiel_o += (lien->val * neurone[lien->entree].s2);
         }
      }
      for (lien_c = neurone[i].coeff; lien_c != NULL; lien_c = lien_c->s)
      {
         if (lien_c->evolution == 1)
         {
            float	pot;

            pot = (lien_c->val * neurone[lien_c->entree].s2);
            potentiel += pot;
            if (isdiff(neurone[lien_c->entree].s2,0.) && ((pot > -0.00000001) && (pot < 0.00000001))) lien_c->val = potentiel_o;
         }
      }

      neurone[i].s = potentiel;
      neurone[i].s1 = potentiel;
      neurone[i].s2 = potentiel;
      if ((potentiel < -0.00000001) || (potentiel > 0.00000001))
      {
         neurone[def_groupe[next_gpe].premier_ele + (i - first)].s = potentiel;
         neurone[def_groupe[next_gpe].premier_ele + (i - first)].s1 = potentiel;
         neurone[def_groupe[next_gpe].premier_ele + (i - first)].s2 = potentiel;
      }
   }
}

void	function_learn_context(int gpe)
{
   int			i = 0;
   t_learn_context	*data = NULL;

   if (def_groupe[gpe].ext == NULL)
   {
      if ((data = malloc(sizeof(t_learn_context))) == NULL)
      {
         EXIT_ON_ERROR("f_learn_context : init : malloc");
      }
      for (i = 0; i < nbre_liaison; i++)
         if (liaison[i].depart == gpe)
         {
            if (!strcmp(liaison[i].nom, "restore"))  data->next_gpe = liaison[i].arrivee;
         }


      data->first = def_groupe[gpe].premier_ele;
      data->nr = def_groupe[gpe].nbre;
      data->seuil = def_groupe[gpe].seuil;
      def_groupe[gpe].ext = (void *)data;
   }
   else  data = (t_learn_context *)def_groupe[gpe].ext;

   function_learn_context_distance(gpe, data);
}
