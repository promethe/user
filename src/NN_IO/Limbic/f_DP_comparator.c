/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_DP_comparator.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
  fonction comparant en entree deux valeurs :                 
  une valeur d`erreur et une de reussite (entrees s2),        
  et qui ajuste DP selon.          	                        
  les liens en entree sont de type algolink, nommes :         
  `R` pour le lien apportant l`info de reussite,              
  `E` pour le lien apportant l`info d` echec                  

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

void function_DP_comparator(int gpe)
{
    int deb, i /*,longueur,nbre */ ;

/*numero des deux groupes afferents (erreur vs reussite)*/
    int num_r;
    int num_e;

/*valeurs reussite/erreur recuperees des neurones des groupes afferents*/
    float reussite;
    float erreur;
    /* unused: float sortie; */
    num_r = num_e = -1;

    deb = def_groupe[gpe].premier_ele;



/* Il doit y avoir deux liens en entree : */
/* pour chacun d`eux je recupere la valeur (erreur/reussite) */
/* selon l`etiquetage du lien     `E` ou `R`                 */

    for (i = 0; i < nbre_liaison; i++)

        if (liaison[i].arrivee == gpe)
        {
            if ((strstr(liaison[i].nom, "R") != NULL)
                || (strstr(liaison[i].nom, "r") != NULL))
                num_r = liaison[i].depart;


            if ((strstr(liaison[i].nom, "E") != NULL)
                || (strstr(liaison[i].nom, "e") != NULL))
                num_e = liaison[i].depart;

        }

    if ((num_r == -1) || (num_e == -1))
    {
        printf
            ("DP_comparator (dans limbic.c): il faut necessairement deux liens afferents pour les comparer!!!! \n");
        exit(0);                /*Avant; il y avait ;exit; */
    }

/* identification des groupes en entree */

    /* identification des groupes en entree */

    reussite = neurone[def_groupe[num_r].premier_ele].s2;
    erreur = neurone[def_groupe[num_e].premier_ele].s2;

    /*if (reussite > 0.2 ) dP = -10;
       if (erreur > reussite )dP  = 3000*erreur;
       if ((reussite <=  0.2)&&(erreur <=0)) dP=0.; */

    /*Pote=  fopen("/tmp/erreur.txt", "a"); */

    neurone[deb].s = reussite - erreur;
/*if (neurone[deb].s>=0.0) sortie=0.0;
  else  sortie=neurone[deb].s;*/

    /*if (sortie >= 0.) dP=sortie;
       else  dP=3000*sortie; */
/*printf("dP dans comp: %f \n",dP); */
    /*neurone[deb].s1=neurone[deb].s2=dP; */

}
