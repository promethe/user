/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_controle_vigilence.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
-----------------------------------------------------------------------
 Fonction VB  :    Fixe la vigilence en fonction du niveau de
  reconnaissance maximum constate sur le PTM.
  Si reconnaissance inferieure au seuil alors vigilence=1 sinon on est
   pret d'un endroit connu et vigilence=0.1
  Pour le moment, le seuil est fixe EMPIRIQUEMENT a 0.7!!!
  La valeur du seuil est fixee sur le lien
  Ah oui au fait, je m'en sers pour la planification
-----------------------------------------------------------------------

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

void function_controle_vigilence(int gpe_sortie)
{
   float seuil = 0.;
   int deb = def_groupe[gpe_sortie].premier_ele;
   int gpe_entre, deb_e = -1;
   int i;

   for (i = 0; i < nbre_liaison; i++)
      if ((liaison[i].arrivee == gpe_sortie) && (strcmp(liaison[i].nom, "temp") != 0))
      {
         gpe_entre = liaison[i].depart;
         deb_e = def_groupe[gpe_entre].premier_ele;
         seuil = atof(liaison[i].nom);
         break;
      }
   dprints("Seuil %1.3f\n", seuil);
   if ((neurone[deb_e].s2 > seuil) || isequal(vigilence,1))
   {
      vigilence = .1;
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 0;
   }
   else
   {
      vigilence = 1;
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 1;
   }
   dprints("f_controle_vigilence :  vigilence = %f\n", vigilence);
}
