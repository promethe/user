/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_diffuse.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Diffuse de maniere exponentielle du maximum                        
  sur le lien on a la valeur de sigma, le max de la gaussienne est = 
  1 * modulation                                                     
  Vrai gaussienne
  
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

void function_diffuse(int numero)
{
    int gp1, i, j, u, v,
        N, M, N1, M1, p, p1, inc1, nofind = 1, ineur_max_g1 =
        0, jneur_max_g1 = 0;
    float max_g1, tmp, d, diff = 0., sigma_carre = 0., modulation = 1.0;

#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_diffuse\n");
#endif


  /*----------------------*/
    /*Find the input gp link */
    for (i = 0; (i < nbre_liaison) && nofind; i++)
        if (liaison[i].arrivee == numero)
            nofind = 0;

    gp1 = liaison[i - 1].depart;

  /*--------------------------------------*/
    /*Value on the link stands for diffusion */
    diff = atof(liaison[i - 1].nom);

    if (diff < 0.)
    {
        printf("ERROR in function_diffuse, diff =%f\n", diff);
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG
    printf("Gausssian diffusion of sigma=%f on groupinput=%d\n", diff, gp1);
#endif

  /*--------------------------------*/
    /*Size of groups must be identical */
    N = def_groupe[numero].taillex;
    M = def_groupe[numero].tailley;
    N1 = def_groupe[gp1].taillex;
    M1 = def_groupe[gp1].tailley;
    inc1 = def_groupe[gp1].nbre / (N1 * M1);
    if ((N != N1 || M != M1) && (N != M1 || M != N1))
    {
        printf("Size of groups  %i and %i must be identical \n", gp1, numero);
        exit(EXIT_FAILURE);
    }
    if (N == M1 && M == N1)
    {
        /*inversion */
        i = M1;
        M1 = N1;
        N1 = i;
    }

  /*----------------------------------*/
    /* Max and position of max in group1 */
    max_g1 = neurone[def_groupe[gp1].premier_ele].s2;
    for (i = 0; i < N1; i++)
        for (j = 0; j < M1; j++)
        {
            p1 = (i + 1) * inc1 - 1 + j * N1 * inc1;
            tmp = neurone[def_groupe[gp1].premier_ele + p1].s2;
            if (tmp > max_g1)
            {
                ineur_max_g1 = i;
                jneur_max_g1 = j;
                max_g1 = tmp;
            }
        }
#ifdef DEBUG
    printf("max=%f\n", max_g1);
#endif

    sigma_carre = (float) diff *diff;

  /*-------------------------------------------*/
    /* Diffusion computation (gaussian)          */
    /* not normal; but normalised 0 to modulation */
    if (max_g1 > 0.)
    {
        modulation = max_g1;
        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                p = def_groupe[numero].premier_ele + i + j * N;
                u = ineur_max_g1 - i;
                v = jneur_max_g1 - j;
                d = (float) (u * u) + (float) (v * v);
                neurone[p].s = neurone[p].s1 = neurone[p].s2 =
                    exp(-d / (2. * sigma_carre)) * modulation;
                /*  printf("ineur_max_g1=%d u=%d, v=%d ,d= %f neurone[p].s=%f \n",ineur_max_g1,u,v,d,neurone[p].s); */
            }
    }
    else
    {
        /*Si pas de max positif rien!!! */
        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                p = def_groupe[numero].premier_ele + i + j * N;
                neurone[p].s = neurone[p].s1 = neurone[p].s2 = 0.;
            }
    }
}
