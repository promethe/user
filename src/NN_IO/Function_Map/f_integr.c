/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\ingroup libIO_Robot
\defgroup f_integr f_integr
\brief 


\section Author 
-Name: XXXXXXX
-Created: xx/xx/xxxx

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

- author: N.Cuperlier
- description: Reset Added
- date: 30/11/2004

- author: J.Hirel
- description: Refactoring and recalibration option added
- date: 18/04/2011

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Summ previous activity with new one; N.C: now this group is clean by inhibitory link, weight<0.
For example this is set in navigation when something new is detected in CA3 <> old way: Must be cleaned with f_cleans

\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */


/*#define DEBUG*/
/*#define log */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include <NN_Core/integr.h>

/* seuil a partir duquel on considere qu'il y a une activite
 * suffisante pour reseter le groupe */
#define SEUIL 0


typedef struct mydata_integr
{
	int gpe_calibration;
}mydata_integr;


void new_integr(int gpe)
{
	int index = 0, link = -1;
	int gpe_calibration = -1;
	mydata_integr *my_data = NULL;

	if (def_groupe[gpe].data == NULL)
	{
		while ((link = find_input_link(gpe, index++)) != -1)
		{
			if (strcmp(liaison[link].nom, "recalibration") == 0)
			{
				dprints("f_integr(%s): recalibration group found\n", def_groupe[gpe].no_name);
				gpe_calibration = liaison[link].depart;
			}
		}

		if (gpe_calibration >= 0 && def_groupe[gpe].nbre != def_groupe[gpe_calibration].nbre)
		{
			EXIT_ON_ERROR("The calibration group must have the same size as f_integr");
		}

		my_data = ALLOCATION(mydata_integr);
		my_data->gpe_calibration = gpe_calibration;
		def_groupe[gpe].data = my_data;
	}
}


/* Pour chaque neurone i du groupe on applique a chacun de ses coeff,
   la formule suivante: 
   \f[ W_{ij} = (-1/N).W_{ij}.Sj + (1/N).s_i.s_j \f] 
   - i neurone courant du groupe 
   - j neurone d'entree de la liaison 
   - \f$ W_{ij} = \f$ coeff->val de la liaison entre i et j */
void apprend_fintegr(int gpe)
{
	int i;
	float dc,Si,Sj;
	type_coeff *coeff;
	int deb,nbre;

#ifdef log
	FILE *filw = fopen("transnf_w.txt","a");
	int wc = 0;
#endif

	deb = def_groupe[gpe].premier_ele;
	nbre = def_groupe[gpe].nbre;

   for(i = deb; i < deb + nbre; i++)
   {		
      coeff = neurone[i].coeff;
      while (coeff != NULL)
      {
	 if (coeff->evolution == 1)
	 {
	    Si = neurone[i].d; /* l'act venant de la voie mvt */
	    Sj = neurone[coeff->entree].s1; /* l'act du neurone de trans */

				if (Si * Sj > 0)
				{
					dc = 1./15*(-Sj*coeff->val + Si);
				}
				else
				{
					dc = 0;
				}

				coeff->val += dc;
				if (coeff->val < 0)	coeff->val = 0.001;

#ifdef log
				/* si dW > 0 record poids de reco et dw */
				if (Si > 0 && Sj > 0)
				{
					fprintf(filw, "%f ", dc);
					wc = 1;
				}
#endif
			}
			coeff=coeff->s;
		}
	}

#ifdef log
	/*act apres maj*/
	if (wc == 1)
		fprintf(filw, "\n");
	fclose(filw);
#endif
}


void function_integrn(int gpe_sortie)
{
   int i=0, N, M, neuron=-1, deb, taille;
   float sortiep, W;
   type_coeff *coeff;

	dprints("-------------------------------------------------\n");
	dprints("function_integr\n");

#ifdef log
	FILE *fila = fopen("transnf_act.txt","a");
	FILE *film = fopen("movement_act.txt","a");
	FILE *filr = fopen("reco_act.txt","a");
	int num_trans = -1;
#endif

	/* -------------------------------- */
	/* info of the group */
	N = def_groupe[gpe_sortie].taillex;
	M = def_groupe[gpe_sortie].tailley;
	deb = def_groupe[gpe_sortie].premier_ele;

   /* var locale */
   taille = (N*M)+deb;

   /* Parcour de tous les neurones du groupes */
   for(i = deb; i < taille; i++)
   {
      /* initialisation des variables pour chaque neurones */
      sortiep = 0;
      coeff = neurone[i].coeff;

#ifdef log
		/* act avant maj */
		fprintf(fila, "%8f ", sortiep);
#endif

		/* on parcourt tous les coeff */
		while (coeff != NULL)
		{
			W = coeff->val;
			neuron = coeff->entree;
			sortiep += W * neurone[neuron].s1;

			if (coeff->type == 1)
			{
#ifdef log
				/* act issue des poids de reco */
				if (neurone[neuron].s1 > 0)
				{
					fprintf(filr, "%8f ", W * neurone[neuron].s1);
					num_trans = neuron;
				}
#endif
			}
			else
			{
#ifdef log
				/* act issue de l'input mvt */
				if (neurone[neuron].s1 > 0)
					fprintf(film, "%8f ", neurone[neuron].s1);
#endif
				/* memorise l'info de mouvement */
				neurone[i].d = W * neurone[neuron].s1;
			}
			coeff = coeff->s;
		}

		if (sortiep - def_groupe[gpe_sortie].simulation_speed < 0) sortiep = 0.0;
		else sortiep -= def_groupe[gpe_sortie].simulation_speed;

		neurone[i].s = sortiep;
		neurone[i].s1 = sortiep;
		neurone[i].s2 = sortiep;
	}

#ifdef log
	/* act apres maj */
	fprintf(fila, "%8f ", sortiep);

	if (num_trans > -1)
	{
		fprintf(fila, " ; %d \n", num_trans);
		fprintf(filr, " %d ; \n", num_trans);
		fprintf(film, " ; %d \n", num_trans);
	}

	fclose(fila);
	fclose(filr);
	fclose (film);
#endif
}


void function_integr(int gpe)
{
	int i = 0, deb, nbre;
	float act = 0, sortien, sortiep, W;
	type_coeff *coeff;
	int gpe_calibration = ((mydata_integr *) def_groupe[gpe].data)->gpe_calibration;

	dprints("-------------------------------------------------\n");
	dprints("function_integr\n");

	/*--------------------------------*/
	/* info of the group */
	nbre = def_groupe[gpe].nbre;
	deb = def_groupe[gpe].premier_ele;

	/* Parcours de tous les neurones du groupes */
	for (i = deb; i < deb + nbre; i++)
	{
		/* initialisation des variables pour chaque neurones */
		sortiep = 0;
		sortien = 0;
		coeff = neurone[i].coeff;

		/* on parcourt tous les coeff */
		while (coeff != NULL)
		{
			W = coeff->val;
			act = neurone[coeff->entree].s1 * W;

			if (W > 0)
			{
				sortiep += act;
			}
			/* sinon on ne regarde que si il n 'y a pas deja eu d'inhib... */
			else
			{
				sortien += act;
			}

			coeff = coeff->s;
		}

		/* si inhibition on ne prend en compte que les nouvelles entrees */
		if (sortien < SEUIL)
		{
			dprints("reset asked of the path_integration ... val = %f\n", sortien);

			if (gpe_calibration < 0)
			{
				neurone[i].s = sortiep;
				neurone[i].s1 = sortiep;
				neurone[i].s2 = sortiep;
			}
			else
			{
				neurone[i].s  = neurone[def_groupe[gpe_calibration].premier_ele+i-deb].s1;
				neurone[i].s1 = neurone[def_groupe[gpe_calibration].premier_ele+i-deb].s1;
				neurone[i].s2 = neurone[def_groupe[gpe_calibration].premier_ele+i-deb].s1;
			}
		}
		/* sinon on conserve la memoire*/
		else
		{
			neurone[i].s += sortiep;
			neurone[i].s1 += sortiep;
			neurone[i].s2 += sortiep;
			/*	 dprints("f_integr(%s): neuron %i activity = %f\n", def_groupe[gpe].no_name, i, neurone[i].s1);*/
		}
	}
}


void destroy_integr(int gpe)
{
	if (def_groupe[gpe].data != NULL)
	{
		free(((mydata_integr*) def_groupe[gpe].data));
		def_groupe[gpe].data = NULL;
	}
	dprints("destroy_integr(%s): Leaving function\n", def_groupe[gpe].no_name);
}
