/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_add_prod_adhoc.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Tous les groupes doivent avoir la meme taille, sinon mise a echelle

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

void function_add_prod_adhoc(int numero)
{
    int l /*,l1 */ , i, j, i1, i2, j1, j2, p, p1, p2, cpt = 1, Add = 0, Prod =
        0, gpe_entree1 =
        -1, gpe_entree2, deb, deb1, deb2, inc1, inc2, N, M, N1, M1, N2, M2;
    float tab_liaisons[256][2];
    char *name, *st;
    float rN1, rM1, rN2, rM2, val1, val2, lambda = .1, Bmax = 1;

#ifdef DEBUG
    printf("------------------------------------\n");
    printf("  function_add_prod_adhoc\n");
#endif

    /*Compter le nbre de groupe en entree */
    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == numero)
        {
            name = liaison[i].nom;

            st = strstr(name, "-Add-W=");
            if (st != NULL)
            {
                Add = 1;
                gpe_entree1 = liaison[i].depart;
                tab_liaisons[0][0] = liaison[i].depart;
                tab_liaisons[0][1] = atof(&st[7]);
#ifdef DEBUG
                printf("ADDITION gpe_entree1=%d tab_liaisons[0][1]=%f\n",
                       gpe_entree1, tab_liaisons[0][1]);
#endif
            }
            else
            {
                st = strstr(name, "-Prod-W=");
                if (st != NULL)
                {
                    Prod = 1;
                    gpe_entree1 = liaison[i].depart;
                    tab_liaisons[0][0] = gpe_entree1;
                    tab_liaisons[0][1] = atof(&st[8]);
#ifdef DEBUG
                    printf("PRODUIT gpe_entree1=%d tab_liaisons[0][1]=%f\n",
                           gpe_entree1, tab_liaisons[0][1]);
#endif

                }
                else
                {
                    st = strstr(name, "-L");
                    if (st != NULL)
                    {
                        tab_liaisons[cpt][0] = (float) liaison[i].depart;
                        tab_liaisons[cpt][1] = atof(&st[2]);
#ifdef DEBUG
                        printf
                            ("liaison[i].depart=%d tab_liaisons[%d][0]=%f tab_liaisons[%d][1]=%f\n",
                             liaison[i].depart, cpt, tab_liaisons[cpt][0],
                             cpt, tab_liaisons[cpt][1]);
#endif
                        cpt++;
                    }
                }
            }
        }

    if (gpe_entree1 < 0)
        return;




  /*----------------------------*/
    /*Taille des groupes et debuts */
    N = def_groupe[numero].taillex;
    M = def_groupe[numero].tailley;
    deb = def_groupe[numero].premier_ele;

    if (cpt == 1)               /*Dans ce cas simple copie (avec chgement d'echelle des activites) */
    {
        gpe_entree1 = tab_liaisons[0][0];
        N1 = def_groupe[gpe_entree1].taillex;
        M1 = def_groupe[gpe_entree1].tailley;
        deb1 = def_groupe[gpe_entree1].premier_ele;
        rN1 = (float) N1 / (float) N;
        rM1 = (float) M1 / (float) M;
        inc1 = def_groupe[gpe_entree1].nbre / (N1 * M1);
#ifdef DEBUG
        printf("gpe_entree1=%d N1=%d M1=%d rN1=%f rM1=%f inc1=%d\n",
               gpe_entree1, N1, M1, deb1, rN1, rM1, inc1);
#endif

        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                i1 = (int) (((float) i) * rN1); /*echelle? */
                j1 = (int) (((float) j) * rM1);
                if (i1 >= 0 && i1 < N1 && j1 >= 0 && j1 < M1)
                {
                    p = i + j * N;
                    p1 = (i1 + 1) * inc1 - 1 + j1 * N1 * inc1;
                    neurone[deb + p].s = neurone[deb1 + p1].s;
                    neurone[deb + p].s1 = neurone[deb1 + p1].s1;
                    neurone[deb + p].s2 = neurone[deb1 + p1].s2;
                }
            }
        return;
    }



  /*------------------------------------------------------------*/
    /* tous les neurones de sortie a 0  pour le groupe numero     */
    for (i = 0; i < (N * M); i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

  /*---------------------------------------------------------------*/
    /*Pour chaxun des groupes en entree il peut y en avoir plus que 2 */
    for (l = 0; l < 1; l++)
    {
        gpe_entree1 = tab_liaisons[l][0];
        gpe_entree2 = tab_liaisons[l + 1][0];
        N1 = def_groupe[gpe_entree1].taillex;
        M1 = def_groupe[gpe_entree1].tailley;
        N2 = def_groupe[gpe_entree2].taillex;
        M2 = def_groupe[gpe_entree2].tailley;
        deb1 = def_groupe[gpe_entree1].premier_ele;
        deb2 = def_groupe[gpe_entree2].premier_ele;

      /*----------------------------------------*/
        /*Les rapports si il y a chgment d'echelle */
        rN1 = (float) N1 / (float) N;
        rM1 = (float) M1 / (float) M;
        rN2 = (float) N2 / (float) N;
        rM2 = (float) M2 / (float) M;

        /*printf("N=%d M=%d N1=%d M1=%d N2=%d M2=%d rN1=%f rM1=%f rN2=%f rM2=%f\n",N,M,N1,M1,N2,M2,rN1,rM1,rN2,rM2);
           getchar();getchar(); */

      /*---------------------------------------------------*/
        /*Incr if there is macro_neurones with micro_neurones */
        inc1 = def_groupe[gpe_entree1].nbre / (N1 * M1);
        inc2 = def_groupe[gpe_entree2].nbre / (N2 * M2);

      /*----------------------------------  */
        /* Recherche de Bmax dans le groupe B */
        Bmax = -1;
        for (p2 = 0; p2 < (N2 * M2); p2 = p2 + inc2)
        {
            if (neurone[deb2 + p2].s > Bmax)
                Bmax = neurone[deb2 + p2].s;
        }
#ifdef DEBUG
        printf("Bmax=%f\n", Bmax);
#endif

      /*-------*/
         /*CALCULS*/ val1 = tab_liaisons[l][1];
        val2 = tab_liaisons[l + 1][1];


        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                i1 = (int) (((float) i) * rN1); /*echelle? */
                j1 = (int) (((float) j) * rM1);
                i2 = (int) (((float) i) * rN2);
                j2 = (int) (((float) j) * rM2);

                /*printf("inc1=%d inc2=%d rN1=%f rM1=%f rN2=%f rM2=%f i=%d j=%d i1=%d j1=%d i2=%d j2=%d\n",inc1,inc2,rN1,rM1,rN2,rM2,i,j,i1,j1,i2,j2); */

                if (i1 >= 0 && i1 < N1 && j1 >= 0 && j1 < M1
                    && i2 >= 0 && i2 < N2 && j2 >= 0 && j2 < M2)
                {
                    p = i + j * N;
                    p1 = (i1 + 1) * inc1 - 1 + j1 * N1 * inc1;
                    p2 = (i2 + 1) * inc2 - 1 + j2 * N2 * inc2;
                    /*printf("p=%d p1=%d p2=%d\n",p,p1,p2);
                       getchar();           getchar(); */


                    if (Add == 1)
                    {
                        /*Addition en tenant compte des poids et normalisation */
                        neurone[deb + p].s +=
                            (neurone[deb1 + p1].s * val1 +
                             neurone[deb2 + p2].s * val2) / (val1 + val2);
                        neurone[deb + p].s1 +=
                            (neurone[deb1 + p1].s1 * val1 +
                             neurone[deb2 + p2].s1 * val2) / (val1 + val2);
                        neurone[deb + p].s2 +=
                            (neurone[deb1 + p1].s2 * val1 +
                             neurone[deb2 + p2].s2 * val2) / (val1 + val2);

                    }
                    else if (Prod == 1)
                        /*On ne fait pas un vrai produit A*B  mais plutot */
                        /*A*(100*B+lambda)/(100*Bmax+lambda) avex lambda<<B */
                    {
                        lambda = val2;
                        neurone[deb + p].s =
                            neurone[deb1 +
                                    p1].s * (10000. * neurone[deb2 + p2].s +
                                             lambda) / (10000. * Bmax +
                                                        lambda);
                        neurone[deb + p].s1 =
                            neurone[deb1 +
                                    p1].s1 * (10000. * neurone[deb2 + p2].s1 +
                                              lambda) / (10000. * Bmax +
                                                         lambda);
                        neurone[deb + p].s2 =
                            neurone[deb1 +
                                    p1].s2 * (10000. * neurone[deb2 + p2].s2 +
                                              lambda) / (10000. * Bmax +
                                                         lambda);

                    }
/*		if(neurone[ deb + p ].s>0.)
		  printf("i1=%d j1=%d i2=%d j2=%d neurone[A*B].s1=%f neurone[A].s1=%f neurone[B].s1=%f\n",i1,j1,i2,j2,
			 neurone[ deb + p ].s1,neurone[ deb1 + p1 ].s1,neurone[ deb2 + p2 ].s1);*/
                }
            }
        /*  getchar();getchar(); */


    }



}
