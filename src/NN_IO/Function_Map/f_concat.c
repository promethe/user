/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_concat.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

void function_concat(int gpe_sortie)
{
    static int nbr_gpe = -1;
    static int gpe_entree[10];
    static int taille_groupe_e[10];
    int taille_groupe_s;
    static int deb_e[10];
    int deb_s;
    static int increment;
    char nom_liaison[32];

    int i, j, k /*, n */ ;
    /*   double    max; */
    /*   int               reponse; */
    static int sum, sum1;
    static int tailleX_e[10], tailleY_e[10];

#ifdef DEBUG
    message("-----------  Function concat -----------\n");
#endif

    nbr_gpe = -1;               /*rajout, Modif Sacha le 23/04/99: pour qu'on utilise plusieurs fois f_concat le static ne marche plus */

    deb_s = def_groupe[gpe_sortie].premier_ele;
    taille_groupe_s = def_groupe[gpe_sortie].nbre;

    if (nbr_gpe < 0)            /* on ne cherche qu'une fois les groupes d'entree */
    {
    	PRINT_WARNING(" DO NOT USE f_concat (problemes) !!! Use f_concatenation instead !");

        for (nbr_gpe = 0, i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == gpe_sortie)
            {
                for (j = 0;
                     (j < nbr_gpe) && (liaison[i].depart != gpe_entree[j]);
                     j++) ;
                if (j == nbr_gpe)
                {
                    gpe_entree[nbr_gpe] = liaison[i].depart;
                    strcpy(nom_liaison, liaison[i].nom);
                    taille_groupe_e[nbr_gpe] =
                        def_groupe[gpe_entree[nbr_gpe]].nbre;
                    deb_e[nbr_gpe] =
                        def_groupe[gpe_entree[nbr_gpe]].premier_ele;
                    tailleX_e[nbr_gpe] =
                        def_groupe[gpe_entree[nbr_gpe]].taillex;
                    tailleY_e[nbr_gpe] =
                        def_groupe[gpe_entree[nbr_gpe]].tailley;
                    nbr_gpe++;
                }
            }

        sum1 = tailleX_e[0] * tailleY_e[0];
        increment = taille_groupe_e[0] / (tailleX_e[0] * tailleY_e[0]); /* tous les groupes d'entree doivent faire la meme taille */
#ifdef DEBUG
        printf("Actuel: X:%d Y%d\n", def_groupe[gpe_sortie].taillex,
               def_groupe[gpe_sortie].tailley);
#endif
        for (sum = 0, j = 0; j < nbr_gpe; j++)
        {
#ifdef DEBUG
            printf("\t%deme groupe d'entree(%d) X:%d, Y:%d\n", j + 1,
                   gpe_entree[j], tailleX_e[j], tailleY_e[j]);
#endif
            sum += tailleX_e[j] * tailleY_e[j];
        }
#ifdef DEBUG
        printf("taille_groupe_s=%d sum=%d\n", taille_groupe_s, sum);
#endif
        if (taille_groupe_s != sum)
        {
            printf
                ("Taille incoherente avec la taille de la carte d'entree, sum=%d ,taille_groupe_s=%d de gpe_sortie=%d\n",
                 sum, taille_groupe_s, gpe_sortie);
            exit(1);
        }
    }
#ifdef DEBUG
    printf("increment=%d\n", increment);
#endif

/*  if (increment==1)	 Micro-neurone */
    if (1)
    {

        for (i = 0; i < taille_groupe_s; i++)
        {
            neurone[deb_s + i].s = neurone[deb_s + i].s1 =
                neurone[deb_s + i].s2 = 0.;
        }

        for (k = 0; k < nbr_gpe; k++)
        {
            for (j = 0; j < tailleY_e[k]; j++)
            {
                for (i = 0; i < tailleX_e[k]; i++)
                {
                    /* printf("c=%d i=%d ",deb_s+i+j*tailleX_e[k]+k*sum1,deb_e[k]+(i+1)*increment-1+j*increment*tailleX_e[k]); */
                    neurone[deb_s + i + j * tailleX_e[k] + k * sum1].s =
                        neurone[deb_e[k] + (i + 1) * increment - 1 +
                                j * increment * tailleX_e[k]].s;
                    neurone[deb_s + i + j * tailleX_e[k] + k * sum1].s1 =
                        neurone[deb_e[k] + (i + 1) * increment - 1 +
                                j * increment * tailleX_e[k]].s1;
                    neurone[deb_s + i + j * tailleX_e[k] + k * sum1].s2 =
                        neurone[deb_e[k] + (i + 1) * increment - 1 +
                                j * increment * tailleX_e[k]].s2;
                }
                /*printf("\n"); */
            }
        }
    }
}
