/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_all_displacement.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  This function must be connected with 2 groups identicals                 
  it mesures the difference between neur of max activity                   
  this group must have 2 times + 1 neurones if you want to                 
  code all diff possible ! Other case could cause a truncature             
  In the case of no input activity: nothing is done                        
  One link must be "max", all differences are computed with the input group
  off "max" link  and the second group.					      
  If there is written "ring" on the first link, the activities can rotate  
  like a ring (un tore)!!!!    

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

void function_all_displacement(int numero)
{
    int M, M1, M2, N, N1, N2, inc1, inc2, i, j, l1 = 0, l2 = 0, cpt = 0, flag = 0, p, p2, temp, inter1, inter2, var1, var2;
    int gp_input1 = 0, gp_input2 = 0;
    int ineur_max_g1 = 0, jneur_max_g1 = 0/*, ineur_max_g2 = 0, jneur_max_g2 = 0*/;
    float max_g1, max_g2, tmp;
    int i_center, j_center, i_diff, j_diff; /*difference */
    int ring = 0;

#ifdef DEBUG
    printf("--------------------------------------------------------\n");
    printf(" function_all_displacement \n");
#endif


    /* ----------------------------------- */
    /* Finding the  link and the group    */
    /* ---------------------------------- */
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == numero)
        {
            cpt = cpt + 1;
            if (flag == 0)
            {
                l1 = i;         /*memo de la liaison */
                gp_input1 = liaison[i].depart;
                flag = 1;
            }
            else
            {
                l2 = i;         /*memo de la liaison */
                gp_input2 = liaison[i].depart;
            }
        }
    }

  /*-------------------------------------*/
    /* Testing if there is 2 groups linked */
    if (cpt != 2)
    {
        printf
            ("Error in function_all_displacement, you must have 2 links connected with the group %i\n",
             numero);
        exit(EXIT_FAILURE);
    }

  /*-------------------------------*/
    /*Finding which link is named max */
    if (strstr(liaison[l1].nom, "max") != NULL)
    {
        /*fisrt link */
    }
    else
    {
        if (strstr(liaison[l2].nom, "max") != NULL)
        {
            /*2nd link must be inverted */
            temp = l2;
            l2 = l1;
            l1 = temp;
            temp = gp_input2;
            gp_input2 = gp_input1;
            gp_input1 = temp;
        }
        else
        {
            printf
                ("Error in function_all_displacement,  group %i, one link must be named :  max\n",
                 numero);
            exit(EXIT_FAILURE);
        }
    }

    if (strstr(liaison[l1].nom, "ring") != NULL)
        ring = 1;               /*the neurons activities can rotate like on a ring !!! */

  /*----------------------------------*/
    /* Getting dimensions of the groups */
    N = def_groupe[numero].taillex;
    M = def_groupe[numero].tailley;

    N1 = def_groupe[gp_input1].taillex;
    M1 = def_groupe[gp_input1].tailley;

    N2 = def_groupe[gp_input2].taillex;
    M2 = def_groupe[gp_input2].tailley;

  /*---------------------------------------------------*/
    /*Incr if there is macro_neurones with micro_neurones */
    inc1 = def_groupe[gp_input1].nbre / (N1 * M1);
    inc2 = def_groupe[gp_input2].nbre / (N2 * M2);

  /*--------------------------------------------*/
    /* Testing if all groups have same dimensions */
    if (N1 != N2 || M1 != M2)
    {
        printf("Error, the groups %i and %i must have same dimensions\n",
               gp_input1, gp_input2);
        exit(EXIT_FAILURE);
    }

  /*---------*/
    /* Warning */
    if (N < (2 * N1 + 1) && M < (2 * M1 + 1))
    {
        printf
            ("!!!!!!!!!!!!! WARNING, diff will operate a trunc if necessary !!!!!!!!!!!!!!!!\n");
    }

  /*-------------------------------------------*/
    /*Localisaton of the max of group1 and group2 */
    /* Note : Each group have N1*M1=N2*M2        */
    /*       this is made on s value !!!!!      */
    max_g1 = neurone[def_groupe[gp_input1].premier_ele].s;
    max_g2 = neurone[def_groupe[gp_input2].premier_ele].s;

    var1 = N1 * inc1;
    var2 = N2 * inc2;

    for (i = 0; i < N1; i++)
    {
        inter1 = (i + 1) * inc1 - 1;
        inter2 = (i + 1) * inc2 - 1;

        for (j = 0; j < M1; j++)
        {
      /*----------------------------------*/
            /* Max and position of max in group1 */
            p = inter1 + j * var1;
            tmp = neurone[def_groupe[gp_input1].premier_ele + p].s;
            if (tmp > max_g1)
            {
                ineur_max_g1 = i;
                jneur_max_g1 = j;
                max_g1 = tmp;
            }

      /*----------------------------------*/
            /* Max and position of max in group2 */
            p = inter2 + j * var2;
            tmp = neurone[def_groupe[gp_input2].premier_ele + p].s;
            if (tmp > max_g2)
            {
                /*ineur_max_g2 = i;
                jneur_max_g2 = j;*/
                max_g2 = tmp;
            }
        }
    }

  /*-----------------------------------*/
    /* Setting to zero value all neurones */
    for (p = 0; p < (N * M); p++)
    {
        neurone[def_groupe[numero].premier_ele + p].s
            = neurone[def_groupe[numero].premier_ele + p].s1
            = neurone[def_groupe[numero].premier_ele + p].s2 = 0.0;
    }


  /*-------------------------------------------------*/
    /*In the case of no input activity: nothing is done */
    if (max_g1 > 0. || max_g2 > 0.)
    {
        int dd;
        int NN2, MM2;

      /*------------------------------------------------------*/
        /* Making the diff group                                */
        /* 0 difference will be the centered positionned neurone */

        i_center = (int) ((float) N / 2.) + 1;
        j_center = (int) ((float) M / 2.) + 1;

      /*--------------------*/
        /*Half size of group 2 */
        NN2 = (int) ((float) N2 / 2.);
        MM2 = (int) ((float) M2 / 2.);


      /*-----------------------*/
        /*Differences computation */
        for (i = 0; i < N2; i++)
        {
            inter2 = (i + 1) * inc2 - 1;
            for (j = 0; j < M2; j++)
            {

                i_diff = ineur_max_g1 - i;
                j_diff = jneur_max_g1 - j;


          /*---------------------------------------*/
                /*The activities can rotate like a ring ? */
                if (ring)
                {
                    if (i_diff > NN2)
                        i_diff = N2 - i_diff;
                    if (j_diff > MM2)
                        j_diff = M2 - j_diff;
                    if (i_diff < -NN2)
                        i_diff = i_diff + N2;
                    if (j_diff < -MM2)
                        j_diff = j_diff + M2;
                }


          /*---------------------------------------------*/
                /*Testing: Have we enough neurone on the group? */
                /*Setting the response                         */
                if (i_diff < N && j_diff < M)
                {
                    dd = i_center - 1 + i_diff + (j_center - 1 + j_diff) * N;

                    p = def_groupe[numero].premier_ele + dd;
                    p2 = def_groupe[gp_input2].premier_ele + inter2 +
                        j * var2;

                    neurone[p].s = neurone[p].s1 = neurone[p].s2 =
                        neurone[p2].s;
                }
            }
        }

    }

}
