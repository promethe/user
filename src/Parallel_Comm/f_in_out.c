#include "libParallel_Comm.h"

#include "libx.h"

#include "blc.h"
#include "outils.h"
#include <limits.h>
#include <unistd.h>
#include <pthread.h>

type_link_option in_out_parameters[] =
   {
      { "a", "block the f_out until an ack is received. The number corresponds to the number of f_in listening with -a1", OPTIONAL_INT,
         { .integer = 0 } },
      { "b", "block the f_in until new data is received. The number in f_out corresponds to the number of f_in  waiting with -b1", OPTIONAL_INT,
         { .integer = 0 } },
      { "B", "broker name", OPTIONAL_STRING,
         { .string = "" } },
      { "c", "receive checkbox data.", FLAG,
         { .integer = 0 } },
      { "i", "receive image data ", FLAG,
         { .integer = 0 } },
      { "t", "textbox data", FLAG,
         { .integer = 0 } },
      { "v", "receive checkbox data.", FLAG,
         { .integer = 0 } },
   /*   { "h", "Address of the connection (default localhost) ", OPTIONAL_STRING,
    { .string = "localhost" } },*/
      { "input", "Input group of data to send. ", REQUIRED_INPUT_GROUP, // As it is the only REQUIRED_INPUT_GROUP you do not to name it in the link
         { 0 } },
      { "n", "variable name", OPTIONAL_STRING,
         { .string = "" } },
      { "s", "s output 0->s, 1>", OPTIONAL_INT,
         { .integer = -1 } },
      { "u", "unsafe", OPTIONAL_INT,
         { .integer = 0 } },
      { NULL } };

struct group_channel {
   blc_channel *channel;
   char channel_name[NAME_MAX + 1];
   uint32_t display_type;
   char hostname[HOST_NAME_MAX + 1];
   type_groupe *input;
   int output_s;
};

typedef struct in_out {
   char sem_abp[4];
   int group_channels_nb;
   int ack, block;
   struct group_channel *group_channels;

} type_in_out;

prom_images_struct *try_to_get_prom_images(struct group_channel *ch)
{
   prom_images_struct *prom_images = NULL;
   blc_channel_info info;

   if (blc_channel_get_info_with_name(&info, ch->channel_name) != -1)
   {
      if (info.type == STRING_TO_UINT32("UIN8"))
      {
         if ((info.dims_nb == 2) && (info.format == STRING_TO_UINT32("Y800"))) prom_images = calloc_prom_image(1, info.lengths[0], info.lengths[1], 1);
         else if ((info.dims_nb == 3) && (info.format == STRING_TO_UINT32("RGB3")) && (info.lengths[0] == 3)) prom_images = calloc_prom_image(1, info.lengths[1], info.lengths[2], 3);
         else EXIT_ON_ERROR("The type '%.4s', format '%.4s', dims_nb '%d', or lentgh0 '%d'.", UINT32_TO_STRING(info.type), UINT32_TO_STRING(info.format), info.dims_nb, info.lengths[0]);
      }
      else if (info.type == STRING_TO_UINT32("FL32"))
      {
         if (info.dims_nb == 2) prom_images = calloc_prom_image(1, info.lengths[0], info.lengths[1], 4);
      }
      else EXIT_ON_ERROR("'%s' is not a managed type (UIN8, FL32) but is: '%.4s'.", ch->channel_name, UINT32_TO_STRING(info.type));
      ch->channel = open_blc_channel(ch->channel_name);
   }

   return prom_images;
}

prom_images_struct *get_prom_images(struct group_channel *ch)
{
   prom_images_struct *prom_images;

   prom_images = try_to_get_prom_images(ch);

   /*In the future we will use a clean blc_channel event semaphore */
   while (prom_images == NULL)
   {
      usleep(100000);
      prom_images = try_to_get_prom_images(ch);
   }
   return prom_images;
}

void blc_channel_recv_neurons(blc_channel *channel, type_groupe *group, int s_output)
{
   int i;
   float *values = (float*) channel->mem.data;
   type_neurone *neurons = &neurone[group->premier_ele];

   switch (s_output)
   {
   case -1:
      FOR_INV (i, group->nbre)
      {
         neurons[i].s = values[i * 3];
         neurons[i].s1 = values[i * 3 + 1];
         neurons[i].s2 = values[i * 3 + 2];
      }
      break;
   case 0:
      FOR_INV (i, group->nbre)
         neurons[i].s = values[i];
      break;
   case 1:
      FOR_INV (i, group->nbre)
         neurons[i].s1 = values[i];
      break;
   case 2:
      FOR (i, group->nbre)
         neurons[i].s2 = values[i];
      break;
   default:
      EXIT_ON_ERROR("Mode %d for 's' is not allowed", s_output);
   }
}

void blc_channel_send_neurons(blc_channel *channel, type_groupe *input_group, int output_mode)
{
   int i;
   float *values = (float*) channel->mem.data;
   type_neurone *neurons = &neurone[input_group->premier_ele];

   switch (output_mode)
   {
   case -1:
      FOR (i, input_group->nbre)
      {
         values[i * 3] = neurons[i].s;
         values[i * 3 + 1] = neurons[i].s1;
         values[i * 3 + 2] = neurons[i].s2;
      }
      break;
   case 0:
      FOR (i, input_group->nbre)
         values[i] = neurons[i].s;
      break;
   case 1:
      FOR (i, input_group->nbre)
         values[i] = neurons[i].s1;
      break;
   case 2:
      FOR (i, input_group->nbre)
         values[i] = neurons[i].s2;
      break;
   default:
      EXIT_ON_ERROR("Mode %d for 's' is not implemented", output_mode);
   }
}

blc_channel *prom_images_create_blc_channel(prom_images_struct *prom_images, char *channel_name, char const *sem_abp)
{
   blc_channel *channel;
   blc_channel_info info;
   int id;

   //Later we could add a dimension with the number of images.
   if (prom_images->image_number > 1) EXIT_ON_ERROR("We can only send one image. You have %d images", prom_images->image_number);

   id = blc_channel_get_info_with_name(&info, channel_name);

   switch (prom_images->nb_band)
   {
   case 1:
      if (id == -1)  channel = create_blc_channel(channel_name, sem_abp, STRING_TO_UINT32("UIN8"), STRING_TO_UINT32("Y800"), NULL, 2, prom_images->sx, prom_images->sy);
      else if( info.type ==  STRING_TO_UINT32("UIN8") && strcmp(info.sem_abp, sem_abp)==0 && info.format ==  STRING_TO_UINT32("Y800") && info.dims_nb==2 && info.lengths[0]== prom_images->sx &&   info.lengths[1]== prom_images->sy)
         channel = open_blc_channel(channel_name);
      else EXIT_ON_ERROR("The blc_channel '%s' exist and you try to reopen it for a image with a different property. You should probably unlink the blc channel.", channel_name);
      break;
   case 3:
      if (id==-1) channel = create_blc_channel(channel_name, sem_abp, STRING_TO_UINT32("UIN8"), STRING_TO_UINT32("RGB3"), NULL, 3, 3, prom_images->sx, prom_images->sy);
      else if( info.type ==  STRING_TO_UINT32("UIN8") && strcmp(info.sem_abp, sem_abp)==0 && info.format ==  STRING_TO_UINT32("RGB3") && info.dims_nb==3 && info.lengths[0]==3 && info.lengths[1]== prom_images->sx &&  info.lengths[2]== prom_images->sy)
         channel = open_blc_channel(channel_name);
      else EXIT_ON_ERROR("The blc_channel '%s' exist and you try to reopen it for a image with a different property. You should probably unlink the blc channel.", channel_name);

      break;
   case 4:
      if (id==-1) channel = create_blc_channel(channel_name, sem_abp, STRING_TO_UINT32("FL32"), 0, NULL, 2, prom_images->sx, prom_images->sy);
      else if( info.type ==  STRING_TO_UINT32("FL32") && strcmp(info.sem_abp, sem_abp)==0 && info.format ==  STRING_TO_UINT32("NDEF") && info.dims_nb==2 && info.lengths[0]== prom_images->sx &&  info.lengths[1]== prom_images->sy)
           channel = open_blc_channel(channel_name);
      else EXIT_ON_ERROR("The blc_channel '%s' exist and you try to reopen it for a image with a different property. You should probably unlink the blc channel.", channel_name);
      break;
   default:
      channel = NULL;
      EXIT_ON_ERROR(" (nb_band = %d)  is not managed.", prom_images->nb_band);
   }
   return channel;
}

void new_in(int gpe)
{
   char *broker_name;
   char *name;
   char channel_basename[NAME_MAX + 1];
   blc_channel_info info;
   int dim;
   struct group_channel *ch;
   type_in_out *my_data;
   int c, i, t, v;

   my_data = ALLOCATION(type_in_out);
   /* f_in has only one group_channel */
   my_data->group_channels = ALLOCATION(struct group_channel);
   my_data->group_channels_nb = 1;

   ch = my_data->group_channels; // This is just a shortcut
   get_parameters(gpe, in_out_parameters, "a", &my_data->ack, "b", &my_data->block, "c", &c, "i", &i, "t", &t, "v", &v, /*"h", ch->hostname,*/ "B", &broker_name, "n", &name, "s", &ch->output_s, NULL);

   if (my_data->ack > 1) EXIT_ON_ERROR("Having the parameter of -a (ack) bigger than 1 (you have '%d') is meaningless in f_in.", my_data->ack);
   if (my_data->block > 1) EXIT_ON_ERROR("Having the parameter of -b (block) bigger than 1 (you have '%d') is meaningless in f_in.", my_data->block);
   if (c + i + t + v > 1) EXIT_ON_ERROR("You cannot have more than one display type (c,i,t,v).");

   if (c) ch->display_type = STRING_TO_UINT32("CBOX");
   else if (i) ch->display_type = 'i';
   else if (t) ch->display_type = STRING_TO_UINT32("TBOX");
   else if (v) ch->display_type = STRING_TO_UINT32("VMET");
   else ch->display_type = 0;

   if (strlen(broker_name) == 0) SPRINTF(channel_basename, "/%s", bus_id); //This is inter-promethe communication
   else STRCPY(channel_basename, broker_name);

   //By default the name of the channel is the name of the group
   if (strcmp(name, "") == 0) name = def_groupe[gpe].no_name;

   if (my_data->ack) my_data->sem_abp[0] = 'a';
   else my_data->sem_abp[0] = '-';

   if (my_data->block) my_data->sem_abp[1] = 'b';
   else my_data->sem_abp[1] = '-';

   my_data->sem_abp[2] = 'p';
   my_data->sem_abp[3] = 0;

   SPRINTF(ch->channel_name, "%s.%s", channel_basename, name);
   //If it is an image we will set the full name later, once  we know the size.
   //Otherwise we know it is a array of floats and we can create it now..
   if (ch->display_type != 'i')
   {
      if (blc_channel_get_info_with_name(&info, ch->channel_name) == -1) //It was not existing, we create it.
      {
         if (ch->output_s == -1) ch->channel = create_blc_channel(ch->channel_name, my_data->sem_abp, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), info.parameter, 3, 3, def_groupe[gpe].taillex, def_groupe[gpe].tailley);
         else ch->channel = create_blc_channel(ch->channel_name, my_data->sem_abp, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), info.parameter, 2, def_groupe[gpe].taillex, def_groupe[gpe].tailley);
      }
      else
      {
         if (info.type != STRING_TO_UINT32("FL32")) EXIT_ON_GROUP_ERROR(gpe, "f_in waits floats but the sender is sending '%.4s'.", UINT32_TO_STRING(info.type));
         if (ch->output_s == -1)
         {
            if (info.dims_nb != 3) EXIT_ON_GROUP_ERROR(gpe, "The sender is not sending a tensor of dim 3 (s, s1, s2)  as it should but a tensor of dim '%d'. Maybe it is wrong option '-s'.", info.dims_nb);
            if (info.lengths[0] != 3) EXIT_ON_GROUP_ERROR(gpe, "The sender is not sending a length of 3  for dim 0 but '%d'.", info.lengths[0]);
            dim = 1;
         }
         else
         {
            if (info.dims_nb != 2) EXIT_ON_GROUP_ERROR(gpe, "The sender is not sending a matrix as it should but a tensor of dim '%d'. Maybe it is a wrong optin '-s'.", info.dims_nb);
            dim = 0;
         }
         if (info.lengths[dim]*info.lengths[dim+1] != def_groupe[gpe].taillex*def_groupe[gpe].tailley) EXIT_ON_GROUP_ERROR(gpe, "f_in has taille '%d' but the sender is sending '%d'.", def_groupe[gpe].taillex*def_groupe[gpe].tailley, info.lengths[dim]*info.lengths[dim+1]);
         ch->channel = open_blc_channel(ch->channel_name);

      }
   }
   def_groupe[gpe].data = my_data;
}

void function_in(int gpe)
{
   type_in_out *my_data;
   prom_images_struct *prom_images;
   void* image_data;
   struct group_channel *ch;

   my_data = def_groupe[gpe].data;
   ch = my_data->group_channels;

   if (ch->display_type == 'i')
   {
      if (def_groupe[gpe].ext == NULL)
      {
         if (my_data->block) def_groupe[gpe].ext = get_prom_images(ch);
         else def_groupe[gpe].ext = try_to_get_prom_images(ch);
      }

      if (def_groupe[gpe].ext != NULL)
      {
         if (my_data->block) SYSTEM_ERROR_CHECK(sem_wait(ch->channel->sem_block), -1, "Waiting channel '%s'", ch->channel->info.name);
         blc_channel_protect(ch->channel, 1);
         prom_images = def_groupe[gpe].ext;
         image_data = (void*) (prom_images->images_table[0]);
         memcpy(image_data, (void const*) ch->channel->mem.data, ch->channel->mem.size);
         blc_channel_protect(ch->channel, 0);
         if (my_data->ack) SYSTEM_ERROR_CHECK(sem_post(ch->channel->sem_ack), -1, "Sending ack for '%s'", ch->channel->info.name);
      }
   }
   else
   {
      if (my_data->block) SYSTEM_ERROR_CHECK(sem_wait(ch->channel->sem_block), -1, "Waiting channel '%s'", ch->channel->info.name);
      blc_channel_protect(ch->channel, 1);
      blc_channel_recv_neurons(ch->channel, &def_groupe[gpe], ch->output_s);
      blc_channel_protect(ch->channel, 0);
      if (my_data->ack) SYSTEM_ERROR_CHECK(sem_post(ch->channel->sem_ack), -1, "Sending ack for '%s'", ch->channel->info.name);
   }
}

void destroy_in(int gpe)
{
   type_in_out *my_data = def_groupe[gpe].data;
   destroy_blc_channel(my_data->group_channels->channel);
   free(my_data->group_channels);
   free(my_data);
}



/******************************** out *************************************************/


/** Create the new out */
void new_out(int gpe)
{
   char *broker_name;
   char channel_basename[NAME_MAX + 1];
   char *name;
   blc_channel_info info;
   int c, i, t, v, links_nb, link_id;
   int dim;
   type_in_out *my_data;
   struct group_channel tmp_ch;

   my_data = ALLOCATION(type_in_out);

   /* We start with no input group and we will add them, one by one */
   my_data->group_channels = NULL;
   my_data->group_channels_nb = 0;

   /* generic parameters */
   get_parameters(gpe, in_out_parameters, "a", &my_data->ack, "b", &my_data->block, NULL);

   if (my_data->ack) my_data->sem_abp[0] = 'a';
   else my_data->sem_abp[0] = '-';

   if (my_data->block) my_data->sem_abp[1] = 'b';
   else my_data->sem_abp[1] = '-';

   my_data->sem_abp[2] = 'p';
   my_data->sem_abp[3] = 0;


   for (links_nb = 0; (link_id = find_input_link(gpe, links_nb)) >= 0; links_nb++)
   {
      // specific parameter per link
      get_parameters_on_link(gpe, &liaison[link_id], in_out_parameters,  "c", &c, "i", &i, "t", &t, "v", &v, "B", &broker_name, "n", &name, "s", &tmp_ch.output_s, "input", &tmp_ch.input, NULL);

      if (tmp_ch.input==NULL) EXIT_ON_ERROR("input = NULL");

      if (c + i + t + v > 1) EXIT_ON_ERROR("You cannot have more than one display type (c,i,t,v).");
      if (c) tmp_ch.display_type = STRING_TO_UINT32("CBOX");
      else if (i) tmp_ch.display_type = 'i';
      else if (t) tmp_ch.display_type = STRING_TO_UINT32("TBOX");
      else if (v) tmp_ch.display_type = STRING_TO_UINT32("VMET");
      else tmp_ch.display_type = 0;

      if (strlen(broker_name) == 0) SPRINTF(channel_basename, "/%s", bus_id);
      else STRCPY(channel_basename, broker_name);

      //Default name, the group's name of the input box.
      if (strlen(name) == 0) name=tmp_ch.input->no_name;

      SPRINTF(tmp_ch.channel_name, "%s.%s", channel_basename, name);

      if (tmp_ch.display_type != 'i')
      {
         if (blc_channel_get_info_with_name(&info, tmp_ch.channel_name) == -1)
         {
            if (tmp_ch.output_s==-1) tmp_ch.channel = create_blc_channel(tmp_ch.channel_name, my_data->sem_abp, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), info.parameter, 3, 3, tmp_ch.input->taillex, tmp_ch.input->tailley);
            else tmp_ch.channel = create_blc_channel(tmp_ch.channel_name, my_data->sem_abp, STRING_TO_UINT32("FL32"), STRING_TO_UINT32("NDEF"), info.parameter, 2, tmp_ch.input->taillex, tmp_ch.input->tailley);
         }
         else
         {
            if (info.type != STRING_TO_UINT32("FL32")) EXIT_ON_GROUP_ERROR(gpe, "f_out sends floats but the receiver is waiting '%.4s'.", UINT32_TO_STRING(info.type));
            if ( tmp_ch.output_s==-1)
            {
               if (info.dims_nb !=3) EXIT_ON_GROUP_ERROR(gpe, "The receiver is not waiting a tensor of dim 3 as it should but a tensor of dim '%d'. Maybe it is wrong option '-s'.", info.dims_nb);
               if (info.lengths[0]!=3) EXIT_ON_GROUP_ERROR(gpe, "The receiver is not waiting a length of 3 for dim 0 but '%d'.", info.lengths[0]);
               dim=1;
            }
            else
            {
               if (info.dims_nb !=2) EXIT_ON_GROUP_ERROR(gpe, "The receiver is not waiting a matrix as it should but a tensor of dim '%d'. Maybe it is a wrong optin '-s'.", info.dims_nb);
               dim=0;
            }

            if (info.lengths[dim]*info.lengths[dim+1] != tmp_ch.input->taillex*tmp_ch.input->tailley)
               EXIT_ON_GROUP_ERROR(gpe, "f_out sends taille '%d' but the receiver is waiting '%d'.", tmp_ch.input->taillex* tmp_ch.input->tailley, info.lengths[dim]*info.lengths[dim+1]);

            tmp_ch.channel = open_blc_channel(tmp_ch.channel_name);
         }
      }
      APPEND_ITEM(&my_data->group_channels, &my_data->group_channels_nb, &tmp_ch);
   }
   def_groupe[gpe].data = my_data;
}

void function_out(int gpe)
{
   int i;
   type_in_out *my_data = def_groupe[gpe].data;
   prom_images_struct *prom_images=NULL;
   struct group_channel *ch;

   /* For all channels, we wait that every receiver have send ack */
   if (my_data->ack) FOR(i, my_data->group_channels_nb)
   {
      ch = &my_data->group_channels[i];

      if ((ch->display_type == 'i') && (ch->input->ext!=NULL))
      {
         prom_images = ch->input->ext;
         if (ch->channel == NULL) ch->channel = prom_images_create_blc_channel(prom_images, ch->channel_name, my_data->sem_abp);
         SYSTEM_ERROR_CHECK(sem_wait(ch->channel->sem_ack), -1, "Waiting ack for '%s'", ch->channel->info.name);
      }
   }

   /* For all channels, we wait that every receiver have send ack */
   FOR(i, my_data->group_channels_nb)
   {
      ch = &my_data->group_channels[i];
      if (ch->display_type == 'i')
      {
         if (ch->input->ext!=NULL)
         {
            prom_images = ch->input->ext;
            if (ch->channel == NULL) ch->channel = prom_images_create_blc_channel(prom_images, ch->channel_name, my_data->sem_abp);
            blc_channel_protect(ch->channel, 1);
            memcpy(ch->channel->mem.data, prom_images->images_table[0], ch->channel->mem.size);
            blc_channel_protect(ch->channel, 0);
         }
      }
      else
      {
         blc_channel_protect(ch->channel, 1);
         blc_channel_send_neurons(ch->channel, ch->input, ch->output_s);
         blc_channel_protect(ch->channel, 0);
      }
   }

   /* For all channels, we send only now all the unblock*/
     if (my_data->block) FOR(i, my_data->group_channels_nb)
     {
        ch = &my_data->group_channels[i];
        if ((ch->display_type != 'i') ||  ((ch->display_type == 'i') && (ch->input->ext != NULL)))  SYSTEM_ERROR_CHECK(sem_post(ch->channel->sem_block), -1, "Unblocking channel '%s'", ch->channel->info.name);

     }
}

void destroy_out(int gpe)
{
   int i;
   type_in_out *my_data = def_groupe[gpe].data;

   // We do not unlink the channel in case we want reconnect to a  persistent f_in.
   FOR(i, my_data->group_channels_nb)  destroy_blc_channel(my_data->group_channels[i].channel);
   free(my_data);
}
