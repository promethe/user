/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_send f_send
   \ingroup libParrallel_Com

   \brief communication between promethe scripts (sending)

   \details

   \section Description La communication est definie par le nom sur le
   lien d'entree (algo). Il doit y avoir un groupe f_recv portant le
   meme nom (meme connection).  Recupere les differentes valeurs s,
   s1, s2 sur le groupe qui transmis ie le groupe en entree du groupe
   f_send. Attention, il faut le meme nombre de neurones entre le
   groupe f_recv et le groupe en entree du f_send.

   \section Options

   Incomplet (?)  

   - -ack : le groupe reste bloque tant qu'un message
   n'a pas ete recu d'ack n'a pas ete recu en provenance du recv.  

   - -noext : ne transmet pas le .ext s'il existe

   - -setnoext : Option a mettre sur un lien sync ou sur le lien
   donnant le nom de la communication. Permet d'utiliser un neurone
   pour modifier dynamiquement la propriete noext de la
   transmission. Si le premier neurone du groupe d'entree est plus
   grand que 0.5, alors on empeche la transmission du .ext. La
   priorite reste a l'option -noext si elle est definie.

   \section Liens
   voir f_recv

   \file  
   \ingroup f_send
   \brief communication between promethe scripts (sending)

**/
/* #define DEBUG */
#include <libx.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>
#include <time.h>

/* Promethe Internal Headers */
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include "virtual_link.h"
#include "consts.h"
#include "prom_tools/include/prom_bus.h"

#define USE_EXT 1


typedef struct s_send
{
  virtual_link_t *vlink;
  int previous_gpe; /* groupe d'entree */
} t_send;

static void build_image(int num_groupe)
{
  int index;
  char* pt;
  unsigned int size;
  unsigned int size_img;
  prom_images_struct *prom_images = NULL;
  unsigned int sx = 0;
  unsigned int sy = 0;
  unsigned int nb_band = 0;
  unsigned int image_number = 0;
  t_send *data = NULL;
  virtual_link_t *vlink = NULL;

  dprints("build image\n");

  data = def_groupe[num_groupe].data;
  vlink = data->vlink;

#ifdef USE_EXT
  prom_images = (prom_images_struct *) def_groupe[data->previous_gpe].ext;
#else
  prom_images = (prom_images_struct *) def_groupe[data->previous_gpe].video_ext;
#endif /* USE_EXT */

/*   dprints("build image : prom_images %d, img %d\n", prom_images, vlink->img); */

  if (prom_images && vlink->img)
  {
    sx = (unsigned int) prom_images->sx;
    sy = (unsigned int) prom_images->sy;
    nb_band = (unsigned int) prom_images->nb_band;
    image_number = (unsigned int) prom_images->image_number;

/*     dprints("f_send : im number %d \n", image_number); */

    if (vlink->fft)
    {
      size_img = (4 * sizeof(unsigned int)) + (sx * (sy + 2)
                 * sizeof(float) * image_number);
    }
    else
    {
      size_img = (4 * sizeof(unsigned int)) + (sx * sy * nb_band
                 * image_number);
    }
  }
  else
  {
    size_img = 0;
  }

  size = (2 * sizeof(unsigned int)) + size_img;

  /* Allocation si besion */
  if ((vlink->size_data + size) > vlink->size_max_data)
  {
    vlink->data = realloc(vlink->data, vlink->size_data + size);
    if (!vlink->data)
    {
      EXIT_ON_ERROR("f_send : build_image() : realloc()");
    }
    vlink->size_max_data = vlink->size_data + size;
  }

  pt = vlink->data + vlink->size_data;

  /* Ajout size et type */
  *(unsigned int *) pt = size;
  pt += sizeof(unsigned int);

  *(unsigned int *) pt = IMAGE;
  pt += sizeof(unsigned int);

/*   dprints("build image 2 : prom_images %d, img %d \n", prom_images, vlink->img); */

  /* Ajout image */
  if (prom_images && vlink->img)
  {
    *(unsigned int *) pt = sx;
    pt += sizeof(unsigned int);

    *(unsigned int *) pt = sy;
    pt += sizeof(unsigned int);

    *(unsigned int *) pt = nb_band;
    pt += sizeof(unsigned int);

    *(unsigned int *) pt = image_number;
    pt += sizeof(unsigned int);

    for (index = 0; (unsigned int)index < image_number; index++)
    {
      if (vlink->fft)
      {
  memcpy(pt, prom_images->images_table[index], sx * (sy + 2)
         * sizeof(float));
  pt += (sx * (sy + 2) * sizeof(float));
      }
      else
      {
  memcpy(pt, prom_images->images_table[index], sx * sy * nb_band);
  pt += (sx * sy * nb_band);
      }
    }
  }

  vlink->size_data += size;
}

static void build_neurone(int num_groupe)
{
  unsigned int size;
  unsigned int nb_neurone;
  unsigned int nb_macro;
  unsigned int incr;
  unsigned int first;
  int index;
  char* pt;
  t_send *data = NULL;
  virtual_link_t* vlink = NULL;

/*   dprints("build neurone\n"); */

  data = def_groupe[num_groupe].data;
  vlink = data->vlink;

  nb_neurone = (unsigned int) def_groupe[data->previous_gpe].nbre;
  nb_macro = def_groupe[data->previous_gpe].taillex
    * def_groupe[data->previous_gpe].tailley;

  /*           --- neurones ---              --- size --- type ---*/
  size = (3 * sizeof(float) * nb_macro) + 2 * sizeof(unsigned int);

  /* allocation si besoin */
  if ((vlink->size_data + size) > vlink->size_max_data)
  {
    vlink->data = realloc(vlink->data, vlink->size_data + size);
    if (!vlink->data)
    {
      EXIT_ON_ERROR("f_send : build_neurone() : realloc()");
    }
    vlink->size_max_data = vlink->size_data + size;
  }

  pt = vlink->data + vlink->size_data;

  /* On met la taille */
  *(unsigned int *) pt = size;
  pt += sizeof(unsigned int);

  /* On met le type */
  *(unsigned int *) pt = NEURONE;
  pt += sizeof(unsigned int);

  incr = nb_neurone / nb_macro;

  first = def_groupe[data->previous_gpe].premier_ele;
  for (index = incr - 1; (unsigned int)index < nb_neurone; index += incr)
  {
    *(float *) pt = neurone[first + index].s;
    pt += sizeof(float);
    *(float *) pt = neurone[first + index].s1;
    pt += sizeof(float);
    *(float *) pt = neurone[first + index].s2;
    pt += sizeof(float);
  }

  vlink->size_data += size;
}

static void build_data(int num_groupe)
{
  char* pt;
  unsigned int flag;
  t_send *data = NULL;
  virtual_link_t* vlink = NULL;
  int with_ext=1;

  data = def_groupe[num_groupe].data;
  vlink = data->vlink;
  /** compute 'enable ext sending' */
  if(vlink->noext_set>-1) {
    with_ext=!(neurone[vlink->noext_set].s1>0.5);
    dprints("with ext : %d\n",with_ext);
  }


  /** Gestion du flag */
  if (with_ext && vlink->img  && vlink->ack)
  {
    flag = DATA_ACK_IMG;
  }
  else if (with_ext && vlink->img && !vlink->ack)
  {
    flag = DATA_IMG;
  }
  else if (!vlink->img && vlink->ack)
  {
    flag = DATA_ACK;
  }
  else
  {
    flag = DATA;
  }

  /** Calcul taille du flag */
  vlink->size_data = sizeof(unsigned int);

  /** Allocation si jamais alloue */
  if (!vlink->data)
  {
    vlink->data = malloc(vlink->size_data);
    if (!vlink->data)
    {
      EXIT_ON_ERROR("f_send : build_data() : malloc()");
    }
    vlink->size_max_data = vlink->size_data;
  }
  else if (vlink->size_data > vlink->size_max_data)
  {
    vlink->data = realloc(vlink->data, vlink->size_data);
    if (!vlink->data)
    {
      EXIT_ON_ERROR("f_send : build_data() : realloc()");
    }
    vlink->size_max_data = vlink->size_data;
  }

  /** Ajout du flag */
  pt = vlink->data;
  *(unsigned int *) pt = flag;

  /**  Ajout des autres donnees */
  switch (flag)
  {
  case DATA:
    build_neurone(num_groupe);
    break;

  case DATA_IMG:
    build_neurone(num_groupe);
    build_image(num_groupe);
    break;

  case DATA_ACK:
    build_neurone(num_groupe);
    break;

  case DATA_ACK_IMG:
    build_neurone(num_groupe);
    build_image(num_groupe);
    break;
  }
}

void new_send(int num_groupe)
{
  t_send* data = NULL;
  virtual_link_t* vlink = NULL;
  int num_liaison=-1;
  int i = 0;
  int gpe_noext_set=-1;
  int noext_set_n=-1;
  char param[256];


  if (!def_groupe[num_groupe].data)
  {
    if ((data = (t_send *) malloc(sizeof(t_send))) == NULL)
    {
      prom_bus_send_message("f_send : new_send() : malloc error [%i] %s !\n", num_groupe, def_groupe[num_groupe].no_name);
      EXIT_ON_ERROR("f_send(%s) : new_send() : malloc error [%i] %s !\n", def_groupe[num_groupe].no_name, num_groupe, def_groupe[num_groupe].no_name);
    }
  
    /*test si il existe au moins un lien en entrée*/
    if(find_input_link(num_groupe,0) < 0)
      EXIT_ON_ERROR("no input link for the group %s!!!", def_groupe[num_groupe].no_name);
    
    /* Recuperer le nom sur le lien */
    while ((num_liaison = find_input_link(num_groupe, i++)) >= 0)
    {

      /** Il faut forcement avoir sync -setNoExt (ou -setnoext) */
      /* Recuperation du lien inhibant l envoi du ext */
      if (prom_getopt(liaison[num_liaison].nom,"setnoext",param)>0 || prom_getopt(liaison[num_liaison].nom,"setNoExt",param)>0) {
        gpe_noext_set = liaison[num_liaison].depart;
        dprints("noext-set gpe : %s\n",def_groupe[gpe_noext_set].no_name);
        noext_set_n=def_groupe[gpe_noext_set].premier_ele;
      }

      /* On ne tien pas compte des liens dont le nom commence par sync */
      if ((strncmp(liaison[num_liaison].nom, "sync", 4)) == 0) {
        continue;
      }

      /* appel fonction rempli virtual link : retourne pointeur sur le vitual link */
      vlink = virtual_links_set_flags(liaison[num_liaison].nom);
      if (!vlink)
      {
        EXIT_ON_ERROR("f_send(%s) : new_send() : Network link %s not found!\n", def_groupe[num_groupe].no_name, liaison[num_liaison].nom);
      }

      data->previous_gpe = liaison[num_liaison].depart;
      vlink->num_groupe = (unsigned int) liaison[num_liaison].depart;

    }
    /*kprints("vlink dans groupe!!!!\n");*/
    vlink->noext_set = noext_set_n;
    data->vlink = vlink;
    def_groupe[num_groupe].data = data;

    if (data->vlink == NULL)
    {
      prom_bus_send_message("f_send : new_send() : Input link not found for group [%i] %s !\n", num_groupe, def_groupe[num_groupe].no_name);
      EXIT_ON_ERROR("f_send(%s) : new_send() : Input link not found for group [%i] %s !\n", def_groupe[num_groupe].no_name, num_groupe, def_groupe[num_groupe].no_name);
    }
  }
}

void function_send(int num_groupe)
{
  struct timeval tv_before, tv_after;

  int retval;
  t_send* data = NULL;
  virtual_link_t *vlink = NULL;
  struct timespec ts;
  struct timeval now;

  dprints("entree dans function send (%s)\n", def_groupe[num_groupe].no_name);

  data = def_groupe[num_groupe].data;
  vlink = data->vlink;

  if (vlink == NULL)
  {
    prom_bus_send_message("f_send : function_send() : vlink pointer lost for group [%i] %s\n", num_groupe, def_groupe[num_groupe].no_name);
    kprints("f_send(%s) : function_send() : vlink pointer lost for group [%i] %s\n", def_groupe[num_groupe].no_name, num_groupe, def_groupe[num_groupe].no_name);
  }

  if ((errno = pthread_mutex_lock(&vlink->mut_send)) != 0)
  {
    EXIT_ON_ERROR("f_send : pthread_lock()");
  }

  /* rempli buffer */
  build_data(num_groupe);


  
  /* envoie */
  retval = virtual_link_send_data(vlink);

  if (retval != RET_OK)
  {
    if ((errno = pthread_mutex_unlock(&vlink->mut_send)) != 0)
    {
      EXIT_ON_ERROR("f_send : pthread_unlock()");
    }
    return;
  }

  if (vlink->ack)
  {
    if (vlink->timeout)
    {
      gettimeofday(&now, NULL);
      ts.tv_sec = now.tv_sec + vlink->nbsec_timeout;
      ts.tv_nsec = 0;

      dprints("f_send(%s): Wait ack with timeout\n", def_groupe[num_groupe].no_name);
      retval = sem_timedwait(&vlink->wait_ack, &ts);

      if (retval != 0)
      {
  prom_bus_send_message("f_send : function_send() : Waiting ack timeout for %s after %u seconds!\n", vlink->name, vlink->nbsec_timeout);
  kprints("f_send(%s) : function_send() : Waiting ack timeout for %s after %u seconds!\n", def_groupe[num_groupe].no_name, vlink->name, vlink->nbsec_timeout);
      }
    }
    else
    {
      gettimeofday(&tv_before, NULL);
      dprints("f_send(%s): Wait ack without timeout\n", def_groupe[num_groupe].no_name);
      retval = sem_wait(&vlink->wait_ack);
      dprints("\t\tf_send(%s): ack received: unblock\n", def_groupe[num_groupe].no_name);
      gettimeofday(&tv_after, NULL);
      cprints("Delta send wait_ck: %3.0f \n", (float)(tv_after.tv_usec - tv_before.tv_usec)/1000);
    }
  }

  if ((pthread_mutex_unlock(&vlink->mut_send)) != 0)
  {
    EXIT_ON_ERROR("f_send : pthread_unlock()");
  }

  dprints("sortie de function send (%s)\n", def_groupe[num_groupe].no_name);
}

void destroy_send(int num_groupe)
{
  (void)num_groupe;
}

