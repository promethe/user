/*
 * f_publish.c
 *
 *  Created on: 30 juil. 2014
 *      Author: sylvain
 *
 * Usage :  -n{ChannelName} sets the channel's name
 *          -i makes this publish an input
 *          -o makes this publish an output
 *          -reliable makes the network connection for this channel reliable (default unreliable)
 *          -raz{NbIterations} if this publish is an input, resets to {default} after {NbIterations} without new value received
 *          -maxFreq{Freq} if this publish is an output, do not send value if the iterations frequency is higher than {Freq}
 *          -minDiff{diff} if this publish is an output, only sends a packet if the the new neuron value differs more than {diff} from the previous neuron value
 *	    -default{default} if this publish is an input, set the initial value to {default} and raz to {default}
 */

#include <pthread.h>
#include <time.h>
#include <math.h>

#include <net_message_debug_dist.h>
#include "../../../prom_tools/include/basic_tools.h"
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <limits.h>

#include <dev.h>
#include <device.h>
#include <Components/publish.h>


typedef struct
{
   Publish *com;
   void* channelID;
   char channelname[NAME_MAX];

   type_groupe *input_group;
   float *buffer;
   int bufferSize;
   int firstBufferSent;
   int firstIteration;

   char mode;

   char nego[NAME_MAX];

   int reliable;
   int raz;
   int razCount;
   float initValue;
   float maxFreq;
   float period;
   double lastSendTimestamp;
   float minDiff;

} type_publish;

void f_output(type_publish *groupdata);
void f_input(type_groupe * group, type_publish *groupdata);
double time_get_microseconds();

static pthread_mutex_t mutex_recv = PTHREAD_MUTEX_INITIALIZER;

void reveive_callback(float *buffer, int bufferSize, void * userData);


void new_publish(int gpe)
{
   type_publish *groupdata;
   type_groupe *group;
   Device *device = NULL;
   char *link_name;
   int links_nb = 0, link_id;


   group = &def_groupe[gpe];
   group->data = ALLOCATION(type_publish);
   groupdata = group->data;
   groupdata->buffer = NULL;
   groupdata->bufferSize = 0;
   groupdata->firstBufferSent = 0;
   groupdata->input_group = NULL;
   groupdata->raz = -1;
   groupdata->razCount = 0;
   groupdata->maxFreq = -1;
   groupdata->lastSendTimestamp = 0;
   groupdata->minDiff = -1;
   groupdata->reliable = FALSE;
   groupdata->initValue = 0;
   groupdata->firstIteration = TRUE;

   device = dev_get_device("publish",NULL);

   groupdata->com = (Publish *) device->components[0];

   for (link_id = find_input_link(gpe, links_nb); link_id != -1; link_id = find_input_link(gpe, links_nb))
   {
      link_name = liaison[link_id].nom;
      if (strncmp(link_name, "sync", 4) != 0)
      {
         if (groupdata->input_group != NULL) EXIT_ON_GROUP_ERROR(gpe, "Only one input link allowed for f_publish\n");

         groupdata->input_group = &def_groupe[liaison[link_id].depart];

         if(!prom_getopt(link_name,"-n",groupdata->channelname)) EXIT_ON_GROUP_ERROR(gpe, "Must specify a name !\n");
         if(prom_getopt_int(link_name, "-raz", &groupdata->raz) == 1) groupdata->raz = -1;
         if(prom_getopt(link_name, "-reliable", NULL)) groupdata->reliable = TRUE;
         if(prom_getopt_float(link_name, "-maxFreq", &groupdata->maxFreq) == 1) groupdata->maxFreq = -1;
         if(prom_getopt_float(link_name, "-minDiff", &groupdata->minDiff) == 1) groupdata->minDiff = -1;
	 if(prom_getopt_float(link_name, "-default", &groupdata->initValue) == 1) groupdata->initValue = 0;

         if (prom_getopt(link_name, "-i", NULL)) groupdata->nego[0] = groupdata->mode = 'I';
         else if (prom_getopt(link_name, "-o", NULL)) groupdata->nego[0] = groupdata->mode = 'O';
         else EXIT_ON_GROUP_ERROR(gpe, "Must specify if the published neuron is an input or an output\n");
     }
      links_nb++;
   }

   if(groupdata->maxFreq > 0){
       groupdata->period = (1e6/groupdata->maxFreq); //compute period in micro seconds
   }else{
       groupdata->period = -1;
   }

   groupdata->channelID = groupdata->com->channel_create(groupdata->com,groupdata->channelname, groupdata->reliable);

   if (groupdata->mode == 'I'){
      groupdata->com->channel_register_receive_callback(groupdata->com, groupdata->channelID, reveive_callback, groupdata);
   }


}

void function_publish(int gpe)
{
   type_groupe *group = &def_groupe[gpe];
   type_publish *groupdata = (type_publish*)group->data;
   type_neurone *neurons;
   int i;

   if(groupdata->firstIteration){
       int size  = -1;

      //Initialize output neurons to initValue
      neurons = &neurone[group->premier_ele];
      i = group->nbre;
      while (i--){
         neurons[i].s = neurons[i].s1 = neurons[i].s2 = groupdata->initValue;
      }

      //set negociation string
      if (groupdata->mode == 'O'){
         size = groupdata->input_group->nbre;
      }
      else{
         size = group->nbre;
      }
      memcpy(&groupdata->nego[1], &size, sizeof(int) );
      groupdata->nego[5] = '\0';

      groupdata->com->channel_set_negociation_message(groupdata->com, groupdata->channelID, groupdata->nego, 5);

      groupdata->firstIteration = FALSE;
   }

   //Don't do anything if the channel is not ready to be used
   if ( !groupdata->com->channel_is_connected(groupdata->com,groupdata->channelID) ){
      return ;
   }

   if (groupdata->mode == 'I') f_input(group,groupdata);
   else f_output(groupdata);


}

void f_output(type_publish *groupdata)
{
   type_neurone *neurons;
   int i;
   float *buffer ;
   int send = FALSE;

   if(groupdata->maxFreq > 0){
       double currentTime = time_get_microseconds();
       if(currentTime - groupdata->lastSendTimestamp < groupdata->period) return ;
       groupdata->lastSendTimestamp = currentTime;
   }

   //allocate the buffer that will contain neuron data if it isn't allocated yet
   if (groupdata->buffer == NULL)
   {
      groupdata->bufferSize = groupdata->input_group->nbre;
      groupdata->buffer = MANY_ALLOCATIONS(groupdata->bufferSize, float);
   }

   buffer = groupdata->buffer;

   //Get a pointer to the actual neurons data
   neurons = &neurone[groupdata->input_group->premier_ele];
   i = groupdata->input_group->nbre;

   //Check if new data should be sent
   if(groupdata->minDiff > 0 && groupdata->firstBufferSent){
       while (i--){
           if(fabsf(buffer[i] - neurons[i].s1) > groupdata->minDiff){
               send = TRUE;
              break;
           }
       }
   }else{
       groupdata->firstBufferSent = TRUE;
       send = TRUE;
   }

   //If new data should be sent, do send it
   if(send){
       i = groupdata->input_group->nbre;
       while(i--){
           buffer[i] = neurons[i].s1;
       }
       groupdata->com->channel_write(groupdata->com, groupdata->channelID, (char*)buffer, groupdata->bufferSize * sizeof(float) / sizeof(char));
   }
}

void f_input(type_groupe * group, type_publish *groupdata)
{
   int i ;
   type_neurone * neurons;

   //allocate the buffer that will contain neuron data if it isn't allocated yet
   if (groupdata->buffer == NULL)
   {
      groupdata->bufferSize = group->nbre;
      groupdata->buffer = MANY_ALLOCATIONS(groupdata->bufferSize, float);
   }

   //Get a pointer to the actual neurons data
   i = group->nbre;
   neurons = &neurone[group->premier_ele];

   pthread_mutex_lock(&mutex_recv);
   if(groupdata->raz >= 0 && groupdata->razCount >= groupdata->raz) {
       while (i--){
          neurons[i].s = neurons[i].s1 = neurons[i].s2 = groupdata->initValue;
       }
   }else {
       while (i--){
          neurons[i].s = neurons[i].s1 = neurons[i].s2 = groupdata->buffer[i];
       }
   }
   groupdata->razCount++;
   pthread_mutex_unlock(&mutex_recv);

}

void reveive_callback(float *buffer, int bufferSize, void * userData)
{
   type_publish * groupdata = (type_publish*) userData;

   if (bufferSize <= groupdata->bufferSize)
   {
      pthread_mutex_lock(&mutex_recv);
      groupdata->razCount = 0;
      memcpy(groupdata->buffer, buffer, bufferSize*sizeof(float));
      pthread_mutex_unlock(&mutex_recv);
   }
}


double time_get_microseconds(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}


void destroy_publish(int gpe)
{
   type_groupe *group = &def_groupe[gpe];
   type_publish *groupdata = (type_publish*)group->data;

   groupdata->com->destroy(groupdata->com);

   free(groupdata->buffer);
}
