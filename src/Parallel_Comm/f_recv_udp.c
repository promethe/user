/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_recv_udp f_recv_udp
\ingroup libParrallel_Com

\brief communication between promethe scripts with UDP (reception)

\details

\section Description
 La communication est definie par le nom sur le lien d'entree (algo). Il doit y avoir un groupe f_send portant le meme nom (meme connection).
 Recupere les differentes valeurs s, s1, s2 sur le groupe qui transmis ie le groupe en entree du groupe f_send. Attention, il faut le meme nombre de neurones entre le groupe f_recv et le groupe en entree.

\section Options

Incomplet (?)
- -block : le groupe reste bloque tant qu'un message n'a pas ete recu.

- -raz : Met la sortie du f_recv a 0 si aucun message n'est recu. On
peut passer en parametre un entier donnant le nombre d'iteration sans
message au bout duquel la sortie doit etre resettee. ATTENTION cela
n'affecte pas le .ext i.e. seuls les neurones sont remis a zero.

- -debug : selectionner ou non l'affichage du debug du reseau a la
reception. Par defaut, l'affichage se fait toute les 30 sec. On peut
modifier (a la ms pres) la periode utilisee pour l'affichage. Si
echange de ext alors affiche un debug specifique sur les ext.

- -EXT : gestion du ext avec extraction via une structure separee.

\section Liens
voir f_send_udp f_recv

\file
\ingroup f_recv_udp
\brief communication between promethe scripts with UDP (reception)

 **/
#include "../../../prom_tools/include/basic_tools.h"
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <limits.h>
#include <pthread.h>
#include <gen_types.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <semaphore.h>
#include "prom_kernel/include/prom_jpeg_uncompressor.h"


#include "f_recv_send_udp.h"

///////////
// TYPES //
///////////

typedef struct
{
   int port;
   boolean blocking;
   boolean compressed;
   int raz_nbIteration;
   int which_s;

   int neurons_nb, ext_size;
   int w,h,bands_nb;

   unsigned char* buf1;
   unsigned char* buf2;
   unsigned char* buf_in;
   unsigned char* buf_out;
   size_t buf_size, buf_max_size;
   int raz_counter;

   prom_jpeg_uncompressor* jpeg_uncompressor;
   prom_images_struct_compressed* img_compressed;

   int sockfd;

   pthread_t thread;
   sem_t mutex_frame;
   pthread_mutex_t mutex_write;
} MyData;




static void copy_received(type_groupe* g);
static void swap_buffers(type_groupe* g);
static int __recv_from(type_groupe* g, void* buf);
static void reset_all_neurons(int gpe, int which_s);
static int wait_for(sem_t* mut, int nbseconds);
static int create_socket(MyData* data);
static void* receive_thread(void* arg);

////////////////////
// MAIN FUNCTIONS //
////////////////////

void new_recv_udp(int gpe)
{
   int nblinks = 0, l = 0;

   type_groupe *g = &def_groupe[gpe];
   MyData* data = g->data = ALLOCATION(MyData);
   data->blocking = FALSE;
   data->compressed = FALSE;
   data->jpeg_uncompressor = NULL;
   data->img_compressed = NULL;
   data->which_s = -1; /* By default all outputs are sent */
   data->neurons_nb = 0; 	// Lazy initialized
   data->ext_size = 0; 		// Lazy initialized
   data->buf_size = data->buf_max_size = PACKET_SIZE;
   data->buf2 = data->buf_out = MANY_ALLOCATIONS(PACKET_SIZE, unsigned char);
   data->buf1 = data->buf_in = MANY_ALLOCATIONS(PACKET_SIZE, unsigned char);
   data->raz_nbIteration = -1;
   sem_init(&data->mutex_frame, 0, 0);
   pthread_mutex_init(&data->mutex_write, NULL);


   // Process links
   for (l = find_input_link(gpe, nblinks); l != -1; l = find_input_link(gpe, nblinks++))
   {
      char *link_name = liaison[l].nom;

      if (prom_getopt(link_name, "-block", NULL ) == 1) data->blocking = 1;
      if (prom_getopt_int(link_name,"-raz",&data->raz_nbIteration)==1) data->raz_nbIteration = 1;
      if (prom_getopt_int(link_name,"-p",&data->port)==1) EXIT_ON_ERROR("Option -p expects a port number");
      nblinks++;
   }

   data->raz_counter = data->raz_nbIteration;

   // Create socket
   if (!create_socket(data)) EXIT_ON_ERROR("Can't bind recv_udp to UDP port");
   pthread_create(&data->thread, NULL, receive_thread, g);
}



void function_recv_udp(int gpe)
{
   type_groupe *g = &def_groupe[gpe];
   MyData* data = (MyData*)g->data;

   // Handle non-available input (blocking, raz, etc...)
   if (data->blocking)  					            // If in blocking mode,
   {
      if (!wait_for(&data->mutex_frame, 3))             // wait for at most 3s if there's nothing to read
      {
         PRINT_WARNING("Group '%s': Timeout waiting for data", g->no_name);
         return;
      }
   }
   else if (sem_trywait(&data->mutex_frame) != 0)   		// If Nothing to read and not blocking
   {
      if (data->raz_nbIteration < 0) return;				// If not in RAZ mode, just keep current state
      if (data->raz_counter-- <= 0)                       // but if in RAZ mode,
      {
         reset_all_neurons(gpe, data->which_s);          // Reset all neurons after raz_nbIteration,
         data->raz_counter = data->raz_nbIteration;
      }
      return;
   }

   // Copy data from buffers to group's neurons and ext
   copy_received(g);
}

void destroy_recv_udp(int gpe)
{
   type_groupe *g = &def_groupe[gpe];
   MyData* data = (MyData*)g->data;
   if (data->buf1) free(data->buf1);
   if (data->buf2) free(data->buf2);
   free(data);
   g->data = NULL;
   close(data->sockfd);
}


////////////////////////
// INTERNAL FUNCTIONS //
////////////////////////

int update_from_header(MyData* data, UDP_FRAME_SPECIAL_HEADER* header)
{
   size_t new_size = header->nb_neurons * (header->which_s==-1 ? 3 : 1) * sizeof(float) + header->extsize;
   if (data->buf_size != new_size)
   {
      pthread_mutex_lock(&data->mutex_write);
      data->neurons_nb = header->nb_neurons;
      data->which_s = header->which_s;
      data->ext_size = header->extsize;
      data->compressed = header->bCompressed;
      if (ROUND_MULTIPLE(new_size,PACKET_SIZE) > data->buf_max_size)
      {
         PRINT_WARNING("REALLOCATING : %u <> %u", new_size, data->buf_max_size);
         PRINT_WARNING("NEW SIZE : %u, round to %u", new_size, ROUND_MULTIPLE(new_size,PACKET_SIZE));
         MANY_REALLOCATIONS(&data->buf1, ROUND_MULTIPLE(new_size,PACKET_SIZE));
         MANY_REALLOCATIONS(&data->buf2, ROUND_MULTIPLE(new_size,PACKET_SIZE));
         data->buf_in = data->buf1;
         data->buf_out = data->buf2;
         data->buf_max_size = ROUND_MULTIPLE(new_size,PACKET_SIZE);
      }
      data->buf_size = new_size;
      pthread_mutex_unlock(&data->mutex_write);
      return TRUE;
   }
   return FALSE;
}

/** recv : last byte = msg type (BEGIN / NORMAL) */
void* receive_thread(void* arg)
{
   int n_to_read;
   type_groupe *g = (type_groupe*)arg;
   MyData* data = (MyData*)g->data;
   unsigned char* buf;
   int n_read;
   UDP_FRAME_SPECIAL_HEADER* header;

   sem_trywait(&data->mutex_frame);

   for (;;)
   {
      buf = data->buf_in;
      do {	n_read = __recv_from(g, buf);	}
      while (buf[n_read-1]!=UDPBYTE_BEGIN);   // Wait for an UDPBYTE_BEGIN message type

frame_begin:
      // sem_trywait(&data->mutex_frame); ????


      // 1) Parse frame header
      header = (UDP_FRAME_SPECIAL_HEADER*) &buf[n_read - sizeof(UDP_FRAME_SPECIAL_HEADER)];
      n_read -= sizeof(UDP_FRAME_SPECIAL_HEADER);

      if (update_from_header(data, header)) buf = data->buf_in;


      // 2) Fill buffer
      n_to_read = data->buf_size - n_read;
      buf += n_read;
      while (n_to_read > 0)
      {
         n_read = __recv_from(g, buf);
         if (buf[n_read-1]==UDPBYTE_BEGIN)
         {
            // ERROR : TOO FEW DATA FOR A FRAME --> skip this one
            //	PRINT_WARNING("TOO FEW DATA (should receive %i more bytes)", n_to_read);
//				swap_buffers(g);
//				sem_post(&data->mutex_frame);

            memcpy(data->buf_in, buf, PACKET_SIZE);
            buf = data->buf_in;
            goto frame_begin;
         }
         n_read--;
         n_to_read -= n_read;
         buf += n_read;
      }

      // 3) Signal new frame
      swap_buffers(g);
      sem_post(&data->mutex_frame);
   }

   return 0;
}


void copy_received(type_groupe* g)
{
   type_neurone *neurons = &neurone[g->premier_ele];
   MyData* data = (MyData*)g->data;
   int s,i;
   int* images_size;
   float* buffer_neurons;

   pthread_mutex_lock(&data->mutex_write);


   // Fill neurons
   buffer_neurons = (float*)data->buf_out;

   if (data->which_s==-1) for (i=0; i<data->neurons_nb; i++)
      {
         neurons->s = *(buffer_neurons++);
         neurons->s1 = *(buffer_neurons++);
         neurons->s2 = *(buffer_neurons++);
         neurons++;
      }
   else if (data->which_s==0) for (i=0; i<data->neurons_nb; i++) { neurons->s = *(buffer_neurons++); neurons++; }
   else if (data->which_s==1) for (i=0; i<data->neurons_nb; i++) { neurons->s1 = *(buffer_neurons++); neurons++; }
   else if (data->which_s==2) for (i=0; i<data->neurons_nb; i++) { neurons->s2 = *(buffer_neurons++); neurons++; }

   // Fill ext if received one
   if (data->ext_size > 0)
   {
      unsigned char* buf_ext = (unsigned char*) buffer_neurons;

      // Lazy allocate ext
      prom_images_struct* img = (prom_images_struct*) g->ext;
      if (img == NULL)
      {
         g->ext = img = ALLOCATION(prom_images_struct);
         memcpy(img, buf_ext, PROM_IMAGES_STRUCT_HEAD_SIZE);
         for (i = 0; i < img->image_number; i++)
            img->images_table[i] = MANY_ALLOCATIONS(img->sx*img->sy*img->nb_band, unsigned char);
      }

      buf_ext += PROM_IMAGES_STRUCT_HEAD_SIZE;
      if (!data->compressed)
      {
         s = img->sx*img->sy*img->nb_band;
         for (i = 0; i < img->image_number; i++)
         {
            memcpy(img->images_table[i], buf_ext, s);
            buf_ext += s;
         }
      }
      else
      {
         // Eventually uncompress incoming compressed ext images

         if (!data->jpeg_uncompressor)
         {
            data->img_compressed = ALLOCATION(prom_images_struct_compressed);
            memcpy(&data->img_compressed->super, img, PROM_IMAGES_STRUCT_HEAD_SIZE);
            for (i = 0; i < img->image_number; i++)
               data->img_compressed->super.images_table[i] = MANY_ALLOCATIONS(img->sx*img->sy*img->nb_band, unsigned char);
            data->jpeg_uncompressor = prom_jpeg_uncompressor_create(data->img_compressed, img);
         }

         for (i = 0; i < img->image_number; i++)
         {
            images_size = (int*)buf_ext;
            data->img_compressed->images_size[i] = *images_size++;
            buf_ext = (unsigned char*) images_size;
            memcpy(data->img_compressed->super.images_table[i], buf_ext, data->img_compressed->images_size[i]);
            buf_ext += data->img_compressed->images_size[i];
         }

         prom_jpeg_uncompressor_uncompress(data->jpeg_uncompressor);
      }
   }

   pthread_mutex_unlock(&data->mutex_write);
}


int wait_for(sem_t* mut, int nbseconds)   // TODO : REPLACE WITH SEMAPHORE
{
   struct timespec timeout;
   struct timeval now;
   gettimeofday(&now, NULL );
   timeout.tv_sec = now.tv_sec + nbseconds; /* timeout 3s */
   timeout.tv_nsec = now.tv_usec * 1000;

   return sem_timedwait(mut, &timeout)==0;
}


void reset_all_neurons(int gpe, int which_s)
{
   type_neurone *neurons = &neurone[def_groupe[gpe].premier_ele];
   size_t j = def_groupe[gpe].nbre;

   if (which_s==-1) while (j--)
      {
         neurons[j].s = 0;
         neurons[j].s1 = 0;
         neurons[j].s2 = 0;
      }
   else if (which_s==0) while (j--) neurons[j].s = 0;
   else if (which_s==1) while (j--) neurons[j].s1 = 0;
   else if (which_s==2) while (j--)	neurons[j].s2 = 0;
}

/** Swap in and out buffers */
void swap_buffers(type_groupe* g)
{
   void* tmp;
   MyData* data = (MyData*)g->data;
   pthread_mutex_lock(&data->mutex_write);
   tmp = data->buf_out;
   data->buf_out = data->buf_in;
   data->buf_in = tmp;
   pthread_mutex_unlock(&data->mutex_write);
}


////////////
// SOCKET //
////////////

int create_socket(MyData* data)
{
   struct addrinfo hints;
   struct addrinfo  *servinfo, *p;
   int rv;
   char port[10]; sprintf(port, "%u", data->port);
   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;
   hints.ai_flags = AI_PASSIVE;
   if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) EXIT_ON_ERROR("Can't open recv UDP socket");

   for (p = servinfo; p != NULL; p = p->ai_next)
   {
      if ((data->sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) continue;
      if (bind(data->sockfd, p->ai_addr, p->ai_addrlen) == -1)
      {
         close(data->sockfd);
         continue;
      }
      break;
   }
   freeaddrinfo(servinfo);
   return p!=NULL;
}

int __recv_from(type_groupe* g, void* buf)
{
   MyData* data = (MyData*)g->data;
   return recvfrom(data->sockfd, buf, PACKET_SIZE, 0, NULL, NULL);
}

