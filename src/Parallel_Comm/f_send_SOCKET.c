/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_send_SOCKET.c 
\brief Envoie les valeurs d'un groupe de neurones

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    Envoie les valeurs d'un groupe de neurones 
    pour parcourir les neurones du groupe d'entree, on a 2 possiblites :
    ou bien, on n'a un neurone dans ce groupe-ci et on va lire les neurones du groupe d'entree directement :
      deb=def_groupe[gpe_entree].premier_ele;
      nbre=def_groupe[gpe_entree].nbre;

     ou bien ou considere que ici on a autant de neurones que dans le groupe d'entree, et que leur valeur vaut celle du groupe d'entree 
     => appel activite = transmission sur les liens de s, s1 et s2
        appel algo = cette fonction qui transmet l'info avec SOCKET 

        si on considere le 1er cas : gpe_entree contient les neurones 
        taille_ext est la taille de ext
            numero_tache est le numero de la tache qui recoit
            numero_message est le numero du message envoye 
     
Macro:
-none 

Local variables:
-int tx_thread_count
-Task* TxList

Global variables:
-none

Internal Tools:
-trouver_la_tache()
-malloc_SOCKET()
-envoie()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libParallel_Comm.h>



void new_send_SOCKET(int num)
{
	new_send(num);
}

void function_send_SOCKET(int num)
{
	function_send(num);
}


void destroy_send_SOCKET(int num)
{
	destroy_send(num);
}
