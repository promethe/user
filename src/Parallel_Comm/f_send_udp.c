/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * f_send_udp.c
 *
 *  Created on: 3 juil. 2014
 *      Author: jfellus
 */

#include "../../../prom_tools/include/basic_tools.h"
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <limits.h>
#include <pthread.h>
#include <gen_types.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include "f_recv_send_udp.h"
#include <time.h>
#include "prom_kernel/include/prom_jpeg_compressor.h"


///////////
// TYPES //
///////////

typedef struct
{
   int port;
   char ip[255];
   struct addrinfo* target_addr;

   type_groupe* input_group;

   boolean compressed;
   prom_jpeg_compressor* jpeg_compressor;
   int compressed_size;
   int compression_quality;
   int which_s;

   int neurons_nb, ext_size;
   int w,h,bands_nb;

   unsigned char buf[PACKET_SIZE];

   int sockfd;
} MyData;



void send_neurons(type_groupe *g, unsigned char* packet_buf, UDP_FRAME_SPECIAL_HEADER* header, unsigned char** curbuf_out);
void send_ext_data(type_groupe* g, unsigned char* packet_buf, unsigned char* data, int n, UDP_FRAME_SPECIAL_HEADER* header, unsigned char** curbuf);
static int send_packet(int sockfd, struct addrinfo* addr, void* buf);
static int create_socket(MyData* data);


////////////////////
// MAIN FUNCTIONS //
////////////////////

void new_send_udp(int gpe)
{
   int nblinks = 0, l = 0;

   type_groupe *g = &def_groupe[gpe];
   MyData* data = g->data = ALLOCATION(MyData);
   data->compressed = FALSE;
   data->which_s = -1; /* By default all outputs are sent */
   data->neurons_nb = 0;
   data->ext_size = 0; // Lazy init
   data->input_group = NULL;
   data->jpeg_compressor = NULL;
   data->compression_quality = 99;


   // Process links
   for (l = find_input_link(gpe, nblinks); l != -1; l = find_input_link(gpe, nblinks++))
   {
      char *link_name;
      // Consider all links for neurons transmission (priority to neural links) ... AB:Pourquoi ?
      if (liaison[l].type!=5)
      {
         data->input_group = &def_groupe[liaison[l].depart];
         continue;
      }
      // ... but only consider algorithmic links for ext transmission
      link_name = liaison[l].nom;
      data->input_group = &def_groupe[liaison[l].depart];

      // Options must be set on the algorithmic link
      if (prom_getopt_int(link_name, "-j", &data->compression_quality ) != 0) {data->compressed = TRUE;}
      if (prom_getopt(link_name, "-h", data->ip ) == 1) EXIT_ON_ERROR("Option -h expects a valid ip address");
      if (prom_getopt_int(link_name, "-s", &data->which_s ) == 1) data->which_s = -1;
      if (prom_getopt_int(link_name,"-p",&data->port)==1) EXIT_ON_ERROR("Option -p expects a port number");
      nblinks++;
   }
   if (!data->input_group) EXIT_ON_ERROR("f_send_udp must have an input algorithmic link");

   data->neurons_nb = data->input_group->taillex*data->input_group->tailley;

   // Create socket
   if (!create_socket(data)) EXIT_ON_ERROR("Can't create send_udp UDP socket");
}



void function_send_udp(int gpe)
{
   int i;
   int ims;
   type_groupe *g = &def_groupe[gpe];
   MyData* data = (MyData*)g->data;
   prom_images_struct* img =data->input_group->ext;
   UDP_FRAME_SPECIAL_HEADER header;
   unsigned char* curbuf;

   // Eventually compress ext
   if (img && data->compressed)
   {
      prom_images_struct_compressed* images_compressed;
      if (!data->jpeg_compressor) data->jpeg_compressor = prom_jpeg_compressor_create(img, NULL, data->compression_quality);
      images_compressed = prom_jpeg_compressor_get_out(data->jpeg_compressor);
      prom_jpeg_compressor_compress(data->jpeg_compressor);
      data->compressed_size = 0;
      for (i=0; i<images_compressed->super.image_number; i++) data->compressed_size += images_compressed->images_size[i] + sizeof(*images_compressed->images_size);
   }

   // Header for first packet
   header.MAGIC_CHAR = UDPBYTE_BEGIN;
   header.nb_neurons = data->neurons_nb;
   header.extsize = data->ext_size = !img ? 0 : !data->compressed ?
                                     (PROM_IMAGES_STRUCT_HEAD_SIZE + img->nb_band * img->image_number * img->sx * img->sy) :
                                     (PROM_IMAGES_STRUCT_HEAD_SIZE + data->compressed_size);
   header.which_s = data->which_s;
   header.bCompressed = data->compressed;
   memcpy(&data->buf[PACKET_SIZE-sizeof(UDP_FRAME_SPECIAL_HEADER)], &header, sizeof(UDP_FRAME_SPECIAL_HEADER));

   // Send neurons
   send_neurons(g, data->buf, &header, &curbuf);

   // Send ext
   if (img)
   {
      ims = img->sx * img->sy * img->nb_band * sizeof(unsigned char);
      send_ext_data(g, data->buf, (unsigned char*)img, PROM_IMAGES_STRUCT_HEAD_SIZE, &header, &curbuf);
      if (data->compressed)
      {
         prom_images_struct_compressed* images_compressed = prom_jpeg_compressor_get_out(data->jpeg_compressor);
         for (i=0; i<img->image_number; i++)
         {
            send_ext_data(g, data->buf, (unsigned char*) &images_compressed->images_size[i], sizeof(unsigned int), &header, &curbuf);
            send_ext_data(g, data->buf, images_compressed->super.images_table[i], images_compressed->images_size[i], &header, &curbuf);
         }
      }
      else
      {
         for (i=0; i<img->image_number; i++)
         {
            send_ext_data(g, data->buf, img->images_table[i], ims, &header, &curbuf);
         }
      }
   }

   if (curbuf != data->buf)	send_packet(((MyData*)g->data)->sockfd, ((MyData*)g->data)->target_addr, data->buf);
   usleep(1000); // AB:Est-ce nécessaire ? Pourquoi 1000
}

void destroy_send_udp(int gpe)
{
   type_groupe *g = &def_groupe[gpe];
   MyData* data = (MyData*)g->data;
   if (data->jpeg_compressor) prom_jpeg_compressor_destroy(data->jpeg_compressor);
   freeaddrinfo(data->target_addr);
   free(data);
   g->data = NULL;
   close(data->sockfd);
}


////////////////////////
// INTERNAL FUNCTIONS //
////////////////////////


int create_socket(MyData* data)
{
   struct addrinfo hints;
   struct addrinfo  *servinfo, *p;
   int rv;
   char port[10]; sprintf(port, "%u", data->port);
   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;
   PRINT_WARNING("IP is %s(%s)\n", data->ip, port);
   if ((rv = getaddrinfo(data->ip, port, &hints, &servinfo)) != 0) EXIT_ON_ERROR("Can't open send UDP socket");

   for (p = servinfo; p != NULL; p = p->ai_next)
   {
      if ((data->sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
      {
         PRINT_WARNING("talker: socket");
         continue;
      }
      break;
   }

   data->target_addr = p;
   PRINT_WARNING("Target addr : %lu", data->target_addr);
   return p!=NULL;
}

int send_packet(int sockfd, struct addrinfo* addr, void* buf)
{
   return sendto(sockfd, buf, PACKET_SIZE, 0, addr->ai_addr, addr->ai_addrlen);
}

void send_neurons(type_groupe *g, unsigned char* packet_buf, UDP_FRAME_SPECIAL_HEADER* header, unsigned char** curbuf_out)
{
   int i;
   float* neuronsbuf = (float*)packet_buf;
   MyData *data = g->data;
   type_neurone *neurons = &neurone[data->input_group->premier_ele];

   for (i=0; i< (int)data->neurons_nb; i++)
   {
      if (header->which_s==-1)
      {
         *(neuronsbuf++) = neurons[i].s;
         *(neuronsbuf++) = neurons[i].s1;
         *(neuronsbuf++) = neurons[i].s2;
      }
      else if (header->which_s==0) *(neuronsbuf++) = neurons[i].s;
      else if (header->which_s==1) *(neuronsbuf++) = neurons[i].s1;
      else if (header->which_s==2) *(neuronsbuf++) = neurons[i].s2;

      if ((unsigned char*)neuronsbuf >= &packet_buf[PACKET_SIZE - UDP_FRAME_SPECIAL_HEADER_SIZE(*header)])
      {
         send_packet(((MyData*)g->data)->sockfd, ((MyData*)g->data)->target_addr, packet_buf);
         neuronsbuf = (float*)packet_buf;
         header->MAGIC_CHAR = packet_buf[PACKET_SIZE-1] = 0;
      }
   }
   *curbuf_out = (unsigned char*) neuronsbuf;
}


void send_ext_data(type_groupe* g, unsigned char* packet_buf, unsigned char* data, int n, UDP_FRAME_SPECIAL_HEADER* header, unsigned char** curbuf)
{
   int nn;
   while (n > 0)
   {
      nn = MIN(n, (int)(PACKET_SIZE - UDP_FRAME_SPECIAL_HEADER_SIZE(*header) - (*curbuf-packet_buf)));
      memcpy(*curbuf, data, nn);
      *curbuf += nn;
      data += nn;
      n -= nn;
      if (*curbuf >= &packet_buf[PACKET_SIZE - UDP_FRAME_SPECIAL_HEADER_SIZE(*header)])
      {
         send_packet(((MyData*)g->data)->sockfd, ((MyData*)g->data)->target_addr, packet_buf);
         *curbuf = packet_buf;
         header->MAGIC_CHAR = packet_buf[PACKET_SIZE-1] = 0;
      }
   }
}


