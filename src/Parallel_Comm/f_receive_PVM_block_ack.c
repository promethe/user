/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_receive_SOCKET_ack_block.c 
\brief Envoie un ack signalant qu'on est pret a recevoir et attend de facon bloquante les donnees. La transmission est synchrone.

Author: Pierre 
Created: XX/XX/2002
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   fonction de reception des messages SOCKET.
   Utile quand le sender a une frequence de mise a
   jour beaucoup plus rapide que le receiver (decalage de l'info
   avec function_receive_SOCKET_(non_)block_ack).  
     - envois d'un ack au sender
     - attente bloquante d'un message du sender
     - lecture du message
     -calcul des activites
Macro:
-none 

Local variables:
-int tx_thread_count
-Task * TxList
-pthread_rwlock_t rwlock
-int ack
-sem_t lecture
-queue * myqueue

Global variables:
-none

Internal Tools:
-free_SOCKET()
-rempli()
-retire()
-malloc_SOCKET()
-trouver_la_tache()


External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libParallel_Comm.h>



void new_receive_PVM_block_ack(int num)
{
	int num_liaison;
	char tmp[255];
	int i = 0;

	  while ((num_liaison = find_input_link(num, i++)) >= 0)
	    {

	      /* On ne tien pas compte des liens dont le nom commence par sync */
	      if ((strncmp(liaison[num_liaison].nom, "sync", 4)) == 0)
		continue;

	      memset(tmp, 0, 255 * sizeof(char));
	      memcpy(tmp, liaison[num_liaison].nom, strlen(liaison[num_liaison].nom));
	      sprintf(liaison[num_liaison].nom, "%s-block", tmp);
	    }
	new_recv(num);
}


void function_receive_PVM_block_ack(int num)
{
	function_recv(num);
}

void destroy_receive_PVM_block_ack(int num)
{
	destroy_recv(num);
}


