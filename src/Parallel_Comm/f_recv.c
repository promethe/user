/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\defgroup f_recv f_recv
\ingroup libParrallel_Com

\brief communication between promethe scripts (reception)

\details

\section Description
 La communication est definie par le nom sur le lien d'entree (algo). Il doit y avoir un groupe f_send portant le meme nom (meme connection).
 Recupere les differentes valeurs s, s1, s2 sur le groupe qui transmis ie le groupe en entree du groupe f_send. Attention, il faut le meme nombre de neurones entre le groupe f_recv et le groupe en entree.

\section Options

Incomplet (?)
- -block : le groupe reste bloque tant qu'un message n'a pas ete recu.

- -raz : Met la sortie du f_recv a 0 si aucun message n'est recu. On
peut passer en parametre un entier donnant le nombre d'iteration sans
message au bout duquel la sortie doit etre resettee. ATTENTION cela
n'affecte pas le .ext i.e. seuls les neurones sont remis a zero.

- -debug : selectionner ou non l'affichage du debug du reseau a la
reception. Par defaut, l'affichage se fait toute les 30 sec. On peut
modifier (a la ms pres) la periode utilisee pour l'affichage. Si
echange de ext alors affiche un debug specifique sur les ext.

- -EXT : gestion du ext avec extraction via une structure separee.

- -next (?) : a chaque passage, attend la reception du prochain
message avant de continuer.

\section Liens
voir f_send

\file  
\ingroup f_recv
\brief communication between promethe scripts (reception)

**/
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>

/* Promethe Internal Headers */
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

#include <virtual_link.h>
#include "consts.h"
#include "prom_tools/include/prom_bus.h"

#define USE_EXT 1
#define SIZE_OF_TIME_STRING 16


static void read_neurone(int num_groupe)
{
  char* pt;
  unsigned int size;
  unsigned int type;
  unsigned int size_neurone;
  unsigned int nb_neurone;
  unsigned int nb_macro;
  unsigned int incr;
  unsigned int first;
  int index;
  virtual_link_t *vlink = NULL;
  
  vlink = (virtual_link_t *) def_groupe[num_groupe].data;
  
  /* Calcul de la taille des neurones */
  nb_neurone = (unsigned int) def_groupe[vlink->num_groupe].nbre;
  nb_macro = def_groupe[vlink->num_groupe].taillex * def_groupe[vlink->num_groupe].tailley;
  size_neurone = 3 * sizeof(float) * nb_macro;
  
  /* On met le pointeur apres le flag */
  pt = vlink->data + sizeof(unsigned int);
  
  do
  {
    size = *(unsigned int *) pt;
    pt += sizeof(unsigned int);
    
    type = *(unsigned int *) pt;
    pt += sizeof(unsigned int);
    
    if (type == NEURONE)
    {
      /* Verification de la taille */
      if (size_neurone != (size - (2 * sizeof(unsigned int))))
      {
	EXIT_ON_ERROR("f_recv(%s) : read_neurone() : Wrong size\n", def_groupe[num_groupe].no_name);
      }
      
      /* Traitement des neurones */
      incr = nb_neurone / nb_macro;
      
      first = def_groupe[vlink->num_groupe].premier_ele;
      for (index = incr - 1; (unsigned int)index < nb_neurone; index += incr)
      {
	neurone[first + index].s = *(float *) pt;
	pt += sizeof(float);
	neurone[first + index].s1 = *(float *) pt;
	pt += sizeof(float);
	neurone[first + index].s2 = *(float *) pt;
	pt += sizeof(float);
      }
    }
    else
    {
      pt += (size - (2 * sizeof(unsigned int)));
    }
  } while (type != NEURONE);
}

static void read_image(int num_groupe)
{
  char* pt;
  unsigned int size;
  unsigned int size_img;
  unsigned int type;
  prom_images_struct *prom_images = NULL;
  unsigned int sx = 0;
  unsigned int sy = 0;
  unsigned int nb_band = 0;
  unsigned int image_number = 0;
  int index;
  virtual_link_t *vlink = NULL;
  
  vlink = (virtual_link_t *) def_groupe[num_groupe].data;
  
  /* On met le pointeur apres le flag */
  pt = vlink->data + sizeof(unsigned int);
  
  do
  {
    size = *(unsigned int *) pt;
    pt += sizeof(unsigned int);
    
    type = *(unsigned int *) pt;
    pt += sizeof(unsigned int);
    
    if (type == IMAGE)
    {
      if (size > (2 * sizeof(unsigned int)))
      {
	sx = *(unsigned int*) pt;
	pt += sizeof(unsigned int);
	
	sy = *(unsigned int*) pt;
	pt += sizeof(unsigned int);
	
	nb_band = *(unsigned int*) pt;
	pt += sizeof(unsigned int);
	
	image_number = *(unsigned int*) pt;
	pt += sizeof(unsigned int);
	
	if (vlink->fft)
	{
	  size_img = sx * (sy + 2) * sizeof(float);
	}
	else
	{
	  size_img = sx * sy * nb_band;
	}
	
#ifdef USE_EXT
	if (!def_groupe[vlink->num_groupe].ext)
	{
	  prom_images
	    = calloc_prom_image(image_number, sx, sy, nb_band);
	  def_groupe[vlink->num_groupe].ext = (void *) prom_images;
	}
	else
	  prom_images
	    = (prom_images_struct *) def_groupe[vlink->num_groupe].ext;
	
#else
	if (!def_groupe[vlink->num_groupe].video_ext)
	{
	  prom_images = calloc_prom_image(image_number, sx, sy, nb_band);
	  def_groupe[vlink->num_groupe].video_ext = (void *) prom_images;
	}
	else
	  prom_images = (prom_images_struct *)def_groupe[vlink->num_groupe].video_ext;
#endif /* USE_EXT */
	
	for (index = 0; (unsigned int)index < image_number; index++)
	{
	  if (prom_images->images_table[index])
	  {
	    memcpy(prom_images->images_table[index], pt, size_img);
	    pt += size_img;
	  }
	}
      }
    }
    else
    {
      pt += (size - (2 * sizeof(unsigned int)));
    }
  } while (type != IMAGE);
}

static void read_image_from_data_ext(int num_groupe)
{
  char* pt;
  unsigned int size;
  unsigned int size_img;
  unsigned int type;
  prom_images_struct *prom_images = NULL;
  unsigned int sx = 0;
  unsigned int sy = 0;
  unsigned int nb_band = 0;
  unsigned int image_number = 0;
  int index;
  virtual_link_t *vlink = NULL;
  
  vlink = (virtual_link_t *) def_groupe[num_groupe].data;
  
  /* On met le pointeur apres le flag */
  pt = vlink->data_ext;
  
  size = *(unsigned int *) pt;
  pt += sizeof(unsigned int);
    
  type = *(unsigned int *) pt;
  pt += sizeof(unsigned int);
    
  if (type != IMAGE)
  {
    EXIT_ON_ERROR("Type is not IMAGE in read_image_from_data_ext !!!");
  }

  if (size > (2 * sizeof(unsigned int)))
  {
    sx = *(unsigned int*) pt;
    pt += sizeof(unsigned int);
      
    sy = *(unsigned int*) pt;
    pt += sizeof(unsigned int);
      
    nb_band = *(unsigned int*) pt;
    pt += sizeof(unsigned int);
      
    image_number = *(unsigned int*) pt;
    pt += sizeof(unsigned int);
      
    if (vlink->fft)
    {
      size_img = sx * (sy + 2) * sizeof(float);
    }
    else
    {
      size_img = sx * sy * nb_band;
    }
      
#ifdef USE_EXT
    if (!def_groupe[vlink->num_groupe].ext)
    {
      prom_images
	= calloc_prom_image(image_number, sx, sy, nb_band);
      def_groupe[vlink->num_groupe].ext = (void *) prom_images;
    }
    else
      prom_images
	= (prom_images_struct *) def_groupe[vlink->num_groupe].ext; 
#else
    if (!def_groupe[vlink->num_groupe].video_ext)
    {
      prom_images = calloc_prom_image(image_number, sx, sy, nb_band);
      def_groupe[vlink->num_groupe].video_ext = (void *) prom_images;
    }
    else
      prom_images = (prom_images_struct *)def_groupe[vlink->num_groupe].video_ext;
#endif /* USE_EXT */
	
    for (index = 0; (unsigned int)index < image_number; index++)
    {
      if (prom_images->images_table[index])
      {
	memcpy(prom_images->images_table[index], pt, size_img);
	pt += size_img;
      }
    }
  }
}

static void read_data(int num_groupe)
{
  unsigned int flag, update_ext_debug_recv=0;
  int retval;
  char* pt;
  char time_string[SIZE_OF_TIME_STRING];
  time_t now;
  
  virtual_link_t *vlink = NULL;

  (void) retval; // (unused)

  vlink = (virtual_link_t *) def_groupe[num_groupe].data;
  
  if (!vlink->data)
  {
    if (! (vlink->error_flag & NO_DATA_TO_READ))
    {
      time(&now);
      
      strftime(time_string, SIZE_OF_TIME_STRING, "%Hh%M", localtime(&now));
      
      prom_bus_send_message("f_recv(%s) : read_data() : No data to read for %s\n", def_groupe[num_groupe].no_name, vlink->name);
      cprints("%s: f_recv(%s): No data to read on link %s\n", time_string, def_groupe[num_groupe].no_name, vlink->name);
      vlink->error_flag |= NO_DATA_TO_READ; /* Ajout du flag */
    }
    return;
  }
  else
  {
    if (vlink->error_flag & NO_DATA_TO_READ)
    {
      time(&now);
      
      strftime(time_string, SIZE_OF_TIME_STRING, "%Hh%M", localtime(&now));
      prom_bus_send_message("f_recv(%s) : read_data() : data ok on link %s\n", def_groupe[num_groupe].no_name, vlink->name);
      cprints("%s: f_recv(%s) : data ok on link %s\n", time_string, def_groupe[num_groupe].no_name, vlink->name);
      vlink->error_flag ^= NO_DATA_TO_READ; /* Suppression du flag */
    }
  }
  
  if ((errno = pthread_mutex_lock(&vlink->mut_data)) != 0)
  {
    EXIT_ON_ERROR("f_recv : read_data() : pthread_lock()");
  }
  
  pt = vlink->data;
  
  /* On recupere le flag */
  flag = *(unsigned int *) pt;
  pt += sizeof(unsigned int);
  
  switch (flag)
  {
  case DATA:
    read_neurone(num_groupe);
    /** if get ext from data_ext + ext was already once recved */
    if(vlink->read_data_ext>-1 && vlink->data_ext!=NULL ) {
      read_image_from_data_ext(num_groupe);
      update_ext_debug_recv=1;
    }
    break;
    
  case DATA_IMG:
    read_neurone(num_groupe);
    read_image(num_groupe);
    update_ext_debug_recv=1;
    break;
    
  case DATA_ACK:
    read_neurone(num_groupe);
    /** if get ext from data_ext + ext was already once recved */
    if(vlink->read_data_ext>-1  && vlink->data_ext!=NULL) {
      read_image_from_data_ext(num_groupe);
      update_ext_debug_recv=1;
    }
    if (vlink->read_data == 0)
    {
      retval = virtual_link_send_ack(vlink);
    }
    break;
    
  case DATA_ACK_IMG:
    read_neurone(num_groupe);
    read_image(num_groupe);
    update_ext_debug_recv=1;
    if (vlink->read_data == 0)
    {
      retval = virtual_link_send_ack(vlink);
    }
    break;
  }
    
  /** update debug network with read time stamp */
  recv_server_update_debug(vlink, update_ext_debug_recv);

  /* Si le message n'a jamais ete lu on indique qu il est lu */
  if (vlink->read_data == 0)
    vlink->read_data = 1;
  
  if(DATA_ACK_IMG || DATA_IMG || (vlink->read_data_ext>-1 && vlink->data_ext!=NULL)) {
    /* Si le message ext n'a jamais ete lu on indique qu il est lu */
    if (vlink->read_data_ext == 0)
      vlink->read_data_ext = 1;
  }

  if ((errno = pthread_mutex_unlock(&vlink->mut_data)) != 0)
  {
    EXIT_ON_ERROR("f_recv : read_data() : pthread_unlock()");
  }
  
}

static void neurone_raz(int num_groupe)
{
  int first;
  int index;
  
  first = def_groupe[num_groupe].premier_ele;
  for (index = 0; index < def_groupe[num_groupe].nbre; index++)
    neurone[first + index].s = neurone[first + index].s1 = neurone[first + index].s2 = 0.f;
}

void new_recv(int num_groupe)
{
  
  virtual_link_t* vlink = NULL;
  int num_liaison = 0;
  int i = 0,ret=-1;
  char param[255];
  
  if (!def_groupe[num_groupe].data)
  {
    /* Recuperer le nom sur le lien */
    while ((num_liaison = find_input_link(num_groupe, i++)) >= 0)
    {
      /* On ne tien pas compte des liens dont le nom commence par sync */
      if ((strncmp(liaison[num_liaison].nom, "sync", 4)) == 0)
	continue;
      
      /* appel fonction rempli virtual link : retourne pointeur sur le vitual link */
      vlink = virtual_links_set_flags(liaison[num_liaison].nom);
      if (!vlink)
      {
	EXIT_ON_ERROR("f_recv(%s) : new_recv() : Virtual link %s not found!\n", def_groupe[num_groupe].no_name, liaison[num_liaison].nom);
      }

      /** set debug period for recv server */
      ret = prom_getopt(liaison[num_liaison].nom,"debug",param); 
      if(ret==1) {
	recv_server_set_debug(vlink,-1);
      }
      if(ret==2) {
	recv_server_set_debug(vlink,atoi(param));
      }

      /** set specific EXT management for recv server */
      ret = prom_getopt(liaison[num_liaison].nom,"EXT",param); 
      if(ret>0) {
	recv_server_init_ext_management(vlink);
      }

      def_groupe[num_groupe].data = (void *) vlink;
      
      vlink->num_groupe = num_groupe;
    }
    
    if (def_groupe[num_groupe].data == NULL)
    {
      prom_bus_send_message("f_recv : new_recv() : Input link not found for group [%i] %s !\n", num_groupe, def_groupe[num_groupe].no_name);
      EXIT_ON_ERROR("f_recv(%s) : new_recv() : Input link not found for group [%i] %s !\n", def_groupe[num_groupe].no_name, num_groupe, def_groupe[num_groupe].no_name);
    }
  }
}

void function_recv(int num_groupe)
{
  struct timeval tv_before, tv_after;
  int sval;
  int retval;
  struct timespec ts;
  struct timeval now;
  virtual_link_t *vlink = NULL;
  
  vlink = (virtual_link_t *) def_groupe[num_groupe].data;

  /** update debug info about try reading */
  recv_server_update_debug_try_reading(vlink);

  if (vlink == NULL)
  {
    prom_bus_send_message("f_recv : function_recv() : vlink pointer lost for group [%i] %s\n", num_groupe, def_groupe[num_groupe].no_name);
    kprints("f_recv(%s) : function_recv() : vlink pointer lost for group [%i] %s\n", def_groupe[num_groupe].no_name, num_groupe, def_groupe[num_groupe].no_name);
  }
  
  if (vlink->next)
  {
    /* concidere le precedent message comme lu  meme s'il n'a pas ete traite */
    vlink->read_data = 1;
    
    /* on consomme le semaphore notifiant qu'un message avait ete recu	 */
    retval = sem_getvalue(&vlink->new_message, &sval);
    
    
    if (sval > 0)
    {
      retval = sem_wait(&vlink->new_message);
      if (retval < 0)
      {
	kprints("f_recv -next : sem_wait()");
	return;
      }
    }
    
  }
  
  if (vlink->block || vlink->next)
  {
    if (vlink->timeout)
    {
      gettimeofday(&now, NULL);
      ts.tv_sec = now.tv_sec + vlink->nbsec_timeout;
      ts.tv_nsec = 0;
      dprints("f_recv(%s): Block until message arrival with timeout\n", def_groupe[num_groupe].no_name);
      retval = sem_timedwait(&vlink->new_message, &ts);
      
      if (retval != 0)
      {
	prom_bus_send_message("f_send : function_recv() : Waiting new message timeout for %s after %u seconds!\n", vlink->name, vlink->nbsec_timeout);
	kprints("f_send(%s) : function_recv() : Waiting new message timeout for %s after %u seconds!\n", def_groupe[num_groupe].no_name, vlink->name, vlink->nbsec_timeout);
	return;
      }
    }
    else
    {
      gettimeofday(&tv_before, NULL);
      dprints("f_recv(%s): Block until message arrival without timeout\n", def_groupe[num_groupe].no_name);
      retval = sem_wait(&vlink->new_message);
      dprints("\t\tf_recv(%s): message received: unblock\n", def_groupe[num_groupe].no_name);
      gettimeofday(&tv_after, NULL);
      dprints("Delta recv_wait new_message: %3.0f \n", (float)(tv_after.tv_usec - tv_before.tv_usec)/1000);
    }
  }
  else
  {
    retval = sem_trywait(&vlink->new_message);
    
    if ((vlink->raz) && (retval != 0))
    {
      vlink->nb_iter_since_raz++;
      
      if (vlink->nb_iter_since_raz > vlink->nbwait_raz)
      {
	/* remise a zero des neurones */
	neurone_raz(vlink->num_groupe);
	vlink->nb_iter_since_raz = 0;
      }
      return;
    }
    else
    {
      vlink->nb_iter_since_raz = 0;
    }
  }
  
  /* Lit les donnees */
  read_data(num_groupe);
}

void destroy_recv(int num_groupe)
{
  (void)num_groupe;
}
