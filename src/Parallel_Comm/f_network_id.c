/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>

#include <net_message_debug_dist.h>
#include "../../../prom_tools/include/basic_tools.h"
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <limits.h>

#define NETWORK_ID_MSG_GET_NAME 0
#define NETWORK_ID_MSG_GET_XML 1

typedef struct{
	int sockfd;
	struct sockaddr_in servaddr,cliaddr;
	char * buffer;
	int buffer_size;

	char *xmlfile;
	int xmlsize;
	char xml[NAME_MAX];
	char network_name[NAME_MAX];
	int running;

	pthread_t thread;
	pthread_mutex_t mutex;
} type_network_id;

static void network_init(type_network_id *my_data);
static void * network_thread(type_network_id *my_data);

static int xml_load(char *filename,char **data);

void new_network_id(int gpe){

	type_groupe *group;
	type_network_id *my_data;
	int link_id, links_nb;
	char *link_name;

	group = &def_groupe[gpe];
	my_data = ALLOCATION(type_network_id);
	group->data = my_data;

	my_data->running = 0;
	my_data->buffer_size = 100;
	my_data->buffer = MANY_ALLOCATIONS(my_data->buffer_size,char);

	network_init(my_data);

	links_nb = 0;
	for (link_id = find_input_link(gpe, links_nb); link_id != -1; link_id = find_input_link(gpe, links_nb)){

		link_name = liaison[link_id].nom;
		if (strncmp(link_name, "sync", 4) != 0){
			strcpy(my_data->network_name, link_name);
		}

		if(prom_getopt(link_name,"-xml",my_data->xml)){
			my_data->xmlsize = xml_load(my_data->xml,&my_data->xmlfile);
			if(my_data->xmlsize > 0) printf("network id: parsing file : %s... done !\n", my_data->xml);
			else printf("network id: parsing file : %s... failed (%i) !\n",my_data->xml, my_data->xmlsize);
		}

		links_nb++;
	}
}

void function_network_id(int gpe){
	type_groupe *group;
	type_network_id *my_data;
	pthread_attr_t pthread_attr;

	group = &def_groupe[gpe];
	my_data = (type_network_id *) group->data;

	if(!my_data->running){
		my_data->running = 1;
		pthread_attr_init(&pthread_attr);
		pthread_mutex_init(&my_data->mutex, NULL);
		pthread_create(&my_data->thread, &pthread_attr, (void*(*)(void*)) network_thread, my_data);
	}
}

void destroy_network_id(int gpe){
	type_groupe *group;
	type_network_id *my_data;

	group = &def_groupe[gpe];
	my_data = (type_network_id *) group->data;

	my_data->running = 0;
	pthread_mutex_lock(&my_data->mutex);
	close(my_data->sockfd);
	free(my_data->buffer);
	pthread_mutex_unlock(&my_data->mutex);
}

static void network_init(type_network_id *my_data){

	int broadcast = 1;
	struct timeval tv;

	my_data->sockfd=socket(AF_INET,SOCK_DGRAM,0);

	bzero(&my_data->servaddr,sizeof(my_data->servaddr));
	my_data->servaddr.sin_family = AF_INET;
	my_data->servaddr.sin_addr.s_addr= INADDR_BROADCAST;
	my_data->servaddr.sin_port=htons(2000);

	tv.tv_sec = 0;
	tv.tv_usec = 100000;

	setsockopt(my_data->sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));
	setsockopt(my_data->sockfd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv));

	bind(my_data->sockfd,(struct sockaddr *)&my_data->servaddr,sizeof(my_data->servaddr));
}

static void * network_thread(type_network_id *my_data){

	socklen_t len;
	int n;
	char cmd;
	char *msgdata = NULL;
	int msglen = -1;

	while(my_data->running){

		len = sizeof(my_data->cliaddr);

		pthread_mutex_lock(&my_data->mutex);

		n = recvfrom(my_data->sockfd,my_data->buffer,my_data->buffer_size,0,(struct sockaddr *)&my_data->cliaddr,&len);
		if(!my_data->running){
			pthread_mutex_unlock(&my_data->mutex);
			break;
		}

		if(n > 0){

			cmd = my_data->buffer[0];

			switch(cmd){
			case NETWORK_ID_MSG_GET_NAME:
				msgdata = my_data->network_name;
				msglen = strlen(my_data->network_name)+1;
				break;
			case NETWORK_ID_MSG_GET_XML:
				msgdata = my_data->xmlfile;
				msglen = my_data->xmlsize;
				break;
			default:
				break;
			}

			printf("network id requested from %s\n", inet_ntoa(my_data->cliaddr.sin_addr));
			if(msglen > 0){
				sendto(my_data->sockfd,msgdata,msglen,0,(struct sockaddr *)&my_data->cliaddr,sizeof(my_data->cliaddr));
			}else{
				printf("network id: cannot send id\n");
			}

		}
		pthread_mutex_unlock(&my_data->mutex);
	}

	return NULL;
}

static int xml_load(char *filename,char **data){
	unsigned int size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)
	{
		*data = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*data = (char *)malloc(size+1);
	if (size != fread(*data, sizeof(char), size, f))
	{
		free(*data);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*data)[size] = 0;
	return size;
}


