/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \file
 \brief xxxxxxxxxxxxxxxxx

 Author: xxxxxxxxxxx
 Created: xxxx
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -none

 Local variables:
 -nnoe

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: none
 - description: none
 - input expected group: Image of real point
 - where are the data?: in the image to convert

 Comments:

 Known bugs: none (yet!)

 Todo:	see the author to comment the file.


 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <Struct/prom_images_struct.h>


prom_images_struct *calloc_prom_image(int im_number, int x, int y, int b)
{
   int i;
   unsigned char *table;
   prom_images_struct *prom_image;

   /*------------------------------*/
   /*Alloc for a prom_images_struct */
   prom_image = ALLOCATION(prom_images_struct);

   /*-----------*/
   /*Store param */
   prom_image->image_number = im_number;
   prom_image->sx = x;
   prom_image->sy = y;
   prom_image->nb_band = b;

   for (i = 0; i < NB_MAX_IMAGES_IN_PROM_IMAGES; i++)
   {
      if (i < prom_image->image_number)
      {
         /*-------------------------*/
         /* Allocate for each table */
         table = (unsigned char *) calloc(x * y * b, sizeof(unsigned char));
         if (table == NULL)
         {
            kprints("Error, while allocating prom_image in calloc_prom_image\n");
            exit(EXIT_FAILURE);
         }
         prom_image->images_table[i] = table;
      }
      else
         /*------------------------------------*/
         /* Assign NULL pointer for other table */
         prom_image->images_table[i] = NULL;
   }
   return (prom_image);
}
