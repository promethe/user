/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
    \file
    \ingroup f_save_image_to_disk
    \brief save an image png to the disk

    Author: xxxxxxxxx
    Created: xxxxxxxxxx
    Modified:
    - author: C.Giovannangeli
    - description: specific file creation
    - date: 20/07/2004

    Theoritical description:

    Description:
    This file contains the C fonction for saving an image from the disk
    This function only saves 1*8bits(gray) or 3*8bits(color) png images

    Macro:
    none

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -memcpy_alloc_prom_image()

    External Tools: 
    -tools/Vision/free_prom_image()

    Links:
    - type: xxxxxxxxxxx
    - description: none
    - input expected group: none
    - where are the data?: none

    Comments: none

    Known bugs: none caus' not testedh

    Todo: see the author for comments


    http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <png.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include "tools/include/memcpy_alloc_prom_image.h"

#include <sys/time.h>



void save_png_to_disk(char *im_name, prom_images_struct prom_image_input,
		int Z_PNG_COMPRESSION_CHOICE /*0 to 9 */ )
{
	FILE *fp;
	png_structp png_ptr;
	png_infop info_ptr;
	png_bytep row_buf;
	int width, height, nbands;
	int y;
	int i;
	char im_name2[256], name[256];
	png_color_8 sig_bit;
	prom_images_struct *prom_image;

#ifdef TIME_TRACE
	struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
	long SecondesFunctionTimeTrace;
	long MicroSecondesFunctionTimeTrace;
	char MessageFunctionTimeTrace[255];

	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

	/*----------------------------------*/
	/* Creating temporary copy of input */
	prom_image = memcpy_alloc_prom_image(&prom_image_input);

	/*----------*/
	/*Parameters */
	width = prom_image->sx;
	height = prom_image->sy;
	nbands = prom_image->nb_band;

	/*---------------------------------*/
	/* Get Name without extension .png */
	for (i = 0; im_name[i] != '.' && im_name[i] != 0x00; i++)
		name[i] = im_name[i];
	name[i] = '\0';

	/*-------------------------*/
	/*How mmany image to store? */
	for (i = 0; i < (prom_image->image_number); i++)
	{

		if (prom_image->image_number == 1)
		{
			/* original name without modif */
			strcpy(im_name2, im_name);
		}
		else
		{
			/* name will be name_???.png */
			sprintf(im_name2, "%s_%03d.png", name, i);
		}

		/*------------------------------------*/
		/*ouverture de l'image en mode binaire */
		fp = fopen(im_name2, "wb");
		if (!fp)
		{
			EXIT_ON_ERROR("Can not open the file %s\n", im_name2);
		}

		/*-------------------------------------------------------*/
		/*allocation memoire des stuctures png_struct et png_info */
		png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
				(void *) NULL, NULL, NULL);

		if (png_ptr == NULL)
		{
			fclose(fp);
			exit(EXIT_FAILURE);
		}

		/* Allocate/initialize the image information data.  REQUIRED */
		info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
		{
			fclose(fp);
			png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
			exit(EXIT_FAILURE);
		}

		/* Set error handling.  REQUIRED if you aren't supplying your own
		 * error hadnling functions in the png_create_write_struct() call.
		 */

		if (setjmp(png_jmpbuf(png_ptr)))
		{
			/* If we get here, we had a problem reading the file */
			kprints("Problem in save_png_to_disk while reading the file\n");
			fclose(fp);
			png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
			return;
		}

		/*---------------------------------------------------*/
		/*mise a jour de l'entree lu dans le pointeur de ping */
		png_init_io(png_ptr, fp);

		/*-----------------------------------------------------*/
		/*remplissage de la sructure info pour une image 8bits */
		if (nbands == 1)
		{
			/*Save to PNG_COLOR_TYPE_GRAY */
			png_set_IHDR(png_ptr, info_ptr, width, height, 8,
					PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
					PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
			sig_bit.gray = 8;
		}
		else
		{
			/*Save to PNG_COLOR_TYPE_RGB */
			png_set_IHDR(png_ptr, info_ptr, width, height, 8,
					PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
					PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
			sig_bit.red = 8;
			sig_bit.green = 8;
			sig_bit.blue = 8;
		}
		png_set_sBIT(png_ptr, info_ptr, &sig_bit);

		/* Taux de compression */
		png_set_compression_level(png_ptr, Z_PNG_COMPRESSION_CHOICE);

		/* Optional gamma chunk is strongly suggested if you have any guess
		 * as to the correct gamma of the image.
		 */
		/* png_set_gAMA(png_ptr, info_ptr, gamma); */

		/* Optionally write comments into the image */
		/*   text_ptr[0].key = "Title";
     text_ptr[0].text = "Promethe image";
     text_ptr[0].compression = PNG_TEXT_COMPRESSION_NONE;
     text_ptr[1].key = "Author";
     text_ptr[1].text = "...";
     text_ptr[1].compression = PNG_TEXT_COMPRESSION_NONE;
     text_ptr[2].key = "Description";
     text_ptr[2].text = "Made with Promethe";
     text_ptr[2].compression = PNG_TEXT_COMPRESSION_zTXt;
     png_set_text(png_ptr, info_ptr, text_ptr, 2);  */


		/*-----------------*/
		/*ecriture des info */
		png_write_info(png_ptr, info_ptr);

		/*------------------------------*/
		/*initialisation de la ligne buf */
		/*row_buf = (png_bytep)NULL; */

		/*-----------------------------------*/
		/*ecriture ligne par ligne de l'image */
		for (y = 0; y < height; y++)
		{
			/*row_buf=(png_bytep)(retine_1+(y*nbands*w)); */
			row_buf =
					(png_bytep) ((prom_image->images_table[i]) +
							y * nbands * width);

			/*ecriture des row buf dans l image write_ptr */
			png_write_rows(png_ptr, &row_buf, 1);
		}

		/*Fin d'ecriture de png */
		png_write_end(png_ptr, info_ptr);

		/* clean up after the write, and free any memory allocated */
		png_destroy_write_struct(&png_ptr, (png_infopp) NULL);

		dprints("Saving PNG picture %s finished width=%i height=%i nbbands=%i\n",im_name2, width, height, nbands);

		/* close the file */
		fclose(fp);

		/*Free copy */
		/*  free_prom_image(prom_image); */
	}
	free_prom_image(prom_image);

#ifdef TIME_TRACE
	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
	if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	else
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
				1;
		MicroSecondesFunctionTimeTrace =
				1000000 + OutputFunctionTimeTrace.tv_usec -
				InputFunctionTimeTrace.tv_usec;
	}
	sprintf(MessageFunctionTimeTrace,
			"Time in save_png_to_disk\t%4ld.%06ld",
			SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
	cprints("%s\n", MessageFunctionTimeTrace);
#endif
}
