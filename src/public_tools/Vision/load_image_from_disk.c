/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  load_image_from_disk.c
\brief xxxxxxxxxxxxxxxxx

Author: xxxxxxxxxxx
Created: xxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Load an image (LENA or PNG) from the disk
 * and gets its array       
 
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-load_png()

External Tools: 
-none

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
***********************************************************/
#include <libx.h>

#include <stdlib.h>
#include <string.h>
#include "tools/include/load_png.h"
#include "tools/include/load_ppm.h"

void load_image_from_disk(char *im_format, char *im_name, unsigned char **image_table   /*image table */
                          , int *width, int *height /*image size */
                          , int *nbands /**/)
{

  /*-------------------------*/
    /*Check the format of image */
    if (!strcmp(im_format, "lena") || !strcmp(im_format, "LENA"))
    {
        image image_lena;
        descripteur dsc;

        image_lena = ouvre('l', &dsc, im_name);
        *width = dsc.nx;
        *height = dsc.ny;
        *nbands = 1;
        *image_table =  (unsigned char *) calloc(dsc.nx * dsc.ny, sizeof(unsigned char));
        if (*image_table == NULL)
        {
            printf("Not enought memory! (in load_image_from_disk)\n");
            exit(EXIT_FAILURE);
        }
        lec(&image_lena, *image_table, dsc.ny);
        dprints("Loading file: %s w=%i, h=%i, nband=%i\n", im_name, *width, *height, *nbands);
    }
    else if (!strcmp(im_format, "png") || !strcmp(im_format, "PNG"))
    {
        /*load png file------------------------------------ */
        /*carefull, the array image_table will be allocated */
        load_png(im_name, image_table, width, height, nbands);
    }
    else if (!strcmp(im_format, "ppm") || !strcmp(im_format, "PPM"))
    {
        /*load ppm file------------------------------------ */
        /*carefull, the array image_table will be allocated */
        load_ppm(im_name, image_table, width, height, nbands);
    }

}
