/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \file 
   \ingroup f_save_image_to_disk

   \brief This function only saves PPM images.

   \details

   Author: xxxxxxx
   Created: xxxxx
   Modified:
   - author: P. PARENT
   - description: specific file creation
   - date: 10/04/2007

 

************************************************************/
/* #define DEBUG */
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define TIME_TRACE

#ifdef TIME_TRACE
#include <sys/time.h>
#endif

void save_ppm_to_disk(char *im_name, prom_images_struct prom_image)
{
  int i;
  char im_name2[256], name[256];
  FILE *file;
  char buffer[80];

#ifdef TIME_TRACE
  struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
  long SecondesFunctionTimeTrace;
  long MicroSecondesFunctionTimeTrace;
  char MessageFunctionTimeTrace[255];

  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  /*----------------------------------*/
  /* Get Name without extension .ppm */
  for (i = 0; im_name[i] != '.' && im_name[i] != '\0'; i++)
    name[i] = im_name[i];
  name[i] = '\0';

  /*-------------------------*/
  /*How many image to store? */
  for (i = 0; i < (prom_image.image_number); i++)
  {

    if (prom_image.image_number == 1)
    {
      /* original name without modif */
      strcpy(im_name2, im_name);
    }
    else
    {
      /* name will be name_???.ppm */
      sprintf(im_name2, "%s_%03d.ppm", name, i);
    }

    file = fopen(im_name2, "wb");
    if (file == NULL) {
      EXIT_ON_ERROR("ERREUR : creation du fichier %s impossible dans save_ppm_to_disk\n",im_name2);
    }

    /* enregistrement de l'image au format ppm */
    sprintf(buffer,"P6\n%d %d\n255\n",prom_image.sx, prom_image.sy);
    fwrite(buffer,strlen(buffer),1,file);
    fwrite(prom_image.images_table[i], sizeof(char), 3*sizeof(char)*prom_image.sx*prom_image.sy, file);
		
    /* fermeture du fichier */
    fclose(file);
		
		
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
      SecondesFunctionTimeTrace =
	OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
      MicroSecondesFunctionTimeTrace =
	OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
      SecondesFunctionTimeTrace =
	OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
	1;
      MicroSecondesFunctionTimeTrace =
	1000000 + OutputFunctionTimeTrace.tv_usec -
	InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
	    "Time in save_ppm_to_disk\t%4ld.%06ld",
	    SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    cprints("%s\n", MessageFunctionTimeTrace);
#endif

    dprints("Saving PPM picture %s finished width=%i height=%i\n",im_name2, prom_image.sx, prom_image.sy);

  }
}
