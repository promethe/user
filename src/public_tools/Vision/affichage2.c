/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  affichage2.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
void affichage2(int n, float *image, int sx, int sy, int x, int y)
{

#ifndef AVEUGLE                 /* 12/06/2003 Olivier Ledoux */
   unsigned char *imchar;
   int i, test = 0;
   void *fenetre;

   /*printf("affichage debut\n");*/
   if (n == 1)  fenetre = &image1;
   else         fenetre = &image2;
   /* il faut un peu de memoire */
   imchar = (unsigned char *) malloc(sx * sy * sizeof(unsigned char));
   if (imchar == NULL)
   {
      EXIT_ON_ERROR("dans affichage : plus de memoire\n");
   }

   for (i = 0; i < sx * sy; i++)
   {
      if (image[i] > 0.0)
      {
         imchar[i] = (unsigned char) floor(image[i] * 255.0);
         test = 1;
      }
      else
      {
         imchar[i] = 0;
      }
   }
   /*printf("fin boucle\n");*/
   if (test != 0)   affiche_ng(fenetre, imchar, sx, sy, 0, x, y, 0);
   free(imchar);
   /*printf("affichage fin\n");*/
#else
   UNUSED_PARAM n; UNUSED_PARAM image; UNUSED_PARAM sx; UNUSED_PARAM sy; UNUSED_PARAM x; UNUSED_PARAM y;
#endif
}
