/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
 \file  load_png.c
 \brief xxxxxxxxxxxxxxxxx

 Author: xxxxxxxxxxx
 Created: xxxx
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 To load an png file and get its array

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal tools:
 -none

 External tools:
 -none

 Links:
 - type: none
 - description: none
 - input expected group: Image of real point
 - where are the data?: in the image to convert

 Comments:

 Known bugs: none (yet!)

 Todo:	eviter de reserver/liberer la memoire apres chaque lecture  !!!


 http://www.doxygen.org
 ************************************************************/
#include <libx.h>

#include <png.h>
#define DEBUG
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void abort_(const char *s, ...) 
{
	va_list args;
	va_start(args, s);
	vfprintf(stderr, s, args);
	fprintf(stderr, "\n");
	va_end(args);

	printf("error in %s \n", __FILE__);
	abort();
}

/*int x, y;

 int width, height;
 png_byte color_type;
 png_byte bit_depth;

 png_structp png_ptr;
 png_infop info_ptr;
 int number_of_passes;
 png_bytep *row_pointers;*/

void load_png(char *file_name, unsigned char **image_table /*image table */
, int *width_return, int *height_return /*image size */
, int *nbands_return /**/) {
	int y;

	int width, height;
	png_byte color_type;
	png_byte bit_depth;

	png_structp png_ptr;
	png_infop info_ptr;
	int number_of_passes;
	png_bytep *row_pointers;

	unsigned char *temp;
	int i, lenght;
	png_byte header[8]; /* 8 is the maximum size that can be checked
	 open file and test for it being a png */
    
	FILE *fp = fopen(file_name, "rb");
	if (!fp)  EXIT_ON_ERROR("[read_png_file] File %s could not be opened for reading",file_name);
	lenght = fread(header, 1, 8, fp); (void) lenght;
	if (png_sig_cmp(header, 0, 8))	EXIT_ON_ERROR("[read_png_file] File %s is not recognized as a PNG file", file_name);

	/* initialize stuff */
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (!png_ptr) EXIT_ON_ERROR("[read_png_file] png_create_read_struct failed");

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) EXIT_ON_ERROR("[read_png_file] png_create_info_struct failed");

	if (setjmp(png_jmpbuf(png_ptr)))	EXIT_ON_ERROR("[read_png_file] Error during init_io");

	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);

	png_read_info(png_ptr, info_ptr);

	width = png_get_image_width(png_ptr, info_ptr);
	height = png_get_image_height(png_ptr, info_ptr);
	color_type = png_get_color_type(png_ptr, info_ptr);
	bit_depth = png_get_bit_depth(png_ptr, info_ptr);

	number_of_passes = png_set_interlace_handling(png_ptr);
	png_read_update_info(png_ptr, info_ptr);

	/* read file */
	if (setjmp(png_jmpbuf(png_ptr))) EXIT_ON_ERROR("[read_png_file] Error during read_image");

	row_pointers = (png_bytep *) malloc(sizeof(png_bytep) * height);
	for (y = 0; y < height; y++)
		row_pointers[y] = (png_byte *) malloc(png_get_rowbytes(png_ptr, info_ptr));

	png_read_image(png_ptr, row_pointers);

	*height_return = png_get_image_height(png_ptr, info_ptr);
	*width_return = png_get_image_width(png_ptr, info_ptr);
	if ((png_get_color_type(png_ptr, info_ptr) & PNG_COLOR_TYPE_PALETTE)	== 0 /*PNG_COLOR_TYPE_PALETTE */)        *nbands_return = 1;
	else                                                                                                          *nbands_return = 3;
   
   if (png_get_color_type(png_ptr, info_ptr) & PNG_COLOR_MASK_ALPHA)    (*nbands_return)++;

	temp = (unsigned char *) malloc((*nbands_return * width * height) * sizeof(unsigned char));

	for (i = 0; i < height; i++)
		memcpy(temp + i * width * (*nbands_return), (unsigned char * ) row_pointers[i], (*nbands_return) * width);

	png_read_end(png_ptr, info_ptr);

	/*--------------*/
	/*returnimg ptrs */

	(*image_table) = (unsigned char *) temp;
	png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);

	/* 08/10/2003 Olivier Ledoux
	 pour eviter fuite de memoire
	 */

	for (i = 0; i < height; i++)
		free(row_pointers[i]);
	free(row_pointers);

	fclose(fp);
}
