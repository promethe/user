/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \file
   \ingroup f_save_image_to_disk
   \brief xxxxxxxxxxxxxxxxx

   Author: xxxxxxxxxxx
   Created: xxxx
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 20/07/2004

************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>


prom_images_struct *memcpy_alloc_prom_image(prom_images_struct *  prom_image_src)
{
   int i, x, y, b;
   prom_images_struct *prom_image_dest;
   unsigned char *table;

   /*------------------------------*/
   /*Alloc for a prom_images_struct */
   prom_image_dest = (prom_images_struct *) malloc(sizeof(prom_images_struct));

   /*----------*/
   /*test error */
   if (prom_image_dest == NULL || prom_image_src == NULL)
   {
      EXIT_ON_ERROR("err in memcpy_alloc_prom_image\n");
   }

   x = prom_image_dest->sx = prom_image_src->sx;
   y = prom_image_dest->sy = prom_image_src->sy;
   b = prom_image_dest->nb_band = prom_image_src->nb_band;
   prom_image_dest->image_number = prom_image_src->image_number;

   for (i = 0; i < NB_MAX_IMAGES_IN_PROM_IMAGES; i++)
   {
      if (i < prom_image_src->image_number)
      {
         /* ------------------------ */
         /*first allocate then copy */
         table =(unsigned char *) calloc(x * y * b, sizeof(unsigned char));

         if (table == NULL)
         {
            EXIT_ON_ERROR("can'tallocate in memcpy_alloc_prom_image\n");
         }

         /* -------------------- */
         /* memcpy of each table */
         memcpy(table, prom_image_src->images_table[i], x * y * b * sizeof(unsigned char));
         prom_image_dest->images_table[i] = table;
      }
      else
      {
         /*------------------------------------*/
         /* Assign NULL pointer for other table */
         prom_image_dest->images_table[i] = NULL;
      }
   }

   /*------------------------------------------------*/
   /*Return the ptr where ther is the copy allocated */
   return (prom_image_dest);
}
