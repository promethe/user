/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
\file  load_png.c
\brief xxxxxxxxxxxxxxxxx

Author: xxxxxxxxxxx
Created: xxxx
Modified:
- author: P. PARENT
- description: specific file creation
- date: 13/04/2007

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   To load a ppm file and get its array 
   
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal tools:
-none

External tools:
-none

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/

#include <libx.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>



/* ----------------------------------------- */
char *readitem(FILE *file,char *buffer)
/* ----------------------------------------- */
/* lecture d'un mot */
{
  char *aux;
  int k;

  k=0;
  aux=buffer;
  while (!feof(file))
    {
      *aux=fgetc(file);
      switch(k)
        {
        case 0:
          if (*aux=='#') k=1;
          if (isalnum(*aux)) k=2,aux++;
          break;
        case 1:
          if (*aux==0xA) k=0;
          break;
        case 2:
          if (!isalnum(*aux))
            {
              *aux=0;
              return buffer;
            }
          aux++;
          break;
        }
    }
  *aux=0;
  return buffer;
}

void load_ppm(char *filename, unsigned char **image_table  /*image table */
              , int *width_return, int *height_return   /*image size */
              , int *nbands_return /**/)
{
	
  /* cette version ne lit que le type P6 */
  long im_gris;
  FILE *file;
  unsigned char *temp;
  char *buffer;
  int i, nb_bytes_read;

  buffer = (char*) calloc(80, sizeof(char));
  
  /* ouverture du fichier */
  file = fopen(filename,"r");
  if (file==NULL)  printf("%s %s ouverture du fichier impossible dans load_ppm\n", __FILE__ , __FUNCTION__ );

  /* lecture de l'entete du fichier ppm */
  readitem(file, buffer);
  if(strcmp(buffer, "P6") != 0)  printf("ERREUR : entete du fichier invalide dans load_ppm\n");
  *width_return  = atoi(readitem(file, buffer));
  *height_return = atoi(readitem(file, buffer));
  *nbands_return = 3;
  im_gris = atoi(readitem(file, buffer));
  dprints("taille image : %d %d %d\n",*width_return, *height_return, *nbands_return);
  
  /* lecture de l'image */
  temp = (unsigned char *) malloc(((*nbands_return) * (*width_return) * (*height_return)) * sizeof(unsigned char));
  for (i = 0; i < (*height_return); i++)
  {
    nb_bytes_read= fread(temp + i * (*width_return) * (*nbands_return), sizeof(unsigned char), (*width_return)*(*nbands_return),file);
    if(nb_bytes_read != (*width_return) * (*nbands_return)) printf("error in %s while reading %s. The size read %d does not match the required size %d \n", __FILE__ , __FUNCTION__ ,nb_bytes_read,(*width_return) * (*nbands_return));
  }
  
  (*image_table) = (unsigned char *) temp;
  
  fclose(file);
  free(buffer);
}
