/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

/*---------------------------------------------------------------------*/
/*             programme a linker avec Promethe                        */
/*             contient la routine d'appel aux fonctions C             */
/*             et la routine de gestion du simulateur de RN            */
/*                                                                     */
/*  Les fonctions C ou RN doivent etre ajoutees dans public.h          */
/*  et leur nom doit etre ajoute dans les fonctions ci dessous         */
/*                                                                     */
/* Version fevrier 1993                                                */
/* modifiee juillet 2001 pour ajout gestion des threads                */
/*---------------------------------------------------------------------*/

/**
 \defgroup group_tools Group tools
 Functions and macro useful to implement new groups.

 \defgroup generic_tools Generics tools
 Generic functions and macro useful to program in C.


 \defgroup plugin_list Libraries of groups
 \defgroup NN_Core NN_Core
 \ingroup plugin_list
 \brief Main neuronal groups
 */

/**
 @relates mainpage
 @mainpage Promethe Neural Network Documentation.
 \section intro Introduction
 - Promethe is a Neural Network simulator. Altogether with Leto Neural
 Network designer, Promethe allow to  simulate complex neural networks
 ( kohonen maps, back-propagation, ART, reentrant maps).
 - Promethe uses vision, sound, and allows designing, testing of
 control architectures (Koala, Katana, Urbi, Robulab, Roburoc).


 \section ref References
 - <a href="http://newserv.ensea.fr/wiki-neuro"> Wiki promethe</a>


 */

/*#define DEBUG */

#include <libx.h>
#include <net_message_debug_dist.h>  /* sockets I/O du kernel */
#include "prom_tools/include/prom_bus.h"
/*#include <linux/videodev.h>*/
#include <sys/types.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#ifndef AVEUGLE
#include	<X11/Xlib.h>
#include	<X11/Xutil.h>
#include	<X11/cursorfont.h>
#endif

#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>

#include <sched.h>

/* Need dlfcn.h for the routines to dynamically load libraries */
#include <dlfcn.h>

/*#include <libfast_computation.h>*/

/**************** incldue des typedef, structures, macros et variables globales *****************/
#include <Typedef/boolean.h>
#include <Typedef/button_fct.h>
#include <Struct/arguments.h>

#ifndef NO_FFT
#include <Struct/list_rfftwnd_plan.h>
#endif

#include <Struct/prom_images_struct.h>
#include <Struct/cam_pos.h>
#include <Struct/motivation.h>
#include <Macro/IO_Robot/macro.h>

#include <Global_Var/Vision.h>
#include <Global_Var/IO_Robot.h>

/**************** include des fonctions kernel *****************/

#include <Kernel_Function/ecriture_lena.h>
#include <Kernel_Function/promethe_main.h>
#include <Kernel_Function/gestion_mask_signaux.h>

/**************** include des fonctions de la libhardware *****************/
#include <libhardware.h>
/***************include des fonction leto ***************/

#include <string.h>
#include <getopt.h>
#include <Sound/Sound.h>
#include <group_function_pointers.h>

#include <NN_Core/seq.h>
#include <NN_Core/classique_rn.h>
#include <NN_Core/integr.h>
#include <NN_Core/classique_rn/majSTM.h>
#include <NN_Core/selective_winner.h>
#include <NN_Core/sigma_pi.h>
#include <NN_Core/macro_neurone.h>
#include <NN_Core/Timing.h>
#include <NN_Core/reset.h>
#include <NN_Core/rt_minimum.h>
#include <NN_Core/pyramidal.h>
#include <NN_Core/pyramidal_temporaire.h>
#include <NN_Core/goal.h>
#include <NN_Core/chaos.h>
#include <NN_Core/vigilance.h>
#include <NN_Core/macro_colonne.h>
#include <NN_Core/saw.h>
#include <NN_Core/plg.h>
#include <NN_Core/pcr.h>
#include <NN_Core/classique_rn/ctrnn.h>
#include <NN_Core/classique_rn/STM.h>
#include <NN_Core/kmean_r.h>
#include <NN_Core/classique_rn/CTRNN2.h>

/*ajout nico*/
#include <NN_Core/ca.h>
#include <NN_Core/Asso_Cond.h>
#include <NN_Core/selective_winner_modulated.h>

#include <NN_Core/Modulation/modulation.h>
#include <NN_Core/Modulation/initialise_modulation.h>

/* TESTS ARNAUD */
#include <NN_Core/WTA_pierre_et_arnaud.h>

#include <NN_Core/BCM.h>
#include <NN_Core/classique_rn/LMS_2.h>

/* Added RCO by: ALEX */
#include <NN_Core/classique_rn/RCO_neuron.h>

/**************include pour lire_ligne **************/
#include <IO_Robot/Expressive_Head.h>

#include <NN_Core/rien.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <IO_Robot/libIO_Robot.h>
#include <NN_IO/libNN_IO.h>
#include <SigProc/libSigProc.h>
#include <IHM/libIHM.h>

#define MODULE_OPTIONAL 0
#define MODULE_REQUIRED 1

void *module_cuda = NULL;
void *init_module(const char *path, const char *module_name, int required);

void *SigProc_libdyn = NULL;
void *IHM_libdyn = NULL;
void *IO_Robot_libdyn = NULL;
void *Parellel_Comm_libdyn = NULL;
void *NN_IO_libdyn = NULL;
void *Sensors_libdyn = NULL;

/*
 ============================================================================================

 Initialisation des Variables Globales

 ============================================================================================
 */

button_fct ftab[4];

arguments args;

float global_temps = 0.;

boolean EMISSION_ROBOT = 1; /*NC: allow robot emission by default        */
/* drapeau autorisation d'ordres moteurs (was in NN_IO/system_champs.c)               */

#ifndef  NO_FFT
list_rfftwnd_plan *list_prom_fft = NULL;
#endif

pthread_mutex_t mutex_list_prom_fft;
pthread_mutex_t mutex_grab_image;

char PORT[MAXBUF];
char SPORT[MAXBUF];

int DEBIT;
int PAN_VALUE;
int CAM_MODE;
int INIT_PAN;
int INIT_TILT;

int MOVE_PAN;
int MOVE_TILT;
int PAN_MAX_DELTA;
int TILT_MAX_DELTA;

int PAN_ADDR;
int TILT_ADDR;

int BS_crhaut = 718;
int BS_crbas = 482;
int USE_COMPASS;

int bc, bs;
int bcmin, bcmax, bsmin, bsmax;

char VIDEO_DEV[MAXBUF];

int MOTOR_LOGICAL_STATUS[5];
float MOTOR_1, MOTOR_2, MOTOR_3, MOTOR_4, MOTOR_5;
int USE_ROBOT;

int USE_CAM;

int USE_KATANA, USE_HEAD;
int OFFSET_PAN = 0;

cam_pos PTposition;

prom_images_struct images_capture; /*was in robot_vision.c */

int AngleServoInt;

long SecondesFunctionTimeTrace;
long MicroSecondesFunctionTimeTrace;
struct timeval InputFunctionTimeTrace;
struct timeval OutputFunctionTimeTrace;

/*pour env_simul*/
int NbrFeaturesPoints;
/* -----------------------------------------------------------*/
int learn_NN;

char GLOB_path_SAVE[MAXBUF] = ".";
char vrai_nom_image[MAXBUF] = "rien";

struct timeval SystemTime;
struct timeval time_0;
button_fct ftab[4];
int alealearn;

float dP = 0.; /*!< Douleur et variation de la douleur */
float ddouleur; /* Variables gerant la douleur et ses variations */
float stress = .0001; /* Variable gerant le stress en situation douloureuse */
float inattention = 0.; /*!< Mesure l'attention du robot */
/*!< Agit sur la reponse des neurones produits */
/*!< Si la valeur est faible -> l'attention est forte */
/*!< Sinon -> augmente le bruit. Investigations des possibilites */

/* -----------------------------------------------------------*/
motivation motivations[nb_motivations];
float Niveau_ressources[nb_motivations];

float Orientation = 1000.;

/* mutex pour eviter que 2 fonctions ne demande a lire au clavier et/ou affiche des messages d'invite entrelaces */
#ifdef USE_THREADS
pthread_mutex_t mutex_lecture_clavier = PTHREAD_MUTEX_INITIALIZER;
#endif

/*------------------------------------------------------------*/

/* creation d'un tableau d'objets */

type_groupe OG_table[nbre_type_groupes];

/*#define DEBUG*/

/*
 ============================================================================================

 Code des Fonctions de main.c

 ============================================================================================
 */

/* mettre ici les appels a des fonctions permettant de faire un auto test */
/* renvoie 1 si OK, 0 ou autre si pbs...                                  */
int autotest()
{
  kprints("autotest vide = [OK]\n");
  return 1;
}

/*-------------------------------------------------------------------------*/

void STM_rien(int gpe)
{
  (void) gpe;
  dprints("**************** STM rien %s *************************\n", def_groupe[gpe].no_name);
}

void rien(int gpe)
{
  (void) gpe;
  dprints("**************** fonction rien %s *************************\n", def_groupe[gpe].no_name);
}

void function_stop_if_non_rt(int gpe)
{
  if (def_groupe[gpe].rttoken <= 0)
  {
    dprints("function_stop_if_non_rt grp %s (%d)\n", def_groupe[gpe].no_name, gpe);
    dprints("rttoken = %d \n", def_groupe[gpe].rttoken);
    exit(1);
  }

  if (def_groupe[gpe].rttoken != (int) neurone[def_groupe[gpe].premier_ele].s1)
  {
    if (neurone[def_groupe[gpe].premier_ele].flag == 1)
    dprints("changement de token avant=%d  maintenant=%d \n", (int) neurone[def_groupe[gpe].premier_ele].s1, def_groupe[gpe].rttoken);
    if (neurone[def_groupe[gpe].premier_ele].flag == 1) exit(1);

    neurone[def_groupe[gpe].premier_ele].s1 = (float) def_groupe[gpe].rttoken;
    neurone[def_groupe[gpe].premier_ele].flag = 1;
  }
}

void mise_a_jour_rien(int no_neurone, int local_gestion_STM, int local_learn)
{
  (void) no_neurone;
  (void) local_gestion_STM;
  (void) local_learn;

  dprints("**************** mise a jour rien neurone:%d *************************\n", no_neurone);
}

void apprend_rien(int gpe)
{
  (void) gpe;

  dprints("**************** apprend rien %d *************************\n", gpe);
}

void gestion_rien(int gpe)
{
  (void) gpe;
  dprints("**************** gestion rien %d *************************\n", gpe);
}

void algo_oublie(int gpe)
{
  kprints("ERROR: algo oublie / non traitee dans main.c, in group %s (%s)\n", def_groupe[gpe].nom, def_groupe[gpe].no_name);
}

void mise_a_jour_oublie(int no_neurone, int local_gestion_STM, int local_learn)
{
  (void) local_gestion_STM;
  (void) local_learn;
  kprints("ERROR: mise a jour oublie / non traitee dans main.c, neurone:%d in group %s (%s)\n", no_neurone, def_groupe[neurone[no_neurone].groupe].nom, def_groupe[neurone[no_neurone].groupe].no_name);
}

void apprend_oublie(int gpe)
{
  kprints("ERROR: gpe %d : apprend oublie/non traiteee dans main.c !!!! \n", gpe);
}

void gestion_oublie(int gpe)
{
  kprints("ERROR: gpe %d : gestion oublie / non traitee dans main.c !!! \n", gpe);
}

/*----------------------------------------------------------------------*/

void gestion_fonction_algo(int gpe, int learn, int gestion_STM)
{
  (void) learn;
  (void) gestion_STM;

  dprints("gestion_fonction_algo %d \n", gpe);
  (*def_groupe[gpe].appel_algo)(gpe /*,learn */);
}

/*--------------------------------------------------------------------*/
/*                    MISE A JOUR DU RESEAU                            */
/*                                                                     */
/*   gpe   = numero du gpe a mettre a jour                             */
/*                                                                     */
/*   n     = No ex etudie dans le cas Widrow ou l'on a besoin corrige  */
/*           sinon sans importance                                     */
/*                                                                     */
/*   learn = 1 apprentissage modif des poids                           */
/*           sinon que propagation info et modif sorties               */
/*                                                                     */
/*   gestion_STM = 1 modification des memoires a court terme           */
/*                   sinon on considere que c'est un transitoire       */
/*                   tres rapide qui ne les affecte pas                */
/*---------------------------------------------------------------------*/

void mise_a_jour_normale_sans_thread(int gpe)
{
  int i;
  int learn = 0;

  dprints("mise a jour des neurones du groupe : %d \n", gpe);
  if (p_trace > 0) kprints("mise a jour des neurones du groupe : %d \n", gpe);

  for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
    (*def_groupe[gpe].appel_activite)(i, 0, learn); /* gestion_STM de 0 peut etre inutile de pouvoir le passer en parametre (peut etre constant) */

#ifndef AVEUGLE
  /*affiche dans la fenetre 2 l'etat des neurones du groupe selectionne */
  if (rapide == 0) ecriture_lena(im4, 256, 256, gpe_courant_a_afficher);

  /* * l'ancien appel etait le suivant:*
   ecriture_lena(im4,256,256,gpe_courant_a_afficher,"aff_neurone.lena")
   *   mais la fonction venant du kernel n'est prototype que pour quatre parametres.
   Christophe Giovannangeli a donc l'appel */
#endif

  (*def_groupe[gpe].appel_gestion)(gpe);
}

#define NB_MAX_THREADS 16

typedef struct arg_mise_a_jour {
  int gpe;
  int i;
  int gestion_STM;
  int learn;
  int taille_bloc;
  int no_thread;
  int increment;
} arg_mise_a_jour;

/*-----------------------------------------------*/

void *fonction_de_thread(void *arg)
{
  int gpe, i, gestion_STM, learn, taille_bloc, j, increment;

  gestion_mask_signaux();
  gpe = ((arg_mise_a_jour *) arg)->gpe;
  i = ((arg_mise_a_jour *) arg)->i;
  gestion_STM = ((arg_mise_a_jour *) arg)->gestion_STM;
  learn = ((arg_mise_a_jour *) arg)->learn;
  taille_bloc = ((arg_mise_a_jour *) arg)->taille_bloc;
  increment = ((arg_mise_a_jour *) arg)->increment;

  dprints("fct de thread : %d \n", ((arg_mise_a_jour *) arg)->no_thread);
  dprints("gpe = %d \n", gpe);
  dprints("i= %d \n", i);
  dprints("learn = %d \n", learn);
  dprints("taille_bloc = %d \n", taille_bloc);

  for (j = i + increment - 1; j < i + taille_bloc && j < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; j += increment)
  {
    (*def_groupe[gpe].appel_activite)(j, gestion_STM, learn);
  }
  pthread_exit(NULL);
}

/*-----------------------------------------------*/

void mise_a_jour_normale(int gpe)
{
  int i, res, taille_bloc;
  int nbre, nbre2, increment;
  void *resultat;
  int nb_threads;
  int aligned_nb_neurons;
  int neurone_restant;
  pthread_t un_thread[NB_MAX_THREADS];
  arg_mise_a_jour arg[NB_MAX_THREADS];

  nbre = def_groupe[gpe].nbre;
  nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;
  increment = nbre / nbre2;

  // le calcul n'est pas fait sur les micro_neurones pour determiner le nombre de threads !
  aligned_nb_neurons = nbre2; /* we consider there are few more neurons to complete the lask block */
  if (aligned_nb_neurons > NB_MAX_THREADS)
  {
    if (aligned_nb_neurons % NB_MAX_THREADS != 0) aligned_nb_neurons = aligned_nb_neurons + (NB_MAX_THREADS - aligned_nb_neurons % NB_MAX_THREADS);
    // Permet de ramener le nombre au multiple superieur de "nb_max_thread" pour que le calcul suivant de taille bloc permette de caser toute les neuronnes dans le nombres maximum imparti
    // L'equation était une énorme erreur pour aligned_nb_neurons<NB_MAX_THREADS car elle posais probleme pour le calcul suivant d'ou le rajout du if aligned_nb_neurons>NB_MAX_THREADS .
  }

  taille_bloc = aligned_nb_neurons / NB_MAX_THREADS;
  if (taille_bloc < 1) // si inferieur au nombre max de thread : on pond autant de thread que de neurone et, forcement, la taille du bloc est de 1 (logique...)
  {
    taille_bloc = 1;
    nb_threads = aligned_nb_neurons;
  }
  else
  {
    nb_threads = nbre2 / taille_bloc;
    if (nbre2 % taille_bloc != 0) nb_threads++; //car on a tronqué à l'unité d'en dessous à la ligne du dessus, il faut quand meme prevoir un thread pour les neurones restant
  }

  if (nb_threads >= NB_MAX_THREADS) nb_threads = NB_MAX_THREADS; // Ne devrais jamais arriver avec le mecanisme ci-dessus mais bon ...

  neurone_restant = nbre2;
  for (i = 0; i < nb_threads; i++)
  {
    if (neurone_restant >= taille_bloc)
    {
      arg[i].taille_bloc = taille_bloc * increment; // pour le sous processus uniquement (fonction_de_thread) : taille bloc reprends sa valeur impliquant les micro-neurones
      neurone_restant = neurone_restant - taille_bloc;
    }
    else if (neurone_restant > 0)
    {
      arg[i].taille_bloc = neurone_restant * increment; // pour le sous processus uniquement (fonction_de_thread) : taille bloc reprends sa valeur impliquant les micro-neurones
    }
    else
    {
      //Ne devrais jamais arriver :
      EXIT_ON_ERROR("Erreur dans mise_a_jour normal\n");
    }

    arg[i].gpe = gpe;
    arg[i].gestion_STM = 0;
    arg[i].learn = 0;
    arg[i].no_thread = i;
    arg[i].increment = increment;
    arg[i].i = def_groupe[gpe].premier_ele + i * taille_bloc * increment; // Le i du neurone de depart du bloc

    res = pthread_create(&(un_thread[i]), NULL, fonction_de_thread, (void *) &(arg[i]));
    if (res != 0)
    {
      EXIT_ON_ERROR("failure on thread creation \n");
    }
  }

//Attente de fin d'execution
  for (i = 0; i < nb_threads; i++)
  {
    res = pthread_join(un_thread[i], &resultat);
    if (res != 0) EXIT_ON_ERROR("Echec de pthread_join %d pour le thread %d\n", res, i);
  }

#ifndef  AVEUGLE
  if (rapide == 0) ecriture_lena(im4, 256, 256, gpe_courant_a_afficher);
  /* l'ancien appel etait le suivant:
   ecriture_lena(im4,256,256,gpe_courant_a_afficher,"aff_neurone.lena");
   mais la fonction venant du kernel n'est prototype que pour quatre parametre.
   Christophe Giovannangeli a donc change l'appel */
#endif
  (*def_groupe[gpe].appel_gestion)(gpe);
}

/* Par defaut on ne fait rien lors de la destruction d'un groupe. */
/* Ces fonctions peuvent servir a enregistrer des donnees a la fin d'une simulation */
/* ou a liberer de la memoire... */

void no_destroy(int gpe)
{
  (void) gpe;
  dprints("Appel de la fonction de destruction par defaut du gpe %d \n", gpe);
  dprints("On ne fait rien : no_destroy() appelee. \n");
}

void no_save(int gpe)
{
  (void) gpe;
  dprints("Appel de la fonction de sauvegarde par defaut du gpe %d \n", gpe);
  dprints("On ne fait rien : no_save() appelee. \n");
}

/* Ce type de fonction devraient remplacer les tests sur les pointeurs NULL utilises avant...*/
/* Pas plus rapide car il vaut mieu faire toujours le test mais plus propre. */

void no_new(int gpe)
{
  (void) gpe;
  dprints("Appel de la fonction de creation par defaut du gpe %d \n", gpe);
  dprints("On ne fait rien : no_new() appelee. \n");
}

void no_activity(int no_neurone, int local_gestion_STM, int local_learn)
{
  (void) local_gestion_STM;
  (void) local_learn;

  cprints("Appel de la fonction du calcul de l'activite d'un neurone %d d'un groupe \n", no_neurone);
  cprints("On ne fait rien : no_activity() appelee. \n");
}

/* les methodes devraient etre initialisees a NULL ou une methode vide connue */

/* on utilise la fonction d'heritage de la classe parente: elle n'existe pas encore pour la classe fille ! */
/* x et y sont des pointeurs vers des structures de type_groupe */
#define inherit(y,x) (x)->inherit(x,y)

void generic_inherit(type_groupe * this, type_groupe * child)
{
  dprints("heritage deb\n");

  child->appel_gestion = this->appel_gestion;
  child->appel_apprend = this->appel_apprend;

  child->appel_algo = this->appel_algo;
  child->appel_activite = this->appel_activite;

  child->function_new = this->function_new;
  child->destroy = this->destroy;
  child->save = this->save;
  child->inherit = this->inherit;

  dprints("heritage end\n");
}

/* initialisation des types de groupes de neurones */

void init_objets(void)
{
  type_groupe OG_generique, OG_oublie;
  type_groupe *p_OG_CTRNN2, *p_OG_Hebb, *p_OG_Hebb_Threshold, *p_OG_Hebb_seuil_binaire, *p_OG_Et, *p_OG_Ou, *p_OG_Hopfield, *p_OG_KO_Discret, *p_OG_KO_Continu, *p_OG_BCM, *p_OG_LMS, *p_OG_LMS_2, *p_OG_LMS_delayed, *p_OG_Pavlov, *p_OG_Winner, *p_OG_Kohonen,
      *p_OG_Special, *p_OG_Sigma_PI, *p_OG_Winner_Selectif, *p_OG_Winner_Selectif_Modulated, *p_OG_Winner_Colonne, *p_OG_But, *p_OG_Pyramidal, *p_OG_Pyramidal_plan, *p_NLMS, *p_Granular, *p_OG_SAW, *p_OG_Kmean_R, *p_OG_Macro_Colonne, *p_OG_PLG,
      *p_Asso_Cond, *p_OG_Sutton_Barto, *p_OG_PCR, *p_OG_RCO; /* added p_OG_RCO by: ALEX*/
  type_groupe OG_generique_Algo, *p_OG_Fonction_Algo, *p_OG_Fonction_Algo_mvt;
  type_groupe OG_generique_reserve, *p_OG_generique_BREAK, *p_OG_generique_RTTOKEN;
  int i;

  dprints("definition des objets generiques \n");

  /* groupes lies a des mots cle reserves BREAK et RTTOKEN */
  OG_generique_reserve.appel_algo = rien;
  OG_generique_reserve.appel_activite = no_activity;
  OG_generique_reserve.appel_gestion = rien;
  OG_generique_reserve.appel_apprend = apprend_rien; /*le kernel appelle la mise_a_jour normale
   et avec apprentissage : dommage perte de temps ... PG */
  OG_generique_reserve.function_new = no_new; /* appelee au debut de chaque simulation */
  OG_generique_reserve.destroy = no_destroy; /* appelee a la fin de chaque simulation */
  OG_generique_reserve.save = no_save; /* appelee a chaque sauvegarde du reseau */
  OG_generique_reserve.inherit = generic_inherit;

  p_OG_generique_BREAK = &OG_table[No_BREAK];
  p_OG_generique_RTTOKEN = &OG_table[No_RTTOKEN];

  inherit(p_OG_generique_BREAK, &OG_generique_reserve);

  inherit(p_OG_generique_RTTOKEN, &OG_generique_reserve);

  /*bases generiques d'une fonction algo */

  inherit(&OG_generique_Algo, &OG_generique_reserve);
  p_OG_Fonction_Algo = &OG_table[No_Fonction_Algo];
  p_OG_Fonction_Algo_mvt = &OG_table[No_Fonction_Algo_mvt];
  inherit(p_OG_Fonction_Algo, &OG_generique_Algo);
  inherit(p_OG_Fonction_Algo_mvt, &OG_generique_Algo);

  /* bases generique d'un groupe de neurones */

  OG_generique.appel_gestion = gestion_rien;
  OG_generique.appel_apprend = apprend_rien;

  OG_generique.appel_algo = mise_a_jour_normale_sans_thread;
  OG_generique.appel_activite = mise_a_jour_neurone_trad; /* ATTENTION !!!!!! pb de type des arguments PG**********************************/

  OG_generique.function_new = no_new; /* appelee au debut de chaque simulation */
  OG_generique.destroy = no_destroy; /* appelee a la fin de chaque simulation */
  OG_generique.save = no_save; /* appelee a chaque sauvegarde du reseau */
  OG_generique.inherit = generic_inherit;

  /* types oublies ... affichage message erreur plutot que core dump*/

  inherit(&OG_oublie, &OG_generique);
  OG_oublie.appel_gestion = gestion_oublie;
  OG_oublie.appel_apprend = apprend_oublie;
  OG_oublie.appel_algo = algo_oublie;
  OG_oublie.appel_activite = mise_a_jour_oublie;

  for (i = 0; i < nbre_type_groupes; i++)
  {
    inherit(&OG_table[i], &OG_oublie);
  }

  /* break : remplace la fonction d'apprentissage*/
  p_OG_generique_BREAK->appel_apprend = rien;

  /* creation de l'objet hebb : No_Hebb */
  p_OG_Hebb = &OG_table[No_Hebb];
  inherit(p_OG_Hebb, &OG_generique);

  p_OG_Hebb->appel_activite = mise_a_jour_neurone_hebb;
  p_OG_Hebb->function_new = new_hebb;
  p_OG_Hebb->destroy = destroy_hebb;
  p_OG_Hebb->appel_apprend = apprend_hebb;

  /* Hebb_Treshold = Hebb inutile relique passe ... */
  p_OG_Hebb_Threshold = &OG_table[No_Hebb_Seuil];
  inherit(p_OG_Hebb_Threshold, p_OG_Hebb);

  /* creation de l'objet hebb seuil binaire : No_Hebb_Seuil_binaire */
  p_OG_Hebb_seuil_binaire = &OG_table[No_Hebb_Seuil_binaire];
  inherit(p_OG_Hebb_seuil_binaire, p_OG_Hebb);
  p_OG_Hebb_seuil_binaire->appel_activite = mise_a_jour_neurone_hebb_seuil_binaire;

  /* No_Pyramidal */
  p_OG_Pyramidal = &OG_table[No_Pyramidal];
  inherit(p_OG_Pyramidal, p_OG_Hebb);
  p_OG_Pyramidal->appel_apprend = apprend_Pyramidal;
  p_OG_Pyramidal->appel_algo = mise_a_jour_groupe_pyramidal;

  /* No_NLMS */
  p_NLMS = &OG_table[No_NLMS];
  inherit(p_NLMS, &OG_generique);
  p_NLMS->function_new = new_NLMS;
  p_NLMS->appel_activite = mise_a_jour_NLMS;
  p_NLMS->appel_gestion = gestion_NLMS;
  p_NLMS->appel_apprend = apprend_NLMS;
  p_NLMS->destroy = destroy_NLMS;

  /* No_Granular */
  p_Granular = &OG_table[No_Granular];
  inherit(p_Granular, &OG_generique);
  p_Granular->function_new = new_DG;
  p_Granular->appel_apprend = rien;
  p_Granular->appel_gestion = function_DG;
  p_Granular->destroy = destroy_DG;

  /* ET et OU logique : No_Et, No_Ou */
  p_OG_Et = &OG_table[No_Et];
  p_OG_Ou = &OG_table[No_Ou];
  inherit(p_OG_Et, p_OG_Hebb_seuil_binaire);
  /* pas d'apprentissage */
  p_OG_Et->appel_apprend = apprend_rien;
  inherit(p_OG_Ou, p_OG_Et);

  /* Hopfield : No_Hopfield */
  p_OG_Hopfield = &OG_table[No_Hopfield];
  inherit(p_OG_Hopfield, p_OG_Hebb);
  p_OG_Hopfield->appel_activite = mise_a_jour_neurone_hopfield;

  /* No_KO_Discret */
  p_OG_KO_Discret = &OG_table[No_KO_Discret];
  inherit(p_OG_KO_Discret, &OG_generique);
  p_OG_KO_Discret->function_new = new_DISCRETE_CHAOS;
  p_OG_KO_Discret->appel_apprend = learn_DISCRETE_CHAOS;
  p_OG_KO_Discret->appel_algo = update_DISCRETE_CHAOS;
  p_OG_KO_Discret->destroy = destroy_DISCRETE_CHAOS;

  p_OG_KO_Continu = &OG_table[No_KO_Continu];
  inherit(p_OG_KO_Continu, &OG_generique);
  p_OG_KO_Continu->function_new = new_CONTINUOUS_CHAOS;
  p_OG_KO_Continu->appel_apprend = learn_CONTINUOUS_CHAOS;
  p_OG_KO_Continu->appel_algo = update_CONTINUOUS_CHAOS;
  p_OG_KO_Continu->destroy = destroy_CONTINUOUS_CHAOS;

  /* No_BCM */
  p_OG_BCM = &OG_table[No_BCM];
  inherit(p_OG_BCM, &OG_generique);
  p_OG_BCM->appel_apprend = apprend_BCM;
  p_OG_BCM->appel_activite = mise_a_jour_neurone_BCM;

  /* No_LMS */
  p_OG_LMS = &OG_table[No_LMS];
  inherit(p_OG_LMS, p_OG_Hebb);
  p_OG_LMS->function_new = new_LMS;
  p_OG_LMS->appel_apprend = apprend_LMS;
  p_OG_LMS->appel_activite = mise_a_jour_neurone_LMS;
  p_OG_LMS->destroy = destroy_lms;
  /*pas de moyennage sur les entrees. Pour pouvoir moyenner et utiliser le champs temps du groupe commenter les lignes suivantes et decommenter les lignes contenant synapse->moy dans LMS.c et gradient_lms.h*/

  /* No_LMS_2 */
  p_OG_LMS_2 = &OG_table[No_LMS_2];
  inherit(p_OG_LMS_2, p_OG_Hebb);
  p_OG_LMS_2->function_new = new_LMS_2;
  p_OG_LMS_2->appel_apprend = apprend_LMS_2;
  p_OG_LMS_2->appel_activite = mise_a_jour_neurone_LMS_2;
  p_OG_LMS_2->destroy = no_destroy;

  /* No_LMS_delayed*/
  p_OG_LMS_delayed = &OG_table[No_LMS_delayed];
  inherit(p_OG_LMS_delayed, p_OG_Hebb);
  p_OG_LMS_delayed->function_new = new_LMS_delayed;
  p_OG_LMS_delayed->appel_apprend = rien;
  p_OG_LMS_delayed->appel_gestion = gestion_groupe_multiple_winner;
  p_OG_LMS_delayed->destroy = no_destroy;

  /* Pavlov - widrow encore ? : No_Widrow */
  p_OG_Pavlov = &OG_table[No_Widrow];
  inherit(p_OG_Pavlov, p_OG_LMS);
  /*  p_OG_Pavlov->appel_apprend = apprend_widrow;*/
  p_OG_Pavlov->appel_activite = mise_a_jour_neurone_widrow;

  /* winner : No_Winner */
  p_OG_Winner = &OG_table[No_Winner];
  inherit(p_OG_Winner, p_OG_Hebb);
  p_OG_Winner->appel_apprend = apprend_winner;
  p_OG_Winner->appel_gestion = gestion_groupe_multiple_winner;
  p_OG_Winner->function_new = new_winner;
  p_OG_Winner->destroy = no_destroy;

  /*   : sutton et bartto*/
  p_OG_Sutton_Barto = &OG_table[No_Sutton_Barto];
  inherit(p_OG_Sutton_Barto, p_OG_Hebb);
  p_OG_Sutton_Barto->appel_gestion = gestion_groupe_S_B;
  p_OG_Sutton_Barto->function_new = new_S_B;
  p_OG_Sutton_Barto->appel_activite = mise_a_jour_S_B;
  p_OG_Sutton_Barto->appel_apprend = apprend_S_B;
  p_OG_Sutton_Barto->destroy = no_destroy;

  /* Kohonen : No_Kohonen */
  p_OG_Kohonen = &OG_table[No_Kohonen];
  inherit(p_OG_Kohonen, &OG_generique);
  p_OG_Kohonen->appel_algo = mise_a_jour_normale_sans_thread;

  p_OG_Kohonen->appel_apprend = apprend_kohonen;
  p_OG_Kohonen->appel_gestion = gestion_groupe_kohonen; /* n'est pas appele PG !!!! */
  p_OG_Kohonen->appel_activite = mise_a_jour_neurone_kohonen;
  p_OG_Kohonen->function_new = new_kohonen;
  p_OG_Kohonen->destroy = destroy_kohonen;

  /* No_Special */
  p_OG_Special = &OG_table[No_Special];
  inherit(p_OG_Special, &OG_generique);
  p_OG_Special->appel_apprend = apprend_Special;
  p_OG_Special->appel_gestion = gestion_groupe_Special;
  p_OG_Special->appel_activite = mise_a_jour_neurone_special;

  /* No_Sigma_PI */
  p_OG_Sigma_PI = &OG_table[No_Sigma_PI];
  inherit(p_OG_Sigma_PI, &OG_generique);
  p_OG_Sigma_PI->appel_apprend = apprend_sigma_pi;
  p_OG_Sigma_PI->appel_algo = mise_a_jour_groupe_sigma_pi;

  /* No_Winner_Selectif */
  p_OG_Winner_Selectif = &OG_table[No_Winner_Selectif];
  inherit(p_OG_Winner_Selectif, &OG_generique);
  p_OG_Winner_Selectif->appel_apprend = apprend_winner_selectif;
  p_OG_Winner_Selectif->appel_gestion = recherche_max_locaux_sigma_pi;
  p_OG_Winner_Selectif->appel_activite = mise_a_jour_neurone_winner_selectif;
  p_OG_Winner_Selectif->function_new = new_selective_winner;
  p_OG_Winner_Selectif->appel_algo = /*mise_a_jour_normale_sans_thread; */mise_a_jour_normale;

  /* No_Winner_Selectif_Modulated */
  p_OG_Winner_Selectif_Modulated = &OG_table[No_selective_winner_modulated];
  inherit(p_OG_Winner_Selectif_Modulated, &OG_generique);
  p_OG_Winner_Selectif_Modulated->appel_gestion = recherche_max_locaux_winner_selectif_modulated;
  p_OG_Winner_Selectif_Modulated->appel_apprend = apprend_winner_selectif_modulated;
  p_OG_Winner_Selectif_Modulated->appel_activite = mise_a_jour_neurone_winner_selectif_modulated_macroneuron;
  p_OG_Winner_Selectif_Modulated->function_new = new_winner_selectif_modulated;
  p_OG_Winner_Selectif_Modulated->appel_algo = mise_a_jour_normale; //mise_a_jour_neurone_winner_selectif_modulated_threaded ;

  /* No_Winner_Colonne */
  p_OG_Winner_Colonne = &OG_table[No_Winner_Colonne];
  inherit(p_OG_Winner_Colonne, &OG_generique);
  p_OG_Winner_Colonne->appel_apprend = apprend_winner_colonne;
  p_OG_Winner_Colonne->appel_gestion = gestion_groupe_macro_neurone_recherche_max;
  p_OG_Winner_Colonne->appel_activite = mise_a_jour_neurone_colonne;

  /* No_But */
  /*Attention, il faut utiiser goal.c.nico avec ce bloc de code*/
  /*    p_OG_But = &OG_table[No_But];
   inherit(p_OG_But, &OG_generique);
   p_OG_But->appel_apprend = apprend_but;
   p_OG_But->appel_activite =*/
  /*Pour Bruno mise_a_jour_neurone_but_bloqueur */
  /* mise_a_jour_neurone_but_bloqueur;*/
  /*Pour Sacha le 14/05/99 *//* pb type lien */
  /* p_OG_But->appel_algo = mise_a_jour_groupe_but; *//* pb type lien */

  /* No_But */
  p_OG_But = &OG_table[No_But];
  inherit(p_OG_But, &OG_generique);
  p_OG_But->appel_apprend = apprend_but;
  /*p_OG_But->appel_activite =*/
  /*Pour Bruno mise_a_jour_neurone_but_bloqueur */
  /*    mise_a_jour_neurone_but_bloqueur;*/
  /*Pour Sacha le 14/05/99 *//* pb type lien */
  p_OG_But->appel_algo = mise_a_jour_groupe_but; /* pb type lien */
  p_OG_But->function_new = new_goal;
  p_OG_But->destroy = destroy_goal;
  /* No_Pyramidal_plan */
  /*Pour les applis de Nico, il faut utiliser ca.c.nico*/
  p_OG_Pyramidal_plan = &OG_table[No_Pyramidal_plan];
  inherit(p_OG_Pyramidal_plan, &OG_generique);
  p_OG_Pyramidal_plan->appel_apprend = apprend_ca;
  p_OG_Pyramidal_plan->appel_activite = mise_a_jour_ca;
  p_OG_Pyramidal_plan->appel_algo = mise_a_jour_groupe_ca;

  /* No_SAW */
  p_OG_SAW = &OG_table[No_SAW];
  inherit(p_OG_SAW, &OG_generique);
  p_OG_SAW->function_new = new_saw;
  p_OG_SAW->appel_apprend = apprend_saw;
  p_OG_SAW->appel_algo = mise_a_jour_normale;/* threadee ... */
  p_OG_SAW->appel_gestion = gestion_groupe_saw;
  p_OG_SAW->appel_activite = mise_a_jour_saw;
  p_OG_SAW->destroy = destroy_saw;

  /* No_Kmean_R */
  p_OG_Kmean_R = &OG_table[No_Kmean_R];
  inherit(p_OG_Kmean_R, &OG_generique);
  p_OG_Kmean_R->function_new = new_Kmean_R;
  p_OG_Kmean_R->appel_apprend = apprend_Kmean_R;
  p_OG_Kmean_R->appel_algo = mise_a_jour_normale;/* threadee ... */
  p_OG_Kmean_R->appel_gestion = gestion_groupe_Kmean_R;
  p_OG_Kmean_R->appel_activite = mise_a_jour_Kmean_R;
  p_OG_Kmean_R->destroy = destroy_Kmean_R;

  /* No_CTRNN2 */
  p_OG_CTRNN2 = &OG_table[No_CTRNN2];
  inherit(p_OG_CTRNN2, &OG_generique);
  p_OG_CTRNN2->function_new = new_CTRNN2;
  p_OG_CTRNN2->appel_apprend = apprend_CTRNN2;
  p_OG_CTRNN2->appel_algo = mise_a_jour_normale;/* threadee ... */
  p_OG_CTRNN2->appel_activite = mise_a_jour_neurone_CTRNN2;
  p_OG_CTRNN2->destroy = destroy_CTRNN2;

  /* No_Macro_Colonne */
  p_OG_Macro_Colonne = &OG_table[No_Macro_Colonne];
  inherit(p_OG_Macro_Colonne, &OG_generique);
  p_OG_Macro_Colonne->appel_activite = mise_a_jour_neurone_macro_colonne;
  p_OG_Macro_Colonne->appel_apprend = apprend_macro_colonne;
  p_OG_Macro_Colonne->appel_gestion = gestion_groupe_macro_colonne;

  /* No_PLG */
  p_OG_PLG = &OG_table[No_PLG];
  inherit(p_OG_PLG, &OG_generique);
  /*a refaire */
  p_OG_PLG->appel_apprend = apprend_plg;
  p_OG_PLG->appel_gestion = gestion_groupe_plg;
  p_OG_PLG->appel_activite = mise_a_jour_plg;

  /* asso_cond : No_Asso_Cond*/
  p_Asso_Cond = &OG_table[No_Asso_Cond];
  inherit(p_Asso_Cond, p_OG_Hebb);
  p_Asso_Cond->appel_gestion = gestion_Asso_Cond;
  p_Asso_Cond->appel_apprend = apprend_Asso_Cond;

  /* No_PCR */
  p_OG_PCR = &OG_table[No_PCR];
  inherit(p_OG_PCR, p_OG_Hebb);
  p_OG_PCR->function_new = new_PCR;
  p_OG_PCR->appel_activite = update_neural_activity_PCR;
  p_OG_PCR->appel_gestion = update_group_PCR;
  p_OG_PCR->appel_apprend = learn_PCR;
  p_OG_PCR->destroy = no_destroy;

  /* RCO: ALEX */
  p_OG_RCO = &OG_table[No_RCO];
  inherit(p_OG_RCO, &OG_generique);
  p_OG_RCO->function_new = new_RCO_neuron;
  p_OG_RCO->appel_activite = mise_a_jour_RCO_neuron;
  p_OG_RCO->appel_gestion = gestion_groupe_RCO_neuron;
  p_OG_RCO->appel_apprend = apprend_RCO_neuron;
  p_OG_RCO->destroy = destroy_RCO_neuron;

  dprints("fin init objets\n");
}

/* pour faire des test avec les librairies dynamiques */
void jefaisrien()
{
  /*get_max_dof_katana_arm();*//* demande par une fonction de IO_Robot car elle n'est pas charger pas le main */
}

type_group_function_pointers NN_Core_function_pointers[] =
  {
    { "debut", rien, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_debut", rien, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_stop_if_non_rt", function_stop_if_non_rt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_min_period", function_min_period, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ca3", function_ca3, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ca3_2", function_ca3_2, new_ca3_2, NULL, NULL, NULL, -1, -1 },
    { "f_qlearning", function_qlearning, new_qlearning, destroy_qlearning, NULL, NULL, -1, -1 },
    { "f_assocond", function_assocond, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_spike_prediction", function_spike_prediction, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_vigilance2neurone", function_vigilance2neurone, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_neurone2vigilance", function_neurone2vigilance, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_flip_flop", function_flip_flop, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_transition_detect", function_transition_detect, new_transition_detect, NULL, NULL, NULL, -1, -1 },
    { "f_even_first_transition_detect", function_even_first_transition_detect, new_even_first_transition_detect, NULL, NULL, NULL, -1, -1 },
    { "f_STM", function_STM, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_grabimages_cleanbuffer", rien, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_amplifi", function_amplifi, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_clean_next", function_clean_next, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_clean_next_conditionel", function_clean_next_conditionel, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cleans_conditionel", function_cleans_conditionel, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_clean", function_clean, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cleans", function_cleans, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_raise_fault", function_raise_fault, new_raise_fault, destroy_raise_fault, NULL, NULL, -1, -1 },
    { "f_DG_1", function_DG, new_DG_1, destroy_DG, NULL, NULL, -1, -1 },
    { "f_DG_t2", function_DG, new_DG_t2, destroy_DG, NULL, NULL, -1, -1 },
    { "f_DG_v3", function_DG, new_DG_v3, destroy_DG, NULL, NULL, -1, -1 },
    { "f_WTA_sorin", function_WTA_sorin, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_TimeGenerator", function_TimeGenerator, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_InitialiseHippo", function_InitialiseHippo, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_EC", function_EC, new_function_EC, destroy_function_EC, NULL, NULL, -1, -1 },
    { "f_base_de_temps", function_base_de_temps, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_observable_moyen", function_observable_moyen, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_recupere_micro_neurone", recupere_micro_neurone, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_seuil_et_memoire", function_seuil_et_memoire, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ctrnn", function_ctrnn, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_lissage_temp", function_lissage_temp, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_fin_echelle", function_fin_echelle, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_learning_error", function_learning_error, new_learning_error, destroy_learning_error, NULL, NULL, -1, -1 },
  /* pour indiquer la fin du tableau*/
    { NULL, NULL, NULL, NULL, NULL, NULL, -1, -1 } };

void set_group_function_pointers(type_group_function_pointers *group_function_pointers)
{
  int no_gpe;
  int i, j;
  int neurone_courant = 0;

  for (no_gpe = 0; no_gpe < nbre_groupe; no_gpe++)
  {
    i = 0;
    /* Initialization of the first neuron of the group */
    def_groupe[no_gpe].premier_ele = neurone_courant;
    neurone_courant = neurone_courant + def_groupe[no_gpe].nbre;

    j = def_groupe[no_gpe].type;
    /* Initialization of the algorithmic functions */
    if ((j == No_Fonction_Algo) || (j == No_Fonction_Algo_mvt))
    {
      while (group_function_pointers[i].name != NULL)
      {
        if (strcmp(def_groupe[no_gpe].nom, group_function_pointers[i].name) == 0)
        {
          def_groupe[no_gpe].appel_algo = group_function_pointers[i].appel_algo;
          if (group_function_pointers[i].new != NULL) def_groupe[no_gpe].function_new = group_function_pointers[i].new;
          if (group_function_pointers[i].destroy != NULL) def_groupe[no_gpe].destroy = group_function_pointers[i].destroy;
          if (group_function_pointers[i].save != NULL) def_groupe[no_gpe].save = group_function_pointers[i].save;
        }
        i++;
      }
    }
  }
}
/*---------------------------------------------------*/
/* lancee au debut du prog et c'est tout             */
/*---------------------------------------------------*/
void initialisation_des_groupes(void)
{
  /*typedef void (*New)(int);
   typedef void (*Appel_algo)(int);
   typedef void (*Destroy)(int);*/

  int no_gpe;
  int i, j; /*, success;*/
  char s[200];
  int size_name, is_f_a_comment;
  int neurone_courant = 0;
  char *buffer, home[MAXBUF], ld_prom_path[MAXBUF];

  /* Variables pour le chargement dynamique des fonctions */
  /*char filename_of_library[MAXIMUM_SIZE_OF_FILENAME], name_of_function[MAXIMUM_SIZE_OF_FILENAME];*/
  /*void *lib_handle;*/

  /*New pointer_to_new;*/
  /*Appel_algo pointer_to_appel_algo;*/
  /*Destroy pointer_to_destroy;*/

  dprints("initialisation des groupes\n");

  init_objets();

  /** ***************************************

   Groups initialization

   For a specific neural group

   - sets the learning function of the neurons in the group
   - sets the update function of the neurons in the group
   - sets the specific management of group

   For algorithmic functions

   - make the correspondance between the name of the function
   as it appears in Leto and the actual C function it must call

   ******************************************/
  for (no_gpe = 0; no_gpe < nbre_groupe; no_gpe++)
  {

    /* Initialization of the supplementary field for pandora (debug) */
    para_pando_group[no_gpe].nbre_neuronne_to_send_links = 0;
    para_pando_group[no_gpe].neuronne_to_send_links = -1;
    para_pando_group[no_gpe].frequence_specifique_groupe = -1.0;

    /* Initialization of the first neuron of the group */
    def_groupe[no_gpe].premier_ele = neurone_courant;
    neurone_courant = neurone_courant + def_groupe[no_gpe].nbre;

    j = def_groupe[no_gpe].type;

    if (j >= nbre_type_groupes) EXIT_ON_ERROR("Le type %d du groupe %d est superieur au nbre_type_groupes = %d defini dans reseau.h \n It is probably because 'reseau.h' (in prom_kernel) has been changed and you need to recompile leto/coeos and regenerate <file>.res.", j, no_gpe, nbre_type_groupes);

    inherit(&def_groupe[no_gpe], &OG_table[j]);
    dprints("fin heritage des proprietes\n");

    def_groupe[no_gpe].fault = 0;
    /* Initialization of the algorithmic functions */

    if ((j == No_Fonction_Algo) || (j == No_Fonction_Algo_mvt))
    {
      sprintf(s, "%s", def_groupe[no_gpe].nom);

      size_name = strlen(s);
      is_f_a_comment = 0;
      for (i = 0; i < size_name; i++)
      {
        if (s[i] == '%' || s[i] == '?')
        {
          is_f_a_comment = 1;
          break;
        }
      }
      if (is_f_a_comment == 1)
      {
        kprints("Function '%s' is not loaded because commented by '%c'.\n", s, s[i]);
        def_groupe[no_gpe].appel_algo = rien;
      }
    }
  }

  set_group_function_pointers(NN_Core_function_pointers);

  buffer = getenv("HOME");
  if (buffer == NULL)
  {
    kprints("WARNING $HOME is null !!! check you bash env...\n");
    strcpy(home, "./");
  }
  strcpy(home, buffer);
  buffer = getenv("LD_PROM_PATH"); /* variable d'environnement pour specifier le chemin des librairies dynamiques chargees explicitement par promethe */
  if (buffer == NULL)
  {
    sprintf(ld_prom_path, "%s/bin_leto_prom/Libraries/prom_user", home);
  }
  else sprintf(ld_prom_path, "%s%s", home, buffer);

  module_cuda = init_module(ld_prom_path, "libfast_computation", MODULE_OPTIONAL);

#ifndef AVEUGLE /* gui */
  /* initialisation des librairies dynamiques */

#ifdef DEBUG_COMPILATION
  SigProc_libdyn = init_module(ld_prom_path,"libSigProc_gui_debug", MODULE_REQUIRED);
  IHM_libdyn = init_module(ld_prom_path,"libIHM_gui_debug", MODULE_REQUIRED);
  IO_Robot_libdyn = init_module(ld_prom_path,"libIO_Robot_gui_debug", MODULE_REQUIRED);
  Parellel_Comm_libdyn = init_module(ld_prom_path,"libParallel_Comm_gui_debug", MODULE_REQUIRED);
  NN_IO_libdyn = init_module(ld_prom_path,"libNN_IO_gui_debug", MODULE_REQUIRED);
  Sensors_libdyn = init_module(ld_prom_path,"libSensors_gui_debug", MODULE_REQUIRED);
#else
  SigProc_libdyn = init_module(ld_prom_path, "libSigProc_gui_release", MODULE_REQUIRED);
  IHM_libdyn = init_module(ld_prom_path, "libIHM_gui_release", MODULE_REQUIRED);
  IO_Robot_libdyn = init_module(ld_prom_path, "libIO_Robot_gui_release", MODULE_REQUIRED);
  Parellel_Comm_libdyn = init_module(ld_prom_path, "libParallel_Comm_gui_release", MODULE_REQUIRED);
  NN_IO_libdyn = init_module(ld_prom_path, "libNN_IO_gui_release", MODULE_REQUIRED);
  Sensors_libdyn = init_module(ld_prom_path, "libSensors_gui_release", MODULE_REQUIRED);
#endif
#else
#ifdef DEBUG_COMPILATION
  SigProc_libdyn = init_module(ld_prom_path, "libSigProc_blind_debug", MODULE_REQUIRED);
  IHM_libdyn = init_module(ld_prom_path, "libIHM_blind_debug", MODULE_REQUIRED);
  IO_Robot_libdyn = init_module(ld_prom_path, "libIO_Robot_blind_debug", MODULE_REQUIRED);
  Parellel_Comm_libdyn = init_module(ld_prom_path, "libParallel_Comm_blind_debug", MODULE_REQUIRED);
  NN_IO_libdyn = init_module(ld_prom_path, "libNN_IO_blind_debug", MODULE_REQUIRED);
  Sensors_libdyn = init_module(ld_prom_path, "libSensors_blind_debug", MODULE_REQUIRED);
#else
  SigProc_libdyn = init_module(ld_prom_path, "libSigProc_blind_release", MODULE_REQUIRED);
  IHM_libdyn = init_module(ld_prom_path, "libIHM_blind_release", MODULE_REQUIRED);
  IO_Robot_libdyn = init_module(ld_prom_path, "libIO_Robot_blind_release", MODULE_REQUIRED);
  Parellel_Comm_libdyn = init_module(ld_prom_path, "libParallel_Comm_blind_release", MODULE_REQUIRED);
  NN_IO_libdyn = init_module(ld_prom_path, "libNN_IO_blind_release", MODULE_REQUIRED);
  Sensors_libdyn = init_module(ld_prom_path, "libSensors_blind_release", MODULE_REQUIRED);

#endif
#endif

  for (no_gpe = 0; no_gpe < nbre_groupe; no_gpe++)
  {
    initialise_modulation(no_gpe);
  }

  for (i = 0; i < nbre_groupe; i++)
  {
    for (j = 0; j < def_groupe[i].nbre; j++)
    {
      neurone[j + def_groupe[i].premier_ele].groupe = i;
    }
  }
}

/*---------------------------------------------------*/

FILE *fpedr;

void initialisation_utilisateur(void)
{
  /* etrange tout est unused ? en tout cas ca compile et link */
  /* unused :
   int i,nb,result;
   #ifndef AVEUGLE
   TxPoint point;
   #endif
   float Niveau_hydratation,Niveau_nourriture;
   char chaine[255];
   */
  /*fpedr=fopen("test_arm.txt","r"); */

}

/*---------------------------------------------------*/

void fin_simulation_utilisateur(void)
{
  gdouble seconds;

  seconds = g_timer_elapsed(simulation_timer, NULL);
  kprints("End of simulation lasting for %lf seconds\n", seconds);
}

void set_defaults(void)
{
  int i;
  sprintf(VIDEO_DEV, "%s", "/dev/video");
  sprintf(PORT, "%s", "/dev/cua0");
  sprintf(SPORT, "%s", "/dev/cua1");
  DEBIT = 9600;
  USE_ROBOT = 0;
  USE_CAM = UNUSED;
  USE_KATANA = UNUSED;
  USE_HEAD = UNUSED;
  CAM_MODE = 180;
  INIT_PAN = 90;
  INIT_TILT = 90;
  PAN_ADDR = 1;
  TILT_ADDR = 7;
  MOTOR_1 = MOTOR_2 = MOTOR_3 = MOTOR_4 = MOTOR_5 = -1;
  OFFSET_PAN = 0;

  for (i = 0; i < 5; i++)
    MOTOR_LOGICAL_STATUS[i] = 0;

  BS_crhaut = 715;
  BS_crbas = 511;
  bcmin = 462;
  bcmax = 768;
  bsmin = 457;
  bsmax = 773;
  USE_COMPASS = 1;
  EMISSION_ROBOT = 0;
}

/*
 sert a afficher la liste d'arguments
 */

void affiche_args(arguments * arg)
{
  if (arg->progname == NULL) EXIT_ON_ERROR("progname\t: null\n");

  if (arg->script != NULL) kprints("script\t\t: %s\n", arg->script);
  else EXIT_ON_ERROR("Script missing ! \n usage [b]promethe[_debug] appli.script appli.config appli.res [appli.dev] [-n appli_name] [appli.prt]");

  if (arg->res != NULL) kprints("res\t\t: %s\n", arg->res);
  else EXIT_ON_ERROR("Network (.res) missing ! \n usage [b]promethe[_debug] appli.script appli.config appli.res [appli.dev] [-n appli_name] [appli.prt]");

  if (arg->config != NULL) kprints("config\t\t: %s\n", arg->config);
  else EXIT_ON_ERROR("Config file missing ! \n usage [b]promethe[_debug] appli.script appli.config appli.res [appli.dev] [-n appli_name] [appli.prt]");

  if (arg->dev != NULL) kprints("dev\t\t: %s\n", arg->dev);
  else kprints("dev\t\t: null\n");

  if (arg->gcd != NULL) kprints("gcd\t\t: %s\n", arg->gcd);
  else kprints("gcd\t\t: null\n");

  if (arg->prt != NULL) kprints("prt\t\t: %s\n", arg->prt);
  else kprints("prt\t\t: null\n");

  if (arg->VirtualNetwork != NULL) kprints("Network name (-n)\t: %s\n", arg->VirtualNetwork);
  else kprints("Network name (-n)\t: null\n");

  /*
   if (arg->mcpu != NULL)
   printf("CPU mask \t: %s\n", arg->mcpu);
   else
   printf("CPU mask \t: null\n");
   */

  /*
   if (arg->telnet != NULL)
   printf("telnet \t\t: %s\n", arg->telnet);
   else
   printf("telnet \t\t: null\n");
   */

  if (arg->ivy_bus != NULL) kprints("ivy_bus (-b) \t: %s\n", arg->ivy_bus);
  else kprints("ivy_bus (-b)\t: null\n");

  if (arg->AutresFlags != NULL) kprints("AutresFlags \t: %s\n", arg->AutresFlags);
  else kprints("AutresFlags \t: null\n");

}

void parse_args(int argc, char *argv[], arguments * args)
{
  int size;
  int i, opt;
  char c;
  /*char *canonical_name;*/
  extern char *optarg;
  extern int optind, opterr, optopt;
  char nom[MAXBUF];
  FILE *f1;

  static int verbose_flag;
  /* getopt_long stores the option index here. */
  int option_index = 0;

  static struct option long_options[] =
    {
    /* These options set a flag. */
      { "verbose", no_argument, &verbose_flag, 1 },
      { "brief", no_argument, &verbose_flag, 0 },
      { "sync", no_argument, &verbose_flag, 0 },
      { "g-fatal-warnings", no_argument, &verbose_flag, 0 },
      { "version", no_argument, NULL, 'v' },
      { "distant-terminal", no_argument, &distant_terminal, 1 },
    /* These options don't set a flag.
     We distinguish them by their indices. */
      { "script", required_argument, 0, 's' },
      { "res", required_argument, 0, 'r' },
      { "config", required_argument, 0, 'c' }, /* avant d changement PG, erreur? */
      { "dev", required_argument, 0, 'd' },
      { "gcd", required_argument, 0, 'g' },
      { "prt", required_argument, 0, 'p' },
      { "net", required_argument, 0, 'n' },
      { "DEBUG", required_argument, 0, 'D' },
      { "WINDOWS", required_argument, 0, 'W' },
      { "STARTMODE", required_argument, 0, 'S' },
      { "mask_cpu", required_argument, 0, 'm' },
      { "telnet", required_argument, 0, 't' },
      { "ivy_bus", required_argument, 0, 'b' },
      { "bus_id", required_argument, 0, 'i' },
      { 0, 0, 0, 0 } };

  args->progname = NULL;
  args->script = NULL;
  args->res = NULL;
  args->config = NULL;
  args->dev = NULL;
  args->gcd = NULL;
  args->prt = NULL;
  args->VirtualNetwork = NULL; /*net*/
  args->DebugState = NULL; /*debug*/
  args->StartMode = NULL; /*startmode*/
  args->Windows = NULL; /*windows*/
  args->mcpu = NULL; /*mask_cpu */
  args->telnet = NULL;
  args->ivy_bus = NULL;
  args->bus_id = NULL;
  args->AutresFlags = NULL;
  opt = 1;

  while (opt)
  {
    c = getopt_long(argc, argv, "s:r:c:d:p:n:D:S:W:m:t:b:vi:", long_options, &option_index);

    switch (c)
    {
    case 's':
      size = 1 + strlen(optarg);
      if ((args->script = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->script, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'r':
      size = 1 + strlen(optarg);
      if ((args->res = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->res, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'c':
      size = 1 + strlen(optarg);
      if ((args->config = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->config, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'd':
      size = 1 + strlen(optarg);
      if ((args->dev = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->dev, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'g':
      size = 1 + strlen(optarg);
      if ((args->gcd = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->gcd, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'p':
      size = 1 + strlen(optarg);
      if ((args->prt = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->prt, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'n':
      size = 1 + strlen(optarg);
      if ((args->VirtualNetwork = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->VirtualNetwork, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'D':
      size = 1 + strlen(optarg);
      args->DebugState = MANY_ALLOCATIONS(size, char);
      strncpy(args->DebugState, optarg, size);
      break;
    case 'S':
      size = 1 + strlen(optarg);
      if ((args->StartMode = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->StartMode, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'W':
      size = 1 + strlen(optarg);
      if ((args->Windows = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->Windows, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'm':
      size = 1 + strlen(optarg);

      if ((args->mcpu = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->mcpu, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
      /*---------------------*/
    case 't':
      size = 1 + strlen(optarg);
      if ((args->telnet = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->telnet, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'b':
      size = 1 + strlen(optarg);
      if ((args->ivy_bus = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->ivy_bus, optarg, size);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;
    case 'v':
      kprints("*** Promethe ***\n");
      prom_kernel_print_version();
      kprints("\nprom_user\n");
      kprints("==========\n");
      kprints("svn version: %s\n\n", STRINGIFY_CONTENT(SVN_REVISION));
      exit(0);
      break;
    case 'i':
      size = 1 + strlen(optarg);
      if ((args->bus_id = malloc(size * sizeof(char))) != NULL)
      {
        strncpy(args->bus_id, optarg, size);
        strncpy(bus_id, optarg, BUS_ID_MAX);
      }
      else
      {
        kprints("%s: unable to alloc memory\n", __FUNCTION__);
      }
      break;

    case -1:
      opt = 0;
      break;
      /*---------------------*/
    default:
      kprints("see previous message for error\n");
      break;
    }
  }

  /* parse pour le cas sans options */
  /*default*/
  if (args->Windows == NULL)
  {
    if ((args->Windows = malloc(3 * sizeof(char))) != NULL)
    {
      strncpy(args->Windows, "-1\0", 3);
    }
    else
    {
      kprints("%s: unable to alloc memory\n", __FUNCTION__);
    }
  }
  if (args->StartMode == NULL)
  {
    if ((args->StartMode = malloc(3 * sizeof(char))) != NULL)
    {
      strncpy(args->StartMode, "-1\0", 3);
    }
    else
    {
      kprints("%s: unable to alloc memory\n", __FUNCTION__);
    }
  }

  if (args->DebugState == NULL)
  {
    args->DebugState = MANY_ALLOCATIONS(3, char);
    strncpy(args->DebugState, "-1\0", 3);
  }

  /* le nom du prog */
  size = 1 + strlen(argv[0]);
  if ((args->progname = malloc(size * sizeof(char))) != NULL)
  {
    strncpy(args->progname, argv[0], size);
  }
  else
  {
    kprints("%s: unable to alloc memory\n", __FUNCTION__);
  }

  /* maintenant on parse a la recherche des ext */
  for (i = optind; i < argc; i++)
  {
    size = strlen(argv[i]);
    /* le script */
    if (size > 4 && args->script == NULL)
    {
      if ((strncmp(".script", argv[i] + size - 7, 7)) == 0)
      {
        if ((args->script = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->script, argv[i], size);
          args->script[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }

      if ((strncmp(".script_o", argv[i] + size - 9, 9)) == 0)
      {
        if ((args->script = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->script, argv[i], size);
          args->script[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le config */
    if (size > 4 && args->config == NULL)
    {
      if ((strncmp(".config", argv[i] + size - 7, 7)) == 0)
      {
        if ((args->config = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->config, argv[i], size);
          args->config[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le res */
    if (size > 4 && args->res == NULL)
    {
      if ((strncmp(".res", argv[i] + size - 4, 4)) == 0)
      {
        if ((args->res = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->res, argv[i], size);
          args->res[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le dev */
    if (size > 4 && args->dev == NULL)
    {
      if ((strncmp(".dev", argv[i] + size - 4, 4)) == 0)
      {
        if ((args->dev = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->dev, argv[i], size);
          args->dev[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le gcd */
    if (size > 4 && args->gcd == NULL)
    {
      if ((strncmp(".gcd", argv[i] + size - 4, 4)) == 0)
      {
        if ((args->gcd = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->gcd, argv[i], size);
          args->gcd[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le prt */
    if (size > 4 && args->prt == NULL)
    {
      if ((strncmp(".prt", argv[i] + size - 4, 4)) == 0)
      {
        if ((args->prt = malloc((size + 1) * sizeof(char))) != NULL)
        {
          strncpy(args->prt, argv[i], size);
          args->prt[size] = '\0';
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

    /* le bus (ivy_bus) */
    if (size > 4 && args->prt == NULL)
    {
      if ((strncmp(".bus", argv[i] + size - 4, 4)) == 0)
      {
        f1 = fopen(argv[i], "r");
        if (f1 == NULL) EXIT_ON_ERROR("Cannot open file %s\n", argv[i]);
        if (fscanf(f1, "%s", nom) != 1) EXIT_ON_ERROR("Scanf fail with %s", f1);

        if ((args->ivy_bus = malloc(strlen(nom) + 1)) != NULL)
        {
          strcpy(args->ivy_bus, nom); /* le contenu du fichier doit etre le numero du port pour ivy */
        }
        else
        {
          kprints("%s: unable to alloc memory\n", __FUNCTION__);
        }
      }
    }

  }
  affiche_args(args);
}

/*---------------------------------------------------------------*/

void fct_module_rien()
{
  dprints("**************** fonction rien module cuda *************************\n");
}

/* pointeurs de fonction pour des tests avec cuda. L'usage final reste a preciser... */

void (*dll_multiply_init)(int size_A, int size_B, int size_C);
void (*dll_multiply_matrix)(float *h_A, int size_A, float *h_B, int size_B, float *h_C, int size_C);
void (*dll_multiply_free)();

void (*demo_function)();

void *init_module(const char *path, const char *module_name, int required)
{
  void *module;
  char nom_fichier[MAXBUF];
  type_group_function_pointers *group_function_pointers;

#ifdef Darwin
  sprintf(nom_fichier,"%s/%s.dylib",path,module_name);
#else
  sprintf(nom_fichier, "%s/%s.so", path, module_name);
#endif
  /* Load dynamically loaded library */
  module = dlopen(nom_fichier, RTLD_NOW);

  if (!module)
  {
    if (required)
    {
      EXIT_ON_ERROR("Module required '%s' not found.\n\tError: %s", nom_fichier, dlerror());
    }
    else
    {
      /*	kprints("Optional %s not loaded. %s\n", nom_fichier, dlerror());*/
      return NULL;
    }
  }

  group_function_pointers = dlsym(module, "group_function_pointers");
  set_group_function_pointers(group_function_pointers);

  return module;
}

void close_module(void *module)
{
  /* All done, close things cleanly */
  if (module != NULL) dlclose(module);
}

/*---------------------------------------------------------------*/
/*                      PROG PRINCIPAL                           */
/*---------------------------------------------------------------*/

struct timeval prom_started_date;

static char *Argv[NB_MAX_ARG];

int main(int argc, char *argv[])
{
  int i;
  int Argc = 0;
  unsigned long mask = 1; /* 2 processor 0 */

  for (i = 0; i < NB_MAX_ARG; i++)
    Argv[i] = NULL; /* init des pointeurs sur le tableau d'arguments qui sera envoye au kernel */

#ifndef ROUTEUR
  setlocale(LC_ALL, "C");
#endif

  parse_args(argc, argv, &args);

  if (pthread_mutex_init(&mutex_list_prom_fft, NULL))
  {
    kprints("*** %s : %d : Can't initialise mutex : mutex_list_prom_fft ***", __FUNCTION__, __LINE__);
  }

  /*
   je reconstruis le tableau d'arguments tel que l'attend
   le kernel, le plus simple serait AMHA de faire le menage
   dans le kernel !
   */

  if (args.progname != NULL)
  {
    Argv[No_arg_progname] = args.progname;
    Argc++;
  }
  else
  {
    /* ca ne devrait jamais arriver ! */
    Argv[No_arg_progname] = NULL;
  }

  if (args.script != NULL)
  {
    Argv[No_arg_script] = args.script;
    Argc++;
  }
  else EXIT_ON_ERROR("A script must be provided to promethe.\n(use <file>.script or -s file_name)\n");

  if (args.res != NULL)
  {
    Argv[No_arg_res] = args.res;
    Argc++;
  }
  else EXIT_ON_ERROR("A <file>.res must be provided to promethe. \n(binary network compiled with leto or ccleto) \n");

  if (args.config != NULL)
  {
    Argv[No_arg_config] = args.config;
    Argc++;
  }
  else EXIT_ON_ERROR("ERROR a file.config must be provided to promethe.\n (config file with number of iteration and simulation speed and ...) \n");
  if (args.dev != NULL)
  {
    Argv[No_arg_dev] = args.dev;
    Argc++;
  }

  if (args.gcd != NULL)
  {
    Argv[No_arg_gcd] = args.gcd;
    Argc++;
  }
  if (args.prt != NULL)
  {
    Argv[No_arg_prt] = args.prt;
    Argc++;
  }
  if (args.VirtualNetwork != NULL)
  {
    Argv[No_arg_VirtualNetwork] = args.VirtualNetwork;
    Argc++;
  }

  if (args.DebugState != NULL)
  {
    Argv[No_arg_DebugState] = args.DebugState;
    Argc++;
  }
  if (args.StartMode != NULL)
  {
    Argv[No_arg_StartMode] = args.StartMode;
    Argc++;
  }
  if (args.Windows != NULL)
  {
    Argv[No_arg_Windows] = args.Windows;
    Argc++;
  }

  if (args.mcpu != NULL)
  {
    kprints("cpu = %s \n", args.mcpu);
    Argv[No_arg_mcpu] = args.mcpu;
    Argc++; /* 8 */
    kprints("mcpu Argc=%d \n", Argc);
    /* bind process to processor 0 */
    mask = (unsigned long) atoi(args.mcpu);
    kprints("CPU MASK main.c = %ld\n", mask);

#ifdef Darwin
    kprints("Warning: mcpu not activated. \n   sched_setaffinity() only works on Linux / not on Darwin... \n");
#else

    if (sched_setaffinity(0, sizeof(mask), (cpu_set_t*) &mask) < 0)
    {
      perror("sched_setaffinity");
    }
#endif
  }

  if (args.telnet != NULL)
  {
    Argv[No_arg_telnet] = args.telnet;
    Argc++;
  }

  if (args.ivy_bus != NULL)
  {
    Argv[No_arg_bus_ivy] = args.ivy_bus;
    Argc++;
  }

  if (args.AutresFlags != NULL)
  {
    Argv[No_arg_AutresFlags] = args.AutresFlags;
    Argc++;
  }

  for (i = 0; i < NB_MAX_ARG; i++)
  {
    if (Argv[i] == NULL) Argv[i] = (char*) calloc(1, sizeof(char)); /* une chaine de caracteres vide pour chaque parametre inexistant */
  }

  gettimeofday(&prom_started_date, (void *) NULL);

  Argc = NB_MAX_ARG;

  promethe_main(Argc, Argv);

  /* Ancienne couche reseau */
  /*fin_transmissions();*/

  global_close_io_server(); /* close io text socket for debug, console and kernel */

  close_module(SigProc_libdyn);
  close_module(IHM_libdyn);
  close_module(IO_Robot_libdyn);
  close_module(Parellel_Comm_libdyn);
  close_module(NN_IO_libdyn);
  close_module(Sensors_libdyn);

  close_module(module_cuda);

  return 0;
}

