/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup plugin_list
\defgroup libNN_IO libNN_IO
\brief Groups for input and output of neurons.
*/

#include <string.h>

#include <libx.h>

#include <group_function_pointers.h>

#include <NN_IO/Function_Map.h>
#include <NN_IO/Time_Tools.h>
#include <NN_IO/Info_Tools.h>
#include <NN_IO/NN_Tools.h>
#include <NN_IO/SequenceIO.h>
#include <NN_IO/Limbic.h>
#include <NN_IO/DSM.h>

#include <NN_IO/libNN_IO.h>





type_group_function_pointers group_function_pointers[]=
{
	/* Function Map */
	{"f_add_prod_adhoc", function_add_prod_adhoc, NULL, NULL, NULL, NULL, -1, -1},
	{"f_all_displacement", function_all_displacement, NULL, NULL, NULL, NULL, -1, -1},
	{"f_concat", function_concat, NULL, NULL, NULL, NULL, -1, -1}, /* a ne plus utilisee */
	{"f_diffuse", function_diffuse, NULL, NULL, NULL, NULL, -1, -1},
	/*{"f_integrn",function_integrn, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	{"f_integr", function_integr, new_integr, destroy_integr, NULL, NULL, -1, -1},
	
	/* Limbic */
	{"f_ask_limbic", function_ask_limbic, NULL, NULL, NULL, NULL, -1, -1},
	{"f_controle_vigilence", function_controle_vigilence, NULL, NULL, NULL, NULL, -1, -1},
	{"f_detection_ressources", function_detection_ressources, new_detection_ressources, NULL, NULL, NULL, -1, -1},
	{"f_DP_comparator", function_DP_comparator, NULL, NULL, NULL, NULL, -1, -1},
	{"f_drive_detected", function_drive_detected, NULL, NULL, NULL, NULL, -1, -1},
	{"f_drive_expected", function_drive_expected, NULL, NULL, NULL, NULL, -1, -1},
	{"f_drives", function_drives, new_drives, NULL, NULL, NULL, -1, -1},
	{"f_global_learn", function_global_learn, NULL, NULL, NULL, NULL, -1, -1},
	/*{"f_learn_context_distance", function_learn_context_distance, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	{"f_learn_context", function_learn_context, NULL, NULL, NULL, NULL, -1, -1},
	{"f_septum", function_septum, NULL, NULL, NULL, NULL, -1, -1},
	{"f_real_action", function_real_action, NULL, NULL, NULL, NULL, -1, -1},
	{"f_switch_motivation", function_switch_motivation, NULL, NULL, NULL, NULL, -1, -1},

	/* NN_Tools */
	{"f_1D_TO_2D", function_1D_TO_2D, NULL, NULL, NULL, NULL, -1, -1},
	{"f_all_n_link", function_all_n_link, NULL, NULL, NULL, NULL, -1, -1},
	/*{"f_ambigous_states_learning_produit", function_ambigous_states_learning_produit, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	/*{"f_ambigous_states_learning_distance", function_ambigous_states_learning_distance, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	{"f_ambigous_states_learning", function_ambigous_states_learning, NULL, NULL, NULL, NULL, -1, -1},
	{"f_ask_planification", function_ask_planification, NULL, NULL, NULL, NULL, -1, -1},
	{"f_baricentre", function_baricentre, NULL, NULL, NULL, NULL, -1, -1},
	{"f_bruit", function_bruit, NULL, NULL, NULL, NULL, -1, -1},
	{"f_bruit_sur_signal", function_bruit_sur_signal, NULL, NULL, NULL, NULL, -1, -1},
	{"f_concatenation", function_concatenation, NULL, NULL, NULL, NULL, -1, -1},
    {"f_concatenation_iter", function_concatenation_iter, new_concatenation_iter, destroy_concatenation_iter, NULL, NULL, -1, -1},
	{"f_detection_nouveaute", function_detection_nouveaute, NULL, NULL, NULL, NULL, -1, -1},
	{"f_analogize",  function_analogize,  new_analogize,  destroy_analogize,  NULL, NULL, -1, -1},
	{"f_discretize", function_discretize, new_discretize, destroy_discretize, NULL, NULL, -1, -1},
	{"f_erreur", function_erreur, NULL, NULL, NULL, NULL, -1, -1},
	{"f_esn", function_esn, new_esn, NULL, NULL, NULL, -1, -1},
	{"f_exponential", function_exponential, NULL, NULL, NULL, NULL, -1, -1},
	{"f_ext_plus_neurone", function_ext_plus_neurone, NULL, NULL, NULL, NULL, -1, -1},
	{"f_extract_neurons", function_extract_neurons, NULL, NULL, NULL, NULL, -1, -1},
	{"f_extract_population", function_extract_population, NULL, NULL, NULL, NULL, -1, -1},
	{"f_extract_coeffs", function_extract_coeffs, new_extract_coeffs, destroy_extract_coeffs, NULL, NULL, -1, -1},
	{"f_extract_multi_vals_to_one_vector", function_extract_multi_vals_to_one_vector, new_extract_multi_vals_to_one_vector, destroy_extract_multi_vals_to_one_vector, NULL, NULL, -1, -1},
	{"f_extract_val_to_vector", function_extract_val_to_vector, new_extract_val_to_vector, destroy_extract_val_to_vector, NULL, NULL, -1, -1},
	{"f_ext_to_gpe", function_ext_to_gpe, new_ext_to_gpe, destroy_ext_to_gpe, NULL, NULL, -1, -1},
	{"f_from_ana", function_from_ana, NULL, NULL, NULL, NULL, -1, -1},
	{"f_f_ana", function_from_ana, NULL, NULL, NULL, NULL, -1, -1},
	{"f_get_angle_from_orientation", f_get_angle_from_orientation, NULL, NULL, NULL, NULL, -1, -1},
	{"f_get_orientation", function_get_orientation, NULL, NULL, NULL, NULL, -1, -1},
	{"f_get_potential", function_get_potential, NULL, NULL, NULL, NULL, -1, -1},
	{"f_gpe_to_ext", function_gpe_to_ext, NULL, NULL, NULL, NULL, -1, -1},
	{"f_grad_to_max", function_grad_to_max, NULL, NULL, NULL, NULL, -1, -1},
	{"f_hebb_seuil", function_hebb_seuil, NULL, NULL, NULL, NULL, -1, -1},
	{"f_im_to_gpe", function_im_to_gpe, NULL, NULL, NULL, NULL, -1, -1},
	{"f_instar_gpe", function_instar_gpe, NULL, NULL, NULL, NULL, -1, -1},
	{"f_instar_dynamic_gpe", function_instar_dynamic_gpe, new_instar_dynamic_gpe, destroy_instar_dynamic_gpe, NULL, NULL, -1, -1},
	{"f_instar_multi_gpe", function_instar_multi_gpe, NULL, NULL, NULL, NULL, -1, -1},
	{"f_sigmo_norm_instar", function_sigmo_norm_instar, NULL, NULL, NULL, NULL, -1, -1},
	{"f_inverse_gpe_population", function_inverse_gpe_population, NULL, NULL, NULL, NULL, -1, -1},
	{"f_keep_last_active_neuron", f_keep_last_active_neuron, NULL, NULL, NULL, NULL, -1, -1},
	{"f_matrice_alea_entre_0_1", function_matrice_alea_entre_0_1, NULL, NULL, NULL, NULL, -1, -1},
	{"f_max_ana", function_max_ana, NULL, NULL, NULL, NULL, -1, -1},
   {"f_contraste_max", function_contraste_max, NULL, NULL, NULL, NULL, -1, -1},
	{"f_merge_potentials", function_merge_potentials, NULL, NULL, NULL, NULL, -1, -1},
	{"f_moyenne_etat", function_moyenne_etat, NULL, NULL, NULL, NULL, -1, -1},
	{"f_moyenne_oneshot", function_moyenne_oneshot, NULL, NULL, NULL, NULL, -1, -1},
	{"f_moy", function_moyenneur, NULL, NULL, NULL, NULL, -1, -1},
	{"f_nb_expressions", function_nb_expressions, new_nb_expressions, NULL, NULL, NULL, -1, -1},
	{"f_neurone_aleatoire", function_neurone_aleatoire, NULL, NULL, NULL, NULL, -1, -1},
	{"f_alea_population", function_neurone_aleatoire, NULL, NULL, NULL, NULL, -1, -1},
	{"f_neurone_seq", function_neurone_seq, NULL, NULL, NULL, NULL, -1, -1},
	{"f_neurone_seq_AR", function_neurone_seq_AR, new_neurone_seq_AR, NULL, NULL, NULL, -1, -1},
	{"f_neurone_seq2", function_neurone_seq2, NULL, NULL, NULL, NULL, -1, -1},
    {"f_neurone_seq_param", function_neurone_seq_param, new_neurone_seq_param, destroy_neurone_seq_param, NULL, NULL, -1, -1},
	{"f_neutre", function_neutre, NULL, NULL, NULL, NULL, -1, -1},
	{"f_normalisation_c", function_normalisation_c, NULL, NULL, NULL, NULL, -1, -1},
	{"f_normalize", function_normalize, NULL, NULL, NULL, NULL, -1, -1},
	{"f_normalize_", function_normalize_, NULL, NULL, NULL, NULL, -1, -1},
	{"f_normalize_PC", function_normalize_PC, NULL, NULL, NULL, NULL, -1, -1},
	{"f_norm_ana", function_norm_ana, NULL, NULL, NULL, NULL, -1, -1},
	{"f_norm_euclidienne", function_norm_euclidienne, NULL, NULL, NULL, NULL, -1, -1},
	{"f_pondere_puissance", f_pondere_puissance, NULL, NULL, NULL, NULL, -1, -1},
	{"f_population_to_neuron", function_population_to_neuron, NULL, NULL, NULL, NULL, -1, -1},
	{"f_pos_offset", function_pos_offset, NULL, NULL, NULL, NULL, -1, -1},
	{"f_previous_ext_to_neurone_at_this_position", f_previous_ext_to_neurone_at_this_position, NULL, NULL, NULL, NULL, -1, -1},
	{"f_product", function_product, NULL, NULL, NULL, NULL, -1, -1},
	{"f_pruning_input_group", function_pruning_input_group, NULL, NULL, NULL, NULL, -1, -1},
	{"f_random_variable", function_random_variable, new_random_variable, destroy_random_variable, NULL, NULL, -1, -1},
	{"f_read_and_map_angle_ana", function_read_and_map_angle_ana, NULL, NULL, NULL, NULL, -1, -1},
	{"f_reset_but", function_reset_but, NULL, NULL, NULL, NULL, -1, -1},
	{"f_reset_integr", function_reset_integr, NULL, NULL, NULL, NULL, -1, -1},
	{"f_seqN_WTA", function_seqN_WTA, NULL, NULL, NULL, NULL, -1, -1},
	{"f_set_zero", function_set_zero, NULL, NULL, NULL, NULL, -1, -1},
	{"f_shift_vector", f_shift_vector, NULL, NULL, NULL, NULL, -1, -1},
	{"f_simulation_status", function_simulation_status, NULL, NULL, NULL, NULL, -1, -1},
	{"f_s_to_s1", function_s_to_s1, NULL, NULL, NULL, NULL, -1, -1},
	{"f_sort", function_sort, new_sort, destroy_sort, NULL, NULL, -1, -1},
	{"f_qsort", function_qsort, new_qsort, destroy_qsort, NULL, NULL, -1, -1},
	{"f_sum_angle", function_sum_angle, NULL, NULL, NULL, NULL, -1, -1},
	{"f_supervision", function_supervision, new_supervision, NULL, NULL, NULL, -1, -1},
	{"f_s_winner_to_s1", function_s_winner_to_s1, NULL, NULL, NULL, NULL, -1, -1},
	{"f_switch_2groups", function_switch_2groups, NULL, NULL, NULL, NULL, -1, -1},
	{"f_switch_input", function_switch_input, NULL, NULL, NULL, NULL, -1, -1},
	/*{"f_telecommande", function_telecommande, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	/*{"f_test_results", function_test_results, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	{"f_transpose", f_transpose, NULL, NULL, NULL, NULL, -1, -1},
	{"f_valeur_emotionnelle", function_valeur_emotionnelle, NULL, NULL, NULL, NULL, -1, -1},
	{"f_write_conditionnal", function_write_conditionnal, NULL, NULL, NULL, NULL, -1, -1},
	{"f_x_1", function_x_1, NULL, NULL, NULL, NULL, -1, -1},
	{"f_z-1", function_z_n, new_z_n, destroy_z_n, NULL, NULL, -1, -1},
	{"f_z-n", function_z_n, new_z_n, destroy_z_n, NULL, NULL, -1, -1},
	{"f_random_bytes_expression", function_random_bytes, new_random_bytes, destroy_random_bytes, NULL, NULL, -1, -1},
	
	//{"f_points_Of_Interest", function_points_Of_Interest, new_points_Of_Interest, destroy_points_Of_Interest, NULL, NULL, -1, -1},
	{"f_calcul_distance", function_calcul_distance, new_calcul_distance, destroy_calcul_distance, NULL, NULL, -1, -1},
	{"f_nelder_mead", function_nelder_mead, new_nelder_mead, destroy_nelder_mead, NULL, NULL, -1, -1},
	{"f_choose_input", function_choose_input, new_choose_input, destroy_choose_input, NULL, NULL, -1, -1},
	{"f_compet_inter_cartes", function_compet_inter_cartes, new_compet_inter_cartes, destroy_compet_inter_cartes, NULL, NULL, -1, -1},
	
	/* Sequence IO */
	{"f_demande_nom_fichier", function_demande_nom_fichier, NULL, NULL, NULL, NULL, -1, -1},
	{"f_detect_traj_pt", function_detect_traj_pt, NULL, NULL, NULL, NULL, -1, -1},
	{"f_end_read_sequence", function_end_read_sequence, NULL, NULL, NULL, NULL, -1, -1},
	{"f_load_activity", function_load_activity, new_load_activity, destroy_load_activity, NULL, NULL, -1, -1},
	{"f_mean_position", function_mean_position, NULL, NULL, NULL, NULL, -1, -1},
	{"f_print_sequence", function_print_sequence, NULL, NULL, NULL, NULL, -1, -1},
	/*{"f_read_pos", function_read_pos, NULL, NULL, NULL, NULL, -1, -1}, inutilisee */
	{"f_read_sequence", function_read_sequence, NULL, NULL, NULL, NULL, -1, -1},
	{"f_read_sequence_conditionnal", function_read_sequence_conditionnal, NULL, NULL, NULL, NULL, -1, -1},
	{"f_read_sequence_discrete", function_read_sequence_discrete, NULL, NULL, NULL, NULL, -1, -1},
	{"f_read_sequence_discrete_alternate", function_read_sequence_discrete_alternate, NULL, NULL, NULL, NULL, -1, -1},
	{"f_read_sequence_discrete_analogique", function_read_sequence_discrete_analogique, new_read_sequence_discrete_analogique, NULL, NULL, NULL, -1, -1},
	{"f_self_organizing_sequences", function_self_organizing_sequences, NULL, NULL, NULL, NULL, -1, -1},
	{"f_write_sequence", function_write_sequence, NULL, NULL, NULL, NULL, -1, -1},
	
	/* Time Tools */
	{"f_clock", function_clock, NULL, NULL, NULL, NULL, -1, -1},
	{"f_delay", function_delay, new_delay, destroy_delay, NULL, NULL, -1, -1},
	{"f_delay_timing", function_delay_timing, new_delay_timing, destroy_delay_timing, NULL, NULL, -1, -1},
	{"f_delta_time", function_delta_time, new_f_delta_time, NULL, NULL, NULL, -1, -1},
	{"f_gettime", function_gettime, new_f_gettime, NULL, NULL, NULL, -1, -1},
	{"f_seconds_to_break", function_seconds_to_break, new_seconds_to_break, NULL, NULL, NULL, -1, -1},
	{"f_time_iterations", function_time_iterations, new_time_iterations, NULL, NULL, NULL, -1, -1},
	{"f_time_level", function_time_level, NULL, NULL, NULL, NULL, -1, -1},
	{"f_wait_nanosleep", function_wait_nanosleep, NULL, NULL, NULL, NULL, -1, -1},
	{"f_waste_time", function_waste_time, NULL, NULL, NULL, NULL, -1, -1},	
	{"f_alpha_cell", function_alpha_cell, new_alpha_cell, destroy_alpha_cell, NULL, NULL, -1, -1},

    /* Dynamic SensoriMotor */
    {"f_SLE_evaluate_threshold", function_SLE_evaluate_threshold, new_SLE_evaluate_threshold, destroy_SLE_evaluate_threshold, NULL, NULL, -1, -1},
    //{"f_DSM_catego", function_DSM_catego, new_DSM_catego, destroy_DSM_catego, NULL, NULL, -1, -1}, /* OLD VERSION */
    {"f_SLE_catego_DSM", function_SLE_catego_DSM, new_SLE_catego_DSM, destroy_SLE_catego_DSM, NULL, NULL, -1, -1},
    {"f_SLE_catego_SW", function_SLE_catego_SW, new_SLE_catego_SW, destroy_SLE_catego_SW, NULL, NULL, -1, -1},
    {"f_SLE_asso", function_SLE_asso, new_SLE_asso, destroy_SLE_asso, NULL, NULL, -1, -1},
    {"f_DSM_output", function_DSM_output, new_DSM_output, destroy_DSM_output, NULL, NULL, -1, -1},
    {"f_DSM_main", function_DSM_main, new_DSM_main, destroy_DSM_main, NULL, NULL, -1, -1},
    {"f_DSM_motor_learn", function_DSM_motor_learn, new_DSM_motor_learn, destroy_DSM_motor_learn, NULL, NULL, -1, -1},

  /* pour indiquer la fin du tableau*/
  {NULL, NULL, NULL, NULL, NULL, NULL, -1, -1}
};

void read_help();

void modify_help();


int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}
