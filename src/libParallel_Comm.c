/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup plugin_list
\defgroup libParrallel_Com libParrallel_Com
\brief Groups for parallel communication (e.g. between different computers). Ex: f_send and f_recv.
*/


#include <string.h>

#include <libx.h>

#include <libParallel_Comm.h>

#include <group_function_pointers.h>

type_group_function_pointers group_function_pointers[]=
{
	{"f_receive_PVM_ack_block", function_receive_PVM_ack_block, new_receive_PVM_ack_block, destroy_receive_PVM_ack_block, NULL, NULL, -1, -1},
	{"f_receive_PVM_block", function_receive_PVM_block, new_receive_PVM_block, destroy_receive_PVM_block, NULL, NULL, -1,- 1},
	{"f_receive_PVM_block_ack", function_receive_PVM_block_ack,	new_receive_PVM_block_ack, destroy_receive_PVM_block_ack, NULL, NULL, -1, -1},
	{"f_receive_PVM_non_block", function_receive_PVM_non_block, new_receive_PVM_non_block, destroy_receive_PVM_non_block, NULL, NULL, -1,- 1},
	{"f_receive_PVM_non_block_ack", function_receive_PVM_non_block_ack, new_receive_PVM_non_block_ack, destroy_receive_PVM_non_block_ack, NULL, NULL, -1, -1},
	{"f_receive_non_block_ack", function_receive_PVM_non_block_ack, new_receive_PVM_non_block_ack, destroy_receive_PVM_non_block_ack, NULL, NULL, -1, -1},
	{"f_receive_SOCKET_ack_block", function_receive_SOCKET_ack_block, new_receive_SOCKET_ack_block, destroy_receive_SOCKET_ack_block, NULL, NULL, -1, -1}, 
	{"f_receive_SOCKET_block", function_receive_SOCKET_block, new_receive_SOCKET_block, destroy_receive_SOCKET_block, NULL, NULL, -1, -1},
	{"f_receive_SOCKET_block_ack", function_receive_SOCKET_block_ack, new_receive_SOCKET_block_ack, destroy_receive_SOCKET_block_ack, NULL, NULL, -1, -1},
	{"f_receive_SOCKET_non_block", function_receive_SOCKET_non_block, new_receive_SOCKET_non_block, destroy_receive_SOCKET_non_block, NULL, NULL, -1, -1},
	{"f_receive_SOCKET_non_block_ack", function_receive_SOCKET_non_block_ack, new_receive_SOCKET_non_block_ack, destroy_receive_SOCKET_non_block_ack, NULL, NULL, -1, -1},
	{"f_receive_non_block_ack", function_receive_SOCKET_non_block_ack, new_receive_SOCKET_non_block_ack, destroy_receive_SOCKET_non_block_ack, NULL, NULL, -1, -1},
	{"f_recv", function_recv, new_recv, destroy_recv, NULL, NULL, -1, -1},
	//{"f_recv_enet", function_recv_enet, new_recv_enet, destroy_recv_enet, NULL, NULL, -1, -1},
	{"f_send", function_send, new_send, destroy_send, NULL, NULL, -1, -1},
	//{"f_send_enet", function_send_enet, new_send_enet, destroy_send_enet, NULL, NULL, -1, -1},
	{"f_send_PVM", function_send_PVM, new_send_PVM, destroy_send_PVM, NULL, NULL, -1, -1},
	{"f_send_PVM_ack", function_send_PVM_ack, new_send_PVM_ack, destroy_send_PVM_ack, NULL, NULL, -1, -1},
	{"f_send_PVM_ack_sync", function_send_PVM_ack_sync, new_send_PVM_ack_sync, destroy_send_PVM_ack_sync, NULL, NULL, -1, -1},
	{"f_send_SOCKET", function_send_SOCKET, new_send_SOCKET, destroy_send_SOCKET, NULL, NULL, -1, -1},
	{"f_send_SOCKET_ack", function_send_SOCKET_ack, new_send_SOCKET_ack, destroy_send_SOCKET_ack, NULL, NULL, -1, -1},
	{"f_send_SOCKET_ack_sync", function_send_SOCKET_ack_sync, new_send_SOCKET_ack_sync, destroy_send_SOCKET_ack_sync, NULL, NULL, -1, -1},
	{"f_network_id", function_network_id, new_network_id, destroy_network_id, NULL, NULL, -1, -1},
	{"f_publish", function_publish, new_publish, destroy_publish, NULL, NULL, -1, -1},
	{"f_send_udp", function_send_udp, new_send_udp, NULL, NULL, NULL, -1, -1},
	{"f_recv_udp", function_recv_udp, new_recv_udp, NULL, NULL, NULL, -1, -1},
   {"f_in", function_in, new_in, destroy_in, NULL, NULL, -1, -1},
   {"f_out", function_out, new_out, destroy_out, NULL, NULL, -1, -1},


  /* pour indiquer la fin du tableau*/
  {NULL, NULL, NULL, NULL, NULL, NULL, -1, -1}
};

void read_help();

void modify_help();


int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}

