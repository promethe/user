/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_load_caracteristics.c f_load_caracteristics.c
\brief


\section Creation
- author: A.Karaouzene
- description: specific file creation
- date: 16/12/2012


\section Description
Le groupe foit contenir 4 neurones en X et 3 neurones en Y.
Chaque colonne (les Y) represente une categorie (objet, emotion, position)
Les lignes (les X) represente une composante du vecteur categorie.

Objet    & o1,o2,o3,o4
Position & p1,p2,p3,p4
Emotion  & e1,e2,e3,e4

En plus de l'image qui est chargée sur le champs ext.
\section Macro
-none

\section Local varirables
-none

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

\file

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */


/* #define DEBUG 1 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include "../../prom_kernel/include/promethe.h"
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>


#ifdef USE_THREADS
extern pthread_mutex_t mutex_lecture_clavier;
#endif

#define NB_MAX_OBJETS_PER_IMAGE 8


typedef struct data_load_caracteristics
{
	char caracfilename[255];
	FILE *carac_df;
	int known;
	int reload;
} Data_load_caracteristics;


void new_function_load_caracteristics(int gpe)
{
	Data_load_caracteristics *mydata=NULL;
	int  i=0,j=0;
	char param_link[255];
	dprints("enter %s\n",__FUNCTION__);

	mydata=(Data_load_caracteristics*)malloc(sizeof(Data_load_caracteristics));
	if(mydata==NULL)
		EXIT_ON_ERROR("pb malloc dans %d\n",gpe);
	mydata->reload = 0;
	mydata->carac_df = NULL;
	i=0;
	j = find_input_link(gpe,i);
	while(j!=-1)
	{
		if (prom_getopt(liaison[j].nom, "-f", param_link) == 2)
		{
			strcpy(mydata->caracfilename,param_link);    dprints("fichier des caracteristiques = %s\n", mydata->caracfilename);
		}
		if (prom_getopt(liaison[j].nom, "-known", param_link) == 1)
		{
			mydata->known = 1;   dprints("Only known images will be loaded\n");
		}
		if (prom_getopt(liaison[j].nom, "-r", param_link) == 1)
		{
			mydata->reload = 1;   dprints("reload set to 1\n");
		}
		/*if (prom_getopt(liaison[j].nom, "-save", param_link) >= 2)
			{
				sprintf(mydata->savefilename,"%s",param_link);   dprints("store file = %s\n", mydata->savefilename);
			}
			if (prom_getopt(liaison[j].nom, "-deb", param_link) == 1)
			{
				mydata->gpe_deb = liaison[j].depart ;   dprints("Restart group = %d\n", mydata->gpe_deb);
			}*/
		i++;
		j = find_input_link(gpe,i);
	}

	def_groupe[gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
	if (def_groupe[gpe].ext == NULL)
		EXIT_ON_ERROR("No enought memory in %s",__FUNCTION__);

   ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0]==NULL;

	/*ouverture du fichier des caracteristique*/
	mydata->carac_df = fopen(mydata->caracfilename,"r");
	if(mydata->carac_df==NULL)
		EXIT_ON_ERROR("Cannot open file %s in %s",mydata->caracfilename,__FUNCTION__);
	def_groupe[gpe].data=mydata;
}


void function_load_caracteristics(int gpe)
{
	Data_load_caracteristics *mydata=NULL;
	FILE * carac_df;
	char  line[512];
	char image[255];
	char carac[255];
	char caracfilename[255];
	char *p;
	int taille = 0, nb_obj = 0, objet[NB_MAX_OBJETS_PER_IMAGE],position_y[NB_MAX_OBJETS_PER_IMAGE], position[NB_MAX_OBJETS_PER_IMAGE],emotion[NB_MAX_OBJETS_PER_IMAGE];
	int j =0 , /*i=0,*/ q =0;
	int nb_real_obj = 0, known;
	unsigned char *im_table;
	int width, height, nbands;
	int test_scanf;
	char reponse_user[255];
	int reload,c=0;
	int nb_val_lues=0;

	(void)known; // (unused)


	dprints("essai...");
	mydata = (Data_load_caracteristics *) def_groupe[gpe].data;
	carac_df = (FILE *) mydata->carac_df;
	known = mydata->known;
	reload = mydata->reload;
	strcpy(caracfilename,mydata->caracfilename);
	/*recuperer la ligne */
	if ( fgets(line,512, carac_df) == NULL)
	{
		if (reload == 1)
		{
			c = fclose(carac_df);
			if (c==0) dprints("\nsucces closing file");
			else EXIT_ON_ERROR("\nCannot close %s ",caracfilename);
			carac_df = fopen(caracfilename, "r");
			mydata->carac_df =  carac_df;
			((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df =  carac_df;
			if ( fgets(line,512, carac_df) == NULL)
				EXIT_ON_ERROR("\nA la relecture le fichier est vide !! %s",caracfilename);
		}
		else
		{
#ifdef USE_THREADS
			pthread_mutex_lock(&mutex_lecture_clavier);
#endif
			cprints("\nFin du fichier  %s : \n \t Voulez vous rejouer la sequence [o/n] : ",caracfilename);
			test_scanf = scanf("%s", reponse_user);
#ifdef USE_THREADS
			pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
			if(test_scanf == 1)
			{
				if (reponse_user[0] != 'n')
				{
					fclose(carac_df);
					carac_df = fopen(caracfilename, "r");
					mydata->carac_df =  carac_df;
					((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df =  carac_df;
					if ( fgets(line,512, carac_df) == NULL)  EXIT_ON_ERROR("\nA la relecture le fichier est vide !! %s",caracfilename);
				}
				else
				{
					cprints("\n Promethe quit!! \n");
					//fclose(carac_df);
					cancel_pressed(NULL,NULL);
					return;
				}
			}
		}
	}

	/*recuperer les caracteristiques : nom d'image, num objet et position*/

	p = line;
	dprints("\nligne = %s",line);
	sscanf (p,"%s",carac);
	while (strstr(carac,"}") != NULL || strstr(carac,"%") != NULL)
	{
		dprints("commented line\n");
		/*lire la prochaine ligne*/
		if ( fgets(line,512, carac_df) == NULL)
		{
			if (mydata->reload == 1)
			{
				fclose(carac_df);
				carac_df = fopen(caracfilename, "r");
				mydata->carac_df =  carac_df;
				((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df =  carac_df;
				if ( fgets(line,512, carac_df) == NULL)
					EXIT_ON_ERROR("\nA la relecture le fichier est vide !! %s",caracfilename);
			}
			else
			{
#ifdef USE_THREADS
				pthread_mutex_lock(&mutex_lecture_clavier);
#endif
				cprints("\nFin du fichier  %s : \n \t Voulez vous rejouer la sequence [o/n] : ",caracfilename);
				test_scanf = scanf("%s", reponse_user);
#ifdef USE_THREADS
				pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
				if(test_scanf == 1)
				{
					if (reponse_user[0] != 'n')
					{
						fclose(carac_df);
						carac_df = fopen(caracfilename, "r");
						mydata->carac_df =  carac_df;
						((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df =  carac_df;
						if ( fgets(line,512, carac_df) == NULL)
							EXIT_ON_ERROR("\nA la relecture le fichier est vide !! %s",caracfilename);
					}
					else
					{
						cprints("\n Promethe quit!! \n");
						//fclose(carac_df);
						cancel_pressed(NULL,NULL);
						return;
					}
				}
			}
		}
		p = line;
		dprints("\nligne = %s",line);
		sscanf (p,"%s",carac);
	}

	/**recupere nom de l'image*/
	sscanf (p,"{%s",carac);
	taille = strlen (carac);
	strcpy (image,carac);
	p += taille + 3;
	dprints("image : %s \n", image);

	/*recuperer le couple objet,position*/
	sscanf (p,"%s",carac);
	taille = strlen (carac);
	nb_obj = 0;
	j = 0;
	nb_real_obj = 0;

//	printf("Analyse ligne:\n ==> %s",carac);
	while (strstr (carac,"}") == NULL)
	{
		nb_val_lues=sscanf(carac,"%d,%d,%d,%d;",&objet[j],&position[j],&position_y[j],&emotion[j]);
		if(nb_val_lues!=4) printf("ERROR %s nb_val_lues = %d \n",__FUNCTION__, nb_val_lues);
		dprints("(%d,%d,%d,%d)",objet[j],position[j],position_y[j],emotion[j]);
		/*if (objet[j]*position[j] <= 0)
			objet[j] = position[j] = -1;
		else*/
		nb_real_obj ++;
		nb_obj ++;
		p += taille+1;
		sscanf (p,"%s",carac);
		taille = strlen (carac);
		j++;
		if(j>NB_MAX_OBJETS_PER_IMAGE)
		{
			EXIT_ON_ERROR ("Too many objects described in the file %s on line : \n   %s\n",caracfilename,line);
		}

	}
	/*if (nb_real_obj <= 0)
		return;*/
	/*initialiser tous les neurones à -1*/
	for (j = 0 ; j < def_groupe[gpe].nbre ; j++)
		neurone[def_groupe[gpe].premier_ele + j].s = neurone[def_groupe[gpe].premier_ele + j].s1 = neurone[def_groupe[gpe].premier_ele + j].s2 = -1;
	if(def_groupe[gpe].taillex < nb_obj)
		EXIT_ON_ERROR ("Nombre d'objet (%d) est different de la taille du groupe en y %s (%d) in %s", nb_obj,def_groupe[gpe].no_name,def_groupe[gpe].taillex,__FUNCTION__);

	q = 0;

	for (j = 0 ; j <  nb_obj ; j++)
	{
		q = j;
		dprints("%d: %d , (%d , %d) ,  %d \n",q, objet[j], position[j], position_y[j], emotion[j]);
		/*copier les objets sur la ligne 1*/
		neurone[def_groupe[gpe].premier_ele + q].s = neurone[def_groupe[gpe].premier_ele + q].s1 = neurone[def_groupe[gpe].premier_ele + q].s2 = objet[j];
		/*copier les positions sur la ligne 2*/
		q = j + def_groupe[gpe].taillex ;
		neurone[def_groupe[gpe].premier_ele + q].s = neurone[def_groupe[gpe].premier_ele + q].s1 = neurone[def_groupe[gpe].premier_ele + q].s2 = position[j];
		/*copier les positions y sur la ligne 3*/
		q = j + (2 * def_groupe[gpe].taillex) ;
		neurone[def_groupe[gpe].premier_ele + q].s = neurone[def_groupe[gpe].premier_ele + q].s1 = neurone[def_groupe[gpe].premier_ele + q].s2 = position_y[j];
		/*copier les expressions faciales sur la colonne*/
		q = j + (3 * def_groupe[gpe].taillex) ;
		neurone[def_groupe[gpe].premier_ele + q].s = neurone[def_groupe[gpe].premier_ele + q].s1 = neurone[def_groupe[gpe].premier_ele + q].s2 = emotion[j];
	}
	strcpy(((MyData_f_load_image_sequential *) def_groupe[gpe].data)->jice_nom_image,image);
	/* afficher l'image en cours*/
   if(((prom_images_struct *) def_groupe[gpe].ext)->images_table[0]!=NULL) free(((prom_images_struct *) def_groupe[gpe].ext)->images_table[0]);
	load_image_from_disk("PNG",image,&im_table, &width,&height, &nbands);
	/*mettre l'image sur le champs ext du groupe*/
	((prom_images_struct *) def_groupe[gpe].ext)->sx = width;
	((prom_images_struct *) def_groupe[gpe].ext)->sy = height;
	((prom_images_struct *) def_groupe[gpe].ext)->nb_band = nbands;
	((prom_images_struct *) def_groupe[gpe].ext)->image_number = 1;
	((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] = im_table;
	dprints("\nimage %s (%d,%d) chargee\n",image, width, height);
	dprints("end of line \n");
 
}
void destroy_function_load_caracteristics(int gpe)
{
	if(((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df !=NULL)
		fclose(((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df);
	/*if((fclose(((Data_load_caracteristics *) def_groupe[gpe].data)->carac_df))!=0)
			PRINT_WARNING("UNABLE TO CLOSE %s\n",((Data_load_caracteristics *)def_groupe[gpe].data)->caracfilename);
		else
			PRINT_WARNING("SUCCESS TO CLOSE %s\n",((Data_load_caracteristics *)def_groupe[gpe].data)->caracfilename);*/

	if(def_groupe[gpe].data != NULL)
	{
		free(def_groupe[gpe].data);
		def_groupe[gpe].data = NULL;
	}


	dprints("exiting %s (file : %s)\n", __FUNCTION__, __FILE__);
}



