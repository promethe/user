/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_save_caracteristics f_save_caracteristics
\brief


\section Creation
- author: A.Karaouzene
- description: specific file creation
- date: 14/12/2012


\section Description


\section Macro
-none

\section Local varirables
-none

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

\file

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */


/*#define DEBUG*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>



typedef struct data_save_caracteristics
{
	int gpe_image;
	int gpe_obj;
	int gpe_pos_x;
        int gpe_pos_y;
	int gpe_deb;
	int gpe_emo;
	char savefilename[255];
	FILE *save_df;
} Data_save_caracteristics;


void new_function_save_caracteristics(int gpe)
{
	Data_save_caracteristics *mydata=NULL;
	int  i=0,j=0;
	char param_link[255];
	dprints("enter %s\n",__FUNCTION__);

	mydata=(Data_save_caracteristics*)malloc(sizeof(Data_save_caracteristics));
	if(mydata==NULL)
		EXIT_ON_ERROR("pb malloc dans %d\n",gpe);
	mydata->gpe_deb = mydata->gpe_image = mydata->gpe_emo = mydata->gpe_pos_x =mydata->gpe_pos_y = mydata->gpe_obj =-1;
	i=0;
	j = find_input_link(gpe,i);
	while(j!=-1)
	{
		if (prom_getopt(liaison[j].nom, "-image", param_link) == 1)
		{
			mydata->gpe_image = liaison[j].depart;    dprints("gpe image d entree = %d\n", mydata->gpe_image);
		}
		if (prom_getopt(liaison[j].nom, "-obj", param_link) == 1)
		{
			mydata->gpe_obj = liaison[j].depart;   dprints("gpe objet = %d\n", mydata->gpe_obj);
		}
		if (prom_getopt(liaison[j].nom, "-pos_x", param_link) == 1)
		{
			mydata->gpe_pos_x = liaison[j].depart;   dprints("gpe pos_x = %d\n", mydata->gpe_pos_x);
		}
                if (prom_getopt(liaison[j].nom, "-pos_y", param_link) == 1)
		{
			mydata->gpe_pos_y = liaison[j].depart;   dprints("gpe pos_y = %d\n", mydata->gpe_pos_y);
		}
		if (prom_getopt(liaison[j].nom, "-emo", param_link) == 1)
		{
			mydata->gpe_emo = liaison[j].depart;   dprints("gpe emo = %d\n", mydata->gpe_pos_x);
		}
		if (prom_getopt(liaison[j].nom, "-save", param_link) >= 2)
		{
			sprintf(mydata->savefilename,"%s",param_link);   dprints("store file = %s\n", mydata->savefilename);
		}
		if (prom_getopt(liaison[j].nom, "-deb", param_link) == 1)
		{
			mydata->gpe_deb = liaison[j].depart ;   dprints("Restart group = %d\n", mydata->gpe_deb);
		}
		i++;
		j = find_input_link(gpe,i);
	}


	if (mydata->gpe_obj == -1 || mydata->gpe_image == -1 || mydata->gpe_pos_x == -1 || mydata->gpe_pos_y == -1  )
		EXIT_ON_ERROR("ERROR : %s need 3 input groups with options \"-image\" \"-obj\" \"-pos\" ",__FUNCTION__);
	/*ouverture du fichier d enregistrerement*/
	mydata->save_df = fopen(mydata->savefilename,"w+");
	if(mydata->save_df==NULL)
		EXIT_ON_ERROR("Cannot open file %s in %s",mydata->savefilename,__FUNCTION__);
	def_groupe[gpe].data=mydata;
}

void function_save_caracteristics(int gpe)
{
	Data_save_caracteristics *mydata=NULL;
	FILE * save_df;
	/*char nom_image[255];*/
	char savefilename[255];
	int gpe_image,gpe_pos_x,gpe_pos_y,gpe_obj,gpe_deb,gpe_emo;
	int objet,position_x,position_y,emotion;
	mydata = (Data_save_caracteristics *) def_groupe[gpe].data;
	save_df = (FILE *) mydata->save_df;
	gpe_image = mydata->gpe_image;
	gpe_obj = mydata->gpe_obj;
	gpe_pos_x = mydata->gpe_pos_x;
        gpe_pos_y = mydata->gpe_pos_y;
	gpe_deb = mydata->gpe_deb;
	gpe_emo = mydata->gpe_emo;
	strcpy(savefilename,mydata->savefilename);

	/*recuperer le nom de l'image en traitement*/
	if (gpe_deb != -1)
	{
		if (neurone[def_groupe[gpe_deb].premier_ele].s1 > 0.9)
		{
			fprintf(save_df," }\n{%s :", ((MyData_f_load_image_sequential *) def_groupe[gpe_image].data)->jice_nom_image );

		}
	}
	else
		fprintf(save_df," }\n{%s :", ((MyData_f_load_image_sequential *) def_groupe[gpe_image].data)->jice_nom_image );

	/*recuperer les caracteristiques objet et position de l'objet*/
	objet = (int)neurone[def_groupe[gpe_obj].premier_ele].s1;
	position_x = (int) neurone[def_groupe[gpe_pos_x].premier_ele].s1;
        position_y = (int) neurone[def_groupe[gpe_pos_y].premier_ele].s1;
	if (gpe_emo != -1)
		emotion = (int)neurone[def_groupe[gpe_emo].premier_ele].s1;
	else
		emotion = -1;

	if (objet * position_x > 0)
		fprintf(save_df," %d,%d,%d,%d;",objet,position_x,position_y,emotion);
	else
		fprintf(save_df," -1,-1,-1,-1;");

}
void destroy_function_save_caracteristics(int gpe)
{
	fprintf(((Data_save_caracteristics *) def_groupe[gpe].data)->save_df," }\n");
	fclose(((Data_save_caracteristics *) def_groupe[gpe].data)->save_df);

	free(def_groupe[gpe].data);
	def_groupe[gpe].data = NULL;
	dprints("exiting %s (file : %s)\n", __FUNCTION__, __FILE__);
}
