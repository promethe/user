/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>
#include <math.h>

typedef struct MyData_f_add_flow
{
   int InputGpe,PX, PY,color,resolution;
   double thick;
   prom_images_struct *prom_flow;
   float *U, *V,amplification;
   void *window;
}MyData_f_add_flow;


void function_affiche_flux_vecteurs(int Gpe)
{
#ifndef AVEUGLE
   int InputGpe=-1;
   prom_images_struct *prom_flow=NULL;
   int sx, sy,i,j,resolution=10;
   float *U, *V,amplification=2.;
   double thick=1;
   void *window = &image1;
   MyData_f_add_flow *My_data;
   int row, column, PX=20, PY=20,IdWindow=1,color=17;
   char param[256];
   TxPoint origin, destination;

   if (def_groupe[Gpe].data == NULL)
   {
      i=0;
      while((j=find_input_link(Gpe,i))!=-1)
      {

         if (prom_getopt(liaison[j].nom, "-I", param) == 2)
         {
            InputGpe = liaison[j].depart;
            prom_flow = (prom_images_struct *) def_groupe[InputGpe].ext;
            IdWindow = atoi(param);
            if(IdWindow==2) window = &image2;
            else window = &image1;
         }

         if (prom_getopt(liaison[j].nom, "-thick", param) == 2)
         {
            thick = atof(param);
            kprints("valeur de l'epaisseur %f\n",thick);
         }
         if (prom_getopt(liaison[j].nom, "-PX", param) == 2)
         {
            PX = atoi(param);
            kprints("valeur de PX %d\n",PX);
         }
         if (prom_getopt(liaison[j].nom, "-PY", param) == 2)
         {
            PY = atoi(param);
            kprints("valeur de PY %d\n",PY);
         }

         if (prom_getopt(liaison[j].nom, "-C", param) == 2)
         {
            color = atoi(param);
            kprints("valeur de color %d\n",color);
         }
         if (prom_getopt(liaison[j].nom, "-D", param) == 2)
         {
            resolution = atoi(param);
            kprints("valeur de precision %d\n",resolution);
         }
         if (prom_getopt(liaison[j].nom, "-f", param) == 2)
         {
            amplification = atof(param);
            kprints("valeur de l'amplification %f\n",amplification);
         }
         i++;

      }
      /*verification de la presence de groupes en amont */
      if (InputGpe == -1)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "pas de groupe amont...\n");
      }

      /*verif d'une carte entrante */
      if (def_groupe[InputGpe].ext == NULL)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "pointeur null sur le lien entrant\n");
      }




      sx = prom_flow->sx;		sy = prom_flow->sy;

      My_data = (MyData_f_add_flow *) malloc(sizeof(MyData_f_add_flow));
      def_groupe[Gpe].data = (MyData_f_add_flow *)My_data;

      if(My_data==NULL)
      {
         printf("Erreur de malloc dans f_add_flow\n");
      }


      My_data->U=U= (float *)prom_flow->images_table[0];
      My_data->V=V= (float *)prom_flow->images_table[1];
      My_data->InputGpe=InputGpe;
      My_data->prom_flow=prom_flow;
      My_data->window=window;
      My_data->PX=PX;
      My_data->PY=PY;
      My_data->color=color;
      My_data->thick=thick;
      My_data->resolution=resolution;
      My_data->amplification=amplification;
   }
   else
   {
      My_data = (MyData_f_add_flow *) def_groupe[Gpe].data;
      U= My_data->U;
      V= My_data->V;
      window=My_data->window;
      PX=My_data->PX;
      PY=My_data->PY;
      thick=My_data->thick;
      color=My_data->color;
      prom_flow=My_data->prom_flow;
      sx = prom_flow->sx;		sy = prom_flow->sy;
      resolution=My_data->resolution;
      amplification=My_data->amplification;
   }


   for (row = 0; row < sy - 1; row+=resolution)
   {
      for (column = 0; column < sx - 1; column+=resolution)
      {
         origin.y= PY + row;
         origin.x= PX + column;
         destination.y= PY + row - V[row*sx +column]*amplification;
         destination.x= PX + column - U[row*sx+column]*amplification; // Vrai que car U inversé !
         //destination.x= PX + column + U[row*sx+column]*amplification;
         TxDessinerFleche(window, color, origin, destination, thick);
      }
   }

   TxFlush(window);

#endif
   (void)Gpe;
}
