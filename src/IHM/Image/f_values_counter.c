/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_values_counter

\author Syed Khursheed HASNAIN
\date 15/11/2012

\details
		This program can be used as a debug program for optical flow algorithm. Normally, Camera 			takes the picture of 320*240 (according to our settings). However, it will be difficult 		to display all the pixels in promethe (to save computational resources), therefore we 			define the resolution less than 320*240 in [f_ext_to_gpe] for example 32*24 pixels. By 			reduced resolution we can not know how much pixels are + ve and -ve.

This algorithm tells us (in command window) how many pixels are +ve and how many are -ve in the actual resolution of 320*240 pixels.
\section Options
\subsection Sorties
-		-S0		: (s0, s1 ou S2)
-		-I 	: No need to put any thing. Simply -I is sufficent
-		 f	: Name of the file where data to be saved
-		-D	: Resolution


************************************************************/



#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>
#include <math.h>

typedef struct MyData_f_counter
{
   int InputGpe,resolution,pos_counter,neg_counter,pos_counterV,neg_counterV;
   prom_images_struct *prom_flow;
   float *U,value_U,*V,value_V;
   void *window;
   FILE *fp;
} MyData_f_counter;


void function_values_counter(int Gpe)
{
#ifndef AVEUGLE
   int InputGpe=-1;
   prom_images_struct *prom_flow=NULL;
   int sx, sy,i,j,resolution=10,pos_counter=0,neg_counter=0,pos_counterV=0,neg_counterV=0;
   float *U,value_U=0.,*V,value_V=0.;
   void *window = &image1;
   MyData_f_counter *My_data;
   int row, column,IdWindow=1;
   char param[256],string[255];
   char *file_name = NULL;
   FILE *fp = NULL;


   if (def_groupe[Gpe].data == NULL)
   {
      i=0;
      while ((j=find_input_link(Gpe,i))!=-1)
      {

         if (prom_getopt(liaison[j].nom, "-I", param) == 2)
         {
            InputGpe = liaison[j].depart;
            prom_flow = (prom_images_struct *) def_groupe[InputGpe].ext;
            IdWindow = atoi(param);
            if (IdWindow==2) window = &image2;
            else window = &image1;
         }

         if (prom_getopt(liaison[j].nom, "f", param) == 2)
         {
            sprintf(string, "%s.SAVE", param);
            file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
            strcpy(file_name, string);
         }


         if (prom_getopt(liaison[j].nom, "-D", param) == 2)
         {
            resolution = atoi(param);
            kprints("valeur de precision %d\n",resolution);
         }
         i++;

      }
      /*verification de la presence de groupes en amont */
      if (InputGpe == -1)
      {
         printf("%s : pas de groupe amont...\n", __FUNCTION__);
         exit(0);
      }

      /*verif d'une carte entrante */
      if (def_groupe[InputGpe].ext == NULL)
      {
         printf("%s : pointeur null sur le lien entrant\n", __FUNCTION__);
         exit(0);
      }

      sx = prom_flow->sx;		sy = prom_flow->sy;

      printf("Image size in X =%d\n", sx);
      printf("Image size in Y =%d\n", sy);

      My_data = (MyData_f_counter *) malloc(sizeof(MyData_f_counter));
      def_groupe[Gpe].data = (MyData_f_counter *)My_data;

      if (My_data==NULL)
      {
         printf("Erreur de malloc dans f_add_flow\n");
      }


      My_data->U=U= (float *)prom_flow->images_table[0];
      My_data->V=V= (float *)prom_flow->images_table[1];
      My_data->InputGpe=InputGpe;
      My_data->prom_flow=prom_flow;
      My_data->window=window;
      My_data->resolution=resolution;
      My_data->pos_counter=pos_counter;
      My_data->neg_counter=neg_counter;
      My_data->pos_counterV=pos_counterV;
      My_data->neg_counterV=neg_counterV;
      My_data->value_U = value_U;
      My_data->value_V = value_V;
      My_data->fp = fp;
   }
   else
   {
      My_data = (MyData_f_counter *) def_groupe[Gpe].data;
      U= My_data->U;
      V= My_data->V;
      window=My_data->window;
      prom_flow=My_data->prom_flow;
      sx = prom_flow->sx;		sy = prom_flow->sy;
      resolution=My_data->resolution;
      My_data->pos_counter=pos_counter;
      My_data->neg_counter=neg_counter;
      My_data->pos_counterV=pos_counterV;
      My_data->neg_counterV=neg_counterV;
      value_U = My_data->value_U;
      value_V = My_data->value_V;
      fp = My_data->fp;
   }


   for (row = 0; row < sy - 1; row+=resolution)
   {
      for (column = 0; column < sx - 1; column+=resolution)
      {

         value_U = U[row*sx+column];
         value_V = U[row*sx+column];

         /* Opening the file for writing */
         /*fp = fopen("data.SAVE", "a");
         if(fp != NULL)
         {fprintf(fp, "%f\n", value_U);}
         else
         {printf("fp = %f, Erreur d'ouverture du fichier",fp);}
         My_data->fp = NULL;
         fclose(fp);*/


         if (value_U >= 0 )
         {
            pos_counter = pos_counter + 1;
         }
         else
         {
            neg_counter = neg_counter + 1;
         }

         if (value_V >= 0 )
         {
            pos_counterV = pos_counterV + 1;
         }
         else
         {
            neg_counterV = neg_counterV + 1;
         }

      }
   }

   printf("Number of postive values U [ ] =%d\n", pos_counter);
   printf("Number of negative values U [ ] =%d\n", neg_counter);


   /*printf("Vector U =%f\n", value_U);*/


   /*------------------------------*/
   /* Opening the file for writing */



   fp = fopen("data.SAVE", "a");

   /*fprintf(fp, "%f\n", value_U);*/

   if (fp != NULL)
   {fprintf(fp, "%d %d\n", pos_counter,neg_counter);}
   else
   {printf("fp = data.SAVE, Erreur d'ouverture du fichier");}

   My_data->fp = NULL;

   fclose(fp);


   /*--------------------------------*/
   My_data->pos_counter=0;
   My_data->neg_counter=0;
   My_data->pos_counterV=0;
   My_data->neg_counterV=0;
   My_data->value_U = 0;
   My_data->value_V = 0;

   TxFlush(window);
#else
   (void)Gpe;
#endif
}
