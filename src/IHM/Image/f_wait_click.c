/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libIHM
\defgroup f_wait_click f_wait_click
\brief 


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004


\section Description
 

\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

\file 

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>


void function_wait_click(int gpe)
{
#ifndef AVEUGLE
	TxPoint robot_pos;
	struct timespec duree_nanosleep, res;
	dprints("enter f_wait_click\n");
	if(def_groupe[gpe].nbre!=2)
	{
		EXIT_ON_ERROR("il faut 2 neurones dans f_wait_click_robot\n");

	}
	
	duree_nanosleep.tv_sec = 0;
	duree_nanosleep.tv_nsec = 50000000;

	while(dragging_image1!=1)
		nanosleep(&duree_nanosleep, &res);
	
	robot_pos.x=image1_posx;
	robot_pos.y=image1_posy;
	printf("Clic dans %s x = %f  y = %f  ",__FUNCTION__,image1_posx,image1_posy);
	neurone[def_groupe[gpe].premier_ele].s=
	neurone[def_groupe[gpe].premier_ele].s1=
	neurone[def_groupe[gpe].premier_ele].s2=robot_pos.x;

	neurone[def_groupe[gpe].premier_ele+1].s=
	neurone[def_groupe[gpe].premier_ele+1].s1=
	neurone[def_groupe[gpe].premier_ele+1].s2=robot_pos.y;

#endif
(void)gpe;
}
