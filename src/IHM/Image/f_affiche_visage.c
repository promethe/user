/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_affiche_visage.c 
\brief 

Author: Boucenna Sofiane
Created: 03/04/2006

Modified:


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: cette fonction affiche simplement si on a un visage ou pas. 

Macro:
-none

Local variables:
-none

Global variables:
-flag_init_seed - see description

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: 

Todo:
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <time.h>
#include <libx.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/*#define DEBUG*/

typedef struct MyData {
  int gpe_precedent;
  int totale;			/*nombre de fois ou l on rentre dans la fonction*/
  int visage;			/*nombre de visage*/
  int non_visage;		/*nombre de non visage*/
}MyData;


void function_affiche_visage(int Gpe)
{
  
    
  int deb;                    /* le premier neurone */
  int i,j;
  int gpe_precedent=-1; 
  MyData *mydata;

  i=j=0;
    
#ifdef DEBUG
  printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif
  
  if(def_groupe[Gpe].data == NULL){
    mydata=(MyData *)malloc(sizeof(MyData));
    if(mydata==NULL){
      printf("pb malloc pour mydata dans la fonction %s",__FUNCTION__);
      exit(0);
    }
    /*recuperation des parametres d'entree*/
    
    while( (j=find_input_link(Gpe,i) ) != -1){
      
      if(strcmp(liaison[j].nom,"entree")==0){
	mydata->gpe_precedent = gpe_precedent = liaison[j].depart;
      }
      
	
      
      i++;
    }
#ifdef DEBUG
    printf("on a fini de recuperer les parametres\n");
#endif
    
    mydata->visage=0;
    mydata->non_visage=0;
    mydata->totale=0;
      
    if(gpe_precedent == -1)
      {
	printf("ATTENTION: erreur dans la fonction %s\n",__FUNCTION__);
	printf("la fonction doit avoir deux groupes en entree \n");
      }
    
    def_groupe[Gpe].data=mydata;	 /*sauvegarde de Mydata*/
  }
  else{
    mydata = def_groupe[Gpe].data;
  }
  
  
  deb=def_groupe[mydata->gpe_precedent].premier_ele;

#ifdef DEBUG
  
  printf("valeur neurone.s=%f\n",neurone[deb].s);
    printf("valeur neurone.s1=%f\n",neurone[deb].s1);
    printf("valeur neurone.s2=%f\n",neurone[deb].s2);

#endif
    mydata->totale++;
    if(neurone[deb].s > neurone[deb+1].s)
      {
	mydata->visage++;

	printf("######################### VISAGE\n");
	printf("nombre de visage=%d / %d\n",mydata->visage,mydata->totale);
      
      }
    else
      {
	mydata->non_visage++;
/*#ifdef DEBUG*/
	printf("######################### NON VISAGE\n");
	printf("nombre de pas visage=%d / %d\n",mydata->non_visage,mydata->totale);
/*#endif*/      
      }

}
