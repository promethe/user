/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libIHM
\defgroup f_select_ROI f_select_ROI
\brief 


\section Modified
- author: A.Karaouzene
- description: specific file creation
- date: 04/06/2012


\section Description


\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

\file 

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */

/*#define DEBUG*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

typedef struct data_zone
{
	int num_image;
} Data_zone;


/*extern type_image_obj image1_obj;*/


void new_function_select_ROI(int gpe)
{
	Data_zone *mydata=NULL;
	int  i=0;
	char *param_link = NULL;
	dprints("enter %s\n",__FUNCTION__);

	if(def_groupe[gpe].nbre!=8)
	{
		EXIT_ON_ERROR("il faut 8 neurones dans %s\n",__FUNCTION__);
	}

	mydata=(Data_zone*)malloc(sizeof(Data_zone));
	if(mydata==NULL)
	{
		printf("pb malloc dans %d\n",gpe);
		exit(0);
	}
	if (prom_getopt(liaison[0].nom, "-IM", param_link) == 2 || prom_getopt(liaison[0].nom, "-i", param_link) == 2)
	{
		mydata->num_image = atoi(param_link);    dprints("image num = %d\n", mydata->num_image);
	}


	for(i=0 ; i<def_groupe[gpe].nbre ; i++)
	{
		neurone[def_groupe[gpe].premier_ele+i].s =-1.;
	}
#ifndef AVEUGLE  
	image1_obj.start_pos1.x=image1_obj.start_pos1.y= image1_obj.start_pos2.x=image1_obj.start_pos2.y=-1;
	image1_obj.end_pos1.x=image1_obj.end_pos1.y= image1_obj.end_pos2.x=image1_obj.end_pos2.y=-1;
#endif  

	def_groupe[gpe].data=mydata;

}
void function_select_ROI(int gpe)
{
#ifndef AVEUGLE
	Data_zone *mydata=NULL;
	int p,res;
	/*int largeur,hauteur;
  TxPoint point;*/

	mydata = (Data_zone *) def_groupe[gpe].data;

	printf("FUNCTION %s attente sem_fini \n",__FUNCTION__);
	res = sem_wait(&image1_obj.sem_fini); /* attend qu'on le reveille */
	if (res < 0)
	{
		dprints("ERROR: pb pour le reveil du gpe %s res semaphore fini = %d\n",def_groupe[gpe].no_name, res);
		/*  exit(EXIT_FAILURE); */
	}

	p=def_groupe[gpe].premier_ele;

	/* 2 coins rectangles 1 */
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.start_pos1.x;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.start_pos1.y;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.end_pos1.x;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.end_pos1.y;

	/* 2 coins rectangle 2 */
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.start_pos2.x;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.start_pos2.y;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.end_pos2.x;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=image1_obj.end_pos2.y;
	/*
   if(image1_obj.start_pos1.x>0) 
   { 
     point.x=image1_obj.start_pos1.x;point.y=image1_obj.start_pos1.y;
    largeur = image1_obj.end_pos1.x - image1_obj.start_pos1.x;
    hauteur = image1_obj.end_pos1.y - image1_obj.start_pos1.y;
    TxDessinerRectangle(&image1, rouge, TxVide, point, largeur, hauteur, 3);
   }
	 */
	printf("positions = [%d,%d,%d,%d]\n",image1_obj.start_pos1.x,image1_obj.start_pos1.y,image1_obj.start_pos2.x,image1_obj.start_pos2.y);
	image1_obj.start_pos1.x=image1_obj.start_pos1.y= image1_obj.start_pos2.x=image1_obj.start_pos2.y=-1;
	image1_obj.end_pos1.x=image1_obj.end_pos1.y= image1_obj.end_pos2.x=image1_obj.end_pos2.y=-1;


#endif
	(void) gpe;
}
void destroy_function_select_ROI(int gpe)
{

	free(def_groupe[gpe].data);
	def_groupe[gpe].data = NULL;
	(void) gpe;
	dprints("exiting %s (file : %s)\n", __FUNCTION__, __FILE__);
}
