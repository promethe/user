/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_select_ROI f_select_ROI
\brief


\section Creation
- author: A.Karaouzene
- description: specific file creation
- date: 09/06/2012


\section Description
 	 Prend 3 donnees en entree : Taille image en x, zone d'exclusion, nouveau centre
 	 retourne 4 neurones qui reprensentent les nouvelles bornes.
 * Recentre le focus (bornes d'exclusion) sur le centre donne.

\section Macro

\section Local varirables

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

\file

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */

/*#define DEBUG*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

typedef struct data_zone
{
	int gpe_exclusion;
	int gpe_centre;
	int taillex;
} Data_zone;


/*extern type_image_obj image1_obj;*/


void new_function_new_center(int gpe)
{
	Data_zone *mydata=NULL;
	int  i=0, l=-1;
	char    param_link[256];
	dprints("enter %s\n",__FUNCTION__);

	if(def_groupe[gpe].nbre!=4)
	{
		EXIT_ON_ERROR("il faut 4 neurones dans %s\n",__FUNCTION__);
	}

	mydata=(Data_zone*)malloc(sizeof(Data_zone));
	if(mydata==NULL)
		EXIT_ON_ERROR("pb malloc dans %d\n",gpe);


	mydata->gpe_centre  = -1;
	mydata->gpe_exclusion  = -1;
	mydata->taillex  = 0;

	if (l == -1)
	{

		l = find_input_link(gpe, i);

		while (l != -1)
		{
			if (strcmp(liaison[l].nom, "-c") == 0)
				mydata->gpe_centre = liaison[l].depart;
			if (strcmp(liaison[l].nom, "-re") == 0)
				mydata->gpe_exclusion = liaison[l].depart;
			if (prom_getopt(liaison[l].nom, "-T", param_link) == 2 || prom_getopt(liaison[l].nom, "-t", param_link) == 2)
				mydata->taillex = atoi(param_link);
			i++;
			l = find_input_link(gpe, i);

		}
	}
	if(mydata->gpe_centre == -1 || mydata->gpe_exclusion == -1 || mydata->taillex == 0)
		EXIT_ON_ERROR("erreur dans  %s c %i re %i taillex %i",
				__FUNCTION__,mydata->gpe_centre,mydata->gpe_exclusion,mydata->taillex);


	for(i=0 ; i<def_groupe[gpe].nbre ; i++)
	{
		neurone[def_groupe[gpe].premier_ele+i].s =-1.;
	}


	def_groupe[gpe].data=mydata;

}
void function_new_center(int gpe)
{

	Data_zone *mydata=NULL;
	int p;
	int oldCenter=0.;
	int newCenter = 0, newmarge_gauche =0,newmarge_droite=0;
	int marge_gauche = 0,marge_droite =0, sx = 0 ;


	mydata = (Data_zone *) def_groupe[gpe].data;
	marge_gauche = neurone[def_groupe[mydata->gpe_exclusion].premier_ele].s1;
	marge_droite = neurone[def_groupe[mydata->gpe_exclusion].premier_ele + 1].s1;
	newCenter = neurone[def_groupe[mydata->gpe_centre].premier_ele].s1;
	sx = mydata->taillex;

	oldCenter =  0.5*( sx - marge_droite + marge_gauche);
	newmarge_gauche = marge_gauche - oldCenter + newCenter;
	if(newmarge_gauche < 0 || newmarge_gauche > sx)
		newmarge_gauche=0;
	newmarge_droite = marge_droite + oldCenter - newCenter;
	if(newmarge_droite<0 || newmarge_droite > sx)
		newmarge_droite =0;

	dprints("taille X = %i, marge_gauche  :  %i marge_droite  : %i  oldcenter : %i\n",
			sx,marge_gauche,marge_droite,oldCenter);
	p=def_groupe[gpe].premier_ele;

	neurone[p].s= neurone[p].s1= neurone[p].s2= newmarge_gauche;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2= newmarge_droite;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=neurone[def_groupe[mydata->gpe_exclusion].premier_ele + 2].s1;
	p++;
	neurone[p].s= neurone[p].s1= neurone[p].s2=neurone[def_groupe[mydata->gpe_exclusion].premier_ele + 3].s1;

	dprints("oldcenter = %i Newcenter = %i and new exclusion = [%f,%f,%f,%f]\n",
			oldCenter,newCenter,neurone[p-3].s1,neurone[p-2].s1,neurone[p-1].s1,neurone[p].s1 );

	(void) gpe;
}
void destroy_function_new_center(int gpe)
{

	free(def_groupe[gpe].data);
	def_groupe[gpe].data = NULL;

	(void) gpe;
	dprints("exiting %s (file : %s)\n", __FUNCTION__, __FILE__);
}
