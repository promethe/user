/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \ingroup libIHM
 \defgroup f_display_group_perso f_display_group_perso
 \brief


 \section Author
 -Name: Ali Karaouzene
 -Created: 22/05/2013

 \section Theoritical description
 - \f$  LaTeX equation: none \f$  

 \section Description
 Fonction qui dessine un rectangle dans l'image. La fonction recoit la position (x,y) du rectangle du groupe precedent.
 La taille du rectangle, sa couleur sont definies sur le lien entrant.

 Option :
 -I1,I2 : Dessiner dans l'image 1 ou 2.
 -F (RECT,CERCLE,FLECHE) : Choix de la forme a dessiner.
 -L : Largeur du rectangle ou Rayon du cercle.
 -H : Hauteur du rectangle.
 -T : Epaisseur du trait.

 -color :
 noir 0
 blanc 16
 gris 8
 rouge 17
 vert 18
 bleu 19
 ciel 20
 jaune 21
 violet 22
 marine 23
 pelouse 24
 bordeau 25
 kaki 26

 \section Macro
 -none

 \section Local variables
 -none

 \section Global variables
 -none

 \section Internal Tools
 -none

 \section External Tools
 -none

 \section Links
 - type: none
 - description: none
 - input expected group: none/xxx
 - where are the data?: none/xxx

 \section Comments

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#define CERCLE 1
#define RECTANGLE 2
#define FLECHE 3
#define CHAINE 4
#define CROIX 5
#define ROBOT 6
typedef struct data_forme {
  int num_image;
  int gpe_enable;
  int gpe_input;
  int gpe_chaine;
  int forme;
  int I;
  int PX;
  int PY;
  int onLinkX;
  int onLinkY;
  unsigned int largeur;
  unsigned int hauteur;
  unsigned int epaisseur;
  unsigned int color;
  double accu[2];
} Data_forme;

void new_dessiner_forme(int gpe)
{
#ifndef AVEUGLE
  Data_forme *mydata = NULL;
  int i = 0, link;
  char param_link[32];
  int gpe_input = -1, gpe_enable = -1, gpe_chaine = -1;

  dprints("enter %s\n", __FUNCTION__);

  if (def_groupe[gpe].data == NULL)
  {

    mydata = (Data_forme*) malloc(sizeof(Data_forme));
    if (mydata == NULL) EXIT_ON_ERROR("pb malloc dans %s\n", def_groupe[gpe].no_name);
    mydata->I = 1;
    mydata->largeur = 1;
    mydata->hauteur = 1;
    mydata->epaisseur = 1;
    mydata->forme = RECTANGLE;
    mydata->accu[0] = 0;
    mydata->accu[1] = 0;
    mydata->onLinkX = -1;
    mydata->onLinkY = -1;
    link = find_input_link(gpe, i);
    while (link != -1)
    {
      if (strstr(liaison[link].nom, "enable") != NULL)
      {
        dprints("enable group found\n", param);
        gpe_enable = liaison[link].depart;
      }
      else if (strstr(liaison[link].nom, "sync") != NULL)
      {
      }
      else
      {
        if (prom_getopt(liaison[link].nom, "I1", param_link) == 1 || prom_getopt(liaison[link].nom, "i1", param_link) == 1)
        {
          mydata->I = 1;
          dprints("forme affichee dans l'image = %d\n", I);
          gpe_input = liaison[link].depart;
        }
        else if (prom_getopt(liaison[link].nom, "I2", param_link) == 1 || prom_getopt(liaison[link].nom, "i2", param_link) == 1)
        {
          mydata->I = 2;
          dprints("forme affichee dans l'image = %d\n", I);
          gpe_input = liaison[link].depart;
        }

        if (prom_getopt(liaison[link].nom, "F", param_link) >= 2 || prom_getopt(liaison[link].nom, "f", param_link) >= 2)
        {
          if ((strstr(param_link, "CERCLE")) != NULL) mydata->forme = CERCLE;
          else if ((strstr(param_link, "RECT")) != NULL) mydata->forme = RECTANGLE;
          else if ((strstr(param_link, "FLECHE")) != NULL) mydata->forme = FLECHE;
          else if ((strstr(param_link, "CHAINE")) != NULL) mydata->forme = CHAINE;
          else if ((strstr(param_link, "CROIX")) != NULL) mydata->forme = CROIX;
          else if ((strstr(param_link, "ROBOT")) != NULL) mydata->forme = ROBOT;
          else mydata->forme = RECTANGLE;
          printf("forme affichee = %d\n", mydata->forme);
        }
        if (prom_getopt(liaison[link].nom, "L", param_link) == 2 || prom_getopt(liaison[link].nom, "l", param_link) == 2)
          mydata->largeur = atoi(param_link);
        if (prom_getopt(liaison[link].nom, "H", param_link) == 2 || prom_getopt(liaison[link].nom, "h", param_link) == 2)
          mydata->hauteur = atoi(param_link);
        if (prom_getopt(liaison[link].nom, "T", param_link) == 2 || prom_getopt(liaison[link].nom, "t", param_link) == 2)
          mydata->epaisseur = atoi(param_link);
        if (prom_getopt(liaison[link].nom, "COLOR", param_link) == 2 || prom_getopt(liaison[link].nom, "color", param_link) == 2)
          mydata->color = atoi(param_link);
        if (prom_getopt(liaison[link].nom, "PX", param_link) == 2 || prom_getopt(liaison[link].nom, "px", param_link) == 2)
        {
          mydata->PX = atoi(param_link);
          mydata->onLinkX = 1;
        }
        if (prom_getopt(liaison[link].nom, "PY", param_link) == 2 || prom_getopt(liaison[link].nom, "py", param_link) == 2)
        {
          mydata->PY = atoi(param_link);
          mydata->onLinkY = 1;
        }
      }
      i++;
      link = find_input_link(gpe, i);

    }
    if (gpe_input == -1 && gpe_chaine == -1)
    EXIT_ON_ERROR("%s gpe %s necessite un groupe en entree pour dessiner", __FUNCTION__, def_groupe[gpe].no_name);
    mydata->gpe_input = gpe_input;
    mydata->gpe_enable = gpe_enable;
    def_groupe[gpe].data = mydata;
  }
#else
  (void)gpe;
#endif
}

void function_dessiner_forme(int gpe)
{
#ifndef AVEUGLE
  TxDonneesFenetre *fenetre = NULL;
  int gpe_enable = -1;
  int forme = -1;
  int gpe_input = -1;
  /*int gpe_chaine = -1;*/
  int I = -1;
  int color = 17;

  double angle = 0., positionX = 1., positionY = 1., floatpoint[2] ={ 0., 0. };
  /*unsigned char *im_save;*/
  Data_forme * mydata = NULL;
  TxPoint point ={ 1, 1 }, source ={ 1, 1 }, destination = { 1, 1 };
  char ligne[100];

  unsigned int largeur, hauteur, epaisseur;
  mydata = (Data_forme *) def_groupe[gpe].data;
  if (mydata == NULL) EXIT_ON_ERROR("ERROR in (%s): Cannot retreive data\n", def_groupe[gpe].no_name);

  I = mydata->I;
  gpe_enable = mydata->gpe_enable;
  forme = mydata->forme;
  gpe_input = mydata->gpe_input;
  largeur = mydata->largeur;
  hauteur = mydata->hauteur;
  epaisseur = mydata->epaisseur;
  color = mydata->color;

  if (gpe_enable != -1 && neurone[def_groupe[gpe_enable].premier_ele].s1 < 0.5) return;
  if (I == 2) fenetre = &image2;
  else fenetre = &image1;
  /*recuperation du point pour le dessin*/
  if (gpe_input != -1)
  {
    if(mydata->onLinkX != 1)
      point.x = (int) neurone[def_groupe[gpe_input].premier_ele].s1;
    else
      point.x = mydata->PX;
    if(mydata->onLinkY != 1)
      point.y = (int) neurone[def_groupe[gpe_input].premier_ele + 1].s1;
    else
      point.y = mydata->PY;
    floatpoint[0] = neurone[def_groupe[gpe_input].premier_ele].s1;
    floatpoint[1] = neurone[def_groupe[gpe_input].premier_ele + 1].s1;
  }
  switch (forme)
  {
  case CERCLE:
    if (point.x >= 0 && point.y >= 0) TxDessinerCercle(fenetre, color, 0/*cercle vide par defaut*/, point, largeur, epaisseur);
    break;
  case RECTANGLE:
    if (point.x >= (int) (largeur / 2) && point.y >= (int) (hauteur / 2))
    {
      source.x = point.x - (largeur / 2);
      source.y = point.y - (hauteur / 2);
      TxDessinerRectangle(fenetre, color, TxPlein, source, largeur, hauteur, epaisseur);
    }
    break;
  case FLECHE: /*Implementation a revoir*/
    destination.x = point.x + largeur;
    destination.y = point.y + hauteur;
    TxDessinerFleche(fenetre, color, point, destination, epaisseur);

    break;
  case CROIX:
    /*point Bas gauche*/
    source.x = point.x - (largeur / 2);
    source.y = point.y - (hauteur / 2);
    /*point Haut Droit*/
    destination.x = point.x + (largeur / 2);
    destination.y = point.y + (hauteur / 2);
    TxDessinerSegment(fenetre, color, source, destination, epaisseur);
    /*point haut gauche*/
    source.x = point.x - (largeur / 2);
    source.y = point.y + (hauteur / 2);
    /*point Bas Droit*/
    destination.x = point.x + (largeur / 2);
    destination.y = point.y - (hauteur / 2);
    TxDessinerSegment(fenetre, color, source, destination, epaisseur);
    break;

  case ROBOT:/*En entree avoir la distance parcourue depuis le debut et l'angle courant*/
    angle = floatpoint[1] * 2. * M_PI;
    positionX = cos(angle) * floatpoint[0];
    positionY = sin(angle) * floatpoint[0];
    mydata->accu[0] = mydata->accu[0] + positionX;
    mydata->accu[1] = mydata->accu[1] + positionY;
    source.x = (int) mydata->accu[0] + 500;
    source.y = (int) mydata->accu[1] + 500;
    angle = neurone[def_groupe[gpe_input].premier_ele + 2].s1 * 2. * M_PI;

    destination.x = (cos(angle) * largeur) + source.x;
    destination.y = (sin(angle) * hauteur) + source.y;

    //printf("(x:%f y:%f) \n",mydata->accu[0],mydata->accu[1]);
    dprints("dist : %f angle %f (x:%f y:%f) int+500(%d,%d)\n", floatpoint[0], floatpoint[1], positionX, positionY, source.x, source.y);
    fflush(stdout);

    if (source.x >= 0 && source.y >= 0)
    //TxDessinerCercle(fenetre,color, 1 ,source, largeur, epaisseur);
    TxDessinerFleche(fenetre, color, source, destination, epaisseur);
    break;
  case CHAINE:
    if(mydata->onLinkX != 1 && mydata->onLinkY != 1)
      sprintf(ligne,"%.3f",neurone[def_groupe[gpe_input].premier_ele+2].s1 );
    else
      sprintf(ligne,"%.3f",neurone[def_groupe[gpe_input].premier_ele].s1 );
    TxEcrireChaineSized(fenetre, color, point, ligne, hauteur,NULL);

    break;

  }

  TxFlush(fenetre);
  //TxCopyWindowImage(fenetre, im_save, 0, 0, 320, 240);

#else
  (void)gpe;
#endif
}
