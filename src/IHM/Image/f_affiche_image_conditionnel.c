/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_affiche_image_conditionnel f_affiche_image_conditionnel
   \ingroup libIHM

   \file  f_affiche_image_conditionnel.c
   \ingroup f_affiche_image_conditionnel

   \brief Met une image depuis un fichier liste dans le champ ext

   \section Description

   Met une image depuis un fichier liste dans le champ ext
   en fonction d'une commande. Le neurone gagnant de 'command' determine
   quelle image de la liste il faut mettre dans le champs ext.  Pour 1
   neurone, il faut 2 images : la premiere pour le neurone inactif.  Pour
   2 neurones, il faut 3 images et ainsi ce suite.


   External Tools:
   - Kernel_Function/prom_getopt()
   - Kernel_Function/find_input_link()


   Author: Cyril Hasson (Julien Hirel non?)
   Created:
   Modified:

************************************************************/

#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

/* #define NO_SOCKET_DEBUG 1 */

typedef struct mydata_affiche_image_conditionnel
{
   int gpe_command;
   prom_images_struct *pt;
} mydata_affiche_image_conditionnel;


void new_affiche_image_conditionnel(int gpe)
{
   int gpe_command = -1;
   int i, l, n,ret;
   int width, height, nbands;
   int nbre_images;
   int first_time;

   prom_images_struct *pt=NULL;
   unsigned char *im_table=NULL;
   char *listfile_name = NULL;
   FILE *listfile = NULL;
   char image_name[256];
   mydata_affiche_image_conditionnel *my_data = NULL;

   (void) ret; // (unused)

   /** recherche les liens en entree */
   l = 0;
   i = find_input_link(gpe, l);
   while (i != -1)
   {
      if (strcmp(liaison[i].nom, "command") == 0)  gpe_command = liaison[i].depart;
      else  listfile_name = liaison[i].nom;

      l++;
      i = find_input_link(gpe, l);
   }
   /** verifications*/
   if (gpe_command == -1)     EXIT_ON_ERROR("La fonction a besoin d'un lien command en entree (conditionnement de l'affichage de l'image)");
   if ( listfile_name == NULL) EXIT_ON_ERROR("La fonction a besoin d'un lien en entree portant le nom de l'image ou du fichier d'images a afficher\n");

   listfile = fopen(listfile_name,"r");
   if (listfile == NULL)     EXIT_ON_ERROR("impossible d'ouvrir '%s' \n", listfile_name);

   ret=fscanf(listfile, "%i\n", &nbre_images);

   pt = (prom_images_struct *) malloc(sizeof(prom_images_struct));
   def_groupe[gpe].ext = (void *) pt;
   pt->images_table[0] = NULL;
   pt->image_number = nbre_images;/**3;*/

   n = 1;

   first_time=1;
   while (!feof(listfile) && n < (nbre_images+1))
   {
      ret=fscanf(listfile, "%s\n", image_name);
      dprints("ouverture de '%s'\n", image_name);

      load_image_from_disk("png", image_name, &im_table, &width, &height, &nbands);
      /*printf("------------------>nbands=%d\n",nbands);
        printf("%d  ,  %d , %d \n",  width,  height,   nbands);*/
      /* manque verif taille */
      if (first_time==1)
      {
         pt->sx = width;
         pt->sy = height;
         pt->nb_band = nbands;
      }
      else if (pt->sx != width || pt->sy != height ||  pt->nb_band != nbands)
      {
         kprints("ERREUR changement de taille dans new_affiche_image_conditionnel \n");
         kprints("%d %d , %d %d ,%d %d \n", pt->sx, width, pt->sy, height,  pt->nb_band, nbands);
      }

      pt->images_table[n]= im_table; /* on garde l'image allouee dans load_image_from_disk */

      n++ /*= n+nbands*/;
   }


   my_data = (mydata_affiche_image_conditionnel*) malloc(sizeof(mydata_affiche_image_conditionnel));
   if (my_data == NULL) EXIT_ON_ERROR("error malloc: (%s )\n", def_groupe[gpe].no_name);

   my_data->gpe_command = gpe_command;
   my_data->pt = pt;
   def_groupe[gpe].data = my_data;

}

void destroy_affiche_image_conditionnel(int gpe)
{
   if (def_groupe[gpe].data != NULL) free((mydata_affiche_image_conditionnel *) def_groupe[gpe].data);
   def_groupe[gpe].data = NULL;
}

void function_affiche_image_conditionnel(int gpe)
{
   int gpe_command = -1;
   int i, max_ind = -1;
   int premier_ele;

   float max = 0.000001;

   mydata_affiche_image_conditionnel *my_data = NULL;

   if (def_groupe[gpe].data == NULL) EXIT_ON_ERROR("ERREUR de la fonction : la structure est vide\n");

   my_data = (mydata_affiche_image_conditionnel *) def_groupe[gpe].data;
   gpe_command = my_data->gpe_command;
   premier_ele=def_groupe[gpe_command].premier_ele;
   for (i = 0; i < def_groupe[gpe_command].nbre; i++)
   {
      if (neurone[premier_ele + i].s1 > max)
      {
         max = neurone[premier_ele + i].s1;
         max_ind = i;
      }
   }

   /*printf("\n\nmax_ind = %i %f\n\n",max_ind,neurone[def_groupe[gpe].premier_ele].s);*/

   if (def_groupe[gpe].ext == NULL)  def_groupe[gpe].ext = (void *) my_data->pt;

   if (max_ind >= 0)
   {
      my_data->pt->images_table[0] = my_data->pt->images_table[max_ind+2];
   }
   else
   {
      my_data->pt->images_table[0] = my_data->pt->images_table[1];
   }

   neurone[def_groupe[gpe].premier_ele].s=max_ind;
}
