/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \ingroup libIHM
 \defgroup f_affiche_image_from_extension f_affiche_image_from_extension

 \brief Display the image stored in the .ext of the group

 \section Modified
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 - author: JC Baccon
 - description: add some comments
 - date: 22/07/2004

 \details
 Cette fonction affiche l'image stockée dans la structure .ext du groupe
 précedent dans l'une des fenêtre 1 ou 2.

 \section Options
 -   -Ixx    :   specifie la fenêtre à utiliser pour l'affichage (xx=1 ou 2)
 la fonction ne fonctionnera pas sans cette option
 -   -PXxx   :   specifie un offset horizontal pour afficher l'image. L'offset
 est calculé par rapport au coin gauche supérieur de la fenêtre
 et donne les coordonnées du pixel droit supérieur de l'image
 (pixel (0,0))
 -   -PYxx   :   specifie un offset vertical pour afficher l'image. L'offset
 est calculé par rapport au coin gauche supérieur de la fenêtre
 et donne les coordonnées du pixel droit supérieur de l'image
 (pixel (0,0))
 -   -NALL   :   affiche les images une à une dans le cas où il y a plus d'une
 image stockée dans la structure .ext
 -   -INV    :   inverse les niveaux de gris
 (non effectif pour des images couleur ou avec l'option -NALL)

 -   -enable :   option à mettre sur le lien venant d'un groupe avec un seul
 neurone pour activer (>=0.5) ou désactiver (<0.5) l'affichage

 -   -sync   :   option pour synchroniser cette boîte avec l'éxécution de la
 boîte précédente

 -   -EXTxxx :   Spécifie l'image en entrée à afficher (si l'option -NALL est
 désactivée).

 \section Todo
 stop using <libx.h> (really ??, maybe <public.h> which has been already done)
 */

/*#define DEBUG 1 */
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>

typedef struct type_my_data
{
   int fen_posy, fen_posx, NbFenetre, inverse_video, nall, ext;
   int InputGpe;
   int EnableGpe;
   void *fenetre;
} type_my_data;

void function_affiche_image_from_extension(int Gpe)
{
#ifndef AVEUGLE
   int InputGpe = -1, EnableGpe = -1, i, l, NbFenetre, fen_posx, fen_posy, nall, ext = 0;
   int inverse_video = 1;
   void *fenetre;
   char param[255];
   type_my_data *data = NULL;
   prom_images_struct *the_image;

   unsigned char *inv_image = NULL;

   dprints("function_affiche_image_from_extension\n");

   nall = 0;
   fenetre = &image1;
   fen_posx = 0;
   fen_posy = 0;

   NbFenetre = 1;

   i = 0;
   if (def_groupe[Gpe].data == NULL) /*Recuperation des options sur ls lien */
   {
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         dprints("liaison = %s \n", liaison[l].nom);
         if (strstr(liaison[l].nom, "enable") != NULL)
         {
            dprints("enable group found\n", param);
            EnableGpe = liaison[l].depart;
         }
         else if (strstr(liaison[l].nom, "sync") != NULL)
         {
         }
         else
         {
            InputGpe = liaison[l].depart;
            if (prom_getopt(liaison[l].nom, "NALL", param) == 1) /* -NALL Affiche une a une les images */
            {
               dprints("mode NALL\n");
               nall = 1;
            }
            if (prom_getopt(liaison[l].nom, "INV", param) == 1) /* -INV Inverse video */
            {
               dprints("mode INV\n");
               inverse_video = 0;
            }
            if (prom_getopt(liaison[l].nom, "I", param) == 2) /* -I1 -I2 affiche dans la fenetre1 (resp fenetre2) */
            {
               dprints("affiche dans la fenetre image %s\n", param);
               NbFenetre = atoi(param);
               if (NbFenetre == 2) fenetre = &image2;
               else fenetre = &image1;
            }

            /*Position beginning of image in the window (default 0,0) */
            if (prom_getopt(liaison[l].nom, "PX", param) == 2)
            {
               dprints("PX= %s\n", param);
               fen_posx = atoi(param);
            }
            if (prom_getopt(liaison[l].nom, "PY", param) == 2)
            {
               dprints("PY= %s\n", param);
               fen_posy = atoi(param);
            }
            if (prom_getopt(liaison[l].nom, "EXT", param) == 2)
            {
               dprints("EXT= %s\n", param);
               ext = atoi(param);
            }
         }
         i++;
         l = find_input_link(Gpe, i);
      }

      if (InputGpe == -1)
      {
         EXIT_ON_ERROR("no input groupe");
      }
      data = (type_my_data*) ALLOCATION(type_my_data);
      data->fen_posx = fen_posx;
      data->fen_posy = fen_posy;
      data->ext = ext;
      data->NbFenetre = NbFenetre;
      data->inverse_video = inverse_video;
      data->fenetre = fenetre;
      data->nall = nall;
      data->InputGpe = InputGpe;
      data->EnableGpe = EnableGpe;
      def_groupe[Gpe].data = (void*) data;
   } /* fin de l'init */
   else
   {
      data = (type_my_data*) def_groupe[Gpe].data;
      fen_posx = data->fen_posx;
      fen_posy = data->fen_posy;
      ext = data->ext;
      NbFenetre = data->NbFenetre;
      inverse_video = data->inverse_video;
      InputGpe = data->InputGpe;
      EnableGpe = data->EnableGpe;
      fenetre = data->fenetre;
      nall = data->nall;
   }

   if (EnableGpe != -1 && neurone[def_groupe[EnableGpe].premier_ele].s < 0.5) return;

   dprints("%s: nall= %d, NbFenetre= %d \n", __FUNCTION__, nall, NbFenetre);
   the_image = (prom_images_struct *) def_groupe[InputGpe].ext;
   /*printf("NbFenetre=%d , fen_posx=%d, fen_posy=%d \n",NbFenetre,fen_posx,fen_posy); */
   if (def_groupe[InputGpe].ext == NULL)
   {
      cprints("Gpe amonte a l'ext nulle dans f_affiche_image_from_extension(%s)\n", def_groupe[Gpe].no_name);
      return;
   }

   if (ext >= (int) the_image->image_number) ext = 0;

   if (nall == 1)
   {
      if (the_image->image_number <= 3)
      {
         for (i = 0;  i < the_image->image_number; i++)
         {
            /* rtp affiche_ng is said to be deprecated .... */
            if (the_image->nb_band == 3) TxAfficheImageCouleur(fenetre, the_image->images_table[i], the_image->sx, the_image->sy, fen_posx + (5 + the_image->sx) * i, fen_posy);
            else TxAfficheImageNB(fenetre, the_image->images_table[i], the_image->sx, the_image->sy, fen_posx + (5 + the_image->sx) * i, fen_posy);
         }
      }
      else for (i = 0;  i < the_image->image_number; i++)
         {
            /* rtp affiche_ng is said to be deprecated .... */
            if (the_image->nb_band == 3) TxAfficheImageCouleur(fenetre, the_image->images_table[i], the_image->sx, the_image->sy, fen_posx, fen_posy);
            else TxAfficheImageNB(fenetre, the_image->images_table[i], the_image->sx, the_image->sy, fen_posx, fen_posy);
            fflush(stdin);
            cprints("Image %d of %d\t\tPress a key (TO BE CHANGED - getchar() see code!!!)\n", i, (*((prom_images_struct *) def_groupe[InputGpe].ext)).image_number);
            getchar();
         }
   }
   else
   {
      dprints(" sx=%d sy=%d \n", the_image->sx, the_image->sy);

      /*  printf("affiche image from ext : nall=%d i%d \n",nall,NbFenetre); */
      /* rtp affiche_ng is said to be deprecated .... */
      if (the_image->nb_band == 3)
      {
         TxAfficheImageCouleur(fenetre, the_image->images_table[ext], the_image->sx, the_image->sy, fen_posx, fen_posy);
      }
      else if (inverse_video == 1) TxAfficheImageNB(fenetre, the_image->images_table[ext], the_image->sx, the_image->sy, fen_posx, fen_posy);
      else
      {
         inv_image = MANY_ALLOCATIONS(the_image->sx* the_image->sy, unsigned char);
         for (i = 0;  i < the_image->sx * the_image->sy; i++)
         {
            inv_image[i] = 255 - the_image->images_table[ext][i];
         }
         TxAfficheImageNB(fenetre, inv_image, the_image->sx, the_image->sy, fen_posx, fen_posy);
         free(inv_image);
      }
   }

   dprints("fin function_affiche_image_from_extension\n\n");
#else
   (void) Gpe;
#endif
}
