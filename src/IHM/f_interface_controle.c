/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_interface_controle f_interface_controle
\brief 
 

\section Author
Name: xxxxxxxx
Created: XX/XX/XXXX

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number

\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#ifndef AVEUGLE
#include <graphic_Tx.h>

gboolean motiv_pressed(GtkWidget * widget, gpointer data);
gboolean decouverte_pressed(GtkWidget * widget, gpointer data);

gboolean cb_destroy_fenetre(GtkWidget * widget, gpointer data);

gboolean fenetre_expose_event(TxDonneesFenetre * fenetre, GtkWidget * widget,
                              GdkEventExpose * event, gpointer data);

gboolean fenetre_configure_event(TxDonneesFenetre * fenetre,
                                 GtkWidget * widget,
                                 GdkEventConfigure * event, gpointer data);


void create_interface_controle(TxDonneesFenetre * fenetre, int gpe);
gboolean interface_controle_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data);
gboolean interface_controle_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data);


TxDonneesFenetre interface_controle;
#endif
typedef struct MyData_f_interface_controle
{
      int *decouverte;
      int *motivation;
      
} MyData_f_interface_controle;


void function_interface_controle(int gpe)
{
#ifndef AVEUGLE
   int i;
   MyData_f_interface_controle * mydata;
   if (def_groupe[gpe].data == NULL)
   {
      	mydata = (MyData_f_interface_controle*)malloc(sizeof(MyData_f_interface_controle));
	if (mydata == NULL)
	{
	   fprintf(stderr, "function_interface_controle : error, memory not allocated\n");
	   exit(1);
	}

	mydata->decouverte = (int *)malloc(def_groupe[gpe].nbre * sizeof(int));
	mydata->motivation = (int *)malloc(def_groupe[gpe].nbre * sizeof(int));
	if (mydata->decouverte == NULL || mydata->motivation == NULL)
	{
	   fprintf(stderr, "function_interface_controle : error, memory not allocated\n");
	   exit(1);
	}

	for (i = 0; i < def_groupe[gpe].nbre; i++)
	{
	   mydata->decouverte[i] = 0;
	   mydata->motivation[i] = 0;
	}

        def_groupe[gpe].data = mydata;

	if(strcmp(interface_controle.name,"Mon interface") != 0)
	{
		sprintf(interface_controle.name, "Mon interface");
		create_interface_controle(&interface_controle, gpe);
		gtk_widget_show_all(interface_controle.window);
	}
    }
    else
    {
        mydata = (MyData_f_interface_controle *) def_groupe[gpe].data;
    }


   for (i = 0; i < def_groupe[gpe].nbre; i++)
   {
      neurone[def_groupe[gpe].premier_ele + i].s2 = mydata->decouverte[i];
      neurone[def_groupe[gpe].premier_ele + i].s  = neurone[def_groupe[gpe].premier_ele + i].s1 = mydata->motivation[i];
   }
#endif
(void) gpe;
}

#ifndef AVEUGLE
void create_interface_controle(TxDonneesFenetre * fenetre, int gpe)
{
    GtkWidget *box2, *box3;
    GtkWidget *button;
    GtkWidget *label;
    GtkWidget *separator;
    char button_name[255];
    int i;

    /* Standard window-creating stuff */
    fenetre->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",
                     G_CALLBACK(cb_destroy_fenetre), fenetre);
    gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);

    fenetre->box1 = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(fenetre->window), fenetre->box1);
    gtk_widget_show(fenetre->box1);

    box2 = gtk_vbox_new(FALSE, 10);
    gtk_container_set_border_width(GTK_CONTAINER(box2), 10);
    gtk_box_pack_start(GTK_BOX(fenetre->box1), box2, FALSE, TRUE, 0);
    gtk_widget_show(box2);

     label = gtk_label_new("Decouverte des buts");
     gtk_box_pack_start(GTK_BOX(box2), label, FALSE, FALSE, 0);
     gtk_widget_show(label);

    box3 = gtk_hbox_new(FALSE, 10);
    gtk_container_set_border_width(GTK_CONTAINER(box3), 10);
    gtk_box_pack_start(GTK_BOX(box2), box3, FALSE, TRUE, 0);
    gtk_widget_show(box3);

/*..... sous boite horizontale ..................*/

   for (i = 0; i < def_groupe[gpe].nbre; i++)
   {
      sprintf(button_name, "Decouverte %d", i);
      button = gtk_check_button_new_with_label(button_name);
      g_signal_connect(GTK_OBJECT(button), "clicked",
		       G_CALLBACK(decouverte_pressed), def_groupe[gpe].data);
      gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
      GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
      gtk_widget_grab_default(button);
      gtk_widget_show(button);
   }
    separator = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(box2), separator, FALSE, TRUE, 0);
    gtk_widget_show(separator);

     label = gtk_label_new("Motivation");
     gtk_box_pack_start(GTK_BOX(box2), label, FALSE, FALSE, 0);
     gtk_widget_show(label);

/*
    separator = gtk_hseparator_new();*/
    gtk_box_pack_start(GTK_BOX(box2), separator, FALSE, TRUE, 0);
    gtk_widget_show(separator);

   for (i = 0; i < def_groupe[gpe].nbre; i++)
   {
      sprintf(button_name, "Envie %d", i);
      button = gtk_check_button_new_with_label(button_name);
      g_signal_connect(GTK_OBJECT(button), "clicked",
		       G_CALLBACK(motiv_pressed), def_groupe[gpe].data);
      gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 0);
      GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
      gtk_widget_grab_default(button);
      gtk_widget_show(button);
   }
}

gboolean decouverte_pressed(GtkWidget * widget, gpointer data)
{
   int i = 0;
   MyData_f_interface_controle *mydata = (MyData_f_interface_controle *) data;
   const char *button_name = gtk_button_get_label(GTK_BUTTON(widget));

   if (button_name == NULL)
   {
      fprintf(stderr, "Error: cannot get the label of the button\n");
      return FALSE;
   }
   else 
   {
      sscanf(button_name, "Decouverte %d", &i);
      if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) 
      {
	 mydata->decouverte[i] = 1;
      }
      else
      {
	 mydata->decouverte[i] = 0;
      }
   }

   return TRUE;
}


gboolean motiv_pressed(GtkWidget * widget, gpointer data)
{
   int i = 0;
   MyData_f_interface_controle *mydata = (MyData_f_interface_controle *) data;
   const char *button_name = gtk_button_get_label(GTK_BUTTON(widget));

   if (button_name == NULL)
   {
      fprintf(stderr, "Error: cannot get the label of the button\n");
      return FALSE;
   }
   else 
   {
      sscanf(button_name, "Envie %d", &i);
      if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) 
      {
	 mydata->motivation[i] = 1;
      }
      else
      {
	 mydata->motivation[i] = 0;
      }
   }
   return TRUE;
}


gboolean interface_controle_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data)
{
 	
    return fenetre_configure_event(&interface_controle, widget, event, data);
}
gboolean interface_controle_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data)
{
    return fenetre_expose_event(&interface_controle, widget, event, data);
}

#endif
