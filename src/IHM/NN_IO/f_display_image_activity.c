/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_display_image_activity f_display_image_activity

\section Modified
- author: J.Bonhoure
- description: ajout de l'option save
- date: 10/08/2011

\details
	Display or save activity of the neurons. Cette fonction affiche l'activite des neurone du groupe d'entree aucours du temps
	et/ou ecrit les activites des neurones du groupe d'entree dans un fichier. 
	Chaque colonne de ce fichier correspond a un neurone et chaque ligne correspond a un releve.  
	Des colonnes complementaires peuvent etre demande pour ecrire de nouvelles colonnes indiquant 
	le gpe de la ligne, le gpe de ligne multiplie par un facteur, le temps ecoule depuis 
	le debut de la simulation.  

\section Options
\subsection Sorties
-	-s0   :  (s0, s1 ou s2)
	
\subsection Affichage
-	-x0   : position du graphe dans l'image
-	-y0   : postion du graphe dans l'image
-	-i1   : gpe de l'interface (1 ou 2)
-	-m20  : memoire temporelle de la boite
-	-d1   : actif, permet la visualisation
-	-n    : nom du graphique
	 
\subsection	Sauvegarde
-	-r              : "rewrite" -> ecrasement du fichier avant de commencer l'ecriture
-	-f0             : sauvegarde des activites dans un fichier inactive
-	-f ou f_nom_fichier        : sauvegarde des activites dans un fichier active
-       -g              : utilise le format \%g au lieu de \%f pour l'ecriture dans le fichier.
-	-c ou -c1       : ecrit la valeur du compteur en premiere colonne (ou deuxieme ou troisieme
			si -tsim ou tabs sont avant)
-	-c0             : n'ecrit pas de colonne compteur
-	-c22.5          : ecrit en premiere colonne le compteur mult par 22.5
-	-tsim ou -tsim1 : ecrit le temps ecoule depuis le debut de la simulation en premiere colonne
	(ou deuxieme si -tabs est avant) en fait deux colonnes, une pour les secondes et la suivante pour les 
			millisecondes
-	-tsim0          : n'ecrit pas de colonne temps
-	-tabs ou tabs1  : ecrit le temps absolu en premiere colonne (-----Attention !----- : necessaire pour pouvoir comparer deux promethes qui interagissent par l'environnement : ex: Osc_arm_cam_couples)
-	-tabs0          : n'ecrit pas de colonne temps absolu
	 save			: ne sauvegarde que quand l'activite du premier neurone de la boite precedente est actif (a utiliser avec un checkbox)

\file
\ingroup f_display_image_activity

*/
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>


#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

#define TAILLE_NOM 20

typedef struct MyData_f_display_image_activity
{
    float **recall;
    int gp_input;
    int gp_enable;
    int max_recall;
    int nb_cell_to_display;
    int PX;
    int PY;
    int I;
    int N;
    int M;
    int inc;
    int type_sortie;
    int display;
    FILE *fp;
    long start_time_sec;
    long start_time_msec;
    int time_sim_flag;
    int time_abs_flag;
    int cpt;
    const char *printing_format;
    float cpt_scale;
    char name[TAILLE_NOM];
    int save;
} MyData_f_display_image_activity;

void affiche_cell_activity(int gpe);

/**
 * Initialise les parametres du groupe.
 */
void new_display_image_activity(int gpe)
{
  int index, link;
  int nb_cell_to_display, i, j, N, M, gp_input=-1, gp_enable=-1, inc;
  int I = 1, PX = 0, PY = 0, t_sortie = 1;
  int display = 0, file_flag = 1, time_sim_flag = 0, time_abs_flag = 0, cpt = 0;
  struct timeval StartTime;
  long start_time_sec, start_time_msec;
  float cpt_scale = 0.;
  char x_pos[4], y_pos[4], interface[1], mem[3], type_sortie[4], display_param[1];
  char f_opt[4], cpt_param[10], time_sim[4], time_abs[4], string[255];
  char nom[TAILLE_NOM];
  char name[TAILLE_NOM];
  FILE *fp = NULL;
  int max_recall = 20;
  float **recall;
  MyData_f_display_image_activity *my_data = NULL;
  char *file_name = NULL;
  int save = -1;
  const char *printing_format="%f ";
 
  memset(name,0,TAILLE_NOM);
     
  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);
    while (link != -1)
    {	 	 
      /*---------------------------------*/
      /*Getting option on the input link */
      if (strstr(liaison[link].nom, "enable") != NULL ) 
      {
        dprints("enable group found\n",param);
        gp_enable = liaison[link].depart;
      }
      else if (strstr(liaison[link].nom, "sync") !=NULL )
      {
      }
      else
      {
        gp_input = liaison[link].depart;
        if (prom_getopt(liaison[link].nom, "x", x_pos) == 2)     PX = atoi(x_pos);
        if (prom_getopt(liaison[link].nom, "y", y_pos) == 2)     PY = atoi(y_pos);
        if (prom_getopt(liaison[link].nom, "i", interface) == 2) I = atoi(interface);
        if (prom_getopt(liaison[link].nom, "m", mem) == 2)       max_recall = atoi(mem);
        if (prom_getopt(liaison[link].nom, "s", type_sortie) == 2)	t_sortie = atoi(type_sortie);
        if (prom_getopt(liaison[link].nom, "d", display_param) > 0)
        {
          if (display_param[0] == '\0')   /* Test de si d est suivi d'un nombre ou non, si non,        */
            display = 1;    /* l'affichage aura lieu. Ainsi nous avons d equivalent a d1 */
          else display = atoi(display_param);
        }

        if (prom_getopt(liaison[link].nom, "f", f_opt) == 1)
        {
          sprintf(string, "%s.%d.by.%d.SAVE", def_groupe[gp_input].nom, gp_input, gpe);
          file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
          strcpy(file_name, string);
        }
        if (prom_getopt(liaison[link].nom, "f", f_opt) == 2)
        {
          sprintf(string, "%s.SAVE", f_opt);
          file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
          strcpy(file_name, string);
          /* fichier sera cree. Ainsi nous avons f equivalent a f1 */
        }		 
        if (prom_getopt(liaison[link].nom, "g", nom) > 0)
        {
          printing_format="%g ";
          dprints("f_diplay_image_activity(%s) : printing format : \%g au lieu de \%f\n",def_groupe[gpe].no_name,nom);
        }

       
        if (prom_getopt(liaison[link].nom, "n", nom) == 2)
        {
          sprintf(name, "%s", nom);
          dprints("f_diplay_image_activity(%s) : graphique : %s\n",def_groupe[gpe].no_name,nom);
        }

        if (prom_getopt(liaison[link].nom, "r", f_opt) == 1)
        {
          file_flag = 2;
        }

        if (prom_getopt(liaison[link].nom, "c", cpt_param) > 0)
        {
          if (cpt_param[0] == '\0')   /* idem */ cpt_scale = 1.;
          else cpt_scale = (float) (atof(cpt_param));
          cpt = 1;
        }
        if (prom_getopt(liaison[link].nom, "tsim", time_sim) > 0)
        {
          if (time_sim[0] == '\0') time_sim_flag = 1;  /* idem */
          else time_sim_flag = atoi(time_sim);
        }
        if (prom_getopt(liaison[link].nom, "tabs", time_abs) > 0)
        {
          if (time_abs[0] == '\0') time_abs_flag = 1;  /* idem */
          else time_abs_flag = atoi(time_abs);
        }
       
        if (strcmp(liaison[link].nom, "save") == 0)	save = liaison[link].depart;
      }
       
      index++;
      link = find_input_link(gpe, index);	 
    }

    /*---------------------------------------*/
    /* Getting dimensions of the input group */
    if(gp_input == -1)
      EXIT_ON_ERROR("need a input group in %s",def_groupe[gpe].no_name);
    
    N = def_groupe[gp_input].taillex;
    M = def_groupe[gp_input].tailley;
    
    inc = def_groupe[gp_input].nbre / (N * M);
    nb_cell_to_display = N * M;

    recall = malloc(max_recall * sizeof(float *));
    for (i = 0; i < max_recall; i++)
    {
      recall[i] = malloc(nb_cell_to_display * sizeof(float *));
      for (j = 0; j < nb_cell_to_display; j++)
      {
        recall[i][j] = 0.5;
      }
    }

    /*------------------------------*/
    /* Opening the file for writing */

    if (file_name != NULL)
    {
      if (file_flag == 1)
        fp = fopen(file_name, "a");
      else
        fp = fopen(file_name, "w");
      if (fp == NULL)
        EXIT_ON_ERROR("ERROR in new_image_display_activity(%s): Impossible to create file %s\n", def_groupe[gpe].no_name, file_name);
    }

    /*--------------------------------------*/
    /* Getting start time of the simulation */
    
    gettimeofday(&StartTime, (void *) NULL);
    start_time_sec =  StartTime.tv_sec;
    start_time_msec = StartTime.tv_usec;

    /*----------------------------------------------------------------------------*/
    /* Putting right thing in right place in the MyData_f_save_activity structure */

    my_data = malloc(sizeof(MyData_f_display_image_activity));
    my_data->recall = recall;
    my_data->gp_input = gp_input;
    my_data->gp_enable = gp_enable;
    my_data->max_recall = max_recall;
    my_data->nb_cell_to_display = nb_cell_to_display;
    my_data->PX = PX;
    my_data->PY = PY;
    my_data->I = I;
    my_data->N = N;
    my_data->M = M;
    my_data->inc = inc;
    my_data->type_sortie = t_sortie;
    my_data->display = display;
    my_data->fp = fp;
    my_data->start_time_sec = start_time_sec;
    my_data->start_time_msec = start_time_msec;
    my_data->time_sim_flag = time_sim_flag;
    my_data->time_abs_flag = time_abs_flag;
    my_data->cpt = cpt;
    my_data->cpt_scale = cpt_scale;
    my_data->printing_format=printing_format;
    sprintf(my_data->name, "%s", name);
    my_data->save = save;
    def_groupe[gpe].data = (void *) my_data;
    

    if (display == 1)
    {
      dprints("new_display_image_activity(%s): display activity at location %dX%d on image %d with max_recall = %d and type_sortie = %d \n", def_groupe[gpe].no_name, PX, PY, I, max_recall, t_sortie);
    }
    else
    {
      dprints("new_display_image_activity(%s): no display\n", def_groupe[gpe].no_name);
    }

    if (file_name != NULL)
    {
      dprints("new_display_image_activity(%s): Write on file %s \n", def_groupe[gpe].no_name, file_name);
      free(file_name);
    }
  }   
}


void function_display_image_activity(int gpe)
{
   struct timeval CurrentTime;
   int nb_cell_to_display, i, j, pos, N, M, neur, gp_input, gp_enable, inc;
   int /*I = 1, PX = 0, PY = 0,*/ t_sortie = 1;
   int display = 0, time_sim_flag = 0, time_abs_flag = 0, cpt = 0;
   long start_time_sec, start_time_msec, time_of_sim_sec = 0, time_of_sim_msec = 0;
   long current_time_sec, current_time_msec;
   float current_time = 0;
   float act = 0, cpt_scale = 0.;
   int save = -1;
   float save_doc = 1;
   FILE *fp;
   int max_recall = 20;
   float **recall;
   const char *printing_format;
   MyData_f_display_image_activity *my_data = NULL;

   dprints("-------------------------------------------------\n");
   dprints("function_display_image_activity---------------------\n");
   
   my_data = (MyData_f_display_image_activity *) def_groupe[gpe].data;
   if (my_data == NULL)
   {
      fprintf(stderr, "ERROR in f_display_image_activity(%s): Cannot retreive data\n", def_groupe[gpe].no_name);
      exit(1);
   }
   
   recall = my_data->recall;
   gp_input = my_data->gp_input;
   gp_enable = my_data->gp_enable;
   max_recall = my_data->max_recall;
   nb_cell_to_display = my_data->nb_cell_to_display;
   /*PX = my_data->PX;
   PY = my_data->PY;
   I = my_data->I;*/
   N = my_data->N;
   M = my_data->M;
   inc = my_data->inc;
   t_sortie = my_data->type_sortie;
   display = my_data->display;
   fp = my_data->fp;
   start_time_sec = my_data->start_time_sec;
   start_time_msec = my_data->start_time_msec;
   time_sim_flag = my_data->time_sim_flag;
   time_abs_flag = my_data->time_abs_flag;
   cpt = my_data->cpt;
   cpt_scale = my_data->cpt_scale;
   save = my_data->save;
   printing_format=my_data->printing_format;
   
   if(gp_enable != -1 && neurone[def_groupe[gp_enable].premier_ele].s1 < 0.5)
    return;
   if (save !=-1) save_doc = neurone[def_groupe[save].premier_ele].s1;
    if (fp != NULL && save_doc > 0.5)
    {
        dprints("debut ecriture fichier(%s)\n", def_groupe[gpe].no_name);

	/*------------------------------------------------*/
        /* Saving neurone activity of input group in file */

        if (time_abs_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            fprintf(fp, "%li.%06li ", CurrentTime.tv_sec, CurrentTime.tv_usec);
        }
        if (time_sim_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec =  CurrentTime.tv_sec;
            current_time_msec =  CurrentTime.tv_usec;
            time_of_sim_sec = current_time_sec - start_time_sec;
            time_of_sim_msec = current_time_msec - start_time_msec;
            current_time = time_of_sim_sec + time_of_sim_msec / 1000000.;
            fprintf(fp, printing_format, current_time);
        }
        if (cpt_scale > 0)
        {
            fprintf(fp, printing_format, cpt * cpt_scale);
            cpt++;
            my_data->cpt = cpt;
        }
	for (j = 0; j < M; j++)
	{
	   for (i = 0; i < N; i++)
	   {
                pos = i + j * N;
                neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
                if (t_sortie == 0)      act = neurone[neur].s;
                else if (t_sortie == 1) act = neurone[neur].s1;
                else if (t_sortie == 2) act = neurone[neur].s2;

                fprintf(fp, printing_format, act);
            }
        }
        fprintf(fp, "\n");
        dprints("fin ecriture fichier\n");
    }

    if (display == 1)
    {
       dprints("debut display (%s)\n", def_groupe[gpe].no_name);

        for (i = 0; i < max_recall - 1; i++)
            for (j = 0; j < nb_cell_to_display; j++)
                recall[i][j] = recall[i + 1][j];

        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                pos = i + j * N;
                neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
                if (t_sortie == 0)       act = neurone[neur].s;
                else if (t_sortie == 1)  act = neurone[neur].s1;
                else if (t_sortie == 2)  act = neurone[neur].s2;

                recall[max_recall - 1][pos] = act;
            }


#ifdef DEBUG
        for (i = 0; i < max_recall; i++)
        {
            for (j = 0; j < nb_cell_to_display; j++)
            {
                printf("%f\t", recall[i][j]);
            }
            printf("\n");
        }
#endif
        affiche_cell_activity(gpe);
    }

    dprints("fin %s\n", __FUNCTION__);
}

void destroy_display_image_activity(int gpe)
{
   MyData_f_display_image_activity *my_data = NULL;
   int i;

   dprints("destroy_display_image_activity(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data != NULL)
   {
      my_data = (MyData_f_display_image_activity *) def_groupe[gpe].data;

      if (my_data->fp != NULL)
      {
	    fclose(my_data->fp);
	    my_data->fp = NULL;
      }

      if (my_data->recall  != NULL)
      {
	    for (i = 0; i < my_data->max_recall; i++)
	    {
	     free(my_data->recall[i]);
	     my_data->recall[i] = NULL;
	    }
	   free(my_data->recall);
	   my_data->recall = NULL;
      }
	 
      free(my_data);
      def_groupe[gpe].data = NULL;
   }

   dprints("destroy_display_image_activity(%s): Leaving function\n", def_groupe[gpe].no_name);
}


void affiche_cell_activity(int gpe)
{
#ifndef AVEUGLE
#define nb_couleurs_cell_activity 10
    int i, j;
    float repx, repy, taille = 200.;
    TxPoint point1, point2, point3;
    int couleur[nb_couleurs_cell_activity];
    char name[20];
    MyData_f_display_image_activity *my_data;

    my_data = (MyData_f_display_image_activity *) def_groupe[gpe].data;


    couleur[0] = 0;
    couleur[1] = 17;
    couleur[2] = 18;
    couleur[3] = 19;
    couleur[4] = 8;
    couleur[5] = 20;
    couleur[6] = 21;
    couleur[7] = 22;
    couleur[8] = 23;
    couleur[9] = 14;



    repx = my_data->PX;
    repy = taille;

    point1.x = repx;
    point1.y = my_data->PY;
    
    point3.x = my_data->PX+5;
    point3.y = my_data->PY+10;
    
	sprintf(name, "%s", my_data->name);
	dprints("nom graphique : %s\n",name);
    if (my_data->I == 1)
    {
        TxDessinerRectangle(&image1, blanc, TxPlein, point1, (int) (5 * my_data->max_recall + repx), (int) (taille + 10), 1);
        TracerAxe(&image1, bleu, bleu, (int) repx, (int) repy + my_data->PY, (int) (repx + taille), my_data->PY, 0, 0);
        TxEcrireChaineSized(&image1, noir, point3, name,15, NULL);
    }
    else
    {
        TxDessinerRectangle(&image2, blanc, TxPlein, point1, (int) (5 * my_data->max_recall + repx), (int) (taille + 10), 1);
        TracerAxe(&image2, bleu, bleu, (int) repx, (int) repy + my_data->PY, (int) (repx + taille), my_data->PY, 0, 0);
        TxEcrireChaineSized(&image2, noir, point3, name, 15, NULL);
    }
    for (i = 0; i < my_data->max_recall - 1; i++)
    {
        for (j = 0; j < my_data->nb_cell_to_display; j++)
        {
            point1.x = 5 * i + my_data->PX;
            point1.y = taille - taille * my_data->recall[i][j] + my_data->PY;
            point2.x = 5 * (i + 1) + my_data->PX;
            point2.y = taille - taille * my_data->recall[i + 1][j] + my_data->PY;
            if (my_data->I == 1)
            {
                TxDessinerSegment(&image1, couleur[(int) j % nb_couleurs_cell_activity], point1, point2, 1);
                if (j >= 16)    /*16, c'est blanc */
                    TxDessinerSegment(&image1, j + 1, point1, point2, 1);
            }
            else
            {
                TxDessinerSegment(&image2, couleur[(int) j % nb_couleurs_cell_activity], point1, point2, 1);
                if (j >= 16)    /*16, c'est blanc */
                    TxDessinerSegment(&image2, j + 1, point1, point2, 1);
            }
        }
    }
    if (my_data->I == 1)  TxFlush(&image1);
    else                  TxFlush(&image2);
#endif
    (void)gpe;
}
