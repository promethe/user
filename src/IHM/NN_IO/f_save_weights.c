/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \ingroup libIHM
   \defgroup f_save_weights f_save_weights
   \brief Sauvegarde les poids de la liaison entre 2 groupes
 


   \section Description
   Sauvegarde les poids de la liaison entre le groupe d'entre 'from' et le groupe d'entre 'to'. Avec le nom:
   weights<indice incremental de experience>_<groupe 'from'>_<groupe 'to'>.SAVE

   Ce groupe doit donc avoir 2 liaisons : un nomme 'from' et un nomme 'to' en entree.
   Par defaut enregistre une premiere ligne correspondant a l'etat au debut de l'execution de promethe.
   Optionally, a link 'condition' can be added. The writting is performed
   only if the first neuron of the input group of this link is greater than 0.5.

   \section Options

   - -c : ajout du numero d'iteration sur la premiere colonne
   - -tabs : ajout temps absolu (sec.usec) sur la premiere colonne (pas de cumul avec -c).
   - -r : rewrite : rewrite file instead of incrementing counter of file
   - -f : prefix name of file
   - -nohead : pas de ligne d'entete pour le fichier cree.
   - -maxid : sauvegarde l'indice du max des coeffs vers un neurone plutot que toutes les valeurs
   - -T<int> : periode (en nb d'iteration) pour la sauvegarde dans le fichier. Il vaut mieux utiliser -c aussi pour avoir l'indice de l'iteration enregistree dans la premiere colonne.

   \file
   \ingroup f_save_weights

   \section Author
   - Arnaud Blanchard
   - Created: 20/11/2009

*/


#include <sys/stat.h>
#include <libx.h>
#include <dev.h>
#include <Components/motor.h>
#include <net_message_debug_dist.h>

#define MAXIMUM_SIZE_OF_OPTION 128
#define SIZE_OF_FILENAME 128

/** group data */
typedef struct box{
  FILE *file;
  type_liaison *link;
  int number_of_final_neurons, index_of_link;
  type_neurone *final_neurons;
  int condition_group_number;
  int mode; /** mode : 0, pas de premiere colonne
		1, premiere colonne entier
		2, premiere colonne timestamp */
  int count; /** used to store number of iteration */
  int only_max; /** store max weight index for each neuron instead of
		 * all values */
  int period; /** count period for printing to file*/
}Box;

void new_save_weights(int index_of_group)
{
  Box *box;
  char filename[SIZE_OF_FILENAME];
  int i;
  int index_of_link, number_of_links;
  int from_group_number, to_group_number;
  type_liaison *link = NULL;
  type_coeff *coeff;
  type_neurone *neuron;
  struct stat buffer_of_stat;
  char param[256];
  char name[256];
  int rewrite=0, only_max=0;
  int nohead=0, period=1;
  float tmp_max;
  int tmp_max_id, tmp_id;
  struct timeval CurrentTime;

  type_groupe *group;

  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box);
  group->data = box;
	
  from_group_number = -1;
  to_group_number = -1;
  box->condition_group_number=-1;
  box->mode=0;
  box->count=0;

  /** init name */
  name[0]='\0';

  /** On recherche les liens */
  for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];

    if(prom_getopt(link->nom,"f",param)==2) {
      strcpy(name,param);
      name[strlen(name)]='_';
      name[strlen(name)+1]='\0';
    }
    if(prom_getopt(link->nom,"c",param)>0) {
      box->mode=1;
    }
    if(prom_getopt(link->nom,"tabs",param)>0) {
      box->mode=2;
    }
    if(prom_getopt(link->nom,"r",param)>0) {
      rewrite=1;
    }
    if(prom_getopt(link->nom,"nohead",param)>0) {
      nohead=1;
    }
    if(prom_getopt(link->nom,"maxid",param)>0) {
      only_max=1;
    }
    if(prom_getopt(link->nom,"T",param)>1) {
      period=atoi(param);
      if(period<=0)
	EXIT_ON_ERROR("Period (-T) must be int strictly positive (%d)!\n",period);
    }
    if (strncmp(link->nom, "from",4)==0) from_group_number = link->depart;
    else if (strncmp(link->nom, "to",2)==0) to_group_number = link->depart;
    else if (strncmp(link->nom, "condition",9)==0) box->condition_group_number = link->depart;
    else EXIT_ON_ERROR("Group: %d, type of link unknowm: %s", index_of_group, link->nom);
  }


  if ((from_group_number == -1)||(to_group_number==-1)) EXIT_ON_ERROR("For the group:%s, you must have 2 input links ('from' and 'to') (optionally : 'condition')", group->no_name);

  if ((box->index_of_link = find_input_link(to_group_number, 0)) == -1) EXIT_ON_ERROR("There is no link between group number %d and group number %d.", from_group_number, to_group_number);
  link = &liaison[box->index_of_link];
  /** On cherche l'indice du lien (box->index_of_link) dont on doit sauver les poids */
  for (number_of_links = 1; link->depart!=from_group_number; number_of_links++)
  {
    if ((box->index_of_link = find_input_link(to_group_number, number_of_links)) == -1) EXIT_ON_ERROR("There is no link between group number %d and group number %d.", from_group_number, to_group_number);
    link = &liaison[box->index_of_link];
  }
	
  box->only_max = only_max;
  box->period = period;
  box->final_neurons = &neurone[def_groupe[to_group_number].premier_ele];
  box->number_of_final_neurons = def_groupe[to_group_number].nbre;

  if(rewrite)
    sprintf(filename, "%sweights_%s_%s.SAVE", name, def_groupe[link->depart].no_name, def_groupe[link->arrivee].no_name);
  else {
    /** On cree le nom du fichier pour sauver les donnees. Si l'indice de l'experience (0 au depart) est pris, on en prend un plus grand.  */
    i=0;
    do
    {
      sprintf(filename, "%sweights%d_%s_%s.SAVE", name,i, def_groupe[link->depart].no_name, def_groupe[link->arrivee].no_name);
      i++;
    }
    while (stat(filename, &buffer_of_stat)==0);
  }

  /** On cree le fichier et les donnees d'entete */
  box->file = fopen(filename, "w");
  if(!nohead) {/** entete */
  if(box->mode>0)   fprintf(box->file, "\t");
  for (i=0; i<box->number_of_final_neurons; i++)
  {
    neuron = &box->final_neurons[i];
    for (coeff = neuron->coeff; coeff != NULL; coeff=coeff->s)
    {
      if (coeff->gpe_liaison == box->index_of_link) fprintf(box->file, "%s_%d->%s_%d \t", def_groupe[from_group_number].no_name, coeff->entree-def_groupe[from_group_number].premier_ele, def_groupe[to_group_number].no_name, i);
    }
    fprintf(box->file, "\t");
  }
  fprintf(box->file, "\n");
  }


  /* enregistrement premiere ligne */
  /** ajout entier */
  if(box->mode==1) {
    fprintf(box->file, "%d\t",box->count);
  }
  /** ajout tabs */
  if (box->mode == 2)
  {
    gettimeofday(&CurrentTime, (void *) NULL);
    fprintf(box->file, "%li.%06li ", CurrentTime.tv_sec, CurrentTime.tv_usec);
  }
  for (i=0; i<box->number_of_final_neurons; i++)
  {
    neuron = &box->final_neurons[i];
    tmp_max = neuron->coeff->val;
    tmp_max_id=0;
    tmp_id=0;
    for (coeff = neuron->coeff; coeff != NULL; coeff=coeff->s)
    {
      if (coeff->gpe_liaison == box->index_of_link) {
	if(only_max) {
	  if(coeff->val>tmp_max) {
	    tmp_max=coeff->val;
	    tmp_max_id=tmp_id;
	  }
	  tmp_id++;
	}
	else fprintf(box->file, "%f\t", coeff->val);
      }
    }
    if(only_max)
      fprintf(box->file, "%d\t", tmp_max_id);
    fprintf(box->file, "\t");
  }
  fprintf(box->file, "\n");

}

void function_save_weights(int index_of_group)
{
  Box *box;
  int i;
  type_coeff *coeff;
  type_neurone *neuron;
  int condition_gpe, only_max=0, period=1;
  float tmp_max;
  int tmp_max_id, tmp_id;
  struct timeval CurrentTime;
   
  /** On recupere la boite a partir du groupe*/
  box = (Box*)def_groupe[index_of_group].data;
  condition_gpe=box->condition_group_number;
  only_max=box->only_max;
  period=box->period;

  box->count++;
  
  if(box->count%period)
    return;   

  if(condition_gpe == -1 || neurone[def_groupe[condition_gpe].premier_ele].s1 >0.5) {
	  
    /** ajout entier */
    if(box->mode==1) {
      fprintf(box->file, "%d\t",box->count);
    }
    /** ajout tabs */
    if (box->mode == 2)
    {
      gettimeofday(&CurrentTime, (void *) NULL);
      fprintf(box->file, "%li.%06li ", CurrentTime.tv_sec, CurrentTime.tv_usec);
    }
	  
    for (i=0; i<box->number_of_final_neurons; i++)
    {
      neuron = &box->final_neurons[i];
      tmp_max = neuron->coeff->val;
      tmp_max_id=0;
      tmp_id=0;
      
	    
      for (coeff = neuron->coeff; coeff != NULL; coeff=coeff->s)
      {
	if (coeff->gpe_liaison == box->index_of_link) {
	  if(only_max) {
	    if(coeff->val>tmp_max) {
	      tmp_max=coeff->val;
	      tmp_max_id=tmp_id;
      }
	    tmp_id++;
	  }
	  else {
	    fprintf(box->file, "%f\t", coeff->val);
	  }
	}
      }
      if(only_max)
	fprintf(box->file, "%d\t", tmp_max_id);
      fprintf(box->file, "\t");
    }
    fprintf(box->file, "\n");
  }
}


void destroy_save_weights(int index_of_group)
{
  Box *box;

  box = (Box*)def_groupe[index_of_group].data;

  fclose(box->file);
  free(def_groupe[index_of_group].data);
}

