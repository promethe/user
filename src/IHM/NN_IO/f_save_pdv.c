/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_affiche_pdv_xy.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: S. Chevallier
- description: specific file creation
- date: 4/02/2010

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  save WTA value in a given file

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: X/Y/FILENAME
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <public_tools/Vision.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data
{
  int GpeX, GpeY;
  char filename[255];
} MyData;

/* #define DEBUG */

void function_save_pdv (int Gpe)
{
  MyData *data = NULL;
  FILE *f = NULL;
  char param[255];
  int i, j, CoordX = -1, CoordY = -1, GpeX = 0, GpeY = 0;
  float Max;

#ifdef DEBUG
  printf ("debut f_save_pdv\n");
#endif
  
  if (def_groupe[Gpe].data == NULL)
    {
		if ((data = (MyData *) malloc(sizeof(MyData))) == NULL)
		  {
			 fprintf (stderr, "%s(%d): malloc failed for data\n", 
						 __FUNCTION__, Gpe);
			 exit (1);
		  }
		data->filename[0] = '\0' ; data->GpeX = -1; data->GpeY = -1;

		i = 0;
		while ((j = find_input_link (Gpe, i)) != -1)
		  {
			 if (prom_getopt (liaison[j].nom, "X", param) == 1)
				{
				  data->GpeX = liaison[j].depart;
				  GpeX = liaison[j].depart;
				}
			 else if (prom_getopt (liaison[j].nom, "Y", param) == 1)
				{
				  data->GpeY = liaison[j].depart;
				  GpeY = liaison[j].depart;
				}
			 if (prom_getopt (liaison[j].nom, "F", data->filename) == 2)
				{
				  if ((f = fopen (data->filename, "w")) == NULL) 
					 {
						printf ("in function_save_pdv: unable to open file %s\n",
								  data->filename);
						exit (1);
					 }
				  fclose (f);
				}
			 i++;
		  }
		if ((data->GpeX == -1) || (data->GpeY == -1) 
			 || (strlen(data->filename) == 0)) 
		  {
			 printf ("in function_save_pdv: need 3 links -X, -Y and -Ffilename\n");
			 exit (1);
		  }
#ifdef DEBUG
		else 
		  printf ("creation de data avec GpeX = %d, GpeY = %d et filename = %s\n",
					 data->GpeX, data->GpeY, data->filename);
#endif
		def_groupe[Gpe].data = (void *) data;
	 }
  else 
	 {
		data = (MyData*) def_groupe[Gpe].data;
		GpeX = data->GpeX;
		GpeY = data->GpeY;
	 }
  /* Find X max */
  Max = 0.0;
  for (i = def_groupe[GpeX].premier_ele;
		 i < def_groupe[GpeX].premier_ele + def_groupe[GpeX].nbre; i++)
	 if (neurone[i].s1 > Max)
		{
		  Max = neurone[i].s1;
		  CoordX = i - def_groupe[GpeX].premier_ele;
		}
  /* Find Y max */
  Max = 0.0;
  for (i = def_groupe[GpeY].premier_ele;
		 i < def_groupe[GpeY].premier_ele + def_groupe[GpeY].nbre; i++)
	 if (neurone[i].s1 > Max)
		{
		  Max = neurone[i].s1;
		  CoordY = i - def_groupe[GpeY].premier_ele;
		}
  /* Save the results */
#ifdef DEBUG
  printf ("printing %d %d in %s\n", CoordX, CoordY, data->filename);
#endif
  if ((f = fopen (data->filename, "a+")) == NULL) 
	 {
		printf ("in function_save_pdv: unable to open file %s\n",
				  data->filename);
		exit (1);
	 }
  fprintf (f, "%d %d\n", CoordX, CoordY);
  fclose (f);

#ifdef DEBUG
  printf("===============fin de f_save_pdv\n");
#endif
#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
		SecondesFunctionTimeTrace =
		  OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace =
		  OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
  else
    {
		SecondesFunctionTimeTrace =
		  OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
		  1;
		MicroSecondesFunctionTimeTrace =
		  1000000 + OutputFunctionTimeTrace.tv_usec -
		  InputFunctionTimeTrace.tv_usec;
    }
  sprintf(MessageFunctionTimeTrace,
			 "Time in function_save_pdv\t%4ld.%06d\n",
			 SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif
}
