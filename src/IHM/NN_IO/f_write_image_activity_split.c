/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: C.G
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Cette fonction affiche l'activite des neurone du groupe d'entree aucours du temps
	et/ou ecrit les activites des neurones du groupe d'entree dans un fichier. 
	Chaque colonne de ce fichier correspond a un neurone et chaque ligne correspond a un releve.  
	Des colonnes complementaires peuvent etre demande pour ecrire de nouvelles colonnes indiquant 
	le numero de la ligne, le numero de ligne multiplie par un facteur, le temps ecoule depuis 
	le debut de la simulation.  

	Option sur le lien et valeur par defaut :
	-s0   : sortie desire (0,1 ou 2)
	
	
	Options sur le lien et valeur par defaut pour l'affichage :
	-x0   : position du graphe dans l'image
	-y0   : postion du graphe dans l'image
	-i1   : numero de l'interface (1 ou 2)
	-m20  : memoire temporelle de la boite
	-d1   : actif, permet la visualisation
	 
	Options sur le lien et valeurs par defaut pour la sauvegarde dans un fichier :
	-f0             : sauvegarde des activites dans un fichier inactive
	-f ou f1        : sauvegarde des activites dans un fichier active
	-c ou -c1       : ecrit la valeur du compteur en premiere colonne (ou deuxieme ou troisieme 
			si -tsim ou tabs sont avant)
	-c0             : n'ecrit pas de colonne compteur
	-c22.5          : ecrit en premiere colonne le compteur mult par 22.5
	-tsim ou -tsim1 : ecrit le temps ecoule depuis le debut de la simulation en premiere colonne 
	(ou deuxieme si -tabs est avant) en fait deux colonnes, une pour les secondes et la suivante pour les 
			millisecondes
	-tsim0          : n'ecrit pas de colonne temps
	-tabs ou tabs1  : ecrit le temps absolu en premiere colonne (-----Attention !----- : necessaire pour pouvoir 
			comparer deux promethes qui interagissent par l'environnement : ex: Osc_arm_cam_couples)
	-tabs0          : n'ecrit pas de colonne temps absolu


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-affiche_cell_activity()

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct MyData_f_write_image_activity_split
{
    int gp_input;
    int nb_cell_to_display;
    int N;
    int M;
    int inc;
    int type_sortie;
    int file_flag;
    char file_name[255];
    char racine_filename[255];
    int nb_file;
    int limit_line;
    long start_time_sec;
    long start_time_msec;
    int time_sim_flag;
    int time_abs_flag;
    int cpt;
    float cpt_scale;
} MyData_f_write_image_activity_split;

/*#define DEBUG*/
void function_write_image_activity_split(int numero)
{
    struct timeval StartTime, CurrentTime;
    int nb_cell_to_display, i, j, l, pos, N, M, neur, gp_input, inc, I =
        1, PX = 0, PY = 0, t_sortie = 1;
    int display = 0, file_flag = 0, time_sim_flag = 0, time_abs_flag =
        0, cpt = 0;
    float start_time_sec, start_time_msec, time_of_sim_sec =
        0, time_of_sim_msec =
        0, current_time_sec, current_time_msec, current_time = 0;
    float act = 0, cpt_scale = 0.;
/* static int cpt;*/
    char type_sortie[4];
    char f_opt[4], cpt_param[10], time_sim[4], time_abs[4], string[255];
    FILE *fp;
    int max_recall = 20;
    float **recall;
    MyData_f_write_image_activity_split *my_data = NULL;
    char file_name[255];
    char racine_filename[255];
    int nb_file;
    int limit_line=-1;
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_write_image_activity_split---------------------\n");
#endif

    if (def_groupe[numero].data == NULL)
    {
  /*---------------------------*/
        /*Looking for the input group */
        l = find_input_link(numero, 0);
        gp_input = liaison[l].depart;

 /*---------------------------------*/
        /*Getting option on the input link */

        if (prom_getopt(liaison[l].nom, "s", type_sortie) == 2)
            t_sortie = atoi(type_sortie);
        if (prom_getopt(liaison[l].nom, "c", cpt_param) > 0)
        {
            if (cpt_param[0] == '\0')   /* idem */
                cpt_scale = 1.;
            else
                cpt_scale = (float) (atof(cpt_param));
            cpt = 0;
        }
        if (prom_getopt(liaison[l].nom, "tsim", time_sim) > 0)
        {
            if (time_sim[0] == '\0')
                time_sim_flag = 1;  /* idem */
            else
                time_sim_flag = atoi(time_sim);
        }
        if (prom_getopt(liaison[l].nom, "tabs", time_abs) > 0)
        {
            if (time_abs[0] == '\0')
                time_abs_flag = 1;  /* idem */
            else
                time_abs_flag = atoi(time_abs);
        }

	if (prom_getopt(liaison[l].nom, "l", cpt_param) > 0)
        {
            limit_line = (atoi(cpt_param));
	    nb_file=0;
        }

        if (prom_getopt(liaison[l].nom, "f", f_opt) == 1)
        {
            sprintf(racine_filename,"%s.%d.by.%d.SAVE",def_groupe[gp_input].nom,
                    gp_input, numero);
	    sprintf(string, "%s.%d.by.%d.SAVE0", def_groupe[gp_input].nom,
                    gp_input, numero);
            strcpy(file_name, string);
            file_flag = 1;      /* fichier sera cree. Ainsi nous avons f equivalent a f1 */
        }
        if (prom_getopt(liaison[l].nom, "f", f_opt) == 2)
        {
            sprintf(racine_filename, "%s.SAVE", f_opt);
	    sprintf(string, "%s.SAVE0", f_opt);
	    sprintf(file_name,"%s",string);
	    file_flag = 1;
            /* fichier sera cree. Ainsi nous avons f equivalent a f1 */
        }
/*---------------------------------------*/
        /* Getting dimensions of the input group */

        N = def_groupe[gp_input].taillex;
        M = def_groupe[gp_input].tailley;

        inc = def_groupe[gp_input].nbre / (N * M);

        if (inc > 1)
            nb_cell_to_display = N * M - def_groupe[gp_input].nbre;
        else
            nb_cell_to_display = N * M;


        recall = malloc(max_recall * sizeof(float *));
        for (i = 0; i < max_recall; i++)
        {
            recall[i] = malloc(nb_cell_to_display * sizeof(float *));
            for (j = 0; j < nb_cell_to_display; j++)
            {
                recall[i][j] = 0.5;
            }

        }

 /*---------------------------*/
        /* Creating destination file */

	if(limit_line==-1)
	   EXIT_ON_ERROR("Incorrect use of group. Pb with limit_line number\n");

/*--------------------------------------*/
/* Getting start time of the simulation */

        gettimeofday(&StartTime, (void *) NULL);
        start_time_sec = (float) StartTime.tv_sec;
        start_time_msec = (float) StartTime.tv_usec;

 /*----------------------------------------------------------------------------*/
        /* Putting right thing in right place in the MyData_f_save_activity structure */

        my_data = malloc(sizeof(MyData_f_write_image_activity_split));
        my_data->gp_input = gp_input;
        my_data->nb_cell_to_display = nb_cell_to_display;
        my_data->N = N;
        my_data->M = M;
        my_data->inc = inc;
        my_data->type_sortie = t_sortie;
        my_data->file_flag = file_flag;
        my_data->start_time_sec = start_time_sec;
        my_data->start_time_msec = start_time_msec;
        my_data->time_sim_flag = time_sim_flag;
        my_data->time_abs_flag = time_abs_flag;
        my_data->cpt = cpt;
        my_data->cpt_scale = cpt_scale;
	sprintf(my_data->file_name,"%s",file_name);
        sprintf(my_data->racine_filename,"%s",racine_filename);
        my_data->nb_file=nb_file;
	my_data->limit_line=limit_line;
	def_groupe[numero].data = (void *) my_data;

        if (display == 1)
        {
            printf
                ("Option du %s.%d:%dX%d sur interface %d avec max_recall = %d et type_sortie = %d \n",
                 __FUNCTION__, numero, PX, PY, I, max_recall, t_sortie);
        }
        else
        {
            printf("Option du %s.%d:no_display\n", __FUNCTION__, numero);
        }
        if (file_name != NULL)
            printf("ecriture sur %s: incx=%f\n", file_name, cpt * cpt_scale);
    }
    else
    {
        my_data = (MyData_f_write_image_activity_split *) def_groupe[numero].data;
        gp_input = my_data->gp_input;
        nb_cell_to_display = my_data->nb_cell_to_display;
        N = my_data->N;
        M = my_data->M;
        inc = my_data->inc;
        t_sortie = my_data->type_sortie;
        file_flag = my_data->file_flag;
        start_time_sec = my_data->start_time_sec;
        start_time_msec = my_data->start_time_msec;
        time_sim_flag = my_data->time_sim_flag;
        time_abs_flag = my_data->time_abs_flag;
        cpt = my_data->cpt;
        cpt_scale = my_data->cpt_scale;
	strcpy(file_name, my_data->file_name);
	strcpy(racine_filename, my_data->racine_filename);
	nb_file=my_data->nb_file;
	limit_line=my_data->limit_line;
    }

    if (file_name != NULL)
    {
#ifdef DEBUG
        printf("debut ecriure fichier\n");
#endif
        /*Open the file */
        fp = fopen(file_name, "a");
        if (fp == NULL)
        {
            printf("Impossible to create file %s\n", file_name);
            exit(EXIT_FAILURE);
        }


 /*------------------------------------------------*/
        /* Saving neurone activity of input group in file */
        if (cpt_scale > 0)
        {
	    if(cpt%limit_line==0)
	    {
		printf("cpt=%d,limit=%d\n",cpt,limit_line);
		fclose(fp);
		nb_file++;
		sprintf(file_name,"%s%d",racine_filename,nb_file);
		fp=fopen(file_name, "a");
		if (fp == NULL)
		{
			printf("Impossible to create file %s\n", file_name);
			exit(EXIT_FAILURE);
		}
		sprintf(my_data->file_name,"%s",file_name);
		my_data->nb_file=nb_file;
		
	    }
            fprintf(fp, "%f ", cpt * cpt_scale);
            cpt++;
            my_data->cpt = cpt;
        }

        if (time_abs_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = (float) CurrentTime.tv_sec;
            current_time_msec = (float) CurrentTime.tv_usec;
            current_time = current_time_sec + current_time_msec / 1000000;
            fprintf(fp, "%f ", current_time);
        }
        if (time_sim_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = (float) CurrentTime.tv_sec;
            current_time_msec = (float) CurrentTime.tv_usec;
            time_of_sim_sec = current_time_sec - start_time_sec;
            time_of_sim_msec = current_time_msec - start_time_msec;
            current_time = time_of_sim_sec + time_of_sim_msec / 1000000;
            fprintf(fp, "%f ", current_time);
        }

        for (i = 0; i < N; i++)
        {
            for (j = 0; j < M; j++)
            {
                pos = i + j * N;
                neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
                if (t_sortie == 0)
                    act = neurone[neur].s;
                else if (t_sortie == 1)
                    act = neurone[neur].s1;
                else if (t_sortie == 2)
                    act = neurone[neur].s2;

                fprintf(fp, "%f ", act);
            }
        }
        fprintf(fp, "\n");
        fclose(fp);
#ifdef DEBUG
        printf("fin ecriture fichier\n");
#endif
    }

#ifdef DEBUG
    printf("fin %s\n", __FUNCTION__);
#endif
}
