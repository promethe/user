/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_save_image_activity.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   For saving all neurone input group activity in a file 
 the name of file must be specified on the link        
 the name of the group is automatically concatened     
  ex:   name.groupXX.NXXX.MXXX.SAVE 
                   
Macro:
-none 

Local variables:
-none

Global variables:
-char GLOB_path_SAVE[512]

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/NN_IO.h>

void function_record_image_activity(int numero)
{
    int i, l, N, M, nofind = 1, gp_input, inc, choice;
    float act = 0.0;
/* static int cpt;*/
    FILE *fp;
    char filename[256], *name;
    int cpt = 0, deb_e;
    int *cpt_data;
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_save_image_activity---------------------\n");
#endif

    if (def_groupe[numero].data == NULL)
    {
        cpt_data = malloc(sizeof(int));
        cpt_data[0] = cpt;
        def_groupe[numero].data = (void *) cpt_data;
    }
    else
    {
        cpt_data = (int *) def_groupe[numero].data;
        cpt = cpt_data[0];
        cpt++;
        cpt_data[0] = cpt;
        def_groupe[numero].data = (void *) cpt_data;
    }

    printf("dans %s, cpt = %d\n", __FUNCTION__, cpt);
 /*---------------------------*/
    /*Looking for the input group */
    for (l = 0; (l < nbre_liaison) && nofind; l++)
        if (liaison[l].arrivee == numero)
            nofind = 0;

    gp_input = liaison[l - 1].depart;
    name = liaison[l - 1].nom;
    sscanf(name, "s%d", &choice);
    if (choice < 0 || choice > 2)
        choice = 2;


 /*---------------------------------------*/
    /* Getting dimensions of the input group */
    N = def_groupe[gp_input].taillex;
    M = def_groupe[gp_input].tailley;
    deb_e = def_groupe[gp_input].premier_ele;
    inc = def_groupe[gp_input].nbre / (N * M);

 /*---------------------------------*/
    /*Create automatically the filename */
/* sprintf(filename,"%s_G%02d_N%03d.txt",name,gp_input,cpt);
 cpt++;
*/
    sprintf(filename, "%s/%s.group%02d.N%03d.M%03d.SAVE", GLOB_path_SAVE,
            name, gp_input, N, M);

 /*--------------*/
    /*Open the file */
    fp = fopen(filename, "a");
    if (fp == NULL)
    {
        printf("Impossible to create file %s\n", filename);
        exit(EXIT_FAILURE);
    }


 /*------------------------------------------------*/
    for (i = deb_e + inc - 1; i < deb_e + def_groupe[gp_input].nbre; i += inc)
    {
        if (choice == 0)
            act = neurone[i].s;
        else if (choice == 1)
            act = neurone[i].s1;
        else if (choice == 2)
            act = neurone[i].s2;
        fprintf(fp, "%f ", act);
    }
    fprintf(fp, "\n");
    fclose(fp);
    printf("fin %s\n", __FUNCTION__);

}
