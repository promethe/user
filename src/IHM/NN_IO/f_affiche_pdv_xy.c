/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \ingroup libIHM
   \defgroup f_affiche_pdv_xy f_affiche_pdv_xy

   \brief Display infos about a point of interest (local view and value of an 
   associated neuron)

   \section Modified
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 20/07/2004

   - author: JC Baccon
   - description: add some comments
   - date: 22/07/2004

   \details
   Cette fonction affiche une ligne reliant un point d'interet avec des
   informations affiches juste en dessous de l'affichage de l'image. Parmis
   les informations affiches il y a l'imagette associee au point d'interet
   et la valeur d'un neurone associe avec cette vue locale (typiquement la
   sortie d'un selective winner ou d'un selective adaptative winner)

   \section Options
   -	X		: 	option sur le lien provenant du groupe codant l'abscisse du
   point d'interet. L'abscisse est code sur un vecteur de meme
   taille que l'image avec un seul neurone a 1.0

   -	Y		: 	option sur le lien provenant du groupe codant l'ordonnee du
   point d'interet. L'ordonnee est code sur un vecteur de meme
   taille que l'image avec un seul neurone a 1.0

   -	r		:	option sur le lien provenant du groupe contenant le rayon
   d'exculsion interieur

   -	R		:	option sur le lien provenant du groupe contenant le rayon
   d'exclusion exterieur

   -	F		:	???

   -	transition_detect:	option sur le lien provenant du groupe de reset.
   L'affichage sera rafraichi lorsque l'entre sera >0.999

   -	image	:	option sur le lien provenant du groupe contenant l'image

   -	color	:	option sur le lien provenant du groupe contenant un code couleur
   sous la forme de 5 neurones dont un seul est a 1.0 a chaque
   instant
   1er		-> blue
   2eme 	-> yellow
   3eme 	-> red
   4eme 	-> green
   5eme 	-> purple

   -	-enable :	option a mettre sur le lien venant d'un groupe avec un seul
   neurone pour activer (>=0.5) ou desactiver (<0.5) l'affichage

   \section Todo
   permetre le choix -PX -PY de l'emplacement de l'affichage dans la fenetre
 */
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>


void function_affiche_pdv_xy(int numero) {
#ifndef AVEUGLE
	int GpeEV, GpeX, GpeY, GpeR, Gper, GpeF, GpeImage, GpeTransition,GpeColor, EnableGpe=-1,
			NbFenetre = 0;
	int i, j /*,nx,ny */, CoordX, CoordY, oldx, oldy;
	int dmin, dmax, rayon_gauss = 0;
	int *Datas;
	float Max, TailleEV, rayon_vision, const_log;
	int nb_max_couleurs = 37;
	int color_file[37];
	char param_link[255];
	char filename_color[255]={0,0,0};
	FILE *color_df;
	int color_from_file=-1;
	char  line[512];
	float rayon;
	char *param = NULL, *param1 = NULL;
	int offsetX = 0, offsetY = 0;
	prom_images_struct *image = NULL, *pt_entree = NULL;
	float max=def_groupe[numero].seuil;
	int i_max=-1;
	GpeColor = -1;


#ifdef TIME_TRACE
	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
	dprints("debut %s\n", __FUNCTION__);

	if (def_groupe[numero].data == NULL)
	{
		Gper = GpeR = GpeEV = GpeX = GpeY = GpeF = GpeImage = GpeTransition = -2;
		for (i = 0; i < nbre_liaison; i++)
		{
			if ((liaison[i].arrivee == numero) && (param1 = strstr(liaison[i].nom, "-I")) != NULL)
			{
				GpeEV = liaison[i].depart;
				NbFenetre = atoi(&param1[2]);
				dprints("parametre image %s NbFenetre = %d \n", param1, NbFenetre);

				if ((param = strstr(liaison[i].nom, "-offX")) != NULL) offsetX = atoi(&param[5]);
				if ((param = strstr(liaison[i].nom, "-offY")) != NULL) offsetY = atoi(&param[5]);
			}
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "X")) GpeX = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "Y")) GpeY = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "r"))	Gper = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "R"))	GpeR = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "F"))	GpeF = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "image"))	GpeImage = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && (strstr(liaison[i].nom, "color")!=NULL))
			{
				GpeColor = liaison[i].depart;
				if(prom_getopt(liaison[i].nom,"-f",param_link) == 2)
				{
					strcpy(filename_color,param_link);
					color_from_file = 1;

				}

			}
			if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "transition_detect")) GpeTransition = liaison[i].depart;
			if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "enable")!=NULL) EnableGpe = liaison[i].depart;
		}
		dprints("GpeX=%d,GpeY=%d,Gper=%d,GpeR=%d,GpeF=%d,GpeImage=%d,GpeTrans=%d\n",GpeX, GpeY, Gper, GpeR, GpeF, GpeImage, GpeTransition);
		if (GpeX == -2 || GpeY == -2 || Gper == -2 || GpeR == -2)
		{
			EXIT_ON_ERROR("Gpe %d , Il faut 5 groups avec <image>, <r>, <R>, <X> et <Y> sur le liens (GpeI, Gper, GpeR,GpeX, GpeY) = (%d, %d, %d,%d,%d)\n", numero, GpeEV, Gper, GpeR, GpeX, GpeY);
		}
		if ((GpeImage != -2) && (GpeTransition == -2))
		{
			EXIT_ON_ERROR("Erreur: Pas de groupe de transition\n");
		}

		if (GpeImage != -2)
		{
			if (def_groupe[GpeImage].ext == NULL)
			{
				kprints("Pas d'ext en amont de f_affiche_image_pdv_xy\n");
				return;
			}
			image =	(prom_images_struct *)calloc_prom_image(((prom_images_struct*) (def_groupe[GpeImage].ext))->image_number,((prom_images_struct*) (def_groupe[GpeImage].ext))->sx,((prom_images_struct*) (def_groupe[GpeImage].ext))->sy,3);
			/* initialisation with band=3 in any case*/

			def_groupe[numero].ext = (prom_images_struct *) image;
			pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;
		}
		for(i=0;i<nb_max_couleurs;i++)
			color_file[i] = 0;
		if(color_from_file==1)
		{

			printf("%s Couleurs peronnalisees par fichier\n",__FUNCTION__);
			color_df = fopen(filename_color,"r");
			if(color_df==NULL)
				EXIT_ON_ERROR("Cannot open file %s in %s",filename_color,__FUNCTION__);
			i=0;
			while((fgets(line,512, color_df) != NULL) && i<nb_max_couleurs)
			{
				sscanf (line,"%d",&color_file[i]);
				i++;
			}
			fclose(color_df);
		}

		def_groupe[numero].data = (void *) malloc((17+nb_max_couleurs) * sizeof(int));
		if (def_groupe[numero].data == NULL)
		{
			EXIT_ON_ERROR("Pas assez de memoire (1)\n Exit in function_affiche_pdv_xy\n\n");
		}

		Datas = (int *) def_groupe[numero].data;
		Datas[0] = NbFenetre;
		Datas[1] = GpeEV;
		Datas[2] = GpeX;
		Datas[3] = GpeY;
		Datas[4] = Gper;
		Datas[5] = GpeR;
		Datas[6] = oldx = -1;
		Datas[7] = oldy = -1;
		Datas[8] = GpeF;
		Datas[10] = GpeImage;
		Datas[11] = GpeTransition;
		Datas[12] = offsetX;
		Datas[13] = offsetY;
		Datas[14] = GpeColor;
		Datas[15] = EnableGpe;
		Datas[16] = color_from_file;
		if(color_from_file==1)
		{
			for(i=0;i<nb_max_couleurs;i++)
				Datas[i+17] = color_file[i];
		}
	}
	else
	{
		Datas = (int *) def_groupe[numero].data;
		NbFenetre = Datas[0];
		GpeEV = Datas[1];
		GpeX = Datas[2];
		GpeY = Datas[3];
		Gper = Datas[4];
		GpeR = Datas[5];
		oldx = Datas[6];
		oldy = Datas[7];
		GpeF = Datas[8];
		GpeImage = Datas[10];
		GpeTransition = Datas[11];
		offsetX = Datas[12];
		offsetY = Datas[13];
		GpeColor = Datas[14];
		EnableGpe = Datas[15];
		color_from_file = Datas[16];
		if(color_from_file==1)
		{
			for(i=0;i<nb_max_couleurs;i++)
				color_file[i] = Datas[i+17];
		}


		if (GpeImage != -2)
		{
			pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;
			image = (prom_images_struct *) def_groupe[numero].ext;
		}
	}

	if(EnableGpe != -1 && neurone[def_groupe[EnableGpe].premier_ele].s < 0.5)
		return;

	TailleEV = (float) def_groupe[GpeEV].taillex;
	dmin = const_log = neurone[def_groupe[Gper].premier_ele].s;
	dmax = rayon_vision = neurone[def_groupe[GpeR].premier_ele].s;

	if (GpeF != -2)
	{
		rayon_gauss = neurone[def_groupe[GpeF].premier_ele].s;
	}
	dprints("dmin=%d\t dmax=%d\n", dmin, dmax);

	/* find the X max */
	CoordX = -1;
	Max = 0.0;
	for (i = def_groupe[GpeX].premier_ele; i < def_groupe[GpeX].premier_ele + def_groupe[GpeX].nbre; i++)
	{
		if (neurone[i].s1 > Max)
		{
			Max = neurone[i].s1;
			CoordX = i - def_groupe[GpeX].premier_ele;
		}
	}

	CoordX += offsetX;

	/* find the Y max */
	CoordY = -1;
	Max = 0.0;
	for (i = def_groupe[GpeY].premier_ele;i < def_groupe[GpeY].premier_ele + def_groupe[GpeY].nbre; i++)
	{
		if (neurone[i].s1 > Max)
		{
			Max = neurone[i].s1;
			CoordY = i - def_groupe[GpeY].premier_ele;
		}
	}
	CoordY += offsetY;

	if (((CoordX - offsetX) != -1) && ((CoordY - offsetY) != -1))
	{
		if(GpeColor != -1)
		{
			/*neurone gagnant (reponse du systeme)*/
			for(i = def_groupe[GpeColor].premier_ele; i < def_groupe[GpeColor].premier_ele + def_groupe[GpeColor].nbre; i++)
			{
				if(neurone[i].s1>max)
				{
					max=neurone[i].s1;
					i_max=i-def_groupe[GpeColor].premier_ele;
				}
			}
		}

		switch (NbFenetre)
		{

		case 1:
			if (oldx != -1)
			{
				/*----------------------------------------*/
				/*Dessin d'un segment + le vieux pt de foc */
				TxPoint pointold, point2;

				pointold.x = oldx + 10;
				pointold.y = oldy + 10;
				point2.x = CoordX + 10;
				point2.y = CoordY + 10;
				/*TxDessinerSegment(&image1,1,pointold,point2,1); */
			}

			if(GpeColor == -1) affiche_pdv(&image1, dmin, dmax, bleu, CoordX, CoordY, (char*)"");
			else
			{
				if(color_from_file==1)
				{
					dprints("color from file\n");
					if(i_max >= 0 )
						affiche_pdv(&image1, dmin, dmax, color_file[i_max%nb_max_couleurs], CoordX, CoordY, (char*)"");
					else
						affiche_pdv(&image1, dmin, dmax, blanc, CoordX, CoordY, (char*)"");
				}
				else
				{
					if(i_max == 0 ) affiche_pdv(&image1, dmin, dmax, bleu, CoordX, CoordY, (char*)"");
					else if(i_max == 1 ) affiche_pdv(&image1, dmin, dmax, jaune, CoordX, CoordY, (char*)"");
					else if(i_max == 2 ) affiche_pdv(&image1, dmin, dmax, rouge, CoordX, CoordY, (char*)"");
					else if(i_max == 3 ) affiche_pdv(&image1, dmin, dmax, vert, CoordX, CoordY, (char*)"");
					else if(i_max == 4 ) affiche_pdv(&image1, dmin, dmax, violet, CoordX, CoordY, (char*)"");
					else {
						affiche_pdv(&image1, dmin, dmax, blanc, CoordX, CoordY, (char*)"");
					}

				}

			}
			if (GpeF != -2)
			{
				affiche_pdv(&image1, 1, rayon_gauss, jaune, CoordX, CoordY,	(char*) "");
			}
			Datas[6] = CoordX;
			Datas[7] = CoordY;
			break;

		case 2:
			if (oldx != -1)
			{
				/*----------------------------------------*/
				/*Dessin d'un segment + le vieux pt de foc */
				TxPoint pointold, point2;

				pointold.x = oldx + 10;
				pointold.y = oldy + 10;
				point2.x = CoordX + 10;
				point2.y = CoordY + 10;
			}

			if (GpeF != -2)
			{
				affiche_pdv(&image2, 1, rayon_gauss, jaune, CoordX, CoordY,	(char*) " ");
			}

			affiche_pdv(&image2, dmin, dmax, bleu, CoordX, CoordY, (char*)" ");

			Datas[6] = CoordX;
			Datas[7] = CoordY;
			break;

		case 0:
			/*no display here*/
			if(GpeColor == -1) affiche_pdv(&image1, dmin, dmax, bleu, CoordX, CoordY, (char*)"");
			else
			{
				/*neurone gagnant (reponse du systeme)*/
				for(i = def_groupe[GpeColor].premier_ele; i < def_groupe[GpeColor].premier_ele + def_groupe[GpeColor].nbre; i++)
				{
					if(neurone[i].s1>max)
					{
						max=neurone[i].s1;
						i_max=i-def_groupe[GpeColor].premier_ele;
					}
				}
				if(i_max == 0 )	affiche_pdv(&image1, dmin, dmax, bleu, CoordX, CoordY, (char*)"");
				else if(i_max == 1 ) affiche_pdv(&image1, dmin, dmax, jaune, CoordX, CoordY, (char*)"");
				else if(i_max == 2 ) affiche_pdv(&image1, dmin, dmax, rouge, CoordX, CoordY, (char*)"");
				else if(i_max == 3 ) affiche_pdv(&image1, dmin, dmax, vert, CoordX, CoordY, (char*)"");
				else if(i_max == 4 ) affiche_pdv(&image1, dmin, dmax, violet, CoordX, CoordY, (char*)"");
				else {
					affiche_pdv(&image1, dmin, dmax, blanc, CoordX, CoordY, (char*)"");
				}
			}
			break;
		default:
			kprints("Numero d'image %d non valide\n\n", NbFenetre);
			break;
		}

		if (GpeImage != -2)
		{
			for (i = 0; (unsigned int)i < image->sx; i++)
			{
				for (j = 0; (unsigned int)j < image->sy; j++)
				{
					if (neurone[def_groupe[GpeTransition].premier_ele].s > 0.999)
					{
						if (pt_entree->nb_band == 3)
						{
							image->images_table[0][(j * image->sx + i) * 3] =	pt_entree->images_table[0][(j * image->sx + i) * 3];
							image->images_table[0][(j * image->sx + i) * 3 +1] = pt_entree->images_table[0][(j * image->sx + i) * 3 + 1];
							image->images_table[0][(j * image->sx + i) * 3 +2] =pt_entree->	images_table[0][(j * image->sx + i) * 3 + 2];
						}
						else if (pt_entree->nb_band == 1) {
							image->images_table[0][(j * image->sx + i) * 3] = pt_entree->images_table[0][(j * image->sx + i)];
							image->images_table[0][(j * image->sx + i) * 3 + 1] = pt_entree->images_table[0][(j * image->sx + i)];
							image->images_table[0][(j * image->sx + i) * 3 + 2] = pt_entree->images_table[0][(j * image->sx + i)];
						}
					}
					rayon = (i - CoordX) * (i - CoordX) + (j - CoordY) * (j -	CoordY);
					rayon = sqrt(rayon);
					if ((rayon > dmax - 1 && rayon < dmax + 1) || (rayon > dmin - 1 && rayon < dmin + 1))
					{
						if(i_max == 0)
						{
							image->images_table[0][(j * image->sx + i) * 3] =	0.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 0.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 255;
						}
						else if(i_max == 1)
						{
							image->images_table[0][(j * image->sx + i) * 3] =	255.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 255.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 0;
						}
						else if(i_max == 2)
						{
							image->images_table[0][(j * image->sx + i) * 3] =	255.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 0.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 0;
						}
						else if(i_max == 3)
						{
							image->images_table[0][(j * image->sx + i) * 3] = 0.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 255.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 0;
						}
						else if(i_max == 4)
						{
							image->images_table[0][(j * image->sx + i) * 3] = 255.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 0.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 255;
						}
						else {
							image->images_table[0][(j * image->sx + i) * 3] = 255.;
							image->images_table[0][(j * image->sx + i) * 3 + 1] = 255.;
							image->images_table[0][(j * image->sx + i) * 3 + 2] = 255.;
						}
					}
				}
			}
		}
	}
	dprints("===============fin de affiche_pdv_xy\n");
#ifdef TIME_TRACE
	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
	if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
	{
		SecondesFunctionTimeTrace =	OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	else
	{
		SecondesFunctionTimeTrace =	OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
		MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	sprintf(MessageFunctionTimeTrace,	"Time in function_affiche_pdv_xy\t%4ld.%06d\n",	SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
	affiche_message(MessageFunctionTimeTrace);
#endif
#else
	(void)numero;
#endif
}
