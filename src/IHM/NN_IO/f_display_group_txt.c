/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Affiche un groupe de neur 1D sur une image Valeurs numeriques (en colonne)      
* -I1 (default) -I2 affiche dans la fenetre image1 ou image2 de Promethe          
* -CW (default) White color -CR Red -CB Bleu -CG Green -CGY gray                  
* -PXval -PYval position x and y of image start printing                          
* -Ttext display the text                                                         
* -S -S1 -S2 sortie .s .s1 .s2                                                    
* -ASTAT display All Statistics (computed on S) and saving 2 files(values +stat): 
*               number of value, max, mean, variance, kurtosis   
    
Macro:
-none 

Local variables:
-none

Global variables:
-char GLOB_path_SAVE[512]

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Global_Var/NN_IO.h>

void function_display_group_txt(int numero)
{
#ifndef AVEUGLE

    float /*max, */ maxs, maxs1, maxs2;
    int taille_entree, groupe_entree = -1, deb2, tmp, inc;
    int color, xmax, l, i, pas =
        14, fen_posx, fen_posy, S, S1, S2, imaxs, imaxs1, imaxs2, ASTAT, nbs;
#ifndef AVEUGLE
    TxPoint point /*,pt */ ;
#endif
    void *fenetre;
    char *name = NULL, st[50], stt[50], stcomment[100], *st2;
    float vals, v2s, sums, sumsqrs, sumfours, means, vars, ks;
    FILE *f = NULL, *ff = NULL;

    printf("------------------------------------\n");
    printf("Function function_display_group_txt\n");


    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == numero)
        {
            l = i;
            name = liaison[i].nom;
            groupe_entree = liaison[i].depart;
            break;
        }
    if (groupe_entree < 0)
        return;

   /*--------*/
    /*Defaults */
    fenetre = &image1;
    color = blanc;
    fen_posx = 0;
    fen_posy = 0;
    /*Give a default name: group xx */
    sprintf(stcomment, "<%d %s>", groupe_entree,
            def_groupe[groupe_entree].nom);
    S = S1 = S2 = 0;

   /*------------------*/
    /*Choice on the link */
   /*------------------*/
    /*Image1 or Image2 */
    if (strstr(name, "-I1") != NULL)
    {
        fenetre = &image1;
    }
    else if (strstr(name, "-I2") != NULL)
    {
        fenetre = &image2;
    }
    /*Color Choice */
    if (strstr(name, "-CW") != NULL)
    {
        color = blanc;
    }
    else if (strstr(name, "-CR") != NULL)
    {
        color = rouge;
    }
    else if (strstr(name, "-CG") != NULL)
    {
        color = vert;
    }
    else if (strstr(name, "-CB") != NULL)
    {
        color = bleu;
    }
    else if (strstr(name, "-CGY") != NULL)
    {
        color = gris;
    }
    /*Position beginning of text display */
    st2 = strstr(name, "-PX");
    if ((st2 != NULL))
        fen_posx = atoi(&st2[3]);
    st2 = strstr(name, "-PY");
    if ((st2 != NULL))
        fen_posy = atoi(&st2[3]);
    /*Text to comment */
    st2 = strstr(name, "-T");
    if ((st2 != NULL))
    {
        i = 0;
        while (st2[2 + i] != '-' && st2[2 + i] != '\0')
        {
            stcomment[i] = st2[2 + i];
            i++;
        }
        stcomment[i] = '\0';
    }
    /*S S1 S2 */
    if (strstr(name, "-S") != NULL)
    {
        S = 1;
    }
    if (strstr(name, "-S1") != NULL)
    {
        S1 = 1;
    }
    if (strstr(name, "-S2") != NULL)
    {
        S2 = 1;
    }
    /*STATistics? */
    ASTAT = 0;
    if (strstr(name, "-ASTAT") != NULL)
    {
        ASTAT = 1;
       /*---------------------------*/
        /*Open 2 files Saving results */
        sprintf(st, "%s/VAL.group%02d.N%03d.M%03d.SAVE", GLOB_path_SAVE,
                groupe_entree, def_groupe[groupe_entree].taillex,
                def_groupe[groupe_entree].tailley);
        f = fopen(st, "a");
        sprintf(st, "%s/STAT.group%02d.N%03d.M%03d.SAVE", GLOB_path_SAVE,
                groupe_entree, def_groupe[groupe_entree].taillex,
                def_groupe[groupe_entree].tailley);
        ff = fopen(st, "a");
    }


    taille_entree = def_groupe[groupe_entree].nbre;
    deb2 = def_groupe[groupe_entree].premier_ele;
    inc =
        def_groupe[groupe_entree].nbre / (def_groupe[groupe_entree].taillex *
                                          def_groupe[groupe_entree].tailley);
    xmax =
        def_groupe[groupe_entree].taillex * def_groupe[groupe_entree].tailley;

   /*---------------------------------------------*/
    /*Recherche du max, on mettra une etoile apres */
    maxs = neurone[deb2 + inc - 1].s;
    maxs1 = neurone[deb2 + inc - 1].s1;
    maxs2 = neurone[deb2 + inc - 1].s2;
    imaxs = 0;
    imaxs1 = 0;
    imaxs2 = 0;
    nbs = 0;
    sums = 0.;
    sumsqrs = 0.;
    sumfours = 0.;
    means = 0.;
    vars = 0.;
    ks = 0.;
    for (i = 0; i < xmax; i++)
    {
        if (ASTAT)
        {
            vals = neurone[deb2 + i * inc + inc - 1].s;
            /*Saving values */
            fprintf(f, "%f %f %f\n", neurone[deb2 + i * inc + inc - 1].s,
                    neurone[deb2 + i * inc + inc - 1].s1,
                    neurone[deb2 + i * inc + inc - 1].s2);

            /*Max */
            if (vals > maxs)
            {
                maxs = neurone[deb2 + i * inc + inc - 1].s;
                imaxs = i;
            }
            /*STAT Only on learned neurons */
            if (neurone[deb2 + i * inc + inc - 1].seuil > 0.9)
            {
                /*Number of values */
                nbs = nbs + 1;
                /*Sum of s!! */
                sums = sums + vals;
                /*sum of square */
                v2s = (vals * vals);
                sumsqrs = sumsqrs + v2s;
                /*sum of fourst */
                sumfours = sumfours + (v2s * v2s);
            }

            /*Storing values of neurones */


        }
        if (!ASTAT && S && (neurone[deb2 + i * inc + inc - 1].s > maxs))
        {
            maxs = neurone[deb2 + i * inc + inc - 1].s;
            imaxs = i;
        }
        if (S1 && (neurone[deb2 + i * inc + inc - 1].s1 > maxs1))
        {
            maxs1 = neurone[deb2 + i * inc + inc - 1].s1;
            imaxs1 = i;
        }
        if (S2 && (neurone[deb2 + i * inc + inc - 1].s2 > maxs2))
        {
            maxs2 = neurone[deb2 + i * inc + inc - 1].s2;
            imaxs2 = i;
        }
    }


    /*xmax ne peut etre plus grand que 40 pour des raisons d'affichage dans la fenetre */
    if (xmax > 35)
        xmax = 35;

   /*-------------------------*/
    /*Effacement zone affichage */
    point.x = fen_posx;
    point.y = fen_posy - pas;
    tmp = 30 + (S + S1 + S2) * 90;
    if ((9 * (int) strlen(stcomment)) > tmp)
        tmp = 9 * (int) strlen(stcomment);
    TxDessinerRectangle(fenetre, gris, TxPlein, point, tmp,
                        (int) (2 + pas * (xmax + 1 + ASTAT * 5)), 1);
    if (ASTAT)
    {
        point.y = fen_posy - pas + 2 + (int) (pas * (xmax + ASTAT));
        TxDessinerRectangle(fenetre, rouge, TxVide, point, tmp,
                            (int) (pas * (ASTAT * 5)), 1);
    }

   /*------------------------*/
    /*Affichage du commentaire */
    point.y = fen_posy;
    TxEcrireChaine(fenetre, color, point, stcomment, NULL);

   /*-----------------------*/
    /*Affichage des activites */
    for (i = 0; i < xmax; i++)
    {
        sprintf(st, "%02d  ", i);
        if (S)
        {
            sprintf(stt, "%f", neurone[deb2 + i * inc + inc - 1].s);
            strcat(st, stt);
            if (i == imaxs)
            {
                /*pt.x=point.x+23;
                   pt.y=fen_posy+ 2 +(int)(pas*i); */
                strcat(st, " * ");
                /*val=(int)(9*strlen(stt));
                   pt.x=pt.x+val;
                   TxDessinerRectangle(fenetre,color,TxVide,pt,val,(int)(pas+1),1); */
            }
            else
                strcat(st, "   ");
        }
        if (S1)
        {
            sprintf(stt, "%f", neurone[deb2 + i * inc + inc - 1].s1);
            strcat(st, stt);
            if (i == imaxs1)
                strcat(st, " * ");
            else
                strcat(st, "   ");
        }
        if (S2)
        {
            sprintf(stt, "%f", neurone[deb2 + i * inc + inc - 1].s2);
            strcat(st, stt);
            if (i == imaxs2)
                strcat(st, " * ");
            else
                strcat(st, "   ");
        }
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);
    }

    if (ASTAT)
    {
        /*Mean */
        means = sums / (float) nbs;
        /*Variance */
        vars = (sumsqrs / (float) nbs) - (means * means);
        /*Kurtosis ]-3;+inf[ */
        ks = (sumfours / (vars * vars)) - 3.;

       /*------------*/
        /*Display stat */
        i = xmax;
        sprintf(st, "STAT nbs  = %d", nbs);
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);

        i++;
        sprintf(st, "STAT maxs = %f", maxs);
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);

        i++;
        sprintf(st, "STAT means= %f", means);
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);

        i++;
        sprintf(st, "STAT vars = %f", vars);
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);

        i++;
        sprintf(st, "STAT kurts = %f", ks);
        point.y = fen_posy + pas * (i + 1);
        TxEcrireChaine(fenetre, color, point, st, NULL);

       /*-------------------*/
        /*Saving results STAT */
        fprintf(ff, "%d %f %f %f %f\n", nbs, maxs, means, vars, ks);
        fclose(f);
        fclose(ff);
    }

    TxFlush(&image1);
    TxFlush(&image2);


#endif
    (void)numero;
}
