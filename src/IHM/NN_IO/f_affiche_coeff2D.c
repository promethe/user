/**
 \ingroup libIHM
 \defgroup f_affiche_coeff2D f_affiche_coeff2D

 \brief Affiche les deux premiers coeffs d'un neurone d'origine comme un point XY sur l'image passée en parametre.

 \section Created
 - author: Nils Beaussé
 - description: création
 - date: 29/05/2014

 \details
 Affiche les deux premiers coeffs d'un neurone d'origine comme un point XY sur l'image passée en parametre, cela permet de represneter les deux premiers axes des points appris par un reseau de neurone si
 Celui-ci fonctionne en distance (exemple : Kohonen, saw, etc...).

 \section Options
 -	-enable :	option a mettre sur le lien venant d'un groupe avec un seul
 neurone pour activer (>=0.5) ou desactiver (<0.5) l'affichage

 */
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <public_tools/Vision.h>

typedef struct data_saw {
  type_liaison *link_vigilence;
  type_liaison *link_learn;
  type_liaison *link_learn_product;
  int neuron_max;
  float sigma;
  float dvp;
  float dvn;
  int dvp_entree;
  int dvn_entree;
  int compet;
  int facteur_compet;
  int mode_seuil;
  int taille;
  float max_d;
  float max;
  int posmax;
  int nbr_neurone_recrute;
  float mmoy;
  float norme;
  int last_recruted;
  int N_entree;
  int** liens;
  int t;
  int def_contin;
} data_saw;

struct timeval tvBegin;

void dessiner_liens(data_saw *mydata, int j, int origine, int no_repre,int scale,int offsetX, int offsetY, int CoordX_depart, int CoordY_depart)
{
  int i,k;
  int neuro_dep = j;
  int neuro_arriv = -1;
  type_coeff* coeff;
  int passe;
  int CoordX_arrive = -1;
  int CoordY_arrive = -1;

  for (i = 0; i < mydata->def_contin; i++)
  {
    if ((mydata->liens[j - def_groupe[origine].premier_ele])[i] > 0) neuro_arriv = (mydata->liens[j - def_groupe[origine].premier_ele])[i];


    coeff=neurone[neuro_arriv].coeff;
    for (k = 0; k < no_repre; k++)
    {
      if (coeff->evolution == 1) coeff = coeff->s;
      else k--;
    }
    passe = 0;
    while (coeff != NULL && passe != 2)
    {
      if (coeff->evolution == 1 && passe == 0)
      {
        CoordX_arrive = (int) (scale - ((neurone[i].coeff->val) * scale) + offsetX);
        passe = 1;
      }
      if (coeff->evolution == 1 && passe == 1)
      {
        CoordY_arrive = (int) (scale - ((neurone[i].coeff->s->val) * scale) + offsetY);
        passe = 2;
      }
      coeff = coeff->s;
    }




  }

}

void new_affiche_coeff2D(int numero)
{
#ifndef AVEUGLE
  int origine = -1, r = 1, R = 8, pt1 = -1, pt2 = -1;
  int scale = 500, i = 0, NoFenetre = 1, offsetX = 10, offsetY = 10, enable = -1, no_repre = 0;
  int* Datas = NULL;
  char* param = NULL;
  gettimeofday(&tvBegin, NULL);

  if (def_groupe[numero].data == NULL)
  {
    for (i = 0; i < nbre_liaison; i++)
    {
      if ((liaison[i].arrivee == numero) && (param = strstr(liaison[i].nom, "-I")) != NULL)
      {
        NoFenetre = atoi(&param[2]);
        if ((param = strstr(liaison[i].nom, "-offX")) != NULL) offsetX = atoi(&param[5]) + offsetX;
        if ((param = strstr(liaison[i].nom, "-offY")) != NULL) offsetY = atoi(&param[5]) + offsetY;
      }
      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "origine") != NULL) origine = liaison[i].depart;
      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "enable") != NULL) enable = liaison[i].depart;
      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "pt1") != NULL) pt1 = liaison[i].depart;
      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "pt2") != NULL) pt2 = liaison[i].depart;
      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "no_repre") != NULL) no_repre = liaison[i].depart;
      if (prom_getopt(liaison[i].nom, "taille_env", param) >= 1)
      {
        scale = atoi(param);
      }
    }

    def_groupe[numero].data = (void *) MANY_ALLOCATIONS(13, int);
    if (def_groupe[numero].data == NULL)
    {
      EXIT_ON_ERROR("Pas assez de memoire (1)\n Exit in function_affiche_pdv_xy\n\n");
    }

    Datas = (int *) def_groupe[numero].data;
    Datas[0] = NoFenetre;
    Datas[1] = origine;
    Datas[2] = enable;
    Datas[3] = r;
    Datas[4] = R;
    Datas[5] = offsetX;
    Datas[6] = offsetY;
    Datas[8] = scale;
    Datas[9] = pt1;
    Datas[10] = pt2;
    Datas[11] = tvBegin.tv_usec + 1000000 * tvBegin.tv_sec;
    Datas[12] = no_repre;
  }
#else
  (void)numero;
#endif
}

void function_affiche_coeff2D(int numero)
{
#ifndef AVEUGLE

  int origine = -2, r = 1, R = 8, i, point1 = -1, point2 = -1,k=-1;
  int scale = 500, NoFenetre = 1, offsetX = 10, offsetY = 10, CoordX = -1, CoordY = -1, passe, no_repre = 0;
  int* Datas = NULL;
  float enable = 0.0;
  struct timeval tvActu;
  type_coeff* coeff;
  TxPoint pt1, pt2;
  data_saw *mydata = NULL;
  int couleur;

  gettimeofday(&tvActu, NULL);

  pt1.x = offsetX;
  pt1.y = offsetY;
  pt2.x = offsetX - 10;
  pt2.y = offsetY - 10;

  Datas = (int *) def_groupe[numero].data;
  if ((tvActu.tv_usec + 1000000 * tvActu.tv_sec) - Datas[11] >= 20000)
  {
    enable = neurone[def_groupe[Datas[2]].premier_ele].s1;
    if (enable > 0.5 || Datas[2] == -1)
    {

      NoFenetre = Datas[0];
      origine = Datas[1];
      r = Datas[3];
      R = Datas[4];
      offsetX = Datas[5];
      offsetY = Datas[6];
      scale = Datas[8];
      point1 = Datas[9];
      point2 = Datas[10];
      Datas[11] = tvActu.tv_usec + 1000000 * tvActu.tv_sec;
      no_repre = Datas[12];
      mydata = (data_saw *) def_groupe[origine].data;

      switch (NoFenetre)
      {

      case 1:
        TxDessinerRectangle(&image1, noir, TxVide, pt2, scale + 20, scale + 20, 10);
        TxDessinerRectangle(&image1, blanc, TxPlein, pt1, scale, scale, 0);
        break;

      case 2:
        TxDessinerRectangle(&image2, noir, TxVide, pt2, scale + 20, scale + 20, 10);
        TxDessinerRectangle(&image2, blanc, TxPlein, pt1, scale, scale, 0);
        break;

      default:
        kprints("Numero d'image %d non valide\n\n", NoFenetre);
        break;
      }

      for (i = def_groupe[origine].premier_ele; i < def_groupe[origine].premier_ele + def_groupe[origine].nbre; i++)
      {
        if (neurone[i].seuil > 0.9)
        {
          coeff = neurone[i].coeff;
          for (k = 0; k < no_repre; k++)
          {
            if (coeff->evolution == 1) coeff = coeff->s;
            else k--;
          }
          passe = 0;
          while (coeff != NULL && passe != 2)
          {
            if (coeff->evolution == 1 && passe == 0)
            {
              CoordX = (int) (scale - ((neurone[i].coeff->val) * scale) + offsetX);
              passe = 1;
            }
            if (coeff->evolution == 1 && passe == 1)
            {
              CoordY = (int) (scale - ((neurone[i].coeff->s->val) * scale) + offsetY);
              passe = 2;
            }
            coeff = coeff->s;
          }

          if ((CoordX >= 0) && (CoordY >= 0))
          {
            if (mydata->neuron_max == i) couleur = vert;
            else couleur = bleu;
            switch (NoFenetre)
            {
            case 1:
              affiche_pdv(&image1, r, R, couleur, CoordX, CoordY, (char*) "");
              break;
            case 2:
              affiche_pdv(&image2, r, R, couleur, CoordX, CoordY, (char*) "");
              break;

            default:
              kprints("Numero d'image %d non valide\n\n", NoFenetre);
              break;
            }
          }
        }
      }

      if (point1 != -1)
      {
        CoordX = (int) (scale - ((neurone[def_groupe[point1].premier_ele].s1) * scale) + offsetX);
        CoordY = (int) (scale - ((neurone[def_groupe[point1].premier_ele + 1].s1) * scale) + offsetX);
        switch (NoFenetre)
        {
        case 1:
          affiche_pdv(&image1, r, R, rouge, CoordX, CoordY, (char*) "");
          break;
        case 2:
          affiche_pdv(&image2, r, R, rouge, CoordX, CoordY, (char*) "");
          break;

        default:
          kprints("Numero d'image %d non valide\n\n", NoFenetre);
          break;
        }
      }
      if (point2 != -1)
      {
        CoordX = (int) (scale - ((neurone[def_groupe[point2].premier_ele].s1) * scale) + offsetX);
        CoordY = (int) (scale - ((neurone[def_groupe[point2].premier_ele + 1].s1) * scale) + offsetX);
        switch (NoFenetre)
        {
        case 1:
          affiche_pdv(&image1, r, R, vert, CoordX, CoordY, (char*) "");
          break;
        case 2:
          affiche_pdv(&image2, r, R, vert, CoordX, CoordY, (char*) "");
          break;

        default:
          kprints("Numero d'image %d non valide\n\n", NoFenetre);
          break;
        }
      }

      switch (NoFenetre)
      {
      case 1:
        if (image1.da != NULL) gtk_widget_queue_draw(image1.da);
        break;
      case 2:
        if (image2.da != NULL) gtk_widget_queue_draw(image2.da);
        break;

        dprints("===============fin de affiche_xy\n");
      }
    }
  }
#else
  (void)numero;
#endif

}

void destroy_affiche_coeff2D(int numero)
{
  free(((int*) def_groupe[numero].data));
  def_groupe[numero].data = NULL;
}
