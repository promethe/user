/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_save_activity.c
\brief 

Author: C.G
Created: XX/XX/XXXX
Modified:
- author: K. PREPIN
- description: specific file creation
- date: 4/08/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Cette fonction enregistre dans un fichier l'activite des neurone du groupe d'entree aucours du temps. 
	Chaque colonne de ce fichier correspond a un neurone et chaque ligne correspond a un releve (un pas de temps).  
	Des colonnes complementaires peuvent etre demande pour indiquer
	le numero de la ligne, le numero de ligne multiplie par un facteur, le temps ecoule depuis 
	le debut de la simulation, ou le temps absolu de la machine.  

	 
	Options sur le lien et valeurs par defaut pour la sauvegarde dans le fichier :
	-nNOM_FICHIER   : Ajout d'un nom specifique en debut du nom de fichier ("NOM_FICHIER.NOM_GROUPE.n�GPE.by.n�f_save_activity.SAVE")
	                Sans l'option -n, le nom du fichier est NOM_GROUPE.n�GPE.by.n�f_save_activity.SAVE.
	-f0             : sauvegarde des activites dans un fichier inactive
	-f, -f1 ou rien : sauvegarde des activites dans un fichier active
	-s0   : sortie desire (0,1 ou 2)
	-c ou -c1       : ecrit la valeur du compteur en premiere colonne (ou deuxieme ou troisieme 
			si -tsim ou tabs sont avant)
	-c0             : n'ecrit pas de colonne compteur
	-c22.5          : ecrit en premiere colonne le compteur mult par 22.5
	-tsim ou -tsim1 : ecrit le temps ecoule depuis le debut de la simulation en premiere colonne 
	(ou deuxieme si -tabs est avant) en fait deux colonnes, une pour les secondes et la suivante pour les 
			millisecondes
	-tsim0          : n'ecrit pas de colonne temps
	-tabs ou tabs1  : ecrit le temps absolu en premiere colonne (-----Attention !----- : necessaire pour pouvoir 
			comparer deux promethes qui interagissent par l'environnement : ex: Osc_arm_cam_couples)
	-tabs0          : n'ecrit pas de colonne temps absolu


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct MyData_f_save_activity
{
    int gp_input;
    int deb_gpe;
    int N;
    int M;
    int inc;
    int type_sortie;
    int file_flag;
    char *file_name;
    char *head_file;
    long start_time_sec;
    long start_time_msec;
    int time_sim_flag;
    int time_abs_flag;
    int cpt;
    float cpt_scale;
} MyData_f_save_activity;


/*#define DEBUG*/
void function_save_activity(int numero)
{
    struct timeval StartTime, CurrentTime;
    int i, l, deb_gpe, N, M, gp_input, inc, t_sortie = 0;
    int file_flag = 1, time_sim_flag = 0, time_abs_flag = 0, cpt = 0;
    float start_time_sec, start_time_msec, time_of_sim_sec =
        0, time_of_sim_msec =
        0, current_time_sec, current_time_msec, current_time = 0;
    float act = 0, cpt_scale = 1.0;
/* static int cpt;*/
    char type_sortie[4], h_file[255] =
        "\0", f_opt[4], cpt_param[10], time_sim[4], time_abs[4], string[255];
    FILE *fp;
    MyData_f_save_activity *my_data = NULL;
    char *file_name = NULL;

#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_save_activity---------------------\n");
#endif

    if (def_groupe[numero].data == NULL)
    {
  /*---------------------------*/
        /*Looking for the input group */
        l = find_input_link(numero, 0);
        gp_input = liaison[l].depart;
/*---------------------------------------*/
        /* Getting dimensions of the input group */

        deb_gpe = def_groupe[gp_input].premier_ele;
        N = def_groupe[gp_input].taillex;
        M = def_groupe[gp_input].tailley;

        inc = def_groupe[gp_input].nbre / (N * M);

 /*---------------------------------*/
        /*Getting option on the input link */

        if (prom_getopt(liaison[l].nom, "n", h_file) > 0)
            strcat(h_file, ".");
        if (prom_getopt(liaison[l].nom, "f", f_opt) > 0)
        {
            if (f_opt[0] == '\0')   /* Test de si f est suivi d'un nombre ou non, si non, le */
                file_flag = 1;  /* fichier sera cree. Ainsi nous avons f equivalent a f1 */
            else
                file_flag = atoi(f_opt);
        }
        if (prom_getopt(liaison[l].nom, "s", type_sortie) == 2)
            t_sortie = atoi(type_sortie);
        if (prom_getopt(liaison[l].nom, "c", cpt_param) > 0)
        {
            if (cpt_param[0] == '\0')   /* idem */
                cpt_scale = 1;
            else
                cpt_scale = (float) (atof(cpt_param));
        }
        if (prom_getopt(liaison[l].nom, "tsim", time_sim) > 0)
        {
            if (time_sim[0] == '\0')
                time_sim_flag = 1;  /* idem */
            else
                time_sim_flag = atoi(time_sim);
        }
        if (prom_getopt(liaison[l].nom, "tabs", time_abs) > 0)
        {
            if (time_abs[0] == '\0')
                time_abs_flag = 1;  /* idem */
            else
                time_abs_flag = atoi(time_abs);
        }

 /*---------------------------*/
        /* Creating destination file */

        if (file_flag == 1)
        {
            sprintf(string, "%s%s.%d.by.%d.SAVE", h_file,
                    def_groupe[gp_input].nom, gp_input, numero);
            file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
            strcpy(file_name, string);
        }

/*--------------------------------------*/
/* Getting start time of the simulation */

        gettimeofday(&StartTime, (void *) NULL);
        start_time_sec = (float) StartTime.tv_sec;
        start_time_msec = (float) StartTime.tv_usec;

 /*----------------------------------------------------------------------------*/
        /* Putting right thing in right place in the MyData_f_save_activity structure */

        my_data = malloc(sizeof(MyData_f_save_activity));
        my_data->gp_input = gp_input;
        my_data->deb_gpe = deb_gpe;
        my_data->N = N;
        my_data->M = M;
        my_data->inc = inc;
        my_data->type_sortie = t_sortie;
        my_data->file_flag = file_flag;
        my_data->file_name = file_name;
        my_data->start_time_sec = start_time_sec;
        my_data->start_time_msec = start_time_msec;
        my_data->time_sim_flag = time_sim_flag;
        my_data->time_abs_flag = time_abs_flag;
        my_data->cpt = cpt;
        my_data->cpt_scale = cpt_scale;
        def_groupe[numero].data = (void *) my_data;

    }
    else
    {
        my_data = (MyData_f_save_activity *) def_groupe[numero].data;
        gp_input = my_data->gp_input;
        deb_gpe = my_data->deb_gpe;
        N = my_data->N;
        M = my_data->M;
        inc = my_data->inc;
        t_sortie = my_data->type_sortie;
        file_flag = my_data->file_flag;
        file_name = my_data->file_name;
        start_time_sec = my_data->start_time_sec;
        start_time_msec = my_data->start_time_msec;
        time_sim_flag = my_data->time_sim_flag;
        time_abs_flag = my_data->time_abs_flag;
        cpt = my_data->cpt;
        cpt_scale = my_data->cpt_scale;
    }

    if (file_name != NULL)
    {
#ifdef DEBUG
        printf("debut ecriture fichier\n");
#endif
        /*Open the file */
        fp = fopen(file_name, "a");
        if (fp == NULL)
        {
            printf
                ("Impossible to create file %s in /prom_user/user/src/NN_Core/Info_Tools/f_save_activity.c\n",
                 file_name);
            exit(EXIT_FAILURE);
        }


 /*------------------------------------------------*/
        /* Saving neurone activity of input group in file */

        if (time_abs_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = (float) CurrentTime.tv_sec;
            current_time_msec = (float) CurrentTime.tv_usec;
            current_time = current_time_sec + current_time_msec / 1000000;
            fprintf(fp, "%f ", current_time);
        }
        if (time_sim_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = (float) CurrentTime.tv_sec;
            current_time_msec = (float) CurrentTime.tv_usec;
            time_of_sim_sec = current_time_sec - start_time_sec;
            time_of_sim_msec = current_time_msec - start_time_msec;
            current_time = time_of_sim_sec + time_of_sim_msec / 1000000;
            fprintf(fp, "%f ", current_time);
        }
        if (cpt_scale > 0)
        {
            fprintf(fp, "%f ", cpt * cpt_scale);
            cpt++;
            my_data->cpt = cpt;
        }
        for (i = deb_gpe + inc - 1; i < deb_gpe + def_groupe[gp_input].nbre;
             i += inc)
        {
            if (t_sortie == 0)
                act = neurone[i].s;
            else if (t_sortie == 1)
                act = neurone[i].s1;
            else if (t_sortie == 2)
                act = neurone[i].s2;
            fprintf(fp, "%f ", act);
        }
        fprintf(fp, "\n");
        fclose(fp);
#ifdef DEBUG
        printf("fin ecriture fichier\n");
#endif
    }
}
