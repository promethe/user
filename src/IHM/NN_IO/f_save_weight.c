/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_save_weight f_save_weight
@brief  Ecrit les poids (coeff) en entree des neurones du groupe amont dans des fichiers
@author K. Prepin
@date 01/09/2004

\section Description
	Cette fonction ecrit les poids (coeff) en entree des neurones du groupe amont dans des fichiers (un fichier par neurone). 
	Chaque colonne de ces fichiers correspond a un poids et chaque ligne correspond a un releve de ce poids.  
	Des colonnes complementaires peuvent etre demande pour ecrire de nouvelles colonnes indiquant 
	le numero de la ligne, le numero de ligne multiplie par un facteur, le temps ecoule depuis 
	le debut de la simulation.  

\section Options
-	-c ou -c1       : ecrit la valeur du compteur en premiere colonne (ou deuxieme ou troisieme si -tsim ou tabs sont avant)
-	-c0             : n'ecrit pas de colonne compteur
-	-c22.5          : ecrit en premiere colonne le compteur mult par 22.5
-	-tsim ou -tsim1 : ecrit le temps ecoule depuis le debut de la simulation en premiere colonne (ou deuxieme si -tabs est avant) en fait deux colonnes, une pour les secondes et la suivante pour les millisecondes
-	-tsim0          : n'ecrit pas de colonne temps
-	-tabs ou tabs1  : ecrit le temps absolu en premiere colonne (-----Attention !----- : necessaire pour pouvoir comparer deux promethes qui interagissent par l'environnement : ex: Osc_arm_cam_couples)
-	-tabs0          : n'ecrit pas de colonne temps absolu
*/

#include <stdio.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct neurone_f_save_weight
{
    char *file_name;
    struct type_coeff *coeff;
} neurone_f_save_weight;

typedef struct MyData_f_save_weight
{
    neurone_f_save_weight *inputfiles;
    int gpe_size;
    long start_time_sec;
    long start_time_msec;
    int time_sim_flag;
    int time_abs_flag;
    int cpt;
    float cpt_scale;
} MyData_f_save_weight;

void function_save_weight(int gpe)
{
    struct timeval StartTime, CurrentTime;
    int i, l, gp_input, gpe_size = -1, premier_ele = -1;
    int time_sim_flag = 0, time_abs_flag = 0, cpt = 0;
    long start_time_sec, start_time_msec, time_of_sim_sec =
        0, time_of_sim_msec = 0, current_time_sec, current_time_msec;
    float cpt_scale = 0.0;
    char cpt_param[10], time_sim[4], time_abs[4], string[255];
    char *file_name;
    FILE *file;
    MyData_f_save_weight *my_data = NULL;
    neurone_f_save_weight *inputfiles = NULL;
    type_coeff *coeff = NULL;
    int evolution = 0;
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_save_weight---------------------\n");
#endif

    if (def_groupe[gpe].data == NULL)
    {
/*--------------------------------------------*/
/*-------- Looking for the input group -------*/
        l = find_input_link(gpe, 0);
        gp_input = liaison[l].depart;

/*--------------------------------------------*/
/*--- Getting start time of the simulation ---*/

        gettimeofday(&StartTime, (void *) NULL);
        start_time_sec = StartTime.tv_sec;
        start_time_msec = StartTime.tv_usec;

/*--------------------------------------------*/
/*----- Getting option on the input link -----*/

        if (prom_getopt(liaison[l].nom, "c", cpt_param) > 0)
        {
            if (cpt_param[0] == '\0')   /* Test de si c est suivi d'un nombre ou non, si non, le compte sera */
                cpt_scale = 1;  /* de 1 en 1. Si oui, le compte sera d'un pas egal au nb indique.    */
            else
                cpt_scale = (float) (atof(cpt_param));
            cpt = 1;
        }
        if (prom_getopt(liaison[l].nom, "tsim", time_sim) > 0)
        {
            if (time_sim[0] == '\0')
                time_sim_flag = 1;  /* idem */
            else
                time_sim_flag = atoi(time_sim);
        }
        if (prom_getopt(liaison[l].nom, "tabs", time_abs) > 0)
        {
            if (time_abs[0] == '\0')
                time_abs_flag = 1;  /* idem */
            else
                time_abs_flag = atoi(time_abs);
        }
        if (prom_getopt(liaison[l].nom, "evolution", string) > 0)
        {
		evolution=1;
        }
/*-----------------------------------------------*/
/*---- Getting dimensions of the input group ----*/

        gpe_size = def_groupe[gp_input].nbre;

        inputfiles = (neurone_f_save_weight *) malloc(gpe_size * sizeof(neurone_f_save_weight));    /* tableau des pointeurs vers les structure contenant les info-fichiers */

/*------------------------------------------------*/
/*- Getting number of input-link of each neurone -*/

        premier_ele = def_groupe[gp_input].premier_ele;

        for (i = 0; i < gpe_size; i++)
        {
            coeff = neurone[premier_ele + i].coeff;

            inputfiles[i].coeff = neurone[premier_ele + i].coeff;


/*---------------------------------------------------*/
/*------------- File name creation ------------------*/

            sprintf(string, "Groupe-%d_Weights-of-%deme-neurone.SAVE",
                    gp_input, i);
            file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
            strcpy(file_name, string);

            inputfiles[i].file_name = file_name;
        }

    /*--------------------------------------*/
        /* Getting start time of the simulation */

        gettimeofday(&StartTime, (void *) NULL);
        start_time_sec = StartTime.tv_sec;
        start_time_msec = StartTime.tv_usec;

 /*----------------------------------------------------------------------------*/
        /* Putting right thing in right place in the MyData_f_save_weight structure */

        my_data = malloc(sizeof(MyData_f_save_weight));
        my_data->inputfiles = inputfiles;
        my_data->gpe_size = gpe_size;
        my_data->start_time_sec = start_time_sec;
        my_data->start_time_msec = start_time_msec;
        my_data->time_sim_flag = time_sim_flag;
        my_data->time_abs_flag = time_abs_flag;
        my_data->cpt = cpt;
        my_data->cpt_scale = cpt_scale;
        def_groupe[gpe].data = (void *) my_data;

    }
    else
    {
        my_data = (MyData_f_save_weight *) def_groupe[gpe].data;
        inputfiles = my_data->inputfiles;
        gpe_size = my_data->gpe_size;
        start_time_sec = my_data->start_time_sec;
        start_time_msec = my_data->start_time_msec;
        time_sim_flag = my_data->time_sim_flag;
        time_abs_flag = my_data->time_abs_flag;
        cpt = my_data->cpt;
        cpt_scale = my_data->cpt_scale;
    }


    for (i = 0; i < gpe_size; i++)
    {
#ifdef DEBUG
        printf("debut ecriture fichier %s\n", inputfiles[i].file);
#endif
    /*--------------*/
        /*Open the file */
        file = fopen(inputfiles[i].file_name, "a");
        if (file == NULL)
        {
            printf
                ("Impossible to create file %s (fonction NN_IO/Info_Tools/f_save_weight.c)\n",
                 inputfiles[i].file_name);
            exit(EXIT_FAILURE);
        }

 /*-----------------------------------------------*/
 /*--- Saving weights of neurone links in file ---*/

        if (time_abs_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = CurrentTime.tv_sec;
            current_time_msec = CurrentTime.tv_usec;
            fprintf(file, "%ld.%ld ", current_time_sec, current_time_msec);
        }
        if (time_sim_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = CurrentTime.tv_sec;
            current_time_msec = CurrentTime.tv_usec;
            time_of_sim_sec = current_time_sec - start_time_sec;
            time_of_sim_msec = current_time_msec - start_time_msec;
            fprintf(file, "%ld.%ld ", time_of_sim_sec, time_of_sim_msec);
        }
        if (cpt_scale > 0)
        {
            fprintf(file, "%f ", cpt * cpt_scale);
            cpt++;
            my_data->cpt = cpt;
        }

        coeff = inputfiles[i].coeff;
        while (coeff != NULL)
        {
            if(evolution==0||coeff->evolution==1)
		    fprintf(file, "%f ", coeff->val);
            coeff = coeff->s;
        }
        fprintf(file, "\n");
        fclose(file);
#ifdef DEBUG
        printf("fin ecriture fichier\n");
#endif
    }
}
