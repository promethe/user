/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_affiche_pdv_info f_affiche_pdv_info

\brief Display infos about a point of interest (local view and value of an 
associated neuron)

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

\section author
- name JC Baccon
- description: add some comments
- date: 22/07/2004

\section Description
	Cette fonction affiche une ligne reliant un point d'int�r�t avec des
	informations affich�s juste en dessous de l'affichage de l'image. Parmis
	les informations affich�s il y a l'imagette associ�e au point d'int�r�t
	et la valeur d'un neurone associ� avec cette vue locale (typiquement la
	sortie d'un selective winner ou d'un selective adaptative winner)

\section Options
-	X		: 	option sur le lien provenant du groupe codant l'abscisse du
				point d'int�r�t. L'abscisse est cod� sur un vecteur de m�me
				taille que l'image avec un seul neurone � 1.0
				
-	Y		: 	option sur le lien provenant du groupe codant l'ordonn�e du
				point d'int�r�t. L'ordonn�e est cod� sur un vecteur de m�me
				taille que l'image avec un seul neurone � 1.0
				
-	land	:	option sur le lien provenant du groupe de cat�gorisation
				(typiquement un selective winner ou un selective adaptative
				winner). Cela permet d'afficher la valeur de sortie du gagnant
				
-	imagette:	option sur le lien provenant du groupe contenant l'imagette
				associ�e avec le point d'int�r�t (typiquement f_rho_theta)
-	-nbcol	:	� mettre sur le lien imagette. Permet de sp�cifier le nombre de
				colonnes pour l'affichage des infos
				
-	azim	:	option sur le lien provenent du groupe contenant l'azimut
				associ� avec le point d'int�r�t (utilis� en navigation avec
				les landmarks)
				
-	transition_detect:	option sur le lien provenant du groupe de reset.
						L'affichage sera rafraichi lorsque l'entr� sera >0.999

-	-i		:	donne le num�ro de la fen�tre o� afficher (1 ou 2)

-	-a		:	ajoute du debug en console


-	-enable :	option � mettre sur le lien venant d'un groupe avec un seul
				neurone pour activer (>=0.5) ou d�sactiver (<0.5) l'affichage

\section Todo
	permetre le choix -PX -PY de l'emplacement de l'affichage dans la fen�tre
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
/*#define DEBUG*/

typedef struct Data_f_affiche_pdv_info
{
    int GpeX;
    int GpeY;
    int GpeLand;
    int GpeAzim;
    int GpeIm;
    int GpeTrans;
    int EnableGpe;
    int interface;
    int ask_display;
    int nbcolaffiche;
    int x_print;
    int y_print;
    int height;
    int width;
    int line;
    int column;
    int nbr_winner_GpeLand;
    int *CoordLand;
    float *ActLand;
} Data_f_affiche_pdv_info;

void new_affiche_pdv_info(int index_of_group)
{
    Data_f_affiche_pdv_info *data=NULL;
    int i, l;
    char param[255];
    int GpeX=-1, GpeY=-1, GpeAzim=-1, GpeLand=-1, GpeIm=-1, GpeTrans=-1, EnableGpe=-1,
        interface=1, ask_display=0, nbcolaffiche=-1, height=20, width=120;
    int nbr_winner_GpeLand;
    int *CoordLand;
    float *ActLand;
   
    dprints("entering\n");

    i = 0;
    l = find_input_link(index_of_group, i);
    while (l != -1)
    {
        if (strcmp(liaison[l].nom, "X") == 0)
        {
            GpeX = liaison[l].depart;
        }
        else if (strcmp(liaison[l].nom, "Y") == 0)
        {
            GpeY = liaison[l].depart;
        }
        else if (strcmp(liaison[l].nom, "azim") == 0)
        {
            GpeAzim = liaison[l].depart;
        }
        else if (strcmp(liaison[l].nom, "land") == 0)
        {
            GpeLand = liaison[l].depart;
        }
        else if (strstr(liaison[l].nom, "imagette") != NULL)
        {
            GpeIm = liaison[l].depart;
            if (prom_getopt(liaison[l].nom, "nbcol", param) == 2)
            {
                nbcolaffiche = atoi(param);
            }
            dprints("GpeIm = %d nbcol=%d %s (%s line %i)\n", GpeIm, nbcolaffiche, __FUNCTION__, __FILE__, __LINE__);
        }
        else if (strcmp(liaison[l].nom, "transition_detect") == 0)
        {
            GpeTrans = liaison[l].depart;
        }
        else
        {
            if (prom_getopt(liaison[l].nom, "i", param) == 2)
            {
                interface = atoi(param);
            }
            if (prom_getopt(liaison[l].nom, "a", param) == 1)
            {
                ask_display = 1;
            }
            if(strstr(liaison[l].nom, "enable") != NULL)
            {
				EnableGpe = liaison[l].depart;
			}
        }
        i++;
        l = find_input_link(index_of_group, i);
    }
    
    if (GpeX<0 || GpeY<0 || GpeLand<0 || GpeIm<0 || GpeTrans<0)
        EXIT_ON_ERROR("Il faut minimum 5 groups avec <LAND>, <X> et <Y> <IMAGETTE> <Trans> sur le liens \n");
    if(GpeAzim<0)
        PRINT_WARNING("Pas de lien <azim>\n");

    /*On calcule nbr_winner: c'est nbre_de_1 si selective_winner et nbre_de_1 <= nbre . sinon c'est 1 */
    if ((def_groupe[GpeLand].nbre_de_1 <= def_groupe[GpeLand].nbre)
        && (def_groupe[GpeLand].type == No_Winner_Selectif))
        nbr_winner_GpeLand = def_groupe[GpeLand].nbre_de_1;
    else if (def_groupe[GpeLand].type == No_Winner_Selectif)
        nbr_winner_GpeLand = def_groupe[GpeLand].nbre;
    else
        nbr_winner_GpeLand = 1;


    dprints("nbr_winner_GpeLand=%d\n", nbr_winner_GpeLand);
    
    /*initialisaiton du tableau des cellules et de leur activity */
    CoordLand = (int *)MANY_ALLOCATIONS(nbr_winner_GpeLand, int);
    ActLand = (float *)MANY_ALLOCATIONS(nbr_winner_GpeLand, float);
    
    height = (nbr_winner_GpeLand + 1) * 10 + 20 + 2*def_groupe[GpeIm].tailley;
    width = def_groupe[GpeIm].taillex>120?def_groupe[GpeIm].taillex:120;
    if(nbcolaffiche < 0)
        nbcolaffiche = (int)def_groupe[GpeX].taillex/width;
    
    data = (Data_f_affiche_pdv_info*)ALLOCATION(Data_f_affiche_pdv_info);
    data->GpeX = GpeX;
    data->GpeY = GpeY;
    data->GpeAzim = GpeAzim;
    data->GpeLand = GpeLand;
    data->GpeIm = GpeIm;
    data->GpeTrans = GpeTrans;
    data->EnableGpe=EnableGpe;
    data->interface = interface;
    data->ask_display = ask_display;
    data->nbcolaffiche = nbcolaffiche;
    data->height = height;
    data->width = width;
    data->line = 0;
    data->column = 0;
    data->nbr_winner_GpeLand = nbr_winner_GpeLand;
    data->CoordLand = CoordLand;
    data->ActLand = ActLand;
    def_groupe[index_of_group].data = (Data_f_affiche_pdv_info *) data;
    
    dprints("GpeIm = %d %s (%s line %i)\n", data->GpeIm, __FUNCTION__, __FILE__, __LINE__);
    
    dprints("exiting\n");
}

void function_affiche_pdv_info(int index_of_group)
{
#ifndef AVEUGLE
    
    
    Data_f_affiche_pdv_info *data=NULL;
    int GpeX=-1, GpeY=-1, GpeAzim=-1, GpeLand=-1, GpeIm=-1, GpeTrans=-1, EnableGpe=-1,
        interface=1, ask_display=0, nbcolaffiche=-1, nbr_winner_GpeLand = 0, 
        x_print=0, y_print=0, width=0, height=0, line=0, column=0;
    int *CoordLand = NULL;
    float *ActLand = NULL;
    
    int test, i, j, ii, jj, CoordX, CoordY, pos = 0, pxlcol, CoordAzim=0, posx=0, posy=0;
    float nmax = 0.0, Max, ActAzim=0;
    char ligne[100], choice;
    TxPoint tmpPoint;
    TxPoint tmpPoint1;
    TxPoint tmpPoint2;
    TxPoint point;
    TxDonneesFenetre *Fenetre = NULL;
    int zoom;

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif


    dprints("entering \n");

    if (def_groupe[index_of_group].data == NULL)
        new_affiche_pdv_info(index_of_group);
        
    data = (Data_f_affiche_pdv_info *) def_groupe[index_of_group].data;
    GpeX = data->GpeX;
    GpeY = data->GpeY;
    GpeAzim = data->GpeAzim;
    GpeLand = data->GpeLand;
    GpeIm = data->GpeIm;
    GpeTrans = data->GpeTrans;
    EnableGpe=data->EnableGpe;
    x_print = data->x_print;
    y_print = data->y_print;
    interface = data->interface;
    ask_display = data->ask_display;
    nbcolaffiche = data->nbcolaffiche;
    nbr_winner_GpeLand = data->nbr_winner_GpeLand;
    CoordLand = data->CoordLand;
    ActLand = data->ActLand;
    height = data->height;
    width = data->width;
    line = data->line;
    column = data->column;
  
	if(EnableGpe != -1 && neurone[def_groupe[EnableGpe].premier_ele].s < 0.5)
		return;

    
    /* find the X max */
    CoordX = -1;
    Max = 0.0;
    
    for (i = def_groupe[GpeX].premier_ele; i < def_groupe[GpeX].premier_ele + def_groupe[GpeX].nbre; i++)
    {
        if (neurone[i].s1 > Max)
        {
            Max = neurone[i].s1;
            CoordX = i - def_groupe[GpeX].premier_ele;
        }
    }
    /* find the Y max */
    CoordY = -1;
    Max = 0.0;
    ii = def_groupe[GpeY].premier_ele;
    jj = ii + def_groupe[GpeY].nbre;
    
    for (i = ii; i < jj; i++)
    {    
        if (neurone[i].s1 > Max)
        {
            Max = neurone[i].s1;
            CoordY = i - def_groupe[GpeY].premier_ele;
        }
    }
    /* find the cell the most active and the other winner */
    dprints("find winner \n");

    /*initialisaiton du tableau des cellules et de leur activity */
    for (j = 0; j < nbr_winner_GpeLand; j++)
    {
        CoordLand[j] = -1;
        ActLand[j] = -1.;
    }
    /*On cree le tableau ordonnee des gagnant */
    ii = def_groupe[GpeLand].premier_ele;
    jj = ii + def_groupe[GpeLand].nbre;
    for (j = 0; j < nbr_winner_GpeLand; j++)
    {
        for (i = ii; i < jj; i++)
        {
            if (neurone[i].s1 > ActLand[j])
            {
                if (j == 0)
                {
                    ActLand[j] = neurone[i].s1;
                    CoordLand[j] = i - ii;
                }
                else if ((neurone[i].s1 < ActLand[j - 1])
                        || (isequal(neurone[i].s1, ActLand[j - 1]) && (CoordLand[j - 1] != i - ii)))
                {
                    ActLand[j] = neurone[i].s1;
                    CoordLand[j] = i - ii;
                }
            }
        }
    }
    
    /* If a Azim group has been specified... */
    if(GpeAzim>=0)
    {
        ActAzim = -1;
        CoordAzim = -1;
        ii = def_groupe[GpeAzim].premier_ele;
        jj = ii + def_groupe[GpeAzim].nbre;

        for (i = ii; i < jj; i++)
        {
            if (neurone[i].s1 > ActAzim)
            {
                ActAzim = neurone[i].s1;
                CoordAzim = i - ii;

            }   
        }
    }
#ifdef DEBUG
    for (j = 0; j < nbr_winner_GpeLand; j++)
    {
        dprints("neurone %d: %f\n", CoordLand[j], ActLand[j]);
    }
    if(GpeAzim>=0)
        dprints("neurone %d: %f\n", CoordAzim, ActAzim);
#endif

    if (interface == 1)
        Fenetre = &image1;
    else
        Fenetre = &image2;

    /*Regarde if transition */
    if (neurone[def_groupe[GpeTrans].premier_ele].s > 0.999)
    {
        line = 0;
        column = 0;
        data->line = line;
        data->column = column;        
    }

    
    if (ask_display == 1)
    {
        fflush(stdin);
        dprints("PDV_INFO:\n");
        for (j = 0; j < nbr_winner_GpeLand; j++)
        {
            dprints("neurone %d: %f\n", CoordLand[j], ActLand[j]);
        }
        dprints("neurone %d: %f\n", CoordAzim, ActAzim);
        dprints("AFFICHAGE (o/n):");
        choice = 'k';
        while (choice != 'o' && choice != 'n')
        {
            fflush(stdin);
            test=scanf("%c", &choice);
            if(test == 0){kprints("Erreur de lecture du choix avec scanf");}
        }
        if (choice != 'o')
            return;
    }
    
    x_print = column*width;
    y_print = def_groupe[GpeY].tailley + line*height;
    
    /*Dessine un rectangle blanc pour effacer les informations precedentes*/
    tmpPoint.x = x_print;
    tmpPoint.y = y_print;
    TxDessinerRectangle(Fenetre, blanc, TxPlein, tmpPoint, width, height, 2);
    
    /*on affiche un point en x,y */
    affiche_pdv(Fenetre, 1, 1, jaune, CoordX, CoordY, (char*)"");
     
     
    /*On cherche le point a partir duquel on ecrit. C'est le precedent_x + dx ou x=0 et precedent_y+dy */
    tmpPoint1.x = CoordX;
    tmpPoint1.y = CoordY;
    tmpPoint2.x = x_print;
    tmpPoint2.y = y_print;
    
    /*dprints("%s CoordX=%d CoordY=%d \n",__FUNCTION__,CoordX,CoordY);*/
    TxDessinerSegment(Fenetre, jaune, tmpPoint1, tmpPoint2, 1);
       

if(GpeAzim>=0)
   {
        sprintf(ligne, "Azimut = %d ", (int)((float)(((float) (CoordAzim * 360)) / def_groupe[GpeAzim].nbre)));
        tmpPoint.x = x_print; 
        tmpPoint.y = y_print + 12 ;
        TxEcrireChaine(Fenetre, noir, tmpPoint, ligne, NULL);
   }
        

    dprints("%s nbr_winner_GpeLand = %d \n",__FUNCTION__,nbr_winner_GpeLand);
    for (j = 1; j < nbr_winner_GpeLand + 1; j++)
    {
        sprintf(ligne, "%3d: %1.4f ", CoordLand[j - 1], ActLand[j - 1]);
        tmpPoint.x = x_print;
        tmpPoint.y = y_print + 12 * (j) + 12;
        TxEcrireChaine(Fenetre, rouge, tmpPoint, ligne, NULL);
    }
  
    nmax = 0.0;
    ii = def_groupe[GpeIm].premier_ele;
    jj = ii + def_groupe[GpeIm].nbre;
    for (i = ii; i < jj; i++)
    {
        if (neurone[i].s >= nmax)
            nmax = neurone[i].s;
    }
    if(nmax < 0.000001)
      nmax = 0.000001;
    
    pos = def_groupe[GpeIm].premier_ele;
    posx = x_print;
    posy = (nbr_winner_GpeLand + 1) * 10 + 10 + y_print;
    zoom=2;
    for (i = 0; i < 2*def_groupe[GpeIm].tailley; i+=2)
    {
        for (j = 0; j < 2*def_groupe[GpeIm].taillex; j+=2)
        {
            ii = posx + j;
            jj = posy + i;
            pxlcol = (int) (neurone[pos].s * (16. / nmax));
            point.x = ii;
            point.y = jj;           
            TxDessinerRectangle(Fenetre, pxlcol, TxPlein, point, zoom, zoom, 2);

            pos++;
        }

    }
    

    /* next column for next time */
    column++;
    
    if(column >= nbcolaffiche)
    {
        line++;
        column = 0;
    }

   TxDisplay(Fenetre);	  
	data->line = line;
    data->column = column;



#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_affiche_pdv_xy\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

#endif
(void) index_of_group;
}

void destroy_affiche_pdv_info(int index_of_group)
{
  Data_f_affiche_pdv_info *data;
  
  
  dprints("entering \n");
  
  data = (Data_f_affiche_pdv_info *)def_groupe[index_of_group].data;
  if(data != NULL)
  {
    if(data->CoordLand != NULL)
      free(data->CoordLand);
    if(data->ActLand != NULL)
      free(data->ActLand);
    free(def_groupe[index_of_group].data);
    def_groupe[index_of_group].data = NULL;
  }
  
  dprints("exiting\n");
}
