/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_display_group_perso f_display_group_perso
\brief 
 

\section Author 
-Name: xxxxxxxx
-Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  Fonction qui affiche un groupe de neurones 1D sous la forme d'une courbe

   * -image designe le groupe de neurones que l'on veut afficher
   * -s0 ( ou  -s1, ou -s2) selectionne la sortie affichee (par defaut sortie s1)
   * -I1 (ou -I2) affiche dans la fenetre image1 (ou image2) de Promethe (par defaut fenetre image1)
   * -PXx (et/ou -PYy) modifie la position en X (et/ou en Y) dans la fenetre (par defaut x=0, y=0)
   * -SXx (et/ou -SYy) modifie la taille en X (et/ou en Y) de l'affichage (par defaut taille_x=100, taille_y=100)
   * -DEL efface l'affichage precedent
   * -NEG active l'affichage des valeurs negatives
   * -C selectionne la couleur de la courbe (-CR : rouge ; -CG : vert ; -CB : bleu ; -CY : jaune) (par defaut bleu)
   * -M selectionne la valeur max affichable (toute valeur superieure en valeur absolue ne sera pas affichee) ; Dans le cas ou l'option -M n'est pas utilisee (cas par defaut) la valeur max affichable correspond a la valeur max (en valeur absolue) du groupe de neurones


\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_display_group_perso(int numero)
{
#ifndef AVEUGLE
    float val_cour, valx1, valx2, valy1, valy2, max = -1.0, min = 0.0;
    int l = 0, i = 0;
    int groupe_entree = -1, deb, taille_entree, fen_posx = 0, fen_posy =
        0, fen_taillex = 100, fen_tailley = 100, mode_neg =
        0, mode_del = 0, sortie = 1;
    int color = bleu;

    TxPoint point, point2, point3;

    void *fenetre = &image1;
    char param[255];

    l = find_input_link(numero, i);
    while (l != -1)
    {
        if (prom_getopt(liaison[l].nom, "image", param) >= 1)
            groupe_entree = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "s", param) == 2)
            sortie = atoi(param);

        if (prom_getopt(liaison[l].nom, "I1", param) >= 1)
            fenetre = &image1;

        if (prom_getopt(liaison[l].nom, "I2", param) >= 1)
            fenetre = &image2;

        if (prom_getopt(liaison[l].nom, "PX", param) == 2)
            fen_posx = atoi(param);

        if (prom_getopt(liaison[l].nom, "PY", param) == 2)
            fen_posy = atoi(param);

        if (prom_getopt(liaison[l].nom, "SX", param) == 2)
            fen_taillex = atoi(param);

        if (prom_getopt(liaison[l].nom, "SY", param) == 2)
            fen_tailley = atoi(param);

        if (prom_getopt(liaison[l].nom, "M", param) == 2)
            max = atof(param);

        if (prom_getopt(liaison[l].nom, "NEG", param) >= 1)
            mode_neg = 1;

        if (prom_getopt(liaison[l].nom, "DEL", param) >= 1)
            mode_del = 1;

        if (prom_getopt(liaison[l].nom, "CR", param) >= 1)
            color = rouge;
        else if (prom_getopt(liaison[l].nom, "CG", param) >= 1)
            color = vert;
        else if (prom_getopt(liaison[l].nom, "CB", param) >= 1)
            color = bleu;
        else if (prom_getopt(liaison[l].nom, "CY", param) >= 1)
            color = jaune;

        i++;
        l = find_input_link(numero, i);
    }

    deb = def_groupe[groupe_entree].premier_ele;
    taille_entree = def_groupe[groupe_entree].nbre;

    if (mode_del == 1)
    {
        point.x = fen_posx;
        point.y = fen_posy;
        TxDessinerRectangle(fenetre, blanc, TxPlein, point,
                            (int) (fen_taillex + 1),
                            (int) ((1 + mode_neg) * fen_tailley + 1), 1);
    }

    /* Affichage du cadre */
    point.x = fen_posx;
    point.y = fen_posy;

    point2.x = fen_posx + fen_taillex;
    point2.y = fen_posy;

    TxDessinerSegment(fenetre, noir, point, point2, 1);

    point.x = fen_posx;
    point.y = fen_posy + fen_tailley;

    point2.x = fen_posx + fen_taillex;
    point2.y = fen_posy + fen_tailley;

    TxDessinerSegment(fenetre, noir, point, point2, 1);

    point.x = fen_posx;
    point.y = fen_posy + (1 + mode_neg) * fen_tailley;

    point2.x = fen_posx + fen_taillex;
    point2.y = fen_posy + (1 + mode_neg) * fen_tailley;

    TxDessinerSegment(fenetre, noir, point, point2, 1);

    point.x = fen_posx;
    point.y = fen_posy;

    point2.x = fen_posx;
    point2.y = fen_posy + (1 + mode_neg) * fen_tailley;

    TxDessinerSegment(fenetre, noir, point, point2, 1);

    point.x = fen_posx + fen_taillex;
    point.y = fen_posy;

    point2.x = fen_posx + fen_taillex;
    point2.y = fen_posy + (1 + mode_neg) * fen_tailley;

    TxDessinerSegment(fenetre, noir, point, point2, 1);

    /* Affichage de la courbe */
    if (max <= 0.0)
    {
        if (sortie == 0)
            val_cour = neurone[deb].s;
        else if (sortie == 1)
            val_cour = neurone[deb].s1;
        else                    /* sortie == 2 */
            val_cour = neurone[deb].s2;

        if (val_cour >= 0.0)
            max = val_cour;
        else
            max = -val_cour;

        for (i = 1; i < taille_entree; i++)
        {
            if (sortie == 0)
                val_cour = neurone[deb + i].s;
            else if (sortie == 1)
                val_cour = neurone[deb + i].s1;
            else                /* sortie == 2 */
                val_cour = neurone[deb + i].s2;

            if (val_cour > max)
                max = val_cour;
            else if (-val_cour > max)
                max = -val_cour;
        }
    }

    if (mode_neg == 1)
        min = -max;

    for (i = 0; i < taille_entree - 1; i++)
    {
        valx1 = (float) i / taille_entree;
        valx2 = (float) (i + 1) / taille_entree;

        if (sortie == 0)
            val_cour = neurone[deb + i].s;
        else if (sortie == 1)
            val_cour = neurone[deb + i].s1;
        else                    /* sortie == 2 */
            val_cour = neurone[deb + i].s2;

        valy1 = val_cour;

        if (sortie == 0)
            val_cour = neurone[deb + i + 1].s;
        else if (sortie == 1)
            val_cour = neurone[deb + i + 1].s1;
        else                    /* sortie == 2 */
            val_cour = neurone[deb + i + 1].s2;

        valy2 = val_cour;

        if (valy1 > max)
            valy1 = max;
        if (valy1 < min)
            valy1 = min;

        if (valy2 > max)
            valy2 = max;
        if (valy2 < min)
            valy2 = min;

        point.x = (int) (fen_posx + valx1 * fen_taillex);
        point.y = (int) (fen_posy + ((max - valy1) / max) * fen_tailley);

        point2.x = (int) (fen_posx + valx2 * fen_taillex);
        point2.y = (int) (fen_posy + ((max - valy1) / max) * fen_tailley);

        point3.x = (int) (fen_posx + valx2 * fen_taillex);
        point3.y = (int) (fen_posy + ((max - valy2) / max) * fen_tailley);

        if (point.x >= 0 && point.y >= 0 && point2.x >= 0 && point2.y >= 0)
            TxDessinerSegment(fenetre, color, point, point2, 1);

        if (point2.x >= 0 && point2.y >= 0 && point3.x >= 0 && point3.y >= 0)
            TxDessinerSegment(fenetre, color, point2, point3, 1);
    }

    valx1 = (float) i / taille_entree;
    valx2 = (float) (i + 1) / taille_entree;

    if (sortie == 0)
        val_cour = neurone[deb + i].s;
    else if (sortie == 1)
        val_cour = neurone[deb + i].s1;
    else                        /* sortie == 2 */
        val_cour = neurone[deb + i].s2;

    valy1 = val_cour;

    if (sortie == 0)
        val_cour = neurone[deb].s;
    else if (sortie == 1)
        val_cour = neurone[deb].s1;
    else                        /* sortie == 2 */
        val_cour = neurone[deb].s2;

    valy2 = val_cour;

    if (valy1 > max)
        valy1 = max;
    if (valy1 < min)
        valy1 = min;

    if (valy2 > max)
        valy2 = max;
    if (valy2 < min)
        valy2 = min;

    point.x = (int) (fen_posx + valx1 * fen_taillex);
    point.y = (int) (fen_posy + ((max - valy1) / max) * fen_tailley);

    point2.x = (int) (fen_posx + valx2 * fen_taillex);
    point2.y = (int) (fen_posy + ((max - valy1) / max) * fen_tailley);

    if (point.x >= 0 && point.y >= 0 && point2.x >= 0 && point2.y >= 0)
        TxDessinerSegment(fenetre, color, point, point2, 1);

    TxFlush(&image1);
    TxFlush(&image2);

#endif
(void) numero;
}
