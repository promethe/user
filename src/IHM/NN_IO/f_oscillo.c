/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_oscillo f_oscillo
\brief 
 

\section Author
- Gaussier
- Created: XX/XX/XXXX
\section Modified
- author: P. Gaussier
- description: oscilloscope pour afficher les activites des neurones
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  active de maniere constante les neurones d'un groupe 
  si un seul neurone son activite est de 1 
  si plusieurs neurones alors on a une rampe d'activite 

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <math.h>
#include <stdlib.h>
#include <libx.h>
#include <Global_Var/NN_Core.h>
#include <Kernel_Function/find_input_link.h>
 
/*---------------------------------------------------------------------------------*/
/* affiche dans image 1 le debug du neurone dont le nymero est indique sur le lien */
/*---------------------------------------------------------------------------------*/

#define DILATATION 20. 

void function_oscillo(int numero)
 {
#ifndef AVEUGLE
   int taille_entree;
   int groupe_entree=-1;
   int i,p;
   int deb2;
   int graduation;
   TxPoint point;

   i=find_input_link(numero,0);
   p=atoi(liaison[i].nom);
   if(i<0) return;
   groupe_entree=liaison[i].depart;
   taille_entree=def_groupe[groupe_entree].nbre;

   cprints("Oscillo:  gpe entree = %d neurone %d \n",groupe_entree,p);

   if(p<0 || p>taille_entree)
     {
       EXIT_ON_ERROR("ERROR: the neuron number %d should belong to [0,%d[\n",p,taille_entree);
       exit(0);
     }


   deb2=def_groupe[groupe_entree].premier_ele;


   /*printf("sonde sur le neurone = %d du groupe %d\n",p,groupe_entree);*/

  /* printf("activity of the neuron %d = %f \n",p,neurone[p+deb2].s2); */
  /* DessinerPoint(&image2,couleur,(int) global_temps,100-(int) (neurone[p].s2*90.)); */

  point.x=(int) (global_temps*eps*DILATATION);
  point.y=500-(int) (neurone[p+deb2].s*480.);
   TxDessinerRectangle(&image1,bleu,TxPlein,point,1,1,1);

  graduation=point.x/10;
  if(graduation*10==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,10,1);         
      }
  graduation=point.x/100;
  if(graduation*100==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,20,1);         
      }


/*  point.y=500-(int) (neurone[p+deb2].s*480.);
  TxDessinerRectangle(&image1,rouge,TxPlein,point,1,1,1); */
#endif
(void) numero;
}
/*---------------------------------------------------------------------------------*/


void function_oscillo_c(int numero)
 {
#ifndef AVEUGLE
   int taille_entree;
   int groupe_entree=-1;
   int i,p,c=0;
   int deb2;
   int graduation;
   TxPoint point;

   i=find_input_link(numero,0);
   p=atoi(liaison[i].nom);
   if(i<0) return;
   groupe_entree=liaison[i].depart;
   taille_entree=def_groupe[groupe_entree].nbre;  


   if(p<0 || p>taille_entree)
     {
       EXIT_ON_ERROR("ERROR: the neuron number %d should belong to [0,%d[\n",p,taille_entree);
       exit(0);
     }
   if(c<0 || c>19)
     {
       EXIT_ON_ERROR("ERROR: the color number %d should belong to [0,%d[\n",p,19);
       exit(0);
     }


   deb2=def_groupe[groupe_entree].premier_ele;


   /*printf("sonde sur le neurone = %d du groupe %d\n",p,groupe_entree);*/

  /* printf("activity of the neuron %d = %f \n",p,neurone[p+deb2].s2); */
  /* DessinerPoint(&image2,couleur,(int) global_temps,100-(int) (neurone[p].s2*90.)); */

  point.x=(int) (global_temps*eps*DILATATION);
  point.y=500-(int) (neurone[p+deb2].s1*480.);
   TxDessinerRectangle(&image1,c,TxPlein,point,1,1,1);

  graduation=point.x/10;
  if(graduation*10==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,10,1);         
      }
  graduation=point.x/100;
  if(graduation*100==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,20,1);         
      }


/*  point.y=500-(int) (neurone[p+deb2].s*480.);
  TxDessinerRectangle(&image1,rouge,TxPlein,point,1,1,1); */
#endif
(void) numero;
}


/*---------------------------------------------------------------------------------*/
/* affiche dans image 1 le debug de tous le groupe, le numero correspond a la 1ere */
/* couleur utilisee pour l'affichage                                               */
/*---------------------------------------------------------------------------------*/


void function_oscillo2(int numero)
 {
#ifndef AVEUGLE                          
   int taille_entree;
   int groupe_entree=-1;
   int i,p;
   int deb2;
   int graduation;
   TxPoint point;
  
   i=find_input_link(numero,0);
   p=atoi(liaison[i].nom);
   if(i<0) return;
   groupe_entree=liaison[i].depart;
   taille_entree=def_groupe[groupe_entree].nbre;  

   if(p<0 || p>19)
     {
       EXIT_ON_ERROR("ERROR: the color number %d should belong to [0,%d[\n",p,19);
       exit(0);
     }


   deb2=def_groupe[groupe_entree].premier_ele;


   /*printf("sonde sur le groupe %d\n",groupe_entree);*/

  
  point.x=(int) (global_temps*eps*DILATATION);

  for(i=0;i<taille_entree;i++)
    {
      point.y=500-(int) (neurone[i+deb2].s1*480.);
      TxDessinerRectangle(&image1,(p+i)%19,TxPlein,point,1,1,1);
    }

  graduation=point.x/10;
  if(graduation*10==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,10,1);         
      }
  graduation=point.x/100;
  if(graduation*100==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&image1,vert,TxPlein,point,1,20,1);         
      }


  point.y=500-(int) (0.8*480.);
  TxDessinerRectangle(&image1,vert,TxPlein,point,1,1,1); 
#endif
(void) numero;
}

/*---------------------------------------------------------------------------------*/
/* affiche dans image 1 le debug de tous le groupe, le numero correspond a la 1ere */
/* couleur utilisee pour l'affichage                                               */
/*---------------------------------------------------------------------------------*/


void function_oscillo_group(int numero)
 {
#ifndef AVEUGLE
   int groupe_entree=-1;
   int i,p;
   int deb,nbre,nbre2,increment,n;
   int graduation;
   TxPoint point;
   int taille_y;

   i=find_input_link(numero,0);
   p=atoi(liaison[i].nom);
   if(i<0) return;
   groupe_entree=liaison[i].depart;

   taille_y=def_groupe[groupe_entree].tailley;

   if(p<0 || p>taille_y)       EXIT_ON_GROUP_ERROR(numero, "the color number %d should belong to [0,%d[\n",p,taille_y);

  deb=def_groupe[groupe_entree].premier_ele;
  nbre=def_groupe[groupe_entree].nbre;
  nbre2=def_groupe[groupe_entree].taillex*def_groupe[groupe_entree].tailley;
  increment=nbre/nbre2;

   /*printf("sonde sur le groupe %d\n",groupe_entree);*/

  
  point.x=(int) (global_temps*eps*DILATATION);

  for(n=deb+increment-1;n<deb+nbre;n=n+increment)
    {
      point.y=500-(int) (neurone[n].s1*480.);
      TxDessinerRectangle(&fenetre2,15+(n%7),TxPlein,point,1,1,1);
    }

  graduation=point.x/10;
  if(graduation*10==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&fenetre2,vert,TxPlein,point,1,10,1);         
      }
  graduation=point.x/100;
  if(graduation*100==point.x)
      {
        point.y=500;
        TxDessinerRectangle(&fenetre2,vert,TxPlein,point,1,20,1);         
      }


  point.y=500-(int) (0.8*480.);
  TxDessinerRectangle(&fenetre2,vert,TxPlein,point,1,1,1); 
#endif
(void) numero;
}

