/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \ingroup libIHM
   \defgroup f_affiche_pdv_xy f_affiche_pdv_xy

   \brief Display infos about a point of interest (local view and value of an 
   associated neuron)

   \section Modified
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 20/07/2004

   - author: JC Baccon
   - description: add some comments
   - date: 22/07/2004

   \details
   Cette fonction affiche une ligne reliant un point d'interet avec des
   informations affiches juste en dessous de l'affichage de l'image. Parmis
   les informations affiches il y a l'imagette associee au point d'interet
   et la valeur d'un neurone associe avec cette vue locale (typiquement la
   sortie d'un selective winner ou d'un selective adaptative winner)

   \section Options
   -	X		: 	option sur le lien provenant du groupe codant l'abscisse du
   point d'interet. L'abscisse est code sur un vecteur de meme
   taille que l'image avec un seul neurone a 1.0
				
   -	Y		: 	option sur le lien provenant du groupe codant l'ordonnee du
   point d'interet. L'ordonnee est code sur un vecteur de meme
   taille que l'image avec un seul neurone a 1.0
				
   -	r		:	option sur le lien provenant du groupe contenant le rayon
   d'exculsion interieur
				
   -	R		:	option sur le lien provenant du groupe contenant le rayon
   d'exclusion exterieur
				
   -	F		:	???
				
   -	transition_detect:	option sur le lien provenant du groupe de reset.
   L'affichage sera rafraichi lorsque l'entre sera >0.999

   -	image	:	option sur le lien provenant du groupe contenant l'image

   -	color	:	option sur le lien provenant du groupe contenant un code couleur
   sous la forme de 5 neurones dont un seul est a 1.0 a chaque
   instant
   1er		-> blue
   2eme 	-> yellow
   3eme 	-> red
   4eme 	-> green
   5eme 	-> purple

   -	-enable :	option a mettre sur le lien venant d'un groupe avec un seul
   neurone pour activer (>=0.5) ou desactiver (<0.5) l'affichage

   \section Todo
   permetre le choix -PX -PY de l'emplacement de l'affichage dans la fenetre
*/
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <public_tools/Vision.h>
struct timeval tvBegin;

void new_affiche_xy(int numero)
{
#ifndef AVEUGLE
  int X=-2,Y=-2,r=1,R=8,EnableGpe=-1;
  int scale=500,i=0,NoFenetre=1,offsetX=10,offsetY=10;
  int* Datas=NULL;
  char* param=NULL;
  TxPoint pt1,pt2;
  gettimeofday(&tvBegin, NULL);

	if (def_groupe[numero].data == NULL)
	  {
	    for (i = 0; i < nbre_liaison; i++)
	    {
	      if ((liaison[i].arrivee == numero) && (param = strstr(liaison[i].nom, "-I")) != NULL)
	      {
	    	  NoFenetre = atoi(&param[2]);
	    	  if ((param = strstr(liaison[i].nom, "-offX")) != NULL) offsetX = atoi(&param[5])+offsetX;
	    	  if ((param = strstr(liaison[i].nom, "-offY")) != NULL) offsetY = atoi(&param[5])+offsetY;
	      }
	      if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "X")) X = liaison[i].depart;
	      if ((liaison[i].arrivee == numero) && !strcmp(liaison[i].nom, "Y")) Y = liaison[i].depart;
	      if ((liaison[i].arrivee == numero) && strstr(liaison[i].nom, "enable")!=NULL) EnableGpe = liaison[i].depart;
	      if (prom_getopt(liaison[i].nom, "taille_env", param) >= 1)
	      	 {
	      		 scale=atoi( param );
	      	 }
	    }
	    if (X == -2 || Y == -2)
	    {
	      EXIT_ON_ERROR("Il faut 2 groupes avec au moins X et Y en entree");
	    }

	    def_groupe[numero].data = (void *) MANY_ALLOCATIONS(12,int);
	    if (def_groupe[numero].data == NULL)
	    {
	      EXIT_ON_ERROR("Pas assez de memoire (1)\n Exit in function_affiche_pdv_xy\n\n");
	    }

	    Datas = (int *) def_groupe[numero].data;
	    Datas[0] = NoFenetre;
	    Datas[1] = X;
	    Datas[2] = Y;
	    Datas[3] = r;
	    Datas[4] = R;
	    Datas[5] = offsetX;
	    Datas[6] = offsetY;
	    Datas[7] = EnableGpe;
	    Datas[8] = scale;
	    Datas[9]= (int)(scale-((neurone[def_groupe[X].premier_ele].s1)*scale)+offsetX)-R/2;
	    Datas[10]=(int)(scale-((neurone[def_groupe[Y].premier_ele].s1)*scale)+offsetY)-R/2;
	    Datas[11]=tvBegin.tv_usec+1000000*tvBegin.tv_sec;

		pt1.x = offsetX;
		pt1.y = offsetY;
		pt2.x = offsetX-10;
		pt2.y = offsetY-10;

	    switch (NoFenetre)
	        {

	        case 1:
	          TxDessinerRectangle(&image1, noir, TxVide, pt2, scale+20, scale+20, 10);
	          TxDessinerRectangle(&image1, blanc, TxPlein, pt1, scale, scale, 0);
	          break;

	        case 2:
	          TxDessinerRectangle(&image2, noir, TxVide, pt2, scale+20, scale+20, 10);
	          TxDessinerRectangle(&image2, blanc, TxPlein, pt1, scale, scale, 0);
	          break;

	        default:
	          kprints("Numero d'image %d non valide\n\n", NoFenetre);
	          break;
	        }



	  }
#else
(void)numero;
#endif
}



void function_affiche_xy(int numero) {
#ifndef AVEUGLE
  int X=-2,Y=-2,r=1,R=8,EnableGpe=-1;
  int scale=500,NoFenetre=1,offsetX=10,offsetY=10,CoordX,CoordY;
  int* Datas=NULL;
  TxPoint ptold;
  struct timeval tvActu;
  gettimeofday(&tvActu, NULL);


    Datas = (int *) def_groupe[numero].data;
    if ((tvActu.tv_usec+1000000*tvActu.tv_sec)-Datas[11]>=50000)
    {
    NoFenetre = Datas[0];
    X = Datas[1];
    Y = Datas[2];
    r = Datas[3];
    R = Datas[4];
    offsetX = Datas[5];
    offsetY = Datas[6];
    EnableGpe = Datas[7];
    scale=Datas[8];
	ptold.x=Datas[9];
	ptold.y=Datas[10];
	Datas[11]=tvActu.tv_usec+1000000*tvActu.tv_sec;
	//pt2.x = offsetX-10;
	//pt2.y = offsetY-10;

  if(EnableGpe != -1 && neurone[def_groupe[EnableGpe].premier_ele].s < def_groupe[numero].seuil)
    return;

  CoordX = (int)(scale-((neurone[def_groupe[X].premier_ele].s1)*scale)+offsetX);
  CoordY = (int)(scale-((neurone[def_groupe[Y].premier_ele].s1)*scale)+offsetY);

  if ((CoordX >= 0) && (CoordY >= 0))
  {

    switch (NoFenetre)
    {

    case 1:
      TxDessinerRectangle(&image1, blanc, TxPlein, ptold, 2*R+1, 2*R+1, 2);
      //TxDessinerRectangle(&image1, noir, TxVide, pt2, scale+20, scale+20, 10);
      affiche_pdv(&image1, r, R, bleu, CoordX, CoordY, (char*)"");


      break;

    case 2:
      TxDessinerRectangle(&image2, blanc, TxPlein, ptold, 2*R+1, 2*R+1, 2);
     // TxDessinerRectangle(&image2, noir, TxVide, pt2, scale+20, scale+20, 10);
      affiche_pdv(&image2, r, R, bleu, CoordX, CoordY, (char*)"");

      break;

    default:
      kprints("Numero d'image %d non valide\n\n", NoFenetre);
      break;
    }

    Datas[9] = CoordX-R;
    Datas[10] = CoordY-R;

  }
  dprints("===============fin de affiche_xy\n");
    }
#else
  (void)numero;
#endif
}
void destroy_affiche_xy(int numero)
{
	free(((int*) def_groupe[numero].data));
	def_groupe[numero].data = NULL;
}
