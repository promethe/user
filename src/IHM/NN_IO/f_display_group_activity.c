/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_display_group_activity f_display_group_activity
\ingroup libIHM

\brief Affiche l'activite des neurones du groupe d'entree au cours du temps

Author: G. Merle
Created: 25/07/2011

\section Description
    Cette fonction affiche l'activite des neurones du groupe d'entree au cours
    du temps. C'est un affichage en population (vecteur d'activites, un peu 
    comme le debug de promethe) et non en analogique (superposition de courbes,
    comme f_display_image_activity). Note : suppose l'activite des neurones 
    bornee entre 0 et 1, et projette les valeurs au-dela sur cet intervalle.
    
\section  Options
-    -iX : affiche dans la fenetre image X (X valant 1 ou 2)                   1
-    -mM : nombre M de cycles a memoriser et a afficher                        1
-    -xX -yY : coordonnees (X,Y) de l'affichage dans la fenetre image      (0,0)
-    -wW -hH : taille (W,H) de l'affichage dans la fenetre image       (128,128)
-    -neg : affichage en negatif                                               /
-    -b : dessine une fine bordure autours du graphique
-    -sS : champ considere pour l'activite (0,1 ou 2 pour s,s1 ou s2)          s
-    -rgbRGB : champs consideres pour les canaux R,G et B separement     (s,s,s)

\file
\ingroup f_display_group_activity
 
*/

#ifdef AVEUGLE

void new_display_group_activity(int gpe) {
   (void )gpe;
}
void function_display_group_activity(int gpe) {
   (void)gpe;
}
void destroy_display_group_activity(int gpe) {
   (void)gpe;
}

#else

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <graphic_Tx.h>


typedef float (*key_func)(int neuron);

typedef struct display_data {
    int in;                     /* groupe neurone entree */
    int image;                  /* fenetre image a afficher */
    int x,y,w,h;                /* position et taille de l'affichage */
    size_t mem_size;            /* nombre de cycles a� memoriser et afficher */
    float* mem_data;            /* memoire circulaire des cycles precedents */
    int    mem_time;            /* cycle actuel dans la memoire circulaire */
    int neg;                    /* affichage en negatif */
    int border;                 /* affichage d'une bordure */
    key_func keys[3];           /* fonctions des champs consideres */
} display_data;

static float key_s0(int neuron) {
    return neurone[neuron].s;
}
static float key_s1(int neuron) {
    return neurone[neuron].s1;
}
static float key_s2(int neuron) {
    return neurone[neuron].s2;
}

void new_display_group_activity(int gpe) {
    int i;
    int in;
    int l;
    int image = 1;
    int x = 0, y = 0, w = 128, h = 128;
    int mem_size = 1;
    float* mem_data = NULL;
    int neg = 0, border = 0;
    key_func keys[3] = { key_s0, key_s0, key_s0 };
    char chaine[255];
    display_data *data = NULL;

    if (def_groupe[gpe].data == NULL) {
        /* get the input link */
        l = find_input_link(gpe, 0);
        if (l == -1) {
            PRINT_WARNING("gpe #%s '%s': the group should have at least one input link!\n", 
                def_groupe[gpe].no_name, def_groupe[gpe].nom);
            in = -1;
        } else {
            in = liaison[l].depart;
            if (prom_getopt(liaison[l].nom, "m", chaine) == 2) {
                mem_size = atoi(chaine);
                if (mem_size <= 0) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -m, expected strictly positive value, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, mem_size);
                    mem_size = 1;
                }
            }
            if (prom_getopt(liaison[l].nom, "i", chaine) == 2) {
                image = atoi(chaine);
                if (image != 1 && image != 2) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -i, expected 1 or 2, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, image);
                    image = 1;
                }
            }
            if (prom_getopt(liaison[l].nom, "x", chaine) == 2) {
                x = atoi(chaine);
                if (x < 0) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -x, expected positive value, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, x);
                    x = 0;
                }
            }
            if (prom_getopt(liaison[l].nom, "y", chaine) == 2) {
                y = atoi(chaine);
                if (y < 0) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -y, expected positive value, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, y);
                    y = 0;
                }
            }
            if (prom_getopt(liaison[l].nom, "w", chaine) == 2) {
                w = atoi(chaine);
                if (w <= 0) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -w, expected strictly positive value, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, w);
                    w = 128;
                }
            }
            if (prom_getopt(liaison[l].nom, "h", chaine) == 2) {
                h = atoi(chaine);
                if (h <= 0) {
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -h, expected strictly positive value, got %i\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, h);
                    h = 128;
                }
            }
            if (prom_getopt(liaison[l].nom, "neg", chaine) != 0) {
                neg = 1;
            }
            if (prom_getopt(liaison[l].nom, "b", chaine) != 0) {
                border = 1;
            }
            if (prom_getopt(liaison[l].nom, "s", chaine) == 2) {
                switch (chaine[0]) {
                    case '0':
                    keys[0] = keys[1] = keys[2] = key_s0;
                    break;
                    case '1':
                    keys[0] = keys[1] = keys[2] = key_s1;
                    break;
                    case '2':
                    keys[0] = keys[1] = keys[2] = key_s2;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -s, expected 0, 1 or 2, got %s\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
            }
            if (prom_getopt(liaison[l].nom, "rgb", chaine) == 2) {
                switch (chaine[0]) {
                    case '0':
                    keys[0] = key_s0;
                    break;
                    case '1':
                    keys[0] = key_s1;
                    break;
                    case '2':
                    keys[0] = key_s2;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -rgb, expected something that matches [0-2]{3}, got %s\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
                switch (chaine[1]) {
                    case '0':
                    keys[1] = key_s0;
                    break;
                    case '1':
                    keys[1] = key_s1;
                    break;
                    case '2':
                    keys[1] = key_s2;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -rgb, expected something that matches [0-2]{3}, got %s\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
                switch (chaine[2]) {
                    case '0':
                    keys[2] = key_s0;
                    break;
                    case '1':
                    keys[2] = key_s1;
                    break;
                    case '2':
                    keys[2] = key_s2;
                    break;
                    default:
                    PRINT_WARNING("gpe #%s '%s': invalid value for option -rgb, expected something that matches [0-2]{3}, got %s\n", 
                        def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
                }
            }
        }
        
        def_groupe[gpe].data = data = malloc(sizeof(display_data));
        if (data == NULL) {
            EXIT_ON_ERROR("gpe #%s '%s': malloc failed\n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
        }
        data->in = in;
        data->image = image;
        data->x = x;
        data->y = y;
        data->w = w;
        data->h = h;
        data->neg = neg;
        data->border = border;
        data->mem_size = mem_size;
        data->mem_time = 0;
        if (in != -1) {
            mem_data = calloc(mem_size * 3 * def_groupe[in].taillex * def_groupe[in].tailley, sizeof(float));
            if (mem_data == NULL) {
                in = -1;
                PRINT_WARNING("gpe #%s '%s': could not allocate memory\n", 
                    def_groupe[gpe].no_name, def_groupe[gpe].nom, chaine);
            } else if (data->neg) {
                for (i = 0; i < 3 * mem_size * def_groupe[in].taillex * def_groupe[in].tailley; i += 3) {
                    mem_data[i+0] = 1;
                    mem_data[i+1] = 1;
                    mem_data[i+2] = 1;
                }
            }
        }
        data->mem_data = mem_data;
        data->keys[0] = keys[0];
        data->keys[1] = keys[1];
        data->keys[2] = keys[2];
    }
}

void destroy_display_group_activity(int gpe) {
    if (def_groupe[gpe].data != NULL) {
        if (((display_data*) def_groupe[gpe].data)->mem_data != NULL)
        free(((display_data*) def_groupe[gpe].data)->mem_data);
        free(def_groupe[gpe].data);
        def_groupe[gpe].data = NULL;
    }
}

static float clamp(float x) {
    return x < 0 ? 0 : x > 1 ? 1 : x;
}

void function_display_group_activity(int gpe) {
    int i,j;
    int increment, taille;
    int x2, y2;
    int no;
    TxPoint pos;
    TxCouleur col;
    TxDonneesFenetre* image = NULL;
    display_data* data = def_groupe[gpe].data;

    if (data == NULL) return;
    if (data->in == -1) return;
    
    taille = def_groupe[data->in].taillex * def_groupe[data->in].tailley;
    increment = def_groupe[data->in].nbre / taille;
    
    /* copying current data */
    for (i = 0; i < taille; ++i) {
        data->mem_data[(data->mem_time * taille + i) * 3 + 0] = clamp(data->keys[0](def_groupe[data->in].premier_ele + (i+1) * increment - 1));
        data->mem_data[(data->mem_time * taille + i) * 3 + 1] = clamp(data->keys[1](def_groupe[data->in].premier_ele + (i+1) * increment - 1));
        data->mem_data[(data->mem_time * taille + i) * 3 + 2] = clamp(data->keys[2](def_groupe[data->in].premier_ele + (i+1) * increment - 1));
        if (data->neg) {
            data->mem_data[(data->mem_time * taille + i) * 3 + 0] = 1 - data->mem_data[(data->mem_time * taille + i) * 3 + 0];
            data->mem_data[(data->mem_time * taille + i) * 3 + 1] = 1 - data->mem_data[(data->mem_time * taille + i) * 3 + 1];
            data->mem_data[(data->mem_time * taille + i) * 3 + 2] = 1 - data->mem_data[(data->mem_time * taille + i) * 3 + 2];
        }
    }
    
    data->mem_time++;
    data->mem_time %= data->mem_size;
    
    if (data->image == 1) image = &image1;
    if (data->image == 2) image = &image2;

    /** displaying */
    no = 0;
    for (i = 0; (unsigned int)i < data->mem_size; ++i) {
        pos.x = data->x + (data->w * i) / data->mem_size;
        x2 = data->x + (data->w * (i+1)) / data->mem_size;
        increment = ((i + data->mem_time) % data->mem_size) * taille;
        for (j = 0; j < taille; ++j) {
            pos.y = data->y + (data->h * j) / taille;
            y2 = data->y + (data->h * (j+1)) / taille;
            col.pixel = no++;
            col.red   = (int) (data->mem_data[(increment + j) * 3 + 0] * 0xffff);
            col.green = (int) (data->mem_data[(increment + j) * 3 + 1] * 0xffff);
            col.blue  = (int) (data->mem_data[(increment + j) * 3 + 2] * 0xffff);
            TxDessinerRectangle_color(image, &col, TxPlein, pos, x2 - pos.x, y2 - pos.y, 1);
        }
    }
    if (data->border) {
        pos.x = data->x;
        pos.y = data->y;
        col.red = col.green = col.blue = 32000;
        TxDessinerRectangle_color(image, &col, TxVide, pos, data->w, data->h, 1);
    }
    TxFlush(image);
}

#endif


