/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_init_var_from_config f_init_var_from_config
\brief 
 

\section Author 
-Name: xxxxxxxx
-Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  Mise a jour var globales du config


\section Macro
-none 

\section Local variables
-int GLOB_rayon_de_vision
-float GLOB_const_log
-float GLOB_pente_sigmoide'
-float seuil_max_correlation

\section Global variables
-boolean emission_robot
-char ** Argv

\section Internal Tools
-tools/include/recherche_string_dans_fichier()

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/arguments.h>
#include <Global_Var/NN_IO.h>
#include <Global_Var/IO_Robot.h>

int GLOB_rayon_de_vision = 50.;
float GLOB_const_log = 20.;
float GLOB_pente_sigmoide;
float seuil_max_correlation;

int recherche_string_dans_fichier(FILE * f, char *st)
{
    char *string;
    char stmp[500];
    int test;

  /*------------------------------------*/
    /*Remet le pointer oa debut du fichier */
    rewind(f);

    do
    {
        test=fscanf(f, "%s", stmp);
        if(test == 0){kprints("Erreur lecture du fichier");}
        string = strstr(stmp, st);
        /*test si fin de fichier */
        if (feof(f))
        {
            cprints("Le fichier ne contient pas le(s) mot(s) : %s\n", st);
            /*
               fclose(f);
               exit(0);
             */
            return (0);
        }
    }
    while (string == NULL);

    return (1);
}

void function_init_var_from_config(int numero)
{
    FILE *fconfig;
    char stmp[100];
    int test;

    /* Lire_config(int argc,char* argv[]);  */

    cprints("\nVAR GLOBALES DU CONFIG\n");

    fconfig = fopen(args.config, "r");
    if (fconfig == NULL)
    {
        cprints("impossible de lire le config: %s\n", args.config);
        return;
    }

    printf("...recherche dans le %s\n", args.config);

  /*-----------------------------------------------------*/
    /*Rajouter vos var gloables a initialiser du config ici */
  /*-----------------------------------------------------*/

    /* emission_robot=0; */
    if (recherche_string_dans_fichier(fconfig, (char*)"emission_robot=") == 1)
    {
        /*fscanf(fconfig,"%d",&emission_robot); */
        test=fscanf(fconfig, "%s", stmp);
        if(test == 0){kprints("Erreur lecture du fichier");}
        EMISSION_ROBOT = atoi(stmp);
        cprints("Emission_Robot =%d\n", EMISSION_ROBOT);
    }

    if (recherche_string_dans_fichier(fconfig, (char*)"GLOB_rayon_de_vision=") == 1)
    {
        test=fscanf(fconfig, "%d", &GLOB_rayon_de_vision);
        if(test == 0){kprints("Erreur lecture du fichier");}
        cprints("GLOB_rayon_de_vision =%d\n", GLOB_rayon_de_vision);
    }

    if (recherche_string_dans_fichier(fconfig, (char*)"GLOB_const_log=") == 1)
    {
        test=fscanf(fconfig, "%f", &GLOB_const_log);
        if(test == 0){kprints("Erreur lecture du fichier");}
        cprints("GLOB_const_log =%f\n", GLOB_const_log);
    }

    /*seuil de correlation du groupe PTM2 (new SV-cor) */
    seuil_max_correlation = .8;
    if (recherche_string_dans_fichier(fconfig, (char*)"seuil_max_correlation=") == 1)
    {
        test=fscanf(fconfig, "%f", &seuil_max_correlation);
        if(test == 0){kprints("Erreur lecture du fichier");}
        cprints("seuil_max_correlation =%f\n", seuil_max_correlation);
    }

    GLOB_pente_sigmoide = 3.;
    if (recherche_string_dans_fichier(fconfig, (char*)"GLOB_pente_sigmoide=") == 1)
    {
        test=fscanf(fconfig, "%f", &GLOB_pente_sigmoide);
        if(test == 0){kprints("Erreur lecture du fichier");}
        cprints("GLOB_pente_sigmoide =%f\n", GLOB_pente_sigmoide);
    }

    fclose(fconfig);

    cprints("FIN VAR GLOBALES DU CONFIG\n");

	(void) numero;
}
