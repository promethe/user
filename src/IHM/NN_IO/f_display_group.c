/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_display_group f_display_group
   \ingroup libIHM

   \brief Affiche un groupe de neur 1D sur une image(comme matlab) (courbe)               

   \details

   \section Description 
   Affiche un groupe de neur 1D sur une image(comme matlab) (courbe)               
   - -I1 (default) -I2 affiche dans la fenetre image1 ou image2 de Promethe          
   - -PXx -PYy avec x et y la position dans la fenetre                               
   - -DEL delete area                                                                
   - -CW (default) White color -CR Red -CB Bleu -CG Green -CGY gray                  
   - -TILT : 90 degrees axes tilt
   
   Also see
   f_display_image_activity

   \file
   \ingroup f_display_group
   \brief   Affiche un groupe de neur 1D sur une image(comme matlab) (courbe)               

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 11/08/2004


**/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

void function_display_group(int numero)
{
#ifndef AVEUGLE
  float max, valx1, valx2, valy1, valy2;
  int taille_entree, groupe_entree = -1, deb2, tilt /*,tmp */ ;
  int color, xmax, ymax, imax, i, /*j, */ l, fen_taillex =
    300, fen_tailley = 300, fen_posx = 200, fen_posy = 20;
  TxPoint point, point2;

  void *fenetre;
  char *name = NULL, st[20], *st2;
	
  point2.x=0;
  point2.y=0;


  dprints("------------------------------------\n");
  dprints("Function function_display_group\n");


  for (i = 0; i < nbre_liaison; i++)
    if (liaison[i].arrivee == numero)
    {
      l = i;
      name = liaison[i].nom;
      groupe_entree = liaison[i].depart;
      break;
    }
  if (groupe_entree < 0)
    return;

  /*Defaults */
  fenetre = &image1;
  color = blanc;
  tilt = 0;

  /*------------------*/
  /*Choice on the link */
  /*------------------*/
  /*Image1 or Image2 */
  if (strstr(name, "-I1") != NULL)
  {
    fenetre = &image1;
  }
  else if (strstr(name, "-I2") != NULL)
  {
    fenetre = &image2;
  }
  /*Position in the window */
  st2 = strstr(name, "-PX");
  if (st2 != NULL)
    fen_posx = atoi(&st2[3]);
  st2 = strstr(name, "-PY");
  if (st2 != NULL)
    fen_posy = atoi(&st2[3]);;
  /*Delete area */
  if (strstr(name, "-DEL") != NULL)
  {
    /* TxEffacerAireDessin(fenetre); ca c'est si on efface toute l'image !! */
    point.x = fen_posx;
    if (point.x < 0)
      point.x = 0;
    point.y = fen_posy - 12;
    if (point.y < 0)
      point.y = 0;
    TxDessinerRectangle(fenetre, noir, TxPlein, point,
			(int) (fen_taillex + 150),
			(int) (fen_tailley + 15), 1);
  }
  /*Color Choice */
  if (strstr(name, "-CW") != NULL)
  {
    color = blanc;
  }
  else if (strstr(name, "-CR") != NULL)
  {
    color = rouge;
  }
  else if (strstr(name, "-CG") != NULL)
  {
    color = vert;
  }
  else if (strstr(name, "-CB") != NULL)
  {
    color = bleu;
  }
  else if (strstr(name, "-CGY") != NULL)
  {
    color = gris;
  }

  /*TILT*/ if (strstr(name, "-TILT") != NULL)
  {
    tilt = 1;
  }

  xmax = def_groupe[groupe_entree].taillex;
  ymax = def_groupe[groupe_entree].tailley;

  taille_entree = def_groupe[groupe_entree].nbre;
  deb2 = def_groupe[groupe_entree].premier_ele;

  imax = 0;

  xmax = def_groupe[groupe_entree].nbre;

  max = neurone[deb2].s;
  for (i = 0; i < xmax; i++)
  {
    if (neurone[deb2 + i].s > max)
    {
      max = neurone[deb2 + i].s;
      imax = i;
    }
  }

  /*----------------------------*/
  /*Dessin de la courbe act(x).s */
  for (i = 0; i < xmax - 1; i++)
  {
    /*-------------------*/
    /*Rotation des axes ? */
    if (!tilt)
    {
      valx1 = (float) i / xmax;
      valx2 = (float) (i + 1) / xmax;
      valy1 = (neurone[deb2 + i].s / max);
      valy2 = (neurone[deb2 + i + 1].s / max);
    }
    else
    {
      valx1 = (neurone[deb2 + i].s / max);
      valx2 = (neurone[deb2 + i + 1].s / max);
      valy1 = (float) i / xmax;
      valy2 = (float) (i + 1) / xmax;
    }

    point.x = (int) (valx1 * fen_taillex + fen_posx);
    point.y = fen_tailley - (int) (valy1 * fen_tailley - fen_posy);
    point2.x = (int) (valx2 * fen_taillex + fen_posx);
    point2.y = fen_tailley - (int) (valy2 * fen_tailley - fen_posy);

    if (point.x >= 0 && point.y >= 0 && point2.x >= 0 && point2.y >= 0)
      TxDessinerSegment(fenetre, color, point, point2, 1);

  }
  /*------------------------------------------------*/
  /*Affichage dernier segment et  max et sa position */
  point.x = point2.x;
  point.y = point2.y;

  if (!tilt)
  {
    valx1 = (float) i / xmax;
    valx2 = (float) (i + 1) / xmax;
    valy1 = (neurone[deb2 + i].s / max);
    valy2 = (neurone[deb2 + i + 1].s / max);

    point2.x = (int) (valx1 * fen_taillex + fen_posx);
    point2.y = fen_tailley - (int) (valy1 * fen_tailley - fen_posy);
    if (point.x >= 0 && point.y >= 0 && point2.x >= 0 && point2.y >= 0)
      TxDessinerSegment(fenetre, color, point, point2, 1);

    point.x = (int) (((float) imax / xmax) * fen_taillex + fen_posx) + 1;
    point.y =
      fen_tailley -
      (int) ((neurone[deb2 + imax].s / max) * fen_tailley - fen_posy);
    sprintf(st, "Max.s=%3.1f neur=%d", max, imax);
  }
  else
  {
    valx1 = (neurone[deb2 + i].s / max);
    valx2 = (neurone[deb2 + i + 1].s / max);
    valy1 = (float) i / xmax;
    valy2 = (float) (i + 1) / xmax;

    point2.x = (int) (valx1 * fen_taillex + fen_posx);
    point2.y = fen_tailley - (int) (valy1 * fen_tailley - fen_posy);
    if (point.x >= 0 && point.y >= 0 && point2.x >= 0 && point2.y >= 0)
      TxDessinerSegment(fenetre, color, point, point2, 1);

    point.x =
      (int) ((neurone[deb2 + imax].s / max) * fen_taillex + fen_posx) +
      1;
    point.y =
      fen_tailley - (int) (((float) imax / xmax) * fen_tailley -
			   fen_posy);
    sprintf(st, "Max.s=%3.1f neur=%d", max, imax);
  }
  if (point.x >= 0 && point.y >= 0)
    TxEcrireChaine(fenetre, color, point, st, NULL);
  TxFlush(&image1);
  TxFlush(&image2);
#else
  (void)numero;
#endif
}
