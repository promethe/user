/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \ingroup libIHM
   \defgroup f_speak f_speak
   \brief Permet de prononcer la phrase passee dans le lien a l'aide d'un synthetiseur vocal.

   @author A. Jauffret
   @date 19/04/2011

   \section Description
   Cette fonction de debug vocal permet de prononcer la phrase passee dans le lien a l'aide d'un synthetiseur vocal libre 'espeak' lorsque le groupe precedant
   possede une activitee superieure au seuil (threshold propre a chaque groupe f_speak). f_speak parse la phrase a prononcer et les parametres presents sur le lien d'entree.
   Elle les transfert ensuite a Shirka via le bus ivy (a l'IP precisee ou en broadcast).
   Pour cela il convient de lancer promethe en ajoutant "-b IP" ou "-b BROADCAST_ADRESS" en ligne de commande.
   ATTENTION: tenter une connexion en broadcast peut prendre du temps (>10 secondes), je vous recommande donc de preciser l'IP cible ou s'execute Shirka si vous la connaissez.
   Shirka (blind themis) est ensuite chargee de capturer ces messages transitant sur ivy afin de les prononcer.

   \section Options
   Mode phrase unique via: shirka
   - -t	texte a prononcer (max 256 chars) (IMPORTANT: remplacer les espaces entre les mots par des '_')
   - -a	amplitude, volume sonore (min 0, max 99, default 99)
   - -s	speed, vitesse en mots/min (min 0, max 200, default 130)
   - -p	pitch, frequence de la voix (grave 0, aigue 200, default 20)

   Mode multiple sentences defined in (file).dev
   - text A mettre au debut pour preciser ce mode
   - -id= nom du device qui definira les differentes phrases en utilisant le .dev (a ne pas utiliser avec -t)

   Fire with the max activity on S1 and 1 on S2 when the sentence is activated. 0 otherwise.

   \file
   \ingroup f_speak

************************************************************/
/*#define DEBUG*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libx.h>

#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <dev.h>
#include <prom_tools/include/prom_bus.h>
#include <Components/voice.h>

#define MAX_SIZE_OF_SPEAK 256
#define SIZE_OF_DEVICE_ID 64

#define	DEFAULT_SPEED 130
#define DEFAULT_PITCH 20
#define	DEFAULT_AMPLITUDE 99

typedef struct speak_s {
  int gpe_input;
  char talk[MAX_SIZE_OF_SPEAK];
  int speed;
  int pitch;
  int amplitude;
  int give_index;
  float previous_max;
  int previous_max_id;

  /* Used by hardware */
  Device *device;
} type_speak;

/********************************************************************/
void new_speak(int gpe)
{

  int i, j, l;
  type_speak *my_data = NULL;
  char speed[64];
  char pitch[64];
  char amplitude[64];
  int option_result;
  char device_id[SIZE_OF_DEVICE_ID];

  /** On enregistre les informations concernant le numero du
     groupe contenant l'image en entree*/

  /** Recuperation de la valeur des parametres de chaque
     lien en entree du groupe  */

  my_data = ALLOCATION(type_speak);
  my_data->gpe_input = -1;

  l = 0;
  j = find_input_link(gpe, l);

  my_data->device = NULL;
  my_data->previous_max = 0;
  my_data->previous_max_id = 0;

  while (j != -1)
  {
    my_data->gpe_input = liaison[j].depart;

    option_result = prom_getopt(liaison[j].nom, "-t", my_data->talk);
    if (option_result == 2)
    {
      for (i = 0; i < MAX_SIZE_OF_SPEAK; i++)
      {
        if (my_data->talk[i] == '_') my_data->talk[i] = ' ';
      }

      option_result = prom_getopt(liaison[j].nom, "-a", amplitude);
      if (option_result <= 1) my_data->amplitude = DEFAULT_AMPLITUDE;
      else my_data->amplitude = atoi(amplitude);

      option_result = prom_getopt(liaison[j].nom, "-p", pitch);
      if (option_result <= 1) my_data->pitch = DEFAULT_PITCH;
      else my_data->pitch = atoi(pitch);

      option_result = prom_getopt(liaison[j].nom, "-s", speed);
      if (option_result <= 1) my_data->speed = DEFAULT_SPEED;
      else my_data->speed = atoi(speed);
      
      option_result = prom_getopt(liaison[j].nom, "-i", speed);
      if (option_result < 1) my_data->give_index = 0;
      else my_data->give_index = 1;
    }
    else if (strstr(liaison[j].nom, "text") == liaison[j].nom) /* link name start with "text" */
    {
      option_result = prom_getopt(liaison[j].nom, "-id=", device_id);
      if (option_result == 0)
      {
	my_data->device = dev_get_device("voice", NULL);
      }
      else if (option_result == 1)
      {
	EXIT_ON_ERROR("Group %s, the parameter of 'id=' is not valid on the link :'%s'.", def_groupe[my_data->gpe_input].no_name, liaison[j].nom);
      }
      else if (option_result == 2)
      {
	my_data->device = dev_get_device("voice", device_id);
      }

      if (def_groupe[my_data->gpe_input].nbre != my_data->device->number_of_components) EXIT_ON_ERROR("The number of voices: %d does not correspond to the size (%d) of the input groupe: %s", my_data->device->number_of_components, def_groupe[my_data->gpe_input].nbre, def_groupe[my_data->gpe_input].no_name);
    }
    l++;
    j = find_input_link(gpe, l);
  }

  if (my_data->gpe_input == -1)
  {
    fprintf(stderr, "ERROR in new_speak(%s): Missing input link\n", def_groupe[gpe].no_name);
    exit(1);
  }
  def_groupe[gpe].data = my_data; /* sauvegarde de My_Data*/
}

void function_speak(int gpe)
{
  type_speak *my_data = NULL;
  int i;
  float threhold;
  float max;
  int winner;
  int gpe_input = -1;
  char speak[MAX_SIZE_OF_SPEAK];
  type_voice **voices;

  dprints("in f_speak (%s)\n",def_groupe[gpe].no_name);

  /** recup donnees */
  my_data = (type_speak *) def_groupe[gpe].data;

  threhold = def_groupe[gpe].seuil;

  if (my_data->gpe_input != -1) gpe_input = my_data->gpe_input;

  neurone[def_groupe[gpe].premier_ele].s1 = 0;
  neurone[def_groupe[gpe].premier_ele].s2 = 0;

  max = neurone[def_groupe[gpe_input].premier_ele].s1;
  winner = 0;

  for (i = 1; i < def_groupe[gpe_input].nbre; i++)
  {
    if (neurone[def_groupe[gpe_input].premier_ele + i].s1 > max)
    {
      max = neurone[def_groupe[gpe_input].premier_ele + i].s1;
      winner = i;
    }
  }
  
  

  if ((max > threhold) && ((winner != my_data->previous_max_id) ||(my_data->previous_max <= threhold))) 
  /** On ne parle que si on est superieur au seuil et (avant on ne l'etait pas(detection front montant) ou la position du max a change)*/
  {
    if (my_data->device == NULL) /** Using directly shirka */
    {
      if(my_data->give_index)
        snprintf(speak, MAX_SIZE_OF_SPEAK, "speak( -ven-us+f0 -p%i -s%i -a%i \"%s %d\")", my_data->pitch, my_data->speed, my_data->amplitude, my_data->talk, winner);
      else
        snprintf(speak, MAX_SIZE_OF_SPEAK, "speak( -ven-us+f0 -p%i -s%i -a%i \"%s\")", my_data->pitch, my_data->speed, my_data->amplitude, my_data->talk);
      dprints("%s \n",speak);
      prom_bus_send_message(speak);
    }
    else /** Using multiple sentences defined in .dev */
    {
      voices = (type_voice **)my_data->device->components;
      voices[winner]->speak(voices[winner]);
    }
    neurone[def_groupe[gpe].premier_ele].s1 = max;
    neurone[def_groupe[gpe].premier_ele].s2 = 1;
    my_data->previous_max_id = winner; /* memory of curr winner index */
  }

  my_data->previous_max = max; /* memoire de l'ancienne valeur */
  dprints("out  f_speak (%s)\n",def_groupe[gpe].no_name);
}

void destroy_speak(int gpe)
{
  type_speak *my_data = def_groupe[gpe].data;
  free(my_data);
}
