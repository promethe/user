/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_demande_angle_degres f_demande_angle_degres
\brief 
 

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  If you learn, ask for a name (winner of input group)
  else tell which name is associated to               
 Sauvegarde dans un fichier les noms                  
 + version si il y a -batch sur le lien alors, on    
 ne demande pas a l'utilisateur une interpretation   
 mais tout est auto, d'apres le nom des images       
 + si -D (display info) -ND (not display par defaut) 

\section Macro
-none 

\section Local variables
-none

\section Global variables
-char GLOB_path_SAVE[512]
-int learNN
-char vrai_nom_image[256}

\section Internal Tools
-tools/include/load_ask_name_learned()
-tools/include/save_ask_name_learned()

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Global_Var/NN_IO.h>

void load_ask_name_learned(char *file_name, int nb_max_ask,
                           char ***name_learned)
{
    FILE *F;
    int pos,test;
    char st[50], *s;

    F = fopen(file_name, "r");
    if (F == NULL)
    {
#ifdef DEBUG
        dprints
            ("\n!!! Impossible to open %s in load_ask_name_learned(At start it is normal) !!!!\n",
             file_name);
#endif
        /*Au debut c'est normal: on ne peut lire que si qqchose a ete ecrit! */
        return;
    }
    for (pos = 0; pos < nb_max_ask; pos++)
    {
        test=fscanf(F, "%s\n", st);
        if( test == 0){kprints("Erreur de lecture du fichier");}
        
        if (strcmp("NULL", st) == 0)
        {
            /* printf("NULL\n"); */
            *(*name_learned + pos) = NULL;
        }
        else
        {
            /* printf("pos=%i %s\n",pos,st); */
            s = calloc(50, sizeof(char));
            if (s == NULL)
            {
                EXIT_ON_ERROR("Impossible to alloc in load_ask_name_learned\n");
                exit(EXIT_FAILURE);
            }
            memcpy(s, st, 50 * sizeof(char));
            *(*name_learned + pos) = s;
        }
    }
    fclose(F);
}

void save_ask_name_learned(char *file_name, int nb_max_ask,
                           char **name_learned)
{
    FILE *F;
    int pos;
    do
    {
        F = fopen(file_name, "w");
        if (F == NULL)
        {
            kprints("Impossible to open %s in save_ask_name_learned\n",
                   file_name);
            /* exit(EXIT_FAILURE); */
        }
    }
    while (F == NULL);

    for (pos = 0; pos < nb_max_ask; pos++)
    {
        if (*(name_learned + pos) != NULL)
        {
            /* printf("pos=%i %s\n",pos,*(name_learned+pos)); */
            fprintf(F, "%s\n", name_learned[pos]);
        }
        else
        {
            /* printf("pos=%i NULL\n",pos); */
            fprintf(F, "NULL\n");
        }
    }
    fclose(F);
}

void function_ask_for_learn(int numero)
{
#define NB_MAX_ASK 512
    char **name_learned, *st2;
    float tab_tmp[NB_MAX_ASK][3]; // J.Fellus : modified 2->3 because of subscript overflow
    int test, gp1, i, j, N, M, N1, M1, p, p1, p1_max, inc1, nofind =
        1, pos, pas, jmax, batch = 0, display = 0;
    char st[256], str[256], file_name_ask[100] =
        "ask", file_name_ask_name[100] = "ask.name", file_name_learned[100] =
        "ask.name_learned";
    char file_name_input[100] = "image_names_input", file_name_output[100] =
        "image_names_output", file_name_error[100] =
        "image_names_error", stmp[512];
    float max_g1, tmp, max, diff, maxs, maxs1, maxs2, means, means1, means2,
        mins, mins1, mins2, tmps, tmps1, tmps2, vals, vals1, vals2, errs,
        errs1, errs2;
    FILE *fp, *fp2, *f_input = NULL, *f_output = NULL, *f_error = NULL;
#ifndef AVEUGLE
    void *fenetre = NULL;
    int fen_posx = 260, fen_posy = 0;
    TxPoint point, point2, point3;
#endif

    (void) display; // (unused)

  /*--------------*/
    /*Allocation mem */
    name_learned = (char **) malloc(NB_MAX_ASK * sizeof(char *));
    if (name_learned == NULL)
    {
        kprints
            ("impossible to allocate name_learned in function_ask_for_learn\n");
        exit(EXIT_FAILURE);
    }

#ifdef DEBUG
    dprints("-----------------------------------------------\n");
    dprints("          function_ask_for_learn \n");
#endif

  /*----------------------*/
    /*Find the input gp link */
    for (i = 0; (i < nbre_liaison) && nofind; i++)
        if (liaison[i].arrivee == numero)
            nofind = 0;

    gp1 = liaison[i - 1].depart;

  /*--------------------------------------------------------------*/
    /*Creation des noms de fichier en fonction de ce qui a en entree */
    sprintf(file_name_ask, "%s_%s.SAVE", file_name_ask, def_groupe[gp1].nom);
    sprintf(file_name_ask_name, "%s_%s.SAVE", file_name_ask_name,
            def_groupe[gp1].nom);
    sprintf(file_name_learned, "%s_%s.SAVE", file_name_learned,
            def_groupe[gp1].nom);
    sprintf(file_name_input, "%s_%s.SAVE", file_name_input,
            def_groupe[gp1].nom);
    sprintf(file_name_output, "%s_%s.SAVE", file_name_output,
            def_groupe[gp1].nom);
    sprintf(file_name_error, "%s_%s.SAVE", file_name_error,
            def_groupe[gp1].nom);

  /*-------------------------------------------------------------------------------*/
    /*Chemin a ecrire apres sinon si on reecrit dans la chaine qu'on utilise ca foire */
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_ask);
    sprintf(file_name_ask, "%s", stmp);
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_ask_name);
    sprintf(file_name_ask_name, "%s", stmp);
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_learned);
    sprintf(file_name_learned, "%s", stmp);
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_input);
    sprintf(file_name_input, "%s", stmp);
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_output);
    sprintf(file_name_output, "%s", stmp);
    sprintf(stmp, "%s/%s", GLOB_path_SAVE, file_name_error);
    sprintf(file_name_error, "%s", stmp);

  /*---------------------------------------------------------*/
    /*Test si on utilise le batch ou on demande a l'utilisateur */
    if (strstr(liaison[i - 1].nom, "-batch") != NULL
        || strstr(liaison[i - 1].nom, "-BATCH") != NULL)
    {
        batch = 1;
        f_input = fopen(file_name_input, "a");
        f_output = fopen(file_name_output, "a");
        f_error = fopen(file_name_error, "a");
    }

#ifndef AVEUGLE
  /*--------------------------------------------------*/
    /* Teste si on doit afficher sur l'image 1 des infos */
    if (strstr(liaison[i - 1].nom, "-D") != NULL
        || strstr(liaison[i - 1].nom, "-d") != NULL)
    {
        display = 1;
        /*Position beginning of text display */
        st2 = strstr(liaison[i - 1].nom, "-PX");
        if ((st2 != NULL))
            fen_posx = atoi(&st2[3]);
        st2 = strstr(liaison[i - 1].nom, "-PY");
        if ((st2 != NULL))
            fen_posy = atoi(&st2[3]);
        point.x = fen_posx;
        point.y = fen_posy + 15;
        point2.x = point.x;
        point2.y = point.y - 15;
        point3.x = point.x;
        point3.y = point.y + 15;
        /*Image1 or Image2 */
        fenetre = &image1;
        if (strstr(liaison[i - 1].nom, "-I1") != NULL)
        {
            fenetre = &image1;
        }
        else if (strstr(liaison[i - 1].nom, "-I2") != NULL)
        {
            fenetre = &image2;
        }
    }
#endif
    if (strstr(liaison[i - 1].nom, "-ND") != NULL
        || strstr(liaison[i - 1].nom, "-nd") != NULL)
    {
        display = 0;
    }

  /*---------------*/
    /*Size of groups */
    N = def_groupe[numero].taillex;
    M = def_groupe[numero].tailley;
    N1 = def_groupe[gp1].taillex;
    M1 = def_groupe[gp1].tailley;
    inc1 = def_groupe[gp1].nbre / (N1 * M1);
    if (N != 1 || M != 1)
    {
        EXIT_ON_ERROR("Size of groups must be 1 neurone in function_ask_for_learn\n"
               /*,gp1,numero */ );
        exit(EXIT_FAILURE);
    }

  /*------------------------------------------*/
    /* Max and position of max in group1 on s1!! */
    mins1 = maxs1 = max_g1 =
        neurone[def_groupe[gp1].premier_ele + inc1 - 1].s1;
    mins = maxs = neurone[def_groupe[gp1].premier_ele + inc1 - 1].s;
    mins2 = maxs2 = neurone[def_groupe[gp1].premier_ele + inc1 - 1].s2;
    means = means1 = means2 = 0.;
    p1_max = def_groupe[gp1].premier_ele + inc1 - 1;    /*index macroneur */
    pos = 0;                    /*index macroneur sans def_groupe[gp1].premier_ele */
    for (i = 0; i < N1; i++)
        for (j = 0; j < M1; j++)
        {
            p1 = def_groupe[gp1].premier_ele + (i + 1) * inc1 - 1 +
                j * N1 * inc1;
            tmps1 = tmp = neurone[p1].s1;
            tmps = neurone[p1].s;
            tmps2 = neurone[p1].s2;
            if (tmp > max_g1)
            {
                pos = i + j * N1;
                p1_max = p1;
                maxs1 = max_g1 = tmp;
            }
            if (tmps > maxs)
                maxs = tmps;
            if (tmps2 > maxs2)
                maxs2 = tmps2;

            if (neurone[p1].seuil > 0.9)    /*Que parmis les neur PTM2 qui ont appris */
            {
                /* minimas */
                if (tmps < mins)
                    mins = tmps;
                if (tmps1 < mins1)
                    mins1 = tmps1;
                if (tmps2 < mins2)
                    mins2 = tmps2;
                /* Moyennes */
                means += tmps;
                means1 += tmps1;
                means2 += tmps2;
            }
    /*--------------------------------------*/
            /*memo val pour classement des activites */
            tab_tmp[i + j * N1][1] = tmp;   /*La valeur  */
            tab_tmp[i + j * N1][2] = (float) (i + j * N1);  /*Sa position */
        }
    /* Moyennes */
    means = means / (N1 * M1);
    means1 = means1 / (N1 * M1);
    means2 = means2 / (N1 * M1);


#ifdef DEBUG
  /*---------*/
    /* Message */
    dprints("The winner of group %d is %d with activities s=%f s1=%f s2=%f\n",
           gp1, p1_max, neurone[p1_max].s, neurone[p1_max].s1,
           neurone[p1_max].s2);
#endif

  /*--------------------------*/
    /*Position in the char array */
    pos = pos;

  /*--------*/
    /*Au debut */
    for (p = 0; p < NB_MAX_ASK; p++)
        name_learned[p] = NULL;

  /*-----*/
    /*Appel */
    load_ask_name_learned(file_name_learned, NB_MAX_ASK,
                          (char ***) &name_learned);

  /*--------------*/
    /*Open the file */
    fp = fopen(file_name_ask, "a");
    fp2 = fopen(file_name_ask_name, "a");
    if (fp == NULL || fp2 == NULL)
    {
        EXIT_ON_ERROR("Impossible to open files %s or %s in ask_for_learn\n",
               file_name_ask, file_name_ask_name);
        exit(EXIT_FAILURE);
    }

 /*------------------------------------------------------------------------------*/
    /*Saving in a file the winner(s1) number and its act, the max, the min the means */
    fprintf(fp, "%d %f %f %f %f %f %f %f %f %f %f %f %f\n", pos,
            neurone[p1_max].s, neurone[p1_max].s1, neurone[p1_max].s2, maxs,
            maxs1, maxs2, mins, mins1, mins2, means, means1, means2);

#ifndef AVEUGLE
 /*-------------------*/
    /*Affichage a l'ecran */
    if (display)
    {
        sprintf(str, "s=%f", neurone[p1_max].s);
        TxDessinerRectangle(fenetre, bleu, TxPlein, point2, (int) 150,
                            (int) 35, 1);
        TxEcrireChaine(fenetre, blanc, point3, str, NULL);
    }
#endif

    vals = maxs;
    vals1 = maxs1;
    vals2 = maxs2;

 /*-------------------------*/
    /*If the system is learning */
    if (learn_NN)
    {
      /*-------------------------------------*/
        /*New allocation of string if necessary */
        if (name_learned[pos] == NULL)
        {
            name_learned[pos] = calloc(50, sizeof(char));
            if (name_learned[pos] == NULL)
            {
                EXIT_ON_ERROR
                    ("Impossible to allocate name_learned[pos] in ask_for_learn\n");
                exit(EXIT_FAILURE);
            }

            if (!batch)
            {
                cprints("Give a name for the winner :\n");
                test=scanf("%s", name_learned[pos]);
                if(test == 0){kprints("Erreur dans la lecture du scanf");}
            }
            else
            {
          /*-------------------*/
                /*Writting input name */
                fprintf(f_input, "%s\n", vrai_nom_image);
          /*--------------------*/
                /*Writting output name */
                fprintf(f_output, "%s\n", vrai_nom_image);  /*the same as input! */
          /*--------------*/
                /*Writting error */
                fprintf(f_error, "%d %f %f %f %f %f %f %f\n", 0, 0., 0., 0., 0., vals, vals1, vals2);   /*aucune erreur: juste */


                strcpy(name_learned[pos], vrai_nom_image);
            }
            fprintf(fp2, "%s\n", name_learned[pos]);    /*Saving the name */
        }
        else
        {
            cprints("This neuron has already learned name :%s\n",
                   name_learned[pos]);
            if (!batch)
            {
                cprints
                    ("Give a NEW name for the winner (it will be concatened to the previous):\n");
                test=scanf("%s", st);
                if(test == 0){kprints("Erreur dans la lecture du scanf");}
                strcat(name_learned[pos], st);
            }
            else
            {
          /*-------------------*/
                /*Writting input name */
                fprintf(f_input, "%s\n", vrai_nom_image);
          /*--------------------*/
                /*Writting output name */
                fprintf(f_output, "ERR\n"); /*Problem */
          /*--------------*/
                /*Writting error */
                fprintf(f_error, "%i %f %f %f %f %f %f %f\n", -1, -1., -1., -1., -1., -1., -1., -1.);   /* erreur d'interpretation -1 napplicable! */

                strcpy(name_learned[pos], "ERR");

            }

            fprintf(fp2, "%s\n", name_learned[pos]);    /*Saving the name */
        }
#ifndef AVEUGLE
      /*-------------------*/
        /*Affichage a l'ecran */
        if (display)
            TxEcrireChaine(fenetre, blanc, point, name_learned[pos], NULL);
#endif

    }
  /*---------------------*/
    /*The system is testing */
    else
    {
        if (name_learned[pos] == NULL)
        {
            cprints("No name stored for the winner\n");
            if (batch)
            {
          /*-------------------*/
                /*Writting input name */
                fprintf(f_input, "%s\n", vrai_nom_image);
          /*--------------------*/
                /*Writting output name */
                fprintf(f_output, "NONAME\n");  /*Problem */
          /*--------------*/
                /*Writting error */
                fprintf(f_error, "%i %f %f %f %f %f %f %f\n", -1, -1., -1., -1., -1., -1., -1., -1.);   /* erreur d'interpretation -1 napplicable! */
            }
            fprintf(fp2, "NONAME\n");   /*Saving NONAME */
#ifndef AVEUGLE
      /*-------------------*/
            /*Affichage a l'ecran */
            if (display)
                TxEcrireChaine(fenetre, blanc, point, (char*)"NONAME", NULL);
#endif
        }
        else
        {
#ifdef DEBUG
            dprints("The name learned for the winner is : %s\n",
                   name_learned[pos]);
#endif
            if (batch)
            {
          /*-------------------*/
                /*Writting input name */
                fprintf(f_input, "%s\n", vrai_nom_image);
          /*--------------------*/
                /*Writting output name */
                fprintf(f_output, "%s\n", name_learned[pos]);
          /*--------------*/
                /*Writting error */
                /*computing... */
                if (strncmp
                    (name_learned[pos], vrai_nom_image,
                     strlen(name_learned[pos])) == 0)
                    fprintf(f_error, "%d %f %f %f %f %f %f %f\n", 0, 0., 0., 0., 0., vals, vals1, vals2);   /*aucune erreur: juste */
                else
                {
          /*---------------------------------------------*/
                    /*dans ce cas: Erreur ! faux                   */
                    /*Calcul de combien de position on fait erreur! */
                    p1 = def_groupe[gp1].premier_ele;
                    nofind = 1;
                    /*recherche du num de neur associe au nom qu'il aurait fallu trouver */
                    for (p = 0; p < (N1 * M1); p++)
                        if (name_learned[p] != NULL)
                            if (strncmp
                                (name_learned[p], vrai_nom_image,
                                 strlen(name_learned[p])) == 0)
                            {
                                nofind = 0;
                                break;
                            }
                    /*memoriser p !! c'est le numero du neur qui aurait du gagner! */


                    if (nofind)
                    {
                        i = -1;
                        diff = -1.; /* si i=-1 : napplicable!PB!!! */
                        errs = errs1 = errs2 = -1.;
                        vals = vals1 = vals2 = -1.;
                    }
                    else
                    {
                        /*ClaSSEMENT partiel des valeurs des neur */
                        i = 0;
                        while (i < N1 * M1)
                        {
                            max = tab_tmp[i][1];
                            jmax = i;
                            /*recherche du max */
                            for (j = i; j < N1 * M1; j++)
                            {
                                if (tab_tmp[j][1] > max)
                                {
                                    max = tab_tmp[j][1];
                                    jmax = j;
                                }
                            }
                            /*echange des vals */
                            tmp = tab_tmp[i][1];
                            tab_tmp[i][1] = tab_tmp[jmax][1];
                            tab_tmp[jmax][1] = tmp;
                            tmp = tab_tmp[i][2];
                            tab_tmp[i][2] = tab_tmp[jmax][2];
                            tab_tmp[jmax][2] = tmp;
                            /*On quitte? */
                            if ((int) tab_tmp[i][2] == p)
                                break;
                            /*on avance... */
                            i = i + 1;
                        }

                        diff = neurone[p1_max].s1 - tab_tmp[i][1];  /*difference de niveau par rapport au max .s1 */

                        vals = neurone[def_groupe[gp1].premier_ele + (p + 1) * inc1 - 1].s; /*niveau du neur qui aurait du gagner */
                        vals1 = neurone[def_groupe[gp1].premier_ele + (p + 1) * inc1 - 1].s1;   /*niveau du neur qui aurait du gagner */
                        vals2 = neurone[def_groupe[gp1].premier_ele + (p + 1) * inc1 - 1].s2;   /*niveau du neur qui aurait du gagner */
                        errs = (maxs - vals) / (maxs - means);
                        errs1 = (maxs1 - vals1) / (maxs1 - means1);
                        errs2 = (maxs2 - vals2) / (maxs2 - means2);
                    }

                    /*dans i on a le ieme max qu'on cherche... si i=-1 : napplicable!PB!!! */
                    fprintf(f_error, "%d %f %f %f %f %f %f %f\n", i, diff,
                            errs, errs1, errs2, vals, vals1, vals2);
                }
            }
            fprintf(fp2, "%s\n", name_learned[pos]);    /*Saving the name */
#ifndef AVEUGLE
      /*-------------------*/
            /*Affichage a l'ecran */
            if (display)
                TxEcrireChaine(fenetre, blanc, point, name_learned[pos],
                               NULL);
#endif

        }
    }

 /*------------------------------------*/
    /*Sauvegarde des infos dans un fichier */
    save_ask_name_learned(file_name_learned, NB_MAX_ASK, name_learned);

    fclose(fp);
    fclose(fp2);
    if (batch)
    {
        fclose(f_input);
        fclose(f_output);
        fclose(f_error);
    }

#ifndef AVEUGLE
  /*----------------*/
    /*AFFICHAGE image1 */
    if (display)
    {

        pas = 14;
        point.x = 213;
        point.y = 50;
      /*----------------------*/
        /*effacement zone dessin */
        TxDessinerRectangle(fenetre, gris, TxPlein, point, (int) (10 * 11),
                            (int) (pas * (N1 * M1)), 1);
      /*------------------------------------*/
        /*Affichage des noms appris en colonne */
        for (i = 0; i < (N1 * M1); i++)
        {
            point.y += (int) pas;
            if (name_learned[i] == NULL)
                TxEcrireChaine(fenetre, blanc, point,(char*) "NONAME", NULL);
            else
                TxEcrireChaine(fenetre, blanc, point, name_learned[i], NULL);
        }

        TxFlush(&image1);
        TxFlush(&image2);
    }
#endif

  /*--------------------------------*/
    /*Liberation de la memoire allouee */
    for (i = 0; i < NB_MAX_ASK; i++)
        if (name_learned[i] != NULL)
            free(name_learned[i]);

    if (name_learned != NULL)
        free(name_learned);

#ifdef DEBUG
    dprints("-----------------------------------------------\n");
#endif

(void) str;
(void) pas;
(void) st2;
}
