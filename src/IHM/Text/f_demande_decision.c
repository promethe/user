/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_demande_decision f_demande_decision
\brief 
 

\section Author
-Name: R. Shirakawa
-Created: 25/07/2006

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
La fonction lit une caractere avec le numero du neurone a activer et l'active en mettant tous les autres neurones a zero.
Si l'utilisateur mis 0 (zero) la fonction n'active aucun neurone

\section Macro
-none 
p
\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-Kernel_Function/find_input_link()

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <stdio.h>

typedef struct
{
    int deb;                    /* variable que indique la valeur du premier neurone */
    int longueur;               /* variable que indique la valeur du dernier neurone */
} decision_data;

void new_demande_decision(int Gpe)
{
    decision_data *data;        /* initialisation de la structure */
#ifdef DEBUG
    dprints("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    /*on cree la structure qui contiendra le pointeur vers ce fichier et on init */
    data = (decision_data *) malloc(sizeof(decision_data));

    data->deb = def_groupe[Gpe].premier_ele;
    data->longueur = def_groupe[Gpe].nbre;

    /* pour pouvoir passer les donnees a la fonction suivante */
    def_groupe[Gpe].data = data;

#ifdef DEBUG
    dprints("~~~~~~~~~~~end of %s\n", __FUNCTION__);
#endif
}

void function_demande_decision(int Gpe)
{

    /* initialisation des variables */
    decision_data *my_data = NULL;  /* structure des parametres regles par la fonction supervision */
    int decision = 0;
    int i,test;

    /* recuperer les donnees */
    my_data = (decision_data *) def_groupe[Gpe].data;

#ifdef DEBUG
    dprints("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

    /* mettre a zero tous les neurones */
    for (i = my_data->deb; i <= my_data->deb + my_data->longueur; i++)
    {
        neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.0;
    }

    do
    {
        cprints
            ("type the neurone number to be activate (between 0 (no activation) and %d)\n",
             my_data->longueur);
        test=scanf("%d", &decision);
        if(test == 0){kprints("Erreur de lecture du scanf");}
        
        if (decision < 0 || decision > my_data->longueur)
            cprints("wrong neurone number\n");
    }
    while (decision < 0 || decision > my_data->longueur);

    /* si OK, activer neurone */
    if (decision != 0)
    {
        neurone[my_data->deb + decision - 1].s2 =
            neurone[my_data->deb + decision - 1].s1 =
            neurone[my_data->deb + decision - 1].s = 1.;
    }

#ifdef DEBUG
    dprints("===========exit of %s\t group %d\n", __FUNCTION__, Gpe);
#endif
}
