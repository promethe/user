/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \ingroup libIHM
 \defgroup f_demande_vecteur f_demande_vecteur
 \author JC. Baccon
 \details Get a vector of given data with a file or the keyboard.

 \section Options
 \subsection Console
 The console (keyboard) is used by default. you can add options:
 - -c[filename] : Text file with the text to display before each request.
 - -s[request]	: Test to display before each request.
 \subsection File
 - -f[name of file]	: File to use as an input.

 \file
 \ingroup f_demande_vecteur
 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#ifdef USE_THREADS
#include <pthread.h>
extern pthread_mutex_t mutex_lecture_clavier;
#endif
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_demande_vecteur {
	char string_to_display[255];
	FILE * file_to_read;
	char filename_to_display[255];
} MyData_f_demande_vecteur;

void function_demande_vecteur(int numero)
{
	int deb, longueur;
	int l, i = 0;
	float val;
	FILE *file_to_read = NULL;
	FILE *file_to_display = NULL;
	char param_link[255];
	MyData_f_demande_vecteur *my_data = NULL;
	char string_to_display[255];
	char filename_to_display[255];
	char c;

	if (def_groupe[numero].data == NULL)
	{
		i = 0;
		sprintf(string_to_display, "-------------");
		sprintf(filename_to_display, "none");
		l = find_input_link(numero, i);

		while (l != -1)
		{
			if (prom_getopt(liaison[l].nom, "f", param_link) == 2)
			{
				file_to_read = fopen(param_link, "r");
				if (file_to_read == NULL) EXIT_ON_ERROR("cannot open %s\n", param_link);
			}

			if (prom_getopt(liaison[l].nom, "c", param_link) == 2)
			{
				sprintf(filename_to_display, "%s", param_link);
			}

			if (prom_getopt(liaison[l].nom, "s", param_link) == 2)
			{
				sprintf(string_to_display, "%s", param_link);
			}

			i++;
			l = find_input_link(numero, i);
		}
		my_data = ALLOCATION(MyData_f_demande_vecteur);

		sprintf(my_data->string_to_display, "%s", string_to_display);
		sprintf(my_data->filename_to_display, "%s", filename_to_display);
		my_data->file_to_read = file_to_read;
		def_groupe[numero].data = (MyData_f_demande_vecteur *) my_data;
	}
	else
	{
		my_data = ((MyData_f_demande_vecteur *) (def_groupe[numero].data));
		file_to_read = my_data->file_to_read;
		sprintf(string_to_display, "%s", my_data->string_to_display);
		sprintf(filename_to_display, "%s", my_data->filename_to_display);
	}

	longueur = def_groupe[numero].nbre;
	deb = def_groupe[numero].premier_ele;

	if (file_to_read == NULL)
	{
#ifdef USE_THREADS
		pthread_mutex_lock(&mutex_lecture_clavier);
#endif

		if (strcmp(filename_to_display, "none") != 0)
		{
			file_to_display = fopen(filename_to_display, "r");
			if (file_to_display == NULL) EXIT_ON_ERROR("Cannot open %s\n", filename_to_display);

			while ((c = getc(file_to_display)) != EOF)
			{
				printf("%c", c);
			}
			fclose(file_to_display);
		}

		cprints("%s\n", string_to_display);
		cprints("\tGive a vector of size %d\n", longueur);

		for (i = deb; i < deb + longueur; i++)
		{
			cprints("Value %d : ", i - deb);
			cscans("%f", &val);
			neurone[i].s = neurone[i].s1 = neurone[i].s2 = val;
		}
#ifdef USE_THREADS
		pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
	}
	else
	{
		for (i = 0; i < def_groupe[numero].nbre; i++)
		{
			if (fscanf(file_to_read, "%s", param_link) != 1) EXIT_ON_ERROR("Fail to read the file\n");
			neurone[def_groupe[numero].premier_ele + i].s = atof(param_link);
			neurone[def_groupe[numero].premier_ele + i].s1 = neurone[def_groupe[numero].premier_ele + i].s2 = neurone[def_groupe[numero].premier_ele + i].s;
		}
		my_data->file_to_read = file_to_read;
	}

}
