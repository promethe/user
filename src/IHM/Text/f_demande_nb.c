/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_demande_nb f_demande_nb
\brief 
 

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  saisie du pave numerique sans bloquer le clavier        
     le chiffre entre active  la cellule correspondante   

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdlib.h>

void function_demande_nb(int numero)
{
    /*unused: int taille_sortie; */
    int deb, longueur;
    int i, x,test;
    char car;
    longueur = def_groupe[numero].nbre;
    deb = def_groupe[numero].premier_ele;



    /* mise a jour des neurones a zero */
    for (i = deb; i < deb + longueur; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;


    /*!  saisie du clavier sans bloquage                           
     *    -attention: le comportement de icanon semble legerement different      
     *    selon son utilisation sous solaris ou linux                 
     *    il est possible de jouer sur le parametre "time"            */

    test=system("stty -icanon min 0 time 0");
    
    if(test != 0){kprints("Problème de clavier");}
    car = getchar();

    /*conversion du caractere en entier correspondant */
    x = car - 48 + deb;
    /*activation de la cellule correspondante */
    if ((x >= deb) && (x < deb + longueur))
        neurone[x].s = neurone[x].s1 = neurone[x].s2 = 1.;

    else
    {
        for (i = deb; i < deb + longueur; i++)
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    }



}
