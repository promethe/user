/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file
\brief 

Author: C.G
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Cette fonction affiche l'activite des neurone du groupe d'entree aucours du temps
	et/ou ecrit les activites des neurones du groupe d'entree dans un fichier. 
	Chaque colonne de ce fichier correspond a un neurone et chaque ligne correspond a un releve.  
	Des colonnes complementaires peuvent etre demande pour ecrire de nouvelles colonnes indiquant 
	le numero de la ligne, le numero de ligne multiplie par un facteur, le temps ecoule depuis 
	le debut de la simulation.  

	Option sur le lien et valeur par defaut :
	-s0   : sortie desire (0,1 ou 2)
	
	
	Options sur le lien et valeur par defaut pour l'affichage :
	-x0   : position du graphe dans l'image
	-y0   : postion du graphe dans l'image
	-i1   : numero de l'interface (1 ou 2)
	-m20  : memoire temporelle de la boite
	-d1   : actif, permet la visualisation
	 
	Options sur le lien et valeurs par defaut pour la sauvegarde dans un fichier :
	-f0             : sauvegarde des activites dans un fichier inactive
	-f ou f1        : sauvegarde des activites dans un fichier active
	-c ou -c1       : ecrit la valeur du compteur en premiere colonne (ou deuxieme ou troisieme 
			si -tsim ou tabs sont avant)
	-c0             : n'ecrit pas de colonne compteur
	-c22.5          : ecrit en premiere colonne le compteur mult par 22.5
	-tsim ou -tsim1 : ecrit le temps ecoule depuis le debut de la simulation en premiere colonne 
	(ou deuxieme si -tabs est avant) en fait deux colonnes, une pour les secondes et la suivante pour les 
			millisecondes
	-tsim0          : n'ecrit pas de colonne temps
	-tabs ou tabs1  : ecrit le temps absolu en premiere colonne (-----Attention !----- : necessaire pour pouvoir 
			comparer deux promethes qui interagissent par l'environnement : ex: Osc_arm_cam_couples)
	-tabs0          : n'ecrit pas de colonne temps absolu


Macro:
-none

Local variables:
-none
nabet
Global variables:
-none

Internal Tools:
-affiche_cell_activity()

External Tools:
-Kernel_Function/find_input35+10*(i+1)+my_data->PX;_link()
-Kernel_Function/prom_getopt()
nabet
Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


typedef struct MyData_f_display_phase_space
{
    float **recall;
    int gp_input;
    int max_recall;
    int nb_cell_to_display;
    int PX;
    int PY;
    int I;
    int N;
    int M;
    int inc;
    int type_sortie;
    int display;
    int file_flag;
    char *file_name;
    long start_time_sec;
    long start_time_msec;
    int time_sim_flag;
    int time_abs_flag;
    int cpt;
    float cpt_scale;
} MyData_f_display_phase_space;


void affiche_phase_space(int numero);

/*#define DEBUG*/
void function_display_phase_space(int numero)
{
    struct timeval StartTime, CurrentTime;
    int nb_cell_to_display, i, j, l, pos, N, M, neur, gp_input, inc, I =
        1, PX = 0, PY = 0, t_sortie = 0;
    int display = 0, file_flag = 0, time_sim_flag = 0, time_abs_flag =
        0, cpt = 0;
    long start_time_sec, start_time_msec, time_of_sim_sec =
        0, time_of_sim_msec = 0, current_time_sec, current_time_msec;
    float act = 0, cpt_scale = 1.0;
/* static int cpt;*/
    char x_pos[4], y_pos[4], interface[1], mem[3], type_sortie[4],
        display_param[1];
    char f_opt[4], cpt_param[10], time_sim[4], time_abs[4], string[255];
    FILE *fp;
    int max_recall = 200;
    float **recall;
    MyData_f_display_phase_space *my_data = NULL;
    char *file_name = NULL;


#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_display_image_activity---------------------\n");
#endif

    if (def_groupe[numero].data == NULL)
    {
  /*---------------------------*/
        /*Looking for the input group */
        l = find_input_link(numero, 0);
        gp_input = liaison[l].depart;

 /*---------------------------------*/
        /*Getting option on the input link */

        if (prom_getopt(liaison[l].nom, "x", x_pos) == 2)
            PX = atoi(x_pos);
        if (prom_getopt(liaison[l].nom, "y", y_pos) == 2)
            PY = atoi(y_pos);
        if (prom_getopt(liaison[l].nom, "i", interface) == 2)
            I = atoi(interface);
        if (prom_getopt(liaison[l].nom, "m", mem) == 2)
	  max_recall = atoi(mem);
        if (prom_getopt(liaison[l].nom, "s", type_sortie) == 2)
            t_sortie = atoi(type_sortie);
        if (prom_getopt(liaison[l].nom, "d", display_param) > 0)
        {
            if (display_param[0] == '\0')   /* Test de si d est suivi d'un nombre ou non, si non,        */
                display = 1;    /* l'affichage aura lieu. Ainsi nous avons d equivalent a d1 */
            else
                display = atoi(display_param);
        }

        if (prom_getopt(liaison[l].nom, "f", f_opt) > 0)
        {
            if (f_opt[0] == '\0')   /* Test de si f est suivi d'un nombre ou non, si non, le */
                file_flag = 1;  /* fichier sera cree. Ainsi nous avons f equivalent a f1 */
            else
                file_flag = atoi(f_opt);
        }
        if (prom_getopt(liaison[l].nom, "c", cpt_param) > 0)
        {
            if (cpt_param[0] == '\0')   /* idem */
                cpt_scale = 1;
            else
                cpt_scale = (float) (atof(cpt_param));
            cpt = 1;
        }
        if (prom_getopt(liaison[l].nom, "tsim", time_sim) > 0)
        {
            if (time_sim[0] == '\0')
                time_sim_flag = 1;  /* idem */
            else
                time_sim_flag = atoi(time_sim);
        }
        if (prom_getopt(liaison[l].nom, "tabs", time_abs) > 0)
        {
            if (time_abs[0] == '\0')
                time_abs_flag = 1;  /* idem */
            else
                time_abs_flag = atoi(time_abs);
        }
/*---------------------------------------*/
        /* Getting dimensions of the input group */

        N = def_groupe[gp_input].taillex;
        M = def_groupe[gp_input].tailley;

        inc = def_groupe[gp_input].nbre / (N * M);

        if (inc > 1)
            nb_cell_to_display = N * M - def_groupe[gp_input].nbre;
        else
            nb_cell_to_display = N * M;


        recall = malloc(max_recall * sizeof(float *));
        for (i = 0; i < max_recall; i++)
        {
            recall[i] = malloc(nb_cell_to_display * sizeof(float *));
            for (j = 0; j < nb_cell_to_display; j++)
            {
                recall[i][j] = 0.5;
            }

        }

 /*---------------------------*/
        /* Creating destination file */

        if (file_flag == 1)
        {
            sprintf(string, "%s.%d.by.%d.SAVE", def_groupe[gp_input].nom,
                    gp_input, numero);
            file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
            strcpy(file_name, string);
        }

/*--------------------------------------*/
/* Getting start time of the simulation */

        gettimeofday(&StartTime, (void *) NULL);
        start_time_sec = StartTime.tv_sec;
        start_time_msec = StartTime.tv_usec;

 /*----------------------------------------------------------------------------*/
        /* Putting right thing in right place in the MyData_f_save_activity structure */

        my_data = malloc(sizeof(MyData_f_display_phase_space));
        my_data->recall = recall;
        my_data->gp_input = gp_input;
        my_data->max_recall = max_recall;
        my_data->nb_cell_to_display = nb_cell_to_display;
        my_data->PX = PX;
        my_data->PY = PY;
        my_data->I = I;
        my_data->N = N;
        my_data->M = M;
        my_data->inc = inc;
        my_data->type_sortie = t_sortie;
        my_data->display = display;
        my_data->file_flag = file_flag;
        my_data->file_name = file_name;
        my_data->start_time_sec = start_time_sec;
        my_data->start_time_msec = start_time_msec;
        my_data->time_sim_flag = time_sim_flag;
        my_data->time_abs_flag = time_abs_flag;
        my_data->cpt = cpt;
        my_data->cpt_scale = cpt_scale;
        def_groupe[numero].data = (void *) my_data;

        if (display == 1)
        {
            printf
                ("Option du %s.%d:%dX%d sur interface %d avec max_recall = %d et type_sortie = %d \n",
                 __FUNCTION__, numero, PX, PY, I, max_recall, t_sortie);
        }
        else
        {
            printf("Option du %s.%d:no_display\n", __FUNCTION__, numero);
        }
        if (file_name != NULL)
            printf("ecriture sur %s: incx=%f\n", file_name, cpt * cpt_scale);
    }
    else
    {
        my_data = (MyData_f_display_phase_space *) def_groupe[numero].data;
        recall = my_data->recall;
        gp_input = my_data->gp_input;
        max_recall = my_data->max_recall;
        nb_cell_to_display = my_data->nb_cell_to_display;
        PX = my_data->PX;
        PY = my_data->PY;
        I = my_data->I;
        N = my_data->N;
        M = my_data->M;
        inc = my_data->inc;
        t_sortie = my_data->type_sortie;
        display = my_data->display;
        file_flag = my_data->file_flag;
        file_name = my_data->file_name;
        start_time_sec = my_data->start_time_sec;
        start_time_msec = my_data->start_time_msec;
        time_sim_flag = my_data->time_sim_flag;
        time_abs_flag = my_data->time_abs_flag;
        cpt = my_data->cpt;
        cpt_scale = my_data->cpt_scale;
    }


    if (file_name != NULL)
    {
#ifdef DEBUG
        printf("debut ecriure fichier\n");
#endif
        /*Open the file */
        fp = fopen(file_name, "a");
        if (fp == NULL)
        {
            printf("Impossible to create file %s\n", file_name);
            exit(EXIT_FAILURE);
        }


 /*------------------------------------------------*/
        /* Saving neurone activity of input group in file */

        if (time_abs_flag == 1)
        {
            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = CurrentTime.tv_sec;
            current_time_msec = CurrentTime.tv_usec;
            fprintf(fp, "%ld.%ld ", current_time_sec, current_time_msec);
        }
        if (time_sim_flag == 1)
        {

            gettimeofday(&CurrentTime, (void *) NULL);
            current_time_sec = CurrentTime.tv_sec;
            current_time_msec = CurrentTime.tv_usec;
            time_of_sim_sec = current_time_sec - start_time_sec;
            time_of_sim_msec = current_time_msec - start_time_msec;
            fprintf(fp, "%ld.%ld ", time_of_sim_sec, time_of_sim_msec);
        }
        if (cpt_scale > 0)
        {

            fprintf(fp, "%f ", cpt * cpt_scale);
            cpt++;
            my_data->cpt = cpt;
        }
        for (i = 0; i < N; i++)
        {
            for (j = 0; j < M; j++)
            {
                pos = i + j * N;
                neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
                if (t_sortie == 0)
                    act = neurone[neur].s;
                else if (t_sortie == 1)
                    act = neurone[neur].s1;
                else if (t_sortie == 2)
                    act = neurone[neur].s2;

                fprintf(fp, "%f ", act);
            }
        }
        fprintf(fp, "\n");
        fclose(fp);
#ifdef DEBUG3
        printf("fin ecriture fichier\n");
        pos = i + j * N;
        neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
        if (t_sortie == 0)
            act = neurone[neur].s;
#endif
    }

    if (display == 1)
    {
#ifdef DEBUG
        printf("debut display\n");
#endif
        for (i = 0; i < max_recall - 1; i++)
            for (j = 0; j < nb_cell_to_display; j++)
                recall[i][j] = recall[i + 1][j];

        for (i = 0; i < N; i++)
            for (j = 0; j < M; j++)
            {
                pos = i + j * N;
                neur = def_groupe[gp_input].premier_ele + pos * inc + inc - 1;
                if (t_sortie == 0)
                    act = neurone[neur].s;
                else if (t_sortie == 1)
                    act = neurone[neur].s1;
                else if (t_sortie == 2)
                    act = neurone[neur].s2;

                recall[max_recall - 1][pos] = act;
            }

#ifdef DEBUG
        for (i = 0; i < max_recall; i++)
        {
            for (j = 0; j < nb_cell_to_display; j++)
            {
                printf("%f\t", recall[i][j]);
            }

            printf("\n");
        }
#endif
        affiche_phase_space(numero);
    }
#ifdef DEBUG
    printf("fin %s\n", __FUNCTION__);
#endif
}


void affiche_phase_space(int numero)
{
#ifndef AVEUGLE

    int i, j;
    float repx, taille = 400.;
    TxPoint point1;
    int couleur[10];
    MyData_f_display_phase_space *my_data;

    my_data = (MyData_f_display_phase_space *) def_groupe[numero].data;


    couleur[0] = 0;
    couleur[1] = 17;
    couleur[2] = 18;
    couleur[3] = 19;
    couleur[4] = 8;
    couleur[5] = 20;
    couleur[6] = 21;
    couleur[7] = 22;
    couleur[8] = 23;
    couleur[9] = 14;



    repx = 35 + my_data->PX;

    point1.x = repx;
    point1.y = my_data->PY;


    if (my_data->I == 1)
    {
        TxDessinerRectangle(&image1, blanc, TxPlein, point1,
                            (int) (10 * my_data->max_recall + repx),
                            (int) (taille + 10), 1);
/*  	TracerAxe(&image1,bleu,bleu,(int)repx,(int)repy+my_data->PY,(int)(repx+taille),my_data->PY,0,0);*/
    }
    else
    {
        TxDessinerRectangle(&image2, blanc, TxPlein, point1,
                            (int) (10 * my_data->max_recall + repx),
                            (int) (taille + 10), 1);
/*  	TracerAxe(&image1,bleu,bleu,(int)repx,(int)repy+my_data->PY,(int)(repx+taille),my_data->PY,0,0);*/
    }




    for (i = 0; i < my_data->max_recall - 1; i++)
    {
        for (j = 0; j < my_data->nb_cell_to_display; j++)
        {
            point1.x = 3 * (taille - taille * my_data->recall[i][j] + my_data->PY) - 700;   /*200 */
            point1.y = 3 * (taille - taille * my_data->recall[i + 1][j] + my_data->PY) - 700;   /* 400 */
            if (my_data->I == 1)
            {
                TxDessinerPoint(&image1, couleur[8], point1);
                if (j >= 16)    /*16, c'est blanc */
                    TxDessinerPoint(&image1, j + 1, point1);
            }
            else
            {
                TxDessinerPoint(&image2, couleur[8], point1);
                if (j >= 16)    /*16, c'est blanc */
                    TxDessinerPoint(&image2, j + 1, point1);
            }
        }

    }

    if (my_data->I == 1)
        TxFlush(&image1);
    else
        TxFlush(&image2);

#endif
    (void)numero;
}
