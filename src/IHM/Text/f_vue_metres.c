/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \defgroup f_vue_metres f_vue_metres
 \ingroup libIHM

 \brief view meter

 \details

 \section Description

 Allow to graphically change the values of the neurons.
 If mode blind -> init value is transmitted to next group.

 \section Options

The options can have negative values.

 -    -i[value] : Initial value of the neuron
 -    -m[value] : Minimal value of the neuron (default 0)
 -    -M[value] : Maximal value of the neuron (default 200)
 -    -n[name]  : Name of the view metter
 -    -s[step]  : Steps between values
 -    -f[file_path]  : A file with initial values of the neurons, and overwritten with current values in the destroy function


 \file
 \ingroup f_vue_metres


 Modification
 - date 2/11/2009
 - author: Arnaud Blanchard
 - description: we add as many view_meters as neurons
 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct vue_metres
{
   float init, min, max, step;
   char path_file_init[256];
   int boolInit;
#ifndef AVEUGLE
   TxFloatVuMetre *view_meters;
#endif
} type_vue_metres;

void new_f_vue_metres(int gpe_sortie)
{
   int lien_entrant = -1;
   float min = 0.f, max = 200.f, step = 0.01f;
   char param_link[256];
   char name[255];
   type_vue_metres *vue_mettre_gpe=NULL;
   int neuroneInit = 0;
   char path_file_init[256];
   FILE *f = NULL;
   int error;
   float x;
   int i;


#ifndef AVEUGLE
   TxFloatVuMetre *view_meter;
#endif

   type_groupe *group = &def_groupe[gpe_sortie];

   if (def_groupe[gpe_sortie].data == NULL)
   {
      vue_mettre_gpe = ALLOCATION(type_vue_metres);

      lien_entrant = find_input_link(gpe_sortie, 0);

      /** check if there is a link */
      if (lien_entrant == -1)
      {
         vue_mettre_gpe->min = min;
         vue_mettre_gpe->max = max;
         vue_mettre_gpe->step = step;
         vue_mettre_gpe->init = vue_mettre_gpe->min;
         sprintf(name, "%8s:", group->no_name);
      }
      else
      {
         if (prom_getopt_float(liaison[lien_entrant].nom, "-m", &vue_mettre_gpe->min) != 2) vue_mettre_gpe->min = min;
         if (prom_getopt_float(liaison[lien_entrant].nom, "-M", &vue_mettre_gpe->max) != 2) vue_mettre_gpe->max = max;
         if (prom_getopt_float(liaison[lien_entrant].nom, "-s", &vue_mettre_gpe->step) != 2) vue_mettre_gpe->step = step;
         if (prom_getopt_float(liaison[lien_entrant].nom, "-i",  &vue_mettre_gpe->init) != 2)   vue_mettre_gpe->init = vue_mettre_gpe->min;
         if (prom_getopt(liaison[lien_entrant].nom, "-f", path_file_init) == 2 )
         {
            neuroneInit = 1;
            strcpy(vue_mettre_gpe->path_file_init,path_file_init);
            dprints(" name of the input init file  = %s .\n",path_file_init);

            f = fopen(path_file_init, "r");
            if (f == NULL)
            {
               EXIT_ON_ERROR("unable to open file %s [%s] \n", path_file_init,def_groupe[gpe_sortie].no_name);
            }

         }
         if (prom_getopt(liaison[lien_entrant].nom, "-n", param_link) == 2) strcpy(name, param_link);
         else sprintf(name, "%8s:", group->no_name);
      }

      def_groupe[gpe_sortie].data = vue_mettre_gpe;


#ifndef AVEUGLE
      vue_mettre_gpe->view_meters = MANY_ALLOCATIONS(group->nbre, TxFloatVuMetre);


      for (i=0; i<group->nbre; i++)
      {
         view_meter = &vue_mettre_gpe->view_meters[i];

         strcpy(view_meter->nom, name);
         view_meter->val = ALLOCATION(float);
         if(!neuroneInit) 	  *view_meter->val = vue_mettre_gpe->init;
         else{
            error = fscanf(f, "%f", &x);
            if (error == EOF || error == 0)
            {
               EXIT_ON_ERROR("file %s size does not match group of neurons %s size \n", path_file_init, def_groupe[gpe_sortie].no_name);
            }
            *view_meter->val = x;
         }

         TxAddVuMetre_opt(&fenetre3, view_meter, vue_mettre_gpe->min, vue_mettre_gpe->max, vue_mettre_gpe->step);
      }

#else

      for (i=0; i<group->nbre; i++)
      {
         if(!neuroneInit) 	  neurone[group->premier_ele+i].s2 = neurone[group->premier_ele+i].s = neurone[group->premier_ele+i].s1 = vue_mettre_gpe->init;
         else{
            error = fscanf(f, "%f", &x);
            if (error == EOF || error == 0)
            {
               EXIT_ON_ERROR("file %s size does not match group of neurons %s size \n", path_file_init, def_groupe[gpe_sortie].no_name);
            }
            neurone[group->premier_ele+i].s2 = neurone[group->premier_ele+i].s = neurone[group->premier_ele+i].s1 = x;
         }
      }
#endif
      vue_mettre_gpe->boolInit = 0;
      if(neuroneInit)		fclose(f);
      group->data = vue_mettre_gpe;
   }

}

void function_vue_metres(int gpe_sortie)
{
   int i;
   type_groupe *group = &def_groupe[gpe_sortie];
   type_vue_metres *vue_mettre_gpe = group->data;

#ifndef AVEUGLE  
   for (i=0; i<group->nbre; i++)
   {
      neurone[group->premier_ele+i].s2 = neurone[group->premier_ele+i].s = neurone[group->premier_ele+i].s1 = *(vue_mettre_gpe->view_meters[i].val);
   }
#else
   int error;
   float x;
   FILE *f;
   if(!vue_mettre_gpe->boolInit)
   {
      vue_mettre_gpe->boolInit = 1;
      if( strlen(vue_mettre_gpe->path_file_init) == 0 )
      {
         for (i = 0; i < group->nbre; i++)
         {
            neurone[group->premier_ele + i].s2 = neurone[group->premier_ele + i].s = neurone[group->premier_ele + i].s1 = vue_mettre_gpe->init;
         }
      }
      else
      {
         f = fopen(vue_mettre_gpe->path_file_init, "r");
         if (f == NULL)
         {
            EXIT_ON_ERROR("unable to open file %s [%s] \n", vue_mettre_gpe->path_file_init,def_groupe[gpe_sortie].no_name);
         }
         for (i=0; i<group->nbre; i++)
         {
            error = fscanf(f, "%f", &x);
            if (error == EOF || error == 0)
            {
               EXIT_ON_ERROR("file %s size does not match group of neurons %s size \n", vue_mettre_gpe->path_file_init, def_groupe[gpe_sortie].no_name);
            }
            neurone[group->premier_ele+i].s2 = neurone[group->premier_ele+i].s = neurone[group->premier_ele+i].s1 = x;
         }
         fclose(f);
      }
   }

#endif

   return;
}

/** Les vus metres devraient etre supprimes ici */
void destroy_vue_metres(int gpe_sortie)
{

#ifndef AVEUGLE
   int i;
   type_groupe *group = &def_groupe[gpe_sortie];
   type_vue_metres *vue_mettre_gpe = group->data;
   FILE *f;
   if( strlen(vue_mettre_gpe->path_file_init) != 0 )
   {

      f = fopen(vue_mettre_gpe->path_file_init, "w");
      if (f != NULL)
      {
         for (i=0; i<group->nbre; i++)
         {
            fprintf(f, "%f ", *(vue_mettre_gpe->view_meters[i].val));
         }
         fclose(f);
      }
   }
#endif
   (void) gpe_sortie;
}

