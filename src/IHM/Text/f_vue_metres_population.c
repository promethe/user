/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup f_vue_metres_population f_vue_metres_population
 \ingroup libIHM

 \brief view meter

 \details

 \section Description

 Allow to graphically change the values of the neurons. It is equivalent to f_vue_metres followed by f_extract_val_to_vector.
 If mode blind -> init value is transmitted to next group.


 \section Options

 -    -i[value] : Initial position of the neuron (default 0)
 -    -m[value] : Minimal position of the neuron (default 0)
 -    -M[value] : Maximal position of the neuron (default 1)
 -    -n[name]  : Name of the view metter

 \remark At this moment it only works with one line but in the future we should add a vertical view meter.

 \file
 \ingroup f_vue_metres_population


 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct vue_metres_population {
  float init, min, max, step, gain;
#ifndef AVEUGLE
TxFloatVuMetre *view_meter;
#endif
} type_vue_metres_population;


void new_vue_metres_population(int gpe_sortie)
{
  int link_id = -1;
  float min, max;
  char param_link[256], *link_name;
  char name[255];
  type_vue_metres_population *vue_mettre_gpe;
  type_groupe *group;


#ifdef AVEUGLE
  int i;
  int id;
#endif

  group = &def_groupe[gpe_sortie];
  vue_mettre_gpe = ALLOCATION(type_vue_metres_population);
  min=0;
  max = group->taillex;

  link_id = find_input_link(gpe_sortie, 0);
  link_name = liaison[link_id].nom;

  /** check if there is a link */
  if (link_id == -1)
  {
    vue_mettre_gpe->min = min;
    vue_mettre_gpe->max = max;
    vue_mettre_gpe->init =  vue_mettre_gpe->min;
    sprintf(name, "%8s:", group->no_name);
  }
  else
  {
    if (prom_getopt_float(link_name, "-m", &vue_mettre_gpe->min) != 2) vue_mettre_gpe->min = min;
    if (prom_getopt_float(link_name, "-M", &vue_mettre_gpe->max) != 2) vue_mettre_gpe->max = max;
    if (prom_getopt_float(link_name, "-i", &vue_mettre_gpe->init) != 2) vue_mettre_gpe->init = vue_mettre_gpe->min;
    if (prom_getopt(link_name, "-n", param_link) == 2) strcpy(name, param_link);
    else sprintf(name, "%8s:", group->no_name);

    def_groupe[gpe_sortie].data = vue_mettre_gpe;
  }

  vue_mettre_gpe->gain = group->taillex / (vue_mettre_gpe->max -  vue_mettre_gpe->min);


  if (group->taillex == 1)
  {
    PRINT_WARNING("With taillex=1 this is equivalent to f_bias");
    vue_mettre_gpe->step = 1;
  }
  else   vue_mettre_gpe->step = (vue_mettre_gpe->max -  vue_mettre_gpe->min) / (group->taillex - 1);


#ifndef AVEUGLE
    vue_mettre_gpe->view_meter = ALLOCATION(TxFloatVuMetre);


      strcpy( vue_mettre_gpe->view_meter->nom, name);
      vue_mettre_gpe->view_meter->val = ALLOCATION(float);
      *vue_mettre_gpe->view_meter->val = vue_mettre_gpe->init;

      TxAddVuMetre_opt(&fenetre3, vue_mettre_gpe->view_meter, vue_mettre_gpe->min, vue_mettre_gpe->max, vue_mettre_gpe->step);

      group->data = vue_mettre_gpe;
#else
  id =  group->premier_ele + (int)(vue_mettre_gpe->init-vue_mettre_gpe->min)*vue_mettre_gpe->gain;

  if (id >= group->premier_ele + group->taillex) id = group->premier_ele + group->taillex -1;

  for (i = 0; i < group->taillex; i++)
  {
    neurone[group->premier_ele + i].s2 = neurone[group->premier_ele + i].s = neurone[group->premier_ele + i].s1 = 0.0;
  }
  neurone[id].s = neurone[id].s1 = neurone[id].s2 = 1.0;
#endif
}

void function_vue_metres_population(int gpe_sortie)
{

  int i, id;
  type_groupe *group = &def_groupe[gpe_sortie];
  type_vue_metres_population *vue_mettre_gpe = group->data;

#ifndef AVEUGLE

  id = group->premier_ele + (int)( (*vue_mettre_gpe->view_meter->val - vue_mettre_gpe->min) * vue_mettre_gpe->gain);
  if (id >= group->premier_ele + group->taillex) id = group->premier_ele + group->taillex - 1;

  for (i = 0; i < group->taillex; i++)
  {
    neurone[group->premier_ele + i].s2 = neurone[group->premier_ele + i].s = neurone[group->premier_ele + i].s1 = 0.0;
  }

  neurone[id].s = neurone[id].s1 = neurone[id].s2 = 1.0;
#else
  id =  group->premier_ele + (int)(vue_mettre_gpe->init-vue_mettre_gpe->min)*vue_mettre_gpe->gain;

    if (id >= group->premier_ele + group->taillex) id = group->premier_ele + group->taillex -1;

    for (i = 0; i < group->taillex; i++)
    {
      neurone[group->premier_ele + i].s2 = neurone[group->premier_ele + i].s = neurone[group->premier_ele + i].s1 = 0.0;
    }
    neurone[id].s = neurone[id].s1 = neurone[id].s2 = 1.0;
#endif


  return;
}

/** Les vus metres devraient etre supprimes ici */
void destroy_vue_metres_population(int gpe_sortie)
{
  (void) gpe_sortie;
}

