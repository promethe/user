/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_demande_angle_degres f_demande_angle_degres
\brief 
 

\section Modified
- author: N.Cuperlier
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Fonction demandant un angle entre 0 et 360� et active le neurone correspondant.
Codage de l'angle par une poupulation de neurone avec mise a l'echelle. Le nombre de neurone repesente l'inverse du pas de discretisation. on peut specifie la plage code en donnant sur le lien de la boite la borne de debut (0 par defaut) et de fin (360) et un angle de rotation (deafut=180) indiquant la rotation de la plage ex par defaut =180 sortie: [-180;180[ le neurone representant un angle de 0
� est code par le neurone a l'index: longueur/2 au lieu de l'index 0 si [0;360[

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-Kernel_Function/find_input_link()

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/*#define DEBUG*/
void function_demande_angle_degres(int numero)
{
    int exit = -1;              /*valeur a rentree pour ne pas avoir d'activite en sortie */
    int deb, longueur;
    int i, index,test;
    int nlink;
    int res = -1;
    float rot = 180.;
    char srot[255];
    float offset = 0., val = -999.;
    /* char * str; */

#ifdef DEBUG
    dprints("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
#endif
    cprints("Function_demande_angle_degres(%d)\n", numero);


    /*get the string of the input link */
    nlink = find_input_link(numero, 0);
    if (nlink > -1)
    {
#ifdef DEBUG
        dprints("info sur la liaison: %s\n", liaison[nlink].nom);
#endif

        res = prom_getopt(liaison[nlink].nom, "--rot=", srot);
        if (res > 0)
            sscanf(srot, "%f", &rot);
    }


    deb = def_groupe[numero].premier_ele;
    longueur = def_groupe[numero].nbre;


    offset = rot * (float) longueur / 360;


    cprints("Output range [-180;180[, meanning angle=0� is neurone n�: %f\n",
           offset);

/*Get the input from user*/
    do
    {
        cprints("Angle:  [0;360[ (%d means no activity)\n", exit);
        test=scanf("%f", &val);
        if(test == 0){kprints("Erreur dans la lecture du scanf f_demande_angle_degres");}
    }
    while (val >= 360 || val < exit);


    /*R.A.Z */
    for (i = deb; i < deb + longueur; i++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    }
    if (isdiff(val, exit))
    {
        if (val > rot)
            val -= 360;
        /*Compute the index */
        index = (int) (((float) longueur * val / 360) + offset);
        /*Set the neuron at the specified index */
        if (index >= 0)
            neurone[index + deb].s = neurone[index + deb].s1 =
                neurone[index + deb].s2 = 1.;
#ifdef DEBUG
        dprints
            ("Angle:%f degre soit %f radians, longueur:%d, neurone set:%d, offset:%f\n",
             val, val * 2 * 3.1415927 / 360, longueur, deb + index, offset);
#endif
    }
    else
    {
        cprints("Pas d'activite demande sur f_demande_angle_degres!");
    }

#ifdef DEBUG
    dprints("===========sortie de %s\n", __FUNCTION__);
#endif
}
