/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*************************************************************
\file  f_display_frame_rate.c
\brief

Author: P. Gaussier
Created: 19/08/06
Modified:
- author: 
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Donne la frequence entre appels sucessifs de la fonction.

Macro:
-none

Local variables:


Global variables:

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <sys/time.h>

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include "basic_tools.h"
#include <net_message_debug_dist.h>


/*#define DEBUG 1*/
#define CHEMIN_MAX 100

typedef struct MyData_f_display_fr
{
   struct timeval Total_Time;  /* somme des temps sur periode_moy iteration */
   float Secondes2;            /* carre de la somme des temps sur periode_moy iteration */
   struct timeval Init_Time;   /* valeur horloge lors de la 1ere iter de periode_moy */
   int pos_x, pos_y, no_image;
   int periode_moy;
   int compteur;               /* compteur allant de 0 a periode_moy */
   int print_screen;
   FILE * resultat;
   int lecture;
   float temps;
} MyData_f_display_fr;

void new_display_frame_rate(int gpe)
{
   int i, l;
   int no_image = -1;
   int pos_x = 0, pos_y = 0;
   int periode_moy = 1;
   char param_link[255];
   MyData_f_display_fr *my_data;
   int print_screen=0;
   char chemin[CHEMIN_MAX];
   int lecture = 0;

#ifdef DEBUG
   printf("-------------------------------------------------\n");

   printf("init du frame rate gpe %s\n", def_groupe[gpe].no_name);
   fflush(stdout);
#endif

   l = 0;
   i = find_input_link(gpe, l);
   while (i != -1)
   {
      if (prom_getopt(liaison[i].nom, "I", param_link) == 2)
      {
         no_image = atoi(param_link);
      }
      if (prom_getopt(liaison[i].nom, "x", param_link) == 2)
      {
         pos_x = atoi(param_link);
      }
      if (prom_getopt(liaison[i].nom, "y", param_link) == 2)
      {
         pos_y = atoi(param_link);
      }
      if (prom_getopt(liaison[i].nom, "n", param_link) == 2)
      {
         periode_moy = atoi(param_link);
      }
      if (prom_getopt(liaison[i].nom, "p", param_link) >= 1)
      {
         print_screen = 1;
      }

      if(prom_getopt(liaison[i].nom, "f", param_link) == 2)
      {
         lecture = 1;
         strncpy(chemin,param_link,CHEMIN_MAX);
      }
      i = find_input_link(gpe, l);
      l++;
   }

   my_data = ALLOCATION(MyData_f_display_fr);

   my_data->temps = 0.;
   if(lecture==1)
   {
      my_data->lecture = 1;
      my_data->resultat = fopen(chemin, "w");
   }
   else my_data->lecture=0;
   my_data->pos_x = pos_x;
   my_data->pos_y = pos_y;
   my_data->no_image = no_image;
   my_data->periode_moy = periode_moy;
   my_data->Total_Time.tv_sec = 0;
   my_data->Total_Time.tv_usec = 0;
   my_data->compteur = 0;
   my_data->print_screen = print_screen;
   def_groupe[gpe].data = (MyData_f_display_fr *) my_data;
}

/*---------------------------------------------------------------------*/

void function_display_frame_rate(int gpe)
{
   int periode_moy, compteur;
   float nb_sec, nb_sec2, ecart;
   long Secondes = 0, MicroSecondes = 0;
   float fSecondes;
   struct timeval my_TimeTrace = { -1, -1 };
   MyData_f_display_fr *my_data;
   char chaine[512];
   char monTab[40];
   int lecture;
#ifndef AVEUGLE
   int print_screen=0;
   int pos_x = 0, pos_y = 0;
#endif
#ifndef AVEUGLE
   TxPoint position;
   TxDonneesFenetre *image;
#endif

#ifdef DEBUG
   printf("-------------------------------------------------\n");
   printf("Calcul du frame rate gpe %s\n", def_groupe[gpe].no_name);
   fflush(stdout);
#endif
   if (def_groupe[gpe].data == NULL)
   {
      EXIT_ON_GROUP_ERROR(gpe, "new_display_frame_rate has failed to allocate the local data\n");
   }

   my_data = (MyData_f_display_fr *) (def_groupe[gpe].data);

#ifndef AVEUGLE
   print_screen = my_data->print_screen;
   pos_x = my_data->pos_x;
   pos_y = my_data->pos_y;
   position.x = pos_x;
   position.y = pos_y;
   if (my_data->no_image == 1)
      image = &image1;
   else
      image = &image2;
#endif
   lecture = my_data->lecture;
   periode_moy = my_data->periode_moy;
   compteur = my_data->compteur;

   gettimeofday(&my_TimeTrace, (void *) NULL);
   if (compteur == 0)
   {
      my_data->Total_Time.tv_sec = 0;
      my_data->Total_Time.tv_usec = 0;
      my_data->Secondes2 = 0.;
   }
   else
   {
      if (my_TimeTrace.tv_usec >= my_data->Init_Time.tv_usec)
      {
         Secondes = my_TimeTrace.tv_sec - my_data->Init_Time.tv_sec;
         MicroSecondes = my_TimeTrace.tv_usec - my_data->Init_Time.tv_usec;
      }
      else
      {
         Secondes = my_TimeTrace.tv_sec - my_data->Init_Time.tv_sec - 1;
         MicroSecondes =
               1000000 + my_TimeTrace.tv_usec - my_data->Init_Time.tv_usec;
      }
      my_data->Total_Time.tv_sec = my_data->Total_Time.tv_sec + Secondes;
      my_data->Total_Time.tv_usec =
            my_data->Total_Time.tv_usec + MicroSecondes;
      fSecondes = (float) Secondes + ((float) MicroSecondes) / 1000000.;
      my_data->Secondes2 = my_data->Secondes2 + fSecondes * fSecondes;
   }
   my_data->Init_Time.tv_sec = my_TimeTrace.tv_sec;
   my_data->Init_Time.tv_usec = my_TimeTrace.tv_usec;
   compteur++;

   /*   printf("%ld %ld | %ld s %ld us \n",Secondes,MicroSecondes,my_data->Total_Time.tv_sec,my_data->Total_Time.tv_usec); */

   if (compteur > periode_moy)
   {
      compteur = compteur - 1;
      nb_sec = (float) my_data->Total_Time.tv_sec;
      nb_sec = nb_sec + ((float) my_data->Total_Time.tv_usec) / 1000000.;
      my_data->temps = my_data->temps + (float)nb_sec;
      nb_sec2 = my_data->Secondes2;
      /*ecart=sqrt(nb_sec*nb_sec-nb_sec2)/compteur; */
      nb_sec = nb_sec / compteur;
      nb_sec2 = nb_sec2 / compteur;
      ecart = sqrt(nb_sec2 - nb_sec * nb_sec);
      /*printf("%e %e \n",nb_sec*nb_sec,nb_sec2); */
      sprintf(chaine,"\n gpe %s , periode = %f +/- %f , Frame rate = %.1f +/- %.2f\n",
            def_groupe[gpe].no_name, nb_sec, ecart, 1. / nb_sec, ecart / nb_sec);
      if(lecture==1)
      {
         /*my_data->temps = my_data->temps + (float)nb_sec;*/
         snprintf(monTab, 40, "temps= %f framerate= %.1f\n", my_data->temps, (1. /nb_sec));
         fputs(monTab,my_data->resultat);
      }
#ifndef AVEUGLE
      TxDessinerRectangle(image, noir, TxPlein, position, 600, 11, 0);
      position.y = position.y + 10;
      TxEcrireChaine(image,blanc, position, chaine, NULL);
      TxDisplay(image); /*TxFlush(image);*/
      if(print_screen==1)
         cprints("%s \n", chaine);
#else
      cprints("%s \n", chaine);
#endif 

      compteur = 0;
      my_data->Init_Time.tv_sec = my_TimeTrace.tv_sec;
      my_data->Init_Time.tv_usec = my_TimeTrace.tv_usec;
   }
   my_data->compteur = compteur;

}

void destroy_frame_rate(int gpe)
{
   MyData_f_display_fr *my_data;
   my_data = (MyData_f_display_fr *) (def_groupe[gpe].data);

   if(my_data==NULL)
   {
      printf("ERROR %s, data field was not initialized \n",__FUNCTION__);
      return;
   }
   if(my_data->lecture==1)
   {
      fclose(my_data->resultat);
   }
}
