/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIHM
\defgroup f_controle_vigilence f_controle_vigilence
\brief 
 

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
-----------------------------------------------------------------------
 Fonction VB  :    Fixe la vigilence en fonction du niveau de
  reconnaissance maximum constate sur le PTM.
  Si reconnaissance inferieure au seuil alors vigilence=1 sinon on est
   pret d'un endroit connu et vigilence=0.1
  Pour le moment, le seuil est fixe EMPIRIQUEMENT a 0.7!!!
  La valeur du seuil est fixee sur le lien
  Ah oui au fait, je m'en sers pour la planification
-----------------------------------------------------------------------

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#ifndef AVEUGLE
#include <Struct/button.h>
#include <graphic_Tx.h>
#endif
#include <Global_Var/NN_Core.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void helk()
{
    dprints("hello\n");
}

/*static void (* button_fct_list)()[2];*/

void function_add_button(int gpe_sortie)
{
#ifndef AVEUGLE
    int lien_entrant = -1;
    char param_link[256];
    char *name = NULL;
    button *button_gpe;

    (void) name; // (unused)

    if (def_groupe[gpe_sortie].data == NULL)
    {
        name = (char *) malloc(255);
        lien_entrant = find_input_link(gpe_sortie, 0);
        button_gpe = (button *) malloc(sizeof(button));
        if (button_gpe == NULL)
            dprints("pb!\n");
        /* definition du label du boutton */
        if (prom_getopt(liaison[lien_entrant].nom, "-l", param_link) == 2)
            strcpy(button_gpe->label, param_link);
        /*else
           sprintf(button_gpe->label,"button(%d)",gpe_sortie);
           printf("%f,%f,%f,%s,%d\n",min,max,step,name,gpe_sortie); */

        /*recherche de la fonction associee */
        if (prom_getopt(liaison[lien_entrant].nom, "-n", param_link) == 2)
            strcpy(button_gpe->name, param_link);
        /*else
           sprintf(button_gpe->name,"fct called by grp: %d",gpe_sortie);

           if(prom_getopt(liaison[lien_entrant].nom,"-N",param_link)==2)
           button_gpe->t=
           else */
        /*button_gpe->t=helk; */
        dprints("ok\n");
        button_gpe->t = ftab[0];
        button_gpe->done = 1;   /*bouton ajoutter */
        def_groupe[gpe_sortie].data = button_gpe;
    }

    button_gpe = def_groupe[gpe_sortie].data;
    if (button_gpe != NULL)
    {
        if (button_gpe->done == 1)
        {
            dprints("%p\n", (void*)button_gpe);
            add_button(param_link /*button_gpe->label */ , &fenetre3,
                       (void * (*)(void))(ftab[0]) /*button_gpe->t */ , 0);
            button_gpe->done = 0;
        }
    }
    else
    {
        EXIT_ON_ERROR ("impossible de recuperer la structuere de donne du boutton: %d\n", gpe_sortie);
        exit(-1);
    }
#endif
(void) gpe_sortie;
}
