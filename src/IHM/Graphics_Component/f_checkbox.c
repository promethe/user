/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
    ___NO_COMMENT___
    ___NO_SVN___

    \file 
    \brief 

    Author: Julien Hirel
    Created: 17/04/2008
    Modified:
    - author:
    - description:
    - date:

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
    -----------------------------------------------------------------------
    Ajoute une checkbox a la fenetre Range Control, qui permet de controler
    un parametre de la simulation
    -----------------------------------------------------------------------

    Macro:
    -none 

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/



#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/checkbox.h>
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

void new_checkbox(int gpe)
{
   int i, index, link, deb = def_groupe[gpe].premier_ele;
   int default_value = 0;
   int nb_boxes;
   char param_link[256];
   char name[256] = "\0";
   data_checkbox *my_data;

   if (def_groupe[gpe].data == NULL)
   {
      index = 0;
      link = find_input_link(gpe, index);

      while (link != -1)
      {

         if (prom_getopt(liaison[link].nom, "-n", param_link) == 2)
         {
            strcpy(name, param_link);
         }

         if (prom_getopt(liaison[link].nom, "-i", param_link) == 2)
         {
            default_value = atoi(param_link);
         }

         index++;
         link = find_input_link(gpe, index);
      }

      if (strlen(name) == 0)
      {
         sprintf(name, "value of group: %s", def_groupe[gpe].no_name);
      }

      nb_boxes = def_groupe[gpe].nbre;
      my_data = ALLOCATION(data_checkbox);
      my_data->values = MANY_ALLOCATIONS(nb_boxes, int);

      for (i = 0; i < nb_boxes; i++)
      {
         my_data->values[i] = default_value;
      }

#ifndef AVEUGLE
      strcpy(my_data->checkbox.nom, name);
      my_data->checkbox.widgets = MANY_ALLOCATIONS(nb_boxes, GtkWidget *);
      my_data->checkbox.val = my_data->values;
      my_data->checkbox.nb_boxes = def_groupe[gpe].nbre;
      TxAddCheckBoxes(&fenetre3, &(my_data->checkbox));
#endif

      def_groupe[gpe].data = my_data;
   }
}

void function_checkbox(int gpe)
{
   int i;
   int deb = def_groupe[gpe].premier_ele;
   data_checkbox *my_data = def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retreive data");
   }
#ifndef AVEUGLE
   for (i = 0; i < def_groupe[gpe].nbre; i++)
   {
      neurone[deb+i].s2 = neurone[deb+i].s = neurone[deb+i].s1 = my_data->values[i];
   }
#endif
}

void destroy_checkbox(int gpe)
{
   (void) gpe; // (unused)
   /* This is not good, but it works. We do not remove memory. SO, next time if we continue simulation it will not create a new "checkbox". May be, it is better to remove the widget and create "checkbox" again.
   data_checkbox *my_data = NULL;
   if (def_groupe[gpe].data != NULL)
   {
      my_data = (data_checkbox *) def_groupe[gpe].data;

   #ifndef AVEUGLE
      if (my_data->checkbox.widgets != NULL)
      {
    free(my_data->checkbox.widgets);
    my_data->checkbox.widgets = NULL;
      }
   #endif
      free(my_data->values);
      my_data->values = NULL;

      free(my_data);
      def_groupe[gpe].data = NULL;
   }*/
   dprints("destroy_checkbox(%s): Leaving function\n", def_groupe[gpe].no_name);
}



