/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_cadrer_depuis_projections.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Cette fonction decoupe a partir d'une image placee en entree via le lien "-image" une imagette.
   Les coordonnees de cette imagette
   sont fournies par les deux fonctions "f_projection verticale" et "f_projection horizontale" 
   via les liens respectifs "-vert" et "hor".
   Pour les coordonnees verticales, elle reprend le reseau de neurones de "f_projection verticale"
   et parcoure ce reseau d'une extremite jusqu'a son milieu et des que la valeur d'un neurone depasse 
   un certain seuil qu'on place sur le lien via "-Sseuil",
   on obtient la premiere coordonnee qui est convertie en pixel relativement a l'image d'entree. 
   La deuxieme coordonnee s'obtient de la meme maniere mais on parcourt cette fois le reseau depuis l'autre extremite vers le milieu.
   Pour les coordonnees horizontales, on realise la meme operation pour obtenir la coordonnee superieure et inferieure de l'imagette. 
   Dans l'application dans laquelle est destinee cette fonction (recherche de visage et cadrage),
   il est souvent necessaire d'estimer la coordonnee inferieure par rapport a la superieure
   (par rapport au proportion du visage et notamment la largeur du visage donnee par la difference entre les coordonnees verticales).
   C'est pourquoi on peut mettre sur le lien l'option "-Pvaleur" qui permet d'estimer la coordonnee inferieure souhaitee.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/cadrage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define IMAGE_LARGEUR 640
#define IMAGE_HAUTEUR 480

void function_cadrer_depuis_projections(int Gpe)
{
    int width, height, l, h;
    int n, link, i, j;
    int x1, x2, y1, y2;
    int i1;
    int co, li, numpixels, num;
    int val;
    float echx, echy;
    int p;
    prom_images_struct *image = NULL;
    prom_images_struct *p_result = NULL;
    char resultat[256];
    cadrage *temp = NULL;

    /*Mesure du temps de traitement dans la fonction */
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* Allocation prom_images_strct */
    if (def_groupe[Gpe].ext == NULL)
    {
        /*recuperation des infos sur les differents liens */
        temp = (cadrage *) malloc(sizeof(cadrage));
        def_groupe[Gpe].data = (cadrage *) temp;

        /*initialisation */

        temp->decalage = 0;
        temp->seuil = 0.33;
        temp->proportion = 2;
        temp->groupe_entree_image = -1;
        temp->groupe_entree_vert = -1;
        temp->groupe_entree_hor = -1;
        temp->largeur = IMAGE_LARGEUR;
        temp->hauteur = IMAGE_HAUTEUR;
        temp->Nxy = temp->largeur * temp->hauteur;
        temp->TailleGroupeX_vert = 0;
        temp->TailleGroupeY_hor = 0;
        temp->Debut_Gpe_vert = -1;
        temp->Debut_Gpe_hor = -1;

        n = -1;
        do
        {
            n++;
            link = find_input_link(Gpe, n);
            if (link != -1)
            {
                if (liaison[link].nom[0] == '-')
                {
                    switch (liaison[link].nom[1])
                    {
                    case 'i':
                        temp->groupe_entree_image = liaison[link].depart;
                        /*recuperation de l'image du groupe precedent et ses dimensions */
                        image =
                            (prom_images_struct *) def_groupe[temp->
                                                              groupe_entree_image].
                            ext;
                        if (image == NULL)
                        {
                            printf
                                ("Probleme dans f_cadrer_depuis_projection: pas d'image dans le groupe %d\n",
                                 temp->groupe_entree_image);
                            exit(EXIT_FAILURE);
                        }
                        else if (image->nb_band != 1)
                        {
                            printf
                                ("\n l'image en entree de f_cadrer_depuis_projection doit etre en noir et blanc !\n");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 'h':
                        temp->groupe_entree_hor = liaison[link].depart;
                        i = link;
                        /*lecture des informations sur le lien ne fonctionne pas parametre pris en compte initialiser en dur */
                        if (prom_getopt(liaison[i].nom, "-D", resultat) == 2)
                        {
                            temp->decalage = atoi(resultat);
                        }
                        if (prom_getopt(liaison[i].nom, "-G", resultat) == 2)
                        {
                            temp->decalage = -atoi(resultat);
                        }
                        if (prom_getopt(liaison[i].nom, "-S", resultat) == 2)
                        {
                            temp->seuil = atof(resultat);
                        }
                        temp->Debut_Gpe_hor =
                            def_groupe[temp->groupe_entree_hor].premier_ele;
                        temp->TailleGroupeY_hor =
                            def_groupe[temp->groupe_entree_hor].tailley;
                        break;
                    case 'v':
                        temp->groupe_entree_vert = liaison[link].depart;
                        j = link;
                        if (prom_getopt(liaison[j].nom, "-P", resultat) == 2)
                        {
                            temp->proportion = atof(resultat);
                        }
                        temp->Debut_Gpe_vert =
                            def_groupe[temp->groupe_entree_vert].premier_ele;
                        temp->TailleGroupeX_vert =
                            def_groupe[temp->groupe_entree_vert].taillex;
                        break;
                    default:
                        printf
                            (" link %s not recognized (f_cadrer_depuis_projections) \n",
                             liaison[link].nom);
                    }
                }
                else
                    printf
                        (" link %s not recognized (f_cadrer_depuis_projections) \n",
                         liaison[link].nom);
            }
        }
        while (link != -1);

        if (image == NULL)
        {
            printf
                ("Probleme dans f_cadrer_depuis_projection: pas d'image dans le groupe %d\n",
                 temp->groupe_entree_image);
            exit(EXIT_FAILURE);
        }

        /* initialisation locale */
        temp->Debut_Gpe = def_groupe[Gpe].premier_ele;
        temp->TailleGroupeX = def_groupe[Gpe].taillex;
        temp->TailleGroupeY = def_groupe[Gpe].tailley;

#ifdef DEBUG
        printf("TaillegroupeX %d\n", temp->TailleGroupeX);
        printf("TaillegroupeY %d\n", temp->TailleGroupeY);
        printf("Debut_Gpe_vert %d\n", temp->Debut_Gpe_vert);
        printf("TaillegroupeX_vert %d\n", temp->TailleGroupeX_vert);
        printf("Debut_Gpe_hor %d\n", temp->Debut_Gpe_hor);
        printf("TailleGroupeY_hor %d\n", temp->TailleGroupeY_hor);
#endif

        p_result = (prom_images_struct *) malloc(sizeof(prom_images_struct));   /* PG suppression : temp->hauteur*temp->largeur !!! */
        if (p_result == NULL)
        {
            printf
                ("ALLOCATION IMPOSSIBLE DANS f_cadrer_depuis_projections.c...! \n");
            exit(-1);
        }
        p_result->image_number = 1;
        p_result->nb_band = 1;
        def_groupe[Gpe].ext = p_result;

        /*allocation memoire pour l'image du cadre */
        temp->Nxy = temp->largeur * temp->hauteur;
        p_result->images_table[0] =
            (unsigned char *) malloc(temp->hauteur * temp->largeur *
                                     sizeof(unsigned char));


#ifdef DEBUG
        printf
            (" valeur des differentes variables dans la fonction de cadrage\n");
        printf(" decalage = %d\n", temp->decalage);
        printf(" seuil = %f \n", temp->seuil);
        printf(" proportion = %f\n", temp->proportion);
        printf(" groupe d'entree contenant l'image = %d\n",
               temp->groupe_entree_image);
        printf(" groupe d'entree contenant l'histogramme verticale = %d\n",
               temp->groupe_entree_vert);
        printf(" groupe d'entree contenant l'histogramme horizontale = %d\n",
               temp->groupe_entree_hor);
        printf(" largeur de l'image en entree = %d\n", temp->largeur);
        printf(" hauteur de l'image en entree = %d\n", temp->hauteur);
        printf(" nombre de pixels de l'image en entree = %d\n", temp->Nxy);
        printf
            (" numero du premier neurone du reseau de la projection du cadre = %d de taille = %d * %d \n",
             temp->Debut_Gpe, temp->TailleGroupeX, temp->TailleGroupeY);
        printf
            (" numero du premier neurone de l'histogramme verticale = %d de taille %d\n",
             temp->Debut_Gpe_vert, temp->TailleGroupeX_vert);
        printf
            (" numero du premier neurone de l'histogramme horizontale = %d de taille %d\n",
             temp->Debut_Gpe_hor, temp->TailleGroupeY_hor);
#endif

    }
    else
    {
        temp = (cadrage *) ((prom_images_struct *) def_groupe[Gpe].data);
        p_result = ((prom_images_struct *) def_groupe[Gpe].ext);
        image =
            (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;
    }

    /*Redimensionnement de la taille en fonction de l'image */
    l = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->
        sx;
    h = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->
        sy;
    if (l > IMAGE_LARGEUR)
    {
        printf("PB largeur %d image entree trop grande %d \n", l,
               temp->largeur);
        exit(EXIT_FAILURE);
    }
    if (h > IMAGE_HAUTEUR)
    {
        printf("PB hauteur %d image entree trop grande %d \n", h,
               temp->hauteur);
        exit(EXIT_FAILURE);
    }
    temp->largeur = l;
    temp->hauteur = h;
    temp->Nxy = temp->largeur * temp->hauteur;

    x1 = 0;
    x2 = temp->TailleGroupeX_vert - 1;

    /*parcours du reseau de neurones du groupe precednt histogramme verticale
       pour trouver la premiere coordonnee verticale du cadre */
    for (i = 0; i < temp->TailleGroupeX_vert; i++)
    {
        if (neurone[i + temp->Debut_Gpe_vert].s > temp->seuil)
        {
            x1 = i;
            break;
        }
    }

    /*parcours du reseau de neurones du groupe precedent histogramme verticale
       pour trouver la deuxieme coordonnee verticale du cadre */
    for (i = temp->TailleGroupeX_vert; i > 0; i--)
    {
        if (neurone[i + temp->Debut_Gpe_vert].s > temp->seuil)
        {
            x2 = i;
            break;
        }
    }

    y1 = 0;
    y2 = temp->TailleGroupeY_hor - 1;

    /*parcours du reseau de neurones du groupe precedent histogramme horizontale
       pour trouver la premiere coordonnee horizontale du cadre */
    for (i = 0; i < temp->TailleGroupeY_hor; i++)
    {
        if (neurone[i + temp->Debut_Gpe_hor].s > temp->seuil)
        {
            y1 = i;
            break;
        }
    }

    /*parcours du reseau de neurones du groupe precednt histogramme horizontale
       pour trouver la deuxieme coordonnee horizontale du cadre */
    for (i = temp->TailleGroupeY_hor; i > 0; i--)
    {
        if (neurone[i + temp->Debut_Gpe_hor].s > temp->seuil)
        {
            y2 = i;
            break;
        }
    }

#ifdef DEBUG
    printf("Avant Conversion");
    printf("Coord x1 :%d, x2:%d, y1:%d, y2:%d\n", x1, x2, y1, y2);
#endif

    /* conversion des numeros de neurone en coordonnees du cadre relativement a l'image */
    if (temp->TailleGroupeX_vert > 0)
    {
        x1 = (int) (((float) x1 / (float) temp->TailleGroupeX_vert) *
                    (float) temp->largeur);
        x2 = (int) (((float) x2 / (float) temp->TailleGroupeX_vert) *
                    (float) temp->largeur);
    }
    else
    {
        x1 = 0;
        x2 = temp->largeur;
    }

    if (temp->TailleGroupeY_hor > 0)
    {
        y1 = (int) (((float) y1 / (float) temp->TailleGroupeY_hor) *
                    (float) temp->hauteur);
        y2 = (int) (((float) y2 / (float) temp->TailleGroupeY_hor) *
                    (float) temp->hauteur);
    }
    else
    {
        y1 = 0;
        y2 = temp->hauteur;
    }

#ifdef DEBUG
    printf("Coordonnees de recadrage");
    printf("\n x1 = %d x2 = %d y1 = %d y2 = %d \n", x1, x2, y1, y2);
#endif

    /* recalcul du cadre en fonction du decalage causee par la luminosite */

    if ((x1 + temp->decalage) > 0 && (x2 + temp->decalage) < temp->largeur)
    {
        x1 = x1 + temp->decalage;
        x2 = x2 + temp->decalage;
    }

#ifdef DEBUG
    printf("Coordonnees apres recadrage de la luminosite");
    printf("x1 %d , x2 :%d", x1, x2);
#endif

    /*calcul de la largeur du cadre entourant le visage */
    width = abs(x1 - x2);

    /*********recalcul de la coordonnee inferieure du cadre en fonction de la largeur du visage***************/
    y2 = y1 + (int) (0.5 + temp->proportion * width);

    if (y2 >= temp->hauteur)
        y2 = temp->hauteur - 1;
    /**********changement en consequence des dimensions de la hauteur de l'image**********/
    height = abs(y1 - y2);

    /*Affectation des dimensions de l'imagette */

    p_result->sx = width;
    p_result->sy = height;


/************************************************************************************************/

    num = 0;
    echx = (float) temp->TailleGroupeX / (float) width;
    echy = (float) temp->TailleGroupeY / (float) height;

    for (li = y1; li < y2; li++)
    {
        p = (int) (temp->Debut_Gpe + (li - y1) * echy * temp->TailleGroupeX);
        for (co = x1; co < x2; co++)
        {
            numpixels = co + li * temp->largeur;
            i = (int) (p + (co - x1) * echx);

            if (numpixels < temp->Nxy)
            {
                val = (int) image->images_table[0][numpixels];
                p_result->images_table[0][num] = (unsigned char) val;
                num++;
                /*neurone[i].s=neurone[i].s1=neurone[i].s2 = (float) val /255.; */
            }

        }
    }

    echx = (float) width / (float) temp->TailleGroupeX;
    echy = (float) height / (float) temp->TailleGroupeY;

    for (j = 0; j < temp->TailleGroupeY; j++)
    {
        p = j * temp->TailleGroupeX + temp->Debut_Gpe;
        for (i = 0; i < temp->TailleGroupeX; i++)
        {
            i1 = i + p;
            numpixels =
                (int) (x1 + i * echx) + (int) (y1 + j * echy) * temp->largeur;
            neurone[i1].s = neurone[i1].s1 = neurone[i1].s2 =
                (float) image->images_table[0][numpixels] / 255.;
            /* printf("\n valeur du neurone %d = %f\n",i1,neurone[i1].s); */
        }
    }

    /*Fin du Timer et affichage des temps */
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", Gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif
}
