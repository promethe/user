/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
\file  f_save_macro_data.c
\brief 

Author: AM.Tousch
Created: 01/03/2006
Modified:
Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction permettant de connaitre/recuperer/modifier des donnees sur un maco-colonne
en particulier :
 - sauvegarde des poids dans un fichier

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()


Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
Cette fonction n'est plus vraiment utile depuis la creation de f_demande_numero0, qui, plus simplement, remet a zero toutes les entrees de supervision. On peut donc maintenant sauvegarder les poids simplement avec 'save NN' dans Promethe.

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <NN_Core/macro_colonne.h> /* Pour MICRO_DIST */

#define NBLINKS 5
#define SEUIL 0.001

typedef struct
{
    int time;                   /* nombre d'iterations */
    int gpe_macro;              /* numero du groupe d'entree correspondant a l'apprentissage */
    int size;                   /* nombre de classes */
    int is_macro;               /* 1 si gpe_macro est un macro_colonne, 0 sinon */
    int with_weights;           /* option -w0 ou -w1 */
    int learn;                  /* option -learn ou -use selon la phase d'utilisation */
    char *filename;             /* nom du fichier du dernier enregistrement */
    char *fileprefix;           /* prefixe pour les noms de fichiers */
} macro_data;

void new_save_macro_data(int Gpe)
{
    int n, link;
    char resultat[256];

    /* phase d'utilisation dans le cas du macro-colonne */
    FILE *record;               /* fichier de sauvegarde des poids du reseau macro-colonne */
    float sauv_weights[1];      /* pour recuperation des poids lus dans le fichier */
    int deb_gpe_macro;          /* pour trafiquer le macro-colonne */
    int nbre;                   /* nombre de neurones dans le groupe deb_groupe_test */
    type_coeff *coeff;
    int mode;
    int ret;

    macro_data *my_data;
    my_data = (macro_data *) malloc(sizeof(macro_data));
    my_data->fileprefix = (char *) malloc(256 * sizeof(char));
    my_data->filename = (char *) malloc(256 * sizeof(char));


    (void) ret; // (unused)

    /* initialisation de la structure */
#ifdef DEBUG
    printf("initialisation structure stat dans new_macro_data groupe %d\n", Gpe);
#endif
    my_data->time = 0;
    my_data->gpe_macro = -1;
    my_data->is_macro = 0;
    my_data->with_weights = 0;
    my_data->learn = -1;
    my_data->size = 0;

    /* Le groupe doit obligatoirement avoir deux entrees :
     * typiquement, on compare le resultat obtenu par le reseau avec celui attendu pour chaque prototype. */
    n = -1;
    do
    {
        n++;
        link = find_input_link(Gpe, n);
        if (link != -1)
        {
            my_data->gpe_macro = liaison[link].depart;
            /* Les options suivantes n'ont de sens que si le groupe d'apprentissage est un macro-colonne */
            /* pour afficher les poids des liens en entree sur le macro-neurone */
            if (prom_getopt(liaison[link].nom, "-w", resultat) == 2)
                my_data->with_weights = atoi(resultat);
            else
                my_data->with_weights = 0;
            /* pour se mettre en mode apprentissage : enregistrement regulier des poids dans un fichier */
            if (prom_getopt(liaison[link].nom, "-learn", resultat) == 1)
                my_data->learn = 1;
            /* pour se mettre en mode 'utilisateur' : recuperation des poids dans un fichier */
            else if (prom_getopt(liaison[link].nom, "-use", resultat) == 1)
                my_data->learn = 0;
            else
                my_data->learn = -1;
            if (prom_getopt(liaison[link].nom, "-F", resultat) == 2)
            {
                strcpy(my_data->fileprefix,
                       "/utilisateurs/annetous/Documents/results/");
                strcat(my_data->fileprefix, resultat);
            }
            else
                strcpy(my_data->fileprefix,
                       "/utilisateurs/annetous/Documents/results/poids");
        }
        else
            printf(" link %s not recognized (f_statistic) \n",
                   liaison[link].nom);
    }
    while (link != -1);

    /* cas particulier ou le groupe d'entree d'apprentissage est un macro-colonne */
    /* A ameliorer pour pouvoir vraiment tenir compte du nombre de classes (pas seulement tailley en theorie */
    if (def_groupe[my_data->gpe_macro].type == No_Macro_Colonne)
    {
        my_data->is_macro = 1;
    }
    strcpy(my_data->filename, my_data->fileprefix);

    def_groupe[Gpe].data = my_data;


    /* CAS MACRO-COLONNE */
  /*********************/

  /******************** chargement des poids en provenance d'un fichier ***************************/

    /* dans le cas ou on est en phase d'utilisation */
    if (my_data->is_macro && my_data->learn == 0)
    {
        /* donnees sur les neurones du macro-colonne */
        deb_gpe_macro = def_groupe[my_data->gpe_macro].premier_ele;
        nbre = def_groupe[my_data->gpe_macro].nbre;

        /* ouverture du fichier ou sont sauvegardes les poids sur les entrees DIST */
        printf("ouverture fichier %s...\n", my_data->filename);
        record = fopen(my_data->filename, "r");

        if (record == NULL)
        {
            printf("fichier %s non trouve", my_data->filename);
            exit(EXIT_FAILURE);
        }

        /* Mise a jour du reseau avec ces poids 
         * Attention, ne pas l'appliquer a un reseau deja appris
         * sinon risque d'avoir un flag a 1 sur un macro-neurone */
        for (n = deb_gpe_macro; n < deb_gpe_macro + nbre; n++)
        {

#ifdef DEBUG
            if (neurone[n].flag)
                printf("???????? flag a 1 neurone %d va tout perturber\n", n);
#endif

            coeff = neurone[n].coeff;
            mode = liaison[coeff->gpe_liaison].mode;
            if (mode == MICRO_DIST)
            {
                while (coeff != NULL)
                {
                    ret =  fscanf(record, "%f ", sauv_weights);
                    coeff->val = *sauv_weights;
                    coeff = coeff->s;
                }
            }
        }
        fclose(record);
        /* fin de la lecture */
    }
/******************** fin du chargement des poids ********************************/


}

/********************************** fin new_macro_data **********************************/



/****************************************************************************************/
void function_save_macro_data(int Gpe)
{
    int gpe_macro;
    int deb_gpe_macro;
    macro_data *my_data = NULL;
    int i;
    int nbre = 0;
    char filename[256];
    char smaj[10];

    /* donnees sur le groupe d'apprentissage */
    float l_rate;
    int mode;
    type_coeff *coeff;

    static int maj = 0;
    FILE *record = NULL;

    /* getting data from current group structure */
    my_data = (macro_data *) def_groupe[Gpe].data;
    if (my_data == NULL)
    {
        printf("error while loading data in group %d (f_macro_data)\n", Gpe);
        exit(EXIT_FAILURE);
    }

    /* information about input groups */
    gpe_macro = my_data->gpe_macro;

    deb_gpe_macro = def_groupe[gpe_macro].premier_ele;
    nbre = def_groupe[gpe_macro].nbre;

    /* Counting number of iterations */
    my_data->time++;
    maj++;
    printf("total of %d iterations\n---------------------------\n", maj);




  /***************************** Affichage des poids ******************************/
    if (my_data->is_macro && my_data->with_weights)
    {
        /* weights every 100 iterations */
        if ((my_data->time % 100) == 1)
        {
            printf("weights after %d iterations\n", my_data->time);
            for (i = deb_gpe_macro; i < deb_gpe_macro + nbre; i++)
            {
                coeff = neurone[i].coeff;
                mode = liaison[coeff->gpe_liaison].mode;
                if (mode == MICRO_DIST)
                {
                    coeff = neurone[i].coeff;
                    while (coeff != NULL)
                    {
                        printf("%f ", coeff->val);
                        coeff = coeff->s;
                    }
                    printf("\n\n");
                }
            }
            printf("-----------------------\n");
        }
    }


  /************* sauvegarde des poids *********************/
    /* dans le cas ou on est en phase d'apprentissage */

    if (my_data->is_macro && my_data->learn)
    {
        strcpy(filename, my_data->fileprefix);
        strcpy(my_data->filename, filename);
        if ((my_data->time % 1000) == 0)
        {
            sprintf(smaj, "%d", (maj + 1) / 2);
            strcat(filename, smaj);
            printf("fichier : %s \n", filename);
            record = fopen(filename, "w");
            if (record == NULL)
            {
                printf("ouverture fichier %s impossible", filename);
                exit(EXIT_FAILURE);
            }
#ifdef DEBUG
            printf("num deb gpe macro = %d\nnbre = %d", deb_gpe_macro, nbre);
#endif
            for (i = deb_gpe_macro; i < deb_gpe_macro + nbre; i++)
            {
                coeff = neurone[i].coeff;
                mode = liaison[coeff->gpe_liaison].mode;

                if (mode == MICRO_DIST)
                {
                    /*  coeff=neurone[i].coeff; */
                    while (coeff != NULL)
                    {
                        fprintf(record, "%f ", coeff->val);
                        coeff = coeff->s;
                    }
                    fprintf(record, "\n");
                }
            }

            fclose(record);
            l_rate = def_groupe[gpe_macro].learning_rate;
            printf(" ==> l_rate = %f\n----------------\n", l_rate);
            printf
                ("file %s : enregistrement des poids effectue apres %d iterations (maj=%d)\n",
                 filename, my_data->time, maj);
        }

    }
  /*************fin sauvegarde des poids *********************/


}
