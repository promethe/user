/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_eye_analog_segment.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Cette fonction recherche dans une image des zones sombres ayant les memes propri�� de dimensions et se situant dans
 les memes conditions environnantes que la r�ions des yeux

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-function_average()
-function_carre_blanc()
-function_force_noir()

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/eye.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
float function_average(unsigned char *image, int x, int y, int h, int w, int nx, int ny);
int function_carre_blanc(unsigned char *image, int x, int y, int h, int w, int nx, int ny);
void function_force_noir(unsigned char *image, int x, int y, int h, int w, int nx, int ny);

float function_average(unsigned char *image, int x, int y, int h, int w, int nx, int ny)
{
    int i = 0, j;
    float moyenne = 0;
    int numpixel;
    int Nxy = 0;

    Nxy = nx * ny;
    for (i = x; i < x + h; i++)
        for (j = y; j < y + w; j++)
        {
            numpixel = j + i * nx;
            if (numpixel < Nxy && numpixel > 0)
            {
                moyenne += (float) image[numpixel];
            }
        }
    moyenne = moyenne / (w * h);
    return (moyenne);
}

int function_carre_blanc(unsigned char *image, int x, int y, int h, int w, int nx, int ny)
{
    int i, j, tout_blanc = 1;
    int numpixel;
    int Nxy;

    Nxy = nx * ny;
    for (i = x; i < x + h; i++)
        for (j = y; j < y + w; j++)
        {
            numpixel = j + i * nx;
            if (numpixel < Nxy)
            {
                if (((int) image[numpixel]) != 255)
                    tout_blanc = 0;
            }
        }
    return (tout_blanc);
}

void function_force_noir(unsigned char *image, int x, int y, int h, int w, int nx, int ny)
{
    int i, j;
    int numpixel;
    int Nxy;

    Nxy = nx * ny;
    for (i = x; i < x + h; i++)
        for (j = y; j < y + w; j++)
        {
            numpixel = j + i * nx;
            if (numpixel < Nxy)
            {
                image[numpixel] = (unsigned char) 0;
            }
        }
}

void function_eye_analog_segment(int Gpe)
{
    int i = 0, j = 0, link = -1, coord_x2 = 0, coord_y2 = 0, coord_x =
        0, coord_y = 0;
    int nx = 100, ny = 100;
    prom_images_struct *image = NULL, *im = NULL;
    int numpixel;
    int h = 10, w = 10;

    char resultat[255];

    eye *temp;

#ifdef DEBUG
    TxPoint point;
#endif

    /*Mesure du temps de traitement dans la fonction */
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {

        /* sauvegarde des infos trouves sur le lien et du numero de groupe */
        temp = (eye *) malloc(sizeof(eye));

        def_groupe[Gpe].data = (void *) temp;

        /*initialisation */
        temp->groupe_entree = -1;
        temp->h = 10;
        temp->w = 10;
        temp->seuil = 0.9;
        temp->seuil2 = 0.2;

        temp->nx = 100;
        temp->ny = 100;

#ifdef DEBUG
        printf("Valeur de la structure eye apres init\n");
        printf("Valeur temp->groupe_entree :%d\n", temp->groupe_entree);
        printf("Valeur temp->h :%d \n", temp->h);
        printf("Valeur temp->w :%d \n", temp->w);
        printf("Valeur temp->seuil :%f \n", temp->seuil);
        printf("Valeur temp->seuil2 :%f \n", temp->seuil2);
        printf("Valeur temp->nx :%d \n", temp->nx);
        printf("Valeur temp->ny :%d \n", temp->ny);
#endif

        /*Finding the link */

        link = find_input_link(Gpe, 0);
        temp->groupe_entree = liaison[link].depart;

        if (link == -1)
        {
            printf("*****> Il n'y a pas de groupe en entree\n");
            exit(EXIT_FAILURE);
        }

        if ((def_groupe[temp->groupe_entree].ext == NULL))
        {
            printf("Gpe en entree avec ext nulle; pas d'image\n");
            return;
        }

        if (((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->
            nb_band == 3)
        {
            printf("*****> L'image en entree doit etre en noir et blanc\n");
            exit(EXIT_FAILURE);
        }

    /***********************recuperation des infos sur le groupe precedent****************/

        im = (prom_images_struct *) def_groupe[temp->groupe_entree].ext;

        temp->nx =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sx;
        temp->ny =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sy;

        if (prom_getopt(liaison[link].nom, "-h", resultat) == 2)
            temp->h = atoi(resultat);
        if (prom_getopt(liaison[link].nom, "-w", resultat) == 2)
            temp->w = atoi(resultat);
        if (prom_getopt(liaison[link].nom, "-S", resultat) == 2)
            temp->seuil = atof(resultat);
        if (prom_getopt(liaison[link].nom, "-M", resultat) == 2)
            temp->seuil2 = atof(resultat);

        image = (prom_images_struct *) malloc(sizeof(prom_images_struct));

        if (image == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        image->nb_band = 1;
        image->image_number = 1;

        /*Recuperation de la taille de l'image */
        image->sx =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sx;
        image->sy =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sy;

#ifdef DEBUG
        printf("Valeur de la structure eye apres lecture du lien\n");
        printf("Valeur temp->groupe_entree :%d\n", temp->groupe_entree);
        printf("Valeur temp->h :%d \n", temp->h);
        printf("Valeur temp->w :%d \n", temp->w);
        printf("Valeur temp->seuil :%f \n", temp->seuil);
        printf("Valeur temp->seuil2 :%f \n", temp->seuil2);
        printf("Valeur temp->nx :%d \n", temp->nx);
        printf("Valeur temp->ny :%d \n", temp->ny);
#endif

#ifdef DEBUG
        printf("Valeur de nx :%d, Valeur de ny:%d\n", nx, ny);
#endif

        /* allocation memoire pour l'image des eye-analog pixels */
        /*      image->images_table[0] = (char *) malloc(nx*ny*sizeof(char)); */

        image->images_table[0] =
            (unsigned char *) malloc(nx * ny * sizeof(unsigned char));

        if (image->images_table[0] == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE...! \n");
            exit(-1);
        }

        def_groupe[Gpe].ext = image;

    }

    else
    {

        image = ((prom_images_struct *) def_groupe[Gpe].ext);
        temp = (eye *) ((prom_images_struct *) def_groupe[Gpe].data);

#ifdef DEBUG
        printf("Valeur de la structure eye apres lecture du lien\n");
        printf("Valeur temp->groupe_entree :%d\n", temp->groupe_entree);
        printf("Valeur temp->h :%d \n", temp->h);
        printf("Valeur temp->w :%d \n", temp->w);
        printf("Valeur temp->seuil :%f \n", temp->seuil);
        printf("Valeur temp->seuil2 :%f \n", temp->seuil2);
        printf("Valeur temp->nx :%d \n", temp->nx);
        printf("Valeur temp->ny :%d \n", temp->ny);
#endif

        im = (prom_images_struct *) def_groupe[temp->groupe_entree].ext;

        temp->nx =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sx;
        temp->ny =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sy;

        /*Effacement de l'image resultat */
        for (i = 0; i < nx * ny; i++)
        {
            image->images_table[0][i] = 0;
        }

        /*redimensionnement de l'image resultat en fonction de l'image de l'entree */
        image->nb_band = 1;
        image->image_number = 1;
        image->sx = temp->nx;
        image->sy = temp->ny;

    }

#ifdef DEBUG
    printf("Valeur de nx %d et Valeur de ny %d\n", nx, ny);
#endif

/*********************recherche des eyes-analog pixels*************************/
    coord_x = floor(w);
    coord_y = floor(h);
    coord_x2 = floor(w / 2);
    coord_y2 = floor(h / 2);

    for (i = coord_y2; i < ny - coord_y2; i++)
        for (j = coord_x2; j < nx - coord_x2; j++)
        {
            numpixel = j + i * nx;
            if ((im->images_table[0][numpixel] <
                 temp->seuil * function_average(im->images_table[0],
                                                i - coord_y2, j - coord_x2,
                                                coord_y2, coord_x2, nx, ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0],
                                                   i - coord_y2, j, coord_y2,
                                                   1, nx, ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0],
                                                   i - coord_y2, j + 1,
                                                   coord_y2, coord_x2, nx,
                                                   ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0], i,
                                                   j - coord_x2, 1, coord_x2,
                                                   nx, ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0], i,
                                                   j + 1, 1, coord_x2, nx,
                                                   ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0], i + 1,
                                                   j - coord_x2, coord_y2,
                                                   coord_x2, nx, ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0], i + 1,
                                                   j, coord_y2, 1, nx, ny))
                && (im->images_table[0][numpixel] <
                    temp->seuil * function_average(im->images_table[0], i + 1,
                                                   j + 1, coord_y2, coord_x2,
                                                   nx, ny))
                /* autres conditions */
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0],
                                                    i - coord_y, j, coord_y,
                                                    1, nx, ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0],
                                                    i - coord_y, j + 1,
                                                    coord_y, coord_x, nx, ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0], i,
                                                    j - coord_x, 1, coord_x,
                                                    nx, ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0], i,
                                                    j + 1, 1, coord_x, nx,
                                                    ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0],
                                                    i + 1, j - coord_x,
                                                    coord_y, coord_x, nx, ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0],
                                                    i + 1, j, coord_y, 1, nx,
                                                    ny))
                && (im->images_table[0][numpixel] >
                    temp->seuil2 * function_average(im->images_table[0],
                                                    i + 1, j + 1, coord_y,
                                                    coord_x, nx, ny)))

            {
                image->images_table[0][numpixel] = 255;
                /*printf("\n %d est un eye analog pixel\n",numpixel); */
            }
            else
            {
                image->images_table[0][numpixel] = 0;
            }
        }

#ifdef DEBUG
    point.x = nx / 2;
    point.y = ny / 2;
#endif

#ifndef AVEUGLE                 /* BRAIK belkeir 15/04 */
#ifdef DEBUG
    TxDessinerRectangle(&image1, vert, TxVide, point, w, h, 1);
    TxFlush(&image1);           /* Braik belkeir le 15/04/04 modif de XFlush (image1.display) */
#endif
#endif



/***********************elimination des faux eyes-analog pixels**************/

    for (i = 0; i < ny; i++)
        for (j = 0; j < nx; j++)
        {
            numpixel = j + i * nx;
            /*elimination */

            if ((image->images_table[0][numpixel] == 255)
                &&
                ((function_carre_blanc
                  (image->images_table[0], i - coord_y2, j, coord_y2, 1, nx,
                   ny) == 1)
                 ||
                 (function_carre_blanc
                  (image->images_table[0], i - coord_y2, j, coord_y2, 1, nx,
                   ny) == 1)
                 ||
                 (function_carre_blanc
                  (image->images_table[0], i + 1, j, coord_y2, 1, nx,
                   ny) == 1)))
            {
                printf("\n zone effacee en (i,j) = %d %d\n", i, j);

#ifndef AVEUGLE                 /* BRAIK belkeir 15/04 */
#ifdef DEBUG
                point.x = i;
                point.y = j;
                TxDessinerRectangle(&image1, rouge, TxVide, point, w, h, 1);
                TxFlush(&image1);   /* Braik belkeir le 15/04/04 modif de XFlush (image1.display) */
#endif
#endif
                function_force_noir(image->images_table[0], i, j,
                                    floor(h / 4), floor(w / 4), nx, ny);
            }

            /* ajout */

            if ((function_average
                 (image->images_table[0], i - floor(h / 4), j - floor(w / 4),
                  floor(h / 4), floor(w / 4), nx, ny) > 0.25 * 255)
                &&
                (function_average
                 (image->images_table[0], i - floor(h / 4), j, floor(h / 4),
                  1, nx, ny) > 0.25 * 255)
                &&
                (function_average
                 (image->images_table[0], i - floor(h / 4), j + 1,
                  floor(h / 4), floor(w / 4), nx, ny) > 0.25 * 255)
                &&
                (function_average
                 (image->images_table[0], i, j - floor(w / 4), 1,
                  floor(w / 4), nx, ny) > 0.25 * 255)
                &&
                (function_average
                 (image->images_table[0], i, j + 1, 1, floor(w / 4), nx,
                  ny) > 0.25 * 255)
                &&
                (function_average
                 (image->images_table[0], i + 1, j - floor(w / 4),
                  floor(h / 4), floor(w / 4), nx, ny) > 0.25 * 255))
                image->images_table[0][numpixel] = 255;

        }
/*Fin du Timer et affichage des temps*/
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", Gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s", MessageFunctionTimeTrace);
#endif

}
