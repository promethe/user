/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_decouper_visage_tracking.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/prom_getopt()
 -Kernel_Function/find_input_link()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>
#include <Struct/feature_face.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct
{
   int taille_fen_x, taille_fen_y; /* Dimension de la fenetre */
   int taille_im_x, taille_im_y; /* Dimension de l'image */
   int val_coin; /* valeur en pixel du coin de la fenetre depuis le coin sup-gauche de l'image */
   TxPoint coin_fen;
   unsigned char *fen;
   /*unsigned char fen[3*taille_x*taille_y]; */
   int InputGpe, display, InputGpeCoord;
   int tab[5], nb_pixel;
   float *tabscore;
   int nb_band;

   float proportion;

} fenetre_track;

void function_decouper_visage_tracking(int gpe_sortie)
{
   int i = 0, j;
   int link1 = -1, link2 = -1;
   int Debut_Gpe;
   int TailleGroupeX, TailleGroupeY;
   int SizeZoneX, SizeZoneY;
   int m, k, l, n, Nhv, i1, j1;
   int t;

   unsigned char *CarteProjection;

   int a, b, co, li, numpixels, num;

   prom_images_struct *image = NULL;
   prom_images_struct *p_result = NULL;

   decoupage *temp;

   char resultat[256];

   int debut_zone_isole_y, limit_zone_isole_y;
   int debut_zone_isole_x, limit_zone_isole_x;

   /*variable necessaire si f_tracking_head precede */
   fenetre_track *fen;

   /* initialisation locale */
   Debut_Gpe = def_groupe[gpe_sortie].premier_ele;
   TailleGroupeX = def_groupe[gpe_sortie].taillex;
   TailleGroupeY = def_groupe[gpe_sortie].tailley;
   Nhv = TailleGroupeX * TailleGroupeY;

   /* Allocation prom_images_strct */
   if (def_groupe[gpe_sortie].ext == NULL)
   {

      /*recuperation des infos sur les differents liens */
      temp = (decoupage *) malloc(sizeof(decoupage));
      def_groupe[gpe_sortie].data = (decoupage *) temp;

      /*initialisation */
      temp->height = 100;
      temp->width = 100;
      temp->groupe_entree_image = -1;
      temp->groupe_entree_coord = -1;
      temp->largeur = 384;
      temp->hauteur = 288;
      temp->Nxy = 100592;

      link1 = find_input_link(gpe_sortie, 0);
      link2 = find_input_link(gpe_sortie, 1);

      if ((link1 == -1) || (link2 == -1))
      {
         EXIT_ON_ERROR("*****> Il n'y a pas deux groupe en entree du Gpe %d\n", gpe_sortie);
      }

      /*recuperation des coordonees lorsque le groupe precedent est f_tracking_head */
      if ((prom_getopt(liaison[link1].nom, "-image", resultat) == 2) && (prom_getopt(liaison[link2].nom, "-coord_tracking", resultat) == 2))
      {
         temp->groupe_entree_image = liaison[link1].depart;
         temp->groupe_entree_coord = liaison[link2].depart;
         /*Modif olivier */
         i = link2;
         /*Fin modif */
      }
      else
      {
         temp->groupe_entree_image = liaison[link2].depart;
         temp->groupe_entree_coord = liaison[link1].depart;
         /*Modif olivier */
         i = link1;
         /*Fin modif */
      }

      dprints("*****> gpe_entree 1 = %d   link1=%d\n", temp->groupe_entree_image, link1);
      dprints("*****> gpe_entree 2 = %d   link2=%d\n", temp->groupe_entree_coord, link2);

      if (prom_getopt(liaison[i].nom, "-H", resultat) == 2)
      {
         temp->height = atoi(resultat);
      }
      if (prom_getopt(liaison[i].nom, "-W", resultat) == 2)
      {
         temp->width = atoi(resultat);
      }

      /*Recuperation de la structure fen */
      fen = (fenetre_track *) ((prom_images_struct *) def_groupe[temp->groupe_entree_coord].data);

      if (prom_getopt(liaison[i].nom, "-P", resultat) == 2)
      {
         fen->proportion = atof(resultat);
         printf("Valeur de proportion %f\n", fen->proportion);
      }

      /*recuperation de l'image du groupe precedent */
      image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;

      if (image == NULL)
      {
         EXIT_ON_ERROR("Probleme : pas d'image dans le groupe %i\n", temp->groupe_entree_image);
      }

      if (image->nb_band != 1)
      {
         EXIT_ON_ERROR("\n l'image en entree de f_decouper_image_tracking (Gpe %d) doit etre en noir et blanc !\n", gpe_sortie);
      }

      temp->largeur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
      temp->hauteur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
      temp->Nxy = temp->largeur * temp->hauteur;

      printf("premiere alloc ext \n");
      p_result = (void *) malloc(sizeof(prom_images_struct));
      ;
      if (p_result == NULL)
      {
         EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE DANS DECOUPER IMAGE...! \n");
      }

      p_result->image_number = 1;
      p_result->sx = temp->width;
      p_result->sy = temp->height;
      p_result->nb_band = 1;

      def_groupe[gpe_sortie].ext = p_result;

      /*allocation memoire pour l'imagette */
      p_result->images_table[0] = (unsigned char *) malloc(temp->width * temp->height * sizeof(unsigned char));

      /* allocation pour la projection de l'image dans le reseau de neurones */
      CarteProjection = (unsigned char *) malloc(Nhv * sizeof(unsigned char));
      if (CarteProjection == NULL)   EXIT_ON_ERROR("Not enought memory!");

      p_result->images_table[1] = CarteProjection;
   }

   else
   {

      temp = (decoupage *) ((prom_images_struct *) def_groupe[gpe_sortie].data);

      /*recuperation de l'image du groupe precedent */
      image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;
      temp->largeur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
      temp->hauteur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
      temp->Nxy = temp->largeur * temp->hauteur;

      p_result = ((prom_images_struct *) def_groupe[gpe_sortie].ext);

      CarteProjection = p_result->images_table[1];

      fen = (fenetre_track *) ((prom_images_struct *) def_groupe[temp->groupe_entree_coord].data);
   }

   printf("Valeur de proportion %f dans le groupe %d", fen->proportion, gpe_sortie);

   if (isdiff(fen->proportion, 0))
   {
      a = (fen->coin_fen.x);
      b = (fen->coin_fen.y) * (fen->proportion);
   }
   else
   {
      a = fen->coin_fen.x;
      b = fen->coin_fen.y;
   }

   /*printf("\n a = %d b = %d \n",a,b); */

   num = 0;

   /*********************************************************************************/
   /*Limite du decoupage en y */
   debut_zone_isole_y = b - floor(temp->height / 2);
   limit_zone_isole_y = b + floor(temp->height / 2);

   /*Limite du decoupage en x */
   debut_zone_isole_x = a - floor(temp->width / 2);
   limit_zone_isole_x = a + floor(temp->width / 2);

   dprints("Function f_decouper_tracking, Gpe : %d\n", gpe_sortie);
   dprints("Valeur de a :%d , valeur de b :%d\n", a, b);
   dprints("Valeur de debut_zone_isole_x %d et limit_zone_isole_x %d\n", debut_zone_isole_x, limit_zone_isole_x);
   dprints("Valeur de debut_zone_isole_y %d et limit_zone_isole_y %d\n", debut_zone_isole_y, limit_zone_isole_y);
   dprints("Valeur de largeur %d et hauteur %d\n", temp->largeur, temp->hauteur);
   dprints("Valeur de width %d et height %d\n", temp->width, temp->height);

   /*Recadrage en x */
   if (limit_zone_isole_x > image->sx)
   {
      t = limit_zone_isole_x - image->sx;
      limit_zone_isole_x = image->sx;
      debut_zone_isole_x = debut_zone_isole_x - t;
      /* p_result->sy =  p_result->sy - t; */

      if (debut_zone_isole_x <= 0) debut_zone_isole_x = 0;

      dprints("Correction en x\n");
      dprints("Valeur de t %d\n", t);
      dprints("Valeur de p_result->sy %d\n", p_result->sy);
   }

   /*Recadrage en x */
   if (debut_zone_isole_x < 0)
   {
      t = debut_zone_isole_x;
      limit_zone_isole_x = limit_zone_isole_x - t;
      debut_zone_isole_x = 0;
      /* p_result->sy =  p_result->sy - t; */

      dprints("Correction en x\n");
      dprints("Valeur de t %d\n", t);
      dprints("Valeur de p_result->sy %d\n", p_result->sy);
   }

   /*Recadrage en y */
   if (limit_zone_isole_y > image->sy)
   {
      t = limit_zone_isole_y - image->sy;
      limit_zone_isole_y = image->sy;
      debut_zone_isole_y = debut_zone_isole_y - t;
      /* p_result->sy =  p_result->sy - t; */

      if (debut_zone_isole_y <= 0) debut_zone_isole_y = 0;

      dprints("Correction en y\n");
      dprints("Valeur de t %d\n", t);
      dprints("Valeur de p_result->sy %d\n", p_result->sy);
   }

   /*Recadrage en y */
   if (debut_zone_isole_y < 0)
   {
      t = debut_zone_isole_y;
      limit_zone_isole_y = limit_zone_isole_y - debut_zone_isole_y;
      debut_zone_isole_y = 0;
      /* p_result->sy =  p_result->sy - t; */

      dprints("Correction en y\n");
      dprints("Valeur de t %d\n", t);
      dprints("Valeur de p_result->sy %d\n", p_result->sy);
   }

   dprints("Apres correction\n");
   dprints("Valeur de debut_zone_isole_x %d et limit_zone_isole_x %d\n",debut_zone_isole_x, limit_zone_isole_x);
   dprints("Valeur de debut_zone_isole_y %d et limit_zone_isole_y %d\n", debut_zone_isole_y, limit_zone_isole_y);
   dprints("Valeur de largeur %d et hauteur %d\n", temp->largeur, temp->hauteur);
   dprints("Valeur de image->sx %d\n", image->sx);
   dprints("Valeur de image->sy %d\n", image->sy);

   /*printf("\n a = %d b = %d \n",a,b); */
   num = 0;

   for (li = debut_zone_isole_y; li < limit_zone_isole_y; li++)
   {
      for (co = debut_zone_isole_x; co < limit_zone_isole_x; co++)
      {
         numpixels = co + li * temp->largeur;
         /*printf("\n numero de pixel de l'image %d : %d\n",i,numpixels); */
         /*printf("\n numero de pixel de l'imagette : %d\n",num); */
         if (numpixels < temp->Nxy)
         {
            p_result->images_table[0][num] = image->images_table[0][numpixels];
            num++;
         }
      }
   }

   /************************************************************************************************/

   /* reset of outputs */
   for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
      CarteProjection[j] = 0;
   }

   /*printf("\njusque la avant boucle\n");*/
   /* local initialisations */
   SizeZoneX = temp->width / TailleGroupeX;
   SizeZoneY = temp->height / TailleGroupeY;

   dprints("TailleGroupeY :%d\n", TailleGroupeY);
   dprints("TailleGroupeX :%d\n", TailleGroupeX);
   dprints("temp->height :%d\n", temp->height);
   dprints("temp->width :%d\n", temp->width);
   dprints("SizeZoneX :%d\n", SizeZoneX);
   dprints("SizeZoneY :%d\n", SizeZoneY);

   /* les projections verticales et horizontales */
   i1 = TailleGroupeY * SizeZoneY;
   j1 = TailleGroupeX * SizeZoneX;

   dprints("i1 :%d\n", i1);
   dprints("j1 :%d\n", j1);

   for (i = 0; i < i1; i++)
   {
      m = i / SizeZoneY; /* indice Oy pour reseaux */
      for (j = 0; j < j1; j++)
      {
         l = j / SizeZoneX; /* indice Ox pour reseaux */
         k = temp->width * i + j; /* indice dans l'image */
         n = TailleGroupeX * m + l; /* indice dans projection */
         /*if( ImageResultat[k] > 0 ) */
         CarteProjection[n] = p_result->images_table[0][k];
         /*printf("\nCarteProjection[n] %c\n",CarteProjection[n]); */
         /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
      }
   }

   dprints(" Valeur de m : %d\n", m);
   dprints(" Valeur de l : %d\n", l);
   dprints(" Valeur de k : %d\n", k);
   dprints(" Valeur de n : %d\n", n);

   /*printf("\njusque la apres boucle\n");*/
   for (i = 0; i < Nhv; i++)
      /*if( (int)CarteProjection[i] > SeuilDetection ) */
      neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / 255; /*((float)(SizeZoneX*SizeZoneY)); */
   /*else */
   /*neurone[Debut_Gpe+i].s=neurone[Debut_Gpe+i].s1=neurone[Debut_Gpe+i].s2 = 0.0; */

}
