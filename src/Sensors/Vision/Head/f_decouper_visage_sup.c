/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_decouper_visage_sup.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Fonction qui isole la partie superieur du visage (zone des yeux)
 Taille du reseau de neurones de 20*20

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/prom_getopt()
 -Kernel_Function/find_input_link()
 -tools/erreur/gestion_erreur()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/decoupage.h>
#include <Struct/feature_face.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_decouper_visage_sup(int gpe_sortie)
{
  int i = 0, j;
  int link1 = -1, link2 = -1;
  int Debut_Gpe;
  int TailleGroupeX, TailleGroupeY;
  int SizeZoneX, SizeZoneY;
  int m, l, k, n, Nhv, i1, j1;
  int t;

  unsigned char *CarteProjection;

  int a, b, co, li, numpixels, num;

  prom_images_struct *image = NULL;
  prom_images_struct *p_result = NULL;

  decoupage *temp;

  char resultat[256];

  feature_face *face = NULL;
  int debut_zone_isole;
  int limit_zone_isole;

  /* initialisation locale */
  Debut_Gpe = def_groupe[gpe_sortie].premier_ele;
  TailleGroupeX = def_groupe[gpe_sortie].taillex;
  TailleGroupeY = def_groupe[gpe_sortie].tailley;
  Nhv = TailleGroupeX * TailleGroupeY;

  /* Allocation prom_images_strct */
  if (def_groupe[gpe_sortie].ext == NULL)
  {

    /*recuperation des infos sur les differents liens */
    temp = (decoupage *) malloc(sizeof(decoupage));
    def_groupe[gpe_sortie].data = (decoupage *) temp;

    /*initialisation */
    /*Les tailles height et width ne doivent pas etre superieur a la taille de l'image qui est decoupe */
    temp->height = 20;
    temp->width = 40;
    temp->groupe_entree_image = -1;
    temp->groupe_entree_coord = -1;
    temp->largeur = 384;
    temp->hauteur = 288;
    temp->Nxy = 100592;

    link1 = find_input_link(gpe_sortie, 0);
    link2 = find_input_link(gpe_sortie, 1);

    if ((link1 == -1) || (link2 == -1))
    {
      EXIT_ON_ERROR("*****> Il n'y a pas deux groupe en entree du Gpe %d\n", gpe_sortie);
    }

    if ((prom_getopt(liaison[link1].nom, "-image", resultat) == 2) && (prom_getopt(liaison[link2].nom, "-coord_sup", resultat) == 2))
    {
      temp->groupe_entree_image = liaison[link1].depart;
      temp->groupe_entree_coord = liaison[link2].depart;
      i = link2;
    }
    else
    {
      temp->groupe_entree_image = liaison[link2].depart;
      temp->groupe_entree_coord = liaison[link1].depart;
      i = link1;
    }

    dprints("*****> gpe_entree 1 (image) = %d   link1=%d\n", temp->groupe_entree_image, link1);
    dprints("*****> gpe_entree 2 (coord) = %d   link2=%d\n", temp->groupe_entree_coord, link2);

    if (prom_getopt(liaison[i].nom, "-H", resultat) == 2)
    {
      temp->height = atoi(resultat);
    }
    if (prom_getopt(liaison[i].nom, "-W", resultat) == 2)
    {
      temp->width = atoi(resultat);
    }

    /*recuperation de l'image du groupe precedent */
    image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;

    if (image == NULL)
    {
      printf("Probleme : pas d'image dans le groupe %i\n", temp->groupe_entree_image);
      exit(EXIT_FAILURE);
    }

    if (image->nb_band != 1)
    {
      EXIT_ON_ERROR("\n l'image en entree de f_decouper_image (Gpe %d)  doit etre en noir et blanc !\n", gpe_sortie);
    }

    temp->largeur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
    temp->hauteur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
    temp->Nxy = temp->largeur * temp->hauteur;

    /*printf("premiere alloc ext \n") ; */
    p_result = (void *) malloc(sizeof(prom_images_struct));

    if (p_result == NULL)
    {
      EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE DANS DECOUPER IMAGE...! \n");
    }

    p_result->image_number = 1;
    p_result->sx = temp->width;
    p_result->sy = temp->height;
    p_result->nb_band = 1;

    def_groupe[gpe_sortie].ext = p_result;

    /*allocation memoire pour l'imagette */
    p_result->images_table[0] = (unsigned char *) malloc(temp->width * temp->height * sizeof(unsigned char));

    /* allocation pour la projection de l'image dans le reseau de neurones */
    CarteProjection = (unsigned char *) malloc(Nhv * sizeof(unsigned char));
    if (CarteProjection == NULL) EXIT_ON_ERROR("Not enought memory!");

    p_result->images_table[1] = CarteProjection;
  }
  else
  {
    temp = (decoupage *) ((prom_images_struct *) def_groupe[gpe_sortie].data);

    /*recuperation de l'image du groupe precedent */
    image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;
    temp->largeur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
    temp->hauteur = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
    temp->Nxy = temp->largeur * temp->hauteur;
    p_result = ((prom_images_struct *) def_groupe[gpe_sortie].ext);
    CarteProjection = p_result->images_table[1];
  }

  /*recuperation des coordonnees du groupe precedent */
  face = (feature_face *) ((prom_images_struct *) def_groupe[temp->groupe_entree_coord].data);

  a = face->nx / 2;
  b = face->pos_oeil_y;
  debut_zone_isole = b - floor(temp->height / 2);
  limit_zone_isole = b + floor(temp->height / 2);

  if (debut_zone_isole <= 0) debut_zone_isole = 0;

  dprints("Valeur de limit_zone_isole %d\n", limit_zone_isole);
  dprints("Valeur de image->sy %d\n", image->sx);

  /*Recadrage de la zone decouper en fonction de la taille de l'image */
  if (limit_zone_isole > image->sx)
  {
    t = limit_zone_isole - image->sx;
    limit_zone_isole = image->sx;
    debut_zone_isole = debut_zone_isole - t;

    dprints("Valeur de t %d\n", t);
    dprints("Valeur de p_result->sy %d\n", p_result->sx);
  }

  /*printf("\n a = %d b = %d \n",a,b); */
  num = 0;

  /************************************************************************************************/
  for (li = debut_zone_isole; li < limit_zone_isole; li++)
  {
    for (co = a - floor(temp->width / 2); co < a + floor(temp->width / 2); co++)
    {
      numpixels = co + li * temp->largeur;
      /*printf("\n numero de pixel de l'image %d : %d\n",i,numpixels);
       printf("\n numero de pixel de l'imagette : %d\n",num); */
      if (numpixels < temp->Nxy)
      {
        p_result->images_table[0][num] = image->images_table[0][numpixels];
        num++;
      }
    }
  }

  /* reset of outputs */
  for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
  {
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    CarteProjection[j] = 0;
  }

  /*printf("\njusque la avant boucle\n"); */
  /* local initialisations */
  SizeZoneX = (int) ((float) temp->width / (float) TailleGroupeX);
  SizeZoneY = (int) ((float) temp->height / (float) TailleGroupeY);

  dprints("TailleGroupeY :%d\n", TailleGroupeY);
  dprints("TailleGroupeX :%d\n", TailleGroupeX);
  dprints("temp->height :%d\n", temp->height);
  dprints("temp->width :%d\n", temp->width);
  dprints("SizeZoneX :%d\n", SizeZoneX);
  dprints("SizeZoneY :%d\n", SizeZoneY);

  /* les projections verticales et horizontales */
  i1 = TailleGroupeY * SizeZoneY;
  j1 = TailleGroupeX * SizeZoneX;

  dprints("i1 :%d\n", i1);
  dprints("j1 :%d\n", j1);

  for (i = 0; i < i1; i++)
  {
    m = i / SizeZoneY; /* indice Oy pour reseaux */
    for (j = 0; j < j1; j++)
    {
      l = j / SizeZoneX; /* indice Ox pour reseaux */
      k = temp->width * i + j; /* indice dans l'image */
      n = TailleGroupeX * m + l; /* indice dans projection */
      /*if( ImageResultat[k] > 0 ) */
      CarteProjection[n] = p_result->images_table[0][k];
      /*printf("\nCarteProjection[n] %c\n",CarteProjection[n]); */
      /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
    }
  }

  dprints(" Valeur de m : %d\n", m);
  dprints(" Valeur de l : %d\n", l);
  dprints(" Valeur de k : %d\n", k);
  dprints(" Valeur de n : %d\n", n);

  /*printf("\njusque la apres boucle\n"); */
  for (i = 0; i < Nhv; i++)
    /*if( (int)CarteProjection[i] > SeuilDetection ) */
    neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / 255; /*((float)(SizeZoneX*SizeZoneY));
     else
     neurone[Debut_Gpe+i].s=neurone[Debut_Gpe+i].s1=neurone[Debut_Gpe+i].s2 = 0.0; */

}
