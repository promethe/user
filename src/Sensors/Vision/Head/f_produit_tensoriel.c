/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/cadrage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include "../../Vision/popout/tools/include/local_var.h"

/*#define DEBUG*/

void function_produit_tensoriel(int gpe)

{

  int i, j;
  int debut_gpe;
  int gpe_precedent_robot = -1;
  int debut_gpe_precedent_robot;
  int taille_robot; /*nombre de neurone sur le groupe precedent*/

  int gpe_precedent_signal = -1;
  int debut_gpe_precedent_signal;
  int taille_gpe;

  float max_robot = -999;
  int i_max_robot = -1;

#ifdef DEBUG
  printf("entree dans %s \n",__FUNCTION__);
#endif

  i = 0;

  while ((j = find_input_link(gpe, i)) != -1)
  {

    if (strcmp(liaison[j].nom, "robot") == 0)
    {

      gpe_precedent_robot = liaison[j].depart;
#ifdef DEBUG
      printf("groupe robot=%d\n",liaison[j].depart);
#endif
    }

    if (strcmp(liaison[j].nom, "interaction") == 0)
    {

      gpe_precedent_signal = liaison[j].depart;
#ifdef DEBUG
      printf("groupe interaction=%d\n",liaison[j].depart);
#endif
    }

    i++;
#ifdef DEBUG
    printf("on a fini de recuperer les parametres\n");
#endif
  }

  if (gpe_precedent_robot == -1 || gpe_precedent_signal == -1)
  {
    printf("Erreur dans la fonctionf_signal_tete, il lui faut deux groupes en entree\n");
    exit(0);
  }

  debut_gpe = def_groupe[gpe].premier_ele;
  taille_gpe = def_groupe[gpe].nbre;

  debut_gpe_precedent_robot = def_groupe[gpe_precedent_robot].premier_ele;
  taille_robot = def_groupe[gpe_precedent_robot].nbre;

  debut_gpe_precedent_signal = def_groupe[gpe_precedent_signal].premier_ele;

  for (i = debut_gpe; i < debut_gpe + taille_gpe; i++)
  {

    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;

  }

#ifdef DEBUG
  printf("taille groupe=%d \n",def_groupe[gpe_precedent_robot].nbre);
  printf("groupe precedent robot= %s \n",def_groupe[gpe_precedent_robot].no_name);
  printf("taille robot=%d \n",def_groupe[gpe_precedent_robot].nbre);
  printf("groupe precedent signal= %s \n",def_groupe[gpe_precedent_signal].no_name);
  printf("taille signal=%d \n",def_groupe[gpe_precedent_signal].nbre);
  printf("taille groupe=%d \n",taille_gpe);
  printf("tailleX=%d\n",def_groupe[gpe].taillex);
  printf("tailleY=%d\n",def_groupe[gpe].tailley);
#endif

  /*on recherche le Max dans le groupe robot*/

  for (i = debut_gpe_precedent_robot; i < debut_gpe_precedent_robot + taille_robot; i++)
  {
    if (neurone[i].s > max_robot)
    {
      max_robot = neurone[i].s;
      i_max_robot = i - debut_gpe_precedent_robot;
    }
  }

#ifdef DEBUG
  printf("neurone gagnant du robot= %d\n",i_max_robot);
  printf("activite du signal interaction.s=%f \n",neurone[debut_gpe_precedent_signal].s);
  printf("activite du signal interaction.s1=%f\n",neurone[debut_gpe_precedent_signal].s1);
  printf("activite du signal interaction.s2=%f\n",neurone[debut_gpe_precedent_signal].s2);

#endif

  /*on cherche si on a un signal*/

  if (neurone[debut_gpe_precedent_signal].s > 0.5)
  {
    for (i = 0; i < 5; i++)
    {
      neurone[debut_gpe + i * def_groupe[gpe].taillex].s = neurone[debut_gpe_precedent_robot + i].s2;
      neurone[debut_gpe + i * def_groupe[gpe].taillex].s1 = neurone[debut_gpe_precedent_robot + i].s2;
      neurone[debut_gpe + i * def_groupe[gpe].taillex].s2 = neurone[debut_gpe_precedent_robot + i].s2;
#ifdef DEBUG
      printf("activite du robot neurone[%d].s=%f \n",i,neurone[debut_gpe_precedent_robot+i].s1);
      printf("activite du robot neurone[%d].s1=%f \n",i,neurone[debut_gpe_precedent_robot+i].s1);
      printf("activite du robot neurone[%d].s2=%f \n\n",i,neurone[debut_gpe_precedent_robot+i].s2);
#endif
    }
  }
  else
  {
    for (i = 0; i < 5; i++)
    {
      neurone[debut_gpe + i * def_groupe[gpe].taillex + 1].s = neurone[debut_gpe_precedent_robot + i].s2;
      neurone[debut_gpe + i * def_groupe[gpe].taillex + 1].s1 = neurone[debut_gpe_precedent_robot + i].s2;
      neurone[debut_gpe + i * def_groupe[gpe].taillex + 1].s2 = neurone[debut_gpe_precedent_robot + i].s2;
#ifdef DEBUG
      printf("activite du robot neurone[%d].s=%f \n",i,neurone[debut_gpe_precedent_robot+i].s);
      printf("activite du robot neurone[%d].s1=%f \n",i,neurone[debut_gpe_precedent_robot+i].s1);
      printf("activite du robot neurone[%d].s2=%f \n",i,neurone[debut_gpe_precedent_robot+i].s2);
#endif

    }
  }

#ifdef DEBUG
  for(i=0;i<10;i++)
  {
    printf("activite du groupe produit[%d].s=%f \n",i,neurone[debut_gpe+i].s);
    printf("activite du groupe produit[%d].s1=%f \n",i,neurone[debut_gpe+i].s1);
    printf("activite du groupe produit[%d].s2=%f \n",i,neurone[debut_gpe+i].s2);

  }
#endif 

#ifdef DEBUG
  printf("sortie de %s\n",__FUNCTION__);
#endif  
  (void) i_max_robot;
}
