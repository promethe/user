/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_hough_neurone.c
\brief

- author: R. Shirakawa / P. Gaussier
- description: put coordinates in neurones (neurone 1 for X and neurone 2 for Y)
- date: 27/07/2006


Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

  ===================================================
  COPIE DE function_circular_hough_transform_for_head
  ===================================================

  Fonction transformee de HOUGH circulaire pour la tete de robot
  Prend en entree une image de gradient (detection des contours)

  image_gradient   : image du gradient ou l'on veut chercher les cercles.
  attention si l'image du gradient n'est pas convenablement seuillee
  alors les resultat peuvent etre pas tres bon !!
  sur le lien de la boite qui va au gradient, passez quelque chose
  comme : -A1.0-Fc-S60 par exemple. Attention le seuil n'est pris
  en compte qu'avec l'option -Fc

  pour le trace des cercles sur l'image, attention, il y a un bord
  blanc de 10 pixels a prendre en compte.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #define DEBUG */

void function_hough_neurone(int gpe_sortie)
{
   /*  int j; */
   int i;
   int link1 = -1;

   int a, b, r, x, y, v;
   int maxima;
   int pt_x_max, pt_y_max;
   int amin, amax, bmin, bmax;

#ifndef AVEUGLE
   TxPoint point, point_depart, point_arrivee;
   int bordx = 0, bordy = 0;   /* changements des bords Braik B. */
   float vf;
#endif


   prom_images_struct *image_gradient = NULL;

   /* definition des variables de gestion de la carte neuronale */
   int first_neuron = def_groupe[gpe_sortie].premier_ele;

   /* declaration faite dans hough.h */
   hough_struct *t_hough = NULL;
   int *accumulator_of_hough = NULL;
   /*!
      The structure for stroring parameter of hough transform is in hough.h
      typedef struct hough_struct_def
      {
      float seuil_max_locaux;
      unsigned int Haut;
      unsigned int Bas;
      unsigned int Gauche;
      unsigned int Droit;
      unsigned int largeur;
      unsigned int hauteur;
      unsigned int largeur_accu;
      unsigned int hauteur_accu;
      unsigned int zone_a_traiter;
      unsigned int PX;
      unsigned int PY;
      unsigned int calcul_ellipse;
      unsigned int f;
      unsigned int min_cercle;
      unsigned int max_cercle;
      unsigned int rayon_moyen;
      unsigned int numpixels;
      unsigned int image_windows_number;
      }hough_struct;
    */

   char resultat[256];         /* obligatoire pour prom_getopt */


   /*printf ("premiere entree dans la fonction \n");
      mettre ici les choses a ne faire qu'une fois */
   if (def_groupe[gpe_sortie].ext == NULL)
   {
      /* allocation de la structure t_hough */

      t_hough = (hough_struct *) malloc(sizeof(hough_struct));
      if (t_hough == NULL)
      {
         printf("impossible d'allouer une hough_struct dans le groupe %d\n",gpe_sortie);
         exit(0);
      }
      def_groupe[gpe_sortie].data = (unsigned char *) t_hough;

      /* valeurs par defaut pour si le parametre est absent */
      t_hough->groupe_entree = -1;
      t_hough->Haut = 0;
      t_hough->Bas = 0;
      t_hough->Gauche = 0;
      t_hough->Droit = 0;
      t_hough->zone_a_traiter = 1;
      t_hough->PX = 0;
      t_hough->PY = 0;
      t_hough->calcul_ellipse = 0;
      t_hough->fx = 2;
      t_hough->fy = 2;
      t_hough->seuil_max_locaux = 0.75;
      t_hough->image_windows_number = 2;


      /* Recherche de l'image du gradient en entree  */

      /*  i = 0;
         j = 0;
         while (i >= 0)
         {
         i = find_input_link (gpe_sortie, j);
         if (prom_getopt (liaison[i].nom, "-gradient", resultat) == 2)
         {
         link1 = i;
         t_hough->groupe_entree = liaison[i].depart;break;
         }
         j++;
         }
       */
      /*Finding the link */

      link1 = find_input_link(gpe_sortie, 0);
      t_hough->groupe_entree = liaison[link1].depart;
      i = link1;

      /*Pour verifier l'existence du liens */
      if (link1 == -1)
      {
         printf("*****> Il n'y a pas de groupe en entree -> entrez un gradient\n");
         exit(EXIT_FAILURE);
      }

      dprints("*****> gpe_entree1 = %d   link1=%d sur le groupe %d\n", t_hough->groupe_entree, link1, gpe_sortie);

      image_gradient =(prom_images_struct *) def_groupe[t_hough->groupe_entree].ext;

      if (image_gradient == NULL)
      {
         EXIT_ON_ERROR("Probleme (function_circular_hough_transform : pas d'image de gradient dans le groupe %i\n",t_hough->groupe_entree);
      }


      def_groupe[gpe_sortie].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
      if (def_groupe[gpe_sortie].ext == NULL)
      {
         EXIT_ON_ERROR("impossible d'allouer l'ext dans le groupe %d\n", gpe_sortie);
      }


      /* allocation memoire pour les coordonnees des centres des visages et initialisation */

      /*centre= (TxPoint*) malloc (sizeof (TxPoint));
         ((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7] = (char*) centre ; */

      /*!
         recuperation des differents parametres sur le lien qui contient le -gradient
         ATTENTION a bien respecter minuscule et majuscule

         -r pour la taille minimum du cercle a rechercher
         -R pour la taille maximum du cercle a rechercher

         -H Haut   : pourcentage entier de la tranche horizontale  haute de l'image a ne pas traiter (facultatif)
         -B Bas    : pourcentage entier de la tranche horizontale  basse de l'image a ne pas traiter (facultatif)
         -G Gauche : pourcentage entier de la tranche verticale gauche de l'image a ne pas traiter (facultatif)
         -D Droite : pourcentage entier de la tranche verticale droite de l'image a ne pas traiter (facultatif)
         -z zone   : affiche la zone d'image traitee sous forme d'un rectangle vert
         -X        : position origine en x de l'image ou l'on superpose les cercles
         -Y        : position origine en y de l'image ou l'on superpose les cercles
         -En       : pour rechercher une ellipse au lieu d'un cercle. passez en parametre la valeur de f dans l'equation de l'ellipse.
         -S        : passer un float pour l'affichage des max locaux. Tous ceux superieur a ce pourcentage seront affiches.
         -In       : n=1 ou 2 suivant la fenetre dans laquelle vous voulez afficher l'image

         pour l'instant le programme ne fait pas les controles de validites compliques qu'il faudrait
         mettre en place pour -H -B -G -D
         aussi merci de faire en sorte que 0 > H+B < 99% et 0 > G+D <99%
         egalement passez une valeur entiere valable
         sinon ....
       */

      if (prom_getopt(liaison[i].nom, "-r", resultat) == 2) t_hough->min_cercle = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-R", resultat) == 2) t_hough->max_cercle = atoi(resultat);


#ifdef DEBUG
      printf("\n\n\n------------------------------------------------\n");
      printf("--- FONCTION Transformee de HOUHG Circulaire ---\n\n\n");
#endif


      if (prom_getopt(liaison[i].nom, "-H", resultat) == 2)  t_hough->Haut = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-B", resultat) == 2)  t_hough->Bas = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-G", resultat) == 2)  t_hough->Gauche = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-D", resultat) == 2)  t_hough->Droit = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-z", resultat) == 2)  t_hough->zone_a_traiter = 1;
      if (prom_getopt(liaison[i].nom, "-X", resultat) == 2)  t_hough->PX = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-Y", resultat) == 2)  t_hough->PY = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-I", resultat) == 2)  t_hough->image_windows_number = atoi(resultat);
      if (prom_getopt(liaison[i].nom, "-x", resultat) == 2)
      {
         t_hough->calcul_ellipse = 1;
         t_hough->fx = atoi(resultat);
      }

      if (prom_getopt(liaison[i].nom, "-y", resultat) == 2)
      {
         t_hough->calcul_ellipse = 1;
         t_hough->fy = atoi(resultat);
      }

      if (prom_getopt(liaison[i].nom, "-S", resultat) == 2)  t_hough->seuil_max_locaux = atof(resultat);

      t_hough->largeur = image_gradient->sx;
      t_hough->hauteur = image_gradient->sy;
      t_hough->numpixels = t_hough->largeur * t_hough->hauteur;

      /*initialisation des coordonnees du centre du cercle  (ou de l'ellipse) gagnant(e) */
      t_hough->x = t_hough->largeur / 2;
      t_hough->y = t_hough->hauteur / 2;


      t_hough->Haut = (int) ((t_hough->Haut * t_hough->hauteur) / 100);
      t_hough->Bas = (int) ((t_hough->Bas * t_hough->hauteur) / 100);
      t_hough->Gauche = (int) ((t_hough->Gauche * t_hough->largeur) / 100);
      t_hough->Droit = (int) ((t_hough->Droit * t_hough->largeur) / 100);


      t_hough->rayon_moyen =(int) ((t_hough->max_cercle - t_hough->min_cercle) / 2) +t_hough->min_cercle;

      t_hough->largeur_accu =t_hough->largeur - t_hough->Gauche - t_hough->Droit;
      t_hough->hauteur_accu =t_hough->hauteur - t_hough->Haut - t_hough->Bas;

      if (t_hough->rayon_moyen < 1)  t_hough->rayon_moyen = 1;

#ifdef DEBUG
      printf("groupe d'entree = %d\n", t_hough->groupe_entree);
      printf("largeur image = %d \n", t_hough->largeur);
      printf("hauteur image = %d \n", t_hough->hauteur);
      printf("largeur accu  = %d \n", t_hough->largeur_accu);
      printf("hauteur accu  = %d \n", t_hough->hauteur_accu);
      printf("max_cercle = %d \n", t_hough->max_cercle);
      printf("min_cercle = %d \n", t_hough->min_cercle);
      printf("numpixels  = %d \n", t_hough->numpixels);
      printf("-Haut   = %d\n", t_hough->Haut);
      printf("-Bas    = %d\n", t_hough->Bas);
      printf("-Gauche = %d\n", t_hough->Gauche);
      printf("-Droit  = %d\n", t_hough->Droit);
      printf("-z      = %d\n", t_hough->zone_a_traiter);
      printf("-X      = %d\n", t_hough->PX);
      printf("-Y      = %d\n", t_hough->PY);
      printf("fx       = %d\n", t_hough->fx);
      printf("fy       = %d\n", t_hough->fy);
      /*       printf("size_XY = %d\n", size_XY); */
      printf("r_moyen = %d\n", t_hough->rayon_moyen);
      printf("-I      = %d\n", t_hough->image_windows_number);
      if (t_hough->calcul_ellipse == 1)
         printf("-E ellipse  = oui\n");
      else
         printf("-E ellipse  = non\n");
      printf("seuil max locaux = %f\n", t_hough->seuil_max_locaux);
#endif

      /* allocation du tableau 2D pour la transformee de HOUHG */
      /* on ne peut pas le faire avant de connaitre largeur et hauteur */
      accumulator_of_hough = (int *) malloc(t_hough->largeur * t_hough->hauteur * sizeof(int));

      ((prom_images_struct *) def_groupe[gpe_sortie].ext)->images_table[1] = (unsigned char *) accumulator_of_hough;

      dprints("*****> initialisation de l'accumulateur \n");

      /* end of first declarations */
   }
   else
   {
      t_hough = (hough_struct *) ((prom_images_struct *) def_groupe[gpe_sortie].data);
      accumulator_of_hough = (int *) ((prom_images_struct *) def_groupe[gpe_sortie].ext)->images_table[1];
      /*centre=(TxPoint*)((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7]; */
      image_gradient = (prom_images_struct *) def_groupe[t_hough->groupe_entree].ext;
   }


#ifndef AVEUGLE
   /* on dessine un rectangle vert autour de la zone a traiter */
   if (t_hough->zone_a_traiter == 1)
   {
      dprints("*****>je dessine le rectangle vert qui delimite la zone de travail dans la fenetre %d \n",t_hough->image_windows_number);

      point.x = t_hough->Gauche + bordx + t_hough->PX;
      point.y = t_hough->Haut + bordy + t_hough->PY;
      /* TxDessinerRectangle(ptr_image,couleur,TxVide ou TxPlein,point d'origine de type TxPoint,
         largeur,hauteur,epaisseur) */

      if (t_hough->image_windows_number == 1)
      {
         TxDessinerRectangle(&image1, vert, TxVide, point,t_hough->largeur - t_hough->Gauche -t_hough->Droit,t_hough->hauteur - t_hough->Haut - t_hough->Bas, 1);
         /*TxFlush (&image1); BRAIK B. le 15/04/04 */
      }
      else
      {
         TxDessinerRectangle(&image2, vert, TxVide, point,t_hough->largeur - t_hough->Gauche - t_hough->Droit, t_hough->hauteur - t_hough->Haut -t_hough->Bas, 1);
         /*TxFlush (&image2); BRAIK B. le 15/04/04 */
      }
   }
#endif

   /* initialisation des coordonnees du centre du cercle gagnant */

   /*centre->x=image_gradient->sx/2;
      centre->y=image_gradient->sy/2; */

   /* initialisation a zero du tableau accumulator_of_hough a chaque nouvelle analyse */
   for (a = 0; a < t_hough->largeur; a++)
      for (b = 0; b < t_hough->hauteur; b++)
         accumulator_of_hough[a + b * t_hough->largeur] = 0;
         
   dprints("*****> recherche des pixels utiles dans l'image du gradient\n");

   /* trouve dans l'image binaire les pixels utiles et valorise l'accumulateur en consequence */
   for (i = 0; i < t_hough->numpixels; i++)
   {
      if (image_gradient->images_table[0][i] != 0)
      {
         x = i % t_hough->largeur;
         y = i / t_hough->largeur;

         amin = x - t_hough->max_cercle; amax = x + t_hough->max_cercle;
         if (amin < t_hough->Gauche)                   amin = t_hough->Gauche;
         if (amax > t_hough->largeur - t_hough->Droit) amax = t_hough->largeur - t_hough->Droit;

         bmin = y - t_hough->max_cercle; bmax = y + t_hough->max_cercle;
         if (bmin < t_hough->Haut)                     bmin = t_hough->Haut;
         if (bmax > t_hough->hauteur - t_hough->Bas)   bmax = t_hough->hauteur - t_hough->Bas;


         /* on aurait pu economiser des lignes de codes ici puisque un cercle
            est une ellipse particuliere, mais en faisant un if else on gagne
            beaucoup plus en tant de calcul si l'on est dans le cas du cercle. */

         if (t_hough->calcul_ellipse == 1)   /* on calcul pour une ellipse */
         {
            for (a = amin; a < amax; a++)
               for (b = bmin; b < bmax; b++)
               {
                  r = 0.5 *(sqrt((x - a + t_hough->fx) * (x - a + t_hough->fx) +(y - b + t_hough->fy) * (y - b + t_hough->fy)) +
                            sqrt((x - a - t_hough->fx) * (x - a -t_hough->fx) + (y - b -t_hough->fy)* (y - b - t_hough->fy)));
                  if ((r >= t_hough->min_cercle) && (r < t_hough->max_cercle))
                     /* on multipli par 2.PI.R pour avoir une importance au prorata
                        de la taille du cercle */
                     accumulator_of_hough[a + b * t_hough->largeur] = accumulator_of_hough[a +b * t_hough->largeur] +6.28 * r;

                  /* accumulator_of_hough[a + b * largeur]++; */
               }
         }
         else                /* on calcul pour un cercle */
         {
            for (a = amin; a < amax; a++)
               for (b = bmin; b < bmax; b++)
               {
                  r = (int) sqrt((x - a) * (x - a) + (y - b) * (y - b));
                  if ((r >= t_hough->min_cercle) && (r < t_hough->max_cercle))
                     accumulator_of_hough[a + b * t_hough->largeur] =accumulator_of_hough[a + b * t_hough->largeur] +6.28 * r;

                  /*accumulator_of_hough[a + b * largeur]++; */
               }
         }

      }
   }

   dprints("*****> recherche des maximas\n");

   /* recherche des maximas dans le tableau accumulateur pour trouver les
      cercles de taille entre min_cercle et max_cercle donnee sur le lien */



   /* inititialisations */
   maxima = 0;
   pt_x_max = 0;
   pt_y_max = 0;
#ifndef AVEUGLE   
   point.x = 0;
   point.y = 0;
#endif
   /* on recherche les maxima pouvant correspondre aux yeux */

   for (a = t_hough->Gauche; a < t_hough->largeur - t_hough->Droit; a++)
      for (b = t_hough->Haut * t_hough->largeur; b < t_hough->numpixels - t_hough->Bas * t_hough->largeur; b += t_hough->largeur)
      {
         v = accumulator_of_hough[a + b];
         if (v > maxima)
         {
            maxima = v;
            pt_y_max = (int) (b / t_hough->largeur);
            t_hough->y = pt_y_max;
            pt_x_max = a;
            t_hough->x = pt_x_max;
         }
         
#ifndef AVEUGLE         
         vf = (float) v;     /*modification Braik b. due a la comparaison de v avec un reel ci-dessous */
         if (vf > (maxima * t_hough->seuil_max_locaux))
         {
            point.x = a + bordx + t_hough->PX;
            point.y = (b / t_hough->largeur) + bordy + t_hough->PY;

            if (t_hough->image_windows_number == 1)
            {
               TxDessinerCercle(&image1, bleu, TxVide, point,t_hough->rayon_moyen, 1);
               /*TxFlush (&image1); BRAIK B. le 15/04/04 */
            }
            else
            {
               TxDessinerCercle(&image2, bleu, TxVide, point,t_hough->rayon_moyen, 1);
               /*TxFlush (&image2); BRAIK B. le 15/04/04 */
            }
         }
#endif         
      }


#ifdef DEBUG
   if (maxima == 0) printf("pas de maxima trouve\n");
   else
   {
      /* donc pas besoin de valoriser la carte neuronale qui vaut zero partout */
      printf("le maxima trouve en (x,y)=(%d,%d) maxima=%d\n", pt_x_max, pt_y_max, maxima);
      printf("****> trace le cercle dans la fenetre %d \n", t_hough->image_windows_number);
   }
#endif

#ifndef AVEUGLE
   /* trace le cercle gagnant dans la fenetre image1 ou 2 de promethe */
   point.x = pt_x_max + t_hough->PX + bordx;   /* modif de PX en t_hough->PX 22/04/04 */
   point.y = pt_y_max + bordy + t_hough->PY;

   if (t_hough->image_windows_number == 1)
   {
      TxDessinerCercle(&image1, rouge, TxVide, point, t_hough->min_cercle,1);
      TxDessinerCercle(&image1, rouge, TxVide, point, t_hough->max_cercle,1);
   }
   else
   {
      TxDessinerCercle(&image2, rouge, TxVide, point, t_hough->min_cercle,1);
      TxDessinerCercle(&image2, rouge, TxVide, point, t_hough->max_cercle,1);
   }

   if (t_hough->min_cercle > 7)
   {
      /* on trace une croix sur le centre du cercle pour les tres gros cercles */
      point_depart = point;
      point_arrivee = point;
      point_depart.x = point.x - 4;
      point_arrivee.x = point.x + 4;

      if (t_hough->image_windows_number == 1)   TxDessinerSegment(&image1, rouge, point_depart, point_arrivee, 1);
      else                                      TxDessinerSegment(&image2, rouge, point_depart, point_arrivee, 1);

      point_depart.x = point.x;
      point_arrivee.x = point.x;
      point_depart.y = point.y - 4;
      point_arrivee.y = point_arrivee.y + 4;

      if (t_hough->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, rouge, point_depart, point_arrivee, 1);
         TxFlush(&image1);   /*BRAIK B. le 15/04/04 */
      }
      else
      {
         TxDessinerSegment(&image2, rouge, point_depart, point_arrivee, 1);
         TxFlush(&image2);   /*BRAIK B. le 15/04/04 */
      }
      TxFlush(&image2);
   }
#endif


   dprints("largeur=%d hauteur=%d\n",,t_hough->largeur, t_hough->hauteur);
   dprints("*****> transfert de l'accumulateur dans la carte neuronale \n");

   /* initialisation a zero de la carte neuronale
      printf("first neuron=%d  last_neuron=%d\n",first_neuron,last_neuron); */
   neurone[first_neuron].s = t_hough->x;
   neurone[first_neuron + 1].s = t_hough->y;

   dprints("neurone[first_neuron] = %f \t neurone[first_neuron + 1] = %f\n", neurone[first_neuron].s, neurone[first_neuron + 1].s);

}
