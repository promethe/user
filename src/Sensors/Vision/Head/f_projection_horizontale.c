/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_projection_horizontale.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    Cette fonction prend une image en entr� et r�lise un vecteur plac�dans un r�eau de neurones.
    Ce vecteur correspond �la projection de la somme des valeurs des pixels pour chaque ligne.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/projection.h>
#include <Struct/prom_images_struct.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
/* #define DEBUG */

void function_projection_horizontale(int Gpe)
{
    int taille_masque = 20;
    unsigned char *image;
    char resultat[256];
    int link = -1;
    int numpixel;
    int i = 0, j;
    int *histogramme_hor;
    int * moyennage_hor;
    float max_moy;

    projection *temp;

#ifndef AVEUGLE
    int max, indice_max2 = 0, indice_max = 0;
    TxPoint point_depart, point_arrivee, point_depart2, point_arrivee2;
#endif


/*Mesure du temps de traitement dans la fonction*/
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        /* allocation de memoire */
        def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        temp = (projection *) malloc(sizeof(projection));
        def_groupe[Gpe].data = (unsigned char *) temp;

        /* param�res par d�ault */

        temp->image_windows_number = 1;
        temp->seuil = 0.4;
        temp->groupe_entree = -1;
        temp->nx = 384;
        temp->ny = 288;
        temp->n = 110592;

        /*Finding the link */

        link = find_input_link(Gpe, 0);
        temp->groupe_entree = liaison[link].depart;

        if (link == -1)
        {
            printf("*****> Il n'y a pas d'image en entr� du Gpe %d\n", Gpe);
            exit(EXIT_FAILURE);
        }

/* image pointeur sur l'image du groupe pr��ent*/
        image =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->
            images_table[0];


/* recuperation des infos sur la taille */
        temp->nx =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sx;
        temp->ny =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->sy;

        temp->n = temp->nx * temp->ny;

#ifdef DEBUG
        printf("i dans projection horizontale : %d\n", i);
        printf("temp->groupe_entree dans projection horizontale : %d\n",
               temp->groupe_entree);
        printf("link dans projection horizontale : %d\n", link);
#endif

        /*recup�ation des infos sur le lien */
        if (prom_getopt(liaison[link].nom, "-I", resultat) == 2)
        {
            temp->image_windows_number = atoi(resultat);
        }
        if (prom_getopt(liaison[link].nom, "-S", resultat) == 2)
        {
            temp->seuil = atof(resultat);
        }

        /*recherche du num�o du premier neurone du groupe et calcul de l'�helle
           pour la projection dans le r�eau de neurones */

        temp->premier_neurone = def_groupe[Gpe].premier_ele;
        temp->ech = (float) def_groupe[Gpe].nbre / (float) temp->ny;

#ifdef DEBUG
        printf("temp->premier_neurone dans projection horizontale : %d\n",
               temp->premier_neurone);
        printf("temp->ech dans projection horizontale : %f\n", temp->ech);
#endif

        /* allocation de memoire pour l'histogramme verticale */
        histogramme_hor = (int *) malloc(temp->ny * sizeof(int));

        if (histogramme_hor == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE POUR L'HISTOGRAMME...! \n");
            exit(-1);
        }

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) histogramme_hor;

        /* allocation de memoire pour l'histogramme moyenn� */
        moyennage_hor = (int *) malloc(temp->ny * sizeof(int));

        if (moyennage_hor == NULL)
        {
            printf
                ("ALLOCATION IMPOSSIBLE POUR L'HISTOGRAMME MOYENNED..! \n");
            exit(-1);
        }

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) moyennage_hor;

#ifdef DEBUG
        printf
            (" valeur des diff�entes variables dans la fonction de l'histogramme horizontale\n");
        printf(" images_windows_number = %d\n", temp->image_windows_number);
        printf(" seuil = %f \n", temp->seuil);
        printf(" groupe entree %d\n", temp->groupe_entree);
        printf(" largeur de l'image en entree = %d\n", temp->nx);
        printf(" hauteur de l'image en entree = %d\n", temp->ny);
        printf(" nombre de pixels de l'image en entr� = %d\n", temp->n);
        printf(" num�o du premier neurone = %d\n", temp->premier_neurone);
        printf
            (" echelle pour la projection dans le r�eau de neurones = %f\n",
             temp->ech);
#endif

    }
    else
    {
        histogramme_hor =
            (int *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                     images_table[0]);
        temp = (projection *) ((prom_images_struct *) def_groupe[Gpe].data);
        /* image pointeur sur l'image du groupe pr��ent */
        image =
            ((prom_images_struct *) def_groupe[temp->groupe_entree].ext)->
            images_table[0];

        moyennage_hor =
            (int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];
    }

    for (i = 0; i < temp->ny; i++)
    {
        histogramme_hor[i] = 0;

        for (j = 0; j < temp->nx; j++)
        {
            numpixel = i * temp->nx + j;
            /*printf("\n numero de pixel de %d \n",numpixel); */
            histogramme_hor[i] = histogramme_hor[i] + (int) image[numpixel];

        }
        /*printf("\n %d %d %d \n",i,(int)image[numpixel],histogramme_ver[i]); */
    }

    for (i = 0; i < taille_masque / 2; i++)
    {
        moyennage_hor[i] = 0;
        moyennage_hor[temp->ny - i - 1] = 0;
    }
    max_moy = 0;
    for (i = taille_masque / 2; i < temp->ny - taille_masque / 2; i++)
    {
        moyennage_hor[i] = 0;
        for (j = -taille_masque / 2; j < taille_masque / 2; j++)
            moyennage_hor[i] = moyennage_hor[i] + histogramme_hor[i + j];
        moyennage_hor[i] = moyennage_hor[i] / 20;
        if (moyennage_hor[i] > max_moy)
            max_moy = moyennage_hor[i];
    }
#ifndef AVEUGLE
    /*initialisation des coordonn�s du cadre */
    indice_max = 0;
    indice_max2 = temp->ny - 1;


    max = -9999;
    for (i = taille_masque / 2; i < temp->ny; i++)
    {
        if (moyennage_hor[i] > max_moy * temp->seuil)
        {
            indice_max2 = i;
            break;
        }
    }

    /* Recherche de la premi�e valeur sup�ieure au produit du seuil avec le max du tableau moyennage_ver dans la deuxi�e moiti�de ce tableau */

    for (i = temp->ny - 1 - taille_masque / 2; i > temp->ny / 2; i--)
    {
        if (moyennage_hor[i] > max_moy * temp->seuil)
        {
            indice_max = i;
            break;
        }
    }

#endif

    /*projection de l'histogramme verticale dans un r�eau de neurones */
    for (i = 0; i < temp->ny; i++)
    {
        j = (int) (i * temp->ech) + temp->premier_neurone;
        neurone[j].s2 = 0.;
        neurone[j].s1 = 0.;
        neurone[j].s = (float) moyennage_hor[i] / max_moy;
    }

#ifndef AVEUGLE
    /*Affichage des traits de cadrage */
    point_depart.x = 0;
    point_depart.y = indice_max;
    point_arrivee.x = temp->ny - 1;
    point_arrivee.y = indice_max;

    point_depart2.x = 0;
    point_depart2.y = indice_max2;
    point_arrivee2.x = temp->ny - 1;
    point_arrivee2.y = indice_max2;
    if (temp->image_windows_number == 1)
    {
        TxDessinerSegment(&image1, vert, point_depart, point_arrivee, 1);
        TxFlush(&image1);
        TxDessinerSegment(&image1, rouge, point_depart2, point_arrivee2, 1);
        TxFlush(&image1);
    }
    else
    {
        TxDessinerSegment(&image2, vert, point_depart, point_arrivee, 1);
        TxFlush(&image2);
        TxDessinerSegment(&image2, rouge, point_depart2, point_arrivee2, 1);
        TxFlush(&image2);
    }
#endif
    /*Fin du Timer et affichage des temps */

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", Gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif
}
