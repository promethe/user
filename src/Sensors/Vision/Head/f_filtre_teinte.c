/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_filtre_teinte.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Cette fonction permet de creer, a partir d'une image de teinte, une image en niveau de gris recopiant les pixels qui ont la meme teinte que celle precisee sur le lien via "-Vvaleur" et tolerant un certain ecart par rapport a cette valeur precise par "-Tvaleur". Les valeurs doivent etre donnees en pixels.Tous les pixels ne repondant pas a ces criteres sont forces a zero.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/filtre.h>
#include "public_tools/Vision.h"

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct data_filtre_teinte
{
   int gpe_image;
   float valeur;
   float range;
} Data_filtre_teinte;


void function_filtre_teinte(int index_of_gpe)
{
   Data_filtre_teinte *data=NULL;
   prom_images_struct *image_input_struct = NULL, *image_output_struct = NULL;
   char resultat[256];
   int gpe_image=-1, l = -1;
   int i = 0;
   float valeur=0., range=0., distance;
   float *t_image;

   if (def_groupe[index_of_gpe].data == NULL)
   {
      printf("coucou %d\n", __LINE__);
      i=0;
      l = find_input_link(index_of_gpe, i);
      while (l != -1)
      {
         if (strstr(liaison[l].nom, "sync") != NULL)     continue;
         if (prom_getopt(liaison[l].nom, "-V", resultat) == 2)   valeur = atof(resultat);
         if (prom_getopt(liaison[l].nom, "-T", resultat) == 2)   range = atof(resultat);

         gpe_image = liaison[l].depart;
         i++;
         l = find_input_link(index_of_gpe, i);
      }
      /* Pour verifier l'existence du lien */
      if (gpe_image == -1)   EXIT_ON_ERROR("le groupe n'a pas d'entree !");
      data = (Data_filtre_teinte *)malloc(sizeof(Data_filtre_teinte));
      data->gpe_image = gpe_image;
      data->range = range;
      data->valeur = valeur;
      def_groupe[index_of_gpe].data = data;
   }
   data = def_groupe[index_of_gpe].data;
   gpe_image = data->gpe_image;
   valeur = data->valeur;
   range = data->range;

   image_input_struct = def_groupe[data->gpe_image].ext;

   if (image_input_struct == NULL)
   {
      PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
      return ;
   }
   if (image_input_struct->image_number != 4)   EXIT_ON_ERROR("the input group must be f_image_des_teintes");

   if (def_groupe[index_of_gpe].ext == NULL)
   {
      /*image pointeur sur l'image du groupe precedent */
      image_input_struct = ((prom_images_struct *) def_groupe[data->gpe_image].ext);
      if (image_input_struct == NULL)
      {
         PRINT_WARNING("no ext in the previous group");
         return;
      }
      /* allocation de memoire */
      image_output_struct = calloc_prom_image(1, image_input_struct->sx, image_input_struct->sy, 1);
      if (image_output_struct == NULL)   EXIT_ON_ERROR("impossible d'allouer la memoire");
      def_groupe[index_of_gpe].ext = image_output_struct;
   }
   image_output_struct = def_groupe[index_of_gpe].ext;

   t_image = (float *)image_input_struct->images_table[1];

   for (i = 0; i < image_input_struct->sx * image_input_struct->sy; i++)
   {
      distance = fabs(t_image[i] - valeur);
      if (distance > 127.)
         distance = 255. - distance;
      /*distance += 0.5 * s_image[i];*/

      if (distance > range)  image_output_struct->images_table[0][i] = 0;
      else                   image_output_struct->images_table[0][i] = 255 - (unsigned char)((int)distance);
   }
}
