/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_cadre_RN.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Fonction permettant une mise a l'echelle et une affectation d'un visage dans un reseau de neurones.
 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>

#include <Kernel_Function/find_input_link.h>

void function_cadre_RN(int Gpe)
{
  prom_images_struct *image, *p_result;
  unsigned int i, j, nx, ny;
  int link = -1;
  int InputGep = -1;
  int Debut_Gpe;
  int TailleGroupeX, TailleGroupeY;
  int SizeZoneX, SizeZoneY;
  int m, k, l, n, Nhv, i1, j1;

  unsigned char *CarteProjection;

  /*Mesure du temps de traitement dans la fonction */
#ifdef TIME_TRACE
  struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
  long SecondesFunctionTimeTrace;
  long MicroSecondesFunctionTimeTrace;
  char MessageFunctionTimeTrace[255];
#endif

  /*Lancement du timer */
#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  /* initialisation locale */
  Debut_Gpe = def_groupe[Gpe].premier_ele;
  TailleGroupeX = def_groupe[Gpe].taillex;
  TailleGroupeY = def_groupe[Gpe].tailley;
  Nhv = TailleGroupeX * TailleGroupeY;

  /*Finding the link */

  link = find_input_link(Gpe, 0);
  InputGep = liaison[link].depart;

  if (link == -1)
  {
    printf("*****> Il n'y a pas d'image en entree du Gpe %d\n", Gpe);
    exit(EXIT_FAILURE);
  }

  /******recuperation de l'image precedente********/

  image = (prom_images_struct *) def_groupe[InputGep].ext;

  if (image == NULL)
  {
    printf("Probleme : pas d'image dans le groupe %d\n", InputGep);
    exit(EXIT_FAILURE);
  }

  if (image->nb_band != 1)
  {
    printf("\n l'image en entree de f_cadrer_depuis_histogrammes doit etre en noir et blanc !\n");
    exit(EXIT_FAILURE);
  }

  nx = ((prom_images_struct *) def_groupe[InputGep].ext)->sx;
  ny = ((prom_images_struct *) def_groupe[InputGep].ext)->sy;

  /* Allocation prom_images_strct */
  if (def_groupe[Gpe].ext == NULL)
  {
    printf("premiere alloc ext \n");
    p_result = (void *) malloc(sizeof(prom_images_struct));
    ;
    if (p_result == NULL)
    {
      printf("ALLOCATION IMPOSSIBLE DANS DECOUPER IMAGE...! \n");
      exit(-1);
    }

    def_groupe[Gpe].ext = p_result;

    /* allocation pour la projection de l'image dans le reseau de neurones */
    CarteProjection = (unsigned char *) malloc(Nhv * sizeof(unsigned char));
    if (CarteProjection == NULL)
    EXIT_ON_ERROR("Not enought memory!");

    p_result->images_table[1] = CarteProjection;

  }
  else
  {
    p_result = ((prom_images_struct *) def_groupe[Gpe].ext);
    CarteProjection = p_result->images_table[1];
  }

  /* reset of outputs */
  for (i = (unsigned int)Debut_Gpe, j = 0; j < (unsigned int)Nhv; i++, j++)
  {
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    CarteProjection[j] = 0;
  }

  /* local initialisations */
  SizeZoneX = nx / TailleGroupeX;
  SizeZoneY = ny / TailleGroupeY;
  /* les projections verticales et horizontales */
  i1 = TailleGroupeY * SizeZoneY;
  j1 = TailleGroupeX * SizeZoneX;
  for (i = 0; i < (unsigned int)i1; i++)
  {
    m = i / SizeZoneY; /* indice Oy pour reseaux */
    for (j = 0; j < (unsigned int)j1; j++)
    {
      l = j / SizeZoneX; /* indice Ox pour reseaux */
      k = nx * i + j; /* indice dans l'image */
      n = TailleGroupeX * m + l; /* indice dans projection */
      /*if( ImageResultat[k] > 0 ) */
      CarteProjection[n] = image->images_table[0][k];
      /*printf("\nCarteProjection[n] %c\n",CarteProjection[n]); */
      /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
    }
  }

  for (i = 0; i < (unsigned int)Nhv; i++)
    /*if( (int)CarteProjection[i] > SeuilDetection ) */
    neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / 255; /*((float)(SizeZoneX*SizeZoneY)); */

  /*Fin du Timer et affichage des temps */
#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
    MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  printf("Fonction du groupe %d\n", Gpe);
  sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n", SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  /*   affiche_message(MessageFunctionTimeTrace); */
  printf("Chaine MessageFunctionTimeTrace %s", MessageFunctionTimeTrace);
#endif

}
