/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_decouper_image.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004
Modified:
- author: R.Shirakawa
- description: change variable 'int numpixels' to 'unsigned int numpixels' for use with 320x240 images
- date: 13/04/2006

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


void function_decouper_image(int gpe_sortie)
{
    int i = 0, j;
    int link1 = -1, link2 = -1;
    int Debut_Gpe;
    int TailleGroupeX, TailleGroupeY;
    int SizeZoneX, SizeZoneY;
    int m, k, l, n, Nhv, i1, j1;

    unsigned char *CarteProjection;

    int a, b, co, li, num;
    unsigned int numpixels;

    prom_images_struct *image = NULL;
    prom_images_struct *p_result = NULL;

    decoupage *temp;

    char resultat[256];

    hough_struct *t_hough = NULL;

/*Mesure du temps de traitement dans la fonction*/
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* initialisation locale */
    Debut_Gpe = def_groupe[gpe_sortie].premier_ele;
    TailleGroupeX = def_groupe[gpe_sortie].taillex;
    TailleGroupeY = def_groupe[gpe_sortie].tailley;
    Nhv = TailleGroupeX * TailleGroupeY;

    /* Allocation prom_images_strct */
    if (def_groupe[gpe_sortie].ext == NULL)
    {

        /*recuperation des infos sur les differents liens */
        temp = (decoupage *) malloc(sizeof(decoupage));
        def_groupe[gpe_sortie].data = (decoupage *) temp;

        /*initialisation */
        temp->height = 100;
        temp->width = 100;
        temp->groupe_entree_image = -1;
        temp->groupe_entree_coord = -1;
        temp->largeur = 384;
        temp->hauteur = 288;
        temp->Nxy = 100592;

        link1 = find_input_link(gpe_sortie, 0);
        link2 = find_input_link(gpe_sortie, 1);

        if ((link1 == -1) || (link2 == -1))
        {
            printf("*****> Il n'y a pas deux groupe en entree du Gpe %d\n", gpe_sortie);
            exit(EXIT_FAILURE);
        }

        if ((prom_getopt(liaison[link1].nom, "-image", resultat) == 2)
            && (prom_getopt(liaison[link2].nom, "-coord", resultat) == 2))
        {
            temp->groupe_entree_image = liaison[link1].depart;
            temp->groupe_entree_coord = liaison[link2].depart;
            i = link2;
        }
        else
        {
            temp->groupe_entree_image = liaison[link2].depart;
            temp->groupe_entree_coord = liaison[link1].depart;
            i = link1;
        }

#ifdef DEBUG
        printf("*****> gpe_entree 1 (image) = %d   link1=%d\n",
               temp->groupe_entree_image, link1);
        printf("*****> gpe_entree 2 (coord) = %d   link2=%d\n",
               temp->groupe_entree_coord, link2);
#endif

        if (prom_getopt(liaison[i].nom, "-H", resultat) == 2)
        {
            temp->height = atoi(resultat);
        }
        if (prom_getopt(liaison[i].nom, "-W", resultat) == 2)
        {
            temp->width = atoi(resultat);
        }

        /*recuperation de l'image du groupe precednt */
        image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;

        if (image == NULL)
        {
            printf("Probleme : pas d'image dans le groupe %i\n", temp->groupe_entree_image);
            exit(EXIT_FAILURE);
        }

        if (image->nb_band != 1)
        {
            printf("\n l'image en entree de f_decouper_image (Gpe %d)  doit etre en noir et blanc !\n", gpe_sortie);
            exit(EXIT_FAILURE);
        }

        temp->largeur =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->sx;
        temp->hauteur =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->sy;
        temp->Nxy = temp->largeur * temp->hauteur;


        printf("premiere alloc ext \n");
        p_result = (void *) malloc(sizeof(prom_images_struct));;
        if (p_result == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE DANS DECOUPER IMAGE...! \n");
            exit(-1);
        }

        p_result->image_number = 1;
        p_result->sx = temp->width;
        p_result->sy = temp->height;
        p_result->nb_band = 1;

        def_groupe[gpe_sortie].ext = p_result;

        /*allocation memoire pour l'imagette */
        p_result->images_table[0] =
            (unsigned char *) malloc(temp->width * temp->height *
                                     sizeof(unsigned char));

        /* allocation pour la projection de l'image dans le reseau de neurones */
        CarteProjection =
            (unsigned char *) malloc(Nhv * sizeof(unsigned char));
        if (CarteProjection == NULL)
            EXIT_ON_ERROR("Not enought memory!");

        p_result->images_table[1] = CarteProjection;

    }
    else
    {
        temp = (decoupage *) ((prom_images_struct *) def_groupe[gpe_sortie].data);

        p_result = ((prom_images_struct *) def_groupe[gpe_sortie].ext);
        CarteProjection = p_result->images_table[1];

        /*recuperation de l'image du groupe precedent */
        image = (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;
    }


/*recuperation des coordonnees du groupe precedent*/
    t_hough = (hough_struct *) ((prom_images_struct *) def_groupe[temp->groupe_entree_coord].data);


    /* recuperation des coordonnees du visage */
    /* centre = (TxPoint*) ((prom_images_struct*)def_groupe[temp->groupe_entree_coord].ext)->images_table[7]; */


    a = t_hough->x;
    b = t_hough->y;
/* printf("\n a = %d b = %d \n",a,b); */
    num = 0;


/************************************************************************************************/
    for (li = b - floor(temp->height / 2); li < b + floor(temp->height / 2);
         li++)
        for (co = a - floor(temp->width / 2); co < a + floor(temp->width / 2);
             co++)
        {
            numpixels = co + li * temp->largeur;
            /* printf("\n numero de pixel de l'image %d : %d\n",i,numpixels);
               printf("\n numero de pixel de l'imagette : %d\n",num); */
            if (numpixels < (uint)temp->Nxy)
            {
                p_result->images_table[0][num] = image->images_table[0][numpixels];
                num++;
            }

        }

    /* reset of outputs */
    for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
        CarteProjection[j] = 0;
    }


    /* printf("\njusque la avant boucle\n"); */
/* local initialisations */
    SizeZoneX = temp->width / TailleGroupeX;
    SizeZoneY = temp->height / TailleGroupeY;

#ifdef DEBUG
    printf("TailleGroupeY :%d\n", TailleGroupeY);
    printf("TailleGroupeX :%d\n", TailleGroupeX);
    printf("temp->height :%d\n", temp->height);
    printf("temp->width :%d\n", temp->width);
    printf("SizeZoneX :%d\n", SizeZoneX);
    printf("SizeZoneY :%d\n", SizeZoneY);
#endif

    /* les projections verticales et horizontales */
    i1 = TailleGroupeY * SizeZoneY;
    j1 = TailleGroupeX * SizeZoneX;

#ifdef DEBUG
    printf("i1 :%d\n", i1);
    printf("j1 :%d\n", j1);
#endif

    for (i = 0; i < i1; i++)
    {
        m = i / SizeZoneY;      /* indice Oy pour reseaux */
        for (j = 0; j < j1; j++)
        {
            l = j / SizeZoneX;  /* indice Ox pour reseaux */
            k = temp->width * i + j;    /* indice dans l'image */
            n = TailleGroupeX * m + l;  /* indice dans projection */
            /* if( ImageResultat[k] > 0 ) */
            CarteProjection[n] = p_result->images_table[0][k];
            /* printf("\nCarteProjection[n] %c\n",CarteProjection[n]); */
            /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
        }
    }

#ifdef DEBUG
    printf(" Valeur de m : %d\n", m);
    printf(" Valeur de l : %d\n", l);
    printf(" Valeur de k : %d\n", k);
    printf(" Valeur de n : %d\n", n);
#endif


    for (i = 0; i < Nhv; i++)
        neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / 255;    /*((float)(SizeZoneX*SizeZoneY)); */


/*Fin du Timer et affichage des temps*/
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
        MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", gpe_sortie);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n", SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif

}
