/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

\file  f_vergence.c
\ingroup libSensors
\defgroup f_vergence f_vergence
\brief

Author: V. Riabchenko

Created: --/09/2013

Description:
 Cette fonction prend deux images en entree:
 - l'une et principale
 - l'autre et dependante
 Elle chenche ou la centre d'une image principale se trouve dans une image dependante.

Macro:
- none

Local variables:
- none

Global variables:
- none

Internal Tools:
- none

External Tools:
- Kernel_Function/prom_getopt()
- Kernel_Function/find_input_link()

\section Options
-master: c'est une image principal
-slave: c'est une image dependant
-M: mode - seule valeur "graph". Si on ne donne pas -Mgraph, la boite est aveugle
-w: la taille de la fenetre. Celle-ci doit etre suivi par parameter X
-W: la taille de la fenetre. Celle-ci ne doit pas etre suivi par parameter. Marque le lien dont boite d'entree donne la taille.
-motor: donne la position de "slave" servomoteur. Marque le lien dont boite d'entree donne la valeur [0..1].
-XObj: coordonne X de point d'interet
-YObj: coordonne Y de point d'interet
-PX: coordonne X de la sortie graphique dans une image de Promethe
-PY: coordonne X de la sortie graphique dans une image de Promethe
-I: image de Promethe pour sortie graphique
**/

#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

enum work_mode {graph, nongraph};

const int MAX_WND_SIZE = 40;
const int MIN_WND_SIZE = 5;

typedef struct vergence {
  int master_group_id,
    slave_group_id,
    PX,
    PY,
    step,
    image_number,
    window_size,
    wnd_size_group_id,
    XObj_group_id, YObj_group_id,
    pos_master_group_id, pos_slave_group_id;
  float a, b;
  enum work_mode mode;
  unsigned char
    *master_window_nb,
    *slave_band_nb,
    *master_window_grad,
    *slave_band_grad,
    *master_window_orient,
    *slave_band_orient;
  unsigned int *first_four_layers;
  unsigned int image_sx;
  FILE* file;
} type_vergence;

void new_vergence(int gpe)
{
  int link, nb_links = 0;
  char param[255];
  int param_int;

  type_vergence *my_data;
  my_data = ALLOCATION(type_vergence);
  def_groupe[gpe].data = my_data;
  my_data->master_group_id = my_data->slave_group_id = -1;
  my_data->mode = nongraph;
  my_data->PX = 0;
  my_data->PY = 0;
  my_data->image_number = 1;
  my_data->window_size = 20;
  my_data->wnd_size_group_id = -1;
  my_data->step = 0;

  my_data->XObj_group_id = my_data->YObj_group_id = -1;
  my_data->pos_slave_group_id = my_data->pos_master_group_id = -1;

/* Recuperation des parameters */
  for (link = find_input_link(gpe, 0); link != -1; link = find_input_link(gpe, nb_links))
  {
    if (prom_getopt(liaison[link].nom, "-master", NULL) == 1)
      my_data->master_group_id = liaison[link].depart;
    if (prom_getopt(liaison[link].nom, "-slave", NULL) == 1)
      my_data->slave_group_id = liaison[link].depart;
    if(prom_getopt(liaison[link].nom, "-M", param) == 2)
      if (strcmp(param, "graph") == 0)
        my_data->mode = graph;

    if(prom_getopt_int(liaison[link].nom, "-w", &param_int) == 2)
      my_data->window_size = param_int;
    if(prom_getopt(liaison[link].nom, "-W", NULL) == 1)
      my_data->wnd_size_group_id = liaison[link].depart;

    if (prom_getopt(liaison[link].nom, "-posSlave", param) == 1)
      my_data->pos_slave_group_id = liaison[link].depart;
    else
      if (prom_getopt(liaison[link].nom, "-posMaster", param) == 1)
        my_data->pos_master_group_id = liaison[link].depart;

    if (prom_getopt(liaison[link].nom, "-XObj", NULL) == 1)
      my_data->XObj_group_id = liaison[link].depart;
    if (prom_getopt(liaison[link].nom, "-YObj", NULL) == 1)
      my_data->YObj_group_id = liaison[link].depart;

    if(prom_getopt_int(liaison[link].nom, "-PX", &param_int) == 2)
      my_data->PX = param_int;
    if(prom_getopt_int(liaison[link].nom, "-PY", &param_int) == 2)
      my_data->PY = param_int;

    if(prom_getopt_int(liaison[link].nom, "-I", &param_int) == 2)
      my_data->image_number = param_int;
    nb_links++;
  };
  if (my_data->master_group_id == -1 || my_data->slave_group_id == -1)
    EXIT_ON_ERROR("f_vergence: there is not enough links: master and slave is needed");
  if (my_data->pos_master_group_id == -1 || my_data->pos_slave_group_id == -1)
    EXIT_ON_ERROR("f_vergence: there is not enough links: pos of master and slave motors are needed");
/**/

  my_data->image_sx = ((prom_images_struct *) def_groupe[my_data->slave_group_id].ext)->sx;

/*Allocation d'une memoire*/
  my_data->master_window_nb = MANY_ALLOCATIONS(MAX_WND_SIZE * MAX_WND_SIZE, unsigned char);
  my_data->master_window_grad = MANY_ALLOCATIONS(MAX_WND_SIZE * MAX_WND_SIZE, unsigned char);
  my_data->master_window_orient = MANY_ALLOCATIONS(MAX_WND_SIZE * MAX_WND_SIZE, unsigned char);
  my_data->slave_band_nb = MANY_ALLOCATIONS(my_data->image_sx * MAX_WND_SIZE, unsigned char);
  my_data->slave_band_grad = MANY_ALLOCATIONS(my_data->image_sx * MAX_WND_SIZE, unsigned char);
  my_data->slave_band_orient = MANY_ALLOCATIONS(my_data->image_sx * MAX_WND_SIZE, unsigned char);

  my_data->first_four_layers = MANY_ALLOCATIONS(my_data->image_sx - MIN_WND_SIZE + 1, unsigned int);
/**/

/*Ouvrir le fichier
  my_data->file = fopen("6px.dat", "w");
*/
}

unsigned char getNBofPixel(unsigned char* image, int number)
{
  return (unsigned char) ((int) (0.299 * image[3 * number] + 0.587 * image[3 * number + 1] + 0.114 * image[3 * number + 2]));
}


float F0 [3][3] = {{ 0.000000, -0.000013, 0.002572},
                   { 0.000008,  0.000006, 0.032911},
                   {-0.001352, -0.033966, 1.000000}};

float F1 [3][3] = {{ 0.000001,  0.000004, -0.002027},
                   {-0.000009,  0.000000,  0.011490},
                   { 0.002466, -0.012798,  1.000000}};

/*
float F0 [3][3] = {{ 0.000000,  0.000008, -0.001352},
                   {-0.000013,  0.000006, -0.033966},
                   { 0.002572,  0.032911, 1.000000}};

float F1 [3][3] = {{ 0.000001, -0.000009,  0.002466},
                   { 0.000004,  0.000000, -0.012798},
                   {-0.002027,  0.011490,  1.000000}};
*/

void get_epiline(float m, float mm, int x, int y, float* a, float* b, float* c)
{
  //printf("m=%f -- mm=%f\n", m, mm);
  m = (mm - m)*2;
  *a = x * (m * (F1[0][0] - F0[0][0]) + F0[0][0]) + y * (m * (F1[1][0] - F0[1][0]) + F0[1][0]) + (m * (F1[2][0] - F0[2][0]) + F0[2][0]);
  *b = x * (m * (F1[0][1] - F0[0][1]) + F0[0][1]) + y * (m * (F1[1][1] - F0[1][1]) + F0[1][1]) + (m * (F1[2][1] - F0[2][1]) + F0[2][1]);
  *c = x * (m * (F1[0][2] - F0[0][2]) + F0[0][2]) + y * (m * (F1[1][2] - F0[1][2]) + F0[1][2]) + (m * (F1[2][2] - F0[2][2]) + F0[2][2]);
}

void function_vergence(int gpe)
{
  type_vergence *my_data;
  int deb;
  int Gx, Gy;
  int imin = 1, k;
  int x, y;
  float a, b, c;


  float mm, ms;
  int i, j, x_obj, y_obj;
  prom_images_struct *master_image, *slave_image;

  my_data = def_groupe[gpe].data;

/*Recuperation des images originales*/
  master_image = (prom_images_struct *) def_groupe[my_data->master_group_id].ext;
  slave_image = (prom_images_struct *) def_groupe[my_data->slave_group_id].ext;
  if (master_image == NULL || slave_image == NULL)
  {
    EXIT_ON_ERROR("f_vergence: There are no images to process (ext == NULL)\n");
    return;
  }
/**/

/*Recuperation de la taille de la fenetre*/
  if (my_data->wnd_size_group_id != -1)
    my_data->window_size = neurone[def_groupe[my_data->wnd_size_group_id].premier_ele].s;
/**/

/*Recuperation de la point d'interet*/
  x_obj = round(neurone[def_groupe[my_data->XObj_group_id].premier_ele].s);
  y_obj = round(neurone[def_groupe[my_data->YObj_group_id].premier_ele].s);
/**/

/*Recuperation de la position du moteur*/
  ms = neurone[def_groupe[my_data->pos_slave_group_id].premier_ele].s;
  mm = neurone[def_groupe[my_data->pos_master_group_id].premier_ele].s;
/**/

/*Ligne epipolaire*/
  get_epiline(ms, mm, x_obj, y_obj, &a, &b, &c);

/**/


/*Calculation des entrees*/
  for (i = 0; i < my_data->window_size; i++)
  {
    x = i + y_obj - my_data->window_size / 2;
    for (j = 0; j < my_data->window_size; j++)
    {
      y = j + x_obj - my_data->window_size / 2;
      my_data->master_window_nb[i * my_data->window_size + j] = getNBofPixel(master_image->images_table[0], x * master_image->sx + y);

      Gx = - getNBofPixel(master_image->images_table[0], (x - 1) * master_image->sx + y - 1)
           - 2 * getNBofPixel(master_image->images_table[0], (x) * master_image->sx + y - 1)
           - getNBofPixel(master_image->images_table[0], (x + 1) * master_image->sx + y - 1)
           + getNBofPixel(master_image->images_table[0], (x - 1) * master_image->sx + y + 1)
           + 2 * getNBofPixel(master_image->images_table[0], (x) * master_image->sx + y + 1)
           + getNBofPixel(master_image->images_table[0], (x + 1) * master_image->sx + y + 1);

      Gy = - getNBofPixel(master_image->images_table[0], (x - 1) * master_image->sx + y - 1)
           - 2 * getNBofPixel(master_image->images_table[0], (x - 1) * master_image->sx + y)
           - getNBofPixel(master_image->images_table[0], (x - 1) * master_image->sx + y + 1)
           + getNBofPixel(master_image->images_table[0], (x + 1) * master_image->sx + y - 1)
           + 2 * getNBofPixel(master_image->images_table[0], (x + 1) * master_image->sx + y)
           + getNBofPixel(master_image->images_table[0], (x + 1) * master_image->sx + y + 1);

      my_data->master_window_grad[i * my_data->window_size + j] = sqrt(Gx * Gx + Gy * Gy);
      my_data->master_window_orient[i * my_data->window_size + j] = (unsigned char) (256. / 1.57079 * atan(abs(Gy / (double)Gx)));
    }
    for (j = 0; j < (int)slave_image->sx; j++)
    {
      y = i - (a * j + c) / b - my_data->window_size / 2;
      if (y < (int)slave_image->sy)
      {
        Gx = - getNBofPixel(slave_image->images_table[0], (y - 1) * slave_image->sx + j - 1)
             - 2 * getNBofPixel(slave_image->images_table[0], (y) * slave_image->sx + j - 1)
             - getNBofPixel(slave_image->images_table[0], (y + 1) * slave_image->sx + j - 1)
             + getNBofPixel(slave_image->images_table[0], (y - 1) * slave_image->sx + j + 1)
             + 2 * getNBofPixel(slave_image->images_table[0], (y) * slave_image->sx + j + 1)
             + getNBofPixel(slave_image->images_table[0], (y + 1) * slave_image->sx + j + 1);

        Gy = - getNBofPixel(slave_image->images_table[0], (y - 1) * slave_image->sx + j - 1)
             - 2 * getNBofPixel(slave_image->images_table[0], (y - 1) * slave_image->sx + j)
             - getNBofPixel(slave_image->images_table[0], (y - 1) * slave_image->sx + j + 1)
             + getNBofPixel(slave_image->images_table[0], (y + 1) * slave_image->sx + j - 1)
             + 2 * getNBofPixel(slave_image->images_table[0], (y + 1) * slave_image->sx + j)
             + getNBofPixel(slave_image->images_table[0], (y + 1) * slave_image->sx + j + 1);

        my_data->slave_band_nb[i * slave_image->sx + j] = getNBofPixel(slave_image->images_table[0], y * slave_image->sx + j);
        my_data->slave_band_grad[i * slave_image->sx + j] = sqrt(Gx * Gx + Gy * Gy);
        my_data->slave_band_orient[i * slave_image->sx + j] = (unsigned char) (256. / 1.57079 * atan(abs(Gy / (double)Gx)));
      }
      else
      {
        my_data->slave_band_nb[i * slave_image->sx + j] = 0;
        my_data->slave_band_grad[i * slave_image->sx + j] = 0;
        my_data->slave_band_orient[i * slave_image->sx + j] = 0;
      }
    }
    my_data->slave_band_nb[i * slave_image->sx] = my_data->slave_band_nb[i * slave_image->sx + slave_image->sx - 1] = 0;
    my_data->slave_band_grad[i * slave_image->sx] = my_data->slave_band_grad[i * slave_image->sx + slave_image->sx - 1] = 0;
    my_data->slave_band_orient[i * slave_image->sx] = my_data->slave_band_orient[i * slave_image->sx + slave_image->sx - 1] = 0;
  }
/**/

  for(k = 1; k < (int)slave_image->sx - my_data->window_size; k++)
  {
    my_data->first_four_layers[k] = 0;

    for (i = 0; i < my_data->window_size; i++)
    {
      for (j = 0; j < my_data->window_size; j++)
      {
        my_data->first_four_layers[k] +=
            abs(my_data->master_window_nb[i*my_data->window_size + j] - my_data->slave_band_nb [i*slave_image->sx + j + k]) +
            abs(my_data->master_window_grad[i*my_data->window_size + j] - my_data->slave_band_grad [i*slave_image->sx + j + k]) +
            abs(my_data->master_window_orient[i*my_data->window_size + j] - my_data->slave_band_orient [i*slave_image->sx + j + k]);
      }
    }
    if (my_data->first_four_layers[k] <= my_data->first_four_layers[imin])
      imin = k;
  }


  deb = def_groupe[gpe].premier_ele;

/*Produire une sortie*/
  neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = (double)imin;//(double)slave_image->sx / 2. - (double)imin;

/*Sauvgarder data
  if (my_data->step < 100)
  {
    fprintf(my_data->file, "%d\t%d\r\n", my_data->step, (int)(neurone[deb].s + 0.01));
    my_data->step++;
  }
*/

#ifndef AVEUGLE
  if (my_data->mode == graph)
  {
    TxDonneesFenetre *fenetre;
    TxPoint point;
    int y0, yend;
    TxPoint point_dest;


    if (my_data->image_number == 2)
      fenetre = &image2;
    else
      fenetre = &image1;

    point.x = my_data->PX;
    point.y = my_data->PY + master_image->sy + 10;
    TxDessinerRectangle(fenetre, blanc, TxPlein, point, slave_image->sx + MAX_WND_SIZE + 10, 3*(MAX_WND_SIZE + 10), 1);

    TxAfficheImageNB(fenetre, my_data->master_window_nb, my_data->window_size, my_data->window_size, my_data->PX + slave_image->sx + 10, my_data->PY + master_image->sy + 10);
    TxAfficheImageNB(fenetre, my_data->master_window_grad, my_data->window_size, my_data->window_size, my_data->PX + slave_image->sx + 10, my_data->PY + master_image->sy + 10 + my_data->window_size + 10);
    TxAfficheImageNB(fenetre, my_data->master_window_orient, my_data->window_size, my_data->window_size, my_data->PX + slave_image->sx + 10, my_data->PY + master_image->sy + 10 + 2 * (my_data->window_size + 10));

    TxAfficheImageNB(fenetre, my_data->slave_band_nb, slave_image->sx, my_data->window_size, my_data->PX, my_data->PY + master_image->sy + 10);
    TxAfficheImageNB(fenetre, my_data->slave_band_grad, slave_image->sx, my_data->window_size, my_data->PX, my_data->PY + master_image->sy + 10 + my_data->window_size + 10);
    TxAfficheImageNB(fenetre, my_data->slave_band_orient, slave_image->sx, my_data->window_size, my_data->PX, my_data->PY + master_image->sy + 10 + 2 * (my_data->window_size + 10));

    point.x = my_data->PX + imin;
    point.y = my_data->PY + master_image->sy + 10;
    TxDessinerRectangle(fenetre, rouge, TxVide, point, my_data->window_size, my_data->window_size - 1, 1);

    point.x = slave_image->sx + x_obj - my_data->window_size / 2;
    point.y = y_obj - my_data->window_size / 2;
    TxDessinerRectangle(fenetre, blanc, TxVide, point, my_data->window_size, my_data->window_size, 1);

    y0 = -c/b;
    yend = (-a * slave_image->sx - c)/b;

    point.x = 0;
    point.y = y0 - my_data->window_size / 2;
    point_dest.x = my_data->PX + slave_image->sx;
    point_dest.y = my_data->PY + yend - my_data->window_size / 2;
    TxDessinerFleche(fenetre, blanc, point, point_dest, 1);

    point.y = my_data->PY + y0 + my_data->window_size / 2;
    point_dest.y = my_data->PY + yend + my_data->window_size / 2;
    TxDessinerFleche(fenetre, blanc, point, point_dest, 1);

    TxFlush(fenetre);
  }
#endif
}

void destroy_vergence(int gpe)
{
  type_vergence *my_data;
  my_data = def_groupe[gpe].data;
//fclose(my_data->file);
  free(my_data->first_four_layers);
  free(my_data->master_window_nb);
  free(my_data->master_window_grad);
  free(my_data->master_window_orient);
  free(my_data->slave_band_nb);
  free(my_data->slave_band_grad);
  free(my_data->slave_band_orient);
  free(def_groupe[gpe].data);
}
