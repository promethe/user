/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
   Cette fonction prend une image en entree et realise un vecteur place dans un reseau de neurones.
   Ce vecteur correspond a la projection de la somme des valeurs des pixels pour chaque colonne.
   Elle est utilise afin de localiser un oeil dans une image representant un visage apres un traitement "eye-analog-segment"

   Modif Ali K. 28/10/2013 : La fonction calcul aussi un histogramme sur un gpe de neurones aussi si l'option -gpe est ajoutee sur le lien.
    Utilisee pour calculer l'histogramme sur la sortie du Rho-theta.
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/projection.h>
#include <Struct/prom_images_struct.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define EXT 0
#define GPE 1

typedef struct
{
	int gpe_in;
	int hist_on;
	int gpe_out;
} Data_hist;

float histo_bw(int first_id, int nbre, prom_images_struct* im )
{
	float ratio = 255.0/(float)(nbre-1);  /* correction bug: nbre-1 a la place de nombre sinon on peut depasser */
	int i,n,k;
	float max=-1.;
	float *pt;
	unsigned char *l_im;

	n = im->sx * im->sy;
	l_im=im->images_table[0];

	for (i = n-1; i--; )
	{
		k = (int)(l_im[i]);
		pt=&neurone[first_id + (int)((float)k /ratio)].s;
		(*pt)++;
		if (*pt>max) max=*pt;
	}
	return max;
}

/* conversion couleur vers luminance */
float histo_col(int first_id, int nbre, prom_images_struct* im )
{
	float ratio = 3*255.0/(float)(nbre-1);  /* correction bug: nbre-1 a la place de nombre sinon on peut depasser */
	int i,n,k;
	int p;
	float max=-1.;
	float *pt;
	unsigned char *l_im,*l_im2;

	printf("histo image couleur vers luminance\n");
	n = im->sx * im->sy;
	l_im=im->images_table[0];

	for (i = 0; i < n; i++)
	{
		p=3*i;
		l_im2=&l_im[p];
		k = ((int)(*l_im2) + (int)(*(l_im2 + 1)) + (int)(*(l_im2+ 2)));
		pt=&neurone[first_id + (int)((float)k /ratio)].s;
		(*pt)++;
		if (*pt>max) max=*pt;
	}
	return max;
}

/* on traite les 3 plans r,g,b ou autre */
/*erreur tout sur la 2eme ligne ???? */
float histo_col2(int first_id, int nbre_col, char sens, prom_images_struct* im )
{
	float ratio = 255.0/(float)(nbre_col-1);
	int i,n,k2;
	float k;
	int c,c2;
	int pas,pas2;
	unsigned char *m_im;
	float max=-1.;

	if(sens=='h') { pas=1; pas2=nbre_col;} else {pas=3;pas2=1;}
	dprints("histogramme couleur pas1=%d, pas2=%d nbre_col=%d \n",pas,pas2, nbre_col);

	n = im->sx * im->sy;
	m_im=im->images_table[0];

	for (c=0; c<3; c++)
	{
		c2=c*pas2;
		for (i = n-1 ; i--; )
		{
			k = (float) m_im[3*i+c] ;
			k2=first_id+ pas*(int)(k /ratio) + c2;
			neurone[ k2 ].s = neurone[ k2 ].s + 1.;
			if(neurone[ k2 ].s>max) max=neurone[ k2 ].s;
		}
	}
	return max;
}

float histo_float(int first_id, int nbre, prom_images_struct* im )
{
	float ratio = 255.0/(float)(nbre-1);
	int i,n,k2;
	float k;
	float * imf;
	float max=-1.;

	n = im->sx * im->sy;
	imf = (float *)(im->images_table[0]);

	for (i = 0; i < n; i++)
	{
		k =  (imf[i] * 255.);
		k2=first_id + (int)(k /ratio);
		neurone[k2].s++;
		if(neurone[k2].s>max) max=neurone[k2].s;
	}
	return max;
}

void histogramme_ext(int gpe_input, int Gpe)
{
	prom_images_struct *img;
	int i,first_n,nbre;
	/* image pointeur sur l'image du groupe precedent */
	img = ((prom_images_struct *) def_groupe[gpe_input].ext);

	/* recuperation des infos sur la taille */
	first_n = def_groupe[Gpe].premier_ele;
	nbre=def_groupe[Gpe].nbre;

	dprints("HISTO :%s : %d \n", def_groupe[Gpe].no_name, img->nb_band);

	for( i=0; i < nbre; i++) neurone[first_n+i].s=0;

	switch(img->nb_band)
	{
	case 1 :
		histo_bw(first_n, nbre,  img );
		break;
	case 3 :
		histo_col(first_n, nbre,  img );
		break;
	case 4 :
		histo_float(first_n, nbre,  img );
		break;
	}
}

void histogramme_gpe(int gpe_in,int Gpe)
{
	int i,n,k,nbre,first_in,first_out;
	nbre = def_groupe[Gpe].nbre;
	n = def_groupe[gpe_in].nbre;
	first_out  = def_groupe[Gpe].premier_ele;
	first_in = def_groupe[gpe_in].premier_ele;

	for (i = 0; i < n; i++)
	{
		k = (int) (neurone[first_in + i].s1 * nbre) ;
		neurone[first_out + k].s++;
	}
}

void new_histogramme(int Gpe)
{
	int link = -1;
	int i = 0 ;
	char param_link[256];
	int hist_on = 0, gpe_in = -1;
	Data_hist * my_data;

	if (def_groupe[Gpe].data == NULL)
	{
		my_data = (Data_hist *) malloc(sizeof(Data_hist));
		link = find_input_link(Gpe, 0);
		while (link!=-1)
		{
			if(strcmp(liaison[link].nom, "sync") == 0) {/* nothing to do */}
			else
			{
				if (prom_getopt(liaison[link].nom, "-gpe", param_link) == 1)
				{
					hist_on = GPE;
					gpe_in = liaison[link].depart;
					dprints("lien en entree trouve : GPE\n");
				}
				else
				{
					hist_on = EXT;
					gpe_in = liaison[link].depart;
					dprints("lien en entree trouve : EXT gpe_in = %d\n",gpe_in);
				}
			}
			i++;
			link = find_input_link(Gpe, i);

		}
		if (gpe_in == -1)
			EXIT_ON_ERROR("*****> Il n'y a pas de groupe en entree du Groupe %s\n", def_groupe[Gpe].no_name);
		my_data->gpe_in = gpe_in;
		my_data->hist_on = hist_on;
		my_data->gpe_out = Gpe;
		def_groupe[Gpe].data = my_data;
	}

}
void function_histogramme(int Gpe)
{
	prom_images_struct *img;
	int i = 0 ;
	int n;
	int first_n;
	int first_out;
	int nbre;
	float max;
	type_neurone *pt;
	int couleur=1;            /* par defaut on est en niveau de gris */
	int c;
	int taille;
	char sens='.';
	int pas,pas2;
	int gpe_in;
	Data_hist * my_data;


	my_data = (Data_hist *) def_groupe[Gpe].data;
	if (my_data == NULL)
		EXIT_ON_ERROR("ERROR in f_histogramme(%s): Cannot retrieve data\n", def_groupe[Gpe].no_name);
	gpe_in = my_data->gpe_in;

	switch (my_data->hist_on)
	{
	case EXT :
		/* image pointeur sur l'image du groupe precedent */
		img = ((prom_images_struct *) def_groupe[gpe_in].ext);

		/* recuperation des infos sur la taille */
		n = img->sx* img->sy;
		first_n = def_groupe[Gpe].premier_ele;
		nbre=def_groupe[Gpe].nbre;

		taille=def_groupe[Gpe].taillex;
		if(def_groupe[Gpe].tailley>taille) taille=def_groupe[Gpe].tailley; /* traitement du cas vecteur horizontal ou vertical */


		if(def_groupe[Gpe].taillex>1 && def_groupe[Gpe].tailley>1)
		{
			if(def_groupe[Gpe].taillex==3) {couleur=3; sens='v'; taille=def_groupe[Gpe].tailley;}
			else if(def_groupe[Gpe].tailley==3) {couleur=3; sens='h'; taille=def_groupe[Gpe].taillex;}
		}

		dprints("HISTO gpe %s : nb_band=%d , rn couleur=%d, nbre=%d\n", def_groupe[Gpe].no_name, img->nb_band, couleur,nbre);

		max=(float)n;
		for ( i=nbre-1; i--; ) neurone[first_n+i].s=0.;

		switch (img->nb_band)
		{
		case 1 :
			max=histo_bw(first_n, nbre,  img );
			break;
		case 3 :
			if (couleur==3) max=histo_col2(first_n, taille, sens, img );
			else  max=histo_col(first_n, nbre,  img );
			break;
		case 4 :
			max=histo_float(first_n, nbre,  img );
			break;
		}

		if (max<1.) max=1.;

		pas=1;pas2=0;
		if(sens=='h') { pas=1; pas2=taille;}
		else if(sens=='v') {pas=3;pas2=1;}

		//printf("couleur = %d , taille = %d pas=%d pas2=%d\n",couleur,taille,pas,pas2);
		for (c=0; c<couleur; c++)
		{
			for (i=0; i<taille; i++)
			{
				pt=& (neurone[first_n+i*pas+c*pas2]);
				(*pt).s1 = (*pt).s2 = (*pt).s= (*pt).s/max ;
			}
		}
		break;
		case GPE :
			n =  def_groupe[gpe_in].nbre;
			histogramme_gpe(gpe_in,Gpe);
			/*projection de l'histogramme verticale dans un reseau de neurones */
			nbre = def_groupe[Gpe].nbre;
			first_out = def_groupe[Gpe].premier_ele;

			for (i = 0; i < nbre; i++)
			{
				neurone[first_out+i].s1 = neurone[first_out+i].s2 = neurone[first_out+i].s= neurone[first_out+i].s/n ;
			}
			break;
		default :
			EXIT_ON_ERROR("Le calcul se fait sur des ext ou des gpe de neurones uniquement!! in %s",__FUNCTION__);
			break;

	}

}

void destroy_histogramme(int Gpe)
{
	dprints("destroy %s(%s): \n", __FUNCTION__,def_groupe[gpe].no_name);
	if (def_groupe[Gpe].data != NULL)
	{
		free(((Data_hist *) def_groupe[Gpe].data));
		def_groupe[Gpe].data = NULL;
	}
}
