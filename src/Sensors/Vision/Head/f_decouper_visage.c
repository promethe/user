/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_decouper_visage.c
\brief 

Author: AM.Tousch
Created: 02/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction permettant le decoupage du visage au milieu, en precisant seulement si on veut la moitie superieure (-sup) ou inferieure (-inf).

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()


Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
Fonction adaptee a partir de f_decoupe_visage_sup_tracking. Mais il reste encore quelques lignes a revoir (pour un code plus propre !)...

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/  
#include <libx.h>
#include <stdlib.h>
    
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
    
#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>
    
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
    
    
#define SUP 0
#define INF 1
    
#define LARGEUR_MAX 200
#define HAUTEUR_MAX 200
    
#undef DEBUG
typedef struct 
{
    int d_moitie;
     int height;               /* image coupee */
     int width;
     int largeur;              /* image originale */
     int hauteur;
     int Nxy;
     int groupe_entree_image;
 } decoupe_simple;
void new_decouper_visage(int gpe) 
{
    	int link = -1;
    
  /*  int Debut_Gpe; */ 
  int TailleGroupeX, TailleGroupeY;
    	int Nhv;
    	int n, moitie;
	prom_images_struct * p_result = NULL;
    	decoupe_simple * decoupe;
    
  /* initialisation locale */ 
  /*  Debut_Gpe = def_groupe[gpe].premier_ele; */ 
	TailleGroupeX = def_groupe[gpe].taillex;
    	TailleGroupeY = def_groupe[gpe].tailley;
    	Nhv = TailleGroupeX * TailleGroupeY;
    
#ifdef DEBUG
        printf("===> fonction new_decouper_visage\n");
    
#endif 
  /* Allocation de la memoire pour sauvegarde des donnees dans la structure decoupe_simple */ 
  decoupe = (decoupe_simple *) malloc(sizeof(decoupe_simple));
  def_groupe[gpe].data = (decoupe_simple *) decoupe;
  /* anciennement     def_groupe[gpe].data = (decoupage*)temp; */ 
  /*initialisation */ 
  decoupe->height = HAUTEUR_MAX / 2;
    	decoupe->width = LARGEUR_MAX;
    	decoupe->groupe_entree_image = -1;
    	decoupe->largeur = LARGEUR_MAX;
    	decoupe->hauteur = HAUTEUR_MAX;
    	decoupe->Nxy = LARGEUR_MAX * HAUTEUR_MAX;
    	decoupe->d_moitie = -1;
    	/* Recuperation des donnees sur les liens */ 
  n = -1;
  do
  {
    n++;
    link = find_input_link(gpe, n);
    if (link != -1)
    {
      if (liaison[link].nom[0] == '-')
      {
        if (liaison[link].nom[1] == 'i')
        {
          /* numero du groupe en entree */ 
          decoupe->groupe_entree_image = liaison[link].depart;
          /* choix de la decoupe superieure ou inferieur */ 
          if (strstr(liaison[link].nom, "-sup") != NULL) 						decoupe->d_moitie = SUP;
          else if (strstr(liaison[link].nom, "-inf") != NULL)
            decoupe->d_moitie = INF;
          else
          {
             printf("erreur : f_decouper_visage groupe %d : moitie superieure (-sup) ou inferieure (-inf) ?\n", gpe);
             exit(EXIT_FAILURE);
          }
					moitie = decoupe->d_moitie;
                    #ifdef DEBUG
          printf("f_decouper_visage *****> gpe_entree image = %d   link=%d\n", decoupe->groupe_entree_image, link);
                    
#endif 
        }
        else
          printf(" lien %s non reconnu (f_decouper_visage dans groupe %d)\n", liaison[link].nom, gpe);
            	  }
      else
         printf(" link %s not recognized (f_decouper_visage) \n", liaison[link].nom);
	}
  }
  while (link != -1);

#ifdef DEBUG      
   printf("f_decouper_visage : premiere alloc ext de prom_images_struct \n");
#endif 
   p_result = (void *) malloc(sizeof(prom_images_struct));;
   if (p_result == NULL)
   {
     printf("ALLOCATION IMPOSSIBLE DANS DECOUPER IMAGE...! \n");
     exit(EXIT_FAILURE);
   }
   p_result->image_number = 1;
   p_result->sx = LARGEUR_MAX;
   p_result->sy = HAUTEUR_MAX / 2;
   p_result->nb_band = 1;
    
        /* sauvegarde de l'image en sortie du groupe */ 
        def_groupe[gpe].ext = p_result;
    
        /* allocation memoire pour l'imagette */ 
        p_result->images_table[0] =
        (unsigned char *) malloc((LARGEUR_MAX * HAUTEUR_MAX / 2) *
                                 sizeof(unsigned char));
} 

/*** fin new_decouper_visage ***/ 
void function_decouper_visage(int gpe) 
{
    int i = -1, j;
    int Debut_Gpe;
    int TailleGroupeX, TailleGroupeY;
    int k, n, Nhv, i1, p, x1, y1;
    int t;
    int ht, lg;
    float echx, echy;
    int moitie;
    int a, b, numpixels, num;
    prom_images_struct * image = NULL;
    
/*  prom_images_struct * p_result = NULL;*/ 
        decoupe_simple * decoupe;
    int debut_zone_isole_y, limit_zone_isole_y;
    int debut_zone_isole_x, limit_zone_isole_x;
    
#ifndef AVEUGLE
        TxPoint point_depart, point_arrivee;
    
#endif /*  */
        
        /* Mesure du temps de traitement dans la fonction */ 
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
    
#endif /*  */
        
        /* Lancement du timer */ 
#ifdef TIME_TRACE
        gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
    
#endif /*  */
        
        /* initialisation locale */ 
        Debut_Gpe = def_groupe[gpe].premier_ele;
    TailleGroupeX = def_groupe[gpe].taillex;
    TailleGroupeY = def_groupe[gpe].tailley;
    Nhv = TailleGroupeX * TailleGroupeY;
    
        /* Recuperation des donnees (.data et .ext) */ 
        decoupe = (decoupe_simple *) def_groupe[gpe].data;
    moitie = decoupe->d_moitie;
    
        /* recuperation de l'image du groupe en entree */ 
        image =
        (prom_images_struct *) def_groupe[decoupe->groupe_entree_image].ext;
    if (image == NULL)
        
    {
        printf("Probleme dans f_decouper_visage : pas d'image dans le groupe %d\n", decoupe->groupe_entree_image);
  	exit(EXIT_FAILURE);
    }
    
    else if (image->nb_band != 1)
        
    {
        printf("\n l'image en entree de f_decouper_visage doit etre en noir et blanc !\n");
        exit(EXIT_FAILURE);
    }

  /* mise a jour des donnees de la structure de decoupe */ 
  lg = image->sx;
    ht = image->sy;
    
/*   decoupe->largeur = image->sx; */ 
/*   decoupe->hauteur = image->sy; */ 
/*   decoupe->Nxy = decoupe->largeur*decoupe->hauteur; */ 
        if (lg > LARGEUR_MAX)
        
    {
        printf("WARNING : image trop large en entree du groupe %d", gpe);
        exit(EXIT_FAILURE);
    }
    if (ht > HAUTEUR_MAX)
        
    {
        printf("WARNING : image trop haute en entree du groupe %d", gpe);
        exit(EXIT_FAILURE);
    }
    decoupe->largeur = lg;
    decoupe->hauteur = ht;
    decoupe->Nxy = decoupe->largeur * decoupe->hauteur;
    
        /* pour decouper simplement en 2 */ 
        decoupe->height = decoupe->hauteur / 2;
    decoupe->width = decoupe->largeur;
    a = decoupe->largeur / 2;
    if (moitie == SUP)
        b = decoupe->hauteur / 2 - decoupe->height / 2;
    
    else if (moitie == INF)
        b = decoupe->hauteur / 2 + decoupe->height / 2;
    
    else
        
    {
        printf
            ("probleme dans f_decouper_visage (groupe %d) : moitie non precisee\n",
             gpe);
        exit(EXIT_FAILURE);
    }
    num = 0;
    
  /************************** decoupage *****************************/ 
        /*Limite du decoupage en y */ 
        debut_zone_isole_y = b - floor(decoupe->height / 2);
    limit_zone_isole_y = b + floor(decoupe->height / 2);
    
        /*Limite du decoupage en x */ 
        debut_zone_isole_x = a - floor(decoupe->width / 2);
    limit_zone_isole_x = a + floor(decoupe->width / 2);
    
#ifdef DEBUG 
        printf("Function f_decouper_tracking, Gpe : %d\n", gpe);
    printf("Valeur de a :%d , valeur de b :%d\n", a, b);
    printf("Valeur de debut_zone_isole_x %d et limit_zone_isole_x %d\n",
            debut_zone_isole_x, limit_zone_isole_x);
    printf("Valeur de debut_zone_isole_y %d et limit_zone_isole_y %d\n",
            debut_zone_isole_y, limit_zone_isole_y);
    printf("Valeur de largeur %d et hauteur %d\n", decoupe->largeur,
            decoupe->hauteur);
    printf("Valeur de width %d et height %d\n", decoupe->width,
            decoupe->height);
    
#endif /*  */
        
        /*Recadrage en x */ 
        if (limit_zone_isole_x > image->sx)
        
    {
        t = limit_zone_isole_x - image->sx;
        limit_zone_isole_x = image->sx;
        debut_zone_isole_x = debut_zone_isole_x - t;
        
            /* p_result->sy =  p_result->sy - t; */ 
            if (debut_zone_isole_x <= 0)
            debut_zone_isole_x = 0;
        
#ifdef DEBUG
            printf("Correction en x\n");
        printf("Valeur de t %d\n", t);
        
/*       printf("Valeur de p_result->sy %d\n",p_result->sy); */ 
#endif /*  */
    }
    
        /*Recadrage en x */ 
        if (debut_zone_isole_x < 0)
        
    {
        t = debut_zone_isole_x;
        limit_zone_isole_x = limit_zone_isole_x - t;
        debut_zone_isole_x = 0;
        
            /* p_result->sy =  p_result->sy - t; */ 
            
#ifdef DEBUG
            printf("Correction en x\n");
        printf("Valeur de t %d\n", t);
        
/*       printf("Valeur de p_result->sy %d\n",p_result->sy); */ 
#endif /*  */
    }
    
        /*Recadrage en y */ 
        if (limit_zone_isole_y > image->sy)
        
    {
        t = limit_zone_isole_y - image->sy;
        limit_zone_isole_y = image->sy;
        debut_zone_isole_y = debut_zone_isole_y - t;
        
            /* p_result->sy =  p_result->sy - t; */ 
            if (debut_zone_isole_y <= 0)
            debut_zone_isole_y = 0;
        
#ifdef DEBUG
            printf("Correction en y\n");
        printf("Valeur de t %d\n", t);
        
/*       printf("Valeur de p_result->sy %d\n",p_result->sy); */ 
#endif /*  */
    }
    
        /*Recadrage en y */ 
        if (debut_zone_isole_y < 0)
        
    {
        t = debut_zone_isole_y;
        limit_zone_isole_y = limit_zone_isole_y - debut_zone_isole_y;
        debut_zone_isole_y = 0;
        
            /* p_result->sy =  p_result->sy - t; */ 
            
#ifdef DEBUG
            printf("Correction en y\n");
        printf("Valeur de t %d\n", t);
        printf("Valeur de p_result->sy %d\n", p_result->sy);
        
#endif /*  */
    }
    
#ifdef DEBUG  
        printf("Apres correction\n");
    printf("Valeur de debut_zone_isole_x %d et limit_zone_isole_x %d\n",
            debut_zone_isole_x, limit_zone_isole_x);
    printf("Valeur de debut_zone_isole_y %d et limit_zone_isole_y %d\n",
            debut_zone_isole_y, limit_zone_isole_y);
    printf("Valeur de largeur %d et hauteur %d\n", decoupe->largeur,
            decoupe->hauteur);
    printf("Valeur de image->sx %d\n", image->sx);
    printf("Valeur de image->sy %d\n", image->sy);
    
#endif /*  */
        num = 0;
    
  /************************************************************************************************/ 
        
        /* reset of outputs */ 
        for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
        
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
    }
    i = 0;
    j = 0;
    k = 0;
    n = 0;
    x1 = debut_zone_isole_x;
    y1 = debut_zone_isole_y;
    echx = (float) decoupe->width / (float) TailleGroupeX;
    echy = (float) decoupe->height / (float) TailleGroupeY;
    for (j = 0; j < TailleGroupeY; j++)
        
    {
        p = j * TailleGroupeX + Debut_Gpe;
        for (i = 0; i < TailleGroupeX; i++)
            
        {
            i1 = i + p;
            numpixels =
                (int) (x1 + i * echx) + (int) (y1 +
                                               j * echy) * decoupe->largeur;
            neurone[i1].s = neurone[i1].s1 = neurone[i1].s2 =
                (float) image->images_table[0][numpixels] / 255.;
    } } 
#ifdef DEBUG 
        printf("\n  k=%d, n=%d, n*XY=%d\n\n", k, n,
               (n + 1) * SizeZoneX * SizeZoneY);
    printf(" Valeur de k : %d\n", k);
    printf(" Valeur de n : %d et Nhv : %d\n", n, Nhv);
    
#endif /*  */
        
#ifndef AVEUGLE
        /*Affichage de la separation */ 
        if (moitie == SUP)
        
    {
        point_depart.x = debut_zone_isole_x;
        point_depart.y = limit_zone_isole_y;
        point_arrivee.x = limit_zone_isole_x;
        point_arrivee.y = limit_zone_isole_y;
        TxDessinerSegment(&image1, vert, point_depart, point_arrivee, 1);
        TxFlush(&image1);
    }
    
#endif /*  */
        
        /*Fin du Timer et affichage des temps */ 
#ifdef TIME_TRACE
        gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
             SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    
        /*   affiche_message(MessageFunctionTimeTrace); */ 
        printf("Chaine MessageFunctionTimeTrace %s\n",
               MessageFunctionTimeTrace);
    
#endif /*  */
}


