/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_circular_hough_transform_for_head.c
\brief

Author: xxxxxxxx P. Gaussier
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Fonction transformee de HOUGH circulaire pour la tete de robot
  Prend en entree une image de gradient (detection des contours)

  image_gradient   : image du gradient ou l'on veut chercher les cercles.
  attention si l'image du gradient n'est pas convenablement seuillee
  alors les resultat peuvent etre pas tres bon !!
  sur le lien de la boite qui va au gradient, passez quelque chose
  comme : -A1.0-Fc-S60 par exemple. Attention le seuil n'est pris
  en compte qu'avec l'option -Fc

  pour le trace des cercles sur l'image, attention, il y a un bord
  blanc de 10 pixels a prendre en compte.

  Attention cette fonction fait doublon (partiellement) avec f_circular_hough_transform_for_head().
  Voir f_circular_hough_transform_for_head() pour les optimisations de la transformee de Hough pour les cercles.


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #define DEBUG */

   /*!
      The structure for stroring parameter of hough transform is in hough.h
      typedef struct hough_struct_def
      {
      float seuil_max_locaux;
      unsigned int Haut;
      unsigned int Bas;
      unsigned int Gauche;
      unsigned int Droit;
      unsigned int largeur;
      unsigned int hauteur;
      unsigned int largeur_accu;
      unsigned int hauteur_accu;
      unsigned int zone_a_traiter;
      unsigned int PX;
      unsigned int PY;
      unsigned int calcul_ellipse;
      unsigned int f;
      unsigned int min_cercle;
      unsigned int max_cercle;
      unsigned int rayon_moyen;
      unsigned int numpixels;
      unsigned int image_windows_number;
      }hough_struct;
    */

void init_table_hough(int gpe_sortie);


void init_function_circular_hough_transform_for_head(int gpe_sortie)
{
   int i,link1 = -1;
   char resultat[256];         /* obligatoire pour prom_getopt */
   prom_images_struct *image_gradient = NULL;
      
   /* declaration faite dans hough.h */
   hough_struct * t_hough = NULL;
   float *accumulator_of_hough = NULL;

   /* allocation de la structure t_hough */

   t_hough = (hough_struct *) malloc(sizeof(hough_struct));
   if (t_hough == NULL)
   {
      printf("impossible d'allouer une hough_struct dans le groupe %d\n", gpe_sortie);
      exit(0);
   }
   def_groupe[gpe_sortie].data = (unsigned char *) t_hough;

   /* valeurs par defaut pour si le parametre est absent */
   t_hough->groupe_entree = -1;
   t_hough->Haut = 0;
   t_hough->Bas = 0;
   t_hough->Gauche = 0;
   t_hough->Droit = 0;
   t_hough->zone_a_traiter = 1;
   t_hough->PX = 0;
   t_hough->PY = 0;
   t_hough->calcul_ellipse = 0;
   t_hough->fx = 2;
   t_hough->fy = 2;
   t_hough->seuil_max_locaux = 0.75;
   t_hough->image_windows_number = 2;
   t_hough->pas=2;


   /* Recherche de l'image du gradient en entree  */

   /*  i = 0;
      j = 0;
      while (i >= 0)
      {
      i = find_input_link (gpe_sortie, j);
      if (prom_getopt (liaison[i].nom, "-gradient", resultat) == 2)
      {
      link1 = i;
      t_hough->groupe_entree = liaison[i].depart;break;
      }
      j++;
      }
    */
   /*Finding the link */

   link1 = find_input_link(gpe_sortie, 0);
   t_hough->groupe_entree = liaison[link1].depart;

   /*Pour verifier l'existence du liens */
   if (link1 == -1)
   {
      printf("*****> Il n'y a pas de groupe en entree -> entrez un gradient\n");
      exit(EXIT_FAILURE);
   }
   i=link1;

#ifdef DEBUG
   printf("*****> gpe_entree1 = %d   link1=%d sur le groupe %d\n",t_hough->groupe_entree, link1, gpe_sortie);
#endif

   image_gradient = (prom_images_struct *) def_groupe[t_hough->groupe_entree].ext;

   if (image_gradient == NULL)
   {
      printf("Probleme (function_circular_hough_transform : pas d'image de gradient dans le groupe %i\n", t_hough->groupe_entree);
      exit(EXIT_FAILURE);
   }


   def_groupe[gpe_sortie].ext =(prom_images_struct *) malloc(sizeof(prom_images_struct));
   if (def_groupe[gpe_sortie].ext == NULL)
   {
      printf("impossible d'allouer l'ext dans le groupe %d\n", gpe_sortie);
      exit(0);
   }


   /* allocation memoire pour les coordonnees des centres des visages et initialisation */

   /*centre= (TxPoint*) malloc (sizeof (TxPoint));
      ((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7] = (char*) centre ; */

   /*!
      recuperation des differents parametres sur le lien qui contient le -gradient
      ATTENTION a bien respecter minuscule et majuscule

      -r pour la taille minimum du cercle a rechercher
      -R pour la taille maximum du cercle a rechercher

      -H Haut   : pourcentage entier de la tranche horizontale  haute de l'image a ne pas traiter (facultatif)
      -B Bas    : pourcentage entier de la tranche horizontale  basse de l'image a ne pas traiter (facultatif)
      -G Gauche : pourcentage entier de la tranche verticale gauche de l'image a ne pas traiter (facultatif)
      -D Droite : pourcentage entier de la tranche verticale droite de l'image a ne pas traiter (facultatif)
      -z zone   : affiche la zone d'image traitee sous forme d'un rectangle vert
      -X        : position origine en x de l'image ou l'on superpose les cercles
      -Y        : position origine en y de l'image ou l'on superpose les cercles
      -En       : pour rechercher une ellipse au lieu d'un cercle. passez en parametre la valeur de f dans l'equation de l'ellipse.
      -S        : passer un float pour l'affichage des max locaux. Tous ceux superieur a ce pourcentage seront affiches.
      -In       : n=1 ou 2 suivant la fenetre dans laquelle vous voulez afficher l'image
      -pn       : pas d'increment pour la position des centres de cercles possibles (n gd reduit le temps de calcul.

      pour l'instant le programme ne fait pas les controles de validites compliques qu'il faudrait
      mettre en place pour -H -B -G -D
      aussi merci de faire en sorte que 0 > H+B < 99% et 0 > G+D <99%
      egalement passez une valeur entiere valable
      sinon ....
    */

   if (prom_getopt(liaison[i].nom, "-r", resultat) == 2)  t_hough->min_cercle = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-R", resultat) == 2)  t_hough->max_cercle = atoi(resultat);

   dprints("\n\n\n------------------------------------------------\n");
   dprints("--- FONCTION Transformee de HOUGH Circulaire ---\n\n\n");

   if (prom_getopt(liaison[i].nom, "-H", resultat) == 2)  t_hough->Haut = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-B", resultat) == 2)  t_hough->Bas = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-G", resultat) == 2)  t_hough->Gauche = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-D", resultat) == 2)  t_hough->Droit = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-z", resultat) == 2)  t_hough->zone_a_traiter = 1;
   if (prom_getopt(liaison[i].nom, "-X", resultat) == 2)  t_hough->PX = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-Y", resultat) == 2)  t_hough->PY = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-I", resultat) == 2)  t_hough->image_windows_number = atoi(resultat);
   if (prom_getopt(liaison[i].nom, "-p", resultat) == 2)  t_hough->pas = atoi(resultat);   
   if (prom_getopt(liaison[i].nom, "-x", resultat) == 2)
   {
      t_hough->calcul_ellipse = 1;
      t_hough->fx = atoi(resultat);
   }

   if (prom_getopt(liaison[i].nom, "-y", resultat) == 2)
   {
      t_hough->calcul_ellipse = 1;
      t_hough->fy = atoi(resultat);
   }

   if (prom_getopt(liaison[i].nom, "-S", resultat) == 2)
      t_hough->seuil_max_locaux = atof(resultat);

   t_hough->largeur = image_gradient->sx;
   t_hough->hauteur = image_gradient->sy;
   t_hough->numpixels = t_hough->largeur * t_hough->hauteur;

   /*initialisation des coordonnees du centre du cercle  (ou de l'ellipse) gagnant(e) */
   t_hough->x = t_hough->largeur / 2;
   t_hough->y = t_hough->hauteur / 2;

   /* bords en pourcentage de la taille de l'image */
   t_hough->Haut = (int) ((t_hough->Haut * t_hough->hauteur) / 100);
   t_hough->Bas = (int) ((t_hough->Bas * t_hough->hauteur) / 100);
   t_hough->Gauche = (int) ((t_hough->Gauche * t_hough->largeur) / 100);
   t_hough->Droit = (int) ((t_hough->Droit * t_hough->largeur) / 100);
   
   if( t_hough->Haut<t_hough->max_cercle )  t_hough->Haut = t_hough->max_cercle;
   if( t_hough->Bas<t_hough->max_cercle )  t_hough->Bas = t_hough->max_cercle;
   if( t_hough->Gauche<t_hough->max_cercle )  t_hough->Gauche = t_hough->max_cercle;
   if( t_hough->Droit<t_hough->max_cercle )  t_hough->Droit = t_hough->max_cercle;
      
   t_hough->rayon_moyen =(int) ((t_hough->max_cercle + t_hough->min_cercle) / 2) ;

   t_hough->largeur_accu = t_hough->largeur - t_hough->Gauche - t_hough->Droit;
   t_hough->hauteur_accu =t_hough->hauteur - t_hough->Haut - t_hough->Bas;

   if (t_hough->rayon_moyen < 1)  t_hough->rayon_moyen = 1;

#ifdef DEBUG
   printf("groupe d'entree = %d\n", t_hough->groupe_entree);
   printf("largeur image = %d \n", t_hough->largeur);
   printf("hauteur image = %d \n", t_hough->hauteur);
   printf("largeur accu  = %d \n", t_hough->largeur_accu);
   printf("hauteur accu  = %d \n", t_hough->hauteur_accu);
   printf("max_cercle = %d \n", t_hough->max_cercle);
   printf("min_cercle = %d \n", t_hough->min_cercle);
   printf("numpixels  = %d \n", t_hough->numpixels);
   printf("pas  = %d \n", t_hough->pas);
   printf("-Haut   = %d\n", t_hough->Haut);
   printf("-Bas    = %d\n", t_hough->Bas);
   printf("-Gauche = %d\n", t_hough->Gauche);
   printf("-Droit  = %d\n", t_hough->Droit);
   printf("-z      = %d\n", t_hough->zone_a_traiter);
   printf("-X      = %d\n", t_hough->PX);
   printf("-Y      = %d\n", t_hough->PY);
   printf("fx       = %d\n", t_hough->fx);
   printf("fy       = %d\n", t_hough->fy);
   printf("size_X  = %d\n", size_X);
   printf("size_Y  = %d\n", size_Y);
   printf("size_XY = %d\n", size_XY);
   printf("r_moyen = %d\n", t_hough->rayon_moyen);
   printf("-I      = %d\n", t_hough->image_windows_number);
   if (t_hough->calcul_ellipse == 1) printf("-E ellipse  = oui\n");
   else printf("-E ellipse  = non\n");
   printf("seuil max locaux = %f\n", t_hough->seuil_max_locaux);
#endif

   dprints("*****> cht : verifications terminees. Debut traitement\n");

   /* allocation du tableau 2D pour la transformee de HOUHG */
   /* on ne peut pas le faire avant de connaitre largeur et hauteur */
   accumulator_of_hough =(float *) malloc(t_hough->largeur * t_hough->hauteur * sizeof(float));

   ((prom_images_struct *) def_groupe[gpe_sortie].ext)->images_table[0] = (unsigned char *) accumulator_of_hough;
   ((prom_images_struct *) def_groupe[gpe_sortie].ext)->nb_band=4;     /* pour pouvoir visualiser le tableau des accumulateurs comme une image de float */
   ((prom_images_struct *) def_groupe[gpe_sortie].ext)->image_number=1;
   ((prom_images_struct *) def_groupe[gpe_sortie].ext)->sx = t_hough->largeur;
   ((prom_images_struct *) def_groupe[gpe_sortie].ext)->sy = t_hough->hauteur;

   dprints("*****> initialisation de l'accumulateur \n");

   /* end of first declarations */
   
   init_table_hough(gpe_sortie);
}

void transformee_hough_ellipse(int gpe_sortie)
{
   int a,b,b2;
   int amin,amax,bmin,bmax;
   int r;
   int max_cercle,min_cercle;
   int i;
   int x,y;

   prom_images_struct *image_gradient = NULL;
   /* declaration faite dans hough.h */
   hough_struct * t_hough = NULL;
   float *accumulator_of_hough = NULL;

   t_hough =(hough_struct *) ((prom_images_struct *) def_groupe[gpe_sortie].data);
   accumulator_of_hough =(float *) ((prom_images_struct *) def_groupe[gpe_sortie].ext)-> images_table[1];
   /*centre=(TxPoint*)((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7]; */
   image_gradient = (prom_images_struct *) def_groupe[t_hough->groupe_entree].ext;

   max_cercle=t_hough->max_cercle*t_hough->max_cercle; /* pour eviter sqrt */
   min_cercle=t_hough->min_cercle*t_hough->min_cercle; /* pour eviter sqrt */

   /* initialisation a zero du tableau accumulator_of_hough a chaque nouvelle analyse */
   for (b = t_hough->hauteur-1; b-- ; )
   {
      b2=b * t_hough->largeur;
      for (a = t_hough->largeur-1; a-- ; )    accumulator_of_hough[a + b2] = 0.;
   }

   dprints("*****> recherche des pixels utiles dans l'image du gradient\n");

   /* trouve dans l'image binaire les pixels utiles et valorise l'accumulateur en consequence */
   for (i = t_hough->numpixels-1; i--; )
   {
      if (image_gradient->images_table[0][i] != 0)
      {
         x = i % t_hough->largeur;                       y = i / t_hough->largeur;

         amin = x - t_hough->max_cercle;                 amax = x + t_hough->max_cercle;
         if (amin < t_hough->Gauche)                     amin = t_hough->Gauche;
         if (amax > t_hough->largeur - t_hough->Droit)   amax = t_hough->largeur - t_hough->Droit;
         
         bmin = y - t_hough->max_cercle;                 bmax = y + t_hough->max_cercle;
         if (bmin < t_hough->Haut)                       bmin = t_hough->Haut;
         if (bmax > t_hough->hauteur - t_hough->Bas)     bmax = t_hough->hauteur - t_hough->Bas;

         for (a = amin; a < amax; a++)
            for (b = bmin; b < bmax; b++)
            {
               r = 0.5 *(((x - a + t_hough->fx) * (x - a + t_hough->fx) +(y - b + t_hough->fy) * (y - b + t_hough->fy)) +
                         ((x - a - t_hough->fx) * (x - a -t_hough->fx) + (y - b - t_hough-> fy)  * (y - b - t_hough->fy)));
               if ((r >= min_cercle)  && (r < max_cercle))
                  /* on multipli par 2.PI.R pour avoir une importance au prorata de la taille du cercle */
                  accumulator_of_hough[a + b * t_hough->largeur] =  accumulator_of_hough[a + b * t_hough->largeur] +  6.28 * r;
               /* accumulator_of_hough[a + b * largeur]++; */
            }
      }
   }
}

void init_table_hough(int gpe_sortie)
{
   int amin,amax,bmin,bmax;
   hough_struct * t_hough = NULL;
   int a,b;
   int dx, dy , r2;
   int max_cercle, max_cercle2,min_cercle, min_cercle2;
   
   int *table_hough;

   if(def_groupe[gpe_sortie].data==NULL) EXIT_ON_ERROR("oublie allocation champs data pour gpe %s \n",def_groupe[gpe_sortie].no_name);
   t_hough =(hough_struct *) ((prom_images_struct *) def_groupe[gpe_sortie].data);

   max_cercle  = t_hough->max_cercle; 
   min_cercle  = t_hough->min_cercle;   
   
   table_hough=(int*) calloc( max_cercle*max_cercle, sizeof(int) );  /* init a zero en meme temps */
   if(table_hough==NULL) EXIT_ON_ERROR("echec allocation table_hough dans gpe %s \n",def_groupe[gpe_sortie].no_name);   
   
   t_hough->table_hough=table_hough;

   max_cercle2 = max_cercle * max_cercle; /* pour eviter sqrt */
   min_cercle2 = min_cercle * min_cercle; /* pour eviter sqrt */
   dprints("min_cercle= %d , max_cercle= %d\n",min_cercle , max_cercle);
        
   amin = - max_cercle;    amax =  max_cercle;
   bmin = - max_cercle;    bmax =  max_cercle;
 
   for (b = bmax-1; b >= bmin ; b--)
   {
      for (a = amax-1 ; a>=amin; a -- )
      {
         dx=abs( - a ); dy=abs( - b);
         r2 = dx * dx + dy * dy;
         if ((r2 >= min_cercle2) && (r2 < max_cercle2)) table_hough[dx + dy*max_cercle]=1;
      }
   }
 /*  pour afficher la table si besoin 
   for(b=0;b<max_cercle; b++)
   {
     for(a=0;a<max_cercle;a++)
     {
       dx=abs( - a ); dy=abs( - b);
       printf("%d ",table_hough[dx + dy*max_cercle]);
     }
     printf("\n");
   }
   scanf("%d",&a);
*/
}

void transformee_hough_cercle(int gpe_sortie)
{
   int pas_x, pas_y;
   int a,b,b2;
   int amin,amax,bmin,bmax;
 //  int r2;
   int max_cercle; /* min_cercle, max_cercle2, min_cercle2;*/
 //  int i;
      int p;
   int x, xx, y,dx,dy;
   int lim_amax, lim_amin, lim_bmax, lim_bmin;
   int largeur, hauteur;

   prom_images_struct *image_gradient = NULL;
   /* declaration faite dans hough.h */
   hough_struct * t_hough = NULL;
   float *accumulator_of_hough = NULL, *m_acc;
   unsigned char *m_im;
   int *table_hough, *m_table_hough;

   t_hough =(hough_struct *) ((prom_images_struct *) def_groupe[gpe_sortie].data);
   accumulator_of_hough =(float *) ((prom_images_struct *) def_groupe[gpe_sortie].ext)-> images_table[0];
   /*centre=(TxPoint*)((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7]; */
   image_gradient = (prom_images_struct *) def_groupe[t_hough->groupe_entree].ext;
   table_hough=t_hough->table_hough;

   max_cercle  = t_hough->max_cercle; 
   pas_x = pas_y = t_hough->pas;
   
 /*  int r2,min_cercle2,max_cercle2,min_cercle;   
   min_cercle  = t_hough->min_cercle;
   max_cercle2 = max_cercle * max_cercle; 
   min_cercle2 = min_cercle * min_cercle; */
   
   largeur = t_hough->largeur; hauteur = t_hough->hauteur ;

   /* initialisation a zero du tableau accumulator_of_hough a chaque nouvelle analyse */
   for (b = hauteur-1; b-- ; )
   {
      b2=b * largeur;
      m_acc=&accumulator_of_hough[b2];
      for (a = largeur-1; a-- ; )    *(m_acc + a) = 0;
   }

   dprints("*****> recherche des pixels utiles dans l'image du gradient\n");

   /* trouve dans l'image binaire les pixels utiles et valorise l'accumulateur en consequence */
   m_im = image_gradient->images_table[0];
   lim_bmin = t_hough->Haut ;   lim_bmax = hauteur - t_hough->Bas; 
   lim_amin = t_hough->Gauche ; lim_amax = largeur - t_hough->Droit ;
   
//   for (i = t_hough->numpixels-1; i--; )
   for(y=lim_bmin; y<lim_bmax ; y=y+pas_y)
   {
     p=y*largeur;
     for(x=lim_amin; x<lim_amax; x=x+pas_x)
     {
 //      printf("x %d y %d p=%d\n",x,y,p);
      if (m_im[p+x] != 0)
      {
 //        x = i % largeur;           y = i / largeur;

         amin = x - max_cercle+1;  amax = x + max_cercle;
         if (amin < lim_amin)      amin = lim_amin;
         if (amax > lim_amax)      amax = lim_amax;
         bmin = y - max_cercle+1;  bmax = y + max_cercle;
         if (bmin < lim_bmin)      bmin = lim_bmin;
         if (bmax > lim_bmax)      bmax = lim_bmax;

        xx=x-amin;
         /* on calcul pour un cercle */

         for (b = bmax-1; b >= bmin ; b--)
         {
            b2=b * largeur+amin;
            m_acc=&accumulator_of_hough[b2];
            dy= abs(y - b)*max_cercle; m_table_hough=&table_hough[dy];
            for (a = amax -amin ; a --; )
            {
               dx= abs(xx - a );  
               (*(m_acc+a))+= *(m_table_hough + dx); // table_hough[dx+dy];

              /* equivalent en version lente a :
               r2 = dx * dx + dy * dy;
               if ((r2 >= min_cercle2) && (r2 < max_cercle2)) (*(m_acc+a))++;
               */
               
            }
         }
      }
     }
   }
}

/*----------------------------------------------------------------------------------------*/

void function_circular_hough_transform_for_head(int gpe_sortie)
{
   int i, largeur, hauteur,h2;
   int a, b, v;

   int maxima,max2;
   int pt_x_max, pt_y_max;
   
#ifndef AVEUGLE   
   TxDonneesFenetre *image;
   TxPoint point, point_depart, point_arrivee;
#endif

   /* definition des variables de gestion de la carte neuronale */
   int size_Y = def_groupe[gpe_sortie].tailley;
   int size_X = def_groupe[gpe_sortie].taillex;
   int size_XY = (size_X * size_Y);
   int first_neuron = def_groupe[gpe_sortie].premier_ele;
   int last_neuron = def_groupe[gpe_sortie].premier_ele + size_XY;
   int local_x, local_y, n;

   /* declaration faite dans hough.h */
   hough_struct * t_hough = NULL;
   float *accumulator_of_hough = NULL;

   /* ratio pour calculer le mapping de l'image d'entree sur la carte neuronale */
   float ratio_x, ratio_y;
   float nouveau_n, ancien_n;

   /*printf ("premiere entree dans la fonction \n");
      mettre ici les choses a ne faire qu'une fois */
   if (def_groupe[gpe_sortie].ext == NULL) init_function_circular_hough_transform_for_head(gpe_sortie);

   t_hough =(hough_struct *) ((prom_images_struct *) def_groupe[gpe_sortie].data);
   accumulator_of_hough =(float *) ((prom_images_struct *) def_groupe[gpe_sortie].ext)-> images_table[0];
   /*centre=(TxPoint*)((prom_images_struct*)def_groupe[gpe_sortie].ext)->images_table[7]; */


#ifndef AVEUGLE
   if (t_hough->image_windows_number == 1) image=&image1; else image=&image2;
   /* on dessine un rectangle vert autour de la zone a traiter */
   if (t_hough->zone_a_traiter == 1)
   {
      dprints("*****>je dessine le rectangle vert qui delimite la zone de travail dans la fenetre %d \n",t_hough->image_windows_number);
      point.x = t_hough->Gauche +  t_hough->PX;
      point.y = t_hough->Haut +  t_hough->PY;
      /* TxDessinerRectangle(ptr_image,couleur,TxVide ou TxPlein,point d'origine de type TxPoint, largeur,hauteur,epaisseur) */

      TxDessinerRectangle(image, vert, TxVide, point,t_hough->largeur - t_hough->Gauche - t_hough->Droit,t_hough->hauteur - t_hough->Haut - t_hough->Bas, 2);
      TxDisplay(image);
   }
#endif

    if (t_hough->calcul_ellipse == 1)  transformee_hough_ellipse(gpe_sortie);
    else transformee_hough_cercle(gpe_sortie);

   dprints("*****> recherche des maximas\n");

   /* recherche des maximas dans le tableau accumulateur pour trouver les
      cercles de taille entre min_cercle et max_cercle donnee sur le lien */

   /* inititialisations */
   maxima = 0;
   pt_x_max = 0;  pt_y_max = 0;

   /* on recherche les maxima  */
//   amax= t_hough->largeur - t_hough->Droit-1-t_hough->Gauche;
   for (b = t_hough->Haut * t_hough->largeur; b < t_hough->numpixels - t_hough->Bas * t_hough->largeur; b += t_hough->largeur)
   {
      for (a = t_hough->Gauche; a < t_hough->largeur - t_hough->Droit; a++)
         /*    b2=b+t_hough->Gauche;
             for (a = amax ; a-- ; )*/
      {
//         v = accumulator_of_hough[a + b2];
         v = accumulator_of_hough[a + b];
         if (v > maxima)
         {
            maxima = v;
            pt_x_max = a; pt_y_max = b ;
         }
         /*
          #ifndef AVEUGLE
          vf = (float) v;

          if (vf > 10) //(maxima * t_hough->seuil_max_locaux))
            {
              point.x = a  + t_hough->PX;
              point.y = (b2 / t_hough->largeur) + t_hough->PY;
              printf("dessine cercle (%d, %d) R= %d \n",point.x,point.y,t_hough->rayon_moyen);
              TxDessinerCercle(image, bleu, TxVide, point,t_hough->rayon_moyen, 1);
            }
          #endif
         */
      }
   }

  pt_y_max   = (int) (pt_y_max / t_hough->largeur); /* a cause de l'increment dans la boucle precedente */
  t_hough->x = pt_x_max;   t_hough->y = pt_y_max;

#ifdef DEBUG
   if (maxima == 0) printf("pas de maxima trouve\n");
   else
   {
      /* donc pas besoin de valoriser la carte neuronale qui vaut zero partout */
      printf("le maxima trouve en (x,y)=(%d,%d) maxima=%d\n", pt_x_max, pt_y_max, maxima);
      printf("****> trace le cercle dans la fenetre %d \n", t_hough->image_windows_number);
   }
#endif

   dprints("gagnant x=%d y=%d \n",pt_x_max, pt_y_max );

#ifndef AVEUGLE                         /* trace le cercle gagnant dans la fenetre image1 ou 2 de promethe */
   point.x = pt_x_max + t_hough->PX ;   /* modif de PX en t_hough->PX 22/04/04 */
   point.y = pt_y_max + t_hough->PY ;
   TxDessinerCercle(image, rouge, TxVide, point, t_hough->max_cercle,3);

   if (t_hough->min_cercle > 7)
   {
      /* on trace une croix sur le centre du cercle pour les tres gros cercles */
      point_depart = point_arrivee = point;
      point_depart.x = point_arrivee.x =point.x - 4;
      TxDessinerSegment(image, rouge, point_depart, point_arrivee, 1);

      point_depart.x = point_arrivee.x = point.x;
      point_depart.y = point.y - 4;
      point_arrivee.y = point_arrivee.y + 4;
      TxDessinerSegment(image, rouge, point_depart, point_arrivee, 3);
   }
   TxDisplay(image); 
#endif

   ratio_x = ((float) size_X / (float) t_hough->largeur);
   ratio_y = ((float) size_Y / (float) t_hough->hauteur);

   dprints("sizex=%d sizey=%d largeur=%d hauteur=%d\n", size_X, size_Y, t_hough->largeur, t_hough->hauteur);
   dprints("*****> transfert de l'accumulateur dans la carte neuronale \n");
   dprints(" ratio_x= %f\n", ratio_x);
   dprints(" ratio_y= %f\n", ratio_y);

   /*printf("maxima pour transfert vers carte neuronale = %d \n",maxima); */
   
   if(size_XY==2) /* on ne sauvegarde que les coordonnees (x,y) du cercle gagnant convertie entre 0 et 1 */
   {
     dprints("%f %f %f %f \n", (float)pt_x_max, (float)t_hough->largeur, (float)pt_y_max, (float)t_hough->hauteur);
     neurone[first_neuron].s   = neurone[first_neuron].s1   = neurone[first_neuron].s2   = ((float)pt_x_max)/((float)t_hough->largeur);
     neurone[first_neuron+1].s = neurone[first_neuron+1].s1 = neurone[first_neuron+1].s2 = ((float)pt_y_max)/((float)t_hough->hauteur);  
     return;   
   }

   /* initialisation a zero de la carte neuronale
      printf("first neuron=%d  last_neuron=%d\n",first_neuron,last_neuron); */
   for (i = first_neuron; i < last_neuron; i++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
   }

  /* transfert de l'accumulateur dans la carte neuronale:  n = neurone a valoriser
    A cause de l'image qui est plus grande que la carte neuronale,  il faut mapper les valeurs d'entree vers la sortie */
      
   max2=(maxima * t_hough->seuil_max_locaux);
   for (hauteur = t_hough->hauteur-1; hauteur-- ; )
   {
      local_y = (int) (hauteur * (float) ratio_y)* size_X;
      h2=hauteur * t_hough->largeur;
      for (largeur = t_hough->largeur-1; largeur--  ; )
      {
         local_x = (int) (largeur * (float) ratio_x);
         n = local_x + local_y ;
         i = accumulator_of_hough[largeur + h2];

         if (i < max2)  i = 0;          /*on pourrait garder l'activite des maximums locaux
                                          if (i < maxima) i=0;  ou seulement l'activite maximum */
         nouveau_n = ((float) i / (float) maxima);

         n = n + first_neuron;
         ancien_n = neurone[n].s1;

         /* a cause du mapping un neurone peut etre la cible de plusieurs on garde donc uniquement la valeur du max */
         if (nouveau_n > ancien_n)
         {
            neurone[n].s = neurone[n].s1 = neurone[n].s2 = nouveau_n;
         }
      }
   }
   /*free (accumulator_of_hough); */
}
