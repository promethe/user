/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
   Cette fonction prend une image en entree et realise un vecteur place dans un reseau de neurones.
   Ce vecteur correspond a la projection de la somme des valeurs des pixels pour chaque colonne.
   Elle est utilise afin de localiser un oeil dans une image representant un visage apres un traitement "eye-analog-segment"
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/projection.h>
#include <Struct/prom_images_struct.h>
#include <Struct/feature_face.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*typedef struct feature_face_ver
{

  int image_windows_number;
  int groupe_entree;
  int nx;
  int ny;
  int n;
  int premier_neurone;
  float ech;

  int pos_image_affiche_x;
  int pos_image_affiche_y;
 
  int pos_oeilg;
  int pos_oeild;
  
  int zone_isole;

}feature_face_ver;*/



void function_histogramme_vertical(int Gpe)
{
    /* int taille_masque =20; */
    unsigned char *image;
    char resultat[256];
    int link = -1;
    int numpixel;
    int i = 0, j;
    /*  int indice_max=0,indice_max2=0,max; */
    int *histogramme_ver;
    float *moyennage_ver;

    /*  float max_moy; */
#ifdef DEBUG
    float pourcent_histo;
#endif

    int max_histo_y;
    int index_max_histo_y;

    feature_face *temp;

#ifdef DEBUG
    TxPoint point_depart, point_arrivee, point_depart2, point_arrivee2;
#endif

    /*Mesure du temps de traitement dans la fonction */
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {

        /* allocation de memoire */
        def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }


        /* sauvegarde des infos trouves sur le lien et du numero de groupe */
        temp = (feature_face *) malloc(sizeof(feature_face));
        /*((prom_images_struct*)def_groupe[Gpe].data)=(unsigned char*)temp; */
        def_groupe[Gpe].data = (unsigned char *) temp;

        /* parametres par default */

        temp->image_windows_number = 1;
        temp->groupe_entree_image = -1;
        temp->nx = 384;
        temp->ny = 288;
        temp->n = 110592;
        temp->pos_oeil_x = 288;
        temp->zone_isole = 5;

        /*Finding the link */

        link = find_input_link(Gpe, 0);
        temp->groupe_entree_image = liaison[link].depart;

        if (link == -1)
        {
            printf("*****> Il n'y a pas d'image en entree du Gpe %d\n", Gpe);
            exit(EXIT_FAILURE);
        }

        /* image pointeur sur l'image du groupe precedent */
        image =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->images_table[0];

        /* recuperation des infos sur la taille */
        temp->nx =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->sx;
        temp->ny =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->sy;

        temp->n = temp->nx * temp->ny;

#ifdef DEBUG
        printf("i dans projection verticale : %d\n", i);
        printf("temp->groupe_entree dans projection verticale : %d\n",
               temp->groupe_entree);
        printf("link dans projection verticale : %d\n", link);
#endif

        if (prom_getopt(liaison[link].nom, "-ZR", resultat) == 2)
        {
            temp->zone_isole = atoi(resultat);
        }
        if (prom_getopt(liaison[link].nom, "-I", resultat) == 2)
        {
            temp->image_windows_number = atoi(resultat);
        }
        if (prom_getopt(liaison[link].nom, "-PX", resultat) == 2)
        {
            temp->pos_image_affiche_x = atoi(resultat);
        }
        if (prom_getopt(liaison[link].nom, "-PY", resultat) == 2)
        {
            temp->pos_image_affiche_y = atoi(resultat);
        }
        /*    printf("Valeur de position en x :%d et position en y : %d",temp->pos_image_affiche_x,temp->pos_image_affiche_y); */

        /*recherche du numero du premier neurone du groupe et calcul de l'echelle
           pour la projection dans le reseau de neurones */

        temp->premier_neurone = def_groupe[Gpe].premier_ele;
        temp->ech = (float) def_groupe[Gpe].nbre / (float) temp->nx;

#ifdef DEBUG
        printf("temp->premier_neurone dans projection verticale : %d\n",
               temp->premier_neurone);
        printf("temp->ech dans projection verticale : %d\n", temp->ech);
#endif

        /* allocation de memoire pour l'histogramme verticale */
        histogramme_ver = (int *) malloc(temp->nx * sizeof(int));

        if (histogramme_ver == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE POUR L'HISTOGRAMME...! \n");
            exit(-1);
        }

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) histogramme_ver;

        /* allocation de memoire pour l'histogramme moyenne */
        moyennage_ver = (float *) malloc(temp->nx * sizeof(float));

        if (moyennage_ver == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE POUR L'HISTOGRAMME MOYENN�...! \n");
            exit(-1);
        }

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) moyennage_ver;

#ifdef DEBUG
        printf
            (" valeur des differentes variables dans la fonction de l'histogramme verticale\n");
        printf(" images_windows_number = %d\n", temp->image_windows_number);
        printf(" groupe entree %d\n", temp->groupe_entree_image);
        printf(" largeur de l'image en entree = %d\n", temp->nx);
        printf(" hauteur de l'image en entree = %d\n", temp->ny);
        printf(" nombre de pixels de l'image en entree = %d\n", temp->n);
        printf(" numero du premier neurone = %d\n", temp->premier_neurone);
        printf
            (" echelle pour la projection dans le reseau de neurones = %f\n",
             temp->ech);
#endif

    }

    else
    {
        histogramme_ver =
            (int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];
        temp = (feature_face *) ((prom_images_struct *) def_groupe[Gpe].data);

        image =
            ((prom_images_struct *) def_groupe[temp->groupe_entree_image].
             ext)->images_table[0];

        moyennage_ver =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];
    }


    for (i = 0; i < temp->nx; i++)
    {
        histogramme_ver[i] = 0;

        for (j = 0; j < temp->ny; j++)
        {
            numpixel = i + j * temp->nx;
            histogramme_ver[i] = histogramme_ver[i] + (int) image[numpixel];

        }
    }

    /*projection de l'histogramme verticale dans un reseau de neurones */
    for (i = 0; i < temp->nx; i++)
    {
        j = (int) (i * temp->ech) + temp->premier_neurone;
        neurone[j].s2 = 0.;
        neurone[j].s1 = 0.;
        /*neurone[j].s=(float)moyennage_ver[i]/max_moy; */
        neurone[j].s = histogramme_ver[i];
    }

    /*Recherche des max dans l'histogramme pour les yeux et les sourcils dans l'imagette */
    /*La recherche des zones de la bouche et des yeux doit se faire sur une image des countours */
    max_histo_y = 0;
    index_max_histo_y = 0;

    for (i = 0; i < temp->nx; i++)
    {
        /* printf("\nValeur de histogramme_hor :%d, numero de pixel de %d \n",histogramme_hor[i],numpixel); */
        if (max_histo_y < histogramme_ver[i])
        {
            max_histo_y = histogramme_ver[i];
            index_max_histo_y = i;
        }
    }

    temp->pos_oeil_x = index_max_histo_y;
    /*      printf("Valeur de pos_oeil_x %d",temp->pos_oeil_x); */

#ifndef AVEUGLE
#ifdef DEBUG
    /*Affichage de l'histogramme */
    for (i = 0; i < temp->nx; i++)
    {
        /*Affichage du tab moyennage */
        /*pourcent_histo = (float)moyennage_ver[i]/100; */
        pourcent_histo = (float) histogramme_ver[i] / 100;
        point_depart.x = temp->pos_image_affiche_x + i;
        point_depart.y = temp->pos_image_affiche_y;
        point_arrivee.x = temp->pos_image_affiche_x + i;
        /*      printf("Valeur de i : %d ,Valeur de histogramme_hor[i] : %f\n",i,pourcent_histo); */
        point_arrivee.y = (int) pourcent_histo + temp->pos_image_affiche_y;

        if (temp->image_windows_number == 1)
        {
            TxDessinerSegment(&image1, vert, point_depart, point_arrivee, 1);
            TxFlush(&image1);
        }
        else
        {
            TxDessinerSegment(&image2, vert, point_depart, point_arrivee, 1);
            TxFlush(&image2);
        }

    }
#endif
#endif

#ifndef AVEUGLE
#ifdef DEBUG
    /*Affichage zone de l'oeil */
    for (i = 0; i < temp->nx; i++)
    {
        /*Limite gauche de la zone */
        point_depart.x = temp->pos_oeil_x - temp->zone_isole;
        point_depart.y = 0;
        point_arrivee.x = temp->pos_oeil_x - temp->zone_isole;
        point_arrivee.y = temp->ny;

        if (point_depart.x <= 0)
        {
            point_depart.x = 0;
        }
        if (point_arrivee.x <= 0)
        {
            point_arrivee.x = 0;
        }

        /*Limite droite de la zone */
        point_depart2.x = temp->pos_oeil_x + temp->zone_isole;
        point_depart2.y = 0;
        point_arrivee2.x = temp->pos_oeil_x + temp->zone_isole;
        point_arrivee2.y = temp->ny;

        /*Affichage de la limite gauche de la zone oeil gauche */
        if (temp->image_windows_number == 1)
        {
            TxDessinerSegment(&image1, vert, point_depart, point_arrivee, 1);
            TxFlush(&image1);
        }
        else
        {
            TxDessinerSegment(&image2, vert, point_depart, point_arrivee, 1);
            TxFlush(&image2);
        }

        /*Affichage de la limite droite de la zone oeil gauche */
        if (temp->image_windows_number == 1)
        {
            TxDessinerSegment(&image1, bleu, point_depart2, point_arrivee2,
                              1);
            TxFlush(&image1);
        }
        else
        {
            TxDessinerSegment(&image2, bleu, point_depart2, point_arrivee2,
                              1);
            TxFlush(&image2);
        }

    }
#endif
#endif


    /*Fin du Timer et affichage des temps */
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", Gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif
}
