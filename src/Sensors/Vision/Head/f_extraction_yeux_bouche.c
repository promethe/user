/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_extraction_yeux_bouche.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    Cette fonction prend une image en entree et realise un vecteur place dans un reseau de neurones.
    Ce vecteur correspond a la projection de la somme des valeurs des pixels pour chaque ligne.
    Elle fournie en sortie les coordonnees en y des max des histogramme horizontales
    sur le lien il faut indiquer la taille de la zone d'isolement (-ZR) , la proportionalite entre les coordonnees des yeux et de la bouche (-P).
    Parametre facultatif PX et PY.
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/projection.h>
#include <Struct/prom_images_struct.h>
#include <Struct/feature_face.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_extraction_yeux_bouche(int Gpe)
{
   char resultat[256];
   int link1 = -1;
   int numpixel;
   int i = 0, j;
   int *histogramme_hor;
   int ny = 100;

   int max_histo_y;
   int index_max_histo_y;

   prom_images_struct *image;
   feature_face *temp;

#ifdef DEBUG
   TxPoint point_depart, point_arrivee;
   TxPoint point_depart2, point_arrivee2;
#endif

   if (def_groupe[Gpe].ext == NULL)
   {
      /* allocation de memoire */
      def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)
      {
         printf("ALLOCATION IMPOSSIBLE ...! \n");
         exit(-1);
      }

      temp = (feature_face *) malloc(sizeof(feature_face));
      def_groupe[Gpe].data = (unsigned char *) temp;

      /* parametres par default */
      temp->image_windows_number = 1;
      temp->groupe_entree_image = -1;
      temp->nx = 100;
      temp->ny = 100;
      temp->n = 10000;
      temp->pos_oeil_y = 0;
      temp->pos_bouche_y = 0;
      temp->zone_isole = 10;
      temp->proportion = 2.0;
      temp->pos_image_affiche_x = 0;
      temp->pos_image_affiche_y = 0;

      /*Finding the link */
      link1 = find_input_link(Gpe, 0);

      if (link1 == -1)
      {
         printf
         ("*****> Il n'y a pas de  groupe en entree du Gpe %d\n", Gpe);
         exit(EXIT_FAILURE);
      }

      temp->groupe_entree_image = liaison[link1].depart;
      i = link1;

#ifdef DEBUG
      printf("*****> gpe_entree 1 (image) = %d   link1=%d\n",
             temp->groupe_entree_image, link1);
      printf("liaison[link1].nom %s", liaison[link1].nom);
      printf("liaison[link1].depart %d", liaison[link1].depart);
#endif
      /* image pointeur sur l'image */
      image =
         (prom_images_struct *) def_groupe[temp->groupe_entree_image].ext;

      /*Allocation memoire pour un histogramme de longeur 100 pixels */
      histogramme_hor = (int *) malloc(ny * sizeof(int));
      /*printf("Valeur de ny d'initialisation de l'histogramme %d",ny); */

      if (histogramme_hor == NULL)
      {
         printf("ALLOCATION IMPOSSIBLE POUR L'HISTOGRAMME...! \n");
         exit(-1);
      }
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
         (unsigned char *) histogramme_hor;

      /* recuperation des infos sur la taille */
      temp->nx =((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
      temp->ny =((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
      temp->n = temp->nx * temp->ny;

      /*recuperation des infos sur le lien image_eye */
      if (prom_getopt(liaison[i].nom, "-P", resultat) == 2)
      {
         temp->proportion = atof(resultat);
      }
      if (prom_getopt(liaison[i].nom, "-ZR", resultat) == 2)
      {
         temp->zone_isole = atoi(resultat);
      }
      if (prom_getopt(liaison[i].nom, "-I", resultat) == 2)
      {
         temp->image_windows_number = atoi(resultat);
      }

      /*recherche du numero du premier neurone du groupe et calcul de l'echelle pour la projection dans le reseau de neurones */
      temp->premier_neurone = def_groupe[Gpe].premier_ele;
      temp->ech = (float) def_groupe[Gpe].nbre / (float) temp->nx;

#ifdef DEBUG
      printf
      (" valeur des differentes variables dans la fonction f_extraction_yeux_bouche\n");
      printf("temp->groupe_entree : %d\n", temp->groupe_entree_image);
      printf("link : %d\n", link1);
      printf(" images_windows_number = %d\n", temp->image_windows_number);
      printf(" largeur de l'image en entree = %d\n", temp->nx);
      printf(" hauteur de l'image en entree = %d\n", temp->ny);
      printf(" nombre de pixels de l'image en entree = %d\n", temp->n);
      printf(" numero du premier neurone = %d\n", temp->premier_neurone);
      printf
      (" echelle pour la projection dans le reseau de neurones = %f\n",
       temp->ech);
      printf(" Proportion %f\n", temp->proportion);
      printf(" Zone isole %d\n", temp->zone_isole);
#endif
   }

   else
   {
      /*Indication de passage */

      /*printf("FONCTION EXTRACTION\n"); */
      temp = (feature_face *) ((prom_images_struct *) def_groupe[Gpe].data);

      /* recuperation des infos sur la taille */
      temp->nx = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sx;
      temp->ny = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext)->sy;
      temp->n = temp->nx * temp->ny;

      temp->premier_neurone = def_groupe[Gpe].premier_ele;
      temp->ech = (float) def_groupe[Gpe].nbre / (float) temp->nx;

      /* image pointeur sur l'image du groupe precedent */
      image = ((prom_images_struct *) def_groupe[temp->groupe_entree_image].ext);

      /*Recuperation de l'histogramme horizontale */
      histogramme_hor =(int *) (((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[0]);
      /*      printf("PASSAGE DANS LE ELSE\n");     */
   }

#ifdef DEBUG
   printf("Function f_extraction_yeux_bouche");
   printf("Valeur de temp->nx %d, temp->ny %d, temp->n %d\n", temp->nx,
          temp->ny, temp->n);
   printf("Valeur de nx %d, ny %d\n", nx, ny);
   printf("Valeur de sx %d  et de sy %d de image\n", image->sx, image->sy);
#endif

   /*Histogramme des niveaux de gris */
   for (i = 0; i < temp->ny; i++)
   {
      histogramme_hor[i] = 0;

      for (j = 0; j < temp->nx; j++)
      {
         numpixel = i * temp->nx + j;
         histogramme_hor[i] = histogramme_hor[i] + (int) (image->images_table[0][numpixel]);
      }
      /*printf("\n %d %d %d \n",i,(int)image[numpixel],histogramme_hor[i]); */
   }

   /*Recherche des max dans l'histogramme pour les yeux et les sourcils dans la partie superieur de l'imagette */
   /*La recherche des zone de la bouche et des yeux doit se faire sur une image provenant de eye_analog_segment */
   max_histo_y = 0;
   index_max_histo_y = 0;

   for (i = temp->ny / 2; i > 0; i--)
   {
      /* printf("\nValeur de histogramme_hor :%d, numero de pixel de %d \n",histogramme_hor[i],numpixel); */
      if (max_histo_y < histogramme_hor[i])
      {
         max_histo_y = histogramme_hor[i];
         index_max_histo_y = i;
      }
   }

   temp->pos_oeil_y = index_max_histo_y;

   temp->pos_bouche_y =
      (int) ((float) temp->proportion * (float) index_max_histo_y);

   /*projection de l'histogramme dans un reseau de neurones */
   /*              for(i=0;i<temp->nx;i++)
      {
      j=(int) (i*temp->ech) +temp->premier_neurone;
      neurone[j].s2=0.;neurone[j].s1=0.; */
   /*neurone[j].s=(float)moyennage_hor[i]/max_moy; */
   /*              neurone[j].s=histogramme_hor[i];
      } */

   dprints("FONCTION EXTRACTION");
   dprints("temp->pos_oeil_y :%d\n", temp->pos_oeil_y);
   dprints("temp->pos_bouche_y :%d\n", temp->pos_bouche_y);
   dprints("Affichage des zones sur l'image %d\n",temp->image_windows_number);


#ifndef AVEUGLE
#ifdef DEBUG
   /*Affichage de l'histogramme*/
   for (i = 0; i < temp->ny; i++)
   {
      pourcent_histo = (float) histogramme_hor[i] / 100;
      point_depart.x = temp->pos_image_affiche_x;
      point_depart.y = temp->pos_image_affiche_y + i;
      point_arrivee.x = (int) pourcent_histo + temp->pos_image_affiche_x;
      /*      printf("Valeur de i : %d ,Valeur de histogramme_hor[i] : %f\n",i,pourcent_histo); */
      point_arrivee.y = temp->pos_image_affiche_y + i;

      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, vert, point_depart, point_arrivee, 1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, vert, point_depart, point_arrivee, 1);
         TxFlush(&image2);
      }
   }
#endif
#endif

#ifndef AVEUGLE
#ifdef DEBUG
   /*Affichage de la moitie de l'image */
   for (i = 0; i < temp->ny; i++)
   {
      point_depart2.x = 0;
      point_depart2.y = temp->ny / 2;
      point_arrivee2.x = temp->nx;
      /*      printf("Valeur de i : %d ,Valeur de histogramme_hor[i] : %f\n",i,pourcent_histo); */
      point_arrivee2.y = temp->ny / 2;

      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, rouge, point_depart2, point_arrivee2,1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, rouge, point_depart2, point_arrivee2,1);
         TxFlush(&image2);
      }

   }
#endif
#endif

#ifndef AVEUGLE
#ifdef DEBUG
   /*Affichage de la zone des yeux */
   for (i = 0; i < temp->ny; i++)
   {
      /*Limite superieur de la zone */
      point_depart.x = 0;
      point_depart.y = temp->pos_oeil_y - temp->zone_isole;
      point_arrivee.x = temp->nx;
      point_arrivee.y = temp->pos_oeil_y - temp->zone_isole;

      if (point_depart.y <= 0)
         point_depart.y = 0;
      if (point_arrivee.y <= 0)
         point_arrivee.y = 0;

      /*Limite inferieur de la zone */
      point_depart2.x = 0;
      point_depart2.y = temp->pos_oeil_y + temp->zone_isole;
      point_arrivee2.x = temp->nx;
      point_arrivee2.y = temp->pos_oeil_y + temp->zone_isole;

      /*Affichage limite superieur */
      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, bleu, point_depart, point_arrivee, 1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, bleu, point_depart, point_arrivee, 1);
         TxFlush(&image2);
      }

      /*Affichage limite inferieur */
      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, vert, point_depart2, point_arrivee2,1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, vert, point_depart2, point_arrivee2,1);
         TxFlush(&image2);
      }

   }
#endif
#endif

#ifndef AVEUGLE
#ifdef DEBUG
   /*Affichage de la zone de la bouche */
   for (i = 0; i < temp->ny; i++)
   {
      /*Limite superieur de la zone de la bouche */
      point_depart.x = 0;
      point_depart.y = temp->pos_bouche_y - temp->zone_isole;
      point_arrivee.x = temp->nx;
      point_arrivee.y = temp->pos_bouche_y - temp->zone_isole;

      /*Limite inferieur de la zone de la bouche */
      point_depart2.x = 0;
      point_depart2.y = temp->pos_bouche_y + temp->zone_isole;
      point_arrivee2.x = temp->nx;
      point_arrivee2.y = temp->pos_bouche_y + temp->zone_isole;

      /*Ajustement de la zone inferieur */
      if (point_depart2.y >= temp->ny)
         point_depart2.y = temp->ny;
      if (point_arrivee2.y >= temp->ny)
         point_arrivee2.y = temp->ny;

      /*Affichage de la limite superieur de la zone de la bouche */
      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, bleu, point_depart, point_arrivee, 1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, bleu, point_depart, point_arrivee, 1);
         TxFlush(&image2);
      }

      /*Affichage de la limite inferieur de la zone de la bouche */
      if (temp->image_windows_number == 1)
      {
         TxDessinerSegment(&image1, vert, point_depart2, point_arrivee2,1);
         TxFlush(&image1);
      }
      else
      {
         TxDessinerSegment(&image2, vert, point_depart2, point_arrivee2,1);
         TxFlush(&image2);
      }

   }
#endif
#endif
}
