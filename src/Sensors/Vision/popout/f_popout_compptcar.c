/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_compptcar.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Competition entre cartes de points caracteristiques 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-popout_compet_inter()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include "tools/include/popout_compet_inter.h"

void function_popout_compptcar(int Gpe)
{
    /* competition inter carte */
    /* normalisation */
    /* seuillage */
    int InputGpe = 0, i, nx, ny, n, j;
    float puissance, norm, max, seuil, *ptcar[4], *result[4];
    char *chaine;

#ifdef DEBUG_FILE
    FILE *sortie0;
    FILE *sortie1;
    FILE *sortie2;
    FILE *sortie3;
    FILE *sortie4;
    FILE *sortie5;
    FILE *sortie6;
    FILE *sortie7;
    int pointeurim;
    float *surimage0;
    float *surimage1;
    float *surimage2;
    float *surimage3;
    float *surimage4;
    float *surimage5;
    float *surimage6;
    float *surimage7;
#endif

/* quelques variables */
    puissance = 20.0;
    seuil = 0.1;


    /* initialisation */
    if (def_groupe[Gpe].ext == NULL)
    {
        /* on cherche le groupe precedent */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                InputGpe = liaison[i].depart;
                break;
            }
        /*printf("dans function_popout_compptcar : alloc de mem pour le groupe\n"); */
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("dans function_popout_compptcar : ALLOCATION IMPOSSIBLE ...! \n");
            exit(0);
        }

        /*printf("dans function_popout_compptcar : recup info\n"); */
        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("dans function_popout_compptcar : votre image est couleur, cette fonction ne fait que le N&B\n");
            exit(0);
        }

        /*printf("dans function_popout_compptcar : mem pour les images\n"); */
        /* pour les images */
        for (i = 0; i < 4; i++)
        {
            ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[i] =
                (unsigned char *) malloc(n * sizeof(float));
            if (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[i] == NULL)
            {
                printf
                    ("dans function_popout_compptcar : ALLOCATION IMPOSSIBLE ...! \n");
                exit(0);
            }
        }
        /*printf("dans function_popout_compptcar : recup images precedentes\n"); */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[0];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[5] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[1];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[2];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[7] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[3];

        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 4;

    }
    else
    {


    }

    /* pour faciliter l'ecriture */
    /*printf("dans function_popout_compptcar : debut fonction\n"); */
    ptcar[0] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[4];
    ptcar[1] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[5];
    ptcar[2] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[6];
    ptcar[3] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[7];
    result[0] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    result[1] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    result[2] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[2];
    result[3] =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[3];

    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;


    /* la compet */
    /*printf("dans function_popout_compptcar : competition inter-carte\n"); */
    popout_compet_inter(ptcar, result, 4, nx, ny, 20);

    /* normalisation et seuillage */
    /*printf("dans function_popout_compptcar : normalisation et seuillage inter-carte\n"); */
    /* par rapport a la plus grande valeur rencontree sur l'ensemble des cartes */
    max = 0.0;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (result[i][j] > max)
                max = result[i][j];
        }
    }
    norm = 1 / max;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < n; j++)
        {
            /* normalisation */
            result[i][j] = result[i][j] * norm;
            /* seuillage */
            if (result[i][j] < seuil)
                result[i][j] = 0.0;
        }
    }

    /* affichage */
    /*affichage(1,result[0],nx,ny,0,0);
       affichage(1,result[1],nx,ny,nx+10,0);
       affichage(1,result[2],nx,ny,0,ny+10);
       affichage(1,result[3],nx,ny,nx+10,ny+10); */


#ifdef DEBUG_FILE
    surimage0 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    surimage1 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    surimage2 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[2];
    surimage3 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[3];
    surimage4 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[4];
    surimage5 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[5];
    surimage6 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[6];
    surimage7 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[7];
    sortie0 = fopen("compptcar0", "a");
    sortie1 = fopen("compptcar1", "a");
    sortie2 = fopen("compptcar2", "a");
    sortie3 = fopen("compptcar3", "a");
    sortie4 = fopen("compptcar4", "a");
    sortie5 = fopen("compptcar5", "a");
    sortie6 = fopen("compptcar6", "a");
    sortie7 = fopen("compptcar7", "a");
    for (pointeurim = 0; pointeurim < nx * ny; pointeurim++)
    {
        fprintf(sortie0, "%f \n", surimage0[pointeurim]);
        fprintf(sortie1, "%f \n", surimage1[pointeurim]);
        fprintf(sortie2, "%f \n", surimage2[pointeurim]);
        fprintf(sortie3, "%f \n", surimage3[pointeurim]);
        fprintf(sortie4, "%f \n", surimage4[pointeurim]);
        fprintf(sortie5, "%f \n", surimage5[pointeurim]);
        fprintf(sortie6, "%f \n", surimage6[pointeurim]);
        fprintf(sortie7, "%f \n", surimage7[pointeurim]);
    }
    fclose(sortie0);
    fclose(sortie1);
    fclose(sortie2);
    fclose(sortie3);
    fclose(sortie4);
    fclose(sortie5);
    fclose(sortie6);
    fclose(sortie7);
#endif

}
