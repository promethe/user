/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_demande_intensite_expression.c
\brief

Author: B.Fouque
Created: XX/XX/XXXX
Modified:
- author: B.Fouque
- description: Support de 3 formats differents pour les entrees lues dans le fichier listimage, lecture des intensites
- date: 01/12/2008

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
- Label extractor from an image list entry

Macro:
-none

Local variables:
-utilise la variable jice_nom_image de Vision/Image_IO/tools/include/local_var.h (lien avec f_load_baccon...)

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

Todo:
- Revoir le test des options d'entr� : dans le cas d'une liste d'image, ouvre le fichier inutilement
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <ctype.h>

#ifdef USE_THREADS
#include <pthread.h>
#endif

#include "tools/include/local_var.h"

/* #include "../../../include/ben_defines.h" */

/*!
 * \brief Indique le type de fichier listimage.
 */
enum E_FILENAME_ENCODE
{
   NOTHING,
   NUMBERS,
   FILENAMES,
   FILENAMES_CATEGORIES,
   FILENAMES_CATEGORIES_INTENSITIES
};

/*!
 * \brief Index des expressions faciales prototypiques.
 */
enum E_EMOTION
{
   TRISTESSE_NO,
   NEUTRE_NO,
   JOIE_NO,
   COLERE_NO,
   SURPRISE_NO,
   PEUR_NO,
   DEGOUT_NO,
   REJECT_NO
};

/* noms possibles (qu'on peut rencontrer dans un listimage) pour chaque type d'expression faciale */
#define TRISTESSE_NAME	        "triste"
#define SAD_NAME		"sad"
#define NEUTRE_NAME 	        "neutre"
#define NORMAL_NAME		"normal"
#define JOIE_NAME		"joie"
#define HAPPY_NAME		"happy"
#define COLERE_NAME		"colere"
#define SLEEPY_NAME		"sleepy"
#define SURPRISE_NAME           "surprise"
#define PEUR_NAME		"peur"
#define DEGOUT_NAME		"degout"
#define REJECT_NAME		"reject"

#define CATEGORIES_FILENAME     "categories_code.txt" /*!< Chemin du fichier stockant toutes les categories (expr faciales OU codes AU) */
#define CATEGORIES_MAX_NB       200 /*!< Nombre maximum de categories */
#define FAC_MAX_LEN             100 /*!< Taille maximale d'un code FACS */
#define STR_NAME_SIZE           100 /*!< Taille maximale d'un nom d'expression faciale (une categorie) */

extern int  mode_avec_categorie;
extern int  mode_categorie_intensite;
extern char nom_categorie[STR_NAME_SIZE];
extern int  listimage_ended;

typedef struct
{
   FILE  *f;
   int    continuer;
   int    file_is_list_of;        /* type de la liste : NUMBERS ou FILENAMES */
   char  *file;
   int    demande_utilisateur;
   char **categories;
   int   *class_vect;
   char **facs;
   int   *class_rank;
   int    nb_categories;
   int    taillex;
   int    tailley;
} file_data;

typedef struct
{
   int index;
   int rank;
} t_rank;

#ifdef USE_THREADS
extern pthread_mutex_t mutex_lecture_clavier;
#endif

/*!
 * \brief Extrait un no a partir d'un nom de categorie.
 */
void fill_category_no_from_name (int* no, char* name)
{
   if (strstr(name, NEUTRE_NAME) != NULL
         || strstr(name, NORMAL_NAME) != NULL)
      *no = NEUTRE_NO;
   else if (strstr(name, TRISTESSE_NAME) != NULL
            || strstr(name, SAD_NAME) != NULL)
      *no = TRISTESSE_NO;
   else if (strstr(name, JOIE_NAME) != NULL
            || strstr(name, HAPPY_NAME) != NULL)
      *no = JOIE_NO;
   else if (strstr(name, COLERE_NAME) != NULL
            || strstr(name, SLEEPY_NAME) != NULL)
      *no = COLERE_NO;
   else if (strstr(name, SURPRISE_NAME) != NULL)
      *no = SURPRISE_NO;
   else if (strstr(name, PEUR_NAME) != NULL)
      *no = PEUR_NO;
   else if (strstr(name, DEGOUT_NAME) != NULL)
      *no = DEGOUT_NO;
   else if (strstr(name, REJECT_NAME) != NULL)
      *no = REJECT_NO;
   else
   {
      EXIT_ON_ERROR("Attention : l'expression n'est pas reconnue dans l'expression %s !\n",name);
   }
}

/*!
 * \brief  Extrait le niveau d'intensite a partir du nom de l'image.
 */
void fill_intensity_no_from_name (int* no, char* name)
{
   char   level_str[STR_NAME_SIZE] = "rien";
   char*  substr = NULL;
   int    i = 0;
   int    first = 6;

   if (no == NULL)
   {
      EXIT_ON_ERROR ("fill_intensity_no_from_name: argument NULL\n");
   }
   substr = strstr (name, "_level");
   while (substr[first + i] != '_' && substr[first + i] != '\0')
   {
      level_str[i] = substr[first + i];
      i++;
   }
   level_str[i] = '\0';
   *no = atoi (level_str);
}

/*!
 * \brief  Ajoute une chaine str a un vecteur de chaines s'il elle n'est pas deja dedans.
 *
 * Un boolean changed indique si le vecteur a ete modifie.
 * Retourne la position de la chaine dans le vecteur.
 *
 * -> permet d'attribuer une cle a chaque code facs
 */
int stringvect_search_add (char** vect, int size, char* str, int* changed)
{
   int i = 0;

   if (vect == NULL)
   {
      EXIT_ON_ERROR ("stringvect_add(): vecteur nul");
   }
   if (str == NULL)
   {
      EXIT_ON_ERROR ("stringvect(): chaine nulle");
   }

   *changed = 0;
   for (i = 0; i < size; i++)
   {
      if (vect[i] != NULL)
      {
         if (strcmp (vect[i], str) == 0)
            return i;
      }
      else
      {
         vect[i] = (char *)malloc (STR_NAME_SIZE * sizeof (char));
         strcpy (vect[i], str);
         *changed = 1;
         return i;
      }
   }
   EXIT_ON_ERROR ("stringvect_search_add(): Taille de vecteur depassee:%i\n", size);
   return -1; /* PG: on ne devrait jamais arriver ici : rajout pour eviter waring compil */
}

void stringvect_print (char** vect, int size)
{
   int i = 0;

   cprints ("stringvect_print():\n");
   for (i = 0; i < size && vect[i] != NULL; i++)
      cprints ("%s ", vect[i]);
   cprints ("\n");
}

/*!
 * \brief  Extrait un vecteur de chaines representant les codes facs presents dans la chaine code.
 *
 * la chaine code est de la forme (1+2+3+...)
 * ex: ["1", "2", "10"]
 * nb_facs est mis a jour et donne le nb de codes FACS trouves
 */
void extract_facs_from_code (char **facs, char* code, int* nb_facs)
{
   int   i = 0;
   int   j = 0;
   int   code_len = strlen (code);
   int   fac_len = 0;
   int   start_pos = 0;

   *nb_facs = 0;
   for (i = 0; i <= code_len; i++)
   {
      if (i == (code_len) || code[i] == '+')
      {
         fac_len = i - start_pos;
         for (j = 0; j < fac_len; j++)
            facs[*nb_facs][j] = code[start_pos + j];
         facs[*nb_facs][fac_len] = '\0';
         if (isalpha (facs[*nb_facs][fac_len - 1]))
            facs[*nb_facs][fac_len - 1] = '\0';
         start_pos = i + 1;
         (*nb_facs)++;
      }
   }
}

/*!
 * \brief   Rempli un vecteur associant a chaque no de classe une valeur binaire (0=absent; 1=present).
 *
 * Utilise une chaine de codes facs (code_name) pour un exemple d'image donne (Ex: image1 1+2+3+...).
 * Rempli aussi un vecteur indiquant le classement d'un code FACS (mis a jour iterativement a chaque nouvel exemple).
 *
 * -> Permet d'avoir une info sur l'importance d'un code FACS en fction de sa frequence d'apparition
 */
void fill_category_classvect_from_any_name (char** names_vect, int* class_vect, char* code_name, char **facs, int* class_rank, int* nb_categories)
{
   int   i = 0;
   int   nb_facs = 0;
   int   no = 0;
   int   changed = 0;

   for (i = 0; i < CATEGORIES_MAX_NB; i++)
      class_vect[i] = 0;

   extract_facs_from_code (facs, code_name, &nb_facs);

   for (i = 0; i < nb_facs; i++)
   {
      no = stringvect_search_add (names_vect, CATEGORIES_MAX_NB, facs[i], &changed);
      if (changed)
         (*nb_categories)++;
      class_vect[no] = 1;
      class_rank[no]++;
   }
}

void intvect_print (int* vect, int size)
{
   int i = 0;

   cprints ("\n intvect_print()\n");
   for (i = 0; i < size; i++)
   {
      cprints ("%i ", vect[i]);
   }
   cprints ("\n");
   cprints ("i=%i\n", i);
}

int stringvect_size2 (char** vect, int size)
{
   int i = 0;

   for (i = 0; i < size; i++)
   {
      if (vect[i] == NULL) break;
   }

   if (i == size)
   {
      EXIT_ON_ERROR ("stringvect_size(): attention: le vecteur est plein!\n");
   }

   return i;
}

static int rank_cmp (void const* e1, void const* e2)
{
   t_rank* r1 = (t_rank *)e1;
   t_rank* r2 = (t_rank *)e2;

   if (r1->rank > r2->rank)      return -1;
   else if (r1->rank < r2->rank) return 1;
   else                          return 0;
}

/*!
 * \brief Sauvegarde des labels dans un fichier.
 *
 * Ecrit dans un fichier le nom de chaque code categorie rencontre (ex: tristesse; AU 12; etc..), avec son classement.
 * Les fonctions d'affichage en mode texte des resultats obtenus par chaque label utilisent ce fichier pour
 * avoir les noms de chaque label.
 */
void write_categories_names (char** categories, int nb, int* class_rank)
{
   FILE*     fp = NULL;
   t_rank    class_sorted[CATEGORIES_MAX_NB];
   int       i = 0;

   dprints ("write_categories_names ()\n");

   fp = fopen (CATEGORIES_FILENAME, "w");

   for (i = 0; i < nb; i++)
   {
      class_sorted[i].index = i;
      class_sorted[i].rank = class_rank[i];
   }

   qsort (class_sorted, nb, sizeof (t_rank), rank_cmp);

   for (i = 0; i < nb; i++)
   {
      fprintf (fp, "%s %i\n", categories[class_sorted[i].index], class_sorted[i].index);
   }
   fclose (fp);
   EXIT_ON_ERROR ("write_categories_code(): categories stockees; fermeture promethe...\n");
}

/*!
 * \brief Extrait une etiquette a partir d'une entree d'une liste de (image, label) chargee avec f_load_baccon.

 * -> Extrait le numero de categorie a activer en fonction du mode lu par f_ben_load_baccon (1, 2 ou 3).
 * -> Extrait eventuellement l'intensite si on est en mode intensite.
 *
 * Modes:
 * 1: 2 cas possibles
 * - categorie correspond a une emotion (tristesse; etc.) -> mise a 1 de l'emotion
 * - categorie correspond a une liste d'AU (1+2+3+...) -> mise a 1 de chaque AU
 *
 * 2: mise a 1 de l'emotion extraite a partir du nom du fichier
 *
 * 3: mise a 1 de l'emotion extraite a partir du nom du fichier,
 * et sur l'intensite recuperee aussi dans le nom de fichier
 */
void function_demande_intensite_expression(int numero)
{
   int deb, longueur;
   int i, x;
   int j = 0;
   int nlink;
   char param_link[255];
   FILE *f;
   int expression;
   int intensity = 0;
   char chaine[256];

   file_data *data = NULL;
   int nb_categories = 0;
   longueur = def_groupe[numero].nbre;

   nlink = find_input_link(numero, 0);

   dprints("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
   deb = def_groupe[numero].premier_ele;

   data = def_groupe[numero].data;
   /*s'il y a un lien en entree */
   if (nlink > -1)
   {
      /*debut initialisation */
      if (data == NULL && neurone[deb].flag < 1)
      {
         /*recherche si le lien contient un nom de fichier */
         if (prom_getopt(liaison[nlink].nom, "-f", param_link) == 2)
         {
            /*si oui, tente de l'ouvrir */
            f = fopen(param_link, "r");
            if (f == NULL)
            {
               EXIT_ON_ERROR("Cannot open input file: %s\n", param_link);
            }
            else
            {
               dprints("fichier %s ouvert avec succes\n", param_link);
               /*on cree la structure qui contiendra le pointeur vers ce fichier et on init */
               data = (file_data *) malloc(sizeof(file_data));
               if (data == NULL)
               {
                  EXIT_ON_ERROR("Erreur: impossible d'allouer la structure data_file du groupe:%d\n",numero);
               }
               data->f = f;
               neurone[deb].flag = -1;
               data->file = (char *) malloc(255);
               strcpy(data->file, param_link);

               /* Ajout AMTousch 24/02/2006 */
               /* on teste si on lit une liste de numeros ou une liste d'images */
               /* pour une liste d'image, a utiliser en lien avec f_load_baccon */
               data->file_is_list_of = NOTHING;
               data->categories = NULL;
               data->class_vect = NULL;
               data->class_rank = NULL;
               data->nb_categories = 0;
               data->taillex = def_groupe[numero].taillex;
               data->tailley = def_groupe[numero].tailley;
               if (strstr(liaison[nlink].nom, "listimage") != NULL)
               {
                  if (mode_avec_categorie)
                  {
                     data->file_is_list_of = FILENAMES_CATEGORIES;
                     data->categories = (char **)malloc (CATEGORIES_MAX_NB * sizeof (char *));
                     for (j = 0; j < CATEGORIES_MAX_NB; j++)   data->categories[j] = NULL;

                     data->class_vect = (int *)malloc (CATEGORIES_MAX_NB * sizeof (int));
                     for (j = 0; j < CATEGORIES_MAX_NB; j++) data->class_vect[j] = 0;

                     data->facs = (char **)malloc (CATEGORIES_MAX_NB * sizeof (char *));
                     for (j = 0; j < CATEGORIES_MAX_NB; j++) data->facs[j] = (char *)malloc (FAC_MAX_LEN * sizeof (char));

                     data->class_rank = (int *)calloc (CATEGORIES_MAX_NB, sizeof (int));

                     /* 			    fp = fopen (CATEGORIES_FILENAME, "w"); */
                     /* 			    fclose (fp); */
                  }
                  else
                  {
                     if (mode_categorie_intensite)  data->file_is_list_of = FILENAMES_CATEGORIES_INTENSITIES;
                     else                           data->file_is_list_of = FILENAMES;
                  }
               }
               else   data->file_is_list_of = NUMBERS;

               def_groupe[numero].data = data;
            }
         }
         else cprints("No input file found on %d\n", numero);
      }
   }
   /*si l'on utilise un fichier comme entree: */
   if (data != NULL)
   {

      switch (data->file_is_list_of)
      {
         case FILENAMES_CATEGORIES:
            /* le nom ne contient pas la categorie; elle est lue dans le 2e membre de la ligne du fichier */
            /* L'etiquette est remplie avec la categorie recuperee dans le fichier listimage par f_ben_load_baccon */
            /* le case FILENAMES gère la suite... */
            /* 	  fill_category_no_from_name (&expression, nom_categorie); */
            /* 	  fill_category_no_from_any_name (data->categories, &expression, nom_categorie); */
            fill_category_classvect_from_any_name (data->categories, data->class_vect, nom_categorie, data->facs, data->class_rank, &(data->nb_categories));
            if (listimage_ended)
            {
               write_categories_names (data->categories, data->nb_categories, data->class_rank);
               listimage_ended = 0;
            }
            cprints ("\n");
            cprints ("################# Nouvelle iteration ####################\n");
            cprints ("nom image:%s code supervision:%s no:%i\n", jice_nom_image, nom_categorie, expression);
            /* 	  intvect_print (data->class_vect, CATEGORIES_MAX_NB); */
            nb_categories = stringvect_size2 (data->categories, CATEGORIES_MAX_NB);
            cprints ("nombre de categories trouvees: %i\n", nb_categories);
            x = expression;
            break;

         case FILENAMES:
            /* le nom du fichier contient la categorie */
            /* On recupere le derniere image traitee par f_load_baccon dans local_var.h sous le nom jice_nom_image */
            /* A Ameliorer : passage par def_groupe.data en sortie du load baccon */
            /* 	  strcpy(nom_image, jice_nom_image);  /\*recuperation dans local_var.h *\/ */
            /* On cherche dans le nom du fichier le nom de l'expression representee par l'image. */
            /* en anglais : base de Yales, colere non representee remplacee par sleepy */
            fill_category_no_from_name (&expression, jice_nom_image);
            /* 	  printf("nom image:%s categorie: %i\n", jice_nom_image, expression); */
            x = expression;
            break;

         case FILENAMES_CATEGORIES_INTENSITIES:
            /* le nom contient a la fois la categorie et l'intensite */
            fill_category_no_from_name (&expression, jice_nom_image);
            fill_intensity_no_from_name (&intensity, jice_nom_image);
            x = expression * data->taillex + intensity;
            break;

         case NUMBERS:
            if (strcmp(liaison[nlink].nom, "") != 0  && strcmp(liaison[nlink].nom, ".") != 0  && strcmp(liaison[nlink].nom, "?") != 0) cprints("%s\n", liaison[nlink].nom);
            f = data->f;
            if (neurone[deb].flag < 1)
            {
               if (f != NULL)
               {
                  /*on lit son contenu pour obtenir l'index du neurone a mettre a 1 */
                  if (fscanf(f, "%d", &x) == EOF)
                  {
                     cprints("Plus d'entree a lire: %d \n", numero);
                     cprints("Voulez vous: \n 1) continuer a saisir manuellement\n 2)n'affecter aucune sortie (-1)\n 3) relancer la lecture depuis le debut \n 4) quitter\n");
                     cscans("%d", &i);
                     if (i == 1)
                     {
                        data = NULL;
                        def_groupe[numero].data = data;
                        do
                        {
                           cprints("Number of the neuron to activate:  [0 %d] (-1 means no activity)",longueur - 1);
                           cscans("%d", &x);
                        }
                        while (x >= longueur);
                        neurone[deb].flag = 1;
                     }
                     else if (i == 2)
                     {
                        neurone[deb].flag = 1;
                        x = -1;
                     }
                     else if (i == 3)
                     {
                        fclose(f);
                        data->f = fopen(data->file, "r");
                        f = data->f;
                        TRY_FSCANF(1,f, "%d", &x);
                     }
                     else EXIT_ON_ERROR("valeur lue i=%d non prevue \n",i);
                  }
                  else  cprints("Number of the neurone activated :%d on group: %d \n", x, numero);
               }
            }
            else
               x = -1;
            break;

         case NOTHING:
            EXIT_ON_ERROR("type de liste du fichier non reconnue\n");
            break;
      }
      /*         printf ("categorie lue:%d\n", x); */
   }

   else                        /*sinon saisie par l'utilisateur */
   {
#ifdef USE_THREADS
      pthread_mutex_lock(&mutex_lecture_clavier);
#endif
      if (neurone[deb].flag==0) do
         {
            cprints("Function_ask_number(%d)\n", numero);
            if (strcmp(liaison[nlink].nom, "") != 0 && strcmp(liaison[nlink].nom, ".") != 0   && strcmp(liaison[nlink].nom, "?") != 0) cprints("%s\n", liaison[nlink].nom);

            cprints("Number of the neurone to activate:  [0 %d] (-1 means no activity)",longueur - 1);
            /* scanf("%d", &x);*/
            cscans("%s",chaine);
            if (chaine[0]=='x' || chaine[0]=='f') {x=-1; neurone[deb].flag=1;}
            else x=atoi(chaine);
         }
         while (x >= longueur);
#ifdef USE_THREADS
      pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
   }


   for (i = deb; i < deb + longueur; i++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
   }

   if (data->class_vect == NULL && x >= 0)
      neurone[x + deb].s = neurone[x + deb].s1 = neurone[x + deb].s2 = 1.;
   else
   {
      for (j = 0; j < longueur; j++)
      {
         if (data->class_vect[j])
            neurone[j + deb].s = neurone[j + deb].s1 = neurone[j + deb].s2 = 1.;
         /* 	    if (j == x) */
         /* 	      neurone[j + deb].s = neurone[j + deb].s1 = neurone[j + deb].s2 = 1.; */
      }
   }
   dprints("===========sortie de %s\n", __FUNCTION__);
}
