/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_master2.c 
\brief 

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-tools/Vision/affichage_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>

#include "tools/include/macro.h"

#include <public_tools/Vision.h>
#include <libhardware.h>
void function_popout_master2(int Gpe)
{
    int i, j, InputGpe = 0, n, nx, ny, x, y, dmin, dmax;
    float *master, maxi, turn;
    float minimum = 1e20;

    extern int USE_ROBOT;
    dmin = 1;
    dmax = 10;
    /* unused
       FILE *fichier;
       int k;
     */
    /* initialisation */
    if (def_groupe[Gpe].ext == NULL)
    {
        /*printf
           ("dans function_popout_master2 : alloc de mem pour le groupe\n"); */
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("dans function_popout_master2 : ALLOCATION IMPOSSIBLE ...! \n");
            exit(0);
        }
        /* on cherche le groupe precedent */
        /*printf("dans function_popout_master2 : recup master map\n"); */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                /*printf("groupe en entree : %d\n",InputGpe); */
                ((prom_images_struct *) def_groupe[Gpe].ext)->
                    images_table[0] =
                    ((prom_images_struct *) def_groupe[InputGpe].ext)->
                    images_table[0];
                /*printf("recup ok\n"); */
                break;
                /*printf("adresse : %x\n", ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[0]); */
            }




        /* printf("dans function_popout_master2 : recup info\n"); */
        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("dans function_popout_master2 : votre image est couleur, cette fonction ne fait que le N&B\n");
            exit(0);
        }








        /*neurone[def_groupe[Gpe].premier_ele].s = (float) j; */


    }




    else
    {


    }
    /*printf("dans function_popout_master2 : simplification\n"); */
    /* simplifications */

    master =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;


    x = 0;
    y = 0;
    /* recherche du point de focalisation */
    maxi = MIN_FOR_FOCUS;
    for (i = 0; i < ny; i++)
        for (j = 0; j < nx; j++)
        {
            if (master[i * nx + j] > maxi)
            {
                maxi = master[i * nx + j];
                x = j;          /* i;  29/04/2003 Olivier Ledoux */
                y = i;          /* j;  29/04/2003 Olivier Ledoux */
            }
            if (master[i * nx + j] < minimum)
                minimum = master[i * nx + j];
        }
    if (maxi > MIN_FOR_FOCUS)
    {
        printf
            ("%s : point de focalisation : x=%d, y=%d, I=%f\n\t(min=%1.2f, only first value is good)\n",
             __FUNCTION__, x, y, maxi, minimum);

        neurone[def_groupe[Gpe].premier_ele].s = (float) x;
        neurone[def_groupe[Gpe].premier_ele + 1].s = (float) y;

#ifndef AVEUGLE                 /* 20/06/2003 Olivier Ledoux */
        /*affiche_pdv(&image1, dmin, dmax, vert, y, x, " "); */
        affiche_pdv(&image1, dmin, dmax, vert, x, y, (char*)" ");  /* 29/04/2003 Olivier Ledoux 
        getchar(); */
#endif

    }
    else
    {
        printf("pas de point de focalisation\n");
        /*getchar(); */
    }
#ifndef AVEUGLE                 /*rajouter par issam le 17/06/2003   */
    affichage2(2, master, nx, ny, 0, 0);
#endif
    /* inhibition */
    /*master[x*nx+y] = 0.0; */
    /* new code for inhibition */
    for (i = 0; i < ny; i++)
        for (j = 0; j < nx; j++)
            /*if (((i - x) * (i - x) + (j - y) * (j - y)) < 25) */
            if (((i - y) * (i - y) + (j - x) * (j - x)) < 25)   /* 29/04/2003 Olivier Ledoux */
                master[i * nx + j] = -1.0e20;



    /* cette partie du code ne sert qu'a sauvegarder
       la sequence de focalisation */
    /* on ecrit a la fin du fichier sans l'ecraser */
    /*fichier = fopen("seqfocus","a");
       fprintf(fichier,"%d %d\n",x,y);
       fclose(fichier); */


    /* champ de la camera : 50 (cam pan tilt), 40 (cam pan) */

    if (USE_ROBOT)
    {
        turn = ((float) y - nx / 2) / ((float) nx / 2) * 20.0;
        printf("%s:(%d) : tourne de %f (%d/%d)\n", __FUNCTION__, __LINE__,
               turn, y, nx);
        /*getchar(); */
        if (turn > 0)
        {
            /* turn right */
            robot_turn_angle(robot_get_first_robot(), turn);
        }
        else
        {
            /* turn left */
            robot_turn_angle(robot_get_first_robot(), -1 * turn);
        }

        /*
           progress(30.0); */
    }

    /*getchar(); */
}
