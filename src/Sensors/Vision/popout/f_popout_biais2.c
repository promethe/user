/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_biais2.c 
\brief 

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: Olivier Ledoux
- description: le lien -m est devenu facultatif. Lorsqu'il n'existe pas les neurones sur lesquels ils devaient jouer sont forces a 0.0
- date: 27/11/2003
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Cette boite permet de moduler les cartes de caracteristiques, il s'agit
   d'une evolution de f_popout_biais.
   elle recupere les infos dans deux groupes LMS :
   - un pour le centre (lien -c)
   - un pour le facteur multiplicatif (lien -m)
  le lien -m est devenu facultatif.
  Lorsqu'il n'existe pas les neurones sur lesquels ils devaient jouer sont forces a 0.0

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()
-Kernel_Function/prom_getopt()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

void function_popout_biais2(int Gpe)
{
    typedef struct
    {
        int GC, GM;
    } data_struct;
    int i, lien;
    char *chaine;
    char retour[256];
    int na, nb, j;
    /* initialisation */

    /* printf ("on est dans popout biais2\n");getchar();
     */


    if (def_groupe[Gpe].data == NULL)
    {
        printf("%s : alloc de mem pour le groupe\n", __FUNCTION__);
        /* allocation de memoire */
        def_groupe[Gpe].data = (void *) malloc(sizeof(data_struct));
        if (def_groupe[Gpe].data == NULL)
        {
            printf("%s : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }
        ((data_struct *) def_groupe[Gpe].data)->GC = -1;
        ((data_struct *) def_groupe[Gpe].data)->GM = -1;

        /* on cherche les groupes precedents */
        printf("%s : recup info lien\n", __FUNCTION__);
        i = 0;
        do
        {
            lien = find_input_link(Gpe, i);
            i++;
            if (lien != -1)
            {
                chaine = liaison[lien].nom;
                if (prom_getopt(chaine, "c", retour) == 1)
                {
                    ((data_struct *) def_groupe[Gpe].data)->GC =
                        liaison[lien].depart;
                    printf("%s : groupe centre : %d \n", __FUNCTION__,
                           liaison[lien].depart);
                }
                if (prom_getopt(chaine, "m", retour) == 1)
                {
                    ((data_struct *) def_groupe[Gpe].data)->GM =
                        liaison[lien].depart;
                    printf("%s : groupe multiplicateur : %d\n", __FUNCTION__,
                           liaison[lien].depart);

                }
            }
        }
        while (lien != -1);
        /* fin de la boucle de recherche sur les liens */
        if (((data_struct *) def_groupe[Gpe].data)->GM == -1
            /* suppression car test obsolete 27/11/2003 Olivier Ledoux */
            /*|| ((data_struct*) def_groupe[Gpe].data)->GC== -1 */
            || ((data_struct *) def_groupe[Gpe].data)->GC ==
            ((data_struct *) def_groupe[Gpe].data)->GM)
        {
            printf("%s : probleme avec les liens, exit\n", __FUNCTION__);
            exit(1);
        }


        /* modification du test 27/11/2003 Olivier Ledoux */
        /*
           if( def_groupe[((data_struct*) def_groupe[Gpe].data)->GM].nbre != def_groupe[((data_struct*) def_groupe[Gpe].data)->GC].nbre
           ||def_groupe[((data_struct*) def_groupe[Gpe].data)->GC].nbre != (def_groupe[Gpe].nbre /2))
         */



        if (((data_struct *) def_groupe[Gpe].data)->GC != -1)
        {
            if (def_groupe[((data_struct *) def_groupe[Gpe].data)->GM].nbre !=
                def_groupe[((data_struct *) def_groupe[Gpe].data)->GC].nbre
                || def_groupe[((data_struct *) def_groupe[Gpe].data)->GC].
                nbre != (def_groupe[Gpe].nbre / 2))
            {
                printf("%s : probleme de nombre de neurones, exit\n",
                       __FUNCTION__);
            }
        }
        else if (def_groupe[((data_struct *) def_groupe[Gpe].data)->GM].
                 nbre != (def_groupe[Gpe].nbre / 2))
        {
            printf("%s : probleme de nombre de neurones, exit\n",
                   __FUNCTION__);
        }
    }
    else
    {

    }
    na = ((data_struct *) def_groupe[Gpe].data)->GC;
    nb = ((data_struct *) def_groupe[Gpe].data)->GM;
    /*printf("%s : groupe %d (entrees : %d,%d)\n",__FUNCTION__,Gpe,na,nb); */
    j = 0;
    /* recopie des neurones des deux autres groupes */
    for (i = def_groupe[Gpe].premier_ele;
         i < def_groupe[Gpe].premier_ele + def_groupe[Gpe].nbre / 2; i++)
    {

        /*printf("%s : copie du neurone %d sur neurone %d (s2=%2.3f)\n",__FUNCTION__,def_groupe[na].premier_ele + j,i,neurone[def_groupe[na].premier_ele + j].s2);
           printf("%s : copie du neurone %d sur neurone %d (s2=%2.3f)\n",__FUNCTION__,def_groupe[nb].premier_ele + j,i+def_groupe[Gpe].nbre/2,neurone[def_groupe[nb].premier_ele + j].s2); */

        if (na != -1)
        {
            neurone[i].s2 = neurone[def_groupe[na].premier_ele + j].s2;
            neurone[i].s1 = neurone[def_groupe[na].premier_ele + j].s1;
            neurone[i].s = neurone[def_groupe[na].premier_ele + j].s;
        }
        else
        {
            neurone[i].s2 = 0.0;
            neurone[i].s1 = 0.0;
            neurone[i].s = 0.0;
        }

        neurone[i + def_groupe[Gpe].nbre / 2].s2 =
            neurone[def_groupe[nb].premier_ele + j].s2;
        neurone[i + def_groupe[Gpe].nbre / 2].s1 =
            neurone[def_groupe[nb].premier_ele + j].s1;
        neurone[i + def_groupe[Gpe].nbre / 2].s =
            neurone[def_groupe[nb].premier_ele + j].s;

        j++;

    }

    /* test temp 16/10/2003 Olivier Ledoux */

    for (i = def_groupe[Gpe].premier_ele;
         i < def_groupe[Gpe].premier_ele + def_groupe[Gpe].nbre; i++)
    {
        printf("boite biais2: neurones (%d)=%f,%f,%f\n", i, neurone[i].s,
               neurone[i].s1, neurone[i].s2);
    }


}
