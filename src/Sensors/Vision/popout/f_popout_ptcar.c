/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_ptcar.c 
\brief Boite pour l'extraction des points caracteristiques 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Boite pour l'extraction des points caracteristiques 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-popout_wta()
-popout_conv()
-popout_norm()
-popout_dog()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>

#include "tools/include/popout_wta.h"
#include "tools/include/popout_conv.h"
#include "tools/include/popout_norm.h"
#include "tools/include/popout_dog.h"

void function_popout_ptcar(int Gpe)
{
    int i, j, nx, ny, n, InputGpe = 0, taille_dog, demi_taille;
    char *chaine;
    float sigma1, sigma2, epsilon1, epsilon2;
    float *grad1, *grad2, *som1, *som2, *som3, *som4, *ptc1, *ptc2, *ptc3, *ptc4, *dog;

    float seuil = 0.2;
    /* unused :
       int  taille_poids,
       float *g2, *g1,* poids,*tmp1, *tmp2;
       float  m[] = { 1., 1., 1., 1., 1., 1., 1., 1., 1. };
     */
#ifdef DEBUG_FILE
    /* Olivier Ledoux 11/04/2003 */
    FILE *sortie1;
    FILE *sortie2;
    int pointeurim;
    char chemin1[128];
    char chemin2[128];
    float *surimage1;
    float *surimage2;
#endif

/* quelques parametres */
    sigma1 = 1.0;               /*3.0; */
    sigma2 = 0.5;               /*2.0; */
    taille_dog = 15;            /*31; */
    epsilon1 = 1e-8;
    epsilon2 = 1e-3;
/* faut il les mettre sur le liens ? */

    /* initilisation */
    if (def_groupe[Gpe].ext == NULL)
    {
        /* on cherche le groupe precedent */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                InputGpe = liaison[i].depart;
                break;
            }
        /* chaine contient les parametre du lien et
           inputGpe le numero du groupe precedent */
        /* verification des parametres du lien */
        /* allocation de memoire */
        def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            EXIT_ON_ERROR("dans function_popout_ptcar : ALLOCATION IMPOSSIBLE ...! \n");
        }
        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            EXIT_ON_ERROR("dans function_popout_ptcar : votre image est couleur, cette fonction ne fait que le N&B\n");
        }

        /* pour les images */
        for (i = 0; i < 8; i++)
        {
            ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[i] =(unsigned char *) malloc(n * sizeof(float));
            if (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[i] == NULL)
            {
                EXIT_ON_ERROR("dans function_popout_ptcar : ALLOCATION IMPOSSIBLE ...! \n");
            }
        }
        /* on reserve 8 et 9 au gradiant */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[8] =((prom_images_struct *) def_groupe[InputGpe].ext)->images_table[6];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[9] =((prom_images_struct *) def_groupe[InputGpe].ext)->images_table[7];

        /* creation des filtres et sauvegarde */
        /* creation de la dog */
        cprints("call popout_dog(%f,%f,%d)", sigma1, sigma2, taille_dog);
        ((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[11] =  (unsigned char *) popout_dog(sigma1, sigma2, taille_dog);

        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[12]) = (unsigned char *) malloc(sizeof(int));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[12] == NULL)
        {
            EXIT_ON_ERROR("dans function_popout_ptcar : plus de memoire\n");
        }
        /*(int) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[12] = taille_dog; */
        /*((prom_images_struct *) def_groupe[Gpe].ext)->images_table[12]=(unsigned char) taille_dog;  11/04/2003 Olivier Ledoux */
        (*((prom_images_struct *) def_groupe[Gpe].ext)->images_table[12]) =  taille_dog;    /* 12/04/2003 Olivier Ledoux */

        /* creation des poids pour la competition */
        /* supprime car trop dur a faire tourner
           popout_motif_compet(epsilon1,epsilon2,sigma1,sigma2,&tmp1,&tmp2,&taille_poids);
           ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[15] = (unsigned char*)malloc(sizeof(int));
           if(((prom_images_struct*)def_groupe[Gpe].ext)->images_table[15] == NULL)
           {printf("dans function_popout_ptcar : plus de memoire\n");exit(0);}
           (int)((prom_images_struct*)def_groupe[Gpe].ext)->images_table[15] = taille_poids;
           ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[13] = (unsigned char*)tmp1;
           ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[14] = (unsigned char*)tmp2; */
    }
    else
    {                           /* recuperation des donnees */
        taille_dog =(int) (* ((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[12]);
        /*taille_poids = (int)((prom_images_struct*)def_groupe[Gpe].ext)->images_table[15]; */
    }
    /* la fonction */
    /* pour simplifier l'ecriture */
    grad1 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[8];
    grad2 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[9];

    som1 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0];
    som2 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1];
    som3 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2];
    som4 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3];
    ptc1 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4];
    ptc2 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[5];
    ptc3 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6];
    ptc4 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[7];

    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;

    dog =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[11];
    /*poids = (float*)((prom_images_struct*)def_groupe[Gpe].ext)->images_table[13];
       puissance = (float*)((prom_images_struct*)def_groupe[Gpe].ext)->images_table[14]; */

    /*affichage(2,dog,taille_dog,taille_dog,0,0); */

    /*printf("dans function_popout_ptcar : creation somme de gradients\n"); */
    /* creation des sommes de gradiants */
    /*printf("x : %d    y : %d   taille : %d\n",nx,ny,nx*ny);getchar(); */

    for (i = 0; i < n; i++)
    {
        if (grad1[i] > 0)
        {
            som1[i] = grad1[i];
            som2[i] = 0;
            som3[i] = 0;
            som4[i] = grad1[i];
        }
        else
        {
            som1[i] = 0.;
            som2[i] = -grad1[i];
            som3[i] = -grad1[i];
            som4[i] = 0.;
        }

        if (grad2[i] > 0)
        {
            som1[i] += grad2[i];
            som2[i] += grad2[i];
            /*som3[i] += 0.; */
            /*som4[i] += 0.; */
        }
        else
        {
            /*som1[i] += 0.; */
            /*som2[i] += 0.; */
            som3[i] += -grad2[i];
            som4[i] += -grad2[i];
        }
    }

    for (i = 0; i < n; i++)
    {
        if (som1[i] < seuil)   som1[i] = 0.0;
        if (som2[i] < seuil)   som2[i] = 0.0;
        if (som3[i] < seuil)   som3[i] = 0.0;
        if (som4[i] < seuil)   som4[i] = 0.0;
    }

    /* elimination des artefactes du gradiant */
    demi_taille = (int) taille_dog / 2;

    for (i = 0; i < ny; i++)
        for (j = 0; j < nx; j++)
        {
            if (i < demi_taille || i > (ny - demi_taille) || j < demi_taille || j > (nx - demi_taille))
            {
                som1[i * nx + j] = 0.0;
                som2[i * nx + j] = 0.0;
                som3[i * nx + j] = 0.0;
                som4[i * nx + j] = 0.0;

            }
        }

    /*printf("dans function_popout_ptcar : filtrage par la dog\n"); */
    /* on filtre par la dog */
/*  printf(" dog=%f som1=%f ptc1=%f \n",dog[12],som1[12],ptc1[12]);*/
    /* printf("x : %d    y : %d   taille : %d\n",nx,ny,nx*ny);getchar(); */
    popout_conv(som1, dog, ptc1, nx, ny, taille_dog, taille_dog);
    popout_conv(som2, dog, ptc2, nx, ny, taille_dog, taille_dog);
    popout_conv(som3, dog, ptc3, nx, ny, taille_dog, taille_dog);
    popout_conv(som4, dog, ptc4, nx, ny, taille_dog, taille_dog);
    /*ecrire(dog,taille_dog*taille_dog); */
    /*for(i=0;i<n;i++)
       {
       ptc1[i]=som1[i];
       ptc2[i]=som2[i];
       ptc3[i]=som3[i];
       ptc4[i]=som4[i];

       } */
    /* normalisation */
    /*printf("dans function_popout_ptcar : Normalisation\n"); */
    popout_norm(ptc1, nx, ny);
    popout_norm(ptc2, nx, ny);
    popout_norm(ptc3, nx, ny);
    popout_norm(ptc4, nx, ny);

    /* seuillage */
    /*printf("dans function_popout_ptcar : Seuillage\n"); */
    for (i = 0; i < n; i++)
    {
        if (ptc1[i] < seuil)   ptc1[i] = 0.0;
        if (ptc2[i] < seuil)   ptc2[i] = 0.0;
        if (ptc3[i] < seuil)   ptc3[i] = 0.0;
        if (ptc4[i] < seuil)   ptc4[i] = 0.0;
    }

    /* affichage */
    /*affichage(2,ptc1,nx,ny,0,0);
       affichage(2,ptc2,nx,ny,nx+10,0);
       affichage(2,ptc3,nx,ny,0,ny+10);
       affichage(2,ptc4,nx,ny,nx+10,ny+10); */

    /* competition */
    /*printf("dans function_popout_ptcar : Competition\n"); */
    /*popout_competition(ptc1,poids,puissance,som1,nx,ny,taille_poids,taille_poids);
       popout_competition(ptc2,poids,puissance,som2,nx,ny,taille_poids,taille_poids);
       popout_competition(ptc3,poids,puissance,som3,nx,ny,taille_poids,taille_poids);
       popout_competition(ptc4,poids,puissance,som4,nx,ny,taille_poids,taille_poids); */

    /* version sans compet */
    /*for(i=0;i<n;i++)
       {
       som1[i] = ptc1[i];
       som2[i] = ptc2[i];
       som3[i] = ptc3[i];
       som4[i] = ptc4[i];
       } */

    /* version WTA local */
    popout_wta(ptc1, som1, nx, ny, taille_dog, taille_dog);
    popout_wta(ptc2, som2, nx, ny, taille_dog, taille_dog);
    popout_wta(ptc3, som3, nx, ny, taille_dog, taille_dog);
    popout_wta(ptc4, som4, nx, ny, taille_dog, taille_dog);
    /* dans ce cas il est inutil ou presque de faire la normalisation et la competition */

    /* normalisation */
    /*printf("dans function_popout_ptcar : Normalisation\n"); */
    popout_norm(som1, nx, ny);
    popout_norm(som2, nx, ny);
    popout_norm(som3, nx, ny);
    popout_norm(som4, nx, ny);
    /* seuillage */
    /*printf("dans function_popout_ptcar : Seuillage\n"); */

    /* 21/07/2003 Olivier Ledoux mise en commentaire du bloc seuillage */
    /*    for (i = 0; i < n; i++) {
       if (som1[i] < seuil)   som1[i] = 0.0;
       if (som2[i] < seuil)   som2[i] = 0.0;
       if (som3[i] < seuil)   som3[i] = 0.0;
       if (som4[i] < seuil)
       som4[i] = 0.0;
       } */

    /* en sortie nous avons donc ds images_table[0-3] les pts carac apres competition */
    /* ds images_table[4-7] les pts carac avant competition */
    /* ds 8 et 9 les gradiants */

    /* affichage dans la fentre 2 des 4 images som* */
    /*affichage(2,som1,nx,ny,2*(nx+10)+0,0);
       affichage(2,som2,nx,ny,2*(nx+10)+nx+10,0);
       affichage(2,som3,nx,ny,2*(nx+10)+0,ny+10);
       affichage(2,som4,nx,ny,2*(nx+10)+nx+10,ny+10);getchar(); */

    /* affichage fenetre 2, mais les unes apres les autres */
    /*affichage(2,som1,nx,ny,0,0);getchar();
       affichage(2,som2,nx,ny,0,0);getchar();
       affichage(2,som3,nx,ny,0,0);getchar();
       affichage(2,som4,nx,ny,0,0);getchar(); */

#ifdef DEBUG_FILE
    /*intf ("debut groupe f_popout_ptcar %d \n",Gpe); */
    surimage1 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[8];
    surimage2 =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[9];
    sprintf(chemin1, "popout_ptcar8_%3.3d", Gpe);
    sprintf(chemin2, "popout_ptcar9_%3.3d", Gpe);
    sortie1 = fopen(chemin1, "a");
    sortie2 = fopen(chemin2, "a");
    for (pointeurim = 0; pointeurim < nx * ny; pointeurim++)
    {
        fprintf(sortie1, "%f \n", surimage1[pointeurim]);
        fprintf(sortie2, "%f \n", surimage2[pointeurim]);
    }
    fclose(sortie1);
    fclose(sortie2);
#endif

}
