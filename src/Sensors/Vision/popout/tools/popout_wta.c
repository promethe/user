/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>

void popout_wta(float *image, float *res, int x, int y, int dim_wtaX,
                int dim_wtaY)
{
    int i, j, optim1, optim4, k, l, dimX, dimY;
    /* int optim2,optim3; */

    /* verification de la taille */
    dimX = dim_wtaX / 2;
    if ((2 * dimX + 1) != dim_wtaX)
        printf("attention dim_wtaX (%d) n'est pas impair, transforme en %d\n",
               dim_wtaX, 2 * dimX + 1);
    dimY = dim_wtaY / 2;
    if ((2 * dimY + 1) != dim_wtaY)
        printf("attention dim_wtaY (%d) n'est pas impair, transforme en %d\n",
               dim_wtaY, 2 * dimY + 1);


    for (i = 0; i < x; i++)
    {
        for (j = 0; j < y; j++)
        {
            optim1 = i + j * x;
            res[optim1] = image[optim1];    /* initialise la sortie */
            if (image[optim1] > 0.0)
            {
                optim4 = 1;
                for (k = -dimX; k <= dimX; k++)
                {               /* scan autour du optim1 */
                    if (((i + k) >= 0) && ((i + k) < x))
                    {           /* test debordement horizontal */
                        for (l = -dimY; l <= dimY; l++)
                        {
                            if (((j + l) >= 0) && ((j + l) < y))
                            {   /* test debordement vertical */
                                if (image[optim1] < image[optim1 + k + x * l])
                                {

                                    res[optim1] = 0.;
                                    optim4 = 0;
                                }
                            }
                            if (optim4 == 0)
                                break;
                        }
                    }
                    if (optim4 == 0)
                        break;
                }
            }
        }
    }


}
