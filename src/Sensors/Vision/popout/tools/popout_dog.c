/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  popout_dog.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    fonction pour creation du filtre DOG
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float *popout_dog(float sigma1, float sigma2, int taille)
{
    int demitaille, i, j;
    float dist, *res, coef1, coef2, t, a1, dvp, dvn;
    /*  float norm; */

    /* on verifie ses histoires de taille */
    printf("popout_dog have been called with :'%f,%f,%d'", sigma1, sigma2,
           taille);
    demitaille = taille / 2;
    if ((2 * demitaille + 1) != taille)
    {
        printf("dans popout_dog : attention, la taille est paire (= %d)\n",
               taille);
        exit(0);
    }

/* une verification pour le rapport entre sigmaX et taille ? */
    if ((2. * sigma1) > (float) demitaille
        || (2. * sigma2) > (float) demitaille)

        printf
            ("dans popout_dog : attention les sigmas sont trop grands par rapport a la taille\n");

/* on alloue de la memoire */
    res = (float *) malloc(sizeof(float) * taille * taille);
    if (res == NULL)
    {

        printf("dans popout_dog : plus de memoire\n");
        exit(0);
    }

/* creation du filtre */
/*sigma2*=1.1;*/
    coef1 = 1 / (sqrt(2. * 3.14) * sigma1);
    coef2 = 1 / (sqrt(2. * 3.14) * sigma2);
/* sigma1 > sigma2 */
    sigma1 = sigma1 * sigma1;
    sigma2 = sigma2 * sigma2;


/* test */
    dvp = 2.;                   /*2 */
    dvn = 7.;                   /*7 */
    a1 = -dvp * dvp / log(0.5);
    t = dvn - dvp;



    for (i = -demitaille; i <= demitaille; i++)
    {
        for (j = -demitaille; j <= demitaille; j++)
        {
            dist = sqrt((float) (i * i + j * j));
            res[(i + demitaille) * taille + j + demitaille] =
                coef1 * exp(-dist / sigma1) - coef2 * exp(-dist / sigma2);
            /*if (dist < dvp + 1) {
               res[(i + demitaille) * taille + j + demitaille] = -2.0 * (exp(-dist * dist / a1) - 0.5);
               } else {
               if (dist >= dvp)
               res[(i + demitaille) * taille + j + demitaille] = fabs(cos(((dist - dvp) / t * pi) + (pi / 2.)) * ((cos((dist - dvp) / t * (pi / 2.))) / 2.)) / 2.;
               else
               res[(i + demitaille) * taille + j + demitaille] = 0.0;
               } */
        }

    }

    return (res);
}
