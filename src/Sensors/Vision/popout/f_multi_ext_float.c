/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_multi_ext_float.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_multi_ext_float(int Gpe)
{
    int InputGpe1 = -1, InputGpe2 = -1, i, nx, ny, n;
    float *input_tab1, *input_tab2, *output_tab;

    /* 20/10/2003 Olivier Ledoux
       pour pouvoir utiliser d'autres images que celles du bloc 0
     */
    int num1 = -1, num2 = -1;   /* numero de l'image dans chaque bloc */
    char *chaine;
    char retour[256];
    int num, numImage;

    if (def_groupe[Gpe].ext == NULL)
    {

        /* 20/10/2003 Olivier Ledoux
           gestion des liens pour le numero de bloc
         */
        i = 0;
        do
        {
            numImage = -1;
            num = find_input_link(Gpe, i);
            if (num != -1)
            {
                chaine = liaison[num].nom;
                if (prom_getopt(chaine, "-N", retour) == 2)
                    numImage = atoi(retour);
            }
            if (num1 == -1)
                num1 = numImage;
            else if (num2 == -1)
                num2 = numImage;
            i++;
        }
        while (num != -1);
        if (num1 == -1)
            num1 = 0;
        if (num2 == -1)
            num2 = 0;
        /* fin gestion */

/* recuperation des liens entrants */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                if (InputGpe1 == -1)
                {
                    InputGpe1 = liaison[i].depart;
                }
                else
                {
                    if (InputGpe2 == -1)
                    {
                        InputGpe2 = liaison[i].depart;
                    }
                    else
                    {
                        printf
                            ("%s:(groupe %d) : erreur, plus de deux groupes en entree\n",
                             __FUNCTION__, Gpe);
                        exit(0);
                    }
                }
            }
        if (InputGpe1 == -1 || InputGpe2 == -1)
        {
            printf("%s:(groupe %d) : erreur, pas assez de groupes amonts \n",
                   __FUNCTION__, Gpe);
            exit(0);
        }


        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s(gpe:%d) : erreur, ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, Gpe);
            exit(0);
        }


        /* recuperation du pointeur sur les images d'entree */
        /* stocke dans images_table[1] et [2] */

        /* 20/10/2003 Olivier Ledoux
           pour faire varier le numero de bloc
         */
        /*((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
           ((prom_images_struct *) def_groupe[InputGpe1].ext)->
           images_table[0];
           ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
           ((prom_images_struct *) def_groupe[InputGpe2].ext)->
           images_table[0];
         */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[InputGpe1].ext)->
            images_table[num1];
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            ((prom_images_struct *) def_groupe[InputGpe2].ext)->
            images_table[num2];

        /* recuperation des infos des groupes precedents */
        nx = ((prom_images_struct *) def_groupe[InputGpe1].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe1].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe1].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("%s : votre image est couleur, cette fonction ne fait que le N&B\n",
                 __FUNCTION__);
            exit(0);
        }

        /* verification du deuxieme groupe */
        if (nx != ((prom_images_struct *) def_groupe[InputGpe2].ext)->sx
            || ny != ((prom_images_struct *) def_groupe[InputGpe2].ext)->sy)
        {
            printf
                ("%s:(groupe %d) : erreur, tailles des images differentes (groupe %d : %dx%d) et (groupe %d : %dx%d)\n",
                 __FUNCTION__, Gpe, InputGpe1, nx, ny, InputGpe2,
                 ((prom_images_struct *) def_groupe[InputGpe2].ext)->sx,
                 ((prom_images_struct *) def_groupe[InputGpe2].ext)->sx);
            exit(0);
        }
#ifdef DEBUG
        printf
            ("%s(groupe %d): groupes en entree : %d et %d, taille image : %dx%d\n",
             __FUNCTION__, Gpe, InputGpe1, InputGpe2, nx, ny);
#endif
        /* alloc de mem pour la sortie */

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(nx * ny * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /* on a une image */
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    }

    input_tab1 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    input_tab2 =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[2];
    output_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];

    n = nx * ny;
    for (i = 0; i < n; i++)
    {
        if ((input_tab1[i] > 0.0) && (input_tab2[i] > 0.0))
            output_tab[i] = input_tab1[i] * input_tab2[i];
        else
            output_tab[i] = 0.0;
    }
#ifdef DEBUG
#ifndef AVEUGLE
    affichage(2, output_tab, nx, ny, 0, 0);
    getchar();
#endif
#endif
}
