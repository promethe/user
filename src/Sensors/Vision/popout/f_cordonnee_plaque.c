/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief Recuperation des cordonnees de la plaque a partir du fichier camion_n.txt 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
    Recuperation des cordonnees de la plaque a partir du fichier camion_n.txt
Macro:
-none 

Local variables:
-char jice_nom_image [256]

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

#include "tools/include/local_var.h"

void function_cordonnee_plaque(int Gpe)
{
    char chaine_2[80];
    int xtg, ytg, xtd, ytd, xbg, ybg, xbd, ybd;
    FILE *jice_txt;
    int ret;

#ifdef DEBUG
    printf("dans function_cordonnee_plaque\n");
#endif
    jice_txt = fopen(jice_nom_image, "r");
    /*
       02/10/2003 Olivier Ledoux
       les coordonnees des fichiers d'Issam sont de la forme (y,x) et non (x,y)
     */

    /*
       fscanf(jice_txt,"%s %s\n",chaine_2,chaine_2);
       fscanf(jice_txt,"%s %d %d\n",chaine_2,&xtg,&ytg);
       fscanf(jice_txt,"%s %d %d\n",chaine_2,&xtd,&ytd);
       fscanf(jice_txt,"%s %d %d\n",chaine_2,&xbg,&ybg);
       fscanf(jice_txt,"%s %d %d\n",chaine_2,&xbd,&ybd);
     */
    ret=fscanf(jice_txt, "%s %s\n", chaine_2, chaine_2);
    ret=fscanf(jice_txt, "%s %d %d\n", chaine_2, &ytg, &xtg);
    ret=fscanf(jice_txt, "%s %d %d\n", chaine_2, &ytd, &xtd);
    ret=fscanf(jice_txt, "%s %d %d\n", chaine_2, &ybg, &xbg);
    ret=fscanf(jice_txt, "%s %d %d\n", chaine_2, &ybd, &xbd);

    fclose(jice_txt);
    neurone[def_groupe[Gpe].premier_ele].s = (float) xtg;
    neurone[def_groupe[Gpe].premier_ele + 1].s = (float) ytg;
    neurone[def_groupe[Gpe].premier_ele + 2].s = (float) xtd;
    neurone[def_groupe[Gpe].premier_ele + 3].s = (float) ytd;
    neurone[def_groupe[Gpe].premier_ele + 4].s = (float) xbg;
    neurone[def_groupe[Gpe].premier_ele + 5].s = (float) ybg;
    neurone[def_groupe[Gpe].premier_ele + 6].s = (float) xbd;
    neurone[def_groupe[Gpe].premier_ele + 7].s = (float) ybd;
#ifdef DEBUG
    printf("je sors de function_cordonnee_plaque\n");
#endif
    /*
       for(i=0;i<8;i++)
       printf("neurone[def_groupe[%d].premier_ele].s+%d].s=%f\n",Gpe,i,neurone[def_groupe[Gpe].premier_ele+i].s);
     */
}
