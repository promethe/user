/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_modulation2.c 
\brief 

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
void function_modulation2(int Gpe)
{
    int i, n, nx, ny, InputGpe[2], mask, j, Input = 0, neuro = 0;
    float *entree, *sortie, centre, seuil;
    char *ST, *chaine[2];


    if (def_groupe[Gpe].ext == NULL)
    {
        j = 0;
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                if (j > 1)
                {
                    printf("%s :trop de liens en entree\n", __FUNCTION__);
                    exit(0);
                }
                chaine[j] = liaison[i].nom;
                InputGpe[j] = liaison[i].depart;
                j++;
            }
        if (j != 2)
        {
            printf("%s : il faut 2 liens\n", __FUNCTION__);
            exit(0);
        }

        /* on a les groupes amonts */
        /* on determine qui est qui */
        ST = NULL;
        mask = -1;
        ST = strstr(chaine[0], "entree");
        if (ST != NULL)
        {
            Input = InputGpe[0];
        }
        else
        {
            ST = strstr(chaine[0], "N");
            if (ST != NULL)
                mask = InputGpe[0];
            neuro = atoi(&ST[1]);
        }
        ST = NULL;
        ST = strstr(chaine[1], "entree");
        if (ST != NULL)
        {
            Input = InputGpe[1];
        }
        else
        {
            ST = strstr(chaine[1], "N");
            if (ST != NULL)
                mask = InputGpe[1];
            neuro = atoi(&ST[1]);
        }
        if (mask == -1)
        {
            printf
                ("%s : verifier les liens, il faut un lien 'entree' et un 'Nx'\n",
                 __FUNCTION__);
            exit(0);
        }


        /* de la mem */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }

        /*printf("dans function_popout_bipoint2 : recup info\n"); */
        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[Input].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[Input].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[Input].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("%s : votre image est couleur, cette fonction ne fait que le N&B\n",
                 __FUNCTION__);
            exit(0);
        }

        /* alloc de mem pour images calcul et resultat */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(n * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        /* recup et sauvegarde des donnees */
        neurone[def_groupe[Gpe].premier_ele].s =
            (float) def_groupe[mask].premier_ele + neuro;
        printf("%s(%d): lit neurone : %d sur groupe %d\n", __FUNCTION__,
               Gpe, neuro, mask);

        /* pointeur sur entree */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) ((prom_images_struct *) def_groupe[Input].
                               ext)->images_table[0];
    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        n = nx * ny;
    }



    centre = neurone[(int) neurone[def_groupe[Gpe].premier_ele].s].s;
    seuil = neurone[(int) neurone[def_groupe[Gpe].premier_ele].s + 24].s;   /* for new function */

    printf("%s(%d): lit neurone : %d => centre=%1.2f, seuil=%1.2f\n",
           __FUNCTION__, Gpe, (int) neurone[def_groupe[Gpe].premier_ele].s,
           centre, seuil);
    sortie =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    entree =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    /* 08/10/2003 Olivier Ledoux
       adaptation par rapport au code de JC
       reactivation le 01/12/2003 Olivier Ledoux
     */

    for (i = 0; i < n; i++)
    {
        /*sortie[i] =    1. *  (1. - fabsf(entree[i] - centre)); */
        if (entree[i] > 0.01)
            sortie[i] = 1. * (1. - fabsf(entree[i] - seuil));
        else
            sortie[i] = 0.0;
    }


/* desactivation du 01/12/2003 Olivier Ledoux */
/*    for (i = 0; i < n; i++)
      {
	if(entree[i] > 0.01)
	  {
	    sortie[i] =  (1. - seuil) * (1.-fabsf(entree[i] - centre));
	  }
	else
	  {
	    sortie[i] = 0.0;
	  }
      }
*/
}
