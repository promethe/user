/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_demande_numero.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

- author: AM.Tousch
- description: si la liste est une liste de nom de fichiers (images), d�ermine le num�o de la classe �partir du nom du fichier.
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-utilise la variable jice_nom_image de Vision/Image_IO/tools/include/local_var.h (lien avec f_load_baccon...)

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

Todo:
- Revoir le test des options d'entr� : dans le cas d'une liste d'image, ouvre le fichier inutilement
- see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>


#ifdef USE_THREADS
#include <pthread.h>
#endif

#include "tools/include/local_var.h"

#define NOTHING   0
#define NUMBERS   1
#define FILENAMES 2

#define NEUTRE    1
#define TRISTESSE 0
#define JOIE      2
#define COLERE    3
#define SURPRISE  4


typedef struct
{
    FILE *f;
    int continuer;
    int file_is_list_of;        /* type de la liste : NUMBERS ou FILENAMES */
    char *file;
    int demande_utilisateur;
} file_data;

#ifdef USE_THREADS
extern pthread_mutex_t mutex_lecture_clavier;
#endif

void function_demande_numero(int numero)
{
    int deb, longueur;
    int i, x;
    int nlink;
    char param_link[255], string[256];
    FILE *f;
    /* Ajout 24/02/2006 */
    int expression;
    char nom_image[256];
    char chaine[256];

    file_data *data = NULL;
    longueur = def_groupe[numero].nbre;

    /*get the string of the input link */
    nlink = find_input_link(numero, 0);

#ifdef DEBUG
    printf("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
#endif
    deb = def_groupe[numero].premier_ele;

    data = def_groupe[numero].data;
    /*s'il y a un lien en entree */
    if (nlink > -1)
    {
        /*debut initialisation */
        if (data == NULL && neurone[deb].flag < 1)
        {
            /*recherche si le lien contient un nom de fichier */
            if (prom_getopt(liaison[nlink].nom, "-f", param_link) == 2)
            {                   /*si oui, tente de l'ouvrir */
				sprintf(string, "%s", param_link);
                f = fopen(string, "r");
                if (f == NULL)
                {
                    printf("Cannot open input file: %s\n", string);
                    exit(-1);
                }
                else
                {
#ifdef DEBUG
                    printf("fichier %s ouvert avec succes\n", string);
#endif
                    /*on cree la structure qui contiendra le pointeur vers ce fichier et on init */
                    data = (file_data *) malloc(sizeof(file_data));
                    data->f = f;
                    neurone[deb].flag = -1;
                    data->file = (char *) malloc(255);
                    strcpy(data->file, string);
                    /* Ajout AMTousch 24/02/2006 */
                    /* on teste si on lit une liste de numeros ou une liste d'images */
                    /* pour une liste d'image, a utiliser en lien avec f_load_baccon */
                    data->file_is_list_of = NOTHING;
                    if (strstr(liaison[nlink].nom, "listimage") != NULL)
                        data->file_is_list_of = FILENAMES;
                    else
                        data->file_is_list_of = NUMBERS;
					if (data == NULL)
                    {
                        printf("Erreur: impossible d'allouer la structure data_file du groupe:%d\n", numero);
                        exit(-1);
                    }
                    def_groupe[numero].data = data;
                }
            }
            else
                dprints("No input file found on %d\n", numero);
        }

    }
    /*si l'on utilise un fichier comme entree: */
    if (data != NULL)
    {
        switch (data->file_is_list_of)
        {
        case FILENAMES:
            /* On recupere le derniere image traitee par f_load_baccon dans local_var.h sous le nom jice_nom_image */
            /* A Ameliorer : passage par def_groupe.data en sortie du load baccon */
            strcpy(nom_image, jice_nom_image);  /*recuperation dans local_var.h */
            /* On cherche dans le nom du fichier le nom de l'expression representee par l'image. */
            /* en anglais : base de Yales, colere non representee remplacee par sleepy */
            if ((strstr(nom_image, "neutre") != NULL)
                || (strstr(nom_image, "normal") != NULL)  
                || (strstr(nom_image, "voyN") !=NULL) )
                expression = NEUTRE;
            else if ((strstr(nom_image, "triste") != NULL)
                     || (strstr(nom_image, "sad") != NULL)
                     || (strstr(nom_image, "voyI") != NULL))
                expression = TRISTESSE;
            else if ((strstr(nom_image, "joie") != NULL)
                     || (strstr(nom_image, "happy") != NULL)
                     || (strstr(nom_image, "voyA") != NULL))
                expression = JOIE;
            else if ((strstr(nom_image, "colere") != NULL)
                     || (strstr(nom_image, "sleepy") != NULL)
                     || (strstr(nom_image, "voyO") != NULL))
                expression = COLERE;
            else if (strstr(nom_image, "surprise") != NULL)
                expression = SURPRISE;
            else
            {
                dprints ("Attention : l'expression n'est pas reconnue dans le nom du fichier %s !\n", nom_image);
                exit(-1);
            }
#ifdef DEBUG
            dprints("fichier %s - expression lue : %d\n", nom_image,expression);
#endif
            x = expression;

            break;

        case NUMBERS:
            if (strcmp(liaison[nlink].nom, "") != 0
                && strcmp(liaison[nlink].nom, ".") != 0
                && strcmp(liaison[nlink].nom, "?") != 0)
                dprints("%s\n", liaison[nlink].nom);
            f = data->f;
            if (neurone[deb].flag < 1)
            {
                if (f != NULL)
                {
                    /*on lit son contenu pour obtenir l'index du neurone a mettre a 1 */
                    if (fscanf(f, "%d", &x) == EOF)
                    {
                        cprints("Plus d'entree a lire: %d \n", numero);
                        cprints ("Voulez vous: \n 1) continuer a saisir manuellement\n 2)n'affecter aucune sortie (-1)\n 3) relancer la lecture depuis le debut \n 4) quitter\n");
                        cscans("%d", &i);
                        if (i == 1)
                        {
                            data = NULL;
                            def_groupe[numero].data = data;
                            do
                            {
                                cprints("Number of the neuron to activate:  [0 %d] (-1 means no activity)", longueur - 1);
                                cscans("%d", &x);
                            } while (x >= longueur);
                            neurone[deb].flag = 1;
                        }
                        else if (i == 2)
                        {
                            neurone[deb].flag = 1;
                            x = -1;
                        }
                        else if (i == 3)
                        {
                            fclose(f);
                            data->f = fopen(data->file, "r");
                            f = data->f;
                            if(fscanf(f, "%d", &x) == -1) printf("erreur lors de la lecture du chiffre dans %d\n", numero);
                        }
                        else exit(-1);
                    }
                    else cprints("Number of the neurone activated :%d on group: %d \n", x, numero);
                }
            }
            else
                x = -1;
            break;
        case NOTHING:
            printf("type de liste du fichier non reconnue\n");
            break;
        }
    }

    else                        /*sinon saisie par l'utilisateur */
    {
#ifdef USE_THREADS
	pthread_mutex_lock(&mutex_lecture_clavier);
#endif
#ifdef DEBUG
	printf("on a pas vu de liste sur le liens => lecture clavier\n");
#endif

      if(neurone[deb].flag==0) do
        {
            cprints("Function_ask_number(%s)\n", def_groupe[numero].no_name);
            fflush(stdout);
            if (strcmp(liaison[nlink].nom, "") != 0 && strcmp(liaison[nlink].nom, ".") != 0  && strcmp(liaison[nlink].nom, "?") != 0)
                cprints("%s\n", liaison[nlink].nom);


            cprints ("Number of the neurone to activate:  [0 %d] (-1 means no activity)",longueur - 1);
            /* scanf("%d", &x);*/
	    cscans("%s",chaine);
	    if(chaine[0]=='x' || chaine[0]=='f') {x=-1;neurone[deb].flag=1;}
	    else x=atoi(chaine);
        }
        while (x >= longueur);
#ifdef USE_THREADS
	pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
    }
    for (i = deb; i < deb + longueur; i++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    }
    if (x >= 0)
        neurone[x + deb].s = neurone[x + deb].s1 = neurone[x + deb].s2 = 1.;
#ifdef DEBUG
    dprints("===========sortie de %s\n", __FUNCTION__);
#endif
}
