/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_ecriture_des_focalisations.c
\brief   ecriture des focalisations et activites sur cartes dans le fichier camion_n_res.txt

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
     ecriture des focalisations et activites sur cartes dans le fichier camion_n_res.txt

Macro:
-none

Local variables:
-char jice_nom_image[256]

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include "tools/include/local_var.h"

void function_ecriture_des_focalisations(int Gpe)
{
   FILE *fichier_resultat;
   int i;
   int ind = 0;
   static int GpePlaque, GpeFoc;
   int InputGpe, x, y;
   char *c, *chaine;
   /* 03/10/2003 Olivier Ledoux
      augmentation du nombre max de carte
    */
   /*
      float MaxMap[24];
    */
   float MaxMap[28];
   char nom_resultat[128];

   i = 127;
   while (jice_nom_image[i] != '/' && i >= 0)
   {
      i--;
   }
   i++;
   while (jice_nom_image[i] != '.' && i < (int) strlen(jice_nom_image))
   {
      nom_resultat[ind] = jice_nom_image[i];
      i++;
      ind++;
   }
   nom_resultat[ind] = '\0';
   /*printf("avant strcat '%s'\n",nom_resultat); */
   strcat(nom_resultat, "_res.txt");

   if (def_groupe[Gpe].ext == NULL)
   {
      for (i = 0; i < nbre_liaison; i++)
         if (liaison[i].arrivee == Gpe)
         {
            InputGpe = liaison[i].depart;
            chaine = liaison[i].nom;

            if (strstr(chaine, "plaque"))
            {
               GpePlaque = InputGpe;
            }
            else if (strstr(chaine, "focus"))
            {
               GpeFoc = InputGpe;
            }
         }
   }
   /*  def_groupe[Gpe].ext=(char*)"fin"; */

   /* 03/10/2003 Olivier Ledoux
      Adaptation du nombre de cartes
    */
   /*
      for(i=0;i<24;i++)
    */

   for (i = 0; i < def_groupe[GpeFoc].nbre; i++)
   {
      if (neurone[def_groupe[GpeFoc].premier_ele + i].s < 0.0)  MaxMap[i] = -neurone[def_groupe[GpeFoc].premier_ele + i].s;
      else   MaxMap[i] = neurone[def_groupe[GpeFoc].premier_ele + i].s;
      /*   printf(" MaxMap[%d]=%f\n",i,MaxMap[i]);getchar(); */
   }

   c = (char *) def_groupe[GpePlaque].ext;
   x = (int) neurone[def_groupe[GpePlaque].premier_ele].s;
   y = (int) neurone[def_groupe[GpePlaque].premier_ele + 1].s;
   /*printf("c=%c x=%d y=%d\n",c,x,y); */
   /*getchar(); */
   fichier_resultat = fopen(nom_resultat, "a");
   if (fichier_resultat == NULL)
   {
      EXIT_ON_ERROR("impossible d'ouvrir '%s'\n", nom_resultat);
   }

   if (c[0] == 'o')
   {
      printf("sur cible\n");
      fprintf(fichier_resultat, "%d-%d-1-", x, y);
   }
   else
   {
      fprintf(fichier_resultat, "%d-%d-0-", x, y);
   }

   /* 03/10/2003 Olivier Ledoux
      adaptation du nombre de cartes
    */

   /*  for(i=0;i<24;i++)  */

   for (i = 0; i < def_groupe[GpeFoc].nbre; i++)
   {
      if (MaxMap[i] > 0.000000000) fprintf(fichier_resultat, "%d(%f)", i, MaxMap[i]);
   }

   fprintf(fichier_resultat, "\n");
   fclose(fichier_resultat);
}
