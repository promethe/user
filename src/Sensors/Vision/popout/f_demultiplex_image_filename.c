/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_demultiplex_image_filename f_demultiplex_image_filename
\ingroup libSensors 

\brief 

\section Description
modification de f_demande_numero pour les primitives et les intensites. remplacement des dprints par printf.
a partir du nom de l'image ouverte par f_load_baccon (groupe precedent) cette fonction active un de ses neurones.
  -chaque ligne correspond a une primitive motrice
  -chaque colonne correspond a une intensite

\section Comments
-utilise la variable jice_nom_image de Vision/Image_IO/tools/include/local_var.h (lien avec f_load_baccon...)

\author Benjamin Blanchard
\date 28/04/2010


 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>


#ifdef USE_THREADS
#include <pthread.h>
#endif

#include "tools/include/local_var.h"

#define NOTHING   0
#define NUMBERS   1
#define FILENAMES 2

#define SOURCILS  0
#define BOUCHE    1
#define SOURIRE   2
#define EMOSIMPLE 3

#define NB_INTENSITES 3


typedef struct
{
	FILE *f;
	int continuer;
	int file_is_list_of;        /* type de la liste : NUMBERS ou FILENAMES */
	char *file;
	int demande_utilisateur;
} file_data;

#ifdef USE_THREADS
extern pthread_mutex_t mutex_lecture_clavier;
#endif

void function_demultiplex_image_filename(int numero)
{
	int deb, longueur;
	int i, x;
	int nlink;
	char param_link[255];
	FILE *f;
	/* Ajout 24/02/2006 */
	int primitive;
	int intensite;
	char nom_image[256];
	char chaine[256];

	file_data *data = NULL;
	longueur = def_groupe[numero].nbre;

	/*get the string of the input link */
	nlink = find_input_link(numero, 0);

#ifdef DEBUG
	printf("~~~~~~~~~~~entree dans %s\n", __FUNCTION__);
#endif
	deb = def_groupe[numero].premier_ele;


	data = def_groupe[numero].data;
	/*s'il y a un lien en entree */
	if (nlink > -1)
	{
		/*debut initialisation */
		if (data == NULL && neurone[deb].flag < 1)
		{
			/*recherche si le lien contient un nom de fichier */
			if (prom_getopt(liaison[nlink].nom, "-f", param_link) == 2)
			{                   /*si oui, tente de l'ouvrir */
				f = fopen(param_link, "r");
				if (f == NULL)
				{
					EXIT_ON_ERROR("Cannot open input file: %s", param_link);
				}
				else
				{
#ifdef DEBUG
					printf("fichier %s ouvert avec succes\n", param_link);
#endif
					/*on cree la structure qui contiendra le pointeur vers ce fichier et on init */
					data = (file_data *) malloc(sizeof(file_data));
					if (data == NULL)
					{
						printf
						("Erreur: impossible d'allouer la structure data_file du groupe:%d\n",
								numero);
						exit(-1);
					}
					data->f = f;
					neurone[deb].flag = -1;
					data->file = (char *) malloc(255);
					strcpy(data->file, param_link);

					/* Ajout AMTousch 24/02/2006 */
					/* on teste si on lit une liste de numeros ou une liste d'images */
					/* pour une liste d'image, a utiliser en lien avec f_load_baccon */
					data->file_is_list_of = NOTHING;
					if (strstr(liaison[nlink].nom, "listimage") != NULL)
						data->file_is_list_of = FILENAMES;
					else
						data->file_is_list_of = NUMBERS;

					def_groupe[numero].data = data;
				}
			}
			else
				printf("No input file found on %d\n", numero);
		}

	}
	/*si l'on utilise un fichier comme entree: */
	if (data != NULL)
	{

		switch (data->file_is_list_of)
		{
		case FILENAMES:
			/* On recupere le derniere image traitee par f_load_baccon dans local_var.h sous le nom jice_nom_image */
			/* A Ameliorer : passage par def_groupe.data en sortie du load baccon */
			strcpy(nom_image, jice_nom_image);  /*recuperation dans local_var.h */
			/* On cherche dans le nom du fichier le nom de la primitive representee par l'image. */

			if (strstr(nom_image, "Sourcils") != NULL || strstr(nom_image, "orientation"))
				primitive = SOURCILS;
			else if (strstr(nom_image, "Bouche") != NULL)
				primitive = BOUCHE;
			else if (strstr(nom_image, "Sourire") != NULL)
				primitive = SOURIRE;            
			else if (strstr(nom_image, "EmoSimple") != NULL)
				primitive = EMOSIMPLE;            
			else
			{
				printf
				("Attention : la primitive n'est pas reconnue dans le nom du fichier %s !\n",
						nom_image);
				primitive = -1;
				/*exit(-1);*/
			}
			/* On cherche dans le nom du fichier le nom de l'intensite representee par l'image. */
			if (strstr(nom_image, "level0") != NULL)
				intensite = 0;
			else if (strstr(nom_image, "level1") != NULL)
				intensite = 1;
			else if (strstr(nom_image, "level2") != NULL)
				intensite = 2;            
			else
			{
				printf
				("Attention : le level (intensite) n'est pas reconnu dans le nom du fichier %s !\n",
						nom_image);
				intensite = -1;
				/*exit(-1);*/
			}

#ifdef DEBUG
			printf("fichier %s - primitive lue : %d - intensite lue : %d\n", nom_image,
					primitive, intensite);
#endif
			if (primitive <0 || intensite<0)
				x = -1;
			else
				x = primitive * NB_INTENSITES + intensite;

			break;

		case NUMBERS:
			if (strcmp(liaison[nlink].nom, "") != 0
					&& strcmp(liaison[nlink].nom, ".") != 0
					&& strcmp(liaison[nlink].nom, "?") != 0)
				dprints("%s\n", liaison[nlink].nom);
			f = data->f;
			if (neurone[deb].flag < 1)
			{
				if (f != NULL)
				{
					/*on lit son contenu pour obtenir l'index du neurone a mettre a 1 */
					if (fscanf(f, "%d", &x) == EOF)
					{
						cprints("Plus d'entree a lire: %d \n", numero);
						cprints
						("Voulez vous: \n 1) continuer a saisir manuellement\n 2)n'affecter aucune sortie (-1)\n 3) relancer la lecture depuis le debut \n 4) quitter\n");
						cscans("%d", &i);
						if (i == 1)
						{
							data = NULL;
							def_groupe[numero].data = data;
							do
							{
								cprints
								("Number of the neuron to activate:  [0 %d] (-1 means no activity)",
										longueur - 1);
								cscans("%d", &x);
							} while (x >= longueur);
							neurone[deb].flag = 1;
						}
						else if (i == 2)
						{
							neurone[deb].flag = 1;
							x = -1;
						}
						else if (i == 3)
						{
							fclose(f);
							data->f = fopen(data->file, "r");
							f = data->f;
							if(fscanf(f, "%d", &x) != 1)
                EXIT_ON_ERROR("impossible de lire un entier dans %s", data->file);
						}
						else
							exit(-1);
					}
					else
						cprints
						("Number of the neurone activated :%d on group: %d \n",
								x, numero);
				}
			}
			else
				x = -1;
			break;
		case NOTHING:
			printf("type de liste du fichier non reconnue\n");
			break;
		}
	}

	else                        /*sinon saisie par l'utilisateur */
	{
#ifdef USE_THREADS
		pthread_mutex_lock(&mutex_lecture_clavier);
#endif
		if(neurone[deb].flag==0) do
		{
			cprints("Function_ask_number(%d)\n", numero);
			if (strcmp(liaison[nlink].nom, "") != 0
					&& strcmp(liaison[nlink].nom, ".") != 0
					&& strcmp(liaison[nlink].nom, "?") != 0)
				cprints("%s\n", liaison[nlink].nom);


			cprints
			("Number of the neurone to activate:  [0 %d] (-1 means no activity)",
					longueur - 1);
			/* scanf("%d", &x);*/
			cscans("%s",chaine);
			if(chaine[0]=='x' || chaine[0]=='f') {x=-1;neurone[deb].flag=1;}
			else x=atoi(chaine);
		}
		while (x >= longueur);
#ifdef USE_THREADS
		pthread_mutex_unlock(&mutex_lecture_clavier);
#endif
	}
	for (i = deb; i < deb + longueur; i++)
	{
		neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
	}
	if (x >= 0)
		neurone[x + deb].s = neurone[x + deb].s1 = neurone[x + deb].s2 = 1.;
#ifdef DEBUG
	printf("===========sortie de %s\n", __FUNCTION__);
#endif
}
