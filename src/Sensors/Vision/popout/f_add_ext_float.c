/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_add_ext_float.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>

void function_add_ext_float(int Gpe)
{
    int InputGpe, NbrInputGpe = 0, i, j, nx = 0, ny = 0;
    float *output_tab, *para;
    char *chaine, *ST;

    /* rajout 21/05/2003 Olivier Ledoux
       - peut maintenant recuperer les poids d'une boite de neurone
       - peut recuperer un biais d'une boite de neurone
     */

    typedef struct
    {
        int gpe_bias;           /* numero du groupe contenant le biais */
        int gpe_weight;         /* numero du groupe contenant les poids */
        int num_bias;           /* numero du neurone contenant le biais */
    } DataStruct;

#ifdef DEBUG_FILE
    FILE *sortie;
    int pointeurim;
    char chemin[128];
    float *surimage;
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : erreur, ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }
        /* recuperation des liens entrants */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                /* test supplementaire car posibilite d'avoir au moins deux liens supplementaires
                   21/05/2003 Olivier Ledoux */
                chaine = liaison[i].nom;
                ST = NULL;
                ST = strstr(liaison[i].nom, "-w");
                if (ST != NULL)
                    NbrInputGpe++;
            }
        /* okay on a NbrInputGpe liens */
        if (NbrInputGpe < 2)
        {
            printf("%s(groupe %d) : erreur, il n'y a que %d\n", __FUNCTION__,
                   Gpe, NbrInputGpe);
            exit(0);
        }
        /* on alloue un tableau pour les donnees */
        para = (float *) malloc(sizeof(float) * NbrInputGpe);
        if (para == NULL)
        {
            printf("%s:%d : erreur, ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }
        /* on alloue la structure pour notre variation 21/05/2003 Olivier Ledoux */
        def_groupe[Gpe].data = (void *) malloc(sizeof(DataStruct));
        if ((prom_images_struct *) def_groupe[Gpe].data == NULL)    /* pb de memoire */
        {
            printf("%s line:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
        /* on initialise a des valeurs  par defaut notre structure 21/05/2003 Olivier Ledoux */
        ((DataStruct *) def_groupe[Gpe].data)->gpe_bias = 0;
        ((DataStruct *) def_groupe[Gpe].data)->gpe_weight = 0;
        ((DataStruct *) def_groupe[Gpe].data)->num_bias = 0;

        NbrInputGpe = 0;
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                /* recuperation du parametre sur le lien */
                chaine = liaison[i].nom;
                ST = NULL;
                ST = strstr(chaine, "-w");
                if (ST != NULL)
                {
                    NbrInputGpe++;
                    para[NbrInputGpe - 1] = atof(&ST[2]);
                    /* recuperation de l'image */
                    if (((prom_images_struct *) def_groupe[InputGpe].ext)->
                        images_table[0] != NULL)
                        ((prom_images_struct *) def_groupe[Gpe].ext)->
                            images_table[NbrInputGpe] =
                            ((prom_images_struct *) def_groupe[InputGpe].
                             ext)->images_table[0];
                    else
                    {
                        printf
                            ("%s:(groupe %d) : groupe %d en entree avec ext NULL\n",
                             __FUNCTION__, Gpe, InputGpe);
                        exit(0);
                    }

                    if ((nx == 0) && (ny == 0)) /* premiere image en entree */
                    {
                        nx = ((prom_images_struct *) def_groupe[InputGpe].
                              ext)->sx;
                        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
                        ny = ((prom_images_struct *) def_groupe[InputGpe].
                              ext)->sy;
                        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
                        ((prom_images_struct *) def_groupe[Gpe].ext)->
                            nb_band =
                            ((prom_images_struct *) def_groupe[InputGpe].
                             ext)->nb_band;
                        if (((prom_images_struct *) def_groupe[Gpe].ext)->
                            nb_band != 4)
                        {
                            printf
                                ("%s : votre image est couleur, cette fonction ne fait que le N&B\n",
                                 __FUNCTION__);
                            exit(0);
                        }
                    }
                    else
                    {
                        /* verification des autres groupes */
                        if (nx !=
                            ((prom_images_struct *) def_groupe[InputGpe].
                             ext)->sx
                            || ny !=
                            ((prom_images_struct *) def_groupe[InputGpe].
                             ext)->sy)
                        {
                            printf
                                ("%s:(groupe %d) : erreur, tailles des images differentes (nx=%d,ny=%d) et pour le groupe %d : %d,%d\n",
                                 __FUNCTION__, Gpe, nx, ny, InputGpe,
                                 ((prom_images_struct *) def_groupe[InputGpe].
                                  ext)->sx,
                                 ((prom_images_struct *) def_groupe[InputGpe].
                                  ext)->sy);
                            exit(0);
                        }
                    }
                }
                else            /* cas lien vers le biais 21/05/2003 Olivier Ledoux */
                {
                    ST = NULL;
                    ST = strstr(chaine, "-b");
                    if (ST != NULL)
                    {
                        if (((DataStruct *) def_groupe[Gpe].data)->gpe_bias !=
                            0)
                        {
                            printf
                                ("%s:(groupe %d) : erreur, il y a plusieurs lien vers une boite biais",
                                 __FUNCTION__, Gpe);
                            exit(0);
                        }
                        else
                        {
                            ((DataStruct *) def_groupe[Gpe].data)->gpe_bias =
                                InputGpe;
                            ((DataStruct *) def_groupe[Gpe].data)->num_bias =
                                atoi(&ST[2]);
                        }
                    }
                    else        /* cas lien vers les poids 21/05/2003 Olivier Ledoux */
                    {
                        ST = NULL;
                        ST = strstr(chaine, "poids");
                        if (ST != NULL)
                        {
                            if (((DataStruct *) def_groupe[Gpe].data)->
                                gpe_weight != 0)
                            {
                                printf
                                    ("%s:(groupe %d) : erreur, il y a plusieurs lien vers une boite poids",
                                     __FUNCTION__, Gpe);
                            }
                            else
                            {
                                ((DataStruct *) def_groupe[Gpe].data)->
                                    gpe_weight = InputGpe;
                            }
                        }
                        else
                        {
                            printf
                                ("%s:(groupe %d) : erreur, il manque un parametre sur le lien avec %d\n",
                                 __FUNCTION__, Gpe, InputGpe);
                            exit(0);
                        }
                    }
                }
            }
        /* alloc de mem pour la sortie */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(nx * ny * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number =
            NbrInputGpe + 1;
        ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[NbrInputGpe + 1] = (unsigned char *) para;
        /* modification des infos du tableau para si on a une boite de poids
           21/05/2003 Olivier Ledoux
         */
        if (((DataStruct *) def_groupe[Gpe].data)->gpe_weight != 0)
        {
            /* on va remplacer les numeros de neurones par leur contenu */
            for (i = 0; i < NbrInputGpe; i++)
            {
                para[i] =
                    neurone[(int) (para[i]) +
                            def_groupe[((DataStruct *) def_groupe[Gpe].data)->
                                       gpe_weight].premier_ele].s;
                /* doit on prendre s, s1 ou s2 ici? sachant que pour le moment f_load_mask y stocke la meme chose
                   22/05/2003 Olivier Ledoux
                 */
            }
        }
    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        NbrInputGpe =
            ((prom_images_struct *) def_groupe[Gpe].ext)->image_number - 1;
        para =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[NbrInputGpe + 1];
    }
    output_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    /* sommation */
    for (i = 0; i < nx * ny; i++)
    {
        /* cas d'un biais
           22/05/2003 Olivier Ledoux
         */
        if (((DataStruct *) def_groupe[Gpe].data)->gpe_bias != 0)
        {
            output_tab[i] =
                neurone[((DataStruct *) def_groupe[Gpe].data)->num_bias +
                        def_groupe[((DataStruct *) def_groupe[Gpe].data)->
                                   gpe_bias].premier_ele].s;
            /* doit on prendre s, s1 ou s2 ici? sacahant que pour le moment f_load_mask y stocke la meme chose
               22/05/2003 Olivier Ledoux
             */
        }
        else
        {
            output_tab[i] = 0.0;
        }
        for (j = 0; j < NbrInputGpe; j++)
            output_tab[i] +=
                para[j] *
                (float) ((float *) ((prom_images_struct *) def_groupe[Gpe].
                                    ext)->images_table[j + 1])[i];
    }

#ifdef DEBUG
#ifndef AVEUGLE
    affichage2(2, output_tab, nx, ny, 0, 0);
    getchar();
#endif
#endif
    /* 28/07/2003 Olivier Ledoux */
#ifdef DEBUG_FILE
    /*printf ("debut groupe %d \n",Gpe); */
    surimage =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    sprintf(chemin, "add_ext_float_%3.3d", Gpe);
    sortie = fopen(chemin, "a");
    for (pointeurim = 0; pointeurim < nx * ny; pointeurim++)
    {
        fprintf(sortie, "%f \n", surimage[pointeurim]);
    }
    fclose(sortie);
#endif
}
