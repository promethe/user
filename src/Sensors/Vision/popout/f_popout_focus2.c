/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_focus2.c 
\brief 

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

void function_popout_focus2(int Gpe)
{
    int i, j, k, x, y, InputGpe, nx, ny, A, n;
    char *chaine, *ST, *reponse;    /* reponse[2]; 15/10/2003 Olivier Ledoux */
    static int Ncartes, master, GpeFoc; /* 15/10/2003 Olivier Ledoux rajout de GpeFoc */
    float MaxActivity, **Carte;
    /* unused MaxMap[24]; */
    A = 0;                      /* taille de la zone de recherche du max */

    /* initialisation */
    if (def_groupe[Gpe].ext == NULL)
    {
#ifdef DEBUG
        printf("dans function_popout_focus : alloc de mem pour le groupe\n");
#endif
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("dans function_popout_focus2 : ALLOCATION IMPOSSIBLE ...! \n");
            exit(0);
        }
        /* on cherche les groupes precedents */
        /*printf("dans function_popout_focus : recup images precedentes\n"); */
        j = 0;
        master = -1;
        GpeFoc = -1;            /* 15/10/2003 Olivier Ledoux */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                /* on cherche la liaison avec master2 et avec les cartes de caracteristiques */
                chaine = liaison[i].nom;
                ST = NULL;
                ST = strstr(chaine, "N");
                if (ST != NULL)
                {
                    ((prom_images_struct *) def_groupe[Gpe].ext)->
                        images_table[atoi(&ST[1])] =
                        ((prom_images_struct *) def_groupe[InputGpe].ext)->
                        images_table[0];
                    j++;
                    /*printf("liens : N%d -> groupe %d\n",atoi(&ST[1]),InputGpe); */
                }
                else
                {
                    ST = strstr(chaine, "master");
                    if (ST != NULL) /* 15/10/2003 Olivier Ledoux rajout test car on a un autre lien possible */
                        master = InputGpe;
                    /* 15/10/2003 Olivier Ledoux rajout pour gerer le lien 'focalisation' */
                    else
                    {
                        ST = strstr(chaine, "focalisation");
                        if (ST != NULL)
                            GpeFoc = InputGpe;
                        else
                        {
                            printf("%s : lien non reconnu\n", __FUNCTION__);
                            exit(0);
                        }
                    }
                }
            }
        Ncartes = j;


        if ((GpeFoc == -1) || (master == -1))
        {
            printf
                ("dans function_popout_focus : il manque le lien 'master' ou 'focalisation'\n");
            exit(0);
        }


#ifdef DEBUG
        printf
            ("dans function_popout_focus2 : recuperation de %d cartes et Gpe master2 = %d\n",
             j, master);
#endif

        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[master].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[master].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[master].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("dans function_popout_focus : votre image est couleur, cette fonction ne fait que le N&B\n");
            exit(0);
        }
        /* put values to 0 */
        for (i = 0; i < Ncartes; i++)
            neurone[def_groupe[Gpe].premier_ele + i].s = 0.0;


    }
    else
    {
    }

    /* 15/10/2003 Olivier Ledoux */
    reponse = (char *) def_groupe[GpeFoc].ext;

    /* master et Ncartes sont static !!! */
    Carte = (float **) malloc(Ncartes * sizeof(float *));
    if (Carte == NULL)
    {
        printf("dans function_popout_focus : plus de memoire\n");
        exit(0);
    }
    /* il nous faut le centre de l'imagette */
    x = (int) neurone[def_groupe[master].premier_ele].s;
    y = (int) neurone[def_groupe[master].premier_ele + 1].s;
    printf("%s : coordonnees du point : x=%d y=%d\n", __FUNCTION__, x, y);
    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;
    for (i = 0; i < Ncartes; i++)
    {
        Carte[i] =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[i];

    }
    for (i = 0; i < 24; i++)
    {
        neurone[def_groupe[Gpe].premier_ele + i].s = 0.0;
        neurone[def_groupe[Gpe].premier_ele + i].s1 = 0.0;
        neurone[def_groupe[Gpe].premier_ele + i].s2 = 0.0;
    }

    /* on cherche le max sur une imagette de chaque carte et le note sur un neurone */
    for (i = 0; i < Ncartes; i++)
    {
        /*printf("recherche sur carte %d\n",i); */
        MaxActivity = 0.0;
        for (j = -A; j <= A; j++)
            /* 22/10/2003 Olivier Ledoux
               petites corrections
             */
            /*
               if (((j + x )>= 0) && ((j + x) < ny))
               for (k = -A; k <= A; k++)
               if (((k + y) >= 0) && ((k + y) < nx)) {
               if (MaxActivity < Carte[i][(j + x) * nx + k + y])
               MaxActivity = Carte[i][(j + x) * nx + k + y];
               }
             */
            if (((j + x) >= 0) && ((j + x) < nx))
                for (k = -A; k <= A; k++)
                    if (((k + y) >= 0) && ((k + y) < ny))
                    {
                        if (MaxActivity < Carte[i][j + x + (k + y) * nx])
                            MaxActivity = Carte[i][j + x + (k + y) * nx];
                    }

        /* copie sur le neurone */
        printf("max activ = %f (carte %d)\n", MaxActivity, i);
        neurone[def_groupe[Gpe].premier_ele + i].s = MaxActivity;
        neurone[def_groupe[Gpe].premier_ele + i].s1 = MaxActivity;
        neurone[def_groupe[Gpe].premier_ele + i].s2 = MaxActivity;


    }


    /* 15/10/2003 Olivier Ledoux
       Automatisation */
    /*
       printf("*** sur cible (o/n) ?");
       fgets(reponse,2,stdin);
       if (strstr(reponse, "o")) {
     */
    if (reponse[0] == 'o')
    {                           /* 15/10/2003 Olivier Ledoux */

        /* augmentation de la vigilence */
        vigilence = 1.0;
    }
    else
    {
        /* diminution */
        vigilence = 0.0;
    }


    free(Carte);
}
