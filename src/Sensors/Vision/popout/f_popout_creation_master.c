/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_popout_creation_master.c 
\brief fonctions directement liees au popout et a l'attention

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Fonction qui qui recupere les cartes apres modulation et qui les ajoute
   en une seule (avec eventuellement une attenuation) 
   
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>

void function_popout_creation_master(int Gpe)
{
    int i, j, InputGpe = 0, n, nx, ny, Ncartes;
    char *ST;
    float *bipoints[28], *master, somme;    /* Olivier Ledoux 10/03/2003 r 24 p 28 */
    /* float maxi;   non utilise, JC, 4/9/2003 */
    int link_nber;

    /* devrait etre globale et dans le .dev ou .config */
    int ATTENUATION = 0;



    /* initialisation */
    if (def_groupe[Gpe].ext == NULL)
    {
#ifdef DEBUG
        printf
            ("dans function_popout_creation_master : alloc de mem pour le groupe\n");
#endif
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("dans function_popout_creation_master : ALLOCATION IMPOSSIBLE ...! \n");
            exit(0);
        }
        /* on cherche les groupes precedents */
        /* nouveau code pour demo de la fonction de Philippe */
#ifdef DEBUG
        printf
            ("dans function_popout_creation_master : recup liens vers images\n");
#endif
        j = 0;                  /* compteur de liens */
        i = 0;                  /* idem */
        link_nber = find_input_link(Gpe, i);
        while (link_nber != -1)
        {
#ifdef DEBUG
            printf("link number %d, from %d to %d with '%s'", link_nber,
                   liaison[link_nber].depart, Gpe, liaison[link_nber].nom);
#endif
            /*on regarde ce qu'il y a sur ce lien */
            ST = strstr(liaison[link_nber].nom, "N");
            if (ST != NULL && (atoi(&ST[1]) < 29))
            {                   /* Olivier Ledoux 10/03/2003 r 25 p 29 */
                ((prom_images_struct *) def_groupe[Gpe].ext)->
                    images_table[atoi(&ST[1])] =
                    ((prom_images_struct *)
                     def_groupe[liaison[link_nber].depart].ext)->
                    images_table[0];
                InputGpe = liaison[link_nber].depart;
                i++;
            }
            else
            {
                /* peut etre une erreur ! */
                printf
                    ("dans function_popout_creation_master : un lien est mal formate ( %d -> %d : '%s')\n",
                     find_input_link(Gpe, i), Gpe, ST);
                exit(0);
            }
            link_nber = find_input_link(Gpe, i);
#ifdef DEBUG
            printf("valeur de link_nber : %d", link_nber);
#endif
        }
        /* sauvegarde du nombre de carte */
        neurone[def_groupe[Gpe].premier_ele].s = (float) i;
#ifdef DEBUG
        printf
            ("dans function_popout_creation_master : recuperation de %d cartes\n",
             i);
#endif
        /*printf("dans function_popout_creation_master : recup info image\n"); */
        /* recuperation des infos d'un des groupes precedents */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("dans function_popout_creation_master : votre image est couleur, cette fonction ne fait que le N&B\n");
            exit(0);
        }
#ifdef DEBUG
        printf
            ("dans function_popout_creation_master : mem pour les images\n");
#endif
        /* pour les images resultats */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(n * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf
                ("dans function_popout_creation_master : ALLOCATION IMPOSSIBLE ...! \n");
            exit(0);
        }


    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        n = nx * ny;

    }
    /* simplifications */
    Ncartes = (int) neurone[def_groupe[Gpe].premier_ele].s;
    for (i = 1; i < Ncartes + 1; i++)
    {
        bipoints[i - 1] =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[i];
        /*printf("bipoints[%d] -> %x\n",i-1,bipoints[i-1]); */
    }
    master =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];

    /* attenuation */
    if (ATTENUATION == 1)
    {
#ifdef DEBUG
        printf("dans function_popout_creation_master : attenuation\n");
#endif
        for (i = 0; i < Ncartes; i++)
        {
            /*printf("carte %d (%x)\n",i,bipoints[i]); */
            somme = 0.0;
            for (j = 0; j < n; j++)
            {
                if (bipoints[i][j] < 0.0)
                    bipoints[i][j] = 0.0;
                somme = somme + bipoints[i][j];
            }
            /*printf("somme %f (carte %d) OK\n", somme, i); */
            if (somme > 0.0)
            {
                somme = 1.0 / somme;
                for (j = 0; j < n; j++)
                {
                    bipoints[i][j] = bipoints[i][j] * somme;
                }
            }
        }
    }
    else
    {
#ifdef DEBUG
        printf("dans function_popout_creation_master : PAS d'attenuation\n");
#endif
    }
    /* creation de la mastermap */


    for (i = 0; i < n; i++)
    {
        master[i] = 0.0;
        for (j = 0; j < Ncartes; j++)
        {
            master[i] = master[i] + bipoints[j][i];
        }
        /* nouveau code */
        /*master[i] = 0.318*(1.57+atan(master[i] + 0.2)); */
    }


    printf("%s : master @ '%p'\n", __FUNCTION__, (void*)master);
}
