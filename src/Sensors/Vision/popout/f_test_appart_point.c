/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_test_appart_point.c 
\brief Fonction qui teste l'appartenance du point detecte a la plaque

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description  
  Fonction qui teste l'appartenance du point detecte a la plaque: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
void function_test_appart_point(int Gpe)
{
    int xtg = 0, ytg = 0, xtd = 0, ytd = 0, xbg = 0, ybg = 0, xbd = 0, ybd =
        0;
    int i, InputGpe;
    int xpoint = 0, ypoint = 0;
    char *c, *chaine;


    float X1, X2, X3, Y1, Y2, Y3, A, B; /* 17/03/2003 Olivier */
    printf("function_test_appart_point\n");
    /* 08/10/2003 Olivier Ledoux
       Correction pour eviter fuite de memoire
     */

    /*
       c=(char *)malloc(1);
     */
    if (def_groupe[Gpe].ext == NULL)
    {
        c = (char *) malloc(1);
        def_groupe[Gpe].ext = (char *) c;
    }
    else
        c = (char *) def_groupe[Gpe].ext;


    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == Gpe)
        {
            chaine = liaison[i].nom;
            InputGpe = liaison[i].depart;

            if (strstr(chaine, "master"))
            {
                xpoint = (int) neurone[def_groupe[InputGpe].premier_ele].s;
                ypoint =
                    (int) neurone[def_groupe[InputGpe].premier_ele + 1].s;
            }
            else if (strstr(chaine, "cord"))
            {
                xtg = (int) neurone[def_groupe[InputGpe].premier_ele].s;
                ytg = (int) neurone[def_groupe[InputGpe].premier_ele + 1].s;
                xtd = (int) neurone[def_groupe[InputGpe].premier_ele + 2].s;
                ytd = (int) neurone[def_groupe[InputGpe].premier_ele + 3].s;
                xbg = (int) neurone[def_groupe[InputGpe].premier_ele + 4].s;
                ybg = (int) neurone[def_groupe[InputGpe].premier_ele + 5].s;
                xbd = (int) neurone[def_groupe[InputGpe].premier_ele + 6].s;
                ybd = (int) neurone[def_groupe[InputGpe].premier_ele + 7].s;
            }
        }

    neurone[def_groupe[Gpe].premier_ele].s = (float) xpoint;
    neurone[def_groupe[Gpe].premier_ele + 1].s = (float) ypoint;
/* c[1]='\0';
  if(   xpoint>=xtg && xpoint<=xbg && xpoint>=xtd && xpoint<=xbd
	&& ypoint>=ytg && ypoint<=ytd && ypoint>=ybg && ypoint<=ybd)
	c[0]='o';
	else
       c[0]='n';*/



/*
02/10/2003 Olivier Ledoux
L'interversion entre X et Y n'a plus lieu d'etre vu qu'elle
est effectuee a la lecture des coordonnees
*/

/*
 X1=(float) (ybg-ytg); Y1=(float) (xbg-xtg);
 X2=(float) (ypoint-ytg); Y2=(float) (xpoint-xtg);
 X3=(float) (ytd-ytg); Y3=(float) (xtd-xtg);
 B=(Y2*X3-X2*Y3)/(X3*Y1-X1*Y3);
 A=(X2-B*X1)/X3;
 if ( (A<0) || (B<0) ) c[0]='n';
 else
   {
     X1=(float) (ytd-ybd); Y1=(float) (xtd-xbd);
     X2=(float) (ypoint-ybd); Y2=(float) (xpoint-xbd);
     X3=(float) (ybg-ybd); Y3=(float) (xbg-xbd);
     B=(Y2*X3-X2*Y3)/(X3*Y1-X1*Y3);
     A=(X2-B*X1)/X3;
     if ( (A<0) || (B<0) ) c[0]='n';
     else c[0]='o';
   }
*/
    Y1 = (float) (ybg - ytg);
    X1 = (float) (xbg - xtg);
    Y2 = (float) (ypoint - ytg);
    X2 = (float) (xpoint - xtg);
    Y3 = (float) (ytd - ytg);
    X3 = (float) (xtd - xtg);
    B = (Y2 * X3 - X2 * Y3) / (X3 * Y1 - X1 * Y3);
    A = (X2 - B * X1) / X3;
    if ((A < 0) || (B < 0))
        c[0] = 'n';
    else
    {
        Y1 = (float) (ytd - ybd);
        X1 = (float) (xtd - xbd);
        Y2 = (float) (ypoint - ybd);
        X2 = (float) (xpoint - xbd);
        Y3 = (float) (ybg - ybd);
        X3 = (float) (xbg - xbd);
        B = (Y2 * X3 - X2 * Y3) / (X3 * Y1 - X1 * Y3);
        A = (X2 - B * X1) / X3;
        if ((A < 0) || (B < 0))
            c[0] = 'n';
        else
            c[0] = 'o';
    }


    def_groupe[Gpe].ext = (char *) c;
    /*printf( "def_groupe[Gpe].ext=%s\n", def_groupe[Gpe].ext);getchar(); */

    printf("def_groupe[Gpe].ext=%c\n", c[0]);

    printf("je sors de function_test_appart_point\n");
}
