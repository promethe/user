/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libSensors
\defgroup f_running_average f_running_average

\brief calcul de la moyenne glissante sur une scene. (Entrees et Sortie sur les champs ext)

Author: Ali Karaouzene
Created: 23/04/2013
- description: specific file creation


\section Description
 This fonction computes the running average of inupt images following
\f[
 DstImage (t)  = \alpha * SrcImage(t) + (1-alpha) * DstImage(t-1)

 \f]

\section Macro
-none

\section Local varirables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function
\file f_img_gris_selection.c 

http://www.doxygen.org
 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

typedef struct	mydata
{
	prom_images_struct	*prev_ext;
	prom_images_struct	*mem;
	int InputGpe;
	int sx;
	int	sy;
	int nb_band;
	float alpha;
}			Data_running_average;

void new_running_average(int gpe)
{
	int i,l;
	char * string = NULL;
	char param_link[256];
	Data_running_average	*mydata = NULL;

	/*Allocation de la structure de donnees*/
	mydata = (Data_running_average *) malloc (sizeof(Data_running_average));
	if (mydata == NULL)
		EXIT_ON_ERROR("Allocation impossible %s",__FUNCTION__);
	/*Recuperation des liens*/
	mydata->InputGpe = -1;
	i = 0;
	l = find_input_link(gpe, i);
	while (l != -1)
	{
		string = liaison[l].nom;
		if (strstr(string,"sync") != NULL )
			dprints("\nlien sync ");
		else
		{
			mydata->InputGpe = liaison[l].depart;
			if (prom_getopt(string, "-a", param_link) == 2 || prom_getopt(string, "-A", param_link) == 2)
				mydata->alpha = atof(param_link);
		}
		i++;
		l = find_input_link(gpe, i);
	}

	if(mydata->InputGpe == -1)
		EXIT_ON_ERROR("No input group in %s",__FUNCTION__);

	/*Stockage des parametres*/
	def_groupe[gpe].data = mydata;

}

void function_running_average(int gpe)
{
	Data_running_average	*mydata = NULL;
	prom_images_struct	*src_im = NULL;
	prom_images_struct	*dst_im = NULL;
	unsigned char  *input = NULL;
	float *acc_im = NULL;
	unsigned char *output = NULL;
	float alpha = 0.;
	int i = 0, n = 0, sx = 0, sy = 0;



	mydata = (Data_running_average *) def_groupe[gpe].data;
	/*Recuperation des infos et allocation des images*/
	if (def_groupe[gpe].ext == NULL)
	{
		if (mydata == NULL)
			EXIT_ON_ERROR("Impossible d'allouer dans %s\n",__FUNCTION__);
		/*Initialiser mydata*/
		mydata->sx			= 0;
		mydata->sy            = 0;
		mydata->nb_band       = 0;

		if (def_groupe[mydata->InputGpe].ext == NULL)
		{
			PRINT_WARNING("Gpe amont avec ext nulle; pas de calcul de moyene %s\n",__FUNCTION__);
			return;
		}

		/* allocation de memoire */
		def_groupe[gpe].ext = (void *) malloc(sizeof(prom_images_struct));
		if (def_groupe[gpe].ext == NULL)  EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...!");

		/* recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image de ce groupe avec les infos recuperees */
		src_im = (prom_images_struct *) def_groupe[mydata->InputGpe].ext;
		mydata->sx = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->sx;
		mydata->sy = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->sy;
		mydata->nb_band = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->nb_band;
		n = mydata->sx * mydata->sy;
		dprints ("%s Taille image %d x %d\n",__FUNCTION__,mydata->sx,mydata->sy);
		if (mydata->nb_band !=1) EXIT_ON_ERROR ("%s L'image d'entree doit etre en niveaux de gris",__FUNCTION__);
		/* allocation de memoire */
		dst_im = calloc_prom_image(1, mydata->sx, mydata->sy, 1);
		dst_im->images_table[0] = (unsigned char *) calloc(n, sizeof(char));
		dst_im->images_table[1] = (unsigned char *) calloc(n, sizeof(float));
		/* attention, pointeurs vers des reels ou des entiers */
		if (dst_im == NULL ||dst_im->images_table[0] == NULL ||dst_im->images_table[1] == NULL  )
			EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! %s ligne(%s)\n",def_groupe[gpe].no_name,__LINE__);
		/*mise a zero de l'accumulateur*/
		for (i =n ;i--;)
			dst_im->images_table[1][i] = 0;

		/* sauvgarde des infos trouves sur le lien */
		def_groupe[gpe].ext  = dst_im;
	}

	/*Restitution des parametres*/
	src_im   =   ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext));
	dst_im  =   ((prom_images_struct *) (def_groupe[gpe].ext));
	input = (unsigned char*) src_im->images_table[0];
	output = (unsigned char*) dst_im->images_table[0];
	acc_im = (float*) dst_im->images_table[1];
	sx = mydata->sx;
	sy = mydata->sy;
	n = sx*sy;
	alpha = mydata->alpha;

	/* calcul la moyenne sur les n images precedentes */
	for (i = 0; i < n; i++)
		acc_im[i] = alpha * input[i] + (1-alpha) * acc_im[i];
	for (i =n ;i--;)
		output[i] = (acc_im[i] < 0 ? 0 : (unsigned char) acc_im[i]);
}

void destroy_running_average(int gpe)
{
	free(def_groupe[gpe].data);
	free(def_groupe[gpe].ext);
	def_groupe[gpe].data = NULL;
	def_groupe[gpe].ext = NULL;
	dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}
