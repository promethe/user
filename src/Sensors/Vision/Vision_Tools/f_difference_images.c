/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/******************************
\ingroup libSensors
\defgroup f_difference_images.c f_difference_images.c
 *
 *
\date modifiee 21/10/2013
\author modifiee par PG.
\brief

Cette fonction calcul :
- La difference entre deux images. 
Elle produit une image analogique ou une image binarisee 0 ou 255 pour 1 si l'option -s est utilisee avec une valeur superieure a 0.
Si le lien -abs n'est pas utilise alors les valeurs negatives sont mise a 0. Les valeur superieures a 255 sont mise a 255 (si cela peut se produire???)


Options sur les liens :
"positif": image en entree
"negatif": image en entree soustraite (negative)
"-sxxx" ou "-Sxxx" avec xxx un float representant la valeur de seuillage sur la difference.
-abs: la difference est mise en valeur absolue.


\section description
Les 2 images doivent faire la meme taille.

 ***************************************************************************************************************************/

/* #define DEBUG */
 
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/cadrage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <public_tools/Vision.h>



typedef struct MyData {
	int gpe_image_positif;
	int gpe_image_negatif;
	int nbre_colonne;
	int nbre_ligne;
	float seuil;
	int v_ABS;
}MyData;

void function_difference_images(int gpe)
{
	MyData *mydata=NULL;
	prom_images_struct * output_image=NULL;
	prom_images_struct *input_image_positif=NULL;
	prom_images_struct *input_image_negatif=NULL;
	unsigned char  *input_positif = NULL;
	unsigned char  *input_negatif=NULL;
	unsigned char *output = NULL;
	char param_link[256];
	int i=0;
	int j=0;
	float seuil=1;
	int v_ABS = 0;

	int nbre_colonne;	   /*la dimension de l'image en x*/
	int nbre_ligne;         /*la dimension de l'image en y*/

	int taille;
	/*Premier passage dans la fonction*/

	dprints("on rentre dans la fonction %s\n",__FUNCTION__);

	if(def_groupe[gpe].data == NULL)
	{
		mydata=(MyData *)malloc(sizeof(MyData));
		if(mydata==NULL)
			EXIT_ON_ERROR("pb malloc pour mydata in function %s",__FUNCTION__);
		/*recuperation des parametres d'entree*/
		mydata->seuil = -1;
		mydata->gpe_image_negatif = -1;
		mydata->gpe_image_positif = -1;

		while((j=find_input_link(gpe,i))!=-1)
		{
			dprints("nom liaison = %s\n",liaison[j].nom);

			if(strcmp(liaison[j].nom,"positif")==0)
			{
				mydata->gpe_image_positif=liaison[j].depart;
				dprints("groupe image positif=%s\n",def_groupe[liaison[j].depart].no_name);
			}

			if(strncmp(liaison[j].nom,"negatif",7)==0)
			{
				mydata->gpe_image_negatif=liaison[j].depart;
				dprints("groupe image negatif=%s\n",def_groupe[liaison[j].depart].no_name);
			}
			if (prom_getopt(liaison[j].nom, "-s", param_link) == 2 || prom_getopt(liaison[j].nom, "-S", param_link) == 2)
			{
				mydata->seuil = atof(param_link);
				dprints(" Seuil =%f\n",mydata->seuil);
			}
			if (prom_getopt(liaison[j].nom, "-abs", param_link) == 1 || prom_getopt(liaison[j].nom, "-ABS", param_link) == 1)
			{
				mydata->v_ABS= v_ABS = 1;
				dprints(" Diff en valeur absolue\n");
			}
			i++;
			dprints("on a fini de recuperer les parametres\n");
		}

		if (mydata->gpe_image_negatif == -1 || mydata->gpe_image_positif == -1)
			EXIT_ON_ERROR ("Les (ou une des deux) images en entree sont manquantes(%s)\n",__FUNCTION__);
		input_image_positif=((prom_images_struct *)(def_groupe[mydata->gpe_image_positif].ext));
		dprints("pointeur imput_image_positif=%p",(void *)input_image_positif);
		input_image_negatif=((prom_images_struct *)(def_groupe[mydata->gpe_image_negatif].ext));
		dprints("pointeur imput_image_negatif=%p",(void *)input_image_negatif);

		output_image=calloc_prom_image(1,input_image_positif->sx,input_image_positif->sy,1);
		def_groupe[gpe].ext=output_image;  /*sauvegarde de l'image*/
		def_groupe[gpe].data=mydata;	 /*sauvegarde de Mydata*/

		dprints("on a fini l allocation de la premiere image\n");
	}

	else
	{
		mydata = def_groupe[gpe].data;
		input_image_positif=((prom_images_struct *)(def_groupe[mydata->gpe_image_positif].ext));
		input_image_negatif=((prom_images_struct *)(def_groupe[mydata->gpe_image_negatif].ext));

		output_image=((prom_images_struct *)(def_groupe[gpe].ext));
		dprints("on a fini l allocation d une image autre que la premiere\n");
        v_ABS=mydata->v_ABS;                 /* PG: ajout de la recopie... */
        seuil=mydata->seuil ;
	}


	dprints("on est avant la boucle\n");

	/******************************************************/
	/**                  difference                      **/
	/******************************************************/

	nbre_colonne = input_image_positif->sx;
	nbre_ligne = input_image_positif->sy;
	taille = nbre_colonne * nbre_ligne;
	output = (unsigned char  *) output_image->images_table[0];
	input_positif = (unsigned char  *)  input_image_positif->images_table[0];
	input_negatif = (unsigned char  *)  input_image_negatif->images_table[0];

	dprints("taille en X=%d\n",input_image_positif->sx);
	dprints("taille en Y=%d\n",input_image_positif->sy);
    dprints("abs =%d seuil = %f \n",v_ABS,seuil);


  if(v_ABS==1) 
  {
    for(i=0 ; i<taille ; i++)
      output[i] = (unsigned char) abs(input_positif[i] - input_negatif[i]);  
  }
  else 
  {
		for(i=0 ; i<taille ; i++)
		{

			if(input_positif[i] - input_negatif[i] < 0)  output[i] = 0;
			else
			{
				if(input_positif[i] - input_negatif[i] > 255)  output[i] = 255;
				else  output[i] = input_positif[i] - input_negatif[i];
			}

			/*output[i] = input_positif[i] - input_negatif[i]  ;*/
		}
  }

  if (seuil > 0.)
  {
	for (i=0;i<taille;i++) 	output[i] = (output[i] < seuil ? 0 : 255);
  }

  dprints("taille en X=%d\n",input_image_positif->sx);
  dprints("taille en Y=%d\n",input_image_positif->sy);
  dprints("===============fin de la fonction %s=======================)\n",__FUNCTION__);
}
