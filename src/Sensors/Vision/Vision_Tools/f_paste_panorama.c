/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_paste_panorama f_paste_panorama


\section Author
-Name: C.Giovannangeli
-Created: 01/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$

\section Description
This fonction paste two panorama from_get_panorama



\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-InitPanorama()
-InitPanoramaInTest()
-GetPanorama()
-GetPanoramaInTest()

\section External Tools
-tools/IO_Robot/Com_Koala/GoToLeftRightUncond()


\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <stdio.h>
#include <libx.h>
#include <stdlib.h>
#include <Typedef/boolean.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <libhardware.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

void function_paste_panorama(int gpe)
{
   int l, i, j;
   prom_images_struct *pt = NULL, *pt_ext_entree = NULL;
   pt = (prom_images_struct *) def_groupe[gpe].ext;
   if (pt == NULL)
   {
      l = find_input_link(gpe, 0);
      if (strcmp(def_groupe[liaison[l].depart].nom, "f_get_panorama") != 0)
      {
         EXIT_ON_ERROR("le groupe d'netre de f_paste_panorama n'est f_get_panorama:%s\n", def_groupe[liaison[l].depart].nom);
      }
      pt = (prom_images_struct *) malloc(sizeof(prom_images_struct)); /*allocation du pointeur */
      if ((pt) == NULL)
      {
         EXIT_ON_ERROR("function_get_panorama_alternate: ALLOCATION IMPOSSIBLE ...! \n");

      }
      pt_ext_entree =(prom_images_struct *) def_groupe[liaison[l].depart].ext;
      pt = calloc_prom_image(1, pt_ext_entree->sx * 2 - 120, pt_ext_entree->sy, pt_ext_entree->nb_band);
      def_groupe[gpe].ext = (prom_images_struct *) pt;
   }
   else
   {
      pt = (prom_images_struct *) def_groupe[gpe].ext;
      l = find_input_link(gpe, 0);
      pt_ext_entree = (prom_images_struct *) def_groupe[liaison[l].depart].ext;
   }
   /*
     for(i=0;i<pt_ext_entree->sy;i++)
     {
     for(j=0; j<pt_ext_entree->sx-50;j++)
     {
     pt->images_table[0][j+i*pt->sx]=pt_ext_entree->images_table[0][j+i*xmax_ng];
     }
     }

     for(i=0;i<pt_ext_entree->sy;i++)
     {
     for(j=0; j<pt_ext_entree->sx-50;j++)
     {
     pt->images_table[0][j+pt_ext_entree->sx-50+i*pt->sx]=pt_ext_entree->images_table[1][j+50+i*xmax_ng];
     }
     } */
   for (i = 0; i < pt_ext_entree->sy; i++)
   {
      for (j = 60; j < pt_ext_entree->sx; j++)
      {
         pt->images_table[0][(j - 60 + i * pt->sx) * 3]     = pt_ext_entree->images_table[0][(j + i * pt_ext_entree->sx) * 3];
         pt->images_table[0][(j - 60 + i * pt->sx) * 3 + 1] = pt_ext_entree->images_table[0][(j + i * pt_ext_entree->sx) * 3 + 1];
         pt->images_table[0][(j - 60 + i * pt->sx) * 3 + 2] = pt_ext_entree->images_table[0][(j + i * pt_ext_entree->sx) * 3 + 2];
      }
   }

   for (i = 0; i < pt_ext_entree->sy; i++)
   {
      for (j = 60; j < pt_ext_entree->sx; j++)
      {

         pt->images_table[0][(j + pt_ext_entree->sx - 120 + i * pt->sx) * 3]     = pt_ext_entree->images_table[1][(j + i * pt_ext_entree->sx) * 3];
         pt->images_table[0][(j + pt_ext_entree->sx - 120 + i * pt->sx) * 3 + 1] = pt_ext_entree->images_table[1][(j + i * pt_ext_entree->sx) * 3 + 1];
         pt->images_table[0][(j + pt_ext_entree->sx - 120 + i * pt->sx) * 3 + 2] = pt_ext_entree->images_table[1][(j + i * pt_ext_entree->sx) * 3 + 2];
      }
   }
}
