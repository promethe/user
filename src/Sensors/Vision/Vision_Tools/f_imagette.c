/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_imagette f_imagette
\brief


\section Author

VILLEMIN Jean-Baptiste
Created: 13/05/09


\section Description
	Cette fonction cree une imagette a��� partir d'une image d'entrae.
	Cette imagette est prise autour d'un point, de coordonnees X et Y sur l'image qui sont obtenues a partir des valeurs des neurones du groupe precedent (f_sequential_winner par exemple).
	La taille de l'imagette est deduite des valeurs -H et -W trouvees sur les liens entrants.

	Les liens sont -image et -taille-Hxxx-Wxxx

	Ajout : Gestion des float et des images couleurs. Ali K.

\section Macro
-none

\section Local variables
-nnoe

\section Global variables
-none

\section Internal Tools
-echange()

\section External Tools
-tools/Vision/Vision/calloc_prom_image()
-tools/Vision/Vision/free_prom_image()

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function
\file f_img_gris_selection.c

http://www.doxygen.org
 */

#include <Struct/prom_images_struct.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>



typedef struct
{
   int GpeImage;
   int GpePoint;
   int hauteur;
   int largeur;
   int numneurone;
} data;




void new_imagette(int numero)
{

   data *my_data;
   int i = -1;
   int link1 = -1, link2 = -1;
   char *string = NULL, *param = NULL;

   dprints(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);

   /**
    * on cree la structure qui contiendra les donnees
    */

   my_data = (data *) malloc(sizeof(data));

   if (my_data == NULL)
      EXIT_ON_ERROR("error in memory allocation in group %d %s\n", numero,__FUNCTION__);

   /* On initialise cette structure */
   my_data->GpeImage = -1;
   my_data->GpePoint = -1;


   /**
    * On recherche les liens et les num��ros des boites entrantes
    */
   link1 = find_input_link(numero, 0);
   link2 = find_input_link(numero, 1);

   if ((link1 == -1) || (link2 == -1))
   {
      EXIT_ON_ERROR("Il n'y a pas deux liens en entree du Gpe %s\n",def_groupe[numero].no_name);
   }

   if ( ((param = strstr(liaison[link1].nom, "-image"))!=NULL ) && ((param = strstr(liaison[link2].nom, "-taille"))!=NULL ) )
   {
      my_data->GpeImage = liaison[link1].depart;
      my_data->GpePoint = liaison[link2].depart;
      i = link2;
   }
   else if ( ((param = strstr(liaison[link2].nom, "-image"))!=NULL ) && ((param = strstr(liaison[link1].nom, "-taille"))!=NULL ) )
   {
      my_data->GpeImage = liaison[link2].depart;
      my_data->GpePoint = liaison[link1].depart;
      i = link1;
   }
   else
      EXIT_ON_ERROR("%s %s : Erreur dans la gestion des liens. Il faut deux liens : -image et -taille-Hxx-Wxx\n",__FUNCTION__,def_groupe[numero].no_name);



   dprints("GpeImage = %d\n", GpeImage);
   dprints("GpePoint = %d\n", GpePoint);

   /**
    * On recupere sur le lien la taille de l'imagette a��� creer
    */

   string = liaison[i].nom;

   param = strstr(string, "-H");
   if ( param != NULL)
      my_data->hauteur = (int) atof(&param[2]);
   else
      EXIT_ON_ERROR("%s : Parametre entree incorrect ! -H avec hauteur imagette derriere\n", __FUNCTION__);

   param = strstr(string, "-W");
   if ( param != NULL)
      my_data->largeur = (int) atof(&param[2]);
   else
      EXIT_ON_ERROR("%s : Parametre entree incorrect ! -W avec largeur imagette derriere\n", __FUNCTION__);

   dprints("Hauteur:%d, largeur:%d\n",hauteur, largeur);

   /**
    * On recupere le numero du premier neurone de la boite contenant les neurones
    * codant pour les coordonnees centrales de l'imagette
    */
   my_data->numneurone = def_groupe[my_data->GpePoint].premier_ele;

   /**
    * Passe les donnes a la fonction
    */
   def_groupe[numero].data = my_data;

   dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);


}


/**
 *	------------------- FIN DU CONSTRUCTEUR ------------------------
 */




void function_imagette(int numero)
{
   int i;
   int j;
   /* int gpe_entree=-1;   La variable n'est apparemment pas utilis���e*/
   int PosX,PosY;
   int x,y,p,pr;
   int debx, deby, finx,finy;
   int taillex, tailley;
   prom_images_struct *p_images_ext, *p_images_dest;
   data *my_data = NULL;


   dprints("----------------------Begin of %s --------------------\n", __FUNCTION__);

   /* Recuperation donnees data */

   my_data = (data *) def_groupe[numero].data;
   if (my_data == NULL)	EXIT_ON_ERROR("error while loading data in group %d %s\n", numero,__FUNCTION__);

   /* Getting the extension of previous group */

   p_images_ext = (prom_images_struct *) def_groupe[my_data->GpeImage].ext;
   dprints("Image en entree du gpe %s est type %d \n",def_groupe[numero].no_name,p_images_ext->nb_band);

   if (p_images_ext == NULL)
   {
      PRINT_WARNING("No image in input group (ext == NULL)\n");
      return;
      /*printf("Problem in %s: there is nothing(may be freed)) in extension of group %i\n",__FUNCTION__,gpe_entree);
        	exit(EXIT_FAILURE);*/
   }


   /**
    * Recuperation de la valeur des deux neurones codant pour le milieu de l'imagette
    * On prend la valeur du premier neurone de la boite
    * Ces valeurs sont multipliees par la taille en X ou Y de l'image pour retrouver le bon point
    */

   taillex = p_images_ext->sx;
   tailley = p_images_ext->sy;

   PosX = (int) (neurone[my_data->numneurone + 0].s1 * taillex);
   PosY = (int) (neurone[my_data->numneurone + 1].s1 * tailley);

   dprints("PosX : %d, PosY:%d\n",PosX,PosY);

   /* Test coherence hauteur largeur */
   if (my_data->hauteur < 0 || my_data->largeur < 0 || my_data->hauteur > (int)tailley || my_data->largeur > (int)taillex)
      EXIT_ON_ERROR("Erreur dans le choix de la hauteur et de la largeur de l'imagette\n");

   /* Calcul limite imagette */
   debx = PosX - floor(my_data->largeur / 2);
   finx = PosX + floor(my_data->largeur / 2);
   deby = PosY - floor(my_data->hauteur / 2);
   finy = PosY + floor(my_data->hauteur / 2);

   if (debx < 0 )				debx = 0;
   if (finx > taillex )		finx = taillex;
   if (deby < 0 )				deby = 0;
   if (finy > tailley )		finy = tailley;

   dprints("debx: %d, finx: %d\t deby: %d, finy: %d\n",debx,finx,deby,finy);

   /**
    * Allocation image dans l'extension du groupe
    */
   if (def_groupe[numero].ext == NULL)
      def_groupe[numero].ext = (void *) calloc_prom_image(p_images_ext->image_number, my_data->largeur, my_data->hauteur,p_images_ext->nb_band);

   p_images_dest = (prom_images_struct *) def_groupe[numero].ext;
   /*if (p_images_dest != NULL)
   	free_prom_image(p_images_dest);

   p_images_dest = (prom_images_struct *) def_groupe[numero].ext;
    */
   if (p_images_dest == NULL)	EXIT_ON_ERROR("ERR\n");


   dprints("p_images_dest : %d %d %d %d\n", p_images_dest->image_number, p_images_dest->sx,p_images_dest->sy, p_images_dest->nb_band);

   /* Decoupage une a une des images*/
//PRINT_WARNING("Les sizeof : gpe %s type %d sont %d un float a la taille %d un char taille %d"	,def_groupe[numero].no_name,p_images_ext->nb_band,sizeof(p_images_dest->images_table[0]),sizeof(float), sizeof(char));

   for (i = 0; i < (int)p_images_ext->image_number; i++)
   {
      for (y = deby; y < finy; y++)
         for (x = debx; x < finx; x++)
         {
            for (j = 0; j < p_images_ext->nb_band; j++)
            {
               p = (x + (p_images_ext->sx * y)) * p_images_ext->nb_band  +  j;
               pr = (x - debx + my_data->largeur * (y - deby)) * p_images_ext->nb_band  +  j;
               (p_images_dest->images_table[i])[pr] = (p_images_ext->images_table[i])[p];
            }
         }
   }

   dprints("Taille image apres %s: %d x %d\n", __FUNCTION__, p_images_dest->sx,p_images_dest->sy);
   dprints("------------------------END OF %s-------------------------\n", __FUNCTION__);

}
