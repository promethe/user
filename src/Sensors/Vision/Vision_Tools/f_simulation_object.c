/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_simulation_object f_simulation_object
\brief 
 

\section Author 
-Name: BOUCENNA Sofiane
-Created: 22/04/2008

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
-none


\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

struct IMG_COL_SEL
{
  int prev_gpe;
  int X;
  int Y;
};






void function_simulation_object(int gpe_sortie)
{
  int i = 0;
  int n = 0;
  int j = 0;
  
  int deb_s = 0;
  int prev_gpe = 0;
  int num_liaison = 0;

  int X = 0;
  int Y = 0;  
  int pixel = 0;  
  struct IMG_COL_SEL *data = NULL;
  prom_images_struct *img = NULL;
  prom_images_struct *prev_img = NULL;

  char *st2;

  (void) num_liaison; // (unused)
  
  deb_s = def_groupe[gpe_sortie].premier_ele;
  
  if (def_groupe[gpe_sortie].data == NULL)
    {
      for (i = 0; i < nbre_liaison; i++)
	if (liaison[i].arrivee == gpe_sortie)
	  {
	    num_liaison = i;
	    prev_gpe = liaison[i].depart;
	    break;
	  }

      /*Position beginning of image in the window (default 0,0) */
      st2 = strstr(liaison[i].nom, "-PX");
      if ((st2 != NULL))
	{
	 
	  X = atoi(&st2[3]);
	  
	}

      st2 = strstr(liaison[i].nom, "-PY");
      if ((st2 != NULL))
	{
	 
	  Y = atoi(&st2[3]);

	}

      if (i == nbre_liaison)
        {
	  EXIT_ON_ERROR(
		 "f_simulation_object : Pas de lien en entree de f_simulation_object !\n");
	}
      
      if ((prev_img = ((prom_images_struct *) def_groupe[prev_gpe].ext)) == NULL)
	{
	  cprints(
		 "f_simulation_object : Pas d image en entree de f_simulation_object !\n");
	  return ;
	}
      
      if ((data =
	   (struct IMG_COL_SEL *) malloc(sizeof(struct IMG_COL_SEL))) ==
	  NULL)
        {
	  EXIT_ON_ERROR(
		 "function_simulation_object : Impossible d'allouer la memoire\n");
        }
      def_groupe[gpe_sortie].data = (void *) data;
      data->prev_gpe = prev_gpe;
      data->X = X;
      data->Y = Y;

      if ((img =
	   (void *) calloc_prom_image(prev_img->image_number, prev_img->sx,
				      prev_img->sy, 1)) == NULL)
        {
	  EXIT_ON_ERROR(
		 "f_img_simulation_object : Impossible d'allouer la memoire pour une nouvelle image\n");
	  
        }
      def_groupe[gpe_sortie].ext = (void *) img;
    }
  else
    {
      data = (struct IMG_COL_SEL *) def_groupe[gpe_sortie].data;
      prev_img = ((prom_images_struct *) def_groupe[data->prev_gpe].ext);
      img = ((prom_images_struct *) def_groupe[gpe_sortie].ext);
    }
  
  if (prev_img == NULL)
    return;

  n = prev_img->sx * prev_img->sy;
  
  for (i = 0; i < def_groupe[gpe_sortie].nbre; i++)
    {
      neurone[deb_s + i].s = neurone[deb_s + i].s1 = neurone[deb_s + i].s2 = 0;
    }
  
  /*La sortie du groupe sera X sur le premier neurone
   *Y sur le deuxieme neurone
   */
  
  neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = data->X;
  neurone[deb_s + 1].s = neurone[deb_s + 1].s1 = neurone[deb_s + 1].s2 = data->Y;

  /*
   *pixel=0     -->    noir
   *pixel=255   -->    blanc
   */

#ifdef DEBUG  
  dprints("taille_x=%d\n",img->sx);
  dprints("taille_y=%d\n",img->sy);
  dprints("X=%d,Y=%d\n",data->X,data->Y);
#endif

  pixel = data->Y * img->sx + data->X;
  for (i = 0; i < (int)prev_img->image_number; i++)
    {
      for (j = 0; j < n; j++)
        {
	  if(j == pixel )
	    {
	      img->images_table[i][j] = 255;
	    }
	  else
	    {
	      img->images_table[i][j] = 0;
	    }
        }
      
    }
 	
 
}

