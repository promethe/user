/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libSensors
\defgroup f_shift_to_north_image_panoramic f_shift_to_north_image_panoramic

\brief 
\file 
\section Author
-none

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 This function just shitf all the pixel of the image so that the left of the image is aligned to the north.
 Two algo link are requiered : the image link and the compass link.
\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
/*#define TIME_TRACE*/

#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <Global_Var/Vision.h>
#ifdef TIME_TRACE
#include <Global_Var/SigProc.h>
#endif
#include <Struct/prom_images_struct.h>
#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_shift_to_north_image_panoramic
{
    int GpeImage;
    int GpeCompass;
} MyData_f_shift_to_north_image_panoramic;


#define DEBUG
void function_shift_to_north_image_panoramic(int Gpe)
{

    int GpeImage = -1;
    int GpeCompass = -1;
    int l = -1, sx, sy, i, j, k;
    MyData_f_shift_to_north_image_panoramic *my_data;
    prom_images_struct *pt;
    prom_images_struct *pt_entree;
    float compass;
    int ip, dec;
#ifdef DEBUG
    dprints("entree function_shift_to_north_image_panoramic\n");
#endif
    if (def_groupe[Gpe].data == NULL)
    {
        i = 0;
        l = find_input_link(Gpe, i);
        while (l != -1)
        {

            if (strcmp(liaison[l].nom, "image") == 0)
                GpeImage = liaison[l].depart;

            if (strcmp(liaison[l].nom, "compass") == 0)
                GpeCompass = liaison[l].depart;
            i++;
            l = find_input_link(Gpe, i);

        }
#ifdef DEBUG
        dprints("GpeCompass=%d, GpeImage= %d\n", GpeCompass, GpeImage);
#endif
        if (GpeCompass == -1 || GpeImage == -1)
        {
            EXIT_ON_ERROR("f_shift_image_panoramic: pas pour les grp d'entree\n");
            exit(0);
        }

        pt = (prom_images_struct *)
            calloc_prom_image(((prom_images_struct *) (def_groupe[GpeImage].
                                                       ext))->image_number,
                              ((prom_images_struct *) (def_groupe[GpeImage].
                                                       ext))->sx,
                              ((prom_images_struct *) (def_groupe[GpeImage].
                                                       ext))->sy,
                              ((prom_images_struct *) (def_groupe[GpeImage].
                                                       ext))->nb_band);

        pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;
        if (pt_entree == NULL)
        {
            EXIT_ON_ERROR
                ("function_shift_to_north_image_panoramic n'a pas de groupe en entre avec une image\n");
            exit(0);
        }
        my_data = malloc(sizeof(MyData_f_shift_to_north_image_panoramic));
        my_data->GpeImage = GpeImage;
        my_data->GpeCompass = GpeCompass;
        def_groupe[Gpe].data =
            (MyData_f_shift_to_north_image_panoramic *) my_data;
        def_groupe[Gpe].ext = (prom_images_struct *) pt;
    }

    else
    {
        my_data =
            (MyData_f_shift_to_north_image_panoramic *) def_groupe[Gpe].data;
        GpeCompass = my_data->GpeCompass;
        GpeImage = my_data->GpeImage;
        pt = (prom_images_struct *) def_groupe[Gpe].ext;
        pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;

    }

#ifdef DEBUG
    dprints("debut code effectif\n");
#endif

    sx = ((prom_images_struct *) (def_groupe[GpeImage].ext))->sx;
    sy = ((prom_images_struct *) (def_groupe[GpeImage].ext))->sy;

    compass = neurone[def_groupe[GpeCompass].premier_ele].s1;

    for (k = 0; k < (int)pt_entree->image_number; k++)
        for (i = 0; i < sx; i++)
            for (j = 0; j < sy; j++)
            {

                dec = compass * sx;
                ip = i + dec;

                if (ip >= sx)
                    ip = ip - sx;

                if (pt->nb_band == 1)
                    pt->images_table[k][j * sx + i] =
                        pt_entree->images_table[k][j * sx + ip];
                else
                {
                    pt->images_table[k][(j * sx + i) * 3] =
                        pt_entree->images_table[k][(j * sx + ip) * 3];
                    pt->images_table[k][(j * sx + i) * 3 + 1] =
                        pt_entree->images_table[k][(j * sx + ip) * 3 + 1];
                    pt->images_table[k][(j * sx + i) * 3 + 2] =
                        pt_entree->images_table[k][(j * sx + ip) * 3 + 2];
                }

            }


#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Tine in function_acquisition\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    cprints("%s\n", MessageFunctionTimeTrace);
#endif



#ifdef DEBUG
    dprints("fin function_shift_to_north_image_panoramic\n");
#endif

}
