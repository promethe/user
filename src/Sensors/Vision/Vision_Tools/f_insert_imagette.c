/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file	f_insert_imagette.c
\author	D. Bailly
\date	04/02/2011
\brief 	Insert an image in a bigger one

Modified:
- author: xxxxx
- description: xxxxx
- date: xx/xx/xxxx

Theoritical description:
 - \f$  LaTeX equation: none \f$  


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / neural / math / stat
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <Struct/prom_images_struct.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG
*/

typedef struct
{
	int gpe_image;
	int gpe_imagette;
	int gpe_coord;
} Data_insert_imagette;




void new_insert_imagette(int index_of_group)
{

	Data_insert_imagette *data;
	int i=0, l;
	int gpe_image=-1, gpe_imagette=-1, gpe_coord=-1;
//	char buffer[32]; (unused)
	

#ifdef DEBUG
  printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
/**
* On recherche les liens et groupes en entree
*/
  
  l = find_input_link(index_of_group, i++);
  if(l==-1) EXIT_ON_ERROR("no input link\n");
  while (l != -1)
  {
    if (strcmp(liaison[l].nom, "-image") == 0)
    {
      if(gpe_image != -1)
        EXIT_ON_ERROR("more than one image input link ('image' on the link name)\n");
      gpe_image = liaison[l].depart; 
    }
    if (strstr(liaison[l].nom, "-imagette") != NULL)
    {
			if(gpe_imagette != -1)
        EXIT_ON_ERROR("more than one imagette input link ('imagette' on the link name)\n");
      gpe_imagette = liaison[l].depart; 
    }
    if (strstr(liaison[l].nom, "coord") != NULL)
    {
			if(gpe_coord != -1)
        EXIT_ON_ERROR("more than one coord input link ('coord' on the link name)\n");
      gpe_coord = liaison[l].depart;
    }
      
    l = find_input_link(index_of_group, i++);
  }
    /* Errors managing */
    if(gpe_image == -1) 
      EXIT_ON_ERROR("no image input link ('image' on the link name)\n");
    if(gpe_imagette == -1) 
      EXIT_ON_ERROR("no imagette input link ('imagette' on the link name)\n");
    if(gpe_coord == -1) 
      EXIT_ON_ERROR("no coord input link ('coord' on the link name)\n");
    if(def_groupe[index_of_group].nbre > 1) 
      PRINT_WARNING("there are unused neurons in this group (only one is used)\n");
		
		
		/* Creation of the data */
    data = (Data_insert_imagette *)ALLOCATION(Data_insert_imagette);
    data->gpe_image = gpe_image;
    data->gpe_imagette = gpe_imagette;
    data->gpe_coord = gpe_coord;
    def_groupe[index_of_group].data = data;


#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


/**
*	------------------- FIN DU CONSTRUCTEUR ------------------------
*/




void function_insert_imagette(int index_of_group)
{
	int i;
	int PosX, PosY, largeurI, hauteurI, largeuri, hauteuri, taille;
	int x, y, X, Y, p, pr;
	int debX, debY, debx = 0, deby = 0, finx, finy, fin;
	prom_images_struct *image, *imagette, *image_dest;
	Data_insert_imagette *data;

#ifdef DEBUG
  printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif

/**
* Recuperation donnees data
*/

	data = (Data_insert_imagette *) def_groupe[index_of_group].data;   
	if (data == NULL)
	{
		EXIT_ON_ERROR("error while loading data in group %d %s\n");
	}
 
/**
* Getting the extension of previous group
*/

	image = (prom_images_struct *) def_groupe[data->gpe_image].ext;
	imagette = (prom_images_struct *) def_groupe[data->gpe_imagette].ext;
	if (image == NULL)
  {
    PRINT_WARNING("No image in image input group (ext == NULL)\n");
		return;
  }

	largeurI = image->sx;
	hauteurI = image->sy;
	
/**
* Allocation image dans l'extension du groupe
*/
  if(def_groupe[index_of_group].ext == NULL)
    def_groupe[index_of_group].ext = (void *) calloc_prom_image(image->image_number, largeurI, hauteurI, image->nb_band);
    
  if (imagette == NULL)
  {
    PRINT_WARNING("No image in imagette input group (ext == NULL)\n");
		return;
  }



/**
* Recuperation de la valeur des deux neurones codant pour le milieu de l'imagette
* On prend la valeur du premier neurone de la boite 
* Ces valeurs sont multipliees par la taille en X ou Y de l'image pour retrouver le bon point
*/
	PosX = (int) (neurone[def_groupe[data->gpe_coord].premier_ele + 0].s1 *largeurI);
	PosY = (int) (neurone[def_groupe[data->gpe_coord].premier_ele + 1].s1 *hauteurI);
	largeuri = imagette->sx;
	hauteuri = imagette->sy;
#ifdef DEBUG
	printf("PosX : %d, PosY:%d\n",PosX,PosY);
#endif

/**
* Calcul limite imagette
*/
	debX = PosX - floor(largeuri / 2);
	finx = PosX + floor(largeuri / 2);
	if (debX < 0 )
	{
		debx = -debX;
		debX = 0;
	}
	if (finx > largeurI)
		finx = largeuri - finx + largeurI;
	else
		finx = largeuri;
	
	debY = PosY - floor(hauteuri / 2);
	finy = PosY + floor(hauteuri / 2);
	if (debY < 0 )
	{
		deby = -debY;
		debY = 0;
	}
	if (finy > hauteurI)
		finy = hauteuri - finy + hauteurI;
	else
		finy = hauteuri;

#ifdef DEBUG
	printf("debx: %d, finx: %d\t deby: %d, finy: %d\n",debx,finx,deby,finy);
#endif



	
	image_dest = (prom_images_struct *) def_groupe[index_of_group].ext;

	if (image_dest == NULL)
	{
		EXIT_ON_ERROR("pb of allocation of image\n");
	}

#ifdef DEBUG
	printf("p_images_dest : %d %d %d %d\n", image_dest->image_number, image_dest->sx,image_dest->sy, image_dest->nb_band);
#endif
	

	taille = largeurI * hauteurI * image_dest->nb_band;
/**
* Decoupage une a une des images
*/
	fin = image->image_number;
	for (i = 0; i < fin; i++)
	{
		memcpy(image_dest->images_table[i], image->images_table[i], taille*sizeof(char));
		for (x=debx, X=debX; x<finx; x++, X++)
		{
			for (y=deby, Y=debY; y<finy; y++, Y++)
			{
	      p = X + largeurI * Y;
	      pr = x + largeuri * y;
	      (image_dest->images_table[i])[p] = (imagette->images_table[i])[pr];
			}
		}
	}
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}


void destroy_insert_imagette(int index_of_group)
{
  
#ifdef DEBUG
  printf("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  free(def_groupe[index_of_group].data);
  
#ifdef DEBUG
  printf("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}

/*#undef DEBUG
*/
