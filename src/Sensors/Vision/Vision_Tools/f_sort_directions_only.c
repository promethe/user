/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\ingroup libSensors
\defgroup f_sort_directions f_sort_directions
\file  f_sort_directions.c
\brief Create images of the intensity of directions

\section Modified
Author: R.Braud
Created: 16/04/2012
Modified:
- author:Syed Khursheed Hasnain
- description: specific file creation
- date: 15/05/2013

Theoritical description:

\details
 f_sort_direction provides specified directions along with amplitude, however  f_sort_directions_only gives only the directions.


\section Options
-	-Dxxx		: 	Number of subdivision of direction, consequently, of created images. Give direction with a precision of 360/xxx° (default = 6)

-	-Tau1=xxx	: 	The value of tau used in the computation of the gaussian for the angle. (default = 30 : good for 6 directions)

-	-Tau2=xxx	: 	The value of tau used in the computation of the gaussian for the intensity. (default = 5)


Macro:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs: none (yet!)

Todo: see the author for comments

\file
\ingroup f_sort_direction

http://www.doxygen.org
 ************************************************************/


#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>


typedef struct My_Data
{
	int InputGpe;
	prom_images_struct *prom_Original;
	prom_images_struct *prom_OutputImage;
	int directions;
	float tau1, tau2;
	int *sumDir;
	int moyDirIte;
	int dirMaxMoy;
}My_Data;


void function_sort_directions_only(int Gpe)
{
	int InputGpe = -1;
	prom_images_struct *prom_Original = NULL;
	prom_images_struct *prom_OutputImage = NULL;
	char param[255];
	My_Data *Mydata;
   int row, column, rc;
	float *ima_output = NULL;
	float *ima_input_dir = NULL;
	float *ima_input_norm = NULL;
	float tau1 = 30, tau2 = 5, accuracy;
	int directions = 6, dir, i = 0, l = -1;
	float gap; /*, gapMax, intensity;*/

	dprints("function_sort_direction\n");



	/********************
	 ** INITIALIZATION **
	 ********************/
	/*Get options on the link */
	if (def_groupe[Gpe].data == NULL)
	{
		i=0;
		l = find_input_link(Gpe, i);
		while (l != -1)
		{
			InputGpe = liaison[l].depart;

			if(prom_getopt(liaison[l].nom, "-D", param) == 2)
			{
				directions = atoi(param);
				prom_Original = (prom_images_struct *) def_groupe[InputGpe].ext;
				dprints("D = %d\n", directions);
			}

			if (prom_getopt(liaison[l].nom, "-Tau1=", param) == 2)
			{
				tau1 = atof(param);
				dprints("T = %f\n", tau1);
			}

			if (prom_getopt(liaison[l].nom, "-Tau2=", param) == 2)
			{
				tau2 = atof(param);
				dprints("T = %f\n", tau2);
			}

			i++;
			l = find_input_link(Gpe, i);

		}

		if ((InputGpe == -1) || (prom_Original == NULL) )
		{
			EXIT_ON_ERROR("%s : Need 1 group with ext in input (with -Dxxx,)\n", __FUNCTION__);
		}


		/* Allocation */
		Mydata=(My_Data*)ALLOCATION(My_Data);
		if(Mydata == NULL)
		{
			EXIT_ON_ERROR("%s : Malloc error Mydata\n", __FUNCTION__);
		}

		prom_OutputImage = calloc_prom_image(directions+1, prom_Original->sx, prom_Original->sy, 4);////////////////////////////// +1
		if(prom_OutputImage==NULL)
		{
			EXIT_ON_ERROR("%s : Malloc error prom_OutputImage\n", __FUNCTION__);
		}


		def_groupe[Gpe].ext = (prom_images_struct *)prom_OutputImage;


		Mydata->InputGpe = InputGpe;
		Mydata->prom_Original=prom_Original;
		Mydata->prom_OutputImage=prom_OutputImage;
		Mydata->directions = directions;
		Mydata->tau1= tau1;
		Mydata->tau2= tau2;

		def_groupe[Gpe].data = (void *)Mydata;
	}
	else
	{
		Mydata = (My_Data *) def_groupe[Gpe].data;

		InputGpe = Mydata->InputGpe;
		prom_Original = Mydata->prom_Original;
		prom_OutputImage = Mydata->prom_OutputImage;
		directions = Mydata->directions;
		tau1 = Mydata->tau1;
		tau2 = Mydata->tau2;
	}


	/************************
	 ** END INITIALIZATION **
	 ************************/

	ima_input_norm  = (float *)prom_Original->images_table[2];
	ima_input_dir  = (float *)prom_Original->images_table[3];
	accuracy = (directions > 0) ?  360./(float)directions : 0.;

	if(ima_input_norm == NULL || ima_input_dir == NULL)
	{
		EXIT_ON_ERROR("%s : Problem with input images_table\n", __FUNCTION__);
	}

	for( dir = 0; dir < directions; dir++){

		ima_output = (float *)prom_OutputImage->images_table[dir];
		for( row = 0; row < prom_Original->sy; row++ )
      {
			for( column = 0; column < prom_Original->sx; column++ )
         {
				rc = row*prom_Original->sx + column;

				gap = (float) fabs( dir*(int)accuracy - (( (float)ima_input_dir[ rc ] > 360. - accuracy/2.) ? (360. - (float)ima_input_dir[ rc ]) : (float)ima_input_dir[ rc ]) );
				/*gapMax = accuracy/2.;
				intensity = (1-exp(-(float)(ima_input_norm[ rc ]*ima_input_norm[ rc ])/(2*tau2*tau2)));
				ima_output[ rc ] = intensity * exp( -gap*gap / (2*tau1*tau1) );*/
				ima_output[ rc ] = exp( -gap*gap / (2*tau1*tau1) );
			}
		}
	}
}
