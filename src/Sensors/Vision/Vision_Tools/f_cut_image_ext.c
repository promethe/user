/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_cut_image_ext f_cut_image_ext
   \ingroup libSensors

   \brief extract sub image in ext

   \details

   \section Description
   Decoupe l'image dans un cadre coord x1y1 x2y2 inclus
   de l'extension precedente: il y a copie memoire !

   \file
   \ingroup f_cut_image_ext
   \brief

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 11/08/2004

   External Tools:
   -tools/Vision/Vision/calloc_prom_image()
   -tools/Vision/Vision/free_prom_image()

   http://www.doxygen.org
**********************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_cut_image_ext
{
   int gpe_image;
   int x1;
   int x2;
   int y1;
   int y2;
} data_cut_image_ext;

void new_cut_image_ext(int gpe)
{
   int link, gpe_image;
   int x1 = 0, x2 = -1, y1 = 0, y2 = -1;
   char string[256];
   data_cut_image_ext *my_data = NULL;

   dprints("new_cut_image_ext(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {

      link = find_input_link(gpe, 0);
      if (link == -1)
      {
         EXIT_ON_ERROR("Missing input link");
      }

      gpe_image = liaison[link].depart;

      if (prom_getopt(liaison[link].nom, "x", string) == 2)
      {
         x1 = atoi(string);
      }

      if (prom_getopt(liaison[link].nom, "X", string) == 2)
      {
         x2 = atoi(string);
      }

      if (prom_getopt(liaison[link].nom, "y", string) == 2)
      {
         y1 = atoi(string);
      }

      if (prom_getopt(liaison[link].nom, "Y", string) == 2)
      {
         y2 = atoi(string);
      }

      my_data = ALLOCATION(data_cut_image_ext);
      my_data->gpe_image = gpe_image;
      my_data->x1 = x1;
      my_data->x2 = x2;
      my_data->y1 = y1;
      my_data->y2 = y2;
      def_groupe[gpe].data = my_data;
   }

   dprints("new_cut_image_ext(%s): Leaving function\n", def_groupe[gpe].no_name);
}

void function_cut_image_ext(int gpe)
{
   int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
   int width, height, nb_band;
   int i, j, x, y, p , pr;
   prom_images_struct *p_images_ext, *p_images_dest;
   data_cut_image_ext *my_data = (data_cut_image_ext *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retrieve data");
   }

   x1 = my_data->x1;
   x2 = my_data->x2;
   y1 = my_data->y1;
   y2 = my_data->y2;

   p_images_ext = (prom_images_struct *) def_groupe[my_data->gpe_image].ext;
   if (p_images_ext == NULL)
   {
      EXIT_ON_ERROR("NULL pointer for input image");
   }

   if (x2 == -1)
   {
      x2 = p_images_ext->sx;
   }

   if (y2 == -1)
   {
      y2 = p_images_ext->sy;
   }

   width = x2 - x1;
   height = y2 - y1;

   if (def_groupe[gpe].ext == NULL)
   {
      if (x1 > p_images_ext->sx || x2 > p_images_ext->sx || y1 > p_images_ext->sy || y2 > p_images_ext->sy)
      {
         EXIT_ON_ERROR("Cut area is out of bounds for input image");
      }

      p_images_dest = calloc_prom_image(p_images_ext->image_number, width, height, p_images_ext->nb_band);
      def_groupe[gpe].ext = p_images_dest;
   }
   else
   {
      p_images_dest = (prom_images_struct *) def_groupe[gpe].ext;
   }

   nb_band = p_images_dest->nb_band;

   for (i = 0; i < p_images_ext->image_number; i++)
   {
      for (x = 0; x < width; x++)
      {
         for (y = 0; y < height; y++)
         {
            for (j = 0; j < nb_band; j++)
            {
               p = (x + x1 + p_images_ext->sx * (y + y1)) * nb_band + j;
               pr = (x + y * width) * nb_band + j;
               p_images_dest->images_table[i][pr] = p_images_ext->images_table[i][p];
            }
         }
      }
   }
}

void destroy_cut_image_ext(int gpe)
{
   data_cut_image_ext *my_data = NULL;

   dprints("destroy_cut_image_ext(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data != NULL)
   {
      my_data = (data_cut_image_ext *) def_groupe[gpe].data;

      free(my_data);
      def_groupe[gpe].data = NULL;
   }

   if (def_groupe[gpe].ext != NULL)
   {
      free_prom_image(def_groupe[gpe].ext);
      def_groupe[gpe].ext = NULL;
   }

   dprints("destroy_cut_image_ext(%s): Entering function\n", def_groupe[gpe].no_name);
}
