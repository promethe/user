/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************

\defgroup f_patch_normalization f_patch_normalization
\ingroup Sensors
\file  f_patch_normalization.c

\brief Image preprocessing : Patch normalization
Description:
Permet de normaliser les pixels de l'image par rapport à leur voisinage

Author: Marwen Belkaid
Created: 15/04/2015

\section Restrictions
Only works with uchar grayscale images (nb_band = 1)

\section Modes
PATCH_NORM_MODE_FULLDYN: Output pixels cover the whole dynamics, i.e. [min_o;max_o] => [0;255]
PATCH_NORM_MODE_CENTERED: Output pixels are centered, i.e. zero corresponds to 127

\section Input Links
Mandatory :
- -image : indicates the input group that contains the grayscale image
Optional :
- -psize : indicates the patch size (default is 11)
- -mc : switches from PATCH_NORM_MODE_FULLDYN (default) to PATCH_NORM_MODE_CENTERED


http://www.doxygen.org
************************************************************/

//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#define DEFAULT_PSIZE 11

typedef enum patch_normalization_mode
{
   PATCH_NORM_MODE_FULLDYN,
   PATCH_NORM_MODE_CENTERED
} patch_normalization_mode ;

typedef struct data_patch_normalization
{
   int input_gpe ;
   int patch_size ;
   int sx ;
   int sy ;
   float *output_buffer ;
   patch_normalization_mode mode;
} data_patch_normalization;


void new_patch_normalization(int gpe)
{
   data_patch_normalization* mydata ;
   int tmp_input_gpe, tmp_patch_size = DEFAULT_PSIZE ;
   patch_normalization_mode tmp_mode = PATCH_NORM_MODE_FULLDYN ;
   int i = 0, links_in ;
   char tmp_input_value[5];

   tmp_input_gpe = -1;

   /* Get Input group */
   while((links_in = find_input_link (gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "-image", NULL) == 1)
         tmp_input_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-psize", tmp_input_value) == 2)
         tmp_patch_size = atoi(tmp_input_value) ;
      if(prom_getopt(liaison[links_in].nom, "-mc", tmp_input_value) == 1)
         tmp_mode = PATCH_NORM_MODE_CENTERED ;

      i++ ;
   }

   /* Check inputs */
   if(tmp_input_gpe < 0)
      EXIT_ON_ERROR("%s needs an input link named \"-image\" \n", __FUNCTION__) ;

   /* Create object */
   mydata = ALLOCATION(data_patch_normalization);
   mydata->input_gpe = tmp_input_gpe ;
   mydata->patch_size = tmp_patch_size ;
   mydata->mode = tmp_mode ;
   mydata->sx = -1 ;
   mydata->sy = -1 ;
   def_groupe[gpe].data = (data_patch_normalization *) mydata;


}

void function_patch_normalization(int gpe)
{
   float *tmp_output_buffer ;
   int tmp_input_gpe, tmp_sx, tmp_sy, tmp_patch_size;
   patch_normalization_mode tmp_mode ;
   data_patch_normalization *mydata ;
   unsigned char *input_img, *output_img ;
   int i,j,k, tmp_nb_pixel;
   int startx, endx, starty, endy, centerx, centery, dist, size ;
   float mean, stdev;
   float min_o_f, max_o_f, norm;
#ifdef DEBUG
   unsigned char max_i, max_o, min_i, min_o;
#endif

   /* get params from local structure */
   mydata = (data_patch_normalization *) def_groupe[gpe].data ;
   tmp_input_gpe = mydata->input_gpe ;
   tmp_mode = mydata->mode ;
   tmp_patch_size = mydata->patch_size ;

   tmp_sx = tmp_sy = tmp_nb_pixel = 0 ; // avoid warning
   mean = stdev = min_o_f = max_o_f = norm = 0. ;

   /* check input ext */
   if(def_groupe[tmp_input_gpe].ext == NULL)
      EXIT_ON_ERROR("The input group %s (%s) must have an ext \n", def_groupe[tmp_input_gpe].nom, def_groupe[tmp_input_gpe].no_name) ;
   if(((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->nb_band != 1)
      EXIT_ON_ERROR("%s This function only works with uchar grayscale images (nb_band = 1) \n", __FUNCTION__) ;
   if(def_groupe[gpe].ext == NULL)
   {
      mydata->sx = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sx ;
      mydata->sy = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sy ;
      def_groupe[gpe].ext = (prom_images_struct *) calloc_prom_image(1, mydata->sx, mydata->sy, 1);
      mydata->output_buffer = MANY_ALLOCATIONS(mydata->sx * mydata->sy, float) ;
   }

   /* set local variables */
   tmp_sx = mydata->sx ;
   tmp_sy = mydata->sy ;
   tmp_output_buffer = mydata->output_buffer ;
   tmp_nb_pixel = tmp_sx * tmp_sy ;
   input_img = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->images_table[0] ;
   output_img = ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] ;

   min_o_f = 255. ;
   max_o_f = -255. ;
#ifdef DEBUG
   min_o = min_i = 255 ;
   max_o = max_i = 0 ;
#endif


   dist = floor(tmp_patch_size / 2) ;

   /* compute patch normalized image (float) */
   for(i = 0 ; i < tmp_nb_pixel ; i++)
   {
      /* patch position and limits */
      centerx = i % tmp_sx ;
      centery = i / tmp_sx ;
      startx = centerx - dist ;
      endx = centerx + dist;
      starty = centery - dist ;
      endy = centery + dist;
      if(startx<0)  startx=0 ;
      if(endx>=tmp_sx)    endx=tmp_sx-1 ;
      if(starty<0)  starty=0 ;
      if(endy>=tmp_sy)    endy=tmp_sy-1 ;
      size = (endx - startx + 1)*(endy - starty + 1);

      dprints(" \t\t %d, %d, %d, %d, %d, %d \n\n", centerx, centery, startx, endx, starty, endy) ;


      /* calculate mean and stdev */
      for(j=starty; j<=endy; j++)
         for(k=startx; k<=endx; k++)
            mean += (float)input_img[k + j*tmp_sx] ;
      mean /= (float)size ;
      for(j=starty; j<=endy; j++)
         for(k=startx; k<=endx; k++)
            stdev+= ((float)input_img[k + j*tmp_sx] - mean)*((float)input_img[k + j*tmp_sx] - mean);
      stdev = sqrt(stdev/(float)size);
      dprints(" \t\t mean = %f, stdev = %f\n\n",  mean,  stdev) ;


      /* get patch normalized pixels */
      tmp_output_buffer[i] = (((float)input_img[i]-mean)/stdev) ;
#ifdef DEBUG
         if (input_img[i] > max_i)
            max_i = input_img[i];
         else if (input_img[i] < min_i)
            min_i = input_img[i];
#endif
      if (tmp_output_buffer[i] > max_o_f)
         max_o_f = tmp_output_buffer[i];
      else if (tmp_output_buffer[i] < min_o_f)
         min_o_f = tmp_output_buffer[i];

   }
   switch(tmp_mode)
   {
      case PATCH_NORM_MODE_FULLDYN :
         norm = max_o_f - min_o_f ;
         break;
      case PATCH_NORM_MODE_CENTERED :
         norm = fmaxf(max_o_f, abs(min_o_f)) ;
         break;
   }


   /* normalize and copy patch normalized image (uchar) */
   for(i = 0 ; i < tmp_nb_pixel ; i++)
   {
      switch(tmp_mode)
      {
         case PATCH_NORM_MODE_FULLDYN :
            output_img[i] = (unsigned char) ((tmp_output_buffer[i] - min_o_f)/norm * 255.) ;
            break;
         case PATCH_NORM_MODE_CENTERED :
            output_img[i] = (unsigned char) ((tmp_output_buffer[i] + norm)/(2*norm) * 255.) ;
            break;
      }

#ifdef DEBUG
      if (output_img[i] > max_o)
         max_o = output_img[i];
      else if (output_img[i] < min_o)
         min_o = output_img[i];
#endif
   }

   dprints("\t\t min_i = %d, max_i = %d\n\n",  min_i,  max_i) ;
   dprints("\t\t min_o = %d, max_o = %d\n\n",  min_o,  max_o) ;
   dprints("\t\t min_o_f= %f, max_o_f = %f\n\n",  min_o_f,  max_o_f) ;


}




void destroy_patch_normalization(int gpe)
{
   dprints("destroy %s(%s) \n", __FUNCTION__,def_groupe[gpe].no_name);
   if (def_groupe[gpe].ext != NULL)
   {
      free(((prom_images_struct *) def_groupe[gpe].ext)) ;
      def_groupe[gpe].ext = NULL;
   }
   if (((data_patch_normalization *) def_groupe[gpe].data)->output_buffer != NULL)
   {
     free(((float *) ((data_patch_normalization *) def_groupe[gpe].data)->output_buffer));
     ((data_patch_normalization *) def_groupe[gpe].data)->output_buffer = NULL;
   }
   if (def_groupe[gpe].data != NULL)
   {
     free(((data_patch_normalization *) def_groupe[gpe].data));
     def_groupe[gpe].data = NULL;
   }
}

