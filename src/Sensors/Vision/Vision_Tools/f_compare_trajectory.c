/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_compare_trajectory f_compare_trajectory
 

\section Modified 
-Name: C.Giovannangeli
-Created: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Author
- name : B Mariat
- date: 09/03/2005

\section Description
-garther the softscalling and hardscalling functions in one
-Capture des images 


\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_compare_trajectory(int gpe_sortie)
{
    int  test,cpt,i,j,nb_pt_opt=0,nb_pt_real=0;
    float ep,et,dmin,dist;
    FILE * file_traj_opt=NULL;
    FILE * file_traj_real=NULL;
    int lien_entrant = -1;
    char param_link[256];
    float traj_opt[1000][2],traj_real[10000][2];
    TxPoint pt1,pt2;
   
	cprints("enter function_compare_trajectory\n");
	cpt=0;
        lien_entrant = find_input_link(gpe_sortie, cpt);
	while(lien_entrant!=-1)
	{
		if (prom_getopt(liaison[lien_entrant].nom, "-o", param_link) == 2)
		{
			cprints("traj_opt:%s\n",param_link);
			file_traj_opt=fopen(param_link,"r+");
			if(file_traj_opt==NULL)
			{
				cprints("probl�me d'ouverture de fichier dans function_compare_trajectory\n");
				exit(0);
			}
			nb_pt_opt=0;
			while(!feof(file_traj_opt))
			{
				test=fscanf(file_traj_opt,"%f %f\n",&(traj_opt[nb_pt_opt][0]),&(traj_opt[nb_pt_opt][1]));
				if(test == 0){kprints("Probleme pendant la lecture du fichier dans f_compare_trajectory");}
		/* 		printf("%f %f\n",traj_opt[nb_pt_opt][0],traj_opt[nb_pt_opt][1]);*/
				nb_pt_opt++;
			}
		}
		if (prom_getopt(liaison[lien_entrant].nom, "-r", param_link) == 2)
		{
			file_traj_real = fopen(param_link,"r+");
			if(file_traj_real==NULL)
			{
				cprints("probl�me d'ouverture de fichier dans function_compare_trajectory\n");
				exit(0);
			}	
			nb_pt_real=0;
			while(!feof(file_traj_real))
			{
				test=fscanf(file_traj_real,"%f %f\n",&traj_real[nb_pt_real][0],&traj_real[nb_pt_real][1]);
				if(test == 0){kprints("Probleme pendant la lecture du fichier dans f_compare_trajectory");}
				nb_pt_real++;
			}
	
		}
		
		cpt++;
		lien_entrant = find_input_link(gpe_sortie, cpt);
	}
	cprints("nb_pt_opt=%d\n",nb_pt_opt);
	cprints("nb_pt_real=%d\n",nb_pt_real);
	if(nb_pt_real==0||nb_pt_opt==0)
		{
			cprints("prbl�me: pas de point opt ou real dans function_compare_trajectory\n");
			exit(0);
		}

	ep=0;
	for(i=0;i<nb_pt_opt;i++)
	{
		dmin=9999.;	
		for(j=0;j<nb_pt_real;j++)
		{
			dist =
            			sqrt((traj_opt[i][0] - traj_real[j][0]) * (traj_opt[i][0] - traj_real[j][0]) +
                 			(traj_opt[i][1] - traj_real[j][1]) * (traj_opt[i][1] - traj_real[j][1]));

			if (dist <= dmin)
				dmin = dist;
			
		}	
		ep=ep+dmin;	
	}
	ep=ep/nb_pt_opt;

	et=0;
	for(i=0;i<nb_pt_real;i++)
	{
		dmin=9999.;	
		for(j=0;j<nb_pt_opt;j++)
		{
			dist =
            			sqrt((traj_opt[j][0] - traj_real[i][0]) * (traj_opt[j][0] - traj_real[i][0]) +
                 			(traj_opt[j][1] - traj_real[i][1]) * (traj_opt[j][1] - traj_real[i][1]));

			if (dist <= dmin)
				dmin = dist;
			
		}	
		et=et+dmin;	
	}
	et=et/nb_pt_real;

	cprints("resultat:\n\tep=%f\n\tet=%f\n",ep,et);

#ifndef AVEUGLE
	/*affiche traj_opt*/
	for(i=0;i<nb_pt_opt-1;i++)
	{
		pt1.x=traj_opt[i][0];
		pt1.y=traj_opt[i][1];
		pt2.x=traj_opt[i+1][0];
		pt2.y=traj_opt[i+1][1];
 		TxDessinerSegment(&image1, bleu, pt1, pt2, 3);
	}
	for(i=0;i<nb_pt_real-1;i++)
	{
		pt1.x=traj_real[i][0];
		pt1.y=traj_real[i][1];
		pt2.x=traj_real[i+1][0];
		pt2.y=traj_real[i+1][1];
		TxDessinerSegmentPointille(&image1, rouge, pt1, pt2, 3);
	}
	TxFlush(&image1);

#endif

(void) pt1;
(void) pt2;
}
