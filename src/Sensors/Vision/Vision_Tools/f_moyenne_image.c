/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libSensors
\defgroup f_moyenne_image f_moyenne_image
\brief 


\section Author
BOUCENNA Sofiane
Created: 22/04/2008


\section Description

 
\section Macro
-none

\section Local varirables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function
\file f_img_gris_selection.c 

http://www.doxygen.org
*/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

typedef struct		s_moyenne_image
{
  prom_images_struct	*prev_ext;
  prom_images_struct	*mem;
  float			*moyennes;
  float			big_moyenne;
  int			m;
  int			nb_image;
  int			nb_pixel;
  int			last_image;
}			t_moyenne_image;

void function_moyenne_image(int gpe)
{
  t_moyenne_image	*data = NULL;
  prom_images_struct	*prev_ext = NULL;
  prom_images_struct	*mem = NULL;
  prom_images_struct	*cur = NULL;
  float			*moyennes = NULL;
  float			cur_moyenne = 0;
  float			big_moyenne = 0;
  int			m = -1;
  int			nb_image;
  int			nb_pixel = -1;
  int			i = 0;
  int			j = 0;
  int			last_image = -1;

  (void) j;
  if (def_groupe[gpe].data == NULL)
    {
      int		prev_gpe = -1;
      int		l = -1;
      char		s_m[16];

      if ((data = malloc(sizeof(t_moyenne_image))) == NULL)
	{
	  fprintf(stderr, "function_moyenne_image : allocation error\n");
	  exit(1);
	}
      l = find_input_link(gpe, 0);
      prev_gpe = liaison[l].depart;

      data->prev_ext = (prom_images_struct *)def_groupe[prev_gpe].ext;

      data-> nb_pixel = data->prev_ext->sx * data->prev_ext->sy;

      if (prom_getopt(liaison[l].nom, "m", s_m) == 2)
	data->m = atoi(s_m);

      data->mem = calloc_prom_image(data->m, data->prev_ext->sx, data->prev_ext->sy, 1);
      if (data->mem == NULL)
	{
	  fprintf(stderr, "function_moyenne_image : allocation error for %i images\n", data->m);
	  exit(2);
	}
      if ((data->moyennes = malloc(data->m * sizeof(float))) == NULL)
	{
	  fprintf(stderr, "function_moyenne_image : allocation error for %i moyennes\n", data->m);
	  exit(3);
	}

      data->nb_image = 0;
      data->last_image = 0;

      def_groupe[gpe].data = (void *)data;

      
      def_groupe[gpe].ext = calloc_prom_image(1, data->prev_ext->sx, data->prev_ext->sy, 1);
      if (def_groupe[gpe].ext == NULL)
	{
	  fprintf(stderr, "function_moyenne_image : allocation error for ext image\n");
	  exit(4);
	}
      def_groupe[gpe].video_ext = def_groupe[gpe].ext;
    }
  else
    data = (t_moyenne_image *)def_groupe[gpe].data;
  m = data->m;
  prev_ext = data->prev_ext;
  mem = data->mem;
  nb_image = data->nb_image;
  moyennes = data->moyennes;
  last_image = data->last_image;
  cur = def_groupe[gpe].ext;
  nb_pixel = data->nb_pixel;

  /* calcul la moyenne sur les n images precedentes */
  for (i = 0; i < nb_image; i++)
    big_moyenne += moyennes[i];
  if (nb_image > 0)
    big_moyenne /= nb_image;
  
  /* calcul la moyenne de l image courrante et on la copie en image memoire */
  for (i = 0; i < nb_pixel; i++)
    {
      moyennes[last_image] += prev_ext->images_table[0][i];
      mem->images_table[last_image][i] = prev_ext->images_table[0][i];
    }
  cur_moyenne = moyennes[last_image] /= nb_pixel;

  data->last_image = (data->last_image + 1) % m;
  if (nb_image < m)
    data->nb_image++;

  for (i = 0; i < nb_pixel; i++)
    {
      int	p = 0;
      
      p = (((float)((float)prev_ext->images_table[0][i]) / cur_moyenne) * big_moyenne);
      if (p < 0)
	cur->images_table[0][i] = 0;
      else if (p > 255)
	cur->images_table[0][i] = 255;
      else
	cur->images_table[0][i] = (unsigned char)p;
    }
}
