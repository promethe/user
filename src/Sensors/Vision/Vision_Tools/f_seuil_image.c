/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_seuil_image.c 
\brief 

Author: Mickael Maillard 
Created: 09/07/04
Modified:
- author: P. Gaussier
- description: ajout de la possiblite de compiler pour gere des images chars ou des floats
- date: 13/05/09

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction permet de seuiller une image en fonction d'un parametre seuil.
f_seuil_image travaille sur une image float en entree et produit une image float en sortie.
f_seuil_image_char idem mais avec des chars.
Si le pixel est en dessous du seuil, il est mis a zero.
On recupere ensuite l'image seuillee dans OutputImage.
Les images doivent etre en floatant N&B ou en floatant couleur (nb_band = 4 ou 12).

Parametre : -s, seuil (par defaut egal 0).
 
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#include <Kernel_Function/find_input_link.h>

#undef DEBUG

 
#define DEF_SEUIL 0
#define MON_TYPE char

void function_seuil_image_char(int Gpe)
#include "f_seuil_image.c.base"

#undef DEF_SEUIL
#undef MON_TYPE
#define DEF_SEUIL 0.
#define MON_TYPE_float 
#define MON_TYPE float

void function_seuil_image(int Gpe)
#include "f_seuil_image.c.base"
