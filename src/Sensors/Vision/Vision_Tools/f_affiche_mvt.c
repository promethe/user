/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_affiche_mvt.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_affiche_mvt(int numero)
{
#ifndef AVEUGLE
    double mvt;
    double x, y;
    int l, i, j, dir;
    int GpeX, debX, longueurX,
        GpeY, debY, longueurY,
        GpeMVT, debMVT, longueurMVT,
        GpeDIR, debDIR, longueurDIR,
        fen_posx, fen_posy, color;
    TxPoint point, point2;
    void *fenetre = &image1;
    char param[255];
    l = 0; i = 0; j = 0; dir = 0;
    x = 0.0; y = 0.0;
    GpeX = -1;
    GpeY = -1;
    GpeMVT = -1;
    GpeDIR = -1;
    fen_posx = 0; fen_posy = 0; color = bleu;

    (void) longueurY; // (unused)
    (void) longueurMVT; // (unused)
    (void) longueurDIR; // (unused)


    l = find_input_link(numero, i);
    while (l != -1)
    {
        if (prom_getopt(liaison[l].nom, "x", param) >= 1)
            GpeX = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "y", param) >= 1)
            GpeY = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "mvt", param) >= 1)
            GpeMVT = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "dir", param) >= 1)
            GpeDIR = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "I1", param) >= 1)
            fenetre = &image1;

        if (prom_getopt(liaison[l].nom, "I2", param) >= 1)
            fenetre = &image2;

        if (prom_getopt(liaison[l].nom, "PX", param) == 2)
            fen_posx = atoi(param);

        if (prom_getopt(liaison[l].nom, "PY", param) == 2)
            fen_posy = atoi(param);

        i++;
        l = find_input_link(numero, i);
    }

    debX = def_groupe[GpeX].premier_ele;
    longueurX = def_groupe[GpeX].nbre;

    debY = def_groupe[GpeY].premier_ele;
    longueurY = def_groupe[GpeY].nbre;

    debMVT = def_groupe[GpeMVT].premier_ele;
    longueurMVT = def_groupe[GpeMVT].nbre;

    debDIR = def_groupe[GpeDIR].premier_ele;
    longueurDIR = def_groupe[GpeDIR].nbre;

    for (i = 0; i < longueurX; i++)
    {
        x = neurone[debX + i].s1;
        y = neurone[debY + i].s1;

        /* Calcul dependant de la taille de l'image et de celle du champ representant le mvt */
        j = (int) ((613 * x - 25) * (560. / 563));

        mvt = neurone[debMVT + j].s1;
        dir = neurone[debDIR + j].s1;

        if (mvt > 0.)
        {
            if (dir == -1)
            {
                point.x = 613 * x + fen_posx;
                point.y = 107 * y + fen_posy;

                point2.x = point.x - 25 * (1 - mvt);
                point2.y = point.y;

                if (mvt <= 0.25)
                    color = bleu;
                else if (mvt <= 0.5)
                    color = vert;
                else
                    color = jaune;

                TxDessinerSegment(fenetre, color, point, point2, 1);
            }
            else if (dir == 1)
            {
                point.x = 613 * x + fen_posx;
                point.y = 107 * y + fen_posy;

                point2.x = point.x + 25 * (1 - mvt);
                point2.y = point.y;

                if (mvt <= 0.25)
                    color = bleu;
                else if (mvt <= 0.5)
                    color = vert;
                else
                    color = jaune;

                TxDessinerSegment(fenetre, color, point, point2, 1);
            }
        }
    }

    TxFlush(&image1);
    TxFlush(&image2);

#endif
	(void) numero; // (unused)
}
