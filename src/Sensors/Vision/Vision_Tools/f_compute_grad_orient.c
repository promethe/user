/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_compute_grad_orient.c 
\brief Calcul d'une carte des gradients et d'une carte des orientations

Author: Mickael Maillard
Created: 01/04/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction calculant la norme du gradient sur la premiere map et donnant l'orientation dans 0->360 sur la deuxieme map de la prom_image_struct
Le gradient est la norme du vecteur {(P(x+1,y) - P(x-1,y)) ; (P(x,y+1) - P(x-1,y-1))}
L'orientation correspond à l'atan2 des  deux composants.
L'image entrante doit etre en N&B en floatant.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#undef DEBUG
/*#define DEBUG*/
void function_compute_grad_orient(int Gpe)
{
    int inputGpe = -1;
    float *outputGradImage, *outputOrientImage, *inputImage;
    float deltaY, deltaX, angle;
    int i, row, column;
    int sx, sy;

#ifdef DEBUG
    printf("debut Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        /*recherche du numero de groupe entrant et gestion des erreurs si inexistant */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                inputGpe = liaison[i].depart;
                break;
            }
        }
        if (inputGpe == -1)
        {
            printf("%s : il manque un lien entrant au groupe %d\n",
                   __FUNCTION__, Gpe);
            exit(0);
        }

        if (def_groupe[inputGpe].ext == NULL)
        {
            printf
                ("%s : le pointeur du lien entrant n'a pas ete initialise\n",
                 __FUNCTION__);
            /*exit(0); */
            return;
        }

        if (((prom_images_struct *) def_groupe[inputGpe].ext)->nb_band != 4)
        {
            printf
                ("%s Cette fonction ne fait que les images N&B en floatant...\n",
                 __FUNCTION__);
            exit(0);
        }

        /*recuperation des infos de l'image en amont */
        sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;

        /*allocation de memoire pour la structure resulante */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
#ifdef DEBUG
        printf("temp Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif


        /*les infos sont ecrite dans la structure de sortie */
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 2;

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            ((prom_images_struct *) def_groupe[inputGpe].ext)->
            images_table[0];

        /*allocation de la memoire pour les cartes de sortie */
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
            malloc(sx * sy * sizeof(float));
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1]) =
            malloc(sx * sy * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL
            || ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1] == NULL)
        {
            printf("%s : %d : Allocation de memoire impossible\n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }

    }
    else
    {
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    }


    inputImage =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[2]);
    outputGradImage =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    outputOrientImage =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];

    /*calcul de la norme du gradient et de l'orientation */
    for (row = 0; row < sy; row++)
    {
        for (column = 0; column < sx; column++)
        {
            if (row != 0 && row != (sy - 1))
                deltaY =
                    *(inputImage + (row + 1) * sx + column) - *(inputImage +
                                                                (row -
                                                                 1) * sx +
                                                                column);
            else
            {
                if (row == 0)   /*limite haute de l'image */
                    deltaY =
                        *(inputImage + sx + column) - *(inputImage + column);
                else            /*limite basse de l'image */
                    deltaY =
                        *(inputImage + row * sx + column) - *(inputImage +
                                                              (row - 1) * sx +
                                                              column);
            }


            if (column != 0 && column != (sx - 1))
                deltaX =
                    *(inputImage + row * sx + column + 1) - *(inputImage +
                                                              row * sx +
                                                              column - 1);
            else
            {
                if (column == 0)    /*limite gauche de l'image */
                    deltaX =
                        *(inputImage + row * sx + 1) - *(inputImage +
                                                         row * sx);
                else            /*limite droite de l'image */
                    deltaX =
                        *(inputImage + row * sx + column) - *(inputImage +
                                                              row * sx +
                                                              column - 1);
            }

            *(outputGradImage + row * sx + column) = (float) sqrt(((deltaY * deltaY) + (deltaX * deltaX)) * 0.5);   /*on normalise par 1/sqrt(2) pour que ca reste dans 0-255 */

            /*calcul de l'orientation et conversion dans 0->360 */
            angle = (float) atan2(deltaY, deltaX);
            if (angle < 0)
                angle = 2 * (float) M_PI + angle;
            *(outputOrientImage + row * sx + column) =
                360 * (angle / (2 * (float) M_PI));
        }
    }

/*	if(Gpe==38)
	{
		FILE * ficgrad=fopen("grad.dat","w");
		FILE * ficorient=fopen("orient.dat","w");
		for(row=0;row<sy;row++)
		{
			for(column=0;column<sx;column++)
			{
				fprintf(ficgrad,"%03.3f ",*(outputGradImage + row*sx + column));
				fprintf(ficorient,"%03.3f ",*(outputOrientImage + row*sx + column));
			}
			
			fprintf(ficgrad,"\n");
			fprintf(ficorient,"\n");
		}
	}*/

#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

    return;
}
