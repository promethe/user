/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libSensors
\defgroup f_bipano_to_pano f_bipano_to_pano
\brief 


\section Modified
- author: 
- description: specific file creation
- date: 


\section Description
 
\section Macro
-none

\section Local varirables
-none

\section Global variables
-none

\section Internal tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function
\file 

http://www.doxygen.org
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

void function_bipano_to_pano(int numero)
{
    int l, i = 0;
    int x0 = 40, y0 = 25, xf = 603, yf = 455, x, y;
    int gpe_src;

    prom_images_struct *image_src, *pt1 = NULL;

    l = find_input_link(numero, i);
    gpe_src = liaison[l].depart;
    image_src = def_groupe[gpe_src].ext;
    
    cprints("enter %s\n",__FUNCTION__);
    
    pt1 = calloc_prom_image(1, (xf - x0) * 2, (yf - y0) / 2, 3);
    if (pt1 == NULL)
    {
        EXIT_ON_ERROR("erreur malloc\n");
    }

    for (y = 0; y <(int)pt1->sy; y++)
    {
/* 	printf("\ty=%d\n",y);*/
        for (x = 0; x < (int)pt1->sx; x++)
        {
/* 	printf("\t\tx=%d\n",x);*/
            if (x < (int)(pt1->sx) / 2)
            {
                pt1->images_table[0][3 * (x + y * pt1->sx)] =
                    image_src->images_table[0][3 *
                                               (x + x0 +
                                                (y + y0) * image_src->sx)];
                pt1->images_table[0][3 * (x + y * pt1->sx) + 1] =
                    image_src->images_table[0][3 *
                                               (x + x0 +
                                                (y + y0) * image_src->sx) +
                                               1];
                pt1->images_table[0][3 * (x + y * pt1->sx) + 2] =
                    image_src->images_table[0][3 *
                                               (x + x0 +
                                                (y + y0) * image_src->sx) +
                                               2];
            }
            if (x >= (int)(pt1->sx) / 2)
            {
                pt1->images_table[0][3 * (x + y * pt1->sx)] =
                    image_src->images_table[0][3 *
                                               (x + x0 - pt1->sx / 2 +
                                                (y + y0 +
                                                 (yf -
                                                  y0) / 2) * image_src->sx)];
                pt1->images_table[0][3 * (x + y * pt1->sx) + 1] =
                    image_src->images_table[0][3 *
                                               (x + x0 - pt1->sx / 2 +
                                                (y + y0 +
                                                 (yf -
                                                  y0) / 2) * image_src->sx) +
                                               1];
                pt1->images_table[0][3 * (x + y * pt1->sx) + 2] =
                    image_src->images_table[0][3 *
                                               (x + x0 - pt1->sx / 2 +
                                                (y + y0 +
                                                 (yf -
                                                  y0) / 2) * image_src->sx) +
                                               2];
            }
        }
    }

    def_groupe[numero].ext = (prom_images_struct *) pt1;
    cprints("end %s\n",__FUNCTION__);
}
