/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\ingroup libSensors
\defgroup f_quad_average f_quad_average
\file  f_quad_average.c
\brief Compute an average image at a desired size. Put a grid pattern on the image, compute average value of each grid and put the value into the pixel of the output image.


\section Modified
Author: R.Braud
Created: 16/04/2012
Modified:
- author: 
- description: specific file creation
- date: 16/04/2012

Theoritical description:

\details
 Compute an average image at a desired size : X * Y. Put a grid pattern on the image, compute average value of each grid and put the value into the pixel of the output image.


\section Options
-	-Xxx	: 	Width of the output image (default : x = 32)

-	-Yxx	: 	Height of the output image (default : y = 24)

-	-Ixx	:	Image number of the input (default : all images are computed)


Macro:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs: none (yet!)

Todo: see the author for comments

\file
\ingroup f_quad_average

http://www.doxygen.org
 ************************************************************/


#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>


typedef struct My_Data
{
   int InputGpe;
   prom_images_struct *prom_Original;
   prom_images_struct *prom_OutputImage;
   int xSize;
   int ySize;
   unsigned int image_nb;
} My_Data;


void function_quad_average(int Gpe)
{
   int InputGpe = -1;
   prom_images_struct *prom_Original = NULL;
   prom_images_struct *prom_OutputImage = NULL;
   char param[255];
   My_Data *Mydata;
   int xSize = 32, ySize = 24, xQuad, yQuad;
   int row, column;
   int image_nb = -1, r, c, colOut, rowOut;
   float moy;
   float *ima_output = NULL;
   float *ima_input = NULL;
   int i = 0, l = -1;

   dprints("function_quad_average\n");



   /********************
    ** INITIALIZATION **
    ********************/
   /*Get options on the link */
   if (def_groupe[Gpe].data == NULL)
   {
      i=0;
      l = find_input_link(Gpe, i);
      if (l != -1)
      {
         InputGpe = liaison[l].depart;
         prom_Original = (prom_images_struct *) def_groupe[InputGpe].ext;

         if (prom_getopt(liaison[l].nom, "-X", param) == 2)
         {
            xSize = atoi(param);
            dprints("X = %d\n", xSize);
         }

         if (prom_getopt(liaison[l].nom, "-Y", param) == 2)
         {
            ySize = atoi(param);
            dprints("Y = %d\n", ySize);
         }

         if (prom_getopt(liaison[l].nom, "-I", param) == 2)
         {
            image_nb = atoi(param);
            if ( image_nb > (int)prom_Original->image_number - 1)
            {
               EXIT_ON_ERROR("%s : Image number bigger than possible in the input link\n", __FUNCTION__);
            }
         }
         dprints("Source image number %d\n", image_nb);

         i++;
         if ( find_input_link(Gpe, i) != -1 )
         {
            EXIT_ON_ERROR("%s : Need just 1 group in input\n", __FUNCTION__);
         }

      }
      else
      {
         EXIT_ON_ERROR("%s : Need 1 group in input\n", __FUNCTION__);
      }

      if ((InputGpe == -1) || (prom_Original == NULL) )
      {
         EXIT_ON_ERROR("%s : Need 1 group with ext in input\n", __FUNCTION__);
      }



      /* Allocation */
      Mydata=(My_Data*)ALLOCATION(My_Data);
      if (Mydata == NULL)
      {
         EXIT_ON_ERROR("%s : Malloc error Mydata\n", __FUNCTION__);
      }

      prom_OutputImage = calloc_prom_image(image_nb == -1 ? prom_Original->image_number : 1 , xSize, ySize, 4);
      if (prom_OutputImage==NULL)
      {
         EXIT_ON_ERROR("%s : Malloc error prom_OutputImage\n", __FUNCTION__);
      }


      def_groupe[Gpe].ext = (prom_images_struct *)prom_OutputImage;


      Mydata->InputGpe = InputGpe;
      Mydata->prom_Original=prom_Original;
      Mydata->prom_OutputImage=prom_OutputImage;
      Mydata->xSize = xSize;
      Mydata->ySize = ySize;
      Mydata->image_nb = image_nb;

      def_groupe[Gpe].data = (void *)Mydata;
   }
   else
   {
      Mydata = (My_Data *) def_groupe[Gpe].data;

      InputGpe = Mydata->InputGpe;
      prom_Original = Mydata->prom_Original;
      prom_OutputImage = Mydata->prom_OutputImage;
      xSize = Mydata->xSize;
      ySize = Mydata->ySize;
      image_nb = Mydata->image_nb;
   }


   /************************
    ** END INITIALIZATION **
    ************************/

   xQuad = (int)(prom_Original->sx / xSize);
   yQuad = (int)(prom_Original->sy / ySize);


   for ( i = 0; i < ((image_nb == -1) ? (int)prom_Original->image_number : 1); i++)
   {

      ima_output = (float *)prom_OutputImage->images_table[i];
      ima_input  = (float *)prom_Original->images_table[image_nb == -1 ? i : image_nb];

      if (ima_output == NULL || ima_input == NULL)
      {
         EXIT_ON_ERROR("%s : Problem with images_table\n", __FUNCTION__);
      }

      colOut = 0;
      rowOut = 0;
      for ( row = 0; row < prom_Original->sy; row += yQuad )
      {
         for ( column = 0; column < prom_Original->sx; column += xQuad )
         {
            moy = 0.;
            for ( r = 0; r < yQuad; r++ )
            {
               for ( c = 0; c < xQuad; c++ )
               {
                  if ( ((row+r) < prom_Original->sy) && ((column+c) < prom_Original->sx) )
                     moy += ima_input[ (row+r)*prom_Original->sx + (column+c) ];
               }
            }
            moy = moy / (float)(xQuad*yQuad);

            if (rowOut == ySize || colOut == xSize) ///////////////////////////////////////
            {
               EXIT_ON_ERROR("%s : Problem with count of rowOut et colOut !!!!! A DEBUGUER\n", __FUNCTION__);
            }////////////////////////////////////////////////////////////////////////////

            ima_output[ rowOut*xSize + colOut ] = moy;
            colOut++;
         }
         rowOut++;
         colOut = 0;
      }
   }
}
