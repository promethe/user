/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file f_image_zoom.c
\author D. Bailly
\date 30/06/2010
\brief  Transform the image by a scale factor (zoom software)

Modified:
- author: xxxxx
- description: xxxxx
- date: xx/xx/xxxx

Theoritical description:
 - \f$  LaTeX equation: none \f$

* Transform the image to obtain a zoommed in image. The size of the output image
* stay the same. This box need an "image" algorythmic input links. The other
* links must non algorythmic. They will be taken in count when the scalefactor
* will be calculated.
* The scalefactor is calculated as the weighted sum of the inputs on the first
* neuron of this group. If there is more than one neurons they will not be
* updated neither used in the calculation.
* An option '-lissage' can be added on the image link. It will activate the
* smoothing one the output image. It is done by convulution with a 3x3 gaussian
* filter    1/16    2/16    1/16
*           2/16    4/16    2/16
*           1/16    2/16    1/16
* The image is convoluted 1/scalefactor times.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / neural / math / stat
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

/*#define DEBUG*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include <libx.h>
#include <net_message_debug_dist.h>

#define MAX_CONV    32

/*
*/
#define NB_VOISINS 9  /* nombre de pixels voisins +case normale pour les differents cas d'interpolation */

typedef struct data_image_zoom
{
  float middleX;
  float middleY;
  int gpe_ext;
  int lissage;
  float scale, old_scale;
  int sx,sy; /* taille de l'image en sortie */
  unsigned char *imageTmp;
  int **i_lookup; /* valeur de l'index pour une position quels sont les voisins*/
  float **v_lookup; /* valeur du coeff multiplicatif associe a la case voisine utilisee */
} Data_image_zoom;



void new_image_zoom(int index_of_group)
{
    Data_image_zoom *data;
    int l, i=0;
    int gpe_ext=-1, lissage=0;
    char buffer[128];
    float scale=1.;
    int sx=-1,sy=-1;

    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

    /* Search the 'image' input link where the image is stored */
    l = find_input_link(index_of_group, i++);
    if(l==-1) EXIT_ON_ERROR("no input link\n");
    while (l != -1)
    {
      dprints("liaison = %s \n",liaison[l].nom);
        if (strstr(liaison[l].nom, "image") != NULL)
        {
            if(gpe_ext != -1)
                EXIT_ON_ERROR("more than one image input link ('image' on the link name)\n");
            gpe_ext = liaison[l].depart;
        }
        if (strstr(liaison[l].nom, "-lissage") != NULL)
        {
            lissage = MAX_CONV;
        }
        if (prom_getopt(liaison[l].nom, "lissage", buffer) == 2)
        {
            lissage = atoi(buffer);
        }
        if (prom_getopt(liaison[l].nom, "SX", buffer) == 2) /* taille en X, si rien taille de l'entree */
        {
            sx = atoi(buffer);
        }
        if (prom_getopt(liaison[l].nom, "SY", buffer) == 2) /* taille en Y, si rien taille de l'entree */
        {
            sy = atoi(buffer);
        }
        if (prom_getopt(liaison[l].nom, "scale", buffer) == 2)
        {
            scale = atof(buffer);
        }

        l = find_input_link(index_of_group, i++);
    }
    /* Errors managing */
    if(gpe_ext == -1)
        EXIT_ON_ERROR("no image input link ('image' on the link name)\n");
    if(def_groupe[index_of_group].nbre > 1)
        PRINT_WARNING("there are unused neurons in this group (only one is used)\n");

    dprints("%s : lissage=%d, scale=%f\n", __FUNCTION__, lissage, scale);

    /* Creation of the data */
    data = (Data_image_zoom*)ALLOCATION(Data_image_zoom);
    data->gpe_ext = gpe_ext;
    data->lissage = lissage;
    data->scale=scale;
    data->sx=sx;data->sy=sy;
    data->imageTmp = NULL;
    def_groupe[index_of_group].data = data;

    /* Creation of the new image using the information of the input group */
    /** We have a little problem here. In most of the old functions the
     * allocation of the .ext is done at the first call of the function. So when
     * I tryied to use the function the .ext didn't exist. Unsolvable problem...
     * unless we rewrite all the functions adding a clean new_function.**/
    /*input_ext = def_groupe[gpe_ext].ext;
    if(input_ext == NULL) EXIT_ON_ERROR("no image on the input group(.ext == NULL)\n");
    def_groupe[index_of_group].ext = (prom_images_struct *)ALLOCATION(prom_images_struct);

    this_ext = def_groupe[index_of_group].ext;
    this_ext->sx = input_ext->sx;
    this_ext->sy = input_ext->sy;
    this_ext->nb_band = input_ext->nb_band;
    this_ext->image_number = 1;
    this_ext->images_table[0] = (unsigned char *)MANY_ALLOCATIONS(this_ext->sx*this_ext->sy, char);

    data->middleX = (float)(this_ext->sx)/2.0;
    data->middleY = (float)(this_ext->sy)/2.0;*/

  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


void function_image_zoom(int index_of_group)
{
    int premier_ele, i, j, ii, jj, k;
    int indexLU, indexCU, indexRU, indexLC, indexCC, indexRC, indexLD, indexCD, indexRD;
    int lissage, maxPassLiss;
    float scalefactor=0.0;
    Data_image_zoom *data=NULL;
    type_coeff *synapse=NULL;
    prom_images_struct *input_ext, *this_ext;
    unsigned char *input_image, *this_image, *imageTmp;
    int imSizeX, imSizeY;
    float coeffVU, coeffVC, coeffVD, coeffHL, coeffHC, coeffHR;
    float middleX, middleY, middle;

    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

    data = (Data_image_zoom *)def_groupe[index_of_group].data;

    /* Creation of data
     * It is not meant to be here but due to the problem explain before we have
     * to do the initializing at the first call */
    if(def_groupe[index_of_group].ext == NULL)
    {
        input_ext = def_groupe[data->gpe_ext].ext;
        if(input_ext == NULL) EXIT_ON_ERROR("no image on the input group (.ext == NULL)\n");

        this_ext = calloc_prom_image(1, input_ext->sx, input_ext->sy, input_ext->nb_band);
        def_groupe[index_of_group].ext = (void *)this_ext;

        data->middleX = (float)(this_ext->sx)/2.0;
        data->middleY = (float)(this_ext->sy)/2.0;

        dprints("%s: sizeX=%d sizeY=%d middleX=%f middleY=%f\n", __FUNCTION__, this_ext->sx, this_ext->sy, data->middleX, data->middleY);
    }

    input_ext = def_groupe[data->gpe_ext].ext;
    middleX = data->middleX;
    middleY = data->middleY;
    lissage = data->lissage;
    imageTmp = data->imageTmp;
    this_ext = def_groupe[index_of_group].ext;
    premier_ele = def_groupe[index_of_group].premier_ele;

    synapse = neurone[premier_ele].coeff;
    while(synapse != NULL)
    {
        scalefactor += synapse->val * neurone[synapse->entree].s1;
        synapse = synapse->s;
    }

    if(scalefactor < 0.0) scalefactor = 0.0;
    if(scalefactor > 1.0) scalefactor = 1.0;
    neurone[premier_ele].s = scalefactor;
    neurone[premier_ele].s1 = scalefactor;
    neurone[premier_ele].s2 = scalefactor;

    input_image = input_ext->images_table[0];
    imSizeX = input_ext->sx;
    imSizeY = input_ext->sy;
    this_image = this_ext->images_table[0];

    if(scalefactor > 0.999999)
        memcpy(this_image, input_image, imSizeX*imSizeY*sizeof(char));
    else if(scalefactor < 0.000001)
        memset(this_image, input_image[(int)(middleX+middleY*imSizeX)], imSizeX*imSizeY*sizeof(char));
    else
    {
        for(i=0; i<imSizeX; i++)
        {
            for(j=0; j<imSizeY; j++)
            {
                /* Calculation for the horizontal coefficients
                 * HL: horizontal left
                 * HR: horizontal right
                 * HC: horizontal center
                 * VU: vertical up
                 * VD: vertical down
                 * VC: vertical center
                 * If we take as a base a square of 3 pixels edges we can see
                 * those factor as the percentage of each of those pixels in the
                 * new one (calculated by llines and then by column)*/
                middle = middleX + ((float)i-middleX+0.5)*scalefactor;
                ii = (int)middle;

                coeffHL = ((float)ii - middle)/scalefactor + 0.5;
                coeffHR = (middle - (float)ii - 1.0)/scalefactor + 0.5;
                if(coeffHL < 0.0) coeffHL = 0.0;
                if(coeffHR < 0.0) coeffHR = 0.0;
                coeffHC = 1.0 - coeffHL - coeffHR;

                middle = middleY + ((float)j-middleY+0.5)*scalefactor;
                jj = (int)middle;

                coeffVU = ((float)jj - middle)/scalefactor + 0.5;
                coeffVD = (middle - (float)jj - 1.0)/scalefactor + 0.5;
                if(coeffVU < 0.0) coeffVU = 0.0;
                if(coeffVD < 0.0) coeffVD = 0.0;
                coeffVC = 1.0 - coeffVU - coeffVD;

                /* We see again the 9 coefficients appearing here with the pixel
                 * they weight (L:left, R:right, U:up, D:down, C:center)*/

                /* Calculation of the indexes */
                indexLU = ii+jj*imSizeX-1-imSizeX;
                indexCU = ii+jj*imSizeX-imSizeX;
                indexRU = ii+jj*imSizeX+1-imSizeX;
                indexLC = ii+jj*imSizeX-1;
                indexCC = ii+jj*imSizeX;
                indexRC = ii+jj*imSizeX+1;
                indexLD = ii+jj*imSizeX-1+imSizeX;
                indexCD = ii+jj*imSizeX+imSizeX;
                indexRD = ii+jj*imSizeX+1+imSizeX;

                /* Edges problem management */
                if(ii == 0)
                {
                  indexLU = indexCC;
                  indexLC = indexCC;
                  indexLD = indexCC;
                }
                if(ii == imSizeX-1)
                {
                  indexRU = indexCC;
                  indexRC = indexCC;
                  indexRD = indexCC;
                }
                if(jj == 0)
                {
                  indexLU = indexCC;
                  indexCU = indexCC;
                  indexRU = indexCC;
                }
                if(jj == imSizeY-1)
                {
                  indexLD = indexCC;
                  indexCD = indexCC;
                  indexRD = indexCC;
                }

                /* Calculation of the new pixel */
                this_image[i + j*imSizeX] = coeffHL*coeffVU*input_image[indexLU]
                                            + coeffHC*coeffVU*input_image[indexCU]
                                            + coeffHR*coeffVU*input_image[indexRU]
                                            + coeffHL*coeffVC*input_image[indexLC]
                                            + coeffHC*coeffVC*input_image[indexCC]
                                            + coeffHR*coeffVC*input_image[indexRC]
                                            + coeffHL*coeffVD*input_image[indexLD]
                                            + coeffHC*coeffVD*input_image[indexCD]
                                            + coeffHR*coeffVD*input_image[indexRD];
            }
        }
        if(lissage > 0)
        {
            maxPassLiss = (int)(1.0/(scalefactor*scalefactor))-1;
            if(maxPassLiss > MAX_CONV) maxPassLiss = MAX_CONV;
            for(k=0; k<maxPassLiss; k++)
            {
                for(i=0; i<imSizeX; i++)
                {
                    for(j=1; j<imSizeY-1; j++)
                    {
                        ii = i+j*imSizeX;
                        imageTmp[ii] = 0.25*this_image[ii -imSizeX]
                                        + 0.5*this_image[ii]
                                        + 0.25*this_image[ii +imSizeX];
                    }
                }
                for(i=1; i<imSizeX-1; i++)
                {
                    for(j=0; j<imSizeY; j++)
                    {
                        ii = i+j*imSizeX;
                        this_image[ii] = 0.25*imageTmp[ii -1]
                                        + 0.5*imageTmp[ii]
                                        + 0.25*imageTmp[ii +1];
                    }
                }
            }
        }

    }
    dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


void destroy_image_zoom(int index_of_group)
{

  dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

  free(def_groupe[index_of_group].data);

  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}

void create_lookup_table_zoom(int index_of_group)
{
    int i, j, ii, jj, k,p,pp,p2,qq;
    int nb_band=-1, nb_band2;
    int sx=-1,sy=-1;
    int indexLU, indexCU, indexRU, indexLC, indexCC, indexRC, indexLD, indexCD, indexRD;
    float scalefactor=0.0;
    Data_image_zoom *data=NULL;
    type_coeff *synapse=NULL;
    int premier_ele;
    prom_images_struct *input_ext=NULL, *this_ext=NULL;

    int imSizeX, imSizeY;
    float coeffVU, coeffVC, coeffVD, coeffHL, coeffHC, coeffHR;
    float middleX, middleY, middle;
    float middleX2, middleY2;
    int **i_lookup; float **v_lookup;

    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

    data = (Data_image_zoom *)def_groupe[index_of_group].data;

    /* Creation of data
     * It is not meant to be here but due to the problem explain before we have
     * to do the initializing at the first call */
    if(def_groupe[index_of_group].ext == NULL)
    {
        input_ext = def_groupe[data->gpe_ext].ext;
        if(input_ext == NULL) EXIT_ON_ERROR("no image on the input group (.ext == NULL)\n");

        if(data->sx==-1) data->sx= this_ext->sx ;
        if(data->sy==-1) data->sy= this_ext->sy ;
        nb_band=input_ext->nb_band;

        data->middleX = (float)(input_ext->sx)*0.5;
        data->middleY = (float)(input_ext->sy)*0.5;
        data->i_lookup=i_lookup= (int**) malloc(sizeof(int*)*data->sx*data->sy);
        data->v_lookup=v_lookup= (float**) malloc(sizeof(float*)*data->sx*data->sy);
        for(k=0;k<data->sx*data->sy;k++)
        {
          data->i_lookup[k]=i_lookup[k]= (int*) malloc(sizeof(int)*NB_VOISINS);
          data->v_lookup[k]=v_lookup[k]= (float*) malloc(sizeof(float)*NB_VOISINS);
        }
        dprints("%s: sizeX=%d sizeY=%d middleX=%f middleY=%f\n", __FUNCTION__, input_ext->sx, input_ext->sy, data->middleX, data->middleY);
    }

    input_ext = def_groupe[data->gpe_ext].ext;
    nb_band=input_ext->nb_band;
    sx=data->sx;
    sy=data->sy;
    dprints("new size SX =%d, SY= %d \n",sx,sy);
    middleX = data->middleX; middleX2=data->sx*0.5;
    middleY = data->middleY; middleY2=data->sy*0.5;

    i_lookup=data->i_lookup;
    v_lookup=data->v_lookup;

    data->old_scale=data->scale;
    scalefactor=data->scale;

    dprints("calcul neuronal \n");
    premier_ele = def_groupe[index_of_group].premier_ele;
    synapse = neurone[premier_ele].coeff;

    while(synapse != NULL)
    {
        scalefactor += synapse->val * neurone[synapse->entree].s1;

        synapse = synapse->s;
    }
    if(scalefactor < 0.0) scalefactor = 0.0;
    if(scalefactor > 1.0) scalefactor = 1.0;

    data->scale=scalefactor;

    imSizeX = input_ext->sx;
    imSizeY = input_ext->sy;

    dprints("scalefactor= %f , sx=%d, sy=%d, image_number= %d, nb_band=%d\n",scalefactor,imSizeX,imSizeY,input_ext->image_number,input_ext->nb_band);
    dprints("sx=%d, sy=%d \n",sx,sy);
    nb_band2=nb_band;
    if(nb_band==4) nb_band2=1; /* en effet les floats sont traitees avec des var de type float et pas char */
    if(scalefactor < 0.000001) return;

        for(j=sy; j--; )
        {
          /* Calculation for the horizontal coefficients
           * HL: horizontal left
           * HR: horizontal right
          * HC: horizontal center
          * VU: vertical up
          * VD: vertical down
          * VC: vertical center
          * If we take as a base a square of 3 pixels edges we can see
          * those factor as the percentage of each of those pixels in the
          * new one (calculated by llines and then by column)*/
          middle = middleY + ((float)j-middleY2+0.5)*scalefactor;
          jj = (int)middle;

          coeffVU = ((float)jj - middle)/scalefactor + 0.5;
          coeffVD = (middle - (float)jj - 1.0)/scalefactor + 0.5;
          if(coeffVU < 0.0) coeffVU = 0.0;
          if(coeffVD < 0.0) coeffVD = 0.0;
          coeffVC = 1.0 - coeffVU - coeffVD;

          p=jj*imSizeX; p2=j*sx;
          for(i=sx; i--; )
          {
            middle = middleX + ((float)i-middleX2+0.5)*scalefactor;
            ii = (int)middle;

            coeffHL = ((float)ii - middle)/scalefactor + 0.5;
            coeffHR = (middle - (float)ii - 1.0)/scalefactor + 0.5;
            if(coeffHL < 0.0) coeffHL = 0.0;
            if(coeffHR < 0.0) coeffHR = 0.0;
            coeffHC = 1.0 - coeffHL - coeffHR;

                /* We see again the 9 coefficients appearing here with the pixel
                 * they weight (L:left, R:right, U:up, D:down, C:center)*/

                /* Calculation of the indexes */
                pp=p+ii;
                indexLU = pp-1-imSizeX;
                indexCU = pp-imSizeX;
                indexRU = pp+1-imSizeX;
                indexLC = pp-1;
                indexCC = pp;
                indexRC = pp+1;
                indexLD = pp-1+imSizeX;
                indexCD = pp+imSizeX;
                indexRD = pp+1+imSizeX;

                 /* Edges problem management */
                if(ii == 0)
                {
                  indexLU = indexCC; indexLC = indexCC;  indexLD = indexCC;
                }
                else if(ii == imSizeX-1)
                {
                  indexRU = indexCC; indexRC = indexCC;  indexRD = indexCC;
                }
                if(jj == 0)
                {
                  indexLU = indexCC; indexCU = indexCC;  indexRU = indexCC;
                }
                else if(jj == imSizeY-1)
                {
                  indexLD = indexCC; indexCD = indexCC; indexRD = indexCC;
                }

                /* lookup table of the new pixel */
                qq=i + p2;
               /* printf("i=%d j=%d p2=%d qq=%d sx*sy=%d; \n",i,j,p2,qq,sx*sy);*/
                 i_lookup[qq][0]=indexLU*nb_band2; v_lookup[qq][0]=coeffHL*coeffVU; 
                 i_lookup[qq][1]=indexCU*nb_band2; v_lookup[qq][1]=coeffHC*coeffVU;
                 i_lookup[qq][2]=indexRU*nb_band2; v_lookup[qq][2]=coeffHR*coeffVU; 
                 i_lookup[qq][3]=indexLC*nb_band2; v_lookup[qq][3]=coeffHL*coeffVC; 
                 i_lookup[qq][4]=indexCC*nb_band2; v_lookup[qq][4]=coeffHC*coeffVC;
                 i_lookup[qq][5]=indexRC*nb_band2; v_lookup[qq][5]=coeffHR*coeffVC;
                 i_lookup[qq][6]=indexLD*nb_band2; v_lookup[qq][6]=coeffHL*coeffVD; 
                 i_lookup[qq][7]=indexCD*nb_band2; v_lookup[qq][7]=coeffHC*coeffVD; 
                 i_lookup[qq][8]=indexRD*nb_band2; v_lookup[qq][8]=coeffHR*coeffVD;
      }

    }

  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}

void function_images_zoom2(int index_of_group)
{
    int i, j, ii, k, n,p,p2,q,qq,qqq;
    int nb_band=-1;
    int sx=-1,sy=-1;
    int lissage, maxPassLiss;
    float scalefactor=0.0;
    Data_image_zoom *data=NULL;
    type_coeff *synapse=NULL;
    int premier_ele;
    prom_images_struct *input_ext=NULL, *this_ext=NULL;
    void *this_image=NULL, *imageTmp=NULL;
    float *input_image_f=NULL, *this_image_f=NULL, *imageTmp_f=NULL; /* si c'est une image de float */
    unsigned char *input_image_c=NULL, *this_image_c=NULL, *imageTmp_c=NULL;  /* si c'est une image de char */
    int imSizeX, imSizeY;
    float total;
    int **i_lookup; float **v_lookup;
    int *index; float *coeff;  
    

    dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);

    data = (Data_image_zoom *)def_groupe[index_of_group].data;

    /* Creation of data
     * It is not meant to be here but due to the problem explain before we have
     * to do the initializing at the first call */
    if(def_groupe[index_of_group].ext == NULL)
    {
      create_lookup_table_zoom( index_of_group);
        input_ext = def_groupe[data->gpe_ext].ext;
        if(input_ext == NULL) EXIT_ON_ERROR("no image on the input group (.ext == NULL)\n");

        if(data->sx==-1) data->sx= this_ext->sx ;
        if(data->sy==-1) data->sy= this_ext->sy ;
        nb_band=input_ext->nb_band;
        this_ext = calloc_prom_image(input_ext->image_number, data->sx, data->sy,nb_band);
        def_groupe[index_of_group].ext = (void *)this_ext;

        data->middleX = (float)(input_ext->sx)/2.0;
        data->middleY = (float)(input_ext->sy)/2.0;

        /* pour le sstockage result intermediaire float ou char par recopie dans tableau avec le bon cast */
        imageTmp= data->imageTmp = (unsigned char*) malloc( this_ext->sx*this_ext->sy*sizeof(char)*input_ext->nb_band);

        dprints("%s: sizeX=%d sizeY=%d middleX=%f middleY=%f\n", __FUNCTION__, this_ext->sx, this_ext->sy, data->middleX, data->middleY);
    }

    input_ext = def_groupe[data->gpe_ext].ext;
    nb_band=input_ext->nb_band;
    sx=data->sx;
    sy=data->sy;

    lissage = data->lissage;
    imageTmp = (void*) data->imageTmp;
    scalefactor=data->scale;
    this_ext = def_groupe[index_of_group].ext;

    premier_ele = def_groupe[index_of_group].premier_ele;
    synapse = neurone[premier_ele].coeff;

    while(synapse != NULL)
    {
        scalefactor += synapse->val * neurone[synapse->entree].s1;

        synapse = synapse->s;
    }
    if(scalefactor < 0.0) scalefactor = 0.0;
    if(scalefactor > 1.0) scalefactor = 1.0;

    if(fabs(scalefactor-data->scale)>1e-3) create_lookup_table_zoom(index_of_group); /* on doit modifier la table de lookup*/

      i_lookup=data->i_lookup;
      v_lookup=data->v_lookup;


    imSizeX = input_ext->sx;
    imSizeY = input_ext->sy;
    this_image = (void*) this_ext->images_table[0];

    dprints("scalefactor= %f , sx=%d, sy=%d, image_number= %d, nb_band=%d\n",scalefactor,imSizeX,imSizeY,input_ext->image_number,input_ext->nb_band);
    dprints("sx=%d, sy=%d \n",sx,sy);
    if(scalefactor < 0.000001)
    {
      for(n=this_ext->image_number;n--;)
      {
        this_image = (void*) this_ext->images_table[n];
        memset(this_image, 0, imSizeX*imSizeY*sizeof(char)*nb_band);
      }
    }
    else
    {
      for(n=this_ext->image_number;n--;)
      {
        dprints("%s copie image %d \n",__FUNCTION__,n);
        if(nb_band==4)
        {
          input_image_f =(float*) input_ext->images_table[n];
          this_image_f = (float*) this_ext->images_table[n];
        }
        else
        {
          input_image_c =(unsigned char*) input_ext->images_table[n];
          this_image_c = (unsigned char*) this_ext->images_table[n];
        }
        
        if(nb_band==4) for(j=sy; j--; )
        {
          p2=j*sx;
          for(i=sx; i--; )
          {
            /* Calculation of the new pixel */
            qq=i + p2;
            coeff=v_lookup[qq]; index=i_lookup[qq];
            total=0.;
            for(k=NB_VOISINS;k--;)
              {
                total +=  coeff[k]*input_image_f[index[k]] ; /*inverser qq et k pour aller plus vite */
              }
            this_image_f[qq] =total;
          }
        }
        
        else for(j=sy; j--; )
        {
          p2=j*sx;
          for(i=sx; i--; )
          {
            /* Calculation of the new pixel */
            qq=i + p2;
            coeff=v_lookup[qq]; index=i_lookup[qq];
            for(q=nb_band;q--;)
            {
              total=0.; qqq=qq*nb_band;
              for(k=NB_VOISINS;k--;)
              {
                total += coeff[k]*input_image_c[index[k]+q] ;
              }
              this_image_c[qqq+q] =(unsigned char) total; 
            }
          }
        }        
        
        if(lissage > 0)
        {
          if(nb_band==4) imageTmp_f=(float*) imageTmp; 
          else imageTmp_c=(unsigned char*) imageTmp;
          
          maxPassLiss = (int)(1.0/(scalefactor*scalefactor))-1;
          if(maxPassLiss > MAX_CONV) maxPassLiss = MAX_CONV;
          for(k=0; k<maxPassLiss; k++)
            {
              for(i=0; i<sx; i++)
                {
                  p= j*sx;
                  for(j=1; j<sy-1; j++)
                    {
                      ii = i+p;
                      if(nb_band==4) imageTmp_f[ii] = 0.25*(this_image_f[ii -sx]+this_image_f[ii +sx]) + 0.5*this_image_f[ii];
                      else for(q=nb_band;q--;)
                      {
                        imageTmp_c[ii*nb_band+q] = 0.25*(this_image_c[(ii -sx)*nb_band+q]+this_image_c[(ii +sx)*nb_band+q]) + 0.5*this_image_c[ii*nb_band+q];
                      }
                    }
                }
              for(i=1; i<sx-1; i++)
                {
                  p= j*sx;
                  for(j=0; j<sy; j++)
                    {
                      ii = i+p;
                      if(nb_band==4) this_image_f[ii] = 0.25*(imageTmp_f[ii -1]+imageTmp_f[ii +1]) + 0.5*imageTmp_f[ii];
                      else for(q=nb_band;q--;)
                        {
                          this_image_c[ii*nb_band+q] = 0.25*(imageTmp_c[(ii -1)*nb_band+q]+imageTmp_c[(ii +1)*nb_band+q]) + 0.5*imageTmp_c[ii*nb_band+q];
                        }
                    }
                }
            }
        }
      }

    }

  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


/*#undef DEBUG
*/
