/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_multi_where.c
\brief met les neurones d'un groupe d'entree sur un groupe de sortie de taille differente

Author: Mickael Maillard 
Created: 09/07/04
Modified:
- author: Maillard
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
si le groupe a 0001 en entree, il aura 00000011 par exemple en sortie -> utilise pour merge des echelles entre elles.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

/*#define DEBUG*/

struct data_multi_where
{
    int Ninput;
    int input_Gpe_table_size[100][2];
    float input_Gpe_table_facteur[100];
    int this_nbre;
    int this_deb;
};


void function_multi_where(int Gpe)
{
    int i,/* j,*/ k;
    int inputGpe = -1;
    float facteur;
    int this_deb, this_nbre, Ninput = 0;
    int *p_input_Gpe_table_size = NULL;
    float *p_input_Gpe_table_facteur = NULL;
    struct data_multi_where *this_data = NULL;
    int deb_prev_gpe, nbre_prev_gpe/*, indice_start, indice_stop*/;
    float value_neur;

    (void) nbre_prev_gpe; // (unused)

    /* initialisation */
#ifdef DEBUG
    printf("groupe %d : %s\n", Gpe, __FUNCTION__);
#endif

    if (def_groupe[Gpe].data == NULL)
    {
        def_groupe[Gpe].data = malloc(sizeof(struct data_multi_where));
        if (def_groupe[Gpe].data == NULL)
        {
            printf
                ("Gpe %d (f_visu_orient_scale_key) : Impossible d'allouer de la memoire\n",
                 Gpe);
            exit(1);
        }
        this_data = (struct data_multi_where *) def_groupe[Gpe].data;
        p_input_Gpe_table_size = &(this_data->input_Gpe_table_size[0][0]);
        p_input_Gpe_table_facteur = &(this_data->input_Gpe_table_facteur[0]);
        this_nbre = def_groupe[Gpe].nbre;
        this_deb = def_groupe[Gpe].premier_ele;

        Ninput = 0;
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                inputGpe = liaison[i].depart;
                /*chaine  = liaison[i].nom; */

                *(p_input_Gpe_table_size + 2 * Ninput) =
                    def_groupe[inputGpe].premier_ele;
                *(p_input_Gpe_table_size + 2 * Ninput + 1) =
                    def_groupe[inputGpe].nbre;
                *(p_input_Gpe_table_facteur + Ninput) =
                    (float) this_nbre / (float) def_groupe[inputGpe].nbre;
                Ninput++;

            }
        }

        this_data->Ninput = Ninput;
        this_data->this_nbre = this_nbre;
        this_data->this_deb = this_deb;
    }
    else
    {
        this_data = (struct data_multi_where *) def_groupe[Gpe].data;
        p_input_Gpe_table_size = &(this_data->input_Gpe_table_size[0][0]);
        p_input_Gpe_table_facteur = &(this_data->input_Gpe_table_facteur[0]);
        Ninput = this_data->Ninput;
        this_nbre = this_data->this_nbre;
        this_deb = this_data->this_deb;
    }

    for (k = this_deb; k < this_nbre + this_deb; k++)
        neurone[k].s2 = neurone[k].s1 = neurone[k].s = 0.;


    for (i = 0; i < Ninput; i++)
    {
        facteur = *(p_input_Gpe_table_facteur + i);
        deb_prev_gpe = *(p_input_Gpe_table_size + 2 * i);
        nbre_prev_gpe = *(p_input_Gpe_table_size + 2 * i + 1);
#ifdef DEBUG
        printf("facteur %f deb_prev_gpe %d nbre_prev_gpe %d --%p   --%p\n",
               facteur, deb_prev_gpe, nbre_prev_gpe,
               p_input_Gpe_table_facteur, p_input_Gpe_table_size);
#endif
        for (k = this_deb; k < this_deb + this_nbre; k++)
        {
            value_neur =
                neurone[deb_prev_gpe +
                        (int) ((float) (k - this_deb) / facteur)].s1;
            if (value_neur > 0.0001)
                neurone[k].s2 = neurone[k].s1 = neurone[k].s = value_neur;
        }
        /*for(j=deb_prev_gpe ; j<deb_prev_gpe+nbre_prev_gpe ; j++)
           {
           if(neurone[j].s1>0.0001)
           {
           value_neur = neurone[j].s1;
           indice_start = (int)((float)(j-deb_prev_gpe) * facteur);
           indice_stop = indice_start + (int)(facteur+0.5);equivalent round
           if(indice_stop>this_nbre)
           indice_stop=this_nbre;
           #ifdef DEBUG
           printf("this_nbre %d, this_deb %d indice_stop %d indice_start %d\n",this_nbre,this_deb,indice_stop,indice_start);
           #endif
           for(k = this_deb+indice_start ; k<= indice_stop + this_deb ;k++)
           neurone[k].s2=neurone[k].s1=neurone[k].s = value_neur;
           }
           } */
    }


#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
    return;

}
