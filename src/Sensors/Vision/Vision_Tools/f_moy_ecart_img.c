/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_moy_ecart_img.c
\brief

Author: VILLEMIN Jean-Baptiste
Created: 15/05/09

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Cette fonction donne sur deux neurones la moyenne et l'ecart type de l'image d'entree.
	Il faut donc que la boite neuronale possede 2 neurones !
	Ajout : Gestion des images float. Ali K.

Macro:
-none

Local variables:
-nnoe

Global variables:
-none

Internal Tools:
-

External Tools:
-
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <Struct/prom_images_struct.h>
#include <libx.h>
#include <stdlib.h>
#include <math.h>

#include <public_tools/Vision.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


typedef struct
{
	int GpeImage;
	int deb;
}data;




void new_moy_ecart_img(int numero)
{
	data *my_data;
	int link1 =-1;

	/**
	 * on cree la structure qui contiendra les donnees
	 */
	my_data = (data *) malloc(sizeof(data));
	if (my_data == NULL)
		EXIT_ON_ERROR("error in memory allocation in group %d %s\n", numero,__FUNCTION__);
	/* On initialise cette structure */
	my_data->GpeImage = -1;

	/**
	 * Recherche du lien d'entree
	 */
	link1 = find_input_link(numero, 0);
	if (link1 == -1)
		EXIT_ON_ERROR("Il n'y a pas de groupe en entree du Gpe %d\n",numero);

	my_data->GpeImage = liaison[link1].depart;

	dprints("GpeImage = %d\n", my_data->GpeImage);

	/**
	 * Verification que le groupe possede bien trois neurones
	 */

	if(def_groupe[numero].nbre<2)
		EXIT_ON_ERROR("Le groupe %s doit obligatoirement avoir 2 neurones (moyenne - ecart-type)\n",def_groupe[numero].no_name);



	/**
	 * Recuperation du numero du premier neurone de la boite
	 */
	my_data->deb = def_groupe[numero].premier_ele;

	/**
	 * Passe les donnes a la fonction
	 */
	def_groupe[numero].data = my_data;

	dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);

}

/**
 *	------------------- FIN DU CONSTRUCTEUR ------------------------
 */

void function_moy_ecart_img(int numero)
{
	int pixel;
	float moyenne=0;
	float ecart=0;
	prom_images_struct *p_images_ext;
	int taille;
	data *my_data = NULL;


	/**
	 * Recuperation donnees data
	 */


	dprints("------------------------%s-------------------------\n",__FUNCTION__);
	my_data = (data *) def_groupe[numero].data;
	if (my_data == NULL)
		EXIT_ON_ERROR("error while loading data in group %d %s\n", numero,__FUNCTION__);

	/**
	 * Getting the extension of previous group
	 */

	p_images_ext = (prom_images_struct *) def_groupe[my_data->GpeImage].ext;

	if (p_images_ext == NULL)
		EXIT_ON_ERROR("Problem in %s: there is nothing(may be freed)) in extension of group %i\n",__FUNCTION__,my_data->GpeImage);


	taille = (p_images_ext->sx * p_images_ext->sy);

	/**
	 * Calcul de la moyenne et de l'ecart type
	 */

	for (pixel=0;pixel<taille; pixel++)
		moyenne+= abs((p_images_ext->images_table[0])[pixel]);

	moyenne /=  taille;


	for (pixel=0;pixel< taille; pixel++)
		ecart+= pow( (abs((p_images_ext->images_table[0])[pixel]) - moyenne),2);	

	ecart = sqrt( ecart / taille);

	dprints("moyenne = %f ecart-type = %f\n", moyenne,ecart);

	/**
	 * Reajustement des valeurs en fonction de la valeur max de l'image pour codage.
	 */
	if(p_images_ext->nb_band == 1)
	{
		neurone[my_data->deb].s = neurone[my_data->deb].s1 =neurone[my_data->deb].s2 = moyenne / 255;
		neurone[my_data->deb+1].s = neurone[my_data->deb+1].s1 =neurone[my_data->deb+1].s2 = ecart / 255;
	}
	else if (p_images_ext->nb_band == 4)
	{
		neurone[my_data->deb].s = neurone[my_data->deb].s1 =neurone[my_data->deb].s2 = moyenne;
		neurone[my_data->deb+1].s = neurone[my_data->deb+1].s1 =neurone[my_data->deb+1].s2 = ecart;
	}
	else
		EXIT_ON_ERROR("%s ne gere que des images N&B ou float gpe : %s",__FUNCTION__,def_groupe[numero].no_name);

}

