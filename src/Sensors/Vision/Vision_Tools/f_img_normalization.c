/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************

\defgroup f_img_normalization f_img_normalization
\ingroup Sensors
\file  f_img_normalization.c

\brief Image preprocessing : image normalization
Description:

Author: Marwen Belkaid
Created: 15/04/2015

\section Restrictions
Only works with uchar grayscale images (nb_band = 1)

\section Input Links
Mandatory :
- -image : indicates the input group that contains the grayscale image

http://www.doxygen.org
************************************************************/

//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>


typedef struct data_img_normalization
{
   int input_gpe ;
   int sx ;
   int sy ;
} data_img_normalization;


void new_img_normalization(int gpe)
{
   data_img_normalization* mydata ;
   int tmp_input_gpe, tmp_sx, tmp_sy;
   int i = 0, links_in ;

   tmp_input_gpe = tmp_sx = tmp_sy = -1;

   /* Get Input group */
   while((links_in = find_input_link (gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "-image", NULL) == 1)
         tmp_input_gpe = liaison[links_in].depart ;

      i++ ;
   }

   /* Check inputs */
   if(tmp_input_gpe < 0)
      EXIT_ON_ERROR("%s needs an input link named \"-image\" \n", __FUNCTION__) ;

   /* Create object */
   mydata = ALLOCATION(data_img_normalization);
   mydata->input_gpe = tmp_input_gpe ;
   mydata->sx = -1 ;
   mydata->sy = -1 ;
   def_groupe[gpe].data = (data_img_normalization *) mydata;

}

void function_img_normalization(int gpe)
{
   int tmp_input_gpe ;
   data_img_normalization *mydata ;
   unsigned char *input_img, *output_img ;
   int i, tmp_nb_pixel, tmp_sy, tmp_sx ;
   float norm = 1;
   unsigned char min_i, max_i;
#ifdef DEBUG
   unsigned char min_o, max_o;
#endif


   /* init local variables */
   mydata = (data_img_normalization *) def_groupe[gpe].data ;
   tmp_input_gpe = mydata->input_gpe ;

   /* check input ext */
   if(def_groupe[tmp_input_gpe].ext == NULL)
      EXIT_ON_ERROR("The input group %s (%s) must have an ext \n", def_groupe[tmp_input_gpe].nom, def_groupe[tmp_input_gpe].no_name) ;
   if(((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->nb_band != 1)
      EXIT_ON_ERROR("%s This function only works with uchar grayscale images (nb_band = 1) \n", __FUNCTION__) ;
   if(def_groupe[gpe].ext == NULL)
   {
      mydata->sx = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sx ;
      mydata->sy = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sy ;
      def_groupe[gpe].ext = (prom_images_struct *) calloc_prom_image(1, mydata->sx, mydata->sy, 1);
   }

   /* set local variables */
   tmp_sx = mydata->sx ;
   tmp_sy = mydata->sy ;
   tmp_nb_pixel = tmp_sx * tmp_sy ;
   input_img = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->images_table[0] ;
   output_img = ((prom_images_struct *) def_groupe[gpe].ext)->images_table[0] ;

   min_i = 255 ;
   max_i = 0 ;
#ifdef DEBUG
   min_o = 255 ;
   max_o = 0 ;
#endif


   for(i = 0 ; i < tmp_nb_pixel ; i++)
   {
      if (input_img[i] > max_i)
         max_i = input_img[i];
      else if (input_img[i] < min_i)
         min_i = input_img[i];
   }
   norm = max_i - min_i ;

   for(i = 0 ; i < tmp_nb_pixel ; i++)
   {
      output_img[i] = (unsigned char) ((input_img[i] - min_i)/norm * 255.) ;

#ifdef DEBUG
      if (output_img[i] > max_o)
         max_o = output_img[i];
      else if (output_img[i] < min_o)
         min_o = output_img[i];
#endif

   }



}




void destroy_img_normalization(int gpe)
{
   dprints("destroy %s(%s) \n", __FUNCTION__,def_groupe[gpe].no_name);
   if (def_groupe[gpe].ext != NULL)
   {
      free(((prom_images_struct *) def_groupe[gpe].ext)) ;
      def_groupe[gpe].ext = NULL;
   }
   if (def_groupe[gpe].data != NULL)
   {
     free(((data_img_normalization *) def_groupe[gpe].data));
     def_groupe[gpe].data = NULL;
   }
}
