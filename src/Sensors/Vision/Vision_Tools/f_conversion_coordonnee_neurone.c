/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_conversion_coordonnee_neurone.c
 \brief

 Author: BOUCENNA Sofiane
 Created: 22/04/2008
 Modified:

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments: Cette fonction prendra 2 parametres en entree X et Y
 qui sont la taille de l image.
 prendra le neurone d activite max dans le groupe precedent
 convertira ces coordonnees dans la nouvelle taille

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

struct DATA {
  int prev_gpe;
  int X;
  int Y;
};

void function_conversion_coordonnee_neurone(int gpe_sortie)
{
  int i = 0;

  int deb_s = 0;
  int prev_gpe = 0;
  int num_liaison = 0;

  int X = 0;
  int Y = 0;
  struct DATA *data = NULL;

  int taille_x = -1;
  int taille_y = -1;
  int prev_deb = -1;
  float max = -999;
  int i_max = -1;
  int coordonneX = -1;
  int coordonneY = -1;
  char *st2;

#ifdef DEBUG
  printf("f_conversion_coordonnee_neurone : Entree\n");
#endif

  deb_s = def_groupe[gpe_sortie].premier_ele;

  if (def_groupe[gpe_sortie].data == NULL)
  {
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == gpe_sortie)
      {
        num_liaison = i;
        prev_gpe = liaison[i].depart;
        break;
      }

    /*Position beginning of image in the window (default 0,0) */
    st2 = strstr(liaison[i].nom, "-PX");
    if ((st2 != NULL))
    {

      X = atoi(&st2[3]);

    }

    st2 = strstr(liaison[i].nom, "-PY");
    if ((st2 != NULL))
    {

      Y = atoi(&st2[3]);

    }

    if (i == nbre_liaison)
    {
      printf("f_img_gris_selection : Pas de lien en entree de f_gris_selection !\n");
      exit(-1);
    }

    if ((data = (struct DATA *) malloc(sizeof(struct DATA))) == NULL)
    {
      printf("function_img_gris_selection : Impossible d'allouer la memoire\n");
      exit(-1);
    }
    def_groupe[gpe_sortie].data = (void *) data;
    data->prev_gpe = prev_gpe;
    data->X = X;
    data->Y = Y;

  }
  else
  {
    data = (struct DATA *) def_groupe[gpe_sortie].data;

  }
#ifdef DEBUG  
  printf("On a fini de reuperer les parametres\n");
#endif

  /*on recupere les infos sur le groupe precedent*/
  taille_x = def_groupe[data->prev_gpe].taillex;
  taille_y = def_groupe[data->prev_gpe].tailley;
  prev_deb = def_groupe[data->prev_gpe].premier_ele;

#ifdef DEBUGw
  printf("taille_x=%d\n",taille_x);
  printf("taille_y=%d\n",taille_y);
#endif

  for (i = prev_deb; i < prev_deb + def_groupe[data->prev_gpe].nbre; i++)
  {
    if (max < neurone[i].s)
    {
      max = neurone[i].s;
      i_max = i - prev_deb;
    }
  }

  coordonneX = i_max % taille_x;
  coordonneY = (i_max - coordonneX) / taille_x;

#ifdef DEBUG
  printf("max=%f\n",max);
  printf("indice=%d\n",i_max);
  printf("CoordonneeX=%d\n",(coordonneX*data->X)/taille_x);
  printf("CoordonneeY=%d\n",(coordonneY*data->Y)/taille_y);
#endif

  for (i = 0; i < def_groupe[gpe_sortie].nbre; i++)
  {
    neurone[deb_s + i].s = neurone[deb_s + i].s1 = neurone[deb_s + i].s2 = 0;
  }

  /*La sortie du groupe sera X sur le premier neurone
   *Y sur le deuxieme neurone
   */

  neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = (coordonneX * data->X) / taille_x;
  neurone[deb_s + 1].s = neurone[deb_s + 1].s1 = neurone[deb_s + 1].s2 = (coordonneY * data->Y) / taille_y;
  (void) num_liaison;
}
