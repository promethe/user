/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** 
 \ingroup libSensors
 \defgroup f_keypoint_orient f_keypoint_orient
 \brief Fonction cherchant l'orientation dominante du voisinage d'un point caracteristique


 \section Author
 - Mickael Maillard
 - Created: 01/04/04

 \section Modified
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 23/08/2004


 \section Description
 Fonction cherchant l'orientation dominante du voisinage d'un point caracteristique
 les orientations sont lissees par une gaussienne de facteur sigma 3*parametre du lien
 voisinage de taille 3*facteur de lissage
 (fonction deprecated)
 
 \section Macro
 -none

 \section Local varirables
 -none

 \section Global variables
 -none

 \section Internal Tools
 -Main_Interpol_Orient_At_XY()

 \section External Tools
 -none

 \section Links
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: une map des points caracteristiques
 un groupe contenant 2 images : en 0 les gradients et en 1 les orientations
 - where are the data?: none/xxx

 \section Comments

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function
 \file

 http://www.doxygen.org
 */
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <math.h>
#include "reseau.h"

#undef DEBUG

float Main_Interpol_Orient_At_XY(float *inputOrient, float *inputGrad, float *inputKeypoints, int sx, int sy, float sigma, int rowK, int columnK)
{/*Pour supprimer les warnings les arguments  "float *inputKeypoints" et "int sy" ont été supprimés car non utilisés*/
  float orientK, weigthedGrad, coeff, tempMax, InterpolValue, orientPrim;
  int radius;
  int sqrradius;
  float Histo[36];
  int i = 0, row, column, radsqrtemp;
  int orient, next, prev;

  (void) inputKeypoints;
  (void) sy;

  radius = (int) (3 * sigma);
  coeff = (float) (1 / (2.0 * M_PI * sigma * sigma));
  sqrradius = radius * radius;
  for (i = 0; i < 36; i++)
    Histo[i] = 0;

  /*Histo des orientations attenuees en fonctions de leur distance au point */
  for (row = rowK - radius; row <= rowK + radius; row++)
  {
    for (column = columnK - radius; column <= columnK + radius; column++)
    {
      radsqrtemp = (rowK - row) * (rowK - row) + (columnK - column) * (columnK - column);
      if (radsqrtemp <= sqrradius)
      {
        orient = (int) (*(inputOrient + row * sx + column) / 10);
        weigthedGrad = (float) (coeff * exp(-radsqrtemp / (2.0 * sigma * sigma))) * *(inputGrad + row * sx + column);
        Histo[orient] = Histo[orient] + weigthedGrad;
      }

    }
  }

  /*recherche du max */
  tempMax = 0;
  orient = 0;

  for (i = 0; i < 36; i++)
  {
    if (Histo[i] >= tempMax)
    {
      tempMax = Histo[i];
      orient = i;
    }
  }

  prev = orient - 1;
  if (orient == 0) prev = 35;

  next = orient + 1;
  if (orient == 35) next = 0;

  /*interpolation pour une plus grande precision : interpolation avec une parabole */
  if (isdiff(((float) (0.5 * (Histo[next] + Histo[prev]))), Histo[orient])) /*si on il existe une parabole */
  {
    orientPrim = (Histo[prev] - Histo[next]) / ((float) 2 * (Histo[next] + Histo[prev] - (float) 2 * Histo[orient]));

    InterpolValue = Histo[orient] - ((((float) 0.5 * (Histo[next] - Histo[prev])) * ((float) 0.5 * (Histo[next] - Histo[prev]))) / ((float) 2 * (Histo[prev] + Histo[next]) - (float) 4 * Histo[orient]));

    orientK = ((float) orient + orientPrim) * 10;
    if (orientK < 0) orientK = 360 + orientK;
  }
  else /*si ce n'est pas une parabole */
  {
    orientK = (float) (10 * orient);
  }
#ifdef DEBUG
  dprints("fin Max_Inter avec orient trouve %f en row %d column %d\n",
      orientK, rowK, columnK);
#endif
  (void) InterpolValue;
  return orientK;
}

void function_keypoint_orient(int Gpe)
{
  int Gpe_K = -1, Gpe_GO = -1;
  float *inputKeypoints = NULL, *inputGrad = NULL, *inputOrient = NULL, *outputImage = NULL;
  int i, row, column;
  int sx, sy;
  char *chaine = NULL, *param = NULL;
  float sigma = (float) -1;
  int voisinage;

#ifdef DEBUG
  float *outputImageVisu, orient;
  int x, y;
#endif

  if (def_groupe[Gpe].ext == NULL)
  {
    /*recherche des parametres et traitement des erreurs */
    for (i = 0; i < nbre_liaison; i++)
    {
      if (liaison[i].arrivee == Gpe)
      {
        chaine = liaison[i].nom;
        if (strstr(chaine, "-K") != NULL) Gpe_K = liaison[i].depart;
        else if (strstr(chaine, "-GO") != NULL)
        {
          Gpe_GO = liaison[i].depart;
          param = strstr(chaine, "-P");
          if (param != NULL) sigma = (float) atof(&param[2]);
          else
          {
            dprints("%s : Parametre sigma pour ponderation incorrect -P##\nATTENTION PAS DE PONDERATION\n", __FUNCTION__);
            sigma = (float) (1 / 3);
          }
        }
        else
        {

          EXIT_ON_ERROR("%s : parametre des liens entrants incorrects (-K keypoints et -GO GradOrient)\n", __FUNCTION__);

        }
      }
    }
    if (Gpe_K == -1 || Gpe_GO == -1)
    {

      EXIT_ON_ERROR("%s : il manque une liaison entrante ou un parametre est incorrect\n", __FUNCTION__);

    }

    if (def_groupe[Gpe_K].ext == NULL || def_groupe[Gpe_GO].ext == NULL)
    {
      dprints("%s : un des pointeurs des liens entrants n'a pas ete initialise\n", __FUNCTION__);
      /*exit(0); */
      return;
    }

    /*la fonction ne traite que des images en floatant */
    if (((prom_images_struct *) def_groupe[Gpe_K].ext)->nb_band != 4 || ((prom_images_struct *) def_groupe[Gpe_GO].ext)->nb_band != 4)
    {
      EXIT_ON_ERROR("%s ne prend que des images en float...\n", __FUNCTION__);

    }

    sx = ((prom_images_struct *) def_groupe[Gpe_K].ext)->sx;
    sy = ((prom_images_struct *) def_groupe[Gpe_K].ext)->sy;

    /*les images doivent etre de meme taille */
    if ((int) ((prom_images_struct *) def_groupe[Gpe_GO].ext)->sx != (int) sx || (int) ((prom_images_struct *) def_groupe[Gpe_GO].ext)->sy != (int) sy)
    {

      EXIT_ON_ERROR("%s : Gpe : %d : les images entrantes ne sont pas de meme tailles...\n", __FUNCTION__, Gpe);

    }

    if (((prom_images_struct *) def_groupe[Gpe_GO].ext)->image_number != 2)
    {

      EXIT_ON_ERROR("%s : Gpe : %d : le groupe contenant l'image des gradients et celle des orientations ne contient pas les 2 images...\n", __FUNCTION__, Gpe);
    }

    /*allocation de memoire pour la structure du resultat */
    def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[Gpe].ext == NULL)
    {
      EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__, __LINE__);
    }

    /*calcul de la nouvelle taille et stockage dans la prom_images_struct */
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
    ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
    ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] = ((prom_images_struct *) def_groupe[Gpe_GO].ext)->images_table[1];
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] = ((prom_images_struct *) def_groupe[Gpe_GO].ext)->images_table[0];
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] = ((prom_images_struct *) def_groupe[Gpe_K].ext)->images_table[0];

    /*alloc de memoire pour le parametre sigma */
    (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4]) = malloc(sizeof(float));
    if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] == NULL)
    {
      printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__, __LINE__);
      exit(0);
    }
    *((float *) (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4])) = sigma;

    /*allocation de memoire pour la carte resulat */
    (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) = malloc(sx * sy * sizeof(float));
    if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
    {
      EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__, __LINE__);

    }

    /*#ifdef VISU
     (float*)(((prom_images_struct *)def_groupe[Gpe].ext)->images_table[5]) = (float*)malloc(sx*sy*sizeof(float));
     #endif*/

  }
  else
  {
    sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    sigma = *((float *) (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4]));
  }

  inputOrient = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1];
  inputGrad = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2];
  outputImage = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0];
  inputKeypoints = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3];

  for (row = 0; row < sy; row++)
    for (column = 0; column < sx; column++)
      *(outputImage + row * sx + column) = 0;
  /*	for(row=0;row<sy;row++)
   for(column=0;column<sx;column++)
   *(outputImage + row*sx + column)=*(inputKeypoints + row*sx + column);
   */

  sigma = 3 * sigma;

  voisinage = (int) (3 * sigma);

  /*appelle de la fonction calculant les orientations en chaque point caracteristique */
  for (row = voisinage; row < (sy - voisinage); row++)
  {
    for (column = voisinage; column < (sx - voisinage); column++)
    {
      if (fabs(*(inputKeypoints + row * sx + column)) > 0.00000001)
      {
        *(outputImage + row * sx + column) = Main_Interpol_Orient_At_XY(inputOrient, inputGrad, inputKeypoints, sx, sy, sigma, row, column);
#ifdef DEBUG
        dprints("Gpe %d apres keypoints ++++++++\n", Gpe);
#endif
      }
    }
  }

  /*ATTENTION VISU MODIFIE LA SORTIE : PERMET UNIQUEMENT D'AVOIR UNE IMAGE VISIBLE AVEC LES ORIENTATIONS*/
  /*A ENLEVER POUR AVOIR UNE SORTIE CORRECTE POUR LES GROUPES SUIVANTS*/
  /*#ifdef VISU
   printf("ATTENTION : VISUALISATION %s les resultats sur .ext sont fausses par la visualisation\n",__FUNCTION__);

   outputImageVisu = (float*)(((prom_images_struct *)def_groupe[Gpe].ext)->images_table[5]) ;


   for(row=0;row<sy;row++)
   {
   for(column=0;column<sx;column++)
   {
   *(outputImageVisu + row*sx + column)=*(inputGrad + row*sx + column);
   if( *(outputImage + row*sx + column)!=0 )
   {
   orient = 2*M_PI*(*(outputImage + row*sx + column)/360);

   if(Gpe == 118)
   printf("Gpe %d Orientation VISU keypoint %f en row %d et column %d\n",Gpe,orient,row,column);

   for(x=-2;x<3;x++)
   for(y=-2;y<3;y++)
   if( (row+x)>=0 && (row+x)<sy && (column+y)>=0 && (column+y)<sx  )
   *(outputImageVisu + (row+x)*sx + column + y) = 255 - *(inputGrad + row*sx + column);

   for(i=0;i<20;i++)
   {
   y=(int)(i*cos(orient));
   x=(int)(i*sin(orient));
   if( (row+x)>=0 && (row+x)<sy && (column+y)>=0 && (column+y)<sx  )
   *(outputImageVisu + (row+x)*sx + column+y)=255;
   }
   }
   }
   }

   for(row=0;row<sy;row++)
   for(column=0;column<sx;column++)
   *(outputImage + row*sx + column) = *(outputImageVisu + row*sx+column);
   #endif*/
#ifdef DEBUG
  dprints("fin Gpe %d\n", Gpe);
#endif

  return;
}
