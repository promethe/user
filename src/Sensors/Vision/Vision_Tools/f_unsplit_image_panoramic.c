/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_unsplit_image_panoramic.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:

Macro:
-none

Local variables:
-none

Global variables:
-prom_images_struct images_capture

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Pan_Tilt/CaptureImage()
-tools/IO_Robot/Pan_Tilt/Refrsh()
-tools/Vision/calloc_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define TIME_TRACE*/

#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <Global_Var/Vision.h>
#ifdef TIME_TRACE
#include <Global_Var/SigProc.h>
#endif
#include <Struct/prom_images_struct.h>
#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct MyData_f_unsplit_image_panoramic
{
    int GpeImage;
    int GpeTransition;
    int nbre_image;
    int cpt;
    int rayon_exclusion;
} MyData_f_unsplit_image_panoramic;

/* #define DEBUG */
void function_unsplit_image_panoramic(int Gpe)
{

    int GpeImage = -1;
    int GpeTransition = -1;
    int nbre_image = 1;
    int rayon_exclusion = 0;

    int l = -1, bigsx, sx, sy, i = 0, x, y;

    MyData_f_unsplit_image_panoramic *my_data;

    char param[10];

    prom_images_struct *pt;
    prom_images_struct *pt_entree;
    int cpt = 0;

#ifdef DEBUG
    printf("entree function_split_image_panoramic\n");
#endif

    if (def_groupe[Gpe].data == NULL)
    {
        l = find_input_link(Gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "N", param) == 2)
            {
                nbre_image = atoi(param);
                GpeImage = liaison[l].depart;
            }
            if (prom_getopt(liaison[l].nom, "RE", param) == 2)
            {
                rayon_exclusion = atoi(param);
            }
            if (strcmp(liaison[l].nom, "transition_detect") == 0)
            {
                GpeTransition = liaison[l].depart;
            }
            i++;
            l = find_input_link(Gpe, i);
        }

#ifdef DEBUG
        printf("N=%d, GpeImage= %d\n", nbre_image, GpeImage);
#endif

        sy = ((prom_images_struct *) (def_groupe[GpeImage].ext))->sy;
        sx = (((prom_images_struct *) (def_groupe[GpeImage].ext))->sx -
              2 * rayon_exclusion) * nbre_image + 2 * rayon_exclusion;
        pt = (prom_images_struct *) calloc_prom_image(1, sx, sy,
                                                      ((prom_images_struct
                                                        *) (def_groupe
                                                            [GpeImage].ext))->
                                                      nb_band);

#ifdef DEBUG
        printf("sx=%d, sy= %d\n", sx, sy);
#endif

        pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;

        if (pt_entree == NULL)
        {
            printf
                ("function_split_image_panoramic n'a pas de groupe en entre avec une image\n");
            exit(0);
        }

        my_data = malloc(sizeof(MyData_f_unsplit_image_panoramic));
        my_data->GpeImage = GpeImage;
        my_data->GpeTransition = GpeTransition;
        my_data->nbre_image = nbre_image;
        my_data->cpt = cpt;
        my_data->rayon_exclusion = rayon_exclusion;
        def_groupe[Gpe].data = (MyData_f_unsplit_image_panoramic *) my_data;
        def_groupe[Gpe].ext = (prom_images_struct *) pt;
    }
    else
    {
        my_data = (MyData_f_unsplit_image_panoramic *) def_groupe[Gpe].data;
        nbre_image = my_data->nbre_image;
        GpeImage = my_data->GpeImage;
        GpeTransition = my_data->GpeTransition;
        cpt = my_data->cpt;
        rayon_exclusion = my_data->rayon_exclusion;
        pt = (prom_images_struct *) def_groupe[Gpe].ext;
        pt_entree = (prom_images_struct *) def_groupe[GpeImage].ext;
    }

#ifdef DEBUG
    printf("debut code effectif, cpt = %d\n", cpt);
#endif

    sx = ((prom_images_struct *) (def_groupe[GpeImage].ext))->sx;
    sy = ((prom_images_struct *) (def_groupe[GpeImage].ext))->sy;
    bigsx = ((prom_images_struct *) (def_groupe[Gpe].ext))->sx;

    if (cpt == 0)
    {
        for (x = 0; x < sx - rayon_exclusion; x++)
            for (y = 0; y < sy; y++)
            {
                pt->images_table[0][y * bigsx +
                                    (x + cpt * (sx - 2 * rayon_exclusion))] =
                    pt_entree->images_table[0][y * sx + x];
            }
    }
    else if (cpt == nbre_image - 1)
    {
        for (x = 0; x < sx; x++)
            for (y = 0; y < sy; y++)
            {
                pt->images_table[0][y * bigsx +
                                    (x + cpt * (sx - 2 * rayon_exclusion))] =
                    pt_entree->images_table[0][y * sx + x];
            }
    }
    else
    {
        for (x = rayon_exclusion; x < sx - rayon_exclusion; x++)
            for (y = 0; y < sy; y++)
            {
                pt->images_table[0][y * bigsx +
                                    (x + cpt * (sx - 2 * rayon_exclusion))] =
                    pt_entree->images_table[0][y * sx + x];
            }
    }

    cpt++;
    if (cpt == nbre_image)
        cpt = 0;

    my_data->cpt = cpt;

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Tine in function_acquisition\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    printf("%s\n", MessageFunctionTimeTrace);
#endif

#ifdef DEBUG
    printf("fin function_split_image_panoramic\n");
#endif
}
