/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_average_position.c 
\brief 

Author: N. Garnault
Created: 14/08/2009
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  function_average_position active un neurone lorsque ce dernier a recu une activité en entree durant au moins les n dernieres iterations

Parametres : 
  t : le seuil à partir duquel on considère que le stimulus est resté suffisement à la même place pour pouvoir justifier de sa position.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: vision
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <limits.h>
#include <public_tools/Vision.h>
/*#include <NN_Core/Limbic/calcule_neuromodulation.h>*/
#include <net_message_debug_dist.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


#define DEFAULT_TETA 20

typedef struct struct_average_position {
  int teta;
} t_average_position;

void function_average_position(int gpe)
{
  int i = 0, l = 0;
  char param[255];
  int teta;
  float sortie;
  type_coeff *coeff;

  /* initialisation test */
  if (def_groupe[gpe].data == NULL)
  {
    teta = DEFAULT_TETA;

    /* recuperations des donnees */
    i = 0;
    l = 0;
    while((l = find_input_link(gpe, i++)) != -1)
    {
      if(prom_getopt(liaison[l].nom, "-t", param) == 2)
      {
        teta = atoi(param);
        if(teta <= 0)
          teta = DEFAULT_TETA;
      }
    }

    def_groupe[gpe].data = (t_average_position *)malloc(sizeof(t_average_position));
    ((t_average_position *)def_groupe[gpe].data)->teta = teta;
  }

  teta = (((t_average_position *)def_groupe[gpe].data)->teta);

  for(i = def_groupe[gpe].premier_ele ; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre ; i++)
  {
    sortie = 0;
    coeff = neurone[i].coeff;
    while(coeff != NULL)
    {
      sortie += (coeff->val)*neurone[coeff->entree].s1;
      coeff = coeff->s;
    }

    if(sortie <= def_groupe[gpe].seuil)
    {
      neurone[i].s = 0;
    }
    else if(neurone[i].s < INT_MAX)
    {
      neurone[i].s += 1; 
    }

    if(neurone[i].s > teta)
    {
      neurone[i].s1 = 1.0;
      neurone[i].s2 = 1.0;
    }
    else
    { 
      neurone[i].s1 = 0.0;
      neurone[i].s2 = 0.0;
    }
  }
}
