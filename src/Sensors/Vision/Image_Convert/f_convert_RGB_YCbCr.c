/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_convert_RGB_YCbCr f_convert_RGB_YCbCr

\brief Convert an RGB image into a YCbCr image

\section Modified
- author: D BAILLY
- description: creation
- date: 03/02/2012

\details
  Cette fonction converti une image RGB en une image YCbCr

\section Options
- un lien sans option particulière en provenance du groupe avec l'image est
          nécessaire
- -sync : option pour synchroniser cette boîte avec l'éxécution de la
        boîte précédente

\section Todo
*/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>
#include "public_tools/Vision.h"

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

#define YR  65.738
#define YG  129.057
#define YB  25.064
#define CbR 37.945
#define CbG 74.494
#define CbB 112.439
#define CrR 112.439
#define CrG 94.154
#define CrB 18.285

#define NB_MAX_THREADS_RGB_YCbCr  100

typedef struct data_convert_RGB_YCbCr
{
   int gpe_image;
} Data_convert_RGB_YCbCr;

typedef struct arg_convert_RGB_YCbCr
{
   int nb_pixel;
   unsigned char *image_input;
   unsigned char *image_output;
} Arg_convert_RGB_YCbCr;


void *compute_Y(void *arg)
{
   int i, nb_pixel=((Arg_convert_RGB_YCbCr*)arg)->nb_pixel;
   unsigned char *image_in = ((Arg_convert_RGB_YCbCr*)arg)->image_input;
   unsigned char *image_out = ((Arg_convert_RGB_YCbCr*)arg)->image_output;
   float r, g, b, tmp;

   for (i = 0; i < nb_pixel; i++)
   {
      r=image_in[3 * i];
      g=image_in[3 * i + 1];
      b=image_in[3 * i + 2];
      tmp = 16. + (YR * r + YG * g + YB * b)/256.;
      if (tmp < 0.) tmp = 0.0;
      if (tmp > 255.0) tmp = 255.0;
      image_out[3 * i] = (unsigned char)((int)tmp);
   }
   return NULL;
}

void *compute_Cb(void *arg)
{
   int i, nb_pixel=((Arg_convert_RGB_YCbCr*)arg)->nb_pixel;
   unsigned char *image_in = ((Arg_convert_RGB_YCbCr*)arg)->image_input;
   unsigned char *image_out = ((Arg_convert_RGB_YCbCr*)arg)->image_output;
   float r, g, b, tmp;

   for (i = 0; i < nb_pixel; i++)
   {
      r=image_in[3 * i];
      g=image_in[3 * i + 1];
      b=image_in[3 * i + 2];
      tmp = 128. + (-CbR * r - CbG * g + CbB * b)/256.;
      if (tmp < 0.) tmp = 0.0;
      if (tmp > 255.0) tmp = 255.0;
      image_out[3 * i + 1] = (unsigned char)((int)tmp);
   }
   return NULL;
}

void *compute_Cr(void *arg)
{
   int i, nb_pixel=((Arg_convert_RGB_YCbCr*)arg)->nb_pixel;
   unsigned char *image_in = ((Arg_convert_RGB_YCbCr*)arg)->image_input;
   unsigned char *image_out = ((Arg_convert_RGB_YCbCr*)arg)->image_output;
   float r, g, b, tmp;

   for (i = 0; i < nb_pixel; i++)
   {
      r=image_in[3 * i];
      g=image_in[3 * i + 1];
      b=image_in[3 * i + 2];
      tmp = 128. + (CrR * r - CrG * g - CrB * b)/256.;
      if (tmp < 0.) tmp = 0.0;
      if (tmp > 255.0) tmp = 255.0;
      image_out[3 * i + 2] = (unsigned char)((int)tmp);
   }
   return NULL;
}


void function_convert_RGB_YCbCr(int index_of_gpe)
{
   int i = 0, j = 0, nb_pixel;
   int l = -1;
   int gpe_image=-1;
   Data_convert_RGB_YCbCr *data;
   prom_images_struct *image_input_struct = NULL, *image_output_struct = NULL;
   Arg_convert_RGB_YCbCr thread_arg[NB_MAX_THREADS_RGB_YCbCr];
   pthread_t thread_tab[NB_MAX_THREADS_RGB_YCbCr];

   if (def_groupe[index_of_gpe].data == NULL)
   {
      /*Finding the link */
      i=0;
      l = find_input_link(index_of_gpe, i);
      while (l != -1)
      {
         if (strstr(liaison[l].nom, "sync") != NULL)
         {
         }
         else
         {
            gpe_image = liaison[l].depart;
         }
         i++;
         l = find_input_link(index_of_gpe, i);
      }
      /* Pour verifier l'existence du lien */
      if (gpe_image == -1)  EXIT_ON_ERROR("le groupe n'a pas d'entree !");

      /*allocation memoire pour la sauvegarde des donnees sur le lien */
      data = (Data_convert_RGB_YCbCr*)ALLOCATION(Data_convert_RGB_YCbCr);

      /* parametres par default */
      data->gpe_image = gpe_image;
      def_groupe[index_of_gpe].data = data;
   }
   data = def_groupe[index_of_gpe].data;
   image_input_struct = def_groupe[data->gpe_image].ext;

   if (image_input_struct == NULL)
   {
      PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
      return ;
   }

   if (def_groupe[index_of_gpe].ext == NULL)
   {
      /*image couleur du groupe precedent */
      image_input_struct = (prom_images_struct *) def_groupe[data->gpe_image].ext;

      if (image_input_struct->nb_band != 3)  EXIT_ON_ERROR("L'image en entree doit etre en couleur");

      /* allocation de memoire */
      image_output_struct = (prom_images_struct *)calloc_prom_image(image_input_struct->image_number, image_input_struct->sx, image_input_struct->sy, 3);
      if (image_output_struct == NULL)  EXIT_ON_ERROR("impossible d'allouer la memoire");
      def_groupe[index_of_gpe].ext = image_output_struct;
   }
   image_output_struct = def_groupe[index_of_gpe].ext;

   nb_pixel = image_input_struct->sx * image_input_struct->sy;
   for (j = 0; j < image_output_struct->image_number && 3*j+3 < NB_MAX_THREADS_RGB_YCbCr; j++)
   {
      thread_arg[3*j].nb_pixel = thread_arg[3*j+1].nb_pixel = thread_arg[3*j+2].nb_pixel = image_output_struct->sx * image_output_struct->sy;
      thread_arg[3*j].image_input = thread_arg[3*j+1].image_input = thread_arg[3*j+2].image_input = image_input_struct->images_table[j];
      thread_arg[3*j].image_output = thread_arg[3*j+1].image_output = thread_arg[3*j+2].image_output = image_output_struct->images_table[j];
      pthread_create(&(thread_tab[3*j]), NULL, compute_Y, (void *) &(thread_arg[3*j]));
      pthread_create(&(thread_tab[3*j+1]), NULL, compute_Cb, (void *) &(thread_arg[3*j+1]));
      pthread_create(&(thread_tab[3*j+2]), NULL, compute_Cr, (void *) &(thread_arg[3*j+2]));
   }

   for (j = 0; j < image_output_struct->image_number && 3*j+3 < NB_MAX_THREADS_RGB_YCbCr; j++)
   {
      pthread_join(thread_tab[3*j], NULL);
      pthread_join(thread_tab[3*j+1], NULL);
      pthread_join(thread_tab[3*j+2], NULL);
   }
}



