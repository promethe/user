/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_optical_flow.c
\brief calcul du flot optique

Author: xxxxxxxx
Created: xxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 26/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
.........................................................................

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/calloc_prom_image()
-Kernel_Function/prom_getopt()

Links:
- type: none
- description: none
- input expected group:
- where are the data?:

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/

/*#define DEBUG*/

#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

/*hue in [0,360[, saturation in [0,255?] and light in [0,1] */
void hsl_to_rgb(int hue, float saturation, float light, unsigned char *r,unsigned char *g, unsigned char *b)
{
	float c, x; /*c is chroma */
	unsigned char cc,cx;
	c=(1.-fabs(2.*light-1.))*saturation;
	cc=(unsigned char) ((int) c);
	/*printf("c=%f , cc= %d \n",c,(int)cc);*/
	hue=hue/60;
	x=c*(1-fabs((hue % 2) -1));
	cx=(unsigned char) ((int)x);
	/*printf("x=%f , cx= %d \n",x,(int)cx);*/
	switch(hue)
	{
		case 0: *r= 0; *g=0; *b=0; break;
		case 1: *r= cc; *g=cx, *b=0; break;
		case 2: *r= cx; *g=cc, *b=0; break;
		case 3: *r= 0; *g=cc, *b=cx; break;
		case 4: *r= 0; *g=cx, *b=cc; break;
		case 5: *r= cx; *g=0, *b=cc; break;
		case 6: *r= cc; *g=0, *b=cx; break;
		default: *r=0; *g=0; *b=0; kprints("Warning %s: problem in switch(hue=%d)\n",__FUNCTION__, hue);
	}
	/*printf("%d %f %f :  %d %d %d \n",hue, saturation, light, (int) *r,(int) *g, (int) *b);*/
} 

/* utilise les tableaux U et V (composantes x et y du mvt) pour fabriquer une image dir/intensite */
/* La couleur represente la	direction */

void mvt_uv_to_hsl(int largeur, int hauteur, float *U, float *V,unsigned char *iv)
{
	int i,j;
	int hue;
	float light, saturation;
	int p,pp,q;
	unsigned char *pt;
	float coeff;
	
	saturation=255.;coeff=180./3.14116;
	
	/*printf("largeur = %d , hauteur=%d \n",largeur,hauteur);*/
  for (j=hauteur-1; j--;) 
  {	  
		q=j*largeur;
    for (i=largeur-1; i--; ) 
    {
			p=(i+q);
      pp=3*p; /* acces aux 3 plans de caracteres (r,g,b) */
			hue=180 + (int)(coeff*atan2(U[p],V[p]));
			light=sqrt(U[p]*U[p]+V[p]*V[p])*0.1; /* etrange c'est plus rapide que les 2 fabs !!! L2 plus rapide que L1 ?*/
			/*light=(fabs(U[p])+fabs(V[p]))*0.2;*/
			if(light>1.)
			{
				/*kprints("Warning %s: light = %f > 1. \n",__FUNCTION__,light); */
				light=1.;
			}
			pt=&iv[pp];
			hsl_to_rgb(hue, saturation, light, pt, pt+1, pt+2);
    }
  }
}		
 

/*---------------------------------------------------------------------------------------------*/

typedef struct MyData_f_convert 
{
	prom_images_struct *prom_UV;
	float *U,*V;
	unsigned char *iv;
} MyData_f_convert;

/* converti 2 images de floats Vx, Vy  en une image rgb: module=intensite et direction/pahse=couleur */
void function_convert_image_2floats_color(int Gpe)
{
	int l, i;
	int largeur = 0, hauteur = 0;
	int Gpe_input = -1;

	float *U= NULL,*V= NULL;
	unsigned char *iv = NULL;
	prom_images_struct *prom_UV=NULL;

	MyData_f_convert *my_data;

	char resultat[256];

    /* Recherche des deux gpes d'entreee  */
	if (def_groupe[Gpe].data == NULL)
	{
		i = 0;
		l = find_input_link(Gpe, i);
		while (l != -1)
		{
			dprints("lien %d: %s--\n",i,liaison[l].nom); 
			if (prom_getopt(liaison[l].nom, "-I", resultat) == 1)
			{
				Gpe_input = liaison[l].depart;
				prom_UV = (prom_images_struct *) def_groupe[Gpe_input].ext;
			}
			i++;
			l = find_input_link(Gpe, i);
		}
		if (Gpe_input == -1)
		{
			kprints("manque un lien -I pour %s %d \n",__FUNCTION__, Gpe_input);
			exit(0);
		}

		my_data =(MyData_f_convert *) malloc(sizeof(MyData_f_convert));
		if (my_data == NULL)
		{
			kprints("erreur malloc dans f_optical_flow\n");
			exit(0);
		}
			/* Test pour voir si les images sont vides */		
		if (prom_UV == NULL)
		{
			kprints ("Probleme %s : il n'y pas d'image dans le groupe %i \n", __FUNCTION__, Gpe_input);
			exit(EXIT_FAILURE);
		}

		largeur = prom_UV->sx;
		hauteur = prom_UV->sy;
	
		/* Allocation memoire pour l'image resultat */
		if (def_groupe[Gpe].data == NULL)
		{
			def_groupe[Gpe].ext =  calloc_prom_image(1, largeur, hauteur, 3); 
		}	
		
		iv=my_data->iv=((prom_images_struct*)  def_groupe[Gpe].ext)->images_table[0];

		U=my_data->U=(float *) prom_UV->images_table[0];
		V=my_data->V=(float *) prom_UV->images_table[1];
		my_data->prom_UV=prom_UV;
		
		def_groupe[Gpe].data = (MyData_f_convert *) my_data;
	}
	else
	{
		my_data = (MyData_f_convert*) (def_groupe[Gpe].data);
		U=my_data->U;
		V=my_data->V;
		prom_UV=my_data->prom_UV;
		
		iv=my_data->iv;
		largeur = prom_UV->sx;
		hauteur = prom_UV->sy;
	}	

	dprints("convert %s\n",__FUNCTION__);
	mvt_uv_to_hsl(largeur, hauteur, U, V, iv);
}

