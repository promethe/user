/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

typedef struct MyData_f_convert_RGB_LAB
{
    int gpe_image;
    int mode_lab;
} MyData_f_convert_RGB_LAB;

float flab(float t)
{
    return (t > 0.008856f) ? pow(t, 1.0f / 3.0f) : (7.787  * t + 16.0f / 116.0f);
}

void function_convert_RGB_LAB(int gpe)
{
    int gpe_image = -1;
    int i, l, n;
    float r, g, b, X, Y, Z, L, a, FX, FY, FZ;
    int mode_lab = -1;
    prom_images_struct *pt_out, *pt_in;
    MyData_f_convert_RGB_LAB *my_data = NULL;
    unsigned char *A;

    //printf("function_convert_RGB_LAB\n");

    if (def_groupe[gpe].data == NULL)
    {
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "-LAB") == 0)
            {
                gpe_image = liaison[l].depart;
                mode_lab = 0;
            }

            if (strcmp(liaison[l].nom, "-L") == 0)
            {
                gpe_image = liaison[l].depart;
                mode_lab = 1;
            }

            if (strcmp(liaison[l].nom, "-A") == 0)
            {
                gpe_image = liaison[l].depart;
                mode_lab = 2;
            }

            if (strcmp(liaison[l].nom, "-B") == 0)
            {
                gpe_image = liaison[l].depart;
                mode_lab = 3;
            }
            i++;
            l = find_input_link(gpe, i);

        }
        //printf("mode f_convert_RGB_LAB:%d\n", mode_lab);

        if (def_groupe[gpe_image].ext == NULL || gpe_image == -1)
        {
            //printf ("pas d'image ds l'ext du groupe image de f_convert_RGB_LAB(%d)\n",gpe);
            exit(0);
        }

        pt_in = ((prom_images_struct *) (def_groupe[gpe_image].ext));

        if (pt_in->nb_band != 3)
        {
            //printf("Image in de f_convert_RGB_LAB(%d) nest pas en RGB\n", gpe);
            exit(0);
        }

        if (mode_lab == 0)
        {
            pt_out = calloc_prom_image(1, pt_in->sx, pt_in->sy, 3);
        }
        else
        {
            pt_out = calloc_prom_image(1, pt_in->sx, pt_in->sy, 1);
        }

        my_data =
            (MyData_f_convert_RGB_LAB *)
            malloc(sizeof(MyData_f_convert_RGB_LAB));
        if (my_data == NULL)
        {
            //printf("erreur malloc dans f_convert_RGB_LAB\n");
            exit(0);
        }
        my_data->mode_lab = mode_lab;
        my_data->gpe_image = gpe_image;
        def_groupe[gpe].data = (MyData_f_convert_RGB_LAB *) my_data;
        def_groupe[gpe].ext = (prom_images_struct *) pt_out;
    }
    else
    {
        my_data = (MyData_f_convert_RGB_LAB *) def_groupe[gpe].data;
        gpe_image = my_data->gpe_image;
        mode_lab = my_data->mode_lab;
    }

    pt_out = (prom_images_struct *) (def_groupe[gpe].ext);
    pt_in = (prom_images_struct *) (def_groupe[gpe_image].ext);
    A = pt_in->images_table[0];
    n = (pt_in->sx) * (pt_in->sy);
    //printf("code effectif convert_RGB_LAB, sx=%d,sy=%d\n", (pt_in->sx),(pt_in->sy));
    for (i = 0; i < n; i++)
    {
//        printf("0 - R = %d \t\t G = %d \t\t B = %d\n", A[3 * i + 0], A[3 * i + 1], A[3 * i + 2]);


        r = A[3 * i + 0] / 255.0;
        g = A[3 * i + 1] / 255.0;
        b = A[3 * i + 2] / 255.0;


//
///////////////////////////////////////////////////////////////////////////////////////////////////
//        /* Version David */
//        X = 0.412453 * r + 0.357580 * g + 0.189423 * b;
//        Y = 0.212671 * r + 0.715160 * g + 0.072169 * b;
//        Z = 0.019334 * r + 0.119193 * g + 0.950227 * b;
//
//        L = ((Y > 0.008856) ? 1.16 * pow(Y,
//                                         1.0f / 3) -
//             0.16 : 9.033 * Y) * 255.0;
//        a = (5 * (flab(X) - flab(Y)) + 0.5) * 255.0;
//        b = (2 * (flab(Y) - flab(Z)) + 0.5) * 255.0;
//
//
//        printf("0 - L = %f \t a = %f \t b = %f\n", L, a, b);
///////////////////////////////////////////////////////////////////////////////////////////////////
//



        /* Version Raphaël = openCV et easyrgb.com */

        if (r > 0.04045) {
          r  = pow(((r + 0.055) / 1.055), 2.4);
        } else {
          r  = r / 12.92;
        }

        if (g > 0.04045) {
          g  = pow(((g + 0.055) / 1.055), 2.4);
        } else {
          g  = g / 12.92;
        }

        if (b > 0.04045) {
          b  = pow(((b + 0.055) / 1.055), 2.4);
        } else {
          b  = b / 12.92;
        }

        r  *= 100;
        g  *= 100;
        b  *= 100;


        //Observer. = 2°, Illuminant = D65
        X = 0.412453 * r + 0.357580 * g + 0.189423 * b;
        Y = 0.212671 * r + 0.715160 * g + 0.072169 * b;
        Z = 0.019334 * r + 0.119193 * g + 0.950227 * b;


        // Observer= 2°, Illuminant= D65
        X /= 95.047;
        Y /= 100.000;
        Z /= 108.883;


        FX = flab(X);
        FY = flab(Y);
        FZ = flab(Z);

        L = Y > 0.008856f ? (116.f * FY - 16.f) : (903.3f * Y);
        a = 500.f * (FX - FY);
        b = 200.f * (FY - FZ);

//        printf("1 - L = %f \t a = %f \t b = %f\n", L, a, b);
//        printf("2 - L = %f \t a = %f \t b = %f\n\n", L*255./100, a+127, b+127);

        L *= 255./100.;
        a += 127.5;
        b += 127.5;


        if (L < 0)
            L = 0;
        else if (L > 255)
            L = 255;

        if (a < 0)
            a = 0;
        else if (a > 255)
            a = 255;

        if (b < 0)
            b = 0;
        else if (b > 255)
            b = 255;

        if (mode_lab == 1)
        {
            pt_out->images_table[0][i] = (int) (L);
        }
        else if (mode_lab == 2)
        {
            pt_out->images_table[0][i] = (int) (a);
        }
        else if (mode_lab == 3)
        {
            pt_out->images_table[0][i] = (int) (b);
        }
        else if (mode_lab == 0)
        {
            pt_out->images_table[0][3*i] = (int) (L);
            pt_out->images_table[0][3*i+1] = (int) (a);
            pt_out->images_table[0][3*i+2] = (int) (b);
        }
    }

    //printf("--- finfunction_convert_RGB_LAB\n");
}
