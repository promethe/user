/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_convert_image_uchar_to_float.c
\brief enable an image of unsisigned char to be converted in an image of float

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:

Description:
This file contains the C fonction for converting an image of unsisigned char to be converted in an image of float

Macro:
-none

Local variables:
-nnoe

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>

void function_convert_image_uchar_to_float(int Gpe)
{
	int InputGpe = -1, i, nx, ny, n;
	float *outputImage;
	unsigned char *inputImage;

	(void)n;
#ifdef TIME_TRACE
	struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
	long SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace;
	char MessageFunctionTimeTrace[255];
#endif

	dprints("Gpe %d : %s\n", Gpe, __FUNCTION__);

#ifdef TIME_TRACE
	gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

	if (def_groupe[Gpe].ext == NULL)
	{


		/* recuperation du lien entrant */
		for (i = 0; i < nbre_liaison; i++)
			if (liaison[i].arrivee == Gpe)
			{
				/*chaine = liaison[i].nom; */
				InputGpe = liaison[i].depart;
				break;
			}
		if (InputGpe == -1)
		{
			EXIT_ON_ERROR("%s : pas de groupe amont !\n", __FUNCTION__);
		}
		if (def_groupe[InputGpe].ext == NULL)
		{
			PRINT_WARNING("Gpe %s : le groupe amont a son ext NULL\n", def_groupe[Gpe].no_name);
			return;
		}
		/* allocation de memoire */
		def_groupe[Gpe].ext =
				(prom_images_struct *) malloc(sizeof(prom_images_struct));
		if (def_groupe[Gpe].ext == NULL)
		{
			EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,__LINE__);
		}

		/* recuperation du pointeur sur l'image d'entree */
		/* stocke dans images_table[1] */
		((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] = ((prom_images_struct *) def_groupe[InputGpe].ext)->images_table[0];

		/* recuperation des info du groupe precedent */
		nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
		((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
		ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
		((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
		n = nx * ny;
		((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
		((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;

		if (((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band != 1)
			EXIT_ON_ERROR("%s : Le NB_band de l'image entrante est incorrect (%d) gpe : %s :  cette fonction ne fait que le N&B\n",
					__FUNCTION__,((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band,def_groupe[Gpe].no_name);

		((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = malloc(nx * ny * sizeof(float));
		if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==NULL)
			EXIT_ON_ERROR("%s : %d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",__FUNCTION__, __LINE__);


		/*on stocke le groupe entrant */
		def_groupe[Gpe].data = (int *) malloc(sizeof(int));
		if ((prom_images_struct *) def_groupe[Gpe].data == NULL)
			EXIT_ON_ERROR("%s : %d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",__FUNCTION__, __LINE__);

		*((int *) ((prom_images_struct *) def_groupe[Gpe].data)) = InputGpe;

	}
	else
	{
		nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
		ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
		/*on met a jour le pointeur de l'image entrante : evite de reallouer l'espace pour l'image de sortie meme si le pointeur sur l'image d'entree a ete modifie */
		((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
				((prom_images_struct *)def_groupe[(int)(*((int *) ((prom_images_struct *) def_groupe[Gpe].data)))].ext)->images_table[0];
	}

	outputImage =(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0];

	inputImage =(unsigned char *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1];


	/*on effectue la conversion */
	for (i = 0; i < (nx * ny); i++)
		*((float *) outputImage + i) =(float) *((unsigned char *) inputImage + i);


#ifdef TIME_TRACE
	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
	if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
		MicroSecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
	}
	else
	{
		SecondesFunctionTimeTrace =
				OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
				1;
		MicroSecondesFunctionTimeTrace =
				1000000 + OutputFunctionTimeTrace.tv_usec -
				InputFunctionTimeTrace.tv_usec;
	}
	sprintf(MessageFunctionTimeTrace,
			"Time in function convert_imag_uchar_to_float (Gpe %d) image %dx%d : \t%4ld.%06d\n",
			Gpe, nx, ny, SecondesFunctionTimeTrace,
			MicroSecondesFunctionTimeTrace);
	affiche_message(MessageFunctionTimeTrace);
#endif

#ifdef DEBUG
	printf("fin de Gpe %d\n", Gpe);
#endif
}
