/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  f_convert_image_complex_to_norm.c
 \brief enable an image of complex to be converted in an image of float (the norm)

 Author: xxxxxxxxx
 Created: xxxxxxxxxx
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:

 Description:
 

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: xxxxxxxxxxx
 - description: none
 - input expected group: none
 - where are the data?: none

 Comments: none

 Known bugs: none

 Todo: see the author for comments


 http://www.doxygen.org
 ************************************************************/

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>

typedef struct Complex_Tag
{
  float x;
  float y;
} Complex;

void
function_convert_image_complex_to_norm(int Gpe)
{
  int InputGpe = -1, i, nx, ny, n, p;
  float *outputImage;
  Complex *inputImage;
  float val_min, val_max, tmp,total;

#ifdef DEBUG
  printf("Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

  if (def_groupe[Gpe].ext == NULL)
    {

      for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == Gpe)
          {
            InputGpe = liaison[i].depart;
          }
      if (InputGpe == -1)
        {
          printf("%s : pas de groupe amont !\n", __FUNCTION__);
          exit(0);
        }

      if (def_groupe[InputGpe].ext == NULL)
        {
          printf("Gpe %s : le groupe amont a son ext NULL\n",
              def_groupe[Gpe].no_name);
          return;
        }
      def_groupe[Gpe].ext = (prom_images_struct *) malloc(
          sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)
        {
          printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
              __LINE__);
          exit(0);
        }

      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1]
          = ((prom_images_struct *) def_groupe[InputGpe].ext)-> images_table[0];

      nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
      ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
      n = nx * ny;
      ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
      ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;

      if (((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band != 8)
        {
          printf(
              "%s : votre image est couleur, cette fonction ne fait que les complex nb_band:%d \n",
              __FUNCTION__,((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band);
          exit(0);
        }

      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]
          = (unsigned char *) malloc(nx * ny * sizeof(float));
      if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
        {
          printf("%s : %d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",  __FUNCTION__, __LINE__);
          exit(0);
        }

      (def_groupe[Gpe].data) = (int *) malloc(sizeof(int));
      if (def_groupe[Gpe].data == NULL)
        {
          printf("%s : %d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",  __FUNCTION__, __LINE__);
          exit(0);
        }
      *((int *) def_groupe[Gpe].data) = InputGpe;
    }
  else
    {
      nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
      ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1]
          = ((prom_images_struct *) def_groupe[(int) (*((int *) def_groupe[Gpe].data))].ext)->images_table[0]; /*pour ceux qui font des pointeurs qui changent dans leur groupe mais ca devrait plus exister... */
    }

  outputImage   = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[0];
  inputImage    = (Complex *) ((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[1];

  val_max = val_min = sqrt(inputImage[0].y * inputImage[0].y  + inputImage[0].x * inputImage[0].x);
  for (p = 0; p < (nx * ny); p++)
    {
      total = sqrt(inputImage[p].y * inputImage[p].y + inputImage[p].x    * inputImage[p].x);
      if (total > val_max)        val_max = total;
      if (total < val_min)        val_min = total;
      outputImage[p] = total;
    }

  tmp = val_max - val_min;

  if (isequal(tmp, 0.))
    {
      for (p = 0; p < (nx * ny); p++)
        outputImage[p] = 0.;
    }
  else
    {
      for (p = 0; p < (nx * ny); p++)
        outputImage[p] = (float) ((outputImage[p] - val_min) / tmp);

    }

#ifdef DEBUG
  printf("fin de Gpe %d\n", Gpe);
#endif
}
