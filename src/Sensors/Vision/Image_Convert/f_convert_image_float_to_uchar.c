/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_convert_image_float_to_uchar.c
\brief enable an image of float to be converted in an image of u_char

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:

Description:
 This file contains the C fonction for converting an image of float to be converted in an image of unsisigned char
 Modif de l'ancienne pour eviter d'appeler convert_im_float_to_im_uchar qui faisait des malloc et des free inutile

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>


typedef struct type_my_data
{
   int InputGpe;
   int expand;
   float val_min;
   float val_max;
} type_my_data;

void function_convert_image_float_to_uchar(int Gpe)
{
   prom_images_struct *my_images=NULL;
   char param[256],param2[256];
   int InputGpe = -1, i,l, nx, ny, n, p;
   int nb_images,j;
   unsigned char *outputImage;
   float *inputImage;
   float val_min=0., val_max=255., tmp,norm;
   int expand = 2;
   float tempp;

   dprints("Gpe %d : %s\n", Gpe, __FUNCTION__);

   i=0;
   if (def_groupe[Gpe].ext == NULL)
   {
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         InputGpe = liaison[l].depart;
         if (prom_getopt(liaison[l].nom, "ne", param) >=1 ) /*pas d'expand */
         {
            dprints("mode ne\n");
            expand = 0;
            break;
         }
         if (prom_getopt(liaison[l].nom, "se", param) >=1) /*simple expand */
         {
            dprints("mode se\n");
            expand = 1;
            break;
         }
         if (prom_getopt(liaison[l].nom, "sn", param) >=1) /*simple normalization */
         {
            dprints("mode se simple normalization\n");
            expand = 2;
            break;
         }
         if (prom_getopt(liaison[l].nom, "pe", param) >=1) /*predefined expand */
         {
            dprints("mode pe\n");
            expand = 3;
            break;
         }
         if((prom_getopt(liaison[l].nom, "-m", param) >=1) && (prom_getopt(liaison[l].nom, "-M", param2) >=1))
         {
            val_min = atof(param);
            val_max = atof(param2);
            expand = 4;
            break;
         }
         i++;
         l = find_input_link(Gpe, i);
      }

      if (InputGpe == -1)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "pas de groupe amont !\n");
      }

      if (def_groupe[InputGpe].ext == NULL)
      {
         kprints("Gpe %s : le groupe amont a son ext NULL\n", def_groupe[Gpe].no_name);
         return;
      }

      nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
      ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
      nb_images=((prom_images_struct *) def_groupe[InputGpe].ext)->image_number;
      def_groupe[Gpe].ext = 	calloc_prom_image(nb_images, nx, ny, 1);
      if (def_groupe[Gpe].ext == NULL)
      {
         EXIT_ON_GROUP_ERROR(Gpe, " ALLOCATION IMPOSSIBLE ...! \n");
      }

      my_images=(prom_images_struct *) def_groupe[Gpe].ext;

      dprints("%s Nombre d'images = %d, nb_band = %d \n",__FUNCTION__, my_images->image_number,my_images->nb_band);

      if (((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band != 4)
      {
         printf("%s  nb_band=%d \n",__FUNCTION__, ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band);
         EXIT_ON_GROUP_ERROR(Gpe, " votre image est couleur, cette fonction ne fait que le N&B n \n");
      }

      (def_groupe[Gpe].data) = (void *) malloc(sizeof(type_my_data));
      if (def_groupe[Gpe].data == NULL)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "ALLOCATION DE MEMOIRE IMPOSSIBLE...\n");
      }

      dprints("%s :expand %d\n", __FUNCTION__, expand);
      ((type_my_data*)def_groupe[Gpe].data)->InputGpe = InputGpe;
      ((type_my_data*)def_groupe[Gpe].data)->expand = expand;
      ((type_my_data*)def_groupe[Gpe].data)->val_min = val_min;
      ((type_my_data*)def_groupe[Gpe].data)->val_max = val_max;
   }
   else
   {
      my_images=(prom_images_struct *) def_groupe[Gpe].ext;
      nx = my_images->sx;
      ny = my_images->sy;

      expand = ((type_my_data*)def_groupe[Gpe].data)->expand;
      InputGpe = ((type_my_data*)def_groupe[Gpe].data)->InputGpe ;
      val_min = ((type_my_data*)def_groupe[Gpe].data)->val_min;
      val_max = ((type_my_data*)def_groupe[Gpe].data)->val_max;
   }
   n = nx * ny;

   dprints("%s Nombre d'images = %d, nb_band = %d, expand=%d \n",__FUNCTION__, my_images->image_number,my_images->nb_band,expand);
   for(j=my_images->image_number;j--;)  /* on convertit toutes les images de float en entree s'il y en a plusieurs */
   {
      dprints("traitement de l'image %d \n",j);
      outputImage =(unsigned char *) my_images->images_table[j];
      inputImage = (float *) ((prom_images_struct *)def_groupe[InputGpe].ext)->images_table[j];
      dprints("Option normalisation : val_min : %f   val_max = %f \n", val_min, val_max);

      switch (expand)
      {
      case 0:
         for (p = n; p-- ; )
         {
            tempp = inputImage[p];
            if (tempp > 255.)tempp = 255.;   /*printf("gggggggrrrrrrrr\n"); */
            if (tempp < 0.)  tempp = 0.;     /*printf("gggggggrrrrrrrr\n"); */
            outputImage[p] = (unsigned char) tempp;
         }
         break;
      case 1:
         for (p = n; p-- ; )
         {
            tempp = 255. * inputImage[p];
            if (tempp > 255.) tempp = 255.;   /*printf("gggggggrrrrrrrr\n"); */
            if (tempp < 0.)   tempp = 0.;     /*printf("gggggggrrrrrrrr\n"); */
            outputImage[p] = (unsigned char) tempp;
         }
         break;
      case 2:
         val_max = inputImage[0];
         val_min = inputImage[0];
         for (p = n; p-- ; )
         {
            if (inputImage[p] > val_max) val_max = inputImage[p];
            if (inputImage[p] < val_min) val_min = inputImage[p];
         }

         tmp = val_max - val_min;

         if (isequal(tmp, 0.))
         {
            for (p = n; p-- ; )   outputImage[p] = 0;
         }
         else
         {
            norm=255. / (tmp);
            dprints("%s: nx=%d, ny=%d ; vmin=%f vmax= %f, norm = %f \n", __FUNCTION__,nx,ny, val_min, val_max , norm);
            for (p = n; p-- ; ) outputImage[p] = (unsigned char) ((inputImage[p] - val_min) * norm);
         }
         break;
      case 3:
         norm=(125. / 1000.);
         for (p = n; p-- ; )
         {
            tmp = (float) inputImage[p] * norm + 125.;
            if (tmp > 255.) tmp = 255.;     /*printf("gggggggrrrrrrrr\n"); */
            if (tmp < 0.)   tmp = 0.;       /*printf("gggggggrrrrrrrr\n"); */
            outputImage[p] = (unsigned char) tmp;
         }
         break;
      case 4:
         tmp = val_max - val_min;

         if (isequal(tmp, 0.))
         {
            for (p = n; p-- ; )   outputImage[p] = 0;
         }
         else
         {
            norm=255. / (tmp);
            dprints("%s: nx=%d, ny=%d ; vmin=%f vmax= %f, norm = %f \n", __FUNCTION__,nx,ny, val_min, val_max , norm);
            for (p = n; p-- ; )
            {
               tempp = ((inputImage[p] - val_min) * norm);
               if (tempp < 0.)   tempp = 0.;
               outputImage[p] = (unsigned char) tempp;
            }
         }
         break;

      default:
         kprints("Erreur d'expand dans function_convert_image_float_to_uchar gpe %d\n",Gpe);
      }
   }


   dprints("%s : fin de Gpe %d\n", __FUNCTION__, Gpe);
}



