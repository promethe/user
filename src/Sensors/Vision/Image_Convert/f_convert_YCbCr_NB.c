/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_convert_RGB_NB.c
\brief

Author: Khursheed
Created: 21/01/2011
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

*/

#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

void function_convert_YCbCr_NB(int Gpe)
{
   prom_images_struct *couleur = NULL, *NB = NULL;
   int i = 0, j = 0, link = -1;
   convert *temp=NULL;

   if (def_groupe[Gpe].ext == NULL)
   {
      /*allocation memoire pour la sauvegarde des donnees sur le lien */
      if (def_groupe[Gpe].data == NULL)
      {
         if ((temp = (convert *) malloc(sizeof(convert))) == NULL)
         {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
         }
         def_groupe[Gpe].data = (unsigned char *)temp;
      }

      /* parametres par default */

      temp->groupe_entree = -1;

      /*Finding the link */

      link = find_input_link(Gpe, 0);
      temp->groupe_entree = liaison[link].depart;

      /* Pour verifier l'existence du lien */
      if (link == -1)
      {
         printf
         ("Erreur : le groupe function_convert_RGB_NB n'a pas d'entree !\n");
         exit(EXIT_FAILURE);
      }

      /*image couleur du groupe precedent */
      couleur = (prom_images_struct *) def_groupe[temp->groupe_entree].ext;

      if (couleur == NULL)
      {
         printf("il n'y a pas d'image dans le gpe d'entree %d\n", i);
         return ;
      }

      if (couleur->nb_band != 3) EXIT_ON_ERROR("L'image en entree doit etre en couleur\n");

      /* recuperation des infos sur la taille */
      temp->nx = couleur->sx;
      temp->ny = couleur->sy;

      temp->n = temp->nx * temp->ny;

      /* allocation de memoire */
      NB = (void *) calloc_prom_image(couleur->image_number, couleur->sx, couleur->sy, 1);

      if (NB == NULL) EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! \n");

      def_groupe[Gpe].ext = NB;
   }
   else
   {
      NB = ((prom_images_struct *) def_groupe[Gpe].ext);
      temp = (convert *) ((prom_images_struct *) def_groupe[Gpe].data);

      /*image couleur du groupe precedent */
      couleur = (prom_images_struct *) def_groupe[temp->groupe_entree].ext;
   }

   for (j = 0; j < NB->image_number; j++)
      for (i = 0; i < temp->n; i++)
      {
         NB->images_table[j][i] = (unsigned char) (int) (couleur->images_table[j][3 * i]);
      }

   /*Fin du Timer et affichage des temps */
#ifdef TIME_TRACE
   gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

   if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
   {
      SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
      MicroSecondesFunctionTimeTrace =  OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
   }
   else
   {
      SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
      MicroSecondesFunctionTimeTrace =1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
   }
   printf("Fonction du groupe %d\n", Gpe);
   sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n", SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
   /*   affiche_message(MessageFunctionTimeTrace); */
   printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif


}
