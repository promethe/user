/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_convert_RGB_HSV.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: D BAILLY
- description: specific file creation
- date: 08/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-PI

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / blueiological / neural
- description: none/ XXX
- input expected greenroup: none/xxx
- where are the data?: none/xxx

Comments:

Known blueugs: none (yet!)

Todo:see authored fortestingreen and commentingreen the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

typedef struct data_convert_RGB_HSV
{
  int gpe_image;
}Data_convert_RGB_HSV;


void function_convert_RGB_HSV(int index_of_gpe)
{
  Data_convert_RGB_HSV *data=NULL;
  unsigned int i=0, cas;
  int l = -1, gpe_image = -1;
  float red=0.0, green=0.0, blue=0.0, teinte=0, saturation, min, max;
  unsigned int n ;

  prom_images_struct *image_input_struct = NULL, *image_output_struct = NULL;
  unsigned char *input_image, *output_image;

#ifdef DEBUG
  dprints("entering %s (%s line %d)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  
  if (def_groupe[index_of_gpe].ext == NULL)
  {
    if (def_groupe[index_of_gpe].data == NULL)
    {
      /*Finding the link */
      i=0;
      l = find_input_link(index_of_gpe, i);
      while (l != -1)
      {
        if (strstr(liaison[l].nom, "sync") != NULL)
        {
        }
        else
        {
          gpe_image = liaison[l].depart;
        }
        i++;
        l = find_input_link(index_of_gpe, i);
      }
      /* Pour verifier l'existence du lien */
      if (gpe_image == -1)
        EXIT_ON_ERROR("le groupe n'a pas d'entree !");

      /*allocation memoire pour la sauvegarde des donnees sur le lien */
      data = (Data_convert_RGB_HSV *)ALLOCATION(Data_convert_RGB_HSV);
      
      /* parametres par default */
      data->gpe_image = gpe_image;
      def_groupe[index_of_gpe].data = data;
    }
    data = (Data_convert_RGB_HSV *)def_groupe[index_of_gpe].data;
    image_input_struct = (prom_images_struct *) def_groupe[data->gpe_image].ext;
    
    /* Poured verifiered l'existence du lien */
    if (image_input_struct == NULL)
    {
      PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
      return ;
    }

    if (image_input_struct->nb_band != 3)
      EXIT_ON_ERROR("L'image en entree doit etre en couleur");

    /* allocation de memoire */
    image_output_struct = (prom_images_struct *)calloc_prom_image(1, image_input_struct->sx, image_input_struct->sy, 3);
    if (image_output_struct == NULL)
      EXIT_ON_ERROR("impossible d'allouer la memoire");
    def_groupe[index_of_gpe].ext = image_output_struct;
  }
  data = (Data_convert_RGB_HSV *)def_groupe[index_of_gpe].data;
  image_input_struct = (prom_images_struct *) def_groupe[data->gpe_image].ext;
  image_output_struct = (prom_images_struct *) def_groupe[index_of_gpe].ext;
  
  input_image = image_input_struct->images_table[0];
  output_image = image_output_struct->images_table[0];

  n = image_output_struct->sx * image_output_struct->sy ;
  for(i = 0; i < n; i++)
  {
    red = (float) (input_image[3 * i]);
    green = (float) (input_image[3 * i + 1]);
    blue = (float) (input_image[3 * i + 2]);
    max = red;
    min = red;
    if(red >= green && red >= blue)
    {
      cas = 1;
      max = red;
      if(blue >= green) min = green;
      else min = blue;
    }
    else if(green >= red && green >= blue)
    {
      cas = 2;
      max = green;
      if(red >= blue) min = blue;
      else min = red;
    }
    else if(blue >= red && blue >= green)
    {
      cas = 3;
      max = blue;
      if(green >= red ) min = red;
      else min = green;
    }
    else cas = -1;
    if(!(red > green || red < green || red < blue || red > blue)) cas = 0;
    
    switch(cas)
    {
      case 0:
        teinte = 0;
        break;
      case 1:
        teinte = (60 * (green-blue)/(max-min) + 360);
        if(teinte > 359.99999 || teinte < 0.0)
          teinte -= floorf(teinte/360.)*360.;
        break;
      case 2:
        teinte = (60 * (blue-red)/(max-min) + 120);
        break;
      case 3:
        teinte = (60 * (red-green)/(max-min) + 240);
        break;
      default:
        PRINT_WARNING("error, not a case I know (%d %d %d)", red , green, blue);
    }
    
    if(max < 0.000001)
      saturation = 0.0;
    else
      saturation = 1.0 - min / max;
    
    output_image[3 * i] = (unsigned char)((int)((teinte * 255.)/360.));
    output_image[3 * i + 1] = (unsigned char)((int)(255. * saturation));
    output_image[3 * i + 2] = (unsigned char)((int)(255. * max));
  }
  
#ifdef DEBUG
  dprints("pointeured qui deconne dans %s:%p\n", __FUNCTION__,
         ((prom_images_struct *) def_groupe[index_of_gpe].ext)->images_table[0]);
  dprints("end f_convert_RGB_HSV\n");
#endif
}
