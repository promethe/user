/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
defgroup f_save_groupe_to_disk_expression
ingroup libSensors

\file 
\brief xxxxxxxxxxxxxxxxx
ingroup f_save_groupe_to_disk_expression

Author: Oriane Dermy
Created: 12/05/15

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  same function (with less parametters) as function_save_images_to_disk_expressions, but to save neurons values instead of images.
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:


External Tools: 

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <zlib.h>
#include <string.h>


#include <Kernel_Function/find_input_link.h>

#define DEBUG 1

void function_save_groupe_to_disk_expression(int gpe_sortie)
{
    char *string = NULL, *ST, char_name[256];
    float act;
    /*unsigned char * im_table; */
    int i, j, N, M;
    int gpe_entree = 0;
	int gpe_supervision=-1;
    int *number;
	int expression=-1;
	int debut, debut2, pos;
	int max=-999;
	FILE *f, *fp;					/*pointeur sur le fichier dans lequel on sauvegarde les noms des images*/

#ifdef DEBUG
    printf("----function_save_groupe_to_disk---------\n");
#endif


	if (def_groupe[gpe_sortie].ext == NULL)
    {
        number = (int *) malloc(sizeof(int));
        *number = 0;
        def_groupe[gpe_sortie].ext = (void *) number;
    }
    else
    {
        number = (int *) def_groupe[gpe_sortie].ext;
        (*number)++;
    }
 /*----------------*/
    /*Finding the link */


    for (i = 0; i < nbre_liaison; i++)
    {
        if ( strstr(liaison[i].nom, "-entree") != NULL && liaison[i].arrivee == gpe_sortie)
        {
            string = liaison[i].nom;
            gpe_entree = liaison[i].depart;
        }
		else if(strstr(liaison[i].nom, "-neurone") != NULL && liaison[i].arrivee == gpe_sortie)
		{
			printf("le groupe de supervision est bien sélectionné !\n");
				gpe_supervision=liaison[i].depart;
		}
    }
    
#ifdef DEBUG
	printf("gpe_supervision=%d\n",gpe_supervision);
#endif
	
	debut=def_groupe[gpe_supervision].premier_ele;
	printf("calcul de l'expression\n");
	for(i=0;i<4;i++){
		if(neurone[debut+i].s>max)
		{
			max=neurone[debut+i].s;
			expression=i;
		}
	}
#ifdef DEBUG
printf("expression=%d\n",expression);
#endif



 /*----------------------------------------*/
    /*Searching the text name of image to save */
    ST = strstr(string, "-T");
    if ((ST != NULL))
    {
        i = 0;
        while (ST[2 + i] != '-' && ST[2 + i] != '\0')
        {
            char_name[i] = ST[2 + i];
            i++;
        }
			
		if(expression == 0){
			char_name[i++]='i';
		}
		
		if(expression == 1){
			char_name[i++]='_';
			char_name[i++]='n';
			char_name[i++]='e';
			char_name[i++]='u';
			char_name[i++]='t';
			char_name[i++]='r';
			char_name[i++]='e';
		}	
		if(expression == 2){
			char_name[i++]='a';
		}
		if(expression == 3){
			char_name[i++]='o';
		}
		/*if(expression == 4){
			char_name[i++]='_';
			char_name[i++]='s';
			char_name[i++]='u';
			char_name[i++]='r';
			char_name[i++]='p';
			char_name[i++]='r';
			char_name[i++]='i';
			char_name[i++]='s';
			char_name[i++]='e';
		}*/
        char_name[i] = '\0';
        
        //TODO j'ai changé pour que tout s'enregistre dans un même fichier
        //sprintf(char_name, "%s%d", char_name, (*number));
#ifdef DEBUG
        printf("Name is %s\n", char_name);
#endif
    }
    else
    {
        EXIT_ON_ERROR("Error in function_save_image,you must sepicied -Tname\n");
    }

    if (char_name != NULL)
    {
		
        fp = fopen(char_name, "a+");
		if (fp == NULL)
		{
			EXIT_ON_ERROR("ERROR in new_image_display_activity(%s): Impossible to create file %s\n", def_groupe[gpe_sortie].no_name, char_name);
		}else printf("fichier pour les images bien ouvert\n");
    }

    N = def_groupe[gpe_entree].taillex;
    M = 1;//def_groupe[gpe_entree].tailley;
	debut2 = def_groupe[gpe_entree].premier_ele;
	for (i=0;i<N; i++)
	{
		for(j=0;j<M;j++)
		{
			pos = i + j*N;
			act=neurone[debut2 + pos].s1;
			fprintf(fp,"%f ", act);
			printf("L'ecriture dans le document image est fait");
		}
	}
	
	fprintf(fp, "\n");
	fclose(fp);
	fp=NULL;
	
	f = fopen("listimage","a"); 
    if(f == NULL)
    {
      printf("Can not open the file in the function f_save_groupe_to_disk.\n");
    }
    
    fprintf(f,"%s\n", char_name);   
    fclose(f);  

}
