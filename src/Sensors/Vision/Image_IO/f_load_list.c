/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_load_list f_load_list
\brief 
\file  
\section Author
Name: R. Shirakawa
Created: 05/04/2006
\section Modified
- author: Julien Hirel 
- description: specific file creation
- date: 11/2008

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Reads a file containing paths to image files
The "ext" member of the group is set with a new image at each iteration of the function.
When the last image is reached, the activity of the first neuron is set to 1 and the function loops back to the first image

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-Kernel_Function/find_input_link()

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

#define FILE_NAME_SIZE 255

typedef struct
{
      int current_image;
} data_load_list;

void new_load_list(int gpe)
{
   int i,l, ret, n;
   int first_time;
   char param_link[FILE_NAME_SIZE];
   char *listfile_name = NULL;
   FILE *listfile = NULL;
   char image_name[FILE_NAME_SIZE];

   int width, height, nbands;
   unsigned char *im_table = NULL;

   data_load_list *my_data = NULL;
   prom_images_struct *pt = NULL;


   dprints("new_load_list(%s): entering function\n", def_groupe[gpe].no_name);

   /* recherche les liens en entree */
   l = 0;
   i = find_input_link(gpe, l);   
   while (i != -1)
   {
      if (prom_getopt(liaison[i].nom, "-f", param_link) == 2)
      {
	 listfile_name = param_link;
      }

      l++;
      i = find_input_link(gpe, l);  
   }

   if (listfile_name == NULL)
   {
      fprintf(stderr, "ERROR in new_load_list(%s): missing input link with format -f[filename]\n", def_groupe[gpe].no_name);
      exit(EXIT_FAILURE);
   }

   listfile = fopen(listfile_name,"r");
  
   if (listfile == NULL)
   {
      fprintf(stderr, "ERROR in new_load_list(%s): Cannot open file '%s'\n", def_groupe[gpe].no_name, listfile_name);
      exit(EXIT_FAILURE);
   }

   dprints("new_load_list(%s): File %s successfully opened\n", def_groupe[gpe].no_name, listfile_name);

   pt = (prom_images_struct *) malloc(sizeof(prom_images_struct));  
   def_groupe[gpe].ext = (void *) pt;
   pt->images_table[0] = NULL; 

   n = 0;
   first_time = 1;

   while (!feof(listfile))
   {
      ret = fscanf(listfile, "%s\n", image_name);
      
      if (ret != 1)
	 break;

      dprints("new_load_list(%s): opening image '%s'\n", def_groupe[gpe].no_name, image_name);
      
      load_image_from_disk("png", image_name, &im_table, &width, &height, &nbands);

      /* verification de la taille */
      if (first_time == 1)
      {
	 pt->sx = width;
	 pt->sy = height;
	 pt->nb_band = nbands;
	 first_time = 0;
      }
      else if((int) pt->sx != width || (int) pt->sy != height || (int) pt->nb_band != nbands)
      {
	 fprintf(stderr, "ERROR in new_load_list(%s): all the images must have the same size\n", def_groupe[gpe].no_name);
	 fprintf(stderr, "%d %d , %d %d ,%d %d \n", pt->sx, width, pt->sy, height,  pt->nb_band, nbands);
	 exit(EXIT_FAILURE);
      }

      n++;
      pt->images_table[n] = im_table; /* on garde l'image allouee dans load_image_from_disk */      
   }

   pt->image_number = n;

   /* passer les donnees a la prochaine passage de la fonction */
   my_data = (data_load_list *) malloc(sizeof(data_load_list));
   if (my_data == NULL)
   {
      fprintf(stderr, "ERROR in new_load_list(%s): malloc failed\n", def_groupe[gpe].no_name);
      exit(EXIT_FAILURE);
   }
   
   my_data->current_image = 1;
   def_groupe[gpe].data = my_data;

   dprints("new_load_list(%s): leaving function\n", def_groupe[gpe].no_name);
}

/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/

void function_load_list(int gpe)
{
    prom_images_struct *pt = NULL;
    data_load_list *my_data =  NULL;

    my_data = (data_load_list *) def_groupe[gpe].data;  /* recuperer les donnees du dernier passage */
    pt = (prom_images_struct *) def_groupe[gpe].ext;  /* recuperer les donnees du dernier passage */

    if (my_data == NULL)
    {
       fprintf(stderr, "ERROR in f_load_list(%s): loading data failed\n", def_groupe[gpe].no_name);
       exit(EXIT_FAILURE);
    }

    if (pt->image_number > 0)
       pt->images_table[0] = pt->images_table[my_data->current_image];

    dprints("f_load_list(%s): current_image = %d\n", def_groupe[gpe].no_name, my_data->current_image);
    my_data->current_image++;

    if (my_data->current_image > (int) pt->image_number)
    {

       dprints("f_load_list(%s): last image reached (%d), restarting from the first image. neuron activity = 1\n", def_groupe[gpe].no_name, my_data->current_image -1);

       my_data->current_image = 1;
       neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = 1;
    }
    else
       neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = 0;
}
