/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
- author: P.Delarboulas
- description: set camera parameters
- date: 20/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 set camera parameters
	- each neuron tunes one parameters in this orders :
	      - exposure;
	      - gain;
    	      - brigthness;
    	      - balance (need 2 neurons, first one for BU and second one for RV;
              - sharpness;
    	      - saturation;
              - shutter;
      	      - gamma;

Macro:
-none

Local variables:
-none

Global variables:

Internal Tools:

External Tools:

Links:

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <Global_Var/Vision.h>

#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_set_camera_param
{
   Camera *cam;
   int gpe_vigilance;
   int nb_features;
   unsigned int exposure_min;
   unsigned int exposure_max;
   unsigned int gain_min;
   unsigned int gain_max;
   unsigned int brigthness_min;
   unsigned int brigthness_max;
   unsigned int white_balance_min;
   unsigned int white_balance_max;
   unsigned int sharpness_min;
   unsigned int sharpness_max;
   unsigned int saturation_min;
   unsigned int saturation_max;
   unsigned int shutter_min;
   unsigned int shutter_max;
   unsigned int gamma_min;
   unsigned int gamma_max;

   unsigned int with_exposure;
   unsigned int with_gain;
   unsigned int with_brigthness;
   unsigned int with_white_balance;
   unsigned int with_sharpness;
   unsigned int with_saturation;
   unsigned int with_shutter;
   unsigned int with_gamma;

} MyData_f_set_camera_param;

float calcul_activity(int n_id)
{
   type_coeff *coeff;
   float sum=0;

   coeff = neurone[n_id].coeff;

   while (coeff != NULL)
   {
      sum+=neurone[coeff->entree].s1 *  coeff->val ;
      coeff = coeff->s;
   }
   return sum;
}

void new_set_camera_param(int gpe)
{
   int lien_entrant = -1;
   char param_link[256], name_cam[256];
   MyData_f_set_camera_param * my_data;
   Camera *cam = NULL;
   int nb_features=0;
   int l=0;

   unsigned int with_exposure=0;
   unsigned int with_gain=0;
   unsigned int with_brigthness=0;
   unsigned int with_white_balance=0;
   unsigned int with_sharpness=0;
   unsigned int with_saturation=0;
   unsigned int with_shutter=0;
   unsigned int with_gamma=0;

   dprints("enter in %s\n", __FUNCTION__);

   lien_entrant = find_input_link(gpe, l);
   while (lien_entrant != -1)
   {
      if (prom_getopt(liaison[lien_entrant].nom, "-h", param_link) == 2 || prom_getopt(liaison[lien_entrant].nom, "-H", param_link)== 2)
      {
         strcpy(name_cam, param_link);
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-E", param_link) || prom_getopt(liaison[lien_entrant].nom, "-e", param_link) )
      {
         with_exposure = 1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-G", param_link) || prom_getopt(liaison[lien_entrant].nom, "-g", param_link) )
      {
         with_gain =1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-B", param_link) || prom_getopt(liaison[lien_entrant].nom, "-b", param_link) )
      {
         with_brigthness = 1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-W", param_link) || prom_getopt(liaison[lien_entrant].nom, "-w", param_link) )
      {
         with_white_balance = 1;
         nb_features+=2;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-S", param_link) || prom_getopt(liaison[lien_entrant].nom, "-s", param_link) )
      {
         with_sharpness = 1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-T", param_link) || prom_getopt(liaison[lien_entrant].nom, "-t", param_link) )
      {
         with_saturation = 1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-U", param_link) || prom_getopt(liaison[lien_entrant].nom, "-u", param_link) )
      {
         with_shutter = 1;
         nb_features++;
      }
      if (prom_getopt(liaison[lien_entrant].nom, "-A", param_link) || prom_getopt(liaison[lien_entrant].nom, "-a", param_link) )
      {
         with_gamma = 1;
         nb_features++;
      }
      l++;
      lien_entrant = find_input_link(gpe, l);
   }
   if ( def_groupe[gpe].nbre != nb_features )
   {
      EXIT_ON_ERROR("You must have one neuron per feature (%d != %d)  : group %s and function %s\n", def_groupe[gpe].nbre, nb_features , def_groupe[gpe].no_name ,__FUNCTION__);
   }

   cam = camera_get_cam_by_name(name_cam);
   if (cam == NULL)
   {
      EXIT_ON_ERROR("Error bad camera name : %s in %s %s\n", name_cam  , def_groupe[gpe].no_name ,__FUNCTION__);
   }

   my_data = malloc(sizeof(MyData_f_set_camera_param));
   if ( my_data == NULL)
   {
      EXIT_ON_ERROR("No more memory, error malloc  in %s %s\n",  def_groupe[gpe].no_name ,__FUNCTION__);
   }
   my_data->nb_features=nb_features;
   my_data->cam=cam;

   my_data->with_exposure=with_exposure;
   if ( with_exposure )
   {
      camera_get_exposure_boundaries(my_data->cam, &(my_data->exposure_min) , &(my_data->exposure_max) );
      
	if( my_data->exposure_max == 0 ) my_data->exposure_max = 255;	
   }

   my_data->with_gain=with_gain;
   if ( with_gain )
   {
      camera_get_gain_boundaries(my_data->cam, &(my_data->gain_min) , &(my_data->gain_max) );
      
	if( my_data->gain_max == 0 ) my_data->gain_max = 255;	
   }

   my_data->with_brigthness=with_brigthness;
   if ( with_brigthness )
   {
      camera_get_brightness_boundaries(my_data->cam, &(my_data->brigthness_min) , &(my_data->brigthness_max) );

      if( my_data->brigthness_max == 0 ) my_data->brigthness_max = 255;	
	
   }

   my_data->with_white_balance=with_white_balance;
   if ( with_white_balance )
   {
      camera_get_white_balance_boundaries(my_data->cam, &(my_data->white_balance_min) , &(my_data->white_balance_max) );
      if( my_data->white_balance_max == 0 ) my_data->white_balance_max = 255;	
   }

   my_data->with_sharpness=with_sharpness;
   if ( with_sharpness )
   {
      camera_get_sharpness_boundaries(my_data->cam, &(my_data->sharpness_min) , &(my_data->sharpness_max) );
	
      if( my_data->sharpness_max == 0 ) my_data->sharpness_max = 255;	
   }

   my_data->with_saturation=with_saturation;
   if ( with_saturation )
   {
      camera_get_saturation_boundaries(my_data->cam, &(my_data->saturation_min) , &(my_data->saturation_max) );
      
      if( my_data->saturation_max == 0 ) my_data->saturation_max = 255;	
   }

   my_data->with_shutter=with_shutter;
   if ( with_shutter )
   {
      camera_get_shutter_boundaries(my_data->cam, &(my_data->shutter_min) , &(my_data->shutter_max) );
      
      if( my_data->shutter_max == 0 ) my_data->shutter_max = 255;	
   }

   my_data->with_gamma=with_gamma;
   if ( with_gamma )
   {
      camera_get_gamma_boundaries(my_data->cam, &(my_data->gamma_min) , &(my_data->gamma_max) );

      if( my_data->gamma_max == 0 ) my_data->gamma_max = 255;	
      	
   }

   def_groupe[gpe].data = my_data;
   dprints("end of %s\n", __FUNCTION__);

}

void function_set_camera_param(int gpe)
{
   int nbre;
   int n_id;

   MyData_f_set_camera_param * my_data ;
   my_data = (MyData_f_set_camera_param *) def_groupe[gpe].data;

   nbre = def_groupe[gpe].nbre;
   n_id = def_groupe[gpe].premier_ele;


   dprints("Deb of %s\n", __FUNCTION__);
   if ( my_data->with_exposure )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );

	printf("Set cam %f \n ", neurone[n_id].s);

      camera_set_exposure_value(my_data->cam,  neurone[n_id].s * my_data->exposure_max + my_data->exposure_min);
      n_id++;
   }
   if ( my_data->with_gain )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_gain_value(my_data->cam,  neurone[n_id].s * my_data->gain_max + my_data->gain_min);
      n_id++;
   }
   if ( my_data->with_brigthness )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_brightness_value(my_data->cam,  neurone[n_id].s * my_data->brigthness_max + my_data->brigthness_min);
	

	printf("Set cam %f %d %d \n ", neurone[n_id].s,  my_data->brigthness_max,  my_data->brigthness_min  );
      
	n_id++;
	

   }
   if ( my_data->with_white_balance )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      neurone[n_id+1].s = neurone[n_id+1].s1 = neurone[n_id+1].s2 =  calcul_activity( n_id+1 );
      camera_set_white_balance_values(my_data->cam,  neurone[n_id].s * my_data->white_balance_max + my_data->white_balance_min,  neurone[n_id+1].s * my_data->white_balance_max + my_data->white_balance_min);
      n_id+=2;
   }
   if ( my_data->with_sharpness )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_sharpness_value(my_data->cam,  neurone[n_id].s * my_data->sharpness_max + my_data->sharpness_min);
      n_id++;
   }
   if ( my_data->with_saturation )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_saturation_value(my_data->cam,  neurone[n_id].s * my_data->saturation_max + my_data->saturation_min);
      n_id++;
   }
   if ( my_data->with_shutter )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_shutter_value(my_data->cam,  neurone[n_id].s * my_data->shutter_max + my_data->shutter_min);
      n_id++;
   }
   if ( my_data->with_gamma )
   {
      neurone[n_id].s = neurone[n_id].s1 = neurone[n_id].s2 =  calcul_activity( n_id );
      camera_set_gamma_value(my_data->cam,  neurone[n_id].s * my_data->gamma_max + my_data->gamma_min);
      n_id++;
   }
   dprints("end of %s\n", __FUNCTION__);
}
