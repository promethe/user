/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  save_lena_to_disk.c
\brief

Author: xxxxxxx
Created: xxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:

Description:
This function only saves 1*8bits(gray)
It could save multiple images name_???.lena*
 
Macro:
-none

Local variables:
-none

Global variables:
-none

-Internal tools:
-none

External tools:
-none 

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <stdio.h>
#include <structlena.h>



void save_lena_to_disk(char *im_name, prom_images_struct prom_image)
{
    image imlena;
    descripteur d_imlena;
    int i;
    char im_name2[256], name[256];

  /*----------------------------------*/
    /* Get Name without extension .lena */
    for (i = 0; im_name[i] != '.'; i++)
        name[i] = im_name[i];
    name[i] = '\0';

  /*-------------------------*/
    /*How mmany image to store? */
    for (i = 0; i < (prom_image.image_number); i++)
    {

        d_imlena.nx = prom_image.sx;
        d_imlena.ny = prom_image.sy;
        d_imlena.nz = 1;
        d_imlena.typ = 0;
        strcpy(d_imlena.nature, "INTENSITE");

        if (prom_image.image_number == 1)
        {
            /* original name without modif */
            strcpy(im_name2, im_name);
        }
        else
        {
            /* name will be name_???.lena */
            sprintf(im_name2, "%s_%03d.lena", name, i);
        }

        imlena = ouvre('e', &d_imlena, im_name2);
        ecri(&imlena, prom_image.images_table[i], prom_image.sy);
#ifdef DEBUG
        printf("Saving LENA picture %s finished width=%i height=%i\n",
               im_name2, prom_image.sx, prom_image.sy);
#endif

    }
}
