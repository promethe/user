/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  yuv2rgb.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Transforms data		  yuvyuvyuv... to rgbrgbrgb.........
 Take care, this function allocating memory at the returned pointer

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

unsigned char *yuv2rgb(unsigned char *yuv_im, int w, int h)
{
    unsigned char *temp;
    int i, j, b, p, nbands = 3;
    int tp, yy, u, v;

  /*--------------------*/
    /*allocating new table */
    temp = (unsigned char *) calloc(nbands * h * w, sizeof(unsigned char));
    if (temp == NULL)
    {
        printf("Impossible to allocate new memory in yuv2rgb function\n");
        exit(EXIT_FAILURE);
    }

  /*--------------------------------*/
    /*Processing color transformations */
    for (p = 0, i = 0; i < h; i++)
        for (j = 0; j < w; j++)
            for (b = 0; b < nbands; b++, p++)
            {
                switch (b)
                {
                case 0:
                    {
                        /*Obtention du Rouge */
                        yy = yuv_im[p];
                        u = yuv_im[p + 1];
                        v = yuv_im[p + 2];

                        tp = (yy + 15 * (v - 128) / 11);
                        if (tp > 255)
                            tp = 255;
                        if (tp < 0)
                            tp = 0;
                        temp[p] = (unsigned char) tp;
                    }
                    break;

                case 1:
                    {
                        /*Obtention du Vert */
                        yy = yuv_im[p - 1];
                        u = yuv_im[p];
                        v = yuv_im[p + 1];

                        tp = (139 * yy - 41 * (u - 128) -
                              67 * (v - 128)) / 125;
                        if (tp > 255)
                            tp = 255;
                        if (tp < 0)
                            tp = 0;
                        temp[p] = (unsigned char) tp;
                    }
                    break;

                case 2:
                    {
                        /*Obtention du Bleu */
                        yy = yuv_im[p - 2];
                        u = yuv_im[p - 1];
                        v = yuv_im[p];

                        tp = yy + 7 * (u - 128) / 4;
                        if (tp > 255)
                            tp = 255;
                        if (tp < 0)
                            tp = 0;
                        temp[p] = (unsigned char) tp;
                    }
                    break;
                }
            }

    return (temp);
}
