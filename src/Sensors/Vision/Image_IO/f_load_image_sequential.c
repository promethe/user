/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \ingroup libSensors
 \defgroup f_load_image_sequential f_load_image_sequential
 \brief
 \file
 /section Options
 -  -f:nam of the liste of images
 -  -t:
 -  -r:reload in a infinite loop

 - \f$  LaTeX equation: none \f$  

 \section Description
 Charge une image du disque vers une fenetre de promethe par exemple image1

 En ajoutant un \% (pourcent) dans une des lignes. La ligne n'est pas lue et l'image non affich��e


 */

/*#define DEBUG*/

#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include "../../prom_kernel/include/promethe.h"

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <public_tools/Vision.h>

#ifdef USE_THREADS
extern pthread_mutex_t mutex_lecture_clavier;
#endif

/*typedef struct MyData_f_load_image_sequential
 {
 char listeim[256];
 char type[256];
 FILE *jice_fichier_liste;
 char  jice_nom_image[256];
 } MyData_f_load_image_sequential;*/

void function_load_image_sequential(int Gpe)
{
   MyData_f_load_image_sequential *mydata = NULL;

   char jice_nom_image[STRING_SIZE];
   char listeim[STRING_SIZE] = "listimage";
   char type[STRING_SIZE] = "PNG";
   char reponse_user[STRING_SIZE];
   char param[STRING_SIZE];
   int etat;
   int test_scanf;

   int l, i, size;
   int width, height, nbands;
   char *ret;

   unsigned char *im_table;
   int reload = 0;
   prom_images_struct *pt;
   FILE *jice_fichier_liste = NULL;

   dprints("\nfunction_load_image_sequential\n");
   i = 0;
   if (def_groupe[Gpe].data == NULL)
   {
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         if (strstr(liaison[l].nom, "sync") != NULL)
         {
         }
         else
         {
            if (prom_getopt(liaison[l].nom, "f", param) >= 2) sprintf(listeim, "%s", param);
            if (prom_getopt(liaison[l].nom, "t", param) >= 2) sprintf(type, "%s", param);
            if (prom_getopt(liaison[l].nom, "r", param) >= 1) reload = 1;
         }

         i++;
         l = find_input_link(Gpe, i);
      }

      mydata = (MyData_f_load_image_sequential *) malloc(sizeof(MyData_f_load_image_sequential));
      if (mydata == NULL) EXIT_ON_ERROR("%s erreur malloc\n", __FUNCTION__);

      pt = (prom_images_struct *) malloc(sizeof(prom_images_struct));
      pt->sx = -1;
      pt->sy = -1;
      pt->nb_band = -1;

      if (pt == NULL)
      {
         EXIT_ON_ERROR("%s erreur sur malloc de pt\n", __FUNCTION__);
      }

      /* ouverture du fichier liste d'image */
      dprints("ouverture de la liste d'image '%s'\n", listeim);

      jice_fichier_liste = fopen(listeim, "r");
      if (jice_fichier_liste == NULL)
      {
         EXIT_ON_ERROR("%s impossible d'ouvrir '%s'\n", __FUNCTION__, listeim);
      }

      pt->images_table[0] = NULL;

      sprintf(mydata->type, "%s", type);
      sprintf(mydata->listeim, "%s", listeim);
      mydata->reload = reload;
      mydata->jice_fichier_liste = (FILE *) jice_fichier_liste;

      def_groupe[Gpe].data = (MyData_f_load_image_sequential *) mydata;
      def_groupe[Gpe].ext = (prom_images_struct *) pt;
   }
   else
   {
      mydata = def_groupe[Gpe].data;
      sprintf(listeim, "%s", mydata->listeim);
      jice_fichier_liste = mydata->jice_fichier_liste;
      sprintf(type, "%s", mydata->type);

      pt = (prom_images_struct *) def_groupe[Gpe].ext;
   }

   relecture: etat = feof(jice_fichier_liste);

   if (etat != 0) /* fin du fichier */
   {
      if (mydata->reload == 1)
      {
         fclose(jice_fichier_liste);
         jice_fichier_liste = fopen(listeim, "r");
         mydata->jice_fichier_liste = jice_fichier_liste;
         ((MyData_f_load_image_sequential *) def_groupe[Gpe].data)->jice_fichier_liste = jice_fichier_liste;
      }
      else
      {

#ifdef USE_THREADS
         pthread_mutex_lock(&mutex_lecture_clavier);
#endif
         cprints("%s (%s) fin du fichier d'image '%s':\n \t Voulez vous rejouer la sequence [o/n] : ", __FUNCTION__, def_groupe[Gpe].no_name, listeim);
         test_scanf = scanf("%s", reponse_user);
#ifdef USE_THREADS
         pthread_mutex_unlock(&mutex_lecture_clavier);
#endif     
         if (test_scanf == 1)
         {
            if (reponse_user[0] != 'n')
            {
               fclose(jice_fichier_liste);
               jice_fichier_liste = fopen(listeim, "r");
               mydata->jice_fichier_liste = jice_fichier_liste;
            }
            else
            {
               cprints("Promethe va s'arreter\n");
               printf("avant le cancel pressed\n");
               cancel_pressed(NULL, NULL);
               PRINT_WARNING("FINSHING FUNCTION IN PROGRESS AND QUITING\n");
               return;
               /*exit(0);*/
            }
         }
      }
   }

   /*if(fscanf(jice_fichier_liste, "%s\n", jice_nom_image) == 1) {
    cprints("ouverture de '%s' au format %s\n", jice_nom_image,type);
    }*/
   //ret=fscanf(jice_fichier_liste, "%s\n", jice_nom_image);
   ret = fgets(jice_nom_image, STRING_SIZE, jice_fichier_liste);
   printf("%s (1) nom lu : %s \n",__FUNCTION__, jice_nom_image);
   while ( (strstr(jice_nom_image, "%")!=NULL) || (strlen(jice_nom_image) < 2) )
   {
      //	ret=fscanf(jice_fichier_liste, "%s\n", jice_nom_image);
      ret = fgets(jice_nom_image, STRING_SIZE, jice_fichier_liste);
      printf("%s (2) nom lu : %s \n",__FUNCTION__, jice_nom_image);
      if (ret == NULL || feof(jice_fichier_liste))
      {
         printf("goto relecture !\n");
         goto relecture;
      }
      if (ret == NULL) PRINT_WARNING("Probleme au niveau des arguments\n");
   }

//	cprints("ouverture de '%s' au format %s\n", jice_nom_image,type);
   strcpy(mydata->jice_nom_image, jice_nom_image);

   /*copy necessaire pour utiliser le nom de l'image comme variable globale Ali K. Overlap*/
   /*strcpy(((MyData_f_load_image_sequential *) def_groupe[Gpe].data)->jice_nom_image, mydata->jice_nom_image);*/

   printf("%s (3 -OK) nom lu : %s \n",__FUNCTION__, jice_nom_image);

   size = strlen(jice_nom_image);
   if (size > 1) if (jice_nom_image[size - 1] == '\n') jice_nom_image[size - 1] = 0; /* suppression du retour à la ligne pour ne garder que le nom correct*/

   load_image_from_disk(type, jice_nom_image, &im_table, &width, &height, &nbands);
   if (((int) pt->sx != width) || ((int) pt->sy != height) || ((int) pt->nb_band != nbands))
   {
      if ((pt->images_table[0] != NULL))
      {
         dprints("warning : change adresse du pointeur de sortie (taille d'image variable ?) !\n");
         free(pt->images_table[0]);
      }
      pt->images_table[0] = NULL;
   }

   pt->sx = width;
   pt->sy = height;
   pt->nb_band = nbands;
   /* rajout du 23/07/2003 Olivier Ledoux */
   pt->image_number = 1;

   if ((pt->images_table[0] == NULL))
   {
      pt->images_table[0] = (unsigned char *) malloc(sizeof(unsigned char) * width * height * nbands);
   }

   /* copie vers la structure du groupe */
   memcpy(pt->images_table[0], im_table, width * height * nbands * sizeof(unsigned char));

   /* liberation de l'espace memoire ayant servit a charger l'image */
   free(im_table); /* PG: dommage ce n'est pas efficace. Si la taille ne change pas il faudrait utiliser la meme zone*/
   /*def_groupe[Gpe].ext = pt; */
   dprints("sortie de function_load_image_sequential\n");
}

void destroy_load_image_sequential(int Gpe)
{
   MyData_f_load_image_sequential *mydata = NULL;
   prom_images_struct *pt = NULL;

   mydata = (MyData_f_load_image_sequential*) def_groupe[Gpe].data;
   fclose((mydata)->jice_fichier_liste);
   mydata->jice_fichier_liste = NULL;
   free(mydata);
   mydata = NULL;

   pt = (prom_images_struct *) def_groupe[Gpe].ext;
   free(pt->images_table[0]);
   pt->images_table[0] = NULL;
   free(pt);
   pt = NULL;

   def_groupe[Gpe].ext = NULL;
   def_groupe[Gpe].data = NULL;
}
