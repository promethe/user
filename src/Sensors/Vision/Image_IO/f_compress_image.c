/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <string.h>

#include <net_message_debug_dist.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <limits.h>

#include <jpeglib.h>

typedef struct memory_destination_mgr_def
{
   struct jpeg_destination_mgr super;

   unsigned int buffer_size;
   unsigned char *current_image;
   unsigned int current_image_size;
} mem_dst_mgr;

typedef struct compress_image
{
   type_groupe *input_group;
   type_groupe *gpe;
   prom_images_struct *images;
   int quality;

   int count;
   int save;

   struct jpeg_compress_struct cinfo;
   struct jpeg_error_mgr jerr;

} type_compress_image;




static void jpeg_init(type_compress_image *data);
static void jpeg_save(prom_images_struct_compressed* img, int img_id, int count);

//Custom destination manager for JPEG compressor
static mem_dst_mgr * mem_dst_mgr_new(struct jpeg_compress_struct *cinfo, unsigned int jpeg_max_size);
static void mem_dst_mgr_destroy(mem_dst_mgr * mgr);
static void mem_dst_mgr_set_image(type_compress_image *my_data, int img_id);
static void mem_dst_mgr_update_image_size(type_compress_image *my_data, int img_id);
//jpeg memory manager callbacks
static void mem_dst_mgr_init(struct jpeg_compress_struct * cinfo);
static int mem_dst_mgr_empty_output_buffer(struct jpeg_compress_struct * cinfo);
static void mem_dst_mgr_term(struct jpeg_compress_struct * cinfo);

void new_compress_image(int gpe)
{

   type_groupe *group;
   type_compress_image *my_data;
   int link_id=0,links_nb=0;
   char *link_name;

   my_data = ALLOCATION(type_compress_image);

   group = &def_groupe[gpe];
   group->data = my_data;
   my_data->gpe = group;

   for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, links_nb))
   {
      link_name = liaison[link_id].nom;

      if (strncmp(link_name, "sync", 4) != 0)
      {
         if (my_data->input_group != NULL ) EXIT_ON_GROUP_ERROR(gpe, "You cannot have more than one incoming group or call it 'sync'.");
         my_data->input_group = &def_groupe[liaison[link_id].depart];
      }
      if (prom_getopt_int(link_name, "-q", &my_data->quality) == 1) EXIT_ON_ERROR("Option -q on %s is expecting a integer", liaison[link_id].nom);
      if (prom_getopt(link_name,"-save", NULL) == 1) my_data->save = 1;
      else my_data->save = 0;

      links_nb++;
   }

   my_data->images = NULL;
   my_data->count = 0;
}

void function_compress_image(int gpe)
{
   prom_images_struct *prom_images;
   type_groupe *group;
   type_compress_image *my_data;
   int i, row_stride;
   JSAMPROW row_pointer[1];

   group = &def_groupe[gpe];
   my_data = (type_compress_image*)group->data;
   prom_images = (prom_images_struct *) my_data->input_group->ext;

   if (prom_images == NULL)
   {
      return ;
   }

   if (my_data->images == NULL)
   {
      my_data->images = prom_images;
      jpeg_init(my_data);
   }

   for (i=0; i<my_data->images->image_number; i++)
   {

      mem_dst_mgr_set_image(my_data,i);

      jpeg_start_compress(&my_data->cinfo, TRUE);

      row_stride = my_data->images->sx * my_data->images->nb_band;
      while (my_data->cinfo.next_scanline < my_data->cinfo.image_height)
      {
         row_pointer[0] = & my_data->images->images_table[i][my_data->cinfo.next_scanline * row_stride];
         jpeg_write_scanlines(&my_data->cinfo, row_pointer, 1);
      }

      jpeg_finish_compress(&my_data->cinfo);

      mem_dst_mgr_update_image_size(my_data,i);

      if (my_data->save) jpeg_save((prom_images_struct_compressed*)group->ext,i,my_data->count);
   }

   my_data->count++;
}

void destroy_compress_image(int gpe)
{
   printf("%i\n",gpe);
}

static void jpeg_save(prom_images_struct_compressed* img, int img_id, int count)
{
   FILE *outfile;
   char filename[NAME_MAX];

   sprintf(filename,"output/output-%i-%i.jpg",img_id,count);
   if ((outfile = fopen(filename, "wb")) == NULL)
   {
      EXIT_ON_ERROR("jpeg_save: Cannot open %s.",filename);
   }
   fwrite(img->super.images_table[img_id],sizeof(unsigned char),img->images_size[img_id],outfile);
   fclose(outfile);

}

static void jpeg_init(type_compress_image *data)
{

   prom_images_struct_compressed *image_struct;
   int jpeg_max_size,i;

   data->cinfo.err = jpeg_std_error(&data->jerr);
   jpeg_create_compress(&data->cinfo);

   data->cinfo.image_height = data->images->sy;
   data->cinfo.image_width = data->images->sx;
   data->cinfo.input_components = data->images->nb_band;

   switch (data->images->nb_band)
   {
      case 1:
         data->cinfo.in_color_space = JCS_GRAYSCALE;
         break;
      case 3:
         data->cinfo.in_color_space = JCS_RGB;
         break;
   }

   jpeg_set_defaults(&data->cinfo);

   printf("JPEG image spec: %i:%ix%i\n",data->images->sx,data->images->sy,data->images->nb_band);

   data->cinfo.dct_method = JDCT_IFAST;
   data->cinfo.do_fancy_downsampling = FALSE;

   jpeg_set_quality(&data->cinfo, data->quality,TRUE);


   image_struct = data->gpe->ext = ALLOCATION(prom_images_struct_compressed);

   image_struct->super.image_number = data->images->image_number;
   image_struct->super.sx = data->images->sx;
   image_struct->super.sy = data->images->sy;
   image_struct->super.nb_band = data->images->nb_band;

   //We suppose that a compressed image won't be larger than the original image (sic)
   jpeg_max_size = data->images->sx * data->images->sy;
   for (i=0; i<image_struct->super.image_number; i++)
   {
      image_struct->super.images_table[i] = MANY_ALLOCATIONS(jpeg_max_size, unsigned char);
      image_struct->images_size[i] = 0;
   }

   mem_dst_mgr_new(&data->cinfo, jpeg_max_size);
}

static mem_dst_mgr * mem_dst_mgr_new(struct jpeg_compress_struct *cinfo, unsigned int jpeg_max_size)
{
   mem_dst_mgr * mgr = malloc(sizeof(mem_dst_mgr));

   mgr->super.init_destination = mem_dst_mgr_init;
   mgr->super.empty_output_buffer = mem_dst_mgr_empty_output_buffer;
   mgr->super.term_destination = mem_dst_mgr_term;

   mgr->buffer_size = jpeg_max_size;

   cinfo->dest = (struct jpeg_destination_mgr *)mgr;

   return mgr;
}

static void mem_dst_mgr_destroy(mem_dst_mgr * mgr)
{
   free(mgr);
}

static void mem_dst_mgr_set_image(type_compress_image *my_data, int img_id)
{
   mem_dst_mgr * mgr = (mem_dst_mgr*) my_data->cinfo.dest;

   mgr->current_image = ((prom_images_struct_compressed*)my_data->gpe->ext)->super.images_table[img_id];
   mgr->current_image_size = 0;

}

static void mem_dst_mgr_update_image_size(type_compress_image *my_data, int img_id)
{
   mem_dst_mgr * mgr = (mem_dst_mgr*) my_data->cinfo.dest;
   ((prom_images_struct_compressed*)my_data->gpe->ext)->images_size[img_id] = mgr->current_image_size;
}

static void mem_dst_mgr_init(struct jpeg_compress_struct * cinfo)
{
   mem_dst_mgr * mgr = (mem_dst_mgr*) cinfo->dest;
   mgr->super.next_output_byte = mgr->current_image;
   mgr->super.free_in_buffer = mgr->buffer_size;
}


static int mem_dst_mgr_empty_output_buffer(struct jpeg_compress_struct * cinfo)
{
   mem_dst_mgr_init(cinfo);
   return 1;
}
static void mem_dst_mgr_term(struct jpeg_compress_struct * cinfo)
{
   mem_dst_mgr * mgr = (mem_dst_mgr*) cinfo->dest;
   mgr->current_image_size = mgr->buffer_size - mgr->super.free_in_buffer;
}
