/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <string.h>

#include <net_message_debug_dist.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>
#include <limits.h>

#include <jpeglib.h>


typedef struct uncompress_image{
	type_groupe *input_group;
	type_groupe *gpe;
	prom_images_struct_compressed *images;

	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	int count;

} type_uncompress_image;


static void jpeg_init(type_uncompress_image* data);


void new_uncompress_image(int gpe){

	type_groupe *group;
	type_uncompress_image *my_data;
	int link_id=0,links_nb=0;
	char *link_name;

	my_data = ALLOCATION(type_uncompress_image);

	group = &def_groupe[gpe];
	group->data = my_data;
	my_data->gpe = group;

	for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, links_nb))
	{
		link_name = liaison[link_id].nom;

		if (strncmp(link_name, "sync", 4) != 0)
		{
			if (my_data->input_group != NULL ) EXIT_ON_GROUP_ERROR(gpe, "You cannot have more than one incoming group or call it 'sync'.");
			my_data->input_group = &def_groupe[liaison[link_id].depart];
		}

		links_nb++;
	}

	my_data->count = 0;

}
void function_uncompress_image(int gpe){
	type_groupe *group;
	type_uncompress_image *my_data;
	unsigned int i, row_stride;
	prom_images_struct_compressed * prom_images;

	group = &def_groupe[gpe];
	my_data = (type_uncompress_image*)group->data;
	prom_images = (prom_images_struct_compressed *) my_data->input_group->ext;

	if(prom_images == NULL) {
			return ;
	}

/*
	if(my_data->images == NULL){
		my_data->images = prom_images;
		jpeg_init(my_data);
	}


	for(i=0;i<my_data->images->super.image_number;i++){

		//TODO : set image

		jpeg_read_header(&my_data->cinfo,TRUE);
		row_stride = my_data->cinfo.output_width * my_data->cinfo.output_components;

		jpeg_start_decompress(&my_data->cinfo);

		while (&my_data->cinfo.output_scanline < &my_data->cinfo.output_height) {
			jpeg_read_scanlines(&my_data->cinfo, buffer, 1);
		}

		jpeg_finish_decompress(&my_data->cinfo);
	}*/
}
void destroy_uncompress_image(int gpe){

}


static void jpeg_init(type_uncompress_image* data){/*

	data->cinfo.err = jpeg_std_error(&data->jerr);
	jpeg_create_decompress(&data->cinfo);

	//TODO: mem src manager*/
}
