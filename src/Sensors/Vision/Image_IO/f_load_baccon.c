/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_load_baccon.c
\brief

Author: JC Baccon
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-char jice_nom_image[256]

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/Vision/load_image_from_disk()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>

char jice_nom_image[256];       /* pour utilisation par f_demande_numero */

void function_load_baccon(int Gpe)
{
   int width, height, nbands;
   unsigned char *im_table=NULL;

   char listeim[256] = "listimage";

   char reponse_user[256];
   prom_images_struct *p_result=NULL;
   static FILE *jice_fichier_liste=NULL;

   dprints("\nfunction_load_baccon\n");

   if (def_groupe[Gpe].ext == NULL)
   {
      /*printf("def_groupe.ext n'existe pas\n"); */
      def_groupe[Gpe].ext = ALLOCATION(prom_images_struct);

      /* ouverture du fichier liste d'image */
      dprints("ouverture de la liste d'image '%s'\n", listeim);

      jice_fichier_liste = fopen(listeim, "r");
      if (jice_fichier_liste == NULL)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "impossible d'ouvrir '%s'\n", listeim);
      }

      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = NULL;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sx = -1;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sy = -1;
      ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 0;

      /* rajout du 23/07/2003 Olivier Ledoux */
      ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
   }
   else
   {
      /* rien !!!! */
   }

   p_result = (prom_images_struct *) def_groupe[Gpe].ext;

   if (feof(jice_fichier_liste))
   {
      cprints("fin du fichier d'image '%s': voulez vous rejouer la sequence [o/n] : ",listeim);
      cscans("%s", reponse_user);
      if (reponse_user[0] != 'n')
      {
         fclose(jice_fichier_liste);
         jice_fichier_liste = fopen(listeim, "r");
      }
      else
      {
         fclose(jice_fichier_liste);
         EXIT_ON_GROUP_ERROR(Gpe, "Promethe va s'arreter\n");
      }
   }

   TRY_FSCANF(1, jice_fichier_liste, "%s\n", jice_nom_image);

   dprints("ouverture de '%s'\n", jice_nom_image);

   load_image_from_disk("png", jice_nom_image, &im_table, &width, &height,&nbands);

   if (  (p_result->sx != width)||(p_result->sy != height)||(p_result->nb_band != nbands)   )/*1ere fois et chgt de taille*/
   {
      if ((p_result->images_table[0] != NULL))/*s il avait deja ete alloue*/
      {
         dprints("warning : change adresse du pointeur de sortie (taille d'image variable ?) !\n");

         free(p_result->images_table[0]);
         p_result->images_table[0] = NULL;
      }
      p_result->images_table[0] = (unsigned char *) malloc(sizeof(unsigned char) * width * height *  nbands);
      if ( p_result->images_table[0]==NULL )
      {
         EXIT_ON_ERROR("Erreur allocation memoire dans f_load_baccon gpe %d \n",Gpe);
      }
      p_result->sx = width;
      p_result->sy = height;
      p_result->nb_band = nbands;
   }

   /* copie vers la structure du groupe */
   memcpy(p_result->images_table[0], im_table, width * height * nbands * sizeof(unsigned char));

   /* liberation de l'espace memoire ayant servit a charger l'image */
   free(im_table);

   dprints("sortie de function_load_baccon\n");

}
