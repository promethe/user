/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_save_images_local.c
\brief xxxxxxxxxxxxxxxxx

Author: xxxxxxxxxxx
Created: xxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  same function as f_save_images_to_disk.c, 
 *   but saves in the /tmp/XX_XX_XXXX directory (local)
 *   XX_XX_XXXX is the current day, month and year
 *   and the user can specify the periodicity of
 *   files (how many images passed for each image saved)

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-yuv2rgb()
-save_lena_to_disk()

External Tools: 
-Kernel_Function/find_input_link()
-tools/Vision/save_lena_to_disk()
-tools/Vision/save_png_to_disk()
-tools/Vision/free_prom_image()

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <zlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include <public_tools/Vision.h>

#include <Struct/prom_images_struct.h>

#include "tools/include/yuv2rgb.h"
#include "tools/include/save_lena_to_disk.h"
#include <Kernel_Function/find_input_link.h>

#define DEFAULT_PERIODICITY 1
#define SIZE 256


void function_save_images_local(int gpe_sortie)
{
    char *string = NULL, *ST, *st, char_name[SIZE], directory[SIZE];
    /*unsigned char * im_table; */
    int i, index_image = 0, NALL = 0,
        Z_PNG_COMPRESSION_CHOICE = Z_DEFAULT_COMPRESSION, YUV2RGB = 0;
    int gpe_entree = 0;
    int *number;
    prom_images_struct *p_images_ext;
    prom_images_struct prom_image_tmp;
    int period = DEFAULT_PERIODICITY;
    struct tm *loctime;
    time_t curtime;

#ifdef DEBUG
    printf("----function_save_images_local---------\n");
#endif


    if (def_groupe[gpe_sortie].ext == NULL)
    {
        number = (int *) malloc(sizeof(int));
        *number = 0;
        def_groupe[gpe_sortie].ext = (void *) number;

        /* Get the current time.  */
        curtime = time(NULL);

        /* Convert it to local time representation.  */
        loctime = localtime(&curtime);

        strftime(directory, SIZE, "/tmp/image_%d_%m_%Y", loctime);
        mkdir(directory, S_IRWXU);
#ifdef DEBUG
        printf("directory = %s\n", directory);
#endif

    }
    else
    {
        number = (int *) def_groupe[gpe_sortie].ext;
        (*number)++;
    }
 /*----------------*/
    /*Finding the link */


    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == gpe_sortie)
        {
            string = liaison[i].nom;
            gpe_entree = liaison[i].depart;
            break;
        }
    }


 /*---------------------------------------*/
    /*Getting the extension of previous group */
    p_images_ext = (prom_images_struct *) def_groupe[gpe_entree].ext;
    if (p_images_ext == NULL)
    {
        printf
            ("Problem in function_save_image_to_disk: there is nothing(may be freed)) in extension of group %i\n",
             gpe_entree);
        exit(EXIT_FAILURE);
    }


 /*----------------------------------------*/
    /*Searching the text name of image to save */
    ST = strstr(string, "-T");
    if ((ST != NULL))
    {
        sprintf(char_name, "%s%s", directory, "/");
        i = 0;
        while (ST[2 + i] != '-' && ST[2 + i] != '\0')
        {
            char_name[i + 22] = ST[2 + i];  /* "/tmp/XX_XX_XXXX/" = 22 caracteres */
            i++;
        }

        char_name[i + 22] = '\0';
        sprintf(char_name, "%s%.4d%s", char_name, ((*number) / period),
                ".png");

    }
    else
    {
        printf
            ("Error in function_save_images_local ,you must sepicied -Tname\n");
        exit(EXIT_FAILURE);
    }

 /*--------------------------------------------------------------------*/
    /*Searching the predicity of images to save */
    if (strstr(string, "-per") != NULL || strstr(string, "-PER") != NULL)
    {
        if ((ST != NULL))
            if (atoi(&ST[2]) >= 0)
                period = atoi(&ST[2]);

    }
    if (period != 0)
        if (((*number) % period) != 0)
            return;

#ifdef DEBUG
    printf("period = %d (groupe %d)\n", period, gpe_sortie);
    printf("Name is %s\n", char_name);
#endif


 /*--------------------------------------------------------------------*/
    /*Searching the index number of image in the struct prom_images_struct */
    if (strstr(string, "-Nall") != NULL || strstr(string, "-NALL") != NULL
        || strstr(string, "-nall") != NULL || strstr(string, "-nALL") != NULL)
    {
        NALL = 1;
#ifdef DEBUG
        printf("All images selected to save\n");
#endif
    }
    else
    {
        NALL = 0;
        ST = strstr(string, "-N");
        if ((ST != NULL))
        {
            index_image = atoi(&ST[2]);
        }
        else
        {
            st = strstr(string, "-n");
            if (st != NULL)
            {
                index_image = atoi(&st[2]);
            }
            else
            {
                printf
                    ("Error in function_save_image,you must specified -Nnumber or -nnumber\n");
                exit(EXIT_FAILURE);
            }
        }
#ifdef DEBUG
        printf("Index number of image to save is %i\n", index_image);
#endif
    }

 /*------------------------------------------*/
    /*testing if this number exist in images_ext */
    if (index_image > p_images_ext->image_number)
    {
        printf
            ("Error in function_save_image,the -Nnumber (or -nnumber)is too much high, there is only %i images in images_ext\n",
             p_images_ext->image_number);
        exit(EXIT_FAILURE);
    }


  /*-------------------------------------------------------*/
    /* Searching Compression level(only used with png images) */

    if (strstr(string, "-ZBC") != NULL || strstr(string, "-zbc") != NULL)
    {
        Z_PNG_COMPRESSION_CHOICE = Z_BEST_COMPRESSION;  /* Z_BEST_COMPRESSION = 9 */
#ifdef DEBUG
        printf("Z_BEST_COMPRESSION(9) \n");
#endif

    }
    else if (strstr(string, "-ZDC") != NULL || strstr(string, "-zdc") != NULL)
    {
        Z_PNG_COMPRESSION_CHOICE = Z_DEFAULT_COMPRESSION;   /* Z_DEFAULT_COMPRESSION = 6 */
#ifdef DEBUG
        printf("Z_DEFAULT_COMPRESSION(6)\n");
#endif
    }
    else if (strstr(string, "-ZBS") != NULL || strstr(string, "-zbs") != NULL)
    {
        Z_PNG_COMPRESSION_CHOICE = Z_BEST_SPEED;    /* Z_BEST_SPEED = 1 */
#ifdef DEBUG
        printf("Z_BEST_SPEED(1)\n");
#endif
    }
    else if (strstr(string, "-ZNC") != NULL || strstr(string, "-znc") != NULL)
    {
        Z_PNG_COMPRESSION_CHOICE = Z_NO_COMPRESSION;    /* Z_NO_COMPRESSION = 0 */
#ifdef DEBUG
        printf("Z_NO_COMPRESSION(0)\n");
#endif
    }
    else
    {
        ST = strstr(string, "-Z");
        if ((ST != NULL))
            if (atoi(&ST[2]) >= 0 && atoi(&ST[2]) <= 9)
            {
                Z_PNG_COMPRESSION_CHOICE = atoi(&ST[2]);
#ifdef DEBUG
                printf("Z_COMPRESSION_LEVEL%i \n", Z_PNG_COMPRESSION_CHOICE);
#endif
            }
        st = strstr(string, "-z");
        if (st != NULL)
            if (atoi(&st[2]) >= 0 && atoi(&st[2]) <= 9)
            {
                Z_PNG_COMPRESSION_CHOICE = atoi(&st[2]);
#ifdef DEBUG
                printf("Z_COMPRESSION_LEVEL%i \n", Z_PNG_COMPRESSION_CHOICE);
#endif
            }
    }


 /*----------------------------------------*/
    /*Searching which type of image is to save */
    if (strstr(string, "-LENA") != NULL || strstr(string, "-lena") != NULL)
    {
#ifdef DEBUG
        printf("LENA image file checked\n");
#endif
        if (p_images_ext->nb_band != 1)
        {
            printf
                ("Error in function_save_image,you cannot proceed LENA with %i bands images(only 1)\n",
                 p_images_ext->nb_band);
            exit(EXIT_FAILURE);
        }
        else
        {
            if (NALL == 1)
            {
                save_lena_to_disk(char_name, *p_images_ext);
            }
            else
            {
         /*------------------------------*/
                /*Create an one image prom_image */
                prom_image_tmp.image_number = 1;
                prom_image_tmp.sx = p_images_ext->sx;
                prom_image_tmp.sy = p_images_ext->sy;
                prom_image_tmp.nb_band = p_images_ext->nb_band;
                prom_image_tmp.images_table[0] =
                    p_images_ext->images_table[index_image - 1];

                save_lena_to_disk(char_name, prom_image_tmp);

            }
        }
    }
    else if (strstr(string, "-PNG") != NULL || strstr(string, "-png") != NULL)
    {
#ifdef DEBUG
        printf("PNG image file checked\n");
#endif
        if (p_images_ext->nb_band == 1)
        {
            if (NALL == 1)
                save_png_to_disk(char_name, *p_images_ext,
                                 Z_PNG_COMPRESSION_CHOICE);
            else
            {
          /*------------------------------*/
                /*Create an one image prom_image */

                prom_image_tmp.image_number = 1;
                prom_image_tmp.sx = p_images_ext->sx;
                prom_image_tmp.sy = p_images_ext->sy;
                prom_image_tmp.nb_band = 1;
                prom_image_tmp.images_table[0] =
                    p_images_ext->images_table[index_image - 1];

                save_png_to_disk(char_name, prom_image_tmp,
                                 Z_PNG_COMPRESSION_CHOICE);
            }
        }
        else if (p_images_ext->nb_band == 3)
        {
      /*----------------------------------------*/
            /* Testing if conversion to rgb requested */
            if (strstr(string, "-YUV2RGB") != NULL
                || strstr(string, "-yuv2rgb") != NULL)
            {
                YUV2RGB = 1;
#ifdef DEBUG
                printf("converting YUV2RGB\n");
#endif
            }

            if (NALL == 1)
            {
          /*------------------------------*/
                /*Create an one image prom_image */

                prom_image_tmp.image_number = p_images_ext->image_number;
                prom_image_tmp.sx = p_images_ext->sx;
                prom_image_tmp.sy = p_images_ext->sy;
                prom_image_tmp.nb_band = p_images_ext->nb_band;

          /*------------------------------------------------------*/
                /*Converting all array in rgbrgb if requested by user... */
                if (YUV2RGB)
                {
                    for (i = 0; i < p_images_ext->image_number; i++)
                    {
                        /*converting yuvyuvyuv image capture to rgbrgbrgb */
                        prom_image_tmp.images_table[i] =
                            yuv2rgb(p_images_ext->images_table[i],
                                    p_images_ext->sx, p_images_ext->sy);
                    }
                    save_png_to_disk(char_name, prom_image_tmp,
                                     Z_PNG_COMPRESSION_CHOICE);
          /*---------------------------*/
                    /*Freeing all tempo rgb table */
                    free_prom_image(&prom_image_tmp);
                }
                else
                {
                    for (i = 0; i < p_images_ext->image_number; i++)
                    {
                        /*copying pointeurs */
                        prom_image_tmp.images_table[i] =
                            p_images_ext->images_table[i];
                    }
                    save_png_to_disk(char_name, prom_image_tmp,
                                     Z_PNG_COMPRESSION_CHOICE);
                }
            }
            else
            {
          /*------------------------------*/
                /*Create an one image prom_image */

                prom_image_tmp.image_number = 1;
                prom_image_tmp.sx = p_images_ext->sx;
                prom_image_tmp.sy = p_images_ext->sy;
                prom_image_tmp.nb_band = p_images_ext->nb_band;

          /*------------------------------------------------------*/
                /*Converting all array in rgbrgb if requested by user... */
                if (YUV2RGB)
                {
                    prom_image_tmp.images_table[0] =
                        yuv2rgb(p_images_ext->images_table[index_image - 1],
                                p_images_ext->sx, p_images_ext->sy);
                    save_png_to_disk(char_name, prom_image_tmp,
                                     Z_PNG_COMPRESSION_CHOICE);

                    free_prom_image(&prom_image_tmp);
                }
                else
                {
                    prom_image_tmp.images_table[0] =
                        p_images_ext->images_table[index_image - 1];
                    save_png_to_disk(char_name, prom_image_tmp,
                                     Z_PNG_COMPRESSION_CHOICE);
                }
            }
        }
        else
        {
            printf
                ("Error in function_save_image_to_disk,you cannot proceed with %i band images(only 1 or 3)\n",
                 p_images_ext->nb_band);
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        printf
            ("Error in function_save_image_to_disk,you must sepicied -LENA or -PNG...\n");
        exit(EXIT_FAILURE);
    }



}
