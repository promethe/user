/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
    \ingroup libSensors
    \defgroup f_grabimages f_grabimages


    @author: B Mariat
    - description: garther the softscalling and hardscalling functions in one
    - date: 09/03/2005

    - author: J. Hirel
    - description: Use of new and destroy functions. Gather grabimages with wait and cleanbuffers versions
    - date: 06/05/2009
*/

/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <Global_Var/Vision.h>

#ifdef TIME_TRACE
#include <Global_Var/SigProc.h>
#endif
#include <libhardware.h>

#include <Components/vision.h> /** dynamic hardware */

#include <dev.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>


typedef struct data_grabimages
{
  Vision *vision; /** To be compatible with the new dynamic hardware */

  Camera *cam;
  int color_wanted;
  float scale_factor;
  int nb_image;
  int img_width;
  int img_height;
  int hardscale;
  int interlace;
  int time_to_wait;
  prom_images_struct *temp_frame;
  int gpe_vigilance;

} data_grabimages;


void new_grabimages(int gpe)
{
  int l;
  int link_in = -1;
  char camera_name[256] = "\0";
  char param_link[256];
  prom_images_struct *pt;
  int nb_image = 1;
  int interlace = 0;
  int color_wanted = -1;
  int hardscale = 0;          /*0: mode softscaling, 1: mode hardscaling */
  float scale_factor = 1.;
  int time_to_wait = 0;
  data_grabimages *my_data;
  Camera *cam=NULL;
  int img_width, img_height;
  int gpe_vigilance = -1;

  char format[SIZE_OF_FORMAT];
  Vision *vision = NULL;
  Device *dev=NULL;

  memset(format, 0, SIZE_OF_FORMAT);

  if (def_groupe[gpe].data == NULL)
  {
    l = 0;
    link_in = find_input_link(gpe, l);
    while (link_in != -1)
    {
      if (strcmp(liaison[link_in].nom, "vigilance") == 0)
      {
	gpe_vigilance = liaison[link_in].depart;
      }

      if (prom_getopt(liaison[link_in].nom, "-C", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-c", param_link) == 2)
      {
	color_wanted = atoi(param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-S", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-s", param_link) == 2)
      {
	scale_factor = atof(param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-n", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-N", param_link) == 2)
      {
	nb_image = atoi(param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-m", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-M", param_link) == 2)
      {
	hardscale = atoi(param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-i", param_link) == 1 || prom_getopt(liaison[link_in].nom, "-I", param_link) == 1)
      {
	interlace = 1;
      }

      if (prom_getopt(liaison[link_in].nom, "-t", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-T", param_link) == 2)
      {
	time_to_wait = atoi(param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-h", param_link) == 2 || prom_getopt(liaison[link_in].nom, "-H", param_link) == 2)
      {
	strcpy(camera_name, param_link);
      }

      if (prom_getopt(liaison[link_in].nom, "-format=", param_link) == 2)
      {
	if (strcmp(param_link, "grey") == 0)
	{
	  if (color_wanted == -1) color_wanted = 0;
	  else if (color_wanted == 1) EXIT_ON_ERROR("You cannot have the option -c1 (color) and a format grey");
	  strcpy(format, param_link);
	}
	else if (strcmp(param_link, "RGB") == 0)
	{
	  if (color_wanted == -1) color_wanted = 1;
	  else if (color_wanted == 0) EXIT_ON_ERROR("Group %s, you cannot have the option -c0 (grey) and the format %s", def_groupe[gpe].no_name, param_link);
	  strcpy(format, param_link);
	}
	else if (strcmp(param_link, "YCbCr") == 0)
	{
	  if (color_wanted == -1) color_wanted = 1;
	  else if (color_wanted == 0) EXIT_ON_ERROR("Group %s, you cannot have the option -c0 (grey) and the format %s", def_groupe[gpe].no_name, param_link);
	  strcpy(format, param_link);
	}
	else EXIT_ON_ERROR("The format (%s) is unknown. The possible formats are: 'grey', 'RGB', 'YCbCr'.", param_link);
      }

      l++;
      link_in = find_input_link(gpe, l);
    }

    if (strlen(camera_name) == 0)
    {
      /** On regarde s'il y a un systeme visuel definie par les nouvelle bibliotheques dynamiques. */
      dev=dev_try_get_device("vision", NULL);
      if(dev!=NULL)
	vision = (Vision*) dev->components[0];
      if (dev==NULL || vision==NULL)
      {
	cam = camera_get_camera_by_number(0);
      }
      else
      {        		
	if (format[0]==0) EXIT_ON_ERROR("The link of grabimages in group %s with the device 'vision' needs a -format=[RGB/grey/YCbCr].", def_groupe[gpe].no_name);
	vision->set_format(vision, format);
      }
    }
    else
    {
      /** On regarde s'il y a un systeme visuel definie par les nouvelle bibliotheques dynamiques. */
      dev=dev_try_get_device("vision", camera_name);
      if(dev!=NULL)
	vision = (Vision*) dev->components[0];
      if (dev==NULL || vision==NULL)
      {
	cam = camera_get_cam_by_name(camera_name);
      }
      else
      {
	if (format[0]==0) EXIT_ON_ERROR("The link of grabimages in group %s with the device 'vision' needs a -format=[RGB/grey/YCbCr].", def_groupe[gpe].no_name);
	vision->set_format(vision, format);
      }
    }

    /** If no color preference is given, we take the hardware preference */
    if (vision == NULL)
    {
      if (color_wanted == -1)
      {
	color_wanted = cam->color;
      }
    }
    else
    {
      if (vision->number_of_channels == 3) color_wanted = 1;
      else if (vision->number_of_channels == 1) color_wanted = 0;
      else EXIT_ON_ERROR("The number of channels (%d) of vision is neither 1 (greyscale) or 3 (color)", vision->number_of_channels);
    }

    dprints("new_grabimages(%s): color_wanted = %d , scale_factor = %f, camera is named : %s, type de scaling: %d, format: %d\n", def_groupe[gpe].no_name, color_wanted, scale_factor, cam->name, hardscale, format);

    /*creation d'un objet camera */
    my_data = ALLOCATION(data_grabimages);
    my_data->cam = cam;
    my_data->scale_factor = scale_factor;
    my_data->color_wanted = color_wanted;
    my_data->nb_image = nb_image;
    my_data->hardscale = hardscale;
    my_data->interlace = interlace;
    my_data->time_to_wait = time_to_wait;
    my_data->gpe_vigilance = gpe_vigilance;

    my_data->vision = vision;

    if (cam!=NULL)
    {
      img_width = cam->width_max / scale_factor;
      img_height = cam->height_max / scale_factor;
    }
    else /** dynamic library vision */
    {
      img_width = vision->width / scale_factor;
      img_height = vision->height / scale_factor;
    }

    if (interlace == 1)
    {
      if (color_wanted == 1)
      {
	my_data->temp_frame = calloc_prom_image(1, img_width, img_height, 3);
      }
      else
      {
	my_data->temp_frame = calloc_prom_image(1, img_width, img_height, 1);
      }
      img_height = img_height/2;
    }
    else
    {
      my_data->temp_frame = NULL;
    }

    if (color_wanted == 1)
    {
      pt = calloc_prom_image(nb_image, img_width, img_height, 3);
    }
    else
    {
      pt = calloc_prom_image(nb_image, img_width, img_height, 1);
    }

    my_data->img_height = img_height;
    my_data->img_width = img_width;

    def_groupe[gpe].ext = (prom_images_struct *) pt;
    def_groupe[gpe].data = (data_grabimages *) my_data;
  }
}


void function_grabimages(int gpe)
{
  int i, j , k;
  prom_images_struct *pt;
  int nb_image = 1;
  int interlace = 0;
  int color_wanted = -1;
  int hardscale = 0;          /*0: mode softscaling, 1: mode hardscaling */
  float scale_factor = 1.;
  data_grabimages *my_data;
  Camera *cam;
  int img_width, img_height;
  int time_to_wait;
  int gpe_vigilance, local_vigilance;
/*     struct timespec duree_nanosleep, res; */

  Vision *vision = NULL;

#ifdef TIME_TRACE
  struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
  char MessageFunctionTimeTrace[256];
#endif

  my_data = (data_grabimages *) def_groupe[gpe].data;
  if (my_data == NULL) EXIT_ON_ERROR("Group (%s): Cannot retrieve data\n", def_groupe[gpe].no_name);

  pt = (prom_images_struct *) def_groupe[gpe].ext;

  if (pt == NULL) EXIT_ON_ERROR("Group (%s): Cannot retrieve ext\n", def_groupe[gpe].no_name);

  cam = my_data->cam;
  scale_factor = my_data->scale_factor;
  color_wanted = my_data->color_wanted;
  nb_image = my_data->nb_image;
  hardscale = my_data->hardscale;
  img_height = my_data->img_height;
  img_width = my_data->img_width;
  interlace = my_data->interlace;
  time_to_wait = my_data->time_to_wait;
  gpe_vigilance = my_data->gpe_vigilance;

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  if (cam != NULL)
  {
    if (gpe_vigilance >= 0)
    {
      local_vigilance = neurone[def_groupe[gpe_vigilance].premier_ele].s1;
    }
    else
    {
      local_vigilance = vigilence;
    }

    if (local_vigilance > 0.5)
    {
      /*
	duree_nanosleep.tv_sec = 2;
	duree_nanosleep.tv_nsec = 0;
	nanosleep(&duree_nanosleep, &res);
      */
    }

    for (i = 0; i < nb_image; i++)
    {
      if (time_to_wait > 0)
      {
	sleep(time_to_wait);
      }

      if (interlace == 1)
      {
	camera_grabimage(cam, my_data->temp_frame->images_table[0], color_wanted, scale_factor, hardscale);

	for (j = 0; j < img_height; j++)
	  for (k = 0; k < img_width; k++)
	    pt->images_table[i][j * img_width + k] = my_data->temp_frame->images_table[0][j * img_width * 2 + k];
      }
      else
      {
	camera_grabimage(cam, pt->images_table[i], color_wanted, scale_factor, hardscale);
      }
    }
  } /** We use the new dynamic library */
  else
  {
    vision = my_data->vision;
    vision->update_image(vision, pt->images_table[0]);
  }



#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
    MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace, "Time in f_grabimages(%d)\t%4ld.%06d at\t%4ld.%06d\n", gpe,
	  SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace,InputFunctionTimeTrace.tv_sec,InputFunctionTimeTrace.tv_usec);
  printf("%s", MessageFunctionTimeTrace);
#endif
}


void destroy_grabimages(int gpe)
{
  data_grabimages *my_data = (data_grabimages *) def_groupe[gpe].data;

  dprints("destroy_grabimages(%s): Entering function\n", def_groupe[gpe].no_name);
  if (my_data != NULL)
  {
    if (my_data->temp_frame != NULL)
    {
      free_prom_image(my_data->temp_frame);
      my_data->temp_frame = NULL;
    }

    if (def_groupe[gpe].ext != NULL)
    {
      free_prom_image(def_groupe[gpe].ext);
      def_groupe[gpe].ext = NULL;
    }

        free(my_data);
        def_groupe[gpe].data = NULL;
    }
    dprints("destroy_reach_destination(%s): Leaving function\n", def_groupe[gpe].no_name);
}
