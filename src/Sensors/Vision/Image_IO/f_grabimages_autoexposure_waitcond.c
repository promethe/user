/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

- author: B Mariat
- description: garther the softscalling and hardscalling functions in one
- date: 09/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 capturing the image(s)	
 * generic function		
 * calling miro or XIL		
 * according to the hardware
 
Macro:
-none

Local variables:
-none

Global variables:
-prom_images_struct images_capture

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Pan_Tilt/CaptureImage()
-tools/IO_Robot/Pan_Tilt/Refrsh()
-tools/Vision/calloc_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <Global_Var/Vision.h>

#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_grabimages_autoexposure_waitcond
{
    Camera *cam;
    int color_wanted;
    float scale_factor;
    int img_width;
    int img_height;
    int hardscale;
    int gpe_vigilance;
    unsigned int expo;
    unsigned int gain;
    int regul;
    int gpe_wait_cond;
} MyData_f_grabimages_autoexposure_waitcond;


void function_grabimages_autoexposure_waitcond(int gpe_sortie)
{
    int i, stable = 0;
    float grad;
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    /*static Camera *cam=NULL; */
    prom_images_struct *pt;
    int lien_entrant = -1;
    char param_link[256], name_cam[256];
    int color_wanted = 0;
    int hardscale = 0;          /*0: mode softscaling, 1: mode hardscaling */
    float scale_factor = 1.;
    MyData_f_grabimages_autoexposure_waitcond *my_data;
    Camera *cam = NULL;
    int img_width, img_height;
    int gpe_vigilance = -1;
    float vigilance_local;
    int stable_mvt, stable_gain, gain_i, expo_i;
    int expo = -1;
    int gain = -1;
    int regul = 25;
    unsigned int *tmp;
    int temps,wait_duration;

    int gpe_wait_cond=-1;
    /*Refresh(); */
    /*NC Simulation de param user passer normalement sur le lien */

    int l = 0;

    printf("begin grabimages_auto\n");

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        lien_entrant = find_input_link(gpe_sortie, l);
        while (lien_entrant != -1)
        {
            if (strcmp(liaison[lien_entrant].nom, "vigilence") == 0 || strcmp(liaison[lien_entrant].nom, "vigilance") == 0)
            {
                gpe_vigilance = liaison[lien_entrant].depart;
            }
            if (prom_getopt(liaison[lien_entrant].nom, "-C", param_link) == 2)
            {
                color_wanted = atoi(param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-c", param_link)
                     == 2)
            {
                color_wanted = atoi(param_link);
            }

            if (prom_getopt(liaison[lien_entrant].nom, "-S", param_link) == 2)
            {
                scale_factor = atof(param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-s", param_link)
                     == 2)
            {
                scale_factor = atof(param_link);
            }

            if (prom_getopt(liaison[lien_entrant].nom, "-m", param_link) == 2)
            {
                hardscale = atoi(param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-M", param_link)
                     == 2)
            {
                hardscale = atoi(param_link);
            }

            if (prom_getopt(liaison[lien_entrant].nom, "-h", param_link) == 2)
            {
                strcpy(name_cam, param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-H", param_link)
                     == 2)
            {
                strcpy(name_cam, param_link);
            }
            if (prom_getopt(liaison[lien_entrant].nom, "-E", param_link) == 2)
            {
                expo = atoi(param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-e", param_link)
                     == 2)
            {
                expo = atoi(param_link);
            }
            if (prom_getopt(liaison[lien_entrant].nom, "-G", param_link) == 2)
            {
                gain = atoi(param_link);
            }
            else if (prom_getopt(liaison[lien_entrant].nom, "-g", param_link)
                     == 2)
            {
                gain = atoi(param_link);
            }
	    else if(strcmp(liaison[lien_entrant].nom,"wait_cond")==0)
	    {
	      gpe_wait_cond=liaison[lien_entrant].depart;
	    }


            cam = camera_get_cam_by_name(name_cam
                                         /*,cam_table,nbr_video_device */ );
            if (cam == NULL)
            {
                printf("erreur dans f_acquisiton\n");
                exit(0);
            }
            l++;
            lien_entrant = find_input_link(gpe_sortie, l);
        }

        printf
            ("param de f_grabimages(%d): color_wanted = %d\n , scale_factor = %f\n, camera is named : %s\ntype de scaling: %d\n",
             gpe_sortie, color_wanted, scale_factor, name_cam, hardscale);
        /*creation d'un objet camera */

        my_data = malloc(sizeof(MyData_f_grabimages_autoexposure_waitcond));
        my_data->cam = cam;
        my_data->scale_factor = scale_factor;
        my_data->color_wanted = color_wanted;
        my_data->hardscale = hardscale;
        my_data->gpe_vigilance = gpe_vigilance;
        my_data->expo = expo;
        my_data->gain = gain;
        my_data->regul = regul;
	my_data->gpe_wait_cond = gpe_wait_cond;
        /*my_data->camera=; */

        /*camera_set_scale_factor(cam,scale_factor); */
        /*Cette fonction reajuste la taille de l'image pour que la prochaine capture soit bonne
           Attention dans un mode thread, ca ne devrait pas marcher */


        if (hardscale != 1)
        {
            /* cas du soft scalling */

            img_width = cam->width_max / scale_factor;
            img_height = cam->height_max / scale_factor;

            if (color_wanted == 1)
            {
                pt = calloc_prom_image(2, img_width, img_height, 3);
            }
            else
            {
                pt = calloc_prom_image(2, img_width, img_height, 1);
            }
            my_data->img_width = img_width;
            my_data->img_height = img_height;
        }
        else
        {
            /* cas du hard scalling */

            if (color_wanted == 1)
                pt = calloc_prom_image(2, cam->width_max / (int) scale_factor,
                                       cam->height_max / (int) scale_factor,
                                       3);
            else
                pt = calloc_prom_image(2, cam->width_max / (int) scale_factor,
                                       cam->height_max / (int) scale_factor,
                                       1);

        }


        def_groupe[gpe_sortie].ext = (prom_images_struct *) pt;
        def_groupe[gpe_sortie].data =
            (MyData_f_grabimages_autoexposure_waitcond *) my_data;

        if (expo != -1)
        {
            camera_set_exposure_value(cam, expo);
        }
        else
	{
            camera_set_exposure_auto(cam);
	}

        if (gain != -1)
        {
            camera_set_gain_value(cam, gain);
        }
        else
        {
            camera_set_gain_auto(cam);
        }

    }
    else
    {
        pt = (prom_images_struct *) def_groupe[gpe_sortie].ext;
        my_data =
            (MyData_f_grabimages_autoexposure_waitcond *) def_groupe[gpe_sortie].data;
        cam = my_data->cam;
        scale_factor = my_data->scale_factor;
        color_wanted = my_data->color_wanted;
        hardscale = my_data->hardscale;
        img_height = my_data->img_height;
        img_width = my_data->img_width;
        gpe_vigilance = my_data->gpe_vigilance;
        expo = my_data->expo;
        gain = my_data->gain;
        regul = my_data->regul;
	gpe_wait_cond = my_data->gpe_wait_cond;
    }

      wait_duration=400;
      vigilance_local=0.;

      if(gpe_vigilance != -1)
      {
	vigilance_local=neurone[def_groupe[gpe_vigilance].premier_ele].s1;
      }

      if(vigilance_local>0.5)
	wait_duration=1500;
      else
	wait_duration=400;

      printf("vigilance = %s\n", def_groupe[gpe_vigilance].no_name);
      printf("vigilance grabimages = %f\n",vigilance_local);
      
      if(neurone[def_groupe[gpe_wait_cond].premier_ele].s1>0.5 && gpe_wait_cond!=-1 && vigilance_local < 0.5)
      {
	printf("=========== nowaiiitttttt\n");
	printf("end grabimages_auto\n");
	return;
      } 

    camera_grabimage(cam, pt->images_table[0], color_wanted, scale_factor,
                     hardscale);

    /*   previous_sum=1000; */
    stable = 0;
    stable_mvt = 0;
    stable_gain = 0;
    gain_i = gain;
    expo_i = expo;
	
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
    while (stable != 1)
    {
       /*camera_set_exposure(cam, expo);
	 camera_set_gain(cam, gain);*/
        tmp = (unsigned int*) (pt->images_table[1]);
        pt->images_table[1] =  (unsigned char*)(pt->images_table[0]);
        pt->images_table[0] = (unsigned char*) tmp;
        camera_grabimage(cam, pt->images_table[0], color_wanted, scale_factor,
                         hardscale);

/*Analyse gain*/
        grad = 0.;
        for (i = 0; i < 99; i++)
        {
            grad =
                grad +
                pt->images_table[0][(int) (i * (pt->sx * pt->sy) / 101.)];
        }
        grad = grad / (99.);

 #ifdef DEBUG
         printf("expo-grad :%d   --  %f\n", expo, grad);
         printf("gain-grad :  %d   --  %f\n", gain, grad);
#endif


        if (grad < 120)
        {
            if (gain_i < 255)
                gain_i = gain_i + 10;
            else
                expo_i = expo_i + 10;
        }
        else if (grad > 134)
        {
            if (expo_i == 0)
                gain_i = gain_i - 10;
            else
                expo_i = expo_i - 10;
        }
        else
        {
            stable_gain = 1;
        }


        if (gain_i < 0)
        {
            gain = 0;
            gain_i = 0;
        }
        else if (gain_i > 255)
        {
            gain = 255;
            gain_i = 255;
        }
        else
            gain = (unsigned int) gain_i;

        if (expo_i < 0)
        {
            expo = 0;
            expo_i = 0;
        }
        else if (expo_i > 255)
        {
            expo_i = 255;
            expo = 255;
        }
        else
            expo = (unsigned int) expo_i;

	gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

	temps=(OutputFunctionTimeTrace.tv_sec*1000+OutputFunctionTimeTrace.tv_usec/1000) -
	(InputFunctionTimeTrace.tv_sec*1000+InputFunctionTimeTrace.tv_usec/1000);
/*	printf("temps = %d \n\n",temps);*/

        if ( temps>wait_duration)
        {

	      stable = 1;
        }
    }

    my_data->expo = expo;
    my_data->gain = gain;

    printf("end grabimages_auto\n");
}
