/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_load_image_png f_load_image_png
\brief 
\file  
/section Author
Name: JC Baccon
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Chargement d'une image .png et sauvegarde dans .ext du groupe
       sous la forme d'une PROM_IMAGES_STRUCT dont les arguments sont:

         image_number : nb d'image (une) ;
         sx, sy : largeur et hauteur de l'image ;
	 nb_band : nb de bandes (1=niveau de gris, 3=couleurs) ;
	 images_table : tableau 1D contenant la valeur des pixels de
	                l'image.

\section Macro
-none 

\section Local variables
-char jice_nom_image[256]

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-tools/Vision/load_image_from_disk()
-tools/Vision/calloc_prom_image()
-tools/Vision/free_prom_image()

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>

void function_load_image_png(int gpe)
{
    int width, height, nbands;
    unsigned char *im_table = NULL;
    static char im_name[20];
    prom_images_struct *p_result = NULL;
	int test_scanf;
	
    width = 0;
    height = 0;
    nbands = 0;


    cprints("\nPlease print the fullname of a PNG image to load : ");
    fflush(stdout);
    test_scanf=scanf("%s", im_name);
    fflush(stdin);

	if(test_scanf == 1) {
	    /*printf("----TEST LOADING PNG IMAGE-----\n") ; */
	    dprints("Load image %s\n", im_name);
	
	    /* Appel de load_image_from_disk */
	    load_image_from_disk("png", im_name, &im_table, &width, &height, &nbands);
	    cprints("width %d, height %d, nbands %d \n", width, height, nbands);
	}

    /* Allocation prom_images_strct */
    if (def_groupe[gpe].ext == NULL)
    {
        dprints("premiere alloc ext \n");
        p_result = calloc_prom_image(1, width, height, nbands);
        def_groupe[gpe].ext = p_result;
    }
    else
    {
        dprints("le .ext n'est pas nul\n");
        p_result = (prom_images_struct *) def_groupe[gpe].ext;
        dprints("width %d, height %d, nbands %d \n", p_result->sx,p_result->sy, p_result->nb_band);

        if ((int) p_result->sx == width && (int) p_result->sy == height
            && (int) p_result->nb_band == nbands)
        {
            dprints("ext et im_table de dimensions identiques.\n");
            def_groupe[gpe].ext = p_result;
        }
        else
        {
            dprints("ext et im_table de dimensions differentes.\n");
            dprints("modif alloc ext.\n");
            free_prom_image(p_result);
            p_result = calloc_prom_image(1, width, height, nbands);
            def_groupe[gpe].ext = p_result;
        }
    }
    /* Memcopy of table */
    memcpy(p_result->images_table[0], im_table, width * height * nbands * sizeof(unsigned char));

    /* Free(image_table) */
    free(im_table);
    im_table = NULL;
}
