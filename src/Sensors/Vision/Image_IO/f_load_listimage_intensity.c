/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \file
   \brief

   Author: B.Fouque
   Created: XX/XX/XXXX

   Modified:
   - author: B.Fouque
   - description: Support de 3 formats differents pour les entrees lues dans le fichier listimage. Gestion des fichiers avec extension .png
   - date: 01/12/2008

   Description:
   - image list file reader

   Theoritical description:
   - \f$  LaTeX equation: none \f$

   Macro:
   -none

   Local variables:
   -none

   Global variables:
   char jice_nom_image[256]
   char nom_categorie[STR_NAME_SIZE];
   int  mode_avec_categorie;
   int  mode_categorie_intensite;

   Internal Tools:
   -int count_lines_from_file (char* file)

   External Tools:
   -tools/Vision/Vision/load_image_from_disk()

   Links:
   - type: algo / biological / neural
   - description: none/ XXX
   - input expected group: none/xxx
   - where are the data?: none/xxx

   Comments:

   Known bugs: none (yet!)

   Todo:see author for testing and commenting the function

   http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <Vision/Image_IO.h>

/* #include "../../../include/ben_defines.h" */

#define STR_PATH_SIZE           500 /*!< Taille maximale d'un chemin d'image */
#define STR_NAME_SIZE           100 /*!< Taille maximale d'un nom d'expression faciale (une categorie) */

char jice_nom_image[1024];       	/* pour utilisation par f_demande_numero */
char nom_categorie[STR_NAME_SIZE]; 	/* indique la categorie de l'image courante */
int  mode_avec_categorie; 	        /* booleean indiquant si le fichier liste a le formalisme: image categorie */
int  mode_categorie_intensite;          /* formalisme avec nom d'image contenant un nom de categorie et un nom d'intensite */

char default_extension[] = "png";
/*int	 script_ended = 0;*/
int	 nb_images = 0;

FILE 			*jice_fichier_liste = NULL;
char 			 listeim[256] = "listimage";
char 			 firstline[STR_PATH_SIZE] = "rien";
int              listimage_ended;

/*!
 * \brief Compte le nombre de lignes d'un fichier
 *
 * Permet de compter le nb d'images d'un fichier listimage (utile pour d'autres boites promethe)
 */
int count_lines_from_file (char* file)
{
   FILE* 	fp = fopen (file, "r");
   int		n = 0;
   char	ligne[STR_PATH_SIZE] = "";

   if (fp == NULL)
   {
      printf("count_lines_from_file(): erreur ouverture\n");
      exit (1);        /* sort du programme (code erreur 1) */
   }

   while (fgets(ligne, STR_PATH_SIZE, fp) != NULL)
      n++;

   fclose(fp);  /* ferme le fichier */

   return n;
}

void new_load_listimage_intensity (int Gpe)
{
   char *buff=NULL;
   printf ("new_ben_load_baccon ()\n");
   /*printf("def_groupe.ext n'existe pas\n"); */
   def_groupe[Gpe].ext =
      (prom_images_struct *) malloc(sizeof(prom_images_struct));
   if (def_groupe[Gpe].ext == NULL)
   {
      printf("erreur sur malloc de p_result\n");
      exit(0);
   }

   /* ouverture du fichier liste d'image */
#ifdef DEBUG
   printf("ouverture de la liste d'image '%s'\n", listeim);
#endif

   jice_fichier_liste = fopen(listeim, "r");
   if (jice_fichier_liste == NULL)
   {
      printf("impossible d'ouvrir '%s'\n", listeim);
      exit(0);
   }

   ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = NULL;
   ((prom_images_struct *) def_groupe[Gpe].ext)->sx = -1;
   ((prom_images_struct *) def_groupe[Gpe].ext)->sy = -1;
   ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 0;

   /* rajout du 23/07/2003 Olivier Ledoux */
   ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

   buff=fgets (firstline, STR_PATH_SIZE, jice_fichier_liste);

   if (buff==NULL)
      EXIT_ON_ERROR("Extraction failed\n");
   if (strstr (firstline, " ")) /* Detection du mode */
      mode_avec_categorie = 1;
   else
      mode_avec_categorie = 0;

   if (strstr (firstline, "level"))
      mode_categorie_intensite = 1;
   else
      mode_categorie_intensite = 0;

   strcpy (nom_categorie, "aucune");
   rewind (jice_fichier_liste);
   nb_images = count_lines_from_file (listeim);
   listimage_ended = 0;
}

/*!
 * \brief Fonction de chargement d'un fichier listimage
 *
 * Prise en charge de l'extension .png
 * -> on peut lire 2 types de chemins d'images: imagepath.png OU imagepath
 *
 * Plusieurs formats de listimages lisibles avec f_ben_load_baccon. Ex:
 * 1) imagepath categorie
 * 2) imagepath_colere
 * 3) imagepath_colere_level2
 *
 * Conditions de detection des 3 modes:
 * 1 si presence d'un espace sur la premiere ligne
 * 2 si non 1 et pas de presence du mot "level" dans la premiere ligne
 * 3 si presence du mot "level"
 */
void function_load_listimage_intensity (int Gpe)
{
   int 			 width = 0;
   int 			 height = 0;
   int 			 nbands = 0;
   unsigned char		*im_table = NULL;

   char 			 reponse_user[256];
   prom_images_struct    *p_result=NULL;

   char                  *extension = NULL;
   int ret;

   dprints("\nfunction_load_baccon\n");

   p_result = (prom_images_struct *) def_groupe[Gpe].ext;

   if (feof(jice_fichier_liste))
   {
      listimage_ended = 1;

      cprints ("fin du fichier d'image '%s': voulez vous rejouer la sequence [o/n] : ", listeim);

      ret=scanf("%s", reponse_user);
      if (reponse_user[0] != 'n')
      {
         fclose(jice_fichier_liste);
         jice_fichier_liste = fopen(listeim, "r");
      }
      else
      {
         fclose(jice_fichier_liste);
         EXIT_ON_ERROR("Promethe va s'arreter\n");
      }
   }

   if (mode_avec_categorie)  ret=fscanf(jice_fichier_liste, "%s %s\n", jice_nom_image, nom_categorie);
   else                      ret=fscanf(jice_fichier_liste, "%s\n", jice_nom_image);

   dprints("ouverture de '%s'\n", jice_nom_image);

   if (!strcmp (jice_nom_image, ""))
   {
      EXIT_ON_ERROR ("Pas d'image chargee!\n");
   }

   if (jice_fichier_liste == NULL)
   {
      EXIT_ON_ERROR ("pas de fichier charge!\n");
   }

   /* Amelioration avec gestion des extensions de fichiers */
   if (jice_nom_image[strlen (jice_nom_image) - 4] == '.') extension = &jice_nom_image[strlen (jice_nom_image) - 3];
   else extension = default_extension;

   if (strcmp (extension, "png"))
   {
      EXIT_ON_ERROR ("f_ben_load_baccon: mauvais format de fichier: %s\n", extension);
   }

   load_image_from_disk(extension, jice_nom_image, &im_table, &width, &height, &nbands);

   if (im_table == NULL || width == 0 || height == 0 || nbands == 0)
   {
      cprints ("f_ben_load_baccon: load_image_from_disk(): chargement d'image echoue \n");
      cprints ("f_ben_load_baccon: extension:%s nom_image:%s\n", extension, jice_nom_image);
      EXIT_ON_ERROR ("f_ben_load_baccon: width:%d height:%d nbands:%d\n", width, height, nbands);
   }

   if (  (p_result->sx != width)||(p_result->sy != height)||(p_result->nb_band != nbands)   )/*1ere fois et chgt de taille*/
   {
      if ((p_result->images_table[0] != NULL))/*s il avait deja ete alloue*/
      {
         dprints("warning : change adresse du pointeur de sortie (taille d'image variable ?) !\n");

         free(p_result->images_table[0]);
         p_result->images_table[0] = NULL;
      }
      p_result->images_table[0] =(unsigned char *) malloc(sizeof(unsigned char) * width * height *   nbands);
      if ( p_result->images_table[0]==NULL )
      {
         EXIT_ON_ERROR("Erreur allocation memoire dans f_load_baccon gpe %d \n",Gpe);
      }
      p_result->sx = width;
      p_result->sy = height;
      p_result->nb_band = nbands;
   }

   /* copie vers la structure du groupe */
   memcpy(p_result->images_table[0], im_table,   width * height * nbands * sizeof(unsigned char));

   /* liberation de l'espace memoire ayant servit a charger l'image */
   free(im_table);

   dprints("sortie de function_load_baccon\n");

}
