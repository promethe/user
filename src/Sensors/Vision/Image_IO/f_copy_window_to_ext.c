/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
\file  f_copy_window_to_ext.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
-none

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-tools/Vision/Vision/save_png_to_disk()
-tools/Vision/Vision/free_prom_image()


Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#ifndef AVEUGLE
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#endif
#include <libx.h>
#include <zlib.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*#define DEBUG*/

#include <public_tools/Vision.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>

#ifndef AVEUGLE
typedef struct data_copy_window_to_ext
{
	TxDonneesFenetre *window;
	int x;
	int y;
	int width;
	int height;
} data_copy_window_to_ext;
#endif

void new_copy_window_to_ext(int gpe)
{
#ifndef AVEUGLE
	int index, link;
	int x = 0, y = 0, width = -1, height = -1;
	int image_width, image_height;
	char string[255];
	data_copy_window_to_ext *my_data = NULL;
	TxDonneesFenetre *window = &image1;

	if (def_groupe[gpe].data == NULL)
	{
		index = 0;
		link = find_input_link(gpe, index);

		while (link != -1)
		{
			if (strstr(liaison[link].nom, "-I1") != NULL || strstr(liaison[link].nom, "-i1") != NULL)
			{
				dprints("new_copy_window_to_ext(%s): Selecting Image 1\n", def_groupe[gpe].no_name);
				window = &image1;
			}
			else if (strstr(liaison[link].nom, "-I2") != NULL || strstr(liaison[link].nom, "-i2") != NULL)
			{
				dprints("new_copy_window_to_ext(%s): Selecting Image 2\n", def_groupe[gpe].no_name);
				window = &image2;
			}
			else if (strstr(liaison[link].nom, "-F1") != NULL || strstr(liaison[link].nom, "-f1") != NULL)
			{
				dprints("new_copy_window_to_ext(%s): Selecting Window 1\n", def_groupe[gpe].no_name);
				window = &fenetre1;
			}
			else if (strstr(liaison[link].nom, "-F2") != NULL || strstr(liaison[link].nom, "-f2") != NULL)
			{
				dprints("new_copy_window_to_ext(%s): Selecting Window 1\n", def_groupe[gpe].no_name);
				window = &fenetre2;
			}

			if (prom_getopt(liaison[link].nom, "x", string) == 2 || prom_getopt(liaison[link].nom, "X", string) == 2 )
			{
				x = atoi(string);
			}

			if (prom_getopt(liaison[link].nom, "y", string) == 2 || prom_getopt(liaison[link].nom, "Y", string) == 2)
			{
				y = atoi(string);
			}

			if (prom_getopt(liaison[link].nom, "w", string) == 2 || prom_getopt(liaison[link].nom, "W", string) == 2)
			{
				width = atoi(string);
			}

			if (prom_getopt(liaison[link].nom, "h", string) == 2 || prom_getopt(liaison[link].nom, "H", string) == 2)
			{
				height = atoi(string);
			}

			index++;
			link = find_input_link(gpe, index);
		}

		if (def_groupe[gpe].ext == NULL)
		{
			if (width > 0)
			{
				image_width = width;
			}
			else
			{
				image_width = window->width;
				x = 0;
			}

			if (height > 0)
			{
				image_height = height;
			}
			else
			{
				image_height = window->height;
				y = 0;
			}

			def_groupe[gpe].ext = calloc_prom_image(1, image_width, image_height, 3);
			dprints("new_copy_window_to_ext(%s): Allocating memory for a %ix%i image\n", def_groupe[gpe].no_name, window->width, window->height);
		}

		my_data = ALLOCATION(data_copy_window_to_ext);

		my_data->window = window;
		my_data->height = height;
		my_data->width = width;
		my_data->x = x;
		my_data->y = y;
		def_groupe[gpe].data = my_data;
	}
#else
	(void)gpe;
#endif
}


void function_copy_window_to_ext(int gpe)
{
#ifndef AVEUGLE
	prom_images_struct *img = (prom_images_struct *) def_groupe[gpe].ext;
	data_copy_window_to_ext *my_data = (data_copy_window_to_ext *) def_groupe[gpe].data;

	if (my_data == NULL)
	{
		EXIT_ON_ERROR("Cannot retrieve data");
	}

	dprints("f_copy_window_to_ext(%s): Copying image\n", def_groupe[gpe].no_name);
	TxCopyWindowImage(my_data->window, img->images_table[0], my_data->x, my_data->y, my_data->width, my_data->height);
#else
	(void)gpe;
#endif
}



void destroy_copy_window_to_ext(int gpe)
{
#ifndef AVEUGLE
	dprints("destroy_copy_window_to_ext(%s): Entering function\n", def_groupe[gpe].no_name);
	if (def_groupe[gpe].data != NULL)
	{
		free(((data_copy_window_to_ext *) def_groupe[gpe].data));
		def_groupe[gpe].data = NULL;
	}

	if (def_groupe[gpe].ext != NULL)
	{
		free_prom_image(def_groupe[gpe].ext);
		def_groupe[gpe].ext = NULL;
	}
	dprints("destroy_copy_window_to_ext(%s): Leaving function\n", def_groupe[gpe].no_name);
#else
	(void)gpe;
#endif
}
