/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_save_image_to_disk f_save_image_to_disk
   \ingroup libSensors

   \brief save image (different format availables, eg png, ppm, lena) to disk

   \details

   NEED TO BE UPDATED

   \section Description
   This function reads info on the link and save lena or png file to disk
   -  the images are taken from ext of previous group struct prom_image_struct
   - on the link input you must choose:
   - -Tname_of_image
   - -Nindex_number the index number into the ext of previous group
   - -LENA or -PNG to commute image format type to save
   - -Znumber or -ZBC -ZDC -ZBS -ZNC where number is 0-9 PNG compression
   - -YUV2RGB for converting the image YUVYUVYUV to RGBRGBRGB
   - -S for adding a suffix (time or number) to the file name (no overwriting of the images)

   - COND: specifies and input group whose activity controls whether the image 
   is saved or not (if absent, the images are saved all the time)


   Internal Tools:
   - save_lena_to_disk()
   - yuv2rgb()

   External Tools:
   - Kernel_Function/find_input_link()
   - tools/Vision/Vision/save_png_to_disk()
   - tools/Vision/Vision/free_prom_image()

   \file
   \ingroup f_save_image_to_disk

   \brief save image (different format availables, eg png, ppm, lena) to disk

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 11/08/2004




 ************************************************************/
/*#define DEBUG*/
#include <libx.h>
#include <zlib.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


#include <public_tools/Vision.h>
#include <Struct/prom_images_struct.h>
#include "tools/include/save_lena_to_disk.h"
#include "tools/include/yuv2rgb.h"
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>
#include <Kernel_Function/prom_getopt.h>

#define NO_SUFFIX 0
#define TIME_SUFFIX 1
#define NUMBER_SUFFIX 2
#define MILI_TIME_SUFFIX 3

#define PPM_IMG 1
#define LENA_IMG 2
#define PNG_IMG 3

#define DIXMILLE 10000;

typedef struct data_save_image_to_disk
{
	int current_nb;
	int gpe_ext;
	int gpe_cond;
	char file_name[256];
	int img_nb;
	int png_compression;
	int file_suffix;
	int yuv2rgb;
	int img_type;

} data_save_image_to_disk;

void new_save_image_to_disk(int gpe)
{
	int i, lien;
	data_save_image_to_disk *my_data = NULL;

	int gpe_cond = -1, gpe_ext = -1;
	int img_nb = 0;
	int img_type = PNG_IMG;
	int yuv2rgb = 0;
	int png_compression = Z_DEFAULT_COMPRESSION;
	int file_suffix = NO_SUFFIX;
	char file_name[256] = "\0";
	char param_link[256];

	if (def_groupe[gpe].data == NULL)
	{
		my_data = (data_save_image_to_disk *) malloc(sizeof(data_save_image_to_disk));
		i = 0;
		lien = find_input_link(gpe, i);
		while (lien != -1)
		{
			if (strcmp(liaison[lien].nom, "-COND") == 0)
			{
				gpe_cond = liaison[lien].depart;
				dprints("new_save_image_to_disk(%s): conditionnal link found\n", def_groupe[gpe].no_name);
			}
			else if(strcmp(liaison[lien].nom, "sync") == 0) {
				/* nothing to do */
			}
			else
			{
				gpe_ext = liaison[lien].depart;

				if (prom_getopt(liaison[lien].nom, "-T", param_link) == 2)
				{
					strcpy(file_name, param_link);
					dprints("new_save_image_to_disk(%s): Name for image files is %s\n", def_groupe[gpe].no_name, file_name);
				}

				if (prom_getopt(liaison[lien].nom, "-n", param_link) == 2 || prom_getopt(liaison[lien].nom, "-N", param_link) == 2)
				{
					if (strcmp(param_link, "ALL") == 0 || strcmp(param_link, "all") == 0)
						img_nb = 0;
					else
						img_nb = atoi(param_link);
				}

				/*-------------------------------------------------------*/
				/* Searching Compression level(only used with png images) */

				if (prom_getopt(liaison[lien].nom, "-z", param_link) == 2 || prom_getopt(liaison[lien].nom, "-Z", param_link) == 2)
				{
					if (strcmp(param_link, "BC") == 0 || strcmp(param_link, "bc") == 0)
						png_compression = Z_BEST_COMPRESSION;  /* Z_BEST_COMPRESSION = 9 */
					else if (strcmp(param_link, "DC") == 0 || strcmp(param_link, "dc") == 0)
						png_compression = Z_DEFAULT_COMPRESSION;  /* Z_DEFAULT_COMPRESSION = 6 */
					else if (strcmp(param_link, "BS") == 0 || strcmp(param_link, "bs") == 0)
						png_compression = Z_BEST_SPEED;  /* Z_BEST_SPEED = 1 */
					else if (strcmp(param_link, "NC") == 0 || strcmp(param_link, "nc") == 0)
						png_compression = Z_NO_COMPRESSION;  /* Z_NO_COMPRESSION = 0 */
					else
						png_compression = atoi(param_link);

					if (png_compression < 0 || png_compression > 9)
						png_compression = Z_DEFAULT_COMPRESSION;

					dprints("new_save_image_to_disk(%s): Compression level for png images is %d\n", def_groupe[gpe].no_name, png_compression);
				}

				if (prom_getopt(liaison[lien].nom, "-s", param_link) == 2 || prom_getopt(liaison[lien].nom, "-S", param_link) == 2)
				{
					if (strcmp(param_link, "T") == 0 || strcmp(param_link, "t") == 0)
						file_suffix = TIME_SUFFIX;
					if (strcmp(param_link, "U") == 0 || strcmp(param_link, "u") == 0)
						file_suffix = MILI_TIME_SUFFIX;
					else if (strcmp(param_link, "N") == 0 || strcmp(param_link, "n") == 0)
						file_suffix = NUMBER_SUFFIX;



				}

				if (strstr(liaison[lien].nom, "-PPM") != NULL || strstr(liaison[lien].nom, "-ppm") != NULL)
				{
					img_type = PPM_IMG;
					dprints("new_save_image_to_disk(%s): Image type is PPM\n", def_groupe[gpe].no_name);
				}
				else if (strstr(liaison[lien].nom, "-LENA") != NULL || strstr(liaison[lien].nom, "-lena") != NULL)
				{
					img_type = LENA_IMG;
					dprints("new_save_image_to_disk(%s): Image type is LENA\n", def_groupe[gpe].no_name);
				}
				else if (strstr(liaison[lien].nom, "-PNG") != NULL || strstr(liaison[lien].nom, "-png") != NULL)
				{
					img_type = PNG_IMG;
					dprints("new_save_image_to_disk(%s): Image type is PNG\n", def_groupe[gpe].no_name);
				}

				/*----------------------------------------*/
				/* Testing if conversion to rgb requested */
				if (strstr(liaison[lien].nom, "-YUV2RGB") != NULL || strstr(liaison[lien].nom, "-yuv2rgb") != NULL)
				{
					yuv2rgb = 1;
					dprints("new_save_image_to_disk(%s): Conversion from YUV to RGB requested\n", def_groupe[gpe].no_name);
				}


			}

			i++;
			lien = find_input_link(gpe, i);
		}

		if (gpe_ext == -1)
		{
			EXIT_ON_ERROR("ERROR in new_save_image_to_disk(%s): Missing input link\n", def_groupe[gpe].no_name);
		}

		if (strlen(file_name) == 0)
		{
			strcpy(file_name, "im_save_");

			if (file_suffix == NO_SUFFIX)
				file_suffix = TIME_SUFFIX;
		}
		dprints ("Enregitrement des images sous la forme : gpe %s %d\n",def_groupe[gpe].no_name,file_suffix);
		my_data->current_nb = 0;
		my_data->gpe_ext = gpe_ext;
		my_data->gpe_cond = gpe_cond;
		strcpy(my_data->file_name, file_name);
		my_data->img_nb = img_nb;
		my_data->png_compression = png_compression;
		my_data->file_suffix = file_suffix;
		my_data->yuv2rgb = yuv2rgb;
		my_data->img_type = img_type;
		def_groupe[gpe].data = my_data;
	}
}



void new_save_images_to_disk(int gpe)
{
	new_save_image_to_disk(gpe);
	/*((data_save_image_to_disk *) def_groupe[gpe].data)->file_suffix = NUMBER_SUFFIX;*/
}



void function_save_image_to_disk(int gpe)
{
	int i, gpe_cond, gpe_ext;
	int img_nb;
	time_t im_num;
	struct timeval daytime;
	int milisec = 0;
	char img_name[256],img_name_tmp[256];
	prom_images_struct *p_images_ext;
	prom_images_struct prom_image_tmp;


	data_save_image_to_disk *my_data = (data_save_image_to_disk *) def_groupe[gpe].data;


	if (my_data == NULL)
	{
		EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
	}

	gpe_cond = my_data->gpe_cond;
	gpe_ext = my_data->gpe_ext;
	img_nb = my_data->img_nb;

	if (gpe_cond != -1 && isequal(neurone[def_groupe[gpe_cond].premier_ele].s1, 0.))
		return;

	/*---------------------------------------*/
	/*Getting the extension of previous group */
	p_images_ext = (prom_images_struct *) def_groupe[gpe_ext].ext;
	if (p_images_ext == NULL)
	{
		EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): NULL ext pointer for input group\n", def_groupe[gpe].no_name);
	}

	/*------------------------------------------*/
	/*testing if this number exist in images_ext */
	if (img_nb > p_images_ext->image_number || img_nb < 0)
	{
		EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): Invalid image number (%i)\n", def_groupe[gpe].no_name, img_nb);
	}
	switch (my_data->file_suffix)
	{
	case NO_SUFFIX:
		strcpy(img_name_tmp, my_data->file_name);
		break;

	case NUMBER_SUFFIX:
		sprintf(img_name_tmp, "%s%06i", my_data->file_name, my_data->current_nb);
		my_data->current_nb++;
		break;

	case TIME_SUFFIX:
		time(&im_num);
		sprintf(img_name_tmp, "%s%ld", my_data->file_name, im_num);
		break;
	case MILI_TIME_SUFFIX :
		gettimeofday(&daytime, (void *) NULL);
		milisec = daytime.tv_usec / DIXMILLE;
		sprintf(img_name_tmp, "%s%ld_%02d", my_data->file_name, daytime.tv_sec,milisec);
		break;

	}

	dprints("f_save_image_to_disk(%s): Image name is %s\n", def_groupe[gpe].no_name, img_name);

	/*----------------------------------------*/
	/*Searching which type of image is to save */
	switch (my_data->img_type)
	{
	case PPM_IMG:
		sprintf(img_name, "%s.ppm", img_name_tmp);
		if (p_images_ext->nb_band != 3)
		{
			EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): Cannot proceed PPM with %i bands images (only 3)\n",def_groupe[gpe].no_name, p_images_ext->nb_band);
		}
		else
		{
			if (img_nb == 0)
			{
				save_ppm_to_disk(img_name, *p_images_ext);
			}
			else
			{
				/*------------------------------*/
				/*Create an one image prom_image */
				prom_image_tmp.image_number = 1;
				prom_image_tmp.sx = p_images_ext->sx;
				prom_image_tmp.sy = p_images_ext->sy;
				prom_image_tmp.nb_band = p_images_ext->nb_band;
				prom_image_tmp.images_table[0] = p_images_ext->images_table[img_nb - 1];

				save_ppm_to_disk(img_name, prom_image_tmp);
			}
		}
		break;

	case LENA_IMG:
		sprintf(img_name, "%s.lena", img_name_tmp);
		if (p_images_ext->nb_band != 1)
		{
			EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): Cannot proceed LENA with %i bands images (only 1)\n",def_groupe[gpe].no_name, p_images_ext->nb_band);
		}
		else
		{
			if (img_nb == 0)
			{
				save_lena_to_disk(img_name, *p_images_ext);
			}
			else
			{
				/*------------------------------*/
				/*Create an one image prom_image */
				prom_image_tmp.image_number = 1;
				prom_image_tmp.sx = p_images_ext->sx;
				prom_image_tmp.sy = p_images_ext->sy;
				prom_image_tmp.nb_band = p_images_ext->nb_band;
				prom_image_tmp.images_table[0] = p_images_ext->images_table[img_nb - 1];

				save_lena_to_disk(img_name, prom_image_tmp);
			}
		}
		break;

	case PNG_IMG:
		sprintf(img_name, "%s.png", img_name_tmp);
		if (p_images_ext->nb_band == 1)
		{
			if (img_nb == 0)
			{
				save_png_to_disk(img_name, *p_images_ext, my_data->png_compression);
			}
			else
			{
				/*------------------------------*/
				/*Create an one image prom_image */

				prom_image_tmp.image_number = 1;
				prom_image_tmp.sx = p_images_ext->sx;
				prom_image_tmp.sy = p_images_ext->sy;
				prom_image_tmp.nb_band = 1;
				prom_image_tmp.images_table[0] = p_images_ext->images_table[img_nb - 1];

				save_png_to_disk(img_name, prom_image_tmp, my_data->png_compression);
			}
		}
		else if (p_images_ext->nb_band == 3)
		{
			if (img_nb == 0)
			{
				/*------------------------------*/
				/*Create an one image prom_image */

				prom_image_tmp.image_number = p_images_ext->image_number;
				prom_image_tmp.sx = p_images_ext->sx;
				prom_image_tmp.sy = p_images_ext->sy;
				prom_image_tmp.nb_band = p_images_ext->nb_band;

				/*------------------------------------------------------*/
				/*Converting all array in rgbrgb if requested by user... */
				if (my_data->yuv2rgb == 1)
				{
					for (i = 0; i < p_images_ext->image_number; i++)
					{
						/*converting yuvyuvyuv image capture to rgbrgbrgb */
						prom_image_tmp.images_table[i] = yuv2rgb(p_images_ext->images_table[i], p_images_ext->sx, p_images_ext->sy);
					}

					save_png_to_disk(img_name, prom_image_tmp, my_data->png_compression);

					/*---------------------------*/
					/*Freeing all tempo rgb table */
					free_prom_image(&prom_image_tmp);
				}
				else
				{
					for (i = 0; i < p_images_ext->image_number; i++)
					{
						/*copying pointers */
						prom_image_tmp.images_table[i] = p_images_ext->images_table[i];
					}

					save_png_to_disk(img_name, prom_image_tmp, my_data->png_compression);
				}
			}
			else
			{
				/*------------------------------*/
				/*Create an one image prom_image */

				prom_image_tmp.image_number = 1;
				prom_image_tmp.sx = p_images_ext->sx;
				prom_image_tmp.sy = p_images_ext->sy;
				prom_image_tmp.nb_band = p_images_ext->nb_band;

				/*------------------------------------------------------*/
				/*Converting all array in rgbrgb if requested by user... */
				if (my_data->yuv2rgb == 1)
				{
					prom_image_tmp.images_table[0] = yuv2rgb(p_images_ext->images_table[img_nb - 1], p_images_ext->sx, p_images_ext->sy);
					save_png_to_disk(img_name, prom_image_tmp, my_data->png_compression);
					free_prom_image(&prom_image_tmp);
				}
				else
				{
					prom_image_tmp.images_table[0] = p_images_ext->images_table[img_nb - 1];
					save_png_to_disk(img_name, prom_image_tmp, my_data->png_compression);
				}
			}
		}
		else
		{
			EXIT_ON_ERROR("ERROR in f_save_image_to_disk(%s): Cannot proceed PNG with %i bands images (only 1 or 3)\n",def_groupe[gpe].no_name, p_images_ext->nb_band);
		}
		break;
	}	
}



void destroy_save_image_to_disk(int gpe)
{
	dprints("destroy_save_image_to_disk(%s): Entering function\n", def_groupe[gpe].no_name);
	if (def_groupe[gpe].data != NULL)
	{
		free(((data_save_image_to_disk *) def_groupe[gpe].data));
		def_groupe[gpe].data = NULL;
	}
	dprints("destroy_save_image_to_disk(%s): Leaving function\n", def_groupe[gpe].no_name);
}
