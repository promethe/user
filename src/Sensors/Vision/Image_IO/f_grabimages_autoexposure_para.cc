/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_grabimage.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

- author: B Mariat
- description: garther the softscalling and hardscalling functions in one
- date: 09/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 capturing the image(s)	
 * generic function		
 * calling miro or XIL		
 * according to the hardware
 
Macro:
-none

Local variables:
-none

Global variables:
-prom_images_struct images_capture

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Pan_Tilt/CaptureImage()
-tools/IO_Robot/Pan_Tilt/Refrsh()
-tools/Vision/calloc_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <Global_Var/Vision.h>

#include <libhardware.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
typedef struct MyData_f_grabimages_autoexposure{
	Camera * cam;
	int color_wanted;
	float scale_factor;
	int img_width;
	int img_height;
	int hardscale;
	int gpe_vigilance;
	unsigned int expo;
	unsigned int gain;
	int regul;
}MyData_f_grabimages_autoexposure;


void function_grabimages_autoexposure(int gpe_sortie)
{
  int i,stable=0,seuil;
  float sum,sum_abs;
float grad;
  char MessageFunctionTimeTrace[256];
struct timeval InputFunctionTimeTrace,OutputFunctionTimeTrace;
long SecondesFunctionTimeTrace;
long MicroSecondesFunctionTimeTrace;
  /*static Camera *cam=NULL;*/
  prom_images_struct *pt;
  int lien_entrant=-1;
  char param_link[256],name_cam[256];
  int color_wanted=0;
  int hardscale=0; /*0: mode softscaling, 1: mode hardscaling */
  float scale_factor=1.;
  MyData_f_grabimages_autoexposure * my_data;
  Camera * cam= NULL;
  int img_width,img_height;  
  int gpe_vigilance=-1;
  float the_vigilance;
  int stable_mvt,stable_gain,gain_i,expo_i;
	unsigned int expo=-1;
	unsigned int gain=-1;
int regul =25;
unsigned int * tmp;
  /*Refresh();*/
  /*NC Simulation de param user passer normalement sur le lien*/
  
  int l=0;
  
  if(def_groupe[gpe_sortie].ext==NULL)
  {
    lien_entrant = find_input_link(gpe_sortie,l);
    while(lien_entrant!=-1)
    {
	if(strcmp(liaison[lien_entrant].nom,"vigilance")==0)
	{
		gpe_vigilance=liaison[lien_entrant].depart;
	}
	if(prom_getopt(liaison[lien_entrant].nom,"-C",param_link)==2)
	{
		color_wanted=atoi(param_link);
	}
	else if(prom_getopt(liaison[lien_entrant].nom,"-c",param_link)==2)
	{
		color_wanted=atoi(param_link);
	}
	
	if(prom_getopt(liaison[lien_entrant].nom,"-S",param_link)==2)
	{
		scale_factor=atof(param_link);
	}
	else if(prom_getopt(liaison[lien_entrant].nom,"-s",param_link)==2)
	{
		scale_factor=atof(param_link);
	}
	
	if(prom_getopt(liaison[lien_entrant].nom,"-m",param_link)==2)
	{
		hardscale=atoi(param_link);
	}
	else if(prom_getopt(liaison[lien_entrant].nom,"-M",param_link)==2)
	{
		hardscale=atoi(param_link);
	}
	
	if(prom_getopt(liaison[lien_entrant].nom,"-h",param_link)==2)
	{
		strcpy(name_cam,param_link);
	}
	else if(prom_getopt(liaison[lien_entrant].nom,"-H",param_link)==2)
	{
		strcpy(name_cam,param_link);
	}
	if(prom_getopt(liaison[lien_entrant].nom,"-E",param_link)==2)
	{
		expo=atoi(param_link);
	}
	else if(prom_getopt(liaison[lien_entrant].nom,"-e",param_link)==2)
	{
		expo=atoi(param_link);
	}
	if(prom_getopt(liaison[lien_entrant].nom,"-G",param_link)==2)
        {
	                gain=atoi(param_link);
	}
        else if(prom_getopt(liaison[lien_entrant].nom,"-g",param_link)==2)
		        {
			            gain=atoi(param_link);
			 }
	cam=camera_get_cam_by_name(name_cam/*,cam_table,nbr_video_device*/);
	if(cam==NULL)
	{
		printf("erreur dans f_acquisiton\n");
		exit(0);
	}
  	l++;
	lien_entrant = find_input_link(gpe_sortie,l);
    }
    
    
    printf("param de f_grabimages(%d): color_wanted = %d\n , scale_factor = %f\n, camera is named : %s\ntype de scaling: %d\n",gpe_sortie,color_wanted,scale_factor,name_cam, hardscale);
    /*creation d'un objet camera*/

    my_data=malloc(sizeof(MyData_f_grabimages_autoexposure));
    my_data->cam=cam;
    my_data->scale_factor=scale_factor;
    my_data->color_wanted=color_wanted;
    my_data->hardscale=hardscale;
    my_data->gpe_vigilance=gpe_vigilance;
    my_data->expo=expo;
    my_data->gain=gain;
    my_data->regul=regul;
    /*my_data->camera=;*/ 
     
    /*camera_set_scale_factor(cam,scale_factor);*/
    /*Cette fonction reajuste la taille de l'image pour que la prochaine capture soit bonne
    Attention dans un mode thread, ca ne devrait pas marcher*/
  

    if(hardscale!=1){
      /* cas du soft scalling */

      img_width=cam->width_max/scale_factor;
      img_height=cam->height_max/scale_factor;

      if(color_wanted==1)	  
      {
	pt= calloc_prom_image(2,img_width,img_height,3);
      }
      else
      {
        pt= calloc_prom_image(2,img_width,img_height,1);
      }
      my_data->img_width=img_width;
      my_data->img_height=img_height;
    }
    else{
      /* cas du hard scalling */

      if(color_wanted==1)	  
        pt= calloc_prom_image(2,cam->width_max/(int)scale_factor,cam->height_max/(int)scale_factor,3);
      else
        pt= calloc_prom_image(2,cam->width_max/(int)scale_factor,cam->height_max/(int)scale_factor,1);
  
    }

  
    def_groupe[gpe_sortie].ext=(prom_images_struct*)pt;
    def_groupe[gpe_sortie].data=(MyData_f_grabimages_autoexposure*)my_data;

#ifdef FIREWIRE   
if(expo!=-1)
{
  camera_set_exposure_auto(cam,0);
  camera_set_exposure(cam,expo);
}
else
	  camera_set_exposure_auto(cam,1);
if(gain!=-1)
{
	  camera_set_gain_auto(cam,0);
	  camera_set_gain(cam,gain);
}
else
{
camera_set_gain_auto(cam,1);
}
#endif

    printf("vigilance = %d\n",gpe_vigilance);
  

  }
  else 
  {
	pt=(prom_images_struct*)def_groupe[gpe_sortie].ext;
	my_data=(MyData_f_grabimages_autoexposure*)def_groupe[gpe_sortie].data;
	cam=my_data->cam;
	scale_factor=my_data->scale_factor;
	color_wanted=my_data->color_wanted;
	hardscale=my_data->hardscale;
	img_height=my_data->img_height;
	img_width=my_data->img_width;
	gpe_vigilance=my_data->gpe_vigilance;
	expo=my_data->expo;
	gain=my_data->gain;
	regul=my_data->regul;
  } 
  /*Refresh();*/
  if(gpe_vigilance>=0)
  {
     the_vigilance=neurone[def_groupe[gpe_vigilance].premier_ele].s1;
  	printf("wait stabilisation\n");
  }
  else
  {
    the_vigilance=vigilence;
    printf("image_ vite\n");
  }
  gettimeofday(&InputFunctionTimeTrace, (void*)NULL);

  camera_grabimage(cam,pt->images_table[0],color_wanted,scale_factor,hardscale);
  camera_grabimage(cam,pt->images_table[0],color_wanted,scale_factor,hardscale);
 
 /*   previous_sum=1000;*/ 
  stable=0;
  stable_mvt=0;
  stable_gain=0;
  gain_i=gain;
expo_i=expo;
  while(stable!=1)
  { 
	camera_set_exposure(cam,expo);
        camera_set_gain(cam,gain); 	
	tmp=pt->images_table[1];
	pt->images_table[1]=pt->images_table[0];
        pt->images_table[0]=tmp;
      	camera_grabimage(cam,pt->images_table[0],color_wanted,scale_factor,hardscale);
    

/*analyse_mvt*/
         sum=0;
	 sum_abs=0;
	 for(i=0;i<pt->sx*pt->sy*pt->nb_band;i++)
	 {
	 	sum_abs=sum_abs+abs(pt->images_table[0][i]-pt->images_table[1][i]);
	 	sum=sum+(pt->images_table[0][i]-pt->images_table[1][i]);
	 }
	
         sum=fabs(sum/(float)(pt->sx*pt->sy*pt->nb_band));
	 sum_abs=sum_abs/(float)(pt->sx*pt->sy*pt->nb_band);
	 printf("sum: %f\nsum_abs=%f\n",sum,sum_abs);
	
	/*if(def_groupe[gpe_sortie].nbre>=2)
        {
	 neurone[def_groupe[gpe_sortie].premier_ele].s=neurone[def_groupe[gpe_sortie].premier_ele].s1=neurone[def_groupe[gpe_sortie].premier_ele].s2=sum_abs;
	 neurone[def_groupe[gpe_sortie].premier_ele+1].s=neurone[def_groupe[gpe_sortie].premier_ele].s1=neurone[def_groupe[gpe_sortie].premier_ele].s2=sum;
        }*/	 
	
	if(stable_mvt!=1)
	{
		if (the_vigilance>0.5)
		{
	
			if(sum_abs<15./*&& sum <1.*/)
			{
				stable_mvt=1;
			}
		}
		else
		{
			if(sum_abs<15.)
			{
				stable_mvt=1;
			}
		}
	}
/*Analyse gain*/
        grad=0.;
  	for(i=0;i<99;i++)
  	{
  		grad=grad+pt->images_table[0][(int)(i*(pt->sx*pt->sy)/101.)];
  	}
	grad=grad/(99.);	
	printf("regul => expo-gain:%d => %d %d  --  grad %f\n",regul,expo,gain,grad );

	if(grad<120)
	{
		regul++;
		if(regul>50)
			regul=50;
	}
	else if (grad>134)
	{	
		regul --;
		if (regul<0)
			regul=0;
	}
	else
	{
		stable_gain=1;	
	}	
	
	expo_i=regul/2*10;
	gain_i= expo + (regul % 2)*10;
	
	if(gain_i<0)
	{
		gain=0;
		gain_i=0;
	}
	else if (gain_i>255)
	{
		gain=255;
		gain_i=255;
	}
	else
		gain=(unsigned int)gain_i;
	
	if(expo_i<0)
	{
		expo=0;
		expo_i=0;
	}
	else if (expo_i>255)
	{
		expo_i=255;
		expo=255;
	}	
	else
		expo=(unsigned int)expo_i;


	if(regul==50||regul==0)
		stable_gain=1;


// 	if(grad<120)
// 	{
// 		if(gain_i<255)
// 			gain_i=gain_i+10;
// 		else		
// 			expo_i=expo_i+10;
// 	}
// 	else if (grad>134)
// 	{
// 	        if(expo_i==0)
// 			gain_i=gain_i-10;
// 		else
// 			expo_i=expo_i-10;
// 	}
// 	else
// 	{
// 		stable_gain=1;	
// 	}
// 	if(gain_i<0)
// 	{
// 		gain=0;
// 		gain_i=0;
// 	}
// 	else if (gain_i>255)
// 	{
// 		gain=255;
// 		gain_i=255;
// 	}
// 	else
// 		gain=(unsigned int)gain_i;
// 	
// 	if(expo_i<0)
// 	{
// 		expo=0;
// 		expo_i=0;
// 	}
// 	else if (expo_i>255)
// 	{
// 		expo_i=255;
// 		expo=255;
// 	}	
// 	else
// 		expo=(unsigned int)expo_i;
// 
// 	if(expo==255||gain==0)
// 		stable_gain=1;

    if(stable_gain==1&&stable_mvt==1)
		stable=1;
    
//     gettimeofday(&OutputFunctionTimeTrace, (void*)NULL);
//     if( OutputFunctionTimeTrace.tv_sec>InputFunctionTimeTrace.tv_sec+1)
// 	{
// 		
// 		stable=1;
// 	}
/*
	if(stable!=1)
	 {
	 		camera_grabimage(cam,pt->images_table[1],color_wanted,scale_factor,hardscale);	

 	 }*/
}

	my_data->expo=expo; 
	my_data->gain=gain;
	my_data->regul=regul;


// 	 sum=sum/(float)(pt->sx*pt->sy*pt->nb_band);
// 	 sum_abs=sum_abs*0.1/(float)(pt->sx*pt->sy*pt->nb_band);
// 	 printf("sum: %f\nsum_abs=%f\n",sum,sum_abs);
// 	
// 	if(def_groupe[gpe_sortie].nbre>=2)
//         {
// 	 neurone[def_groupe[gpe_sortie].premier_ele].s=neurone[def_groupe[gpe_sortie].premier_ele].s1=neurone[def_groupe[gpe_sortie].premier_ele].s2=sum_abs;
// 	 neurone[def_groupe[gpe_sortie].premier_ele+1].s=neurone[def_groupe[gpe_sortie].premier_ele].s1=neurone[def_groupe[gpe_sortie].premier_ele].s2=sum;
//         }	 
// 	 
// 	 
// 	 if (the_vigilance>0.5)
//   	 {
// 
// 		if(sum_abs<1.5 && abs((int)(sum*100)) <20.)
// 		{
// 			stable=1;
// 		}
// 		else
// 		{
// 			camera_grabimage(cam,pt->images_table[1],color_wanted,scale_factor,hardscale);
// 	
// 		}
//   	}
// 	else
// 	{s
// 		if(sum_abs<1.5)
// 	 	{
// 	 		stable=1;
// 	 	}
// 	 	else
// 	 	{
// 	 		camera_grabimage(cam,pt->images_table[1],color_wanted,scale_factor,hardscale);	
// 
//  	 	}
// 	}
//   
//   
//  }
  
 
 printf("============================= end grabimages\n");
}
