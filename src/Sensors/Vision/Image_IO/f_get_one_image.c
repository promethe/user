/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\defgroup f_get_one_image.c  f_get_one_image.c
\ingroup Sensors
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004
Modified:
- author: A. de RENGERVE
- description: update
- date: 07/06/2011
Modified:
- author: Ali K.
- description: update
- date: 23/05/2013

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 get one image specified by N (first image is 0)
 * include YUV2RGB transform    
 * result in ext

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-yuv2rgb()

External Tools: 
-tools/Vision/Vision/calloc_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
/* #define DEBUG */
#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>

#include "tools/include/yuv2rgb.h"

typedef struct mydata
{
	int gpe_entree;
	int YUV2RGB ;
	int index_image;
} Data_this;
void new_get_one_image(int gpe)
{
	Data_this *mydata = NULL;
	int  i=0, link;
	char param_link[32];
	dprints("enter %s\n",__FUNCTION__);

	if (def_groupe[gpe].data == NULL)
	{
		mydata=(Data_this*)malloc(sizeof(Data_this));
		if(mydata==NULL) EXIT_ON_ERROR("pb malloc dans %s\n",def_groupe[gpe].no_name);
		mydata->YUV2RGB = 0;
		mydata->index_image = -1;
		mydata->gpe_entree = -1;
		link = find_input_link(gpe, i);
		while (link!=-1)
		{
			if (strstr(liaison[link].nom, "sync") !=NULL )
			{
			}
			else
			{
				/*--------------------------------------------------------------------*/
				/*Searching the index number of image in the struct prom_images_struct */
				if (prom_getopt(liaison[link].nom, "N", param_link) == 2 || prom_getopt(liaison[link].nom, "n", param_link) == 2)
				{
					mydata->index_image = atoi(param_link); dprints("Index number of image to get is %i\n", index_image);
					/*Finding the input group */
					mydata->gpe_entree = liaison[link].depart;
				}
				/* Testing if conversion to rgb requested */
				if (prom_getopt(liaison[link].nom, "-YUV2RGB",param_link) == 1 || prom_getopt(liaison[link].nom, "-yuv2rgb",param_link) == 1)
				{
					mydata->YUV2RGB = 1;
					dprints("converting YUV2RGB\n");
				}
			}
			i++;
			link = find_input_link(gpe, i);

		}

	}
	if(mydata->index_image == -1 || mydata->gpe_entree == -1)
	{
		EXIT_ON_ERROR("Error in function_get_one_image (%s), you must specified -Nnumber or -nnumber\n",def_groupe[mydata->gpe_entree].no_name);
	}
	def_groupe[gpe].data=mydata;
}



void function_get_one_image(int gpe)
{
	unsigned char *im_table;
	int gpe_entree;
	int index_image = 0, YUV2RGB = 0;
	prom_images_struct *p_images_ext, *p_result;
	Data_this  *mydata   = NULL;


	mydata = (Data_this *) def_groupe[gpe].data;
	if (mydata == NULL)  EXIT_ON_ERROR("pb malloc");
	dprints("Enter in function_get_one_image (%s)\n",def_groupe[gpe_sortie].no_name);
	gpe_entree = mydata->gpe_entree;
	if (def_groupe[gpe].ext == NULL)
	{
		/*allocation de memoire*/

		if (def_groupe[mydata->gpe_entree].ext == NULL)
		{
			PRINT_WARNING("Gpe amont avec ext nulle; pas de calcul dans %s \n",__FUNCTION__);
			return;
		}
		p_images_ext = (prom_images_struct *) def_groupe[gpe_entree].ext;
		def_groupe[gpe].ext = (void *) malloc(sizeof(prom_images_struct));
		if (def_groupe[gpe].ext == NULL)  EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE %s...!",__FUNCTION__);
		/*alloc the p_result im wich goes into ext */
		printf("l'image d'entree dait %d\n",p_images_ext->nb_band);
		p_result = calloc_prom_image(1, p_images_ext->sx, p_images_ext->sy, p_images_ext->nb_band);/*a corriger Ali K.*/
		def_groupe[gpe].ext  = p_result;
	}
	else
	{
		/*Getting the extension of previous group */
		p_images_ext = (prom_images_struct *) (def_groupe[gpe_entree].ext);
		p_result = ((prom_images_struct *) (def_groupe[gpe].ext));

	}
	/*testing if this number exist in images_ext */
	if (index_image > (int)(p_images_ext->image_number))
	{
		EXIT_ON_ERROR("ERROR in function_get_one_image,the -Nnumber (or -nnumber)is too much high, "
				"there is only %i images in images_ext\n",p_images_ext->image_number);
	}
	/*----------------*/
	/*For color image */
	if (p_images_ext->nb_band == 3)
	{
		/*------------------------------------------------------*/
		/*Converting all array in rgbrgb if requested by user... */
		if (YUV2RGB)
		{
			/*-----------------------------------------------*/
			/*transformed RGB (yuv2rgb() allocates new table) */
			im_table = yuv2rgb(p_images_ext->images_table[index_image], p_images_ext->sx, p_images_ext->sy);

			/*------------------*/
			/*memcpy of im_table */
			memcpy(p_result->images_table[0], im_table,	p_images_ext->sx * p_images_ext->sy *
					p_images_ext->nb_band * sizeof(unsigned char));

			/*-------------*/
			/*free im_table */
			free(im_table);
			im_table = NULL;
		}
		else
		{
			/*---------------------------------------------------*/
			/*memcpy of p_images_ext->images_table[index_image] */
			memcpy(p_result->images_table[0],
					p_images_ext->images_table[index_image],
					p_images_ext->sx * p_images_ext->sy *
					p_images_ext->nb_band * sizeof(unsigned char));
		}
	}
	/*--------------*/
	/*For GRAY image */
	else
	{
		/*---------------------------------------------------*/
		/*memcpy of p_images_ext->images_table[index_image] */
		memcpy(p_result->images_table[0],
				p_images_ext->images_table[index_image],
				p_images_ext->sx * p_images_ext->sy * p_images_ext->nb_band *
				sizeof(unsigned char));
	}

	/*---------------------*/
	/*Filling the extension */
	def_groupe[gpe].ext = (prom_images_struct *) p_result;
	/*printf("%d %d \n",p_result->sx,p_result->sy); */

}
