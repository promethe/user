/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_load_image_from_disk.c
\brief load an image from the disk

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:

Description:
This file contains the C fonction for loading an image from the disk

Macro:
-none

Local variables:
-nnoe

Global variables:
-none

Internal Tools:
-none

External Tools: 
-tools/Vision/load_image_from_disk()

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs: none caus' not testedh

Todo: see the author for comments


http://www.doxygen.org
************************************************************/

#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <public_tools/Vision.h>

void function_load_image_from_disk(int Gpe)
{
  char *CompString, *LinkString, *ImageType = NULL, ImageName[256];
  unsigned char *Image;
  int sx, sy, i, nbands;

  if (def_groupe[Gpe].ext == NULL)
  {
    def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[Gpe].ext == NULL)   EXIT_ON_ERROR("No enought memory");

    for (i = 0; i < nbre_liaison; i++)
    {
      if (liaison[i].arrivee == Gpe)
      {
        LinkString = liaison[i].nom;
        CompString = strstr(LinkString, "-T");
        if (CompString != NULL)
        {
          CompString = strcpy(ImageName, CompString + 2);

          ImageType = strstr(LinkString, "lena");
          if (ImageType == NULL)    ImageType = strstr(LinkString, "png");
          if (ImageType == NULL)    EXIT_ON_ERROR("il faut une extension lena ou png");
        }
        else
          EXIT_ON_ERROR("il faut -Tnom");
        break;
      }
    }

    load_image_from_disk(ImageType, ImageName, &Image, &sx, &sy, &nbands);
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
    ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nbands;
    ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = Image;
    printf("%s(%d) : image %d X %d chargee, %d bands \n", __FUNCTION__, Gpe, sx, sy, nbands);
  }
}
