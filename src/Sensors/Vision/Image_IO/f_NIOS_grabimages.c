/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
    \ingroup libSensors
    \defgroup f_NIOSgrabimages f_NIOS_grabimages


   @author: N. Cuperlier
    - description: grab images / keypoint images from RobotSoc
    - date: 04/11/13
*/

/*#define TIME_TRACE*/

#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <Global_Var/Vision.h>
#include <stdint.h>
#ifdef TIME_TRACE
#include <Global_Var/SigProc.h>
#endif
#include <libhardware.h>

#include <Components/vision.h> /** dynamic hardware */

#include <dev.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>
#include <Struct/nios_pt_carac.h>



void new_NIOS_grabimages(int gpe)
{
	int i=0,l;

/*initailise data structure */
	data_NIOS_grabimages * pt=(data_NIOS_grabimages *)malloc(sizeof(data_NIOS_grabimages));
	
	
	

        def_groupe[gpe].data = (data_NIOS_grabimages *) pt;

 l = find_input_link(gpe, i);
  while (l != -1)
  {

if (strcmp(liaison[l].nom, "image_grey") == 0)
      pt->order=0x1| 0x8 ;
   else if (strcmp(liaison[l].nom, "image_grad") == 0)
     pt-> order=0x2| 0x8 ;
      
    else if (strcmp(liaison[l].nom, "image_dog") == 0)
     pt-> order = 0x5| 0x8;
else
pt->order = 0x0 | 0x8;
      
    if (prom_getopt_int(liaison[l].nom, "-sx", &(pt->sx)) == 0 || prom_getopt_int(liaison[l].nom, "-sx", &(pt->sx)) == 1 ) PRINT_WARNING("No image width defined");
     if (prom_getopt_int(liaison[l].nom, "-sy", &(pt->sy)) == 0 || prom_getopt_int(liaison[l].nom,"-sy", &(pt->sy)) == 1 ) PRINT_WARNING("No image heigth defined");

   if (prom_getopt_int(liaison[l].nom, "-landsx", &(pt->land_sx)) == 0 || prom_getopt_int(liaison[l].nom,  "-landsx", &(pt->land_sx) )== 1 ) PRINT_WARNING("No image width defined for keypoints");
     if (prom_getopt_int(liaison[l].nom, "-landsy", &(pt->land_sy)) == 0 || prom_getopt_int(liaison[l].nom,  "-landsy", &(pt->land_sy)) == 1 ) PRINT_WARNING("No image heigth defined for keypoints");
 if (prom_getopt_int(liaison[l].nom, "-nbkpt", &(pt->nb_pt)) == 0 || prom_getopt_int(liaison[l].nom,"-nbkpt", &(pt->nb_pt)) == 1 ) PRINT_WARNING("No image heigth defined for keypoints");

  i++;
    l = find_input_link(gpe, i);   
  }
printf("order request to NIOS_Client: %d\n",pt->order);
if( pt-> order&0x8){
/* TODO alloue 16 pt_carac*/
	pt->pts_carac=(prom_NIOS_pt_carac*)malloc(pt->nb_pt*sizeof(prom_NIOS_pt_carac));
	/*alloue une prom image pour chacun*/
	for(i=0;i<pt->nb_pt;i++){
		pt->pts_carac[i].rho_teta_image=(prom_images_struct *)calloc_prom_image(1, pt->land_sx, pt->land_sy, 4);
	}
}
/*si une image est demandee..*/
if( pt-> order&0x7){
/*TODO initialise image en dur pour le moment...*/
	def_groupe[gpe].ext=(prom_images_struct *)calloc_prom_image(1, pt->sx,pt->sy, 4);
}
}


void function_nios_grabimages(int gpe)
{
    int i , index;
    data_NIOS_grabimages *pt;
    Nios_pt_carac* cam;
    alt_u16 min=0xffff;
	alt_u16 max=0;
	int x,y;
    
/*     struct timespec duree_nanosleep, res; */



#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    char MessageFunctionTimeTrace[256];
#endif


    /*recupere la structure data_NIOS_grabimages */
    pt= (data_NIOS_grabimages *)def_groupe[gpe].data;
  
 if (pt == NULL) EXIT_ON_ERROR("function_nios_grabimages (%s): Cannot retrieve Nios_pt_carac !\n", def_groupe[gpe].no_name);
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
    printf("--------------\n Enter in function_nios_grabimages\n");
    printf(" -> demande le traitement d'une image au nios\n");
    /*demande le traitement d'une image*/
    cam=nioscam_grabimages(pt->order);
	/*TODO retirer la ligne suivante...*/
    cam=(get_nios_Client()->nios_pt_carac);
   printf("demande traitee par le nios: %p nbpt=%d\n",cam,cam->nb_pt);
   
printf("Remplissage de la struct keypoints nios cote prom_user...\n");    
	
if(pt->nb_pt!=(int)cam->nb_pt)PRINT_WARNING("function_nios_grabimages (%s): ask for %d keypoints, found %d !\n", def_groupe[gpe].no_name, pt->nb_pt,cam->nb_pt);

    /*rempli la structure*/
    pt->nb_pt=cam->nb_pt;
    pt->num_frame=(int)cam->dim_image;
    for(index=0;index< pt->nb_pt;index++){
    	pt->pts_carac[index].posx=(int)cam->pts_carac[index].posx;
    	pt->pts_carac[index].posy=(int)cam->pts_carac[index].posy;
    	pt->pts_carac[index].intensity=cam->pts_carac[index].intensity;
    	for(i=0;i<pt->land_sx*pt->land_sy;i++)
    		{
    		*((float *) (pt->pts_carac[index].rho_teta_image->images_table[0]) + i) = (float)(( cam->pts_carac[index].image [i]))/ (float)65535.;
       		}
    
    }

    printf("function_nios_grabimages : nb_pt=%d!\n",pt->nb_pt);
printf("Remplissage de l'image complete renvoyee par le nios cote prom_user...\n");

for (y = 0 ; y < pt->sy ; y++)
		{
			for (x = 0 ; x < pt->sx ; x++)
			{
				if ((get_nios_Client()->buffer[y*480+x]&0xFFFF) > max) 
					max = get_nios_Client()->buffer[y*480+x] & 0xFFFF;
				if ((get_nios_Client()->buffer[y*480+x]&0xFFFF) < min) 
					min = get_nios_Client()->buffer[y*480+x] & 0xFFFF;
			}
		}
printf("normalisation max=%d min=%d!\n",max,min);
for(i=0;i<(pt->sx)*(pt->sy);i++){
if (max == min) *((unsigned char *) (((prom_images_struct *)def_groupe[gpe].ext)->images_table[0]) + i) = 0;
					else *((unsigned char *) (((prom_images_struct *)def_groupe[gpe].ext)->images_table[0]) + i) = (unsigned char)((((get_nios_Client()->buffer[i])) - min)*255 / ((max - min)));
}
printf("Remplissage de l'image complete renvoyee par le nios cote prom_user : done!\n");

    printf("leaving function_nios_grabimages :  done!\n--------------\n");





#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
        MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Time in f_grabimages(%d)\t%4ld.%06d at\t%4ld.%06d\n", gpe,
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace,InputFunctionTimeTrace.tv_sec,InputFunctionTimeTrace.tv_usec);
    printf("%s", MessageFunctionTimeTrace);
#endif
}


