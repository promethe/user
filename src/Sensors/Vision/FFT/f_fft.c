/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_fft.c
\brief realise fft of an image

Author: M. Maillard
Created: 2002
Modified:
- author: M. Maillard
- description: specific file creation
- date: 20/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 realise la fft d'une image REELLE et stockee dans des floats
 la sortie est complexe
 attention : pour une image nx * ny, la transformee complexe est :
 nx * (ny/2+1) composee de la partie Im et Re. Du coup work_tab est plus
 grand que l'image
 pour acceder aux elements de la sortie, faite une copie vers un
 tableau de complexe fftw

Macro:
-none

Local variables:
-none

Global variables:
-list_rfftwnd_plan * list_prom_fft
-pthread_mutex_t mutex_list_prom_fft

Internal Tools:
-none

External Tools: 
-none

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/
#ifndef NO_FFT
#include <libx.h>
#include <string.h>
#include <pthread.h>
#include "fftw3.h"
#include <Struct/prom_images_struct.h>
#include <Struct/list_rfftwnd_plan.h>
#include <stdlib.h>

#include <Global_Var/Vision.h>
#endif


void function_fft_real_to_complex(int Gpe)
{
#ifndef NO_FFT
    list_rfftwnd_plan *list_curseur = NULL;
    int InputGpe = -1, image2convert;
    int real_ny;
    int i, j, nx, ny, n;
#ifdef USE_DOUBLE_PRECISION_FFT
    double *work_tab;
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    float *work_tab;
#endif
    float *input_tab, *output_tab;
    /*    int debbug = 0; */
    char *chaine = NULL, *ST = NULL;

#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif


#ifdef DEBUG
    printf("Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        /* verification de l'initialisation de list_prom_fft */
        if (list_prom_fft == NULL)
        {
            printf
                ("%s : erreur : list_prom_fft n'est pas initialise, utlisez 'function_init_fft'\n",
                 __FUNCTION__);
            exit(0);
        }
        list_curseur = list_prom_fft;

        /* recuperation du lien entrant */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                /*chaine = liaison[i].nom; */
                InputGpe = liaison[i].depart;
                chaine = liaison[i].nom;
                break;
            }
        if (InputGpe == -1)
        {
            /*printf("%s : pas de groupe amont !\n"); */
            printf("%s : pas de groupe amont !\n", __FUNCTION__);   /*RAJOUTER LE 02/04/2003 PAR ISSAM */
            exit(0);
        }
        if (def_groupe[InputGpe].ext == NULL)
        {
            printf("Gpe %d : le pointeur ext du groupe entrant est NULL\n",
                   Gpe);
            return;
        }
        /* recup d'info sur le lien */
        ST = NULL;
        ST = strstr(chaine, "-N");
        if (ST != NULL)
        {
            image2convert = atoi(&ST[2]);
            if (((prom_images_struct *) def_groupe[InputGpe].ext)->
                image_number > image2convert)
            {
#ifdef DEBUG
                printf
                    ("%s(groupe %d): recuperation de l'image numero %d, sur groupe %d\n",
                     __FUNCTION__, Gpe, image2convert, InputGpe);
#endif
            }
            else
            {
                printf
                    ("%s : %d (groupe %d): pas d'image %d dans le groupe %d\n",
                     __FUNCTION__, __LINE__, Gpe, image2convert, InputGpe);
                exit(0);
            }
        }
        else
        {
#ifdef DEBUG
            printf("%s(groupe %d): utilise image 0 du groupe %d\n",
                   __FUNCTION__, Gpe, InputGpe);
#endif
            image2convert = 0;
        }



        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }


        /* recuperation du pointeur sur l'image d'entree */
        /* stocke dans images_table[1] */
        if (((prom_images_struct *) def_groupe[InputGpe].ext)->image_number >
            image2convert)
        {
            ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
                ((prom_images_struct *) def_groupe[InputGpe].ext)->
                images_table[image2convert];
        }
        else
        {
            printf
                ("%s : %d (groupe %d): pas d'image %d dans le groupe amont\n",
                 __FUNCTION__, __LINE__, Gpe, image2convert);
            exit(0);
        }


        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {

            printf
                ("%s(%d) : votre image n'a pas son nbands=4  (nbands=%d), cette fonction ne fait que le N&B en float\n",
                 __FUNCTION__, Gpe,
                 ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band);

            exit(0);

        }
#ifdef DEBUG
        printf("%s(%d): %dx%d\n", __FUNCTION__, Gpe, nx, ny);
#endif

        /*prise du mutex pour que la liste chainne ne soit pas modifiee par init pendant le parcours */
        pthread_mutex_lock(&mutex_list_prom_fft);
        /*parcours de la liste chainee pour verifier qu'une structure correspondante a la taille a ete initialisee */
        while (list_curseur != NULL)
        {
            if (list_curseur->sx == nx && list_curseur->sy == ny
                && list_curseur->type == real_data)
            {
#ifdef DEBUG
                printf
                    ("Gpe :%d : %s : un tableau de FFT de taille %dx%d a ete trouve\n",
                     Gpe, __FUNCTION__, nx, ny);
#endif
                break;
            }
            list_curseur = list_curseur->next_fft_plan;

        }
        pthread_mutex_unlock(&mutex_list_prom_fft);

        if (list_curseur == NULL)
        {
            printf
                ("%s : groupe %d :  Aucun tableau de fft n'a ete initialise pour une taille de %dx%d en reel\n",
                 __FUNCTION__, Gpe, nx, ny);
            exit(0);
        }

        /*stockage du pointeur pour eviter d'avoir a le rechercher */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            (unsigned char *) list_curseur;



        /* alloc de mem pour la sortie */

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = (unsigned char *) malloc(nx * (ny / 2 + 1) * 2 * sizeof(float));    /* la taille du malloc est fait ainsi (une bande de plus) suivant les conseils de la doc de fftw */
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);

            exit(0);
        }

    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        n = nx * ny;
        list_curseur =
            (list_rfftwnd_plan
             *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                 images_table[3]);

    }

    real_ny = (ny / 2 + 1) * 2;

    input_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    output_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    work_tab = list_curseur->in_out;

    /*prise du mutex pour le tableau de fft correspondant : evite que 2 fft ayant la meme taille utilise le meme tableau simultanement */
    pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
    /* conversion de l'image vers les doubles si necessaire */
    for (i = 0; i < nx; i++)
        for (j = 0; j < real_ny; j++)
        {
            if (j < ny)
            {
#ifdef USE_DOUBLE_PRECISION_FFT
                work_tab[i * real_ny + j] = (double) (input_tab[j * nx + i]);
#endif
#ifdef USE_FLOAT_PRECISION_FFT
                work_tab[i * real_ny + j] = (input_tab[j * nx + i]);
#endif
            }
            else
                work_tab[i * real_ny + j] = 0.0;
        }

#ifdef USE_DOUBLE_PRECISION_FFT
    fftw_execute(list_curseur->prom_fft);
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    fftwf_execute(list_curseur->prom_fft);
#endif



    /* conversion vers les floats si nex=czssaire */
    /* copie de l'image resultat */
#ifdef USE_DOUBLE_PRECISION_FFT
    for (i = 0; i < nx * real_ny; i++)
        output_tab[i] = (float) work_tab[i];
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    for (i = 0; i < nx * real_ny; i++)
        output_tab[i] = work_tab[i];
#endif

    /*liberation du mutex pour cette taille de fft */
    pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));



#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function fft real to complex (Gpe %d) image %dx%d : \t%4ld.%06d\n",
            Gpe, nx, ny, SecondesFunctionTimeTrace,
            MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
#endif
}
