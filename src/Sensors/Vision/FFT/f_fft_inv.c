/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\brief realise the inverse fft of a complex image

\author M. Maillard
\date 20/07/2004


\section Description
This file contains the C fonction for inverse fft

\section sec_global Global variables
- list_rfftwnd_plan * list_prom_fft
- pthread_mutex_t mutex_list_prom_fft
*/
#ifndef NO_FFT
#include <libx.h>
#include <string.h>
#include <pthread.h>
#include "fftw3.h"
#include <Struct/prom_images_struct.h>
#include <Struct/list_rfftwnd_plan.h>

#include "stdlib.h"

#include <Global_Var/Vision.h>
#endif


void function_fft_inv_complex_to_real(int Gpe)
{
#ifndef NO_FFT
    list_rfftwnd_plan *list_curseur = NULL;
    int InputGpe = -1;
    int i, j, k, l, m, q, o, p, nx, ny, n;
#ifdef USE_DOUBLE_PRECISION_FFT
    double *work_tab;
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    float *work_tab;
#endif
    float *output_tab, *input_tab;
    int real_ny;
    int *temp;

    /*float val_min = +1e-10, val_max = -1e10;
       changement de val_min
       25/09/2003 Olivier Ledoux
     */
#ifdef DEBUG
    float val_min = +1e10, val_max = -1e10;
#endif
    int correction = 0, cuth = 0, cutv = 0;
    char *chaine = NULL, *ST = NULL;
    float scales;

#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif


#ifdef DEBUG
    printf("Gpe %d : %s", Gpe, __FUNCTION__);
#endif

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
    if (def_groupe[Gpe].ext == NULL)
    {

        /* verification de l'initialisation de list_prom_fft */
        if (list_prom_fft == NULL)
        {
            printf
                ("%s : erreur : list_prom_fft n'est  pas initialise, utlisez 'function_init_fft'\n",
                 __FUNCTION__);
            exit(0);
        }
        list_curseur = list_prom_fft;

        /* recuperation du lien entrant */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                InputGpe = liaison[i].depart;
                break;
            }
        if (InputGpe == -1)
        {
            printf("%s : pas de groupe amont !\n", __FUNCTION__);
            exit(0);
        }
        if (def_groupe[InputGpe].ext == NULL)
        {
            printf("%s : le pointeur ext en amont est NULL\n", __FUNCTION__);
            return;
        }

        ST = NULL;
        ST = strstr(chaine, "-center");
        if (ST != NULL)
        {
            /* l'image sera corrigee pour etre centree */
#ifdef DEBUG
            printf("%s(groupe %d): correction des cadrants (-center)\n",
                   __FUNCTION__, Gpe);
#endif
            correction = 1;
        }
        ST = NULL;
        ST = strstr(chaine, "-cuth");
        if (ST != NULL)
        {
            /* une partie de l'image sera mise a zero */
            cuth = atoi(&ST[5]);
#ifdef DEBUG
            if (cuth > 0)
                printf
                    ("%s(groupe %d): mise a zero des %d colonnes de gauche\n",
                     __FUNCTION__, Gpe, cuth);

            else
            {
                if (cuth < 0)
                    printf
                        ("%s(groupe %d): mise a zero des %d colonnes de droite\n",
                         __FUNCTION__, Gpe, -cuth);

                else
                    printf
                        ("%s(groupe %d): attention : '-cuth0' sur le lien entrant, est-ce normal ?\n",
                         __FUNCTION__, Gpe);
            }
#endif
        }
        ST = NULL;
        ST = strstr(chaine, "-cutv");
        if (ST != NULL)
        {
            /* une partie de l'image sera mise a zero */
            cutv = atoi(&ST[5]);
#ifdef DEBUG
            if (cutv > 0)
                printf
                    ("%s(groupe %d): mise a zero des %d lignes du haut\n",
                     __FUNCTION__, Gpe, cutv);
            else
            {
                if (cutv < 0)
                    printf
                        ("%s(groupe %d): mise a zero des %d lignes du bas\n",
                         __FUNCTION__, Gpe, -cutv);
                else
                    printf
                        ("%s(groupe %d): attention : '-cutv0' sur le lien entrant, est-ce normal ?\n",
                         __FUNCTION__, Gpe);
            }
#endif
        }
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }


        /* recuperation du pointeur sur l'image d'entree */
        /* stocke dans images_table[1] */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[0];

        /* recuperation des info du groupe precedent */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("%s : votre image est couleur ou de type 'uchar', cette fonction ne fait que le 'float N&B'\n",
                 __FUNCTION__);
            exit(0);
        }

        /*parcours de la liste chainee pour verifier si un tableau de fft de la taille correspondante a bien ete initialise */
        /*prise du mutex de la list pour eviter une modif de celle-ci pendant le parcours */
        pthread_mutex_lock(&mutex_list_prom_fft);
        while (list_curseur != NULL)
        {
            if (list_curseur->sx == nx && list_curseur->sy == ny)
            {
#ifdef DEBUG
                printf
                    ("Gpe %d : %s : un tableau de fft de taille %dx%d a ete trouve\n",
                     Gpe, __FUNCTION__, nx, ny);
#endif
                break;
            }
            list_curseur = list_curseur->next_fft_plan;
        }
        pthread_mutex_unlock(&mutex_list_prom_fft);
        if (list_curseur == NULL)
        {
            printf
                ("%s : aucun tableau initialise de fft de taille %dx%d n'a ete trouve\n",
                 __FUNCTION__, nx, ny);
            exit(0);
        }

        /*stockage du pointeur pour eviter d'avoir a la retrouve */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] =
            (unsigned char *) list_curseur;

        /* alloc de mem pour la sortie */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(nx * ny * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }


        /* alloc de mem pour les paramettres */

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            (unsigned char *) malloc(3 * sizeof(int));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[3] == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        temp =
            ((int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
             images_table[3]);
        temp[0] = correction;
        temp[1] = cuth;
        temp[2] = cutv;
    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;

        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;

        n = nx * ny;

        temp =
            ((int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
             images_table[3]);
        correction = temp[0];
        cuth = temp[1];
        cutv = temp[2];

        list_curseur =
            (list_rfftwnd_plan
             *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                 images_table[4]);
    }


    real_ny = (ny / 2 + 1) * 2;

    input_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    output_tab =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    work_tab = list_curseur->in_out_inv;


    /*prise du mutex sur le tableau pour eviter qu'une fft de meme dimension s'effectue simultanement sur le meme tableau */
    pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
    /* conversion de la fft float vers les doubles si necessaire */
#ifdef USE_DOUBLE_PRECISION_FFT
    for (i = 0; i < nx * real_ny; i++)  /* ne doit pas poser de pb de taille */
        work_tab[i] = (double) input_tab[i];
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    for (i = 0; i < nx * real_ny; i++)  /* ne doit pas poser de pb de taille */
        work_tab[i] = input_tab[i];
#endif

    /* fft */
#ifdef USE_DOUBLE_PRECISION_FFT
    fftw_execute(list_curseur->prom_fft_inv);
#endif
#ifdef USE_FLOAT_PRECISION_FFT
    fftwf_execute(list_curseur->prom_fft_inv);
#endif



/* conversion vers les floats */
/* copie de l'image resultat */
/* sans ou avec correction des cadrants */


/*les indices c'est nimp mais j'ai optimise le tout pour que gcc -o2 puisque imbrique certaines boucles*/
    scales = (float) n;
    scales = 1.0 / scales;
    if (correction == 0)
    {
        for (i = 0; i < nx; i++)
            for (j = 0; j < ny; j++)
            {
                output_tab[j * nx + i] =
                    (float) work_tab[i * real_ny + j] * scales;
            }
    }
    else
    {
        for (i = 0; i < nx / 2; i++)
            for (j = 0; j < ny / 2; j++)
            {
                output_tab[j * nx + i] =
                    (float) work_tab[(i + nx / 2) * real_ny + j +
                                     ny / 2] * scales;
            }
        for (k = nx / 2; k < nx; k++)
            for (l = 0; l < ny / 2; l++)
            {
                output_tab[l * nx + k] =
                    (float) work_tab[(k - nx / 2) * real_ny + l +
                                     ny / 2] * scales;
            }
        for (m = 0; m < nx / 2; m++)
            for (q = ny / 2; q < ny; q++)
            {
                output_tab[q * nx + m] =
                    (float) work_tab[(m + nx / 2) * real_ny + q -
                                     ny / 2] * scales;
            }
        for (o = nx / 2; o < nx; o++)
            for (p = ny / 2; p < ny; p++)
            {
                output_tab[p * nx + o] =
                    (float) work_tab[(o - nx / 2) * real_ny + p -
                                     ny / 2] * scales;
            }
    }

    /*on libere le mutex pou cette taille de fft donnee */
    pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));

    /* cuth */
    if (cuth != 0)
    {
        if (cuth > 0)
            for (i = 0; i < cuth; i++)
                for (j = 0; j < ny; j++)
                    output_tab[j * nx + i] = 0.0;
        else
            for (i = nx + cuth; i < nx; i++)
                for (j = 0; j < ny; j++)
                    output_tab[j * nx + i] = 0.0;
    }
    /* cutv */
    if (cutv != 0)
    {
#ifdef DEBUG
        printf("cutv : %d\n", cutv);
#endif
        if (cutv > 0)
            for (k = 0; k < nx; k++)
                for (l = 0; l < cutv; l++)
                    output_tab[l * nx + k] = 0.0;
        else
            for (k = 0; k < nx; k++)
                for (l = ny + cutv; l < ny; l++)
                    output_tab[l * nx + k] = 0.0;

    }




#ifdef DEBUG
    for (i = 0; i < n; i++)
    {
        if (output_tab[i] > val_max)
            val_max = output_tab[i];
        else if (output_tab[i] < val_min)
            val_min = output_tab[i];

    }

    printf("Gpe: %d , %f < im2 < %f\n", Gpe, val_min, val_max);
#endif

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function complex to real (Gpe %d) image %dx%d : \t%4ld.%06d\n",
            Gpe, nx, ny, SecondesFunctionTimeTrace,
            MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
#endif

}
