/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_fft.c
 \brief realise fft of an image

 Author: M. Maillard
 Created: 2002
 Modified:
 - author: M. Maillard
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 realise la fft d'une image REELLE et stockee dans des floats
 la sortie est complexe
 attention : pour une image nx * ny, la transformee complexe est :
 nx * (ny/2+1) composee de la partie Im et Re. Du coup work_tab est plus
 grand que l'image
 pour acceder aux elements de la sortie, faite une copie vers un
 tableau de complexe fftw

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -list_rfftwnd_plan * list_prom_fft
 -pthread_mutex_t mutex_list_prom_fft

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: none
 - description: none
 - input expected group: Image of real point
 - where are the data?: in the image to convert

 Comments:

 Known bugs: none (yet!)

 Todo:	see the author to comment the file.


 http://www.doxygen.org
 ************************************************************/
#ifndef NO_FFT
#include <libx.h>
#include <string.h>
#include <pthread.h>
#include "fftw3.h"
#include <Struct/prom_images_struct.h>
#include <Struct/list_rfftwnd_plan.h>
#include <stdlib.h>

#include <Global_Var/Vision.h>
#endif

#undef DEBUG

#ifdef NO_FFT
void function_filtre_FFT_complex(int Gpe)
{
  ;
  return;
}
#endif

#ifndef NO_FFT
#ifdef USE_FLOAT_PRECISION_FFT
typedef struct struct_fftwf_complex_tag
{
  float *InputImage;
  fftwf_complex *mask;
  list_rfftwnd_plan *curseur;
  int correction;

}struct_fftwf_complex;
#endif
#endif

#ifndef NO_FFT
#ifdef USE_FLOAT_PRECISION_FFT
void function_filtre_FFT_complex(int Gpe)
{
  list_rfftwnd_plan *list_curseur = NULL;
  int i, j, nx, ny, n, k, m, o, u, l, p, q;
  fftwf_complex *mask, *work_tab, *in_out_complex, *in_out_inv_complex;
  int Gpe_Mask = -1, Gpe_Image = -1;
  char *chaine = NULL;
  float *reel_mask, *imag_mask;
  float *InputImage, *OutputImage;
  struct_fftwf_complex *this_data = NULL;
  float scales;
  int correction = 0;
#ifdef DEBUG
  printf("Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  if (def_groupe[Gpe].ext == NULL)
  {
    /* verification de l'initialisation de list_prom_fft */
    if (list_prom_fft == NULL)
    {
      printf
      ("%s : erreur : list_prom_fft n'est pas initialise, utlisez 'function_init_fft'\n",
          __FUNCTION__);
      exit(0);
    }
    list_curseur = list_prom_fft;
    for (i = 0; i < nbre_liaison; i++)
    {
      if (liaison[i].arrivee == Gpe)
      {
        chaine = liaison[i].nom;
        if (strstr(chaine, "-center") != NULL)
        correction = 1;
        if (strstr(chaine, "-Mask") != NULL)
        Gpe_Mask = liaison[i].depart;
        else if (strstr(chaine, "-Image") != NULL)
        Gpe_Image = liaison[i].depart;
        else
        {
          printf
          ("%s : parametre des liens entrants incorrects (-UP et -DOWN)\n",
              __FUNCTION__);
          exit(0);
        }
      }
    }
    if (Gpe_Mask == -1 || Gpe_Image == -1)
    {
      printf
      ("%s : il manque une liaison entrante ou un parametre est incorrect\n",
          __FUNCTION__);
      exit(0);
    }

    if (def_groupe[Gpe_Mask].ext == NULL
        || def_groupe[Gpe_Image].ext == NULL)
    {
      printf
      ("%s : un des pointeurs des liens entrants n'a pas ete initialise\n",
          __FUNCTION__);
      /*exit(0); */
      return;
    }

    /* verif des groupes entrants */
    if (((prom_images_struct *) def_groupe[Gpe_Image].ext)->nb_band != 4
        || ((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        image_number != 1)
    {
      printf("Erreur sur le groupe image entrant fft_complex\n");
      exit(1);
    }
    if (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->nb_band != 4
        || ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->
        image_number != 2)
    {
      printf("Erreur sur le groupe mask entrant fft_complex\n");
      exit(1);
    }

    /* allocation de memoire */
    def_groupe[Gpe].ext =
    (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[Gpe].ext == NULL)
    {
      printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
          __LINE__);
      exit(0);
    }
    def_groupe[Gpe].data = malloc(sizeof(struct_fftwf_complex));
    if (def_groupe[Gpe].data == NULL)
    {
      printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
          __LINE__);
      exit(0);
    }

    this_data = (struct_fftwf_complex *) def_groupe[Gpe].data;
    this_data->correction = correction;
    /**/
    InputImage = this_data->InputImage =
    (float *) (((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        images_table[0]);

    /* recuperation des info du groupe precedent et remplissage prom_images_struct */
    nx = ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->sx;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
    ny = ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->sy;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
    n = nx * ny;
    ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
    ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;

    /*on alloue image resultat */
    OutputImage = malloc(n * sizeof(float));
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
    (unsigned char *) OutputImage;
    if (((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        images_table[0] == NULL)
    {
      printf("Erreur malloc dans complex_fftw\n");
      exit(1);
    }

    /*on recherche le plan de fft associe (mask et image) */
    /*prise du mutex pour que la liste chainne ne soit pas modifiee par init pendant le parcours */
    pthread_mutex_lock(&mutex_list_prom_fft);
    /*parcours de la liste chainee pour verifier qu'une structure correspondante a la taille a ete initialisee */
    while (list_curseur != NULL)
    {
      if (list_curseur->sx == nx && list_curseur->sy == ny
          && list_curseur->type == complex_data)
      {
#ifdef DEBUG
        printf
        ("Gpe :%d : %s : un tableau de FFT de taille %dx%d a ete trouve\n",
            Gpe, __FUNCTION__, nx, ny);
#endif
        break;
      }
      list_curseur = list_curseur->next_fft_plan;

    }
    pthread_mutex_unlock(&mutex_list_prom_fft);

    if (list_curseur == NULL)
    {
      printf
      ("%s : groupe %d :  Aucun tableau de fft n'a ete initialise pour une taille de %dx%d en complex\n",
          __FUNCTION__, Gpe, nx, ny);
      exit(0);
    }

    /*on retient le list_curseur */
    this_data->curseur = list_curseur;

    /*on s'occupe du mask */
    this_data->mask = fftwf_malloc(nx * ny * sizeof(fftwf_complex));
    if (this_data == NULL)
    {
      printf("Erreur malloc dans complex_fftw\n");
      exit(1);
    }

    /*on lock cette fft pour eviter probleme thread */
    pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
    work_tab = list_curseur->in_out_complex;

    /*on recopie le mask dans le plan de fft */
    reel_mask = (float *) (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->images_table[0]);
    imag_mask = (float *) (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->images_table[1]);
    for (i = 0; i < nx; i++)
    for (j = 0; j < ny; j++)
    {
      work_tab[i * ny + j][0] = *(reel_mask + j * nx + i);
      work_tab[i * ny + j][1] = *(reel_mask + i + j * nx);
    }
    /*on fait la fft du mask */
    fftwf_execute(list_curseur->prom_fft);

    mask = this_data->mask;
    /*on recopie la fft du mask dans this_data->mask */
    for (i = 0; i < n; i++)
    {
      mask[i][0] = work_tab[i][0];
      mask[i][1] = work_tab[i][1];
    }

    /*on rend le mutex */
    pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));

  }
  else
  {
    OutputImage =
    (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0]);
    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;
    this_data = def_groupe[Gpe].data;

    InputImage = this_data->InputImage;
    mask = this_data->mask;
    list_curseur = this_data->curseur;
    correction = this_data->correction;

  }

  in_out_complex = list_curseur->in_out_complex;
  in_out_inv_complex = list_curseur->in_out_inv_complex;

#ifdef DEBUG
  printf
  ("taille %d %d output %p input %p mask %p curseur %p in_out %p in_out_inv %p\n",
      nx, ny, OutputImage, InputImage, mask, list_curseur, in_out_complex,
      in_out_inv_complex);
#endif

  pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
  for (i = 0; i < nx; i++)
  for (j = 0; j < ny; j++)
  {
    in_out_complex[i * ny + j][0] = *(InputImage + j * nx + i);
    in_out_complex[i * ny + j][1] = (float) 0.;
  }
  /*fft sur place de l'image */
  fftwf_execute(list_curseur->prom_fft);

  /*multiplication sur in_out_inv_complex entre fft_image et fft_mask */

  for (i = 0; i < n; i++)
  {
    in_out_inv_complex[i][0] =
    in_out_complex[i][0] * mask[i][0] -
    in_out_complex[i][1] * mask[i][1];
    in_out_inv_complex[i][1] =
    in_out_complex[i][0] * mask[i][1] +
    in_out_complex[i][1] * mask[i][0];
  }

  /*fft_inv de la nouvelle_image */
  fftwf_execute(list_curseur->prom_fft_inv);
  /*stockage et calcule de l'image reelle dans la prom_images_struct */

  scales = (float) n;
  scales = 1.0 / scales;
  if (correction == 0)
  {
    for (i = 0; i < nx; i++)
    for (j = 0; j < ny; j++)
    {
      u = i * ny + j;
      OutputImage[j * nx + i] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
  }
  else
  {
    for (i = 0; i < nx / 2; i++)
    for (j = 0; j < ny / 2; j++)
    {
      u = (i + nx / 2) * ny + j + ny / 2;
      OutputImage[j * nx + i] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (k = nx / 2; k < nx; k++)
    for (l = 0; l < ny / 2; l++)
    {
      u = (k - nx / 2) * ny + l + ny / 2;
      OutputImage[l * nx + k] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (m = 0; m < nx / 2; m++)
    for (q = ny / 2; q < ny; q++)
    {
      u = (m + nx / 2) * ny + q - ny / 2;
      OutputImage[q * nx + m] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (o = nx / 2; o < nx; o++)
    for (p = ny / 2; p < ny; p++)
    {
      u = (o - nx / 2) * ny + p - ny / 2;
      OutputImage[p * nx + o] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
  }

  pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));

#ifdef DEBUG
  printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
}
#endif
#endif

#ifndef NO_FFT
#ifdef USE_DOUBLE_PRECISION_FFT
typedef struct struct_fftw_complex_tag
{
  float *InputImage;
  fftw_complex *mask;
  list_rfftwnd_plan *curseur;
  int correction;

}struct_fftw_complex;
#endif
#endif

#ifndef NO_FFT
#ifdef USE_DOUBLE_PRECISION_FFT
void function_filtre_FFT_complex(int Gpe)
{
  list_rfftwnd_plan *list_curseur = NULL;
  int i, j, nx, ny, n, k, m, o, u, l, p, q;
  fftw_complex *mask, *work_tab, *in_out_complex, *in_out_inv_complex;
  int Gpe_Mask = -1, Gpe_Image = -1;
  char *chaine = NULL;
  float *reel_mask, *imag_mask;
  float *InputImage, *OutputImage;
  struct_fftw_complex *this_data = NULL;
  int correction = 0;

#ifdef DEBUG
  printf("Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  if (def_groupe[Gpe].ext == NULL)
  {
    /* verification de l'initialisation de list_prom_fft */
    if (list_prom_fft == NULL)
    {
      printf
      ("%s : erreur : list_prom_fft n'est pas initialise, utlisez 'function_init_fft'\n",
          __FUNCTION__);
      exit(0);
    }
    list_curseur = list_prom_fft;
    for (i = 0; i < nbre_liaison; i++)
    {
      if (liaison[i].arrivee == Gpe)
      {
        chaine = liaison[i].nom;
        if (strstr(chaine, "-center") != NULL)
        correction = 1;
        if (strstr(chaine, "-Mask") != NULL)
        Gpe_Mask = liaison[i].depart;
        else if (strstr(chaine, "-Image") != NULL)
        Gpe_Image = liaison[i].depart;
        else
        {
          printf
          ("%s : parametre des liens entrants incorrects (-UP et -DOWN)\n",
              __FUNCTION__);
          exit(0);
        }
      }
    }
    if (Gpe_Mask == -1 || Gpe_Image == -1)
    {
      printf
      ("%s : il manque une liaison entrante ou un parametre est incorrect\n",
          __FUNCTION__);
      exit(0);
    }

    if (def_groupe[Gpe_Mask].ext == NULL
        || def_groupe[Gpe_Image].ext == NULL)
    {
      printf
      ("%s : un des pointeurs des liens entrants n'a pas ete initialise\n",
          __FUNCTION__);
      /*exit(0); */
      return;
    }

    /* verif des groupes entrants */
    if (((prom_images_struct *) def_groupe[Gpe_Image].ext)->nb_band != 4
        || ((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        image_number != 1)
    {
      printf("Erreur sur le groupe image entrant fft_complex\n");
      exit(1);
    }
    if (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->nb_band != 4
        || ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->
        image_number != 2)
    {
      printf("Erreur sur le groupe mask entrant fft_complex\n");
      exit(1);
    }

    /* allocation de memoire */
    def_groupe[Gpe].ext =
    (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[Gpe].ext == NULL)
    {
      printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
          __LINE__);
      exit(0);
    }
    def_groupe[Gpe].data = malloc(sizeof(struct_fftw_complex));
    if (def_groupe[Gpe].data == NULL)
    {
      printf("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
          __LINE__);
      exit(0);
    }

    this_data = (struct_fftw_complex *) def_groupe[Gpe].data;
    this_data->correction = correction;
    /**/
    InputImage = this_data->InputImage =
    (float *) (((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        images_table[0]);

    /* recuperation des info du groupe precedent et remplissage prom_images_struct */
    nx = ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->sx;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
    ny = ((prom_images_struct *) def_groupe[Gpe_Mask].ext)->sy;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
    n = nx * ny;
    ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
    ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;

    /*on alloue image resultat */
    OutputImage = malloc(n * sizeof(float));
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
    (unsigned char *) OutputImage;
    if (((prom_images_struct *) def_groupe[Gpe_Image].ext)->
        images_table[0] == NULL)
    {
      printf("Erreur malloc dans complex_fftw\n");
      exit(1);
    }

    /*on recherche le plan de fft associe (mask et image) */
    /*prise du mutex pour que la liste chainne ne soit pas modifiee par init pendant le parcours */
    pthread_mutex_lock(&mutex_list_prom_fft);
    /*parcours de la liste chainee pour verifier qu'une structure correspondante a la taille a ete initialisee */
    while (list_curseur != NULL)
    {
      if (list_curseur->sx == nx && list_curseur->sy == ny
          && list_curseur->type == complex_data)
      {
#ifdef DEBUG
        printf
        ("Gpe :%d : %s : un tableau de FFT de taille %dx%d a ete trouve\n",
            Gpe, __FUNCTION__, nx, ny);
#endif
        break;
      }
      list_curseur = list_curseur->next_fft_plan;

    }
    pthread_mutex_unlock(&mutex_list_prom_fft);

    if (list_curseur == NULL)
    {
      printf
      ("%s : groupe %d :  Aucun tableau de fft n'a ete initialise pour une taille de %dx%d en complex\n",
          __FUNCTION__, Gpe, nx, ny);
      exit(0);
    }

    /*on retient le list_curseur */
    this_data->curseur = list_curseur;

    /*on s'occupe du mask */
    this_data->mask = fftw_malloc(nx * ny * sizeof(fftw_complex));
    if (this_data == NULL)
    {
      printf("Erreur malloc dans complex_fftw\n");
      exit(1);
    }

    /*on lock cette fft pour eviter probleme thread */
    pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
    work_tab = list_curseur->in_out_complex;

    /*on recopie le mask dans le plan de fft */
    reel_mask = (float *) (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->images_table[0]);
    imag_mask = (float *) (((prom_images_struct *) def_groupe[Gpe_Mask].ext)->images_table[1]);
    for (i = 0; i < nx; i++)
    for (j = 0; j < ny; j++)
    {
      work_tab[i * ny + j][0] = *(reel_mask + j * nx + i);
      work_tab[i * ny + j][1] = *(reel_mask + i + j * nx);
    }
    /*on fait la fft du mask */
    fftw_execute(list_curseur->prom_fft);

    mask = this_data->mask;
    /*on recopie la fft du mask dans this_data->mask */
    for (i = 0; i < n; i++)
    {
      mask[i][0] = work_tab[i][0];
      mask[i][1] = work_tab[i][1];
    }

    /*on rend le mutex */
    pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));

  }
  else
  {
    OutputImage =
    (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0]);
    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
    n = nx * ny;
    this_data = def_groupe[Gpe].data;

    InputImage = this_data->InputImage;
    mask = this_data->mask;
    list_curseur = this_data->curseur;
    correction = this_data->correction;

  }

  in_out_complex = list_curseur->in_out_complex;
  in_out_inv_complex = list_curseur->in_out_inv_complex;

  pthread_mutex_lock(&(list_curseur->mutex_prom_fft));
  /*image recopier dans in_out_complex */
  for (i = 0; i < nx; i++)
  for (j = 0; j < ny; j++)
  {
    in_out_complex[i * ny + j][0] = *(InputImage + j * nx + i);
    in_out_complex[i * ny + j][1] = (float) 0.;
  }
  /*fft sur place de l'image */
  fftw_execute(list_curseur->prom_fft);

  /*multiplication sur in_out_inv_complex entre fft_image et fft_mask */

  for (i = 0; i < n; i++)
  {
    in_out_inv_complex[i][0] =
    in_out_complex[i][0] * mask[i][0] -
    in_out_complex[i][1] * mask[i][1];
    in_out_inv_complex[i][1] =
    in_out_complex[i][0] * mask[i][1] +
    in_out_complex[i][1] * mask[i][0];
  }

  /*fft_inv de la nouvelle_image */
  fftw_execute(list_curseur->prom_fft_inv);
  /*stockage et calcule de l'image reelle dans la prom_images_struct */

  scales = (float) n;
  scales = 1.0 / scales;
  if (correction == 0)
  {
    for (i = 0; i < nx; i++)
    for (j = 0; j < ny; j++)
    {
      u = i * ny + j;
      OutputImage[j * nx + i] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
  }
  else
  {
    for (i = 0; i < nx / 2; i++)
    for (j = 0; j < ny / 2; j++)
    {
      u = (i + nx / 2) * ny + j + ny / 2;
      OutputImage[j * nx + i] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (k = nx / 2; k < nx; k++)
    for (l = 0; l < ny / 2; l++)
    {
      u = (k - nx / 2) * ny + l + ny / 2;
      OutputImage[l * nx + k] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (m = 0; m < nx / 2; m++)
    for (q = ny / 2; q < ny; q++)
    {
      u = (m + nx / 2) * ny + q - ny / 2;
      OutputImage[q * nx + m] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
    for (o = nx / 2; o < nx; o++)
    for (p = ny / 2; p < ny; p++)
    {
      u = (o - nx / 2) * ny + p - ny / 2;
      OutputImage[p * nx + o] =
      sqrt(in_out_inv_complex[u][0] * in_out_inv_complex[u][0] +
          in_out_inv_complex[u][1] *
          in_out_inv_complex[u][1]) * scales;
    }
  }

  pthread_mutex_unlock(&(list_curseur->mutex_prom_fft));

#ifdef DEBUG
  printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

}
#endif
#endif
