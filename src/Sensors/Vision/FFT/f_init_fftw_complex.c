/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_init_fftw.c
\brief intialisation of fft and fftw table creation

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:

Description:
This file contains the C fonction for initialising fft and creating fftw tables

Macro:
-none

Local variables:
-none

Global variables:
-list_rfftwnd_plan * list_prom_fft
-pthread_mutex_t mutex_list_prom_fft


Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs:

Todo: see the author to comment the file


http://www.doxygen.org
************************************************************/
#ifndef NO_FFT
#include <libx.h>
#include <pthread.h>
#include <string.h>
#include <gen_types.h>
#include "fftw3.h"
#include <Struct/list_rfftwnd_plan.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Global_Var/Vision.h>
#endif

/*#define USE_FFT_THREADS
 *impossible a faire tourner au linkage!?
 */

void function_init_fftw_complex(int Gpe)
{
#ifndef NO_FFT
    char *chaine = NULL, *info;
    int i, taillex, tailley;
    list_rfftwnd_plan *list_curseur = NULL, *list_curseur2 = NULL;
    bool is_new_fft = 1;
    bool isfirst_time = 0;
#ifdef DEBUG
    printf
        ("%s : utilisation de la lib fftw, disponible sur http://www.fftw.org en GPL\n",
         __FUNCTION__);
#endif
    /* recuperation du lien */
    for (i = 0; i < nbre_liaison; i++)
        if (liaison[i].arrivee == Gpe)
        {
            chaine = liaison[i].nom;
            break;
        }

    if (chaine == NULL)
    {
        printf("Gpe : %d : %s : pas de lien entrant...\n", Gpe, __FUNCTION__);
        exit(0);
    }
    /* recuperation des infos du lien */
    info = NULL;
    info = strstr(chaine, "-X");
    if (info != NULL)
        taillex = atoi(&info[2]);
    else
    {
        printf
            ("%s : il faut definir les dimensions des FFT (-X### -Y###)\n",
             __FUNCTION__);
        exit(0);
    }
    info = NULL;
    info = strstr(chaine, "-Y");
    if (info != NULL)
        tailley = atoi(&info[2]);
    else
    {
        printf
            ("%s : il faut definir les dimensions des FFT (-X### -Y###)\n",
             __FUNCTION__);
        exit(0);
    }
    if ((taillex < 1) || (tailley < 1))
    {
        printf
            ("%s : il faut definir les dimensions des FFT (-X### -Y###)\n",
             __FUNCTION__);
        exit(0);
    }
    else
    {
#ifdef DEBUG
        printf("%s : tailles des images : %d x %d\n", __FUNCTION__,
               taillex, tailley);
#endif
    }

    /*prise du mutex pour eviter que d'autres appels a init en  ne viennent modifier la liste chainee */
    /*utile aussi pour fftw qui ne peut faire les init de plan enc ontexte threade */
    pthread_mutex_lock(&mutex_list_prom_fft);
    /*curseur de parcours de la liste chainee */
    list_curseur = NULL;
    list_curseur2 = list_prom_fft;

    if (list_curseur == NULL && list_curseur2 == NULL)
        isfirst_time = 1;

    while (list_curseur2 != NULL)   /*parcours */
    {
        /*si une fft de meme taille a deja ete initialisee */
        if (list_curseur2->sx == taillex && list_curseur2->sy == tailley
            && list_curseur2->type == complex_data)
        {
            printf
                ("%s : il ne faut appeler qu'une fois la fonction d'initialisation pour une taille de fft donnee\n",
                 __FUNCTION__);
            is_new_fft = 0;
            break;
        }
        list_curseur = list_curseur2;
        list_curseur2 = list_curseur2->next_fft_plan;

    }
    if (is_new_fft)             /*si c'est une nouvelle taille de fft */
    {
#ifdef DEBUG
        printf("%s : initialisation d'une nouvelle taille de fft : %dx%d\n",
               __FUNCTION__, taillex, tailley);
#endif

        if (isfirst_time)
        {
            /*on cree le premier element de la liste chainee */
            list_curseur =
                (list_rfftwnd_plan *) malloc(sizeof(list_rfftwnd_plan));
            if (list_curseur == NULL)
            {
                printf("%s : %d : Allocation de memoire impossible\n",
                       __FUNCTION__, __LINE__);
                exit(0);
            }
            list_prom_fft = list_curseur;
        }
        else
        {
            /*creation d'une nouvelle structure */
            list_curseur->next_fft_plan =
                (list_rfftwnd_plan *) malloc(sizeof(list_rfftwnd_plan));
            if (list_curseur->next_fft_plan == NULL)
            {
                printf("%s : %d : Allocation de memoire impossible\n",
                       __FUNCTION__, __LINE__);
                exit(0);
            }
            list_curseur = list_curseur->next_fft_plan;
        }

        /*remplissage de la structure */
        list_curseur->sx = taillex;
        list_curseur->sy = tailley;
        list_curseur->type = complex_data;

        /* initialisation des pointeurs prom_fft et prom_fft_inv */

#ifdef USE_DOUBLE_PRECISION_FFT
        list_curseur->in_out_complex =
            fftw_malloc(sizeof(fftw_complex) * taillex * tailley);
        if (list_curseur->in_out_complex == NULL)
        {
            printf("erreur d'allocation d'un tableau de fft\n");
            exit(1);
        }
        list_curseur->prom_fft =
            fftw_plan_dft_2d(taillex, tailley, list_curseur->in_out_complex,
                             list_curseur->in_out_complex, FFTW_FORWARD,
                             FFTW_PATIENT);

        list_curseur->in_out_inv_complex = fftw_malloc(sizeof(fftw_complex) * taillex * tailley);   /*attention complexe */
        if (list_curseur->in_out_inv_complex == NULL)
        {
            printf("erreur d'allocation d'un tableau de fft\n");
            exit(1);
        }
        list_curseur->prom_fft_inv =
            fftw_plan_dft_2d(taillex, tailley,
                             list_curseur->in_out_inv_complex,
                             list_curseur->in_out_inv_complex, FFTW_BACKWARD,
                             FFTW_PATIENT);
#endif
#ifdef USE_FLOAT_PRECISION_FFT
        list_curseur->in_out_complex =
            fftwf_malloc(sizeof(fftwf_complex) * taillex * tailley);
        if (list_curseur->in_out_complex == NULL)
        {
            printf("erreur d'allocation d'un tableau de fft\n");
            exit(1);
        }
        list_curseur->prom_fft =
            fftwf_plan_dft_2d(taillex, tailley, list_curseur->in_out_complex,
                              list_curseur->in_out_complex, FFTW_FORWARD,
                              FFTW_PATIENT);

        list_curseur->in_out_inv_complex =
            fftwf_malloc(sizeof(fftwf_complex) * taillex * tailley);
        if (list_curseur->in_out_inv_complex == NULL)
        {
            printf("erreur d'allocation d'un tableau de fft\n");
            exit(1);
        }
        list_curseur->prom_fft_inv =
            fftwf_plan_dft_2d(taillex, tailley,
                              list_curseur->in_out_inv_complex,
                              list_curseur->in_out_inv_complex, FFTW_BACKWARD,
                              FFTW_PATIENT);
#endif

        /*init du mutex propre a chaque taille de fft */
        if (pthread_mutex_init(&(list_curseur->mutex_prom_fft), NULL))
        {
            printf("%s : %d : le mutex ne peut etre initialise...\n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }
        /*la nouvelle structure etant ajjoutee a la fin de la liste chainee */
        list_curseur->next_fft_plan = NULL;
    }

    pthread_mutex_unlock(&mutex_list_prom_fft);

    return;
#endif

}
