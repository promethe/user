/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_wta_ext_float.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: C'est un WTA sur des images float dans lequel la taille du voisinage de competition est indiquee sur le lien : plusieurs gangant dans l'image sont possibles sur differents voisinage

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

void function_wta_ext_float(int Gpe)
{
    /* olivier 02/04/2003 */
    /*     FILE * sortie; */
    /*     int pointeurim; */
    /*     char chemin[128]; */
    /*     float *surimage; */
    /* olivier 02/04/2003 */



    int i, j, centre, continuer, k, l, dimX, dimY, nx, ny,
        dim_wtaX = -1, dim_wtaY = -1, InputGpe = -1;
    float *image, *res;
    char *chaine = NULL, *ST;
    if (def_groupe[Gpe].ext == NULL)
    {
        /* recuperation du lien entrant */
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                chaine = liaison[i].nom;
                break;
            }
        if (InputGpe == -1)
        {
            printf("%s(%d) : erreur, pas de groupe amont \n", __FUNCTION__,
                   Gpe);
            exit(0);
        }
        /* recuperation de dim_wtaX et dim_wtaY */
        ST = NULL;
        ST = strstr(chaine, "-X");
        if (ST != NULL)
        {
            dim_wtaX = atoi(&ST[2]);
        }
        ST = NULL;
        ST = strstr(chaine, "-Y");
        if (ST != NULL)
        {
            dim_wtaY = atoi(&ST[2]);
        }
        if (dim_wtaX == -1 || dim_wtaY == -1)
        {
            printf("%s(%d) : erreur le lien doit comporter -x##-Y##\n",
                   __FUNCTION__, Gpe);
            exit(0);
        }
#ifdef DEBUG
        printf("%s(%d) : infos du lien : -x%d-Y%d\n", __FUNCTION__, Gpe,
               dim_wtaX, dim_wtaY);
#endif


        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s line:%d : erreur, ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }


        /* recuperation du pointeur sur les images d'entree */
        /* stocke dans images_table[1] */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->
            images_table[0];

        /* recuperation des infos des groupes precedents */
        nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band;
        if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
        {
            printf
                ("%s : votre image (de float) est couleur, cette fonction ne fait que le N&B\n",
                 __FUNCTION__);
            exit(0);
        }
#ifdef DEBUG
        printf
            ("%s(groupe %d): groupe en entree : %d , taille image : %dx%d\n",
             __FUNCTION__, Gpe, InputGpe, nx, ny);
#endif

        /* alloc de mem pour la sortie */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(nx * ny * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0] == NULL)
        {
            printf("%s line:%d : ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }

        /* stockage parametres */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            (unsigned char *) malloc(sizeof(int));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[2] == NULL)
        {
            printf("%s line:%d : ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            (unsigned char *) malloc(sizeof(int));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[3] == NULL)
        {
            printf("%s line:%d : ALLOCATION IMPOSSIBLE ...! \n",
                   __FUNCTION__, __LINE__);
            exit(0);
        }
        *((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            dim_wtaX;
        *((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            dim_wtaY;


    }
    else
    {
        nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        dim_wtaX =
            *((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2];
        dim_wtaY =
            *((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3];

    }

    image =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[1];
    res =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
/* partie calcul */

/* verification de la taille */
    dimX = dim_wtaX / 2;
    if ((2 * dimX + 1) != dim_wtaX)
        printf
            ("%s(%d) : attention dim_wtaX (%d) n'est pas impair, transforme en %d",
             __FUNCTION__, __LINE__, dim_wtaX, 2 * dimX + 1);
    dimY = dim_wtaY / 2;
    if ((2 * dimY + 1) != dim_wtaY)
        printf
            ("%s(%d) : attention dim_wtaY (%d) n'est pas impair, transforme en %d",
             __FUNCTION__, __LINE__, dim_wtaY, 2 * dimY + 1);
#ifdef DEBUG
    printf("dimX=%d,dimY=%d\n", dimX, dimY);
#endif
    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++)
        {
            /* 24/09/2003 Olivier Ledoux
               optim1 = i * ny + j;
             */
            centre = i + j * nx;

            continuer = 1;
            res[centre] = image[centre];
            for (k = -dimX; k <= dimX; k++)
            {
                if ((i + k >= 0) && (i + k < nx))
                {
                    for (l = -dimY; l <= dimY; l++)
                    {
                        if ((j + l >= 0) && (j + l < ny))
                        {
                            /* 24/09/2003 Olivier Ledoux
                               if (image[optim1] < image[optim1 + k * ny + l]) {
                             */
                            if (image[centre] < image[centre + k + nx * l])
                            {
                                res[centre] = 0.;
                                continuer = 0;
                            }
                        }
                        if (continuer == 0)
                            break;
                    }
                }
                if (continuer == 0)
                    break;
            }
        }
    /* non linearite */
    /*for (i = 0; i < nx * ny; i++)   
       res[i] = 0.64 * atan(res[i]); *//*mise en commentaire le 03/04/2003 pour comparer mes resultats avec ceux d'olivier *//* constante pour ??? */


    /* affichage pour debug */
    /*float val_max=-100.;
       float val_min=100.;
       for(i=0;i<nx*ny;i++)
       {
       if(res[i] > val_max)
       val_max = res[i];
       else
       {
       if(res[i] < val_min)
       val_min = res[i];
       }
       }
       printf("%f < im2 < %f\n",val_min,val_max);
       for(i=0;i<nx*ny;i++)
       {
       res[i] = (res[i] - val_min)/(val_max - val_min);
       }



       affichage2(2,res,nx,ny,0,0);
       getchar(); */


    /* Olivier 02/04/2003 */
    /*printf ("debut groupe %d \n",Gpe);
       surimage=(float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0];
       sprintf(chemin,"wta_box_%3.3d",Gpe);
       sortie=fopen (chemin,"w");
       for (pointeurim=1;pointeurim<nx*ny+1;pointeurim++)
       26/09/2003 Olivier Ledoux



       for (pointeurim=0;pointeurim<nx*ny;pointeurim++)
       {
       fprintf (sortie,"%f \n",surimage[pointeurim]);
       }
       fclose (sortie); */
    /* Olivier 02/04/2003 */

}
