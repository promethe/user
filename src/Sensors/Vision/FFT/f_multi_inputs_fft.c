/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_multi_inputs_fft.c
 \brief xxxxxxxxxxxxxxxxxx

 Author: M. Maillard
 Created: 2002
 Modified:
 - author: M. Maillard
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:

 Description:
 xxxxxxxx

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Macro:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: none
 - description: none
 - input expected group: Image of real point
 - where are the data?: in the image to convert

 Comments: xxxxxxxxxxxxxxxx

 Known bugs: none (yet!)

 Todo:	see the author to comment the file.


 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>

void function_multi_inputs_fft(int Gpe)
{
  int InputGpe1 = -1, InputGpe2 = -1, i, nx, ny, j, ij;
  float *input_tab1, *input_tab2, *output_tab;
  typedef struct 
  {
    float re, im;
  } fft_complex;

  fft_complex *A, *B, *C;

#ifdef TIME_TRACE
  struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
  long SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace;
  char MessageFunctionTimeTrace[255];
#endif

#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  if (def_groupe[Gpe].ext == NULL)
  {
    dprints("%s : attention fftw ne respecte pas le format d'image de promethee, voir doc, http://www.fftw.org\n", __FUNCTION__);

    /* recuperation des liens entrants */
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == Gpe)
      {
        if (InputGpe1 == -1)
        {
          InputGpe1 = liaison[i].depart;
        }
        else
        {
          if (InputGpe2 == -1)
          {
            InputGpe2 = liaison[i].depart;
          }
          else
          {
            EXIT_ON_ERROR("%s:(groupe %d) : erreur, plus de deux groupes en entree\n", __FUNCTION__, Gpe);
          }
        }
      }
    if (InputGpe1 == -1 || InputGpe2 == -1)
    {
      EXIT_ON_ERROR("%s:(groupe %d) : erreur, pas assez de groupes amonts \n", __FUNCTION__, Gpe);
    }

    if (def_groupe[InputGpe1].ext == NULL || def_groupe[InputGpe2].ext == NULL)
    {
      printf("%s (Gpe %d ) : le pointeur ext d'un des groupes amonts est NULL...\n", __FUNCTION__, Gpe);
      return;
    }

    /* allocation de memoire */
    def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[Gpe].ext == NULL)
    {
      EXIT_ON_ERROR("%s:%d : erreur, ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__, __LINE__);
    }

    /* recuperation du pointeur sur les images d'entree */
    /* stocke dans images_table[1] et [2] */
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] = ((prom_images_struct *) def_groupe[InputGpe1].ext)->images_table[0];
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] = ((prom_images_struct *) def_groupe[InputGpe2].ext)->images_table[0];
    /* recuperation des infos des groupes precedents */
    nx = ((prom_images_struct *) def_groupe[InputGpe1].ext)->sx;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
    ny = ((prom_images_struct *) def_groupe[InputGpe1].ext)->sy;
    ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
    ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
    ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = ((prom_images_struct *) def_groupe[InputGpe1].ext)->nb_band;
    if (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band != 4)
    {
      EXIT_ON_ERROR("%s : %d : votre fft est couleur, cette fonction ne fait que le N&B\n", __FUNCTION__, __LINE__);
    }
    /* verification du deuxieme groupe */
    if (nx != ((prom_images_struct *) def_groupe[InputGpe2].ext)->sx || ny != ((prom_images_struct *) def_groupe[InputGpe2].ext)->sy)
    {
      EXIT_ON_ERROR("%s:(groupe %d) : erreur, tailles des fft differentes (groupe %d : %dx%d) et (groupe %d : %dx%d)\n", __FUNCTION__, Gpe, InputGpe1, nx, ny, InputGpe2, ((prom_images_struct *) def_groupe[InputGpe2].ext)->sx, ((prom_images_struct *) def_groupe[InputGpe2].ext)->sy);
    }
    dprints
    ("%s(groupe %d): groupes en entree : %d et %d, taille image : %dx%d\n",__FUNCTION__, Gpe, InputGpe1, InputGpe2, nx, ny);

    /* alloc de mem pour la sortie */
    /* okay, la taille allouee ne correspond pas
     au sx,sy de prom_images_struct. Ceci s'explique
     par le format utilise par fftw. Les "images" dans
     le plan reel ont une bande vide sur la gauche
     pour permettre de stocker la fft sur la meme zone
     de memoire, or la fft est plus grande.
     pour de plus amples infos, voir la doc, addresse indique
     en haut de la fonction */
    ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = (unsigned char *) malloc(nx * (ny / 2 + 1) * 2 * sizeof(float));
    if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
    {
      EXIT_ON_ERROR("%s:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__, __LINE__);
    }

  }
  else
  {
    nx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
  }

  input_tab1 = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1];
  input_tab2 = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2];
  output_tab = (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0];

  C = (fft_complex *) output_tab;
  A = (fft_complex *) input_tab1;
  B = (fft_complex *) input_tab2;

  /*on peut modifier pour avoir C=A*B */
  for (i = 0; i < nx; i++)
    for (j = 0; j < ny / 2 + 1; j++)
    {
      ij = i * (ny / 2 + 1) + j;
      C[ij].re = A[ij].re * B[ij].re - A[ij].im * B[ij].im;
      C[ij].im = A[ij].re * B[ij].im + A[ij].im * B[ij].re;
    }

  /* une convulution dans le plan x,y
   devient une multiplication en fourier f1,f2 */
#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace =OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace =OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -1;
    MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace, "Time in function multi_inputs_fft (Gpe %d) image %dx%d : \t%4ld.%06d\n",Gpe, nx, ny, SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  affiche_message(MessageFunctionTimeTrace);
#endif

}
