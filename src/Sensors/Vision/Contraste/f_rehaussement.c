/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/cadrage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <public_tools/Vision.h>


#define DEBUG

typedef struct MyData {
  int gpe_image;
  int nbre_colonne;
  int nbre_ligne;
  int taille_masque;
}MyData;

void function_rehaussement(int gpe){
  
  MyData *mydata=NULL;
  prom_images_struct * output_image=NULL;
  prom_images_struct *input_image=NULL;
  float I_moy=0;
  float Xo=0;	
  int i=0;
  int j=0;
  int k=0;
  int z=0;
  int taille_masque;
  char param[255];
  int nbre_colonne;	   /*la dimension de l'image en x*/
  int nbre_ligne;         /*la dimension de l'image en y*/
  int decalage;
  int cpt;
  int compteur_generale;
  
  /*Premier passage dans la fonction*/
  
#ifdef DEBUG
  printf("on rentre dans la fonction de rehaussement de contraste\n");
#endif
  
  
#ifdef DEBUG
  printf("on est juste avant le passage des arguments\n");
#endif
  
  if(def_groupe[gpe].data == NULL)
    {
      mydata=(MyData *)malloc(sizeof(MyData));
      if(mydata==NULL)
      {
        printf("pb malloc pour mydata");
        exit(0);
      }
      /*recuperation des parametres d'entree*/
      
      while((j=find_input_link(gpe,i))!=-1)
      {
        if(prom_getopt(liaison[j].nom,"-taille",param)==2)
        {
          mydata->gpe_image=liaison[j].depart;
          mydata->taille_masque=atoi(param);
          #ifdef DEBUG
          printf("groupe=%d,taille_masque=%d",liaison[j].depart,mydata->taille_masque);
          #endif
        }
      
      i++;
      
#ifdef DEBUG
	printf("on a fini de recuperer les parametres\n");
#endif
	
    }
      
    input_image=((prom_images_struct *)(def_groupe[mydata->gpe_image].ext));
    printf("pointeur imput_image=%p",(void *)input_image);
    output_image=calloc_prom_image(1,input_image->sx,input_image->sy,1);
    def_groupe[gpe].ext=output_image;  /*sauvegarde de l'image*/
    def_groupe[gpe].data=mydata;	 /*sauvegarde de Mydata*/
    
#ifdef DEBUG
      printf("on a fini l allocation de la premiere image\n");
#endif
  
    }

  else
    {	
      mydata = def_groupe[gpe].data;
      input_image=((prom_images_struct *)(def_groupe[mydata->gpe_image].ext));
      output_image=((prom_images_struct *)(def_groupe[gpe].ext));		
#ifdef DEBUG
      printf("on a fini l allocation d une image autre que la premiere\n");
#endif
      
    }

  
#ifdef DEBUG
  printf("on est avant la boucle\n");
#endif
  
  /******************************************************/
  /**                Rehaussement                      **/
  /******************************************************/

  nbre_colonne=input_image->sx;
  nbre_ligne=input_image->sy;

#ifdef DEBUG
  printf("on est avant le X\n");
  printf("taille en X=%d\n",input_image->sx);
  printf("taille en Y=%d\n",input_image->sy);
#endif
  
  
  
#ifdef DEBUG
  printf("on est avant la boucle\n");
#endif
  
  k=0;
  taille_masque=mydata->taille_masque;
  decalage=(taille_masque - 1) / 2;
  cpt=compteur_generale=0;
  for (i = 0; i < nbre_colonne; i++)
  {
    for (j = 0; j < nbre_ligne; j++)
    {
      cpt=0;
      for( z = 0 ; z < taille_masque * taille_masque ; z++)
      {
        if(z%taille_masque == 0 && z!=0)
        {
          k++;
        }
	      if((i - decalage + z%taille_masque)<0 || (i - decalage + z%taille_masque)>=nbre_colonne 
              || (j - decalage + k)<0 || (j - decalage + k)>=nbre_ligne )
        {
          compteur_generale++;
          cpt++;
          /*printf("i=%d,j=%d,horsI=%d,horsJ=%d\n",i,j,i - decalage + z%taille_masque,j - decalage + k);*/
        }
	      else
        {
          I_moy =I_moy + input_image->images_table[0][(i - decalage + z%taille_masque) + (j - decalage + k ) * nbre_colonne ];
        }
	    }
	  
	  I_moy = I_moy /  (taille_masque * taille_masque - cpt) ;
	  Xo = 0.1 + ( 410 * I_moy) / ( 105.5 + I_moy );
	  output_image->images_table[0][j*nbre_colonne+i] = (( 255 + Xo ) * input_image->images_table[0][j*nbre_colonne+i]) / ( Xo +  input_image->images_table[0][j*nbre_colonne+i] );  
	  k=0;
	  I_moy=0;
	}
} 
#ifdef DEBUG
  printf("taille en X=%d\n",input_image->sx);
  printf("taille en Y=%d\n",input_image->sy);
  printf("compteur=%d\n",compteur_generale);
  printf("decalage=%d\n",decalage);
#endif
#ifdef DEBUG
  printf("===============fin de la fonction rehaussement=======================)\n");
#endif
  
  
}
