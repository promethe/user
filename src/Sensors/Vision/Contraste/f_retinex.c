/* #include <stdlib.h> */

/* #include <math.h> */
/* #include <string.h> */
/* #include <sys/time.h> */
/* #include <stdio.h> */

/* #include <Struct/prom_images_struct.h> */

/* #include <Kernel_Function/prom_getopt.h> */
/* #include <Kernel_Function/find_input_link.h> */
/* #include <opencv/cxcore.h> */
/* #include <opencv/cv.h> */
/* #include <libx.h> */
/* #include <public_tools/Vision.h> */

/* #define LUT_SIZE 256 */

/* typedef struct s_retinex */
/* { */
/*   int		gpe_in; */
/*   int		depth; */
/*   float		gain; */
/*   IplImage	*src; */
/*   IplImage	*dst; */
/*   IplImage	*in; */
/*   IplImage	*out; */
/*   IplImage	**pyr; */
/*   IplImage	*L; */
/*   IplImage	*L_log; */
/*   IplImage	*I_log; */
/*   IplImage	*R_log; */
/*   IplImage	*sum; */
/*   IplImage	*LOG_LUT; */
/* }		t_retinex; */

/* void new_retinex(int gpe) */
/* { */
/*   t_retinex *retinex = ALLOCATION(t_retinex); */
  
/*   int i, j; */
/*   char param[255]; */

/*   retinex->gpe_in = -1; */
/*   retinex->depth = -1; */
/*   retinex->gain = -1; */

/*   for (i = 0; (j = find_input_link(gpe, i)) != -1; i++) */
/*     { */
/*       if(prom_getopt(liaison[j].nom,"-gain",param) == 2) */
/*         { */
/*           retinex->gpe_in = liaison[j].depart; */
/* 	  retinex->gain = atof(param); */
/*         } */
/*     } */

/*   if (retinex->gpe_in == -1) */
/*     { */
/*       EXIT_ON_GROUP_ERROR(gpe, "input group was not found"); */
/*     } */

/*   if (retinex->gain == -1) */
/*     { */
/*       EXIT_ON_GROUP_ERROR(gpe, "gain was not specified"); */
/*     } */

/*   def_groupe[gpe].data = retinex; */
/*   def_groupe[gpe].ext = NULL; */
/* } */

/* void function_retinex(int gpe) */
/* { */
/*   int k; */
/*   t_retinex *retinex = def_groupe[gpe].data; */
/*   prom_images_struct *in = def_groupe[retinex->gpe_in].ext; */
  
/*   assert(in != NULL); */
/*   assert(in->image_number == 1); */
/*   assert(in->nb_band == 1); */

/*   if (def_groupe[gpe].ext == NULL) */
/*     { */
/*       prom_images_struct *out = NULL;  */
/*       int sx = in->sx; */
/*       int sy = in->sy; */
/*       int n = sx * sy; */

/*       out = calloc_prom_image(1, in->sx, in->sy, in->nb_band); */
/*       def_groupe[gpe].ext = out; */

/*       retinex->in = cvCreateImageHeader(cvSize(in->sx, in->sy), IPL_DEPTH_8U, 1); */
/*       retinex->in->imageData = in->images_table[0]; */

/*       retinex->out = cvCreateImageHeader(cvSize(in->sx, in->sy), IPL_DEPTH_8U, 1); */
/*       retinex->out->imageData = out->images_table[0]; */
      
/*       retinex->L = cvCreateImage(cvSize(in->sx, in->sy), IPL_DEPTH_8U, 1); */
/*       retinex->L_log = cvCreateImage(cvSize(in->sx, in->sy), IPL_DEPTH_32F, 1); */
/*       retinex->I_log = cvCreateImage(cvSize(in->sx, in->sy), IPL_DEPTH_32F, 1); */
/*       retinex->R_log = cvCreateImage(cvSize(in->sx, in->sy), IPL_DEPTH_32F, 1); */
/*       retinex->sum = cvCreateImage(cvSize(in->sx, in->sy), IPL_DEPTH_32F, 1); */

/*       retinex->LOG_LUT = cvCreateImage(cvSize(LUT_SIZE, 1), IPL_DEPTH_32F, 1); */
/*       float *log_data = retinex->LOG_LUT->imageData; */
      
/*       log_data[0] = 0; */
/*       for (k = 1; k < LUT_SIZE; k++) */
/* 	{ */
/* 	  log_data[k] = logf((float)k); */
/* 	} */

/*       double lx = log2(in->sx); */
/*       double ly = log2(in->sy); */
/*       double lm = lx < ly ? lx : ly; */
/*       int depth = (int)lm + 1; */
      
/*       retinex->depth = depth; */
/*       retinex->pyr = malloc(retinex->depth * sizeof(IplImage *)); */
/*       for (k = 0; k < retinex->depth; k++) */
/* 	{ */
/* 	  if (sx == 0 || sy == 0) */
/* 	    { */
/* 	      EXIT_ON_GROUP_ERROR(gpe, "depth is to high"); */
/* 	    } */
/* 	  retinex->pyr[k] = cvCreateImage(cvSize(sx, sy), IPL_DEPTH_8U, 1); */

/* 	  sx /= 2; */
/* 	  sy /= 2; */
/* 	} */
/*     } */


/*   cvLUT(retinex->in, retinex->I_log, retinex->LOG_LUT); */
/*   cvCopy(retinex->in, retinex->pyr[0], NULL); */
  
/*   const float w = 1.0/(float)(retinex->depth - 1); */
/*   cvZero(retinex->sum); */
/*   for (k = 1; k < retinex->depth; k++) */
/*     { */
/*       cvPyrDown(retinex->pyr[k - 1], retinex->pyr[k], CV_GAUSSIAN_5x5); */
/*       cvResize(retinex->pyr[k], retinex->L, CV_INTER_LINEAR); */

/*       cvLUT(retinex->L, retinex->L_log, retinex->LOG_LUT); */

/*       cvSub(retinex->I_log, retinex->L_log, retinex->R_log, NULL); */
/*       cvAddWeighted(retinex->sum, 1, retinex->R_log, w, 0, retinex->sum); */
/*     } */

/*   double min, max; */
/*   CvScalar mean, std_dev; */

/*   cvAvgSdv(retinex->sum, &mean, &std_dev, NULL); */
/*   max = mean.val[0] + std_dev.val[0]*retinex->gain; */
/*   min = mean.val[0] - std_dev.val[0]*retinex->gain; */

/*   double range = max - min; */
/*   double scale = 255.0/range; */
/*   double shift = -(min*scale); */

/*   cvConvertScale(retinex->sum, retinex->out, scale, shift); */
/* } */


/* void destroy_retinex(int gpe) */
/* { */
/*   t_retinex *retinex = def_groupe[gpe].data; */
/*   int	k; */

/*   cvReleaseImageHeader(&retinex->in); */
/*   cvReleaseImageHeader(&retinex->out); */
/*   for (k = 0; k < retinex->depth; k++) */
/*     cvReleaseImage(&retinex->pyr[k]); */
/*   cvReleaseImage(&retinex->L); */
/*   cvReleaseImage(&retinex->L_log); */
/*   cvReleaseImage(&retinex->I_log); */
/*   cvReleaseImage(&retinex->R_log); */
/*   cvReleaseImage(&retinex->sum); */
/*   cvReleaseImage(&retinex->LOG_LUT); */

/*   free_prom_image(def_groupe[gpe].ext); */

/*   free(retinex); */
/* } */
