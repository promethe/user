/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** 
\file 
\brief application d'un logarithme sur une image NB (.ext)

Author: Ali Karaouzene
Created: 23/02/2012
- description: specific file creation

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Application d'un logarithme avec normalisation. 
-
I(x,y) = log (I(x,y) - I_min +1) / log (I_max - I_min +1)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include "net_message_debug_dist.h"

/*#define TIME_TRACE*/
/*#define DEBUG*/
typedef struct data_log_ext
{
   int deb_gpe;
   int input_gpe;
   int sx;
   int sy;
   float * t_log;
} Data_log_ext;



void function_log_ext(int Gpe)
{

   int i,l;
   int sx, sy;
   int input_gpe=-1;
   Data_log_ext *this_log_ext_data;
   prom_images_struct *input_img=NULL;
   prom_images_struct *gpe_img=NULL;
   float n, den = 0.;
   unsigned char vmin = 255,vmax = 0;
   unsigned char *im_in = NULL;
   unsigned char * im_out;
   float *t_log = NULL;
#ifdef TIME_TRACE
   struct timeval debut, fin, diff;
   gettimeofday(&debut, NULL);
#endif
   dprints("entree dans fonction %s %s \n", __FUNCTION__,def_groupe[Gpe].no_name);

   if (def_groupe[Gpe].ext == NULL)
   {

      i = 0;
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         if(strstr(liaison[l].nom, "sync") != NULL)
         {; }
         else
            input_gpe = liaison[l].depart;
         i++;
         l = find_input_link(Gpe, i);
      }
      if ((l = find_input_link(Gpe, 0)) == -1)
         EXIT_ON_ERROR("Erreur de lien entrant dans %s : gpe %s\n",__FUNCTION__,def_groupe[Gpe].no_name);


      /* recuperation de l'image precedente */
      input_img = (prom_images_struct *) def_groupe[input_gpe].ext;

      if (input_img== NULL)
      {
         PRINT_WARNING("Erreur (%s) : il n'y a pas d'image dans le gpe d'entree %s\n", def_groupe[Gpe].no_name,def_groupe[input_gpe].no_name);
         return ;
      }

      if (input_img->nb_band != 1) EXIT_ON_ERROR("(%s) : L'image en entree doit etre en NB\n",def_groupe[Gpe].no_name);

      /* recuperation des infos sur la taille */
      sx  = input_img->sx;
      sy  = input_img->sy;
      n   = sx*sy;
      /* allocation de memoire */
      gpe_img = (void *)calloc_prom_image(input_img->image_number, input_img->sx, input_img->sy, 1);
      gpe_img->images_table[0] =(unsigned char *) calloc(n, sizeof(char));

      if (gpe_img == NULL) EXIT_ON_ERROR("(%s) ALLOCATION IMPOSSIBLE ...! \n",def_groupe[Gpe].no_name);

      this_log_ext_data = (Data_log_ext *) malloc(sizeof(Data_log_ext));
      if ( this_log_ext_data == NULL)    EXIT_ON_ERROR("\nerror in memory allocation in (%s) at [%s]",def_groupe[Gpe].no_name,__LINE__);

      /*create a lookup for log */
      t_log = calloc (257,sizeof(float));
      t_log[0] = NAN;
      for (i = 1; i <= 256;i++)
         t_log[i] = log (i);
      this_log_ext_data->input_gpe = input_gpe;
      this_log_ext_data->sx = sx;
      this_log_ext_data->sy = sy;
      this_log_ext_data->t_log = t_log;
      def_groupe[Gpe].data = this_log_ext_data;
      def_groupe[Gpe].ext = gpe_img;
   }
   else
   {
      this_log_ext_data = (Data_log_ext *) def_groupe[Gpe].data;
      input_gpe = this_log_ext_data->input_gpe;
      input_img = (prom_images_struct *) def_groupe[input_gpe].ext;
      gpe_img = (prom_images_struct *) def_groupe[Gpe].ext;
      sx = this_log_ext_data->sx;
      sy = this_log_ext_data->sy;
      t_log = this_log_ext_data->t_log;


   }
   im_in = (unsigned char * ) input_img->images_table[0];
   im_out = (unsigned char*) gpe_img->images_table[0];
   /*recuperer min et max*/
   //im_in = (float * ) input_img->images_table[0];
   for (i = sx*sy;i--;)
   {
      if (im_in[i]<vmin )         vmin = im_in[i];
      else if (im_in[i] > vmax )  vmax = im_in[i];
   }
   /*Données mises a l'echelle  log*/
   /*vmin = 0;
	vmax = 255;*/
   den = 255./t_log [vmax-vmin+1];
   for (i = sx*sy;i--;){
      if(im_in[i] - vmin > 0) im_out[i] =(unsigned char) (den * t_log[im_in[i] - vmin +1 ]); /*(255./log(max-min+1))*log(I-min+1)*/
      else im_out[i] = 0;
   }

#ifdef TIME_TRACE
   gettimeofday(&fin, NULL);
   timersub(&fin, &debut, &diff);
   printf ( "Temps d'execution : %fs\n",  (float)( (float)diff.tv_sec + (float)( (float)diff.tv_usec / (float)1000000 )));
#endif
}

void destroy_log_ext(int Gpe)
{
   dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
   if(def_groupe[Gpe].data != NULL)
   {
      free(def_groupe[Gpe].data);
      def_groupe[Gpe].data = NULL;
   }
   if(def_groupe[Gpe].ext != NULL)
   {
      free(def_groupe[Gpe].ext);
      def_groupe[Gpe].ext = NULL;
   }
   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}
