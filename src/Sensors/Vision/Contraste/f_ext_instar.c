/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief application d'un instar sur une image NB (.ext) (d'apres le modele de Grossberg)

Author: Antoine de Rengerve
Created: 01/04/2004
Modified:
- author: A. de Rengerve
- description: specific file creation
- date: 19/05/2011


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Calcul d'une normalisation des images d'apres le modele Instar de Grossberg. L'image d'entree doit etre en niveau de gris.

eps*d(ni)/dt = -Ani + (b - ni)*pi -ni *sum(pj)(j!=i)

Si A=1 :
A l'equilibre
ni =((bP)/(1+P))*qi

P=sum(pj)(quelquesoit j) !! c'est bien sur tous les pj entrants
et qi=pj/P

L'activite total de la carte vaut ((bP)/(1+P))<=b

b represente le max de la sum des activites de la carte desire
ma represente correspond a cette activite divise par le nombre de neurones de la carte : donc
ramene a un neurone (donc b=sx*sy*ma).
ATTENTION : ce n'est pas la meme chose que le max d'un neurone de la carte.
en fait c'est (bp)/(A+p) ici.

Le nombre de neurones du groupe doit correspondre a la taille du groupe entrant.

Parametres : -A down (=1 par defaut)
       -B up (=sx*sy par defaut)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include "net_message_debug_dist.h"


#undef DEBUG
struct ext_instar_data
{
    int deb_gpe;
    int input_gpe;
    int sx;
    int sy;
    float up;
    float cut;
    float down;
};



void function_ext_instar(int Gpe)
{
  int lien, i, j, k, l;
  float f_sum = 0., f_norm = 0.;
  int sx, sy;
  float up = 1.,cut=-1.;          /*par defaut on considere que c'est 1 pour faire des sorties de type neurone */
  float down,tmp;
  char *param = NULL, *chaine = NULL;
  int deb_gpe = -1, deb_gpe_input = -1, input_gpe=-1;
  struct ext_instar_data *this_instar_gpe_data;
  prom_images_struct *input_img=NULL;
  prom_images_struct *gpe_img=NULL;
  float n;
  
  (void) deb_gpe; // (unused)
  (void) deb_gpe_input; // (unused)


  dprints("entree dans fonction ext_instar %s \n", def_groupe[Gpe].no_name);

  if (def_groupe[Gpe].ext == NULL)
  {
    if ((lien = find_input_link(Gpe, 0)) == -1)
    {
      printf("Erreur de lien entrant dans function_instar_gpe : gpe %s\n",def_groupe[Gpe].no_name);
      exit(1);
    }

    chaine = liaison[lien].nom;
    if ((param = strstr(chaine, "-A")) != NULL)
      down = (float) atof(&param[2]);
    else
      down = 1.;

    input_gpe = liaison[lien].depart;

    /* recuperation de l'image precedente */
    input_img = (prom_images_struct *) def_groupe[input_gpe].ext;

    if (input_img== NULL)
    {
      printf("Erreur (%s) : il n'y a pas d'image dans le gpe d'entree %s\n", def_groupe[Gpe].no_name,def_groupe[input_gpe].no_name);
      return ;
    }

    if (input_img->nb_band != 1) EXIT_ON_ERROR("(%s) : L'image en entree doit etre en NB\n",def_groupe[Gpe].no_name);

    /* recuperation des infos sur la taille */
    sx  = input_img->sx;
    sy  = input_img->sy;
    n   = sx*sy;
    /* allocation de memoire */
    gpe_img = (void *)calloc_prom_image(input_img->image_number, input_img->sx, input_img->sy, 1);
    gpe_img->images_table[1] =(unsigned char *) calloc(n, sizeof(char));
    
    if (gpe_img == NULL) EXIT_ON_ERROR("(%s) ALLOCATION IMPOSSIBLE ...! \n",def_groupe[Gpe].no_name);

    def_groupe[Gpe].ext = gpe_img;

    if ((param = strstr(chaine, "-B")) != NULL)
    {
    up = (float) atof(&param[2]);
    /*printf("init up=%f \n",up);*/
    }
    else
      up = (float) sx *(float) sy;
    /*comme vu dans les explication on il faut up par rapport a la carte */


    if ((param = strstr(chaine, "-T")) != NULL)
    {
      cut = (float) atof(&param[2]);
      /*printf("init cut=%f \n",cut);*/
    }
    else
      cut = 255;


    deb_gpe = def_groupe[Gpe].premier_ele;

    def_groupe[Gpe].data = malloc(sizeof(struct ext_instar_data));
    this_instar_gpe_data = (struct ext_instar_data *) def_groupe[Gpe].data;
    this_instar_gpe_data->deb_gpe = deb_gpe;
    this_instar_gpe_data->input_gpe = input_gpe;
    this_instar_gpe_data->sx = sx;
    this_instar_gpe_data->sy = sy;
    this_instar_gpe_data->up = up;
    this_instar_gpe_data->cut = cut;
    this_instar_gpe_data->down = down;

    if ((lien = find_input_link(Gpe, 1)) != -1)
    {
      printf("Erreur : la fonction instar_gpe (groupe %s) ne prend qu'un lien algo -Axxx-Byyy en entree \n",def_groupe[Gpe].no_name);
      exit(1);
    }
  }
  else
  {
    this_instar_gpe_data = (struct ext_instar_data *) def_groupe[Gpe].data;
    deb_gpe = this_instar_gpe_data->deb_gpe;
    input_gpe = this_instar_gpe_data->input_gpe;
    deb_gpe_input = def_groupe[input_gpe].premier_ele;
    input_img = (prom_images_struct *) def_groupe[input_gpe].ext;
    gpe_img = (prom_images_struct *) def_groupe[Gpe].ext;
    sx = this_instar_gpe_data->sx;
    sy = this_instar_gpe_data->sy;
    up = this_instar_gpe_data->up;
    cut = this_instar_gpe_data->cut;
    down = this_instar_gpe_data->down;
  }

  /*Données mises a l'echelle  log*/
    for (i = 0; i < sy; i++)
      for (j = 0; j < sx; j++) {
        gpe_img->images_table[1][i*sx+j] =(unsigned char) ( log((float)(input_img->images_table[0][i*sx+j]+1)) * 255./ log (10000.));
      }
  
/*  ******************************/
  for (i = 0; i < sy; i++)
      for (j = 0; j < sx; j++) {
       f_sum += ((float) (int) (input_img->images_table[0][i*sx+j]))/255.;
       /*f_sum += ((float) (int) (gpe_img->images_table[1][i*sx+j]));*/
      }

  if (f_sum < 0.000001)       /*cas ou les entrees sont nulles */
  {
      for (k = 0; k < sy; k++)
          for (l = 0; l < sx; l++)
          {
            gpe_img->images_table[0][k*sx+l] = 0;
          }
  }
  else
  {
    f_norm = (up * f_sum) / (down + f_sum);
      printf("f_norm = %f , f_sum=%f, up =%f, down=%f \n",f_norm,  f_sum, up, down);
    for (k = 0; k < sy; k++)
      for (l = 0; l < sx; l++) {
/*          printf(" Val_init [%d,%d] : %d\n",i,j,input_img->images_table[0][k*sx+l]);*/
      tmp = f_norm * ((float)(input_img->images_table[0][k*sx+l])) / f_sum;
      /*tmp = f_norm * ((float)(gpe_img->images_table[1][k*sx+l])) / f_sum;*/
      if(tmp > cut)
        tmp=cut;
      gpe_img->images_table[0][k*sx+l]=(unsigned char) tmp;
/*        printf(" Val [%d,%d] : %f / %d\n",k,l,tmp, gpe_img->images_table[0][k*sx+l]);*/
      }
  }

}

void destroy_ext_instar(int Gpe)
{
  dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
  free(def_groupe[Gpe].data);
  free(def_groupe[Gpe].ext);
  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}
