/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  Compute_Keypoint_Descriptor_At_XY.c
 \brief

 Author: Mickael Maillard
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 23/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <math.h>
#include <Struct/prom_images_struct.h>

int Compute_Keypoint_Descriptor_At_XY(float KeyOrientation_360, float *inputImagesG, float *inputImagesO, void *ext, int rowK, int columnK, int sx, int sy)
{
  int i, j, k, localrow, localcolumn, RotLocalColumn, RotLocalRow;

  int radius, radiussqr;
  int SelectedHistoRow, SelectedHistoColumn, OrientIndex;
  float HistoArray[2][2][8];
  float KeyOrientation, pixOrientation, GradValue;
  float sigma, sigmasqr, coeff;

  float Mean, StdDev;
  /*Caracterisation* caract;

   caract =(Caracterisation*)caracte; */

  radius = (int) (8 * sqrt((double) 2));
  radiussqr = radius * radius;

  sigma = (float) 8;
  sigmasqr = sigma * sigma;
  coeff = (float) (1 / (2 * (float) M_PI * sigmasqr));

  if (rowK < radius || rowK > (sy - radius) || columnK < radius || columnK > (sx - radius))
  {
#ifdef DEBUG
    printf("keypoint rejected\n");
#endif
    return 0;
  }

  KeyOrientation = (2 * (float) M_PI) * (KeyOrientation_360 / 360);

  for (i = 0; i < 2; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 8; k++)
        HistoArray[i][j][k] = 0;

  for (localrow = -radius; localrow <= radius; localrow++)
  {
    for (localcolumn = -radius; localcolumn <= radius; localcolumn++)
    {
      if ((localrow * localrow + localcolumn * localcolumn) <= radiussqr) /*si on est bien dans le cercle circonscrit a la fenetre du descripteur */
      {
        RotLocalColumn = (int) (cos(KeyOrientation) * localcolumn + sin(KeyOrientation) * localrow);
        RotLocalRow = (int) (cos(KeyOrientation) * localrow - sin(KeyOrientation) * localcolumn);

        if (RotLocalRow > -8 && RotLocalRow <= 8 && RotLocalColumn > -8 && RotLocalColumn <= 8) /*si appartient au carre apres rotation */
        {
          pixOrientation = *(inputImagesO + (rowK + localrow) * sx + columnK + localcolumn) - KeyOrientation_360;
          if (pixOrientation < 0) pixOrientation = 360 + pixOrientation;
          if (pixOrientation >= 360) pixOrientation = pixOrientation - 360;

          OrientIndex = (int) ((pixOrientation / 360) * 8);
          GradValue = *(inputImagesG + (rowK + localrow) * sx + columnK + localcolumn);
          GradValue = GradValue * coeff * exp(-(localrow * localrow + localcolumn * localcolumn) / (2.0 * sigmasqr));
          SelectedHistoRow = RotLocalRow + 7;
          SelectedHistoColumn = RotLocalColumn + 7;

          SelectedHistoRow = (int) (SelectedHistoRow / 8);
          SelectedHistoColumn = (int) (SelectedHistoColumn / 8);

          /*      printf("blabla %d %d %f\n",SelectedHistoRow,SelectedHistoColumn,GradValue); */

          HistoArray[SelectedHistoRow][SelectedHistoColumn][OrientIndex] = HistoArray[SelectedHistoRow][SelectedHistoColumn][OrientIndex] + GradValue;

        }
      }
    }
  } /*fin du for de parcours des pixels potientiels */
#ifdef DEBUG
  printf("fin des histos\n");
#endif
  for (i = 0; i < 2; i++)
    for (j = 0; j < 2; j++)
    {
      float X, Y, Rmoy, THETAmoy;
      /*      for(k=0;k<8;+k++)
       printf("%f\n",HistoArray[i][j][k]);
       */
      Mean = 0;
      StdDev = 0;

      Y = 0;
      X = 0;
      for (k = 0; k < 8; k++)
      {
        Y = Y + HistoArray[i][j][k] * sin((2. * (float) M_PI * k / 8.) + (2. * (float) M_PI / 16.));
        X = X + HistoArray[i][j][k] * cos((2. * (float) M_PI * k / 8.) + (2. * (float) M_PI / 16.));
      }
      Mean = atan2(Y, X);
      if (Mean < 0) Mean = 2. * (float) M_PI + Mean;

      Rmoy = (float) sqrt(X * X + Y * Y) / (float) 8;
      THETAmoy = Mean;

      for (k = 0; k < 8; k++)
        StdDev = StdDev + HistoArray[i][j][k] * HistoArray[i][j][k] + Rmoy * Rmoy - 2. * Rmoy * HistoArray[i][j][k] * cos(THETAmoy - ((2. * (float) M_PI * k / 8.) + (2. * (float) M_PI / 16.)));
      StdDev = StdDev / 8.;

      /*      printf("caracterisation effectuee en rowK %d columnK %d\n",rowK,columnK); */
      *((float *) (((prom_images_struct *) (ext))->images_table[i * 2 + j]) + rowK * sx + columnK) = Mean / (2 * (float) M_PI);
      /*printf("StdDev %f\n",StdDev); */
      *((float *) (((prom_images_struct *) (ext))->images_table[i * 2 + j + 4]) + rowK * sx + columnK) = StdDev;
    }

  return 1;
}
