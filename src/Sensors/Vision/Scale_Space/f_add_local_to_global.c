/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief 

Author: Maickael Maillard
Created: 01/04/2004
Modified:
- author: M. Maillard
- description: specific file creation
- date: 19/09/2005

Theoritical description:

Description: 
Le but de cette fonction est de gerer le deplacement de vue de la camera, pour pointer vers un objet ou une direction.
On recupere dans un premier champ de neurones, la position du neurone (max) qui contient l'information de la position où on veut que la camera pointe.
Après normalisation de la position du neurone max, pour etre independant de la longueur du champ de neurones, on calcule, à l'aide du pas recupere en parametre, la rotation a effectuer par la cam pour pointer sur la direction desiree.

Paramètres : -S, represente le pas (35/255 par défaut).
	     -proprio, represente la position angulaire du moteur.

Modifications :
Correction dans le cas ou c'est la 2eme liaison qui contient l'information du pas. On ne recuperait pas ce pas, mais c'etait la valeur par defaut qui etait prise.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/


#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

/*#undef DEBUG*/
/*#define DEBUG*/
struct add_local_to_global_data
{
    int deb_gpe;
    int deb_gpe_input;
    int longueur;
    float pas;
    int deb_gpe_proprio;

};

void function_add_local_to_global(int Gpe)
{
    int lien, i;

    int deb_gpe = -1, deb_gpe_input = -1;
    int input_gpe = -1;
    int deb_gpe_proprio;
    struct add_local_to_global_data *this_gpe_data;
    char *param = NULL, *chaine = NULL;
    int longueur;
    float pas;

    float max = 0.01;
    int max_pos;
    float new_val;
    float pos_float;

#ifdef DEBUG
    printf("entree dans fonction add_local_to_global Gpe %d \n", Gpe);
#endif
    if (def_groupe[Gpe].data == NULL)
    {
        if ((lien = find_input_link(Gpe, 0)) == -1)
        {
            printf
                ("Erreur de lien entrant dans function_add_local_to_global : gpe %d\n",
                 Gpe);
            exit(1);
        }
        chaine = liaison[lien].nom;
        if ((param = strstr(chaine, "-S")) != NULL)
            pas = (float) atof(&param[2]);
        else
            pas = 35. / 255.;

        input_gpe = liaison[lien].depart;

        if ((lien = find_input_link(Gpe, 1)) == -1)
        {
            printf
                ("Erreur de lien entrant dans function_add_local_to_global (2) : gpe %d\n",
                 Gpe);
            exit(1);
        }
        chaine = liaison[lien].nom;
        if ((param = strstr(chaine, "-proprio")) != NULL)
            deb_gpe_proprio = def_groupe[liaison[lien].depart].premier_ele;
        else
        {
            deb_gpe_proprio = def_groupe[input_gpe].premier_ele;
            input_gpe = liaison[lien].depart;
	    if ((param = strstr(chaine, "-S")) != NULL)
            	pas = (float) atof(&param[2]);
            else
            	pas = 35. / 255.;
        }

        printf("input_gpe %d\n", input_gpe);

        deb_gpe = def_groupe[Gpe].premier_ele;
        deb_gpe_input = def_groupe[input_gpe].premier_ele;
        longueur = def_groupe[input_gpe].nbre;

        def_groupe[Gpe].data =
            malloc(sizeof(struct add_local_to_global_data));
        this_gpe_data =
            (struct add_local_to_global_data *) def_groupe[Gpe].data;
        this_gpe_data->deb_gpe = deb_gpe;
        this_gpe_data->deb_gpe_input = deb_gpe_input;
        this_gpe_data->longueur = longueur;
        this_gpe_data->pas = pas;
        this_gpe_data->deb_gpe_proprio = deb_gpe_proprio;

    }
    else
    {
        this_gpe_data =
            (struct add_local_to_global_data *) def_groupe[Gpe].data;
        deb_gpe = this_gpe_data->deb_gpe;
        deb_gpe_input = this_gpe_data->deb_gpe_input;
        longueur = this_gpe_data->longueur;
        pas = this_gpe_data->pas;
        deb_gpe_proprio = this_gpe_data->deb_gpe_proprio;
    }

	/*recherche position max*/
    max_pos = longueur / 2;
    for (i = 0; i < longueur; i++)
    {
        if (neurone[i + deb_gpe_input].s1 > max)
        {
            max = neurone[i + deb_gpe_input].s1;
            max_pos = i;
        }
    }


	/*norm pour etre independant de la taille du champ : -1 1*/
    pos_float = (   (float) max_pos / (  (float)( (int) (longueur / 2) )  )   ) - 1.;

    new_val = neurone[deb_gpe_proprio].s1 + pas * pos_float;
#ifdef DEBUG
    printf("new_val %f pas %f pos_float %f\n", new_val, pas, pos_float);
#endif
    if (new_val > 1.)
        new_val = 1.;
    if (new_val < 0.)
        new_val = 0.;

    neurone[deb_gpe].s1 = neurone[deb_gpe].s2 = neurone[deb_gpe].s = new_val;
}
