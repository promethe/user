/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_keypoint_descriptor.c 
\brief 

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
(plus utilise maintenant : deprecated)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-Compute_Keypoint_Descriptor_At_XY()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#include "tools/include/Compute_Keypoint_Descriptor_At_XY.h"

#undef DEBUG

void function_keypoint_descriptor(int Gpe)
{
    int i, inputGpe = -1, sx, sy, row, column, NMap = 0;
    char *ST, *chaine;
    float *inputImagesK[10];
    float *inputImagesG[10];
    float *inputImagesO[10];
    float KeyOrient;

    /* initialisation */
#ifdef DEBUG
    printf("groupe %d : %s\n", Gpe, __FUNCTION__);
#endif

    /* on cherche les groupes precedents */
#ifdef DEBUG
    printf("dans function_keypoint_descriptor : recup liens vers images\n");
#endif
    NMap = 0;
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == Gpe)
        {
            inputGpe = liaison[i].depart;
            if (def_groupe[inputGpe].ext == NULL)
            {
                printf("Gpe %d : un groupe amont a son ext NULL\n", Gpe);
                return;
            }
            chaine = liaison[i].nom;
            if ((ST = strstr(chaine, "KO")) != NULL)
            {
                inputImagesK[atoi(&ST[2])] =
                    (float *) ((prom_images_struct *) def_groupe[inputGpe].
                               ext)->images_table[0];
#ifdef DEBUG
                printf("input Gpe %d K carte %d \n", inputGpe, atoi(&ST[2]));
#endif
                NMap++;
            }
            else if ((ST = strstr(chaine, "GO")) != NULL)
            {
                inputImagesG[atoi(&ST[2])] =
                    (float *) ((prom_images_struct *) def_groupe[inputGpe].
                               ext)->images_table[0];
                inputImagesO[atoi(&ST[2])] =
                    (float *) ((prom_images_struct *) def_groupe[inputGpe].
                               ext)->images_table[1];
#ifdef DEBUG
                printf("input Gpe %d GO carte %d\n", inputGpe, atoi(&ST[2]));
#endif
            }
            else
            {
                printf("%s (%d) Un lien en entree est mal formate....\n",
                       __FUNCTION__, __LINE__);
                exit(0);
            }
        }
    }
    if (inputGpe == -1)
    {
        printf("%s : Gpe %d : Pas de groupe en amont...\n", __FUNCTION__,
               Gpe);
        exit(0);
    }
#ifdef DEBUG
    printf("Gpe %d recup_info\n", Gpe);
#endif

    /*recup d'infos dans un des groupes */
    sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
    sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;

    if (((prom_images_struct *) def_groupe[inputGpe].ext)->nb_band != 4)
    {
        printf
            ("%s : (Gpe %d) cette fonction ne fait que les images en floatants N&B...\n",
             __FUNCTION__, Gpe);
        exit(0);
    }

    if (def_groupe[Gpe].ext == NULL)
    {
        /* allocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s (%d) : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        for (i = 0; i < 8; i++)
        {
            ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[i] =
                (unsigned char *) calloc(sx * sy, sizeof(float));
            if (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[i] == NULL)
            {
                printf("%s (%d) : ALLOCATION IMPOSSIBLE ...! \n",
                       __FUNCTION__, __LINE__);
                exit(0);
            }
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 8;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;

    }
    else
    {
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        for (i = 0; i < 8; i++)
            for (row = 0; row < sy; row++)
                for (column = 0; column < sx; column++)
                {
                    *((float
                       *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                           images_table[i]) + row * sx + column) = 0.;
                }
    }
#ifdef DEBUG
    printf("Gpe %d calcul...\n", Gpe);
    printf("sx %d sy%d NMap %d\n", sx, sy, NMap);
#endif

    for (i = 0; i < NMap; i++)  /*parcours des maps */
    {
#ifdef DEBUG
        printf("map a annalyser %d\n", i);
#endif
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
            {
                /*printf("row %d column %d\n",row,column); */
                if (fabs(*(inputImagesK[i] + row * sx + column)) > 0.0000001)   /*!=0. */
                {
                    KeyOrient = *(inputImagesK[i] + row * sx + column);
                    Compute_Keypoint_Descriptor_At_XY(KeyOrient,
                                                      inputImagesG[i],
                                                      inputImagesO[i],
                                                      def_groupe[Gpe].ext,
                                                      row, column, sx, sy);
                }
            }
    }                           /*fin parcours des maps */
#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
    return;

}
