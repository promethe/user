/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_merge_servo_flow.c 
\brief 

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>

/*fonction totalement ad-hoc a ne pas utilise et a virer des que possible...*/
typedef struct TAG_STRUCT_MERGE_SERVO_FLOW
{
    int deb_mvt;
    int S;
    int deb_servo;
    int nb;
    int index;
    float last_pos;
    float *registre;

} struct_merge_servo_flow;


void function_merge_servo_flow(int Gpe)
{
    int i, j;
    int lien, lien_servo = -1, lien_mvt = -1;
    struct_merge_servo_flow *this_data;
    int nb = 1;
    int index;
    float last_pos;
    int S, deb_mvt, deb_servo;
    float *registre;
    float sum;
    float new_value, delta, toto;
    int deb;
    char *param;

    if (def_groupe[Gpe].data == NULL)
    {
        for (i = 0; i < 2; i++)
        {
            if ((lien = find_input_link(Gpe, i)) == -1)
            {
                printf
                    ("%s : Gpe %d : Le nombre de groupe entrant est incorrect...\n",
                     __FUNCTION__, Gpe);
                exit(0);
            }
            if (strstr(liaison[lien].nom, "-servo") != NULL)
                lien_servo = lien;
            else
                lien_mvt = lien;
            if ((param = strstr(liaison[lien].nom, "-N")) != NULL)
                nb = atoi(&param[2]);

        }
        if (lien_servo == -1 || lien_mvt == -1)
        {
            printf
                ("%s : Gpe %d : Le nombre de groupe entrant est incorrect...\n",
                 __FUNCTION__, Gpe);
            exit(0);
        }

        this_data = malloc(sizeof(struct_merge_servo_flow));
        def_groupe[Gpe].data = (void *) this_data;
        if (this_data == NULL)
        {
            printf
                (" Erreur d'alloc memoire dans function_merge_servo_flow\n");
            exit(1);
        }
        this_data->registre = calloc(nb, sizeof(float));
        registre = this_data->registre;
        if (registre == NULL)
        {
            printf
                (" Erreur d'alloc memoire dans function_merge_servo_flow\n");
            exit(1);
        }

        this_data->deb_mvt = deb_mvt =
            def_groupe[liaison[lien_mvt].depart].premier_ele;
        this_data->S = S =
            def_groupe[liaison[lien_mvt].depart].taillex *
            def_groupe[liaison[lien_mvt].depart].tailley;
        this_data->deb_servo = deb_servo =
            def_groupe[liaison[lien_servo].depart].premier_ele;
        this_data->nb = nb;
        index = this_data->index = 0;
        last_pos = this_data->last_pos = 0.;

        if (S != (def_groupe[Gpe].taillex * def_groupe[Gpe].tailley))
        {
            printf
                (" Erreur : nombre de neurone du gpe != nombre neurone du gpe de mvt en entree (function_merge_servo_flow) gpe %d\n",
                 Gpe);
            exit(1);
        }
    }
    else
    {
        this_data = (struct_merge_servo_flow *) def_groupe[Gpe].data;
        deb_mvt = this_data->deb_mvt;
        S = this_data->S;
        deb_servo = this_data->deb_servo;
        nb = this_data->nb;
        registre = this_data->registre;
        index = this_data->index;
        last_pos = this_data->last_pos;
    }

    deb = def_groupe[Gpe].premier_ele;
    new_value = neurone[deb_servo].s1;
    delta = new_value - last_pos;
    registre[index] = delta;
    sum = 0.;
    for (j = 0; j < nb; j++)
        sum += registre[j];

/*	printf("%f\n",sum);*/

    if (sum < 0.000001 && sum > -0.0000001) /*cas ou servo bouge pas : on laisse tout passe */
        for (i = 0; i < S; i++)
            neurone[deb + i].s2 = neurone[deb + i].s1 = neurone[deb + i].s =
                neurone[deb_mvt + i].s1;
    else if (sum > 0.)
    {
        for (i = 0; i < S; i++)
        {
            toto = neurone[deb_mvt + i].s1;
            if (toto < 0.)
                neurone[deb + i].s2 = neurone[deb + i].s1 =
                    neurone[deb + i].s = toto;
            else
                neurone[deb + i].s2 = neurone[deb + i].s1 =
                    neurone[deb + i].s = 0.;

        }

    }
    else
    {
        for (i = 0; i < S; i++)
        {
            toto = neurone[deb_mvt + i].s1;
            if (toto > 0.)
                neurone[deb + i].s2 = neurone[deb + i].s1 =
                    neurone[deb + i].s = toto;
            else
                neurone[deb + i].s2 = neurone[deb + i].s1 =
                    neurone[deb + i].s = 0.;

        }

    }


    this_data->last_pos = new_value;
    this_data->index = (index + 1) % nb;




    return;
}
