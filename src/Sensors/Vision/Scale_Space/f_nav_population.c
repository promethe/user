/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_nav_vit_orient.c
\brief

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction permettant de determiner la vitesse et l'orientation du robot.

Parametres : -V, groupe entrant info vitesse.
	     -O, groupe entrant info orientation
	     -B, indique si le robot recule (back-speed = 1, sinon = 0)
	     -M, max-speed (par defaut = MAX_MOTOR).

Macro:
-COM_D
-MOTOR_MAX

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-tools/IO_Robot/Com_Koala/koala_ir()
-tools/IO_Robot/Com_Koala/commande_koala()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Macro/IO_Robot/macro.h>
#include "tools/include/macro.h"

#include <Kernel_Function/find_input_link.h>

#undef DEBUG
void function_nav_population(int Gpe)
{
    int i;
    int lien, lien_vit = -1, lien_orient = -1;
    int Gpe_vit, Gpe_orient;
    int longueur_vit, deb_vit, longueur_orient, deb_orient;
    int pos_orient = 0, pos_vit = -1;
    float vit, angle;
    float temp = 0.;
    float main_speed, right_speed, left_speed;
    int param[2];
    float IR[16];
    int front_shock_left = 0;
    int front_shock_right = 0;
    int left_shock = 0;
    int right_shock = 0;
    int back_speed = 0;
    char *parse;
    float max_speed = (float) MAX_MOTOR;
    /*      int max=0; */
#ifdef DEBUG
    printf("f_nav_population\n");
#endif
    for (i = 0; (lien = find_input_link(Gpe, i)) != -1; i++)
        if (strstr(liaison[lien].nom, "-V") != NULL)
        {
            lien_vit = lien;
            if (strstr(liaison[lien].nom, "-B") != NULL)
                back_speed = 1;
            if ((parse = strstr(liaison[lien].nom, "-M")) != NULL)
                max_speed = (float) (atoi(&parse[2]));
        }
        else if (strstr(liaison[lien].nom, "-O") != NULL)
            lien_orient = lien;
        else
        {
            printf
                ("%s : Gpe : %d : le formatage des liens est incorrect...\n",
                 __FUNCTION__, Gpe);
            exit(0);
        }


    if (lien_vit == -1 || lien_orient == -1)
    {
        printf("%s : Gpe : %d : le formatage des liens est incorrect...\n",
               __FUNCTION__, Gpe);
        exit(0);
    }

    Gpe_vit = liaison[lien_vit].depart;
    longueur_vit = def_groupe[Gpe_vit].nbre;
    if (longueur_vit == 1)
        vit = max_speed;        /*si 1 neurone : vitesse max */
    else
    {
        deb_vit = def_groupe[Gpe_vit].premier_ele;
        for (i = deb_vit; i < deb_vit + longueur_vit; i++)
            if (neurone[i].s1 > temp)
            {
                pos_vit = i;
                temp = neurone[i].s1;
            }
        pos_vit -= deb_vit;
        if (temp < 0.0001)      /*y'avait pas de max */
        {
            vit = 0.;
        }
        else
        {
            if (back_speed)     /*possibilite de recule */
            {
                vit = (float) pos_vit / (float) (longueur_vit - 1);
                vit -= 0.5;
                vit *= (2 * max_speed);
            }
            else
            {
                vit = (float) pos_vit / (float) (longueur_vit - 1);
                vit *= max_speed;
            }
        }
    }

    /*temp=0.;
       Gpe_orient = liaison[lien_orient].depart;
       longueur_orient = def_groupe[Gpe_orient].nbre;
       deb_orient = def_groupe[Gpe_orient].premier_ele;
       pos_orient = deb_orient+(longueur_orient+1)/2;
       for(i=deb_orient;i<deb_orient + longueur_orient;i++)
       if(neurone[i].s1>temp)
       {
       pos_orient = i;
       temp = neurone[i].s1;
       }
       pos_orient -= deb_orient;
       if((longueur_orient%2)==0)
       angle = (float)pos_orient / longueur_orient;
       else
       angle = (float)pos_orient / (longueur_orient-1);
       angle -= 0.5;
       angle *= 2*max_speed; */

    temp = 0.;
    Gpe_orient = liaison[lien_orient].depart;
    longueur_orient = def_groupe[Gpe_orient].nbre;
    deb_orient = def_groupe[Gpe_orient].premier_ele;
    pos_orient = deb_orient + longueur_orient / 2;
    for (i = deb_orient; i < deb_orient + longueur_orient; i++)
        if (neurone[i].s1 > temp)
        {
            pos_orient = i;
            temp = neurone[i].s1;
        }
    pos_orient -= deb_orient;
    if (longueur_orient != 1)
        angle = (float) pos_orient / (longueur_orient - 1);
    else
        angle = (float) 0.5;
    angle -= 0.5;
    angle *= 2 * max_speed;

    /*angle=neurone[deb_orient+1].s1 - neurone[deb_orient].s1; */

#ifdef DEBUG
    printf("avant robot_get_ir\n");
#endif


#ifdef DEBUG
    /*printf("------------ vainqueur %d : %f\n",max,temp); */
#endif
    robot_get_ir(robot_get_first_robot(), IR);

#ifdef DEBUG
    printf("apres robot_get_ir\n");
#endif
    /*      vit = -5; */
    /*      main_speed = 0-vit; */
    main_speed = vit;
    right_speed = main_speed * (1 - angle / (2. * (float) (max_speed)));
    left_speed = main_speed * (1 + angle / (2. * (float) (max_speed)));

    for (i = 8; i < 12; i++)
        if (IR[i] > 0.5)
        {
            front_shock_left++;
        }
    if (front_shock_left)
    {
        left_speed = -right_speed;
    }


    for (i = 0; i < 4; i++)
        if (IR[i] > 0.5)
        {
            front_shock_right++;
        }
    if (front_shock_right)
    {
        right_speed = -left_speed;
    }

    for (i = 3; i < 7; i++)
        if (IR[i] > 0.5)
            left_shock++;
    if (left_shock)
        left_speed += (10 * left_speed) / 100;



    for (i = 11; i < 15; i++)
        if (IR[i] > 0.5)
            right_shock++;
    if (right_shock)
        right_speed += (10 * right_speed) / 100;


    /*if(IR[6]>0.7 || IR[7]>0.7 || IR[15]>0.7 || IR[14]>0.7 )
       back_shock ++;

       if(back_shock) */


    param[0] = (int) left_speed;
    param[1] = (int) right_speed;
    /*printf("commande %d %d\n",left_speed,right_speed); */
    /*      if(global_learn==0)
       { */
#ifdef DEBUG
    printf("avant robot_go_by_speed\n");
#endif
    robot_go_by_speed(robot_get_first_robot(), param[0], param[1]);
    /*              commande_koala(COM_D,param,NULL); */
    /*      } */

#ifdef DEBUG
    printf("apres_go_by_speed\n");
#endif
    return;
}
