/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_save_image_activity.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction permet de sauvegarder une image codee en floatant.
Le nom du groupe est automatiquement concatene.
ex : nom.gpexx.Nxxx.Mxxx.SAVE

Parametres : -a, -->mode = 0 ... mode = 1 ...
	     -T, on recupere le nom du fichier.
Macro:
-none 

Local variables:
-none

Global variables:
-char GLOB_path_SAVE[512]

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>

typedef struct TAG_STRUCT_SAVE_EXT_FLOAT
{
    int compt;
    char name[64];
    float *image_float;
    int sx;
    int sy;
    char mode;

} struct_save_ext_float;


void function_save_ext_float(int numero)
{
    FILE *fp;
    char char_name[64];
    int row, column;
    struct_save_ext_float *this_struct = NULL;
    int lien, i;
    char *param;
    int sx, sy, mode, compt;
    float *image_float;


#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("function_save_ext_float---------------------\n");
#endif

    if (def_groupe[numero].data == NULL)
    {

        if ((lien = find_input_link(numero, 0)) == -1)
        {
            printf
                ("erreur de lien entrant dans function_save_image_activity gpe %d\n",
                 numero);
            exit(1);
        }

        if (def_groupe[liaison[lien].depart].ext == NULL)
        {
            printf
                ("pas d'ext dans le gpe entrant dans function_save_image_activity gpe %d\n",
                 numero);
        }

        if (((prom_images_struct *) (def_groupe[liaison[lien].depart].ext))->
            nb_band != 4)
        {
            printf
                ("erreur ds function_save_image_activity gpe %d: ne traite que les images en floatant\n",
                 numero);
            exit(1);
        }

        this_struct = malloc(sizeof(struct_save_ext_float));
        if (this_struct == NULL)
        {
            printf
                ("Erreur Alloc memoire dans function_save_image_activity gpe %d\n",
                 numero);
            exit(1);
        }
        def_groupe[numero].data = (void *) this_struct;
        compt = this_struct->compt = 0;

        sx = ((prom_images_struct *) (def_groupe[liaison[lien].depart].ext))->
            sx;
        sy = ((prom_images_struct *) (def_groupe[liaison[lien].depart].ext))->
            sy;

        image_float =
            (float
             *) ((prom_images_struct *) (def_groupe[liaison[lien].depart].
                                         ext))->images_table[0];

        this_struct->image_float = image_float;
        this_struct->sx = sx;
        this_struct->sy = sy;

        if ((param = strstr(liaison[lien].nom, "-a")) != NULL)
            mode = 0;
        else
            mode = 1;

        this_struct->mode = mode;


        if ((param = strstr(liaison[lien].nom, "-T")) != NULL)
        {
            i = 0;
            while (param[2 + i] != '-' && param[2 + i] != '\0' && i < 63)
            {
                this_struct->name[i] = param[2 + i];
                i++;
            }
            this_struct->name[i] = '\0';
        }
        else
            sprintf(this_struct->name, "save_");

        strcpy(char_name, this_struct->name);

    }
    else
    {
        this_struct = (struct_save_ext_float *) def_groupe[numero].data;
        sx = this_struct->sx;
        sy = this_struct->sy;
        mode = this_struct->mode;
        strcpy(char_name, this_struct->name);
        compt = this_struct->compt;
        image_float = this_struct->image_float;

    }

    if (mode == 1)
    {
        sprintf(char_name, "%s_gpe%d_%d", char_name, numero, compt);
    }


    fp = fopen(char_name, "a");
    if (fp == NULL)
    {
        printf("Impossible to create file %s\n", char_name);
        exit(EXIT_FAILURE);
    }

    for (row = 0; row < sy; row++)
    {
        for (column = 0; column < sx; column++)
        {
            fprintf(fp, "%f, ", *(image_float + row * sx + column));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "\n\n");

    fclose(fp);

    this_struct->compt++;

#ifdef DEBUG
    printf("fin %s\n", __FUNCTION__);
#endif
}
