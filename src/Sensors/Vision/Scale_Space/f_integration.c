/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_add_neural_Gpe.c 
\brief 

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Memoire du contenu du groupe precedent, reset par un signal entrant
Les groupes doivent avoir le meme nombre de neurones.
Evite de mettre heb avec lien recurrent et signal de reset faisant perdre un coup.

Parametres : reset, lien du reset.
	     -m, facteur de modulation.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>

#define DEBUG

typedef struct TAG_DATA_INTEGRATION
{
    int last_reset;
    int deb;
    int deb1;
    int deb_reset;
    float modulation;
    int longueur;

} data_integration;

/*fonction totalement ad-hoc a ne pas utilise et a virer des que possible...*/
void function_integration(int Gpe)
{
    int i, j, k;
    int lien, lien_reset, lien_temp;
    int Gpe1, Gpe_reset;
    int longueur1, deb1, deb_reset, longueur, deb;
    float modulation = 1.;
    char *tmp;
    data_integration *mydata = NULL;
    float last_reset = 1, val_reset;


    if (def_groupe[Gpe].data == NULL)
    {
        if ((lien = find_input_link(Gpe, 0)) == -1
            || (lien_reset = find_input_link(Gpe, 1)) == -1)
        {
            printf
                ("%s : Gpe %d : Le nombre de groupe entrant est incorrect...\n",
                 __FUNCTION__, Gpe);
            exit(0);
        }

        if (strstr(liaison[lien].nom, "-reset") != NULL)
        {
            lien_temp = lien;
            lien = lien_reset;
            lien_reset = lien_temp;
        }
        if ((tmp = strstr(liaison[lien].nom, "-m")) != NULL)
            modulation = (float) atof(&tmp[2]);


        Gpe1 = liaison[lien].depart;
        longueur1 = def_groupe[Gpe1].nbre;
        deb1 = def_groupe[Gpe1].premier_ele;

        Gpe_reset = liaison[lien_reset].depart;
        deb_reset = def_groupe[Gpe_reset].premier_ele;

        longueur = def_groupe[Gpe].nbre;
        deb = def_groupe[Gpe].premier_ele;

        if (longueur != longueur1)
        {
            printf
                ("%s : Gpe %d : Les groupes n'ont pas le meme nombre de neurones...\n",
                 __FUNCTION__, Gpe);
            exit(0);
        }
        mydata = malloc(sizeof(data_integration));
        mydata->last_reset = last_reset;
        mydata->deb = deb;
        mydata->deb1 = deb1;
        mydata->deb_reset = deb_reset;
        mydata->modulation = modulation;
        mydata->longueur = longueur;
        def_groupe[Gpe].data = (void *) mydata;
    }
    else
    {
        mydata = (data_integration *) def_groupe[Gpe].data;
        last_reset = mydata->last_reset;
        deb = mydata->deb;
        deb1 = mydata->deb1;
        deb_reset = mydata->deb_reset;
        longueur = mydata->longueur;
        modulation = mydata->modulation;
    }

    val_reset = neurone[deb_reset].s1;
    if (isdiff(val_reset, last_reset))
    {
        for (i = deb; i < (deb + longueur); i++)
        {
            neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.;
        }
        mydata->last_reset = val_reset;
#ifdef DEBUG
        printf("____reset sur gpe f_integration___\n");
#endif
    }


    for (i = 0; i < longueur; i++)
    {
        j = i + deb;
        k = i + deb1;
        neurone[j].s += modulation * neurone[k].s;
        neurone[j].s1 += modulation * neurone[k].s1;
        neurone[j].s2 += modulation * neurone[k].s2;
    }
    return;
}
