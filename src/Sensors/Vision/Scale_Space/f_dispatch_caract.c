/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_dispatch_caract.c 
\brief 

Author: Mickael Maillard
Created: 01/04/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction dispatch dans differents groupes la structure caract recupere sur le groupe amont.
Le dispatch est effectue en fonction des parametres du lien : -C : numero de la caracterisation
							      -V : numero du voisinage

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#undef DEBUG

void function_dispatch_caract(int Gpe)
{
    float *outputImage;
    int i, row, column;
    int sx, sy;
    int *param_table;
    char *chaine = NULL, *param = NULL;
    int inputGpe = -1;
    int Caract, Voisin;
    prom_images_struct *inputCaract = NULL;

#ifdef DEBUG
    printf("Gpe %d dispatch_caract\n", Gpe);
#endif



    if (def_groupe[Gpe].ext == NULL)
    {
        /*recherche du numero de groupe entrant et gestion des erreurs si existantes */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                inputGpe = liaison[i].depart;
                break;
            }
        }
        if (inputGpe == -1)
        {
            printf("%s : Gpe : %d : pas de liaison entrante\n", __FUNCTION__,
                   Gpe);
            exit(0);
        }

        param = strstr(chaine, "-C");
        if (param != NULL)
            Caract = atoi(&param[2]);
        else
        {
            printf("%s : Parametre de lien incorrect -C##\n", __FUNCTION__);
            exit(0);
        }

        param = strstr(chaine, "-V");
        if (param != NULL)
            Voisin = atoi(&param[2]);
        else
        {
            printf("%s : Parametre de lien incorrect -V##\n", __FUNCTION__);
            exit(0);
        }

        inputCaract = (prom_images_struct *) def_groupe[inputGpe].ext;
        if (inputCaract == NULL)
        {
            printf("Gpe : %d : Pas de pointeur ext dans le Gpe amont \n",
                   Gpe);
            return;
        }
        sx = inputCaract->sx;
        sy = inputCaract->sy;


        /*allocation de memoire pour la structure resulante */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /*taille et nb_band de la map resultat */
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;


        /*allocation de memoire pour la map resultat */
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
            malloc(sx * sy * sizeof(float));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
        outputImage =
            (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                       images_table[0]);

        /*stockage du pointeur d'entree */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) inputCaract;

        /*sauvegarde des parametres du lien */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            malloc(sx * sy * sizeof(int));
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] ==
            NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        param_table =
            (int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[2];
        *(param_table + 0) = Caract;
        *(param_table + 1) = Voisin;
    }
    else
    {
        outputImage =
            (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                       images_table[0]);
        inputCaract =
            (prom_images_struct *) ((prom_images_struct *) def_groupe[Gpe].
                                    ext)->images_table[1];
        sx = inputCaract->sx;
        sy = inputCaract->sy;
        param_table =
            (int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[2];
        Caract = *(param_table + 0);
        Voisin = *(param_table + 1);
    }

    switch (Caract)
    {
    case 1:
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
            {
                *(outputImage + row * sx + column) =
                    *((float *) (inputCaract->images_table[Voisin]) +
                      row * sx + column);
            }
        break;

    case 2:
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
            {
                *(outputImage + row * sx + column) =
                    *((float *) (inputCaract->images_table[Voisin + 4]) +
                      row * sx + column);
            }
        break;

    default:
        printf
            ("%s : %d : Gpe : %d : aucune caracterisation associee au parametre -C%d\n",
             __FUNCTION__, __LINE__, Gpe, Caract);
        exit(0);
        break;


    }
    return;
}
