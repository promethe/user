/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_nav_vit_orient.c
\brief

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction ad-hoc deprecated a supprimer.

Macro:
-COM_D
-MOTOR_MAX

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()
-tools/IO_Robot/Com_Koala/koala_ir()
-tools/IO_Robot/Com_Koala/commande_koala()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Macro/IO_Robot/macro.h>
#include "tools/include/macro.h"

#include <Kernel_Function/find_input_link.h>

#undef DEBUG
void function_merge_neuro_mod(int Gpe)
{
    int i;
    int lien, deb;
    float temp = -9999999999.;
    float base, multi;
    char *param = NULL;
    float max = 1;

    multi = 1.;
    base = 0.;
/*	int max=0;*/
    for (i = 0; (lien = find_input_link(Gpe, i)) != -1; i++)
    {
        if ((param = strstr(liaison[lien].nom, "-max")) != NULL)
            max = atof(&param[4]);
        if (strstr(liaison[lien].nom, "-principale") != NULL)
        {
            base = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
        }
        else
        {
            temp = neurone[def_groupe[liaison[lien].depart].premier_ele].s1;
            if (temp > 0.)
            {
                multi = multi * temp;
            }
        }
    }
    deb = def_groupe[Gpe].premier_ele;
    /*vig comprise entre 1 et base (pres de 1 quand multi petite car distance au centre */
    if (base < 0.)
    {
        neurone[deb].s2 = neurone[deb].s1 = neurone[deb].s = base;
    }
    else
    {
        neurone[deb].s2 = neurone[deb].s1 = neurone[deb].s =
            (1 - base) * (1 - multi) + base;
        if (neurone[deb].s1 > max)
            neurone[deb].s2 = neurone[deb].s1 = neurone[deb].s = max;
        printf("dans merge neuro mod %f\n", (1 - base) * (1 - multi) + base);

    }
    return;
}
