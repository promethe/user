/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_coarse_to_fine.c 
\brief oscillateur sur une population de neurones dependant de l'echelle de temps

Author: Maickael Maillard
Created: 01/04/2004
Modified:
- author: M. Maillard
- description: specific file creation
- date: 19/09/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdlib.h>
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>

#undef DEBUG
struct coarse_to_fine_gpe_data
{
    int deb_gpe;
    int nbre;
    int count;

};

void function_coarse_to_fine(int Gpe)
{
    int /*lien, */ i;
    int deb_gpe = -1;
    int nbre, count;
    struct coarse_to_fine_gpe_data *this_coarse_to_fine_gpe_data;
    int pos_abs, pos_neur;


#ifdef DEBUG
    printf("entree dans function_coarse_to_fine Gpe %d \n", Gpe);
#endif
    if (def_groupe[Gpe].data == NULL)
    {
        /*if( (lien = find_input_link(Gpe,0)) ==-1 ) 
           {
           printf("Erreur de lien entrant dans function_coarse_to_fine : gpe %d\n",Gpe);
           exit(1);
           } */

        deb_gpe = def_groupe[Gpe].premier_ele;
        nbre = def_groupe[Gpe].nbre;
        count = 0;

        def_groupe[Gpe].data = malloc(sizeof(struct coarse_to_fine_gpe_data));
        if (def_groupe[Gpe].data == NULL)
        {
            printf
                ("Erreur Allocation memoire dans f_coarse_to_fine gpe %d \n",
                 Gpe);
            exit(1);
        }
        this_coarse_to_fine_gpe_data =
            (struct coarse_to_fine_gpe_data *) def_groupe[Gpe].data;
        this_coarse_to_fine_gpe_data->deb_gpe = deb_gpe;
        this_coarse_to_fine_gpe_data->nbre = nbre;
        this_coarse_to_fine_gpe_data->count = count;

    }
    else
    {
        this_coarse_to_fine_gpe_data =
            (struct coarse_to_fine_gpe_data *) def_groupe[Gpe].data;
        deb_gpe = this_coarse_to_fine_gpe_data->deb_gpe;
        nbre = this_coarse_to_fine_gpe_data->nbre;
        count = this_coarse_to_fine_gpe_data->count;
    }

    for (i = deb_gpe; i < deb_gpe + nbre; i++)
    {
        neurone[i].s2 = neurone[i].s1 = neurone[i].s = 0.;
    }

    pos_neur = count % nbre;

    pos_abs = deb_gpe + pos_neur;
    neurone[pos_abs].s2 = neurone[pos_abs].s1 = neurone[pos_abs].s = 1.;

    this_coarse_to_fine_gpe_data->count =
        (this_coarse_to_fine_gpe_data->count + 1) % nbre;

}
