/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_gather_scale_desc.c 
\brief 

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Fonction reunissant les caracterisations de chaque echelle dans une meme caracterisation 
  la reunion se fait sur la carte de plus grande taille
 
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

#undef DEBUG
#undef CARACT_PAR_OCTAVE

#define DEF_X 21
#define DEF_Y 16

void function_gather_scale_desc(int Gpe)
{

     /**/
#ifdef DEBUG
    static int nb_passage = 0;
#endif
    int i, k, inputGpe = -1, sx, sy, size, row, column, NCaract;
    /*float size_factor_x,size_factor_y; */
    int input_sx, input_sy;
    float size_factor_x, size_factor_y;
    prom_images_struct *inputCaract[10], *outputCaract;
/*  int local_row, local_column;
  int norm1, norm2;*/
/*  float  partial_sum1,partial_sum2,max_local_row,max_local_column;
  float *in_im_1,*in_im_2,*out_im_1,*out_im_2;*/
    float global_sum = 0.;
    int ponderation[8][36][47];

/*  static int echelle=-1;
  static int compteur_echelle = 0;*/
    int compteur_points = 0;

#ifdef DEBUG
    printf("groupe %d : %s\n", Gpe, __FUNCTION__);
#endif

    /* on cherche les groupes precedents */
#ifdef DEBUG
    printf("dans function_gather_scales_desc: recup liens vers images\n");
#endif

#ifdef DEBUG
    nb_passage++;

    printf(".....Image apprise numero %d .....", nb_passage);
#endif


    NCaract = 0;
    size = 10000 * 10000;       /*mouai je suis d'accord ca sert a rien : juste au cas ou quelqu'un veut reunir sur autre achose que la carte de plus grande taille mais plutot sur une autre : je crois que le code le supporte si on modifie ca. Faudrait le faire de facon dynamique et pas comme ca en dur dans le code */
    /*d'autant + que finalement ca sert a rien etant donne que c'est DEF_Y et DEF_X qui sont pris en compte */
    /*je laisse pour faire joli :) mais faudrait que ca soit un parametre du lien */
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == Gpe)
        {
            inputGpe = liaison[i].depart;
            if (def_groupe[inputGpe].ext == NULL)
            {
                printf("Gpe %d : l'extension du groupe precedent est NULL",
                       Gpe);
                return;
            }
            if ((int)(((prom_images_struct *) def_groupe[inputGpe].ext)->sx *
                 ((prom_images_struct *) def_groupe[inputGpe].ext)->sy) <
                size)
            {
                sx = ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
                sy = ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;
                size = sx * sy;
            }
            inputCaract[NCaract] =
                (prom_images_struct *) (def_groupe[inputGpe].ext);
            NCaract++;
        }
    }

    if (inputGpe == -1)
    {
        printf("%s : Gpe %d : Pas de groupe en amont...\n", __FUNCTION__,
               Gpe);
        exit(0);
    }

    if (def_groupe[Gpe].ext == NULL)
    {
        /*alloc de memoire pour ext */
        def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("%s : %d : Gpe :%d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",
                 __FUNCTION__, __LINE__, Gpe);
            exit(0);
        }

        /*alloc de memoire pour les cartes de caracterstiques */

        sx = DEF_X;
        sy = DEF_Y;
        for (i = 0; i < 8; i++)
        {
            ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[i] =
                calloc(sy * sx, sizeof(float));
            if (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[i] == NULL)
            {
                printf("%s (%d) : ALLOCATION IMPOSSIBLE ...! \n",
                       __FUNCTION__, __LINE__);
                exit(0);
            }
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 8;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
    }
    else
    {
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        for (i = 0; i < 8; i++)
            for (row = 0; row < sy; row++)
                for (column = 0; column < sx; column++)
                {
                    *((float
                       *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                           images_table[i]) + row * sx + column) = 0.;
                }
    }

    outputCaract = (prom_images_struct *) def_groupe[Gpe].ext;

    global_sum = (float) 0.0;
    /*FILE *fic;
       fic=fopen("res_caract.dat","w"); */



/*test d'influence des differentes echelles*/
/*	if(learn == 0)
	{
		if(compteur_echelle<1)
		{
			printf("compteur echelle =0 %d\n",compteur_echelle);
			echelle = -1;
			while(echelle<0)
			{
				printf("Choix d'une echelle entre 0 et 3 (toutes : >3): ");
				scanf("%d",&echelle);
				getchar();
			}
			if(echelle<=3)
				printf("---Taille de l'image a l'echelle selectionnee : %dx%d\n",inputCaract[echelle]->sx,inputCaract[echelle]->sy);
			else
				printf("toutes les echelles selectionnees\n");
			
			compteur_echelle=75;
		}
		else
		{
			printf("compteur echelle!=0 %d \n",compteur_echelle);
			compteur_echelle--;
		}
	}
	else
	{
	 echelle = 4;
	}*/

    for (k = 0; k < 4; k++)
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
            {
                ponderation[k][row][column] = 0;
                ponderation[k + 4][row][column] = 0;
            }
/*****************************
 * 	attention test des caract
 * 	*********************/
/*
	if(echelle>3)
	{*/
    for (i = 0; i < NCaract; i++)
    {
        compteur_points = 0;
        input_sx = inputCaract[i]->sx;
        input_sy = inputCaract[i]->sy;
        size_factor_x = ((float) sx / (float) input_sx);
        size_factor_y = ((float) sy / (float) input_sy);


        for (k = 0; k < 4; k++)
        {
            for (row = 0; row < input_sy; row++)
                for (column = 0; column < input_sx; column++)
                {

                    if (fabs
                        (*
                         ((float *) (inputCaract[i]->images_table[k]) +
                          row * input_sx + column)) > 0.0000001)
                    {
                        *((float *) (outputCaract->images_table[k]) +
                          (int) (row * size_factor_y) * sx +
                          (int) (column * size_factor_x)) +=
*((float *) (inputCaract[i]->images_table[k]) + row * input_sx + column);
                        ponderation[k][(int) (row * size_factor_y)][(int)
                                                                    (column *
                                                                     size_factor_x)]++;

                        if (k == 0)
                            compteur_points++;
                    }


                    if (fabs
                        (*
                         ((float *) (inputCaract[i]->images_table[k + 4]) +
                          row * input_sx + column)) > 0.00000001)
                    {
                        *((float *) (outputCaract->images_table[k + 4]) +
                          (int) (row * size_factor_y) * sx +
                          (int) (column * size_factor_x)) +=
*((float *) (inputCaract[i]->images_table[k + 4]) + row * input_sx + column);

                        ponderation[k +
                                    4][(int) (row *
                                              size_factor_y)][(int) (column *
                                                                     size_factor_x)]++;

                    }
                }
        }
#ifdef CARACT_PAR_OCTAVE
        printf("Nbre de points caracteristiques : octave %d %d  : %d\n",
               input_sx, input_sy, compteur_points);
#endif
    }
/*	}
	else
	{
	{
		i=echelle;
		input_sx = inputCaract[i]->sx;
		input_sy = inputCaract[i]->sy;
		size_factor_x = ((float)sx / (float)input_sx);
		size_factor_y = ((float)sy / (float)input_sy);


		for(k=0;k<4;k++)
		{			
			for(row=0;row<input_sy;row++)
				for(column=0;column<input_sx;column++)
				{
					
					if( *((float*)(inputCaract[i]->images_table[k]) + row*input_sx + column)!=0 )
					{	
						*((float*)(outputCaract->images_table[k]) + (int)(row*size_factor_y)*sx + (int)(column*size_factor_x)) += *((float*)(inputCaract[i]->images_table[k]) + row*input_sx + column);			
						ponderation[k][(int)(row*size_factor_y)][(int)(column*size_factor_x)]++;
			
					}
						
							
					if( *((float*)(inputCaract[i]->images_table[k+4]) + row*input_sx + column)!=0 )
					{
						*((float*)(outputCaract->images_table[k+4]) + (int)(row*size_factor_y)*sx + (int)(column*size_factor_x)) += *((float*)(inputCaract[i]->images_table[k+4]) + row*input_sx + column);
						
						ponderation[k+4][(int)(row*size_factor_y)][(int)(column*size_factor_x)]++;
											
					}	
				}
		}
	}
	}*/



    for (row = 0; row < sy; row++)
        for (column = 0; column < sx; column++)
            for (k = 0; k < 4; k++)
            {
                if (ponderation[k][row][column] != 0)
                {
                    *((float *) (outputCaract->images_table[k]) + row * sx +
                      column) /= ponderation[k][row][column];
                    global_sum +=
                        *((float *) (outputCaract->images_table[k]) +
                          row * sx + column);
                }
                if (ponderation[k + 4][row][column] != 0)
                {
                    *((float *) (outputCaract->images_table[k + 4]) +
                      row * sx + column) /= ponderation[k + 4][row][column];
                    global_sum +=
                        *((float *) (outputCaract->images_table[k + 4]) +
                          row * sx + column);
                }
            }
/*	FILE *fic;
	static int index = 0;
	char nom[255];
	sprintf(nom,"res_%d.dat",index);
	index++;
	fic = fopen(nom,"w");

*/
    if (fabs(global_sum) > 0.0000001)   /*!=0 */
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
                for (k = 0; k < 4; k++)
                {
                    *((float *) (outputCaract->images_table[k]) + row * sx +
                      column) /= global_sum;
                    *((float *) (outputCaract->images_table[k + 4]) +
                      row * sx + column) /= global_sum;
                    /*if(k==0)
                       fprintf(fic,"%d %d %f\n",row,column,*((float*)(outputCaract->images_table[k]) + row*sx + column));   */
                }
/*	fclose(fic);*/


/*	global_sum=0.0;
	for(i=0;i<NCaract;i++)
	{

		input_sx = inputCaract[i]->sx;
		input_sy = inputCaract[i]->sy;

		max_local_row = input_sy / sy;
		max_local_column = input_sx / sx;
		
		for(k=0;k<4;k++)
		{
			in_im_1 = (float*)(inputCaract[i]->images_table[k]);
			in_im_2 = (float*)(inputCaract[i]->images_table[k+4]);
			out_im_1 = (float*)(outputCaract->images_table[k]);
			out_im_2 = (float*)(outputCaract->images_table[k+4]);
			for(row=0;row<sy;row++)
				for(column=0;column<sx;column++)
				{
					partial_sum1=0.;
					norm1 = 0;
					partial_sum2=0.;
					norm2=0;
					
					for(local_row=0;local_row<(int)max_local_row;local_row++)
						for(local_column=0;local_column<(int)max_local_column;local_column++)
						{
							if( *(in_im_1 + (int)((row*max_local_row+local_row)*input_sx) + (int)(column*max_local_column+local_column)  )!=0	 )
							{
								partial_sum1 +=  *(in_im_1 + (int)((row*max_local_row+local_row)*input_sx) + (int)(column*max_local_column+local_column)  );
								norm1++;
							}
							
							
							if( *(in_im_2 + (int)((row*max_local_row+local_row)*input_sx) + (int)(column*max_local_column+local_column)  )!=0	 )
							{
								partial_sum2 +=  *(in_im_2 + (int)((row*max_local_row+local_row)*input_sx) + (int)(column*max_local_column+local_column)  );
								norm2++;
							}

						}

					if(norm1!=0)
						partial_sum1 /= (float)norm1;
					if(norm2!=0)
						partial_sum2 /= (float)norm2;
					
					*(out_im_1 + row*sx + column) += partial_sum1;
					*(out_im_2 + row*sx + column) += partial_sum2;
					global_sum = global_sum + partial_sum1 + partial_sum2;
				}
		}

	}
	
	
	if(global_sum != (float)0.0 )
		for(row=0;row<sy;row++)
			for(column=0;column<sx;column++)
				for(k=0;k<4;k++)
				{
					*((float*)(outputCaract->images_table[k]) + row*sx + column) /= global_sum;
					*((float*)(outputCaract->images_table[k+4]) + row*sx + column) /= global_sum;
				}
*/
/*	FILE *fic;
	fic=fopen("res_caract.dat","w");
	for(row=0;row<sy;row++)
		for(column=0;column<sx;column++)
			for(k=0;k<4;k++)
			if( ( *((float*)(outputCaract->images_table[k])+row*sx+column)!=0
		    		||  *((float*)(outputCaract->images_table[k+4]) + row*sx + column)!=0 ))
			{
					fprintf(fic,"row %d, column %d\n",row,column);
					fprintf(fic,"Voisinage %d\n",k);
					fprintf(fic,"%f %f \n\n",*((float*)(outputCaract->images_table[k]) + (int)(row)*sx + (int)(column)), *((float*)(outputCaract->images_table[k+4]) + (int)(row)*sx + (int)(column)) );
			}
	fclose(fic);*/
    return;
}
