/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_over_sample_two.c 
\brief Fonction sur-echantillonnant une image

Author: Mickael Maillard
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction effectue un sur-echantillonnage avec interpolation quadratique.
Elle permet de doubler la taille d'une image.
- L'image entrante doit etre en floatant (N&B ou couleur).
- Image resultat de taille 2*sx-2 , 2*sy-2 avec sx et sy la taille de l'image d'entree.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/prom_images_struct.h>

#undef DEBUG

void function_over_sample_two(int Gpe)
{

    int InputGpe = -1;
    float *inputImage, *outputImage;
    int i, row, column, band;
    int sx, sy, nb_band, ba, Nsx, Nsy;
    prom_images_struct *input_prom_images;
#ifdef DEBUG
    printf("Gpe %d oversample\n", Gpe);
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        /*recherche du numero de groupe entrant et gestion des erreur si inexistant */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                break;
            }
        }
        if (InputGpe == -1)
        {
            printf("%s : pas de groupe amont...\n", __FUNCTION__);
            exit(0);
        }

        /*verif d'une carte entrante */
        if (def_groupe[InputGpe].ext == NULL)
        {
            printf("%s : pointeur null sur le lien entrant\n", __FUNCTION__);
            /*exit(0); */
            return;
        }
        input_prom_images = (prom_images_struct *) def_groupe[InputGpe].ext;
        nb_band = input_prom_images->nb_band;

        /*la fonction effectue les calculs en floatant d'ou verif image rentrant en floatant */
        if (!((nb_band == 4) || (nb_band == 12)))
        {
            printf("%s ne prend que des images en float...\n", __FUNCTION__);
            exit(0);
        }

        /*allocation de memoire pour la structure resulante */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /*recuperation des info de la prom_images_struct a traiter */
        sx = input_prom_images->sx;
        sy = input_prom_images->sy;
        Nsx = 2 * sx - 2;
        Nsy = 2 * sy - 2;

        /*taille et nb_band de la map resultat */
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = Nsx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = Nsy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nb_band;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            input_prom_images->images_table[0];

        /*allocation de memoire pour la map resultat */
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
            malloc((2 * sx - 2) * (2 * sy - 2) * nb_band);
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
    }
    else
    {
        Nsx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        Nsy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        sx = (Nsx + 2) / 2;
        sy = (Nsy + 2) / 2;
        nb_band = ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band;
    }
    /*recuperation de pointeur pour alleger les expressions... */
    ba = nb_band / 4;
    outputImage =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[0]);
    inputImage =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[1]);

    /*calcul de la map resultante */
    for (row = 0; row < sy - 1; row++)
    {
        for (column = 0; column < sx - 1; column++)
        {
            for (band = 0; band < ba; band++)
            {
                *(outputImage + 2 * row * Nsx * ba + 2 * column * ba + band) =
                    *(inputImage + row * sx * ba + column * ba + band);
                *(outputImage + 2 * row * Nsx * ba + 2 * column * ba + band +
                  ba) =
(*(inputImage + row * sx * ba + column * ba + band) + *(inputImage + row * sx * ba + column * ba + band + ba)) / 2;
                *(outputImage + 2 * row * Nsx * ba + Nsx * ba +
                  2 * column * ba + band) =
(*(inputImage + row * sx * ba + column * ba + band) +
*(inputImage + (row + 1) * sx * ba + column * ba + band)) / 2;
                *(outputImage + 2 * row * Nsx * ba + Nsx * ba +
                  2 * column * ba + band + ba) =
(*(inputImage + row * sx * ba + column * ba + band) +
*(inputImage + (row + 1) * sx * ba + column * ba + band) + *(inputImage + row * sx * ba + column * ba +
                                    band + ba) + *(inputImage + (row +
                                                                 1) * sx *
                                                   ba + column * ba + band +
                                                   ba)) / 4;
            }
        }
    }
#ifdef DEBUG
    printf("fin Gpe %d oversample\n", Gpe);
#endif
    return;
}
