/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sub_sample.c 
\brief Fonction sous-echantillonnant une image 

Author: Mickael Maillard 
Created: 01/04/04
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction permet de sous échantillonner une image.
Sous échantillonnage sans interpolation.
L'image doit etre en floatant (N&B ou couleur).

Parametre : -S, facteur de sous echantillonnage. Par défaut il est égal à 2 en hauteur et 2 en largeur.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

/*#define DEBUG*/
#undef DEBUG

void function_sub_sample(int Gpe)
{
    int InputGpe = -1;
    char *chaine = NULL, *param;
    float *inputImage, *outputImage;
    int i, row, column, band;
    int sx, sy, nb_band, ba, ssx, ssy;
    int factor;
    prom_images_struct *input_prom_images;
#ifdef DEBUG
    printf("entree ds Gpe %d\n", Gpe);
#endif
    if (def_groupe[Gpe].ext == NULL)
    {
        /*recuperation du lien entrnt et traitement des erreurs */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                InputGpe = liaison[i].depart;
                chaine = liaison[i].nom;
                break;
            }
        }
        if (InputGpe == -1)
        {
            printf("%s : pas de groupe amont...\n", __FUNCTION__);
            exit(0);
        }

        if ((param = strstr(chaine, "-S")) != NULL)
            factor = atoi(&param[2]);
        else
        {
            printf
                ("%s : Le sous echantillonnage est effectue avec un facteur par defaut de 2...\n",
                 __FUNCTION__);
            factor = 2;
        }

        if (def_groupe[InputGpe].ext == NULL)
        {
            printf("%s : pointeur null sur le lien entrant\n", __FUNCTION__);
            /*exit(0); */
            return;
        }

        /*la fonction ne traite que des images en floatant */
        input_prom_images = (prom_images_struct *) def_groupe[InputGpe].ext;
        nb_band = input_prom_images->nb_band;

        /*if(!((nb_band==4) || (nb_band==12)))
           {
           printf("%s ne prend que des images en float...\n",__FUNCTION__);
           exit(0);
           } */

        /*allocation de memoire pour la structure du resultat */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /*recup des infos */
        sx = input_prom_images->sx;
        sy = input_prom_images->sy;

        /*calcul de la nouvelle taille et stockage dans la prom_images_struct */
        ssx = (int) (sx / factor);
        ssy = (int) (sy / factor);
        if (ssx <= 1 || ssy <= 1)
        {
            printf
                ("%s : image de taille %d x %d trop petite pour un sous echantillonnage par 2... \n",
                 __FUNCTION__, ssx, ssy);
            exit(0);
        }
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = ssx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ssy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nb_band;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        /*allocation de memoire pour la carte resulat */
        (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0]) =
            (unsigned char *) malloc(ssx * ssy * nb_band);
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }

        /*stockage de la taille de l'image d'entree : on ne sait pas si elle est paire ou impaire si on ne connait que ssx et ssy */
        (def_groupe[Gpe].data) = (int *) malloc(2 * sizeof(int));
        if (def_groupe[Gpe].data == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ...\n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }
        *((int *) (def_groupe[Gpe].data) + 0) = sx;
        *((int *) (def_groupe[Gpe].data) + 1) = sy;

        /*stockage pointeur de l'image d'entree */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            input_prom_images->images_table[0];
    }
    else
    {
        ssx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        ssy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        sx = *((int *) (def_groupe[Gpe].data) + 0);
        sy = *((int *) (def_groupe[Gpe].data) + 1);
        factor = (int) (sx / ssx);
        nb_band = ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band;
    }

    /*calcul de la nouvelle map */
    switch (((prom_images_struct *) def_groupe[Gpe].ext)->nb_band)
    {
    case 4:
    case 12:
        ba = nb_band / 4;
        outputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];
        inputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];
        for (row = 0; row < ssy; row++)
        {
            for (column = 0; column < ssx; column++)
            {
                for (band = 0; band < ba; band++)
                {
                    *(outputImage + row * ssx * ba + column * ba + band) =
                        *(inputImage + factor * row * sx * ba +
                          factor * column * ba + band);
                }
            }
        }
        break;

    case 1:
    case 3:
        ba = ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band;
        outputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];
        inputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];
        for (row = 0; row < ssy; row++)
        {
            for (column = 0; column < ssx; column++)
            {
                for (band = 0; band < ba; band++)
                {
                    *((unsigned char *) outputImage + row * ssx * ba +
                      column * ba + band) =
*((unsigned char *) inputImage + factor * row * sx * ba + factor * column * ba + band);
                }
            }
        }
        break;
    default:
        printf("Erreur dans le nb_band...(Gpe %d)\n", Gpe);
        exit(0);

    }
#ifdef DEBUG
    printf("fin Gpe %d\n", Gpe);
#endif
    return;
}
