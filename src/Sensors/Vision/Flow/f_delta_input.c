/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>

#define DEBUG

typedef struct GENERAL_STRUCT
{
  float in_old;
  int ren_neuron;
}general_structur;

/*--------------------------------------------------------*/
/* Start NEW */
void new_delta_input(int gpe)
{
  general_structur* ge_st = NULL;
  
  int lien=-1;
  int gpe_input;
  int deb_neuron;
  int ren_neuron;
  
  lien = find_input_link(gpe,0);
  gpe_input = liaison[lien].depart;
  deb_neuron =  def_groupe[gpe_input].premier_ele;
  ren_neuron = deb_neuron + def_groupe[gpe_input].nbre -1;
  ge_st = malloc(sizeof(general_structur));
  if(ge_st==NULL)
    {
      printf("Erreur memoire f_delta_input\n");
      exit(1);
    }
  
  ge_st->in_old = neurone[deb_neuron].s1;
  ge_st->ren_neuron = ren_neuron;
  
  /*          la sortie          */
  neurone[def_groupe[gpe].premier_ele].s  = 0.0;
  neurone[def_groupe[gpe].premier_ele].s1 = 0.0;
  neurone[def_groupe[gpe].premier_ele].s2 = 0.0;
   
  printf("Fine de Delta Input new ...\n");
  
  def_groupe[gpe].ext = (void*) ge_st;
 
}

/* End NEW */
/*--------------------------------------------------------*/
void function_delta_input(int gpe)
{
  general_structur* ge_st = NULL;
  int ren_neuron;
  float in_old, in, d_in ;
  
  ge_st = (general_structur*)def_groupe[gpe].ext;
  
  ren_neuron = ge_st->ren_neuron;
  in_old     = ge_st->in_old;
  
  /* Read input */
  in = neurone[ren_neuron].s1;
  /*printf("Reinforcement = %f\n",in);*/
  
  /* Calcul Delta */
  d_in = in - in_old;
  
  /* Save in Memory */
  ge_st->in_old = in;
  
  if(d_in<0.) d_in=-1.;
  else if(d_in>0.) d_in=1.;

  neurone[def_groupe[gpe].premier_ele].s  = -d_in;
  neurone[def_groupe[gpe].premier_ele].s1 = -d_in;
  neurone[def_groupe[gpe].premier_ele].s2 = -d_in;
#ifdef DEBUG  
  printf("passe=%f\n",in_old);
  printf("present=%f\n",in); 
  printf("--->>Delta Reinforcement = %f\n", neurone[def_groupe[gpe].premier_ele].s);
#endif
}
