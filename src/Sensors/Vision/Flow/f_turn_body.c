/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_turn_body.c 
\brief turn the body

Author: C Giovannangeli
Created: 01/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <libhardware.h>
#include <string.h>
#include <Struct/cam_pos.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
void function_turn_body(int Gpe)
{
    int DebutGpe;
    int TailleGpe;
    int MvtNeuron;
    float Angle = 0;
    int i;
    float Maxim;
    int gpe_entree = -1;
    int gpe_body = -1;
    Robot *robot=NULL;
    float body = -1, wanted;
    int link;
    /* float angle_head; */
    i = 0;
    link = find_input_link(Gpe, i);
    while (link != -1)
    {
        if (strcmp(liaison[find_input_link(Gpe, i)].nom, "body") == 0)
            gpe_body = liaison[find_input_link(Gpe, i)].depart;
        else if (strcmp(liaison[find_input_link(Gpe, i)].nom, "target") == 0)
            gpe_entree = liaison[find_input_link(Gpe, i)].depart;
        i++;
        link = find_input_link(Gpe, i);
    }


    body = neurone[def_groupe[gpe_body].premier_ele].s;
#ifdef DEBUG
    printf("body=%f\n", body);
    printf("gpe_entree=%d\n", gpe_entree);
#endif

    if ((gpe_entree == -1) || (gpe_body == -1))
    {
        printf("ERROR f_turn_servo not connected \n");
        exit(0);
    }
    DebutGpe = def_groupe[gpe_entree].premier_ele;
    TailleGpe = def_groupe[gpe_entree].nbre;


    /*! reset all group activation */
    /*for(i=TailleGpe; i--; )
       neurone[DebutGpe+i].s=neurone[DebutGpe+i].s1=neurone[DebutGpe+i].s2 = 0.0;

       *! compute the group's neuron.s input activity and find the maxim input activity */
    MvtNeuron = -1;
    Maxim = 0.0;
    for (i = 0; i < TailleGpe; i++)
    {
        if (neurone[DebutGpe + i].s > Maxim)
        {
            Maxim = neurone[DebutGpe + i].s;
            MvtNeuron = i;
        }
    }
#ifdef DEBUG
    printf("neuron max : %f : %d\n", Maxim, MvtNeuron);
#endif
  /** If we detect an input activity, turn in the focus point. The direction is between -1 (left) 
   *  and 1 (right).  Turn in the focus point direction; the field of view is about 70 degrees so 
   *  the half is 35. Set the winner neuron to 1.0
   */
    if (MvtNeuron != -1)
    {

        wanted = MvtNeuron / ((float) TailleGpe) + (0.5 / (float) TailleGpe);

        Angle = -wanted + body;
        while (Angle >= 0.5 || Angle < -0.5)
        {
            if (Angle >= 0.5)
                Angle = Angle - 1;
            else if (Angle < -0.5)
                Angle = Angle + 1;
        }
#ifdef DEBUG
        printf("angle = %f\n", Angle);

        printf("wanted=%f , body = %f\n", wanted, body);

        printf("Turn the robot of %f degres \n", Angle * 360);
#endif
        /*joint_servo_command(joint_get_serv_by_name((char*)"pan"),0.5,-1); */

        robot = robot_get_first_robot();
        if (robot->robot_type == 1)
        {
            /* Ajout Vincent */
            /* tentative de gestion de l'odometrie */
            robot_go_by_speed(robot, 0., 0.);
            robot_go_by_position_with_odo(robot, 2500., 2500.);
            robot_turn_angle(robot, Angle * 360.);
            robot_go_by_position_with_odo(robot, -2500., -2500.);
            robot_go_by_speed(robot, 0., 0.);

            /* Ce qui etait precedemment effectue */
            /*robot_go_by_position(robot, 2500. / 333., 2500. / 333.);
               robot_turn_angle(robot, Angle * 360);
               robot_go_by_position(robot, -2500. / 333., -2500. / 333.); */
        }
        else if (robot->robot_type == 3)
        {
            robot_go_forward(robot, 180.);
            robot_turn_angle(robot, Angle * 360);
            robot_go_forward(robot, -180.);
        }
        else
        {
            printf("a devel\n");
            exit(-1);
        }
    }
    else
        printf("No turn robot mvt_neuron vaut -1\n");


    neurone[def_groupe[Gpe].premier_ele].s =
        neurone[def_groupe[Gpe].premier_ele].s1 =
        neurone[def_groupe[Gpe].premier_ele].s2 = robot->angle / 360.;

    printf("rotate de %f\n", robot->angle / 360.);


}
