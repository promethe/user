/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_detect_mvt_a.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Detecte le mouvement par difference des images.
On construit 2 images par la sommation des premieres TemporalImagesIntegration
images et les derniere TemporalImagesIntegration images acquisitiones.
On calcule la difference entre les 2 images. Les sorties ont une activite 
proportionelle avec le nombre des pixels qui ont l'intensite change. 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>

void function_detect_mvt_a(int gpe_sortie)
{
    prom_images_struct *ImagesCaptures, *ImageLocale;
    /* pointeurs sur les tableaux qui contiendront les images */
    int Debut_Gpe;
    int TailleGroupeX, TailleGroupeY;
    int SizeZoneX, SizeZoneY;
    int Sx, Sy;
    unsigned char *ImageResultat;
    unsigned char *CarteProjection;
    int temp1, temp2;
    int i, j, m, k, l, n, Nhv, i1, j1, Nxy;
    int TemporalImagesIntegration, SeuilDetectMvt, SeuilDetection;
    int *Seuils;
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* initialisation locale */
    Debut_Gpe = def_groupe[gpe_sortie].premier_ele;
    TailleGroupeX = def_groupe[gpe_sortie].taillex;
    TailleGroupeY = def_groupe[gpe_sortie].tailley;
    Nhv = TailleGroupeX * TailleGroupeY;


    /* initialisation test */
    if (def_groupe[gpe_sortie].ext == NULL)
    {
        int Index;
        int InputGpe = -1;

        TemporalImagesIntegration = 4;
        SeuilDetectMvt = 10;
        SeuilDetection = 10;

        /* recuperations des donnees */
        for (Index = 0; Index < nbre_liaison; Index++)
            if (liaison[Index].arrivee == gpe_sortie)
            {
                InputGpe = liaison[Index].depart;
                if (liaison[Index].nom[0] == 'S')
                {
                  sscanf(liaison[Index].nom, "S%d,%d,%d", &TemporalImagesIntegration, &SeuilDetectMvt, &SeuilDetection);
                }
                break;
            }
        if (InputGpe == -1)
            EXIT_ON_ERROR("Aucune lien <input> n'a pas ete trouve");

        ImagesCaptures = (prom_images_struct *) def_groupe[InputGpe].ext;
        if (ImagesCaptures == NULL)
            EXIT_ON_ERROR("Le groupe precedant n'a pas d'extension");

        Sx = ImagesCaptures->sx;
        Sy = ImagesCaptures->sy;
        Nxy = Sx * Sy;


        /* creation de la structure */
        def_groupe[gpe_sortie].ext =
            (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[gpe_sortie].ext == NULL)
            EXIT_ON_ERROR("Not enought memory!");

        CarteProjection = (unsigned char *) malloc(Nhv * sizeof(char));
        ImageResultat = (unsigned char *) malloc(Nxy * sizeof(char));
        Seuils = (int *) malloc(3 * sizeof(int));
        if ((CarteProjection == NULL) || (ImageResultat == NULL) || (Seuils == NULL))
            EXIT_ON_ERROR("Not enought memory!");

        Seuils[0] = TemporalImagesIntegration;
        Seuils[1] = SeuilDetectMvt;
        Seuils[2] = SeuilDetection;

        ImageLocale = (prom_images_struct *) def_groupe[gpe_sortie].ext;
        ImageLocale->images_table[0] = ImageResultat;
        ImageLocale->images_table[1] = CarteProjection;
        ImageLocale->images_table[2] = (unsigned char *) ImagesCaptures;
        ImageLocale->images_table[3] = (unsigned char *) Seuils;
        ImageLocale->sx = Sx;
        ImageLocale->sy = Sy;
        ImageLocale->nb_band = 1;
        ImageLocale->image_number = 1;
    }
    else
    {
        ImageLocale = (prom_images_struct *) def_groupe[gpe_sortie].ext;
        ImageResultat = ImageLocale->images_table[0];
        CarteProjection = ImageLocale->images_table[1];
        ImagesCaptures =
            (prom_images_struct *) (ImageLocale->images_table[2]);
        Seuils = (int *) (ImageLocale->images_table[3]);
        TemporalImagesIntegration = Seuils[0];
        SeuilDetectMvt = Seuils[1];
        SeuilDetection = Seuils[2];
        Sx = ImageLocale->sx;
        Sy = ImageLocale->sy;
        Nxy = Sx * Sy;
    }
    /*
       printf("TemporalImagesIntegration=%d\tSeuilDetectMvt=%d\tSeuilDetection=%d\n",
       TemporalImagesIntegration, SeuilDetectMvt, SeuilDetection); 
     */

    /* the global tests */
    if (ImagesCaptures->image_number < 2 * TemporalImagesIntegration)
    {
        printf("\n\n The function needs %d not %d images\n",
               2 * TemporalImagesIntegration, ImagesCaptures->image_number);
        printf("Exit in function_detect_mvt_a\n");
        exit(-1);
    }
    if (ImagesCaptures->nb_band != 1)
        EXIT_ON_ERROR("There are not the black & white images!");


    /* reset of outputs */
    for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
        CarteProjection[j] = 0;
    }


    /* compute the difference image */
    for (i = 0; i < Nxy; i++)
    {
        temp1 = temp2 = 0;
        /* compute the first and second temporal integrated image */
        for (j = 0, k = TemporalImagesIntegration;
             j < TemporalImagesIntegration; j++, k++)
        {
            temp1 += (int) (ImagesCaptures->images_table[j][i]);
            temp2 += (int) (ImagesCaptures->images_table[k][i]);
        }
        temp1 = (int) abs(temp1 - temp2);
        temp1 /= TemporalImagesIntegration;
        if (temp1 > SeuilDetectMvt)
            ImageResultat[i] = (unsigned char) temp1;
        else
            ImageResultat[i] = (unsigned char) 0;
    }


    /* local initialisations */
    SizeZoneX = Sx / TailleGroupeX;
    SizeZoneY = Sy / TailleGroupeY;
    /* les projections verticales et horizontales */
    i1 = TailleGroupeY * SizeZoneY;
    j1 = TailleGroupeX * SizeZoneX;
    for (i = 0; i < i1; i++)
    {
        m = i / SizeZoneY;      /* indice Oy pour reseaux */
        for (j = 0; j < j1; j++)
        {
            l = j / SizeZoneX;  /* indice Ox pour reseaux */
            k = Sx * i + j;     /* indice dans l'image */
            n = TailleGroupeX * m + l;  /* indice dans projection */
            if (ImageResultat[k] > 0)
                CarteProjection[n] = (unsigned char) ((int) CarteProjection[n] + 1);
            /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
        }
    }


/*#define SeuilDetection 10*/
    for (i = 0; i < Nhv; i++)
        if ((int) CarteProjection[i] > SeuilDetection)
            neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / ((float) (SizeZoneX * SizeZoneY));
        else
            neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = 0.0;

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
        MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Tine in function_detect_mvt_a\t%4ld.%06d\n", SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
/*
#undef SeuilDetection
#undef SeuilDetectMvt
#undef TemporalImagesIntegration
*/
}
