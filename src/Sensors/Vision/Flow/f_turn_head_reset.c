/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_turn_head_reset.c 
\brief turn the head

Author: C Giovannangeli
Created: 01/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <libhardware.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#define DEBUG
void function_turn_head_reset(int Gpe)
{
    int i;
    int gpe_reset = -1;
    int gpe_movement = -1;
    float movement, reset;
    Joint *servo;
    int link;
    float pct;
    /* float angle_head; */
    i = 0;
    link = find_input_link(Gpe, i);
    while (link != -1)
    {
        if (strcmp(liaison[link].nom, "movement") == 0)
            gpe_movement = liaison[link].depart;
        else if (strcmp(liaison[link].nom, "reset") == 0)
            gpe_reset = liaison[link].depart;
        i++;
        link = find_input_link(Gpe, i);
    }

    servo = joint_get_serv_by_name((char*)"tilt");
    if (servo->rotation == 1)
    {
        pct =
            (float) (servo->init - servo->start) / (float) (servo->stop -
                                                            servo->start);
    }
    else
    {
        pct =
            (float) (servo->stop -
                     servo->init) / (float) (servo->stop -
                      servo->start);
    }
    /*mise a la position init */
    joint_servo_command(servo /*->name*/ , pct, 0);

    if (gpe_reset == -1 || gpe_movement == -1)
    {
        printf("error in %s\n", __FUNCTION__);
        exit(0);
    }


    movement = neurone[def_groupe[gpe_movement].premier_ele].s;
    reset = neurone[def_groupe[gpe_reset].premier_ele].s;
    servo = joint_get_serv_by_name((char*)"pan");

    if (reset > 0.5)
    {
        joint_servo_command(servo, 0.5, -1);
        printf("No input where turn, go at the beginning\n");
        return;
    }


    joint_servo_command(servo, 0.5 + movement * 180. / (float) (servo->range),
                        -1);
    printf("position:%f\n", 0.5 + movement * 180. / (float) (servo->range));
}
