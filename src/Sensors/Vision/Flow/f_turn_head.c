/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   ___NO_COMMENT___
   ___NO_SVN___

   \file 
   \brief turn the head

   Author: C Giovannangeli
   Created: 01/2005

   Theoritical description:
   - \f$  LaTeX equation: none \f$  

   Description: 

   Macro:
   -none 

   Local variables:
   -none

   Global variables:
   -none

   Internal Tools:
   -none

   External Tools: 
   -none

   Links:
   - type: algo / biological / neural
   - description: none/ XXX
   - input expected group: none/xxx
   - where are the data?: none/xxx

   Comments:

   Known bugs: none (yet!)

   Todo:see author for testing and commenting the function

   http://www.doxygen.org
************************************************************/
#define DEBUG
#include <libx.h>
#include <libhardware.h>
#include <string.h>
#include <Struct/cam_pos.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
void function_turn_head(int Gpe)
{
  int DebutGpe;
  int TailleGpe;
  float MvtNeuron;
  float Angle = 0;
  int i;
  float Maxim;
  int gpe_entree = -1;
  int gpe_head = -1;
  int gpe_compass = -1;
  float head = -1, wanted, futur_pan_pos,tilt;
  Joint *servo;
  int link;
  struct timespec duree_nanosleep,res;

  /* float angle_head; */

  if(def_groupe[Gpe].nbre!=2)
  {
    EXIT_ON_ERROR("il faut deux neurones dans function_turn_head\n");
  }

  i = 0;
  link = find_input_link(Gpe, i);
  while (link != -1)
  {
    if (strcmp(liaison[link].nom, "head") == 0)
      gpe_head = liaison[find_input_link(Gpe, i)].depart;
    else if (strcmp(liaison[find_input_link(Gpe, i)].nom, "target") == 0)
      gpe_entree = liaison[find_input_link(Gpe, i)].depart;
    else if (strcmp(liaison[find_input_link(Gpe, i)].nom, "compass") == 0)
      gpe_compass = liaison[find_input_link(Gpe, i)].depart;
    i++;
    link = find_input_link(Gpe, i);
  }


  head = neurone[def_groupe[gpe_head].premier_ele].s;
  dprints("head=%f\n", head);
  dprints("gpe_entree=%d\n", gpe_entree);

  if ((gpe_entree == -1) || (gpe_head == -1)||(gpe_compass == -1))
  {
    EXIT_ON_ERROR("ERROR f_turn_servo not connected \n");
  }
  DebutGpe = def_groupe[gpe_entree].premier_ele;
  TailleGpe = def_groupe[gpe_entree].nbre;


  /*! reset all group activation */
  /*for(i=TailleGpe; i--; )
    neurone[DebutGpe+i].s=neurone[DebutGpe+i].s1=neurone[DebutGpe+i].s2 = 0.0;

    *! compute the group's neuron.s input activity and find the maxim input activity */
  MvtNeuron = -1;
  Maxim = 0.0;
  for (i = 0; i < TailleGpe; i++)
  {
    if (neurone[DebutGpe + i].s > Maxim)
    {
      Maxim = neurone[DebutGpe + i].s;
      MvtNeuron = i;
    }
  }
  dprints("neuron max : %f : %f\n", Maxim, MvtNeuron);
  /** If we detect an input activity, turn in the focus point. The direction is between -1 (left) 
   *  and 1 (right).  Turn in the focus point direction; the field of view is about 70 degrees so 
   *  the half is 35. Set the winner neuron to 1.0
   */
  if (isequal(MvtNeuron, -1))
  {
    cprints("No input where turn, go at the compass value\n");
		
    DebutGpe = def_groupe[gpe_compass].premier_ele;
    TailleGpe = def_groupe[gpe_compass].nbre;
	
	
    /*! reset all group activation */
    /*for(i=TailleGpe; i--; )
      neurone[DebutGpe+i].s=neurone[DebutGpe+i].s1=neurone[DebutGpe+i].s2 = 0.0;
	
      *! compute the group's neuron.s input activity and find the maxim input activity */
    MvtNeuron = -1;
    Maxim = 0.0;
    for (i = 0; i < TailleGpe; i++)
    {
      if (neurone[DebutGpe + i].s > Maxim)
      {
	Maxim = neurone[DebutGpe + i].s;
	MvtNeuron = i;
      }
    }
		
    DebutGpe = def_groupe[gpe_entree].premier_ele;
    TailleGpe = def_groupe[gpe_entree].nbre;
  }
	
  wanted = MvtNeuron / ((float) TailleGpe) + (0.5 / (float) TailleGpe);
  dprints("wanted=%f , head = %f\n", wanted, head);
  Angle = -head + wanted;
  while (Angle >= 0.5 || Angle < -0.5)
  {
    if (Angle >= 0.5)
      Angle = Angle - 1;
    else if (Angle < -0.5)
      Angle = Angle + 1;
  }
  dprints("angle = %f\n", Angle);

  printf("Turn the servo motor of %f degres \n", Angle * 360);
  servo = joint_get_serv_by_name("pan");
  futur_pan_pos = servo->position + Angle * 360 / servo->range;

  if ( futur_pan_pos > 1. ) {
    cprints("### PLOP #### : %f\n",futur_pan_pos);
    futur_pan_pos=((futur_pan_pos-0.5)*servo->range-360.)/servo->range+0.5;
    cprints("### PLOP2 #### : %f\n",futur_pan_pos);
  }
  else if (  futur_pan_pos < 0.) {
    cprints("### PLOP #### : %f\n",futur_pan_pos);
    futur_pan_pos=((futur_pan_pos-0.5)*servo->range+360.)/servo->range+0.5;
    cprints("### PLOP2 #### : %f\n",futur_pan_pos);
  }

  /*futur_pan_pos=   (get_PTposition()).servo_pan  +  Angle *  360* ( get_camstop()-get_camstart() )/AngleDebatement; */
/* #ifdef DEBUG */
  cprints("pan pos : %f\n", servo->position);
  cprints("futur panpos = %f\n", futur_pan_pos);
/* #endif   */    
  duree_nanosleep.tv_nsec =0;
  duree_nanosleep.tv_sec = 0;

  if(Angle>60.||Angle<-60.)
  {
    duree_nanosleep.tv_sec = 0;
    duree_nanosleep.tv_nsec = 25000000;
/*         	joint_servo_command(servo, futur_pan_pos, -1);*/
  }
	
  joint_servo_command(servo, futur_pan_pos, -1);
  nanosleep(&duree_nanosleep, &res);


  /* 
     if((futur_pan_pos<get_camstart())||(futur_pan_pos>get_camstop()))
     {    
     Angle = body-wanted;

     while(Angle>=0.5||Angle<-0.5)
     {
     if(Angle>=0.5)
     Angle=Angle-1;
     else if (Angle<-0.5)
     Angle=Angle+1;
     }

     printf("futur rotation = %f\n",2*Angle);
     pan((get_camstop()-get_camstart())/2);

     if(Angle>0)
     {    pan(get_camstart());
     turn_right(Angle*360+( (get_camstop()-get_camstart())/2 - get_camstart() -5) *AngleDebatement/ (get_camstop()-get_camstart()));              
     }
     else
     { 
     pan(get_camstop());
     turn_left(-Angle*360+( get_camstop() - (get_camstop()-get_camstart())/2 -5) *AngleDebatement/ (get_camstop()-get_camstart()));               

     }
     }
     else
     {
     pan(futur_pan_pos);
     }
  */
  /*   }
       else
       {		
		
    
       }*/
	
  tilt=(joint_get_serv_by_name("tilt"))->position;

  neurone[def_groupe[Gpe].premier_ele].s=neurone[def_groupe[Gpe].premier_ele].s1=neurone[def_groupe[Gpe].premier_ele].s2=futur_pan_pos;
  neurone[def_groupe[Gpe].premier_ele+1].s=neurone[def_groupe[Gpe].premier_ele+1].s1=neurone[def_groupe[Gpe].premier_ele+1].s2=tilt;

}
