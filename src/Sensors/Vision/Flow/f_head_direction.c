/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file 
\brief give the direction of the pan of neuro

Author: C Giovannangeli
Created: 01/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <libhardware.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
 /* #define DEBUG */
typedef struct My_Data_f_head_direction
{
    int gpe_entree;
} My_Data_f_head_direction;

void function_head_direction(int Gpe)
{
    /*int DebutGpe ;
       int TailleGpe; */
    /* int MvtNeuron;
       int Angle = 0;
       int i;
       float Maxim;
       float Temp2; */
    float /*Degree, */ dec_h, dec_v;
    Joint *servo_pan;
    Joint *servo_tilt;
    /* type_coeff *CoeffTemp; */
    int gpe_entree = -1;
    float compass, head, head_v;

    int i, l;
    My_Data_f_head_direction *my_data = NULL;

    if (def_groupe[Gpe].data == NULL)
    {

        gpe_entree = -2;
        i = 0;
        l = find_input_link(Gpe, i);
        if (l == -1)
        {
            printf("erruer dans %s, pas de groupe en entee\n", __FUNCTION__);
            exit(0);
        }
        gpe_entree = liaison[l].depart;
        my_data =
            (My_Data_f_head_direction *)
            malloc(sizeof(My_Data_f_head_direction));
        my_data->gpe_entree = gpe_entree;
        def_groupe[Gpe].data = (My_Data_f_head_direction *) my_data;
    }
    else
    {

        my_data = (My_Data_f_head_direction *) def_groupe[Gpe].data;
        gpe_entree = my_data->gpe_entree;
    }

    compass = neurone[def_groupe[gpe_entree].premier_ele].s;


    servo_pan = joint_get_serv_by_name((char*)"pan");
#ifdef DEBUG
    printf("actuel pos:%f\nrange:%d\n", servo_pan->position,
           servo_pan->range);
#endif
    dec_h = (servo_pan->position - 0.5) * servo_pan->range / 360.;
    /* #ifdef DEBUG */

    servo_tilt = joint_get_serv_by_name((char*)"tilt");
#ifdef DEBUG
    printf("actuel pos:%f\nrange:%d\n", servo_tilt->position,
           servo_tilt->range);
#endif
    dec_v = (servo_tilt->position - 0.5) * servo_tilt->range / 360.;

#ifdef DEBUG
    printf("dec=%f\n", dec_h);
#endif
    head = compass + dec_h;
    while (head < 0 || head >= 1)
    {
        if (head >= 1)
            head = head - 1;
        if (head < 0)
            head = head + 1;
    }

    head_v = 0.5 + dec_v;

    if (head_v >= 1)
        head_v = head_v - 1;
    if (head_v < 0)
        head_v = head_v + 1;

    neurone[def_groupe[Gpe].premier_ele].s =
        neurone[def_groupe[Gpe].premier_ele].s1 =
        neurone[def_groupe[Gpe].premier_ele].s2 = head;

    if (def_groupe[Gpe].nbre == 2)
        neurone[def_groupe[Gpe].premier_ele + 1].s =
            neurone[def_groupe[Gpe].premier_ele + 1].s1 =
            neurone[def_groupe[Gpe].premier_ele + 1].s2 = head_v;

}
