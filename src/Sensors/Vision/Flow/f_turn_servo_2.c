/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_turn_servo_2.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-CAMERA_VISION_ANGLE

Local variables:
-none

Global variables:
-int USE_CAM
-int CAM_MODE

Internal Tools:
-WaitASecond()
-GetAngleServoInt()
-SetServoIntDirection()

External Tools: 
-tools/IO_Robot/Pan_Tilt/pan()
-tools/IO_Robot/Pan_Tilt/return_pan_pos()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <sys/time.h>
#include <limits.h>
#include "tools/include/macro.h"

#include <Global_Var/IO_Robot.h>

#include <libhardware.h>

#include "tools/include/GetAngleServoInt.h"
#include "tools/include/SetServoIntDirection.h"
void WaitASecond(float WaitLap);


void WaitASecond(float WaitLap)
{
#define IsAltB(a,b,res) ( ((a.tv_sec)>(b.tv_sec)) ? (res=FALSE) : ( ((a.tv_sec)==(b.tv_sec)) ?  ( (a.tv_usec)<(b.tv_usec) ? ((res)=TRUE) : ((res)=FALSE) ): ((res)=TRUE) ) );

    struct timeval TimeWait, Time0, TimeTemp;
    long Xsec, Xusec, Maxusec, Minusec;
    int r;

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    gettimeofday(&Time0, (void *) NULL);

    Xsec = (long) floor(WaitLap);
    Xusec = (long) (floor((WaitLap - (float) Xsec) * 1000000.0));

    /*printf("%7ld\t%7ld\n",Xsec,Xusec); */

    TimeWait.tv_sec = Time0.tv_sec + Xsec;

    if (Xusec < Time0.tv_usec)
    {
        Maxusec = Time0.tv_usec;
        Minusec = Xusec;
    }
    else
    {
        Minusec = Time0.tv_usec;
        Maxusec = Xusec;
    }

    if (Maxusec >= LONG_MAX / 2)
    {
        Maxusec = Maxusec - LONG_MAX / 2;
        if ((Maxusec + Minusec) > LONG_MAX / 2)
        {
            TimeWait.tv_sec = TimeWait.tv_sec + 1;
            TimeWait.tv_usec = Maxusec + Minusec - LONG_MAX / 2;
        }
        else
            TimeWait.tv_usec = Time0.tv_usec + Xusec;
    }
    else
        TimeWait.tv_usec = Time0.tv_usec + Xusec;

    for (;;)
    {
        gettimeofday(&TimeTemp, (void *) NULL);
        IsAltB(TimeWait, TimeTemp, r);
        if (r == TRUE)
            break;
    }
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Tine in WaitASecond\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}

void function_turn_servo_2(int Gpe)
{
    int DebutGpe, TailleGpe;
    int DebutGpeAmont, TailleGpeAmont;
    int MvtNeuron;
    int GpeAmont;
    int Angle;
    int i;
    float Degree;
    float Maxim;
    float WaitTemps = 0.0;

    printf
        ("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ",
         __FUNCTION__);
    exit(0);


#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* check the amont group and get info's about him */
    GpeAmont = -1;
    for (i = 0; i < nbre_liaison; i++)
        if ((liaison[i].arrivee == Gpe))
        {
            if (liaison[i].nom[0] == 'S')
            {
                sscanf(liaison[i].nom, "S%f", &WaitTemps);
                GpeAmont = liaison[i].depart;
            }
            break;
        }
    if (GpeAmont == -1)
    {
        printf
            ("Liasion innexistante !\n (Exit in function_commande_servo_2)\n");
        exit(-1);
    }

    DebutGpe = def_groupe[Gpe].premier_ele;
    TailleGpe = def_groupe[Gpe].nbre;
    DebutGpeAmont = def_groupe[GpeAmont].premier_ele;
    TailleGpeAmont = def_groupe[GpeAmont].nbre;


    /*! reset all neuron activity */
    for (i = DebutGpe; i < DebutGpe + TailleGpe; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;


    /*! find the maximum input */
    Maxim = 0.0;
    MvtNeuron = -1;
    for (i = DebutGpeAmont; i < DebutGpeAmont + TailleGpeAmont; i++)
        if (neurone[i].s > Maxim)
        {
            Maxim = neurone[i].s;
            MvtNeuron = i - DebutGpeAmont;
        }


    if (MvtNeuron != -1)
    {
        Degree = ((float) MvtNeuron) / ((float) TailleGpeAmont - 1.0);
        Degree = 2 * Degree - 1.0;
        Degree = Degree * CAMERA_VISION_ANGLE;
        Angle = (int) Degree;

        /*

           if (camera_get_first_camera()->camera_type == 1)          
           {SetServoIntDirection(get_angleservoint()+Angle);
           Angle = get_angleservoint() + 90;
           i = (int) floor( (double)TailleGpe * (double)Angle / 180.0 );
           }
           if (camera_get_first_camera()->camera_type == 2) 
           { 
           Angle = return_pan_pos() + (int)((Angle*get_cammode())/255);
           pan(Angle);
           i = (int) floor( (double)TailleGpe * (double)Angle / 255.0 );
           }
         */


        neurone[i + DebutGpe].s = neurone[i + DebutGpe].s1 =
            neurone[i + DebutGpe].s2 = 1.0;
    }


    if (WaitTemps > 0.0)
        WaitASecond(WaitTemps);
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_commande_servo_2\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
#ifdef ANGLE_TRACE
    printf("\t\t Neurone %d activ dans le gpe %d\n", i, Gpe);
#endif

}
