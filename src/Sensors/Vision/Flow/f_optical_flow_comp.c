/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libSensors
\defgroup f_optical_flow_comp f_optical_flow_comp

\brief calcul du flot optique

\section Modified 
\author Syed Khursheed HASNAIN.
\date Feb. 2012

\details

Introduce an algorthmic link named "enable". It is an optional link. If it is not used optical flow works as previous. But if enable link is used than (enable = 0) resets the optical flow output and (enable = 1) works normally. 

\section Option
- 	enable : optional, this link will reset the optical flow if the input neuron is 0.

\file
\ingroup f_optical_flow_comp f_optical_flow_comp

*/

/* #define DEBUG */

#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

#define PI 3.14159265

typedef struct MyData_f_optical_flow_comp 
{

	int forward_comp, reverse_comp;
	int gpe_input_flow;
	prom_images_struct *prom_im_output;
} MyData_f_optical_flow_comp;


void function_optical_flow_comp(int Gpe)
{
	int l, i;
	int largeur = 0, hauteur = 0,n=0;
	int gpe_input_flow = -1; 
	float F=0., R=0., FR = 0.0;
	char resultat[256];

	float *U= NULL,*V= NULL,*N= NULL,*D= NULL,*U_input= NULL,*V_input= NULL ;

	prom_images_struct *prom_im_output = NULL,*prom_im_input = NULL ;

	MyData_f_optical_flow_comp *my_data;


	
/* ****** modified  shifted start */

my_data =(MyData_f_optical_flow_comp *) malloc(sizeof(MyData_f_optical_flow_comp));
		if (my_data == NULL)
			EXIT_ON_ERROR("erreur malloc dans f_optical_flow_comp\n");


/* ****** modified   shifted stop    */


     
    /* Recherche des gpes d'entreee  */
	if (def_groupe[Gpe].data == NULL)
	{
		i = 0;
		l = find_input_link(Gpe, i);
		while (l != -1)
		{
		 	 
			dprints("lien %d: %s--\n",i,liaison[l].nom); 
			
			if (prom_getopt(liaison[l].nom, "-optical", resultat) == 1)
			{
				gpe_input_flow = liaison[l].depart;
				U_input = (float *) (((prom_images_struct *) def_groupe[gpe_input_flow].ext)->images_table[0]);
				V_input = (float *) (((prom_images_struct *) def_groupe[gpe_input_flow].ext)->images_table[1]);
			}

			if (prom_getopt(liaison[l].nom, "-F", resultat) == 1)
			{
				my_data->forward_comp = liaison[l].depart;
				
			}
			if (prom_getopt(liaison[l].nom, "-R", resultat) == 1)
			{
				my_data->reverse_comp = liaison[l].depart; 
							

			}

			i++;
			l = find_input_link(Gpe, i);
		}
		if (gpe_input_flow == -1)
			EXIT_ON_ERROR("%s needs an optical flow group as input \n",def_groupe[Gpe].no_name);
		
			/* Test pour voir si les images sont vides */		
		if (U_input == NULL || V_input  == NULL)
		{
			kprints ("Probleme %s : il n'y pas d'image dans le groupe %s \n",
					def_groupe[Gpe].no_name,def_groupe[gpe_input_flow].no_name );
			return;
		}

		largeur = ((prom_images_struct *) def_groupe[gpe_input_flow].ext)->sx;		
		hauteur = ((prom_images_struct *) def_groupe[gpe_input_flow].ext)->sy;
		n = largeur*hauteur;
		/* Allocation memoire pour l'image resultat */
		if (def_groupe[Gpe].ext == NULL)
		{
			prom_im_output = calloc_prom_image(4, largeur, hauteur, 4);		/* copie z-1 image unsigned char en entree It*/	
			prom_im_output->images_table[0] = (unsigned char *) calloc(n, sizeof(float)); /*allocate the first table*/
			prom_im_output->images_table[1] = (unsigned char *) calloc(n, sizeof(float)); /*allocate the first table*/
			prom_im_output->images_table[2] = (unsigned char *) calloc(n, sizeof(float)); /*allocate the first table*/
			prom_im_output->images_table[3] = (unsigned char *) calloc(n, sizeof(float)); /*allocate the first table*/
			def_groupe[Gpe].ext = prom_im_output;
		}	
		
		my_data->gpe_input_flow = gpe_input_flow; 
		def_groupe[Gpe].data = (MyData_f_optical_flow_comp *) my_data; 
		
	}
	else
	{
		my_data = (MyData_f_optical_flow_comp *) (def_groupe[Gpe].data); /*restore all the datas*/
		gpe_input_flow = my_data->gpe_input_flow;
		/*restore the .ext*/
		prom_im_output = (prom_images_struct *) def_groupe[Gpe].ext;
		
	}
/*until here we have all datas and the prom_output with all the tables*/
	
	largeur = ((prom_images_struct *) def_groupe[gpe_input_flow].ext)->sx;		
	hauteur = ((prom_images_struct *) def_groupe[gpe_input_flow].ext)->sy;
	n = largeur*hauteur;
	if(my_data->forward_comp != -1)
		F= neurone[def_groupe[my_data->forward_comp].premier_ele].s1;
	if(my_data->reverse_comp != -1)
		R= neurone[def_groupe[my_data->reverse_comp].premier_ele].s1;
	FR = F+R;

	prom_im_input= (prom_images_struct *) def_groupe[gpe_input_flow].ext;
	prom_im_output->sx = largeur;
	prom_im_output->sy = hauteur;
	prom_im_output->nb_band = ((prom_images_struct *) def_groupe[gpe_input_flow].ext)->nb_band;
	U_input =  (float*) prom_im_input->images_table[0];
	V_input =  (float*) prom_im_input->images_table[1];

	U = (float*) prom_im_output->images_table[0];
	V = (float*) prom_im_output->images_table[1];
	N = (float*) prom_im_output->images_table[2];
	D = (float*) prom_im_output->images_table[3];
	
/*just copy the vertical direction input to the table 2*/
//float *U= NULL,*V= NULL,*N= NULL,*D= NULL 
	memcpy(V,V_input,n*sizeof(float));

	for (i=n; i--; ) 
	{	 	
	   U[i]=U_input[i]+FR;
	   /*V[i]=V_input[i];*/
	   N[i]=sqrt(U[i]*U[i] + V[i]*V[i]);
	}	
		/*******************************************************/
	
	for (i=n; i--; ) 
	{	 
	   D[i]= (atan2 (V[i],U[i]) * 180. / PI )+ 180.;
	}	

}

