/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\brief 
	Flot optique hiérarchique

	sx,sy 
	correspondent  aux dimensions de l'image sous-échantillonnée

	Ox, Oy 
	correspondent  aux dimensions de l'image originale

	U,V 
	correspondent aux vecteurs vitesses qu'on trouvera en sortie de la fonction .

	SU,SV 
	correspondent aux vecteurs vitesses de l'image sous-échantillonnée.

	OU,OV 
	correspondent aux vecteurs vitesses du niveau hiérarchique précédent. 

	-alpha 
	permet de donner plus ou moins de poids aux SU SV par rapport aux OU OV dans l'interpolation.

	-N : Spécifie la présence d'une 3ème image supplémentaire contenant la norme des vecteurs vitesses


	-D : Spécifie la présence d'une 4ème image supplémentaire contenant leurs directions :
 *         270°
 * 	180°	    0°
 * 		  90°
 *


	Dans zhg,zhgi,zbd,zbdi... z désigne les points déjà interpolés, h=haut, b=bas, g=gauche, d=droite.

	On considère que les vecteurs vitesses du niveau hiérarchique bas(l'image sous-échantillonné) sont déjà interpolés.
 */

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>

#define PI 3.14159265

typedef struct MyData_f_interpolation_2D
{
   int InputGpe1;
   int InputGpe2, version, direction, norme;
   prom_images_struct *prom_SSampled, *prom_Original;
   prom_images_struct *prom_OutputImage;
   float *U, *V, *SU, *SV, *OU, *OV, *D, *N;
   float alpha;

}MyData_f_interpolation_2D;


void function_interpolation_2D(int Gpe)
{
   int InputGpe1 = -1;
   int InputGpe2 = -1;
   int direction = 0, norme = 0;
   prom_images_struct *prom_SSampled=NULL, *prom_Original=NULL;
   prom_images_struct *prom_OutputImage;
   int sx, sy, Ox, Oy,i,j,version=1;
   float *U, *V, *SU, *SV, *OU, *OV, *D, *N;
   float beta,div,div2,alpha=1;
   MyData_f_interpolation_2D *My_data;
   int row, column;
   int zhg,zhd,zbg,zbd,k;
   int zhgi,zhdi /*,zbgi,zbdi*/;
   int zh,zb,zg,zd;
   int zh2,zb2,zg2,zd2,k2;

   char param[256];

   if (def_groupe[Gpe].data == NULL)
   {
      i=0;
      /* récupération des paramètres*/
      while((j=find_input_link(Gpe,i))!=-1)
      {

         if(prom_getopt(liaison[j].nom, "-O", param) == 1)
         {
            InputGpe2 = liaison[j].depart;
            prom_Original = (prom_images_struct *) def_groupe[InputGpe2].ext;
         }

         if (prom_getopt(liaison[j].nom, "-SE", param) == 1)
         {
            InputGpe1 = liaison[j].depart;
            prom_SSampled = (prom_images_struct *) def_groupe[InputGpe1].ext;
         }

         if (prom_getopt(liaison[j].nom, "-alpha", param) == 2)
         {
            alpha = atof(param);
            kprints("valeur de alpha %f\n",alpha);
         }
         if (prom_getopt(liaison[j].nom, "-V", param) == 2)
         {
            version = atoi(param);
            if(version==1)
               kprints("interpolation bicubique\n");
            else
               kprints("interpolation bilinéaire\n");
         }

         if (prom_getopt(liaison[j].nom, "-N", param) == 1)
         {
            norme = 1;
         }

         if (prom_getopt(liaison[j].nom, "-D", param) == 1)
         {
            direction = 1;
         }

         i++;

      }
      /*vérification de la présence de groupes en amont*/
      if ((InputGpe1 == -1)||(InputGpe2 == -1) || (prom_SSampled == NULL) || (prom_Original == NULL) )
      {
         EXIT_ON_GROUP_ERROR(Gpe, " pas assez de groupe amont...\n");
      }


      /*verif d'une carte entrante */
      if ((def_groupe[InputGpe1].ext == NULL)||(def_groupe[InputGpe2].ext == NULL))
      {
         EXIT_ON_GROUP_ERROR(Gpe, "pointeur null sur un des liens entrants\n");
      }


      sx = prom_SSampled->sx;		sy = prom_SSampled->sy;
      Ox = prom_Original->sx;		Oy = prom_Original->sy;

      /*vérification de la compatibilité entre les deux images*/
      if(!((Ox/2==sx)&&(Oy/2==sy)))
      {
         EXIT_ON_GROUP_ERROR(Gpe, " L'image original n'est pas 2 fois plus grande que l'image sousEchant \n");
      }

      My_data = ALLOCATION(MyData_f_interpolation_2D);
      def_groupe[Gpe].data = My_data;


      /* TODO : Allocation et remplissage des 2 images supplémentaires N et D que si direction == 1 ! */

      /*allocation de mémoire pour l'image résultante*/
      prom_OutputImage = calloc_prom_image(4, Ox, Oy, 4);

      if(prom_OutputImage==NULL)
      {
         EXIT_ON_GROUP_ERROR(Gpe, "Erreur de malloc dans f_remonte_flot (prom_OutputImage)\n");
      }



      def_groupe[Gpe].ext = (prom_images_struct *)prom_OutputImage;

      My_data->U=U =  (float *)prom_OutputImage->images_table[0];
      My_data->V=V =  (float *)prom_OutputImage->images_table[1];
      My_data->N=N =  (float *)prom_OutputImage->images_table[2];
      My_data->D=D =  (float *)prom_OutputImage->images_table[3];
      My_data->SU=SU= (float *)prom_SSampled->images_table[0];
      My_data->SV=SV= (float *)prom_SSampled->images_table[1];
      My_data->OU=OU= (float *)prom_Original->images_table[0];
      My_data->OV=OV= (float *)prom_Original->images_table[1];
      My_data->InputGpe1=InputGpe1;
      My_data->InputGpe2=InputGpe2;
      My_data->prom_SSampled=prom_SSampled;
      My_data->prom_Original=prom_Original;
      My_data->prom_OutputImage=prom_OutputImage;
      My_data->version=version;
      My_data->alpha=alpha;
      My_data->direction=direction;
      My_data->norme=norme;
   }
   else
   {
      My_data = (MyData_f_interpolation_2D *) def_groupe[Gpe].data;

      U = My_data->U;
      V = My_data->V;
      N = My_data->N;
      D = My_data->D;
      SU= My_data->SU;
      SV= My_data->SV;
      OU= My_data->OU;
      OV= My_data->OV;
      prom_SSampled=My_data->prom_SSampled;
      prom_Original=My_data->prom_Original;
      version=My_data->version;
      alpha=My_data->alpha;
      direction=My_data->direction;
      norme=My_data->norme;
      sx = prom_SSampled->sx;		sy = prom_SSampled->sy;
      Ox = prom_Original->sx;		Oy = prom_Original->sy;
   }


   beta = 1.-alpha;
   div  = 3*alpha+1.;
   div2 = alpha+1.;
   if(version==1)
   {
      /*boucles pour l'interpolation bicubique*/

      /* interpolation des vecteurs */

      /*	on doit arriver à ceci après les  deux boucles "for" imbriquées "1" et "2" représentent l'ordre dans lequel est calculé le pixel

			{2,0,2,0,2,0,2,0,2,0 "matrice contenant l'image de sortie par exemple U"
			 0,1,0,1,0,1,0,1,0,0
			 2,0,2,0,2,0,2,0,2,0
			 0,1,0,1,0,1,0,1,0,0
			 2,0,2,0,2,0,2,0,2,0}	*/
      /* Les vecteurs représentés par les "1" sont calculés et insérés dans l'image de sortie
		puis les 4 vecteurs adjacents représentés par les "2" sont à leur tour insérés	*/ 
      for (row = 1; row <= Oy - 2; row+=2)
      {
         for (column = 1; column <= Ox - 2; column+=2)
         {
            /*Calcul des indices des données dans l'image du niveau hierarchique précedent*/
            zhg=((row-1)/2) * sx + (column-1)/2;
            zhd=((row-1)/2) * sx + (column+1)/2;
            zbg=((row+1)/2) * sx + (column-1)/2;
            zbd=((row+1)/2) * sx + (column+1)/2;
            /****************************************/

            k=row*Ox+column;/*indice du pixel où vont être placés les données*/

            U[k]=(OU[k]*beta+alpha*(SU[zhg]*2+SU[zhd]*2+SU[zbg]*2 + SU[zbd]*2))/div;
            V[k]=(OV[k]*beta+alpha*(SV[zhg]*2+SV[zhd]*2+SV[zbg]*2 + SV[zbd]*2))/div;


            if(norme)
               N[k]=(sqrt(U[k]*U[k] + V[k]*V[k]));

            if(direction){
               if( isequal( U[k], 0. ) ){
                  if( isequal( V[k], 0. ) )
                     D[k] = 0.;
                  else
                     D[k] = 90.;
               }else
                  D[k]= atan2 (V[k],U[k]) * 180. / PI + 180.;
            }else
               D[k]= 0.;

            /* recopie des vecteurs vitesses du niveau hiérarchique bas dans l'image de sortie*/
            /* ils sont replacés une ligne sur deux et une colonne sur deux en commençant par le pixel (0,0) (en haut à gauche)*/
            zhgi=(row-1)*Ox+column-1;
            zhdi=(row-1)*Ox+column+1;
            /*zbgi=(row+1)*Ox+column-1;
				zbdi=(row+1)*Ox+column+1;*/
            U[zhgi]=SU[zhg]*2;
            U[zhdi]=SU[zhd]*2;

            V[zhgi]=SV[zhg]*2;
            V[zhdi]=SV[zhd]*2;

            if(norme){
               N[zhgi]=(sqrt(U[zhgi]*U[zhgi] + V[zhgi]*V[zhgi]));
               N[zhdi]=(sqrt(U[zhdi]*U[zhdi] + V[zhdi]*V[zhdi]));
            }

            if(direction){
               if( isequal( U[zhgi], 0. ) ){
                  if( isequal( V[zhgi], 0. ) )
                     D[zhgi] = 0.;
                  else
                     D[zhgi] = 90.;
               }else
                  D[zhgi]= atan2 (V[zhgi],U[zhgi]) * 180. / PI + 180.;
            }else
               D[zhgi]= 0.;

            if(direction){
               if( isequal( U[zhdi], 0. ) ){
                  if( isequal( V[zhdi], 0. ) )
                     D[zhdi] = 0.;
                  else
                     D[zhdi] = 90.;
               }else
                  D[zhdi]= atan2 (V[zhdi],U[zhdi]) * 180. / PI + 180.;
            }else
               D[zhdi]= 0.;

         }
      }

      /* on doit arriver à ceci après les 2 boucles "for" imbriquées

			{2,0,2,0,2,0,2,0,2,0
			 0,1,3,1,3,1,3,1,3,0
			 2,4,2,4,2,4,2,4,2,0
			 0,1,3,1,3,1,3,1,3,0
			 2,0,2,0,2,0,2,0,2,0}
       */
      /* */

      for (row = 1; row <= Oy - 2; row+=2)
      {
         for (column = 2; column <= Ox - 2; column+=2)
         {
            /* lignes impaires*/
            zh=((row-1)/2) * sx + column/2;
            zb=((row+1)/2) * sx + column/2;
            zg=row*Ox + column-1;
            zd=row*Ox + column+1;
            k=row*Ox + column;
            U[k]=(OU[k]*beta+(alpha*(U[zg]+U[zd] + SU[zh]*2 + SU[zb]*2)))/div;
            V[k]=(OV[k]*beta+(alpha*(V[zg]+V[zd] + SV[zh]*2 + SV[zb]*2)))/div;

            if(norme)
               N[k]=(sqrt(U[k]*U[k] + V[k]*V[k]));

            if(direction){
               if( isequal( U[k], 0. ) ){
                  if( isequal( V[k], 0. ) )
                     D[k] = 0.;
                  else
                     D[k] = 90.;
               }else
                  D[k]= atan2 (V[k],U[k]) * 180. / PI + 180.;
            }else
               D[k]= 0.;

            zh = D[k];


            /* lignes paires*/
            zh2=(row/2) * sx + (column-1)/2;
            zb2=((row+2)/2) * sx + (column-1)/2;
            zg2=(row+1)*Ox + column-2;
            zd2=(row+1)*Ox + column;
            k2=(row+1)*Ox + column-1;

            U[k2]=(OU[k2]*beta+(alpha*(U[zg2]+U[zd2] + SU[zh2]*2 + SU[zb2]*2)))/div;
            V[k2]=(OV[k2]*beta+(alpha*(V[zg2]+V[zd2] + SV[zh2]*2 + SV[zb2]*2)))/div;

            if(norme)
               N[k2]=(sqrt(U[k2]*U[k2] + V[k2]*V[k2]));

            if(direction){
               if( isequal( U[k2], 0. ) ){
                  if( isequal( V[k2], 0. ) )
                     D[k2] = 0.;
                  else
                     D[k2] = 90.;
               }else
                  D[k2]= atan2 (V[k2],U[k2]) * 180. / PI + 180.;
            }else
               D[k2]= 0.;

            zh2=D[k2];

            if(zh > 255 || zh2 > 255 )
               zb = zh;
            if(zh > 180 || zh2 > 180 )
               zb = zh;
            if(zh < 0 || zh2 < 0 )
               zb = zh;

         }
      }
      zb++;
      zb--;
   }
   else
   {
      /*interpolation bilinéaire */



      /*{2,0,2,0,2,0,2,0,2,0
			 1,0,1,0,1,0,1,0,1,0
			 2,0,2,0,2,0,2,0,2,0
			 1,0,1,0,1,0,1,0,1,0
			 2,0,2,0,2,0,2,0,2,0}*/

      for (row = 0; row < Oy - 3; row+=2)
      {
         for (column = 0; column < Ox - 2; column+=2)
         {
            k=(row+1)*Ox + column;
            zh=(row/2)*sx + column/2;
            zb=((row+2)/2)*sx + column/2;
            U[k]=(OU[k]*beta + (SU[zh]+SU[zb])*2*alpha)/div2;
            V[k]=(OV[k]*beta + (SV[zh]+SV[zb])*2*alpha)/div2;

            k=row*Ox + column;
            k2=(row+2)*Ox + column;
            U[k]=SU[zh]*2;
            /*U[k2]=SU[zb]*2;*/
            V[k]=SV[zh]*2;
            /*V[k2]=SV[zb]*2;*/
         }
      }


      /*{2,3,2,3,2,3,2,3,2,0
			 1,3,1,3,1,3,1,3,1,0
			 2,3,2,3,2,3,2,3,2,0
			 1,3,1,3,1,3,1,3,1,0
			 2,3,2,3,2,3,2,3,2,0}*/

      for (column = 0; column < Ox - 2; column+=2)
      {
         for (row = 0; row < Oy - 2; row++)
         {
            k=row*Ox + column +1;
            zg=row*Ox + column;
            zd=row*Ox + column +2;
            U[k]=(OU[k]*beta + (U[zg]+U[zd])*alpha)/div2;
            V[k]=(OV[k]*beta + (V[zg]+V[zd])*alpha)/div2;


         }
      }
   }

}
