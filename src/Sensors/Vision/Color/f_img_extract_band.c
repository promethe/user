/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_img_extract_band f_img_extract_band

\brief Convert an RGB image into a YCbCr image

\section Modified
- author: D BAILLY
- description: creation
- date: 03/02/2012

\details
  Cette fonction converti une image RGB en une image YCbCr

\section Options
- un lien sans option particulière en provenance du groupe avec l'image est
          nécessaire
- -sync : option pour synchroniser cette boîte avec l'éxécution de la
        boîte précédente

\section Todo
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
typedef struct data_img_extract_band
{
   int gpe_image;
   int band;
} Data_img_extract_band;


/*******************************************************************
 * Les fonctions d'appel
 ******************************************************************/
void function_img_extract_band (int Gpe)
{
   int i = 0, j = 0, nb_pixel;
   int l = -1;
   int gpe_image=-1, band=-1;
   Data_img_extract_band *data;
   prom_images_struct *image_struct_input = NULL, *image_struct_output = NULL;
   unsigned char *image_in, *image_out;
   char opt[256];


   if (def_groupe[Gpe].data == NULL)
   {
      /*Finding the link */
      l = find_input_link(Gpe, i);
      while (l != -1)
      {
         if (strstr(liaison[l].nom, "sync") != NULL)
            continue;
         if (prom_getopt(liaison[l].nom, "-b", opt) == 2)
         {
            band = atoi(opt);
            gpe_image = liaison[l].depart;
         }
         i++;
         l = find_input_link(Gpe, i);
      }
      /* Pour verifier l'existence du lien */
      if (gpe_image == -1)        EXIT_ON_ERROR("le groupe n'a pas d'entree !");
      if (band < 0 || band > 2)   EXIT_ON_ERROR("la bande specifiee doit etre 0 1 ou 2");

      /*allocation memoire pour la sauvegarde des donnees sur le lien */
      data = (Data_img_extract_band*)ALLOCATION(Data_img_extract_band);

      /* parametres par default */
      data->gpe_image = gpe_image;
      data->band = band;
      def_groupe[Gpe].data = data;
   }
   data = def_groupe[Gpe].data;
   image_struct_input = def_groupe[data->gpe_image].ext;
   band = data->band;

   if (image_struct_input == NULL)
   {
      PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
      return ;
   }

   if (def_groupe[Gpe].ext == NULL)
   {
      /*image couleur du groupe precedent */
      image_struct_input = (prom_images_struct *) def_groupe[data->gpe_image].ext;

      if (image_struct_input->nb_band != 3)   EXIT_ON_ERROR("L'image en entree doit etre en couleur");

      /* allocation de memoire */
      image_struct_output = (void *) calloc_prom_image(image_struct_input->image_number, image_struct_input->sx, image_struct_input->sy, 1);
      if (image_struct_output == NULL)   EXIT_ON_ERROR("impossible d'allouer la memoire");
      def_groupe[Gpe].ext = image_struct_output;
   }
   image_struct_output = def_groupe[Gpe].ext;

   nb_pixel = image_struct_input->sx * image_struct_input->sy;
   for (j = 0; j < image_struct_output->image_number; j++)
   {
      image_in = image_struct_input->images_table[j];
      image_out = image_struct_output->images_table[j];
      for (i = 0; i < nb_pixel; i++)
      {
         image_out[i] = image_in[3 * i + band];
      }
   }
}

