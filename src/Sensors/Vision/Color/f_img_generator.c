/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\ingroup libSensors
\defgroup f_img_couleur_selection f_img_couleur_selection

\brief

\section Modified

Author: Philippe Gaussier
Created: 12/10/2013

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description

Creation d'une image de meme taille que l'image d'entree contenant des rectangles dont les coordonnees sont les valeurs des neurones dans une matrice Nx5  (X0 y0 X1 Y1 valeur) ou N est le nombre de colonnes de la matrice de neurones et valeur le niveau de gris associe.
La configuration testee correspond aux sorties s et dans s1 les coordonnees sauvegardees.
d contient l'evaluation/reward precedente de l'image.

Si la recompense est meilleure (plus basse), l'algorithme memorise la configuration courante et continue l'exploration aleatoire (modification aleatoire d'un rectangle).
Si la recompense est moins bonne (erreur plus forte), l'agorithme recopie dans la configuration courante la configuration sauvegardere dans s1.

Un recuit simule est aussi disponible mais mal parametre. Il n'est pas utile tel que le probleme est traite.

L'algorithme peut aussi fonctionner en utilisant des triangles mais les performances sont moins bonnes sur l'exemple traite. L'appel a la fonction a ete commente.

\section Local variables
-none


\section Global variables

Dans le cas d'un dessin avec des triangles le clipping est traite en global.
Le programme suppose que l'image fait moins de 512x512 pour qu'il n'y ait pas d'effet de clipping.


*
\section External Tools
fonction pour dessiner un rectancle ou un triangle dans l'extension image d'un groupe

\section Links
-R: entree reward
-S: entree size (taille max des rectangles generes)

\section Comments

Known bugs: none (yet!)

Todo: revoir le recuit simuler. Gerer de manière moins ad hoc la decroissance de temperature (prevoir des entrees specifiques.
L'echantillonnage des niveau de gris est mis en dur. Cela pourrait etre un parametre.

http://www.doxygen.org

************************************************************/
// #define DEBUG
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>


int min_clip_x=0;
int min_clip_y=0;
int max_clip_x=512;
int max_clip_y=512;

//This function draws a triangle with a flat top:
void Draw_Top_Tri(int x1,int y1, int x2,int y2, int x3,int y3, int color, unsigned char *dest_buffer, int mempitch)
{
// this function draws a triangle that has a flat top

   float dx_right,    // the dx/dy ratio of the right edge of line
         dx_left,     // the dx/dy ratio of the left edge of line
         xs,xe,       // the starting and ending points of the edges
         height;      // the height of the triangle

   int temp_x,        // used during sorting as temps
       temp_y,
       right,         // used by clipping
       left;
   unsigned char *p;

// destination address of next scanline
   unsigned char  *dest_addr = NULL;

   y2=y2+0; /* eviter warning car y2 n'est pas utilise */

// test order of x1 and x2
   if (x2 < x1)
   {
      temp_x = x2;
      x2     = x1;
      x1     = temp_x;
   } // end if swap

// compute delta's
   height = y3-y1;

   dx_left  = (x3-x1)/height;
   dx_right = (x3-x2)/height;

// set starting points
   xs = (float)x1;
   xe = (float)x2+(float)0.5;

// perform y clipping
   if (y1 < min_clip_y)
   {
      // compute new xs and ys
      xs = xs+dx_left*(float)(-y1+min_clip_y);
      xe = xe+dx_right*(float)(-y1+min_clip_y);

      // reset y1
      y1=min_clip_y;

   } // end if top is off screen

   if (y3>max_clip_y)
      y3=max_clip_y;

// compute starting address in video memory
   dest_addr = dest_buffer+y1*mempitch;

// test if x clipping is needed
   if (x1>=min_clip_x && x1<=max_clip_x && x2>=min_clip_x && x2<=max_clip_x &&  x3>=min_clip_x && x3<=max_clip_x)
   {
      // draw the triangle
      for (temp_y=y1; temp_y<=y3; temp_y++,dest_addr+=mempitch)
      {
//        memset((unsigned char *)dest_addr+(unsigned int)xs, color,(unsigned int)(xe-xs+1));
         for (p=dest_addr+(unsigned int)xs; p<dest_addr+(unsigned int)xs+(unsigned int)(xe-xs+1); p++)
         {
            if (*p==0) *p=color;
            else *p =(*p+color)>>1;
         }

         // adjust starting point and ending point
         xs+=dx_left;
         xe+=dx_right;

      } // end for

   } // end if no x clipping needed
   else
   {
      // clip x axis with slower version

      // draw the triangle
      for (temp_y=y1; temp_y<=y3; temp_y++,dest_addr+=mempitch)
      {
         // do x clip
         left  = (int)xs;
         right = (int)xe;

         // adjust starting point and ending point
         xs+=dx_left;
         xe+=dx_right;
         // clip line
         if (left < min_clip_x)
         {
            left = min_clip_x;
            if (right < min_clip_x)   continue;
         }

         if (right > max_clip_x)
         {
            right = max_clip_x;
            if (left > max_clip_x)    continue;
         }

         // memset((unsigned char  *)dest_addr+(unsigned int)left, color,(unsigned int)(right-left+1));

         for (p=dest_addr+(unsigned int)left; p<dest_addr+(unsigned int)left+(unsigned int)(unsigned int)(right-left+1); p++)
         {
            if (*p==0) *p=color;
            else *p =(*p+color)>>1;
         }

      } // end for

   } // end else x clipping needed

} // end Draw_Top_Tri

//This function draws a triangle with a flat bottom:

void Draw_Bottom_Tri(int x1,int y1,
                     int x2,int y2,
                     int x3,int y3,
                     int color,
                     unsigned char *dest_buffer, int mempitch)
{
// this function draws a triangle that has a flat bottom

   float dx_right,    // the dx/dy ratio of the right edge of line
         dx_left,     // the dx/dy ratio of the left edge of line
         xs,xe,       // the starting and ending points of the edges
         height;      // the height of the triangle

   int temp_x,        // used during sorting as temps
       temp_y,
       right,         // used by clipping
       left;

// destination address of next scanline
   unsigned char  *dest_addr,*p;

   y2=y2+0; /* eviter warning car y2 n'est pas utilise */

// test order of x1 and x2
   if (x3 < x2)
   {
      temp_x = x2;
      x2     = x3;
      x3     = temp_x;
   } // end if swap

// compute delta's
   height = y3-y1;

   dx_left  = (x2-x1)/height;
   dx_right = (x3-x1)/height;

// set starting points
   xs = (float)x1;
   xe = (float)x1; // +(float)0.5;

// perform y clipping
   if (y1<min_clip_y)
   {
      // compute new xs and ys
      xs = xs+dx_left*(float)(-y1+min_clip_y);
      xe = xe+dx_right*(float)(-y1+min_clip_y);

      // reset y1
      y1=min_clip_y;

   } // end if top is off screen

   if (y3>max_clip_y)
      y3=max_clip_y;

// compute starting address in video memory
   dest_addr = dest_buffer+y1*mempitch;

// test if x clipping is needed
   if (x1>=min_clip_x && x1<=max_clip_x &&
         x2>=min_clip_x && x2<=max_clip_x &&
         x3>=min_clip_x && x3<=max_clip_x)
   {
      // draw the triangle
      for (temp_y=y1; temp_y<=y3; temp_y++,dest_addr+=mempitch)
      {
//        memset((unsigned char  *)dest_addr+(unsigned int)xs, color,(unsigned int)(xe-xs+1));

         for (p=dest_addr+(unsigned int)xs; p<dest_addr+(unsigned int)xs+(unsigned int)(xe-xs+1); p++)
         {
            if (*p==0) *p=color;
            else *p =(*p+color)>>1;
         }

         // adjust starting point and ending point
         xs+=dx_left;
         xe+=dx_right;

      } // end for

   } // end if no x clipping needed
   else
   {
      // clip x axis with slower version

      // draw the triangle

      for (temp_y=y1; temp_y<=y3; temp_y++,dest_addr+=mempitch)
      {
         // do x clip
         left  = (int)xs;
         right = (int)xe;

         // adjust starting point and ending point
         xs+=dx_left;
         xe+=dx_right;

         // clip line
         if (left < min_clip_x)
         {
            left = min_clip_x;
            if (right < min_clip_x) continue;
         }

         if (right > max_clip_x)
         {
            right = max_clip_x;
            if (left > max_clip_x) continue;
         }

//      memset((unsigned char  *)dest_addr+(unsigned int)left, color,(unsigned int)(right-left+1));

         for (p=dest_addr+(unsigned int)left; p<dest_addr+(unsigned int)left+(unsigned int)(unsigned int)(right-left+1); p++)
         {
            if (*p==0) *p=color;
            else *p =(*p+color)>>1;
         }

      } // end for

   } // end else x clipping needed

} // end Draw_Bottom_Tri

//And finally, this function draws a general triangle by splitting it into a flat top and flat bottom if needed:

void Draw_Triangle_2D(int x1,int y1,
                      int x2,int y2,
                      int x3,int y3,
                      int color,
                      unsigned char *dest_buffer, int mempitch)
{
// this function draws a triangle on the destination buffer
// it decomposes all triangles into a pair of flat top, flat bottom

   int temp_x, // used for sorting
       temp_y,
       new_x;

// test for h lines and v lines
   if ((x1==x2 && x2==x3)  ||  (y1==y2 && y2==y3))
      return;

   dprints("trace triangle (%d,%d) (%d,%d) (%d,%d) \n",x1,y1,x2,y2,x3,y3);

// sort p1,p2,p3 in ascending y order
   if (y2<y1)
   {
      temp_x = x2;
      temp_y = y2;
      x2     = x1;
      y2     = y1;
      x1     = temp_x;
      y1     = temp_y;
   } // end if

// now we know that p1 and p2 are in order
   if (y3<y1)
   {
      temp_x = x3;
      temp_y = y3;
      x3     = x1;
      y3     = y1;
      x1     = temp_x;
      y1     = temp_y;
   } // end if

// finally test y3 against y2
   if (y3<y2)
   {
      temp_x = x3;
      temp_y = y3;
      x3     = x2;
      y3     = y2;
      x2     = temp_x;
      y2     = temp_y;

   } // end if
// do trivial rejection tests for clipping
   if ( y3<min_clip_y || y1>max_clip_y ||
         (x1<min_clip_x && x2<min_clip_x && x3<min_clip_x) ||
         (x1>max_clip_x && x2>max_clip_x && x3>max_clip_x) )
      return;

// test if top of triangle is flat
   if (y1==y2)
   {
      Draw_Top_Tri(x1,y1,x2,y2,x3,y3,color, dest_buffer, mempitch);
   } // end if
   else if (y2==y3)
   {
      Draw_Bottom_Tri(x1,y1,x2,y2,x3,y3,color, dest_buffer, mempitch);
   } // end if bottom is flat
   else
   {
      // general triangle that's needs to be broken up along long edge
      new_x = x1 + (int)(0.5+(float)(y2-y1)*(float)(x3-x1)/(float)(y3-y1));

      // draw each sub-triangle
      Draw_Bottom_Tri(x1,y1,new_x,y2,x2,y2,color, dest_buffer, mempitch);
      Draw_Top_Tri(x2,y2,new_x,y2,x3,y3,color, dest_buffer, mempitch);

   } // end else

} // end Draw_Triangle_2D



/****************************************************************************************************/

void dessine_rectangle(unsigned char *im, int xmax, int x1, int y1, int x2, int y2, unsigned char val)
{
   int x,y,p1;
   int largeur;
   unsigned char *pt;

   largeur=x2-x1;

   if (largeur<=0) return;

   for (y=y1; y<y2; y++)
   {

      p1=y*xmax+x1;
      pt=&im[p1];
      for (x=largeur; x--;)
      {
         if (*pt==0) *pt=val; else *pt= (*pt+val)>>1;
         pt++;
      }
   }
}

void dessine_cercle(unsigned char *im, int taille_x, int taille_y, int x1, int y1, int rayon, unsigned char val)
{
   int x,y,p1;
   int distance;
   int xmin,xmax,ymin,ymax;
   unsigned char *pt;

   dprints("xmax=%d, ymax=%d \n",taille_x,taille_y);
   dprints("x=%d , y=%d, r=%d \n",x1,y1,rayon);

   xmin=x1-rayon; xmax=x1+rayon;
   ymin=y1-rayon; ymax=y1+rayon;

   if (xmin<0) xmin=0; else if (xmin>=taille_x) xmin=taille_x-1;
   if (ymin<0) ymin=0; else if (ymin>=taille_y) ymin=taille_y-1;
   if (xmax<0) xmax=0; else if (xmax>=taille_x) xmax=taille_x-1;
   if (ymax<0) xmax=0; else if (ymax>=taille_y) ymax=taille_y-1;

   dprints("xmin=%d xmax=%d ymin=%d ymax=%d \n",xmin,xmax,ymin,ymax);

   for (y=ymin; y<=ymax; y++)
   {
      p1=y*taille_x;
      for (x=xmin; x<=xmax; x++)
      {
         distance=(int) sqrt((x1-x)*(x1-x)+(y1-y)*(y1-y));
         if (distance <=rayon)
         {
            pt=&im[x+p1];
            if (*pt==0) *pt=val; else *pt= (*pt+val)>>1;
         }
      }
   }
}



struct IMG_GEN_DATA
{
   int prev_group;
   int reward_group;
   int size_group;
   int iter;
};

#define V_INIT 100000000

void function_img_generator(int group)
{
   int i = 0;
   int  n = 0;
   int x1,y1,x2,y2,x3,y3;
   int xmax,ymax;
   unsigned int val;
   int reward_group=-1;
   int reward_neuron;  /* No du neurone associe a la recompense */
   float reward;
   int ech=1;

   int deb_s = 0;
   int prev_group = -1;
   int l;
   char resultat[256];
   int selection;
   int modif;
   int size_group=-1;
   int size;
   int neuron_inc,nb_points;

   float de,temperature; /* pour le recuit simule */
   int tirage;

   struct IMG_GEN_DATA *data = NULL;
   prom_images_struct *img = NULL;
   prom_images_struct *prev_img = NULL;

   /*  int pas, pasx, pasy;
     int posx,posy;*/

   int deb_mod=0;
   int nb_surf=1;

// dprints("f_img_generator : Entree\n");

   deb_s = def_groupe[group].premier_ele;
   nb_points=(def_groupe[group].taillex-1)/2;
   neuron_inc=(nb_points*2+1);

   if (def_groupe[group].data == NULL)
   {
      if (nb_points<2) EXIT_ON_ERROR("ERREUR: %s, le groupe %s devrait avoir au moins 5 neurones pour taillex pour fonctionner au lieu de %d\n",__FUNCTION__,def_groupe[group].no_name,def_groupe[group].taillex);
      else if (nb_points==2) printf("%s on doit travailler avec des rectangles \n",__FUNCTION__);
      else if (nb_points>=2) printf("%s on peut travailler avec des triangles nb_points=%d \n",__FUNCTION__,nb_points);

      neurone[deb_s].d=V_INIT;  /* memoire du renforcement tres faible pour prendre la premiere valeur proposee */
      for (i = 0; i < def_groupe[group].nbre; i++) /* normalement inutile: on efface toute la memoire du dessin */
      {
         neurone[deb_s + i].s1   = 0.;
      }

      if (def_groupe[group].taillex!=neuron_inc )
      {
         EXIT_ON_ERROR("erreur %s : taillex groupe = %d au lieu de %d \n",__FUNCTION__,def_groupe[group].taillex,neuron_inc);
      }

      i = 0;
      l = find_input_link(group, i);
      while (l != -1)
      {
         dprints("lien %d: %s--\n",i,liaison[l].nom);

         if (prom_getopt(liaison[l].nom, "-I", resultat) == 1)
         {
            prev_group = liaison[l].depart;
            /*prom_It = (prom_images_struct *) def_groupe[prev_group].ext;	*/
         }
         if (prom_getopt(liaison[l].nom, "-R", resultat) == 1)
         {
            reward_group = liaison[l].depart;
            printf("reward_group = %s \n",def_groupe[reward_group].no_name);
         }
         if (prom_getopt(liaison[l].nom, "-S", resultat) == 1)
         {
            size_group = liaison[l].depart;
            printf("size_group = %s \n",def_groupe[size_group].no_name);
         }
         /*
         			if (prom_getopt(liaison[l].nom, "-alpha", resultat) == 2)
         			{
         				alpha = atof(resultat);
         			}
         */
         i++;
         l = find_input_link(group, i);
      }
      if (prev_group == -1)
      {
         EXIT_ON_ERROR("manque un groupe en entree avec lien -I \n");
         exit(0);
      }

      if ((prev_img = ((prom_images_struct *) def_groupe[prev_group].ext)) == NULL)
      {
         PRINT_WARNING("f_img_generator : Pas d image en entree de f_img_generator !\n");
         return ;
      }

      if ((data =(struct IMG_GEN_DATA *) malloc(sizeof(struct IMG_GEN_DATA))) ==NULL)
         EXIT_ON_ERROR( "f_img_generator : Impossible d'allouer la memoire\n");

      def_groupe[group].data = (void *) data;
      data->prev_group = prev_group;
      data->reward_group=reward_group;
      data->size_group=size_group;
      data->iter=0;

      if ((img = (void *) calloc_prom_image(prev_img->image_number, prev_img->sx,prev_img->sy, 1)) == NULL)
         EXIT_ON_ERROR ("f_img_generator : Impossible d'allouer la memoire pour une nouvelle image\n");

      printf("creation image %d %d %d \n",prev_img->image_number, prev_img->sx,prev_img->sy);

      def_groupe[group].ext = (void *) img;
   }
   else
   {
      data = (struct IMG_GEN_DATA *) def_groupe[group].data;
      prev_img = ((prom_images_struct *) def_groupe[data->prev_group].ext);
      img = ((prom_images_struct *) def_groupe[group].ext);
      reward_group=data->reward_group;
      size_group=data->size_group;
   }

   if (prev_img == NULL) return;

   xmax = img->sx ;
   ymax = img->sy ;
   n = xmax*ymax;


   data->iter++;
   /*-----------------------------------------------------------------------*/


   /*  ech=xmax/16; */
#define Res_min 4  /*16 */

   if (size_group!=-1)
   {
      dprints("size_group = %s \n", def_groupe[size_group].no_name);
      size=(int)neurone[def_groupe[size_group].premier_ele].s1;
      if (size<1)
      {
         printf("ATTENTION %s sortie size =%d pour group entree %s \n",__FUNCTION__,size,def_groupe[size_group].no_name);
         size=1;
      }
      ech=xmax/size;
      if (ech>xmax) ech=xmax;
      if (ech>ymax) ech=ymax;
      deb_mod=0; nb_surf=def_groupe[group].nbre/neuron_inc;
   }
   else  /* evolution automatique dans le temps de la taille et de la selection des ractangles modifies */
   {
      if (/*data->iter>=3000 && */ data->iter<10000)  {ech=xmax/128; deb_mod=0; nb_surf=500;}
            if (/*data->iter>=3000 && */ data->iter>=10000 && data->iter<20000)  {ech=xmax/64; deb_mod=0; nb_surf=500;}

      if (data->iter>=20000 && data->iter<40000)  {ech=xmax/16; deb_mod=500; nb_surf=def_groupe[group].nbre/neuron_inc-500;}
      if (data->iter>=40000 )  {ech=xmax/4; deb_mod=500; nb_surf=def_groupe[group].nbre/neuron_inc-500;}
      if (nb_surf<0) nb_surf=1;
      if (deb_mod+nb_surf>def_groupe[group].nbre/neuron_inc)
      {
         EXIT_ON_ERROR("erreur groupe %s (%s) deb_mod=%d + nb_surf=%d > nbre/neuron_inc\n",__FUNCTION__,def_groupe[group].no_name,deb_mod,nb_surf, def_groupe[group].nbre/neuron_inc);
      }
   }

   if (reward_group!=-1)
   {
      reward_neuron=def_groupe[reward_group].premier_ele;
      reward=neurone[reward_neuron].s1;
      de= reward- neurone[deb_s].d ;
      temperature=0.00001/(1.+sqrt(data->iter));
//      temperature=20./(0.1+pow((float)data->iter,0.4));
//   dprints("%s tps=%d, reward = %f de = %f temp= %f min= %f , proba = %f\n",__FUNCTION__,data->iter,reward,de,temperature, neurone[deb_s].d,(100.*exp(-de/temperature)) );
      // if(reward<-1.) reward=-1.;
      dprints("%s tps=%d, reward = %f min= %f \n",__FUNCTION__,data->iter,reward,neurone[deb_s].d);

      tirage=(random()%100);
//     if ( ((de < 0.) && fabs(neurone[deb_s].d - V_INIT)>0.1)   || ( tirage < (int)(100.*exp(-(de)/temperature))) )
      if ( (de < 0.)    || ( tirage < (int)(100.*exp(-(de)/temperature))) )
      {
#ifdef DEBUG		  
         if ((de >= 0.) && (tirage < (int)(100.*exp(-de/temperature)))) printf("accepter a cause tirage aleatoire %d < %d \n",	tirage , (int)(100.*exp(-(de)/temperature)));
         if (de<0.) printf("meilleure image de= %f (%f %f)\n", de, reward, neurone[deb_s].d	);
#endif         
         if (isequal(reward,0.)==0) neurone[deb_s].d=reward; /* meilleure reward rencontree */
         else {printf("error reward cannot be null ! \n");}
#ifdef DEBUG         
         if (de>0.00001) printf("%s tps=%d, reward = %f taille=%d  | de=%e t°=%e, proba=%f, tirage=%d \n",__FUNCTION__, data->iter,reward,xmax/ech,de, temperature, 100.*exp(-(de)/temperature),tirage);
         else printf("%s tps=%d, reward = %f de= %f taille=%d \n",__FUNCTION__, data->iter,reward,de, xmax/ech);
#endif         
         /* sauve les coordonnees precedentes */
         if (isequal(reward,0.)==0)   for (i = 0; i < def_groupe[group].nbre; i++)       neurone[deb_s + i].s = neurone[deb_s + i].s1;
      }

      else  /* on recupere l'ancienne configuration */
      {
         dprints("%s tps=%d, ECHEC modif, reward = %f tirage=%d < %f \n",__FUNCTION__, data->iter, reward, tirage ,100.*exp(-(1.+de)/temperature));
         for (i = 0; i < def_groupe[group].nbre; i++) neurone[deb_s + i].s1 = neurone[deb_s + i].s;
      }

//     if (fabs(neurone[deb_s].d-V_INIT)<0.1 ) neurone[deb_s].d = neurone[deb_s].d-10.;

   }

   /* on tente une ou plusieurs modifications de l'image */


   for (modif=0; modif<1 /*1+10/(log(1+data->iter))*/; modif++)
   {
//   selection=random()%(def_groupe[group].nbre/neuron_inc);

      if (nb_surf>0) selection=deb_mod+random()%nb_surf;
      else selection=random()%(def_groupe[group].nbre/neuron_inc);

      x1=random()%xmax; y1=random()%ymax; x2=x1+random()%(xmax/ech); y2=y1+random()%(ymax/ech);   x3=x1+random()%(xmax/ech); y3=y1+random()%(ymax/ech);

      i=neuron_inc*selection;
      if (i>=def_groupe[group].nbre) i=def_groupe[group].nbre-1;

      if (x2>=xmax) x2=xmax-1;
      if (y2>=ymax) y2=ymax-1;
      if (x3>=xmax) x3=xmax-1;
      if (y3>=ymax) y3=ymax-1;

      val=8*(random()%32); /* %8 */
      neurone[deb_s + i].s1   = (float)x1;
      neurone[deb_s + i+1].s1 = (float)y1;
      neurone[deb_s + i+2].s1 = (float)x2;
      neurone[deb_s + i+3].s1 = (float)y2;
      neurone[deb_s + i+4].s1 = (float)x3;
      neurone[deb_s + i+5].s1 = (float)y3;
      neurone[deb_s + i+6].s1 = (float)val;
   }
   memset(img->images_table[0],0, n);

   for (i = 0; i<(deb_mod+nb_surf)*neuron_inc; i=i+neuron_inc  )
   {
      x1  = (int) neurone[deb_s + i].s1   ;
      y1  = (int) neurone[deb_s + i+1].s1 ;
      x2  = (int) neurone[deb_s + i+2].s1 ;
      y2  = (int) neurone[deb_s + i+3].s1 ;
      x3  = (int) neurone[deb_s + i+4].s1 ;
      y3  = (int) neurone[deb_s + i+5].s1 ;
      val = (int) neurone[deb_s + i+6].s1 ;

//    Draw_Triangle_2D(x1,y1,x2, y2, x3, y3, val, img->images_table[0], xmax);
//        if(x1!=0 &&y1!=0 && x2!=0 && x3!=0) Draw_Triangle_2D(150,10,30, 200, 250, 150, val, img->images_table[0], xmax);
      dessine_rectangle(img->images_table[0],xmax, x1, y1, x2, y2, val);
   }

//   dprints("f_img_generator : Sortie\n");
}


/* cree une image avec un cercle pour test de hough */
void function_img_test(int group)
{
   int i = 0;
   int  n = 0;
   int x1,y1,rayon;
   int xmax,ymax;
   unsigned char val;

   int prev_group = -1;
   int l;
   char resultat[256];
   int size_group=-1, size;

   struct IMG_GEN_DATA *data = NULL;
   prom_images_struct *img = NULL;
   prom_images_struct *prev_img = NULL;

   dprints("f_img_generator : Entree\n");

   if (def_groupe[group].data == NULL)
   {
      i = 0;
      l = find_input_link(group, i);
      while (l != -1)
      {
         dprints("lien %d: %s--\n",i,liaison[l].nom);

         if (prom_getopt(liaison[l].nom, "-I", resultat) == 1)
         {
            prev_group = liaison[l].depart;
            /*prom_It = (prom_images_struct *) def_groupe[prev_group].ext;	*/
         }

         if (prom_getopt(liaison[l].nom, "-S", resultat) == 1)
         {
            size_group = liaison[l].depart;
            printf("size_group = %s \n",def_groupe[size_group].no_name);
         }

         i++;
         l = find_input_link(group, i);
      }
      if (prev_group == -1)
      {
         EXIT_ON_ERROR("manque un groupe en entree avec lien -I \n");
         exit(0);
      }

      if ((prev_img = ((prom_images_struct *) def_groupe[prev_group].ext)) == NULL)
      {
         PRINT_WARNING("f_img_generator : Pas d image en entree de f_img_generator !\n");
         return ;
      }

      if ((data =(struct IMG_GEN_DATA *) malloc(sizeof(struct IMG_GEN_DATA))) ==NULL)
         EXIT_ON_ERROR( "f_img_generator : Impossible d'allouer la memoire\n");

      def_groupe[group].data = (void *) data;
      data->prev_group = prev_group;
      data->size_group=size_group;
      data->iter=0;

      if ((img = (void *) calloc_prom_image(prev_img->image_number, prev_img->sx,prev_img->sy, 1)) == NULL)
         EXIT_ON_ERROR ("f_img_generator : Impossible d'allouer la memoire pour une nouvelle image\n");

      printf("creation image %d %d %d \n",prev_img->image_number, prev_img->sx,prev_img->sy);

      def_groupe[group].ext = (void *) img;
   }
   else
   {
      data = (struct IMG_GEN_DATA *) def_groupe[group].data;
      prev_img = ((prom_images_struct *) def_groupe[data->prev_group].ext);
      img = ((prom_images_struct *) def_groupe[group].ext);
      size_group=data->size_group;
   }

   if (prev_img == NULL) return;

   xmax = img->sx ;
   ymax = img->sy ;
   n = xmax*ymax;

   data->iter++;
   /*-----------------------------------------------------------------------*/

   if (size_group!=-1)
   {
      dprints("size_group = %s \n", def_groupe[size_group].no_name);
      size=(int)neurone[def_groupe[size_group].premier_ele].s1;
      if (size<1)
      {
         printf("ATTENTION %s sortie size =%d pour group entree %s \n",__FUNCTION__,size,def_groupe[size_group].no_name);
         size=1;
      }
      rayon=size;

      if (rayon>xmax/2) rayon=xmax/2;
      if (rayon>ymax/2) rayon=ymax/2;
   }
   else  /* evolution automatique dans le temps de la taille et de la selection des ractangles modifies */
   {
      if (data->iter<10000)  { }    /* inutile, a utiliser un jour ? */
      else {}
      rayon=30;
   }

   x1=rayon+random()%(xmax-2*rayon); y1=rayon+random()%(ymax-2*rayon) ;  val=255;
   dprints("x1= %d , y1=%d xmax=%d  ymax=%d \n",x1,y1,xmax,ymax);

   memset(img->images_table[0],0, n);
   dessine_cercle(img->images_table[0],xmax,ymax,x1,y1,rayon,val);

   dprints("f_img_test : Sortie\n");
}

