/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\ingroup libSensors
\defgroup  f_img_select_color  f_img_select_color


	 \brief 

\section Modified

	 Author: Sylvain Chevallier
	 Created: 14/10/2009
	 Modified:

\section Theoritical description:

	 - \f$  I =\frac{r+g+b}{3}, R=r-\frac{g+b}{2}, G=g-{r+b}{2}, B=b-\frac{r+g}{2}, I=\frac{r+g}{2}-{|r-g|}{2}-b \f$  

	 Description: 
	 Selection d'un canal de couleur d'une image donnee : intensite (= niv de gris),
	 rouge, bleu, vert ou jaune. L'image de sortie correspond a l'image des valeurs
	 normalisee en luminance du canal choisi (voir equation).
	 Cette boite prend en entree une des options suivantes sur son lien : -I, -R, 
	 -G, -B ou -Y. 
	 De plus, elle accepte un parametre optionnel -T pour specifier une valeur de
	 seuillage. Toutes les valeurs de l'image de sortie inferieure a ce seuil
	 seront mises a zero.

	 Macro:
	 -none

	 Local variables:
	 -none

	 Global variables:
	 -none 

	 Internal Tools:
	 -none

	 External Tools: 
	 -none

	 Links:
	 - type: algo / biological / neural
	 - description: none/ XXX
	 - input expected group: none/xxx
	 - where are the data?: none/xxx

	 Comments:

	 Known bugs: none (yet!)

	 Todo:see author for testing and commenting the function

	 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
typedef struct data
{
	int InputGpe;
	int ProcessType;
	int* Thres;
} MyData;

/* #define DEBUG */

/*******************************************************************
 * Les fonctions sur les canaux de couleurs
 ******************************************************************/
void intensity (prom_images_struct* im_in, prom_images_struct* im_out, int img_nb, int n)
{
	int i, j;
	float * out;
	for (i = 0; i < img_nb; i++)
	{
		out=(float*)im_out->images_table[i];
		for (j = 0; j < n; j++)
			out[j] = (float) (im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 1] + im_in->images_table[i][3*j + 2]) / 3.;
	}
}

void red (prom_images_struct* im_in, prom_images_struct* im_out, int img_nb, int n)
{
	int i, j;
	float * out;
	for (i = 0; i < img_nb; i++){
		out=(float*)im_out->images_table[i];
		for (j = 0; j < n; j++)
			out[j] = im_in->images_table[i][3*j] -
			(im_in->images_table[i][3*j + 1] + im_in->images_table[i][3*j + 2]) / 2.;
	}
}

void green (prom_images_struct* im_in, prom_images_struct* im_out, int img_nb, int n)
{
	int i, j;
	float * out;
	for (i = 0; i < img_nb; i++)
	{
		out=(float*)im_out->images_table[i];
		for (j = 0; j < n; j++)
			out[j] = im_in->images_table[i][3*j + 1] -
			(im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 2]) / 2.;
	}
}

void blue (prom_images_struct* im_in, prom_images_struct* im_out, int img_nb, int n)
{
	int i, j;
	float * out;
	for (i = 0; i < img_nb; i++)
	{
		out=(float*)im_out->images_table[i];
		for (j = 0; j < n; j++)
			out[j] = im_in->images_table[i][3*j + 2] -
			(im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 1]) / 2.;
	}
}

void yellow (prom_images_struct* im_in, prom_images_struct* im_out, int img_nb, int n)
{
	int i, j;
	float tmp;
	float * out;
	for (i = 0; i < img_nb; i++)
	{
		out=(float*)im_out->images_table[i];
		for (j = 0; j < n; j++) 
		{
			tmp = (im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 1]) / 2 -
					(abs (im_in->images_table[i][3*j] - im_in->images_table[i][3*j + 1])) / 2.
					- im_in->images_table[i][3*j + 2];
			if (tmp > 0) out[j] = tmp;
			else out[j] = 0;
		}
	}
}

void intensity_t (prom_images_struct* im_in, prom_images_struct* im_out, 
		int img_nb, int n, int thres)
{
	int i, j;
	float tmp;
	for (i = 0; i < img_nb; i++)
	{
		for (j = 0; j < n; j++) 
		{
			tmp = (im_in->images_table[i][3*j] +
					im_in->images_table[i][3*j + 1] +
					im_in->images_table[i][3*j + 2]) / 3.;
			if (tmp > thres) im_out->images_table[i][j] = tmp;
			else im_out->images_table[i][j] = 0;
		}
	}

}

void red_t (prom_images_struct* im_in, prom_images_struct* im_out, 
		int img_nb, int n, int thres)
{
	int i, j;
	float tmp;
	for (i = 0; i < img_nb; i++)
	{
		for (j = 0; j < n; j++) 
		{
			tmp = im_in->images_table[i][3*j] -
					(im_in->images_table[i][3*j + 1] + im_in->images_table[i][3*j + 2]) / 2.;
			if (tmp > thres) im_out->images_table[i][j] = tmp;
			else im_out->images_table[i][j] = 0;
		}
	}
}

void green_t (prom_images_struct* im_in, prom_images_struct* im_out, 
		int img_nb, int n, int thres)
{
	int i, j;
	float tmp;
	for (i = 0; i < img_nb; i++)
	{
		for (j = 0; j < n; j++) 
		{
			tmp = im_in->images_table[i][3*j + 1] -
					(im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 2]) / 2.;
			if (tmp > thres) im_out->images_table[i][j] = tmp;
			else im_out->images_table[i][j] = 0;
		}
	}
}

void blue_t (prom_images_struct* im_in, prom_images_struct* im_out, 
		int img_nb, int n, int thres)
{
	int i, j;
	float tmp;
	for (i = 0; i < img_nb; i++)
	{
		for (j = 0; j < n; j++)
		{
			tmp = im_in->images_table[i][3*j + 2] -
					(im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 1]) / 2.;
			if (tmp > thres) im_out->images_table[i][j] =tmp;
			else im_out->images_table[i][j] =0;
		}
	}
}

void yellow_t (prom_images_struct* im_in, prom_images_struct* im_out, 
		int img_nb, int n, int thres)
{
	int i, j;
	float tmp;
	for (i = 0; i < img_nb; i++)
	{
		for (j = 0; j < n; j++) 
		{
			tmp = (im_in->images_table[i][3*j] + im_in->images_table[i][3*j + 1]) / 2 -
					(abs (im_in->images_table[i][3*j] - im_in->images_table[i][3*j + 1])) / 2.
					- im_in->images_table[i][3*j + 2];
			if (tmp > thres) im_out->images_table[i][j] = tmp;
			else im_out->images_table[i][j] = 0;
		}
	}
}

/*******************************************************************
 * Les fonctions d'appel
 ******************************************************************/
void function_img_select_color_channel (int Gpe)
{
	MyData *data = NULL;
	prom_images_struct *prev_img = NULL, *img = NULL;
	int* Thres = NULL, ProcessType = -1, n = 0, img_nb = 0;
	int i, j;
	char param[256];

	if (def_groupe[Gpe].data == NULL)
	{

		dprints(stderr, "%s(%d): Debut init\n", __FUNCTION__, Gpe);

		/* Allocation de la structure data */
		if ((data = (MyData *) malloc(sizeof(MyData))) == NULL)
			EXIT_ON_ERROR("%s(%d): malloc failed for data\n",__FUNCTION__, Gpe);

		data->Thres = NULL;
		data->ProcessType = -1;
		data->InputGpe = -1;

		/* Recherche des informations sur les liens entrants */
		i = 0;
		while ((j = find_input_link (Gpe, i)) != -1) 
		{
			if (prom_getopt(liaison[j].nom, "I", param) == 1)
			{
				data->InputGpe = liaison[j].depart;
				data->ProcessType = 0;
			}
			else if (prom_getopt(liaison[j].nom, "-R", param) == 1)
			{
				data->InputGpe = liaison[j].depart;
				data->ProcessType = 1;
			}
			else if (prom_getopt(liaison[j].nom, "-G", param) == 1)
			{
				data->InputGpe = liaison[j].depart;
				data->ProcessType = 2;
			}
			else if (prom_getopt(liaison[j].nom, "-B", param) == 1)
			{
				data->InputGpe = liaison[j].depart;
				data->ProcessType = 3;
			}
			else if (prom_getopt(liaison[j].nom, "-Y", param) == 1)
			{
				data->InputGpe = liaison[j].depart;
				data->ProcessType = 4;
			}
			if (prom_getopt(liaison[j].nom,"-T",param) == 2)
			{
				data->Thres = (int*) malloc (sizeof(int));
				sscanf(param,"%d",data->Thres);
			}
			i++;
		}


		/* Il faut specifier le type de traitment sur le lien entrant */
		if (data->ProcessType == -1) 
			EXIT_ON_ERROR("%s(%s): il doit y avoir un lien entrant avec {-I,-R,-G,-B,-Y}[-Tx]\n",__FUNCTION__, def_groupe[Gpe].no_name);

		def_groupe[Gpe].data = (void *) data;
		prev_img = (prom_images_struct *) (def_groupe[data->InputGpe].ext);
	}		
 else
	{
		data = (MyData*) def_groupe[Gpe].data;
		img = (prom_images_struct*) def_groupe[Gpe].ext;
		prev_img = ((prom_images_struct*) def_groupe[data->InputGpe].ext);
	}
	
				/* Il faut une image couleur en entree */

	if (prev_img == NULL)
		{
			dprints ("%s(%s): pas d'image couleur dans la boite d'entree\n", __FUNCTION__, def_groupe[Gpe].no_name);
			return;
		}
	else if(def_groupe[Gpe].ext==NULL)
	{
		if (prev_img->nb_band != 3) 
			EXIT_ON_ERROR ("%s(%s): cette fonction ne traite que des images uchar couleur (nb_band = 3)\n", __FUNCTION__, def_groupe[Gpe].no_name);

		/* Allocation de l'image de sortie en niveau de gris*/
		img=(prom_images_struct *) malloc(sizeof(prom_images_struct));
		if (img == NULL) EXIT_ON_ERROR("%s (%d) : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__,__LINE__);

		for(i=0;i<(int)prev_img->image_number;i++){
			img->images_table[i] = (unsigned char *) calloc(prev_img->sx *prev_img->sy, sizeof(float));
			if ( img->images_table[i] == NULL)
				EXIT_ON_ERROR("%s (%d) : ALLOCATION IMPOSSIBLE ...! \n",__FUNCTION__, __LINE__);
		}
		img->nb_band=4; /*floattant!*/
		img->sx=prev_img->sx;
		img->sy=prev_img->sy;
		img->image_number=prev_img->image_number;

		def_groupe[Gpe].ext  = (void *) img;
	}
	
	ProcessType = data->ProcessType;
	Thres = data->Thres;
	n = img->sx * img->sy;
	img_nb = 1;/*img->image_number;*/

	dprints("%s(%d): Debut\n", __FUNCTION__, Gpe);

	if (Thres == NULL)
	{
		switch (ProcessType)
		{
		case 0 : intensity (prev_img, img, img_nb, n); break;
		case 1 : red (prev_img, img, img_nb, n); break;
		case 2 : green (prev_img, img, img_nb, n); break;
		case 3 : blue (prev_img, img, img_nb, n); break;
		case 4 : yellow (prev_img, img, img_nb, n); break;
		}
	}
	else
	{
		switch (ProcessType)
		{
		case 0 : intensity_t (prev_img, img, img_nb, n, *Thres); break;
		case 1 : red_t (prev_img, img, img_nb, n, *Thres); break;
		case 2 : green_t (prev_img, img, img_nb, n, *Thres); break;
		case 3 : blue_t (prev_img, img, img_nb, n, *Thres); break;
		case 4 : yellow_t (prev_img, img, img_nb, n, *Thres); break;
		}
	}

}

