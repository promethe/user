/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_couleur_selection.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-none 

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <Struct/couleur_selection_save.h>

void function_couleur_selection(int gpe_sortie)
{
    static int /*first=1, */ num_liaison, deb_s;

    int i /*,j,num */ , R = 0, G = 0, B = 0, T =
        0, sx, sy, n, seuil_OK, BT1, BT2, RT1, RT2, GT1, GT2;
    couleur_selection_save *data_groupe;
    unsigned char *image;
    int point;
    /*   float ratio,sum; */

    /*   float pourcent_couleur; */

    void *ptr = NULL;

    char *string, *st;

    /*printf("*------Fonction Couleur Selection-----------*\n"); */

    deb_s = def_groupe[gpe_sortie].premier_ele;

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == gpe_sortie)
            {
                num_liaison = i;
                ptr = def_groupe[liaison[i].depart].ext;
                break;
            }

        if (i == nbre_liaison)
        {
            printf
                ("\n ERREUR : Pas de lien en entree de f_couleur_selection !\n");
            exit(0);
        }

        string = liaison[num_liaison].nom;
        printf("string=%s\n", string);

        st = strstr(string, "-R");
        if (st != NULL)
            R = atoi(&st[2]);

        if ((R < 0) || (R > 256))
        {
            printf
                (" function_couleur_selection : parametre  R hors bornes! [0-255] ");
            exit(EXIT_FAILURE);
        }

        st = strstr(string, "-G");
        if (st != NULL)
            G = atoi(&st[2]);
        if ((G < 0) || (G > 256))
        {
            printf
                (" function_couleur_selection : parametre  G hors bornes! [0-255] ");
            exit(EXIT_FAILURE);
        }

        st = strstr(string, "-B");
        if (st != NULL)
            B = atoi(&st[2]);
        if ((B < 0) || (B > 256))
        {
            printf
                (" function_couleur_selection : parametre  B hors bornes ![0-255] ");
            exit(EXIT_FAILURE);
        }

        st = strstr(string, "-T");
        if (st != NULL)
            T = atoi(&st[2]);
        if ((T < 0) || (T > 256))
        {
            printf
                ("  function_couleur_selection : seuil  T hors bornes! [0-256] ");
            exit(EXIT_FAILURE);
        }

        printf("Les parametres recuperes sont : -R=%d -G=%d -B=%d -T=%d\n", R,
               G, B, T);



        sx = ((prom_images_struct *) (ptr))->sx;
        sy = ((prom_images_struct *) (ptr))->sy;

        data_groupe =
            (couleur_selection_save *) malloc(sizeof(couleur_selection_save));
        def_groupe[gpe_sortie].ext = (void *) data_groupe;
        (*data_groupe).sx = sx;
        (*data_groupe).sy = sy;
        (*data_groupe).T = T;
        (*data_groupe).R = R;
        (*data_groupe).G = G;
        (*data_groupe).B = B;
        image = (*data_groupe).image =
            ((prom_images_struct *) (ptr))->images_table[0];

    }
    else
    {
        data_groupe = (couleur_selection_save *) def_groupe[gpe_sortie].ext;
        sx = (*data_groupe).sx;
        sy = (*data_groupe).sy;
        T = (*data_groupe).T;
        R = (*data_groupe).R;
        G = (*data_groupe).G;
        B = (*data_groupe).B;
        image = (*data_groupe).image;

    }

    n = 0;




    RT1 = (R - T);
    RT2 = (R + T);
    GT1 = (G - T);
    GT2 = (G + T);
    BT1 = (B - T);
    BT2 = (B + T);

    for (i = 0, n = 0; i < 3 * sx * sy; i += 3, n++)
    {
        seuil_OK = 0;
        /* sum= ((float)image[i]) + ((float)image[i+1]) + ((float)image[i+2] );

           if (sum==0) ratio =1.;
           else ratio=255./sum; */


        /*  point=(int)((((float)image[i])*ratio)); */


        point = (int) image[i];
        if ((point > RT1) && (point < RT2))
            seuil_OK++;


        point = (int) image[i + 1];
        /* point=(int)((((float)image[i+1])*ratio)); */
        if ((point > GT1) && (point < GT2))
            seuil_OK++;

        point = (int) image[i + 2];
        /*  point=(int)((((float)image[i+2])*ratio)); */
        if ((point > BT1) && (point < BT2))
            seuil_OK++;

        if (seuil_OK == 3)
            neurone[deb_s + n].s = neurone[deb_s + n].s1 =
                neurone[deb_s + n].s2 = 1.;
        else
            neurone[deb_s + n].s = neurone[deb_s + n].s1 =
                neurone[deb_s + n].s2 = 0.;

    }


}
