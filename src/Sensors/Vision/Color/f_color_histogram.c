/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_convert_RGB_HSV.c
\brief

Author: Marwen Belkaid
Created: 30/07/2014

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

The function builds a HS (Hue Saturation) historam for the image sent by the former group (e.g. f_convert_RGB_HSV). H is represented on the Y axis and S on the X axis.
Optional parameters:
- hr : levels of hue represented in the histogram
- hs : levels of saturation represented in the histogram
Warning:
if -hr and -hs are omitted, these values are taken from taillex and tailley of the group. Otherwise, they have to be equals to the latter !

Known bugs: none (yet!)

Todo:see authored fortestingreen and commentingreen the function

http://www.doxygen.org
************************************************************/

//#define DEBUG

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/prom_images_struct.h>


#define COLOR_HISTO_INPUT_SIZE         2 // 99 hue and saturation ranges at most

typedef struct data_color_histogram
{
   int input_gpe;
   int band_x;
   int band_y;
   int circ_x;
   int circ_y;
} data_color_histogram;


void new_color_histogram(int gpe)
{
   data_color_histogram* mydata ;
   int tmp_input_gpe, tmp_band_x, tmp_band_y, tmp_max_x, tmp_max_y, tmp_circ_x, tmp_circ_y;
   int i = 0, links_in ;
   char tmp_input_value[COLOR_HISTO_INPUT_SIZE] ;

   tmp_input_gpe = tmp_band_x = tmp_band_y = tmp_max_x = tmp_max_y = -1;
   tmp_circ_x = tmp_circ_y = 0 ;

   /* Get Input group */
   while((links_in = find_input_link (gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "sync", NULL) == 0) // If this link is not a synchronization link then suppose it is the input gpe
         tmp_input_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-bx", tmp_input_value) == 2)
         tmp_band_x = atoi(tmp_input_value) ;
      if(prom_getopt(liaison[links_in].nom, "-by", tmp_input_value) == 2)
         tmp_band_y = atoi(tmp_input_value) ;
      if(prom_getopt(liaison[links_in].nom, "-cx", tmp_input_value) == 1)
         tmp_circ_x = 1 ;
      if(prom_getopt(liaison[links_in].nom, "-cy", tmp_input_value) == 1)
         tmp_circ_y = 1 ;

      i++ ;
   }

   /* Check input params */
   if(tmp_input_gpe == -1)
      EXIT_ON_ERROR("No input group found ! \n") ;
   if((tmp_band_x == -1) && (tmp_band_y == -1))
      EXIT_ON_ERROR("At least one of the input image bands (B) have to be specified (-bxB and/or -byB)\n") ;
   if( (tmp_band_x != -1 && ((tmp_band_x < 1) || (tmp_band_x > 3))) || (tmp_band_y != -1 && ((tmp_band_y > 3) || (tmp_band_y < 1))) )
      EXIT_ON_ERROR("The image band numbers (-bxB and/or -byB) have to be in {1,2,3} \n") ;
   if((tmp_circ_x != 0) && (tmp_circ_y != 0))
      PRINT_WARNING("The circular mode is supposed to be used only for one band (H in HSV). Please check your case is correctly handled... \n") ;

   /* Create object */
   mydata = ALLOCATION(data_color_histogram);
   mydata->input_gpe = tmp_input_gpe ;
   mydata->band_x = tmp_band_x;
   mydata->band_y = tmp_band_y;
   mydata->circ_x = tmp_circ_x;
   mydata->circ_y = tmp_circ_y;
   def_groupe[gpe].data = (data_color_histogram *) mydata;

}

void function_color_histogram(int gpe)
{
   int tmp_input_gpe, tmp_nb_pixel, tmp_size_y, tmp_size_x, tmp_band_x, tmp_band_y, tmp_circ_x, tmp_circ_y, tmp_nb_band ;
   float fx, fy ;
   data_color_histogram *mydata ;
   unsigned char *input_img ;
   int i, x, y, first_neuron ;

   /* init local variables */
   mydata = (data_color_histogram *) def_groupe[gpe].data ;
   tmp_input_gpe = mydata->input_gpe ;
   tmp_band_x = mydata->band_x;
   tmp_band_y = mydata->band_y;
   tmp_circ_x = mydata->circ_x;
   tmp_circ_y = mydata->circ_y;
   tmp_size_x = def_groupe[gpe].taillex ;
   tmp_size_y = def_groupe[gpe].tailley ;

   /* check input ext */
   if(def_groupe[tmp_input_gpe].ext == NULL)
      EXIT_ON_ERROR("The input group %s (%s) must have an ext \n", def_groupe[tmp_input_gpe].nom, def_groupe[tmp_input_gpe].no_name) ;

   tmp_nb_pixel = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sx * ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->sy ;
   tmp_nb_band = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->nb_band ;
   input_img = ((prom_images_struct *) def_groupe[tmp_input_gpe].ext)->images_table[0] ;



   /* init neurons */
   first_neuron = def_groupe[gpe].premier_ele;
   for(y = 0 ; y < tmp_size_y ; y++)
      for(x = 0 ; x < tmp_size_x ; x++)
         neurone[first_neuron + tmp_size_x*y + x].s1 = 0 ;

   /* build color histogram */
   for(i = 0 ; i < tmp_nb_pixel ; i++)
   {
      if(tmp_band_x > 0)
      {
         if(tmp_circ_x)
         {
            fx = (float)(((int)input_img[tmp_nb_band * i + (tmp_band_x - 1)]) / 255. * 360.) ;
            x = (int)((fx + (360. /(2*tmp_size_x))) / (360. / tmp_size_x)) % tmp_size_x ; // divise circular HUE space into bins taking into account a proportional offset
         }
         else
         {
            fx = (float)(((int)input_img[tmp_nb_band * i + (tmp_band_x - 1)]) / 255.);
            x = (int)(fx / (1.0/(float)tmp_size_x)) ;
         }
         if (x >= tmp_size_x)
            x = tmp_size_x - 1 ;
      }
      else
      {
         x = 0 ;
      }

      if(tmp_band_y > 0)
      {
         if(tmp_circ_y)
         {
            fy = (float)(((int)input_img[tmp_nb_band * i + (tmp_band_y - 1)]) / 255. * 360.) ;
            y = (int)((fy + (360. /(2*tmp_size_x))) / (360. / tmp_size_y)) % tmp_size_y ; // divise circular HUE space into bins taking into account a proportional offset
         }
         else
         {
            fy = (float)(((int)input_img[tmp_nb_band * i + (tmp_band_y - 1)]) / 255.);
            y = (int)(fy / (1.0/(float)tmp_size_y)) ;
         }
         if (y >= tmp_size_y)
            y = tmp_size_y - 1 ;
      }
      else
      {
         y = 0 ;
      }

      neurone[first_neuron + tmp_size_x*y + x].s1 = neurone[first_neuron + tmp_size_x*y + x].s1 + 1./tmp_nb_pixel ;
   }

}




void destroy_color_histogram(int gpe)
{
   dprints("destroy %s(%s) \n", __FUNCTION__,def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
     free(((data_color_histogram *) def_groupe[gpe].data));
     def_groupe[gpe].data = NULL;
   }
}
