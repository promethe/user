/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup f_img_select_color f_img_select_color
 \ingroup libSensors

 \brief detection de couleur dans une image RGB

 \details

 \section Description
 Dans une image de la meme taille que l'image RGB d'entree :
 pixel blanc si le pixel d'entree est de la bonne couleur, pixel noir sinon.
 Le resultat peut etre mis sur les neurones du groupe (sous echantillonage en fonction du nombre de neurones)
 La couleur a reconnaitre est definie en HSV.

 \section Options
 Ajout des options suivantes:
 Les options suivantes doivent etre definie par ensemble sinon on considere qu'elle ne sont pas du tout presente :
 -h et -rh : hue min et hue range : valeur min et largeur pour le seuillage. On tient compte de la boucle 0-360
 -s et -rs : saturation min et saturation range : valeur min et largeur pour le seuillage.
 -v et -rv : value min et value range : valeur min et largeur pour le seuillage.
 -c tout seul : donne le groupe d'entree sur lequel lire les valeurs des 6 parametres.
 -cq idem à -c mais quiet sans l'affichage des parametres. Ideal si 6 vuemetres concatenés servent a régler la boite.
 A utiliser en combinaison avec f_vue_metres (de taille 6) pour regler les parametres.
 Le message d'affichage donne ce qu'il faut copier sur le lien entre le groupe image et la boite pour que ces parametres soient definis en dur

 \file  f_img_select_color.c
 \ingroup f_img_select_color
 */
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
typedef struct data_img_select_color
{
   int prev_gpe;
   /* valeur de hue, saturation et value, avec leur range, entre 0 et 1
    a multiplier par la taille de l'intervalle H S V */
   float h; /* *360 */
   float range_h; /* *360 */
   float s; /* *100 */
   float range_s; /* *100 */
   float v; /* *100 */
   float range_v; /* *100 */
   int check_gpe, quiet;
} Data_img_select_color;

/* #define DEBUG */

/* valeurs par defaut en l'absence de specification */
#define HMIN 350
#define HRANGE 70
#define SMIN 50
#define SRANGE 50
#define VMIN 50
#define VRANGE 50

#define	HSV_MAX(a, b, c)	((a > b) ? ((a > c) ? a : c) : ((b > c) ? b : c))
#define	HSV_MIN(a, b, c)	((a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c))

/*
 ** in : R entre 0 et 255
 ** in : G entre 0 et 255
 ** in : B entre 0 et 255
 **
 ** out : H entre 0 et 360
 ** out : S entre 0 et 100
 ** out : V entre 0 et 100
 */
void RGBtoHSV_(unsigned char R, unsigned char G, unsigned char B,  int *H,  int *S,  int *V)
{
   float fR = 0, fG = 0, fB = 0;
   float minVal = 0, maxVal = 0;
   float fH = 0, fS = 0, fV = 0;
   float Delta = 0;

   fR = ((float) R);
   fG = ((float) G);
   fB = ((float) B);

   minVal = HSV_MIN(fR, fG, fB);
   maxVal = HSV_MAX(fR, fG, fB);

   Delta = maxVal - minVal;

   /* H */
   if (isequal(fR, maxVal))
   {
      fH = (60 * (fG - fB)) / Delta;
      if (fG < fB) fH += 360;
   }
   else if (isequal(fG, maxVal)) fH = 120 + (60 * (fB - fR)) / Delta;
   else if (isequal(fB, maxVal)) fH = 240 + (60 * (fR - fG)) / Delta;

   /* S */
   if (isequal(maxVal, 0)) fS = 0;
   else fS = (1 - (minVal / maxVal)) * 100;

   /* V */
   fV = maxVal * 100 / 255;

   *H = (( int) fH);
   *S = (( int) fS);
   *V = (( int) fV);
}

void function_img_select_color(int gpe_sortie)
{
   int i = 0, ii = 0, i1 = 0;
   int k = 0, l = 0, m = 0, n = 0;
   int j = 0, jj = 0, j1 = 0;
   int SizeZoneX = 0, SizeZoneY = 0;
   int deb_s = 0;
   int prev_gpe = -1, check_gpe = -1;
   int H = 0, S = 0, V = 0;
   Data_img_select_color *data = NULL;
   prom_images_struct *img = NULL;
   prom_images_struct *prev_img = NULL;
   char param[256];

   int h, range_h, s, range_s, v, range_v;

   dprints("f_img_couleur_selection : Entree\n");

   deb_s = def_groupe[gpe_sortie].premier_ele;

   if (def_groupe[gpe_sortie].data == NULL )
   {
      if ((data = (Data_img_select_color*) malloc(sizeof(Data_img_select_color))) == NULL )
      {
         EXIT_ON_ERROR("function_img_selection_couleur : Impossible d'allouer la memoire\n");
      }
      data->h = 0.0;
      data->range_h = 0.0;
      data->s = 0.0;
      data->range_s = 0.0;
      data->v = 0.0;
      data->range_v = 0.0;
      data->prev_gpe = -1;
      data->check_gpe = -1;

      i = 0;
      while ((j = find_input_link(gpe_sortie, i)) != -1)
      {
         /* recuperation des parametres donne en option */
         /*- la val min et son range doivent �tre donne pour etre pris en compte */
         if (prom_getopt(liaison[j].nom, "-h", param) == 2)
         {
            sscanf(param, "%f", &(data->h));
            data->h = data->h / 360.0;
            cprints("h: %f\n", data->h * 360.0);
         }
         if (prom_getopt(liaison[j].nom, "-rh", param) == 2)
         {
            sscanf(param, "%f", &(data->range_h));
            data->range_h = data->range_h / 360.0;
            cprints("range_h: %f\n", data->range_h * 360.0);
         }
         if (prom_getopt(liaison[j].nom, "-s", param) == 2)
         {
            sscanf(param, "%f", &(data->s));
            data->s = data->s / 100.0;
            cprints("s: %f\n", data->s * 100.0);
         }
         if (prom_getopt(liaison[j].nom, "-rs", param) == 2)
         {
            sscanf(param, "%f", &(data->range_s));
            data->range_s = data->range_s / 100.0;
            cprints("range_s: %f\n", data->range_s * 100.0);
         }
         if (prom_getopt(liaison[j].nom, "-v", param) == 2)
         {
            sscanf(param, "%f", &(data->v));
            data->v = data->v / 100.0;
            cprints("v: %f\n", data->v * 100.0);
         }
         if (prom_getopt(liaison[j].nom, "-rv", param) == 2)
         {
            sscanf(param, "%f", &(data->range_v));
            data->range_v = data->range_v / 100;
            cprints("range_v: %f\n", data->range_v * 100);
         }
         if (prom_getopt(liaison[j].nom, "-c", param) > 0)
         {
            data->check_gpe = liaison[j].depart;
            if (param[0] == 'q') data->quiet = 1;
            /* DEPRECATED !! pb of type !
             * option check : on donne la valeur que l'on va tester hue ou saturation ou value
             if (strcmp(param,"h")==0)
             {
             data->h = &vigilence;
             data->range_h = &eps;
             printf("check val : %s : vigilence  %f, eps %f\n",param,*(data->h),*(data->range_h));
             }
             else if (strcmp(param,"s")==0)
             {
             data->s =&vigilence;
             data->range_s = &eps;
             printf("check val : %s : vigilence  %f, eps %f\n",param,*(data->s),*(data->range_s));
             }
             else if (strcmp(param,"v")==0)
             {
             data->v = &vigilence;
             data->range_v = &eps;
             printf("check val : %s : vigilence  %f, eps %f\n",param,*(data->v),*(data->range_v));
             }*/
         }
         else
         {
            prev_gpe = liaison[j].depart;
         }
         i++;
      }
      if (prev_gpe == -1)
      {
         EXIT_ON_ERROR("this group need a input group with -h, -rh, -s, -rs, -v, -rv on the link");
      }
      if (data->check_gpe == -1)
      {
         PRINT_WARNING("no check group. You can tune your group by adding an input group with 6 neurons and -c on the link");
      }
      else
      {
         if (def_groupe[data->check_gpe].nbre != 6) EXIT_ON_ERROR("the check group needs 6 neurons");
      }

      def_groupe[gpe_sortie].data = (void *) data;
      data->prev_gpe = prev_gpe;
      def_groupe[gpe_sortie].ext = NULL;
   }
   else
   {
      data = (Data_img_select_color*) def_groupe[gpe_sortie].data;
      prev_img = ((prom_images_struct *) def_groupe[data->prev_gpe].ext);
      img = ((prom_images_struct *) def_groupe[gpe_sortie].ext);
      check_gpe = data->check_gpe;
   }

   if (img == NULL )
   {
      if ((prev_img = ((prom_images_struct *) def_groupe[data->prev_gpe].ext)) == NULL )
      {
         cprints("f_img_couleur_selection(%s) : Pas d image en entree de f_couleur_selection ! (prev_gpe : %s)\n", def_groupe[gpe_sortie].no_name, def_groupe[data->prev_gpe].no_name);
         return;
      }
      else
      {
         if ((img = (prom_images_struct *) calloc_prom_image(prev_img->image_number, prev_img->sx, prev_img->sy, 1)) == NULL )
         {
            EXIT_ON_ERROR("f_img_selection_couleur(%d) : Impossible d'allouer la memoire pour une nouvelle image\n", def_groupe[gpe_sortie].no_name);
         }
         def_groupe[gpe_sortie].ext = (void *) img;
      }
   }

   /* recuperation des valeurs entieres pour les seuils de HSV*/
   if (check_gpe > 0)
   {
      i = def_groupe[check_gpe].premier_ele;

      h = (int) (neurone[i].s * 360.0);
      range_h = (int) (neurone[i + 1].s * 360.0);
      s = (int) (neurone[i + 2].s * 100.0);
      range_s = (int) (neurone[i + 3].s * 100.0);
      v = (int) (neurone[i + 4].s * 100.0);
      range_v = (int) (neurone[i + 5].s * 100.0);
      if (!data->quiet) cprints("-h%d-rh%d-s%d-rs%d-v%d-rv%d\n", h, range_h, s, range_s, v, range_v);
   }
   else
   {
      h = (int) (data->h * 360.0);
      range_h = (int) (data->range_h * 360.0);
      s = (int) (data->s * 100.0);
      range_s = (int) (data->range_s * 100.0);
      v = (int) (data->v * 100.0);
      range_v = (int) (data->range_v * 100.0);
   }

   dprints("hmin: %d, hrange: %d, smin: %d, srange: %d, vmin: %d, vrange: %d\n", h, range_h, s, range_s, v, range_v);

   if (prev_img == NULL ) return;

   n = prev_img->sx * prev_img->sy;

   for (i = 0; i < def_groupe[gpe_sortie].nbre; i++)
   {
      //neurone[deb_s + i].s = neurone[deb_s + i].s1 = neurone[deb_s + i].s2 = 0;
      neurone[deb_s + i].s1=0; //on évite des copies inutiles
   }

   for (i = 0;  i < prev_img->image_number; i++)
   {
      for (j = 0; j < n; j++)
      {

         RGBtoHSV_((prev_img->images_table[i][3 * j]), /* R */(prev_img->images_table[i][3 * j + 1]), /* G */ (prev_img->images_table[i][3 * j + 2]), /* B */ &H, &S, &V);

         /* On prend en compte le cycle des valeurs pour hue. */
         if ((((H >= h) && (H < (h + range_h + 1))) || ((360 + H >= h) && (360 + H < h + range_h + 1))) && (S >= s) && (S < s + range_s + 1) && (V >= v) && (V < v + range_v + 1)) img->images_table[i][j] = 255;
         else img->images_table[i][j] = 0;
      }

      SizeZoneX = img->sx / def_groupe[gpe_sortie].taillex;
      SizeZoneY = img->sy / def_groupe[gpe_sortie].tailley;

      i1 = def_groupe[gpe_sortie].tailley * SizeZoneY;
      j1 = def_groupe[gpe_sortie].taillex * SizeZoneX;
      for (ii = 0; ii < i1; ii++)
      {
         m = ii / SizeZoneY;
         for (jj = 0; jj < j1; jj++)
         {
            l = jj / SizeZoneX;
            k = img->sx * ii + jj;
            n = def_groupe[gpe_sortie].taillex * m + l;
            //if (img->images_table[i][k] > 0) neurone[deb_s + n].s = neurone[deb_s + n].s1 = neurone[deb_s + n].s2 = 1;
            if (img->images_table[i][k] > 0) neurone[deb_s + n].s1 = 1;
            /* 	  else */
            /* 	    neurone[deb_s + n].s = neurone[deb_s + n].s1 = neurone[deb_s + n].s2 = 0; */
         }
      }
   }

   dprints("f_img_couleur_selection : Sortie\n");
}
