/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_conv_dog.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Modified:
- author : A. Karaouzene
- date : 22/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

- Cette fonction permet la convolution d'une image par une DoG. 
- L'image d'entree doit etre en niveaux de gris et de type (uchar).
- L'image de sortie est une image de flottants normalisee entre 0 et 1.
- Pour afficher l'image de sortie il est conseille de la convertir en uchar
  en utilisant (f_float_to_uchar) par exemple.
- L'image d'entree est de preference une image de gradient.

Option:

- R : Theta 1, necessaire a la creation de la DoG
- T : Theta 2, necessaire a la creation de la DoG
- P : Taille du masque. Taille de la dog en Pixels. 
 * Attention La convolution est calculee sur deux fois la taille precisee.
- RE: Rayon d'exclusion. Definie la taille de la bande non consideree autour de l'image.
- S : Seuil. La convolution est uniquement calculee sur les points a forte intensite.



Macro:
-DEFAULT_THETA1
-DEFAULT_THETA2
-DEFAULT_TAILLEMSQ

Local variables:
-none

Global variables:
-none

Internal Tools:
-tools/include/process_dog()
-tools/include/init_masque_pt_carac()

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/convert.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include "tools/include/process_dog.h"
#include "tools/include/init_masque_pt_carac.h"
#include <public_tools/Vision.h>


/*typedef struct data_conv_dog{

  float theta1;
  float theta2;
  int   taille_msq;
  int   seuil;
  int   R_exclusion;
  int   mode_pano;
  int   mode_occlusion;
  float mode_minmax;
  float min_global_after_filtering;
  float max_global_after_filtering;
  float head;
  int InputGep;
} Data_conv_dog;*/


void function_conv_dog(int Gpe)
{
   Data_conv_dog  *mydata   = NULL;
   char    *string   = NULL;
   char    param_link[256];

   prom_images_struct *rez_image;

   register int i, j;
   unsigned int nx, ny;
   unsigned int n;
   float valeur_max = 0.0;
   /*float valeur_min = 0.0;*/


   /*unsigned char *retine;*/
   float *retine;
   float *im_tmp;
   float **masque_carac;
   int occlusion;
   int maxi;



#ifdef TIME_TRACE
   gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

   dprints("====debut %s\n",__FUNCTION__);
   /* initialisation */
   if (def_groupe[Gpe].ext == NULL)
   {
      mydata = ALLOCATION(Data_conv_dog);


      mydata->theta2          = DEFAULT_THETA2;
      mydata->theta1          = DEFAULT_THETA1;
      mydata->seuil           = 0;
      mydata->taille_msq      = DEFAULT_TAILLEMSQ;
      mydata->R_exclusion     = 0;
      mydata->mode_pano       = 0;
      mydata->mode_minmax     = -1.;
      mydata->mode_occlusion  = 0;
      mydata->head            = 0;
      mydata->InputGep        = 0;

      /* recuperation des infos sur le gpe precedent */
      mydata->InputGep = liaison[find_input_link(Gpe, 0)].depart;
      string           = liaison[find_input_link(Gpe, 0)].nom;

      if (def_groupe[mydata->InputGep].ext == NULL)
      {
         printf("Gpe amonte avec ext nulle; pas de calcul de ptc\n");
         return;
      }

      /* alocation de memoire */  
      def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)
      {
         printf("ALLOCATION IMPOSSIBLE ...! \n");
         exit(-1);
      }

      printf("\n");
      /* recuperation d'info sur le lien : theta2, theta1, seuil, taille_msq, R_exclusion*/

      if (prom_getopt(string, "-T", param_link) == 2 || prom_getopt(string, "-t", param_link) == 2)
      {
         mydata->theta2 = atof(param_link);printf("theta2 = %f\n", mydata->theta2);
      }

      if (prom_getopt(string, "-R", param_link) == 2 || prom_getopt(string, "-r", param_link) == 2)
      {
         mydata->theta1 = atof(param_link);        printf("theta1 = %f\n", mydata->theta1);
      }
      if (prom_getopt(string, "-S", param_link) == 2 || prom_getopt(string, "-s", param_link) == 2)
      {
         mydata->seuil = atoi(param_link);         printf("Seuil = %d\n", mydata->seuil);
      }
      if (prom_getopt(string, "-P", param_link) == 2 || prom_getopt(string, "-p", param_link) == 2)
      {
         mydata->taille_msq = atoi(param_link);    printf("Taille_msq = %d\n", mydata->taille_msq);
      }
      if (prom_getopt(string, "-RE", param_link) == 2 || prom_getopt(string, "-re", param_link) == 2)
      {
         mydata->R_exclusion = atoi(param_link);   printf("Rayon exclusion = %d\n", mydata->R_exclusion);
      }
      if (prom_getopt(string, "-CP", param_link) == 1 || prom_getopt(string, "-cp", param_link) == 1)
      {
         mydata->mode_pano = 1;                    printf("Mode cam_pano actif\n");
      }
      if (prom_getopt(string, "-MINMAX", param_link) == 2 || prom_getopt(string, "-minmax", param_link) == 2)
      {
         mydata->mode_minmax = atof(param_link);   printf("mode minmax activated: %f\n",mydata->mode_minmax);
      }
      if (prom_getopt(string, "-OCC", param_link) == 1 || prom_getopt(string, "-occ", param_link) == 1)
      {
         mydata->mode_occlusion = 1;               printf("mode occlusion\n");
      }


      /* recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image de ce groupe avec les infos recuperees */
      nx = ((prom_images_struct *) def_groupe[mydata->InputGep].ext)->sx;
      ny = ((prom_images_struct *) def_groupe[mydata->InputGep].ext)->sy;
      def_groupe[Gpe].ext = 	calloc_prom_image(1, nx, ny, 4);
      n = nx * ny;
      dprints ("%s Taille image %d x %d\n",__FUNCTION__,nx,ny);

      /* sauvgarde des infos trouves sur le lien */
      def_groupe[Gpe].data = mydata; 

      if (mydata->theta1<mydata->theta2)
         PRINT_WARNING ("Le parametre R(theta1) doit etre plus grand que T (theta2) sinon ce n'est plus une cellule centre-off");


      /* creation du masque DOG */
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6] =(unsigned char *) init_masque_pt_carac(mydata->taille_msq, mydata->theta2, mydata->theta1);
      masque_carac = (float **) (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6]);

      /*Recherche valeur max pour ce filtre*/
      recherche_valeur_minmax_dog(mydata->taille_msq,masque_carac,&mydata->min_global_after_filtering,&mydata->max_global_after_filtering);
      dprints("Valeur minmax global apres filtrage= %f <-> %f \n",mydata->min_global_after_filtering,mydata->max_global_after_filtering);

      /* recuperation du pointeur de l'image d'entree */
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] = ((prom_images_struct *) def_groupe[mydata->InputGep].ext)->images_table[0];

      /* allocation de memoire *//* attention, pointeurs vers des reels ou des entiers */
      /*((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = (unsigned char *) calloc(n, sizeof(unsigned char));*/

      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = (unsigned char *) calloc(n, sizeof(float));
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =(unsigned char *) calloc(n, sizeof(float));
      if ((((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6] == NULL))
      {
         printf("ALLOCATION IMPOSSIBLE ...! \n");
         exit(-1);
      }

      rez_image = (prom_images_struct *) def_groupe[Gpe].ext;
   }
   else
   {
      rez_image = ((prom_images_struct *) def_groupe[Gpe].ext);
      nx = rez_image->sx;
      ny = rez_image->sy;
      n = nx * ny;     
      mydata = def_groupe[Gpe].data;  

      /*recupere le masque DOG*/
      masque_carac = (float **) rez_image->images_table[6];

      /*recupere l'image d'entree*/
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] = ((prom_images_struct *) def_groupe[mydata->InputGep].ext)->images_table[0];
   }

   /* utilisation des variables locales */
   /* image de sortie */
   /* retine = (unsigned char *) rez_image->images_table[0];*/
   retine = (float *) rez_image->images_table[0];
   /*resultat de la dog*/
   im_tmp = (float *) rez_image->images_table[2];
   /*masque de la dog*/
   masque_carac = (float **) rez_image->images_table[6];



   process_dog_thread(rez_image->images_table[4], mydata->taille_msq, mydata->taille_msq, nx, ny,im_tmp, masque_carac, mydata->seuil);


   /*Renormalisation par le min et le max*/
   maxi = n;  


   valeur_max = im_tmp[0];
   for (i = maxi ;i--;)
   {
      if (im_tmp[i] > valeur_max ) valeur_max = im_tmp[i];
      if (im_tmp[i] < 0 ) im_tmp[i] = 0;
   }
   /*normalisation*/
   for (i = maxi ;i--;)
      retine[i] = (im_tmp[i] )/(valeur_max);



   if ((2 * mydata->R_exclusion >= (int)nx) || (2 * mydata->R_exclusion >= (int)ny))
   {
      printf ("pb dans %s: aucun point de focalisation car R_exclusion trop grand\n", __FUNCTION__);
      exit(0);
   }


   /*Traitement rayon exclusion sur les bords droit et gauche */
   for (i = mydata->R_exclusion; i < ((int)ny - mydata->R_exclusion); i++)
   {
      for (j = mydata->R_exclusion;j--;)
         retine[nx * i + j] = 0;
      for (j = (int)nx - mydata->R_exclusion; j < (int)nx; j++)
         retine[nx * i + j] = 0;
   }

   if (mydata->mode_pano == 0)
   {
      /*Traitement rayon exclusion sur les bords haut et bas */
      for (i = mydata->R_exclusion * nx; i--;)
      {
         retine[i] = 0;
      }
      for (i = ((int)ny - mydata->R_exclusion) * (int)nx; i < (int)ny * (int)nx; i++)
      {
         retine[i] = 0;
      }
   }
   else
   {
      for (j = nx; j--;)
      {
         for (i = mydata->R_exclusion + (int) ((45. / 240.) * ny); i--;)
         {
            if (i < mydata->R_exclusion + (int) ((10. / 240.) * ny))
            {
               retine[nx * i + j] = 0;
            }
            else if ((j > (300. / 1540.) * nx) && (j < ((545. / 1540.) * nx)))
               retine[nx * i + j] = 0;

         }

         for (i = ny - mydata->R_exclusion - (int) ((40. / 240.) * ny); i < (int)ny; i++)
         {
            retine[nx * i + j] = 0;

         }
      }
   }


   if (mydata->mode_occlusion == 1)
   {
      occlusion = mydata->head * (nx - 2 * mydata->R_exclusion) / 2. + mydata->R_exclusion;

      mydata->head = 0.;

      ((float *) (((prom_images_struct *) def_groupe[Gpe].ext)-> images_table[5]))[7] = mydata->head;
      for (i = occlusion;i < occlusion + (nx - 2 * mydata->R_exclusion) * (1. / 2.); i++)
         for (j = 0; j < (int)ny; j++)
         {
            retine[nx * j + i] = 0.;
         }
   }

#ifdef DEBUG
   printf("~~~~fin %s~~~~\n", __FUNCTION__);
#endif

#ifdef TIME_TRACE
   gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
   if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
   {
      SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
      MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
   }
   else
   {
      SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
      MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
   }
   sprintf(MessageFunctionTimeTrace, "Time in function_conv_dog\t%4ld.%06d\n",
         SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
   affiche_message(MessageFunctionTimeTrace);
#endif
}
