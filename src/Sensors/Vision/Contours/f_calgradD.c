/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\ingroup libSensors
\defgroup f_calgradD f_calgradD

\author unknown modified by C. GRAND
\date 06/02/2013
\details
Calcul du gradient d'une image.

\section Links
- lien algo comprenant les options suivantes

\section Options
- alpha :  -Ax avec x in R+
- type de filtre : -Fx avec x in {i,c,q,m}  
Paramètres optionnels :
  - lissage = -L. Il faut bien noter que le lissage est réalisé par défaut, l'ajout de "-L" sur le lien supprime le lissage.                                
  - seuillage = -Sx avec x in R+ (par défaut le seuil est nul).  

\remark 
\file
\ingroup f_calgradD


 ************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/Seuillage.h"
#include "tools/include/bords.h"
/*#include "net_message_debug_dist.h"*/

/*#define DEBUG*/

#define CALGRADD_RE_DEFAULT             0

void filtre_ghv(float *image, float alpha, int nx, int ny);
void fh_ghv(float *image, float alpha, int nx, int ny);
void fv_ghv(float *image, float alpha, int nx, int ny);
void mamphiJ(float *ik, float *in, unsigned Nx, unsigned Ny);


typedef struct data
{
   float s_alpha;
   float seuil;
   int Intensity;
   int R;
   int O;
   int M;
   int C;
   int Q;
   int L;
   int gpe_entree;
   int RE;
} MyData;


#define TAILLE_SQRT 2*2000*2000
int mem_my_sqrt[TAILLE_SQRT];


void init_my_sqrt()
{
   int x;

   for(x=0;x<TAILLE_SQRT;x++)
   {
      mem_my_sqrt[x]=sqrt((float) x);
   }
}

inline int my_sqrt(float x)
{
   return mem_my_sqrt[(int) x];
}

void filtre_ghv(float *image, float alpha, int nx, int ny)
{
   int ix, iy;
   float y, yy, yyy, x, xx;
   float a,a2,b,v1,v2,v3;
   float *p;
   float *pnt;

   a = exp(-alpha);
   a2=a*a;
   b=2*a;

   v1= (1 - a) * (1 - a);
   v2= (1 - a2);
   v3 = (1 - a) / (1 + a);

   for (iy = 0; iy < ny; iy++)
   {
      p = iy * nx + image;
      y = yy = yyy = 0.;
      x = xx = 0.;
      for (ix = 0; ix < nx; ix++)
      {
         pnt = p + ix;
         xx = x;
         x = *(pnt) ;/*x = image[pnt]; */
         yyy = yy;
         yy = y;
         y = v1 * (x + xx) + b * yy - a2 * yyy;
         *(pnt) = y; /*image[pnt] = y;*/
      }

      y = yy = yyy = 0.;
      x = xx = 0.;
      for (ix = nx-1 ; ix--;  )
      {
         pnt = p + ix;
         xx = x;
         x = *(pnt); /*x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v2 * x + (b * yy - a2 * yyy);
         *(pnt) = y;/*image[pnt] = y;*/
      }
   }

   for (ix = 0; ix < nx; ix++)
   {
      p = ix + image;
      y = yy = yyy = 0.;
      x = xx = 0.;
      for (iy = 0; iy < ny; iy++)
      {
         pnt = iy * nx + p;
         xx = x;
         x = *(pnt); /*x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v1 * (x + xx) + b * yy - a2 * yyy;
         *(pnt) = y;/*image[pnt] = y;*/
      }

      y = yy = yyy = 0.;
      x = xx = 0.;
      for (iy = ny -1;  iy--;  )
      {
         pnt = iy * nx + p;
         xx = x;
         x = *(pnt); /*x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v2 * x + (b * yy - a2 * yyy);
         *(pnt) = (y / 4) * v3;/*image[pnt] = (y / 4) * v3;*/
      }
   }
}

void fh_ghv(float *image, float alpha, int nx, int ny)
{
   int ix, iy;
   float y, yy, yyy, x, xx, xxx;
   float a,a2,b,v1,v2;
   float *p;
   float *pnt;

   a = exp(-alpha);
   a2=a*a;
   b=2*a;

   v1= (1 - a) * (1 - a); /*1+a2-b*/
   v2= (1 - a2);
   for (iy = 0; iy < ny; iy++)
   {
      y = yy = yyy = 0.;
      x = xx = 0.;
      p=iy * nx + image;
      for (ix = 0; ix<nx; ix++ )
      {
         pnt = p + ix;
         xx = x;
         x= *(pnt); /* x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v1 * xx + b * yy - a2 * yyy;
         *(pnt) =y; /* image[pnt] = y; */
      }

      y = yy = yyy = 0.;
      x = xx = xxx = 0.;
      for (ix = nx-1 ; ix --; )
      {
         pnt = p + ix; /* pnt = iy * nx + ix; */
         xxx = xx;
         xx = x;
         x= *(pnt);  /*x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v2 * (xxx - x) + (b* yy - a2 * yyy); /*y = (1 - a * a) * (xxx - x) + (2 * a * yy - a * a * yyy);*/
         *(pnt) =y; /* image[pnt] = y; */
      }
   }
}

void fv_ghv(float *image, float alpha, int nx, int ny)
{
   int ix, iy;
   float y, yy, yyy, x, xx, xxx;
   float a,a2,b,v1,v2;
   float *p;
   float *pnt = NULL;

   a = exp(-alpha);
   a2=a*a;
   b=2*a;

   v1= (1 - a) * (1 - a);
   v2= (1 - a2);

   for (ix = 0; ix < nx; ix++)
   {
      y = yy = yyy = 0.;
      x = xx = 0.;
      p=ix + image ;
      for (iy = 0; iy < ny; iy++)
      {
         pnt = iy * nx + p;
         xx = x;
         x= *( pnt); /*x = image[pnt];*/
         yyy = yy;
         yy = y;
         y = v1 * xx + b* yy - a2 * yyy;
         *( pnt) =y;
      }

      y = yy = yyy = 0.;
      x = xx = xxx = 0.;
      for (iy = ny-1 ;  iy--; )
      {
         pnt = iy * nx + p; /* pnt = iy * nx + ix; */
         xxx = xx;
         xx = x;
         x= *( pnt); /* x = image[pnt]; */
         yyy = yy;
         yy = y;
         y = v2 * (xxx - x) + (b * yy - a2 * yyy);
         *( pnt) =y; /* image[pnt] = y; */
      }
   }
}

void mamphiJ(float *ik, float *in, unsigned Nx, unsigned Ny)
{
   int sign;
   unsigned indice, indice1, indice2, indice3, indice4;
   unsigned kn;
   //register int i, j;
   unsigned int i,j;
   float *a;
   float angle;
   float g1, g2, u, ux, uy;
   float v1,v2;

   kn = Nx * Ny;
   if ((a = (float *) calloc(kn, sizeof(float))) == NULL)
   {
      printf("ALLOCATION IMPOSSIBLE ...! \n");
      exit(1);
   }

   /*
    * calcul de l'amplitude
    */
   for (i = kn-1; i--; )
   {
      v1=ik[i] ;v2= in[i];
      a[i] = my_sqrt(v1 * v1 + v2 * v2);
   }
   /*
    * suppression de non maximum
    */
   for (i = 1; i <= Ny - 2; ++i)
      for (j = 1; j <= Nx - 2; ++j)
      {
         indice = i * Nx + j;
         if (isdiff(ik[indice], 0))  angle = in[indice] / ik[indice];
         else                        angle = 2.;

         sign = 1;
         if (angle < 0) sign = -1;

         ux = ik[indice];
         if (ux < 0)   ux = -ux;
         uy = in[indice];

         if (uy < 0)   uy = -uy;
         ik[indice] = 0;

         switch (sign)
         {
         case -1:
            if (angle < -1)
            {
               u = a[indice] * uy;
               indice1 = (i - 1) * Nx + j + 1;
               indice2 = indice1 - 1;
               g1 = ux * a[indice1] + (uy - ux) * a[indice2];
               if (u < g1)
               {
                  ik[indice] = 0;
                  continue;
               }
               indice3 = (i + 1) * Nx + j - 1;
               indice4 = indice3 + 1;
               g2 = ux * a[indice3] + (uy - ux) * a[indice4];
               if (u <= g2)
               {
                  ik[indice] = 0;
                  continue;
               }
            }
            else
            {
               u = a[indice] * ux;
               indice1 = (i - 1) * Nx + j + 1;
               indice2 = indice1 + Nx;
               g1 = uy * a[indice1] + (ux - uy) * a[indice2];
               if (u < g1)
               {
                  ik[indice] = 0;
                  continue;
               }
               indice3 = (i + 1) * Nx + j - 1;
               indice4 = indice3 - Nx;
               g2 = uy * a[indice3] + (ux - uy) * a[indice4];
               if (u <= g2)
               {
                  ik[indice] = 0;
                  continue;
               }
            }
            break;
         case 1:
            if (angle >= 1)
            {
               u = a[indice] * uy;
               indice1 = (i + 1) * Nx + j + 1;
               indice2 = indice1 - 1;
               g1 = ux * a[indice1] + (uy - ux) * a[indice2];
               if (u < g1)
               {
                  ik[indice] = 0;
                  continue;
               }
               indice3 = (i - 1) * Nx + j - 1;
               indice4 = indice3 + 1;
               g2 = ux * a[indice3] + (uy - ux) * a[indice4];
               if (u <= g2)
               {
                  ik[indice] = 0;
                  continue;
               }
            }
            else
            {
               u = a[indice] * ux;
               indice1 = (i - 1) * Nx + j - 1;
               indice2 = indice1 + Nx;
               g1 = uy * a[indice1] + (ux - uy) * a[indice2];
               if (u < g1)
               {
                  ik[indice] = 0;
                  continue;
               }
               indice3 = (i + 1) * Nx + j + 1;
               indice4 = indice3 - Nx;
               g2 = uy * a[indice3] + (ux - uy) * a[indice4];
               if (u <= g2)
               {
                  ik[indice] = 0;
                  continue;
               }
            }
            break;
         }                   /*  fin du switch */

         ik[indice] = a[indice];
      }                       /* fin du for */

   for (j = 0; j < Nx; ++j)
   {
      ik[j] = 0;
      ik[Nx + j] = 0;
      indice = (Ny - 1) * Nx + j;
      ik[indice] = 0;
      indice -= Nx;
      ik[indice] = 0;
   }
   for (i = 0; i < Ny; ++i)
   {
      indice = i * Nx;
      ik[indice] = 0;
      ++indice;
      ik[indice] = 0;
      indice = (i + 1) * Nx - 1;
      ik[indice] = 0;
      --indice;
      ik[indice] = 0;
   }
   for (i = 0; i <= ((Ny - 1) * (Nx + 1)); ++i)     in[i] = a[i];

   free(a);
   return;
}


void new_calgradD(int Gpe)
{
   char ST[256];
   MyData *data=NULL;
   const char *link_name;
   int link, nb_links = 0;

   /* allocation de memoire */
   if(def_groupe[Gpe].data!=NULL)
   {
      cprints("%s : les structures ont deja ete allouees, on ne les realloue pas \n",__FUNCTION__);
      return;
   }

   data = (MyData *) malloc(sizeof(MyData));
   if (data == NULL)	  EXIT_ON_ERROR("error malloc in %s\n", __FUNCTION__);

   /**************************************************************************************/
   /********** Construction de la structure ext du groupe ********************************/
   /**************************************************************************************/
   /*    On recupere les parametre sur le lien entrant:  alpha = -Ax avec x in R+        */
   /*                                                    type = -Fx avec x in {i,c,q,m}  */
   /*    et les parametres optionnels:   lissage : si option -L => pas de lissage        */
   /*                                    seuillage = -Sx avec x in R+                    */
   /* recuperation des infos sur le gpe precedent                                        */
   /**************************************************************************************/

   data->s_alpha = -1;
   data->RE = CALGRADD_RE_DEFAULT;

   for( link = find_input_link(Gpe, 0); link != -1; link=find_input_link(Gpe, nb_links))
   {

      link_name = liaison[link].nom;
      if (strstr(link_name,"sync") != NULL )
         dprints("\nlien sync ");
      else
      {

         /* recuperation d's_alpha */
         if (prom_getopt_float(link_name, "-A", &data->s_alpha) == 2 ) data->gpe_entree = liaison[link].depart;
         if (prom_getopt_float(link_name, "-A", &data->s_alpha) == 1 ) EXIT_ON_ERROR("You must give a positive float following -A to specify the max in %s", link_name);
         if (prom_getopt_float(link_name, "-A", &data->s_alpha) == 0 ) EXIT_ON_ERROR("Error in function_calgradDS (%s),you must specified a positive -Aalpha\n",def_groupe[Gpe].no_name);

         /* recuperation type filtrage */
         data->Intensity = data->O = data->M = data->C = data->Q = 0;

         if (prom_getopt(link_name, "-F", ST) == 2 )
         {
            if (ST[1] == '\0')
            {
               switch (ST[0])
               {
               case 'i':
                  dprints("Filtre i \n");
                  data->Intensity = 1;
                  break;
               case 'c':
                  dprints("Filtre c\n");
                  data->C = 1;
                  break;
               case 'q':
                  dprints("Filtre q\n");
                  data->Q = 1;
                  break;
               case 'm':
                  dprints("Filtre m\n");
                  data->M = 1;
                  break;
               default:   EXIT_ON_ERROR("Type inconnu \n\n Exit in function_calgradDS, Gpe = %d\n", Gpe);
               }
            }
            else     EXIT_ON_ERROR("On attend qu un seul caractere derriere -F");
         }
         else       EXIT_ON_ERROR("Error in function_calgradDS,you must specified -Ftype\n");
      }

      /* recuperation info lissage */
      data->L = 1;
      if (prom_getopt(link_name, "-L", NULL) == 1 ) data->L = 0;
      if(data->L == 1) PRINT_WARNING("Lissage du gradient par défaut, si lissage non voulu rajouté -L sur le lien");
      if(data->L == 0) PRINT_WARNING("Suppression du lissage du gradient, si lissage désiré supprimé le -L sur le lien");

      /* recuperation info sur le seuil */
      data->seuil = 0.0;
      if (prom_getopt_float(link_name, "-S", &data->seuil) == 0 || prom_getopt_float(link_name, "-S", &data->seuil) == 1 ) PRINT_WARNING("Aucun seuil n'est défini");
      dprints("Seuil = %f\n", data->seuil);

      /* recuperation info sur la region d'exclusion */
      if (prom_getopt(link_name, "-RE", ST) == 2)
               data->RE = atoi(ST);

      nb_links ++;
   }
   if (data->s_alpha < 0 )
      EXIT_ON_ERROR("Error in function_calgradDS (%s),you must specified a positive -Aalpha\n",def_groupe[Gpe].no_name);

   def_groupe[Gpe].data = data;
   init_my_sqrt();
}



/******************************************************************************************************************************/
/************************ Traitement: calcul du gradinent  ********************************************************************/
/******************************************************************************************************************************/
/*    On dispose ici des parametre:   Intensity, C, Q, M. Un seul vaut 1, cela depend de l'otion -Fx avec x in {icqm}         */
/*                                    R = O = 0.                                                                              */
/*                                    L = 1 si pas d'option -L, et 0 sinon                                                    */
/*                                    s_alpha = x venant de -Ax                                                               */
/*                                    seuil = 0. ou x venant de l'otion -Sx                                                   */
/******************************************************************************************************************************/


void function_calgradD(int Gpe)
{
   unsigned char *im;
   float *lh, *lv, *tmp;
   prom_images_struct *rez_image=NULL;

   int i,RE;
   unsigned j;
   unsigned int nx, ny;
   unsigned int n;
   int InputGpe = 0;
   float Seuil = 0;
   float D0 = 0., D1 = 0., S0 = 0., S1 = 0.;
   MyData *data=NULL;
#ifdef time_trace
   gettimeofday(&inputfunctiontimetrace, (void *) NULL);
#endif

   dprints("begin f_calgradd\n");

   data = (MyData *) (def_groupe[Gpe].data);
   InputGpe = data->gpe_entree;
   Seuil = data->seuil;
   RE = data->RE;
   if (def_groupe[InputGpe].ext == NULL) /* pas d'entree valide */
   {
      PRINT_WARNING("gpe amont avec ext nulle; pas de calcul de gradient\n");
      return;
   }
   else if(def_groupe[Gpe].ext==NULL)     /* les images n'ont pas encore ete crees */
   {
      def_groupe[Gpe].ext = (prom_images_struct *) malloc(sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)    EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! \n");

      /* recuperation des infos sur la taille */
      nx = ((prom_images_struct *) def_groupe[InputGpe].ext)->sx;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
      ny = ((prom_images_struct *) def_groupe[InputGpe].ext)->sy;
      ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
      n = nx * ny;
      if(((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band!=1) EXIT_ON_ERROR("%s f_calgradD ne sais pas gere autre chose que des images en niveaux de gris nb_band=%d input = %s \n",__FUNCTION__,((prom_images_struct *) def_groupe[InputGpe].ext)->nb_band,def_groupe[InputGpe].no_name) ;
      ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 1;

      ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

      /**************************************************************************************/
      /********** Construction de la structure ext du groupe ********************************/
      /**************************************************************************************/
      /*                                                                                    */
      /*    les parametres sont gardes dans ext->image_table[5]                              */
      /*    Le numero du groupe d'entree est garde dans ext->image_table[4]                 */
      /**************************************************************************************/

      /* allocation de memoire */
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] = (unsigned char *) malloc(n * sizeof(char));
      /* attention, pointeurs vers de reels */
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] = (unsigned char *) malloc(n * sizeof(float));
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] = (unsigned char *) malloc(n * sizeof(float));
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] = (unsigned char *) malloc(n * sizeof(float));
      ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] = (unsigned char *) malloc(1 * sizeof(int));

      if ((((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] == NULL) || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] == NULL) || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] == NULL))   EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! \n");


      /* sauvgarde du numero du groupe amont */
      *(int *) (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4]) = InputGpe;
   }

   rez_image = ((prom_images_struct *) def_groupe[Gpe].ext);
   nx = rez_image->sx;
   ny = rez_image->sy;
   n = nx * ny;
   dprints("nx=%d ny=%d \n",nx,ny);
   dprints("s_alpha=%f\n",data->s_alpha);
   dprints("seuil=%f\n",data->seuil);  /* utilisation des variables locales */

   /********************************************************************/
   /* on copie l'image d'entree dans   im=ext->image_table[0]          */
   /*                                  tmp=ext->image_table[3]         */
   /*                                  lh=ext->image_table[1]          */
   /*                                  lv=ext->image_table[2]          */
   /********************************************************************/

   im = (unsigned char *) rez_image->images_table[0];
   lh = (float *) rez_image->images_table[1];
   lv = (float *) rez_image->images_table[2];
   tmp = (float *) rez_image->images_table[3];

   memcpy(rez_image->images_table[0], ((prom_images_struct *) def_groupe[InputGpe].ext)->images_table[0], n * sizeof(char));
   for (i = n ; i-- ; )
      tmp[i] = lh[i] = lv[i] = (float) im[i];

   /**************************************/
   /* Lissage    si pas option -L        */
   /*            sinon pas de lissage    */
   /**************************************/
   if (data->L)
   {

      filtre_ghv(tmp, data->s_alpha, nx, ny);
      for (j = 1; j < n - nx; j++)
      {
         D1 = tmp[j + nx] - tmp[j] ;
         S1 = tmp[j + nx] + tmp[j] ;
         S1 = S1/2.;
         lh[j - 1] = (D0 + D1)/2.;
         lv[j - 1] = (S1 - S0);
         S0 = S1;
         D0 = D1;
      }
   }
   else
   {
      /*calcul du gradient en horizontal et vertical*/
      fh_ghv(lv, data->s_alpha, nx, ny);
      fv_ghv(lh, data->s_alpha, nx, ny);
   }
   /* O different de 0 toujours etant donne le code actuel */
   if (data->O == 1)
   {
      EXIT_ON_ERROR("\n\n Le resultat est de type float !!! A VOIR\n\n");
      /*strcpy(NomOUT, NomImage);strcat   (NomOUT, "-o.gdr");
                        O_out(NomOUT,p_im,lh, lv); */
   }

   /***********************************************************************/
   /* amphi(lv,lh): Extraction des maxima locaux                 */
   /* ------------        en retour lv contient les maxima locaux,       */
   /*             lh contient la norme du gradient                       */
   /*********************************************************************/
   /* amphi(lv,lh,nx,ny); */

   /*calcul dans lv le gradient "aminci" (suivi du max) et dans lh la norme*/
   /*mamphiJ(lv, lh, nx, ny); */   /* original lounis */
   /* bords(lh, BORDS, n, nx, ny);
           bords(lv, BORDS, n, nx, ny); */

   /* On effectue le traitement associe a l'ootion -Fx qui definit le type I, C, Q, M */
   if (data->Intensity == 1)
   {
      /*strcpy(NomOUT, NomImage);strcat   (NomOUT, "-i.gdr");
	          I_out(NomOUT,p_im,im,lh, lv); */
      for (i = n-1; i--; )
      {
         /* a decommenter pour utiliserle code de Lounis*/
         /*im[i] = (unsigned char) floor(lh[i]);*/

         /*  im[i]= floor(sqrt(lh[i]*lh[i]+lv[i]*lv[i])) ; */
         /* im[i]= my_sqrt(lh[i]*lh[i]+lv[i]*lv[i]) ; */
         /* printf("i=%d '%d,%d) lh=%f lv=%f %d\n",i,i%nx,i/nx,  lh[i],lv[i],(int)((lh[i]*lh[i]+lv[i]*lv[i]))); */
         /*    im[i]= mem_my_sqrt[ (int) (lh[i]*lh[i]+lv[i]*lv[i]) ] ; */

         im[i]= (int)( fabs(lh[i])+ fabs(lv[i]) );
         im[i]= im[i]>>2 ; /* division par 2 */
         im[i] = (im [i] < (unsigned char)Seuil ? 0 : (unsigned char)im[i]);
         /*im[i] = (im [i] < (unsigned char)Seuil ? 0 : 255);*/
      }
   }
   if (data->C == 1)
   {
      /*strcpy(NomOUT, NomImage);strcat   (NomOUT, "-c.gdr");
	           C_out(NomOUT,p_im,im,lv,seuil); */

      /* Seuillage(data->seuil, lv, im, n);  PG: code de l'ancienne version */

      Seuil=2*Seuil;
      for (i = n-1; i--; )
      {
         im[i]= (int)( fabs(lh[i])+ fabs(lv[i]) );
         im[i] = (im [i] < Seuil ? 0 : 255);
      }
   }

   if (data->M == 1)
   {
      /*strcpy(NomOUT, NomImage);strcat   (NomOUT, "-m.gdr");
	          M_out(NomOUT,p_im,im,lv); */
      for (i = n; i--; )
         im[i] = (unsigned char) floor(lv[i]);
   }

   if(RE >0 )
   {
   /*Traitement rayon exclusion sur les bords droit et gauche */
      for (i = RE; i < ((int)ny - RE); i++)
      {
         for (j = RE;j--;)
            im[nx * i + j] = 0;
         for (j = (int)nx - RE; j < (int)nx; j++)
            im[nx * i + j] = 0;
      }
      /*Traitement rayon exclusion sur les bords haut et bas */
      for (i = RE * nx; i--;)
      {
         im[i] = 0;
      }
      for (i = ((int)ny - RE) * (int)nx; i < (int)ny * (int)nx; i++)
      {
         im[i] = 0;
      }
   }
}
