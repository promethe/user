/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_paquerette.c
\brief

Author: A.Jauffret
Created: 14/05/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Cette fonction découpe l'image en 32 secteurs angulaires (32 pétales) et calcul la quantité de gradient bien orienté comprise dans chaque pétale.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-f_orient

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: La fonction doit recevoir en entrée un tableau des orientations du gradient (donné par f_orient). 
					Elle doit également sortir un lien secondaire de rétroaction vers f_seuil_dynamic.

					- Le paramètre à passer sur le lien secondaire de rétroaction est : "feedback"

					- Les paramètres à passer sur le lien d'entrée sont :	

									-Z  pour définir la zone de l'image [0 -> extrème gauche]
																											[1 -> gauche]
																											[2 -> centre]
																											[3 -> droite]
																											[4 -> extrème droite]

									-T	pour définir la tolérance (en radians, exemple : 0.3)

									-E  pour définir l'echelle de discrétisation (exemple : 8 --> pour ne calculer qu' 1/8 pixel)

									-H  pour définir la hauteur de l'horizon (attention on se base sur le haut de l'image --> exemple : 0.25 place l'horizon a 25% de 																														l'image en partant du haut) 	 
					
									

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <libx.h>

#include <Struct/convert.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include "Vision/Contours.h"
#include "Vision/Image_Processing.h"
#include <Kernel_Function/prom_getopt.h>

/* Echantillonnage de l'image */
#define	ECHELLE	8

/*attention penser a modifier aussi dans f_orient*/
#define NOPOINT -10.  /* flag indiquant les pixels sans interêt */

/* Tolerance en radian sur l'orientation */
#define	TOLERANCE	0.1

/* dimension des zones */
#define ext_gauche_dim 0.1641
#define gauche_dim 0.328
#define centre_h_dim 0.5
#define droite_dim 0.664
#define ext_droite_dim 0.828
#define deb_dim 0.04
#define fin_dim 0.04
#define deb_h_dim 0.04
#define fin_h_dim 0.04
#define HORIZON 0.25


typedef struct MyData{
  int gpe_orient;
	int zone;
	float tolerance;
	int echelle;	
	float horizon;
} MyData; 


/********************************************************************/
void new_paquerette(int gpe){
  
  int 								i,j,l;
  MyData 							*mydata=NULL;
	char 								param_link[256];
  
	#ifndef AVEUGLE
	init_rgb_colormap16M();
	#endif

  /* On enregistre les informations concernant le numero du 
     groupe contenant l'image en entree*/
    
  /* Recuperation de la valeur des parametres de chaque
     lien en entree du groupe  */
  
	if (def_groupe[gpe].data == NULL)
   	{      
		mydata=(MyData*)malloc(sizeof(MyData));
		if(mydata==NULL) 
		{
			printf("pb malloc dans %d\n",gpe);
			exit(0);
		}
		
		mydata->gpe_orient = -1;
		mydata->tolerance = TOLERANCE;
		mydata->echelle = ECHELLE;
		mydata->horizon = HORIZON;

		l = 0;
		j = find_input_link(gpe, l);
		
		while (j != -1)
			{
					
				if (prom_getopt(liaison[j].nom, "-Z", param_link) == 2 || prom_getopt(liaison[j].nom, "-z", param_link) == 2)
					{
						mydata->gpe_orient=liaison[j].depart;
				
						mydata->zone=atoi(param_link);

						if(mydata->zone>4||mydata->zone<0){
							printf("Error in f_paquerette : missing zones input link\n");
							exit(2);
						}
					}

				if (prom_getopt(liaison[j].nom, "-T", param_link) == 2 || prom_getopt(liaison[j].nom, "-t", param_link) == 2)
					{
							mydata->tolerance = atof(param_link);
					}	

				if (prom_getopt(liaison[j].nom, "-E", param_link) == 2 || prom_getopt(liaison[j].nom, "-e", param_link) == 2)
					{
							mydata->echelle = atoi(param_link);
					}	

				if (prom_getopt(liaison[j].nom, "-H", param_link) == 2 || prom_getopt(liaison[j].nom, "-h", param_link) == 2)
					{
							mydata->horizon = atof(param_link);
					}	
							
				l++;
				j = find_input_link(gpe, l);
			
			}
      
  	if (mydata->gpe_orient == -1)
			{
				fprintf(stderr, "ERROR in new_paquerette(%s): Missing direction input link\n", def_groupe[gpe].no_name);
				exit(1);
			}
	
 	}	
  for(i=0 ; i<def_groupe[gpe].nbre ; i++)
   				{
      			neurone[def_groupe[gpe].premier_ele+i].s =0;
    			}
	def_groupe[gpe].ext=NULL;
  def_groupe[gpe].data=mydata; /* sauvegarde de My_Data*/
}


void destroy_paquerette(int gpe){
	(void) gpe;
}


void function_paquerette(int gpe)
{	
  MyData 							*mydata=NULL;
  unsigned char  			*grad=NULL;
  float 							*ori=NULL;
  prom_images_struct 	*orient_struct=NULL;

  int				i=0,j,k,indice;
  float			direction; /* Secteur angulaire en cours */
  float			angl=0.;
	int 			taillegroupe ;
  int				quadrant_g=0,quadrant_d=0;	/* compteur dans quadrants bas point en cours */
  int				accumulateur=0;
  int 			deb1;
	/*int				nmin = 5;*/
	float			angle ;
  float			largeur,hauteur,ext_gauche,gauche,centre_h,droite,ext_droite,centre_v,deb,fin,deb_h,fin_h;
  float     tolerance=0.3;
	/*int				n;*/

	#ifndef AVEUGLE
	float 	t;
	TxPoint oripoint;
	TxPoint extpoint;
	TxPoint oripoint1;
	TxPoint	oripoint4;
	#endif

	mydata = def_groupe[gpe].data;
  
  /*debut du groupe courant*/
  deb1 = def_groupe[gpe].premier_ele;
  taillegroupe = def_groupe[gpe].nbre;
  angle = M_PI/(float)taillegroupe;  
	

  /* ----------------------------------------------------- */
  /* Recuperation de l'orientation et du gradient binarisé */
  /* ----------------------------------------------------- */
	if(((prom_images_struct *)(def_groupe[mydata->gpe_orient].ext))==NULL)
		{
			printf("erreur: pas d'ext dans le groupe avant f_paquerette\n");
			exit(-1);
		}      
  orient_struct=(prom_images_struct *) (def_groupe[mydata->gpe_orient].ext);
	ori =(float *) orient_struct->images_table[0];
	grad =(unsigned char *)  orient_struct->images_table[1];


	/* calcul des constantes de dimension de l'image */
  largeur = orient_struct->sx;
  hauteur = orient_struct->sy;
  ext_gauche = ext_gauche_dim*largeur;
  gauche = gauche_dim*largeur;
  centre_h = centre_h_dim*largeur;
  droite	= droite_dim*largeur;
  ext_droite = ext_droite_dim*largeur;
  centre_v = mydata->horizon*hauteur;  
  
  /* Bordure de l'image non visitee */
  deb = deb_dim*largeur;		
  fin = fin_dim*largeur;
	deb_h = deb_h_dim*hauteur;
	fin_h = fin_h_dim*hauteur;
  

/**********************************************************************************************************/
   
      /* --- */
      /* RAZ */
      /* --- */      
      
      quadrant_g=quadrant_d=0;
      	
      accumulateur=0; /* neurones integrateurs*/

      /*initialisation des neurones du groupe courant*/
 					for(i=0 ; i<taillegroupe ; i++)
   				{
      			neurone[deb1+i].s1 = neurone[deb1+i].s2=0;
    			}
      

      /* ------------------- */
      /* Balayage de l'image */
      /* ------------------- */
      for (i=deb;i<largeur-fin;i+=mydata->echelle)
	    	{
	     	for (j=deb_h;j<hauteur-fin_h;j+=mydata->echelle)
	     	 { 
	      	 indice=i+j*largeur;
	      
	       	 for(k=0;k<taillegroupe;k++)
		     		 {
		       		 direction=2*k*angle-M_PI;	/* Direction du secteur angulaire */
		  
			
		  				 if (ori[indice]>NOPOINT)		/* Zone d'interet */ 
		   					 {
		    					 if (fabs(j-centre_v) > 0.001)
										{
											if(mydata->zone==0){		
												if (fabs(i - ext_gauche) > 0.001)
													angl=-(float)atan2(j-centre_v,i-ext_gauche);
												else	angl=0.;
											}

											else if(mydata->zone==1){					
												if (fabs(i - gauche) > 0.001)
													angl=-(float)atan2(j-centre_v,i-gauche);
												else	angl=0.;
											}

											else if(mydata->zone==2){
												if (fabs(i - centre_h) > 0.001)
													angl=-(float)atan2(j-centre_v,i-centre_h);
												else	angl=0.;
											}
																
											else if(mydata->zone==3){		
												if (fabs(i - droite) > 0.001)
													angl=-(float)atan2(j-centre_v,i-droite);
												else	angl=0.;
											}

											else if(mydata->zone==4){		
												if (fabs(i - ext_droite) > 0.001)
													angl=-(float)atan2(j-centre_v,i-ext_droite);
												else	angl=0.;
											}
											else{
												printf("erreur, ne correspond à aucune zone !\n");
											}

									}
		     					else
									{
											printf("erreur, ne correspond à aucun neurone !\n");	
											angl=0.;										 
											exit(2);
									}

									if(((fabs(direction) > 0.2)&&(fabs(direction) < 2.9)&&(fabs(direction) > 1.9)) || ((fabs(direction) > 0.2)&&(fabs(direction) < 2.9)&&(fabs(direction) < 1.3)))	/* inhibition des directions verticales et horizontales */	  
									{							
							
									if (fabs(angl-direction)<angle) /*selection du secteur angulaire (position du point concerne)*/
										{
										
											/*ramenne sur -pi/2 ; pi/2 comme ori!!!*/
											if (angl>M_PI_2) angl-=M_PI; /* pi/2+alpha = -alpha */
											if (angl<=-M_PI_2)	angl+=M_PI; /* -pi/2-alpha = alpha */
																				
										
											if (fabs(angl-ori[indice])<tolerance) /* comparaison entre orientation gradient et secteur angulaire  */
												{	
																				
												if (!((fabs(angl-ori[indice])>(M_PI_2-tolerance) && fabs(angl-ori[indice])<(M_PI_2+tolerance))&&(fabs(angl-ori[indice])>(-M_PI_2-tolerance) && fabs(angl-ori[indice])<(-M_PI_2+tolerance)))) /* inhibition des orientations verticales et horizontales */
													{
													/* On compte un matching de plus pour le pétale courant */
													neurone[deb1+k].s1 +=1;
													neurone[deb1+k].s2 +=1;
											 		
													/********* Debug : Affichage des points et de leur orientation ************/
													/**************************************************************************/ 
													#ifndef AVEUGLE		
													oripoint.y = (int)(indice/largeur);
													oripoint.x = (indice - (oripoint.y*largeur)) - 1 + mydata->zone*350;											
		
													t=ori[indice]+M_PI_2;											
													if(t>=2*M_PI)t-=2*M_PI;
													else if(t<0)t+=2*M_PI;
												
													extpoint.x = oripoint.x + 12*cos(t);
													extpoint.y = oripoint.y + 12*sin(t);			
											
													/*centre*/											
													oripoint1.y = centre_v ;	
													oripoint1.x = ext_gauche + mydata->zone*(gauche-ext_gauche) + mydata->zone*350;	
													/*axe*/											
													/*oripoint2.y = centre_v + 40*sin(M_PI);	
													oripoint2.x = centre_h + 40*cos(M_PI);	
													oripoint3.y = centre_v + 40*sin(M_PI_2);	
													oripoint3.x = centre_h + 40*cos(M_PI_2);	*/
													/*direction*/
												
													t=angl;/*-M_PI;*/
													if(t>=2*M_PI)t-=2*M_PI;
													else if(t<0)t+=2*M_PI;	
													oripoint4.y = (int)(indice/largeur)+ 16*sin(t);	
													oripoint4.x = oripoint.x - 1 + 16*cos(t);	

													/*TxDessinerSegment_rgb16M(&image1, rgb16M(128,128,128), oripoint, oripoint1, 1);*/
													/*TxDessinerSegment_rgb16M(&image1, rgb16M(0,255,0), oripoint1, oripoint2, 2);*/
													/*TxDessinerSegment_rgb16M(&image1, rgb16M(255,0,0), oripoint1, oripoint3, 2);*/
													
													/*switch(mydata->zone)
													{

															case 0:
																	TxDessinerSegment_rgb16M(&image1, rgb16M(255,0,0), oripoint, extpoint, 2);break;
															case 1:
																	TxDessinerSegment_rgb16M(&image1, rgb16M(0,255,0), oripoint, extpoint, 2);break;
															case 2:												
																	TxDessinerSegment_rgb16M(&image1, rgb16M(0,0,255), oripoint, extpoint, 2);break;
															case 3:												
																	TxDessinerSegment_rgb16M(&image1, rgb16M(255,255,0), oripoint, extpoint, 2);break;										
															default:
																	TxDessinerSegment_rgb16M(&image1, rgb16M(0,255,255), oripoint, extpoint, 2);break;
																	
													}
																						
													TxFlush(&image1);*/
													#endif
													/**************************************************************************/ 

												}/*verticale,horizontale*/
																		
											}/*angl-ori*/
										}/*angl-dir*/

										/*printf("angl : %f  direction : %f  fabs(angl-direction) : %f  angle : %f\n",angl,direction,fabs(angl-direction),angle);*/
								}

		   		 	}				
		  
					}
	  		
				}
	
 			}

			/*pour chaque compteur (accumulateur des petal par neurone) on divise en 2 cadrants bas (en desous de la ligne d'horizon): gauche et droite*/
			for (i=0;i<taillegroupe/4;i++)	/* Quart d'image */
			{
					quadrant_g+=neurone[deb1+i].s1;	quadrant_d+=neurone[deb1+taillegroupe/2-i].s1;
					
			}
				  

			/*NC: reglage dynamique du seuil : ex :comparer non plus par rapport à un seuil fixe mais une difference entre un neurone max et min*/
		  /* Il faut qu'il y ait de l'information dans l'un des quadrants du bas */
		/*  if ((quadrant_g>nmin)||(quadrant_d>nmin))
			{
				neurone[deb1].s =0;
				
			}
			else{

					printf("\nLe seuil est trop haut !!\n");*/
					
					/*initialisation des neurones du groupe courant*/
					
 	/*		for(i=0 ; i<taillegroupe ; i++)
   				{
      			neurone[deb1+i].s1 = neurone[deb1+i].s2=1;
    			}
					neurone[deb1].s+= 0; 				

			}     		 
 
		}*/	/* voir feedback dans f_orient */
		/* end while */

}

/**********************************************************************************************************/

