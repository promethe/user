/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**************************************************************

\ingroup libSensors
\defgroup f_gradient_vectoriel f_gradient_vectoriel

\author K. Furet
\date 07/2013
\details
Le groupe effectue un traitement basé sur le gradient et le produit vectoriel, qui améliore la détection de coins.

Pour chacun des pixels, le calcul réalisé est :

\f$ k = \frac{Ix^2*<Iy^2>+Iy^2*<Ix^2>-2*Ix*Iy*<Ix*Iy>}{{<Ix^2>+<Iy^2>}}\f$

Où Ix est le gradient en x, Iy le gradient en y, <Ix> la convolution du gradient par la matrice 3x3 suivante :
1 1 1
1 0 1
1 1 1

Si le groupe suivant est f_compet_ptc, il est nécessaire de rajouter -NORM sur le lien
pour assurer de convertir les valeurs entre 0 et 1, comme attendu dans f_compet_ptc.

\section Links
- Lien algorithmique provenant de f_calgradD, portant les options ci-dessous:

\section Options
- Zone d'exclusion : Défini la taille du "cadre" créé autour de l'image pour supprimer les effets de bord : -REX avec X dans R+
- Normalisation : Permet de normaliser les valeurs du gradient vectoriel, dans le cas de l'utilisation ensuite du groupe f_compet_ptc : -NORM
- Exemple : -RE10-NORM

\file
\ingroup libSensors

http://www.doxygen.org
 **************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Struct/convert.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include "tools/include/competition.h"
#include <public_tools/Vision.h>
#include "Vision/Contours.h"
#include "Vision/Image_Processing.h"

typedef struct data_gradient_vectoriel
{
   int input_group;
   int exclusion;
   int normalisation;
   float * ixy;
   float * ix2;
   float * iy2;
   unsigned int nx;
   unsigned int ny;
   unsigned int nb_band;
} Data_gradient_vectoriel;



void new_gradient_vectoriel(int Gpe)
{
   Data_gradient_vectoriel *my_data=NULL;
   int input_group=-1, exclusion = -1, normalisation = 0;
   int i, l;
   char *string = NULL;
   char param_link[256];

   dprints(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);


   /**
    * On recherche les liens et les Gpes des boites entrantes
    */
   i = 0;
   l = find_input_link(Gpe, i);
   while (l != -1)
   {
      string = liaison[l].nom;
      if (strstr(string,"sync") != NULL )
      {printf("\nlien sync "); }
      else
      {
         input_group = liaison[l].depart;
         if (prom_getopt(string, "-re", param_link) >= 2 || prom_getopt(string, "-RE", param_link) >= 2)
            exclusion = atoi(param_link);
         if (prom_getopt(string, "-norm", param_link) == 1 || prom_getopt(string, "-NORM", param_link) == 1)
            normalisation = 1;
      }

      i++;
      l = find_input_link(Gpe, i);
   }

   if (input_group == -1)
      EXIT_ON_ERROR("Missing input link\n");
   if (exclusion == -1) {
      exclusion = 3;
      PRINT_WARNING("Parametre d'exclusion de l'effet de bord par defaut (%d) utilise dans %s\n", exclusion, def_groupe[Gpe].no_name);
   }

   my_data = (Data_gradient_vectoriel *) malloc(sizeof(Data_gradient_vectoriel));
   if (my_data == NULL)    EXIT_ON_ERROR("\nerror in memory allocation");
   if (input_group == -1)     EXIT_ON_ERROR("\nthere is no group at the input of %s",def_groupe[Gpe].no_name);

   my_data->input_group = input_group;
   my_data->exclusion = exclusion;
   my_data->normalisation = normalisation;
   def_groupe[Gpe].data = my_data;


   dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);
}


void function_gradient_vectoriel(int gpe)
{

   Data_gradient_vectoriel * my_data = NULL;
   prom_images_struct * input_ext = NULL;
   prom_images_struct * output_ext = NULL;

   float * output = NULL;
   float * ix = NULL, * iy = NULL, * ixy = NULL, * ix2 = NULL, * iy2 = NULL, * resultat = NULL;
   float ixyc = 0, ix2c = 0, iy2c = 0, norme = 0., max = 0.;
   int n = 0, i, i1, i2, j, k, j1, k1, j2, k2, exclusion, normalisation, nx, colonne;

   /******* Recuperation de la structure *******/
   my_data = (Data_gradient_vectoriel *) def_groupe[gpe].data;

   if (def_groupe[gpe].ext == NULL) {

      dprints(" ~~~~~~~ Debut %s ~~~~~~~\n", __FUNCTION__);

      if (my_data == NULL) EXIT_ON_ERROR("Error retrieving data (NULL pointer)\n");

      /* Initialisation de my_data */
      my_data->nx = 0;
      my_data->ny = 0;
      my_data->nb_band = 0;

      /* Si la sortie du groupe precedent est vide, on arrete tout */
      if (def_groupe[my_data->input_group].ext == NULL) {
         PRINT_WARNING("Input group has not send anything. Without any image, it's impossible to perform anything.\n");
         return;
      }

      /* Allocation de memoire pour la sortie */
      def_groupe[gpe].ext = (void *) malloc(sizeof(prom_images_struct));
      if (def_groupe[gpe].ext == NULL)  EXIT_ON_ERROR("Error in memory allocation of output image\n");

      /* Recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image du groupe courant */
      input_ext = (prom_images_struct *) def_groupe[my_data->input_group].ext;
      my_data->nx = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->sx;
      my_data->ny = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->sy;
      my_data->nb_band = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->nb_band;

      dprints("%s Taille image %d x %d\n", __FUNCTION__, my_data->nx, my_data->ny);
      //if (my_data->nb_band !=4) EXIT_ON_ERROR("%s Input image must be of float type\n", __FUNCTION__);

      /* Allocation de memoire pour la sortie */
      output_ext = calloc_prom_image(1, my_data->nx, my_data->ny, 4);
      n = my_data->nx * my_data->ny;
      output_ext->images_table[0] = (unsigned char *) calloc(n, sizeof(float));
      if (output_ext == NULL || output_ext->images_table[0] == NULL  || input_ext->images_table[0] == NULL)
         EXIT_ON_ERROR("Error in memory allocation in %s line %s\n", def_groupe[gpe].no_name, __LINE__);
      if (input_ext->images_table[1] == NULL || input_ext->images_table[2] == NULL)
         EXIT_ON_ERROR("input group is not sending the required images in %s line %s\n", def_groupe[gpe].no_name, __LINE__);

      /* Sauvegarde des infos trouvees sur le lien */
      def_groupe[gpe].ext = output_ext;

      /******* Allocation des variables de calcul *******/
      my_data->ixy = (float *) malloc(n * sizeof(float));
      my_data->ix2 = (float *) malloc(n * sizeof(float));
      my_data->iy2 = (float *) malloc(n * sizeof(float));

   }

   /******* Recuperation des parametres *******/
   n = my_data->nx * my_data->ny;
   exclusion = my_data->exclusion;
   normalisation = my_data->normalisation;
   ixy = my_data->ixy;
   ix2 = my_data->ix2;
   iy2 = my_data->iy2;
   input_ext = ((prom_images_struct *) (def_groupe[my_data->input_group].ext));
   output_ext = ((prom_images_struct *) (def_groupe[gpe].ext));

   /******* Initialisation des variables de travail *******/
   nx = (int) my_data->nx;
   ix = (float *) input_ext->images_table[1];
   iy = (float *) input_ext->images_table[2];
   output = (float *) output_ext->images_table[0];

   /******* Sortie *******/
   resultat = output;

   /******* Debut du calcul du gradient *******/

   for (i = n-1; i--;) {

      /******* Initialisation *******/
      ixy[i] = ix[i]*iy[i];
      ix2[i] = ix[i]*ix[i];
      iy2[i] = iy[i]*iy[i];

   }

   for (i = exclusion * nx + exclusion; i < n-exclusion*(nx+1); i++) {

      colonne = i % nx;
      if (colonne < exclusion || colonne > nx-exclusion) {

         /******* Effet de bord *******/
         resultat[i] = 0.;

      } else {

         /******* Convolution *******/
         i1 = i - 1;
         i2 = i + 1;
         j = i - nx - 1;
         j1 = j + 1;
         j2 = j1 + 1;
         k = i + nx - 1;
         k1 = k + 1;
         k2 = k1 + 1;

         ixyc = (ixy[j] + ixy[j1] + ixy[j2] + ixy[i1] + ixy[i2] + ixy[k] + ixy[k1] + ixy[k2])/8.;
         ix2c = (ix2[j] + ix2[j1] + ix2[j2] + ix2[i1] + ix2[i2] + ix2[k] + ix2[k1] + ix2[k2])/8.;
         iy2c = (iy2[j] + iy2[j1] + iy2[j2] + iy2[i1] + iy2[i2] + iy2[k] + iy2[k1] + iy2[k2])/8.;

         /******* Norme *******/
         norme = ix2c + iy2c;

         /******* Calcul du resultat *******/
         resultat[i] = iy2[i]*ix2c + ix2[i]*iy2c - 2*ixy[i]*ixyc;

         /******* Division par la norme du resultat *******/
         if (norme >= 1.)
            resultat[i] /= norme;

      }

   }

   /******* Normalisation si passee en option *******/
   if (normalisation == 1) {

      for (i = n-1; i--;)
         if (resultat[i] > max)
            max = resultat[i];

      for (i = n-1; i--;)
         if (resultat[i] > 0.)
            resultat[i] /= max;

   }

   /******* Fin du calcul du gradient *******/

   dprints(" ~~~~~~~ Fin %s ~~~~~~~\n", __FUNCTION__);

}

void destroy_gradient_vectoriel(int gpe)
{
   Data_gradient_vectoriel * my_data = NULL;
   my_data = (Data_gradient_vectoriel *) def_groupe[gpe].data;
   free(my_data->ixy);
   free(my_data->ix2);
   free(my_data->iy2);
   free(def_groupe[gpe].data);
   free(def_groupe[gpe].ext);
   def_groupe[gpe].data = NULL;
   def_groupe[gpe].ext = NULL;
   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


