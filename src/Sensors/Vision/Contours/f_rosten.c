/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include "tools/include/macro.h"
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/recherche_pt_carac.h"
#include "tools/include/init_masque_pt_carac.h"
#include <public_tools/Vision.h>

typedef struct { int x, y; } xy; 
typedef unsigned char byte;

xy*  fast_nonmax(const byte* im, int xsize, int ysize, xy* corners, int numcorners, int barrier, int* numnx);
int corner_score(const byte*  imp, const int *pointer_dir, int barrier);
void nonmax(byte* im, int xsize, int ysize, int rayon);

void function_rosten(int gpe)
{
	prom_images_struct *input=NULL,*output=NULL;
	const byte* im;
	int xsize, ysize, nb_band;
	int barrier=30;
	char param[255];
	
	int gpe_image=-1;
	int rayon=3;
	int i=0;
	int j;

	int boundary , y, cb, c_b;																
	const byte  *line_max, *line_min;															
	int			rsize, total;																
	xy	 		*ret ;											
	const byte* cache_0;
	const byte* cache_1;
	const byte* cache_2;
	int	pixel[16];	

	int cpt=0;

	while( (j=find_input_link(gpe,i)) != -1 ) {
		
		if(strcmp(liaison[j].nom,"image")==0)
			gpe_image=liaison[j].depart;
		if(prom_getopt(liaison[j].nom, "R", param)==2) {
			rayon=atoi(param);
			printf("rayon = %d\n",rayon);
			if ( rayon < 3 ) {
				printf("Rayon trop petit dans %s\n",__FUNCTION__);
				exit(0);
			}
		}
		if(prom_getopt(liaison[j].nom, "S", param)==2) {
			barrier=atoi(param);
			printf("seuil = %d\n",barrier);
			if ( barrier <= 0 ) {
				printf("Seuil negatif %s\n",__FUNCTION__);
				exit(0);
			}
		}
		
		i++;	
	}
	if (gpe_image == -1) {
		printf("pas de groupe en entree avec le lien 'image'\n");
		exit(0);
	}
	
	
	
/* 	printf("function_rosten :%d - %s\n",gpe_image,def_groupe[gpe_image].nom);*/
	input=((prom_images_struct *)(def_groupe[gpe_image].ext));
	if ( input == NULL )
		return;
	
	im=(const byte*)input->images_table[0];
	xsize=input->sx;
	ysize=input->sy;
	
	/*Initialisation de l'image resultat*/
	nb_band=input->nb_band; /* le nombre de bandes */
	/* allocation de "1" image de taille "sx"*"sy"*"nb_band"*/
	output=calloc_prom_image(1, xsize, ysize, nb_band);
	
																								
	boundary = rayon;																
																
	rsize=512;
	total=0;																
	ret = (xy*)malloc(rsize*sizeof(xy));											
	
	pixel[0] = 0 + 3 * xsize;		
	pixel[1] = 1 + 3 * xsize;		
	pixel[2] = 2 + 2 * xsize;		
	pixel[3] = 3 + 1 * xsize;		
	pixel[4] = 3 + 0 * xsize;		
	pixel[5] = 3 + -1 * xsize;		
	pixel[6] = 2 + -2 * xsize;		
	pixel[7] = 1 + -3 * xsize;		
	pixel[8] = 0 + -3 * xsize;		
	pixel[9] = -1 + -3 * xsize;		
	pixel[10] = -2 + -2 * xsize;		
	pixel[11] = -3 + -1 * xsize;		
	pixel[12] = -3 + 0 * xsize;		
	pixel[13] = -3 + 1 * xsize;		
	pixel[14] = -2 + 2 * xsize;		
	pixel[15] = -1 + 3 * xsize;
	
	for(y = boundary ; y < ysize - boundary; y++)												
	{																							
		cache_0 = im + boundary + y*xsize;														
		line_min = cache_0 - boundary;															
		line_max = im + xsize - boundary + y * xsize;											
																								
		cache_1 = cache_0 + pixel[5];
		cache_2 = cache_0 + pixel[14];
																								
		for(; cache_0 < line_max;cache_0++, cache_1++, cache_2++)
		{
			cpt++;
			output->images_table[0][y*xsize+cache_0-line_min]=0.;																			
			cb = *cache_0 + barrier;															
			c_b = *cache_0 - barrier;															
            if(*cache_1 > cb)
                if(*cache_2 > cb)
                    if(*(cache_0+3) > cb)
                        if(*(cache_0 + pixel[0]) > cb)
                            if(*(cache_0 + pixel[3]) > cb)
                                if(*(cache_0 + pixel[6]) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[15]) > cb)
                                            if(*(cache_0 + pixel[1]) > cb)
                                                goto success;
                                            else if(*(cache_0 + pixel[1]) < c_b)
                                                continue;
                                            else
                                                if(*(cache_0 + pixel[10]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        if(*(cache_0 + pixel[7]) > cb)
                                                            if(*(cache_0 + pixel[9]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else if(*(cache_0 + pixel[15]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[8]) > cb)
                                                if(*(cache_0 + pixel[7]) > cb)
                                                    if(*(cache_0 + pixel[1]) > cb)
                                                        goto success;
                                                    else if(*(cache_0 + pixel[1]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[10]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else if(*(cache_2+4) < c_b)
                                        continue;
                                    else
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[9]) > cb)
                                                if(*(cache_0 + pixel[10]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        if(*(cache_0 + pixel[7]) > cb)
                                                            goto success;
                                                        else if(*(cache_0 + pixel[7]) < c_b)
                                                            continue;
                                                        else
                                                            if(*(cache_0+-3) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                    else if(*(cache_0 + pixel[8]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0+-3) > cb)
                                                            if(*(cache_0 + pixel[1]) > cb)
                                                                if(*(cache_0 + pixel[13]) > cb)
                                                                    goto success;
                                                                else
                                                                    continue;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_0 + pixel[6]) < c_b)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_1+-6) > cb)
                                                continue;
                                            else if(*(cache_1+-6) < c_b)
                                                if(*(cache_0 + pixel[15]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[15]) > cb)
                                            if(*(cache_2+4) > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    goto success;
                                                else if(*(cache_0 + pixel[1]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        if(*(cache_1+-6) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else if(*(cache_2+4) < c_b)
                                                continue;
                                            else
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    if(*(cache_0+-3) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            if(*(cache_0 + pixel[10]) > cb)
                                                                if(*(cache_1+-6) > cb)
                                                                    goto success;
                                                                else
                                                                    continue;
                                                            else
                                                                continue;
                                                        else if(*(cache_0 + pixel[1]) < c_b)
                                                            continue;
                                                        else
                                                            if(*(cache_0 + pixel[8]) > cb)
                                                                if(*(cache_0 + pixel[10]) > cb)
                                                                    goto success;
                                                                else
                                                                    continue;
                                                            else
                                                                continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else if(*(cache_0 + pixel[3]) < c_b)
                                continue;
                            else
                                if(*(cache_0+-3) > cb)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[8]) > cb)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    goto success;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_2+4) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                            else if(*(cache_0 + pixel[8]) < c_b)
                                                if(*(cache_0 + pixel[7]) > cb || *(cache_0 + pixel[7]) < c_b)
                                                    continue;
                                                else
                                                    goto success;
                                            else
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[13]) > cb)
                                                        if(*(cache_0 + pixel[15]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else if(*(cache_2+4) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            if(*(cache_0 + pixel[13]) > cb)
                                                                if(*(cache_0 + pixel[15]) > cb)
                                                                    goto success;
                                                                else
                                                                    continue;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[0]) < c_b)
                            if(*(cache_0 + pixel[7]) > cb)
                                if(*(cache_0 + pixel[10]) > cb)
                                    goto success;
                                else
                                    continue;
                            else
                                continue;
                        else
                            if(*(cache_0 + pixel[7]) > cb)
                                if(*(cache_0 + pixel[10]) > cb)
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            if(*(cache_0 + pixel[8]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else if(*(cache_2+4) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_1+-6) > cb)
                                                        if(*(cache_0 + pixel[9]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[6]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[15]) > cb)
                                                if(*(cache_0+-3) > cb)
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else if(*(cache_0 + pixel[3]) < c_b)
                                        continue;
                                    else
                                        if(*(cache_0+-3) > cb)
                                            if(*(cache_0 + pixel[8]) > cb)
                                                if(*(cache_1+-6) > cb)
                                                    if(*(cache_0 + pixel[6]) > cb)
                                                        if(*(cache_0 + pixel[9]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else if(*(cache_0 + pixel[6]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[15]) > cb)
                                                            if(*(cache_0 + pixel[13]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_0 + pixel[10]) < c_b)
                                    continue;
                                else
                                    if(*(cache_0 + pixel[1]) > cb)
                                        if(*(cache_0 + pixel[9]) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[3]) > cb)
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else if(*(cache_0+3) < c_b)
                        if(*(cache_0+-3) > cb)
                            if(*(cache_0 + pixel[9]) > cb)
                                if(*(cache_1+-6) > cb)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                    else
                        if(*(cache_0+-3) > cb)
                            if(*(cache_1+-6) > cb)
                                if(*(cache_0 + pixel[7]) > cb)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[9]) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        goto success;
                                                    else if(*(cache_0 + pixel[8]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[0]) > cb)
                                                            if(*(cache_0 + pixel[1]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                else if(*(cache_0 + pixel[6]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            goto success;
                                                        else if(*(cache_0 + pixel[8]) < c_b)
                                                            continue;
                                                        else
                                                            if(*(cache_0 + pixel[0]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                    else
                                                        continue;
                                            else if(*(cache_0 + pixel[9]) < c_b)
                                                continue;
                                            else
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[3]) > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[7]) < c_b)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        if(*(cache_2+4) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[0]) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else if(*(cache_2+4) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            if(*(cache_0 + pixel[15]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else if(*(cache_0 + pixel[1]) < c_b)
                                                            continue;
                                                        else
                                                            if(*(cache_0 + pixel[8]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[3]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_0 + pixel[13]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else
                                continue;
                        else
                            continue;
                else if(*cache_2 < c_b)
                    if(*(cache_0+3) > cb)
                        if(*(cache_0 + pixel[7]) > cb)
                            if(*(cache_0 + pixel[1]) > cb)
                                if(*(cache_0 + pixel[9]) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            if(*(cache_0 + pixel[3]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_2+4) < c_b)
                                        continue;
                                    else
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[3]) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_0 + pixel[9]) < c_b)
                                    if(*(cache_0 + pixel[15]) > cb)
                                        if(*(cache_2+4) > cb)
                                            if(*(cache_0 + pixel[3]) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[0]) > cb)
                                        if(*(cache_0 + pixel[8]) > cb)
                                            if(*(cache_2+4) > cb)
                                                if(*(cache_0 + pixel[3]) > cb)
                                                    if(*(cache_0 + pixel[6]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[8]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[15]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else if(*(cache_0 + pixel[1]) < c_b)
                                if(*(cache_1+-6) > cb)
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0 + pixel[3]) < c_b)
                                        continue;
                                    else
                                        if(*(cache_0+-3) > cb)
                                            if(*(cache_0 + pixel[10]) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_1+-6) < c_b)
                                    if(*(cache_0 + pixel[9]) > cb)
                                        if(*(cache_0 + pixel[3]) > cb)
                                            if(*(cache_2+4) > cb)
                                                if(*(cache_0 + pixel[10]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else if(*(cache_2+4) < c_b)
                                                if(*(cache_0 + pixel[10]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[3]) < c_b)
                                            if(*(cache_0 + pixel[15]) < c_b)
                                                if(*(cache_0+-3) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_2+4) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[0]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        if(*(cache_2+4) < c_b)
                                            if(*(cache_0 + pixel[10]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[15]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[3]) < c_b)
                                                    if(*(cache_0 + pixel[15]) < c_b)
                                                        if(*(cache_0 + pixel[0]) < c_b)
                                                            if(*(cache_0+-3) < c_b)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                else
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[8]) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                if(*(cache_0 + pixel[10]) > cb)
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_2+4) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else if(*(cache_2+4) < c_b)
                                            continue;
                                        else
                                            if(*(cache_1+-6) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else if(*(cache_0 + pixel[3]) < c_b)
                                        continue;
                                    else
                                        if(*(cache_0+-3) > cb)
                                            if(*(cache_1+-6) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[7]) < c_b)
                            if(*(cache_1+-6) < c_b)
                                if(*(cache_0 + pixel[15]) > cb)
                                    continue;
                                else if(*(cache_0 + pixel[15]) < c_b)
                                    if(*(cache_0+-3) < c_b)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[13]) < c_b)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        continue;
                                                    else if(*(cache_0 + pixel[8]) < c_b)
                                                        goto success;
                                                    else
                                                        if(*(cache_0 + pixel[1]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    if(*(cache_2+4) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[3]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[6]) < c_b)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0+-3) < c_b)
                                                if(*(cache_0 + pixel[8]) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                        else
                            if(*(cache_0 + pixel[0]) < c_b)
                                if(*(cache_0 + pixel[10]) > cb)
                                    continue;
                                else if(*(cache_0 + pixel[10]) < c_b)
                                    if(*(cache_0 + pixel[9]) > cb)
                                        continue;
                                    else if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0+-3) < c_b)
                                            if(*(cache_0 + pixel[1]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[1]) < c_b)
                                                if(*(cache_1+-6) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        if(*(cache_0 + pixel[15]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[8]) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        if(*(cache_1+-6) < c_b)
                                                            if(*(cache_0 + pixel[15]) < c_b)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_2+4) < c_b)
                                            if(*(cache_0+-3) < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    if(*(cache_1+-6) < c_b)
                                                        if(*(cache_0 + pixel[13]) < c_b)
                                                            if(*(cache_0 + pixel[15]) < c_b)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    if(*(cache_0 + pixel[3]) < c_b)
                                        if(*(cache_1+-6) < c_b)
                                            if(*(cache_0+-3) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else if(*(cache_0+3) < c_b)
                        if(*(cache_0+-3) > cb)
                            if(*(cache_0 + pixel[13]) > cb)
                                goto success;
                            else
                                continue;
                        else if(*(cache_0+-3) < c_b)
                            if(*(cache_0 + pixel[9]) > cb)
                                if(*(cache_0 + pixel[13]) < c_b)
                                    goto success;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[9]) < c_b)
                                goto success;
                            else
                                if(*(cache_0 + pixel[6]) > cb || *(cache_0 + pixel[6]) < c_b)
                                    continue;
                                else
                                    if(*(cache_2+4) < c_b)
                                        goto success;
                                    else
                                        continue;
                        else
                            continue;
                    else
                        if(*(cache_1+-6) > cb)
                            if(*(cache_0 + pixel[13]) > cb)
                                if(*(cache_0 + pixel[9]) > cb)
                                    if(*(cache_0 + pixel[7]) > cb)
                                        if(*(cache_0+-3) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else if(*(cache_1+-6) < c_b)
                            if(*(cache_0 + pixel[3]) > cb)
                                if(*(cache_0 + pixel[8]) < c_b)
                                    if(*(cache_0 + pixel[15]) > cb)
                                        continue;
                                    else if(*(cache_0 + pixel[15]) < c_b)
                                        if(*(cache_0 + pixel[13]) < c_b)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[0]) < c_b)
                                                goto success;
                                            else
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0 + pixel[6]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[3]) < c_b)
                                if(*(cache_2+4) > cb)
                                    continue;
                                else if(*(cache_2+4) < c_b)
                                    if(*(cache_0 + pixel[0]) < c_b)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[1]) < c_b)
                                            if(*(cache_0 + pixel[15]) < c_b)
                                                if(*(cache_0+-3) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[8]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[1]) < c_b)
                                            if(*(cache_0+-3) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[8]) < c_b)
                                                if(*(cache_0 + pixel[0]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else
                                if(*(cache_0 + pixel[1]) > cb)
                                    continue;
                                else if(*(cache_0 + pixel[1]) < c_b)
                                    if(*(cache_0 + pixel[10]) < c_b)
                                        if(*(cache_0+-3) < c_b)
                                            if(*(cache_0 + pixel[9]) > cb)
                                                if(*(cache_2+4) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else if(*(cache_0 + pixel[9]) < c_b)
                                                if(*(cache_0 + pixel[15]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[15]) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    if(*(cache_0 + pixel[6]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                            else
                                                if(*(cache_2+4) < c_b)
                                                    if(*(cache_0 + pixel[15]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[7]) > cb)
                                        continue;
                                    else if(*(cache_0 + pixel[7]) < c_b)
                                        if(*(cache_0 + pixel[15]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[15]) < c_b)
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0+-3) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        if(*(cache_0 + pixel[13]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[6]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        if(*(cache_0 + pixel[0]) < c_b)
                                            if(*(cache_0 + pixel[8]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                        else
                            continue;
                else
                    if(*(cache_0 + pixel[7]) > cb)
                        if(*(cache_0 + pixel[3]) > cb)
                            if(*(cache_0 + pixel[10]) > cb)
                                if(*(cache_0+3) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            if(*(cache_0 + pixel[8]) > cb)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    goto success;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                            else if(*(cache_0 + pixel[8]) < c_b)
                                                continue;
                                            else
                                                if(*(cache_0 + pixel[15]) > cb)
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else if(*(cache_2+4) < c_b)
                                        if(*(cache_1+-6) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    if(*(cache_0 + pixel[9]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_0+3) < c_b)
                                    continue;
                                else
                                    if(*(cache_0+-3) > cb)
                                        if(*(cache_0 + pixel[13]) > cb)
                                            if(*(cache_1+-6) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        if(*(cache_0 + pixel[9]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else if(*(cache_0 + pixel[10]) < c_b)
                                if(*(cache_0 + pixel[15]) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            if(*(cache_0+3) > cb)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[15]) < c_b)
                                    continue;
                                else
                                    if(*(cache_0 + pixel[8]) > cb)
                                        if(*(cache_0 + pixel[0]) > cb)
                                            goto success;
                                        else if(*(cache_0 + pixel[0]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[9]) > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    if(*(cache_0 + pixel[6]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else
                                if(*(cache_0 + pixel[1]) > cb)
                                    if(*(cache_0 + pixel[9]) > cb)
                                        if(*(cache_0 + pixel[6]) > cb)
                                            if(*(cache_0+3) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        goto success;
                                                    else if(*(cache_0 + pixel[8]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[15]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[0]) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0 + pixel[0]) > cb)
                                            if(*(cache_0+3) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_2+4) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else if(*(cache_0 + pixel[15]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            if(*(cache_2+4) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[3]) < c_b)
                            if(*(cache_0 + pixel[13]) > cb)
                                if(*(cache_1+-6) > cb)
                                    if(*(cache_0 + pixel[9]) > cb)
                                        if(*(cache_0+-3) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[13]) < c_b)
                                continue;
                            else
                                if(*(cache_0+3) > cb)
                                    if(*(cache_0+-3) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else
                            if(*(cache_0+-3) > cb)
                                if(*(cache_0 + pixel[13]) > cb)
                                    if(*(cache_1+-6) > cb)
                                        if(*(cache_0 + pixel[9]) > cb)
                                            if(*(cache_0 + pixel[6]) > cb)
                                                if(*(cache_0 + pixel[10]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[13]) < c_b)
                                    if(*(cache_0 + pixel[0]) > cb)
                                        goto success;
                                    else
                                        continue;
                                else
                                    if(*(cache_0+3) > cb)
                                        if(*(cache_0 + pixel[9]) > cb)
                                            if(*(cache_1+-6) > cb)
                                                if(*(cache_0 + pixel[6]) > cb)
                                                    if(*(cache_0 + pixel[10]) > cb)
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        continue;
            else if(*cache_1 < c_b)
                if(*(cache_0 + pixel[15]) > cb)
                    if(*(cache_1+-6) > cb)
                        if(*(cache_2+4) > cb)
                            if(*(cache_0+-3) > cb)
                                if(*(cache_0 + pixel[10]) > cb)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*cache_2 > cb)
                                                goto success;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[1]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[7]) > cb)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[10]) < c_b)
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_0 + pixel[13]) > cb)
                                            if(*cache_2 > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*cache_2 > cb)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    if(*(cache_0 + pixel[13]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                        else if(*(cache_2+4) < c_b)
                            if(*(cache_0 + pixel[7]) > cb)
                                if(*(cache_0+-3) > cb)
                                    if(*cache_2 > cb)
                                        if(*(cache_0 + pixel[13]) > cb)
                                            if(*(cache_0 + pixel[9]) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[7]) < c_b)
                                if(*(cache_0 + pixel[9]) > cb)
                                    if(*(cache_0 + pixel[1]) > cb)
                                        if(*(cache_0+-3) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[9]) < c_b)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        continue;
                                    else if(*(cache_0 + pixel[10]) < c_b)
                                        if(*(cache_0 + pixel[3]) < c_b)
                                            if(*(cache_0+3) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0 + pixel[1]) < c_b)
                                            if(*(cache_0 + pixel[3]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    if(*(cache_0 + pixel[0]) < c_b)
                                        goto success;
                                    else
                                        continue;
                            else
                                if(*(cache_0 + pixel[0]) > cb)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[9]) > cb)
                                            if(*cache_2 > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    if(*(cache_0 + pixel[10]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else if(*(cache_0 + pixel[1]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        if(*(cache_0+-3) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else
                            if(*(cache_0 + pixel[9]) > cb)
                                if(*(cache_0+-3) > cb)
                                    if(*(cache_0 + pixel[1]) > cb)
                                        if(*cache_2 > cb)
                                            if(*(cache_0 + pixel[10]) > cb)
                                                if(*(cache_0 + pixel[13]) > cb)
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0 + pixel[1]) < c_b)
                                        continue;
                                    else
                                        if(*(cache_0 + pixel[7]) > cb)
                                            if(*(cache_0 + pixel[10]) > cb)
                                                if(*(cache_0 + pixel[13]) > cb)
                                                    if(*cache_2 > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[7]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[0]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    if(*(cache_0 + pixel[6]) < c_b)
                                                        if(*(cache_0 + pixel[10]) > cb)
                                                            if(*(cache_0 + pixel[13]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                else
                                    continue;
                            else
                                continue;
                    else if(*(cache_1+-6) < c_b)
                        if(*(cache_0 + pixel[3]) > cb)
                            if(*(cache_0 + pixel[13]) > cb)
                                if(*(cache_0+-3) > cb)
                                    if(*(cache_0+3) > cb)
                                        goto success;
                                    else
                                        continue;
                                else if(*(cache_0+-3) < c_b)
                                    if(*(cache_0+3) < c_b)
                                        if(*(cache_0 + pixel[6]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[13]) < c_b)
                                if(*(cache_0 + pixel[7]) < c_b)
                                    if(*(cache_0 + pixel[6]) < c_b)
                                        if(*(cache_0 + pixel[8]) < c_b)
                                            if(*(cache_0+-3) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                if(*(cache_0+3) < c_b)
                                    if(*(cache_0+-3) < c_b)
                                        if(*(cache_0 + pixel[7]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[3]) < c_b)
                            if(*(cache_0 + pixel[8]) < c_b)
                                if(*(cache_0 + pixel[9]) < c_b)
                                    if(*(cache_0 + pixel[7]) < c_b)
                                        if(*(cache_0+3) > cb)
                                            continue;
                                        else if(*(cache_0+3) < c_b)
                                            if(*(cache_0 + pixel[10]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[6]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                        else
                                            if(*(cache_0 + pixel[13]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            if(*(cache_0+-3) < c_b)
                                if(*(cache_0+3) > cb)
                                    continue;
                                else if(*(cache_0+3) < c_b)
                                    if(*(cache_0 + pixel[6]) < c_b)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[9]) < c_b)
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[13]) < c_b)
                                        if(*(cache_0 + pixel[7]) < c_b)
                                            if(*(cache_0 + pixel[6]) < c_b)
                                                if(*(cache_0 + pixel[10]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        if(*(cache_0 + pixel[9]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        if(*(cache_2+4) > cb)
                            if(*(cache_0+3) > cb)
                                if(*(cache_0+-3) > cb)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_0 + pixel[3]) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else if(*(cache_2+4) < c_b)
                            if(*(cache_0 + pixel[10]) > cb)
                                continue;
                            else if(*(cache_0 + pixel[10]) < c_b)
                                if(*(cache_0+3) < c_b)
                                    if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[3]) < c_b)
                                            if(*(cache_0 + pixel[7]) < c_b)
                                                if(*(cache_0 + pixel[6]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                if(*(cache_0 + pixel[1]) < c_b)
                                    if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[3]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else
                            continue;
                else if(*(cache_0 + pixel[15]) < c_b)
                    if(*(cache_0+3) > cb)
                        if(*(cache_0+-3) < c_b)
                            if(*(cache_1+-6) < c_b)
                                if(*(cache_0 + pixel[13]) < c_b)
                                    if(*(cache_0 + pixel[7]) > cb)
                                        continue;
                                    else if(*(cache_0 + pixel[7]) < c_b)
                                        goto success;
                                    else
                                        if(*(cache_0 + pixel[8]) < c_b)
                                            if(*(cache_0 + pixel[0]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                    else if(*(cache_0+3) < c_b)
                        if(*(cache_0 + pixel[6]) > cb)
                            if(*(cache_0 + pixel[13]) > cb)
                                if(*cache_2 > cb)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[13]) < c_b)
                                if(*(cache_0 + pixel[0]) < c_b)
                                    if(*(cache_2+4) < c_b)
                                        if(*cache_2 < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else if(*(cache_0 + pixel[6]) < c_b)
                            if(*(cache_0 + pixel[3]) > cb)
                                if(*(cache_0+-3) < c_b)
                                    if(*(cache_0 + pixel[1]) < c_b)
                                        continue;
                                    else
                                        goto success;
                                else
                                    continue;
                            else if(*(cache_0 + pixel[3]) < c_b)
                                if(*(cache_0 + pixel[7]) > cb)
                                    if(*cache_2 < c_b)
                                        goto success;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[7]) < c_b)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else if(*(cache_2+4) < c_b)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[1]) < c_b)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[0]) < c_b)
                                                goto success;
                                            else
                                                if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[8]) < c_b)
                                                    if(*(cache_0 + pixel[9]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        if(*(cache_1+-6) < c_b)
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[8]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    if(*cache_2 < c_b)
                                        if(*(cache_2+4) < c_b)
                                            if(*(cache_0 + pixel[0]) < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                if(*(cache_0+-3) < c_b)
                                    if(*(cache_1+-6) < c_b)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[8]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[8]) < c_b)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[7]) > cb)
                                                        continue;
                                                    else if(*(cache_0 + pixel[7]) < c_b)
                                                        goto success;
                                                    else
                                                        if(*(cache_0 + pixel[13]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    if(*(cache_2+4) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                            else
                                                if(*(cache_0 + pixel[13]) < c_b)
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        if(*(cache_0 + pixel[7]) > cb || *(cache_0 + pixel[7]) < c_b)
                                                            continue;
                                                        else
                                                            goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else
                            if(*(cache_0 + pixel[13]) < c_b)
                                if(*(cache_2+4) > cb)
                                    continue;
                                else if(*(cache_2+4) < c_b)
                                    if(*cache_2 < c_b)
                                        if(*(cache_0 + pixel[3]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[3]) < c_b)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[0]) < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    if(*(cache_1+-6) < c_b)
                                                        if(*(cache_0 + pixel[8]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            if(*(cache_0+-3) < c_b)
                                                if(*(cache_0 + pixel[10]) < c_b)
                                                    if(*(cache_0 + pixel[1]) > cb)
                                                        continue;
                                                    else if(*(cache_0 + pixel[1]) < c_b)
                                                        goto success;
                                                    else
                                                        if(*(cache_0 + pixel[7]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_1+-6) < c_b)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[0]) < c_b)
                                                if(*cache_2 < c_b)
                                                    if(*(cache_0 + pixel[10]) < c_b)
                                                        if(*(cache_0+-3) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        if(*(cache_0 + pixel[1]) > cb || *(cache_0 + pixel[1]) < c_b)
                                                            continue;
                                                        else
                                                            goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        if(*(cache_0+-3) < c_b)
                            if(*(cache_0 + pixel[13]) < c_b)
                                if(*(cache_1+-6) < c_b)
                                    if(*(cache_0 + pixel[9]) > cb)
                                        if(*(cache_0 + pixel[3]) < c_b)
                                            if(*(cache_2+4) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0 + pixel[9]) < c_b)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[7]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[7]) < c_b)
                                                if(*cache_2 > cb || *cache_2 < c_b)
                                                    goto success;
                                                else
                                                    if(*(cache_0 + pixel[6]) < c_b)
                                                        if(*(cache_0 + pixel[8]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[1]) < c_b)
                                                    if(*cache_2 < c_b)
                                                        if(*(cache_0 + pixel[0]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        if(*(cache_0 + pixel[8]) < c_b)
                                                            if(*cache_2 < c_b)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                        else
                                            if(*(cache_0 + pixel[3]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                    else
                                        if(*(cache_2+4) < c_b)
                                            if(*(cache_0 + pixel[1]) < c_b)
                                                if(*(cache_0 + pixel[10]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[10]) < c_b)
                                                    if(*cache_2 < c_b)
                                                        if(*(cache_0 + pixel[0]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    if(*(cache_0 + pixel[3]) < c_b)
                                                        if(*(cache_0 + pixel[0]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                else
                    if(*(cache_0 + pixel[8]) > cb)
                        if(*(cache_0 + pixel[6]) > cb)
                            if(*cache_2 > cb)
                                if(*(cache_1+-6) > cb)
                                    if(*(cache_0 + pixel[10]) > cb)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                    else if(*(cache_0 + pixel[8]) < c_b)
                        if(*(cache_0 + pixel[3]) > cb)
                            if(*(cache_0 + pixel[13]) > cb)
                                continue;
                            else if(*(cache_0 + pixel[13]) < c_b)
                                if(*(cache_0+-3) < c_b)
                                    if(*(cache_0 + pixel[7]) < c_b)
                                        if(*(cache_1+-6) < c_b)
                                            if(*(cache_0 + pixel[6]) < c_b)
                                                if(*(cache_0 + pixel[10]) < c_b)
                                                    if(*(cache_0 + pixel[9]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                if(*(cache_0+3) < c_b)
                                    if(*(cache_0+-3) < c_b)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[3]) < c_b)
                            if(*(cache_2+4) > cb)
                                if(*(cache_1+-6) < c_b)
                                    if(*(cache_0 + pixel[7]) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                            else if(*(cache_2+4) < c_b)
                                if(*(cache_0 + pixel[6]) < c_b)
                                    if(*(cache_0+3) > cb)
                                        continue;
                                    else if(*(cache_0+3) < c_b)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                continue;
                                            else if(*(cache_0 + pixel[0]) < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[1]) < c_b)
                                                        if(*(cache_0 + pixel[7]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[7]) < c_b)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    goto success;
                                                else
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0 + pixel[1]) < c_b)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[7]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        if(*(cache_0 + pixel[7]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                    else
                                        if(*(cache_0+-3) < c_b)
                                            if(*(cache_0 + pixel[13]) < c_b)
                                                if(*(cache_1+-6) < c_b)
                                                    if(*(cache_0 + pixel[7]) < c_b)
                                                        if(*(cache_0 + pixel[10]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                            else
                                if(*(cache_1+-6) < c_b)
                                    if(*(cache_0+3) > cb)
                                        continue;
                                    else if(*(cache_0+3) < c_b)
                                        if(*(cache_0 + pixel[6]) < c_b)
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    if(*(cache_0 + pixel[9]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0+-3) < c_b)
                                            if(*(cache_0 + pixel[13]) < c_b)
                                                if(*(cache_0 + pixel[6]) < c_b)
                                                    if(*(cache_0 + pixel[7]) < c_b)
                                                        if(*(cache_0 + pixel[10]) < c_b)
                                                            if(*(cache_0 + pixel[9]) < c_b)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    continue;
                        else
                            if(*(cache_0+-3) < c_b)
                                if(*(cache_0 + pixel[13]) > cb)
                                    if(*(cache_0+3) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[13]) < c_b)
                                    if(*(cache_1+-6) < c_b)
                                        if(*(cache_0 + pixel[7]) < c_b)
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[6]) < c_b)
                                                    if(*(cache_0 + pixel[9]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0+3) < c_b)
                                        if(*(cache_0 + pixel[10]) < c_b)
                                            if(*(cache_0 + pixel[6]) < c_b)
                                                if(*(cache_1+-6) < c_b)
                                                    if(*(cache_0 + pixel[7]) < c_b)
                                                        if(*(cache_0 + pixel[9]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        continue;
            else
                if(*(cache_0+-3) > cb)
                    if(*cache_2 > cb)
                        if(*(cache_0 + pixel[7]) > cb)
                            if(*(cache_1+-6) > cb)
                                if(*(cache_0 + pixel[6]) > cb)
                                    if(*(cache_0 + pixel[13]) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[9]) > cb)
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    goto success;
                                                else if(*(cache_0 + pixel[8]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                            else if(*(cache_0 + pixel[9]) < c_b)
                                                continue;
                                            else
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[3]) > cb)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[6]) < c_b)
                                    continue;
                                else
                                    if(*(cache_0 + pixel[15]) > cb)
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_0 + pixel[9]) > cb)
                                                    if(*(cache_0 + pixel[8]) > cb)
                                                        goto success;
                                                    else if(*(cache_0 + pixel[8]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                else if(*(cache_0 + pixel[9]) < c_b)
                                                    continue;
                                                else
                                                    if(*(cache_2+4) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            if(*(cache_0 + pixel[0]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                            else
                                                continue;
                                        else if(*(cache_0 + pixel[10]) < c_b)
                                            continue;
                                        else
                                            if(*(cache_0 + pixel[3]) > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    if(*(cache_2+4) > cb)
                                                        if(*(cache_0 + pixel[13]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else if(*(cache_1+-6) < c_b)
                                continue;
                            else
                                if(*(cache_0+3) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                if(*(cache_0 + pixel[3]) > cb)
                                                    if(*(cache_0 + pixel[13]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[7]) < c_b)
                            if(*(cache_2+4) > cb)
                                if(*(cache_1+-6) > cb)
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_0 + pixel[15]) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_0 + pixel[1]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0 + pixel[3]) < c_b)
                                        continue;
                                    else
                                        if(*(cache_0 + pixel[10]) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    if(*(cache_0 + pixel[1]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                else if(*(cache_1+-6) < c_b)
                                    if(*(cache_0+3) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_0 + pixel[0]) > cb)
                                                if(*(cache_0 + pixel[3]) > cb)
                                                    goto success;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0+3) > cb)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_0 + pixel[3]) > cb)
                                                    if(*(cache_0 + pixel[0]) > cb)
                                                        if(*(cache_0 + pixel[15]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else if(*(cache_2+4) < c_b)
                                continue;
                            else
                                if(*(cache_0 + pixel[9]) > cb)
                                    if(*(cache_0 + pixel[0]) > cb)
                                        if(*(cache_1+-6) > cb)
                                            goto success;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                        else
                            if(*(cache_0 + pixel[0]) > cb)
                                if(*(cache_0 + pixel[10]) > cb)
                                    if(*(cache_2+4) > cb)
                                        if(*(cache_0 + pixel[13]) > cb)
                                            if(*(cache_1+-6) > cb)
                                                if(*(cache_0 + pixel[15]) > cb)
                                                    if(*(cache_0 + pixel[1]) > cb)
                                                        goto success;
                                                    else if(*(cache_0 + pixel[1]) < c_b)
                                                        continue;
                                                    else
                                                        if(*(cache_0 + pixel[8]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                else
                                                    continue;
                                            else if(*(cache_1+-6) < c_b)
                                                continue;
                                            else
                                                if(*(cache_0+3) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                    else if(*(cache_2+4) < c_b)
                                        if(*(cache_0 + pixel[1]) > cb)
                                            if(*(cache_0 + pixel[3]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0 + pixel[9]) > cb)
                                            if(*(cache_0 + pixel[1]) > cb)
                                                if(*(cache_0 + pixel[13]) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_1+-6) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else if(*(cache_0 + pixel[1]) < c_b)
                                                continue;
                                            else
                                                if(*(cache_0 + pixel[8]) > cb)
                                                    if(*(cache_1+-6) > cb)
                                                        if(*(cache_0 + pixel[13]) > cb)
                                                            if(*(cache_0 + pixel[15]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                        else
                                            continue;
                                else if(*(cache_0 + pixel[10]) < c_b)
                                    if(*(cache_0+3) > cb)
                                        if(*(cache_0 + pixel[13]) > cb)
                                            if(*(cache_2+4) > cb)
                                                if(*(cache_0 + pixel[3]) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else if(*(cache_0+3) < c_b)
                                        continue;
                                    else
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[3]) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    if(*(cache_0 + pixel[3]) > cb)
                                        if(*(cache_1+-6) > cb)
                                            if(*(cache_0 + pixel[13]) > cb)
                                                if(*(cache_2+4) > cb)
                                                    if(*(cache_0 + pixel[15]) > cb)
                                                        if(*(cache_0 + pixel[1]) > cb)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else if(*(cache_1+-6) < c_b)
                                            if(*(cache_0+3) > cb)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0+3) > cb)
                                                if(*(cache_0 + pixel[13]) > cb)
                                                    if(*(cache_0 + pixel[1]) > cb)
                                                        if(*(cache_2+4) > cb)
                                                            if(*(cache_0 + pixel[15]) > cb)
                                                                goto success;
                                                            else
                                                                continue;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        continue;
                else if(*(cache_0+-3) < c_b)
                    if(*(cache_0 + pixel[15]) > cb)
                        if(*cache_2 < c_b)
                            if(*(cache_0 + pixel[6]) < c_b)
                                if(*(cache_0 + pixel[10]) < c_b)
                                    if(*(cache_0 + pixel[7]) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                    else if(*(cache_0 + pixel[15]) < c_b)
                        if(*(cache_0 + pixel[10]) > cb)
                            if(*(cache_0+3) > cb)
                                continue;
                            else if(*(cache_0+3) < c_b)
                                if(*(cache_0 + pixel[3]) < c_b)
                                    if(*(cache_0 + pixel[13]) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                if(*(cache_1+-6) < c_b)
                                    if(*(cache_0 + pixel[3]) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else
                                    continue;
                        else if(*(cache_0 + pixel[10]) < c_b)
                            if(*cache_2 < c_b)
                                if(*(cache_0 + pixel[9]) > cb)
                                    if(*(cache_2+4) < c_b)
                                        goto success;
                                    else
                                        continue;
                                else if(*(cache_0 + pixel[9]) < c_b)
                                    if(*(cache_1+-6) > cb)
                                        continue;
                                    else if(*(cache_1+-6) < c_b)
                                        if(*(cache_0 + pixel[13]) < c_b)
                                            if(*(cache_0 + pixel[1]) > cb)
                                                if(*(cache_0 + pixel[7]) < c_b)
                                                    goto success;
                                                else
                                                    continue;
                                            else if(*(cache_0 + pixel[1]) < c_b)
                                                if(*(cache_0 + pixel[0]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[0]) < c_b)
                                                    goto success;
                                                else
                                                    if(*(cache_0 + pixel[7]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                            else
                                                if(*(cache_0 + pixel[7]) > cb)
                                                    continue;
                                                else if(*(cache_0 + pixel[7]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        if(*(cache_0 + pixel[8]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                        else
                                            continue;
                                    else
                                        if(*(cache_0+3) < c_b)
                                            if(*(cache_0 + pixel[3]) < c_b)
                                                goto success;
                                            else
                                                continue;
                                        else
                                            continue;
                                else
                                    if(*(cache_2+4) < c_b)
                                        if(*(cache_1+-6) > cb)
                                            continue;
                                        else if(*(cache_1+-6) < c_b)
                                            if(*(cache_0 + pixel[13]) < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            if(*(cache_0+3) < c_b)
                                                if(*(cache_0 + pixel[3]) < c_b)
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                    else
                                        continue;
                            else
                                continue;
                        else
                            if(*(cache_0 + pixel[3]) < c_b)
                                if(*(cache_1+-6) > cb)
                                    continue;
                                else if(*(cache_1+-6) < c_b)
                                    if(*(cache_2+4) < c_b)
                                        if(*(cache_0 + pixel[13]) < c_b)
                                            if(*cache_2 < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    if(*(cache_0 + pixel[0]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    if(*(cache_0+3) < c_b)
                                        if(*(cache_2+4) < c_b)
                                            if(*cache_2 < c_b)
                                                if(*(cache_0 + pixel[1]) < c_b)
                                                    if(*(cache_0 + pixel[13]) < c_b)
                                                        if(*(cache_0 + pixel[0]) < c_b)
                                                            goto success;
                                                        else
                                                            continue;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                            else
                                continue;
                    else
                        if(*(cache_0 + pixel[6]) < c_b)
                            if(*cache_2 < c_b)
                                if(*(cache_0 + pixel[7]) < c_b)
                                    if(*(cache_1+-6) < c_b)
                                        if(*(cache_0 + pixel[13]) < c_b)
                                            if(*(cache_0 + pixel[10]) < c_b)
                                                if(*(cache_0 + pixel[9]) < c_b)
                                                    if(*(cache_0 + pixel[8]) < c_b)
                                                        goto success;
                                                    else
                                                        continue;
                                                else
                                                    continue;
                                            else
                                                continue;
                                        else
                                            continue;
                                    else
                                        continue;
                                else
                                    continue;
                            else
                                continue;
                        else
                            continue;
                else
                    continue;
			success:
				if(total >= rsize)	
				{	
					rsize *=2;
					ret=(xy*)realloc(ret, rsize*sizeof(xy));
				}
				/*output->images_table[0][y*xsize+cache_0-line_min]=1.;*/
				output->images_table[0][y*xsize+cache_0-line_min]=corner_score((const byte *)(input->images_table[0]) + (int)(y*xsize+cache_0-line_min), pixel, barrier);
				/*printf("%d %d\n",cache_0-line_min,y);
				printf("score=%d\n",corner_score((const byte *)(input->images_table[0]) + (int)(y*xsize+cache_0-line_min), pixel, barrier));*/
				ret[total].x = cache_0-line_min;
				ret[total++].y = y;
		}
	}
	
	printf("nbre de points = %d - %d\n",total,cpt);
	
	/*int *number_of_nonmax=(int *)malloc(sizeof(int));
	xy *points=fast_nonmax(output->images_table[0],xsize,ysize,ret,total,barrier,number_of_nonmax);
	
	for(i=0;i<(*number_of_nonmax);i++) {
		output->images_table[0][points[i].y*xsize+points[i].x]=1.;
	}*/
	
	nonmax(output->images_table[0],xsize,ysize,rayon);
	
	def_groupe[gpe].ext=output; /* sauvegarde de l'image*/
}

void nonmax(byte* im, int xsize, int ysize, int rayon) {
	
	/* Recherche des maximums locaux*/
	int isMaximumLocal;
	int i,j,k,l,val_1,val_2;
	
	
	int cpt=0,cpt_2=0;
	for (i=0+rayon;i<xsize-rayon;i++) {
		for (j=0+rayon;j<ysize-rayon;j++) {
			
			val_1=im[i+j*xsize];
			if ( val_1 > 0 ) {
				cpt++;
				isMaximumLocal=1;
				for(k=i-rayon;k<i+rayon+1 && isMaximumLocal==1 ;k++) {
					for(l=j-rayon;l<j+rayon+1;l++) {
						if ( k!=i || l!=j ) {
							val_2=im[k+l*xsize];
							if ( val_1 <= val_2) {
								isMaximumLocal=0;
								break;
							} else {
								im[k+l*xsize]=0;
							}
						}
					}
				}
                
				/* Maximum local */
				if ( isMaximumLocal == 0 ) {
					cpt_2++;
					im[i+j*xsize]=0;
					/*printf("%d %d\n",position,im[position]);*/
				} else {
					/*printf("%d %d\n",position,im[position]);*/
				}
			}
		}
	}
	
	printf("points en moins = %d\n",cpt_2);
}



/*void fast_nonmax(const BasicImage<byte>& im, const vector<ImageRef>& corners, int barrier, vector<ReturnType>& nonmax_corners)*/
xy*  fast_nonmax(const byte* im, int xsize, int ysize, xy* corners, int numcorners, int barrier, int* numnx)
{
  
	/*Create a list of integer pointer offstes, corresponding to the */
	/*direction offsets in dir[]*/
	int	pointer_dir[16];
	int* row_start = (int*) malloc(ysize * sizeof(int));
	int* scores    = (int*) malloc(numcorners * sizeof(int));
	xy*  nonmax_corners=(xy*)malloc(numcorners* sizeof(xy));
	int num_nonmax=0;
	int prev_row = -1;
	int i, j;
	int point_above = 0;
	int point_below = 0;


	pointer_dir[0] = 0 + 3 * xsize;		
	pointer_dir[1] = 1 + 3 * xsize;		
	pointer_dir[2] = 2 + 2 * xsize;		
	pointer_dir[3] = 3 + 1 * xsize;		
	pointer_dir[4] = 3 + 0 * xsize;		
	pointer_dir[5] = 3 + -1 * xsize;		
	pointer_dir[6] = 2 + -2 * xsize;		
	pointer_dir[7] = 1 + -3 * xsize;		
	pointer_dir[8] = 0 + -3 * xsize;		
	pointer_dir[9] = -1 + -3 * xsize;		
	pointer_dir[10] = -2 + -2 * xsize;		
	pointer_dir[11] = -3 + -1 * xsize;		
	pointer_dir[12] = -3 + 0 * xsize;		
	pointer_dir[13] = -3 + 1 * xsize;		
	pointer_dir[14] = -2 + 2 * xsize;		
	pointer_dir[15] = -1 + 3 * xsize;		

	if(numcorners < 5)
	{
		free(row_start);
		free(scores);
		free(nonmax_corners);
		return 0;
	}

	/*xsize ysize numcorners corners*/

	/*Compute the score for each detected corner, and find where each row begins*/
	/* (the corners are output in raster scan order). A beginning of -1 signifies*/
	/* that there are no corners on that row.*/

  
	for(i=0; i <ysize; i++)
		row_start[i] = -1;
	  
	  
	for(i=0; i< numcorners; i++)
	{
		if(corners[i].y != prev_row)
		{
			row_start[corners[i].y] = i;
			prev_row = corners[i].y;
		}
		  
		scores[i] = corner_score(im + corners[i].x + corners[i].y * xsize, pointer_dir, barrier);
	}
  
  
	/*Point above points (roughly) to the pixel above the one of interest, if there*/
	/*is a feature there.*/
  
  
	for(i=1; i < numcorners-1; i++)
	{
		int score = scores[i];
		xy pos = corners[i];

		/*Check left*/
		/*if(corners[i-1] == pos-ImageRef(1,0) && scores[i-1] > score)*/
		if(corners[i-1].x == pos.x-1 && corners[i-1].y == pos.y && scores[i-1] > score)
			continue;

		/*Check right*/
		/*if(corners[i+1] == pos+ImageRef(1,0) && scores[i+1] > score)*/
		if(corners[i+1].x == pos.x+1 && corners[i+1].y == pos.y && scores[i-1] > score)
			continue;

		/*Check above*/
		if(pos.y != 0 && row_start[pos.y - 1] != -1) 
		{
			if(corners[point_above].y < pos.y - 1)
				point_above = row_start[pos.y-1];

			/*Make point above point to the first of the pixels above the current point,*/
			/*if it exists.*/
			for(; corners[point_above].y < pos.y && corners[point_above].x < pos.x - 1; point_above++);


			for(j=point_above; corners[j].y < pos.y && corners[j].x <= pos.x + 1; j++)
			{
				int x = corners[j].x;
				if( (x == pos.x - 1 || x ==pos.x || x == pos.x+1) && scores[j] > score)
				{
					goto cont;
				}
			}

		}

		/*Check below*/
		if(pos.y != ysize-1 && row_start[pos.y + 1] != -1) /*Nothing below*/
		{
			if(corners[point_below].y < pos.y + 1)
				point_below = row_start[pos.y+1];

			/* Make point below point to one of the pixels belowthe current point, if it*/
			/* exists.*/
			for(; corners[point_below].y == pos.y+1 && corners[point_below].x < pos.x - 1; point_below++);

			for(j=point_below; corners[j].y == pos.y+1 && corners[j].x <= pos.x + 1; j++)
			{
				int x = corners[j].x;
				if( (x == pos.x - 1 || x ==pos.x || x == pos.x+1) && scores[j] > score)
				{
					goto cont;
				}
			}
		}
		

		nonmax_corners[num_nonmax].x = corners[i].x;
		nonmax_corners[num_nonmax].y = corners[i].y;

		num_nonmax++;

		cont:
				;
	}

	*numnx = num_nonmax;
	
	free(row_start);
	free(scores);
	return nonmax_corners;
}

int corner_score(const byte*  imp, const int *pointer_dir, int barrier)
{
	/*The score for a positive feature is sum of the difference between the pixels
	and the barrier if the difference is positive. Negative is similar.
	The score is the max of those two.
	  
	B = {x | x = points on the Bresenham circle around c}
	Sp = { I(x) - t | x E B , I(x) - t > 0 }
	Sn = { t - I(x) | x E B, t - I(x) > 0}
	  
	Score = max sum(Sp), sum(Sn)*/

	int cb = *imp + barrier;
	int c_b = *imp - barrier;
	int sp=0, sn = 0;

	int i=0;

	for(i=0; i<16; i++)
	{
		int p = imp[pointer_dir[i]];

		if(p > cb)
			sp += p-cb;
		else if(p < c_b)
			sn += c_b-p;
	}
	
	if(sp > sn)
		return sp;
	else 
		return sn;
}

