/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_compet_ptc.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A. Karaouzene
- description: specific file creation
- date: 23/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Competition sur un voisinage de taille "ecart" specifie dans -Eecart.

Options :

- -Eecart

Entree Sortie:
- En entree le groupe n'accepte que des images de flottants.
- Le groupe retourne en sortie une image en uchar.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/convert.h>
#include "tools/include/macro.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include "tools/include/competition.h"
#include <public_tools/Vision.h>
#include "Vision/Contours.h"
#include "Vision/Image_Processing.h"
#include "tools/include/process_dog.h"

typedef struct data_compet_ptc
{
   int mode_pondere;
   int InputGpe;
   int ecart;
   int exclusion;
   unsigned int nx;
   unsigned int ny;
   unsigned int nb_band;
   int take_previous;
} Data_compet_ptc;

void competition_ptc(int exclusion,int ecart, int xmax, int ymax, float *input, int *im_tmp)
{
   /*im_tmp : resultat de la competition*/
   /*valeur max =1 rapport = 256.*/
   int p,p1,p2,i,j,qi,j2;
   /*int nbr;*/
   float v,v1,rapport;
   int p3;
   rapport = 255.;
   /*nbr = xmax * ymax;*/
   for (p2 = (exclusion - 1); p2 < ymax - (exclusion - 1); p2++)
   {
      p3=p2*xmax;
      for(p1 = (exclusion - 1); p1 < xmax - (exclusion - 1); p1++)
      {
         p=p1+p3;
         v1=input[p];
         if(v1>0. && im_tmp[p]>=0)
         {
            for (j = 1 - ecart/2; j < ecart/2; j++)
            {
               j2=p+xmax * j;
               for (i = 1 - ecart/2; i < ecart/2; i++)
               {
                  qi = j2+ i ;
                  /*si i == j == 0*/
                  if ( (i | j)/* && im_tmp[qi] !=-1 */ ) /* 1) AC COMM : permet r��action en chaine de haut en bas */
                  {
                     if (input[qi] >= v1)
                     {
                        im_tmp[p] = 0;
                        goto finie2;
                     }
                     else
                     {
                        im_tmp[qi]=-1; /* un point dans le voisinage plus faible on le met a -1 pour accelerer la suite des calculs */
                     }
                  }
               }
            }
            v = v1 * rapport;
            im_tmp[p] = (int) v; /* (int) floor(v+0.5); */
         }
         else im_tmp[p] = 0;  /* 1) NE PAS COMM CETTE LIGNE : permet r��action en chaine de bas en haut */
         finie2:;
      }
   }

}


void new_compet_ptc(int Gpe)
{
   Data_compet_ptc *my_data=NULL;
   int mode_pondere, InputGpe=-1, ecart=-1, exclusion = 0;
   int i, l,take_previous = 0;
   char *string = NULL;
   char param_link[256];

   dprints(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);


   /**
    * On recherche les liens et les Gpes des boites entrantes
    */
   i = 0;
   l = find_input_link(Gpe, i);
   while (l != -1)
   {
      string = liaison[l].nom;
      if (strstr(string,"sync") != NULL )
      {printf("\nlien sync "); }
      else
      {
         InputGpe = liaison[l].depart;
         if (prom_getopt(string, "-p", param_link) == 1 || prom_getopt(string, "-P", param_link) == 1)
            mode_pondere = 1;
         if (prom_getopt(string, "-e", param_link) >= 2 || prom_getopt(string, "-E", param_link) >= 2)
            ecart = atoi(param_link);
         if (prom_getopt(string, "-re", param_link) >= 2 || prom_getopt(string, "-RE", param_link) >= 2)
            exclusion = atoi(param_link);
         /*si aucune exclusion definie recuperer l'exclusion de la conv_dog en amont si elle existe*/
         if (prom_getopt(string, "-re", param_link) == 0 || prom_getopt(string, "-RE", param_link) == 0)
            if( strcmp(def_groupe[InputGpe].nom,"f_conv_dog") == 0)
            {
               PRINT_WARNING("Le rayon exclusion sur la DOG doit etre supeieur a la moitiee du parametre E\n");
               take_previous = 1;
            }
      }

      i++;
      l = find_input_link(Gpe, i);
   }

   if (InputGpe == -1)
      EXIT_ON_ERROR("Missing input link\n");

   my_data = (Data_compet_ptc *) malloc(sizeof(Data_compet_ptc));
   if (my_data == NULL)    EXIT_ON_ERROR("\nerror in memory allocation");
   if (InputGpe == -1)     EXIT_ON_ERROR("\nthere is no group at the input of %s",def_groupe[Gpe].no_name);
   if (ecart == -1)
   {
      PRINT_WARNING("\nVoisinage de competition par defaut utilise dans %s",def_groupe[Gpe].no_name);
      ecart = 15;
   }
   my_data->InputGpe = InputGpe;
   my_data->mode_pondere = mode_pondere;
   my_data->ecart = ecart;
   my_data->exclusion = exclusion;
   my_data->take_previous = take_previous;

   def_groupe[Gpe].data = my_data;


   dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);
}


void function_compet_ptc(int Gpe)
{
   Data_compet_ptc  *mydata   = NULL;
   /*char    *string   = NULL;*/
   prom_images_struct *ext_input = NULL;
   prom_images_struct *ext_output = NULL;
   float *input = NULL;
   int *im_tmp = NULL;
   unsigned char *output = NULL;
   int n = 0, nx = 0, ny = 0, i;

   mydata = (Data_compet_ptc *) def_groupe[Gpe].data;

   if (def_groupe[Gpe].ext == NULL)
   {
      dprints("====debut %s\n", __FUNCTION__);

      if (mydata == NULL)  EXIT_ON_ERROR("pb malloc");

      /*Initialiser mydata*/
      mydata->nx            = 0;
      mydata->ny            = 0;
      mydata->nb_band       = 0;

      if (def_groupe[mydata->InputGpe].ext == NULL)
      {
         PRINT_WARNING("Gpe amont avec ext nulle; pas de calcul de competition");
         return;
      }

      /* allocation de memoire */
      def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
      if (def_groupe[Gpe].ext == NULL)  EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...!");


      /* recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image de ce groupe avec les infos recuperees */
      ext_input = (prom_images_struct *) def_groupe[mydata->InputGpe].ext;
      mydata->nx = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->sx;
      mydata->ny = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->sy;
      mydata->nb_band = ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext))->nb_band;
      n = mydata->nx * mydata->ny;
      if(mydata->take_previous)
         mydata->exclusion = ((Data_conv_dog*)def_groupe[mydata->InputGpe].data)->R_exclusion;
      dprints ("%s Taille image %d x %d\n",__FUNCTION__,mydata->nx,mydata->ny);
      if (mydata->nb_band !=4) EXIT_ON_ERROR ("%s L'image d'entree doit etre de type float ",__FUNCTION__);
      /* allocation de memoire */
      ext_output = calloc_prom_image(1, mydata->nx, mydata->ny, 1);
      ext_output->images_table[0] = (unsigned char *) calloc(n, sizeof(char));
      ext_output->images_table[1] = (unsigned char *) calloc(n, sizeof(int));
      /*ext_input->images_table[0] = (unsigned char *) calloc(n, sizeof(float));*/
      /* attention, pointeurs vers des reels ou des entiers */
      if (ext_output == NULL ||ext_output->images_table[0] == NULL ||ext_input->images_table[0] == NULL  )
         EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! %s ligne(%s)\n",def_groupe[Gpe].no_name,__LINE__);

      /* sauvgarde des infos trouves sur le lien */
      def_groupe[Gpe].ext  = ext_output;
   }
   /*Restitution des parametres*/
   ext_input   =   ((prom_images_struct *) (def_groupe[mydata->InputGpe].ext));
   ext_output  =   ((prom_images_struct *) (def_groupe[Gpe].ext));
   input = (float*) ext_input->images_table[0];
   output = (unsigned char*) ext_output->images_table[0];
   im_tmp = (int*) ext_output->images_table[1];


   nx = mydata->nx;
   ny = mydata->ny;
   n = nx*ny;

   competition_ptc(mydata->exclusion,mydata->ecart,nx,ny,input,im_tmp);

   for (i =n ;i--;){
      output[i] = (im_tmp[i] < 0 ? 0 : (unsigned char) im_tmp[i]);
      im_tmp[i] = 0;
   }

   //output[i] = (unsigned char) (255*input[i]);
   //

}

void destroy_compet_ptc(int Gpe)
{
   free(def_groupe[Gpe].data);
   free(def_groupe[Gpe].ext);
   def_groupe[Gpe].data = NULL;
   def_groupe[Gpe].ext = NULL;
   dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}



