/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
 /** ***********************************************************
\file  f_seuil_dynamic.c
\brief

Author: A.Jauffret
Created: 01/07/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Cette fonction prend une image du gradient en entrée et en calcul un seuil dynamique s'adaptant au contraste et à la luminosité, puis la fonction 		sort une image binarisée grâce à ce seuil.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: L'image en entrée doit être l'image du gradient.

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <libx.h>

#include <Struct/convert.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

typedef struct MyData{
  int gpe_image; 
  int option; 
	/*int gpe_joy;*/
	int gpe_fb;
	/*float feedback;*/
} MyData; 


/********************************************************************/


void new_seuil_dynamic(int gpe){
  
  int 								j,l;
  MyData 							*mydata=NULL;
  

  /* On enregistre les informations concernant le numero du 
     groupe contenant l'image en entree*/
    
  /* Recuperation de la valeur des parametres de chaque
     lien en entree du groupe  */
  
	/*if (def_groupe[gpe].data == NULL)
		{      
		printf("alloc mydata \n");
			mydata=(MyData*)malloc(sizeof(MyData));
			if(mydata==NULL) 
				{
					printf("pb malloc dans %d\n",gpe);
					exit(0);
				}
		
			mydata->gpe_image=-1;
			mydata->gpe_joy=-1;*/
			mydata->gpe_fb=-1;

			l = 0;
			j = find_input_link(gpe, l);
		
			while (j != -1)
				{
					if(strcmp(liaison[j].nom,"image")==0)
					mydata->gpe_image=liaison[j].depart;
		
					/*if(strcmp(liaison[j].nom, "joystick") == 0)
					mydata->gpe_joy= liaison[j].depart;

					if(strcmp(liaison[j].nom, "feedback") == 0)
					{
					mydata->gpe_fb= liaison[j].depart;
					mydata->feedback = neurone[def_groupe[mydata->gpe_fb].premier_ele].s;
					printf("feedback found :%f\n",mydata->feedback);
					}
					else
						mydata->feedback = 0.;*/

				
					l++;
					j = find_input_link(gpe, l);
			
			}
		    
			if (mydata->gpe_image == -1)
				{
					fprintf(stderr, "ERROR in new_seuil_dynamic(%s): Missing 'image' input link\n", def_groupe[gpe].no_name);
					exit(1);
				}
			
			/*if (mydata->gpe_joy == -1)
				{
					fprintf(stderr, "ERROR in new_seuil_dynamic(%s): Missing 'joy' input link\n", def_groupe[gpe].no_name);
					exit(1);
				}
		
			if (mydata->gpe_fb == -1)
				{
					fprintf(stderr, "ERROR in new_seuil_dynamic(%s): Missing 'feedback' input link\n", def_groupe[gpe].no_name);
					exit(1);
				}
		
 		}*/
  	
  def_groupe[gpe].data=mydata; /* sauvegarde de My_Data*/

}


void destroy_seuil_dynamic(int gpe)
{
  (void) gpe;
}



void function_seuil_dynamic(int gpe)
{	
  MyData 		*mydata=NULL;
  prom_images_struct 	*input=NULL, *output=NULL;

  int 			i;
  /*int			gpe_joy=0;*/
  int			tailleimage=0;
  /*int 			deb0, nbre;*/
  /*float			joy=0.;*/
  float 		moyenne;
  float 		ecart_type;
  int 			min_image=255,max_image=0;
  float			seuil = 30.0;
	
printf("\nbefore img data \n");

/* récup données et image */	
  mydata = (MyData *) def_groupe[gpe].data;

  if(((prom_images_struct *)(def_groupe[mydata->gpe_image].ext))==NULL)
		{
			printf("erreur: pas d'ext dans le groupe avant f_seuil_dynamic\n");
			exit(-1);
		}

printf("after test \n");

	/*mydata->feedback = neurone[def_groupe[mydata->gpe_fb].premier_ele].s;*/
  input=((prom_images_struct *)(def_groupe[mydata->gpe_image].ext));
  tailleimage=(input->sx)*(input->sy);	

  def_groupe[gpe].ext=(void *)calloc_prom_image(1,input->sx,input->sy,1);
  output=(prom_images_struct *)def_groupe[gpe].ext;  /*sauvegarde de l'image*/


	/* affinage manuel du seuil par le joystick */
	/*gpe_joy = mydata->gpe_joy;
	deb0 = def_groupe[gpe_joy].premier_ele;
	nbre = def_groupe[gpe_joy].nbre;

	for(i=deb0;i<deb0 + nbre;i++){
		if(neurone[i].s == 1)
			joy = 2 + 0.2*(i-11);
			
	}*/

	/* Calcul de l'écart type */
	moyenne = 0;
 	ecart_type = 0;
  
  for(i=0;i<tailleimage;i++){
    
    if( input->images_table[0][i] < min_image)
      min_image = input->images_table[0][i];
    
    if( input->images_table[0][i] > max_image)
      max_image = input->images_table[0][i];
       
    moyenne = moyenne + input->images_table[0][i];
  }
  
  moyenne = moyenne / tailleimage;
  
  for(i=0;i<tailleimage;i++)
    ecart_type = ecart_type + pow(input->images_table[0][i]-moyenne,2);	
  
  ecart_type = sqrt(ecart_type/tailleimage);

/*	if(joy > 0)
	seuil = ecart_type*joy*0.8 - mydata->feedback; */
	seuil = moyenne*2 - 10;

	/***************** DEBUG ********************/
  
  printf("moyenne : %f\n",moyenne);
  printf("ecart type : %f\n",ecart_type);
  printf("seuil : %f\n",seuil);
  /*printf("joy : %f\n",joy);*/
  /*printf("feedback : %f\n",mydata->feedback);*/
	/********************************************/
	

	/* sortie du seuil dynamique */
	neurone[def_groupe[gpe].premier_ele].s = seuil;

	/* binarisation de l'image */
	for(i=0;i<tailleimage;i++)
		{
			if(	input->images_table[0][i] > seuil )
				output->images_table[0][i] = 255;

			else
				output->images_table[0][i] = 0;
		}
	

}


