/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_vanish_point.c
\brief

Author: C. GRAND
Created: 17/10/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Cette fonction permet de trouver quel est le meilleur point de fuite potentiel entre X points (X nombre de neurones du groupe)

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-f_calgraD

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: La fonction doit recevoir en entrée un tableau des valeurs des gradients de l'image sur x et y. 

					- Les paramètres à passer sur le lien d'entrée sont :	


									-T	pour définir la tolérance (en radians, exemple : 0.3)

									-E  pour définir l'echelle de discrétisation (exemple : 8 --> pour ne calculer qu' 1/8 pixel)

									-H  pour définir la hauteur de l'horizon (attention on se base sur le haut de l'image --> exemple : 0.25 place l'horizon a 25% de l'image en partant du haut) 	 
					
									-Sh pour définir le pourcentage du haut de l'image non considéré
	
									-Sb pour définir le pourcentage du bas de l'image non considéré

									-Sc pour définir le pourcentage des côtés droit et gauche de l'image non considéré
Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <libx.h>

#include <Struct/convert.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>
#include "Vision/Contours.h"
#include "Vision/Image_Processing.h"
#include <Kernel_Function/prom_getopt.h>

/* Echantillonnage de l'image */
#define	ECHELLE	8

/*Valeur de HORIZON par défault*/
#define HORIZON 0.25

/*Dimension des zones par défault*/
#define Sh_def 0.04
#define Sb_def 0.04
#define Sc_def 0.04

typedef struct MyData{
  int gpe_grad;
	float Sh;
	float Sb;
	float Sc;
	int echelle;	
	float horizon;
} MyData; 


/********************************************************************/
void new_vanish_point(int gpe){
  
  	int i,j,l;
  	MyData *mydata=NULL;
	char param_link[256];
  
	#ifndef AVEUGLE
	init_rgb_colormap16M();
	#endif

  /* On enregistre les informations concernant le numero du 
     groupe contenant l'image en entree*/
    
  /* Recuperation de la valeur des parametres de chaque
     lien en entree du groupe  */
  
/*	if (def_groupe[gpe].data == NULL)
   	{    */
   	  
		mydata=(MyData*)malloc(sizeof(MyData));
		if(mydata==NULL) 
		{
			EXIT_ON_ERROR("pb malloc dans %d\n",gpe);
		}
		
		mydata->gpe_grad = -1;
		mydata->echelle = ECHELLE;
		mydata->horizon = HORIZON;
		mydata->Sh = Sh_def;
		mydata->Sb = Sb_def;
		mydata->Sc = Sc_def;

		l = 0;
		j = find_input_link(gpe, l);
		
		while (j != -1)
			{
				if (prom_getopt(liaison[j].nom, "-E", param_link) == 2 || prom_getopt(liaison[j].nom, "-e", param_link) == 2)
					{
						mydata->gpe_grad=liaison[j].depart;
						mydata->echelle = atoi(param_link);
					}	

				if (prom_getopt(liaison[j].nom, "-H", param_link) == 2 || prom_getopt(liaison[j].nom, "-h", param_link) == 2)
					{
						mydata->horizon = atof(param_link);
					}	
				
				if (prom_getopt(liaison[j].nom, "-Sh", param_link) == 2 || prom_getopt(liaison[j].nom, "-sh", param_link) == 2)
					{
						mydata->Sh = atof(param_link);
					}

				if (prom_getopt(liaison[j].nom, "-Sb", param_link) == 2 || prom_getopt(liaison[j].nom, "-sb", param_link) == 2)
					{
						mydata->Sb = atof(param_link);
					}

				if (prom_getopt(liaison[j].nom, "-Sc", param_link) == 2 || prom_getopt(liaison[j].nom, "-sc", param_link) == 2)
					{
						mydata->Sc = atof(param_link);
					}

				l++;
				j = find_input_link(gpe, l);
			
		}
      
  		if (mydata->gpe_grad == -1)
			{
				fprintf(stderr, "ERROR in new_vanish_point(%s): Missing direction input link\n", def_groupe[gpe].no_name);
				exit(1);
			}
	
/* 	}	*/

  	for(i=0 ; i<def_groupe[gpe].nbre ; i++)
   		{
      		neurone[def_groupe[gpe].premier_ele+i].s =0;
    		}
		def_groupe[gpe].ext=NULL;
  	def_groupe[gpe].data=mydata; /* sauvegarde de My_Data*/
}


void destroy_vanish_point(int gpe)
{
		free(def_groupe[gpe].data);
		def_groupe[gpe].data=NULL;
}

float fonc_R(float x)
{
   if (x<0)  return 1;
   if (x>1)  return 0;
   
   return 1-x;
}

float fonc_H(float x)
{
   if (x<0.9)  return 0;///on ne compte pas les pixels qui ne sont pas dans la bonne zone
   if (x>1)  return 1;

   return x;//((x-0.9)*10);
}


/*Produit scalaire normalisé des vecteurs x(x1,x2) et y(y1,y2) */

float produit_scalaire(float  x1,float  x2,float  y1,float  y2){
	float ps = 0;
	float nx = 0;
	float ny = 0;
	nx = x1*x1 + x2*x2;
	ny = y1*y1 + y2*y2;
	
	if (nx < 10|| ny < 10 || fabs(y1) <10 || fabs(x1) <10 || fabs(x2) <10 || fabs(y2) <10 )
	{
		ps =0;
	}
	else 
	{
		ps = (x1*y1 +x2*y2) /((sqrt(nx) * sqrt(ny))); /* ps = (x1*y1 +x2*y2) /((sqrt(nx) * sqrt(ny))); */
	}
	
	return ps;
}

float produit_scalaire2(float  x1,float  x2,float  y1,float  y2){
	float ps = 0;
	float nx = 0;
	float ny = 0;
	nx = x1*x1 + x2*x2;
	ny = y1*y1 + y2*y2;
	
	if (nx < 10|| ny < 10 || fabs(y1) <10 || fabs(x1) <10 || fabs(x2) <10 || fabs(y2) <10 )
	{
		ps =1;
	}
	else 
	{
		ps = (x1*y1 +x2*y2) /((sqrt(nx) * sqrt(ny))); /* ps = (x1*y1 +x2*y2) /((sqrt(nx) * sqrt(ny))); */
	}
	
	return ps;
}

void function_vanish_point(int gpe)
{	
 		MyData *mydata=NULL;
  	prom_images_struct *grad_struct=NULL;
	 	float 	*ix = NULL, *iy = NULL, *n = NULL;

  	int i = 0;
		int j = 0;
		int k,l,indice,taillegroupe,taillegroupe2,taillegroupe3;
		float Sh,Sb,Sc;
		float deb_X = 0;
		float fin_X = 0;
		float deb_Y = 0;
		float fin_Y = 0;
		int haut_d = 0;
		int bas_d = 0;
		int haut_g = 0;
		int bas_g = 0;
  	int deb1;
		float ps1 = 0;
		float ps2 = 0;

  	float X_image,Y_image,Y_Horizon;
		int L_image;
		float * tab_X;
		float * tab_Xp;
		float * tab_Yp;
		float * Act_PF;

	int p1;
	float * p2;
						

		mydata = def_groupe[gpe].data;
  
  	/*debut du groupe courant*/
  	deb1 = def_groupe[gpe].premier_ele;

		/*Nombre de points de fuite considérés*/
		taillegroupe = def_groupe[gpe].nbre;
	
		tab_X = malloc(taillegroupe*sizeof(float));
	  tab_Xp = malloc(4*taillegroupe*sizeof(float));
		tab_Yp = malloc(4*taillegroupe*sizeof(float));
		Act_PF = malloc(4*taillegroupe*sizeof(float));
	
		if(tab_Xp==NULL) 
		{
			printf("pb malloc pour tab_Xp");
			exit(0);
		}

		if(Act_PF==NULL) 
		{
			printf("pb malloc pour Act_PF");
			exit(0);
		}

 		/* ----------------------------------------------------- */
  	/* Recuperation de l'orientation et du gradient binarisé */
  	/* ----------------------------------------------------- */
	
	if(((prom_images_struct *)(def_groupe[mydata->gpe_grad].ext))==NULL)
	{
		printf("erreur: pas de grad dans le groupe avant f_calgraD, vérifier que le groupe précédent est f_calgraD\n");
		exit(-1);
	}   
	grad_struct=(prom_images_struct *) (def_groupe[mydata->gpe_grad].ext); 
	

		  /* recup ix & iy du calgrad */
  ix =(float*) ((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))->images_table[1];
  iy =(float*) ((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))->images_table[2];
	n =(float*) ((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))->images_table[3];

	/* calcul des constantes de dimension de l'image */

  X_image = grad_struct->sx;
  Y_image = grad_struct->sy;
	Sh = mydata->Sh;
	Sb = mydata->Sb;
	Sc = mydata->Sc;
	if (Sh > 1 || Sh <0 || Sb > 1 || Sb <0 || (Sb+Sh)>1){
		printf("Probleme au niveau des dimensions d'exclusion de l'image, on doit respecter les conditions suivantes : 0<Sh<1, 0<Sb<1,	0<Sh+Sb<1\n");
		exit(0);
	}
	
	if (Sc > 1 || Sc <0){
		printf("Probleme au niveau des dimensions d'exclusion de l'image, on doit respecter les conditions suivantes : 0<Sc<1\n");
		exit(0);
	}


	L_image = X_image*(1-2*Sc);
  Y_Horizon = Y_image * mydata->horizon;
	deb_X = Sc* X_image;
	fin_X = (1 - Sc)* X_image; 
	deb_Y =  Sh* Y_image;
	fin_Y =  (1 - Sb)* Y_image; 

	deb_X= (int) deb_X;
	fin_X= (int) fin_X;
	deb_Y= (int) deb_Y;
	fin_Y= (int) fin_Y;


	
	/* Positionnement des points de fuites potentiels*/
	/*initialisation des neurones du groupe courant*/
	/*Calcul des orientations privilégiées*/
		/*Initialisation du tableau d'activité*/
	
	
	for(k=taillegroupe;  k--;   )
	{
		*(tab_X+k) = 0;	
		//*(tab_X+k)  = (L_image/(taillegroupe*2))*(k*2+1) + Sc*X_image;
		*(tab_X+k)  = (X_image/(taillegroupe*2))*(k*2+1);

		*(tab_Xp+0+k) = - (X_image - *(tab_X+k));
		*(tab_Xp+1*taillegroupe+k) = *(tab_X+k);
		*(tab_Xp+2*taillegroupe+k)  = *(tab_X+k);
		*(tab_Xp+3*taillegroupe+k)  = - (X_image - *(tab_X+k));

		*(tab_Yp+0+k) = Y_Horizon;
		*(tab_Yp+1*taillegroupe+k) = Y_Horizon;
		*(tab_Yp+2*taillegroupe+k)  = Y_Horizon - Y_image;
		*(tab_Yp+3*taillegroupe+k)  = Y_Horizon - Y_image;
			
		neurone[deb1+k].s = neurone[deb1+k].s1 = neurone[deb1+k].s2=0.;
	    for(l=4; l-- ; ) 
		{
			*(Act_PF+l*taillegroupe+k) =0;
		}
	} 
	

	
	


	/*------------------------------------------------------*/
	/*                 Balayage de l'image                  */
	/*------------------------------------------------------*/
	

	
	for(i=deb_X; i<fin_X; i+=mydata->echelle)
	{
		for(j=deb_Y; j<fin_Y; j+=mydata->echelle)
		{
		  indice = i + j*X_image;
			//printf("n[%d] = %f\n",indice,n[indice]);
			
		   if (n[indice] > 200) //(ix[indice] > 0.005)|| (iy[indice] > 0.005) && 
			 {	
				//t++; 
			  for (k=taillegroupe; k--; )	
			    {
				  for(l=4; l--; )
				    {

						/*produit scalaire du vecteur de la direction privilégiées avec le vecteur allant du pixel vers le point de fuite*/
						ps1 = fonc_H(produit_scalaire(*(tab_Xp+k+l*taillegroupe),*(tab_Yp+k+l*taillegroupe),*(tab_X+k)-i,Y_Horizon-j));
						/*produit scalaire du vecteur du gradient avec le vecteur allant du pixel vers le point de fuite*/
						ps2 = fonc_R(fabs(produit_scalaire2(ix[indice],iy[indice],*(tab_X+k)-i,Y_Horizon-j)));
						/*Calcul de l'activité*/
						*(Act_PF+l*taillegroupe+k) += ps1 *ps2 /** n[indice]*/;
						//printf(" *(tab_Xp+k+l*taillegroupe) = %f, *(tab_Yp+k+l*taillegroupe)= %f, *(tab_X+k)-i= %f, Y_Horizon-j= %f, ps1 = %f\n",*(tab_Xp+k+l*taillegroupe),*(tab_Yp+k+l*taillegroupe),*(tab_X+k)-i, Y_Horizon-j,ps1);
						}
					}
				}
			 } 
			
		}

	
//printf("t = %d\n",t);

    taillegroupe2= 2*taillegroupe; taillegroupe3= 3*taillegroupe;
	for (k=taillegroupe; k--;  )
	{
		bas_d = (int)((*(Act_PF+ taillegroupe3 +k)));
		bas_g = (int)((*(Act_PF+ taillegroupe2 +k)));
		haut_d =  (int)( (*(Act_PF+            +k)));
		haut_g =  (int) ((*(Act_PF+ taillegroupe+k)));
		p1= deb1+k; p2 = Act_PF +k; 
		neurone[p1].s= neurone[p1].s1 = neurone[p1].s2 =bas_d *bas_g +haut_d*haut_g ;

		//printf("les valeurs pour le debug haut_d = %d ,haut_g = %d, bas_d = %d et bas_g = %d pour le pf %d\n", haut_d,haut_g, bas_d , bas_g ,k);
	}

		free(tab_X);
		free(tab_Xp);
		free(tab_Yp);
		free(Act_PF);

	/*Fin de la fonction*/

}


















