/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_orient.c
\brief

Author: A.Jauffret
Created: 15/07/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Calcul de l'orientation et l'amplitude du gradient dans l'image en entrée.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-f_orient

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: L'image en entrée doit être une image du gradient, si possible binarisée.

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <libx.h>
#include <string.h>

/* #include "tools/include/amphi.h" */
#include "tools/include/orient.h"
#include "tools/include/foncy.h"
#include "tools/include/fons1.h"
#include "tools/include/Seuillage.h"
/* #include "tools/include/coeff.h" */
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

#define	ALPHA		0.5

/*attention penser a modifier aussi dans vanishpoint*/
#define NOPOINT -10. /* flag indiquant les pixels sans interêt */

typedef unsigned char uchar;

float alpha, a1, a2, a3, a4, a5, a6, a7, a8, b1, b2, c1, c2, k, k1, a0;

typedef struct MyData
{
   int gpe_seuil_dyn;
   int gpe_grad;
} MyData;

/*prototype*/
void function_test_orientation (uchar * imIN, float *ori,float mon_seuil, unsigned int hauteur,unsigned int largeur, float *ix, float *iy);



/***************************************************************************************************************************************/
void new_orient (int gpe)
{

   int j, l;
   MyData *mydata = NULL;


   /* On enregistre les informations concernant le numero du
      groupe contenant l'image en entree */

   /* Recuperation de la valeur des parametres de chaque
      lien en entree du groupe  */

   if (def_groupe[gpe].data == NULL)
   {
      mydata = (MyData *) malloc (sizeof (MyData));
      if (mydata == NULL)
      {
         EXIT_ON_ERROR ("pb malloc dans %d\n", gpe);
      }

      mydata->gpe_seuil_dyn = -1;

      l = 0;
      j = find_input_link (gpe, l);

      while (j != -1)
      {
         if (strcmp (liaison[j].nom, "seuil_dyn") == 0)   mydata->gpe_seuil_dyn = liaison[j].depart;
         if (strcmp (liaison[j].nom, "grad") == 0)         mydata->gpe_grad = liaison[j].depart;

         l++;
         j = find_input_link (gpe, l);

      }

      if (mydata->gpe_seuil_dyn == -1)
      {
         EXIT_ON_ERROR ("ERROR in new_f_orient(%s): Missing 'seuil_dyn' input link\n",def_groupe[gpe].no_name);
      }

      if (mydata->gpe_grad == -1)
      {
         EXIT_ON_ERROR ("ERROR in new_f_orient(%s): Missing 'grad' input link\n", def_groupe[gpe].no_name);
      }

   }

   def_groupe[gpe].ext = NULL;
   def_groupe[gpe].data = mydata;	/* sauvegarde de My_Data */
}

void destroy_orient (int gpe)
{
   (void) gpe;
}

/***************************************************************************************************************************************/

void function_orient (int gpe)
{
   MyData 		*mydata = NULL;
   prom_images_struct 	*input = NULL, *output = NULL;

   /*float 	*ori = NULL;*/
   float 	seuil = 30.0;
   float 	*ix = NULL, *iy = NULL;
   float 	* sortie=NULL;
   /*float		*grad=NULL;*/


   mydata = (MyData *) def_groupe[gpe].data;
   if ((((prom_images_struct *) (def_groupe[mydata->gpe_seuil_dyn].ext)) || ((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))) ==  0)
   {
      EXIT_ON_ERROR ("erreur: pas d'ext dans le groupe avant f_orient\n");
   }


   /* recup ix & iy du calgrad */
   ix =(float*)((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))->images_table[1];
   iy =(float*)	((prom_images_struct *) (def_groupe[mydata->gpe_grad].ext))->images_table[2];

   /* récup données et image */
   input = ((prom_images_struct *) (def_groupe[mydata->gpe_seuil_dyn].ext));
   seuil = neurone[def_groupe[mydata->gpe_seuil_dyn].premier_ele].s;

   if (input == NULL)
   {
      EXIT_ON_ERROR ("Erreur : le champs ext du groupe precedent existe bien mais est nulle\n");
   }

   output = def_groupe[gpe].ext ;

   /* image en sortie */
   if (output == NULL)
   {
      output = (prom_images_struct *) malloc (sizeof (prom_images_struct));
      if (output == NULL)
      {
         EXIT_ON_ERROR ("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,  __LINE__);
      }

      output->image_number = 1;
      output->sx = input->sx;
      output->sy = input->sy;
      output->nb_band = 4;
      def_groupe[gpe].ext = output;

      if ((output->images_table[0] = (unsigned char *) malloc (input->sx*input->sy * sizeof (float))) == 0) /*Attention au malloc ici, utiliser plutot la fonction d'allocation */
      {
         EXIT_ON_ERROR ("ALLOCATION IMPOSSIBLE ...! \n");
      }


   }
   sortie= (float *) output->images_table[0];

   /* ----------------------- */
   /* Calcul de l'orientation */
   /* ----------------------- */
   function_test_orientation(input->images_table[0],sortie,seuil,input->sx,input->sy,ix,iy);

}

/***************************************************************************************************************************************/

void function_test_orientation (uchar * imIN, float *ori, float mon_seuil, unsigned int hauteur,unsigned int largeur, float *ix, float *iy)
{
   unsigned n;			/* Taille de l'image */
   long indice;			/* Indices de tableaux */
   /*float angle;*/

   (void) mon_seuil;

   /* Constantes */
   n = hauteur * largeur;
   a0 = alpha = ALPHA;

   /* Calcul de l'orientation des contours */
   orient (ori, iy, ix, n);

   for (indice = 0; indice < n; indice++)
   {
      if (imIN[indice] < 255)	(ori)[indice] = NOPOINT;
   }

}

/***************************************************************************************************************************************/


