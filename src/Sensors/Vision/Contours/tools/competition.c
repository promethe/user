/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************

************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>



void competition_ptcg_pondere(int ecart, int xmax, int ymax, int *input, int *output)

{
  int p,i,j,qi,j2;
  int nbr;
  float /*v,rapport,*/v1;
  float v2,v3,ponderation;

  /*rapport = 256. / valeur_max;*/
  nbr = xmax * ymax;
  ponderation = 3./(ecart*(ecart-1)*4); /*facteur de ponderation*/
                                  /*important: difficile a manipuler*/
  
  
  for (p = (ecart - 1) * (1 + xmax); p < (nbr - ((1 + xmax) * (ecart - 1))); p++)
    {
      v2=v1=input[p];
      if(v1>0.)
	{
       	  for (j = 1 - ecart; j < ecart; j++)
	    {
	      j2=p+xmax * j;
	      for (i = 1 - ecart; i < ecart; i++)
		{
		  qi = j2+ i ;
		  if (i | j)
		    {
		      v3=input[qi];
		      if (v3 >= v1) 
			{ 
			  output[p] = 0;
			  goto finie2;
			}
		      else if (v3 > 0) 
			v2=v2-v3*ponderation; /*retire la moyenne*/
		      
		    }
		}
	    }
	  /*v = v2 * rapport;*//*les points ne sont plus normalises entre 0 et 255*/
	  if (input[p]<0) input[p]=0;
	  output[p] = input[p];
	}
      else output[p] = 0;
    finie2:;
    }
}  

void competition_ptcg(int ecart, int xmax, int ymax, int *input, int *output)
{
  int p,p1,p2,i,j,qi,j2,p3;
  float /*v,*/v1;
  /*,rapport;*/
  /*rapport = 256. / valeur_max;*/

  for (p2 = (ecart - 1); p2 < ymax - (ecart - 1); p2++)
  {
    p3=p2*xmax;
    for(p1 = (ecart - 1); p1 < xmax - (ecart - 1); p1++)
    {
      p=p1+p3;
      v1=input[p];
      if(v1>0. && output[p]>=0)
      {
        for (j = 1 - ecart/2; j < ecart/2; j++)
        {
          j2=p+xmax * j;
          for (i = 1 - ecart/2; i < ecart/2; i++)
          {
            qi = j2+ i ;
            if (i | j)
            {
              if (input[qi] >= v1)
              {
                output[p] = 0;
                goto finie2;
              }
              else
              {
                  output[qi]=-1; /* un point dans le voisinage plus faible on le met a -1 pour accelerer la suite des calculs */
              }
            }
           }
          }
          /*v = v1 * rapport;
          im_pt_carac[p] = (int) v;*/
          output[p] = input[p];  /*l'image d'entree est deja normalisee*/
       }
      else output[p] = 0;
      finie2:;
     }
    }
    
}  


