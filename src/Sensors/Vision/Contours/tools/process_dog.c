/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  process_dog.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Modified:
- author: A. Karaouzene
- description: Threading
- date: 23/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

 - Recherche les points caracteristiques en utilisant le masque
 - La convolution est threadée. Le nombre de threads est le rapport entre la taille en Y et la taille du masque.
 - La normalisation s'effectue dans f_conv_dog.

Ordre des appels :
 * Process_dog_thread.
 *
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

/*#define DEBUG */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <pthread.h> 
#include "net_message_debug_dist.h"
#include "include/process_dog.h"

pthread_mutex_t mut_bandes[1024];

extern void gestion_mask_signaux();

typedef struct type_arg_pt_carac
{
  int y;
  int nb_bandes;
  unsigned char *im_contour;
  int l;
  int la;
  int xmax;
  int ymax; 
  float *im_fl; 
  int *im_pt_carac;
  float **tableau;
  int seuil;
  int no_thread;
} type_arg_pt_carac;

/*Calcul une convolution normale passage de la dog sur l'image et non le contraire*/
void process_dog_ligne2(void *arg) {
  int x, i, j, p;

  float *p2;
  int j2, j3;
  float v;
  float ech_gris;
  float z;
  float v1, seuil_float = 0.;
  float *tab2;
  unsigned char *p_contour, *tmp_contour;
  int y;
  unsigned char *im_contour;
  int l; /*int la;*/
  int xmax; /*int ymax;*/
  float *im_fl; /*int *im_pt_carac;*/
  float **tableau; /*Le masque DOG*/
  int seuil;
  type_arg_pt_carac *my_arg;

  gestion_mask_signaux();

  my_arg = (type_arg_pt_carac*) arg;
  y = my_arg->y;
  im_contour = my_arg->im_contour;
  l = my_arg->l;
  /*la=my_arg->la;*/
  xmax = my_arg->xmax;
  /*ymax=my_arg->ymax;*/
  im_fl = my_arg->im_fl;
  /*im_pt_carac=my_arg->im_pt_carac;*/
  tableau = my_arg->tableau;
  seuil = my_arg->seuil;

  seuil_float = ((float) seuil) / 255.; /*Le seuil est mis entre 0 t 1 */
  ech_gris = 1 / 255.0;

  j2 = xmax * y;
  p = l + j2;
  p2 = im_fl + p;
  p_contour = &(im_contour[p]);
  dprints("%s no_thread %d, y=%d  xmax=%d \n", __FUNCTION__, my_arg->no_thread, y, xmax);
  for (x = l; x < xmax - l; x++)   //for (x = xmax-2*l-1;x--;)
  {
    for (j = -l + 1; j < l; j++)
    {
      j3 = xmax * j;
      tmp_contour = p_contour - l + 1 + j3;
      tab2 = &(tableau[l + j][1]);
      for (i = l + l - 1; i--;)/* for (i = -l + 1; i < l; i++)*/
      {
        v1 = (float) (*tmp_contour) * ech_gris;
        v = *tab2;
        if (v1 > seuil_float)
          z = (*p2) += (v * v1); /* z= im_fl1[j2+x+i+xmax*j] = im_fl1[j2+x+i+xmax*j]+ tableau[l + j][l + i]*v1;*/
        /*if ( z > valeur_max)  valeur_max = z;
					 else if ( z < valeur_min)  valeur_min = z;*/
        tab2++;
        tmp_contour++;
      }
    }
    p2++;
    p_contour++;
    /*printf ("val max = %f, a la ligne : %d colonne : %d \n",valeur_max,y,x);*/
  }
  /* pthread_exit(NULL);*/
  (void) z; /*variable inutilisee*/
}

/* calcule l'effet de la colonne y sur le voisinage [-l, +l] */
/* contribution du filtre tableau */
void process_dog_ligne(void *arg)
{
  int x, i, j, p;

  float *p2,*q;
  int j2,j3;

  float v;
  float ech_gris;
  float z;
  float v1, seuil_float = 0.;
  float *tab2;
  unsigned char *p_contour;
  int y; 
  unsigned char *im_contour;
  int l;
  int xmax;
  float *im_fl;
  float **tableau;
  int seuil;
  type_arg_pt_carac *my_arg;

  gestion_mask_signaux();

  my_arg=( type_arg_pt_carac*) arg;
  y=my_arg->y;
  im_contour=my_arg->im_contour;
  l=my_arg->l;
  xmax=my_arg->xmax;
  im_fl=my_arg->im_fl;
  tableau=my_arg->tableau;
  seuil=my_arg->seuil;

  seuil_float = ((float) seuil) / 255.;   /*Le seuil est mis entre 0 t 1 */
  ech_gris=1/ 255.0;

  j2= xmax * y;
  p = l + j2;
  p2=im_fl+p;
  p_contour=&(im_contour[p]);
  dprints("%s no_thread %d, y=%d  xmax=%d \n",__FUNCTION__,my_arg->no_thread,y,xmax);
  for (x = l; x < xmax - l; x++)
  {
    v1 = (float) (*p_contour) * ech_gris;
    /* v1 = (float)im_contour[j2+x]*ech_gris;*/
    if (v1 > seuil_float)
      for (j = -l + 1; j < l; j++)
      {
        j3 = xmax * j;
        /*q= & im_fl1[p-l+1+xmax*j];  */ 
        q = p2  -l + 1 + j3;  
        tab2=&(tableau[l + j][1]);
        for (i = l+l-1; i --; )
          /* for (i = -l + 1; i < l; i++)*/
        {
          v = *tab2; /*tableau[l + j][l + i];*/
          /* printf("x=%d y=%d  i=%d j=%d xmax=%d \n",x,y,i,j,xmax);*/
          z = (*q) +=  (v * v1); /* z= im_fl1[j2+x+i+xmax*j] = im_fl1[j2+x+i+xmax*j]+ tableau[l + j][l + i]*v1;*/
          /*if ( z > valeur_max)  valeur_max = z;
          else if ( z < valeur_min)  valeur_min = z;*/
          q++;
          tab2++;
        }
      }
    p2++;
    p_contour++;
    /*printf ("val max = %f, a la ligne : %d colonne : %d \n",valeur_max,y,x);*/
  }
  /* pthread_exit(NULL);*/
  (void) z; /*variable inutilisee*/
}


void *process_dog_nlignes(void *arg)
{
  type_arg_pt_carac *my_arg;
  int y,k,nbre,l,i;
  int ret;

  my_arg=( type_arg_pt_carac*) arg;
  y=my_arg->y; 
  nbre=my_arg->nb_bandes;
  l = my_arg->l;
  dprints("start thread %d y=%d, nbre=%d \n",my_arg->no_thread,y,nbre);

  for (i = y -l + 1; i < y+l;i++)
  {
    ret = pthread_mutex_lock (&mut_bandes[i]);
    dprints("thread %d lock=%d\n",my_arg->no_thread,i);
    if(ret!=0) EXIT_ON_ERROR("first lock %d",ret);
  }
  for(k=y;k<y+nbre;k++)
  {
    /*printf ("ligne : %d\tthread n° : %d\n",y,my_arg->no_thread);*/
    my_arg->y=k; /* attention y est maintenant le No de la ligne courante */
    /*Mutex*/

    process_dog_ligne(my_arg);  /* lance le calcul a partir de la ligne k */
    ret = pthread_mutex_unlock (&mut_bandes[k-l+1]); /*dprints ("thread %d unlocked ligne %d \n",my_arg->no_thread,k-l+1);*/
    if(ret!=0) EXIT_ON_ERROR("unlock %d",ret);
    ret = pthread_mutex_lock (&mut_bandes[k+l]); /*dprints ("thread %d  locked ligne %d \n",my_arg->no_thread,k+l+1);*/
    if(ret!=0) EXIT_ON_ERROR("lock %d",ret);
  }
  for (i = k-l+1;i<k+l;i++)
  {
    ret = pthread_mutex_unlock (&mut_bandes[i]);
    if(ret!=0) EXIT_ON_ERROR("last unlock %d",ret);
    /* dprints("thread %d lock=%d\n",my_arg->no_thread,i);*/
  }

  dprints("Stop thread %d\n",my_arg->no_thread);
  return NULL;
}

#define NB_MAX_THREADS 8000
#define NB_COL_THREAD 10

/*Calcul le nombre de thread a lancer et affecte à chaque thread le nombre de ligne a convoluer
 * Cree les threads, les appel, et les detruit a la fin*/
void convol_dog_thread(unsigned char *im_contour, int l, int la, int xmax,
    int ymax, float *im_fl, float **tableau, int seuil )
/* tableaux de flottants intermediaires   */
{
  int x, y, n, j;
  float *im_fl1;
  type_arg_pt_carac arg[NB_MAX_THREADS];
  pthread_t un_thread[NB_MAX_THREADS];
  void *resultat;
  int res, nb_threads;
  int nbre;

  im_fl1=im_fl;
  x = xmax * ymax; 
  memset(im_fl1,0,x*sizeof(float)); /* ne marche que si 0. est 4 0 char OK norme IEEE mais sinon... */


  //  nb_threads=ymax/(2*l); /*nombre de thread*/
  //  nbre=ymax/nb_threads ; /*nbre de lignes par bande*/
  nb_threads = (ymax - 2* l) / (2 * l); /*nombre de thread*/
  //nb_threads = 4;
  nbre = (ymax - 2*l ) / nb_threads; /*nbre de lignes par bande*/
  dprints("nombre de threads a lancer = %d ymax=%d l=%d \n",nb_threads,ymax,l);
  /*  for (y = l+y0; y < ymax - (2*l+1)*nbre; y=y+nbre*(2*l+1))*/
  /*for (y = l+y0; y < ymax - l; y=y+(2*l+1)) */
  //  for(n=nb_threads-1;n--;) /*boucle inversee dernier thread premier a demarrer ***plus safe!!!!****/
  for(n=0;n<nb_threads;n++)
  {

    y=n*nbre+l-1;

    arg[n].y=y;arg[n].im_contour=im_contour;arg[n].l=l; arg[n].la=la;
    arg[n].xmax=xmax; arg[n].ymax=ymax;arg[n].im_fl=im_fl;
    arg[n].tableau=tableau; arg[n].seuil=seuil;
    arg[n].nb_bandes= nbre ;          /*nb_bandes est le nombre de ligne par bande*//*nbre*2*l*/ /*+1*/
    arg[n].no_thread=n;
    if(n==nb_threads-1)
    {
      arg[n].nb_bandes = ymax - y -  l;
    }
    dprints("lance thread %d , traitant  %d  ligne de (%d) a (%d)\n ",
        arg[n].no_thread, arg[n].nb_bandes, arg[n].y,
        arg[n].y + arg[n].nb_bandes - 1);

    res = pthread_create(&(un_thread[n]), NULL, process_dog_nlignes, (void *) &(arg[n]));

    if (res != 0)
      EXIT_ON_ERROR("fealure on thread creation \n");

  }

  dprints("---- %d threads on ete lances ---\n",n);
  //  for(j=nb_threads-1;j--;)
  for (j = 0; j < nb_threads; j++)
  {
    /*printf("j= %d \n",j); */
    res = pthread_join(un_thread[j], &resultat);
    if (res == 0)
    {
      dprints("thread %d recueilli \n",j);
    }
    else
    {
      kprints("echec de pthread_join %d pour le thread %d\n", res, j);
      exit(0);
    }
  }

  dprints("execution THREADS terminee \n");
  /* }*/

}


/* lancement de threads */
void process_dog_thread(unsigned char *im_contour, int l, int la, int xmax,int ymax,
    float *im_fl,float **tableau, int seuil )
{
  int i,ret ;
  for(i=0;i<1024;i++){
    ret = pthread_mutex_init(&mut_bandes[i], NULL);
    if(ret!=0) EXIT_ON_ERROR("a linit %s ret = %d",__FUNCTION__,ret);

  }
  convol_dog_thread(im_contour, l,la, xmax, ymax, im_fl, tableau, seuil);
}


void recherche_valeur_minmax_dog(int l /*taille_masque*/ , float ** tableau /*masque*/, float * valeur_min, float * valeur_max /*valeur_resultat*/)
{
  int j,i;
  float v1;
  float v,zM=0,zm=0;

  *valeur_max=-100.;
  for (j = 0; j < 2*l-1; j++)
  {
    for (i = 0 ; i < 2*l-1; i++)
    {
      v = tableau[i][j];

      if(v>0)   v1=1.;  else    v1=0.;
      zM+=  (v * v1);

      if(v>0)   v1=0.;  else    v1=1.;
      zm+=  (v * v1);
      /*  else if ( z < valeur_min)  valeur_min = z;*/
    }
  }
  *valeur_max = zM;
  *valeur_min = zm;
}

