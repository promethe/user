/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  init_masque_pt_carac.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Definit le masque utilise pour l'extraction de points carac
 l       : taille du masque
 theta1  : coefficient de la gaussienne partie pos
 theta2  : coeff de la 2emme gaussienne partie neg
 Operateur forme par la difference de 2 gaussiennes

 Macro:
 -Taille_Max_Tableau

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <math.h>

#include "include/macro.h"

float **init_masque_pt_carac(int l, float theta2, float theta1)
{
  float **tableau;
  int i, j;
  float a1, a2, d, d1, d2, a3, a4;

  /* unused :
   int x,y,p,q,s,fin;
   float v,m;
   */

  if (l > Taille_Max_Tableau)
  {
    PRINT_WARNING("Erreur : largeur du masque demandee trop grande\n");
    tableau = 0;
    return (tableau);
  }

  /*
   cree_tableau_2d(tableau,float,2*l,2*l);
   */

  tableau = (float **) calloc(2 * l, sizeof(float *));
  if (tableau == NULL)
  EXIT_ON_ERROR("allocation memoire impossible -> ARRET\n");

  for (i = 0; i < 2 * l; i++)
  {
    tableau[i] = (float *) calloc(2 * l, sizeof(float));
    if (tableau[i] == NULL)
    EXIT_ON_ERROR("allocation memoire impossible -> ARRET\n");
  }

  a3 = (2. * theta1 * theta1);
  a4 = (2. * theta2 * theta2);
  a1 = 1. / (2. * M_PI * theta1 * theta1); /*M.M. en 2D c'est ca le facteur de normalisation... */
  a2 = 1. / (2. * M_PI * theta2 * theta2);

  for (j = -l + 1; j < l; j++)
  {
    for (i = -l + 1; i < l; i++)
    {
      d = (float) (i * i + j * j);
      d1 = exp(-d / a3);
      d2 = exp(-d / a4);
      tableau[l + i][l + j] = ((a1 * d1 - a2 * d2));
    }
  }
  return (tableau);
}
