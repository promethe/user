/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_calgradDS.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-BORDS 

Local variables:
-none

Global variables:
-none

Internal Tools:
-Seuillage()
-bords()
-SeuilOrient()
-orient()
-amphi()
-fons1()
-foncy()
-coeff()


External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <Struct/prom_images_struct.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/Seuillage.h"
#include "tools/include/bords.h"
#include "reseau.h"
void SeuilOrient(float Seuil, float *Psrce, float *Osrce,
                 unsigned char *Pdest, unsigned N);
void orient(float *or, float *ik, float *in, unsigned kn);
void amphi(float *ik, float *in, unsigned Nx, unsigned Ny);
void fons1(float *ik, int nx, int ny);
void foncy(float *ik, int nx, int ny);
int coeff(unsigned char c, float alpha);


void SeuilOrient(float Seuil, float *Psrce, float *Osrce,
                 unsigned char *Pdest, unsigned N)
{
    unsigned char Gr;
    unsigned long Compt;
    float Val;

    for (Compt = 0; Compt < N; Compt++)
    {
        Gr = (unsigned char) ceil(Psrce[Compt]);
        if (Gr < (unsigned char) Seuil)
            Pdest[Compt] = 0;
        else
        {
            Val = (Osrce[Compt] + M_PI) * 64.0 / M_PI;
            Pdest[Compt] = (unsigned char) Val + 128;
        }
    }
}

void orient(float *or, float *ik, float *in, unsigned kn)
{
    register int i;

    for (i = 0; i < (int)kn; ++i)
    {
      if (isequal(ik[i], 0) && isequal(in[i], 0.0))
            or[i] = 0.0;
        else
        {
            or[i] = (float) atan2((double) ik[i], (double) in[i]);
        }
    }

}

void amphi(float *ik, float *in, unsigned Nx, unsigned Ny)
{
    int k, sign, indice, indice1, indice2, indice3, indice4;
    register int i, j;
    float *a;
    float angle;
    float g1, g2, u, ux, uy;
    k = Nx * Ny;
    a = (float *) malloc(sizeof(float) * k);

    /*   calculer amplitude    */
    for (i = 0; i < (int) Ny; i++)
        for (j = 0; j < (int) Nx; j++)
        {
            indice = i * Nx + j;
            a[indice] =
                sqrt(ik[indice] * ik[indice] + in[indice] * in[indice]);
        }
    /*    suppression de non maximun      */
    for (i = 1; i <= (int) Ny - 2; i++)
        for (j = 1; j <= (int) Nx - 2; j++)
        {
            indice = i * Nx + j;
            if (isdiff(ik[indice], 0))
                angle = in[indice] / ik[indice];
            else
                angle = 2.;
            sign = 1;
            if (angle < 0)
                sign = (-1);
            ux = ik[indice];
            if (ux < 0)
                ux = ux * (-1);
            uy = in[indice];
            if (uy < 0)
                uy = uy * (-1);
            ik[indice] = 0;
            switch (sign)
            {
            case -1:
                if (angle < -1)
                {
                    u = a[indice] * uy;
                    indice1 = (i - 1) * Nx + j + 1;
                    indice2 = (i - 1) * Nx + j;
                    g1 = ux * a[indice1] + (uy - ux) * a[indice2];
                    if (u < g1)
                        continue;
                    indice3 = (i + 1) * Nx + j - 1;
                    indice4 = (i + 1) * Nx + j;
                    g2 = ux * a[indice3] + (uy - ux) * a[indice4];
                    if (u < g2)
                        continue;
                    if (isdiff(ik[indice1], 0) || isdiff(ik[indice2], 0))
                        continue;
                    ik[indice] = a[indice];
                    continue;
                }
                else
                {
                    u = a[indice] * ux;
                    indice1 = (i - 1) * Nx + j + 1;
                    indice2 = i * Nx + j + 1;
                    g1 = uy * a[indice1] + (ux - uy) * a[indice2];
                    if (u < g1)
                        continue;
                    indice3 = (i + 1) * Nx + j - 1;
                    indice4 = i * Nx + j - 1;
                    g2 = uy * a[indice3] + (ux - uy) * a[indice4];
                    if (u < g2)
                        continue;
                    if (isdiff(ik[indice1], 0) || isdiff(ik[indice4], 0))
                        continue;
                    ik[indice] = a[indice];
                    continue;
                }
                /* break; inutile ??? */
            case 1:
                if (angle >= 1)
                {
                    u = a[indice] * uy;
                    indice1 = (i + 1) * Nx + j + 1;
                    indice2 = (i + 1) * Nx + j;
                    g1 = ux * a[indice1] + (uy - ux) * a[indice2];
                    if (u < g1)
                        continue;
                    indice3 = (i - 1) * Nx + j - 1;
                    indice4 = (i - 1) * Nx + j;
                    g2 = ux * a[indice3] + (uy - ux) * a[indice4];
                    if (u < g2)
                        continue;
                    if (isdiff(ik[indice3], 0) || isdiff(ik[indice4], 0))
                        continue;
                    ik[indice] = a[indice];
                    continue;
                }
                else
                {
                    u = a[indice] * ux;
                    indice1 = (i - 1) * Nx + j - 1;
                    indice2 = i * Nx + j - 1;
                    g1 = uy * a[indice1] + (ux - uy) * a[indice2];
                    if (u < g1)
                        continue;
                    indice3 = (i + 1) * Nx + j + 1;
                    indice4 = i * Nx + j + 1;
                    g2 = uy * a[indice3] + (ux - uy) * a[indice4];
                    if (u < g2)
                        continue;
                    if (isdiff(ik[indice1], 0) || isdiff(ik[indice2], 0))
                        continue;
                    ik[indice] = a[indice];
                    continue;
                }
                /* break; inutile ??? */
            }
        }
    for (j = 0; j <= (int) Nx - 1; j++)
    {
        ik[j] = 0;
        ik[Nx + j] = 0;
        indice = (Ny - 1) * Nx + j;
        ik[indice] = 0;
        indice = (Ny - 2) * Nx + j;
        ik[indice] = 0;
    }
    for (i = 0; i <= (int) Ny - 1; i++)
    {
        indice = i * Nx;
        ik[indice] = 0;
        indice = i * Nx + 1;
        ik[indice] = 0;
        indice = i * Nx + Nx - 1;
        ik[indice] = 0;
        indice = i * Nx + Nx - 2;
        ik[indice] = 0;
    }
    for (i = 0; i <= (int) ((Ny - 1) * (Nx + 1)); i++)
        in[i] = a[i];
    free(a);
}

void fons1(float *ik, int nx, int ny)
{
    register int i, j;
    unsigned ind, indice, indice1, indice2;
    float *y1;
    float *y2;
    unsigned n = (unsigned) nx * (unsigned) ny;

    /*
     *  buffer image
     */

    if ((y1 = (float *) calloc(n, sizeof(float))) == NULL)
    {
        printf("ALLOCATION IMPOSSIBLE ...! \n");
        exit(1);
    };

    /*
     * buffer ligne
     */

    if ((y2 = (float *) calloc(nx, sizeof(float))) == NULL)
    {
        printf("ALLOCATION IMPOSSIBLE ...! \n");
        exit(1);
    };

    for (i = 0; i <= ny - 1; i++)
    {
        ind = i * nx;
        indice2 = ind + 1;

        y1[ind] = 0;
        y1[indice2] = a1 * ik[indice2] + a2 * ik[ind] + b1 * y1[ind];
        for (j = 2; j <= nx - 1; j++)
        {
            indice = ind + j;
            indice1 = indice - 1;
            indice2 = indice - 2;

            y1[indice] =
                a1 * ik[indice] + a2 * ik[indice1] + b1 * y1[indice1] +
                b2 * y1[indice2];
        }
    }
    for (i = 0; i <= ny - 1; i++)
    {
        ind = i * nx;
        indice1 = ind + nx - 1;
        y2[nx - 1] = 0;
        y2[nx - 2] = a3 * ik[indice1] + b1 * y2[nx - 1];
        for (j = nx - 3; j >= 0; j--)
        {
            indice = ind + j;
            indice1 = indice + 1;
            indice2 = indice + 2;
            y2[j] =
                a3 * ik[indice1] + a4 * ik[indice2] + b1 * y2[j + 1] +
                b2 * y2[j + 2];
            ik[indice2] = c1 * (y1[indice2] + y2[j + 2]);
        }

        indice2 = ind + 1;
        ik[ind] = c1 * (y1[ind] + y2[0]);
        ik[indice2] = c1 * (y1[indice2] + y2[1]);
    }
    free(y1);
    free(y2);
    return;
}

void foncy(float *ik, int nx, int ny)
{
    register int i, j;
    unsigned n, indice, indice1, indice2;
    float *y1;
    float *y2;
    /* unused :
       ind;
     */


    n = nx * ny;
    if ((y1 = (float *) calloc(n, sizeof(float))) == NULL)
    {
        printf("ALLOCATION IMPOSSIBLE ...! \n");
        exit(1);
    };
    if ((y2 = (float *) calloc(ny, sizeof(float))) == NULL)
    {
        printf("ALLOCATION IMPOSSIBLE ...! \n");
        exit(1);
    };
    for (j = 0; j <= nx - 1; j++)
    {
        indice1 = j;
        indice2 = nx + j;
        y1[indice1] = a5 * ik[indice1];
        y1[indice2] = a5 * ik[indice2] + a6 * ik[indice1] + b1 * y1[indice1];
        for (i = 2; i <= ny - 1; i++)
        {

            indice = i * nx + j;
            indice1 = indice - nx;
            indice2 = indice1 - nx;

            y1[indice] = a5 * ik[indice] + a6 * ik[indice1] +
                b1 * y1[indice1] + b2 * y1[indice2];
        }
    }
    for (j = 0; j <= nx - 1; j++)
    {
        indice1 = (ny - 1) * nx + j;
        indice2 = (ny - 2) * nx + j;
        y2[ny - 1] = 0;
        y2[ny - 2] = a7 * ik[indice1] + b1 * y2[ny - 1];

        for (i = ny - 3; i >= 0; i--)
        {
            indice = i * nx + j;
            indice1 = indice + nx;
            indice2 = indice1 + nx;

            y2[i] = a7 * ik[indice1] + a8 * ik[indice2] +
                b1 * y2[i + 1] + b2 * y2[i + 2];
            ik[indice2] = c2 * (y1[indice2] + y2[i + 2]);
        }
        indice2 = nx + j;
        ik[j] = c2 * (y1[j] + y2[0]);
        ik[indice2] = c2 * (y1[indice2] + y2[1]);
    }
    free(y1);
    free(y2);
    return;
}

int coeff(unsigned char c, float alpha)
{
    k = ((1 - exp(-alpha)) * (1 - exp(-alpha))) / (1 + 2 * alpha * exp(-alpha) - exp((-2) * alpha));
    k1 = (1 - exp(-2 * alpha)) / (2 * alpha * exp(-alpha));
    b1 = 2 * exp(-alpha);
    b2 = (-1) * exp((-2) * alpha);
    switch (c)
    {
    case 'x':
        /*derivee en x2      k*x*exp(-alpha*abs(x)) */
        a1 = 0;
        a2 = 1;
        a3 = (-1);
        a4 = 0;
        c1 = (-1) * (1 - exp(-alpha)) * (1 - exp(-alpha));
        /*lissage en x1      k*(*alpha*abs(x) +1)*exp(-alpha*abs(x)) */
        a5 = k;
        a6 = k * exp(-alpha) * (alpha - 1);
        a7 = k * exp(-alpha) * (alpha + 1);
        a8 = (-k) * exp((-2) * alpha);
        c2 = 1;
        break;
    case 'y':
        /*lissage en x2      k*(alpha*abs(x) +1)*exp(-alpha*abs(x)) */
        a1 = k;
        a2 = k * exp(-alpha) * (alpha - 1);
        a3 = k * exp(-alpha) * (alpha + 1);
        a4 = (-k) * exp((-2) * alpha);
        c1 = 1;
        /* derivee en x1   k*x*exp(-alpha*abs(x))   */
        a5 = 0;
        a6 = 1;
        a7 = (-1);
        a8 = 0;
        c2 = (-1) * (1 - exp(-alpha)) * (1 - exp(-alpha));
        break;
    case '7':                  /* Shen lissage */
        alpha = -log(1 - a0);
        b1 = exp(-alpha);
        b2 = 0;
        /*filtre exponentiel lissage en x2  k0*exp(-alpha*abs(x))   */
        a1 = 1;
        a2 = 0;
        a3 = exp(-alpha);
        a4 = 0;
        c1 = a0 / (2 - a0);
        /*filtre exponentiel lissage en x1 k0*exp(-alpha*abs(x))    */

        a5 = 1;
        a6 = 0;
        a7 = a3;
        a8 = 0;
        c2 = c1;
        break;
    case '8':                  /* Shen derivee premiere */
        alpha = -log(1 - a0);
        b1 = exp(-alpha);
        b2 = 0;
        /*filtre exponentiel derivee prem. en x2  exp(-alpha*abs(x))        */
        a1 = 0;
        a2 = 1;
        a3 = -1;
        a4 = 0;
        c1 = a0 / (2 - a0);
        /*filtre exponentiel derivee prem. en x1 exp(-alpha*abs(x)) f2(x1) */
        a5 = 0;
        a6 = 1;
        a7 = -1;
        a8 = 0;
        c2 = c1;
        break;
    }
    return (0);
}

void function_calgradDS(int Gpe)
{
    char *string = NULL, *ST;
    unsigned char *im;
    float *ix, *iy, *trans, *gradx, *grady;
    prom_images_struct *rez_image;

    unsigned int i;
    unsigned int nx, ny;
    unsigned int n;
    int InputGep = 0;

    float seuil, s_alpha, a0;
    /* 07/10/2003
       Olivier Ledoux
       passage des donnees en int
       JC passage en variables locales
     */

    /*
       static unsigned char typeDS,Intensity,R,O,M,C,Q,L,orientation;
     */
    int typeDS, Intensity, R, O, M, C, Q, L, orientation;

    /* 07/10/2003 Olivier Ledoux
       rajout d'un pointeur pour stocker les variables
       utilisees entre deux executions
     */
    float *temp;

    (void)a0; // (unused)

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif


    if (def_groupe[Gpe].ext == NULL)
    {

        /* recuperation des infos sur le gpe precedent */
        for (i = 0; i < (uint) nbre_liaison; i++)
            if (liaison[i].arrivee == Gpe)
            {
                InputGep = liaison[i].depart;
                string = liaison[i].nom;
                break;
            }

        if (def_groupe[InputGep].ext == NULL)
        {
            printf("Gpe amonte avec ext nulle; pas de calcul de gradient\n");
            return;
        }

        /* alocation de memoire */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        /* recuperation d'info sur le lien */
        /* recuperation du type (D ou S) */
        ST = strstr(string, "-T");
        if (ST != NULL)
            if (ST[2] == 'D')
            {
                printf("Type D\n");
                /* 07/10/2003 Olivier Ledoux
                   typeDS = '1';
                 */
                typeDS = 1;
            }
            else if (ST[2] == 'S')
            {
                printf("Type S\n");
                /* 07/10/2003 Olivier Ledoux
                   typeDS = '1';
                 */
                typeDS = 2;
            }
            else
            {
                printf("\n\nType %c inconnu \n\n", ST[2]);
                printf("Exit in function_calgradDS, Gpe = %d\n", Gpe);
                exit(-1);
            }
        else
        {
            printf("Error in function_calgradDS,you must specified -Ttype\n");
            exit(-1);
        }
        /* recuperation d's_alpha */
        ST = strstr(string, "-A");
        if ((ST != NULL))
        {
            s_alpha = atof(&ST[2]);
            printf("s_alpha = %f\n", s_alpha);
        }
        else
        {
            printf
                ("Error in function_calgradDS,you must specified -Aalpha\n");
            exit(-1);
        }
        /* recuperation type filtrage */
        Intensity = R = O = M = C = Q = orientation = 0;
        ST = strstr(string, "-F");
        if (ST != NULL)
            switch (ST[2])
            {
            case 'i':
                printf("Filtre i \n");
                Intensity = 1;
                break;
            case 'c':
                printf("Filtre c\n");
                C = 1;
                break;
            case 'q':
                printf("Filtre q\n");
                Q = 1;
                break;
            case 'm':
                printf("Filtre m\n");
                M = 1;
                break;
            case 'o':          /* pour avoir le gradiant oriente, Baccon 2001 */
                printf("Filtre o\n");
                orientation = 1;
                break;
            default:
                printf("Type inconnu \n\n");
                printf("Exit in function_calgradDS, Gpe = %d\n", Gpe);
                exit(-1);
            }
        else
        {
            printf("Error in function_calgradDS,you must specified -Ftype\n");
            exit(-1);
        }
        /* recuperation info lissage */
        L = 1;
        ST = strstr(string, "-L");
        if (ST != NULL)
            L = 0;
        /* recuperation info sur le seuil */
        seuil = 0.0;
        ST = strstr(string, "-S");
        if (ST != NULL)
        {
            seuil = atof(&ST[2]);
            printf("Seuil = %f\n", seuil);
        }

        /* recuperation des infos sur la taille */
        nx = ((prom_images_struct *) def_groupe[InputGep].ext)->sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = nx;
        ny = ((prom_images_struct *) def_groupe[InputGep].ext)->sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = ny;
        n = nx * ny;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band =
            ((prom_images_struct *) def_groupe[InputGep].ext)->nb_band;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;

        /* sauvgarde des infos trouves sur le lien */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[5] =
            (unsigned char *) malloc(11 * sizeof(float));

        /* 07/10/2003 Olivier Ledoux
           Test de reservation memoire
         */
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[5] ==
            NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(1);
        }
        else
        {
            /* 07/10/2003 Olivier Ledoux
               la variable est deja declaree
             */

            /* float *temp = (float*)(((prom_images_struct*)def_groupe[Gpe].ext)->images_table[5]);
             */
            temp =
                (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                           images_table[5]);

            temp[0] = (float) Intensity;
            temp[1] = (float) R;
            temp[2] = (float) O;
            temp[3] = (float) C;
            temp[4] = (float) Q;
            temp[5] = (float) L;
            temp[6] = (float) M;
            temp[7] = (float) s_alpha;
            temp[8] = (float) seuil;
            temp[10] = (float) orientation;
        }
        /* 07/10/2003 Olivier Ledoux
           stockage du parametre typeDS
         */

        /*
           if( typeDS == '2' )
           ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[5][8] = 2.0;
           else
           ((prom_images_struct*)def_groupe[Gpe].ext)->images_table[5][8] = 1.0;
         */
        temp[9] = (float) typeDS;

        /* recuperation du pointeur de l'image d'entree */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[4] =
            ((prom_images_struct *) def_groupe[InputGep].ext)->
            images_table[0];
        /* allocation de memoire */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(n * sizeof(char));
        /* attention, pointeurs vers de reels */
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) malloc(n * sizeof(float));
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            (unsigned char *) malloc(n * sizeof(float));
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[3] =
            (unsigned char *) malloc(n * sizeof(float));
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[6] =
            (unsigned char *) malloc(n * sizeof(float));
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[7] =
            (unsigned char *) malloc(n * sizeof(float));
        if ((((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
             NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[1] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[2] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[3] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[6] == NULL)
            || (((prom_images_struct *) def_groupe[Gpe].ext)->
                images_table[7] == NULL))
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        rez_image = def_groupe[Gpe].ext;
    }
    else
    {
        /* 07/10/2003 Olivier Ledoux
           variable deja declaree
         */

        /* float *temp;
         */

        rez_image = ((prom_images_struct *) def_groupe[Gpe].ext);
        nx = rez_image->sx;
        ny = rez_image->sy;
        n = nx * ny;
        temp = (float *) rez_image->images_table[5];
        Intensity = (unsigned char) temp[0];
        R = (unsigned char) temp[1];
        O = (unsigned char) temp[2];
        C = (unsigned char) temp[3];
        Q = (unsigned char) temp[4];
        L = (unsigned char) temp[5];
        M = (unsigned char) temp[6];
        s_alpha = temp[7];
        seuil = temp[8];
        if (temp[9] > 1.5)
            /* 07/10/2003 Olivier
               changement de l'affectation
             */

            /*typeDS = '2';
               else
               typeDS = '1';
             */
            typeDS = 2;
        else
            typeDS = 1;
        /* 07/10/2003 Olivier Ledoux
           changement de l'affectation du au changement de type
         */
        /*orientation = (unsigned char) temp[10];
         */
        orientation = (int) temp[10];
    }


    /* utilisation des variables locales */
    im = (unsigned char *) rez_image->images_table[0];
    ix = (float *) rez_image->images_table[1];
    iy = (float *) rez_image->images_table[2];
    trans = (float *) rez_image->images_table[3];
    gradx = (float *) rez_image->images_table[6];
    grady = (float *) rez_image->images_table[7];
    a0 = s_alpha;

    /* on copie l'image d'entree */
    memcpy(rez_image->images_table[0], rez_image->images_table[4],
           n * sizeof(char));

    switch (typeDS)
    {
        /* 07/10/2003 Olivier Ledoux
           remplacement des '1' par 1 et des '2' par 2
         */
    case 1:                    /*'1':              derivee premiere deriche        */
    case 2:                    /*'2':               derivee premiere shen  */

        for (i = 0; i < n; i++)
            ix[i] = iy[i] = (float) im[i];

        if (typeDS == 1)
        {                       /*'1'){          derivee premiere deriche */
            /*printf("\n\n--> Calcul de la norme du gradient par DERICHE ... \n"); */
            coeff('x', s_alpha);
            fons1(ix, nx, ny);  /* derivee suivant x  k*x*exp(-s_alpha*abs(x)) */
            if (L)
                foncy(ix, nx, ny);  /* lissage suivant x1 k*(s_alpha*abs(x) +1)*exp(-s_alpha*abs(x)) */

            coeff('y', s_alpha);
            foncy(iy, nx, ny);  /* derivee suivant x1 : k*x*exp(-s_alpha*abs(x)) */
            if (L)
                fons1(iy, nx, ny);  /* lissage suivant x2 : k*(s_alpha*abs(x) +1)*exp(-s_alpha*abs(x)) */
        }

        if (typeDS == 2)
        {                       /*'2'){          derivee premiere shen */
            /*        printf("\n\n--> Calcul de la norme du gradient par SHEN ... \n"); */
            coeff('7', s_alpha);
            if (L)
            {
                fons1(iy, nx, ny);  /* lissage  k0*exp(-s_alpha*abs(x)) suivant x2 */
                foncy(ix, nx, ny);  /* lissage k0*exp(-s_alpha*abs(x)) suivant x1 */
            }

            coeff('8', s_alpha);

            fons1(ix, nx, ny);  /*  d.p.  k0*exp(-s_alpha*abs(x)) suivant x2 */
            foncy(iy, nx, ny);  /*  d.p.  k0*exp(-s_alpha*abs(x)) suivant x1 */
        }


        if (O == 1)
        {

            printf("\n\n Le resultat est de type float !!! A VOIR\n\n");
            exit(-1);
            /*
               strcpy(NomOUT, NomImage);
               strcat       (NomOUT, "-o.lena");
             */
            orient(trans, iy, ix, n);   /* c'est bien dans cet ordre ix, iy */
            /*
               printf("--> Ouverture et Ecriture de l'image Orientation %s\n",NomOUT);
               ident.nz = sizeof(float);
               p_im = ouvre('e', &ident, NomOUT);
               ecri(&p_im, (unsigned char *)trans, ny<<2);
             */
        }

        if (Q == 1)
        {
            orient(trans, iy, ix, n);   /* c'est bien dans cet ordre ix, iy */
            amphi(ix, iy, nx, ny);
            bords(ix, BORDS, n, nx, ny);
            bords(iy, BORDS, n, nx, ny);
            SeuilOrient(seuil, ix, trans, im, n);


            /* printf("--> Ouverture et Ecriture de l'image des contours et orientations \n"); */
            /*strcpy(ident.AttribVisu, "CONTOUR"); A modifier pour en tenir compte */
            /*strcpy(NomOUT, NomImage);
               strcat       (NomOUT, "-q.lena"); */
            /*ident.nz = 1; 
               p_im=ouvre('e',&ident,NomOUT);
               ecri(&p_im,im,ny); */
        }
        /*
         * extraction des maxima locaux
         * en retour ix contient les maxima locaux, 
         * iy contient la norme du gradient
         */
        if (orientation == 1)
        {
            /* nbands = 4 -> image de float */
            ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
            for (i = 0; i < n; i++)
            {
                gradx[i] = ix[i];
                grady[i] = iy[i];
            }

        }
        else                    /* juste pour eviter le calcul dans le cas du gradiant avec orientation, Baccon 2001 */
        {
            amphi(ix, iy, nx, ny);
            bords(ix, BORDS, n, nx, ny);
            bords(iy, BORDS, n, nx, ny);

            if (Intensity == 1)
            {
                for (i = 0; i < n; ++i)
                    im[i] = (unsigned char) iy[i];


                /* printf("--> Ouverture et Ecriture de l'image de la norme \n"); */
                /*
                   strcpy(NomOUT, NomImage);
                   strcat   (NomOUT, "-i.lena");
                   ident.nz = 1;
                   p_im = ouvre ('e', &ident,NomOUT);
                   ecri (&p_im, im, ny);
                 */
            }

            if (C == 1)
            {
                Seuillage(seuil, ix, im, n);
                /* printf("--> Ouverture et Ecriture de l'image des contours \n"); */
                /*
                   strcpy(NomOUT, NomImage);
                   strcat   (NomOUT, "-c.lena");

                   ident.nz = 1;
                 */
                /*strcpy(ident.AttribVisu, "CONTOUR"); A modifier pour en tenir compte */
                /*  p_im=ouvre('e',&ident,NomOUT);
                   ecri(&p_im,im,ny); */
            }


            if (R == 1)
            {
                /*
                   strcpy(NomOUT, NomImage);    
                   strcat       (NomOUT, "-r.lena");
                   printf("--> Ouverture et Ecriture de l'image de la norme %s\n",NomOUT);

                   ident.nz =sizeof(float);
                   p_im = ouvre('e', &ident,NomOUT);
                   ecri(&p_im, (unsigned char *)iy, ny<<2);
                 */
            }

            if (M == 1)
            {

                for (i = 0; i < n; ++i)
                    im[i] = (unsigned char) ix[i];
                /*
                   strcpy(NomOUT, NomImage);        
                   strcat   (NomOUT, "-m.lena");

                   printf   ("--> Ouverture et Ecriture de l'image des maxima %s\n", NomOUT);
                   ident.nz = sizeof(char);
                   p_im = ouvre ('e', &ident,NomOUT);
                   ecri(&p_im, (unsigned char *)im, ny);
                 */
            }
        }
        break;
    }
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_calgradDS\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

#ifdef DEBUG
    printf("Je Sors de calcgradDS\n");
#endif
}
