/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
 \defgroup f_nios_give_focus_point



 \brief
 Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance
 generee par la carte de RobotSoc (NIOS)

 \ingroup libSensors

 \defgroup f_nios_give_focus_point f_nios_give_focus_point
 \ingroup libSensors

 \brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance
 generee par la carte de RobotSoc (NIOS)

 \details

 \section Description

 le groupe traite la liste des points de l'image d'entree (groupe f_nios_grab_images via la liaison algo 'image') a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
 du point sur ces deux premiers neurones. Le troisieme neurone donne l'activite (saillance) et peut etre connecte a un break (quand activite=0!);


 \file  f_give_focus_point.c
 \ingroup f_give_focus_point
 \brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: N. Cuperlier
 - description: specific file creation
 - date: 06/11


 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe
 a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
 du max sur ces deux premiers neurone. Lorsque tous les points sont traites, le troisieme neurone s'active et peut etre connecte a un break;

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG

#include <Typedef/liste_chaine.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Struct/nios_pt_carac.h>
#include <public_tools/Vision.h>

typedef struct MyData_f_give_focus_point_from_saved_data {
   int gpe_image;
   int gpe_memory;
   int gpe_transition_detect;
   int gpe_transition_detect_lieu;
   int gpe_head;
   int index_pt;
   int nb_pt;
   int iter; /* numero d'image, utilisee pour modifier le nom du fichier de keypoint correspondant*/
   char name[255];
   char nameRhoTheta[255];
   int sx;
   int sy;
   FILE *fd; /* for the n best keypoints in a same scale and a same image*/
   FILE *fdt;
   prom_NIOS_pt_carac pts_carac;
   int rhosy;/*taille imagette*/
   int rhosx;
} MyData_f_give_focus_point_from_saved_data;

void new_give_focus_point_from_saved_data(int gpe)
{
   MyData_f_give_focus_point_from_saved_data *my_data;
   int gpe_head = -1, gpe_memory = -1;
   int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
   int i, l, sx = -1, sy = -1, f = -1, nb_pt = -1, rhosx = -1, rhosy = -1;
   char param[255];
   char name[255] = "toto";
   if (def_groupe[gpe].nbre < 4)
   {
      EXIT_ON_ERROR("(new_give_focus_point_from_saved_data) Group size must be 4");
   }

   i = 0;
   l = find_input_link(gpe, i);
   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "transition_detect") == 0) gpe_transition_detect = liaison[l].depart;

      if (strcmp(liaison[l].nom, "transition_detect_lieu") == 0) gpe_transition_detect_lieu = liaison[l].depart;

      if (strcmp(liaison[l].nom, "head") == 0) gpe_head = liaison[l].depart;
      if (strcmp(liaison[l].nom, "memory") == 0) gpe_memory = liaison[l].depart;

      if (prom_getopt(liaison[l].nom, "f", param) >= 2)
      {
         sprintf(name, "%s", param);
         f = 1;
      };
      if (sx < 0) prom_getopt_int(liaison[l].nom, "-sx", &sx);
      if (sy < 0) prom_getopt_int(liaison[l].nom, "-sy", &sy);
      if (rhosx < 0) prom_getopt_int(liaison[l].nom, "-rhosx", &rhosx); /*taille imagette rho-theta*/
      if (rhosy < 0) prom_getopt_int(liaison[l].nom, "-rhosy", &rhosy);
      if (nb_pt < 0) prom_getopt_int(liaison[l].nom, "-nb_pt", &nb_pt);
      i++;
      l = find_input_link(gpe, i);
   }
   if (sx < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing image size sx");
   if (sy < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing image size sy");
   if (rhosx < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing rho-theta image size sx");
   if (rhosy < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing rho-theta image size sy");
   if (f < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing -f data file name");
   if (nb_pt < 0) EXIT_ON_ERROR("new_give_focus_point_from_saved_data missing -nb_pt: number of keypoints to find at max in a single image");
   if (gpe_memory == -1) EXIT_ON_ERROR("(new_give_focus_point_from_saved_data) Missing memory link\n");

   if (gpe_transition_detect == -1) EXIT_ON_ERROR("(new_give_focus_point_from_saved_data) Missing transition_detect link\n");

   if (gpe_transition_detect_lieu == -1 || gpe_head == -1)
   {
      fprintf(stderr, "WARNING in (new_give_focus_point_from_saved_data) : No head or transition_detect_lieu link -> memory_focus_point will not be used\n");
   }

   my_data = (MyData_f_give_focus_point_from_saved_data *) malloc(sizeof(MyData_f_give_focus_point_from_saved_data));
   my_data->gpe_memory = gpe_memory;
   my_data->gpe_transition_detect = gpe_transition_detect;
   my_data->gpe_transition_detect_lieu = gpe_transition_detect_lieu;
   my_data->gpe_head = gpe_head;
   my_data->sx = sx;
   my_data->sy = sy;
   my_data->rhosx = rhosx;
   my_data->rhosy = rhosy;
   strcpy(my_data->name, name);
   my_data->nb_pt = nb_pt;
   /* my_data->pts_carac=(prom_NIOS_pt_carac *)malloc(nb_pt*sizeof(prom_NIOS_pt_carac ));*/
   /*allocation de l'image rho-theta*/
   my_data->pts_carac.rho_teta_image = (prom_images_struct*) calloc_prom_image(1, 16, 16, 4);
   def_groupe[gpe].ext =(prom_images_struct*) calloc_prom_image(1, 16, 16, 4);
   my_data->fd = NULL;
   my_data->fdt = NULL;
   my_data->iter=0;/*pour HAkim le premier fichier commence a 2...*/
   def_groupe[gpe].data = my_data;
}

void function_give_focus_point_from_saved_data(int gpe)
{
   MyData_f_give_focus_point_from_saved_data *my_data;
   int gpe_head = -1, gpe_memory = -1;
   int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
   int x, y, sx, sy, read, rhosy, rhosx;
   float z;
   float head_h, head_v;
   int x_shifted, y_shifted;
   int index_pt = 0;
   int nb_pt = 0;
   char name[255];
   FILE *fd; /* for the n best keypoints in a same scale and a same image*/
   FILE *fdt; /*for rho-theta*/

   printf("function_give_focus_point_from_saved_data(%s): Entering function\n", def_groupe[gpe].no_name);

   /*recupere les donnees du groupe*/
   if (def_groupe[gpe].data == NULL) EXIT_ON_ERROR("(function_give_focus_point_from_saved_data) Error retrieving data (NULL pointer)\n");
   my_data = (MyData_f_give_focus_point_from_saved_data *) def_groupe[gpe].data;
   gpe_transition_detect = my_data->gpe_transition_detect;
   gpe_transition_detect_lieu = my_data->gpe_transition_detect_lieu;
   gpe_head = my_data->gpe_head;
   index_pt = my_data->index_pt; /* index du pt crac courant*/
   nb_pt = my_data->nb_pt;
   sx = my_data->sx;/*((prom_images_struct *) def_groupe[gpe_image].ext)->sx;*/
   sy = my_data->sy;/*((prom_images_struct *) def_groupe[gpe_image].ext)->sy;*/
   rhosy = my_data->rhosy;
   rhosx = my_data->rhosx;
   /* traitement d'une nouvelle image*/
   if (isequal(neurone[def_groupe[gpe_transition_detect].premier_ele].s2, 1.))
   {
      printf("f_give_focus_point_from_saved_data(%s): transition detect -> reset index_pt my_data->iter=%d\n", def_groupe[gpe].no_name, my_data->iter);
      /*new image RAZ de l'index: */
      index_pt = my_data->index_pt = 0;

      /*TODO ouvrir le fichier XXXX_n contenant les n meilleurs points trouves sur l'image n */
      if (my_data->fd != NULL) fclose(my_data->fd);
      /*  sprintf(my_data->name, "%s%06i", my_data->name, my_data->iter);*/
     
      sprintf(name, "%s%i", my_data->name, my_data->iter);
      printf("%s\n", name);
      fd = fopen(name, "r");
      if (fd == NULL)
      {
         EXIT_ON_ERROR("ERROR in f_give_focus_point_from_saved_data(%s): Cannot open file %s -> %s\n", def_groupe[gpe].no_name, my_data->name, name);
      }
      my_data->fd = fd;
      my_data->iter++;
      /*  if (feof(fd))
       {
       rewind(fd);
       dprints("function_give_focus_point_from_saved_data(%s): End of file reached, rewinding : maybe a problem...\n", def_groupe[gpe].no_name);
       }*/

   }
   else
   {
      fd = my_data->fd;
   }

   /* TODO tester si encore des pts a traiter*/
   if (index_pt < nb_pt)
   {
      /*recupere les infos x, y, act et index pour mapping sur neurone...*/
      /* Lecture et parsage d'une ligne
       *  en-tete du fichier:
       *   posx    posy    act    nom_fichier_rho_theta
       * */
      read = fscanf(fd, "%d", &(my_data->pts_carac.posy));
      printf("%d\n", my_data->pts_carac.posy);
      /*if (read != 1) EXIT_ON_ERROR("ERROR in function_give_focus_point_from_saved_data(%s): keypoint posy reading failed\n", def_groupe[gpe].no_name);*/
      read = fscanf(fd, "%d", &x);
      printf("%d\n", x);
      (my_data->pts_carac.posx) = x;
      /*if (read != 1) EXIT_ON_ERROR("ERROR in function_give_focus_point_from_saved_data(%s): keypoint posx reading failed\n", def_groupe[gpe].no_name);*/
      read = fscanf(fd, "%f", &(my_data->pts_carac.intensity));
      printf("%f\n", my_data->pts_carac.intensity);
    /*  if (read != 1) EXIT_ON_ERROR("ERROR in function_give_focus_point_from_saved_data(%s): keypoint activity reading failed\n", def_groupe[gpe].no_name);*/
      read = fscanf(fd, "%s", my_data->nameRhoTheta);
      printf("%s\n", my_data->nameRhoTheta);
     /* if (read != 1) EXIT_ON_ERROR("ERROR in function_give_focus_point_from_saved_data(%s): keypoint activity reading failed\n", def_groupe[gpe].no_name);*/
      read = fscanf(fd, "\n");

      /*Chargement de l'imagette Rhotheta
       * **/
      printf("openning rhotheta file: %s\n", my_data->nameRhoTheta);
      fdt = fopen(my_data->nameRhoTheta, "r");
      if (fdt == NULL)
      {
         EXIT_ON_ERROR("ERROR in f_give_focus_point_from_saved_data(%s): Cannot open file %s\n", def_groupe[gpe].no_name, my_data->nameRhoTheta);
      }
      printf("reading rhotheta file: %s\n", my_data->nameRhoTheta);
      for (x = 0; x < rhosy * rhosx; x++)
      {
         read = fscanf(fdt, "%f", &z);
         /*       printf("%f\n",z);*/
        ((float*) (my_data->pts_carac.rho_teta_image->images_table[0]))[x] = z;
        printf("%f %f\n",z, ((float*) (my_data->pts_carac.rho_teta_image->images_table[0]))[x]);
         /*   if (read != 1 )EXIT_ON_ERROR("ERROR in function_give_focus_point_from_saved_data(%s): Rho Theta image reading failed for point= %d\n", def_groupe[gpe].no_name,index_pt);*/
      }
      fclose(fdt);
      printf("reading rhotheta file: done\n");
      x = my_data->pts_carac.posx;
      y = my_data->pts_carac.posy;
      head_h = neurone[def_groupe[gpe_head].premier_ele].s;
      head_v = neurone[def_groupe[gpe_head].premier_ele + 1].s;
      if (gpe_transition_detect_lieu >= 0 && gpe_memory >= 0 && def_groupe[gpe_memory].ext != NULL && gpe_head >= 0 && isdiff(neurone[def_groupe[gpe_transition_detect_lieu].premier_ele].s, 1.0))
      {
         printf("suite traitement image %d, points numero %d\n", my_data->iter, my_data->index_pt);
         x_shifted = head_h * ((prom_images_struct *) def_groupe[gpe_memory].ext)->sx;
         y_shifted = head_v * ((prom_images_struct *) def_groupe[gpe_memory].ext)->sy;

         x_shifted = x_shifted + sx * (((float) x) / sx - 0.5);
         y_shifted = y_shifted + sy * (((float) y) / sy - 0.5);

         if (x_shifted < 0) x_shifted = x_shifted + sx;
         else if (x_shifted > sx) x_shifted = x_shifted - sx;

         if (y_shifted < 0) y_shifted = y_shifted + sy;
         else if (y_shifted > sy) y_shifted = y_shifted - sy;

         /* On fait la multiplication de l'intensite du point de foca par le masque garde dans memory permettant de savoir si le point a deja
          ete traite dans ce panorama (recouvrement avec une autre image du meme panorama) */
         z = (((prom_images_struct *) def_groupe[gpe_memory].ext)->images_table[0][x_shifted + y_shifted * ((prom_images_struct *) def_groupe[gpe_memory].ext)->sx] / 255) * my_data->pts_carac.intensity;

         if (z > 0) printf("f_give_focus_point_from_saved_data(%s): New point using memory,  index=%d -> x=%d, y=%d,x_act=%f,y_act=%f, head=%f, x_shifted=%d, y_shifted=%d \n", def_groupe[gpe].no_name, index_pt, x, y, ((float) x) / sx, ((float) y) / sy, head_h, x_shifted, y_shifted);
         else printf("f_give_focus_point_from_saved_data(%s): this point already exists in memory (same panorama),  index=%d -> x=%d, y=%d,x_act=%f,y_act=%f, head=%f, x_shifted=%d, y_shifted=%d \n", def_groupe[gpe].no_name, index_pt, x, y, ((float) x)
               / sx, ((float) y) / sy, head_h, x_shifted, y_shifted);

      }
      else
      {
         z = my_data->pts_carac.intensity;
         printf("f_give_focus_point_from_saved_data(%s): New point from new panorama index=%d -> x=%d, y=%d, head=%f, \n", def_groupe[gpe].no_name, index_pt, x, y, head_h);

      }

      /* Mise a jour des activites de la boite avec les coordonnees du point gagnant */

      neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = x / (float) sx;/*((point3d *) maillon_fort->data)->x;*/

      neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = y / (float) sy;/*((point3d *) maillon_fort->data)->y;*/

      neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = z; //0.;
      if (def_groupe[gpe].nbre > 3) neurone[def_groupe[gpe].premier_ele + 3].s = neurone[def_groupe[gpe].premier_ele + 3].s1 = neurone[def_groupe[gpe].premier_ele + 3].s2 = index_pt;/*((point3d *) maillon_fort->data)->z/255.0;*/

     def_groupe[gpe].ext = my_data->pts_carac.rho_teta_image;
      my_data->index_pt = ++index_pt;

   }
   else
   {
      printf("f_give_focus_point_from_saved_data(%s): No more points\n", def_groupe[gpe].no_name);

      neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = 0.;
      index_pt = my_data->index_pt = 0;

   }

   printf("f_give_focus_point_from_saved_data(%s): Leaving function\n", def_groupe[gpe].no_name);

}

void destroy_give_focus_point_from_saved_data(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      fclose(((MyData_f_give_focus_point_from_saved_data*) def_groupe[gpe].data)->fd);
      fclose(((MyData_f_give_focus_point_from_saved_data*) def_groupe[gpe].data)->fdt);
      free(((MyData_f_give_focus_point_from_saved_data*) def_groupe[gpe].data)->pts_carac.rho_teta_image);
      free(((MyData_f_give_focus_point_from_saved_data*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
}
