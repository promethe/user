/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include <string.h>

#include "tools/include/init_look_up_polaire.h"

typedef struct My_Data_f_NIOS_rho_theta
{
    int GpeIm;
    int GpeIndex;
} My_Data_f_NIOS_rho_theta;


/**
 * Projection de l'image d'entree sur des neurones
 *
 */
void function_NIOS_rho_theta(int gpe_sortie)
{
    unsigned char *Image = NULL;
    int GpeIndex, GpeIm;
    int i, deb, Index;
    int TailleGpe=16*16; /*en dur pour le moment... TODO a modifier*/
    int nb_band;
    prom_images_struct  *ImageEntre = NULL;


#ifdef DEBUG
#ifndef AVEUGLE
    TxPoint point,point1;
    int couleur;
    static prom_images_struct * im_rec=NULL;
    static int * ind_rec;
#endif
#endif

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

#ifdef DEBUG
    printf("---- function_rho_theta ----\n");
#endif
    if (def_groupe[gpe_sortie].data == NULL)
    {
        /* test de la taille */
        if (def_groupe[gpe_sortie].taillex != def_groupe[gpe_sortie].tailley)
        {
            printf("Le groupe % doit etre une matrice carree\a\n",
                   gpe_sortie);
            printf("Exit in function_rho_theta\n");
            exit(-1);
        }

        /* recuperation info */
         GpeIndex = GpeIm = -1;
        for (Index = 0; Index < nbre_liaison; Index++)
            if (liaison[Index].arrivee == gpe_sortie)
            {
                if (!strcmp(liaison[Index].nom, "ext"))
                    GpeIm = liaison[Index].depart;
                if (!strcmp(liaison[Index].nom, "index"))
                    GpeIndex = liaison[Index].depart;
            }
        if ( (GpeIm == -1)||(GpeIndex == -1))
        {
            printf
                ("Le groupe % doit etre connecte a 2 autres groupes avec les liens <Im> <index>\n",
                 gpe_sortie);
            printf("Exit in function_rho_theta\n");
            exit(-1);
        }


        if (def_groupe[GpeIm].ext == NULL)
        {
            printf("L'ext du groupe d'entre est nulle\n");
            printf("Exit in function_rho_theta\n");
            return;
        }
        ImageEntre = (prom_images_struct *) def_groupe[GpeIm].ext;
        nb_band = ImageEntre->nb_band;
        Image = ImageEntre->images_table[0];

        if ( nb_band != 4)
        {
            printf("nb_band non gere par f_rho_theta gpe %d\n", gpe_sortie);
            exit(1);
        }


        def_groupe[gpe_sortie].data =
            (void *) malloc(sizeof(My_Data_f_NIOS_rho_theta));
        if (def_groupe[gpe_sortie].data == NULL)
        {
            printf("Not enought memory!\n");
            printf("Exit in function_rho_theta\n");
            exit(-1);
        }
        ((My_Data_f_NIOS_rho_theta* ) def_groupe[gpe_sortie].data)->GpeIndex=GpeIndex;
        ((My_Data_f_NIOS_rho_theta *) def_groupe[gpe_sortie].data)->GpeIm=GpeIm;

    }
    else
    {
        GpeIm=    ((My_Data_f_NIOS_rho_theta *) def_groupe[gpe_sortie].data)->GpeIm;
        printf("gp ima=%d, pt=%p\n",GpeIm,(float*) (((prom_images_struct *) def_groupe[GpeIm].ext)->images_table[0]));
        Image =  ((prom_images_struct *) def_groupe[GpeIm].ext)->images_table[0];
    }



    deb = def_groupe[gpe_sortie].premier_ele;
    TailleGpe = def_groupe[gpe_sortie].taillex * def_groupe[gpe_sortie].tailley;
    for(i=0;i<TailleGpe/*16*16*/;i++)
     {
    	neurone[deb+i].s=neurone[deb+i].s1=neurone[deb+i].s2= *((float*) Image+i);
     }


}

