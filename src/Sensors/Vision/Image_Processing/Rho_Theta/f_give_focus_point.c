/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

/** 
\defgroup f_sequential_winner f_sequential_winner
\deprecated
VOIR f_give_focus_points

\brief
DEPRECATED - Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance

\ingroup libSensors

\defgroup f_give_focus_point f_give_focus_point
\ingroup libSensors

\brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance

\details

\section Description

    le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe
    a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
    du max sur ces deux premiers neurone. Lorsque tous les points sont traites, le troisieme neurone s'active et peut etre connecte a un break;


    \file  f_give_focus_point.c
  \ingroup f_give_focus_point
    \brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance

    Author: xxxxxxxx
    Created: XX/XX/XXXX
    Modified:
    - author: C.Giovannangeli
    - description: specific file creation
    - date: 02/05

    - author: J. Hirel
    - description: clarification, memory management
    - date: 11/2008

    Theoritical description:
    - \f$  LaTeX equation: none \f$

    Description:
    le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe
    a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
    du max sur ces deux premiers neurone. Lorsque tous les points sont traites, le troisieme neurone s'active et peut etre connecte a un break;

    Il est est possible d'avoir une zone d'exclusion sur l'image traitee en entree. Ces zonnes peuvent etre definies directement sur un lien entrant avec les options
     -Xmin -Xmax -Ymin -Ymax ou bien chargees a partir du groupe precedent relie par un lien RE. Le groupe RE doit contenir 4 neurones [Xmin Xmax Ymin Ymax].

 ***** Habituation *****
	Un point d'intérêt resté dans une zone IC durant IH itération ne sera plus traité pendant ID itérations. Activée via l'une des options suivantes :
	-IHxxx : Active l'habituation et permet de spécifier le nombre d'itération avant qu'il y ait eu habituation d'un point d'intérêt (défaut : 5).

	-IDxxx : Active l'habituation et permet de spécifier le nombre d'itération avant qu'un point d'intérêt habituel, et donc inhibé, ne soit plus inhibé (défaut : 15).

	-ICxxx : Active l'habituation et permet de spécifier la moitié du coté (en pixel) du carré autour du point d'intérêt qui sera traité au même titre que lui (défaut : 2, soit un carré de 4*4 pixels).

	-Lien avec juste "habituation" : Active l'habituation quand le neurone d'entré est > 0.5

	Rmq :	- Il faut que ID > IH pour obtenir des résultats.
			- Le nombre de vignettes supplémentaires qui pourront être vues est fonction de ID / IH.
			- A priori, si ID est faible l'hystérésis à donné à la reconnaissance d'un objet pourra être plus faible (car les vignettes seront vues plus souvent)
			- Influence quasi nulle sur le framerate. Permet même de faire diminuer le nombre de vignettes vues dans la boucle (et donc d'augmenter le framerate).
			- Si IH = 1 et que l'échelle de temps vaut n, alors on inhibe ID*n points d'intérêts en ID itérations.

 ***** Play *****
	Activable en mettant 2 boites en entrée :
	- L'une avec un lien "confidence" contenant un neurone indiquant l'indice de confiance accordé au point d'intérêt précédemment traité
		(exemple : un lien secondaire sortant d'une boite donnant le taux de reconnaissance d'un objet si c'est bien l'objet reconnu, et si le LMS n'a reconnu que cet objet là (pas d'ambiguité).
	- L'autre avec un lien "play" contenant 2 neurones :
	 		- le 1er (bouton "Jouer") servant à lancer le jeu : le point d'intérêt votant pour l'objet en cours dont la confiance est la plus élevé est mis en valeur, puis d'autres s'ils sont proches et au delà d'un seuil.
			- le 2ème  (bouton "oui") à dire que la reconnaissance a réussi
	=> Si l'on dit "oui" (on pourra penser à une manipulation qui viendra donner ce signal) le 5ème neurone sera à 1 pour les points uniquement reconnu comme sûrs
		=> Ce qui pourra servir pour accroitre le learning rate sur ces points et ainsi accélérer considérablement l'apprentissage.

	-Pcxxx : Définit la moitié du coté (en pixel) du carré autour du point d'intérêt qui sera traité au même titre que lui ( play_cote par défaut = 2, soit un carré de 4*4 pixels).
	-PCxxx : Définit la distance autour de laquelle de nouveaux points d'intérêts pourront être recrutés s'ils dépassent le seuil ( play_prox par défaut = 10, soit un carré de 20*20 pixels).
	-PTxxx : Définit le seuil de confiance au delà duquel la reconnaissance est suffisamment sûre pour être proposée (défaut : 0.5 * valMax)

	Fonctionnement :
	- Dans un 1er temps on parcourt pendant iteDesappear fois tous les pts d'intérêts, à la recherche du max (= valMax).     ((((((((( avant : dépassant un seuil de confiance PTxxx ))))))))).
	- Une fois le max obtenu, on créé des carrés de Pcxxx pour le bruit et de PCxxx pour la zone de proximité dans une image (valeur de 250 et 50 respectivement).
	- Par la suite, tout point d'intérêt dépassant le seuil de confiance PTxxx (par défaut =  0.15 mais après moyenne coulante s'adaptant au plus basse valeurs) situé dans une zone de proximité créé un carré identique.
	- Si le signal "oui" est activé, le 5ème neurone est mis à un sur les points d'intérêts situé sur une zone à 250.

 ***** Local tracking *****
	-	Quand un point d'intérêt est reconnu avec une certitude > -PTxxx au cours d'une boucle, la prochaine boucle se fera que
		dans un voisinage du point de ce type qui aura la valeur max au cours d'une boucle.
	-	Le 6ème neurone vaut alors 1 pendant la boucle où on ne regarde que le voisinage. Peut servir à empêcher le recrutement du SAW (voire à baisser sa vigilence)

 ***** Ambiguity *****
	Activable en mettant la sortie du LMS (avec le dernier neurone "d'inintérêt") avec un lien secondaire et "ambiguity"
	-ARxxx : Définit le ratio entre des zones d'influences d'objets différents, au delà duquel un point d'intérêt dans une zone "faible" sera ignoré. (défaut = 5.)

	Fonctionnement :
	- On regarde la carte qui a la valeur max au point traité
	- Si un neurone reconnait un objet et que ça carte n'est pas max, si le ratio entre la carte max et celle de l'objet reconnu > ARxxx alors
	le neurone correspondant (premier_ele + 6 + neurone_de_l'objet) vaut -valeur courante (qu'on ajoutera donc au vote, après soustraction de l'inintérêt pour annuler l'effet du LMS.)



 ***** Neurones utilisés *****
	- 0 -> 2  : COORD x, y +  BREAK z, plus de points d'intérêts
	- 3 : VALEUR z normalisée sur 255
	- 4 : PLAY + Oui => neurone à 1 uniquement pour les points reconnus comme sûrs
	- 5 : LOCAL TRACKING : neurone vaut alors 1 pendant la boucle où on ne regarde que le voisinage. Peut servir à empêcher le recrutement du SAW (voire à baisser sa vigilence)
	- 6 -> ? (neurone_de_l'objet) : AMBIGUITÉ : = -valeur précédente de vote du LMS lorsque la ratio d'AMBIGUITÉ est dépassé.



    Macro:
    -none

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools:
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG*/

#include <Typedef/liste_chaine.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <public_tools/Vision.h>

typedef struct MyData_f_give_focus_point
{
   int gpe_image;
   int gpe_transition_detect;
   int gpe_transition_detect_lieu;
   int gpe_head;
   int gpe_memory;
   int gpe_play;
   int gpe_confidence;
   int gpe_ambiguity;
   int gpe_re;
   int gpe_habituation;
   int gpe_play_threshold;
   int Xmin;
   int Xmax;
   int Ymin;
   int Ymax;
   liste_chaine *liste_maillon_resolu;
   liste_chaine *liste_maillon_a_resoudre;
   int habituation;
   int iteHabituation;
   int iteDesappear;
   int icc;
   int play;
   int is_playing;
   int max_detected;
   int x_prev;
   int y_prev;
   int x_local_track;
   int y_local_track;
   float max_local_track;
   int is_local_tracking;
   int x_max;
   int y_max;
   float confiance_max;
   int itePlay;
   int play_cote;
   int play_prox;
   float play_conf_threshold;
   int ambiguity;
   int nb_objects;
   float ambiguity_ratio;
   prom_images_struct *prom_OutputImage;//
   unsigned char **ima_output_ambiguity;
   float *current_objects_value;

   /* int nbPtInteretsInhib; PR DEBUG */
   /* int nbIte; PR DEBUG */
   int image_ext;
} MyData_f_give_focus_point;


void new_give_focus_point(int gpe)
{
   MyData_f_give_focus_point *my_data;
   int gpe_image = -1, gpe_head = -1, gpe_memory = -1, gpe_re = -1, gpe_play = -1, gpe_confidence = -1, gpe_play_threshold = -1;
   int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
   int i, l, Xmin = 0, Xmax = 0, Ymin = 0, Ymax = 0;
   char param[32];
   int habituation = 0, iteHabituation = 5, iteDesappear = 15, icc = 2, gpe_habituation = -1;
   int play = 0, play_cote = 2, play_prox = 10;
   float play_conf_threshold = 0.15, ambiguity_ratio = 5.;
   int ambiguity = 0, gpe_ambiguity = -1, nb_objects = 0, image_ext = 0;
   unsigned char **ima_output_ambiguity = NULL;
   float *current_objects_value = NULL;


   if (def_groupe[gpe].nbre < 3)
   {
      EXIT_ON_ERROR("Group size must be 3 at least");
   }

   i = 0;
   l = find_input_link(gpe, i);
   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "transition_detect") == 0)
         gpe_transition_detect = liaison[l].depart;

      if (strcmp(liaison[l].nom, "transition_detect_lieu") == 0)
         gpe_transition_detect_lieu = liaison[l].depart;

      if (strstr(liaison[l].nom, "image") != NULL)
      {
         gpe_image = liaison[l].depart;
         if (prom_getopt(liaison[l].nom, "-EXT", param) == 2) image_ext = atoi(param);
         //EXIT_ON_ERROR("in %s[%s]: Liaison ok vers image EXT : %d",  __FUNCTION__,def_groupe[gpe].no_name, image_ext);
         else image_ext = 0;
         //EXIT_ON_ERROR("in %s[%s]: Liaison ok vers image EXT par défaut : %d",  __FUNCTION__,def_groupe[gpe].no_name, image_ext);
      }


      if (strcmp(liaison[l].nom, "head") == 0)
         gpe_head = liaison[l].depart;

      if (strcmp(liaison[l].nom, "memory") == 0)
         gpe_memory = liaison[l].depart;

      if (strcmp(liaison[l].nom, "RE") == 0){
         gpe_re = liaison[l].depart;
         if( def_groupe[gpe_re].nbre < 4 )
            EXIT_ON_ERROR("in %s[%s]: La boite Rayon Exclusion (lien \"RE\") ne contient pas 4 neurones !\n",  __FUNCTION__,def_groupe[gpe].no_name );
      }

      if (prom_getopt(liaison[l].nom, "-Xmin", param) == 2)	Xmin = atoi(param);
      if (prom_getopt(liaison[l].nom, "-Ymin", param) == 2)	Ymin = atoi(param);
      if (prom_getopt(liaison[l].nom, "-Xmax", param) == 2)	Xmax = atoi(param);
      if (prom_getopt(liaison[l].nom, "-Ymax", param) == 2)	Ymax = atoi(param);

      if (prom_getopt(liaison[l].nom, "-IH", param) == 2)
      {
         iteHabituation = atoi(param);
         if(iteHabituation < 1) iteHabituation = 1;
         habituation = 1;
         dprints("-IH = %f\n", iteHabituation);
      }
      if (prom_getopt(liaison[l].nom, "-ID", param) == 2){
         iteDesappear = atoi(param);
         habituation = 1;
         dprints("-ID = %f\n", iteDesappear);
      }
      if (prom_getopt(liaison[l].nom, "-IC", param) == 2){
         icc = atoi(param);
         habituation = 1;
         dprints("-IC = %f\n", icc);
      }

      if (strcmp(liaison[l].nom, "habituation") == 0)
      {
         gpe_habituation = liaison[l].depart;
         habituation = 1;
      }

      if( habituation ){
         dprints("Habituation activated\n"
               "Itération avant habituation :\t %d\n"
               "Itération avant réapparition :\t %d\n"
               "Coté du carré autour des points d'intérêt (pix) : %d", iteHabituation, iteDesappear, icc*2);
         if(iteDesappear < iteHabituation){
            iteDesappear = iteHabituation;
            iteDesappear = fmax(iteHabituation,2);
            fprintf(stderr, "WARNING in %s[%s]: For good results you should take a value of ID > IH ! So ID = %d at least\n",  __FUNCTION__,def_groupe[gpe].no_name, iteHabituation);
         }
      }
      if (strcmp(liaison[l].nom, "confidence") == 0)
         gpe_confidence = liaison[l].depart;
      if (strcmp(liaison[l].nom, "play") == 0)
         gpe_play = liaison[l].depart;

      if (prom_getopt(liaison[l].nom, "-Pc", param) == 2)
      {
         play_cote = atoi(param);
         dprints("-Pc = %f\n", play_cote);
      }
      if (prom_getopt(liaison[l].nom, "-PC", param) == 2){
         play_prox = atoi(param);
         dprints("-PC = %f\n", play_prox);
      }
      if (prom_getopt(liaison[l].nom, "-PT", param) == 2){		// NON UTILISE car valeur = 0.5 * valMax dès qu'on "joue" une fois.
         play_conf_threshold = atof(param);
         dprints("-PT = %f\n", play_conf_threshold);
      }
      if (prom_getopt(liaison[l].nom, "-AR", param) == 2){
         ambiguity_ratio = atof(param);
         if( ambiguity_ratio < 1. )	ambiguity_ratio = 1.;
         dprints("-AR = %f\n", ambiguity_ratio);
      }
      if (strcmp(liaison[l].nom, "ambiguity") == 0){
         gpe_ambiguity = liaison[l].depart;
         ambiguity = 1;
         nb_objects = def_groupe[gpe_ambiguity].nbre;
      }
      if (strcmp(liaison[l].nom, "play_threshold") == 0)
      {
         gpe_play_threshold = liaison[l].depart;
         play_conf_threshold = neurone[def_groupe[gpe_play_threshold].premier_ele].s1;
         dprints("-PT canceled, replaced by neurone on link play_threshold = %f\n", play_conf_threshold);
      }


      i++;
      l = find_input_link(gpe, i);
   }


   if( gpe_confidence != -1 || gpe_play != -1 )
   {
      if( gpe_confidence == -1 || gpe_play == -1 )
         fprintf(stderr, "WARNING in %s[%s]: To be enable, the playing functionality should have 2 entry, the previous confidence and the play neuron\n",  __FUNCTION__,def_groupe[gpe].no_name);
      else
      {
         play = 1;

         if (def_groupe[gpe].nbre < 5)
         {
            EXIT_ON_ERROR("in %s[%s]: Group size must be 5 at least with the play option",  __FUNCTION__,def_groupe[gpe].no_name);
         }
      }
   }




   if( ambiguity )
   {
      if( !play )
      {
         ambiguity = 0;
         nb_objects = 0;
         fprintf(stderr, "WARNING in %s[%s]: To be enable, the ambiguity functionality requires the playing functionality\n",  __FUNCTION__,def_groupe[gpe].no_name);
      }
      else
      {
         if (def_groupe[gpe].nbre < 5+nb_objects)
         {
            EXIT_ON_ERROR("in %s[%s]: Group size must be = (5 + number of neurons in the box ambiguity) at least with the ambiguity option",  __FUNCTION__,def_groupe[gpe].no_name);
         }

         //ima_output_ambiguity = (unsigned char **)malloc(nb_objects*sizeof(unsigned char *));
         ima_output_ambiguity = MANY_ALLOCATIONS(nb_objects, unsigned char * );

         //current_objects_value = (float*)malloc((nb_objects -1) * sizeof(float));
         current_objects_value = MANY_ALLOCATIONS((nb_objects -1), float);
      }
   }

   if (gpe_image == -1)
      EXIT_ON_ERROR("Missing image link\n");

   if (gpe_transition_detect == -1)
      EXIT_ON_ERROR("Missing transition_detect link\n");


   if (gpe_transition_detect_lieu == -1 || gpe_memory == -1 || gpe_head == -1)
   {
      fprintf(stderr,
            "WARNING in new_focus_point(%s): No memory, head or transition_detect_lieu link -> memory_focus_point will not be used\n",
            def_groupe[gpe].no_name);
   }


   my_data = ALLOCATION(MyData_f_give_focus_point);
   my_data->gpe_image = gpe_image;
   my_data->gpe_transition_detect = gpe_transition_detect;
   my_data->gpe_transition_detect_lieu = gpe_transition_detect_lieu;
   my_data->gpe_head = gpe_head;
   my_data->gpe_memory = gpe_memory;
   my_data->gpe_re = gpe_re;
   my_data->Xmin = Xmin;
   my_data->Xmax = Xmax;
   my_data->Ymin = Ymin;
   my_data->Ymax = Ymax;
   my_data->liste_maillon_resolu = NULL;
   my_data->liste_maillon_a_resoudre = NULL;
   my_data->habituation = habituation;
   my_data->iteDesappear = iteDesappear;
   my_data->iteHabituation = iteHabituation;
   my_data->icc = icc;
   my_data->gpe_habituation = gpe_habituation;
   my_data->gpe_play = gpe_play;
   my_data->gpe_confidence = gpe_confidence;
   my_data->play = play;
   my_data->is_playing = 0;
   my_data->max_detected = 0;
   my_data->x_prev = -1;
   my_data->y_prev= -1;
   my_data->x_local_track = -1;
   my_data->y_local_track= -1;
   my_data->max_local_track= -1;
   my_data->is_local_tracking= 0;
   my_data->x_max= -1;
   my_data->y_max= -1;
   my_data->confiance_max= -1;
   my_data->itePlay = 0;
   my_data->play_cote = play_cote;
   my_data->play_prox = play_prox;
   my_data->play_conf_threshold = play_conf_threshold;
   my_data->gpe_play_threshold = gpe_play_threshold;
   my_data->ambiguity = ambiguity;
   my_data->gpe_ambiguity = gpe_ambiguity;
   my_data->nb_objects = nb_objects;
   my_data->ambiguity_ratio = ambiguity_ratio;

   my_data->prom_OutputImage=NULL;//
   /* mmy_data->nbPtInteretsInhib = 0; PR DEBUG */
   /* my_data->nbIte = 0; PR DEBUG */
   my_data->image_ext = image_ext;
   my_data->ima_output_ambiguity = ima_output_ambiguity;
   my_data->current_objects_value = current_objects_value;

   def_groupe[gpe].data = my_data;
}


void function_give_focus_point(int gpe)
{
   MyData_f_give_focus_point *my_data;
   int gpe_image = -1, gpe_head = -1, gpe_memory = -1, gpe_re = -1;
   int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
   int i, x, y, z, sx, sy, sx_memory, sy_memory;
   liste_chaine *liste_maillon_resolu = NULL;
   liste_chaine *liste_maillon_a_resoudre = NULL;
   liste_chaine *maillon_temp = NULL;
   liste_chaine *maillon_parcours = NULL;
   liste_chaine *avant_maillon_parcours = NULL;
   point3d *thepoint = NULL;
   liste_chaine *maillon_fort;
   float head_h, head_v;
   int x_shifted, y_shifted;
   int Xmin,Xmax,Ymin,Ymax;
   int habituation = 0, icc, inhib = 1, gpe_habituation = -1, gpe_play_threshold = -1;
   unsigned char iteHabituation, iteDesappear;
   int xInhib, yInhib;
   prom_images_struct *prom_OutputImage = NULL;//
   unsigned char *ima_output_inhib = NULL;//
   unsigned char *ima_output_play = NULL;//
   unsigned char **ima_output_ambiguity = NULL;//
   int nb_imaOut = 0;
   int allInhib = 0;
   int play = 0, max_just_detected = 0;
   int ambiguity = 0, nb_objects = 0, confident_recog = -1, found_objects = 0, winner_map = -1, j = 0, image_ext = 0;
   float n_ambi_ininteresting = 0.;
   float *current_objects_value = NULL;
   int max1, arr = 0;
   float ratio, conf_obj_max = 0., conf_obj_cur = 0., val_max1, val_max2;
   int XparcoursMin, XparcoursMax, YparcoursMin, YparcoursMax;

   dprints("f_give_focus_point(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
      EXIT_ON_ERROR("Error retrieving data (NULL pointer)\n");

   my_data = (MyData_f_give_focus_point *) def_groupe[gpe].data;
   gpe_image = my_data->gpe_image;
   gpe_transition_detect = my_data->gpe_transition_detect;
   gpe_transition_detect_lieu = my_data->gpe_transition_detect_lieu;
   gpe_memory = my_data->gpe_memory;
   gpe_head = my_data->gpe_head;
   gpe_re = my_data->gpe_re;
   Xmin = my_data->Xmin;
   Ymin = my_data->Ymin;
   Xmax = my_data->Xmax;
   Ymax = my_data->Ymax;
   liste_maillon_resolu = my_data->liste_maillon_resolu;
   liste_maillon_a_resoudre = my_data->liste_maillon_a_resoudre;
   habituation = my_data->habituation;
   gpe_habituation = my_data->gpe_habituation;
   gpe_play_threshold = my_data->gpe_play_threshold;
   iteHabituation = (unsigned char)my_data->iteHabituation;
   iteDesappear = (unsigned char)my_data->iteDesappear;
   icc = my_data->icc;
   prom_OutputImage = my_data->prom_OutputImage;
   play = my_data->play;
   ambiguity = my_data->ambiguity;
   nb_objects = my_data->nb_objects;
   image_ext = my_data->image_ext;
   ima_output_ambiguity = my_data->ima_output_ambiguity;
   current_objects_value = my_data->current_objects_value;



   /*******************
    * Initialisations *
    ******************/
   if (def_groupe[gpe_image].ext == NULL)
   {
      PRINT_WARNING("No image in input group (ext == NULL)\n");
      neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = 1.;
      /** if previous ext is null, there is no salient point */
      return;
   }

   sx = ((prom_images_struct *) def_groupe[gpe_image].ext)->sx;
   sy = ((prom_images_struct *) def_groupe[gpe_image].ext)->sy;

   if( prom_OutputImage == NULL && (habituation || play) )
   {
      // INIT
      nb_imaOut = habituation + play + (play && ambiguity ? nb_objects : 0);
      prom_OutputImage = /*(prom_images_struct *)*/calloc_prom_image(nb_imaOut, sx, sy, 1);
      if(prom_OutputImage==NULL)
      {
         EXIT_ON_ERROR("%s : Malloc error prom_OutputImage\n", __FUNCTION__);
      }
      def_groupe[gpe].ext = (prom_images_struct *)prom_OutputImage;
      my_data->prom_OutputImage = prom_OutputImage;

      if( play )
      {
         my_data->x_prev = -1;
         my_data->y_prev = -1;
      }
   }

   if( habituation )		ima_output_inhib = (unsigned char *)prom_OutputImage->images_table[0];

   if( play )
   {
      ima_output_play = (unsigned char *)prom_OutputImage->images_table[habituation+play-1];

      if( !my_data->is_playing && neurone[def_groupe[my_data->gpe_play].premier_ele].s1 > 0.9 )
      {
         // Va passer en mode Jeu : Re-Initialisation
         for (x = 0; x < sx; x++)
            for (y = 0; y < sy; y++)
            {
               ima_output_play[x + y * sx] = 0;
            }

         my_data->max_detected = 0;
         my_data->x_max = -1;
         my_data->y_max = -1;
         my_data->confiance_max = -1;
         my_data->itePlay = 0;
      }

      my_data->is_playing = neurone[def_groupe[my_data->gpe_play].premier_ele].s1 > 0.9 ? 1 : 0;

      /* N'active pas le neurone de reconnaissance (pour le learning rate) par défaut */
      neurone[def_groupe[gpe].premier_ele + 4].s = neurone[def_groupe[gpe].premier_ele + 4].s1 = neurone[def_groupe[gpe].premier_ele + 4].s2  = 0;


      if( ambiguity )
      {
         for(x=0; x<nb_objects; x++)
         {
            ima_output_ambiguity[x] = (unsigned char *)prom_OutputImage->images_table[habituation+play+x];

            /* En sortie les neurones sont tous à 1 par défaut */
            neurone[def_groupe[gpe].premier_ele + 6 + x].s = neurone[def_groupe[gpe].premier_ele + 6 + x].s1 = neurone[def_groupe[gpe].premier_ele + 6 + x].s2  = 0.;
         }
      }
   }


   if (gpe_re != -1 && Xmin !=0 && Ymin != 0)
   {
      fprintf(stderr, "WARNING in %s[%s]: Two differents exclusions defined. -Xmin-Xmax-Ymin-Ymax link ignored \n",  __FUNCTION__,def_groupe[gpe].no_name);
   }
   if (gpe_re != -1 )
   {
      Xmin = neurone[def_groupe[gpe_re].premier_ele].s1;
      Xmax = neurone[def_groupe[gpe_re].premier_ele+1].s1;
      Ymin = neurone[def_groupe[gpe_re].premier_ele+2].s1;
      Ymax = neurone[def_groupe[gpe_re].premier_ele+3].s1;
   }
   dprints("f_give_focus_point(%s): sx = %d, sy = %d, R_e  = [%d, %d, %d, %d]\n", def_groupe[gpe].no_name, sx, sy, Xmin,Xmax,Ymin,Ymax);


   /**************************************************************
    * 1er parcours : remplit la liste chaînée triée des gagnants *
    *************************************************************/
   if (isequal(neurone[def_groupe[gpe_transition_detect].premier_ele].s2,1.))
   {
      dprints("f_give_focus_point(%s): transition detect -> sx = %d, sy = %d, R_e  = [%d, %d, %d, %d]\n", def_groupe[gpe].no_name, sx, sy, Xmin,Xmax,Ymin,Ymax);

      if (Xmin >= sx || Ymin >= sy || Xmax >= sx || Ymax >= sy)
         PRINT_WARNING("R_e too high -> no focus point will be used\n");

      /* Remet tous les maillons deja alloues dans la liste "liste_maillon_resolu" pour qu'ils puissent etre reutilises */
      if (liste_maillon_resolu == NULL)
      {
         liste_maillon_resolu = liste_maillon_a_resoudre;
      }
      else
      {
         maillon_temp = liste_maillon_resolu;
         while (maillon_temp->suivant != NULL)
            maillon_temp = maillon_temp->suivant;

         maillon_temp->suivant = liste_maillon_a_resoudre;
      }
      liste_maillon_a_resoudre = NULL;


      if( play && my_data->is_playing )	my_data->itePlay++;

      XparcoursMin = Xmin;
      XparcoursMax = sx - Xmax;
      YparcoursMin = Ymin;
      YparcoursMax = sy - Ymax;
      if(my_data->x_local_track != -1)
      {
         XparcoursMin = fmax(XparcoursMin,my_data->x_local_track - my_data->play_prox);
         XparcoursMax = fmin(XparcoursMax,my_data->x_local_track + my_data->play_prox);
         YparcoursMin = fmax(YparcoursMin,my_data->y_local_track - my_data->play_prox);
         YparcoursMax = fmin(YparcoursMax,my_data->y_local_track + my_data->play_prox);
         my_data->x_local_track = -1;
         my_data->y_local_track = -1;
         my_data->max_local_track = -1;
         my_data->is_local_tracking = 1;
         //printf("PARCOURS DE %d->%d\t\t%d->%d\n", XparcoursMin, XparcoursMax, YparcoursMin, YparcoursMax);
         my_data->is_local_tracking = 1;
      }else
         my_data->is_local_tracking = 0;

      XparcoursMin = fmax(XparcoursMin,0);
      XparcoursMax = fmin(XparcoursMax,sx);
      YparcoursMin = fmax(YparcoursMin,0);
      YparcoursMax = fmin(YparcoursMax,sy);

      /* Extraction des gagnants */
      for (x = XparcoursMin; x < XparcoursMax; x++)
      {
         for (y = YparcoursMin; y < YparcoursMax; y++)
         {
            {

               i = x + y * sx;
               inhib = 1; // Pas d'inhib


               /* Check for inhibition */
               if (habituation)
               {
                  if (ima_output_inhib[i] > 0)   ima_output_inhib[i]--;
                  inhib =  (ima_output_inhib[i] >= iteHabituation ? 0 : 1);	/* TODO: Inhib passe de 1 à 0 lentement ?	Bof... Un objet très saillant ne sera pê plus inhibé... (fausse bonne idée donc?)
																Et si inhibition croissante : problème : décroissance tt aussi lente... et donc attente avt visibilité imprévisible !
																Idées : si ima_output[i] > iteHabituation un moment, des qu'elle repasse en dessous de (iteHabituation-?) ça repasse à 0... (utile si iteHabituation est grand) */
               }
               if( play && neurone[def_groupe[my_data->gpe_play].premier_ele + 1].s1 > 0.99 && ima_output_play[i] < 50 ){
                  /* When playing, only look points of interests around the object */
                  inhib = 0;
               }

               /* Ambiguity : update objects maps */
               if( ambiguity )
               {
                  for(j=0; j<nb_objects; j++)
                  {
                     if(ima_output_ambiguity[j][i] > 0)
                     {
                        if( (ima_output_ambiguity[j][i] % iteDesappear) == 0 )
                           ima_output_ambiguity[j][i] = 0;
                        else
                           ima_output_ambiguity[j][i]--;
                     }
                  }
               }

               if ( inhib * ((prom_images_struct *) (def_groupe[gpe_image].ext))->images_table[image_ext][i] > def_groupe[gpe].seuil)
               {
                  /* Utilisation conjointe du groupe f_memory_focus_point */
                  /* A utiliser avec une camera pan-tilt lorsqu'un panorama est realise en prenant plusieurs images */
                  /* Les points de focalisation deja utilises dans une image precedente du meme panorama sont inhibes */
                  if (gpe_transition_detect_lieu >= 0 && gpe_memory >= 0 && def_groupe[gpe_memory].ext != NULL && gpe_head >= 0 && isdiff(neurone[def_groupe[gpe_transition_detect_lieu].premier_ele].s,1.0))
                  {
                     /* repositionnement du point de focalisation dans des coordonnees absolues en utilisant la direction de la camera */
                     sx_memory = ((prom_images_struct *) def_groupe[gpe_memory].ext)->sx;
                     sy_memory = ((prom_images_struct *) def_groupe[gpe_memory].ext)->sy;

                     head_h = neurone[def_groupe[gpe_head].premier_ele].s;
                     head_v = neurone[def_groupe[gpe_head].premier_ele + 1].s;

                     x_shifted = head_h * sx_memory;
                     y_shifted = head_v * sy_memory;

                     x_shifted = x_shifted + sx * (((float) x) / sx - 0.5);
                     y_shifted = y_shifted + sy * (((float) y) / sy - 0.5);

                     if (x_shifted < 0)                x_shifted = x_shifted + sx_memory;
                     else if (x_shifted > sx_memory)   x_shifted = x_shifted - sx_memory;


                     if (y_shifted < 0)
                        y_shifted = y_shifted + sy_memory;
                     else if (y_shifted > sy_memory)
                        y_shifted = y_shifted - sy_memory;

                     /* On fait la multiplication de l'intensite du point de foca par le masque garde dans memory permettant de savoir si le point a deja */
                     /* ete traite dans ce panorama (recouvrement avec une autre image du meme panorama) */
                     z = (((prom_images_struct *) def_groupe[gpe_memory].ext)->images_table[0][x_shifted + y_shifted * sx_memory] / 255) * ((prom_images_struct *) def_groupe[gpe_image].ext)->images_table[image_ext][i];

                     dprints("f_give_focus_point(%s): New point using memory-> i=%d, j=%d, x=%f, y=%f, z=%d, memory=%d\n", def_groupe[gpe].no_name, x, y, ((float) x) / sx, ((float) y) / sy, ((prom_images_struct *) def_groupe[gpe_image].ext)->images_table[image_ext][i], ((prom_images_struct *) def_groupe[gpe_memory].ext)->images_table[0][x_shifted + y_shifted * sx_memory]);
                  }
                  else
                  {
                     /* intensite du point de foca */
                     z = ((prom_images_struct *) def_groupe[gpe_image].ext)->images_table[image_ext][i];

                     dprints("f_give_focus_point(%s): New point -> i=%d, j=%d, x=%f, y=%f, z=%d\n", def_groupe[gpe].no_name, x, y, ((float) x) / sx, ((float) y) / sy, z);
                  }

                  if (z > 0)
                  {
                     if (liste_maillon_resolu == NULL)
                     {
                        /* Creation d'un maillon supplementaire */
                        maillon_temp = (liste_chaine *) malloc(sizeof(liste_chaine));
                        maillon_temp->suivant = NULL;
                        thepoint = (point3d *) malloc(sizeof(point3d));
                        maillon_temp->data = (point3d *) thepoint;

                        dprints("f_give_focus_point(%s): Creating new link\n", def_groupe[gpe].no_name);
                     }
                     else
                     {
                        /* Reutilisation d'un maillon deja alloue, non utilise */
                        maillon_temp = liste_maillon_resolu;
                        liste_maillon_resolu = liste_maillon_resolu->suivant;
                     }

                     /* Localisation en x et y du point de foca + intensite */
                     ((point3d *) (maillon_temp->data))->x = ((float) x); /* Avt :  / sx; */
                     ((point3d *) (maillon_temp->data))->y = ((float) y); /* Avt :  / sy; */
                     ((point3d *) (maillon_temp->data))->z = z;

                     /* insertion du maillon nouvellement cree dans la liste triée des maillons a resoudre */
                     maillon_parcours = liste_maillon_a_resoudre;
                     avant_maillon_parcours = NULL;
                     while ( maillon_parcours &&  (((point3d *) maillon_parcours->data)->z  >  ((point3d *) maillon_temp->data)->z) )
                     {
                        avant_maillon_parcours = maillon_parcours;
                        maillon_parcours = maillon_parcours->suivant;
                     }
                     maillon_temp->suivant = maillon_parcours;
                     if (avant_maillon_parcours) avant_maillon_parcours->suivant = maillon_temp;
                     else liste_maillon_a_resoudre = maillon_temp;
                  }
               }
            }
         }
      }
      /* Compte le nombre de points d'intérêts inhibés (DEBUG) *
		my_data->nbIte++;

		if (my_data->nbIte == my_data->iteDesappear)
		{
			/printf("Nombre de points d'interets inhibe : %d\n", my_data->nbPtInteretsInhib);*
			my_data->nbPtInteretsInhib = 0;
			my_data->nbIte = 0;
		}
       */
   }


   /**********************************************
    * Traite de gagnant et le retire de la liste *
    *********************************************/
   if (liste_maillon_a_resoudre != NULL)
   {

      maillon_fort = liste_maillon_a_resoudre;

      /* Suppression du maillon le plus fort (le premier puisque la liste est triée) de la liste des maillons a resoudre et insertion dans la liste des resolus */
      liste_maillon_a_resoudre = liste_maillon_a_resoudre->suivant;
      maillon_fort->suivant = liste_maillon_resolu;
      liste_maillon_resolu = maillon_fort;

      dprints("f_give_focus_point(%s): max intensity = %f\n", def_groupe[gpe].no_name, ((point3d *) maillon_fort->data)->z);



      /* Mise a jour des activites de la boite avec les coordonnees du point gagnant */
      neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = ((point3d *) maillon_fort->data)->x / (float)sx;

      neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = ((point3d *) maillon_fort->data)->y / (float)sy;

      neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = 0.;
      if(def_groupe[gpe].nbre > 3)
         neurone[def_groupe[gpe].premier_ele + 3].s = neurone[def_groupe[gpe].premier_ele + 3].s1 = neurone[def_groupe[gpe].premier_ele + 3].s2 = ((point3d *) maillon_fort->data)->z/255.0;




      /*********************************
       *  Habituation, play, ambiguity *
       *********************************/

      if( habituation || play )
      {
         xInhib = (int)((point3d *) maillon_fort->data)->x;
         yInhib = (int)((point3d *) maillon_fort->data)->y;
      }


      /***************
       * Habituation *
       **************/
      if( habituation && ( gpe_habituation == -1 || neurone[def_groupe[gpe_habituation].premier_ele].s1 > 0.5) )
      {
         /* Habituation activée :
          * Les pixels dans un rayon de icc (ICxxx) sont incrémentés :
          *  - De 1 (donc de 2 ici puisqu'il y a un -1 partout lors du transition_detect)
          *  - De iteDesappear	si la valeur du pixel autour est déjà au delà de iteHabituation,
          *  					ou bien partout si c'est le pt d'intérêt lui-même qui passe la valeur */
         if( ima_output_inhib[xInhib + yInhib * sx] >= iteHabituation-1 )
         {
            allInhib = 1;
            /* my_data->nbPtInteretsInhib++; PR DEBUG */
         }
         for (x = xInhib - icc; x < xInhib + icc; x++)
         {
            if( x >= 0 && x < sx)
               for (y = yInhib - icc; y < yInhib + icc; y++)
               {
                  if( y >= 0 && y < sy)
                  {
                     i = x + y * sx;
                     if(ima_output_inhib[i] >= iteHabituation-1 || allInhib == 1)
                        ima_output_inhib[i] = iteHabituation + iteDesappear; /* Il y aura iteDesappear -1 avant que ima_output ne repasse sous iteHabituation. */
                     else
                        ima_output_inhib[i] += 2; /* +2, car à la prochaine transition_detect il y aura un -1.*/
                  }
               }
         }
      }

      /********
       * Play *
       ********/
      if( play && my_data->x_prev != -1)
      {
         if(gpe_play_threshold != -1)	my_data->play_conf_threshold = neurone[def_groupe[my_data->gpe_play_threshold].premier_ele].s1;
         if( my_data->is_playing )
         {
            if( !my_data->max_detected )
            {
               /* Cherche le point d'intérêt dont la confiance est maximale sur tous les pts d'intérêts pendant iteDesappear itérations */
               if( neurone[def_groupe[my_data->gpe_confidence].premier_ele].s1 > my_data->confiance_max ) //(my_data->play_conf_threshold > my_data->confiance_max ? my_data->play_conf_threshold : my_data->confiance_max) )
               {
                  my_data->confiance_max = neurone[def_groupe[my_data->gpe_confidence].premier_ele].s1;
                  my_data->x_max = my_data->x_prev;
                  my_data->y_max = my_data->y_prev;
                  dprints("NOUVEAU PT MAX : x = %d    y = %d   Conf = %f\n", my_data->x_max, my_data->y_max, my_data->confiance_max );
               }
               /* Tous les pts d'intérêts ont été parcourus iteDesappear fois, on enregistre le max (si le seuil a été dépassé) */
               if( my_data->itePlay > iteDesappear + 1 && !isequal(my_data->confiance_max, -1) ) ////// Si parcouru un nb > NbVignettes/frame * iteDesappear et qu'une gagnante (>seuil) a été trouvé
               {
                  my_data->max_detected = 1;
                  max_just_detected = 1;
                  // On met les bonnes valeurs pour remplir l'image avec les coord du max
                  my_data->x_prev = my_data->x_max;
                  my_data->y_prev = my_data->y_max;
               }
            }
            if(my_data->max_detected && neurone[def_groupe[my_data->gpe_play].premier_ele + 1].s1 < 0.1)
            {
               /* Remplit l'image d'un carré autour du point d'intérêt, si : seuil dépassé dans une zone de proximité, ou si il s'agit du 1er (le max)
                * On remplit à 50 dans la zone de proximité, avec le rayon loin déf par PCxxx (play_prox), et à 250 dans le rayon proche déf par Pcxxx (play_cote)*/
               if( ( neurone[def_groupe[my_data->gpe_confidence].premier_ele].s1 > /*0.5*(my_data->confiance_max)*/my_data->play_conf_threshold  && ima_output_play[my_data->x_prev + my_data->y_prev * sx] == 50 ) || max_just_detected == 1)

                  for (x = my_data->x_prev - my_data->play_prox; x < my_data->x_prev + my_data->play_prox; x++)
                     if( x >= 0 && x < sx)
                        for (y = my_data->y_prev - my_data->play_prox; y < my_data->y_prev + my_data->play_prox; y++)
                        {
                           if( y >= 0 && y < sy)
                           {
                              i = x + y * sx;
                              if( abs(x - my_data->x_prev) < my_data->play_cote && abs(y - my_data->y_prev) < my_data->play_cote)
                                 ima_output_play[i] = 250;
                              else
                                 if( ima_output_play[i] <= 50 ) ima_output_play[i] = 50;
                           }
                        }
            }
         }
         else if(ambiguity)
         {
            /* Not playing, and we have the value of the LMS (thx to ambiguity) */

            /*********
             * FOCUS *
             *********/

            /* Get the 2 max */
            val_max1 = -1;
            val_max2 = -1;
            n_ambi_ininteresting = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + nb_objects -1].s1;
            for(x=0; x<nb_objects-1; x++)
            {
               if( neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting > fmax(0.0001, val_max2) )
               {
                  if( neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting > val_max1 ){
                     val_max2 = val_max1;
                     val_max1 = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting;
                  }else
                     val_max2 = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting;

               }
            }

            /* If > threshold and current max, store the coord */
            if(  (val_max1 - fmax(val_max2,0.))  >  my_data->play_conf_threshold )
            {
               /* A interesting point is find */

               /* Next time we will look only point of interest in a square around this point */


               if( my_data->max_local_track < (val_max1 - fmax(val_max2,0.)) )
               {
                  my_data->max_local_track = (val_max1 - fmax(val_max2,0.));
                  my_data->x_local_track = my_data->x_prev;
                  my_data->y_local_track = my_data->y_prev;
                  /* Prochain parcours on active is_local_tracking et on met le neurone 5 à la valeur 1 */
               }
            }
            else{
               /* No interesting point find */
               //my_data->x_local_track = -1;
               //my_data->y_local_track = -1;
            }
         }

         /* Si l'utilisateur dit "oui, c'est bien l'objet" (2ème neurone avec le lien , on met le learning rate à 1 pour les points d'intérêts certifiés (neurone 5 du groupe) */
         if( /*my_data->is_playing == 1 &&*/ my_data->max_detected && neurone[def_groupe[my_data->gpe_play].premier_ele + 1].s1 > 0.99 && ima_output_play[xInhib + yInhib * sx] == 250)
            neurone[def_groupe[gpe].premier_ele + 4].s = neurone[def_groupe[gpe].premier_ele + 4].s1 = neurone[def_groupe[gpe].premier_ele + 4].s2  = 1;






         /*************
          * Ambiguity *
          ************/
         if( ambiguity )
         {
            /* Computed for the previous point (due to the secondary link from the LMS box ambiguity */
            n_ambi_ininteresting = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + nb_objects -1].s1;

            /* Get the activated neurons and the winner map */
            winner_map = -1;
            max1 = -1;
            val_max1 = 0.;
            found_objects = 0;

            for(x=0; x<nb_objects-1; x++)
            {
               /* Get the map max */
               if( ima_output_ambiguity[x][my_data->x_prev + my_data->y_prev * sx] > max1 )
               {
                  max1 = ima_output_ambiguity[x][my_data->x_prev + my_data->y_prev * sx];
                  val_max1 = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1;
                  winner_map = x;
               }

               /* Get the activated neurons (i.e. the recognized objects) */
               current_objects_value[x] = -1;
               if( neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting > 0.0001 )
               {
                  current_objects_value[x] = neurone[def_groupe[my_data->gpe_ambiguity].premier_ele + x].s1 - n_ambi_ininteresting;
                  found_objects++;
               }
            }

            /* Look for neurons to inhibit and inhibit them */
            for(x=0; x<nb_objects-1; x++)
            {
               if( current_objects_value[x] > 0 && x != winner_map)
               {
                  /* There is a neuron to inhibit, and it do not correspond to the winnig map */

                  conf_obj_cur = floor((float)ima_output_ambiguity[x][my_data->x_prev + my_data->y_prev * sx]/(float)iteDesappear);
                  conf_obj_max = floor((float)max1/(float)iteDesappear);

                  /* We compare the map of the recognized object with the map of the winnig object  */
                  if( isequal(conf_obj_cur, 0.) )
                     ratio = conf_obj_max;
                  else
                     ratio = conf_obj_max / conf_obj_cur;

                  if( ratio >= my_data->ambiguity_ratio )
                  {
                     /* We erase the ambiguity */
                     neurone[def_groupe[gpe].premier_ele + 6 + x].s 			= neurone[def_groupe[gpe].premier_ele + 6 + x].s1
                           = neurone[def_groupe[gpe].premier_ele + 6 + x].s2 	= - (current_objects_value[x]);
                  }
               }
            }

            /* Fill the spatial confidence map */
            if( (found_objects == 1) && (my_data->x_prev != -1) )
            {
               /* Just one object recognized : we add it to the spatial confidence map */
               for(confident_recog=0; confident_recog<nb_objects-1; confident_recog++)
               {
                  if( current_objects_value[confident_recog] > my_data->play_conf_threshold )
                  {
                     /* confident_recog est le seul objet reconnu, et il dépasse le seuil.
                      * Remplit l'image correspondant à l'objet qui a été reconnu sans ambiguité autour du point d'intérêt, dans une zone de proximité. */
                     for (x = my_data->x_prev - my_data->play_prox; x < my_data->x_prev + my_data->play_prox; x++)
                     {
                        if( x >= 0 && x < sx)
                        {
                           for (y = my_data->y_prev - my_data->play_prox; y < my_data->y_prev + my_data->play_prox; y++)
                           {
                              if( y >= 0 && y < sy)
                              {
                                 i = x + y * sx;
                                 //
                                 if( ima_output_ambiguity[confident_recog][i] > 0 )
                                 {
                                    arr = (int)floor((float)ima_output_ambiguity[confident_recog][i] / (float)(iteDesappear));
                                    j = (arr+2)*(iteDesappear);
                                    j--;
                                    while( j > 255)		j = j - (iteDesappear);
                                    ima_output_ambiguity[confident_recog][i] = (unsigned char)((j < 0) ? 0 : j);
                                 }
                                 else
                                    ima_output_ambiguity[confident_recog][i] = (unsigned char)(iteDesappear - 1);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      if( habituation || play )
      {
         /* Remplit le point courant */
         my_data->x_prev = xInhib;
         my_data->y_prev = yInhib;
      }
   }
   else
   {
      dprints("f_give_focus_point(%s): No more points\n", def_groupe[gpe].no_name);

      neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = 1.;
   }

   if( ambiguity ){
      neurone[def_groupe[gpe].premier_ele + 5].s = neurone[def_groupe[gpe].premier_ele + 5].s1 = neurone[def_groupe[gpe].premier_ele + 5].s2 = (float)my_data->is_local_tracking;
   }

   my_data->liste_maillon_resolu = liste_maillon_resolu;
   my_data->liste_maillon_a_resoudre = liste_maillon_a_resoudre;


#ifdef DEBUG
   dprints("f_give_focus_point(%s): Leaving function\n", def_groupe[gpe].no_name);
#endif
}

void destroy_give_focus_point(int gpe)
{
   liste_chaine *maillon_temp;
   liste_chaine *maillon_free;
   MyData_f_give_focus_point *my_data;

   dprints("destroy_give_focus_point(%s): Entering function\n", def_groupe[gpe].no_name);


   if (def_groupe[gpe].data != NULL)
   {
      my_data = (MyData_f_give_focus_point *) def_groupe[gpe].data;

      maillon_temp = my_data->liste_maillon_a_resoudre;
      while (maillon_temp != NULL)
      {
         free((point3d *)maillon_temp->data);
         maillon_temp->data = NULL;

         maillon_free = maillon_temp;
         maillon_temp = maillon_temp->suivant;

         free(maillon_free);
      }
      my_data->liste_maillon_a_resoudre = NULL;

      maillon_temp = my_data->liste_maillon_resolu;
      while (maillon_temp != NULL)
      {
         free((point3d *)maillon_temp->data);
         maillon_temp->data = NULL;

         maillon_free = maillon_temp;
         maillon_temp = maillon_temp->suivant;

         free(maillon_free);
      }
      my_data->liste_maillon_resolu = NULL;

      free(my_data->prom_OutputImage);

      if( my_data->ambiguity ){
         free(my_data->ima_output_ambiguity);
         free(my_data->current_objects_value);
      }

      free(my_data);
   }

   dprints("destroy_give_focus_point(%s): Leaving function\n", def_groupe[gpe].no_name);
}
