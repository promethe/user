/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include <string.h>

#include "tools/include/init_look_up_polaire.h"

typedef struct My_Data_f_rho_theta
{
   int GpeX;
   int GpeY;
   int GpeLand;
   int GpeAzim;
   int GpeIm;
   int GpeTrans;
   int interface;
   int x_print;
   int y_print;
} My_Data_f_rho_theta;

int **init_lookup_rho_theta(int xmax, int ymax, float r, int R, int gpe);


/**
 * Voir la fonction init_lookup_rho_theta pour une description de la lookup table
 */
void function_rho_theta(int gpe_sortie)
{
   unsigned char *Image = NULL;
   float *Image_float = NULL;
   int GpeX, GpeY, GpeR, Gper, GpeIm;
   int Sx, Sy;
   int i, j, deb, Index;
   int PositionX, PositionY;
   int decalage;
   int TailleGpe;
   float TempMax = 0.;
   int nb_band;
   prom_images_struct *ImageLocale = NULL, *ImageEntre = NULL;
   int anti_nan, taille_image;

   int **lookup = NULL;
   float sum;
   int k;
   float min,max;
#ifdef DEBUG
#ifndef AVEUGLE
   TxPoint point,point1;
   int couleur;
   static prom_images_struct * im_rec=NULL;
   static int * ind_rec;
#endif
#endif

   dprints("---- function_rho_theta ----\n");
   if (def_groupe[gpe_sortie].data == NULL)
   {
      float R, r;

      /* test de la taille */
      if (def_groupe[gpe_sortie].taillex != def_groupe[gpe_sortie].tailley)
      {
         printf("Le groupe % doit etre une matrice carree\a\n", gpe_sortie);
         printf("Exit in function_rho_theta\n");
         exit(-1);
      }

      /* recuperation info */
      GpeX = GpeY = GpeR = Gper = GpeIm = -1;
      for (Index = 0; Index < nbre_liaison; Index++)
         if (liaison[Index].arrivee == gpe_sortie)
         {
            if (!strcmp(liaison[Index].nom, "X"))   GpeX = liaison[Index].depart;
            if (!strcmp(liaison[Index].nom, "Y"))   GpeY = liaison[Index].depart;
            if (!strcmp(liaison[Index].nom, "R"))   GpeR = liaison[Index].depart;
            if (!strcmp(liaison[Index].nom, "r"))   Gper = liaison[Index].depart;
            if (!strcmp(liaison[Index].nom, "ext")) GpeIm = liaison[Index].depart;
         }
      if ((GpeX == -1) || (GpeY == -1) || (GpeR == -1) || (Gper == -1)  || (GpeIm == -1))
      {
         printf ("Le groupe % doit etre connecte a 5 autre groupes avec les liens <X>, <Y>, <R>, <r> et <Im>\n",gpe_sortie);
         printf("Exit in function_rho_theta\n");
         exit(-1);
      }

      R = neurone[def_groupe[GpeR].premier_ele].s;
      r = neurone[def_groupe[Gper].premier_ele].s;
      /*printf("GpeX=%d GpeY=%d GpeR=%d Gper=%d GpeIm=%d\n",GpeX,GpeY,GpeR,Gper,GpeIm); */
      if (def_groupe[GpeIm].ext == NULL)
      {
         printf("L'extension du groupe d'entre est nulle\n");
         printf("Exit in function_rho_theta\n");
         return;
      }
      ImageEntre = (prom_images_struct *) def_groupe[GpeIm].ext;
      Sx = ImageEntre->sx;
      Sy = ImageEntre->sy;
      nb_band = ImageEntre->nb_band;
      Image = ImageEntre->images_table[0];

      if (nb_band != 1 && nb_band != 4)
      {
         printf("nb_band non gere par f_rho_theta gpe %d\n", gpe_sortie);
         exit(1);
      }

      /* initialisation table look up */
      lookup = init_lookup_rho_theta(Sx, Sy, /*(int) */ r, (int) R, gpe_sortie);

      def_groupe[gpe_sortie].data = (void *) malloc(sizeof(prom_images_struct));
      if (def_groupe[gpe_sortie].data == NULL)
      {
         printf("Not enought memory!\n");
         printf("Exit in function_rho_theta\n");
         exit(-1);
      }
      ImageLocale = (prom_images_struct *) (def_groupe[gpe_sortie].data);
      ImageLocale->image_number = 0;
      ImageLocale->sx = Sx;
      ImageLocale->sy = Sy;
      ImageLocale->nb_band = nb_band;
      /*	ImageLocale->images_table[0] = NULL;
      	ImageLocale->images_table[0] = malloc(sizeof(unsigned char)*def_groupe[gpe_sortie].nbre);
              if (ImageLocale->images_table[0] == NULL)
              {
                  printf
                      ("impossible d'allouer de la memoire dans f_rho_theta gpe : %d\n",
                       gpe_sortie);
                  exit(1);
              }*/
      ImageLocale->images_table[1] = (unsigned char *) lookup;
      ImageLocale->images_table[2] = NULL;
      ImageLocale->images_table[2] = (unsigned char *) malloc(3 * sizeof(int));
      if (ImageLocale->images_table[2] == NULL)
      {
         printf
         ("impossible d'allouer de la memoire dans f_rho_theta gpe : %d\n",
               gpe_sortie);
         exit(1);
      }
      ((int *) (ImageLocale->images_table[2]))[0] = GpeX;
      ((int *) (ImageLocale->images_table[2]))[1] = GpeY;
      ((int *) (ImageLocale->images_table[2]))[2] = GpeIm;
#ifndef AVEUGLE
#ifdef DEBUG
      im_rec=calloc_prom_image(1, Sx, Sy, 1);
      ind_rec=malloc(sizeof(int)*Sx*Sy);
      for (i=0; i<Sx*Sy; i++)
      {
         ind_rec[i]=99999;
         im_rec->images_table[0][i]=(unsigned char)255;
      }
#endif
#endif

   }
   else
   {
      ImageLocale = (prom_images_struct *) (def_groupe[gpe_sortie].data);
      Sx = ImageLocale->sx;
      Sy = ImageLocale->sy;
      nb_band = ImageLocale->nb_band;
      GpeIm = ((int *) (ImageLocale->images_table[2]))[2];
      GpeX = ((int *) (ImageLocale->images_table[2]))[0];
      GpeY = ((int *) (ImageLocale->images_table[2]))[1];
      lookup = (int **) ImageLocale->images_table[1];
      Image = ((prom_images_struct *) def_groupe[GpeIm].ext)->images_table[0];
   }

   /* cherche la position X et Y */
   TempMax = 0.0;
   PositionX = -1;
   for (i = 0, j = def_groupe[GpeX].premier_ele; i < def_groupe[GpeX].nbre; i++, j++)
      if (neurone[j].s1 > TempMax) PositionX = i;

   TempMax = 0.0;
   PositionY = -1;
   for (i = 0, j = def_groupe[GpeY].premier_ele; i < def_groupe[GpeY].nbre; i++, j++)
      if (neurone[j].s1 > TempMax) PositionY = i;


   deb = def_groupe[gpe_sortie].premier_ele;
   TailleGpe = def_groupe[gpe_sortie].taillex * def_groupe[gpe_sortie].tailley;
   /* reset des neurones */

/*	printf("%s x=%d, y=%d \n",__FUNCTION__, PositionX, PositionY);
	printf("%s s= %e s=%e \n",__FUNCTION__, neurone[PositionX+def_groupe[GpeX].premier_ele].s1, neurone[PositionY+def_groupe[GpeY].premier_ele].s1);*/

   if ((PositionX > 0) && (PositionY > 0))
   {
      decalage = PositionX + PositionY * Sx;

      if (nb_band == 1)
      {
         max=0.;
         min=1.;
         for (i = 0, j = deb; i < TailleGpe; i++, j++)
         {
            neurone[j].s = neurone[j].s1 = 0.;
            neurone[j].s2 = 1.; /* s2 vaut 1 partout la ou le groupe suivant devra apprendre */
            sum = 0.;
            for (k = 0; k < lookup[i][0]; k++)  sum = sum + Image[decalage + lookup[i][k + 1]];

            neurone[j].s = neurone[j].s1 = (float) (sum / (float) (lookup[i][0] * 255));

            if (neurone[j].s >max)  max=neurone[j].s ;
            if (neurone[j].s <min)  min=neurone[j].s;
         }
#ifdef DEBUG
#ifndef AVEUGLE
         /*efface image accumul*/
         point.x=0;
         point.y=Sy+10;
         TxDessinerRectangle_rgb16M(&image1,rgb16M(255,255,255),TxPlein,point, Sx,Sy,0);

         for (i = 0, j = deb; i < TailleGpe; i++, j++)
         {
            neurone[j].s = neurone[j].s1 = 1.-(neurone[j].s-min)/(max-min);
            for (k = 0; k < lookup[i][0]; k++)
            {
               couleur=rgb16M((int) ( neurone[j].s1 * 255.) ,(int)(neurone[j].s1*255.),(int)(neurone[j].s1*255.));
               point.x=/*500+*/(decalage + lookup[i][k + 1])%Sx;
               point.y=/*500+*/(decalage + lookup[i][k + 1])/Sx;
               TxDessinerPoint_rgb16M(&image1,couleur, point);
               point.x=(decalage + lookup[i][k + 1])%Sx;
               point.y=Sy+10+(decalage + lookup[i][k + 1])/Sx;
               TxDessinerPoint_rgb16M(&image1,couleur, point);
               if (lookup[i][0] <  ind_rec[decalage + lookup[i][k + 1]])
               {
                  im_rec->images_table[0][decalage + lookup[i][k + 1]]=(int)(neurone[j].s*255);
                  ind_rec[decalage + lookup[i][k + 1]]=lookup[i][0];
               }
            }

            couleur=rgb16M((int) ( neurone[j].s1 * 255.) ,(int)(neurone[j].s1*255.),(int)(neurone[j].s1*255.));
            point1.x=500+(i%def_groupe[gpe_sortie].taillex)*10;
            point1.y=500-(i/def_groupe[gpe_sortie].taillex)*10;

            TxDessinerRectangle_rgb16M(&image1,couleur,TxPlein,point1, 10,10, 0);
         }
         TxAfficheImageNB(&image1, im_rec->images_table[0],Sx,Sy,Sx+10,0);
         /*efface image rec*/
         point.x=Sx+10;
         point.y=0;
         TxDessinerRectangle_rgb16M(&image1,0,TxVide,point, Sx,Sy, 0);
         TxFlush(&image1);

#endif
#endif
      }
      else
      {
         taille_image = Sx * Sy;
         Image_float = (float *) Image;
         for (i = 0, j = deb; i < TailleGpe; i++, j++)
         {
            neurone[j].s = neurone[j].s1 = 0.;
            neurone[j].s2 = 1.; /* s2 vaut 1 partout la ou le groupe suivant devra apprendre */
            sum = 0.;
#ifdef LOOKUP
            printf("%d ", lookup[i][0]);
#endif
            for (k = 0; k < lookup[i][0]; k++)
            {
               anti_nan = lookup[i][k + 1] + decalage;
               if (anti_nan > 0 && anti_nan < taille_image)  sum = sum + Image_float[anti_nan];
#ifdef LOOKUP
               printf("%d,%d ", lookup[i][k + 1] / Sx, lookup[i][k + 1] % Sx);
#endif
            }
            neurone[j].s = neurone[j].s1 = (float) (sum / (float) (lookup[i][0] * 255));
#ifdef LOOKUP
            printf("\n");
#endif
         }
#ifdef LOOKUP
         printf("---------\n");
#endif

      }
   }

   else
   {
      /*Dessin d'un pattern particulier, puisqu'il n'y a pas de position de pt de foc specifiee */
      TempMax = 0.0;
      for (i = deb; i < TailleGpe + deb; i++)
      {
         /* TempMax = 1.0 - TempMax; */
         neurone[i].s = neurone[i].s1 = 0.;  /*M.M. j'ai besoin de ce pattern particulier */
      }
   }

}


/**
 *
 * Explication des différents tableaux:
 * xmax,ymax: coordonnee max de l'image d'entree
 * xmax2,ymax2 : coordonnee max de la sortie (groupe de neurone)
 *
 * 2 tableaux codent la projection dans les 2 sens :
 * lookup[xmax2*ymax2]: coordonnee du point dans l'image d'entrée code par le ieme neurone du groupe
 * other_pt[xmax,ymax]:index du neurone de sortie qui code le mieux le point donne en index
 *
 * 2 tableaux comptent le nombre de point (reel et normalise) represente par un neurone. Cette info est utilise pour moyenner l'activite du neurone
 * compteur[xmax2*ymax2]: nombre de points contenu dans les tableaux other_pt et lookup qui pointent vers le même neurone de sortie
 * compteur_egal[xmax2*ymax2]: normalisation: un même point dans l'image dorigine peut être code par plusieurs neurones en sortie.
 *                             chacun de ces neurones doit avoir la meme valeur de compteur -> propagation de la valeur max.
 *
 *
 * finalement la fonction renvoie un tableau nomme real_lookup:
 * real_lookup [i][0]=compteur [i]
 * real_lookup [i][1]=lookup[i]
 * real_lookup [i][k]=les coordonnees des (k-2) points de l'image d'entree tels que other_pt=i
 */


int **init_lookup_rho_theta(int xmax, int ymax, float r, int R, int gpe)
{
   int x, y, p;
   float rho1, theta1;

   int xmax2, ymax2;
   float dist, min, max;
   int *lookup;
   int i, j, k, cpt, neur;

   int xl, yl;
   /*  int cpt=0; */
   int *other_pt;
   int *compteur;
   int *compteur_egal;
   int **real_lookup;
   int ind_min = -1;

#ifndef AVEUGLE
#ifdef DEBUG
#ifndef AVEUGLE
   TxPoint point;
#endif
#endif /*DEBUG*/
#endif
   dprints("initialisation de la table de look-up\n");
   dprints("r= %f R=%d \n", r, R);

   xmax2 = def_groupe[gpe].taillex;
   ymax2 = def_groupe[gpe].tailley;

   dprints("groupe %d, xmax=%d, ymax=%d \n", gpe, xmax, ymax);

   lookup = (int *) malloc(sizeof(int) * xmax2 * ymax2);
   if (lookup == NULL)
   {
      printf
      ("ERREUR la table de look-up log-polaire n'a pu etre initialisee \n");
      exit(-1);
   }
   for (i = 0; i < xmax2 * ymax2; i++)
   {
      lookup[i] = 0;
   }
#ifdef DEBUG
   printf("lookup\n");
#endif
   rho1 = ((float) xmax2) / (log(1. + (float) R - r));
   theta1 = ((float) ymax2) / (2. * pi);

   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         neur = i * xmax2 + j;
         dist = exp(j / rho1) - 1 + r;
         x = dist * cos(i / theta1);
         y = dist * sin(i / theta1);
         lookup[neur] = y * xmax + x;
#ifndef AVEUGLE
#ifdef DEBUG
         point.x = x + xmax / 2;
         point.y = y + ymax / 2;

         TxDessinerPoint(&image1, rouge, point);
         printf("%5d ", lookup[neur]);
#endif
#endif
      }
#ifdef DEBUG
      printf("\n");
#endif
   }
#ifndef AVEUGLE

#ifdef DEBUG
   TxFlush(&image1);
#endif

#endif
   /*je compte le nbre de point qui sont aussi dans la couronne et j'alloue un tablau de cette taille ou chaque point
      est represente par une case.
      le contenu de chaque case represente l'indice du neurone dont ce point est le plus pret.

    */
   for (y = -ymax / 2; y < ymax / 2; y++)
      for (x = -xmax / 2; x < xmax / 2; x++)
      {
         dist = sqrt(x * x + y * y);
         if (dist < R && dist > r)
         {
            cpt++;
         }
      }
   other_pt = MANY_ALLOCATIONS(xmax * ymax, int);

   for (y = -ymax / 2; y < ymax / 2; y++)
      for (x = -xmax / 2; x < xmax / 2; x++)
      {
         other_pt[x + xmax / 2 + (y + ymax / 2) * xmax] = -1;
      }
   dprints("other_pt\n");
   for (y = -ymax / 2; y < ymax / 2; y++)
   {
      for (x = -xmax / 2; x < xmax / 2; x++)
      {
         ind_min = -1;
         dist = sqrt(x * x + y * y);
         p = x + xmax / 2 + (y + ymax / 2) * xmax;
         if (dist <= R && dist >= r)
         {



            /*pour chaque point, on va chercher le point de lookup le plus proche   */
            min = 9999999.;
            for (i = 0; i < xmax2 * ymax2; i++)
            {
               xl = (lookup[i] + xmax / 2 + ymax / 2 * xmax) % xmax;
               yl = (lookup[i] + xmax / 2 + ymax / 2 * xmax) / xmax;
               dist =
                     (x + xmax / 2 - xl) * (x + xmax / 2 - xl) + (y +
                           ymax /
                           2 -
                           yl) *
                           (y + ymax / 2 - yl);
               if (dist <= min)
               {
                  min = dist;
                  ind_min = i;
               }           /*
                                   else if(dist==min)
                                   {

                                   } */
            }

            other_pt[p] = ind_min;
         }
         /* 		if( x+y*xmax==lookup[other_pt[p]]  )*/
         /* 			printf("X%4dX ",other_pt[p]);*/
         /* 		else if(x==0 && y==0)*/
         /* 			printf("OOOOO ");*/
         /* 		else */
         /* 			printf(" %5d ",other_pt[p]);*/

      }
      /* 	printf("\n");*/
   }

   /*ensuite, je compte pour chaque neurone le nbre de point de other_pt qui senvoie sur le neurone, y compris le pt de lookup.
      Je met ca dans compteur et compteur_egal.
      Comme certain neurone utilise le meme point, il faut que tout le neurone utilisant le meme point utilise le meme nombre de pt en global
      compteur_egal contient pour chaque neurone le nombre de point precis a utilser. Alors que compteur donne juste le nombre de pt donne par lookup et other_pt */

   compteur =  MANY_ALLOCATIONS(xmax2 * ymax2, int );
   compteur_egal =  MANY_ALLOCATIONS( xmax2 * ymax2, int);


   for (i = 0; i < xmax2 * ymax2; i++)
   {
      compteur[i] = 1;
      compteur_egal[i] = 1;
   }
   dprints("compteur\n");
   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         for (y = -ymax / 2; y < ymax / 2; y++)
            for (x = -xmax / 2; x < xmax / 2; x++)
            {

               if (other_pt[x + xmax / 2 + (y + ymax / 2) * xmax] ==
                     j + i * xmax2
                     && x + xmax / 2 + (y + ymax / 2) * xmax !=
                           lookup[j + i * xmax2])
               {
                  compteur[j + i * xmax2]++;
                  compteur_egal[j + i * xmax2]++;
               }
            }
         dprints("%3d ", compteur[j + i * xmax2]);
      }
      dprints("\n");
   }

   for (i = 0; i < ymax2 * xmax2; i++)
   {
      max = compteur_egal[i];
      for (j = 0; j < ymax2 * xmax2; j++)
      {
         if (lookup[i] == lookup[j])
            if (compteur_egal[j] > max)
            {
               compteur_egal[i] = compteur_egal[j];
               max = compteur_egal[j];
            }
      }
   }
#ifdef DEBUG
   printf("newcompteur\n");
   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         printf("%3d ", compteur_egal[j + i * xmax2]);
      }
      printf("\n");
   }
   printf("real_lookup\n");
#endif

   real_lookup = MANY_ALLOCATIONS(xmax2 * ymax2, int*);

   for (i = 0; i < xmax2 * ymax2; i++)
   {
      /*printf(":\n"); */
      real_lookup[i] =  MANY_ALLOCATIONS((compteur_egal[i] + 1), int);

      real_lookup[i][0] = compteur[i];
      real_lookup[i][1] = lookup[i];
      /*  	printf("%d,%d \n",i,lookup[i]);*/
      cpt = 2;
      for (y = -ymax / 2; y < ymax / 2; y++)
         for (x = -xmax / 2; x < xmax / 2; x++)
         {
            if (other_pt[x + xmax / 2 + (y + ymax / 2) * xmax] == i)
            {
               real_lookup[i][cpt] = x + y * xmax;
               cpt++;
               /* 			printf("%d ",x+xmax/2+(y+ymax/2)*xmax);*/
            }

         }

   }
#ifdef DEBUG
   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         printf("%2d ", real_lookup[i * xmax2 + j][0]);
      }
      printf("\n");
   }
   printf("real_llokup\n");
#endif
   for (i = 0; i < ymax2 * xmax2; i++)
   {
      max = real_lookup[i][0];
      for (j = 0; j < ymax2 * xmax2; j++)
      {
         if (real_lookup[i][1] == real_lookup[j][1])
            if (real_lookup[j][0] > max)
            {
               max = real_lookup[j][0];
               for (k = 0; k <= real_lookup[j][0]; k++)
               {
                  real_lookup[i][k] = real_lookup[j][k];
               }
            }
      }
   }
#ifndef AVEUGLE
#ifdef DEBUG
   printf("real_lookup after fusion\n");
   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         printf("%2d ", real_lookup[i * xmax2 + j][0]);
      }
      printf("\n");
   }

   for (i = 0; i < ymax2; i++)
   {
      for (j = 0; j < xmax2; j++)
      {
         printf("%4d->%2d:", i * xmax2 + j, real_lookup[i * xmax2 + j][0]);
         for (k = 0; k < real_lookup[i * xmax2 + j][0]; k++)
         {
            printf("%5d ", real_lookup[i * xmax2 + j][k + 1]);
            point.x = x + xmax / 2;
            point.y = y + ymax / 2;
            x = (real_lookup[i * xmax2 + j][k + 1]) + (ymax / 2) * xmax + xmax / 2;
            y = x / xmax + 100;
            x = x % xmax + 100;
            point.x = x;
            point.y = y;
            TxDessinerPoint(&image1, rouge, point);
            TxFlush(&image1);
         }
         /* 		getchar();*/
         printf("\n");
      }
      printf("\n");
   }
#endif
#endif
   return (real_lookup);
}
