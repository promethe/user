/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  init_look_up_polaire.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

int *init_look_up_polaire(int xmax, int ymax, float a, int d, int gpe)
{
    int x, y, p, p2 /*,u,v,k */ ;
    float rho1 /*,rho2,rho3,rho4 */ , theta1 /*,uu,vv */ ;
    float rho, theta, max;
    int irho, itheta;
    int *look_up_polaire;
    int deb, xmax2, ymax2;

    (void) max; // (unused)
    (void) deb; // (unused)

#ifndef AVEUGLE
#ifdef DEBUG
#ifndef AVEUGLE
    TxPoint point;
#endif
#endif /*DEBUG*/
#endif
#ifdef DEBUG
        printf("initialisation de la table de look-up\n");
    printf("a= %f d=%d \n", a, d);
#endif

    deb = def_groupe[gpe].premier_ele;
    xmax2 = def_groupe[gpe].taillex;
    ymax2 = def_groupe[gpe].tailley;

#ifdef DEBUG
    printf("groupe %d, xmax=%d, ymax=%d \n", gpe, xmax, ymax);
#endif

    look_up_polaire = (int *) malloc(sizeof(int) * xmax * ymax);
    if (look_up_polaire == NULL)
    {
        printf
            ("ERREUR la table de look-up log-polaire n'a pu etre initialisee \n");
        exit(-1);
    }
    memset(look_up_polaire, 0, sizeof(int) * xmax * ymax);
    max = (float) xmax;


    if (ymax > xmax)
        max = (float) ymax;
    rho1 = ((float) xmax2) / (log(1. + (float) d));
    theta1 = ((float) ymax2) / (2. * pi);
/*
  for(y=0;y<ymax2;y++)
   for(x=0;x<xmax2;x++)
    {
     p=x+xmax2*y;
     rho=(float)(x);
     rho=exp((rho+a)/rho1)-1;     
     theta=(float)(y);
     theta=theta/theta1 - pi;
     uu=rho*cos(theta);
     vv=rho*sin(theta);
     if(fabs(uu)<2.*max && fabs(vv)<2.*max) 
      {
       u=xmax/2+(int)(uu); 
       v=ymax/2+(int)(vv);
       if(u>=0 && v>=0 && u<xmax && v<ymax)
        {
         p2=u+v*xmax;
         
#ifdef DEBUG
         printf("%d %d\n",u,v);
         point.x=u;point.y=v;TxDessinerPoint(&image1,rouge,point);
#endif

         look_up_polaire[p2]=p;
        }
      }
    }

  printf("part2 \n");
*/
    for (y = 0; y < ymax; y++)
        for (x = 0; x < xmax; x++)
        {
            p = x + xmax * y;
            rho =
                (float) ((x - xmax / 2) * (x - xmax / 2) +
                         (y - ymax / 2) * (y - ymax / 2));
            rho = log(sqrt(rho) + 1.) * rho1 - a;
            /*   printf("rho = %f a =%f \n",rho ,a); */
            if ((rho > 0.) && (x != xmax / 2 || y != ymax / 2))
            {
                theta =
                    (pi +
                     atan2(((double) (y - ymax / 2)),
                           ((double) (x - xmax / 2)))) * theta1;
                irho = (int) rho;
                itheta = (int) theta;
                if (irho < xmax2 && itheta < ymax2)
                {
                    p2 = irho + xmax2 * itheta;
                    /*     point.x=x;point.y=y;TxDessinerPoint(&image1,irho/4+itheta/4,point);  */
                    look_up_polaire[p] = p2;
                }
            }
        }
#ifdef DEBUG
    printf("fin de l'initialisation \n");
#endif
    return (look_up_polaire);
}
