/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\defgroup f_nios_give_focus_point
 


\brief
Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance
 generee par la carte de RobotSoc (NIOS)

\ingroup libSensors

\defgroup f_nios_give_focus_point f_nios_give_focus_point
\ingroup libSensors

\brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance
 generee par la carte de RobotSoc (NIOS)

\details

\section Description

    le groupe traite la liste des points de l'image d'entree (groupe f_nios_grab_images via la liaison algo 'image') a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
    du point sur ces deux premiers neurones. Le troisieme neurone donne l'activite (saillance) et peut etre connecte a un break (quand activite=0!);


    \file  f_give_focus_point.c
  \ingroup f_give_focus_point
    \brief Pour l'extraction des coordonnees des points caracteristiques dans une image contenant une carte de saillance

    Author: xxxxxxxx
    Created: XX/XX/XXXX
    Modified:
    - author: N. Cuperlier
    - description: specific file creation
    - date: 06/11


    Theoritical description:
    - \f$  LaTeX equation: none \f$

    Description:
    le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe
    a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisse et l'ordonnee
    du max sur ces deux premiers neurone. Lorsque tous les points sont traites, le troisieme neurone s'active et peut etre connecte a un break;

    Macro:
    -none

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools:
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG

#include <Typedef/liste_chaine.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <Struct/nios_pt_carac.h>

typedef struct MyData_f_give_nios_focus_point
{
  int gpe_image;
  int gpe_memory;
  int gpe_transition_detect;
  int gpe_transition_detect_lieu;
  int gpe_head;
  int index_pt;
  int nb_pt;

} MyData_f_give_nios_focus_point;


void new_give_nios_focus_point(int gpe)
{
  MyData_f_give_nios_focus_point *my_data;
  int gpe_image = -1, gpe_head = -1,gpe_memory=-1;
  int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
  int i, l;


  if (def_groupe[gpe].nbre < 4)
  {
    EXIT_ON_ERROR("(new_give_nios_focus_point) Group size must be 4");
  }

  i = 0;
  l = find_input_link(gpe, i);
  while (l != -1)
  {
    if (strcmp(liaison[l].nom, "transition_detect") == 0)
      gpe_transition_detect = liaison[l].depart;

    if (strcmp(liaison[l].nom, "transition_detect_lieu") == 0)
      gpe_transition_detect_lieu = liaison[l].depart;

    if (strcmp(liaison[l].nom, "image") == 0)
      gpe_image = liaison[l].depart;

    if (strcmp(liaison[l].nom, "head") == 0)
    	gpe_head = liaison[l].depart;
    if (strcmp(liaison[l].nom, "memory") == 0)
			gpe_memory = liaison[l].depart;
    i++;
    l = find_input_link(gpe, i);
  }
if (gpe_memory == -1)
    EXIT_ON_ERROR("(new_give_nios_focus_point) Missing memory link\n");
  if (gpe_image == -1)
    EXIT_ON_ERROR("(new_give_nios_focus_point) Missing image link\n");

  if (gpe_transition_detect == -1)
    EXIT_ON_ERROR("(new_give_nios_focus_point) Missing transition_detect link\n");

  if (gpe_transition_detect_lieu == -1  || gpe_head == -1)
  {
    fprintf(stderr,
    "WARNING in (new_give_nios_focus_point) : No head or transition_detect_lieu link -> memory_focus_point will not be used\n");
  }

  my_data = (MyData_f_give_nios_focus_point *) malloc(sizeof(MyData_f_give_nios_focus_point));
  my_data->gpe_image = gpe_image;
  my_data->gpe_memory=gpe_memory;
  my_data->gpe_transition_detect = gpe_transition_detect;
  my_data->gpe_transition_detect_lieu = gpe_transition_detect_lieu;
  my_data->gpe_head = gpe_head;
  def_groupe[gpe].data = my_data;
}


void function_nios_give_focus_point(int gpe)
{
  MyData_f_give_nios_focus_point *my_data;
  int gpe_image = -1, gpe_head = -1,gpe_memory=-1;
  int gpe_transition_detect = -1, gpe_transition_detect_lieu = -1;
  int  x, y, sx, sy;
  float z;
  float head_h, head_v;
  int x_shifted, y_shifted;
  int index_pt=0;
  int nb_pt=0;
  data_NIOS_grabimages *data;

  printf("function_nios_give_focus_point(%s): Entering function\n", def_groupe[gpe].no_name);

  /*recupere les donnees du groupe*/
  if (def_groupe[gpe].data == NULL)
    EXIT_ON_ERROR("(function_nios_give_focus_point) Error retrieving data from f_NIOS_grabimages (NULL pointer)\n");
  my_data = (MyData_f_give_nios_focus_point *) def_groupe[gpe].data;
  gpe_image = my_data->gpe_image;
  gpe_transition_detect = my_data->gpe_transition_detect;
  gpe_transition_detect_lieu = my_data->gpe_transition_detect_lieu;
  gpe_head = my_data->gpe_head;
  index_pt=my_data->index_pt; /* index du pt crac courant*/

  /*TODO a modifier: recupere les datas du groupe d'entree*/
  if (def_groupe[gpe_image].data == NULL)
       EXIT_ON_ERROR("(function_nios_give_focus_point) Error retrieving data (NULL pointer)\n");
  data=(data_NIOS_grabimages *) def_groupe[gpe_image].data;
  nb_pt=data->nb_pt; /*recupere le nb de pt total pour cette image*/
  printf("function_nios_give_focus_point nb_pt=%d\n",nb_pt );

  sx =data->sx ;/*((prom_images_struct *) def_groupe[gpe_image].ext)->sx;*/
  sy = data->sy;/*((prom_images_struct *) def_groupe[gpe_image].ext)->sy;*/

  if (isequal(neurone[def_groupe[gpe_transition_detect].premier_ele].s2,1.))
  {
    printf("f_nios_give_focus_point(%s): transition detect -> reset index_pt sx = %d, sy = %d\n", def_groupe[gpe].no_name, sx, sy);
    /*new image RAZ de l'index: */
    index_pt=my_data->index_pt=0;
  }

  if((data->pts_carac)==NULL)
 	  {
 		  printf("f_nios_give_focus_point(%s) error: (data->pts_carac[%d]==NULL\n",def_groupe[gpe].no_name,index_pt);
 	  }
  /* TODO tester si encore des pts a traiter*/
  if (index_pt<nb_pt)
  {
   /*recupere les infos x, y, act et index pour mapping sur neurone...*/

	  x=data->pts_carac[index_pt].posx;
	  y=data->pts_carac[index_pt].posy;
 head_h = neurone[def_groupe[gpe_head].premier_ele].s;
      head_v = neurone[def_groupe[gpe_head].premier_ele + 1].s;
if (gpe_transition_detect_lieu >= 0 && gpe_memory >= 0 && def_groupe[gpe_memory].ext != NULL && gpe_head >= 0 && isdiff(neurone[def_groupe[gpe_transition_detect_lieu].premier_ele].s,1.0))
						{




      x_shifted = head_h * ((prom_images_struct *) def_groupe[gpe_memory].ext)->sx;
      y_shifted = head_v *  ((prom_images_struct *) def_groupe[gpe_memory].ext)->sy;

      x_shifted = x_shifted + sx * (((float) x) / sx - 0.5);
      y_shifted = y_shifted + sy * (((float) y) / sy - 0.5);

      if (x_shifted < 0)
        x_shifted = x_shifted + data->sx;
      else if (x_shifted > data->sx)
        x_shifted = x_shifted - data->sx;


      if (y_shifted < 0)
        y_shifted = y_shifted + data->sy;
      else if (y_shifted > data->sy)
        y_shifted = y_shifted - data->sy;

 /* On fait la multiplication de l'intensite du point de foca par le masque garde dans memory permettant de savoir si le point a deja
       ete traite dans ce panorama (recouvrement avec une autre image du meme panorama) */
     z = (((prom_images_struct *) def_groupe[gpe_memory].ext)->images_table[0][x_shifted + y_shifted * ((prom_images_struct *) def_groupe[gpe_memory].ext)->sx] / 255) * data->pts_carac[index_pt].intensity;

if(z>0)
      printf("f_nios_give_focus_point(%s): New point using memory,  index=%d -> x=%d, y=%d,x_act=%f,y_act=%f, head=%f, x_shifted=%d, y_shifted=%d \n", def_groupe[gpe].no_name,index_pt, x, y,((float) x) / data->sx, ((float) y) / data->sy, head_h, x_shifted, y_shifted);
else
   printf("f_nios_give_focus_point(%s): this point already exists in memory (same panorama),  index=%d -> x=%d, y=%d,x_act=%f,y_act=%f, head=%f, x_shifted=%d, y_shifted=%d \n", def_groupe[gpe].no_name,index_pt, x, y,((float) x) / data->sx, ((float) y) / data->sy, head_h, x_shifted, y_shifted);





}else{
z=data->pts_carac[index_pt].intensity;
 printf("f_nios_give_focus_point(%s): New point from new panorama index=%d -> x=%d, y=%d, head=%f, \n", def_groupe[gpe].no_name,index_pt, x, y, head_h);

}

    /* Mise a jour des activites de la boite avec les coordonnees du point gagnant */

    neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = x/(float)data->sx;/*((point3d *) maillon_fort->data)->x;*/

    neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = y/(float)data->sy;/*((point3d *) maillon_fort->data)->y;*/

    neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 =z;//0.;
    if(def_groupe[gpe].nbre > 3)
      neurone[def_groupe[gpe].premier_ele + 3].s = neurone[def_groupe[gpe].premier_ele + 3].s1 = neurone[def_groupe[gpe].premier_ele + 3].s2 = index_pt;/*((point3d *) maillon_fort->data)->z/255.0;*/

    def_groupe[gpe].ext=data->pts_carac[index_pt].rho_teta_image;
    my_data->index_pt=++index_pt;

}
  else
  {
    printf("f_nios_give_focus_point(%s): No more points\n", def_groupe[gpe].no_name);

    neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = 0.;
    index_pt=my_data->index_pt=0;
  }

   printf("f_nios_give_focus_point(%s): Leaving function\n", def_groupe[gpe].no_name);

}

