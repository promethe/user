/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  f_memory_focus_point.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 02/05

 - author: J. Hirel
 - description: big correction, clarification, memory management
 - date: 11/2008

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 Le groupe memorise les points traites par f_give_focus_point pendant un panorama.

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG*/

#include <Typedef/liste_chaine.h>
#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>
#include <net_message_debug_dist.h>

typedef struct MyData_f_memory_focus_point {
   int gpe_focus;
   int gpe_transition_detect;
   int gpe_head;
   int gpe_image;
   int rayon;
   float debat_h;
   float debat_v;
   int sx;
   int sy;

} MyData_f_memory_focus_point;

void new_memory_focus_point(int gpe)
{
   MyData_f_memory_focus_point *my_data;
   int gpe_transition_detect = -1, gpe_focus = -1, gpe_head = -1, gpe_image = -1;
   char param[32];
   float debat_h = 0., debat_v = 0.;
   int i, l, sx = -1, sy = -1, rayon = 1.;

   i = 0;
   l = find_input_link(gpe, i);

   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "transition_detect") == 0)
      {
         gpe_transition_detect = liaison[l].depart;
      }

      if (strcmp(liaison[l].nom, "image") == 0)
      {
         gpe_image = liaison[l].depart;
      }

      if (strcmp(liaison[l].nom, "focus") == 0)
      {
         gpe_focus = liaison[l].depart;
      }

      if (strcmp(liaison[l].nom, "head") == 0)
      {
         gpe_head = liaison[l].depart;
      }

      if (prom_getopt(liaison[l].nom, "h", param) == 2) debat_h = atof(param);

      if (prom_getopt(liaison[l].nom, "v", param) == 2) debat_v = atof(param);

      if (prom_getopt(liaison[l].nom, "r", param) == 2) rayon = atoi(param);

      if (prom_getopt(liaison[l].nom, "sx", param) == 2) sx = atoi(param);
      if (prom_getopt(liaison[l].nom, "sy", param) == 2) sy = atoi(param);

      i++;
      l = find_input_link(gpe, i);
   }

   if ((gpe_image == -1 && (sx < 0 || sy < 0)) || gpe_transition_detect == -1 || gpe_focus == -1 || gpe_head == -1)
   {
      fprintf(stderr, "ERROR in new_memory_focus_point(%s): Missing input link\n", def_groupe[gpe].no_name);
      exit(1);
   }

   if (isequal(debat_h, 0.) || isequal(debat_v, 0.))
   {
      fprintf(stderr, "ERROR in new_memory_focus_point(%s): Missing link with option -h or -v (camera angles)\n", def_groupe[gpe].no_name);
      exit(1);
   }

   my_data = (MyData_f_memory_focus_point *) malloc(sizeof(MyData_f_memory_focus_point));
   my_data->gpe_transition_detect = gpe_transition_detect;
   my_data->gpe_focus = gpe_focus;
   my_data->gpe_head = gpe_head;
   my_data->gpe_image = gpe_image;
   my_data->rayon = rayon;
   my_data->debat_h = debat_h;
   my_data->debat_v = debat_v;
   my_data->sx = sx;
   my_data->sy = sy;

   def_groupe[gpe].data = (MyData_f_memory_focus_point *) my_data;
}

void function_memory_focus_point(int gpe)
{
   MyData_f_memory_focus_point *my_data;
   int gpe_image = -1;
   int gpe_transition_detect = -1, gpe_focus = -1, gpe_head = -1;
   prom_images_struct *img_in, *img_out;
   int x_shifted, y_shifted;
   int i, j, i2, j2, sx, sy, rayon = 1.;
   float head_h, head_v;

   if (def_groupe[gpe].data == NULL)
   {
      kprints("ERROR in f_memory_focus_point(%s): Error retrieving data (NULL pointer)\n", def_groupe[gpe].no_name);
      exit(1);
   }

   my_data = (MyData_f_memory_focus_point *) def_groupe[gpe].data;
   gpe_focus = my_data->gpe_focus;
   gpe_transition_detect = my_data->gpe_transition_detect;
   gpe_head = my_data->gpe_head;
   gpe_image = my_data->gpe_image;
   rayon = my_data->rayon;

   if (gpe_image >= 0)
   {
      img_in = (prom_images_struct *) def_groupe[gpe_image].ext;

      if (img_in == NULL)
      {
         PRINT_WARNING("ERROR in f_memory_focus_point(%s): No image in input group (ext == NULL)\n", def_groupe[gpe].no_name);
         return;
      }
      sx = img_in->sx;
      sy = img_in->sy;
   }
   else
   {
      sx = my_data->sx;
      sy = my_data->sy;

   }
   if (def_groupe[gpe].ext == NULL)
   {
      img_out = calloc_prom_image(1, sx * 360 / my_data->debat_h, sy * 360 / my_data->debat_v, 1);
      def_groupe[gpe].ext = img_out;

      dprints("f_memory_focus_point(%s): Size of output image -> sx = %d, sy = %d\n", def_groupe[gpe].no_name, img_out->sx, img_out->sy);
   }
   else img_out = (prom_images_struct *) def_groupe[gpe].ext;

   sx = img_out->sx;
   sy = img_out->sy;

   if (isequal(neurone[def_groupe[gpe_transition_detect].premier_ele].s2, 1.))
   {
      dprints("f_memory_focus_point(%s): transition detect -> reseting all points\n", def_groupe[gpe].no_name);

      /* Tous les points sont mis a 255 : accessibles */
      for (i = 0; i < sx * sy; i++)
      {
         img_out->images_table[0][i] = 255;
      }
   }

   /* repositionnement du point de focalisation dans des coordonnees absolues en utilisant la direction de la camera */
   head_h = neurone[def_groupe[gpe_head].premier_ele].s;
   head_v = neurone[def_groupe[gpe_head].premier_ele + 1].s;

   x_shifted = head_h * sx;
   y_shifted = head_v * sy;

   x_shifted = x_shifted + sx * (neurone[def_groupe[gpe_focus].premier_ele].s - 0.5);
   y_shifted = y_shifted + sy * (neurone[def_groupe[gpe_focus].premier_ele + 1].s - 0.5);

   if (x_shifted < 0) x_shifted = x_shifted + sx;
   else if (x_shifted > sx) x_shifted = x_shifted - sx;

   if (y_shifted < 0) y_shifted = y_shifted + sy;
   else if (y_shifted > sy) y_shifted = y_shifted - sy;

   dprints("f_memory_focus_point(%s): position of the focus point in the panorama -> x = %d, y = %d\n", def_groupe[gpe].no_name, x_shifted, y_shifted);

   if (isdiff(neurone[def_groupe[gpe_focus].premier_ele + 2].s, 1.)) for (i = y_shifted - rayon; i < y_shifted + rayon; i++)
      for (j = x_shifted - rayon; j < x_shifted + rayon; j++)
      {
         /* et puis on integre le point active avec le rayon definit */
         if ((i - y_shifted) * (i - y_shifted) + (j - x_shifted) * (j - x_shifted) < rayon * rayon)
         {
            if (i < 0) i2 = i + sy;
            else if (i >= sy) i2 = i - sy;
            else i2 = i;

            if (j < 0) j2 = j + sx;
            else if (j >= sx) j2 = j - sx;
            else j2 = j;

            img_out->images_table[0][i2 * sx + j2] = 0;
         }
      }
}

void destroy_memory_focus_point(int gpe)
{
   dprints("destroy_memory_focus_point(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data != NULL)
   {
      free((MyData_f_memory_focus_point *) def_groupe[gpe].data);
      def_groupe[gpe].data = NULL;
   }

   if (def_groupe[gpe].ext != NULL)
   {
      free((prom_images_struct *) def_groupe[gpe].ext);
      def_groupe[gpe].ext = NULL;
   }

   dprints("destroy_memory_focus_point(%s): Leaving function\n", def_groupe[gpe].no_name);
}
