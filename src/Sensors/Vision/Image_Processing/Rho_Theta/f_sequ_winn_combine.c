/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sequ_winn_combine.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 02/05

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe  
a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisce et l'ordonne
du max sur ces deux premiers neurone. Lorsque tout les point sont traite, le troisieme neurone s'active et peut erte connecte a un break;

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

#include <Typedef/liste_chaine.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include <string.h>
/*#define DEBUG*/
typedef struct MyData_f_sequ_winn_combine
{
    int gpe_image;
    int gpe_gradH;
    int gpe_transition_detect;
    int sx;
    int sy;
    int R_exclusion;
    int mode_campano;
    liste_chaine *liste_maillon_resolu;
    liste_chaine *liste_maillon_a_resoudre;
    int nb_maillon_a_resoudre;

} MyData_f_sequ_winn_combine;

void function_sequ_winn_combine(int gpe)
{
    MyData_f_sequ_winn_combine *my_data;
    int cpt, gpe_image = -1, gpe_gradH = -1;
    int gpe_transition_detect = -1;
    int i, l, sx, sy;
    liste_chaine *liste_maillon_resolu = NULL;
    liste_chaine *liste_maillon_a_resoudre = NULL;
    liste_chaine *maillon_a_resoudre = NULL;
    point3d *thepoint = NULL;
    int nb_maillon_a_resoudre;
    liste_chaine *maillon_fort;
    liste_chaine *avant_maillon_fort;
    char param[32];
    float max;
    int R_exclusion = 0;
    int x = 0, y = 0, mode_campano = 0;
    if (def_groupe[gpe].data == NULL)
    {
        if (def_groupe[gpe].nbre != 3)
        {
            printf("il faut deux neurones pour f_sequ_winn_combine(%d)\n",
                   gpe);
            exit(0);

        }
        i = 0;
        l = find_input_link(gpe, i);

        while (l != 0)
        {
            l = find_input_link(gpe, i);
            if (strcmp(liaison[l].nom, "transition_detect") == 0)
            {
                gpe_transition_detect = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "image") == 0)
            {
                gpe_image = liaison[l].depart;
            }

            if (strcmp(liaison[l].nom, "gradH") == 0)
            {
                gpe_gradH = liaison[l].depart;
            }


            if (prom_getopt(liaison[l].nom, "R", param) == 2)
            {
                R_exclusion = atoi(param);
            }

            if (prom_getopt(liaison[l].nom, "CP", param) >= 1)
            {
                mode_campano = 1;
            }

            i++;
            l = find_input_link(gpe, i);

        }
        if (gpe_image == -1 || gpe_transition_detect == -1 || gpe_gradH == -1)
        {
            printf
                ("il manque un groupe en entree de f_sequ_winn_combine(%d)\n",
                 gpe);
            exit(0);
        }
        if (def_groupe[gpe_image].ext == NULL)
        {
            printf
                ("pas d'image ds l'ext du groupe image de f_sequ_winn_combine(%d)\n",
                 gpe);
            exit(0);
        }


        liste_maillon_resolu = NULL;
        liste_maillon_a_resoudre =
            (liste_chaine *) malloc(sizeof(liste_chaine *));
        liste_maillon_a_resoudre->suivant = NULL;
        thepoint = (point3d *) malloc(sizeof(point3d));
        liste_maillon_a_resoudre->data = (point3d *) thepoint;

        maillon_a_resoudre = NULL;
        sx = ((prom_images_struct *) (def_groupe[gpe_image].ext))->sx;
        sy = ((prom_images_struct *) (def_groupe[gpe_image].ext))->sy;
        my_data =
            (MyData_f_sequ_winn_combine *)
            malloc(sizeof(MyData_f_sequ_winn_combine));
        my_data->sx = sx;
        my_data->sx = sy;
        my_data->R_exclusion = R_exclusion;
        my_data->mode_campano = mode_campano;
        my_data->gpe_image = gpe_image;
        my_data->gpe_gradH = gpe_gradH;
        my_data->gpe_transition_detect = gpe_transition_detect;
        my_data->liste_maillon_resolu = liste_maillon_resolu;
        my_data->liste_maillon_a_resoudre = liste_maillon_a_resoudre;
        my_data->nb_maillon_a_resoudre = nb_maillon_a_resoudre = 0;


        def_groupe[gpe].data = (MyData_f_sequ_winn_combine *) my_data;
    }
    else
    {
        my_data = (MyData_f_sequ_winn_combine *) def_groupe[gpe].data;
        sx = my_data->sx;
        sy = my_data->sy;
        gpe_image = my_data->gpe_image;
        gpe_gradH = my_data->gpe_gradH;
        gpe_transition_detect = my_data->gpe_transition_detect;
        liste_maillon_resolu = my_data->liste_maillon_resolu;
        liste_maillon_a_resoudre = my_data->liste_maillon_a_resoudre;
        nb_maillon_a_resoudre = my_data->nb_maillon_a_resoudre;
        R_exclusion = my_data->R_exclusion;
        mode_campano = my_data->mode_campano;

    }
    /*code effectif */

    /*ya un bug */
    sx = ((prom_images_struct *) (def_groupe[gpe_image].ext))->sx;
    sy = ((prom_images_struct *) (def_groupe[gpe_image].ext))->sy;
    if (isequal(neurone[def_groupe[gpe_transition_detect].premier_ele].s2, 1))
    {
        /*concat liste maillon a resoudre et liste maillon resolu (qui est alors null) si besoin */
        printf("transition detect\n");
        maillon_a_resoudre = liste_maillon_a_resoudre;
        while (maillon_a_resoudre->suivant != NULL)
            maillon_a_resoudre = maillon_a_resoudre->suivant;

        maillon_a_resoudre->suivant = liste_maillon_resolu;
        liste_maillon_resolu = NULL;

        /* extraire les gagant */

        maillon_a_resoudre = liste_maillon_a_resoudre;
        nb_maillon_a_resoudre = 0;


        printf("R_e: %d,   sx=%d   ,   sy=%d\n", R_exclusion, sx, sy);
        if (2 * R_exclusion >= sx || 2 * R_exclusion >= sy)
        {
            printf("error in f_sequ_winn_combine:R_exclusion trop grand");
        }
        for (x = 0 + R_exclusion; x < sx - R_exclusion; x++)
            for (y = 0 + R_exclusion; y < sy - R_exclusion; y++)
            {
                i = x + y * sx;

                /*creer la liste ou la rempli */
                if ((mode_campano == 0) ||
                    (y > R_exclusion + (10. / 240.) * sy
                     && ((x < (300. / 1540.) * sx)
                         || (x > (545. / 1540.) * sx))
                     && (y < sy - (40. / 240.) * sy - R_exclusion))
                    || (y > R_exclusion + (45. / 240.) * sy
                        && (y < sy - (40. / 240.) * sy - R_exclusion)))
                    if (((prom_images_struct *) (def_groupe[gpe_image].ext))->
                        images_table[0][i] > def_groupe[gpe].seuil)
                    {
                        if (maillon_a_resoudre->suivant == NULL)
                        {
                            maillon_a_resoudre->suivant =
                                (liste_chaine *)
                                malloc(sizeof(liste_chaine *));
                            maillon_a_resoudre->suivant->suivant = NULL;
                            thepoint = (point3d *) malloc(sizeof(point3d));
                            maillon_a_resoudre->suivant->data =
                                (point3d *) thepoint;
#ifdef DEBUG
                            printf("create maillon\n");
#endif
                        }
                        nb_maillon_a_resoudre++;
                        /*refresh maillon_a_resoudre */
                        ((point3d *) (maillon_a_resoudre->data))->x =
                            ((float) (i % sx)) / sx;
                        ((point3d *) (maillon_a_resoudre->data))->y =
                            ((float) (i / sx)) / sy;
                        ((point3d *) (maillon_a_resoudre->data))->z =
                            (((prom_images_struct *) (def_groupe[gpe_image].
                                                      ext))->
                             images_table[0][i]) *
                            ((((prom_images_struct *) (def_groupe[gpe_gradH].
                                                       ext))->
                              images_table[0][i % sx]));
#ifdef DEBUG
                        printf("x=%f,y=%f,z=%f\n",
                               ((point3d *) (maillon_a_resoudre->data))->x,
                               ((point3d *) (maillon_a_resoudre->data))->y,
                               ((point3d *) (maillon_a_resoudre->data))->z);
#endif
                        maillon_a_resoudre = maillon_a_resoudre->suivant;
                    }

            }

#ifdef DEBUG
        printf("fin creation liste. nb_maill=%d\n", nb_maillon_a_resoudre);
#endif
    }

    nb_maillon_a_resoudre--;
    if (nb_maillon_a_resoudre >= 0)
    {
        /*prend le premier maillon de la liste a resoudre et le met dans la liste de maillon resolu*
           maillon_a_resoudre=liste_maillon_a_resoudre;
           liste_maillon_a_resoudre=liste_maillon_a_resoudre->suivant;
           maillon_a_resoudre->suivant=liste_maillon_resolu;
           liste_maillon_resolu=maillon_a_resoudre; */

        maillon_a_resoudre = liste_maillon_a_resoudre;
        maillon_fort = maillon_a_resoudre;
        avant_maillon_fort = liste_maillon_a_resoudre;
        max = ((point3d *) (maillon_a_resoudre->data))->z;
        cpt = 0;
        while ((maillon_a_resoudre->suivant != NULL)
               && (cpt < nb_maillon_a_resoudre))
        {
            if (((point3d *) (maillon_a_resoudre->suivant->data))->z > max)
            {
                maillon_fort = maillon_a_resoudre->suivant;
                avant_maillon_fort = maillon_a_resoudre;
                max = ((point3d *) (maillon_a_resoudre->suivant->data))->z;
            }

            maillon_a_resoudre = maillon_a_resoudre->suivant;
            cpt++;
        }
        if (maillon_fort == liste_maillon_a_resoudre)
        {
            /*prend le premier maillon de la liste a resoudre et le met dans la liste de maillon resolu */
            maillon_a_resoudre = liste_maillon_a_resoudre;
            liste_maillon_a_resoudre = liste_maillon_a_resoudre->suivant;
            maillon_a_resoudre->suivant = liste_maillon_resolu;
            liste_maillon_resolu = maillon_a_resoudre;
        }
        else
        {
            avant_maillon_fort->suivant = maillon_fort->suivant;
            maillon_fort->suivant = liste_maillon_resolu;
            liste_maillon_resolu = maillon_fort;
        }

        printf("x=%f  y=%f  max=%f\n", ((point3d *) (maillon_fort->data))->x,
               ((point3d *) (maillon_fort->data))->y, max);
        

        neurone[def_groupe[gpe].premier_ele].s =
            ((point3d *) (maillon_fort->data))->x;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            ((point3d *) (maillon_fort->data))->y;
        neurone[def_groupe[gpe].premier_ele].s1 =
            ((point3d *) (maillon_fort->data))->x;
        neurone[def_groupe[gpe].premier_ele + 1].s1 =
            ((point3d *) (maillon_fort->data))->y;
        neurone[def_groupe[gpe].premier_ele].s2 =
            ((point3d *) (maillon_fort->data))->x;
        neurone[def_groupe[gpe].premier_ele + 1].s2 =
            ((point3d *) (maillon_fort->data))->y;


        if (nb_maillon_a_resoudre > 0)
        {
            neurone[def_groupe[gpe].premier_ele + 2].s = 0.;
            neurone[def_groupe[gpe].premier_ele + 2].s1 = 0.;
            neurone[def_groupe[gpe].premier_ele + 2].s2 = 0.;
        }
        else
        {
            neurone[def_groupe[gpe].premier_ele + 2].s = 0.;
            neurone[def_groupe[gpe].premier_ele + 2].s1 = 0.;
            neurone[def_groupe[gpe].premier_ele + 2].s2 = 0.;
#ifdef DEBUG
            printf("dernier point, break est active\n");
#endif
        }

    }
    else
    {
#ifdef DEBUG
        printf("plus de point a traiter pour f_sequentiel_winner(%d)\n", gpe);
#endif
        /*neurone[def_groupe[gpe].premier_ele].s=0.;
           neurone[def_groupe[gpe].premier_ele+1].s=0.; */
        neurone[def_groupe[gpe].premier_ele + 2].s = 1.;    /*
                                                               neurone[def_groupe[gpe].premier_ele].s1=0.;
                                                               neurone[def_groupe[gpe].premier_ele+1].s1=0.; */
        neurone[def_groupe[gpe].premier_ele + 2].s1 = 1.;   /*
                                                               neurone[def_groupe[gpe].premier_ele].s2=0.;
                                                               neurone[def_groupe[gpe].premier_ele+1].s2=0.; */
        neurone[def_groupe[gpe].premier_ele + 2].s2 = 1.;
    }

    my_data->liste_maillon_resolu = liste_maillon_resolu;
    my_data->liste_maillon_a_resoudre = liste_maillon_a_resoudre;

    my_data->nb_maillon_a_resoudre = nb_maillon_a_resoudre;
}
