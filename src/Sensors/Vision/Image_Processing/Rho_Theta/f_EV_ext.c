/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_EV_ext.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: Maillard
- description: specific file creation
- date: 02/05

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Lmocal variables:
-none

Global variables:
-none 

Internal Tools:
-init_look_up_polaire()

External Tools: 
-none

Links:
- type: algo / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
realise la transfo log(rho)-theta et met le resulat sur des neurones (matrice carree)
Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <string.h>

#include "tools/include/init_look_up_polaire.h"

/*#define DEBUG*/

void function_EV_ext(int gpe_sortie)
{
    unsigned char *Image = NULL;
    float *Image_float = NULL;
    int *TableLookup = NULL;
    int GpeX, GpeY, GpeR, Gper, GpeIm;
    int Sx, Sy;
    int i, j, deb, tempo, Index;
    int PositionX, PositionY;
    int decalage, pos, correspondant;
    int TailleGpe;
    float TempMax = 0.;
    int nb_band;
    prom_images_struct *ImageLocale = NULL, *ImageEntre = NULL;
#ifdef DEBUG
#ifndef AVEUGLE
    TxPoint point;
    float nmax = 0., nmin = 1.;
#endif
#endif

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

#ifdef DEBUG
    printf("---- function_EV_ext ----\n");
#endif

    if (def_groupe[gpe_sortie].data == NULL)
    {
        float R, r;

        /* test de la taille */
        if (def_groupe[gpe_sortie].taillex != def_groupe[gpe_sortie].tailley)
        {
            printf("Le groupe %s doit etre une matrice carree\a\n",
                   def_groupe[gpe_sortie].no_name);
            printf("Exit in function_EV_ext\n");
            exit(-1);
        }

        /* recuperation info */
        GpeX = GpeY = GpeR = Gper = GpeIm = -1;
        for (Index = 0; Index < nbre_liaison; Index++)
            if (liaison[Index].arrivee == gpe_sortie)
            {
                if (!strcmp(liaison[Index].nom, "X"))
                    GpeX = liaison[Index].depart;
                if (!strcmp(liaison[Index].nom, "Y"))
                    GpeY = liaison[Index].depart;
                if (!strcmp(liaison[Index].nom, "R"))
                    GpeR = liaison[Index].depart;
                if (!strcmp(liaison[Index].nom, "r"))
                    Gper = liaison[Index].depart;
                if (!strcmp(liaison[Index].nom, "ext"))
                    GpeIm = liaison[Index].depart;
            }
        if ((GpeX == -1) || (GpeY == -1) || (GpeR == -1) || (Gper == -1)
            || (GpeIm == -1))
        {
            printf
                ("Le groupe % doit etre connecte a 5 autre groupes avec les liens <X>, <Y>, <R>, <r> et <Im>\n",
                 gpe_sortie);
            printf("Exit in function_EV_ext\n");
            exit(-1);
        }

        R = neurone[def_groupe[GpeR].premier_ele].s;
        r = neurone[def_groupe[Gper].premier_ele].s;
        /*printf("GpeX=%d GpeY=%d GpeR=%d Gper=%d GpeIm=%d\n",GpeX,GpeY,GpeR,Gper,GpeIm); */
        if (def_groupe[GpeIm].ext == NULL)
        {
            printf("L'extension du groupe d'entree est nulle\n");
            printf("Exit in function_EV_ext\n");
            /*exit(-1); */
            return;
        }
        ImageEntre = (prom_images_struct *) def_groupe[GpeIm].ext;
        Sx = ImageEntre->sx;
        Sy = ImageEntre->sy;
        nb_band = ImageEntre->nb_band;
        Image = ImageEntre->images_table[0];

        if (nb_band != 4 && nb_band != 1)
        {
            printf("nb_band non gere par f_Gpe_ext gpe %d\n", gpe_sortie);
            exit(1);
        }

        /* initialisation table look up */
        TableLookup = init_look_up_polaire(Sx, Sy, r, (int) R, gpe_sortie);

        def_groupe[gpe_sortie].data =
            (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[gpe_sortie].data == NULL)
        {
            printf("Not enought memory!\n");
            printf("Exit in function_EV_ext\n");
            exit(-1);
        }
        ImageLocale = (prom_images_struct *) (def_groupe[gpe_sortie].data);
        ImageLocale->image_number = 0;
        ImageLocale->sx = Sx;
        ImageLocale->sy = Sy;
        ImageLocale->nb_band = nb_band;
        ImageLocale->images_table[1] = (unsigned char *) TableLookup;
        ImageLocale->images_table[2] =
            (unsigned char *) malloc(3 * sizeof(int));
        if (ImageLocale->images_table[2] == NULL)
        {
            printf
                ("impossible d'allouer de la memoire dans f_EV_ext gpe : %d\n",
                 gpe_sortie);
            exit(1);
        }
        ((int *) (ImageLocale->images_table[2]))[0] = GpeX;
        ((int *) (ImageLocale->images_table[2]))[1] = GpeY;
        ((int *) (ImageLocale->images_table[2]))[2] = GpeIm;
    }
    else
    {
        ImageLocale = (prom_images_struct *) (def_groupe[gpe_sortie].data);
        Sx = ImageLocale->sx;
        Sy = ImageLocale->sy;
        nb_band = ImageLocale->nb_band;
        GpeIm = ((int *) (ImageLocale->images_table[2]))[2];
        GpeX = ((int *) (ImageLocale->images_table[2]))[0];
        GpeY = ((int *) (ImageLocale->images_table[2]))[1];
        TableLookup = (int *) ImageLocale->images_table[1];
        Image =
            ((prom_images_struct *) def_groupe[GpeIm].ext)->images_table[0];
    }

    /* cherche la position X et Y */
    TempMax = 0.0;
    PositionX = -1;
    for (i = 1, j = def_groupe[GpeX].premier_ele; i < def_groupe[GpeX].nbre;
         i++, j++)
        if (neurone[j].s1 > TempMax)
            PositionX = i;

    TempMax = 0.0;
    PositionY = -1;
    for (i = 1, j = def_groupe[GpeY].premier_ele; i < def_groupe[GpeY].nbre;
         i++, j++)
        if (neurone[j].s1 > TempMax)
            PositionY = i;


    deb = def_groupe[gpe_sortie].premier_ele;
    TailleGpe =
        def_groupe[gpe_sortie].taillex * def_groupe[gpe_sortie].tailley;
    /* reset des neurones */
    for (i = 0, j = deb; i < TailleGpe; i++, j++)
    {
        neurone[j].s = neurone[j].s1 = 0.;
        neurone[j].s2 = 1.;     /* s2 vaut 1 partout la ou le groupe suivant devra apprendre */
    }

    tempo = Sx * Sy;
    if ((PositionX > 0) && (PositionY > 0))
    {
        decalage = PositionX + PositionY * Sx - (tempo + Sx) / 2;
        if (nb_band == 1)
        {
            for (i = 0; i < tempo; i++)
            {
                pos = i - decalage;
                if (pos >= 0 && pos < tempo)
                {
                    correspondant = deb + TableLookup[pos];
                    neurone[correspondant].s = neurone[correspondant].s1 =
                        (float) Image[i] / 255.;
                }
            }
        }
        else
        {
            Image_float = (float *) Image;
            for (i = 0; i < tempo; i++)
            {
                pos = i - decalage;
                if (pos >= 0 && pos < tempo)
                {
                    correspondant = deb + TableLookup[pos];
                    neurone[correspondant].s = neurone[correspondant].s1 =
                        Image_float[i] / 255.;
                }
            }
        }
    }
    else
    {
        /*Dessin d'un pattern particulier, puisqu'il n'y a pas de position de pt de foc specifiee */
        printf
            ("f_EV_ext tous les neurones a 0 car pas de pt de focus specifie \n");
        TempMax = 0.0;
        for (i = deb; i < TailleGpe + deb; i++)
        {
            /*TempMax = 1.0 - TempMax; */
            neurone[i].s = neurone[i].s1 = /*TempMax */ 0.0;
        }
    }


#ifdef DEBUG
#ifndef AVEUGLE

  /*------------------------*/
    /*dessin de l'Ev sur image1 */
  /*-------------------------*/
    /*recherche max min */
    for (i = 0; i < def_groupe[gpe_sortie].taillex; i++)
    {
        for (j = 0; j < def_groupe[gpe_sortie].tailley; j++)
        {
            pos = i + j * def_groupe[gpe_sortie].taillex;
            if (neurone[deb + pos].s > nmax)
                nmax = neurone[deb + pos].s;
            if (neurone[deb + pos].s < nmin)
                nmin = neurone[deb + pos].s;
            /*printf("%f ",neurone[deb+pos].s); */
        }
        /*printf("\n"); */
    }

    /*Dessin */
    for (i = 0; i < def_groupe[gpe_sortie].taillex; i++)
    {
        point.x = i + 20 + Sx;
        for (j = 0; j < def_groupe[gpe_sortie].tailley; j++)
        {
            pos = i + j * def_groupe[gpe_sortie].taillex;
            point.y = j;
            TxDessinerPoint(&image1,
                            (int) (neurone[deb + pos].s * (16. / nmax)),
                            point);
        }
        TxFlush(&image1);
    }

    printf("\n\nnmax=%f nmin=%f\n", nmax, nmin);
#endif
#endif

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Tine in function_EV_ext\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}
