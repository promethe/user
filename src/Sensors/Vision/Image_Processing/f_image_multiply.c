/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_image_multiply.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: D BAILLY
- description: specific file creation
- date: 28/02/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  creates the image rusulting of the multiplication pixel by pixel of the input
  images

Macro:
-PI

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / blueiological / neural
- description: none/ XXX
- input expected greenroup: none/xxx
- where are the data?: none/xxx

Comments:

Known blueugs: none (yet!)

Todo:see authored fortestingreen and commentingreen the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/convert.h>

#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <public_tools/Vision.h>

#define NB_MAX_GPES_IMAGES 100

typedef struct data_image_multiply
{
  int nb_gpe_images;
  int nb_gpe_neurons;
  int gpes_images[NB_MAX_GPES_IMAGES];
  int gpes_neurons[NB_MAX_GPES_IMAGES];
  int sorties_desirees[NB_MAX_GPES_IMAGES];
}Data_image_multiply;


void new_image_multiply(int index_of_gpe)
{
  Data_image_multiply *data=NULL;
  char *param=NULL;
  int gpes_images[NB_MAX_GPES_IMAGES];
  int gpes_neurons[NB_MAX_GPES_IMAGES];
  int sorties_desirees[NB_MAX_GPES_IMAGES];
  int i=0, nb_gpe_images=0, nb_gpe_neurons=0;
  int l = -1;
  
  dprints("entering %s (%s line %d)\n", __FUNCTION__, __FIE__, __LINE__);
  
  if (def_groupe[index_of_gpe].data == NULL)
  {
    /*Finding the link */
    i=0;
    nb_gpe_images = 0;
    nb_gpe_neurons = 0;
    gpes_images[0] = -1;
    gpes_neurons[0] = -1;
    l = find_input_link(index_of_gpe, i);
    while (l != -1)
    {
      if (strstr(liaison[l].nom, "sync") != NULL)
      {
      }
      else if((param=strstr(liaison[l].nom, "-s")) != NULL || (param=strstr(liaison[l].nom, "-S")) != NULL)
      {
        sorties_desirees[nb_gpe_neurons] = 0;
        sorties_desirees[nb_gpe_neurons] = atoi(param+2);
        gpes_neurons[nb_gpe_neurons] = liaison[l].depart;
        printf("groupe %s trouvé en entree avec la sortie %d selectionnee\n", def_groupe[liaison[l].depart].nom, sorties_desirees[nb_gpe_neurons]);
        nb_gpe_neurons++;
      }
      else
      {
        gpes_images[nb_gpe_images] = liaison[l].depart;
        printf("groupe %s trouvé en entree %d\n", def_groupe[liaison[l].depart].nom,nb_gpe_images);
        nb_gpe_images++;
      }
      i++;
      l = find_input_link(index_of_gpe, i);
    }
    /* Pour verifier l'existence du lien */
    if (gpes_images[0] == -1 && gpes_neurons[0] == -1)  EXIT_ON_ERROR("le groupe n'a pas d'entree !");

    /*allocation memoire pour la sauvegarde des donnees sur le lien */
    data = (Data_image_multiply *)ALLOCATION(Data_image_multiply);
    
    /* parametres par default */
    memcpy(data->gpes_images, gpes_images, sizeof(data->gpes_images));
    memcpy(data->gpes_neurons, gpes_neurons, sizeof(data->gpes_neurons));
    memcpy(data->sorties_desirees, sorties_desirees, sizeof(data->sorties_desirees));
    data->nb_gpe_images = nb_gpe_images;
    data->nb_gpe_neurons = nb_gpe_neurons;
    def_groupe[index_of_gpe].data = data;
  }
  
  dprints("exiting %s (%s %d)\n", __FUNCTION__, __FILE__, __LINE__);
}

void function_image_multiply(int index_of_gpe)
{
  Data_image_multiply *data=NULL;
  int i=0, j=0, nb_gpe_images, nb_gpe_neurons;
  int premier_ele;
  
  prom_images_struct *image_input_struct = NULL, *image_output_struct = NULL;
  float *input_image_f=NULL, *output_image_f=NULL, f;
  unsigned char *input_image, *output_image;

  dprints("entering %s (%s line %d)\n", __FUNCTION__, __FIE__, __LINE__);
  
  data = (Data_image_multiply *)def_groupe[index_of_gpe].data;
  nb_gpe_images = data->nb_gpe_images;
  nb_gpe_neurons = data->nb_gpe_neurons;
  image_output_struct = (prom_images_struct *) def_groupe[index_of_gpe].ext;
  
  
  if (def_groupe[index_of_gpe].ext == NULL)
  {
    image_input_struct = (prom_images_struct *)def_groupe[data->gpes_images[0]].ext;
    
    for(i=0; i < nb_gpe_images; i++)
    {
      if(def_groupe[data->gpes_images[i]].ext == NULL)
      {
        PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
        return ;
      }
      if(((prom_images_struct *)def_groupe[data->gpes_images[i]].ext)->sx != image_input_struct->sx || ((prom_images_struct *)def_groupe[data->gpes_images[i]].ext)->sy != image_input_struct->sy)
      {
        EXIT_ON_ERROR("l'image du groupe %s n'est pas de taille %dx%d", def_groupe[data->gpes_images[i]].nom, image_input_struct->sx, image_input_struct->sy);
      }
      if(((prom_images_struct *)def_groupe[data->gpes_images[i]].ext)->nb_band != image_input_struct->nb_band)
      {
        EXIT_ON_ERROR("l'image du groupe %s n'a pas le même nombre de bandes (%d != %d)", def_groupe[data->gpes_images[i]].nom, ((prom_images_struct *)def_groupe[data->gpes_images[i]].ext)->nb_band, image_input_struct->nb_band);
      }
    }
    
    if(def_groupe[index_of_gpe].nbre != 1)
    {
      if(def_groupe[index_of_gpe].taillex != (int)image_input_struct->sx || def_groupe[index_of_gpe].taillex != (int)image_input_struct->sx)
        EXIT_ON_ERROR("le groupe n'a pas la bonne taille: il doit etre soit de taille 1 soit %dx%d", image_input_struct->sx, image_input_struct->sy);
    }
    
    for(i=0; i < nb_gpe_neurons; i++)
    {
      if(def_groupe[data->gpes_neurons[i]].taillex != (int)image_input_struct->sx || def_groupe[data->gpes_neurons[i]].tailley != (int)image_input_struct->sy)
      {
        EXIT_ON_ERROR("la taille du groupe %s n'est pas %dx%d", def_groupe[data->gpes_neurons[i]].nom, image_input_struct->sx, image_input_struct->sy);
      }
    }
    
    /* allocation de memoire */
    image_output_struct = (prom_images_struct *)calloc_prom_image(1, image_input_struct->sx, image_input_struct->sy, image_input_struct->nb_band);
    if (image_output_struct == NULL)  EXIT_ON_ERROR("impossible d'allouer la memoire");
    def_groupe[index_of_gpe].ext = image_output_struct;
  }
  
  if(image_output_struct->nb_band == 4)
  {
    output_image_f = (float *)image_output_struct->images_table[0];
    for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
      output_image_f[i] = 1.0;
      
    for(j=0; j<nb_gpe_images; j++)
    {
      image_input_struct = (prom_images_struct *) def_groupe[data->gpes_images[j]].ext;
      input_image_f = (float *)image_input_struct->images_table[0];
  
      for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
      {
        output_image_f[i] = output_image_f[i] * input_image_f[i];
      }
    }
    for(j=0; j<nb_gpe_neurons; j++)
    {
      premier_ele = def_groupe[data->gpes_neurons[j]].premier_ele;
      switch(data->sorties_desirees[j])
      {
        case 0:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            output_image_f[i] = output_image_f[i] * neurone[premier_ele+i].s;
          }
          break;
        case 1:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            output_image_f[i] = output_image_f[i] * neurone[premier_ele+i].s1;
          }
          break;
        case 2:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            output_image_f[i] = output_image_f[i] * neurone[premier_ele+i].s2;
          }
          break;
        default:
          EXIT_ON_ERROR("l'option -s%d n'est pas une option valide pour le lien entre %s et %s", data->sorties_desirees[j], def_groupe[data->gpes_neurons[j]].nom, def_groupe[index_of_gpe].nom);
      }
    }
    if(def_groupe[index_of_gpe].nbre != 1)
    {
      premier_ele = def_groupe[index_of_gpe].premier_ele;
      for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
      {
        neurone[premier_ele+i].s = neurone[premier_ele+i].s1 = neurone[premier_ele+i].s2 = output_image_f[i];
      }
    }
  }
  else
  {
    output_image = image_output_struct->images_table[0];
    memset(output_image, 255, image_output_struct->sx * image_output_struct->sy * sizeof(unsigned char));
      
    for(j=0; j<nb_gpe_images; j++)
    {
      image_input_struct = (prom_images_struct *) def_groupe[data->gpes_images[j]].ext;
      input_image = image_input_struct->images_table[0];
  
      for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
      {
        f = (float)output_image[i];
        f = f * (float)input_image[i] / 255.;
        output_image[i] = (unsigned char)((int)f);
      }
    }
    for(j=0; j<nb_gpe_neurons; j++)
    {
      premier_ele = def_groupe[data->gpes_neurons[j]].premier_ele;
      switch(data->sorties_desirees[j])
      {
        case 0:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            f = (float)output_image[i];
            f = f * neurone[premier_ele+i].s;
            output_image[i] = (unsigned char)((int)f);
          }
          break;
        case 1:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            f = (float)output_image[i];
            f = f * neurone[premier_ele+i].s1;
            output_image[i] = (unsigned char)((int)f);
          }
          break;
        case 2:
          for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
          {
            f = (float)output_image[i];
            f = f * neurone[premier_ele+i].s2;
            output_image[i] = (unsigned char)((int)f);
          }
          break;
        default:
          EXIT_ON_ERROR("l'option -s%d n'est pas une option valide pour le lien entre %s et %s", data->sorties_desirees[j], def_groupe[data->gpes_neurons[j]].nom, def_groupe[index_of_gpe].nom);
      }
    }
    if(def_groupe[index_of_gpe].nbre != 1)
    {
      premier_ele = def_groupe[index_of_gpe].premier_ele;
      for(i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
      {
        neurone[premier_ele+i].s = neurone[premier_ele+i].s1 = neurone[premier_ele+i].s2 = (float)output_image[i] / 255.;
      }
    }
  }
  
  dprints("exiting %s (%s %d)\n", __FUNCTION__, __FILE__, __LINE__);
}
