/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_image_difference.c 
\brief Soustraction de 2 images

Author: Mickael Maillard
Created: 01/07/2004
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction permet de soustraire deux images ayant la meme taille.
Les deux images à soustraire, peuvent être soit en floatant N&B soit en floatant couleur.
(but : creer un DOG par soustraction de deux images convoluees par un mask gaussien de longueur differente pour chaque image).

Parametres : -UP, image + dans la soustraction
	     -DOWN, image - dans la soustraction

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Struct/prom_images_struct.h>

#undef DEBUG

void function_image_difference(int Gpe)
{
    int Gpe_Up = -1, Gpe_Down = -1;
    float *outputImage, *up_image, *down_image;
    int i, row, column;
    int sx, sy, band, ba, nb_band;
    char *chaine = NULL;


#ifdef DEBUG
    printf("Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif


    if (def_groupe[Gpe].ext == NULL)
    {
        /*recherche du numero de groupe entrant et gestion des erreurs si inexistant */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                chaine = liaison[i].nom;
                if (strstr(chaine, "-UP") != NULL)
                    Gpe_Up = liaison[i].depart;
                else if (strstr(chaine, "-DOWN") != NULL)
                    Gpe_Down = liaison[i].depart;
                else
                {
                    printf
                        ("%s : parametre des liens entrants incorrects (-UP et -DOWN)\n",
                         __FUNCTION__);
                    exit(0);
                }
            }
        }
        if (Gpe_Up == -1 || Gpe_Down == -1)
        {
            printf
                ("%s : il manque une liaison entrante ou un parametre est incorrect\n",
                 __FUNCTION__);
            exit(0);
        }

        if (def_groupe[Gpe_Up].ext == NULL
            || def_groupe[Gpe_Down].ext == NULL)
        {
            printf
                ("%s : un des pointeurs des liens entrants n'a pas ete initialise\n",
                 __FUNCTION__);
            /*exit(0); */
            return;
        }

        /*on verifie que le format de l'image est correct */
        if (!(((prom_images_struct *) def_groupe[Gpe_Up].ext)->nb_band == 4 ||
              ((prom_images_struct *) def_groupe[Gpe_Up].ext)->nb_band == 12
              || ((prom_images_struct *) def_groupe[Gpe_Down].ext)->nb_band ==
              4
              || ((prom_images_struct *) def_groupe[Gpe_Down].ext)->nb_band ==
              12))
        {
            printf
                ("%s les images doivent etre en floatant couleurs ou N&B...\n",
                 __FUNCTION__);
            exit(0);
        }



        if (((prom_images_struct *) def_groupe[Gpe_Up].ext)->sx !=
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->sx
            || ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sy !=
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->sy)
        {
            printf
                ("%s : les 2 images a soustraire doivent etre de meme taille...\n",
                 __FUNCTION__);
            exit(0);
        }

        /*allocation de memoire pour la structure résulante */
        def_groupe[Gpe].ext =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf("%s:%d : ALLOCATION IMPOSSIBLE ... \n", __FUNCTION__,
                   __LINE__);
            exit(0);
        }


        /*recuperation des infos */
        nb_band = ((prom_images_struct *) def_groupe[Gpe_Up].ext)->nb_band;
        sx = ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe_Up].ext)->sy;

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            ((prom_images_struct *) def_groupe[Gpe_Down].ext)->
            images_table[0];

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            ((prom_images_struct *) def_groupe[Gpe_Up].ext)->images_table[0];



        /*stockage des infos */
        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = nb_band;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(sx * sy * nb_band);
        if (((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] ==
            NULL)
        {
            printf
                ("%s (%d): Gpe : %d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",
                 __FUNCTION__, __LINE__, Gpe);
            exit(0);
        }

    }
    else
    {
        sx = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        nb_band = ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band;
    }

    /*recup pointeurs/infos */
    outputImage =
        (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
        images_table[0];
    band = (int) (nb_band / 4);

    up_image =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[2]);
    down_image =
        (float *) (((prom_images_struct *) def_groupe[Gpe].ext)->
                   images_table[1]);


    /*calcul du resulat */
    for (row = 0; row < sy; row++)
        for (column = 0; column < sx; column++)
            for (ba = 0; ba < band; ba++)
                *(outputImage + row * sx * band + column * band + ba) =
                    (*(up_image + row * sx * band + column * band + ba) -
                     *(down_image + row * sx * band + column * band + ba));



#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif

    return;
}
