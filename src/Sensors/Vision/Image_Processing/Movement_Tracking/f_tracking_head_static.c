/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief realise suivi d'une cible

Author: Maillard
Created: 2002
Modified:
- author: BASTY
- description: specific file creation
- date: 20/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Suit une cible en mvt dont la position de depart est speifie par les coordonnees de la fonction presente.
 Ici la cible est une zone contenant un visage, les coordonnes fournies proviennent
 de la fonction f_hough_for_head
 Fonction par difference de regions et comparaissant avec seulement quelques vosins du pixel
Macro:
-none

Local variables:
-none

Global variables:

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: Une image
- where are the data?:

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.
Place le resultat quelque part

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEF_TAILLE_X 6
#define DEF_TAILLE_Y 6
#define DEF_NB_PIXEL 3

#define DEBUG

typedef struct
{
   int taille_fen_x, taille_fen_y; /* Dimension de la fenetre */
   int taille_im_x, taille_im_y;   /* Dimension de l'image */
   int val_coin;               /* valeur en pixel du coin de la fenetre depuis le coin sup-gauche de l'image */
   TxPoint coin_fen;
   unsigned char *fen;
   int InputGpe, display, InputGpeCoord;
   int tab[5], nb_pixel;
   float *tabscore;
   int nb_band;

   int coord_init_x_hough, coord_init_y_hough;

} fenetre_track;



void function_tracking_head_static(int Gpe) /* Version qui fonctionne pour 3 fenetres independantes */
{
   fenetre_track *fen = NULL;
   int i, j, k, h, pa, pb, x0, y0, link1 = -1, link2 = -1;
   int minvert = -1, minhoriz = -1, hvert = -1, hhoriz = -1, dirhoriz =
                                   -1, dirvert = -1;
   int xmax, ymax;
   float *tabscore = NULL, coef = 0.2, score;  /* coef < 1 --> la memoire est + forte */
   /*  char *string = NULL, *st=NULL; */
   void *prev_ptr = NULL;
   unsigned char *image = NULL;

#ifndef AVEUGLE
#ifdef DEBUG
   TxPoint cercle;
#endif
#endif

   int nb_band = 0;
   int pos = 0;
   int speedx = 0, speedy = 0;
   int tata;

   char resultat[256];
   hough_struct *t_hough = NULL;

   /* printf("----- function extraction fenetre -----\n"); */
   if (def_groupe[Gpe].data == NULL)
   {

      /*neurone[def_groupe[Gpe].premier_ele].s2 =
         neurone[def_groupe[Gpe].premier_ele].s1 =
         neurone[def_groupe[Gpe].premier_ele].s = 0.5; */


      /*printf("1er passage\n"); */
      /*allocation de la structure fenetre et remplissage des champs */
      fen = (fenetre_track *) malloc(sizeof(fenetre_track));
      if (fen == NULL)
      {
         EXIT_ON_ERROR("Impossible d'allouer la structure de la fenetre groupe %d (function_tracking_filter)\n",Gpe);
      }

      /*recuperation des infos sur les diff�ents liens */
      link1 = find_input_link(Gpe, 0);
      link2 = find_input_link(Gpe, 1);

      if ((link1 == -1) || (link2 == -1))
      {
         EXIT_ON_ERROR("*****> Il n'y a pas deux groupe en entree du Gpe %d\n",Gpe);
      }

      if ((prom_getopt(liaison[link1].nom, "-image", resultat) == 2) && (prom_getopt(liaison[link2].nom, "-coord", resultat) == 2))
      {
         fen->InputGpe = liaison[link1].depart;
         fen->InputGpeCoord = liaison[link2].depart;
         i = link1;
      }
      else
      {
         fen->InputGpe = liaison[link2].depart;
         fen->InputGpeCoord = liaison[link1].depart;
         i = link2;
      }

      dprints("*****> gpe_entree 1 = %d   link1=%d\n", fen->InputGpe, link1);
      dprints("*****> gpe_entree 2 = %d   link2=%d\n", fen->InputGpeCoord,link2);
      dprints("Premier passage\n");

      /*recuperation image et affectation */
      prev_ptr = (prom_images_struct *) def_groupe[fen->InputGpe].ext;

      /*affectation des tailles de l'image */
      fen->taille_im_x = ((prom_images_struct *) (prev_ptr))->sx;
      fen->taille_im_y = ((prom_images_struct *) (prev_ptr))->sy;
      nb_band = fen->nb_band = ((prom_images_struct *) (prev_ptr))->nb_band;


      if (prev_ptr == NULL)
      {
         EXIT_ON_ERROR("PB : pas d'images dans le groupe %i\n", fen->InputGpe);
      }

      if ((fen->nb_band != 3) && (fen->nb_band != 1))
      {
         EXIT_ON_ERROR("Le nb_band n'est pas correct dans le Gpe %d : function_tracking_filter ne prend que des images char (couleur ou non)\n", Gpe);
      }

      /*Affectation des tailles de la fenetre en dur */
      fen->taille_fen_x = DEF_TAILLE_X;
      fen->taille_fen_y = DEF_TAILLE_Y;

      if (prom_getopt(liaison[i].nom, "-Th", resultat) == 2)
      {
         fen->taille_fen_x = atoi(resultat);
      }
      if (prom_getopt(liaison[i].nom, "-Tv", resultat) == 2)
      {
         fen->taille_fen_y = atoi(resultat);
      }

      /*recuperation des coordonnees du groupe present */
      t_hough =(hough_struct *) ((prom_images_struct *) def_groupe[fen->InputGpeCoord].data);

      /*affectation des coordonnees pour la fenetre */
      x0 = fen->coin_fen.x = (int) (fen->taille_im_x / 2 - fen->taille_fen_x / 2);
      y0 = fen->coin_fen.y = (int) (fen->taille_im_y / 2 - fen->taille_fen_y / 2);

      x0 = fen->coin_fen.x = fen->coord_init_x_hough = t_hough->x;
      y0 = fen->coin_fen.y = fen->coord_init_y_hough = t_hough->y;

      /*Affectation des pixel voisins a compare en dur */
      fen->nb_pixel = DEF_NB_PIXEL;
      if (prom_getopt(liaison[i].nom, "-P", resultat) == 2)
      {
         fen->nb_pixel = atoi(resultat);
      }

      /*Display ou non du resultat pour la fenetre */
      fen->display = 0;
      if (prom_getopt(liaison[i].nom, "-D", resultat) == 2)
      {
         fen->display = atoi(resultat);
      }

      /*Allocation de la fenetre */
      fen->fen =(unsigned char *) malloc(nb_band * fen->taille_fen_x *fen->taille_fen_y * sizeof(unsigned char));
      if (fen->fen == NULL)
      {
         EXIT_ON_ERROR("Impossible d'allouer la fenetre groupe %d (function_tracking_filter)\n",Gpe);
      }
      /* Taille de l'image */

      /*on remplit les derniers champs */
      fen->val_coin =nb_band * (fen->coin_fen.y * fen->taille_im_x + fen->coin_fen.x);

      /* Initialisation du tableau permettant d'avoir l'offset des pixels voisins */
      fen->tab[0] = 0;
      fen->tab[1] = -nb_band * fen->taille_im_x;  /* 1 pixel en haut */
      fen->tab[2] = nb_band * fen->taille_im_x;   /* 1 pixel en bas */
      fen->tab[3] = -nb_band; /* 1 pixel a gauche */
      fen->tab[4] = nb_band;  /* 1 pixel a droite */


      fen->tabscore = (float *) calloc((1 + 4 * fen->nb_pixel), sizeof(float));   /*on compare avec l'image sans deplacement et avec des deplacements de nb_pixel dans 4 directions */
      if (fen->tabscore == NULL)
      {
         EXIT_ON_ERROR("Impossible d'allouer le tabscore, groupe %d (function_tracking_filter)\n",Gpe);
      }
      tabscore = fen->tabscore;

      /*init de la fenetre de depart */
      image = ((prom_images_struct *) (prev_ptr))->images_table[0];
      for (j = 0; j < fen->taille_fen_y; j++)
      {
         pa = j * nb_band * fen->taille_fen_x;
         pb = fen->val_coin + j * nb_band * fen->taille_im_x;
         for (i = 0; i < nb_band * fen->taille_fen_x; i++)
            fen->fen[pa + i] = image[pb + i];
      }

      /*on stock la structure initialise dans le data du groupe */
      def_groupe[Gpe].data = (void *) fen;

   }
   else
   {
      /*printf("Autre passage\n"); */
      fen = (fenetre_track *) def_groupe[Gpe].data;

      /*recuperation des coordonnees du groupe present */
      t_hough = (hough_struct *) (def_groupe[fen->InputGpeCoord].data);

      dprints("InputGpeCoord %d\n", fen->InputGpeCoord);
      dprints("Coordonnees de la transformee de hough\n");
      dprints("x :%d , y %d\n", t_hough->x, t_hough->y);
      dprints("Coordonnees initiales de la transformee de hough\n");
      dprints("coord_init_x :%d , coord_init_y %d\n", fen->coord_init_x_hough, fen->coord_init_y_hough);

      prev_ptr = (prom_images_struct *) def_groupe[fen->InputGpe].ext;

      /*remise a jour des coordonnees de l'ellipse trouvee */
      if ((fen->coord_init_x_hough == t_hough->x) && (fen->coord_init_y_hough == t_hough->y))
      {
         x0 = fen->coin_fen.x;
         y0 = fen->coin_fen.y;
         nb_band = fen->nb_band;
         /*printf("Remise a jour de x0 et y0 par la transforme de hough\n"); */
      }
      else
      {
         x0 = fen->coin_fen.x = fen->coord_init_x_hough = t_hough->x;
         y0 = fen->coin_fen.y = fen->coord_init_y_hough = t_hough->y;
         /*printf("Remise a jour de x0 et y0 par la fenetre\n"); */
      }

      /** TEMPORAIRE SHIRAKAWA **/

      /*printf("temporaire - shirakawa: grpoue %d , f_tracking_head_static , valeur de x0 et y0: %d \t%d\n",Gpe,x0,y0); */
#ifndef AVEUGLE
      TxDessinerCercle(&image1, rouge, TxVide, fen->coin_fen, 10, 1);
#endif

      /** FIN TEMPORAIRE SHIRAKAWA **/

      dprints("Valeur de x0 :%d , Valeur de y0 : %d\n", x0, y0);
      dprints("Passage dans le .data, allocation hough structure\n");

      /*t_hough->x = x0;
         t_hough->y = y0; */

      image = ((prom_images_struct *) (prev_ptr))->images_table[0];
      tabscore = fen->tabscore;

      /*printf("Passage de parmetre\n"); */

      /* printf("debut %d %d\n",x0,y0); */
      /*on remet a zero les scores */
      for (j = 0; j < (1 + 4 * fen->nb_pixel); j++)
         *(fen->tabscore + j) = (float) 0.;

      /*calcul des bords max */
      xmax = fen->taille_im_x - fen->taille_fen_x - fen->nb_pixel - 1;
      ymax = fen->taille_im_y - fen->taille_fen_y - fen->nb_pixel - 1;

      /* On compare la fenetre de depart avec d'autres */

      /* Enregistrement des scores dans un tableau */
      for (h = 0; h <= fen->nb_pixel; h++)    /*h -> pixel voisin */
      {
         if (h)
            for (i = 1; i < 5; i++) /*4 directions pour les pixels voisins */
               for (j = 0; j < fen->taille_fen_y; j++)
               {
                  pa = j * nb_band * fen->taille_fen_x;
                  pb = fen->val_coin + j * nb_band * fen->taille_im_x;
                  for (k = 0; k < nb_band * fen->taille_fen_x; k++)
                  {
                     pos = pb + k + h * fen->tab[i];
                     if (pos < fen->taille_im_x * fen->taille_im_y * nb_band)    /*c'est pas tres bien gere */
                        tabscore[i + 4 * (h - 1)] =tabscore[i + 4 * (h - 1)] +fabs((float)(fen->fen[pa + k] - image[pos]));
                  }
               }
         else
            for (j = 0; j < fen->taille_fen_y; j++) /*cas ou la fenetre ne bouge pas */
            {
               pa = j * nb_band * fen->taille_fen_x;
               pb = fen->val_coin + j * nb_band * fen->taille_im_x;
               for (k = 0; k < nb_band * fen->taille_fen_x; k++)
               {
                  pos = pb + k + fen->tab[0];
                  if (pos <
                        fen->taille_im_x * fen->taille_im_y * nb_band)
                     tabscore[0] =
                        tabscore[0] +
                        fabs((float) (fen->fen[pa + k] - image[pos]));
               }
            }
      }

      /* Comparaison */
      minvert = tabscore[0];
      minhoriz = tabscore[0];
      hhoriz = 0;
      hvert = 0;
      for (i = 1; i <= fen->nb_pixel; i++)
         for (j = 1; j <= 2; j++)
         {
            if (tabscore[j + 2 + 4 * (i - 1)] < minhoriz)
            {
               dirhoriz = j;
               hhoriz = i;
               minhoriz = tabscore[j + 2 + 4 * (i - 1)];
            }
            if (tabscore[j + 4 * (i - 1)] < minvert)
            {
               dirvert = j;
               hvert = i;
               minvert = tabscore[j + 4 * (i - 1)];
            }
         }

      /* On garde en memoire la valeur du coin et l'image de la meilleure fenetre */
      switch (dirhoriz)
      {
         case 0:
            break;
         case 1:
            if (x0 > fen->nb_pixel + 1)
            {
               x0 -= hhoriz;
               speedx = -hhoriz;
               /*fen->val_coin += fen->tab[3] * hhoriz; */
            }
            break;
         case 2:
            if (x0 < xmax)
            {
               x0 += hhoriz;
               speedx = hhoriz;
               /*fen->val_coin += fen->tab[4] * hhoriz; */
            }
            break;
      }

      switch (dirvert)
      {
         case 0: break;
         case 1: if (y0 > fen->nb_pixel + 1)
            {
               y0 -= hvert;
               speedy = -hvert;
               /*fen->val_coin += fen->tab[1] * hvert; */
            }
            break;
         case 2: if (y0 < ymax)
            {
               y0 += hvert;
               speedy = hvert;
               /*fen->val_coin += fen->tab[2] * hvert; */
            }
            break;
      }

      tata = fen->val_coin;
      fen->val_coin =nb_band * ((speedy + fen->coin_fen.y) * fen->taille_im_x + fen->coin_fen.x + speedx);
      fen->coin_fen.x += speedx;
      fen->coin_fen.y += speedy;


      score = 0.0;
      /*calcul avec la memoire */
      for (j = 0; j < fen->taille_fen_y; j++)
      {
         pa = j * nb_band * fen->taille_fen_x;
         pb = tata + j * nb_band * fen->taille_im_x;
         for (i = 0; i < nb_band * fen->taille_fen_x; i++)
         {
            pos = pb + i;
            if (pos < fen->taille_im_x * fen->taille_im_y * nb_band)    /*c'est tjs aussi mal gere */
            {
               fen->fen[pa + i] = (fen->fen[pa + i] + coef * image[pos]) / (coef + 1);
               /*fen->fen[pa + i] + (image[pb + i]-fen->fen[pa+i])/2; */
               score += fen->fen[pa + i];
            }
         }
      }


      /*fen->coin_fen.x = x0;
         fen->coin_fen.y = y0; */

      /*printf("fin %d %d\n",x0,y0); */

      /*  neurone[def_groupe[Gpe].premier_ele].s2 =
      	  neurone[def_groupe[Gpe].premier_ele].s1 =
      	  neurone[def_groupe[Gpe].premier_ele].s = neurone[def_groupe[Gpe].premier_ele].s1 + 0.01*(0. - ( ((float)x0-127.)/(float)fen->taille_im_x));*/
   }
#ifndef AVEUGLE
#ifdef DEBUG
   if (fen->display)
   {
      cercle.x = x0;
      cercle.y = y0;
      TxDessinerRectangle(&image1, rouge, TxVide, cercle, fen->taille_fen_x, fen->taille_fen_y, 1);
      TxDessinerRectangle(&image2, rouge, TxVide, cercle, fen->taille_fen_x, fen->taille_fen_y, 1);
      TxFlush(&image1);
   }
#endif
#endif
   /*neurone[def_groupe[Gpe].premier_ele].s2 =
      neurone[def_groupe[Gpe].premier_ele].s1 =
      neurone[def_groupe[Gpe].premier_ele].s = (float)1 - ((float)x0/(float)fen->taille_im_x); */


   /*(neurone[def_groupe[Gpe].premier_ele].s1*255)-127 */

}
