/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file f_wta_special.c 
\brief 

Author: LAGARDE Matthieu
Created: 06/07/2006
Modified:

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* #define DEBUG 1 */

struct WTA_SPECIAL_STRUCT
{
    int neur;
    float potentiel;
};

void f_wta_special(int numero)
{
    int first, nr;
    int i, j;
    struct WTA_SPECIAL_STRUCT *data;
    /*
       Coeff :
       ->entree : numero du neurone en entree du lien
       ->val : valeur du lien
       ->s : lien suivant
     */
    type_coeff *lien = NULL;
    int nr_max = 1;
    int idx_min = 9999999, idx_max = 0;

#ifdef DEBUG
    printf("~~~~~~~~~~~entree dans f_wta_special\n");
#endif

    /* nombre de neurones dans le groupe */
    nr = def_groupe[numero].nbre;

    /* 1er neuronne du groupe */
    first = def_groupe[numero].premier_ele;

    if (def_groupe[numero].data == NULL)
    {
        if ((data =
             malloc(nr_max * sizeof(struct WTA_SPECIAL_STRUCT))) == NULL)
        {
            fprintf(stderr,
                    "f_wta_special : Probleme d'allocation memoire\n");
            exit(-1);
        }
        def_groupe[numero].data = (void *) data;
    }
    else
    {
        data = (struct WTA_SPECIAL_STRUCT *) def_groupe[numero].data;
    }

    memset(data, 0, nr_max * sizeof(struct WTA_SPECIAL_STRUCT));

    for (j = first; j < (first + nr); j++)
    {
        float potentiel = 0;

        neurone[j].s = neurone[j].s1 = neurone[j].s2 = 0;

        /* On recupere le potentiel sur les liens d'entree */
        for (lien = neurone[j].coeff; lien != NULL; lien = lien->s)
        {
            potentiel += (lien->val * neurone[lien->entree].s2);
        }
        if (potentiel >= def_groupe[numero].seuil)
        {
            if (idx_min > j)
                idx_min = j;
            if (idx_max < j)
                idx_max = j;

            for (i = 0; i < nr_max; i++)
                if (potentiel >= data[i].potentiel)
                {
                    data[i].neur = j;
                    data[i].potentiel = potentiel;
                    break;
                }
        }
    }
/*   for (i = idx_min; i < idx_max; i++) */
/*     { */
/*       neurone[i].s = neurone[i].s1 = neurone[i].s2 = 1; */
/*     } */
    for (i = 0; i < nr_max; i++)
    {
        neurone[data[i].neur].s = neurone[data[i].neur].s1 =
            data[i].potentiel;
        neurone[data[i].neur].s2 = 1;
    }
#ifdef DEBUG
    printf("~~~~~~~~~~~sortie de f_wta_special\n");
#endif

}
