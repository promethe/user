/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_detect_flow_fast_signed_x.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Cette fonction effectue un calcul simple du flux optique sur l'axe x.
Le calcul est effectue par un produit entre :
	-une difference d'image temporelle signee (im(t+1)-im(t))
	-un calcul de gradient sur x

Utilisation d'un buffer circulaire pour les images
L'image ne doit pas etre en char N&B.(nb_band=1)

Parametre : -nbre d'image dans le buffer (TemporalImagesIntegration) (max =128).
	    -seuil de detection du mouvement (SeuilDetectMvt).
	    -seuil plutot lié au capteur et au nombre de neurones en sortie (SeuilDetection).

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <net_message_debug_dist.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>


typedef struct TAG_STRUCT_MVT_F_S
{
    unsigned char *buffer_images[256];
    int *buffer_grad[256];
    int *ImageResultat;
    int *CarteProjection;
    int nb_buffers;
    int sx;
    int sy;
    int SeuilDetectMvt;
    int SeuilDetection;
    int index_buffer;
    unsigned char *Input_Image;

    float joint_neur[2];
/*	int neurone_gpe_joint;
	int a_bouge;*/
} struct_mvt_f_s;


void function_detect_flow_fast_signed_x(int gpe_sortie)
{
    prom_images_struct *ImagesCaptures;
    /* pointeurs sur les tableaux qui contiendront les images */
    unsigned char *Input_Image;
    int Debut_Gpe;
    int TailleGroupeX, TailleGroupeY;
    int SizeZoneX, SizeZoneY;
    int sx, sy;
    int *ImageResultat;
    int *CarteProjection;
    int temp1, temp2;
    int i, j, m, k, l, n, Nhv, i1, j1, S;
    int TemporalImagesIntegration, SeuilDetectMvt, SeuilDetection;
    int nb_buffers = 1;
    int index_buffer;
    struct_mvt_f_s *my_data;
/*  int neurone_gpe_joint=-1;
  int a_bouge=1;*/
    int row, column, deltaX;
    int temp3, temp4;
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* initialisation locale */
    Debut_Gpe = def_groupe[gpe_sortie].premier_ele;
    TailleGroupeX = def_groupe[gpe_sortie].taillex;
    TailleGroupeY = def_groupe[gpe_sortie].tailley;
    Nhv = TailleGroupeX * TailleGroupeY;


    /* initialisation test */
    if (def_groupe[gpe_sortie].data == NULL)
    {
        int Index;
        int InputGpe = -1;

        TemporalImagesIntegration = 4;
        SeuilDetectMvt = 10;
        SeuilDetection = 10;

        /* recuperations des donnees */
        for (Index = 0; Index < nbre_liaison; Index++)
            if (liaison[Index].arrivee == gpe_sortie)
            {
                if (liaison[Index].nom[0] == 'S')
                {
                    sscanf(liaison[Index].nom, "S%d,%d,%d",
                           &TemporalImagesIntegration, &SeuilDetectMvt,
                           &SeuilDetection);
                    InputGpe = liaison[Index].depart;
                    /*break; */
                }
                /*else if(strstr(liaison[Index].nom,"joint")!=NULL)
                   {
                   neurone_gpe_joint = def_groupe[liaison[Index].depart].premier_ele;
                   } */

            }
        if (InputGpe == -1)
            EXIT_ON_ERROR("Aucune lien <input> n'a pas ete trouve");

        ImagesCaptures = (prom_images_struct *) def_groupe[InputGpe].ext;
        if (ImagesCaptures == NULL)
            EXIT_ON_ERROR("Le groupe precedant n'a pas d'extension");
        if (ImagesCaptures->nb_band != 1)
            EXIT_ON_ERROR("Le groupe precedant a un nb_band incorrect");
        if (TemporalImagesIntegration > 128)
            EXIT_ON_ERROR("Trop d'image pour le buffer des fact_detect_mvt");

        sx = ImagesCaptures->sx;
        sy = ImagesCaptures->sy;

        def_groupe[gpe_sortie].data = malloc(sizeof(struct_mvt_f_s));
        if (def_groupe[gpe_sortie].data == NULL)
            EXIT_ON_ERROR("Not enought memory! (1)");

        my_data = (struct_mvt_f_s *) def_groupe[gpe_sortie].data;

        Input_Image = my_data->Input_Image = ImagesCaptures->images_table[0];

        nb_buffers = 2 * TemporalImagesIntegration;
        my_data->sx = sx;
        my_data->sy = sy;
        my_data->nb_buffers = nb_buffers;

        for (i = 0; i < nb_buffers; i++)
        {
            my_data->buffer_images[i] =
                calloc(sx * sy, sizeof(unsigned char));
            my_data->buffer_grad[i] = calloc(sx * sy, sizeof(int));
            if (my_data->buffer_images[i] == NULL || my_data->buffer_grad[i] == NULL)
                EXIT_ON_ERROR("Not enought memory! (2)");
        }


        my_data->ImageResultat = calloc(sx * sy, sizeof(int));
        if (my_data->ImageResultat == NULL)
            EXIT_ON_ERROR("Not enought memory! (3)");
        ImageResultat = my_data->ImageResultat;

        my_data->CarteProjection = calloc(Nhv, sizeof(int));
        if (my_data->CarteProjection == NULL)
            EXIT_ON_ERROR("Not enought memory! (4)");
        CarteProjection = my_data->CarteProjection;

        my_data->SeuilDetection = SeuilDetection;
        my_data->SeuilDetectMvt = SeuilDetectMvt;

        index_buffer = my_data->index_buffer = 0;
/*	my_data->neurone_gpe_joint = neurone_gpe_joint;
	my_data->a_bouge=a_bouge;*/

        S = sx * sy;

    }
    else
    {
        my_data = (struct_mvt_f_s *) def_groupe[gpe_sortie].data;
        sx = my_data->sx;
        sy = my_data->sy;
        S = sx * sy;
        nb_buffers = my_data->nb_buffers;
        CarteProjection = my_data->CarteProjection;
        ImageResultat = my_data->ImageResultat;
        SeuilDetectMvt = my_data->SeuilDetectMvt;
        SeuilDetection = my_data->SeuilDetection;
        index_buffer = my_data->index_buffer;
        Input_Image = my_data->Input_Image;
        TemporalImagesIntegration = nb_buffers / 2;
        /*neurone_gpe_joint = my_data->neurone_gpe_joint;
           a_bouge=my_data->a_bouge; */
    }

    /*recopie de l'image dans buffer_images circulaire */
    memcpy(my_data->buffer_images[index_buffer], Input_Image, S);
    /*calcul du gradient en x */
    for (row = 0; row < sy; row++)
        for (column = 0; column < sx; column++)
        {
            if (column != 0 && column != (sx - 1))
                deltaX =
                    *(my_data->buffer_images[index_buffer] + row * sx +
                      column + 1) - *(my_data->buffer_images[index_buffer] +
                                      row * sx + column - 1);
            else
            {
                deltaX = 0;
            }
            *(my_data->buffer_grad[index_buffer] + row * sx + column) =
                deltaX;
        }

/*  if(neurone_gpe_joint!=-1)
  {
	  if(a_bouge==1)
	  {
		for(i=1;i<nb_buffers;i++)
			memcpy(my_data->buffer_images[(index_buffer+i)%nb_buffers],Input_Image,S);
  		my_data->index_buffer = 0;
		my_data->a_bouge=0;
		my_data->joint_neur[0] = neurone[neurone_gpe_joint].s1;
		my_data->joint_neur[1] = neurone[neurone_gpe_joint+1].s1;
		return;
	  }
	  
	if(neurone[neurone_gpe_joint].s1<my_data->joint_neur[0]-0.0001 || neurone[neurone_gpe_joint+1].s1<my_data->joint_neur[1]-0.0001
			|| neurone[neurone_gpe_joint].s1>my_data->joint_neur[0]+0.0001 || neurone[neurone_gpe_joint+1].s1>my_data->joint_neur[1]+0.0001)
	{
		my_data->a_bouge=1;	
		my_data->joint_neur[0] = neurone[neurone_gpe_joint].s1;
		my_data->joint_neur[1] = neurone[neurone_gpe_joint+1].s1;
		return;
	}
	my_data->joint_neur[0] = neurone[neurone_gpe_joint].s1;
	my_data->joint_neur[1] = neurone[neurone_gpe_joint+1].s1;
  }*/

    for (i = Debut_Gpe, j = 0; j < Nhv; i++, j++)
    {
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
        CarteProjection[j] = 0;
    }


    /*compute the difference image */
    for (i = 0; i < S; i++)
    {
        temp1 = temp2 = temp3 = temp4 = 0;
        for (j = 1; j < TemporalImagesIntegration + 1; j++)
        {
            temp1 +=
                (int) (my_data->
                       buffer_images[(index_buffer + j) % (nb_buffers)][i]);
            temp2 +=
                (int) (my_data->
                       buffer_images[(index_buffer + j +
                                      TemporalImagesIntegration) %
                                     nb_buffers][i]);

            temp3 +=
                (my_data->buffer_grad[(index_buffer + j) % (nb_buffers)][i]);
            temp4 +=
                (my_data->
                 buffer_grad[(index_buffer + j +
                              TemporalImagesIntegration) % nb_buffers][i]);
        }
        temp1 = ((temp2 - temp1));
        temp1 /= TemporalImagesIntegration;

        temp3 = (temp4 + temp3);
        temp3 /= (2 * TemporalImagesIntegration);
        if (temp1 > SeuilDetectMvt || temp1 < -SeuilDetectMvt)
            ImageResultat[i] = temp1 * temp3;
        else
            ImageResultat[i] = 0;
    }

    /* local initialisations */
    SizeZoneX = sx / TailleGroupeX;
    SizeZoneY = sy / TailleGroupeY;
    /* les projections verticales et horizontales */
    i1 = TailleGroupeY * SizeZoneY;
    j1 = TailleGroupeX * SizeZoneX;
    for (i = 0; i < i1; i++)
    {
        m = i / SizeZoneY;      /* indice Oy pour reseaux */
        for (j = 0; j < j1; j++)
        {
            l = j / SizeZoneX;  /* indice Ox pour reseaux */
            k = sx * i + j;     /* indice dans l'image */
            n = TailleGroupeX * m + l;  /* indice dans projection */
            /*if( ImageResultat[k] > 0 )
               CarteProjection[n] = (unsigned char)( (int)CarteProjection[n] + 1); */
            CarteProjection[n] += ImageResultat[k];
            /*CarteProjection[n] += (unsigned char)ImageResultat[k]; */
        }
    }

    for (i = 0; i < Nhv; i++)
        if ((CarteProjection[i] > SeuilDetection) || (CarteProjection[i] < -SeuilDetection))
            neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = (float) CarteProjection[i] / ((float) (SizeZoneX * SizeZoneY /**255*/ ));
        else
            neurone[Debut_Gpe + i].s = neurone[Debut_Gpe + i].s1 = neurone[Debut_Gpe + i].s2 = 0.0;

    index_buffer = (index_buffer + 1) % nb_buffers;
    my_data->index_buffer = index_buffer;
}
