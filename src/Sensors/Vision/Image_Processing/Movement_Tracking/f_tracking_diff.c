/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**************************************************************

\ingroup libSensors
\defgroup f_tracking_diff f_tracking_diff

\author K. Furet
\date 07/2013
\details
Le groupe effectue un tracking en cherchant une vignette de référence dans le voisinage d'un point donné, et en renvoyant les coordonnées du point le plus probable.

La vignette est initialisée au démarrage autour des coordonnes (normalisées) transmises par l'un des liens.
Elle est extraite de l'image courante envoyée sur l'autre lien (lien "image").
La taille de la zone de recherche autour du point fourni est définie par la taille de la vignette voulue (-h50-w50),
à laquelle s'ajoute une zone de recherche supplémentaire (-tzrx7-tzry7).

Un calcul est fait sur chaque point de la zone de recherche :
La somme des différences entre tous les points du voisinage et les points de la vignette centrée en ce point est faite.
Plus cette somme est petite, moins il y a de différences si la vignette était à ce point, ie plus il y a de chances que notre vignette recherchée soit en ce point.

On trouve donc l'emplacement du minimum, qui nous donne les coordonnées de la vignette recherchée.
On envoie ces coordonnées en sortie de la boite.

Enfin, on actualise la vignette recherchée avec la vignette trouvée, selon le paramètre alpha (-a0.2).
La somme est faite pixel par pixel.
Plus alpha est grand, plus la vignette trouvée est importante. (Prépondérance du présent)
Plus alpha est faible, plus la vignette recherchée est importante. (Prépondérance du passé)

Un lien facultatif permet de remettre à zéro la vignette recherchée (lien "reset").
Utile pour initialiser le suivi, ou en cas de perte de l'objet tracké.
Si la valeur envoyée par ce lien est supérieure à 0.5, la vignette est remise à zéro.


\section Links
- Lien algorithmique portant le nom "image", provenant de l'image source.
- Lien algorithmique facultatif portant le nom "reset", déclenchant la remise à zéro de la vignette.
- Lien algorithmique passant les coordonnées du point autour duquel chercher (type f_give_focus_point, f_compet_ptc_update, etc.), avec les options ci-dessous.

\section Options
- Alpha : Conditionne l'importance du présent ou du passé lors de l'actualisation de la vignette : -aX avec X entre 0 et 1
- Hauteur de la vignette en pixels : -hX avec X dans R+
- Largeur de la vignette en pixels : -wX avec X dans R+
- Taille de la Zone de Recherche sur l'axe des x : -tzrxX avec X dans R+
- Taille de la Zone de Recherche sur l'axe des y : -tzryX avec X dans R+
- Exemple : -a0.2-h50-w50-tzrx7-tzry7

\file
\ingroup libSensors

http://www.doxygen.org
**************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Struct/convert.h>
/*#include "tools/include/macro.h"*/
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
/*#include "tools/include/competition.h"*/
#include <public_tools/Vision.h>
/*#include "Vision/Contours.h"*/
#include "Vision/Image_Processing.h"

typedef struct data_tracking_diff
{
	int input_group;
	int coords_group;
	int neurone;
	int reset;
	int stop_update;
	int taille_zone_recherche_x;
	int taille_zone_recherche_y;
	int mode_update;
	float alpha;
	float * vignette;
	unsigned int nx;
	unsigned int ny;
	unsigned int nb_band;
	unsigned int nx_vignette;
	unsigned int ny_vignette;
	unsigned int nb_band_vignette;
} Data_tracking_diff;



void new_tracking_diff(int Gpe)
{
	Data_tracking_diff * my_data=NULL;
	int input_group = -1, coords_group = -1, reset_group = -1, stop_update_group = -1, nx_vignette = -1, ny_vignette = -1, taille_zone_recherche_x = -1, taille_zone_recherche_y = -1;
	int i, l, mode_update = -1;
	float alpha = 0.5;
	char *string = NULL;
	char param_link[256];
    
	dprints(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);


	/**
	 * On recherche les liens et les Gpes des boites entrantes
	 */
	i = 0;
	l = find_input_link(Gpe, i);
	while (l != -1)
	{
		string = liaison[l].nom;
		if (strstr(string, "sync") != NULL)
			printf("\nlien sync ");
		else if (strcmp(string, "image") == 0)
			input_group = liaison[l].depart;
		else if (strcmp(string, "reset") == 0)
			reset_group = liaison[l].depart;
		else if (strcmp(string, "stop_update") == 0)
			stop_update_group = liaison[l].depart;
		//else if (strstr(string, "coords") != NULL) {
		else {	

			coords_group = liaison[l].depart;
			
			if (prom_getopt(string, "-h", param_link) == 2 || prom_getopt(string, "-H", param_link) == 2)
				ny_vignette = atoi(param_link);
			if (prom_getopt(string, "-w", param_link) == 2 || prom_getopt(string, "-W", param_link) == 2)
				nx_vignette = atoi(param_link);
			if (prom_getopt(string, "-tzrx", param_link) == 2 || prom_getopt(string, "-TZRX", param_link) == 2)
				taille_zone_recherche_x = atoi(param_link);
			if (prom_getopt(string, "-tzry", param_link) == 2 || prom_getopt(string, "-TZRY", param_link) == 2)
				taille_zone_recherche_y = atoi(param_link);
			if (prom_getopt_float(string, "-a", &alpha) == 2 || prom_getopt_float(string, "-A", &alpha) == 2)
				dprints("alpha utilise dans tracking_diff = %f\n", alpha);
		    if (prom_getopt(string, "-update", param_link) == 2 || prom_getopt(string, "-UPDATE", param_link) == 2){
			    mode_update = atoi(param_link);
			    if ( (mode_update != 0) && (mode_update != 1) ) EXIT_ON_ERROR("Update mode must be 0 for global update and 1 for pixel by pixel update, Verify that there is a -update0 or -update1");		
		    }
		}

		i++;
		l = find_input_link(Gpe, i);
	}
	
	if (i > 4)
		EXIT_ON_ERROR("Too many links at the input of %s\n",def_groupe[Gpe].no_name);
	if (input_group == -1)
		EXIT_ON_ERROR("Missing input link at the input of %s\n",def_groupe[Gpe].no_name);
	if (coords_group == -1)
		EXIT_ON_ERROR("Missing coords link at the input of %s\n",def_groupe[Gpe].no_name);
	if (nx_vignette == -1) {
		nx_vignette = 37;
		dprints("Parametre de largeur de la vignette par defaut (%d) utilise dans %s\n", nx_vignette, def_groupe[Gpe].no_name);
	} else {
		if (nx_vignette%2 == 0) {
			nx_vignette--;
			dprints("Parametre de largeur de la vignette pair, remplace par %d dans %s\n", nx_vignette, def_groupe[Gpe].no_name);
		}
	}
	if (ny_vignette == -1) {
		ny_vignette = 37;
		dprints("Parametre de hauteur de la vignette par defaut (%d) utilise dans %s\n", ny_vignette, def_groupe[Gpe].no_name);
	} else {
		if (ny_vignette%2 == 0) {
			ny_vignette--;
			dprints("Parametre de hauteur de la vignette pair, remplace par %d dans %s\n", ny_vignette, def_groupe[Gpe].no_name);
		}
	}
	if (taille_zone_recherche_x == -1) {
		taille_zone_recherche_x = 7;
		dprints("Taille de la zone de recherche en x par defaut (%d) utilise dans %s\n", taille_zone_recherche_x, def_groupe[Gpe].no_name);
	}
	if (taille_zone_recherche_y == -1) {
		taille_zone_recherche_y = 7;
		dprints("Taille de la zone de recherche en y par defaut (%d) utilise dans %s\n", taille_zone_recherche_y, def_groupe[Gpe].no_name);
	}
    
    if (stop_update_group == -1) {
	  PRINT_WARNING("Warning : automatically update of tracking image");	
    }
    
    
	my_data = (Data_tracking_diff *) malloc(sizeof(Data_tracking_diff));
	if (my_data == NULL)    EXIT_ON_ERROR("\nerror in memory allocation");
	
	my_data->input_group = input_group;
	my_data->coords_group = coords_group;
	my_data->neurone = def_groupe[coords_group].premier_ele;
	my_data->reset = def_groupe[reset_group].premier_ele;
	my_data->stop_update = def_groupe[stop_update_group].premier_ele;
	my_data->nx_vignette = nx_vignette;
	my_data->ny_vignette = ny_vignette;
	my_data->vignette = NULL;
	my_data->alpha = alpha;
	my_data->mode_update = mode_update;
	my_data->taille_zone_recherche_x = taille_zone_recherche_x;
	my_data->taille_zone_recherche_y = taille_zone_recherche_y;
	def_groupe[Gpe].data = my_data;

	dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);
}


void function_tracking_diff(int gpe)
{

	Data_tracking_diff * my_data = NULL;
	prom_images_struct * input_ext = NULL;
	prom_images_struct * output_ext = NULL;
	
	int stop_update = 0, mode_update = -1;
	float  alpha=0.5, alpha_c = 0.5, alpha1, reset = 0., test_min = 0., min, alpha_pixc ;
	unsigned char * input = NULL;
	float * vignette = NULL, *init_position_vignette, *position_vignette;
    int taille_zone_recherche_x = 7, taille_zone_recherche_y = 7;
	int  nx, ny, ecart, ecart2, id_min = 0, coord_x, coord_y/*,output_coord_x,output_coord_y*/;
    int min_u,min_v, max_u, max_v;
    int max_i,max_j;
    int u,v, i, j,p ,  ligne;
    int  init_u, init_pp, init_v;
    unsigned char *pp,*p2, *position, *p_init_ligne, *p_ligne;

	/*Variable pour calcul de l'erreur de reconnaissance*/
	float err_reco = 0;
	/*float somme_alphapixc = 0;*/
	float reco = 0;
	
	/******* Recuperation de la structure *******/
	my_data = (Data_tracking_diff *) def_groupe[gpe].data;

	if (def_groupe[gpe].ext == NULL) 
	{

		dprints(" ~~~~~~~ Debut %s ~~~~~~~\n", __FUNCTION__);

		if (my_data == NULL) EXIT_ON_ERROR("Error retrieving data (NULL pointer)\n");

		/* Initialisation de my_data */
		my_data->nx = 0;
		my_data->ny = 0;
		my_data->nb_band = 0;

		/* Si la sortie du groupe precedent est vide, on arrete tout */
		if (def_groupe[my_data->input_group].ext == NULL) {
			PRINT_WARNING("Input group has not sent anything. Without any image, it's impossible to perform anything.\n");
			return;
		}

		/* Allocation de memoire pour la sortie */
		def_groupe[gpe].ext = (void *) malloc(sizeof(prom_images_struct));
		if (def_groupe[gpe].ext == NULL)  EXIT_ON_ERROR("Error in memory allocation of output image\n");

		/* Recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image du groupe courant */
		input_ext = (prom_images_struct *) def_groupe[my_data->input_group].ext;
		nx = my_data->nx = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->sx;
		ny = my_data->ny = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->sy;
		my_data->nb_band = ((prom_images_struct *) (def_groupe[my_data->input_group].ext))->nb_band;

		dprints("%s Taille image %d x %d\n", __FUNCTION__, my_data->nx, my_data->ny);
		dprints("%s Taille vignette %d x %d\n", __FUNCTION__, my_data->nx_vignette, my_data->ny_vignette);
		//if (my_data->nb_band !=4) EXIT_ON_ERROR("%s Input image must be of float type\n", __FUNCTION__);
		
		/* Allocation de memoire pour la sortie */
		output_ext = calloc_prom_image(1, my_data->nx_vignette, my_data->ny_vignette, 4);
		vignette = (float*) output_ext->images_table[0];
		my_data->vignette = vignette ;
	/*	n = my_data->nx_vignette * my_data->ny_vignette; */
	/*	output_ext->images_table[0] = (unsigned char *) calloc(n, sizeof(unsigned char)); */
		
		if (output_ext == NULL || output_ext->images_table[0] == NULL  || input_ext->images_table[0] == NULL)
			EXIT_ON_ERROR("Error in memory allocation in %s line %s\n", def_groupe[gpe].no_name, __LINE__);

		/* Sauvegarde des infos trouvees sur le lien */
		def_groupe[gpe].ext = output_ext;
		reset = 1.0;
	}
	else
	{
	/******* Recuperation des parametres *******/
	nx = (int) my_data->nx;
	ny = (int) my_data->ny;
  
	reset = (float) neurone[my_data->reset].s;
	stop_update = (float) neurone[my_data->stop_update].s;
	input_ext = ((prom_images_struct *) (def_groupe[my_data->input_group].ext));

	vignette = my_data->vignette;
    }
   
    mode_update = my_data->mode_update;
	alpha = my_data->alpha;    
    min=(my_data->ny_vignette*my_data->nx_vignette*255.);
	coord_x = (int) (neurone[my_data->neurone].s * nx);
	coord_y = (int) (neurone[my_data->neurone + 1].s * ny);
	taille_zone_recherche_x = my_data->taille_zone_recherche_x;
	taille_zone_recherche_y = my_data->taille_zone_recherche_y;
	input = (unsigned char *) input_ext->images_table[0];

	ecart = my_data->ny_vignette/2;
	ecart2 = my_data->nx_vignette/2;
	    
/*	if (ecart > ecart2)
		exclusion = ecart;
	else
		exclusion = ecart2;
*/
	/******* Verification des valeurs de coordonnees cherchees *******/
	if (coord_x < ecart2 + taille_zone_recherche_x)               coord_x = ecart2 + taille_zone_recherche_x;
    else 	if (coord_x > nx - ecart2 - taille_zone_recherche_x)		coord_x = nx - ecart2 - taille_zone_recherche_x;
	if (coord_y < ecart + taille_zone_recherche_y)		            coord_y = ecart + taille_zone_recherche_y;
    else	if (coord_y > ny - ecart - taille_zone_recherche_y)  		coord_y = ny - ecart - taille_zone_recherche_y;
	
	dprints("Recherche autour du point x = %d, y = %d, avec alpha = %f\n", coord_x, coord_y, alpha);

	/******* Initialisation de la vignette *******/
/*	
	if (my_data->vignette == NULL) 
	{
		vignette=my_data->vignette = (unsigned char *) calloc(my_data->nx_vignette * my_data->ny_vignette, sizeof(unsigned char));
		reset = 1.0;
	}
    else 	vignette = my_data->vignette;
*/	
	if (reset >= 0.5) 
	{	
      position_vignette = vignette+(2*ecart+1)*(2*ecart2+1);

	  max_v = coord_y+ecart; min_v= coord_y-ecart;
	  max_u = coord_x+ecart2; min_u= coord_x-ecart2;
      init_u= max_u-min_u+1; init_v= (max_v- min_v+1)*nx;
      p_init_ligne=input+min_v*nx + min_u;
    
	  for (v = init_v; (v=v-nx) ; ) 
      {
	    p_ligne = v+  p_init_ligne;
		for (u = init_u; u--;) 
        {
		  pp = p_ligne + u;
		  *position_vignette = (float) *pp;
		  position_vignette--;
		}
	  }
		dprints("Vignette correctement initialisee\n");
	}

	/******* Initialisation de la sortie *******/
 /* output_ext = ((prom_images_struct *) (def_groupe[gpe].ext));
  output_ext->images_table[0] = vignette; */

	/******* Debut de la Difference *******/
  max_v = coord_y+ecart+taille_zone_recherche_y;
  min_v = coord_y- ecart-taille_zone_recherche_y;
  max_u = coord_x+ecart2+taille_zone_recherche_x;
  min_u = coord_x-ecart2-taille_zone_recherche_x;
  
  max_j=(2*ecart+1)*nx;
  max_i= 2*ecart2+1;
  init_position_vignette = vignette+(2*ecart+1)*(2*ecart2+1); 
  init_pp= -ecart*nx -ecart2;
  
  for (v = max_v; v >= min_v; v--)
  {
    ligne = v * nx;
	for (u = max_u; u>= min_u; u--) 
    {
	  p = ligne + u;
						
			/******* Difference *******/
	  test_min = 0.;
      position_vignette = init_position_vignette; 

      pp=input+p+init_pp;               // adr image entree
      for (j = max_j; (j=j-nx) ;   )    // position verticale dans le voisinage de l'image
      {
        p2= pp +j;                     // adr image entree
        for (i = max_i; i--;   )  
        {
		   position = p2 +  i  ; // position dans le voisinage de l'image en entree
    	   test_min += abs(*(position) - (unsigned char) *(position_vignette));
           position_vignette--; 
		}			
       }		
       if (test_min < min) 
       {
	     min = test_min;
	     id_min = p;
	   }
    }
  }
  /******* Fin de la Difference *******/
  
  
  /******* Envoi des coordonnees du min trouve *******/ 
  coord_x = (id_min % nx);
  coord_y = (id_min / nx);
  /*output_coord_x = (int) (  (float)  nx/2   -  (float) coord_x  );
	output_coord_y = (int) (  (float)  ny/2   -  (float) coord_y  );*/
  // c'est un pb car l'activite des neurones est reutilisee par  f_tracking 
	
  /*neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = output_coord_x;
  neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = output_coord_y;*/

  p=def_groupe[gpe].premier_ele;
  neurone[p].s = neurone[p].s1 = neurone[p].s2 = coord_x/((float)nx);
  neurone[p + 1].s = neurone[p + 1].s1 = neurone[p + 1].s2 = coord_y/((float) ny);
	
  dprints("Point trouve aux coordonnes : x = %d, y = %d\nMinimum vaut %f\n", coord_x, coord_y, min);

	/******* On a enfin trouve la nouvelle vignette, on la somme avec l'ancienne *******/

  position_vignette = vignette+(2*ecart+1)*(2*ecart2+1);
  
  max_v = coord_y+ecart;  min_v = coord_y - ecart;
  max_u = coord_x+ecart2; min_u = coord_x - ecart2;
  p_init_ligne=input+ min_v*nx+min_u;                   // vignette dans l'image d'entree
  init_u=max_u-min_u +1;   init_v=(max_v-min_v+1)*nx;

  /**************************************test des différentes méthodes de mise à jour de la vignette********************************/
  if(mode_update == 0) {
	  /* Mise à jour globale */
	  alpha_c = (1 - stop_update) * alpha *(1.- (min / (my_data->ny_vignette*my_data->nx_vignette*255.)));
	  alpha1= (1. - alpha_c); 
	  for (v = init_v ; (v=v-nx) ; ){
		  p_ligne = v+ p_init_ligne;
		  for (u = init_u ; u--; ){
			  pp = p_ligne + u;                        // vignette dans l'image d'entree
			  *position_vignette =  alpha_c * (*pp) + alpha1 * (*position_vignette); 
			  position_vignette--;
		  }
	  }
      reco = min / (my_data->ny_vignette*my_data->nx_vignette*255.)	;
  }
  else {
	 /*printf("\n mode pixelized \n");*/
	/*Mise à jour de chaque pixel  indépendemment et calcul de la reco */  
	for (v = init_v ; (v=v-nx) ; ) {
		p_ligne = v+ p_init_ligne;
		for (u = init_u ; u--; ) {
			pp = p_ligne + u;                        // vignette dans l'image d'entree
			/*alpha_pixc = alpha * (1 - abs(*pp - *position_vignette)/255);*/
			alpha_pixc = (1 - stop_update) * exp((- abs(*pp - *position_vignette))/3);
			alpha1 = 1 - alpha_pixc;
			/*printf("alpha_pixc = %f et alpha1 = %f \n", alpha_pixc, alpha1);*/
			/*err_reco += alpha_pixc * abs(*pp - *position_vignette);
			somme_alphapixc += alpha_pixc;*/
			err_reco += abs(*pp - *position_vignette);
			*position_vignette =  alpha_pixc * (*pp) + alpha1 * (*position_vignette);
			position_vignette--;
			/*if (stop_update == 1) printf("pas d'actu");*/
		}
	}
	reco = err_reco/(my_data->ny_vignette*my_data->nx_vignette*255.); 
    /*printf("reco = %f \n",reco);  */
  }
   
  neurone[p + 2].s = neurone[p + 2].s1 = neurone[p + 2].s2 = reco;
		
}

void destroy_tracking_diff(int gpe)
{
	Data_tracking_diff * my_data = NULL;
	my_data = (Data_tracking_diff *) def_groupe[gpe].data;
	free(my_data->vignette);
	free(def_groupe[gpe].data);
	free(def_groupe[gpe].ext);
	def_groupe[gpe].data = NULL;
	def_groupe[gpe].ext = NULL;
	dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


