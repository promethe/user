/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_visu_orient_scale_key.c 
\brief 

Author: Mickael Maillard 
Created: 09/07/04
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 *  Fonction dessinant des cercles de taille fct de l'echelle de l'image (la taille)
 *  avec une orientation dessinnee par un tres et le point caracteristique par un carre
 *  deprecated : fct de debug had-hoc
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>

/*#define DEBUG*/
#undef DEBUG

#undef COMPT_CARACT
/*#define COMPT_CARACT*/
void function_visu_orient_scale_key(int Gpe)
{
    int i, inputGpe = -1, sx, sy, row, column, sx_im = 0, sy_im = 0, x, y;
    float sy_factor, sx_factor;
    int *input_Gpe_table = NULL;
    float *outputImage = NULL, *inputImage = NULL, orient;
    float *K_image;
    int Ninput = 0;
    int radius, bound, j, k, nrow, ncolumn;
    char *chaine;
    /* initialisation */
#ifdef DEBUG
    printf("groupe %d : %s\n", Gpe, __function__);
#endif
#ifdef COMPT_CARACT
    int compteur_points;
#endif

    if (def_groupe[Gpe].ext == NULL)
    {
        input_Gpe_table = (int *) malloc(100 * sizeof(int));
        if (input_Gpe_table == NULL)
        {
            printf
                ("Gpe %d (f_visu_orient_scale_key) : Impossible d'allouer de la memoire\n",
                 Gpe);
        }

        Ninput = 0;
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == Gpe)
            {
                inputGpe = liaison[i].depart;
                chaine = liaison[i].nom;
                if (strstr(chaine, "Image") != NULL)
                {
                    if (def_groupe[inputGpe].ext == NULL)
                    {
                        printf
                            ("Gpe %d : le pointeur de l'ext entrant est NULL\n",
                             Gpe);
                        free(input_Gpe_table);
                        return;
                    }
                    sx_im =
                        ((prom_images_struct *) def_groupe[inputGpe].ext)->sx;
                    sy_im =
                        ((prom_images_struct *) def_groupe[inputGpe].ext)->sy;
                    inputImage =
                        (float *) ((prom_images_struct *)
                                   def_groupe[inputGpe].ext)->images_table[0];
#ifdef DEBUG
                    printf("%s ,Gpe %d :  image Gpe %d\n", __FUNCTION__, Gpe,
                           inputGpe);
#endif
                }
                else
                {
                    Ninput++;
                    input_Gpe_table[Ninput] = inputGpe;
                    if (def_groupe[inputGpe].ext == NULL)
                    {
                        printf
                            ("Gpe %d : un pointeur entrant a son ext NULL\n",
                             Gpe);
                        free(input_Gpe_table);
                        return;
                    }
#ifdef DEBUG
                    printf("%s ,Gpe %d :  K Gpe %d\n", __FUNCTION__, Gpe,
                           inputGpe);
#endif
                }
            }
        }
        input_Gpe_table[0] = Ninput;

        if (inputImage == NULL)
        {
            printf("%s , Gpe : %d : il n'y a pas de lien vers l'image...\n",
                   __FUNCTION__, Gpe);
            exit(0);
        }


        def_groupe[Gpe].ext = (void *) malloc(sizeof(prom_images_struct));
        if (def_groupe[Gpe].ext == NULL)
        {
            printf
                ("%s : %d : Gpe :%d : ALLOCATION DE MEMOIRE IMPOSSIBLE...\n",
                 __FUNCTION__, __LINE__, Gpe);
            exit(0);
        }

        ((prom_images_struct *) def_groupe[Gpe].ext)->sx = sx_im;
        ((prom_images_struct *) def_groupe[Gpe].ext)->sy = sy_im;
        ((prom_images_struct *) def_groupe[Gpe].ext)->nb_band = 4;
        ((prom_images_struct *) def_groupe[Gpe].ext)->image_number = 1;
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[0] =
            (unsigned char *) malloc(sx_im * sy_im * sizeof(float));
        outputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];

        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[1] =
            (unsigned char *) input_Gpe_table;
        ((prom_images_struct *) def_groupe[Gpe].ext)->images_table[2] =
            (unsigned char *) inputImage;
    }
    else
    {
        sx_im = ((prom_images_struct *) def_groupe[Gpe].ext)->sx;
        sy_im = ((prom_images_struct *) def_groupe[Gpe].ext)->sy;
        inputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[2];
        input_Gpe_table =
            (int *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[1];
        Ninput = input_Gpe_table[0];
        outputImage =
            (float *) ((prom_images_struct *) def_groupe[Gpe].ext)->
            images_table[0];
    }


    for (row = 0; row < sy_im; row++)
        for (column = 0; column < sx_im; column++)
            *(outputImage + row * sx_im + column) =
                *(inputImage + row * sx_im + column);

    for (i = 1; i <= Ninput; i++)
    {
        sx = ((prom_images_struct *) def_groupe[input_Gpe_table[i]].ext)->sx;
        sy = ((prom_images_struct *) def_groupe[input_Gpe_table[i]].ext)->sy;
        K_image =
            (float *) ((prom_images_struct *) def_groupe[input_Gpe_table[i]].
                       ext)->images_table[0];

        sx_factor = (float) sx_im / (float) sx;
        sy_factor = (float) sy_im / (float) sy;
#ifdef COMPT_CARACT
        compteur_points = 0;
#endif
/*rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr*/

        /*      if(sx_factor==1) */
/*rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr*/
        for (row = 0; row < sy; row++)
            for (column = 0; column < sx; column++)
                if (*(K_image + row * sx + column) < -0.0000001
                    || *(K_image + row * sx + column) > 0.00000001)
                {
#ifdef COMPT_CARACT
                    compteur_points++;
#endif
                    nrow = (int) (row * sy_factor);
                    ncolumn = (int) (column * sx_factor);

                    *(outputImage + nrow * sx_im + ncolumn) =
                        255 - *(inputImage + nrow * sx_im + ncolumn);

                    /*dessin du point */
                    for (j = nrow - 1; j <= nrow + 1; j++)
                        for (k = ncolumn - 1; k <= ncolumn + 1; k++)
                            if (j >= 0 && j < sy_im && k >= 0 && k < sx_im)
                                *(outputImage + j * sx_im + k) = 0;

                    *(outputImage + nrow * sx_im + ncolumn) = 0;

                    /*dessin de l'orientation */
                    orient =
                        2 * M_PI * (*(K_image + row * sx + column) / 360);
                    for (j = 0; j < 30; j++)
                    {
                        y = (int) (j * cos(orient));
                        x = (int) (j * sin(orient));
                        if ((nrow + x) >= 0 && (nrow + x) < sy_im
                            && (ncolumn + y) >= 0 && (ncolumn + y) < sx_im)
                            *(outputImage + (nrow + x) * sx_im + ncolumn +
                              y) = 255;
                    }

                    /*dessin du cercle representant l'echelle */
                    radius = (int) (8 * (sx_factor + sy_factor) / 2);
                    bound = (int) (radius * 1.41);

                    for (j = nrow - bound; j <= nrow + bound; j++)
                        for (k = ncolumn - bound; k <= ncolumn + bound; k++)
                            if ((((j - nrow) * (j - nrow) +
                                  (k - ncolumn) * (k - ncolumn)) >=
                                 ((radius - 1) * (radius - 1)))
                                &&
                                (((j - nrow) * (j - nrow) +
                                  (k - ncolumn) * (k - ncolumn)) <=
                                 ((radius) * (radius))))
                                if (j >= 0 && j < sy_im && k >= 0
                                    && k < sx_im)
                                    *(outputImage + j * sx_im + k) =
                                        255 - *(outputImage + j * sx_im + k);
                }
#ifdef COMPT_CARACT
        printf("Nbre de points dessine octave %d %d : %d\n", sx, sy,
               compteur_points);
#endif
    }
#ifdef DEBUG
    printf("fin Gpe %d : %s\n", Gpe, __FUNCTION__);
#endif
    return;

}
