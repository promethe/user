/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief realise suivi d'une cible

Author: Maillard
Created: 2002
Modified:
- author: BASTY
- description: specific file creation
- date: 20/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Suit une cible en mvt dont la position de depart est specifiee par passage de coordonnees des fonctions precedentes, f_extraction_yeux_bouche_f_histogramme_verticale.
 L'image d'entree doit elle aussi lui etre fournie.
 Fonction par difference de regions et comparaissant avec seulement quelques vosins du pixel
Macro:
-none

Local variables:
-none

Global variables:

Internal Tools:
-none

External Tools: 
-none

Links:
- type: none
- description: none
- input expected group: Une image
- where are the data?:

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.
Place le resultat quelque part

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/feature_face.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEF_TAILLE_X 6
#define DEF_TAILLE_Y 6
#define DEF_NB_PIXEL 3

typedef struct
{
    int taille_fen_x, taille_fen_y; /* Dimension de la fenetre */
    int taille_im_x, taille_im_y;   /* Dimension de l'image */
    int val_coin;               /* valeur en pixel du coin de la fenetre depuis le coin sup-gauche de l'image */
    TxPoint coin_fen;
    unsigned char *fen;
    int InputGpe, InputGpeCoordYeux, InputGpeCoordOeil;
    int display;
    int tab[5], nb_pixel;
    float *tabscore;
    int nb_band;

    int coord_init_x_hough, coord_init_y_hough;

} fenetre_track;



void function_tracking_eye_static(int Gpe)  /* Version qui fonctionne pour 3 fenetres independantes */
{
    fenetre_track *fen = NULL;
    int i=0, j, k, h, pa, pb, x0, y0, link1 = -1, link2 = -1, link3 = -1;
    int minvert = -1, minhoriz = -1, hvert = -1, hhoriz = -1, dirhoriz =
        -1, dirvert = -1;
    int xmax, ymax;
    float *tabscore = NULL, coef = 0.2, score;  /* coef < 1 --> la memoire est + forte */
    /*  char *string = NULL, *st=NULL; */
    void *prev_ptr = NULL;
    unsigned char *image = NULL;

#ifdef DEBUG
    TxPoint cercle;
#endif

    int nb_band = 0;
    int pos = 0;
    int speedx = 0, speedy = 0;
    int tata;

    char resultat[256];

    feature_face *face = NULL;
    /* hough_struct *t_houghEye=NULL; */
    feature_face *face_oeil = NULL;

    char *string = NULL, *st = NULL;

    /*Mesure du temps de traitement dans la fonction */
#ifdef TIME_TRACE
    struct timeval InputFunctionTimeTrace, OutputFunctionTimeTrace;
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
    char MessageFunctionTimeTrace[255];
#endif

    /*Lancement du timer */
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

    /* printf("----- function extraction fenetre -----\n"); */
    if (def_groupe[Gpe].data == NULL)
    {
        /*printf("1er passage\n"); */
        /*allocation de la structure fenetre et remplissage des champs */
        fen = (fenetre_track *) malloc(sizeof(fenetre_track));
        if (fen == NULL)
        {
            printf
                ("Impossible d'allouer la structure de la fenetre groupe %d (function_tracking_filter)\n",
                 Gpe);
            exit(1);
        }

        link1 = find_input_link(Gpe, 0);
        link2 = find_input_link(Gpe, 1);
        link3 = find_input_link(Gpe, 2);

#ifdef DEBUG
        printf("f_projection_depuis_projections\n");
        printf("link1 :%d\n", link1);
        printf("link2 :%d\n", link2);
        printf("link3 :%d\n", link3);
#endif

        if ((link1 == -1) || (link2 == -1) || (link3 == -1))
        {
            printf
                ("*****> Il n'y a pas trois groupes en entree du Gpe %d\n",
                 Gpe);
            exit(EXIT_FAILURE);
        }

#ifdef DEBUG
        printf("nom link1 :%s\n", liaison[link1].nom);
        printf("nom link2 :%s\n", liaison[link2].nom);
        printf("nom link3 :%s\n", liaison[link3].nom);
#endif

        /*Lecture du lien 1 */
        string = liaison[link1].nom;
        /*    printf("Chaine dans string pour link1 %s\n",string); */
        st = strstr(string, "-image");
        if (st != NULL)
        {
            fen->InputGpe = liaison[link1].depart;
            i = link1;
        }
        st = strstr(string, "-coord_yeux");
        if (st != NULL)
        {
            fen->InputGpeCoordYeux = liaison[link1].depart;
        }
        st = strstr(string, "-coord_oeil");
        if (st != NULL)
        {
            fen->InputGpeCoordOeil = liaison[link1].depart;
        }


        /*Lecture du lien 2 */
        string = liaison[link2].nom;
        /*       printf("Chaine dans string pour link2 %s\n",string); */
        st = strstr(string, "-image");
        if (st != NULL)
        {
            fen->InputGpe = liaison[link2].depart;
            i = link2;
        }
        st = strstr(string, "-coord_yeux");
        if (st != NULL)
        {
            fen->InputGpeCoordYeux = liaison[link2].depart;
        }
        st = strstr(string, "-coord_oeil");
        if (st != NULL)
        {
            fen->InputGpeCoordOeil = liaison[link2].depart;
        }

        /*Lecture du lien 3 */
        string = liaison[link3].nom;
        /*       printf("Chaine dans string pour link3 %s\n",string); */
        st = strstr(string, "-image");
        if (st != NULL)
        {
            fen->InputGpe = liaison[link3].depart;
            i = link3;
        }
        st = strstr(string, "-coord_yeux");
        if (st != NULL)
        {
            fen->InputGpeCoordYeux = liaison[link3].depart;
        }
        st = strstr(string, "-coord_oeil");
        if (st != NULL)
        {
            fen->InputGpeCoordOeil = liaison[link3].depart;
        }

#ifdef DEBUG
        printf("*****> gpe_entree 1 = %d   link1=%d\n", fen->InputGpe, link1);
        printf("*****> gpe_entree 2 = %d   link2=%d\n",
               fen->InputGpeCoordYeux, link2);
        printf("*****> gpe_entree 3 = %d   link3=%d\n",
               fen->InputGpeCoordOeil, link3);
        printf("Premier passage\n");
#endif

        /*recuperation image et affectation */
        prev_ptr = (prom_images_struct *) def_groupe[fen->InputGpe].ext;

        /*affectation des tailles de l'image */
        fen->taille_im_x = ((prom_images_struct *) (prev_ptr))->sx;
        fen->taille_im_y = ((prom_images_struct *) (prev_ptr))->sy;
        nb_band = fen->nb_band = ((prom_images_struct *) (prev_ptr))->nb_band;

#ifdef DEBUG
        printf("Taille x de l'image d'entree %d\n", fen->taille_im_x);
        printf("Taille y de l'image d'entree %d\n", fen->taille_im_y);
        printf("Nb band de l'image d'entree %d\n", fen->nb_band);
#endif

        if (prev_ptr == NULL)
        {
            printf("PB : pas d'images dans le groupe %i\n", fen->InputGpe);
            exit(EXIT_FAILURE);
        }

        if ((fen->nb_band != 3) && (fen->nb_band != 1))
        {
            printf
                ("Le nb_band n'est pas correct dans le Gpe %d : function_tracking_filter ne prend que des images char (couleur ou non)\n",
                 Gpe);
            exit(1);
        }

        /*Affectation des tailles de la fenetre en dur */
        fen->taille_fen_x = DEF_TAILLE_X;
        fen->taille_fen_y = DEF_TAILLE_Y;

        if (prom_getopt(liaison[i].nom, "-Th", resultat) == 2)
        {
            fen->taille_fen_x = atoi(resultat);
        }
        if (prom_getopt(liaison[i].nom, "-Tv", resultat) == 2)
        {
            fen->taille_fen_y = atoi(resultat);
        }

        /*recuperation des coordonnees du groupe localisant le visage */
        face =
            (feature_face *) ((prom_images_struct *)
                              def_groupe[fen->InputGpeCoordYeux].data);

        /*recuperation des coordonnees du groupe localisant le visage */
        face_oeil =
            (feature_face *) ((prom_images_struct *)
                              def_groupe[fen->InputGpeCoordOeil].data);

        /*recuperation des coordonnees du groupe localisant les eyes */
        /*      t_houghEye=(hough_struct*) ((prom_images_struct*)def_groupe[fen->InputGpeCoordOeil].data); */

        /*affectation des coordonnees pour la fenetre */
        x0 = fen->coin_fen.x =
            (int) (fen->taille_im_x / 2 - fen->taille_fen_x / 2);
        y0 = fen->coin_fen.y =
            (int) (fen->taille_im_y / 2 - fen->taille_fen_y / 2);

#ifdef DEBUG
        printf("Taille image provenant de t_houghVisage sx %d et sy %d\n",
               face->nx, face->ny);
#endif

        x0 = fen->coin_fen.x = fen->coord_init_x_hough =
            face_oeil->pos_oeil_x;
        y0 = fen->coin_fen.y = fen->coord_init_y_hough = face->pos_oeil_y;


        /*Affectation des pixel voisins a compare en dur */
        fen->nb_pixel = DEF_NB_PIXEL;
        if (prom_getopt(liaison[i].nom, "-P", resultat) == 2)
        {
            fen->nb_pixel = atoi(resultat);
        }

        /*Display ou non du resultat pour la fenetre */
        fen->display = 0;
        if (prom_getopt(liaison[i].nom, "-D", resultat) == 2)
        {
            fen->display = atoi(resultat);
        }

        /*Allocation de la fenetre */
        fen->fen =
            (unsigned char *) malloc(nb_band * fen->taille_fen_x *
                                     fen->taille_fen_y *
                                     sizeof(unsigned char));
        if (fen->fen == NULL)
        {
            printf
                ("Impossible d'allouer la fenetre groupe %d (function_tracking_filter)\n",
                 Gpe);
            exit(1);
        }
        /* Taille de l'image */

        /*on remplit les derniers champs */
        fen->val_coin =
            nb_band * (fen->coin_fen.y * fen->taille_im_x + fen->coin_fen.x);

        /* Initialisation du tableau permettant d'avoir l'offset des pixels voisins */
        fen->tab[0] = 0;
        fen->tab[1] = -nb_band * fen->taille_im_x;  /* 1 pixel en haut */
        fen->tab[2] = nb_band * fen->taille_im_x;   /* 1 pixel en bas */
        fen->tab[3] = -nb_band; /* 1 pixel a gauche */
        fen->tab[4] = nb_band;  /* 1 pixel a droite */


        fen->tabscore = (float *) calloc((1 + 4 * fen->nb_pixel), sizeof(float));   /*on compare avec l'image sans deplacement et avec des deplacements de nb_pixel dans 4 directions */
        if (fen->tabscore == NULL)
        {
            printf
                ("Impossible d'allouer le tabscore, groupe %d (function_tracking_filter)\n",
                 Gpe);
            exit(1);
        }
        tabscore = fen->tabscore;

        /*init de la fenetre de depart */
        image = ((prom_images_struct *) (prev_ptr))->images_table[0];
        for (j = 0; j < fen->taille_fen_y; j++)
        {
            pa = j * nb_band * fen->taille_fen_x;
            pb = fen->val_coin + j * nb_band * fen->taille_im_x;
            for (i = 0; i < nb_band * fen->taille_fen_x; i++)
                fen->fen[pa + i] = image[pb + i];
        }

        /*on stock la structure initialisee dans le data du groupe */
        def_groupe[Gpe].data = (void *) fen;

    }
    else
    {
        /*printf("Autre passage\n"); */
        fen = (fenetre_track *) def_groupe[Gpe].data;

        /*recuperation des coordonnees du groupe precedent */
        face = (feature_face *) (def_groupe[fen->InputGpeCoordYeux].data);

        /*recuperation des coordonnees du groupe precedent */
        face_oeil =
            (feature_face *) (def_groupe[fen->InputGpeCoordOeil].data);

#ifdef DEBUG
        printf("InputGpeCoordVis %d\n", fen->InputGpeCoordYeux);
        printf("Coordonnees de la transforme de hough\n");
        printf("x   :%d et y  %d de la zone des yeux\n", face->nx / 2,
               face->pos_oeil_y);
        printf("Coordonnees initiales de la transforme de hough\n");
        printf("coord_init_x :%d , coord_init_y %d\n",
               fen->coord_init_x_hough, fen->coord_init_y_hough);
#endif

        prev_ptr = (prom_images_struct *) def_groupe[fen->InputGpe].ext;

        /*Affectation de y */
        /*remise a jour des coordonnees de l'ellipse trouve */
        if ((fen->coord_init_x_hough == face_oeil->pos_oeil_x)
            && (fen->coord_init_y_hough == face->pos_oeil_y))
        {
            x0 = fen->coin_fen.x;
            y0 = fen->coin_fen.y;
            nb_band = fen->nb_band;
        }
        else
        {
            x0 = fen->coin_fen.x = fen->coord_init_x_hough =
                face_oeil->pos_oeil_x;
            y0 = fen->coin_fen.y = fen->coord_init_y_hough = face->pos_oeil_y;
        }

#ifdef DEBUG
        printf("Fonction f_tracking_eye_static\n");
        printf("Valeur de x0 :%d , Valeur de y0 : %d\n", x0, y0);
        printf("Passage dans le .data, allocation hough structure\n");
#endif

        image = ((prom_images_struct *) (prev_ptr))->images_table[0];
        tabscore = fen->tabscore;

        /*printf("Passage de parmetre\n"); */

        /* printf("debut %d %d\n",x0,y0); */
        /*on remet a zero les scores */
        for (j = 0; j < (1 + 4 * fen->nb_pixel); j++)
            *(fen->tabscore + j) = (float) 0.;

        /*calcul des bords max */
        xmax = fen->taille_im_x - fen->taille_fen_x - fen->nb_pixel - 1;
        ymax = fen->taille_im_y - fen->taille_fen_y - fen->nb_pixel - 1;

        /* On compare la fenetre de depart avec d'autres */

        /* Enregistrement des scores dans un tableau */
        for (h = 0; h <= fen->nb_pixel; h++)    /*h -> pixel voisin */
        {
            if (h)
                for (i = 1; i < 5; i++) /*4 directions pour les pixels voisins */
                    for (j = 0; j < fen->taille_fen_y; j++)
                    {
                        pa = j * nb_band * fen->taille_fen_x;
                        pb = fen->val_coin + j * nb_band * fen->taille_im_x;
                        for (k = 0; k < nb_band * fen->taille_fen_x; k++)
                        {
                            pos = pb + k + h * fen->tab[i];
                            if (pos < fen->taille_im_x * fen->taille_im_y * nb_band)    /*c'est pas tres bien gere */
                                tabscore[i + 4 * (h - 1)] =
                                    tabscore[i + 4 * (h - 1)] +
                                    fabs((float)
                                         (fen->fen[pa + k] - image[pos]));
                        }
                    }
            else
                for (j = 0; j < fen->taille_fen_y; j++) /*cas ou la fenetre ne bouge pas */
                {
                    pa = j * nb_band * fen->taille_fen_x;
                    pb = fen->val_coin + j * nb_band * fen->taille_im_x;
                    for (k = 0; k < nb_band * fen->taille_fen_x; k++)
                    {
                        pos = pb + k + fen->tab[0];
                        if (pos <
                            fen->taille_im_x * fen->taille_im_y * nb_band)
                            tabscore[0] =
                                tabscore[0] +
                                fabs((float) (fen->fen[pa + k] - image[pos]));
                    }
                }
        }

        /* Comparaison */
        minvert = tabscore[0];
        minhoriz = tabscore[0];
        hhoriz = 0;
        hvert = 0;
        for (i = 1; i <= fen->nb_pixel; i++)
            for (j = 1; j <= 2; j++)
            {
                if (tabscore[j + 2 + 4 * (i - 1)] < minhoriz)
                {
                    dirhoriz = j;
                    hhoriz = i;
                    minhoriz = tabscore[j + 2 + 4 * (i - 1)];
                }
                if (tabscore[j + 4 * (i - 1)] < minvert)
                {
                    dirvert = j;
                    hvert = i;
                    minvert = tabscore[j + 4 * (i - 1)];
                }
            }

        /* On garde en memoire la valeur du coin et l'image de la meilleure fenetre */
        switch (dirhoriz)
        {
        case 0:
            break;
        case 1:
            if (x0 > fen->nb_pixel + 1)
            {
                x0 -= hhoriz;
                speedx = -hhoriz;
                /*fen->val_coin += fen->tab[3] * hhoriz; */
            }
            break;
        case 2:
            if (x0 < xmax)
            {
                x0 += hhoriz;
                speedx = hhoriz;
                /*fen->val_coin += fen->tab[4] * hhoriz; */
            }
            break;
        }

        switch (dirvert)
        {
        case 0:
            break;
        case 1:
            if (y0 > fen->nb_pixel + 1)
            {
                y0 -= hvert;
                speedy = -hvert;
                /*fen->val_coin += fen->tab[1] * hvert; */
            }
            break;
        case 2:
            if (y0 < ymax)
            {
                y0 += hvert;
                speedy = hvert;
                /*fen->val_coin += fen->tab[2] * hvert; */
            }
            break;
        }

        tata = fen->val_coin;
        fen->val_coin =
            nb_band * ((speedy + fen->coin_fen.y) *
                       fen->taille_im_x + fen->coin_fen.x + speedx);
        fen->coin_fen.x += speedx;
        fen->coin_fen.y += speedy;


        score = 0.0;
        /*calcul avec la memoire */
        for (j = 0; j < fen->taille_fen_y; j++)
        {
            pa = j * nb_band * fen->taille_fen_x;
            pb = tata + j * nb_band * fen->taille_im_x;
            for (i = 0; i < nb_band * fen->taille_fen_x; i++)
            {
                pos = pb + i;
                if (pos < fen->taille_im_x * fen->taille_im_y * nb_band)    /*c'est tjs aussi mal gere */
                {
                    fen->fen[pa + i] =
                        (fen->fen[pa + i] + coef * image[pos]) / (coef + 1);
                    /*fen->fen[pa + i] + (image[pb + i]-fen->fen[pa+i])/2; */
                    score += fen->fen[pa + i];
                }
            }
        }


        /*fen->coin_fen.x = x0;
           fen->coin_fen.y = y0; */

#ifdef DEBUG
        printf("Fonction f_tracking_eye_static");
        printf("Valeur x0 : %d et y0 : %d\n", x0, y0);
#endif

/*  neurone[def_groupe[Gpe].premier_ele].s2 =
	  neurone[def_groupe[Gpe].premier_ele].s1 =
	  neurone[def_groupe[Gpe].premier_ele].s = neurone[def_groupe[Gpe].premier_ele].s1 + 0.01*(0. - ( ((float)x0-127.)/(float)fen->taille_im_x));*/
    }

#ifndef AVEUGLE
#ifdef DEBUG
    if (fen->display)
    {
        cercle.x = x0;
        cercle.y = y0;
        TxDessinerRectangle(&image1, rouge, TxVide, cercle, fen->taille_fen_x,
                            fen->taille_fen_y, 1);
        TxDessinerRectangle(&image2, rouge, TxVide, cercle, fen->taille_fen_x,
                            fen->taille_fen_y, 1);
        TxFlush(&image1);
    }
#endif
#endif

    /*neurone[def_groupe[Gpe].premier_ele].s2 =
       neurone[def_groupe[Gpe].premier_ele].s1 =
       neurone[def_groupe[Gpe].premier_ele].s = (float)1 - ((float)x0/(float)fen->taille_im_x); */


    /*(neurone[def_groupe[Gpe].premier_ele].s1*255)-127 */

    /*Fin du Timer et affichage des temps */
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);

    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    printf("Fonction du groupe %d\n", Gpe);
    sprintf(MessageFunctionTimeTrace, "Time in fonction \t%4ld.%06ld\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    /*   affiche_message(MessageFunctionTimeTrace); */
    printf("Chaine MessageFunctionTimeTrace %s\n", MessageFunctionTimeTrace);
#endif

}
