/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_quantite_mvt.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: S.BOUCENNA
- description: specific file creation
- date: 20/06/2007

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-Cette fonction permet simplement d avoir la quantite de mouvement.
 Elle compte seulement le nombre de pixel qui se sont d�places.
 Le resultat de la quantite de mouvement se trouve sur le premier neurone.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/
 
typedef struct mydata
{  
  int gpe;
} mydata;


void function_quantite_mvt(int gpe)

{
 
  mydata *data = NULL;
  int debut_gpe_precedent; /*debut du groupe precedent*/
  int debut_gpe;   /*debut du groupe courant*/
  int j;
  int i=0;
  float mvt=0;
  FILE *f;

  if(def_groupe[gpe].data == NULL)
    
    {
    
      data=(mydata *)malloc(sizeof(mydata));
      
      if(data==NULL)
      
	{

	  printf("pb malloc pour mydata");
	  exit(0);
	  
	}
      
      /*recuperation des parametres d'entree*/
	
      while((j=find_input_link(gpe,i))!=-1)
	
	{

	  if(strcmp(liaison[j].nom,"mvt")==0)
	    
	    {
	      
	      data->gpe=liaison[j].depart;
	     
#ifdef DEBUG
	      printf("groupe=%d",liaison[j].depart);
#endif
	    }
	   
	  i++;
	  
#ifdef DEBUG
	  printf("on a fini de recuperer les parametres\n");
#endif
	}

	   

      def_groupe[gpe].data=data;	 /*sauvegarde de Mydata*/

    }

  else
    {
      
      data = def_groupe[gpe].data;
      	
    }
  
  debut_gpe_precedent = data->gpe;
  debut_gpe = def_groupe[gpe].premier_ele;
  

#ifdef DEBUG
  printf("nombre=%d",def_groupe[debut_gpe_precedent].nbre);
#endif

  for (j = def_groupe[debut_gpe_precedent].premier_ele; j < def_groupe[debut_gpe_precedent].premier_ele + def_groupe[debut_gpe_precedent].nbre; j++)
    
    {
      if(neurone[j].s > 0.00001)	
	{
	  mvt = mvt + neurone[j].s1;
	  /*neurone[debut_gpe].s = neurone[debut_gpe].s1 =
	    neurone[debut_gpe].s2 = mvt / def_groupe[debut_gpe_precedent].nbre;*/
	}
      
      else
	{
	  /*sinon on ne fait rien*/
	}
    }

  neurone[debut_gpe].s = neurone[debut_gpe].s1 =
	    neurone[debut_gpe].s2 = mvt / def_groupe[debut_gpe_precedent].nbre;	  
	  

  if(neurone[debut_gpe].s < 0.1)               /*0.5*/
    {
      neurone[debut_gpe].s = neurone[debut_gpe].s1 =
	neurone[debut_gpe].s2 = 0;
    }
  else
    {
      /*
	neurone[debut_gpe].s = neurone[debut_gpe].s1 =
	neurone[debut_gpe].s2 = 1;
      */
    }
  
  
  f = fopen("quantite.txt","a"); 
  if(f==NULL)
    {
      printf("impossible d'ouvrir le fichier dans la fonction f_quantite_mvt");
    }
    
  fprintf(f,"%f\n", neurone[debut_gpe].s);   
  fclose(f);  
  
  
#ifdef DEBUG
  printf(" quantite de mouvement=%f\n",neurone[debut_gpe].s);
#endif
  
  
}
