/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_ctr_sp f_ctr_sp

\section Author
Author: xxxxxxxx
Created: XX/XX/XXXX


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description
-none




\section Macro
-none
\section Local variables
-none


\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/


#include <stdlib.h>
#include "tools/include/local_var.h"
void Initialise_Fenetres()
{
#ifdef	DEBUG
    int i, r, v, b;
    Colormap TheMap;
    int Ecran;

    Ecran = DefaultScreen(image1.display);
    TheMap = DefaultColormap(image1.display, Ecran);

    for (i = 0; i < 255; i++)
        Cellules[i].pixel = i;

    XQueryColors(image1.display, TheMap, Cellules, 256);

    for (i = 255; i >= 0; i--)
    {
        r = Cellules[i].red / 4096;
        v = Cellules[i].green / 4096;
        b = Cellules[i].blue / 4096;

        if ((r < 1) && (v < 1) && (b < 1))  /* Blanc */
            Transcode[0] = i;

        if ((r == v) && (r == b))
        {
            Transcode[r] = i;
        }

        if ((r > 0) && (v < 1) && (b < 1))  /* Rouge */
        {
            Transcode[16] = i;
        }
        if ((r < 1) && (v > 0) && (b < 1))  /* Vert */
        {
            Transcode[17] = i;
        }
        if ((r < 1) && (v < 1) && (b > 0))  /* Bleu */
        {
            Transcode[18] = i;
        }
        if ((r > 0) && (v < 1) && (b > 0))  /* Magenta */
        {
            Transcode[19] = i;
        }
        if ((r > 0) && (v > 0) && (b < 1))  /* Jaune */
        {
            Transcode[20] = i;
        }
        if ((r < 1) && (v > 0) && (b > 0))  /* Cyan */
        {
            Transcode[21] = i;
        }
    }
#endif

}

/* copie l'image en niveau de gris dans l'image de contour */
void function_ctr_sp(gpe_sortie)
     int gpe_sortie;
{
    int i;
    unsigned char val;

    xmax_contour = xmax_ng;
    ymax_contour = ymax_ng;

    if (premiere_fois_contours == 1)
    {
#ifndef AVEUGLE
        Initialise_Fenetres();
#endif
        im_contour =
            (unsigned char *) malloc(sizeof(unsigned char) * xmax_ng *
                                     ymax_ng);
        im_c_aff = (char *) malloc(sizeof(unsigned char) * xmax_ng * ymax_ng);
        premiere_fois_contours = 0;
    }


    /* image speciale pour l'affichage */
    for (i = 0; i < xmax_contour * ymax_contour; i++)
    {
        val = im_ng[i];
        im_contour[i] = val;
#ifndef AVEUGLE
        im_c_aff[i] = Transcode[(int) val >> 4];
#endif
    }

    (void) gpe_sortie;
}
