/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>

int posx;                       /* position courante de l'oeil dans l'image   */
int posy;                       /* coordonnee de l'image des contours */
int xmax_contour;
int ymax_contour;
unsigned char *im_contour;
int xmax_carac;
int ymax_carac;
unsigned char *im_carac;
unsigned char *im_carac2;
int position_courante_x;        /* position courante de l'oeil dans l'image   */
int position_courante_y;        /* coordonnee de l'image des points caracteristiques */
int flag_affiche_pano = 1;
char nom_image[256];
 /* int         inverse_video=1; */

float const_log_ng = 20.;
int rayon_vision_ng = 8;

/* Sorin


float const_log_ng= 25.;
int rayon_vision_ng= 10;

*/

float const_log_text = 20.;     /* translation image log pour eviter le centre  */
                  /* blind zone                                   */
int rayon_vision_text = 5;      /* rayon dans lequel est pris en compte l'image */

int NbrTexture = 0;

int xmax_ng;
int ymax_ng;

int premiere_fois_contours = 1;
char *im_c_aff = NULL;          /* memoire pour affichage direct de l'image */
unsigned char *im_ng;
int Transcode[256];
