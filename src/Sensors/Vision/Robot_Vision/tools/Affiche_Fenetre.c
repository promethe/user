/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_Affiche_Fenetre f_Affiche_Fenetre

\section Author
Author: xxxxxxxx
Created: XX/XX/XXXX


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description
  Affichage d'images dans une fenetre 
 - No = numero de fenetre (1 ou 2)   
 - Data = Tableau de donnees         
 - Largeur, Hauteur de l'image




\section Macro
-none
\section Local variables
-none 


\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/


#include <libx.h>

void Affiche_Fenetre(int No, char *Data, int Largeur, int Hauteur)
{
#ifdef	DEBUG

    GC gc;
    int Ecran, i;
    XImage *Img;
    Visual *Visuel;

    switch (No)
    {
    case 1:
        Ecran = DefaultScreen(image1.display);
        gc = image1.gc;
        Visuel = DefaultVisual(image1.display, Ecran);
        Img =
            XCreateImage(image1.display, Visuel,
                         DefaultDepth(image1.display, Ecran), ZPixmap, 0,
                         Data, Largeur, Hauteur, 8, 0);
        XPutImage(image1.display, image1.window, gc, Img, 0, 0, 10, 10,
                  Largeur, Hauteur);
        break;

    case 2:
        Ecran = DefaultScreen(image2.display);
        gc = image2.gc;
        Visuel = DefaultVisual(image2.display, Ecran);
        Img =
            XCreateImage(image2.display, Visuel,
                         DefaultDepth(image2.display, Ecran), ZPixmap, 0,
                         Data, Largeur, Hauteur, 8, 0);
        XPutImage(image2.display, image2.window, gc, Img, 0, 0, 10, 10,
                  Largeur, Hauteur);
        break;

    default:
        fprintf(stderr, "Vous avez entre un mauvais numero d'image\n");
    }

#endif

    (void) Data;
    (void) No;
    (void) Largeur;
    (void) Hauteur;
}
