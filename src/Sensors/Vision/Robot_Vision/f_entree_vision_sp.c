/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_entree_vision_sp.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
     Fonction Raoul? : Dans la prise de panorama, les activites des neurones	
 de cette fonction representent une imagette apres transformation. 		
 REMARQUE: SI un PTM2 est en sortie, ne pas oublier de mettre les s2 a 1	
 pour qu declencher l'apprentissage des valeurs analogiques s1		

Macro:
-none 

Local variables:
-int posx
-int xmax_contour
-int ymax_contour
-unsigned char * im_contour

Global variables:
-none

Internal Tools:
-getposition()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <math.h>
#include "tools/include/local_var.h"
int getposition(int i);

int getposition(int i)
{
    if (i < 8)
        return (i);             /* pour les 8 premiers pixels, l'echelle est lineaire */
    i = exp((double) i / 3.8);  /* puis logarithmique */
    return (i);
}

void function_entree_vision_sp(int numero)
{
    int i, j, k, l;
    int p1, milieu;
    int deb, sum, nbr;
    int xmax2, ymax2;
    int xstart, xstop;
    int ystart, ystop;
    /*   int max,pos,decalage,correspondant; */
    int gpe_entree1, nofind = 1;

    printf("-------------------------------------------------\n");

    /*printf("RAOUL: fabrication de l'entree visuelle en niveau de gris\n"); */

    for (i = 0; (i < nbre_liaison) && nofind; i++)
    {
        if (liaison[i].arrivee == numero)
        {
            gpe_entree1 = liaison[i].depart;
            nofind = 0;
        }
    }

    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    milieu = xmax2 / 2;

    for (i = 0; i < xmax2; i++)
        for (j = 0; j < ymax2; j++)
        {
            p1 = deb + i + j * xmax2;
            if (i < milieu)
            {
                xstop = posx - getposition(abs(i - milieu));
                xstart = posx - (getposition(abs(i - milieu) + 1) - 1);
            }
            else
            {
                xstart = posx + getposition(abs(i - milieu));
                xstop = posx + (getposition(abs(i - milieu) + 1) - 1);
            }
            if (xstart < 0)
                xstart = 0;
            if (xstop >= xmax_contour)
                xstop = xmax_contour - 1;
            ystart = j * ymax_contour / ymax2;
            ystop = (j + 1) * ymax_contour / ymax2;

            /* moyenne sur la fenetre */
            sum = 0;
            nbr = 0;
            for (k = xstart; k <= xstop; k++)
                for (l = ystart; l < ystop; l++, nbr++)
                    sum += im_contour[k + l * xmax_contour];

            if (nbr)
                neurone[p1].s = neurone[p1].s1 = neurone[p1].s2 =
                    (float) sum / ((float) nbr * 255.);
            else
                neurone[p1].s = neurone[p1].s1 = neurone[p1].s2 = 0.;
            neurone[p1].s2 = 1;
        }
}
