/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * f_shift_ext_non_circular.c
 *
 *  Created on: 17 juil. 2012
 *      Author: nico
 */

#include <stdlib.h>
#include <string.h>

#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
void function_shift_ext_non_circular(int Gpe)
{
   int i,l, nx, ny, offsetgpe=-1, imagegpe=-1, j,x;
   unsigned char *entree, *sortie;
   /*char *ST, *chaine[2]*/;

   if (def_groupe[Gpe].ext == NULL)
   {
      def_groupe[Gpe].ext= (void *) calloc_prom_image(1,320,240,1);  /* HORREUR ...PG. */
   }
   
   i = 0;
   l = find_input_link(Gpe, i);
   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "angle_offset") == 0)  offsetgpe = liaison[l].depart;
      if (strcmp(liaison[l].nom, "image") == 0)          imagegpe = liaison[l].depart;
      i++;
      l = find_input_link(Gpe, i);
   }
   
   if(offsetgpe==-1) EXIT_ON_ERROR("offsetgpe=-1 need link angle_offset\n");
   if(imagegpe==-1) EXIT_ON_ERROR("imagegpe=-1 need link image\n");
   
   dprints("ima gpe:%d, offset gpe :%d\n",imagegpe,offsetgpe);
   entree=((prom_images_struct*)def_groupe[imagegpe].ext)->images_table[0];
   nx=((prom_images_struct*)def_groupe[imagegpe].ext)->sx;
   ny=((prom_images_struct*)def_groupe[imagegpe].ext)->sy;
   sortie=((prom_images_struct*)def_groupe[Gpe].ext)->images_table[0];
   l=(int) neurone[def_groupe[offsetgpe].premier_ele].s1;
   cprints("offset:%d\n",l);
   if (((prom_images_struct*)def_groupe[imagegpe].ext)!=NULL)
      for (i=0; i<nx; i++)
      {
         for (j=0; j<ny; j++)
         {
            x=i+l;
            if (x>=0&&x<nx)
               sortie[x+j*nx]=entree[i+j*nx];
            else
               sortie[i+j*nx]=0;

         }

      }

}
