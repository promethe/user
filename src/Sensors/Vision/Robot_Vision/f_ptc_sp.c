/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_ptc_sp.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
      RAOUL: Je vais enlever les variables globales de cette fonction, et en meme temps,
  je lui rajoute ce qui manque et qui a ete fait dans function_cpt_sp 
     im_contour[p]  c est l image utilisee et traitee. On va donc la calibrer sur
  l'extension  
    projette l'image en NG sur 1d derive et prend les max comme points caracteristiques 

Macro:
-none 

Local variables:
-int flag_affiche_pano

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <Struct/prom_images_struct.h>

#include "tools/include/local_var.h"

void function_ptc_sp(int gpe_sortie)
{
    static int mask[] = { -1, -1, -1, -1, -1, 0, 1, 1, 1, 1, 1 };   /* defin. du masque pour la derive */
    static int taille_mask = 5; /* taille du masque (longueur-1)/2) */
    int i, j, k, p;
    int med, min, moyen;
    int *val1d, *val1dp;
    float normalisation = 0.;

    int large = 20;
    int r_large = 68;           /* valeur tiree des effets de bords de f_EV_sp */

    /* RAOUL: Nouvelles Variables */
    int xmax_contour, ymax_contour, xmax_carac, /*ymax_carac,*/ gpe_image = 0;
    unsigned char *im_carac, *im_carac2, *im_contour;
    prom_images_struct *p_image;
    prom_images_struct *image_in;

    flag_affiche_pano = 1;
    /*RAOUL: On cherche le groupe qui aboutit a la fonction_ptc_sp */
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == gpe_sortie)
        {
            gpe_image = liaison[i].depart;
            break;
        }
    }

    /*RAOUL: On verifie si l extension du groupe precedent correspond a ce qu on voulait */
    image_in = (prom_images_struct *) def_groupe[gpe_image].ext;
    if (image_in == NULL)
    {
        printf("RAOUL: il n y a pas d image a traiter. BUG  (f_ptc_sp)\n");
        exit(-1);
    }

    /*RAOUL: On verifie si l extension du groupe actuel est prise */

    p_image = (prom_images_struct *) def_groupe[gpe_sortie].ext;
    if (p_image == NULL)
    {
        /*printf("RAOUL: p_image est defini une fois  (f_ptc_sp)\n"); */
        p_image = (prom_images_struct *) malloc(sizeof(prom_images_struct));    /*allocation du pointeur */
        if ((p_image) == NULL)
        {
            printf("function_ptc_sp: ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        /*RAOUL: je fait la copie de l'extension */
        /*def_groupe[gpe_sortie].ext-> */
        p_image->image_number = 4;  /*4 car il y a des images temporaires */
        p_image->nb_band = image_in->nb_band;
        p_image->sx = image_in->sx;
        p_image->sy = 1;
        p_image->images_table[0] = (unsigned char *) malloc((image_in->sx * 1) * sizeof(unsigned char));    /*im_carac resultat */
        p_image->images_table[1] = (unsigned char *) malloc((image_in->sx * 1) * sizeof(unsigned char));    /*im_carac2 */
        p_image->images_table[2] = (unsigned char *) malloc((image_in->sx * 1) * sizeof(int));  /*val1d */
        p_image->images_table[3] = (unsigned char *) malloc((image_in->sx * 1) * sizeof(int));  /*val1dp */

        if (p_image->images_table[0] == NULL
            || p_image->images_table[1] == NULL
            || p_image->images_table[2] == NULL
            || p_image->images_table[3] == NULL)
        {
            printf("function_ptc_sp: ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

    }

    /*image d entree */
    xmax_contour = image_in->sx;
    ymax_contour = image_in->sy;
    im_contour = image_in->images_table[0];

    /*images resultat et temporaires */
    xmax_carac = p_image->sx;
    /*ymax_carac = 1;*/
    im_carac = p_image->images_table[0];

    im_carac2 = p_image->images_table[1];
    val1d = (int *) p_image->images_table[2];
    val1dp = (int *) p_image->images_table[3];


    med = ymax_contour / 2;
    min = med - (ymax_contour * 0.32);


    for (i = 0; i < xmax_carac; i++)
        val1d[i] = 0;

    for (p = 0, j = 0; j < ymax_contour; j++)
        for (i = 0; i < xmax_contour; i++, p++)
        {
            /* les points centraux sont plus importants */
            if (abs(j - med) > min)
                val1d[i] += im_contour[p] / 10;
            else
                val1d[i] += im_contour[p];
        }

    normalisation = 0.;
    for (j = 0; j < ymax_contour; j++)
    {
        /* les points centraux sont plus importants */
        if (abs(j - med) > min)
            normalisation = normalisation + .10;
        else
            normalisation = normalisation + 1.0;
    }

    for (i = 0; i < xmax_contour; i++)
        im_carac[i] = (unsigned char) ((float) val1d[i] / normalisation);

    for (i = 0; i < xmax_carac; i++)
        val1d[i] = 0;

    /*on moyenne avant de deriver */
    moyen = 4;
    for (i = moyen; i < (xmax_carac - moyen); i++)
        for (j = -moyen; j <= moyen; j++)
            val1d[i] += abs(im_carac[i + j]);
    for (i = 0; i < xmax_carac; i++)
        im_carac[i] = val1d[i] / (moyen * 2 + 1);


    /* on derive */
    for (i = 0; i < xmax_carac; i++)
        val1d[i] = 0;

    for (i = taille_mask; i < (xmax_carac - taille_mask); i++)
        for (k = -taille_mask; k <= taille_mask; k++)
            val1d[i] += mask[k + taille_mask] * im_carac[i + k];

    /*on moyenne */
    for (i = 0; i < xmax_carac; i++)
        val1dp[i] = 0;

    moyen = 6;
    for (i = moyen; i < (xmax_carac - moyen); i++)
        for (j = -moyen; j <= moyen; j++)
            val1dp[i] += abs(val1d[i + j]);

    for (i = 0; i < xmax_carac; i++)
        im_carac[i] = val1dp[i] / (moyen * 2 + 1);

    /* recherche des max locaux dans 20 -20 */

    /*  for(i=0; i<large; i++)  Raoul: Ne tient pas compte de f_EV_sp (voir ci-dessus) */
    for (i = 0; i < r_large; i++)
        im_carac2[i] = im_carac2[xmax_contour - i - 1] = 0; /*NONODEBUG : -1 rajoute (grobug) */

    for (i = r_large; i < (xmax_contour - r_large); i++)
    {
        im_carac2[i] = im_carac[i];
        for (j = -large; (j <= large) && im_carac2[i]; j++)
            if (im_carac[i] < im_carac[i + j])
                im_carac2[i] = 0;
    }

    for (i = 1; i < xmax_contour; i++)
        if (im_carac2[i] == im_carac2[i - 1])
            im_carac2[i] = 0;

    for (i = 0; i < xmax_contour; i++)
        im_carac[i] = im_carac2[i];


/*RAOUL: On fait le cast vers l extension */
    def_groupe[gpe_sortie].ext = (void *) p_image;

}
