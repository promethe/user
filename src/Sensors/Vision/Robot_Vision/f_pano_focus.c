/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
 \file  f_pano_focus.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 RAOUL: Cette fonction usilise une tonne de variables globales.
 Je vais nettoyer tout ca...

 Macro:
 -none

 Local variables:
 -int ymax_contour
 -unsigned char * im_contour
 -int xmax_carac
 -int ymax_carac
 -unsignd char *im_carac
 -unsignd char *im_carac2
 -int position_courante_x
 -int position_courante_y
 -int flag_affiche_pano
 -char nom_image[256]
 -int posx
 -int posy

 Global variables:
 -int NbrFeaturesPoints

 Internal Tools:
 -affiche()
 -affiche_point_de_vue()
 -Affiche_Fenetre()

 External Tools:
 -tools/erreur/message()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ***********************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Struct/prom_images_struct.h>
#include "tools/include/local_var.h"
#include <Global_Var/IO_Robot.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/affiche.h"
#include "tools/include/Affiche_Fenetre.h"
void affiche_point_de_vue(int xmax_carac, int xmax_contour);

void affiche_point_de_vue(int xmax_carac, int xmax_contour) {
#ifndef AVEUGLE
	TxPoint point /*,point2 */;
	int pas;
	int min, max;
	double dmin, dmax;

	if (NbrTexture) {
		dmin = exp((log(1.0 + (double) rayon_vision_text)
				* ((double) const_log_text)) / 32.0) - 1.0;
		dmax = exp((log(1.0 + (double) rayon_vision_text) * (31.0
				+ (double) const_log_text)) / 32.0) - 1.0;
	} else {
		dmin = exp((log(1.0 + (double) rayon_vision_ng)
				* ((double) const_log_ng)) / 32.0) - 1.0;
		dmax = exp((log(1.0 + (double) rayon_vision_ng) * (31.0
				+ (double) const_log_ng)) / 32.0) - 1.0;
	}

	pas = 256 / xmax_carac;
	if (pas < 1)
		pas = 1;

	min = (int) (dmin * pas);
	max = (int) (dmax * pas);

	if (min < 0)
		min = 0;
	if (max < 0)
		max = abs(max);

	point.x = 10 + position_courante_x * pas;
	point.y = 10 + position_courante_y * pas;

	/*TxDessinerCercle(&image2,bleu,TxVide,point,min,1);
	 TxDessinerCercle(&image2,rouge,TxVide,point,max,1); */

	pas = 256 / xmax_contour;
	if (pas < 1)
		pas = 1;

	min = (int) (dmin * pas);
	max = (int) (dmax * pas);

	if (min < 0)
		min = 0;
	if (max < 0)
		max = abs(max);

	point.x = 10 + posx * pas;
	point.y = 10 + posy * pas;

	TxDessinerCercle(&image1, bleu, TxVide, point, min, 1);
	TxDessinerCercle(&image1, rouge, TxVide, point, max, 1);

	TxFlush(&image1);
	/*TxFlush(&image2); */
#endif

	(void) xmax_carac;
	(void) xmax_contour;
}

void function_pano_focus(int gpe_sortie) {
	int max;
	int i, j, p, p2, p_old;
	int find = 0;
	/*   static char rep[80]; */
	static char old_nom[256];

	/* RAOUL: Nouvelles Variables */
	/*int xmax_contour,ymax_contour,xmax_carac,ymax_carac; */
	int gpe_ptc_sp = 0, gpe_image = 0;
	/* unsigned char *im_carac, *im_carac2, *im_contour; */
	/* char *im_c_aff; */
	prom_images_struct *p_image; /* prom image de pano focus */
	prom_images_struct *p_image_ptc; /* prom image venant de ptc */
	prom_images_struct *p_image_pano; /* prom image venant de get pano alt */

#ifndef AVEUGLE
	fprintf(
			stdout,
			"%s\n",
			(char*) "----------------------------------------------------------------\n");
	fprintf(stdout, "%s\n", (char*) "RAOUL Fonction pano focus\n");

#endif

	/*RAOUL: On cherche le groupe qui aboutit a la fonction_pano_focus */
	for (i = 0; i < nbre_liaison; i++) {
		if (liaison[i].arrivee == gpe_sortie) {
			/* On passe ici le test du nom */
			if ((strcmp(def_groupe[liaison[i].depart].nom, "f_gradH") == 0)
					|| (strcmp(def_groupe[liaison[i].depart].nom, "f_ptc_sp")
							== 0)) {
				gpe_ptc_sp = liaison[i].depart;
				/*printf("f_ptc_sp trouve: groupe:%d\n",depart); */
			}
			if ((strcmp(def_groupe[liaison[i].depart].nom, "f_get_pano_alt")
					== 0) || (strcmp(def_groupe[liaison[i].depart].nom,
					"f_get_pano") == 0)
					|| (strcmp(def_groupe[liaison[i].depart].nom,
							"f_load_image_from_disk") == 0) || (strcmp(
					def_groupe[liaison[i].depart].nom, "f_load_baccon") == 0)
					|| (strcmp(def_groupe[liaison[i].depart].nom,
							"f_extend_image_panoramic") == 0)
					|| (strcmp(def_groupe[liaison[i].depart].nom,
							"f_convert_RGB_NB") == 0)) {
				gpe_image = liaison[i].depart;
				/*printf("f_get_pano_alt trouve: groupe:%d\n",gpe_image); */
			}
			/*break; */
		}
	}

	/*RAOUL: On verifie si l extension du groupe precedent correspond a ce qu on voulait */
	/*RAOUL TEST printf("RAOUL: Normalement, on doit voir ci-dessus les valeurs des groupes\n"); */

	p_image_ptc = (prom_images_struct *) def_groupe[gpe_ptc_sp].ext;
	p_image_pano = (prom_images_struct *) def_groupe[gpe_image].ext;

	if (p_image_ptc == NULL) {
		printf(
				"RAOUL: il n y a pas d image a traiter depuis f_ptc_sp. BUG  (f_pano_focus)\n");
		exit(-1);
	}
	if (p_image_pano == NULL) {
		printf(
				"RAOUL: il n y a pas d image a traiter depuis f_get_pano_alt. BUG  (f_pano_focus)\n");
		exit(-1);
	}

	/*RAOUL: On verifie si l extension du groupe actuel est prise */
	p_image = (prom_images_struct *) def_groupe[gpe_sortie].ext;

	if (p_image == NULL) {
		/*printf("RAOUL: p_image est defini une fois  (f_pano_focus)\n"); */
		p_image = (prom_images_struct *) malloc(sizeof(prom_images_struct)); /*allocation du pointeur */
		if ((p_image) == NULL) {
			printf("ALLOCATION IMPOSSIBLE ...! \n");
			exit(-1);
		}

		/*RAOUL: je fait la copie de l'extension */
		/*def_groupe[gpe_sortie].ext-> */
		p_image->image_number = 3; /*3 car il y a des images temporaires */
		p_image->nb_band = p_image_ptc->nb_band;
		p_image->sx = p_image_ptc->sx;
		p_image->sy = p_image_ptc->sy;
		p_image->images_table[0] = (unsigned char *) malloc((p_image_ptc->sx
				* p_image_ptc->sy) * sizeof(unsigned char)); /*im_carac resultat */
		p_image->images_table[1] = (unsigned char *) malloc((p_image_ptc->sx
				* p_image_ptc->sy) * sizeof(unsigned char)); /*im_carac2 */
		p_image->images_table[2] = (unsigned char *) malloc((p_image_pano->sx
				* p_image_pano->sy) * sizeof(char)); /*im_c_aff */

		if (p_image->images_table[0] == NULL || p_image->images_table[1]
				== NULL || p_image->images_table[2] == NULL) {
			printf("fonction_pano_focus: ALLOCATION IMPOSSIBLE ...! \n");
			exit(-1);
		}
	}

	/*image d entree */
	xmax_contour = p_image_pano->sx; /* dimensions de l image originale */
	ymax_contour = p_image_pano->sy; /* idem */
	im_contour = p_image_pano->images_table[0]; /* dans l ancienne fonction ctr, im_contour etait en fait l image originale... */

	/*images resultat et temporaires */
	xmax_carac = p_image->sx;
	ymax_carac = p_image->sy;
	im_carac = p_image_ptc->images_table[0]; /*p_image->images_table[0]; */
	im_carac2 = p_image->images_table[1];
	/*  im_c_aff	= (char *)p_image->images_table[2];

	 #ifndef AVEUGLE
	 for(i=0; i<xmax_contour*ymax_contour; i++) im_c_aff[i] = Transcode[(int)(im_contour[i])>>4];
	 #endif
	 */
	/* RAOUL: Debut de la procedure pano_focus */

	p_old = position_courante_x + position_courante_y * xmax_carac;

	memset(im_carac2, 0, xmax_carac * ymax_carac);

	position_courante_x = 0;
	position_courante_y = 0;

	/* RAOUL: image1 et image2 ne sont pas definis ici. Qu'en fait-on? Pareil pour im_cont */

#ifndef AVEUGLE
	/* RAOUL : Ce truc ne marche pas, et ca commence a me courir serieusement...*/
	/*  if(NbrTexture)
	 affiche_multi(&image1,im_cont_text,xmax_cont_text,ymax_cont_text);
	 else
	 if(flag_affiche_pano==1)		*if(strcmp(old_nom, nom_image))*
	 if(im_c_aff)
	 Affiche_Fenetre(1,im_c_aff,xmax_contour,ymax_contour);
	 else
	 *affiche(&image1,im_contour,xmax_contour,ymax_contour);*
	 Affiche_Fenetre(1,im_contour,xmax_contour,ymax_contour);*/

	if (flag_affiche_pano == 1) {
		affiche(&image1, im_contour, xmax_contour, ymax_contour);
		/*affiche(&image2,im_carac,xmax_carac,ymax_carac); */
		Affiche_Fenetre(1, (char *) im_contour, xmax_contour, ymax_contour);

		flag_affiche_pano = 0;
	}
	strcpy(old_nom, nom_image);
#endif

	/*On recupere le max dans im_carac */
	max = 0;
	position_courante_x = 0;
	position_courante_y = 0;

	for (j = 0; j < ymax_carac; j++)
		for (i = 0; i < xmax_carac; i++) {
			p = i + j * xmax_carac;
			if (im_carac[p] > (unsigned char) max) {
				max = im_carac[p];
				position_courante_x = i;
				position_courante_y = j;
			}
		}

	/* transformation des coordonnees de la position du system
	 image points caracteristiques, images contours */
	posx = position_courante_x * xmax_contour / xmax_carac;
	if (ymax_carac == 1) {
		/* points caracteristiques sur une ligne */
		/* on prend alors le milieu de l'image   */
		posy = ymax_contour / 2;
	} else
		posy = position_courante_y * ymax_contour / ymax_carac;

#ifndef AVEUGLE
	printf("                coord effectives : x= %d , y= %d \n", posx, posy);

	/*Ici, on a besoin de xmax_contour et xmax_carac*/

	affiche_point_de_vue(xmax_carac, xmax_contour);
#endif
	p = position_courante_x + position_courante_y * xmax_carac;
	im_carac2[p] = im_carac[p];

	/* on efface le point s'il reste d'autres points de focalisation */
	for (j = 0; (j < ymax_carac) && (!find); j++)
		for (i = 0; (i < xmax_carac) && (!find); i++) {
			p2 = i + j * xmax_carac;
			if ((p2 != p) && (im_carac[p2] != 0))
				find = 1;
		}
	if (find) {
		im_carac[p] = 0;

		/* inhibition sur toute la colonne de 20 points */
		p2 = p % xmax_carac;
		for (i = p2 - 10; i < (p2 + 10); i++)
			if ((i >= 0) && (i < xmax_carac))
				for (j = 0; j < ymax_carac; j++)
					im_carac[i + j * xmax_carac] /= 4;
	}

	/* Sorin

	 p2=p%xmax_carac;
	 for(i=p2-xmax_carac/32; i<(p2+xmax_carac/32); i++)
	 if((i>=0)&&(i<xmax_carac))
	 for(j=0; j<ymax_carac; j++)
	 im_carac[i+j*xmax_carac] /= 2;
	 }
	 */
	/* nombre de points explores */
	if (p_old != p)
		NbrFeaturesPoints++;

	/*RAOUL: On fait le cast vers l extension */
	def_groupe[gpe_sortie].ext = (void *) p_image;

	(void) old_nom;

}
