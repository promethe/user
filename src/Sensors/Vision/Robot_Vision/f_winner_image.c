/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\ingroup libSensors
\defgroup f_winner_image f_winner_image

Author: Ali K.
Created: 07/12/2014

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

- Cette fonction stock un nombre d'image sequentiellement puis renvoie l'image gagnante.
- Gereuniquement les images N&B en uchar
Option:

- I : groupe qui contient l'image
- W : Groupe qui contient la gestion du winner

Macro:

Local variables:
-none

Global variables:
-none

Internal Tools:

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>


typedef struct data_winner_image{
  int gpe_im;
  int gpe_win;
  int gperest;
  int decalage;
} Data_winner_image;


void new_winner_image(int gpe)
{
  int i,l;
  char param[256];
  Data_winner_image *my_data = NULL;
  if(def_groupe[gpe].data == NULL)
  {
    my_data = (Data_winner_image *) malloc(sizeof(Data_winner_image));

    my_data->gpe_im = my_data->gpe_win  = -1;
    i = 0;
    l = find_input_link(gpe, i);
    while (l != -1)
    {
      if(strstr(liaison[l].nom,"sync")!= NULL)
      {}
      else
      {
        if (prom_getopt(liaison[l].nom, "I", param) == 1 || prom_getopt(liaison[l].nom, "i", param) == 1)
          my_data->gpe_im = liaison[l].depart;

        if (prom_getopt(liaison[l].nom, "W", param) == 1 || prom_getopt(liaison[l].nom, "w", param) == 1)
          my_data->gpe_win=liaison[l].depart;
        if(!strcmp(liaison[l].nom, "transition_detect"))
          my_data->gperest = liaison[l].depart;

      }
      i = i + 1;
      l = find_input_link(gpe, i);
    }
  }
  if( my_data->gpe_im == -1 ||  my_data->gpe_win  == -1)
    EXIT_ON_ERROR("le groupe %s doit avoir deux liens -I et -W en entree \n",def_groupe[gpe].no_name);
  my_data->decalage = 0;
  def_groupe[gpe].data = my_data;
}
void function_winner_image(int gpe)
{
  Data_winner_image  *mydata   = NULL;
  prom_images_struct *rez_image;
  unsigned char *inputim;
  float  maxi;
  int gpe_win, gpe_im, nbim, firstwin, first ,posmax , winnbre, gpereset;

  register int i;
  unsigned int nx, ny;
  unsigned int n;

  dprints("====debut %s\n",__FUNCTION__);
  mydata = (Data_winner_image*) def_groupe[gpe].data;
  gpe_im = mydata->gpe_im;
  gpe_win = mydata->gpe_win;
  gpereset = mydata->gperest;
  nbim = def_groupe[gpe].nbre;
  if(def_groupe[gpe_im].ext == NULL)
  {
    PRINT_WARNING("PAS D IMAGE EN ENTREE DU GROUPE %s",def_groupe[gpe].no_name);
    return;
  }
  /* initialisation */
  if (def_groupe[gpe].ext == NULL)
  {
    /* alocation de memoire */
    def_groupe[gpe].ext =  (prom_images_struct *) malloc(sizeof(prom_images_struct));
    if (def_groupe[gpe].ext == NULL)
      EXIT_ON_ERROR("ALLOCATION IMPOSSIBLE ...! \n");


    /* recuperation des infos sur la taille de l'image du groupe precedent et initialisation de l'image de ce groupe avec les infos recuperees */
    nx = ((prom_images_struct *) def_groupe[mydata->gpe_im].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[mydata->gpe_im].ext)->sy;
    if(((prom_images_struct *) def_groupe[mydata->gpe_im].ext)->nb_band !=1)
      EXIT_ON_ERROR("Le groupe %s ne gere que les image N&B en uchar\n",def_groupe[gpe].no_name);
    def_groupe[gpe].ext = calloc_prom_image(10, nx, ny, 1);
    n = nx * ny;
    dprints ("%s Taille image %d x %d\n",__FUNCTION__,nx,ny);

  }

  rez_image = ((prom_images_struct *) def_groupe[gpe].ext);
  nx = rez_image->sx;
  ny = rez_image->sy;
  n = nx * ny;
  /*recupere l'image d'entree*/
  inputim = ((prom_images_struct *) def_groupe[mydata->gpe_im].ext)->images_table[0];


  firstwin =   def_groupe[gpe_win].premier_ele;
  winnbre = def_groupe[gpe_win].nbre;
  first = def_groupe[gpe].premier_ele;

  if(neurone[def_groupe[gpereset].premier_ele].s1 > 0.5)
  {
    mydata->decalage = 0;
    for(i=0;i<nbim;i++)
      neurone[first+i].s=neurone[first+i].s1=neurone[first+i].s2=0.;
  }




  /*trouver le gagnant sur le groupe winner*/
  maxi = -9999999;
  for(i=0 ; i < winnbre ; i++)
  {
    if(neurone[firstwin+i].s1 > maxi)
      maxi = neurone[firstwin+i].s1;
  }
  /*mettre les entree (neurone et image) en file sequentiellement*/
  neurone[first+mydata->decalage].s =
      neurone[first+mydata->decalage].s1  =
          neurone[first+mydata->decalage].s2 = maxi;

  memcpy(rez_image->images_table[mydata->decalage + 1],inputim,n*sizeof(unsigned char));


  mydata->decalage ++;
  if(mydata->decalage > nbim) mydata->decalage = 0;
  maxi = -9999999;
  posmax = -1;
  for(i=0;i<nbim;i++)
  {
    if(neurone[first+i].s1 > maxi && neurone[first+i].s1 > 0.)
    {
      maxi = neurone[first+i].s1;
      posmax = i;
    }
  }
 if(posmax == -1)
   memset(rez_image->images_table[0],0,n);
 else
  memcpy(rez_image->images_table[0],rez_image->images_table[posmax + 1],n*sizeof(unsigned char));


}


void destroy_winner_image(int gpe)
{
  int i;
  for (i = 0; i < def_groupe[gpe].nbre + 1; i++)
  {
    free(((prom_images_struct *) def_groupe[gpe].ext)->images_table[i]);
    ((prom_images_struct *) def_groupe[gpe].ext)->images_table[i] = NULL;
  }

  free(def_groupe[gpe].data);
  free(def_groupe[gpe].ext);
  def_groupe[gpe].data = NULL;
  def_groupe[gpe].ext = NULL;
  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
}


