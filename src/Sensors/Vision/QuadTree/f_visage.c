/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_visage.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  
Macro:
-PI

Local variables:
-none

Global variables:
-none

Internal Tools:
-load_param_face()
-detection_visage()
-recadrer()
-reechant()

External Tools: 
-tools/Vision/calloc_prom_image()
-tools/Vision/free_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:
  parametres, s_cadre : structures definies dans visages_tools.h

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <Struct/s_cadre.h>
#include <string.h>
#include <Struct/parametres.h>

#include "tools/include/macro.h"

#include <public_tools/Vision.h>

#include "tools/include/load_param_face.h"
#include "tools/include/detection_visage.h"
#include "tools/include/recadrer.h"
#include "tools/include/reechant.h"

void function_visage(int gpe_sortie)
{
    int i, nb, link = -1, gpe_entree = -1, t;
/*  int nb_iteration=10, para_teinte,para_fusion ;*/
    float R, G, B, teinte;
/*  float para_ratio ;*/
    float rg, gb, br, vmin;
    static parametres para = { 50, 6, 0.4, 8, 1, 10, 32, -1, 0 };   /*{50,6,0.4,8,1,10,32,1,0} ; */
    s_cadre cadre;
    prom_images_struct *im_pers_sans_fond = NULL, *sans_fond_NG =
        NULL, *sans_fond_teinte = NULL;
    prom_images_struct *im_visage_recadre = NULL, *visage_reechant = NULL;

#ifndef AVEUGLE
    TxPoint point;
#endif

    /*  printf("\n--------------------------------------\n") ;
       printf("--- FONCTION CADRAGE SUR LE VISAGE ---\n") ; */

    /* Recherche du gpe d'entree */
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == gpe_sortie)
        {
            printf("lien = %s %d \n", liaison[i].nom,
                   strcmp(liaison[i].nom, "image"));
            if (strcmp(liaison[i].nom, "image") == 0)
            {
                printf("lien OK\n");
                gpe_entree = liaison[i].depart;
                link = i;
                break;
            }

        }
    }

    /* Pour verifier l'existence du lien */
    if (link == -1)
    {
        printf
            ("Erreur : ce groupe n'a pas d'entree ou n'a pas de lien portant le nom image!\n");
        exit(EXIT_FAILURE);
    }

    /* Pour tester si le .ext des gpes d'entree ont une 1 bande prom_image */
    im_pers_sans_fond = (prom_images_struct *) def_groupe[gpe_entree].ext;

    if (im_pers_sans_fond == NULL)
    {
        printf
            ("Probleme function_visage : il n'y a pas d'image dans le gpe d'entree %i\n",
             gpe_entree);
        exit(EXIT_FAILURE);
    }

    /* Parametres par defaut */

    if (para.oeil == -1)
        load_param_face(&para);

    /*  printf("valeur de la teinte= %d, valeur du ratio = %f  , nb iter = %d\n ", para.teinte, para.ratio, para.fusion); fflush(stdout); */



    /* Conversion de im_pers_sans_fond               */
    /* en niveaux de gris (NG) et en image de teinte */
    sans_fond_NG =
        calloc_prom_image(1, im_pers_sans_fond->sx, im_pers_sans_fond->sy, 1);
    sans_fond_teinte =
        calloc_prom_image(1, im_pers_sans_fond->sx, im_pers_sans_fond->sy, 1);

    vmin = 4.;

    nb = im_pers_sans_fond->nb_band;
    for (i = 0; i < im_pers_sans_fond->sx * im_pers_sans_fond->sy; i++)
    {
        R = (float) (im_pers_sans_fond->images_table[0][nb * i]);
        G = (float) (im_pers_sans_fond->images_table[0][nb * i + 1]);
        B = (float) (im_pers_sans_fond->images_table[0][nb * i + 2]);

        sans_fond_NG->images_table[0][i] =
            (unsigned char) ((int) ((R + G + B) / nb));

        /* if (R==0 && G==0 && B==0) sans_fond_teinte->images_table[0][i] = 
           (unsigned char) 0 ; */
        if (R < vmin && G < vmin && B < vmin)
            sans_fond_teinte->images_table[0][i] = (unsigned char) 0;
        else
        {
            if (fabs(R - G) < vmin && fabs(G - B) < vmin)
                sans_fond_teinte->images_table[0][i] = (unsigned char) 0;
            else
            {
                /*teinte = (float)((180/PI)*acos((double)(0.5*((R-G)+(R-B))/sqrt((R-G)*(R-G)+(R-B)*(G-B))))) ;
                   if (G>B) teinte = (float)(360 - teinte) ;
                   if ( (teinte<0) || (teinte>360) ) printf("%f, %f, %f\n", R, G, B) ;
                   sans_fond_teinte->images_table[0][i] = (unsigned char)((int)(teinte/360.*255.)) ; */

                rg = R / (G + 1);
                gb = G / (B + 1);
                br = B / (R + 1);
                teinte =
                    ((180. / PI) *
                     acos((double)
                          (0.5 * ((rg - gb) + (rg - br)) /
                           sqrt((rg - gb) * (rg - gb) +
                                (rg - br) * (gb - br)))));
                if (G > B)
                    teinte = (float) (360 - teinte);
                if ((teinte < 0) || (teinte > 360))
                    printf("%f, %f, %f\n", R, G, B);
                sans_fond_teinte->images_table[0][i] =
                    (unsigned char) ((int) (teinte / 360. * 255.));
            }
        }
    }
    /* save_png_to_disk("teinte.png", *sans_fond_teinte, compression) ; */


    t = detection_visage(sans_fond_teinte, &cadre, &para);
    if (t == 0)
    {
        printf("pas de visage detecte...\n");
        cadre.l1 = 0;
        cadre.l2 = sans_fond_teinte->sy;
        cadre.c1 = 0;
        cadre.c2 = sans_fond_teinte->sx;
    }
    else                        /* entoure en rouge le viage trouve */
    {

#ifndef AVEUGLE
        point.x = cadre.l1;
        point.y = cadre.c1;
        TxDessinerRectangle(&image1, rouge, TxVide, point,
                            cadre.l2 - cadre.l1, cadre.c2 - cadre.c1, 2);
        TxFlush(&image1);
#endif

    }

    /* Recadrage du visage */
    /*  printf("\n--------------------\n") ;
       printf("recadrage du visage \n") ; */

    im_visage_recadre =
        calloc_prom_image(1, sans_fond_NG->sx, sans_fond_NG->sy, 1);

    t = recadrer(sans_fond_NG, im_visage_recadre, &cadre);

    for (i = 0; i < im_visage_recadre->sx * im_visage_recadre->sy; i++)
    {
        if (im_visage_recadre->images_table[0][i] == (unsigned char) 0)

            im_visage_recadre->images_table[0][i] =
                (unsigned char) ((im_visage_recadre->images_table[0][i - 1] +
                                  im_visage_recadre->images_table[0][i -
                                                                     im_visage_recadre->
                                                                     sx]) /
                                 2);
    }

    /*   printf("sauvegarde du visage recadre dans : visage_recadre.png \n") ; 
       save_png_to_disk("visage_recadre.png", *im_visage_recadre, compression) ; */

    /* Reechantillonnage */
    if (def_groupe[gpe_sortie].ext == NULL)
    {
        visage_reechant = calloc_prom_image(1, 60, 80, 1);
        def_groupe[gpe_sortie].ext = visage_reechant;
    }
    else
        visage_reechant = def_groupe[gpe_sortie].ext;

    t = reechant(im_visage_recadre, visage_reechant);

    /*    printf("sauvegarde du visage recadre & reechantillonne ds : visage_reech.png \n") ; 
       save_png_to_disk("visage_reech.png", *visage_reechant, compression) ;   */

    free_prom_image(sans_fond_NG);
    /*free_prom_image(sans_fond_teinte) ; */
    free_prom_image(im_visage_recadre);
}
