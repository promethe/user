/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  detection_teinte.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Detection de la teinte dans l'image des teintes
les resultats sont renvoyes dans para

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-split_image()
-merge_regions()
-update_cadre()
-free_liste_regions()

External Tools: 
-tools/Vision/calloc_prom_image()
-tools/Vision/free_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <Struct/s_cadre.h>
#include <Struct/region.h>
#include <Struct/liste.h>
#include <Struct/parametres.h>

#include <public_tools/Vision.h>

#include "include/split_image.h"
#include "include/merge_regions.h"
#include "include/update_cadre.h"
#include "include/free_liste_regions.h"

int detection_teinte(prom_images_struct * image_teinte, s_cadre * extrait,
                     parametres * para)
{
    int i;
    liste *bonnes_regions = NULL, *merge_region = NULL, *pt;
    region *region_init = NULL;
    int flag = 0;
    /*unused:  char c; */

    /* mis a jour 31/10/2003 Olivier Ledoux */
    int nx, ny;
    prom_images_struct *bloc;
    int startx, starty, endx, endy, xx, yy;
    region *regionCourante;
    float *pointeurFloat;
    /* fin rajout variables */



    /* printf("---- fonction detection_visage ----\n") ; */
    bonnes_regions = (liste *) malloc(sizeof(liste));
    if (bonnes_regions == NULL)
    {
        printf("ERROR Malloc bonnes_regions \n");
        exit(1);
    }

    merge_region = (liste *) malloc(sizeof(liste));
    if (merge_region == NULL)
    {
        perror("Malloc");
        exit(1);
    }
    region_init = (region *) malloc(sizeof(region));
    if (region_init == NULL)
    {
        perror("Malloc");
        exit(1);
    }

    region_init->image =
        calloc_prom_image(1, image_teinte->sx, image_teinte->sy, 1);
    region_init->next_region = NULL;
    region_init->prev_region = NULL;
    region_init->cadre.l1 = 0;
    region_init->cadre.l2 = image_teinte->sx;
    region_init->cadre.c1 = 0;
    region_init->cadre.c2 = image_teinte->sy;
    region_init->surface = 0;

    /* printf("Initialisation des structures \n") ; */
    bonnes_regions->premiere = NULL;
    bonnes_regions->derniere = NULL;
    bonnes_regions->nb_element = 0;

    for (i = 0; i < (int)(image_teinte->sx * image_teinte->sy); i++)
        (region_init->image)->images_table[0][i] =
            image_teinte->images_table[0][i];

    /* printf("sauvegarde du resultat dans region_image.png.\n") ;
       save_png_to_disk("region_image.png", *(region_init->image), compression) ; *//* plantage ici .... */

    split_image(bonnes_regions, region_init, region_init->image, 0,
                para->teinte, para->range, para->ratio, para->iteration);

    /*printf("nb de bonnes regions : %d \n", bonnes_regions->nb_element) ; */

    /*if (!(bonnes_regions->nb_element)) return 0 ; */

    merge_region->nb_element = 0;
    merge_region->premiere = NULL;
    merge_region->derniere = NULL;

/* flag=1;
if (merge_regions(merge_region, bonnes_regions, para->proximite)==0) flag=0; else update_cadre(merge_region, extrait) ;*/

    for (i = 0; i < para->fusion; i++)
    {
        printf("MERGE %d \n", i);
        flag = 1;
        if (merge_regions(merge_region, bonnes_regions, para->proximite) == 0)
            flag = 0;
        else
            update_cadre(merge_region, extrait);
        printf("iter= %d , merge_region = %d , bonnes_regions = %d \n", i,
               merge_region->nb_element, bonnes_regions->nb_element);
        pt = bonnes_regions;
        bonnes_regions = merge_region;
        merge_region = pt;
        if (merge_region->nb_element != 0)
        {
            printf("erreur apres merge ...\n");
            exit(1);
        }
        if (merge_region->premiere != NULL)
        {
            printf("erreur apres merge ...\n");
            exit(1);
        }
        if (merge_region->derniere != NULL)
        {
            printf("erreur apres merge ...\n");
            exit(1);
        }

        /*    merge_region->nb_element = 0 ;
           merge_region->premiere = NULL ;
           merge_region->derniere = NULL ;    */
    }
    printf("fin merge\n");

/* save_png_to_disk("region_image.png", *(region_init->image), compression) ;*/

    /**********************************************
     * mise a jour 31/10/2003 Olivier Ledoux      *
     * pas de traitement des erreurs potentielles *
     **********************************************/


    printf("AVANT MON CODE\n");
    /*
       bonnes_regions est une liste des differents cadres interressessants
       image_teinte contient une seule image en niveau de gris du filtre teinte jaune

       ATTENTION: pour l'abscisse c1/2 et pour l'ordonne l1/2
     */
    bloc = image_teinte;        /* simplification */
    nx = bloc->sx;
    ny = bloc->sy;
    printf("ICI1\n");
    bloc->images_table[1] = (unsigned char *) malloc(nx * ny * sizeof(float));  /* on reserve l'espace pour notre futur masque */
    pointeurFloat = (float *) bloc->images_table[1];
    for (i = 0; i < nx * ny; i++)
        pointeurFloat[i] = 0.25;    /* on prepare notre masque */
    regionCourante = bonnes_regions->premiere;
    while (regionCourante != NULL)
    {
        starty = (regionCourante->cadre).c1;
        endy = (regionCourante->cadre).c2;
        startx = (regionCourante->cadre).l1;
        endx = (regionCourante->cadre).l2;
        printf("startx=%d starty=%d endx=%d endy=%d\n", startx, starty, endx,
               endy);
        /* on a notre cadre courant */
        for (yy = starty; yy < endy; yy++)
            for (xx = startx; xx < endx; xx++)
                pointeurFloat[yy * nx + xx] = pointeurFloat[yy * nx + xx] + 1.25;   /*0.50; */
        /* on passe au cadre suivant */
        regionCourante = regionCourante->next_region;
    }
    /* en sortant, nous avons notre masque dans le bloc 1 de image_teinte */

    /* fin mise a jour */
    printf("LA\n");

    free_liste_regions(bonnes_regions);
    free(bonnes_regions);
    bonnes_regions = NULL;
    free(merge_region);
    merge_region = NULL;
    free_prom_image(region_init->image);
    /* rajout 17/11/2003 Olivier Ledoux */
    free(region_init->image);
    region_init->image = NULL;

    free(region_init);
    region_init = NULL;
    printf("fin detection_visage\n");

    return flag;
}
