/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  recadrer.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Fonction de recadrage de l'image  
   Prend en entree une image Niveau de gris

   Fonction modifiee par F.Balleux pour que les 
   pixels noirs du visage dus au seuillage prennent 
   la couleur de para->teinte.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>
#include <Struct/s_cadre.h>
#include <stdlib.h>

int recadrer(prom_images_struct * imagein, prom_images_struct * imageout,
             s_cadre * cadre)
{
    int i, j, k, l;

    /*  printf("---- fonction recadrer ---- \n") ; */

    if (cadre == NULL)
    {
        printf("ERROR cadre=NULL\n");
        exit(1);
    }
    if (imagein == NULL)
    {
        printf("ERROR imagein=NULL\n");
        exit(1);
    }
    if (imageout == NULL)
    {
        printf("ERROR imageout=NULL\n");
        exit(1);
    }

    /*  printf("cadre : c2 = %d, c1 = %d, l2 = %d, l1 = %d \n",
       cadre->c2, cadre->c1, cadre->l2, cadre->l1) ; */

    imageout->sx = cadre->l2 - cadre->l1;
    imageout->sy = cadre->c2 - cadre->c1;

/*  nb = imagein->nb_band ;*/
    /*  debut = cadre->l1 + (imagein->sx * cadre->c1) ;  */
/*   fin = cadre->l2 + (imagein->sx * cadre->c2) ;   */
/*   bord = debut + (cadre->l2 - cadre->l1) ;   */
/*   saut = imagein->sx - (cadre->l2 - cadre->l1) ; */

    j = 0;
    l = 0;
    for (k = cadre->c1; k < cadre->c2; k++, l++)
    {
        for (i = cadre->l1; i < cadre->l2; i++, j++)
        {
            imageout->images_table[0][j + (imageout->sx * l)] =
                imagein->images_table[0][i + (imagein->sx * k)];

            /*if (imageout->images_table[0][nb*i+(imageout->sx*j)+k] > max)
               max = imageout->images_table[0][nb*i+(imageout->sx*j)+k] ;
               if (imageout->images_table[0][nb*i+(imageout->sx*j)+k] < min)
               min = imageout->images_table[0][nb*i+(imageout->sx*j)+k] ; */
        }
        j = 0;
    }
    /*    printf("sauvegarde du visage recadre ds : visage_recadre.png \n") ;
       save_png_to_disk("visage_recadre.png", *imageout, compression);  */
    return 1;

}
