/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  split_image.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Creation d'une liste de regions 
   correspondant a de la peau 
   
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-test_region()
-push()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/liste.h>
#include <Struct/region.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>

#include "include/push.h"

int test_region(region * R, prom_images_struct * image_init, int teinte,
                int range, float ratio)
{

    int i = 0, k, debut, fin, bord_cadre, saut, nb_visage = 0, nb;
    int surface = 0, taille_max;
#ifndef AVEUGLE
    TxPoint point;
#endif

    /* printf("test region \n"); */
    if (R == NULL)
    {
        printf("ERROR r=NULL \n");
        exit(1);
    }
    if (image_init == NULL)
    {
        printf("ERROR image_init=NULL \n");
        exit(1);
    }

    surface = (R->cadre.l2 - R->cadre.l1) * (R->cadre.c2 - R->cadre.c1);

    nb = image_init->nb_band;
    debut = R->cadre.l1 + (image_init->sx * R->cadre.c1);
    fin = R->cadre.l2 + (image_init->sx * R->cadre.c2);
    bord_cadre = debut + (R->cadre.l2 - R->cadre.l1);
    saut = image_init->sx - (R->cadre.l2 - R->cadre.l1);

    taille_max = image_init->sx * image_init->sy;

    for (i = debut; i < fin; i++)
    {
        for (k = 0; k < nb; k++)
        {
            if (nb * i + k >= taille_max)
                break;
            if ((image_init->images_table[0][nb * i + k] > teinte - range) &&
                (image_init->images_table[0][nb * i + k] < teinte + range))
                nb_visage++;
        }
        if (((i - bord_cadre) % image_init->sx) == 0)
            i = i + saut;
    }


    if ((nb_visage) > (int) (surface * ratio))
    {
#ifndef AVEUGLE
        point.x = R->cadre.l1;
        point.y = R->cadre.c1;
        /* point.x= R->cadre.l1;point.y=R->cadre.c1 ; */
        TxDessinerRectangle(&image1, vert, TxVide, point,
                            R->cadre.l2 - R->cadre.l1,
                            R->cadre.c2 - R->cadre.c1, 2);
        TxFlush(&image1);
#endif

        return 1;
    }



    /* TxFlush(&image1); */
    if (!nb_visage)
        return -1;
    return 0;
}

void split_image(liste * bonnes_regions, region * R,
                 prom_images_struct * im_init, int iter, int teinte,
                 int range, float ratio, int nb_iteration)
{
    int bonne;
    region *R1, *R2, *R3, *R4;
    /* R1 : region en haut a gauche de R
       R2 : region en haut a droite de R
       R3 : region en bas a gauche de R
       R4 : region en bas a droite de R */

    /*  printf("---- fonction split_image ----\n") ;
       printf("iter = %d, nb_iteration = %d \n", iter, nb_iteration) ; */

    if (iter > nb_iteration)
        return;
    if (((R->cadre.l2 - R->cadre.l1) < 2)
        || ((R->cadre.c2 - R->cadre.c1) < 2))
        return;

    R1 = (region *) malloc(sizeof(region));
    if (R1 == NULL)
    {
        printf("Error Malloc R1 \n");
        exit(1);
    }
    R2 = (region *) malloc(sizeof(region));
    if (R2 == NULL)
    {
        printf("Error Malloc R2 \n");
        exit(1);
    }
    R3 = (region *) malloc(sizeof(region));
    if (R3 == NULL)
    {
        printf("Error Malloc R3 \n");
        exit(1);
    }
    R4 = (region *) malloc(sizeof(region));
    if (R4 == NULL)
    {
        printf("Error Malloc R4 \n");
        exit(1);
    }

    iter++;


    R1->cadre.l1 = R->cadre.l1;
    R1->cadre.l2 = (R->cadre.l2 - R->cadre.l1) / 2 + R->cadre.l1;
    R1->cadre.c1 = R->cadre.c1;
    R1->cadre.c2 = (R->cadre.c2 - R->cadre.c1) / 2 + R->cadre.c1;
    R1->surface = 0;
    R1->next_region = NULL;
    R1->prev_region = NULL;

    R2->cadre.l1 = R->cadre.l1;
    R2->cadre.l2 = R1->cadre.l2;
    R2->cadre.c1 = R1->cadre.c2 + 1;
    R2->cadre.c2 = R->cadre.c2;
    R2->surface = 0;
    R2->next_region = NULL;
    R2->prev_region = NULL;

    R3->cadre.l1 = R1->cadre.l2 + 1;
    R3->cadre.l2 = R->cadre.l2;
    R3->cadre.c1 = R->cadre.c1;
    R3->cadre.c2 = R1->cadre.c2;
    R3->surface = 0;
    R3->next_region = NULL;
    R3->prev_region = NULL;

    R4->cadre.l1 = R1->cadre.l2 + 1;
    R4->cadre.l2 = R->cadre.l2;
    R4->cadre.c1 = R1->cadre.c2 + 1;
    R4->cadre.c2 = R->cadre.c2;
    R4->surface = 0;
    R4->next_region = NULL;
    R4->prev_region = NULL;

/*  free(R) ;R=NULL;    NON on desalloue la region d'appel!!! */


    bonne = test_region(R1, im_init, teinte, range, ratio);
    switch (bonne)
    {
    case 1:
        push(bonnes_regions, R1);
        break;
    case -1:
        free(R1);
        R1 = NULL;
        break;
    case 0:
        split_image(bonnes_regions, R1, im_init, iter, teinte, range, ratio,
                    nb_iteration);
        free(R1);
        break;
    }

    bonne = test_region(R2, im_init, teinte, range, ratio);
    switch (bonne)
    {
    case 1:
        push(bonnes_regions, R2);
        break;
    case -1:
        free(R2);
        R2 = NULL;
        break;
    case 0:
        split_image(bonnes_regions, R2, im_init, iter, teinte, range, ratio,
                    nb_iteration);
        free(R2);
        break;
    }

    bonne = test_region(R3, im_init, teinte, range, ratio);
    switch (bonne)
    {
    case 1:
        push(bonnes_regions, R3);
        break;
    case -1:
        free(R3);
        R3 = NULL;
        break;
    case 0:
        split_image(bonnes_regions, R3, im_init, iter, teinte, range, ratio,
                    nb_iteration);
        free(R3);
        break;
    }

    bonne = test_region(R4, im_init, teinte, range, ratio);
    switch (bonne)
    {
    case 1:
        push(bonnes_regions, R4);
        break;
    case -1:
        free(R4);
        R4 = NULL;
        break;
    case 0:
        split_image(bonnes_regions, R4, im_init, iter, teinte, range, ratio,
                    nb_iteration);
        free(R4);
        break;
    }

}
