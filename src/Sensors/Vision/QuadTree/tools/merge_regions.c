/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file  
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
     Merge les regions et rempli le cadre   

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-pop()
-push()
-proche()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/liste.h>
#include <Struct/region.h>
#include <stdlib.h>

#include "include/push.h"

int pop(liste * bonne_regions, region * R)
{
    region *prev, *next;

    /* printf("fonction pop \n"); */

    if (bonne_regions == NULL)
    {
        printf("ERROR bonne_regions=NULL \n");
        exit(1);
    }
    if (bonne_regions->nb_element <= 0)
    {
        printf("ERROR pop sur pile vide... \n");
        exit(1);
    }

    if (R == NULL)
    {
        printf("ERROR R=NULL \n");
        exit(1);
    }

    prev = R->prev_region;
    if (prev != NULL)
        prev->next_region = R->next_region;
    else
        bonne_regions->premiere = R->next_region;

    next = R->next_region;
    if (next != NULL)
        next->prev_region = R->prev_region;
    else
        bonne_regions->derniere = R->prev_region;

    R->prev_region = NULL;
    R->next_region = NULL;      /* .... pb ... */

    bonne_regions->nb_element = bonne_regions->nb_element - 1;

    return 1;
}

int proche(region * R1, region * R2, int proximite)
{
    int i, j;
    s_cadre r1, r2;

    /*printf("proche \n"); */
    if (R1 == NULL)
    {
        printf("ERROR R1=NULL \n");
        exit(1);
    }
    if (R2 == NULL)
    {
        printf("ERROR R2=NULL \n");
        exit(1);
    }

    /*printf("---- fonction proche ----\n") ; */
    if ((r1.l1 = R1->cadre.l1 - proximite) < 0)
        r1.l1 = 0;
    if ((r1.c1 = R1->cadre.c1 - proximite) < 0)
        r1.c1 = 0;


/* possibilite d'une erreur de segmentation (sortir des bords de l'image) issam bouaita le 29/07/2003 */
    r1.l2 = R1->cadre.l2 + proximite;
    r1.c2 = R1->cadre.c2 + proximite;
    r2.l1 = R2->cadre.l1;
    r2.l2 = R2->cadre.l2;
    r2.c1 = R2->cadre.c1;
    r2.c2 = R2->cadre.c2;

    for (i = r2.l1; i < r2.l2; i++) /* peut etre tres optimise !!! PG */
        for (j = r2.c1; j < r2.c2; j++)
        {
            if ((i > r1.l1) && (i < r1.l2) && (j > r1.c1) && (j < r1.c2))
                return 1;
        }
    return 0;
}

int merge_regions(liste * region_merge, liste * bonnes_regions, int proximite)
{
    region *merge = NULL, *current = NULL, *pt_current, *pt_current2 = NULL;
#ifndef AVEUGLE
    TxPoint point;
#endif

    /* printf("---- fonction merge_region ----\n") ; */
    if (bonnes_regions == NULL)
    {
        printf("ERROR bonnes_region=NULL \n");
        exit(1);
    }
    if (region_merge == NULL)
    {
        printf("ERROR region_merge=NULL \n");
        exit(1);
    }

    /* printf("bonnes_regions->nb_element=%d \n",bonnes_regions->nb_element); */


    while (bonnes_regions->nb_element > 0)
    {
        current = bonnes_regions->premiere;
        if (current == NULL)
        {
            printf("current should not be NULL ... \n");
            exit(1);
        }
        pop(bonnes_regions, current);

#ifndef AVEUGLE
        printf("cadre courant pour la fusion\n");
        point.x = current->cadre.l1;
        point.y = current->cadre.c1;
        /*  getchar(); */
        TxDessinerRectangle(&image1, bleu, TxVide, point,
                            current->cadre.l2 - current->cadre.l1,
                            current->cadre.c2 - current->cadre.c1, 2);
        TxFlush(&image1);
#endif


        merge = (region *) malloc(sizeof(region));
        if (merge == NULL)
        {
            printf("Pb Malloc merge\n");
            exit(1);
        }

        merge->cadre.l1 = current->cadre.l1;
        merge->cadre.l2 = current->cadre.l2;
        merge->cadre.c1 = current->cadre.c1;
        merge->cadre.c2 = current->cadre.c2;
        merge->surface = current->surface;
        free(current);
        current = NULL;
        merge->next_region = NULL;
        merge->prev_region = NULL;

        pt_current = bonnes_regions->premiere;
        while (pt_current != NULL)
        {
            if (proche(merge, pt_current, proximite) == 1)
            {

#ifndef AVEUGLE                 /*rajouter par issam le 17/06/2003 */
                printf("region a fusionner \n");
                /* getchar(); */
                point.x = pt_current->cadre.l1;
                point.y = pt_current->cadre.c1;
                TxDessinerRectangle(&image1, gris, TxVide, point,
                                    pt_current->cadre.l2 -
                                    pt_current->cadre.l1,
                                    pt_current->cadre.c2 -
                                    pt_current->cadre.c1, 2);
                TxFlush(&image1);
#endif

                /* agrandissement de la region merge */
                if (merge->cadre.l1 > pt_current->cadre.l1)
                    merge->cadre.l1 = pt_current->cadre.l1;
                if (merge->cadre.l2 < pt_current->cadre.l2)
                    merge->cadre.l2 = pt_current->cadre.l2;
                if (merge->cadre.c1 > pt_current->cadre.c1)
                    merge->cadre.c1 = pt_current->cadre.c1;
                if (merge->cadre.c2 < pt_current->cadre.c2)
                    merge->cadre.c2 = pt_current->cadre.c2;
                /* merge->surface = pt_current->surface ; mis en commentaire par issam bouaita le 30/07/20032 */

                /* merge->surface =merge->surface+ pt_current->surface ;rajouter par issam bouaita le 30/07/20032 */
                merge->surface =
                    (merge->cadre.l2 - merge->cadre.l1) * (merge->cadre.c2 -
                                                           merge->cadre.c1);
                /* point.x= merge->cadre.l1;point.y=merge->cadre.c1 ; */
                /*      TxDessinerRectangle(&image1,rouge,TxVide,point,merge->cadre.l2 - merge->cadre.l1,merge->cadre.c2 - merge->cadre.c1,2); 
                   TxFlush(&image1); */
                pop(bonnes_regions, pt_current);
                pt_current2 = pt_current;
                pt_current = pt_current->next_region;
                free(pt_current2);
                pt_current2 = NULL;
            }
            else
                pt_current = pt_current->next_region;
        }
        push(region_merge, merge);
    }


    /* printf("Nbre de regions apres fusion: %d \n", region_merge->nb_element) ; */

    if (region_merge->nb_element == 0)
        return 0;
    else
        return 1;
}
