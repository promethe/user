/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_soustraction_images.c
\brief soustraction os two images

Author: xxxxxxxx
Created: xxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 26/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Soustraction de deux images
 image_fond : image de fond (sans personne) - format .png
 image_pers : image de la meme scene avec une personne - format .png
 image_res  : image resultat = image binaire de la difference
                   de image_fond et de image_pers.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/calloc_prom_image()
-Kernel_Function/prom_getopt()

Links:
- type: none
- description: none
- input expected group:
- where are the data?:

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

void function_soustraction_images(int gpe_sortie)
{
	long k, l;
	int i, nb, F, seuil = 0;
	int largeur = 0, hauteur = 0;
	int gpe_entree1 = -1, gpe_entree2 = -1, link1 = -1, link2 = -1;
	prom_images_struct *image_fond = NULL, *image_pers = NULL;
	prom_images_struct *image_res = NULL;
	char c;
	float x;
	char resultat[256];
	float *im1 = NULL, *im2 = NULL;
	float max;
	/*  printf("\n-------------------------------------\n") ;
       printf("---FONCTION SOUSTRACTION D'IMAGES ---\n") ; */

	/* Recherche des deux gpes d'entreee  */
	F = 0;                      /* Flag pour premier groupe trouve */
	for (i = 0; i < nbre_liaison; i++)
	{
		if (liaison[i].arrivee == gpe_sortie)
		{
			if (F == 0)
			{
				gpe_entree1 = liaison[i].depart;
				if (liaison[i].nom[0] == 's')
					sscanf(liaison[i].nom, "%c%d", &c, &seuil);
				link1 = i;
				F = 1;
			}
			else
			{
				gpe_entree2 = liaison[i].depart;
				if (liaison[i].nom[0] == 's')
					sscanf(liaison[i].nom, "%c%d", &c, &seuil);
				link2 = i;
				F = 2;
			}

		}
	}

	/*Pour verifier l'existence de deux liens */
	if (link1 == -1)
	{
		printf
		("Ce groupe doit avoir deux groupes en entree... Il n'y en a meme pas UN!\n");
		exit(EXIT_FAILURE);
	}
	if (link2 == -1)
	{
		printf("Ce groupe doit avoir 2 groupes en entree.");
		exit(EXIT_FAILURE);
	}

	/* Pour tester si le .ext des gpes d'entree ont une 1 band prom_image */
	image_fond = (prom_images_struct *) def_groupe[gpe_entree1].ext;
	image_pers = (prom_images_struct *) def_groupe[gpe_entree2].ext;

	/*modification de Braik B. le 22/07/2004*/

	if (prom_getopt(liaison[link1].nom, "-s", resultat) == 2)
	{
		seuil = atoi(resultat);
	}
	if (prom_getopt(liaison[link2].nom, "-s", resultat) == 2)
	{
		seuil = atoi(resultat);
	}
	/*fin modif*/

	/* printf("Difference entre 2 images, Seuil = %d \n",seuil);
       scanf("%c",&c); */

	/* Test pour voir si les groupes sont vides */
	if (image_fond == NULL)
		EXIT_ON_ERROR("Probleme (function_soustraction_images) : il n'y pas d'image dans le groupe %i \n",gpe_entree1);

	if (image_pers == NULL)
		EXIT_ON_ERROR("Probleme (function_soustraction_images) : il n'y pas d'image dans le groupe %i\n", gpe_entree2);

	/* Pour verifier que les deux images ont bien la meme taille */
	if ((image_fond->sx != image_pers->sx) || (image_fond->sy != image_pers->sy))
		EXIT_ON_ERROR("ATTENTION!\nLes deux images en entree doivent avoir la meme taille.\nImpossible de les soustraire l'une a l'autre.\n");
	else
	{
		largeur = image_fond->sx;
		hauteur = image_fond->sy;
	}

	/* Allocation memoire pour l'image resultat */
	if (def_groupe[gpe_sortie].ext == NULL)
	{
		image_res = calloc_prom_image(1, largeur, hauteur, 1);
		def_groupe[gpe_sortie].ext = image_res;
	}
	else
	{
		image_res = def_groupe[gpe_sortie].ext;
	}
	/*
       printf("Entrer un seuil : "); fflush(stdout) ;
       scanf("%d", &seuil); fflush(stdin) ; */


	nb = image_fond->nb_band;
	if(image_fond->nb_band != image_pers->nb_band) EXIT_ON_ERROR("Les images doivent etre de meme type %s",def_groupe[gpe_sortie].no_name);
	if(nb == 4)
	{
		im1 = (float*) image_fond->images_table[0];
		im2 = (float*) image_pers->images_table[0];
		max = -1;
		for (k = hauteur * largeur; k-- ; )
		{

			//printf ("%f - %f \n",im1[ k],im2[k]);
			x = fabs(im1[k ] - im2[ k ]);
			if(x>max ) max = x;
			if (x  > (float) seuil){
				image_res->images_table[0][k] = (unsigned char) 255;
				//printf(" %f - %f : %f\n",im1[k ], im2[ k ],x);

			}
			else
				image_res->images_table[0][k] = (unsigned char) 0;

		}
		//for (k = hauteur * largeur; k-- ; ) image_res->images_table[0][k] = (image_res->images_table[0][k] / max);
	}
	else
	{


		for (k = 0; k < hauteur * largeur; k++)
		{
			x = 0.;
			for (l = 0; l < nb; l++)
			{
				dprints ("%d - %d + ",image_fond->images_table[0][nb * k + l],image_pers->images_table[0][nb * k + l]);
				x = x + fabs(image_fond->images_table[0][nb * k + l] - image_pers->images_table[0][nb * k + l]);
			}
			dprints ("\n");
			if (x / (float) nb > (float) seuil){
				image_res->images_table[0][k] = (unsigned char) 255;
				dprints("diff : %f\n",x / (float) nb);
			}
			else
				image_res->images_table[0][k] = (unsigned char) 0;
		}
	}
	/*free(image_fond->images_table[0]) ;
       free(image_pers->images_table[0]) ;

       printf("sauvegarde du resultat dans image_soustraction.png.\n") ;
       save_png_to_disk("image_soustraction.png", *image_res, compression) ; */
}


/* soustraction stricte champs par champs avec seuil */
/* le lien avec le seuil est sur l'image soustraite (celle avec le signe - )*/
void function_diff_images(int gpe_sortie)
{
	long k, l;
	int i, nb, F, seuil = 0,res;
	int largeur = 0, hauteur = 0;
	int gpe_entree1 = -1, gpe_entree2 = -1, link1 = -1, link2 = -1;
	prom_images_struct *image_fond = NULL, *image_pers = NULL;
	prom_images_struct *image_res = NULL;
	char c;
	char resultat[256];
	unsigned char *carac,*max_locaux;
	int p1,p2,nbr;
	int cmult;
	/*  printf("\n-------------------------------------\n") ;
       printf("---FONCTION SOUSTRACTION D'IMAGES ---\n") ; */

	/* Recherche des deux gpes d'entreee  */
	F = 0;                      /* Flag pour premier groupe trouve */
	for (i = 0; i < nbre_liaison; i++)
	{
		if (liaison[i].arrivee == gpe_sortie)
		{
			if (F == 0)
			{
				gpe_entree1 = liaison[i].depart;
				if (liaison[i].nom[0] == 's')
					sscanf(liaison[i].nom, "%c%d", &c, &seuil);
				link1 = i;
				F = 1;
			}
			else
			{
				gpe_entree2 = liaison[i].depart;
				if (liaison[i].nom[0] == 's')
					sscanf(liaison[i].nom, "%c%d", &c, &seuil);
				link2 = i;
				F = 2;
			}

		}
	}



	/*Pour verifier l'existence de deux liens */
	if (link1 == -1)
	{
		printf
		("Ce groupe doit avoir deux groupes en entree... Il n'y en a meme pas UN!\n");
		exit(EXIT_FAILURE);
	}
	if (link2 == -1)
	{
		printf("Ce groupe doit avoir 2 groupes en entree.");
		exit(EXIT_FAILURE);
	}

	/* Pour tester si le .ext des gpes d'entree ont une 1 band prom_image */
	image_fond = (prom_images_struct *) def_groupe[gpe_entree1].ext;
	image_pers = (prom_images_struct *) def_groupe[gpe_entree2].ext;

	/*modification de Braik B. le 22/07/2004*/

	if (prom_getopt(liaison[link1].nom, "-s", resultat) == 2)
	{
		seuil = atoi(resultat);
	}
	if (prom_getopt(liaison[link2].nom, "-s", resultat) == 2)
	{
		seuil = atoi(resultat);
	}
	/*fin modif*/

	/* printf("Difference entre 2 images, Seuil = %d \n",seuil);
       scanf("%c",&c); */

	/* Test pour voir si les groupes sont vides */
	if (image_fond == NULL)
	{
		printf
		("Probleme (function_soustraction_images) : il n'y pas d'image dans le groupe %i \n",
				gpe_entree1);
		exit(EXIT_FAILURE);
	}

	if (image_pers == NULL)
	{
		printf
		("Probleme (function_soustraction_images) : il n'y pas d'image dans le groupe %i\n",
				gpe_entree2);
		exit(EXIT_FAILURE);
	}

	/* Pour verifier que les deux images ont bien la meme taille */
	if ((image_fond->sx != image_pers->sx)
			|| (image_fond->sy != image_pers->sy))
	{
		printf
		("ATTENTION!\nLes deux images en entree doivent avoir la meme taille.\nImpossible de les soustraire l'une a l'autre.\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		largeur = image_fond->sx;
		hauteur = image_fond->sy;
	}

	/* Allocation memoire pour l'image resultat */
	if (def_groupe[gpe_sortie].ext == NULL)
	{
		image_res = calloc_prom_image(2, largeur, hauteur, 1);
		def_groupe[gpe_sortie].ext = image_res;
	}
	else
	{
		image_res = def_groupe[gpe_sortie].ext;
	}
	/*
       printf("Entrer un seuil : "); fflush(stdout) ;
       scanf("%d", &seuil); fflush(stdin) ; */

	carac=image_res->images_table[1]; /* on suppose une seule bande */
	max_locaux=image_res->images_table[0];


	nb = image_fond->nb_band;
	nbr= (hauteur-10) * largeur;
	cmult=7;
	for (k = 0; k <nbr; k++)
	{
		p1=nb * k;
		for (l = 0; l < nb; l++)
		{
			p2=p1+l;
			res=(int)(1.2*image_pers->images_table[0][p2] - image_fond->images_table[0][p2]);
			/*if(cmult*res>255) printf("pb saturation ... %d\n",cmult*res);*/
			if(res<seuil) res=0;
			else
			{
				res= (cmult*res);
				if(res>255) res=255;
			}

			max_locaux[p2]      =carac[p2]=res;
		}
	}




	/* la boucle ne commence pas a 0 et ne finie pas a nbr pour ne pas sortir du tableau */
	/* la = taille_msq */
	/*  la=10;

    for (p = (la - 1) * (1 + largeur); p < (nbr - ((1 + largeur) * (la - 1))); p++)
    {
        v = carac[p];
       	if(v>0)
	  {
	    for (j = 1 - la; j < la; j++)
	      for (i = 1 - la; i < la; i++)
		{
		  q = p + i + largeur * j;
		  if ((i != 0 || j != 0) &&  carac[q] > v)
		    {
		      max_locaux[p] = 0;
		      goto finie2;
		    }
		}
	  }
	 *max_locaux[p]=(unsigned char)v;*
    finie2:;
    }
	 */

	/*free(image_fond->images_table[0]) ;
       free(image_pers->images_table[0]) ;

       printf("sauvegarde du resultat dans image_soustraction.png.\n") ;
       save_png_to_disk("image_soustraction.png", *image_res, compression) ; */
}
