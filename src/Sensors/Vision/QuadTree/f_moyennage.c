/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_moyennage.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Cette fonction permet de moyenner une image par une gaussienne

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>

#include <Struct/prom_images_struct.h>

#include <Kernel_Function/find_input_link.h>

void function_moyennage(int Gpe)
{

    prom_images_struct *image_moyenne, *im;
    int gpe_entree = -1, link = -1;
    int nx, ny, i, j, nb, k, numero;
    float **gaussienne;



    /*Finding the link */
    link = find_input_link(Gpe, 0);
    gpe_entree = liaison[link].depart;


    if (link == -1)
    {
        printf("*****> Il n'y a pas de groupe en entree\n");
        exit(EXIT_FAILURE);
    }

    if ((def_groupe[gpe_entree].ext == NULL))
    {
        printf("Gpe %d en entree avec ext nulle; pas d'image link %d\n",
               gpe_entree, link);
        return;
    }



/***********************recuperation des infos sur le groupe precedent****************/

    im = (prom_images_struct *) def_groupe[gpe_entree].ext;
    nx = ((prom_images_struct *) def_groupe[gpe_entree].ext)->sx;
    ny = ((prom_images_struct *) def_groupe[gpe_entree].ext)->sy;
    nb = ((prom_images_struct *) def_groupe[gpe_entree].ext)->nb_band;

/*************************************************************************************/



    if (def_groupe[Gpe].ext == NULL)
    {

        image_moyenne = (void *) malloc(sizeof(prom_images_struct));
        if (image_moyenne == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE ...! \n");
            exit(-1);
        }

        image_moyenne->nb_band = nb;
        image_moyenne->image_number = 1;
        image_moyenne->sx = nx;
        image_moyenne->sy = ny;

        /* allocation memoire pour l'image moyennee */

        image_moyenne->images_table[0] =
            (unsigned char *) malloc(nb * nx * ny * sizeof(unsigned char));
        if (image_moyenne->images_table[0] == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE...! \n");
            exit(-1);
        }

        /* allocation memoire pour la gaussienne */
        gaussienne = (float **) malloc(3 * sizeof(float *));
        if (gaussienne == NULL)
        {
            printf("ALLOCATION IMPOSSIBLE...! \n");
            exit(-1);
        }

        for (i = 0; i < 3; i++)
        {
            gaussienne[i] = (float *) malloc(3 * sizeof(float));
            if (gaussienne[i] == NULL)
            {
                printf("ALLOCATION IMPOSSIBLE...! \n");
                exit(-1);
            }
        }


        /*valeurs de la matrice pour la gaussienne */

        gaussienne[0][0] = 0.3;
        gaussienne[0][1] = 0.6;
        gaussienne[0][2] = 0.3;
        gaussienne[1][0] = 0.6;
        gaussienne[1][1] = 1.0;
        gaussienne[1][2] = 0.6;
        gaussienne[2][0] = 0.3;
        gaussienne[2][1] = 0.6;
        gaussienne[2][2] = 0.3;



        def_groupe[Gpe].ext = image_moyenne;
        image_moyenne->images_table[1] = (unsigned char *) gaussienne;
    }
    else
    {
        image_moyenne = ((prom_images_struct *) def_groupe[Gpe].ext);
        gaussienne = (float **) image_moyenne->images_table[1];
    }



    for (i = nb - 1; i < ny - nb + 1; i++)
        for (j = nb - 1; j < nx - nb + 1; j++)
            for (k = 0; k < nb; k++)
            {

                numero = i * nx * nb + j * nb + k;

                image_moyenne->images_table[0][numero] =
                    (unsigned
                     char) ((int) ((gaussienne[0][0] *
                                    (float) im->images_table[0][numero -
                                                                nb * nx -
                                                                nb] +
                                    gaussienne[0][1] *
                                    (float) im->images_table[0][numero -
                                                                nb * nx] +
                                    gaussienne[0][2] *
                                    (float) im->images_table[0][numero -
                                                                nb * nx +
                                                                nb] +
                                    gaussienne[1][0] *
                                    (float) im->images_table[0][numero - nb] +
                                    gaussienne[1][1] *
                                    (float) im->images_table[0][numero] +
                                    gaussienne[1][2] *
                                    (float) im->images_table[0][numero + nb] +
                                    gaussienne[2][0] *
                                    (float) im->images_table[0][numero +
                                                                nb * nx -
                                                                nb] +
                                    gaussienne[2][1] *
                                    (float) im->images_table[0][numero +
                                                                nb * nx] +
                                    gaussienne[2][2] *
                                    (float) im->images_table[0][numero +
                                                                nb * nx +
                                                                nb]) / 4.6));

            }


    /*free(image_moyenne); */
    /*free(im); */
}
