/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_fusion_rectangle.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Les deux fonctions suivantes ("function_fusion_rectangles" et "function_images_des_teintes") sont equivalentes
   a la fonction "function_visage". On a separe cette fonction
   en deux parties pour pouvoir exploiter differemment l'image des teintes. De cette maniere, on peut la faire preceder
   de "function_fusion_rectangle" et dans ce cas aboutir a la meme fonctionalite que "f_visage" ou la soumettre a d'autres traitements.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-reechant()
-recadrer()
-load_param_face()
-detection_visage()

External Tools:
-tools/Vision/calloc_prom_image()
-tools/Vision/free_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Struct/prom_images_struct.h>
#include <Struct/s_cadre.h>
#include <Struct/parametres.h>

#include <public_tools/Vision.h>

#include "tools/include/reechant.h"
#include "tools/include/recadrer.h"
#include "tools/include/load_param_face.h"
#include "tools/include/detection_visage.h"

void function_fusion_rectangles(int Gpe)
{
    int i, link = -1, gpe_entree = -1, t, image_NG = 0;

    static parametres para = { 50, 6, 0.4, 8, 1, 10, 32, -1, 0 };   /*{50,6,0.4,8,1,10,32,1,0} ; */
    s_cadre cadre;
    prom_images_struct *sans_fond_NG = NULL, *sans_fond_teinte = NULL;
    prom_images_struct *im_visage_recadre = NULL, *visage_reechant = NULL;

#ifndef AVEUGLE
    TxPoint point;
#endif

    printf("begin fusion rectangle\n");
    /* Recherche du gpe d'entree */

    /*  CORRECTION: Utiliser find_input_link() et prom_getopt()  */

    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == Gpe)
        {
            if (strcmp(liaison[i].nom, "fusion") == 0)
            {
                gpe_entree = liaison[i].depart;
                link = i;
                break;
            }

        }
    }

    /* Pour verifier l'existence du lien */
    if (link == -1)
    {
        printf
            ("Erreur : ce groupe n'a pas d'entree ou n'a pas de lien portant le nom image!\n");
        exit(EXIT_FAILURE);
    }


/*recuperation de l'image des teintes*/

    sans_fond_teinte = (prom_images_struct *) def_groupe[gpe_entree].ext;

    if (sans_fond_teinte == NULL)
    {
        printf
            ("Probleme function_visage : il n'y a pas d'image dans le gpe d'entree %i\n",
             gpe_entree);
        exit(EXIT_FAILURE);
    }

    if (sans_fond_teinte->images_table[1] != NULL)
    {
        image_NG = 1;
        sans_fond_NG =
            calloc_prom_image(1, sans_fond_teinte->sx, sans_fond_teinte->sy,
                              1);
        sans_fond_NG->images_table[0] = sans_fond_teinte->images_table[1];
    }

    /* Parametres par defaut */

    if (para.oeil == -1)
        load_param_face(&para);
    t = detection_visage(sans_fond_teinte, &cadre, &para);

    if (t == 0)
    {
        printf("pas de visage detecte...\n");
        cadre.l1 = 0;
        cadre.l2 = sans_fond_teinte->sy;
        cadre.c1 = 0;
        cadre.c2 = sans_fond_teinte->sx;
    }
    else                        /* entoure en rouge le visage trouve */
    {

#ifndef AVEUGLE
        point.x = cadre.l1;
        point.y = cadre.c1;
        TxDessinerRectangle(&image1, rouge, TxVide, point,
                            cadre.l2 - cadre.l1, cadre.c2 - cadre.c1, 2);
        TxFlush(&image1);
#endif

    }

    /* Recadrage du visage */
    /*  printf("\n--------------------\n") ;
       printf("recadrage du visage \n") ; */

    im_visage_recadre =
        calloc_prom_image(1, sans_fond_teinte->sx, sans_fond_teinte->sy, 1);

    if (image_NG == 1)
    {
        t = recadrer(sans_fond_NG, im_visage_recadre, &cadre);
    }

    else
    {

        t = recadrer(sans_fond_teinte, im_visage_recadre, &cadre);
    }

    for (i = 0; i < im_visage_recadre->sx * im_visage_recadre->sy; i++)
    {
        if (im_visage_recadre->images_table[0][i] == (unsigned char) 0)

            im_visage_recadre->images_table[0][i] =
                (unsigned char) ((im_visage_recadre->images_table[0][i - 1] +
                                  im_visage_recadre->images_table[0][i -
                                                                     im_visage_recadre->
                                                                     sx]) /
                                 2);
    }

    /*   printf("sauvegarde du visage recadre dans : visage_recadre.png \n") ;
       save_png_to_disk("visage_recadre.png", *im_visage_recadre, compression) ; */


    /* Reechantillonnage */
    if (def_groupe[Gpe].ext == NULL)
    {
        visage_reechant = calloc_prom_image(1, 60, 80, 1);
        def_groupe[Gpe].ext = visage_reechant;
    }
    else
        visage_reechant = def_groupe[Gpe].ext;

    t = reechant(im_visage_recadre, visage_reechant);

    /*    printf("sauvegarde du visage recadre & reechantillonne ds : visage_reech.png \n") ;
       save_png_to_disk("visage_reech.png", *visage_reechant, compression) ;   */

    free_prom_image(sans_fond_NG);
    /*free_prom_image(sans_fond_teinte) ; */
    free_prom_image(im_visage_recadre);

    printf("end fusion rectangle\n");
}
