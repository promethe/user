/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_image_des_teintes.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-PI

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/find_input_link()

Links:
- type: algo / blueiological / neural
- description: none/ XXX
- input expected greenroup: none/xxx
- where are the data?: none/xxx

Comments:

Known blueugs: none (yet!)

Todo:see authored fortestingreen and commentingreen the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <Struct/prom_images_struct.h>

#include "tools/include/macro.h"
#include "public_tools/Vision.h"

#include <Kernel_Function/find_input_link.h>

typedef struct data_image_des_teintes
{
   int gpe_image;
} Data_image_des_teintes;

void function_image_des_teintes(int index_of_gpe)
{
   Data_image_des_teintes *data;
   int i=0, cas;
   int l = -1, gpe_image = -1;
   float red=0.0, green=0.0, blue=0.0, teinte=0, saturation, min=0, max=0;

   prom_images_struct *image_input_struct = NULL, *image_output_struct = NULL;
   unsigned char *image_input, *image_output;
   float *t_image, *s_image, *v_image;

#ifdef DEBUG
   printf("begin f_image_des_teintes\n");
#endif

   if (def_groupe[index_of_gpe].ext == NULL)
   {
      if (def_groupe[index_of_gpe].data == NULL)
      {
         /*Finding the link */
         i=0;
         l = find_input_link(index_of_gpe, i);
         while (l != -1)
         {
            if (strstr(liaison[l].nom, "sync") != NULL) continue;
            gpe_image = liaison[l].depart;
            i++;
            l = find_input_link(index_of_gpe, i);
         }
         /* Pour verifier l'existence du lien */
         if (gpe_image == -1) EXIT_ON_ERROR("le groupe n'a pas d'entree !");

         /*allocation memoire pour la sauvegarde des donnees sur le lien */
         data = (Data_image_des_teintes *)ALLOCATION(Data_image_des_teintes);

         /* parametres par default */
         data->gpe_image = gpe_image;
         def_groupe[index_of_gpe].data = data;
      }
      data = (Data_image_des_teintes *)def_groupe[index_of_gpe].data;
      /*image couleur du groupe precedent */
      image_input_struct = (prom_images_struct *) def_groupe[data->gpe_image].ext;

      /* Poured verifiered l'existence du lien */
      if (image_input_struct == NULL)
      {
         PRINT_WARNING("il n'y a pas d'image dans le gpe d'entree");
         return ;
      }
      if (image_input_struct->nb_band != 3)   EXIT_ON_ERROR("L'image en entree doit etre en couleur");

      /* allocation de memoire */
      image_output_struct = (void *)calloc_prom_image(1, image_input_struct->sx, image_input_struct->sy, 1);
      if (image_output_struct == NULL) EXIT_ON_ERROR("impossible d'allouer la memoire");
      image_output_struct->image_number = 4;
      image_output_struct->images_table[1] = (unsigned char *)calloc(image_output_struct->sx * image_output_struct->sy, sizeof(float));
      if (image_output_struct->images_table[1] == NULL)  EXIT_ON_ERROR("impossible d'allouer la memoire");
      image_output_struct->images_table[2] = (unsigned char *)calloc(image_output_struct->sx * image_output_struct->sy, sizeof(float));
      if (image_output_struct->images_table[2] == NULL)  EXIT_ON_ERROR("impossible d'allouer la memoire");
      image_output_struct->images_table[3] = (unsigned char *)calloc(image_output_struct->sx * image_output_struct->sy, sizeof(float));
      if (image_output_struct->images_table[3] == NULL)  EXIT_ON_ERROR("impossible d'allouer la memoire");
      def_groupe[index_of_gpe].ext = image_output_struct;
   }
   data = (Data_image_des_teintes *)def_groupe[index_of_gpe].data;
   image_input_struct = (prom_images_struct *) def_groupe[data->gpe_image].ext;
   image_output_struct = (prom_images_struct *) def_groupe[index_of_gpe].ext;
   image_input = image_input_struct->images_table[0];
   image_output = image_output_struct->images_table[0];

   t_image = (float *)(image_output_struct->images_table[1]);
   s_image = (float *)(image_output_struct->images_table[2]);
   v_image = (float *)(image_output_struct->images_table[3]);

   for (i = 0; i < image_output_struct->sx * image_output_struct->sy; i++)
   {
      red   =   (float) (image_input[3 * i]);
      green =   (float) (image_input[3 * i + 1]);
      blue  =   (float) (image_input[3 * i + 2]);

      if (red >= green && red >= blue)
      {
         cas = 1;
         max = red;
         if (blue >= green) min = green;
         else min = blue;
      }
      else if (green >= red && green >= blue)
      {
         cas = 2;
         max = green;
         if (red >= blue) min = blue;
         else min = red;
      }
      else if (blue >= red && blue >= green)
      {
         cas = 3;
         max = blue;
         if (green >= red ) min = red;
         else min = green;
      }
      else cas = -1;
      if (!(red > green || red < green || red < blue || red > blue)) cas = 0;

      switch (cas)
      {
         case 0:
            teinte = 0;
            break;
         case 1:
            teinte = (60 * (green-blue)/(max-min) + 360);
            if (teinte > 359.99999 || teinte < 0.0)
               teinte -= floorf(teinte/360.)*360.;
            break;
         case 2:
            teinte = (60 * (blue-red)/(max-min) + 120);
            break;
         case 3:
            teinte = (60 * (red-green)/(max-min) + 240);
            break;
         default:
            PRINT_WARNING("error, not a case I know (%d %d %d)", red , green, blue);
      }

      if (max < 0.000001)  saturation = 0.0;
      else                 saturation = 1.0 - min / max;

      image_output[i] = (unsigned char)(int)(teinte *255 / 360);
      t_image[i] = teinte;
      s_image[i] = saturation;
      v_image[i] = max;
   }

   dprints("pointeured qui deconne dans %s:%p\n", __FUNCTION__, ((prom_images_struct *) def_groupe[index_of_gpe].ext)->images_table[0]);
   dprints("end f_image_des_teintes\n");
}
