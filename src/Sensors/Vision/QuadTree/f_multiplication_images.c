/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_multiplication_images.c
\brief realise multiplication of two images

Author: xxxxx
Created: xxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 26/07/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
       image_pers : image de la scene de fond avec la personne.
       image_res_soustraction : image resultat de la soustraction d'images
                               (image binaire).
       image_res  : image resultat de la multiplication de image_pers*image_res_soustraction
                   pour n'obtenir que la personne.
       hauteur    : hauteur des images
       largeur    : largeur des images

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/calloc_prom_image()
-Kernel_Function/prom_getopt()

Links:
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

Comments:

Known bugs: none (yet!)

Todo:	see the author to comment the file.


http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/prom_images_struct.h>

#include <stdlib.h>

#include <Kernel_Function/prom_getopt.h>
#include <public_tools/Vision.h>

void function_multiplication_images(int gpe_sortie)
{
    int F /*flag */ , i, nb;
    int largeur = 0, hauteur = 0;
    int gpe_entree1 = -1, gpe_entree2 = -1, link1 = -1, link2 = -1;
    long k, l;
    prom_images_struct *image_pers = NULL, *image_res_soustraction = NULL;
    prom_images_struct *image_res = NULL;
    char resultat[256];
    /*  printf("\n----------------------------------------\n") ;
       printf("--- FONCTION MULTIPLICATION D'IMAGES ---\n") ; */

    /* Recherche des deux gpes d'entreee  */
    F = 0;
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == gpe_sortie)
        {
            if (F == 0)
            {
                gpe_entree1 = liaison[i].depart;
                link1 = i;
                F = 1;
            }
            else
            {
                gpe_entree2 = liaison[i].depart;
                link2 = i;
                break;
            }
        }
    }

    /*Pour verifier l'existence de deux liens */
    if (link1 == -1)
    {
        printf
            ("Ce groupe doit avoir deux groupes en entree... Il n'en a meme pas UN!\n");
        exit(EXIT_FAILURE);
    }
    if (link2 == -1)
    {
        printf("Ce groupe doit avoir deux groupes en entree.");
        exit(EXIT_FAILURE);
    }
/*  printf("%d %d \n",link1,link2); */

/*   if(strcmp(liaison[link1].nom,"-Ncouleurs") != 0 ) */
/*     { */
/*       printf("Permutation des liens %s %s\n",liaison[link1].nom,liaison[link2].nom) ; */
/*       i = gpe_entree1 ; */
/*       gpe_entree1 = gpe_entree2 ; */
/*       gpe_entree2 = i ; */
/*     } */
    /* passez sur le lien un -binaire pour celui qui sort de la soustraction */


    /*modification de Braik B. le 22/07/2004 */

    if (prom_getopt(liaison[link1].nom, "-binaire", resultat) == 2)
    {

#ifdef DEBUG
        printf
            ("c'est le lien %d qui est le resultat binaire de la soustraction \n",
             gpe_entree1);
#endif
        image_pers = (prom_images_struct *) def_groupe[gpe_entree2].ext;
        image_res_soustraction =
            (prom_images_struct *) def_groupe[gpe_entree1].ext;
    }
    else
    {

#ifdef DEBUG
        printf
            ("c'est le lien %d qui est le resultat binaire de la soustraction \n",
             gpe_entree2);
#endif
        image_pers = (prom_images_struct *) def_groupe[gpe_entree1].ext;
        image_res_soustraction =
            (prom_images_struct *) def_groupe[gpe_entree2].ext;
    }
/*fin modif*/

    /* Pour tester si le .ext des gpes d'entree ont une 1 bande prom_image */
    /*image_pers = (prom_images_struct *) def_groupe[gpe_entree1].ext ; */
    /*image_res_soustraction = (prom_images_struct *) def_groupe[gpe_entree2].ext ; */

    if (image_pers == NULL)
    {
        printf
            ("Probleme (function_multiplication_images : il n'y a pas d'image dans le groupe %i\n",
             gpe_entree1);
        exit(EXIT_FAILURE);
    }

    if (image_res_soustraction == NULL)
    {
        printf
            ("Probleme (function_multiplication_images : il n'y a pas d'image dans le groupe %i\n",
             gpe_entree2);
        exit(EXIT_FAILURE);
    }

    /* Pour verifier que les deux images ont bien la meme taille */
    if ((image_pers->sx != image_res_soustraction->sx) ||
        (image_pers->sy != image_res_soustraction->sy))
    {
        printf("Les deux images en entree doivent avoir la meme TAILLE. \n");
        printf("Impossible de les multiplier. \n");
        exit(EXIT_FAILURE);
    }
    else
    {
        largeur = image_pers->sx;
        hauteur = image_pers->sy;
    }


    /* Allocation memoire pour l'image resultat */
    if (def_groupe[gpe_sortie].ext == NULL)
    {
        image_res =
            calloc_prom_image(1, largeur, hauteur, image_pers->nb_band);
        def_groupe[gpe_sortie].ext = image_res;
    }
    else
        image_res = def_groupe[gpe_sortie].ext;


    /* Multiplication des 2 images */
    /*printf("\n-------------------------\n") ;
       printf("Multiplication des images\n") ; */
    nb = image_pers->nb_band;
    /*printf("nb bandes = %d \n",nb) ; */

    for (k = 0; k < largeur * hauteur; k++)
    {
        for (l = 0; l < nb; l++)
            if (image_res_soustraction->images_table[0][k] != 0)
                image_res->images_table[0][nb * k + l] =
                    image_pers->images_table[0][nb * k + l];
            else
                image_res->images_table[0][nb * k + l] = 0; /* le 25/06/2003 Bouaita.  Rajouter pour traitement it�atif de detection de mouvement(il faut virer le mouvement de l'iteration pr��ente car images_table est une variable globale) */
    }



    /*  printf("sauvegarde de l'image dans : image_multiplication.png \n") ;
       save_png_to_disk("image_multiplication.png", *image_res, compression); */
}
