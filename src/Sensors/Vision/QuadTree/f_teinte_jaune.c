/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_teinte_jaune.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Fonction de recuperation des endroits a teinte jaune dans une image

Macro:
-none 

Local variables:
- prom_image_struct * image_teinte_var_globale	

Global variables:
-none

Internal Tools:
-load_param_face()
-detection_teinte()
-recadrer()
-reechant()

External Tools: 
-tools/Vision/Vision/free_prom_image()
-tools/Vision/Vision/calloc_prom_image()


Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Struct/prom_images_struct.h>
#include <Struct/s_cadre.h>
#include <Struct/parametres.h>

#include "tools/include/local_var.h"

#include <public_tools/Vision.h>

#include "tools/include/load_param_face.h"
#include "tools/include/detection_teinte.h"
#include "tools/include/recadrer.h"
#include "tools/include/reechant.h"

void function_teinte_jaune(int gpe_sortie)
{
    int i, nb, /*im, larg, haut, */ link = -1, gpe_entree =
        0, /* compression=9, */ t;
    /*unused:   int nb_iteration=10, para_teinte,para_fusion ; */
    float R, G, B /*, teinte, para_ratio */ ;
    float /*rg,gb,br, */ vmin;
    float min, max, x, ech;
    static parametres para = { 50, 6, 0.4, 8, 1, 10, 32, -1, 0 };   /*{50,6,0.4,8,1,10,32,1,0} ; */
    s_cadre /**extrait=NULL,*/ cadre;
    prom_images_struct *im_pers_sans_fond = NULL, *sans_fond_NG =
        NULL, *sans_fond_teinte = NULL;
    prom_images_struct /**im_detect_teinte=NULL,*/  * im_teinte_recadre =
        NULL, *teinte_reechant = NULL;
    /*unused:  char c; */
#ifndef AVEUGLE                 /*rajouter par issam le 17/06/2003 */
    TxPoint point;
#else
    typedef struct
    {
        int x, y;
    } TxPoint;
    TxPoint point;
#endif

    /* rajout de variable 31/10/2003 Olivier Ledoux */
    int nx, ny;
    prom_images_struct *prom_image;
    float *pointeurFloatSrc;
    float *pointeurFloatDest;
    /* fin rajout variables */
    /* rajout de variable 09/12/2003 Olivier Ledoux */
    unsigned char *rgbMasque;

    /* rajout 31/10/2003 Olivier Ledoux */
    if (def_groupe[gpe_sortie].ext == NULL)
    {


        /*  printf("\n--------------------------------------\n") ;
           printf("--- FONCTION CADRAGE SUR LA TEINTE ---\n") ; */

        /* Recherche du gpe d'entree */
        for (i = 0; i < nbre_liaison; i++)
        {
            if (liaison[i].arrivee == gpe_sortie)
            {
                printf("lien = %s %d \n", liaison[i].nom,
                       strcmp(liaison[i].nom, "image"));
                if (strcmp(liaison[i].nom, "image") == 0)
                {
                    printf("lien OK\n");
                    gpe_entree = liaison[i].depart;
                    link = i;
                    break;
                }

            }
        }

        /* Pour verifier l'existence du lien */
        if (link == -1)
        {
            printf
                ("Erreur : ce groupe n'a pas d'entree ou n'a pas de lien portant le nom image!\n");
            exit(EXIT_FAILURE);
        }



        /* Pour tester si le .ext des gpes d'entree ont une 1 bande prom_image */
        im_pers_sans_fond = (prom_images_struct *) def_groupe[gpe_entree].ext;

        if (im_pers_sans_fond == NULL)
        {
            printf
                ("Probleme function_teinte : il n'y a pas d'image dans le gpe d'entree %i\n",
                 gpe_entree);
            exit(EXIT_FAILURE);
        }

        /* rajout 31/10/2003 Olivier Ledoux */
        prom_image =
            (prom_images_struct *) malloc(sizeof(prom_images_struct));
        if (prom_image == NULL)
        {
            printf("%s : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__);
            exit(0);
        }
        nx = ((prom_images_struct *) def_groupe[gpe_entree].ext)->sx;
        ny = ((prom_images_struct *) def_groupe[gpe_entree].ext)->sy;
        prom_image->sx = nx;
        prom_image->sy = ny;
        prom_image->image_number = 2;   /* il y aura 2 images */
        prom_image->nb_band = 4;    /* on fait croire que les deux images sont reelles */
        prom_image->images_table[0] = (unsigned char *) malloc(nx * ny * sizeof(unsigned char));    /* image niveau de gris pour calgradDS */
        prom_image->images_table[1] = (unsigned char *) malloc(nx * ny * sizeof(float));    /* image pour le masque */
        def_groupe[gpe_sortie].ext = prom_image;

        neurone[def_groupe[gpe_sortie].premier_ele].s = (float) gpe_entree; /* 03/11/2003 Olivier Ledoux */
    }
    else
    {
        prom_image = def_groupe[gpe_sortie].ext;
        nx = prom_image->sx;
        ny = prom_image->sy;

        /* rajout du 03/11/2003 Olivier Ledoux */
        gpe_entree = (int) neurone[def_groupe[gpe_sortie].premier_ele].s;
        im_pers_sans_fond = (prom_images_struct *) def_groupe[gpe_entree].ext;
    }
    /* fin rajout */

    /* Parametres par defaut */

    if (para.oeil == -1)
        load_param_face(&para);
    /*  printf("valeur de la teinte= %d, valeur du ratio = %f  , nb iter = %d\n ", para.teinte, para.ratio, para.fusion); fflush(stdout); */



    /* Conversion de im_pers_sans_fond               */
    /* en niveaux de gris (NG) et en image de teinte */
    sans_fond_NG =
        calloc_prom_image(1, im_pers_sans_fond->sx, im_pers_sans_fond->sy, 1);
    sans_fond_teinte =
        calloc_prom_image(1, im_pers_sans_fond->sx, im_pers_sans_fond->sy, 1);

    vmin = 4.;

    nb = im_pers_sans_fond->nb_band;
    if (nb != 3)
    {
        printf
            ("L'image en entree n'est pas en couleurs (voir pb sur gpe %d en entree du gpe %d)\n",
             gpe_entree, gpe_sortie);
        exit(EXIT_FAILURE);
    }

    min = 255.;
    max = 0.;
    for (i = 0; i < im_pers_sans_fond->sx * im_pers_sans_fond->sy; i++)
    {
        R = (float) (im_pers_sans_fond->images_table[0][nb * i]);
        G = (float) (im_pers_sans_fond->images_table[0][nb * i + 1]);
        B = (float) (im_pers_sans_fond->images_table[0][nb * i + 2]);

        sans_fond_NG->images_table[0][i] =
            (unsigned char) ((int) ((R + G + B) / nb));
        x = G - B;
        if (x < min)
            min = x;
        else if (x > max)
            max = x;
    }

    /*
       #ifndef AVEUGLE
       TxAfficheImageCouleur(&image2, im_pers_sans_fond->images_table[0],
       im_pers_sans_fond->sx,im_pers_sans_fond->sy,1,1);
       affiche_ng(&image1,sans_fond_NG->images_table[0],sans_fond_NG->sx,sans_fond_NG->sy,0,0,1,1); 
       #endif
     */

#ifndef AVEUGLE
    TxAfficheImageCouleur(&image1, im_pers_sans_fond->images_table[0],
                          im_pers_sans_fond->sx, im_pers_sans_fond->sy, 1, 1);
#endif

    printf("min=%f max=%f\n", min, max);
    ech = 255. / (max - min);
    for (i = 0; i < im_pers_sans_fond->sx * im_pers_sans_fond->sy; i++)
    {
        R = (float) (im_pers_sans_fond->images_table[0][nb * i]);
        G = (float) (im_pers_sans_fond->images_table[0][nb * i + 1]);
        B = (float) (im_pers_sans_fond->images_table[0][nb * i + 2]);

        /*      sans_fond_teinte->images_table[0][i] = (unsigned char)((int)((min+G-B)*ech)) ; */
        sans_fond_teinte->images_table[0][i] =
            (unsigned char) ((int) ((255. + G - B) * 0.5));

    }

    image_teinte_var_globale = sans_fond_teinte;
    /*<-----   save_png_to_disk("teinte.png", *sans_fond_teinte, compression) ; */
    printf("Teinteeee!\n");


    t = detection_teinte(sans_fond_teinte, &cadre, &para);

    if (t == 0)
    {
        printf("pas de teinte detecte...\n");
        cadre.l1 = 0;
        cadre.l2 = sans_fond_teinte->sy;
        cadre.c1 = 0;
        cadre.c2 = sans_fond_teinte->sx;
    }
    else                        /* entoure en rouge la teinte trouvee */
    {
        point.x = cadre.l1;
        point.y = cadre.c1;

#ifndef AVEUGLE
        TxDessinerRectangle(&image1, rouge, TxVide, point,
                            cadre.l2 - cadre.l1, cadre.c2 - cadre.c1, 2);
        TxFlush(&image1);
#endif

#ifndef AVEUGLE
        TxDessinerRectangle(&image2, rouge, TxVide, point,
                            cadre.l2 - cadre.l1, cadre.c2 - cadre.c1, 2);
        TxFlush(&image2);
#endif

    }

    /* Recadrage de la teinte */
    printf("\n--------------------\n");
    printf("recadrage de la teinte \n");

    im_teinte_recadre =
        calloc_prom_image(1, sans_fond_NG->sx, sans_fond_NG->sy, 1);

    t = recadrer(sans_fond_NG, im_teinte_recadre, &cadre);

    /* mais a quoi ca sert ???? PG */
    for (i = im_teinte_recadre->sx;
         i < im_teinte_recadre->sx * im_teinte_recadre->sy; i++)
    {
        if (im_teinte_recadre->images_table[0][i] == (unsigned char) 0)

            im_teinte_recadre->images_table[0][i] =
                (unsigned char) ((im_teinte_recadre->images_table[0][i - 1] +
                                  im_teinte_recadre->images_table[0][i -
                                                                     im_teinte_recadre->
                                                                     sx]) /
                                 2);
    }

    printf("sauvegarde de la teinte recadree dans : teinte_recadre.png \n");
    /*   save_png_to_disk("teinte_recadre.png", *im_teinte_recadre, compression) ; */

    /* Reechantillonnage */
    /* modification 12/11/2003 Olivier Ledoux */
    /* pour ne pas provoquer de conflit avec le veritable bloc ext du groupe */
    /*
       if (def_groupe[gpe_sortie].ext == NULL) 
       { 
       teinte_reechant = calloc_prom_image(1, 60, 80, 1) ;   
       def_groupe[gpe_sortie].ext = teinte_reechant ; 
       } 
       else 
       teinte_reechant =  def_groupe[gpe_sortie].ext ;
     */
    if (def_groupe[gpe_sortie].data == NULL)
    {
        teinte_reechant = calloc_prom_image(1, 60, 80, 1);
        def_groupe[gpe_sortie].data = teinte_reechant;
    }
    else
        teinte_reechant = def_groupe[gpe_sortie].data;



    t = reechant(im_teinte_recadre, teinte_reechant);

    printf
        ("sauvegarde de la teinte recadree & reechantillonnee ds : teinte_reech.png \n");
    /*        save_png_to_disk("teinte_reech.png", *teinte_reechant, compression) ;   */


    /* rajout du 31/10/2003 Olivier Ledoux */
    /*
       ici nous avons notre masque reel dans le bloc 1 de sans_fond_teinte
       et notre image en niveau de gris dans le bloc 0 de sans_fond_NG
     */


    printf("ok avant for\n");
    pointeurFloatSrc = (float *) sans_fond_teinte->images_table[1];
    pointeurFloatDest = (float *) prom_image->images_table[1];
    for (i = 0; i < nx * ny; i++)
    {
        prom_image->images_table[0][i] = sans_fond_NG->images_table[0][i];
        pointeurFloatDest[i] = pointeurFloatSrc[i];
    }
    printf("ok apres for\n");
    free(sans_fond_teinte->images_table[1]);    /* libere le bloc allouer */
    sans_fond_teinte->images_table[1] = NULL;



    /* rajout du 09/12/2003 Olivier Ledoux */
    /*
       generation d'une image couleur avec masquage par le filtre jaune
     */
    rgbMasque = (unsigned char *) malloc(nx * ny * 3 * sizeof(unsigned char));
    pointeurFloatSrc = (float *) prom_image->images_table[1];   /* pointeur sur le masque */
    for (i = 0; i < nx * ny; i++)
    {
        if (pointeurFloatSrc[i] > 0.25)
        {
            rgbMasque[i * 3 + 0] =
                im_pers_sans_fond->images_table[0][i * 3 + 0];
            rgbMasque[i * 3 + 1] =
                im_pers_sans_fond->images_table[0][i * 3 + 1];
            rgbMasque[i * 3 + 2] =
                im_pers_sans_fond->images_table[0][i * 3 + 2];
        }
        else
        {
            rgbMasque[i * 3 + 0] = 0;
            rgbMasque[i * 3 + 1] = 0;
            rgbMasque[i * 3 + 2] = 0;
        }
    }
#ifndef AVEUGLE
    TxAfficheImageCouleur(&image2, rgbMasque, nx, ny, 1, 1);

#endif



    /* fin rajout */

    printf("et 1\n");

    free_prom_image(sans_fond_NG);
    free(sans_fond_NG);
    sans_fond_NG = NULL;
    printf("et 2\n");
    free_prom_image(sans_fond_teinte);
    free(sans_fond_teinte);
    sans_fond_teinte = NULL;
    printf("et 3\n");
    free_prom_image(im_teinte_recadre);
    free(im_teinte_recadre);
    im_teinte_recadre = NULL;
    printf("fin teinte_jaune\n");
}

/*******************************************************************************/
