/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_check_visual_compass.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

typedef struct My_Data_f_check_visual_compass
{
    int gpe_compass;
} My_Data_f_check_visual_compass;

/*#define DEBUG*/
void function_check_visual_compass(int numero)
{
    int gpe_compass, i, l;
    My_Data_f_check_visual_compass *my_data = NULL;
    char param[32];
    float max0 = -1.;
    float max1 = -1.;
    int imax0 = -1;
    int imax1 = -1;
    float d;

    (void) imax1; // (unused)

    if (def_groupe[numero].data == NULL)
    {
        gpe_compass = -1;
        i = 0;
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "c", param) >= 1)
                gpe_compass = liaison[l].depart;
            i++;
            l = find_input_link(numero, i);
        }
        if (gpe_compass == -1)
        {
            printf("pas de groupe correct en entree de %s\n", __FUNCTION__);
            exit(0);
        }
        my_data =
            (My_Data_f_check_visual_compass *)
            malloc(sizeof(My_Data_f_check_visual_compass));
        if (my_data == NULL)
        {
            printf("problem de malloc dans %s\n", __FUNCTION__);
            exit(0);
        }
        my_data->gpe_compass = gpe_compass;
        def_groupe[numero].data = my_data;
    }
    else
    {
        my_data = (My_Data_f_check_visual_compass *) def_groupe[numero].data;
        gpe_compass = my_data->gpe_compass;
    }

    max0 = -1.;
    max1 = -1.;
    imax0 = -1;

    for (i = 0; i < def_groupe[gpe_compass].nbre; i++)
    {
        if (neurone[i - 1 + def_groupe[gpe_compass].premier_ele].s1 > max0)
        {
            max0 = neurone[i + def_groupe[gpe_compass].premier_ele].s1;
            imax0 = i;
        }
    }
    for (i = 0; i < def_groupe[gpe_compass].nbre; i++)
    {
        d = imax0 - i;
        if (d > 0.5 * def_groupe[gpe_compass].nbre)
            d = d - def_groupe[gpe_compass].nbre;
        else if (d < -0.5 * def_groupe[gpe_compass].nbre)
            d = d + def_groupe[gpe_compass].nbre;

        if (abs(d) > def_groupe[gpe_compass].nbre / 6)
        {
            if (neurone[i - 1 + def_groupe[gpe_compass].premier_ele].s1 >
                max1)
            {
                max1 = neurone[i + def_groupe[gpe_compass].premier_ele].s1;
                imax1 = i;
            }
        }
    }

    if (max1 > 0. && max1 > 0.75 * max0)
    {
        neurone[def_groupe[numero].premier_ele].s =
            neurone[def_groupe[numero].premier_ele].s1 =
            neurone[def_groupe[numero].premier_ele].s2 = 0.;
    }
    else
    {
        neurone[def_groupe[numero].premier_ele].s =
            neurone[def_groupe[numero].premier_ele].s1 =
            neurone[def_groupe[numero].premier_ele].s2 = 1.;
    }

#ifdef DEBUG
    printf("===============fin de %s\n", __FUNCTION__);
#endif
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_check_visual_compass\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

}
