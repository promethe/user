/** ***********************************************************
\file  energy.c
\brief calculate distance between two points

Author: Oriane Dermy
Created: 20/05/15

Theoritical description:

Description:
Computes the energy of signals. Signals should be put in line. 
* There are y_size signals composed in x_size samples.
* The outpuet is a vector of y_size energies.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options

Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
//#define DEBUG 1
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>


typedef struct energy{
	int inGroup;
} type_energy;


void new_energy(int Gpe)
{
	int l;
	
	type_energy *my_energy=NULL;

	dprints("Begining of new_energy\n");

	my_energy=(type_energy*)malloc(sizeof(type_energy));

	if(my_energy==NULL)
	{
		dprints("my_energy is NULL!\n");
		EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
	}
	
	/**Initialisation **/
	my_energy->inGroup=-1;
	/******END Init********/
	/******************************************************Links Treatments*************************************/
	dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
		l = find_input_link(Gpe, 0);
		if(l == -1)
		{
			EXIT_ON_ERROR("Error, it should have one input of this function composed of signals\n");
		}
		my_energy->inGroup = liaison[l].depart;
		
	}
	if (my_energy->inGroup == -1)
	{
		dprints("%s : No group in input !\n", __FUNCTION__);
		kprints("%s : No group in input !\n", __FUNCTION__);
		EXIT_ON_ERROR("xxxxx %d\n",Gpe);
	}

	dprints("Groupe input is : %s\n", def_groupe[my_energy->inGroup].no_name);

	
	dprints("End of the reading links\n");
	/****************************************End Links Treatments**********************************/
	def_groupe[Gpe].data= my_energy; /*Save of My_Data*/

	dprints("End of new_energy\n");
}


void destroy_energy(int gpe)
{
	free(def_groupe[gpe].data);
	def_groupe[gpe].data=NULL;
	def_groupe[gpe].ext=NULL;
}


void function_energy(int Gpe)
{
	int debut, debutInGpe, i, j, p;
	type_energy *my_energy=NULL;
	float *tempVal;
	
	dprints("At the begining of the function energy.\n");

	my_energy = def_groupe[Gpe].data;
	
	debut = def_groupe[Gpe].premier_ele;
	debutInGpe = def_groupe[my_energy->inGroup].premier_ele;
	
	//verification des tailles
	if(def_groupe[Gpe].taillex!=1)
	{
		EXIT_ON_ERROR("Error taillex should be equal to 1.\n");
	}
	if(def_groupe[Gpe].tailley != def_groupe[Gpe].tailley)
	{
		EXIT_ON_ERROR("Error tailley should be the same as the previous group.\n");
	}
	
	//recupération des signaux et calcul de leur puissance
	tempVal = malloc((def_groupe[my_energy->inGroup].tailley)*sizeof(float));
	dprints("Sortie : \n");
	for(i=0;i<def_groupe[my_energy->inGroup].tailley;i++)
	{
			tempVal[i] = 0.0;
			for(j=0;j<def_groupe[my_energy->inGroup].taillex;j++)
			{
				p = debutInGpe + j + i*def_groupe[my_energy->inGroup].taillex;
				tempVal[i] += neurone[p].s1*neurone[p].s1;
			}
			tempVal[i] /= (float) def_groupe[my_energy->inGroup].taillex;
			neurone[debut + i].s1 = neurone[debut + i].s2 = neurone[debut + i].s = tempVal[i];
				dprints("%f\t", neurone[debut +i ].s1);
	}
	dprints("\n");

	dprints("end of energy Gpe %d : %s\n", Gpe, __FUNCTION__);
}
