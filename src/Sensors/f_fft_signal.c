/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file  
\brief 

Author: Oriane Dermy
Created: 20.05.15

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description: 

 Execute une fft 1d (réel vers complexe) sur les neurones du groupe précédent. 
 * Si il s'agit d'une matrice de deux lignes. Il effectue une fft sur chaque vecteur d'un même indice "y".
 * Ainsi, pour deux oreilles, les intensités de l'une sont données pour y=0 et l'autre sur y=1.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo 
- description: Donne la taille de la FFT
- input expected group: un signal de deux lignes N colonnes
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#ifndef NO_ALSA
#include <Sound/Sound.h>
#endif
#include <Kernel_Function/find_input_link.h>
#include <fftw3.h>
#include <complex.h> 

//#define DEBUG 1

typedef struct MyData_f_fft_signal
{
	int GpeIn;
	int buffer_size;
} MyData;

void new_fft_signal(int Gpe)
{
	int l, buffer_size, GpeIn;
	MyData *my_data;
	
	my_data = (MyData *)malloc(sizeof(MyData));
	if (my_data == NULL)
    {
      dprints("error in memory allocation in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }

		l = find_input_link(Gpe, 0);
		if (l == -1)
		{
			exit(1);
		}

		GpeIn = liaison[l].depart;
		buffer_size = def_groupe[GpeIn].taillex;

		/** test denew_fft_signalnew_fft_signal	 la taille et la position de la matrice a extraire
		 * vs la taille du groupe d'entree */
		if(buffer_size != 1024 && buffer_size != 128 && buffer_size!=256 && buffer_size !=512 && buffer_size !=2048)
		{
			EXIT_ON_ERROR("In groupe (%s): The size of taillex of the prevous function has to be a multiple of 2 (128, 256, 512, 1024).\n",def_groupe[Gpe].no_name);
		}
		
		/*La FFT donne un vecteur de fréquence de taille correspondant à l'echantillon / 2 +1. On verifie ici que le nombre de neurone est correct.*/
		if(def_groupe[GpeIn].taillex/2 +1 != def_groupe[Gpe].taillex)
		{
			EXIT_ON_ERROR("In group (%s) : The x_size (%d) shoud be the equal to x'_size/2+1 (%d) with x'_size = %d.\n", def_groupe[Gpe].no_name, def_groupe[Gpe].taillex, (def_groupe[GpeIn].taillex/2+1), def_groupe[Gpe].taillex);
		}
		if(def_groupe[GpeIn].tailley <def_groupe[Gpe].tailley)
		{
			EXIT_ON_ERROR("Erreur le vecteur en entré est plus petit que le vecteur de sortie.\n");
		}
		def_groupe[Gpe].data = my_data = (MyData *) malloc(sizeof(MyData));
        if (my_data == NULL) 
        {
            EXIT_ON_ERROR("error: in f_fft_signal(gpe = %i): malloc failed\n", Gpe);
        }
        my_data->GpeIn = GpeIn;
        my_data->buffer_size = buffer_size;
		def_groupe[Gpe].data = my_data;
		if(my_data == NULL || def_groupe[Gpe].data ==NULL) printf("ERROOOOR\n\n");

}

void function_fft_signal(int Gpe)
{

	int GpeIn = -1, i,j, p, buffer_size;
	fftw_complex *out;
	fftw_plan pl;
	double *e;
    MyData *my_data =NULL;
   
	#ifdef DEBUG
		printf("entree function_fft_signal\n");
	#endif 
	
	if(def_groupe[Gpe].data ==NULL) printf("ERROOOOER222\n\n");
	my_data = (MyData *) def_groupe[Gpe].data; 
    if (my_data == NULL)
    {
      dprints("error while loading data in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
	dprints("%d\n", my_data->buffer_size);

	buffer_size = my_data->buffer_size;
	GpeIn = my_data->GpeIn;

	e = malloc(buffer_size*sizeof(double));

	// We copy values of the previous box into vectors e[]
	for(j=0;j < def_groupe[Gpe].tailley; j++)
	{
		for (i = 0; i < def_groupe[GpeIn].taillex; i++)
		{		
				p = def_groupe[GpeIn].premier_ele + i + j*def_groupe[GpeIn].taillex;
				e[i] = (double) neurone[p].s1;
				//printf("p=%d\t",p);					
		}
		//printf("\n");
		//for all y vectors, we create the plan to compute fftw
		out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (buffer_size/2 +1));
		pl = fftw_plan_dft_r2c_1d(buffer_size, e, out, FFTW_ESTIMATE);
		
		if(pl==NULL) dprints("ERROR during the creation of the plan \"pl\"\n");
		fftw_execute(pl);
		fftw_destroy_plan(pl);
		//Copy results into neurons : for an input size = N the output size is N/2 +1
		for(i=0;i<buffer_size/2+1; i++)
		{
			p = def_groupe[Gpe].premier_ele + i + j*((buffer_size/2)+1);
			neurone[p].s1 = (float) sqrt(out[i][0]*out[i][0] + out[i][1]*out[i][1]);
		}
		fftw_free(out);
	}
	free(e);
/*

	//plan creation allowing fft (inputs are real and outputs are complex (phase and amplitude).
	// with FFTW_ESTIMATE we simplify calculation (speeder but less optimal)
	 pl = fftw_plan_dft_r2c_1d(buffer_size, e_left, out_left, FFTW_ESTIMATE);
	 pr = fftw_plan_dft_r2c_1d(buffer_size, e_right, out_right, FFTW_ESTIMATE);
	 
	 if(pl==NULL) printf("ERREUR PLAN L\n");
	 if(pr==NULL) printf("ERREUR plan R\n");

	 //exectution of the fft
	 fftw_execute(pl);
	 fftw_execute(pr);

	 fftw_destroy_plan(pl);	 
	 fftw_destroy_plan(pr);


	 //Copy results into neurons : for an input size = N the output size is N/2 +1
	 for(i=0;i<buffer_size/2+1; i++)
	 {
		 p = def_groupe[Gpe].premier_ele + i;
		 q = def_groupe[Gpe].premier_ele + i + buffer_size/2+1;
		 neurone[p].s1 = (float) sqrt(out_left[i][0]*out_left[i][0] + out_left[i][1]*out_left[i][1]);
		 neurone[q].s1 =(float) sqrt(out_right[i][0]*out_right[i][0] + out_right[i][1]*out_right[i][1]);
		//spectrum_l[j] = 20*log10(spectrum_l[j]);
		//spectrum_r[j] = 20*log10(spectrum_r[j]);
	 }
	 
	 fftw_free(out_left);
	 fftw_free(out_right);
	 free(e_left);
	 free(e_right);
*/

#ifdef DEBUG
	printf("End of f_fft_signal function\n");
#endif

}

