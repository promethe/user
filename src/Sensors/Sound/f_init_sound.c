/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_init_sound f_init_sound
   \ingroup libSensors

   \brief initialise les handles pour la capture ou playback audio

   \section Description
   initialise les handles pour la capture ou playback audio
   tire de Alsalib

   \section Comments
   - pour sauver les trames acquises dans un fichier wav nomme sample.wav mettre -sound,wav sur le lien entrant.
   - pour sauver les trames dans un fichier ascii nomme dat(pour ouvrir dans mathlab par exemple mettre -sound,save
   - pour les deux mettre -sound,wave,save
	   
   **************** AVANT TOUT : ***************************
   verifier avec la commande alsamixer que la capture audio est disponible!!!!

   Known bugs: 1/la taille de la trame (chunk_size)ne peut etre change pour le moment...
   voir f_audio_capture line 334 environ
   2/ le fichier sample.wav contient un en-tete corrompu, il indique une taille incorrecte
   ceci empeche le traitement de ce fichier par certain programmes(mathlab, etc.)
		
   \file
   \ingroup f_init_sound
   \brief initialise les handles pour la capture ou playback audio

   Author: A.HIOLLE

   Description:
   initialise les handles pour la capture ou playback audio
   tire de Alsalib



   Comments:  pour sauver les trames acquises dans un fichier wav nomme sample.wav
   mettre -sound,wav sur le lien entrant.
   pour sauver les trames dans un fichier ascii nomme dat(pour ouvrir dans mathlab par exemple
   mettre -sound,save
   pour les deux mettre -sound,wave,save
	   
   **************** AVANT TOUT : ***************************
   verifier avec la commande alsamixer que la capture audio est disponible!!!!

   Known bugs: 1/la taille de la trame (chunk_size)ne peut etre change pour le moment...
   voir f_audio_capture line 334 environ
   2/ le fichier sample.wav contient un en-tete corrompu, il indique une taille incorrecte
   ceci empeche le traitement de ce fichier par certain programmes(mathlab, etc.)
		

************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Sound/Sound.h>

#include "tools/include/local_var.h"


int mrtype;
char *name;
int write_wav = 0;
int write_dat = 0;
int __attribute__ ((unused)) timelimit = 0;
int __attribute__ ((unused)) quiet_mode = 0;
int __attribute__ ((unused)) file_type = FORMAT_DEFAULT;
unsigned int __attribute__ ((unused)) sleep_min = 0;
int __attribute__ ((unused)) open_mode = 0;

snd_pcm_stream_t __attribute__ ((unused)) stream = SND_PCM_STREAM_CAPTURE;
int __attribute__ ((unused)) mmap_flag = 0;
int __attribute__ ((unused)) interleaved = 1;
int __attribute__ ((unused)) nonblock = 0;
u_char * __attribute__ ((unused)) audiobuf = NULL;
snd_pcm_uframes_t __attribute__ ((unused)) chunk_size = 0;
unsigned __attribute__ ((unused)) period_time = 0;
unsigned __attribute__ ((unused)) buffer_time = 0;
snd_pcm_uframes_t __attribute__ ((unused)) period_frames = 0;
snd_pcm_uframes_t __attribute__ ((unused)) buffer_frames = 0;
int __attribute__ ((unused)) avail_min = -1;
int __attribute__ ((unused)) start_delay = 0;
int __attribute__ ((unused)) stop_delay = 0;
int __attribute__ ((unused)) verbose = 0;
int __attribute__ ((unused)) buffer_pos = 0;
size_t
__attribute__ ((unused)) bits_per_sample,
  __attribute__ ((unused)) bits_per_frame;
size_t __attribute__ ((unused)) chunk_bytes;
snd_output_t *mylog;

int __attribute__ ((unused)) fd = -1;
#ifndef NO_ALSA
off64_t __attribute__ ((unused)) pbrec_count =
  (off64_t) - 1, __attribute__ ((unused)) fdcount;
#endif
int __attribute__ ((unused)) vocmajor, __attribute__ ((unused)) vocminor;


#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/


void set_params(void);
void capture(const char *filename) __attribute__ ((unused));
void playback(char *filename) __attribute__ ((unused));
void playbackv(char **filenames, unsigned int count) __attribute__ ((unused));
void capturev(char **filenames, unsigned int count) __attribute__ ((unused));
void begin_voc(int fd, size_t count) __attribute__ ((unused));
void end_voc(int fd) __attribute__ ((unused));
void begin_wave(int fd, size_t count) __attribute__ ((unused));
void end_wave(int fd) __attribute__ ((unused));
void end_raw(int fd) __attribute__ ((unused));
void begin_au(int fd, size_t count) __attribute__ ((unused));
void end_au(int fd) __attribute__ ((unused));
int sample_size = 1000;
FILE *f;
size_t cur = 0;
int __attribute__ ((unused)) s_trame = 0;
struct fmt_capture
{
  void (*start) (int fd, size_t count);
  void (*end) (int fd);
  const char *what;
} __attribute__ ((unused)) fmt_rec_table[] =
{
  {
    NULL, end_raw, "raw data"},
  {
    begin_voc, end_voc, "VOC"},
  {
    begin_wave, end_wave, "WAVE"},
  {
    begin_au, end_au, "Sparc Audio"}
};



/****Fonction a appeler dans leto****************/
void function_init_sound(int numero)
{

  char *fbuf = malloc(1);
  char *chaine = NULL, *info = NULL;  /*Stocke le nom du lien et les options dessus */
  int Gpe_sound = -1, l, i = 0, link = -1;

  if (def_groupe[numero].data == NULL)
  {
    /*recherche du lien en entree */
    l = find_input_link(numero, i);
    while (l != -1)
    {
      if (strncmp(liaison[l].nom, "-sound", 6) == 0)
      {
	Gpe_sound = liaison[l].depart;
	link = l;
      }
      i++;
      l = find_input_link(numero, i);
    }
#ifdef DEBUG
    printf("link sound=%d", Gpe_sound);
    endl;
#endif
    /*Sortie si le lien n'est pas trouve */
    if (Gpe_sound == -1)
    {
      printf("liaison avec groupe sound non trouve \n");
      exit(0);
    }
    else
    {
      chaine = liaison[link].nom;
      if (chaine == NULL)
      {
	printf("numero : %d : %s : pas de lien entrant...\n", numero,
	       __FUNCTION__);
	exit(0);
      }
      info = NULL;
      info = strstr(chaine, "-sound");
      printf("chaine=%s,info=%s\n", chaine, info);
      if (info != NULL)
      {


	if ((strncmp(&info[6], ",wav", 4)) == 0)
	{
	  printf("wav found\n");
	  printf("inf=%s\n", &info[6]);
	  write_wav = 1;
	  info = strstr(chaine, "-sound,wav");
	  printf("dat : info=%s\n", &info[10]);
	  if ((strncmp(&info[10], ",dat", 4)) == 0)
	  {
	    printf("dat trouve aussi\n");
	    printf("dat : info=%s\n", info);
	    write_dat = 1;
	  }

	}
	else if ((strncmp(&info[6], ",dat", 3)) == 0)
	{
	  pr("only dat found\n");
	  printf("inf=%s\n", &info[6]);
	  write_dat = 1;
	}
	printf("so write_dat=%d, write_wav=%d\n", write_dat,
	       write_wav);

      }
      else
      {
	printf
	  ("%s : il faut definir les dimensions des FFT (-X### )\n",
	   __FUNCTION__);
	exit(0);
      }

      def_groupe[numero].data = (void *) malloc(sizeof(int));

      if (def_groupe[numero].data == NULL)
      {

	perror("init_sound allocation error\n");
	exit(-1);
      }

      *(int *) (def_groupe[numero].data) = Gpe_sound;
#ifdef DEBUG
      printf("link sound stored=%d",
	     *(int *) (def_groupe[numero].data));
      endl;
#endif
    }


  }
  else
  {

    printf("Init_sound ne s'execute qu'une fois\n");

  }

  if (f_audio_init() == -1)
  {


    pr("Erreur a l'ouverture");
    exit(-1);

  }
  if (f_audio_capture(fbuf) != EXIT_SUCCESS)
  {
    pr("Error while initializing capture handles\n");
    exit(-1);
  }


}

void device_list(void)
{
#ifndef NO_ALSA
  snd_ctl_t *handle;
  int card, err, dev, idx;
  snd_ctl_card_info_t *info;
  snd_pcm_info_t *pcminfo;
  snd_ctl_card_info_alloca(&info);
  snd_pcm_info_alloca(&pcminfo);

  card = -1;
  if (snd_card_next(&card) < 0 || card < 0)
  {
    error("no soundcards found...");
    return;
  }
  while (card >= 0)
  {
    char name[32];
    sprintf(name, "hw:%d", card);
    if ((err = snd_ctl_open(&handle, name, 0)) < 0)
    {
      error("control open (%i): %s", card, snd_strerror(err));
      goto next_card;
    }
    if ((err = snd_ctl_card_info(handle, info)) < 0)
    {
      error("control hardware info (%i): %s", card, snd_strerror(err));
      snd_ctl_close(handle);
      goto next_card;
    }
    fprintf(stderr, "**** List of %s Hardware Devices ****\n",
	    snd_pcm_stream_name(stream));
    dev = -1;
    while (1)
    {
      unsigned int count;
      if (snd_ctl_pcm_next_device(handle, &dev) < 0)
	error("snd_ctl_pcm_next_device");
      if (dev < 0)
	break;
      snd_pcm_info_set_device(pcminfo, dev);
      snd_pcm_info_set_subdevice(pcminfo, 0);
      snd_pcm_info_set_stream(pcminfo, stream);
      if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0)
      {
	if (err != -ENOENT)
	  error("control digital audio info (%i): %s", card,
		snd_strerror(err));
	continue;
      }
      fprintf(stderr, "card %i: %s [%s], device %i: %s [%s]\n",
	      card, snd_ctl_card_info_get_id(info),
	      snd_ctl_card_info_get_name(info), dev,
	      snd_pcm_info_get_id(pcminfo),
	      snd_pcm_info_get_name(pcminfo));
      count = snd_pcm_info_get_subdevices_count(pcminfo);
      fprintf(stderr, "  Subdevices: %i/%i\n",
	      snd_pcm_info_get_subdevices_avail(pcminfo), count);
      for (idx = 0; idx < (int) count; idx++)
      {
	snd_pcm_info_set_subdevice(pcminfo, idx);
	if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0)
	{
	  error("control digital audio playback info (%i): %s",
		card, snd_strerror(err));
	}
	else
	{
	  fprintf(stderr, "  Subdevice #%i: %s\n", idx,
		  snd_pcm_info_get_subdevice_name(pcminfo));
	}
      }
    }
    snd_ctl_close(handle);
  next_card:
    if (snd_card_next(&card) < 0)
    {
      error("snd_card_next");
      break;
    }
  }
#endif
}

void pcm_list(void)
{
#ifndef NO_ALSA
  snd_config_t *conf;
  snd_output_t *out;
  int err = snd_config_update();
  if (err < 0)
  {
    error("snd_config_update: %s", snd_strerror(err));
    return;
  }
  err = snd_output_stdio_attach(&out, stderr, 0);
  assert(err >= 0);
  err = snd_config_search(snd_config, "pcm", &conf);
  if (err < 0)
    return;
  fprintf(stderr, "PCM list:\n");
  snd_config_save(conf, out);
  snd_output_close(out);
#endif
}

/*
  void version(void)
  {
  fprintf(stderr, "%s: version " SND_UTIL_VERSION_STR " by Jaroslav Kysela <perex@suse.cz>\n", command);
  }
*/
void signal_handler(int sig)
{
#ifndef NO_ALSA
  if (!quiet_mode)
    fprintf(stderr, "Aborted by signal %s...\n", strsignal(sig));
  if (stream == SND_PCM_STREAM_CAPTURE)
  {
    if (write_wav == 1)
    {
      fmt_rec_table[file_type].end(fd);
      stream = -1;
    }
  }
  if (fd > 1)
  {
    close(fd);
    fd = -1;
  }
  if (handle)
  {
    snd_pcm_close(handle);
    handle = NULL;
  }
  exit(EXIT_FAILURE);
#endif
}

enum
{
  OPT_VERSION = 1,
  OPT_PERIOD_SIZE,
  OPT_BUFFER_SIZE
};
/*
 * fonction d'ouverture des handles necessaires 
 * a l'acquisition audio
 */
int f_audio_init()
{
#ifndef NO_ALSA
  const char *pcm_name = "default";
  int /*tmp, */ err /*, c */ ;
  int do_device_list = 0, do_pcm_list = 0;
  snd_pcm_info_t *info;

  snd_pcm_info_alloca(&info);

  err = snd_output_stdio_attach(&mylog, stderr, 0);
  assert(err >= 0);

  /*command = argv[0]; */
  /*file_type = FORMAT_DEFAULT; */
  /* choix entre la lecture et la capture */


  start_delay = 1;

  stream = SND_PCM_STREAM_CAPTURE;
  file_type = FORMAT_WAVE;
  sprintf(command,"%s","arecord");
  chunk_size = -1;
  rhwparams.format = SND_PCM_FORMAT_U8;
  rhwparams.rate = DEFAULT_SPEED;
  rhwparams.channels = 1;






  if (do_device_list)
  {
    if (do_pcm_list)
      pcm_list();
    device_list();
    snd_config_update_free_global();
    return 0;
  }
  else if (do_pcm_list)
  {
    pcm_list();
    snd_config_update_free_global();
    return 0;
  }

  err = snd_pcm_open(&handle, pcm_name, stream, open_mode);
  if (err < 0)
  {
    error("audio open error: %s", snd_strerror(err));
    return 1;
  }

  if ((err = snd_pcm_info(handle, info)) < 0)
  {
    error("info error: %s", snd_strerror(err));
    return 1;
  }


  return EXIT_SUCCESS;
#endif
}

/*
 * fonction de fermeture des handles audio
 */
int f_audio_close()
{
#ifndef NO_ALSA
  snd_pcm_close(handle);

  free(audiobuf);
  snd_output_close(mylog);
  snd_config_update_free_global();
  return EXIT_SUCCESS;
#endif
}

int f_audio_capture(char *buf) /* before : size_t size_trame was also an input */
{

#ifndef NO_ALSA
  chunk_size = 1024;
  hwparams = rhwparams;

  if (buf == NULL)
  {
    error("not enough memory");
    return 1;
  }

  if (mmap_flag)
  {
    writei_func = snd_pcm_mmap_writei;
    readi_func = snd_pcm_mmap_readi;
    writen_func = snd_pcm_mmap_writen;
    readn_func = snd_pcm_mmap_readn;
  }
  else
  {
    writei_func = snd_pcm_writei;
    readi_func = snd_pcm_readi;
    writen_func = snd_pcm_writen;
    readn_func = snd_pcm_readn;
  }


  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGABRT, signal_handler);
  /*if (interleaved) {  $ */
  /*printf("cs=%d,,cb=%d",(int)chunk_size,chunk_bytes);endl; */

  capture("sample.wav");

  /*} */

  /* exclu car le cas non interleaved n'a pas ete envisage
     else {
     if (stream == SND_PCM_STREAM_PLAYBACK)
     playbackv(&argv[optind], argc - optind);
     else
     capturev(&argv[optind], argc - optind);
     } */

  return (EXIT_SUCCESS);
#endif

}

/*
 * Safe read (for pipes)
 */

ssize_t safe_read(int fd, void *buf, size_t count)
{
  ssize_t result = 0, res;

  while (count > 0)
  {
    if ((res = read(fd, buf, count)) == 0)
      break;
    if (res < 0)
      return result > 0 ? result : res;
    count -= res;
    result += res;
    buf = ((unsigned char*)buf) + res;
  }
  return result;
}

/*
 * Test, if it is a .VOC file and return >=0 if ok (this is the length of rest)
 *                                       < 0 if not 
 */
int test_vocfile(void *buffer)
{
  VocHeader *vp = buffer;

  {
    if (strstr((char*)vp->magic, VOC_MAGIC_STRING))
      vocminor = vp->version & 0xFF;
    vocmajor = vp->version / 256;
    if (vp->version != (0x1233 - vp->coded_ver))
      return -2;          /* coded version mismatch */
    return vp->headerlen - sizeof(VocHeader);   /* 0 mostly */
  }
  return -1;                  /* magic string fail */
}

/*
 * helper for test_wavefile
 */

size_t test_wavefile_read(int fd, char *buffer, size_t * size, size_t reqsize,
                          int line)
{
  if (*size >= reqsize)
    return *size;
  if ((size_t) safe_read(fd, buffer + *size, reqsize - *size) !=
      reqsize - *size)
  {
    error("read error (called from line %i)", line);
    exit(EXIT_FAILURE);
  }
  return *size = reqsize;
}

#define check_wavefile_space(buffer, size, len, blimit) \
  if (size + len > blimit)				\
    blimit = size + len;				\
  if ((buffer = realloc(buffer, blimit)) == NULL) {	\
    error("not enough memory");				\
    exit(EXIT_FAILURE);					\
  }

/*
 * test, if it's a .WAV file, > 0 if ok (and set the speed, stereo etc.)
 *                            == 0 if not
 * Value returned is bytes to be discarded.
 */
ssize_t test_wavefile(int fd, u_char *_buffer, size_t size)
{
#ifndef NO_ALSA
  WaveHeader *h = (WaveHeader *) _buffer;
  char *buffer = NULL;
  size_t blimit = 0;
  WaveFmtBody *f;
  WaveChunkHeader *c;
  u_int type, len;

  if (size < sizeof(WaveHeader))
    return -1;
  if (h->magic != WAV_RIFF || h->type != WAV_WAVE)
    return -1;
  if (size > sizeof(WaveHeader))
  {
    check_wavefile_space(buffer, size, size - sizeof(WaveHeader), blimit);
    memcpy(buffer, _buffer + sizeof(WaveHeader),
	   size - sizeof(WaveHeader));
  }
  size -= sizeof(WaveHeader);
  while (1)
  {
    check_wavefile_space(buffer, size, sizeof(WaveChunkHeader), blimit);
    test_wavefile_read(fd, buffer, &size, sizeof(WaveChunkHeader),
		       __LINE__);
    c = (WaveChunkHeader *) buffer;
    type = c->type;
    len = LE_INT(c->length);
    if (size > sizeof(WaveChunkHeader))
      memmove(buffer, buffer + sizeof(WaveChunkHeader),
	      size - sizeof(WaveChunkHeader));
    size -= sizeof(WaveChunkHeader);
    if (type == WAV_FMT)
      break;
    check_wavefile_space(buffer, size, len, blimit);
    test_wavefile_read(fd, buffer, &size, len, __LINE__);
    if (size > len)
      memmove(buffer, buffer + len, size - len);
    size -= len;
  }

  if (len < sizeof(WaveFmtBody))
  {
    error
      ("unknown length of 'fmt ' chunk (read %u, should be %u at least)",
       len, (u_int) sizeof(WaveFmtBody));
    exit(EXIT_FAILURE);
  }
  check_wavefile_space(buffer, size, len, blimit);
  test_wavefile_read(fd, buffer, &size, len, __LINE__);
  f = (WaveFmtBody *) buffer;
  if (LE_SHORT(f->format) != WAV_PCM_CODE)
  {
    error("can't play not PCM-coded WAVE-files");
    exit(EXIT_FAILURE);
  }
  if (LE_SHORT(f->modus) < 1)
  {
    error("can't play WAVE-files with %d tracks", LE_SHORT(f->modus));
    exit(EXIT_FAILURE);
  }
  hwparams.channels = LE_SHORT(f->modus);
  switch (LE_SHORT(f->bit_p_spl))
  {
  case 8:
    hwparams.format = SND_PCM_FORMAT_U8;
    break;
  case 16:
    hwparams.format = SND_PCM_FORMAT_S16_LE;
    break;
  case 24:
    switch (LE_SHORT(f->byte_p_spl) / hwparams.channels)
    {
    case 3:
      hwparams.format = SND_PCM_FORMAT_S24_3LE;
      break;
    case 4:
      hwparams.format = SND_PCM_FORMAT_S24_LE;
      break;
    default:
      error
	(" can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)",
	 LE_SHORT(f->bit_p_spl), LE_SHORT(f->byte_p_spl),
	 hwparams.channels);
      exit(EXIT_FAILURE);
    }
    break;
  case 32:
    hwparams.format = SND_PCM_FORMAT_S32_LE;
    break;
  default:
    error(" can't play WAVE-files with sample %d bits wide",
	  LE_SHORT(f->bit_p_spl));
    exit(EXIT_FAILURE);
  }
  hwparams.rate = LE_INT(f->sample_fq);

  if (size > len)
    memmove(buffer, buffer + len, size - len);
  size -= len;

  while (1)
  {
    u_int type, len;

    check_wavefile_space(buffer, size, sizeof(WaveChunkHeader), blimit);
    test_wavefile_read(fd, buffer, &size, sizeof(WaveChunkHeader),
		       __LINE__);
    c = (WaveChunkHeader *) buffer;
    type = c->type;
    len = LE_INT(c->length);
    if (size > sizeof(WaveChunkHeader))
      memmove(buffer, buffer + sizeof(WaveChunkHeader),
	      size - sizeof(WaveChunkHeader));
    size -= sizeof(WaveChunkHeader);
    if (type == WAV_DATA)
    {
      if (len < pbrec_count)
	pbrec_count = len;
      if (size > 0)
	memcpy(_buffer, buffer, size);
      free(buffer);
      return size;
    }
    check_wavefile_space(buffer, size, len, blimit);
    test_wavefile_read(fd, buffer, &size, len, __LINE__);
    if (size > len)
      memmove(buffer, buffer + len, size - len);
    size -= len;
  }

  /* shouldn't be reached */
#endif
  return -1;
}

/*

*/

int test_au(int fd, void *buffer)
{
#ifndef NO_ALSA
  AuHeader *ap = buffer;

  if (ap->magic != AU_MAGIC)
    return -1;
  if (BE_INT(ap->hdr_size) > 128 || BE_INT(ap->hdr_size) < 24)
    return -1;
  pbrec_count = BE_INT(ap->data_size);
  switch (BE_INT(ap->encoding))
  {
  case AU_FMT_ULAW:
    hwparams.format = SND_PCM_FORMAT_MU_LAW;
    break;
  case AU_FMT_LIN8:
    hwparams.format = SND_PCM_FORMAT_U8;
    break;
  case AU_FMT_LIN16:
    hwparams.format = SND_PCM_FORMAT_S16_BE;
    break;
  default:
    return -1;
  }
  hwparams.rate = BE_INT(ap->sample_rate);
  if (hwparams.rate < 2000 || hwparams.rate > 256000)
    return -1;
  hwparams.channels = BE_INT(ap->channels);
  if (hwparams.channels < 1 || hwparams.channels > 128)
    return -1;
  if ((size_t)
      safe_read(fd, (unsigned char*)buffer + sizeof(AuHeader),
		BE_INT(ap->hdr_size) - sizeof(AuHeader)) !=
      BE_INT(ap->hdr_size) - sizeof(AuHeader))
  {
    error("read error");
    exit(EXIT_FAILURE);
  }
#endif
  return 0;
}

void set_params(void)
{
#ifndef NO_ALSA
  snd_pcm_hw_params_t *params;
  snd_pcm_sw_params_t *swparams;
  snd_pcm_uframes_t buffer_size;
  int err;
  size_t n;
  snd_pcm_uframes_t xfer_align;
  unsigned int rate;
  snd_pcm_uframes_t start_threshold, stop_threshold;
  snd_pcm_hw_params_alloca(&params);
  snd_pcm_sw_params_alloca(&swparams);
  err = snd_pcm_hw_params_any(handle, params);
  if (err < 0)
  {
    error
      ("Broken configuration for this PCM: no configurations available");
    exit(EXIT_FAILURE);
  }
  if (mmap_flag)
  {
    snd_pcm_access_mask_t *mask = alloca(snd_pcm_access_mask_sizeof());
    snd_pcm_access_mask_none(mask);
    snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_INTERLEAVED);
    snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_NONINTERLEAVED);
    snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_COMPLEX);
    err = snd_pcm_hw_params_set_access_mask(handle, params, mask);
  }
  else if (interleaved)
    err = snd_pcm_hw_params_set_access(handle, params,
				       SND_PCM_ACCESS_RW_INTERLEAVED);
  else
    err = snd_pcm_hw_params_set_access(handle, params,
				       SND_PCM_ACCESS_RW_NONINTERLEAVED);
  if (err < 0)
  {
    error("Access type not available");
    exit(EXIT_FAILURE);
  }
  err = snd_pcm_hw_params_set_format(handle, params, hwparams.format);
  if (err < 0)
  {
    error("Sample format non available");
    exit(EXIT_FAILURE);
  }
  err = snd_pcm_hw_params_set_channels(handle, params, hwparams.channels);
  if (err < 0)
  {
    error("Channels count non available");
    exit(EXIT_FAILURE);
  }

#if 0
  err = snd_pcm_hw_params_set_periods_min(handle, params, 2);
  assert(err >= 0);
#endif
  rate = hwparams.rate;
  err = snd_pcm_hw_params_set_rate_near(handle, params, &hwparams.rate, 0);
  assert(err >= 0);
  if ((float) rate * 1.05 < hwparams.rate
      || (float) rate * 0.95 > hwparams.rate)
  {
    if (!quiet_mode)
    {
      fprintf(stderr,
	      "Warning: rate is not accurate (requested = %iHz, got = %iHz)\n",
	      rate, hwparams.rate);
      fprintf(stderr,
	      "         please, try the plug plugin (-Dplug:%s)\n",
	      snd_pcm_name(handle));
    }
  }
  rate = hwparams.rate;
  if (buffer_time == 0 && buffer_frames == 0)
    buffer_time = 500000;
  if (period_time == 0 && period_frames == 0)
  {
    if (buffer_time > 0)
      period_time = buffer_time / 4;
    else
      period_frames = buffer_frames / 4;
  }
  if (period_time > 0)
    err = snd_pcm_hw_params_set_period_time_near(handle, params,
						 &period_time, 0);
  else
    err = snd_pcm_hw_params_set_period_size_near(handle, params,
						 &period_frames, 0);
  assert(err >= 0);
  if (buffer_time > 0)
  {
    err = snd_pcm_hw_params_set_buffer_time_near(handle, params,
						 &buffer_time, 0);
  }
  else
  {
    err = snd_pcm_hw_params_set_buffer_size_near(handle, params,
						 &buffer_frames);
  }
  assert(err >= 0);
  err = snd_pcm_hw_params(handle, params);
  if (err < 0)
  {
    error("Unable to install hw params:");
    snd_pcm_hw_params_dump(params, mylog);
    exit(EXIT_FAILURE);
  }
#ifdef DEBUG
  pr("dans set_params");
  endl;
#endif
  /*printf("chunksize=%hu",(unsigned short)chunk_size);endl; */
  snd_pcm_hw_params_get_period_size(params, &chunk_size, 0);
  snd_pcm_hw_params_get_buffer_size(params, &buffer_size);
  if (chunk_size == buffer_size)
  {
    error("Can't use period equal to buffer size (%lu == %lu)",
	  chunk_size, buffer_size);
    exit(EXIT_FAILURE);
  }
  snd_pcm_sw_params_current(handle, swparams);
  err = snd_pcm_sw_params_get_xfer_align(swparams, &xfer_align);
  if (err < 0)
  {
    error("Unable to obtain xfer align\n");
    exit(EXIT_FAILURE);
  }
  if (sleep_min)
    xfer_align = 1;
  err = snd_pcm_sw_params_set_sleep_min(handle, swparams, sleep_min);
  assert(err >= 0);
  if (avail_min < 0)
    n = chunk_size;
  else
    n = (double) rate *avail_min / 1000000;
  err = snd_pcm_sw_params_set_avail_min(handle, swparams, n);

  /* round up to closest transfer boundary */
  n = (buffer_size / xfer_align) * xfer_align;
  if (start_delay <= 0)
  {
    start_threshold = n + (double) rate *start_delay / 1000000;
  }
  else
    start_threshold = (double) rate *start_delay / 1000000;
  if (start_threshold < 1)
    start_threshold = 1;
  if (start_threshold > n)
    start_threshold = n;
  err =
    snd_pcm_sw_params_set_start_threshold(handle, swparams,
					  start_threshold);
  assert(err >= 0);
  if (stop_delay <= 0)
    stop_threshold = buffer_size + (double) rate *stop_delay / 1000000;
  else
    stop_threshold = (double) rate *stop_delay / 1000000;
  err =
    snd_pcm_sw_params_set_stop_threshold(handle, swparams,
					 stop_threshold);
  assert(err >= 0);

  err = snd_pcm_sw_params_set_xfer_align(handle, swparams, xfer_align);
  assert(err >= 0);
  if (snd_pcm_sw_params(handle, swparams) < 0)
  {
    error("unable to install sw params:");
    snd_pcm_sw_params_dump(swparams, mylog);
    exit(EXIT_FAILURE);
  }

  if (verbose)
    snd_pcm_dump(handle, mylog);

  bits_per_sample = snd_pcm_format_physical_width(hwparams.format);
  bits_per_frame = bits_per_sample * hwparams.channels;


  chunk_bytes = chunk_size * bits_per_frame / 8;

  audiobuf = realloc(audiobuf, chunk_bytes);
  if (audiobuf == NULL)
  {
    error("not enough memory");
    exit(EXIT_FAILURE);
  }
  /* fprintf(stderr, "real chunk_size = %i, frags = %i, total = %i\n", chunk_size, setup.buf.block.frags, setup.buf.block.frags * chunk_size); */
#endif
}

#ifndef timersub
#define	timersub(a, b, result)				\
  do {							\
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;	\
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;	\
    if ((result)->tv_usec < 0) {			\
      --(result)->tv_sec;				\
      (result)->tv_usec += 1000000;			\
    }							\
  } while (0)
#endif

/* I/O error handler */
void xrun(void)
{
#ifndef NO_ALSA
  snd_pcm_status_t *status;
  int res;

  snd_pcm_status_alloca(&status);
  if ((res = snd_pcm_status(handle, status)) < 0)
  {
    error("status error: %s", snd_strerror(res));
    exit(EXIT_FAILURE);
  }
  if (snd_pcm_status_get_state(status) == SND_PCM_STATE_XRUN)
  {
    struct timeval now, diff, tstamp;
    gettimeofday(&now, 0);
    snd_pcm_status_get_trigger_tstamp(status, &tstamp);
    timersub(&now, &tstamp, &diff);
    fprintf(stderr, "%s!!! (at least %.3f ms long)\n",
	    stream == SND_PCM_STREAM_PLAYBACK ? "underrun" : "overrun",
	    diff.tv_sec * 1000 + diff.tv_usec / 1000.0);
    if (verbose)
    {
      fprintf(stderr, "Status:\n");
      snd_pcm_status_dump(status, mylog);
    }
    if ((res = snd_pcm_prepare(handle)) < 0)
    {
      error("xrun: prepare error: %s", snd_strerror(res));
      exit(EXIT_FAILURE);
    }
    return;                 /* ok, data should be accepted again */
  }
  if (snd_pcm_status_get_state(status) == SND_PCM_STATE_DRAINING)
  {
    if (verbose)
    {
      fprintf(stderr, "Status(DRAINING):\n");
      snd_pcm_status_dump(status, mylog);
    }
    if (stream == SND_PCM_STREAM_CAPTURE)
    {
      fprintf(stderr,
	      "capture stream format change? attempting recover...\n");
      if ((res = snd_pcm_prepare(handle)) < 0)
      {
	error("xrun(DRAINING): prepare error: %s", snd_strerror(res));
	exit(EXIT_FAILURE);
      }
      return;
    }
  }
  if (verbose)
  {
    fprintf(stderr, "Status(R/W):\n");
    snd_pcm_status_dump(status, mylog);
  }
  error("read/write error, state = %s",
	snd_pcm_state_name(snd_pcm_status_get_state(status)));
#endif
  exit(EXIT_FAILURE);
}

/* I/O suspend handler */
void suspend(void)
{
#ifndef NO_ALSA
  int res;

  if (!quiet_mode)
    fprintf(stderr, "Suspended. Trying resume. ");
  fflush(stderr);
  while ((res = snd_pcm_resume(handle)) == -EAGAIN)
    sleep(1);               /* wait until suspend flag is released */
  if (res < 0)
  {
    if (!quiet_mode)
      fprintf(stderr, "Failed. Restarting stream. ");
    fflush(stderr);
    if ((res = snd_pcm_prepare(handle)) < 0)
    {
      error("suspend: prepare error: %s", snd_strerror(res));
      exit(EXIT_FAILURE);
    }
  }
  if (!quiet_mode)
    fprintf(stderr, "Done.\n");
#endif
}

/* peak handler */
void compute_max_peak(u_char * data, size_t count)
{
#ifndef NO_ALSA
  signed int val, max, max_peak = 0, perc;
  size_t step, ocount = count;

  while (count-- > 0)
  {
    switch (bits_per_sample)
    {
    case 8:
      val =
	*(signed char *) data ^ snd_pcm_format_silence(hwparams.
						       format);
      step = 1;
      break;
    case 16:
      val =
	*(signed short *) data ^ snd_pcm_format_silence_16(hwparams.
							   format);
      step = 2;
      break;
    case 32:
      val =
	*(signed int *) data ^ snd_pcm_format_silence_32(hwparams.
							 format);
      step = 4;
      break;
    default:
      val = 0;
      step = 1;
      break;
    }
    data += step;
    val = abs(val);
    if (max_peak < val)
      max_peak = val;
  }
  max = 1 << (bits_per_sample - 1);
  if (max <= 0)
    max = 0x7fffffff;
  printf("Max peak (%li samples): %05i (0x%04x) ", (long) ocount, max_peak,
	 max_peak);
  if (bits_per_sample > 16)
    perc = max_peak / (max / 100);
  else
    perc = max_peak * 100 / max;
  for (val = 0; val < 20; val++)
    if (val <= perc / 5)
      putc('#', stdout);
    else
      putc(' ', stdout);
  printf(" %i%%\n", perc);
#endif
}

/*
 *  write function
 */

ssize_t pcm_write(u_char * data, size_t count)
{
#ifndef NO_ALSA
  ssize_t r;
  ssize_t result = 0;

  if (sleep_min == 0 && count < chunk_size)
  {
    snd_pcm_format_set_silence(hwparams.format,
			       data + count * bits_per_frame / 8,
			       (chunk_size - count) * hwparams.channels);
    count = chunk_size;
  }
  while (count > 0)
  {
    r = writei_func(handle, data, count);
    if (r == -EAGAIN || (r >= 0 && (size_t) r < count))
    {
      snd_pcm_wait(handle, 1000);
    }
    else if (r == -EPIPE)
    {
      xrun();
    }
    else if (r == -ESTRPIPE)
    {
      suspend();
    }
    else if (r < 0)
    {
      error("write error: %s", snd_strerror(r));
      exit(EXIT_FAILURE);
    }
    if (r > 0)
    {
      if (verbose > 1)
	compute_max_peak(data, r * hwparams.channels);
      result += r;
      count -= r;
      data += r * bits_per_frame / 8;
    }
  }
  return result;
#endif
}

ssize_t pcm_writev(u_char ** data, unsigned int channels, size_t count)
{
#ifndef NO_ALSA
  ssize_t r;
  size_t result = 0;
/*  void *bufs[channels]; // berk !*/    
  void **bufs = NULL;
  bufs=MANY_ALLOCATIONS(channels,void*);

  if (sleep_min == 0 && count != chunk_size)
  {
    unsigned int channel;
    size_t offset = count;
    size_t remaining = chunk_size - count;
    for (channel = 0; channel < channels; channel++)
      snd_pcm_format_set_silence(hwparams.format,
				 data[channel] +
				 offset * bits_per_sample / 8,
				 remaining);
    count = chunk_size;
  }
  while (count > 0)
  {
    unsigned int channel;
    size_t offset = result;
    for (channel = 0; channel < channels; channel++)
      bufs[channel] = data[channel] + offset * bits_per_sample / 8;
    r = writen_func(handle, bufs, count);
    if (r == -EAGAIN || (r >= 0 && (size_t) r < count))
    {
      snd_pcm_wait(handle, 1000);
    }
    else if (r == -EPIPE)
    {
      xrun();
    }
    else if (r == -ESTRPIPE)
    {
      suspend();
    }
    else if (r < 0)
    {
      error("writev error: %s", snd_strerror(r));
      exit(EXIT_FAILURE);
    }
    if (r > 0)
    {
      if (verbose > 1)
      {
	for (channel = 0; channel < channels; channel++)
	  compute_max_peak(data[channel], r);
      }
      result += r;
      count -= r;
    }
  }

  if(bufs!=NULL)
    free(bufs);
  return result;
#endif
}

/*
 *  read function
 */

ssize_t pcm_read(u_char * data, size_t rcount)
{
#ifndef NO_ALSA
  ssize_t r;
  size_t result = 0;
  size_t count = rcount;

  if (sleep_min == 0 && count != chunk_size)
  {
    count = chunk_size;
  }

  while (count > 0)
  {
    r = readi_func(handle, data, count);
    if (r == -EAGAIN || (r >= 0 && (size_t) r < count))
    {
      snd_pcm_wait(handle, 1000);
    }
    else if (r == -EPIPE)
    {
      xrun();
    }
    else if (r == -ESTRPIPE)
    {
      suspend();
    }
    else if (r < 0)
    {
      error("read error: %s", snd_strerror(r));
      exit(EXIT_FAILURE);
    }
    if (r > 0)
    {
      if (verbose > 1)
	compute_max_peak(data, r * hwparams.channels);
      result += r;
      count -= r;
      data += r * bits_per_frame / 8;
    }
  }
  return rcount;
#endif
}

ssize_t pcm_readv(u_char ** data, unsigned int channels, size_t rcount)
{
#ifndef NO_ALSA
  ssize_t r;
  size_t result = 0;
  size_t count = rcount;
/*  void *bufs[channels]; // berk !*/    
  void **bufs = NULL;
  bufs=MANY_ALLOCATIONS(channels,void*);

  if (sleep_min == 0 && count != chunk_size)
  {
    count = chunk_size;
  }

  while (count > 0)
  {
    unsigned int channel;
    size_t offset = result;


    for (channel = 0; channel < channels; channel++)
      bufs[channel] = data[channel] + offset * bits_per_sample / 8;
    r = readn_func(handle, bufs, count);
    if (r == -EAGAIN || (r >= 0 && (size_t) r < count))
    {
      snd_pcm_wait(handle, 1000);
    }
    else if (r == -EPIPE)
    {
      xrun();
    }
    else if (r == -ESTRPIPE)
    {
      suspend();
    }
    else if (r < 0)
    {
      error("readv error: %s", snd_strerror(r));
      exit(EXIT_FAILURE);
    }
    if (r > 0)
    {
      if (verbose > 1)
      {
	for (channel = 0; channel < channels; channel++)
	  compute_max_peak(data[channel], r);
      }
      result += r;
      count -= r;
    }
  }

  if(bufs!=NULL)
    free(bufs);
  return rcount;
#endif
}

/*
 *  ok, let's play a .voc file
 */

ssize_t voc_pcm_write(u_char * data, size_t count)
{
#ifndef NO_ALSA
  ssize_t result = count, r;
  size_t size;

  while (count > 0)
  {
    size = count;
    if (size > chunk_bytes - buffer_pos)
      size = chunk_bytes - buffer_pos;
    memcpy(audiobuf + buffer_pos, data, size);
    data += size;
    count -= size;
    buffer_pos += size;
    if ((size_t) buffer_pos == chunk_bytes)
    {
      if ((size_t) (r = pcm_write(audiobuf, chunk_size)) != chunk_size)
	return r;
      buffer_pos = 0;
    }
  }
  return result;
#endif
}

void voc_write_silence(unsigned x)
{
#ifndef NO_ALSA
  unsigned l;
  char *buf;

  buf = (char *) malloc(chunk_bytes);
  if (buf == NULL)
  {
    error("can't allocate buffer for silence");
    return;                 /* not fatal error */
  }
  snd_pcm_format_set_silence(hwparams.format, buf,
			     chunk_size * hwparams.channels);
  while (x > 0)
  {
    l = x;
    if (l > chunk_size)
      l = chunk_size;
    if (voc_pcm_write((u_char*)buf, l) != (ssize_t) l)
    {
      error("write error");
      exit(EXIT_FAILURE);
    }
    x -= l;
  }
#endif
}

void voc_pcm_flush(void)
{
#ifndef NO_ALSA
  if (buffer_pos > 0)
  {
    size_t b;
    if (sleep_min == 0)
    {
      if (snd_pcm_format_set_silence
	  (hwparams.format, audiobuf + buffer_pos,
	   chunk_bytes - buffer_pos * 8 / bits_per_sample) < 0)
	fprintf(stderr, "voc_pcm_flush - silence error");
      b = chunk_size;
    }
    else
    {
      b = buffer_pos * 8 / bits_per_frame;
    }
    if (pcm_write(audiobuf, b) != (ssize_t) b)
      error("voc_pcm_flush error");
  }
  snd_pcm_drain(handle);
#endif
}

void voc_play(int fd, int ofs, char *name)
{
#ifndef NO_ALSA
  int l;
  VocBlockType *bp;
  VocVoiceData *vd;
  VocExtBlock *eb;
  size_t nextblock, in_buffer;
  u_char *data, *buf;
  char was_extended = 0, output = 0;
  u_short *sp, repeat = 0;
  size_t silence;
  off64_t filepos = 0;

#define COUNT(x)	nextblock -= x; in_buffer -= x; data += x
#define COUNT1(x)	in_buffer -= x; data += x

  data = buf = (u_char *) malloc(64 * 1024);
  buffer_pos = 0;
  if (data == NULL)
  {
    error("malloc error");
    exit(EXIT_FAILURE);
  }
  if (!quiet_mode)
  {
    fprintf(stderr, "Playing Creative Labs Channel file '%s'...\n", name);
  }
  /* first we waste the rest of header, ugly but we don't need seek */
  while (ofs > (ssize_t) chunk_bytes)
  {
    if ((size_t) safe_read(fd, buf, chunk_bytes) != chunk_bytes)
    {
      error("read error");
      exit(EXIT_FAILURE);
    }
    ofs -= chunk_bytes;
  }
  if (ofs)
  {
    if (safe_read(fd, buf, ofs) != ofs)
    {
      error("read error");
      exit(EXIT_FAILURE);
    }
  }
  hwparams.format = SND_PCM_FORMAT_U8;
  hwparams.channels = 1;
  hwparams.rate = DEFAULT_SPEED;
  set_params();

  in_buffer = nextblock = 0;
  while (1)
  {
  Fill_the_buffer:         /* need this for repeat */
    if (in_buffer < 32)
    {
      /* move the rest of buffer to pos 0 and fill the buf up */
      if (in_buffer)
	memcpy(buf, data, in_buffer);
      data = buf;
      if ((l =
	   safe_read(fd, buf + in_buffer, chunk_bytes - in_buffer)) > 0)
	in_buffer += l;
      else if (!in_buffer)
      {
	/* the file is truncated, so simulate 'Terminator' 
	   and reduce the datablock for safe landing */
	nextblock = buf[0] = 0;
	if (l == -1)
	{
	  perror(name);
	  exit(EXIT_FAILURE);
	}
      }
    }
    while (!nextblock)
    {                       /* this is a new block */
      if (in_buffer < sizeof(VocBlockType))
	goto __end;
      bp = (VocBlockType *) data;
      COUNT1(sizeof(VocBlockType));
      nextblock = VOC_DATALEN(bp);
      if (output && !quiet_mode)
	fprintf(stderr, "\n");  /* write /n after ASCII-out */
      output = 0;
      switch (bp->type)
      {
      case 0:
#if 0
	d_printf("Terminator\n");
#endif
	return;         /* VOC-file stop */
      case 1:
	vd = (VocVoiceData *) data;
	COUNT1(sizeof(VocVoiceData));
	/* we need a SYNC, before we can set new SPEED, STEREO ... */

	if (!was_extended)
	{
	  hwparams.rate = (int) (vd->tc);
	  hwparams.rate = 1000000 / (256 - hwparams.rate);
#if 0
	  d_printf("Channel data %d Hz\n", dsp_speed);
#endif
	  if (vd->pack)
	  {           /* /dev/dsp can't it */
	    error("can't play packed .voc files");
	    return;
	  }
	  if (hwparams.channels == 2) /* if we are in Stereo-Mode, switch back */
	    hwparams.channels = 1;
	}
	else
	{               /* there was extended block */
	  hwparams.channels = 2;
	  was_extended = 0;
	}
	set_params();
	break;
      case 2:            /* nothing to do, pure data */
#if 0
	d_printf("Channel continuation\n");
#endif
	break;
      case 3:            /* a silence block, no data, only a count */
	sp = (u_short *) data;
	COUNT1(sizeof(u_short));
	hwparams.rate = (int) (*data);
	COUNT1(1);
	hwparams.rate = 1000000 / (256 - hwparams.rate);
	set_params();
	silence = (((size_t) * sp) * 1000) / hwparams.rate;
#if 0
	d_printf("Silence for %d ms\n", (int) silence);
#endif
	voc_write_silence(*sp);
	break;
      case 4:            /* a marker for syncronisation, no effect */
	sp = (u_short *) data;
	COUNT1(sizeof(u_short));
#if 0
	d_printf("Marker %d\n", *sp);
#endif
	break;
      case 5:            /* ASCII text, we copy to stderr */
	output = 1;
#if 0
	d_printf("ASCII - text :\n");
#endif
	break;
      case 6:            /* repeat marker, says repeatcount */
	/* my specs don't say it: maybe this can be recursive, but
	   I don't think somebody use it */
	repeat = *(u_short *) data;
	COUNT1(sizeof(u_short));
#if 0
	d_printf("Repeat loop %d times\n", repeat);
#endif
	if (filepos >= 0)
	{               /* if < 0, one seek fails, why test another */
	  if ((filepos = lseek64(fd, 0, 1)) < 0)
	  {
	    error("can't play loops; %s isn't seekable\n", name);
	    repeat = 0;
	  }
	  else
	  {
	    filepos -= in_buffer;   /* set filepos after repeat */
	  }
	}
	else
	{
	  repeat = 0;
	}
	break;
      case 7:            /* ok, lets repeat that be rewinding tape */
	if (repeat)
	{
	  if (repeat != 0xFFFF)
	  {
#if 0
	    d_printf("Repeat loop %d\n", repeat);
#endif
	    --repeat;
	  }
#if 0
	  else
	    d_printf("Neverending loop\n");
#endif
	  lseek64(fd, filepos, 0);
	  in_buffer = 0;  /* clear the buffer */
	  goto Fill_the_buffer;
	}
#if 0
	else
	  d_printf("End repeat loop\n");
#endif
	break;
      case 8:            /* the extension to play Stereo, I have SB 1.0 :-( */
	was_extended = 1;
	eb = (VocExtBlock *) data;
	COUNT1(sizeof(VocExtBlock));
	hwparams.rate = (int) (eb->tc);
	hwparams.rate = 256000000L / (65536 - hwparams.rate);
	hwparams.channels = eb->mode == VOC_MODE_STEREO ? 2 : 1;
	if (hwparams.channels == 2)
	  hwparams.rate = hwparams.rate >> 1;
	if (eb->pack)
	{               /* /dev/dsp can't it */
	  error("can't play packed .voc files");
	  return;
	}
#if 0
	d_printf("Extended block %s %d Hz\n",
		 (eb->mode ? "Stereo" : "Mono"), dsp_speed);
#endif
	break;
      default:
	error("unknown blocktype %d. terminate.", bp->type);
	return;
      }                   /* switch (bp->type) */
    }                       /* while (! nextblock)  */
    /* put nextblock data bytes to dsp */
    l = in_buffer;
    if (nextblock < (size_t) l)
      l = nextblock;
    if (l)
    {
      if (output && !quiet_mode)
      {
	if (write(2, data, l) != l)
	{               /* to stderr */
	  error("write error");
	  exit(EXIT_FAILURE);
	}
      }
      else
      {
	if (voc_pcm_write(data, l) != l)
	{
	  error("write error");
	  exit(EXIT_FAILURE);
	}
      }
      COUNT(l);
    }
  }                           /* while(1) */
__end:
  voc_pcm_flush();
  free(buf);
#endif
}

/* that was a big one, perhaps somebody split it :-) */

/* setting the globals for playing raw data */
void init_raw_data(void)
{
  hwparams = rhwparams;
}

/* calculate the data count to read from/to dsp */
off64_t calc_count(void)
{
#ifndef NO_ALSA
  off64_t count;

  if (!timelimit && pbrec_count == (off64_t)( - 1))
  {
    count = (off64_t) - 1;
  }
  else
  {
    if (timelimit == 0)
    {
      count = pbrec_count;
    }
    else
    {
      count =
	snd_pcm_format_size(hwparams.format,
			    hwparams.rate * hwparams.channels);
      count *= (off64_t) timelimit;
    }
  }
  return count < pbrec_count ? count : pbrec_count;
#endif
}

/* write a .VOC-header */
void begin_voc(int fd, size_t cnt)
{
#ifndef NO_ALSA
  VocHeader vh;
  VocBlockType bt;
  VocVoiceData vd;
  VocExtBlock eb;

  strncpy((char*)vh.magic, VOC_MAGIC_STRING, 20);
  vh.magic[19] = 0x1A;
  vh.headerlen = sizeof(VocHeader);
  vh.version = VOC_ACTUAL_VERSION;
  vh.coded_ver = 0x1233 - VOC_ACTUAL_VERSION;

  if (write(fd, &vh, sizeof(VocHeader)) != sizeof(VocHeader))
  {
    error("write error");
    exit(EXIT_FAILURE);
  }
  if (hwparams.channels > 1)
  {
    /* write an extended block */
    bt.type = 8;
    bt.datalen = 4;
    bt.datalen_m = bt.datalen_h = 0;
    if (write(fd, &bt, sizeof(VocBlockType)) != sizeof(VocBlockType))
    {
      error("write error");
      exit(EXIT_FAILURE);
    }
    eb.tc = (u_short) (65536 - 256000000L / (hwparams.rate << 1));
    eb.pack = 0;
    eb.mode = 1;
    if (write(fd, &eb, sizeof(VocExtBlock)) != sizeof(VocExtBlock))
    {
      error("write error");
      exit(EXIT_FAILURE);
    }
  }
  bt.type = 1;
  cnt += sizeof(VocVoiceData);    /* Channel_data block follows */
  bt.datalen = (u_char) (cnt & 0xFF);
  bt.datalen_m = (u_char) ((cnt & 0xFF00) >> 8);
  bt.datalen_h = (u_char) ((cnt & 0xFF0000) >> 16);
  if (write(fd, &bt, sizeof(VocBlockType)) != sizeof(VocBlockType))
  {
    error("write error");
    exit(EXIT_FAILURE);
  }
  vd.tc = (u_char) (256 - (1000000 / hwparams.rate));
  vd.pack = 0;
  if (write(fd, &vd, sizeof(VocVoiceData)) != sizeof(VocVoiceData))
  {
    error("write error");
    exit(EXIT_FAILURE);
  }
#endif
}

/* write a WAVE-header */
void begin_wave(int fd, size_t cnt)
{
#ifndef NO_ALSA
  WaveHeader h;
  WaveFmtBody f;
  WaveChunkHeader cf, cd;
  int bits;
  u_int tmp;
  u_short tmp2;

  /* WAVE cannot handle greater than 32bit (signed?) int */

  if (cnt == (size_t) - 2)
    cnt = 0x7fffff00;

  bits = 8;
  switch ((unsigned long) hwparams.format)
  {
  case SND_PCM_FORMAT_U8:
    bits = 8;
    break;
  case SND_PCM_FORMAT_S16_LE:
    bits = 16;
    break;
  case SND_PCM_FORMAT_S32_LE:
    bits = 32;
    break;
  case SND_PCM_FORMAT_S24_LE:
  case SND_PCM_FORMAT_S24_3LE:
    bits = 24;
    break;
  default:
    error("Wave doesn't support %s format...",
	  snd_pcm_format_name(hwparams.format));
    exit(EXIT_FAILURE);
  }

  h.magic = WAV_RIFF;
  tmp =
    cnt + sizeof(WaveHeader) + sizeof(WaveChunkHeader) +
    sizeof(WaveFmtBody) + sizeof(WaveChunkHeader) - 8;
  h.length = LE_INT(tmp);
  printf("cnt=%d\n", (u_int) cnt);
  h.type = WAV_WAVE;
  printf("h.length=%d", h.length);
  endl;
  cf.type = WAV_FMT;
  cf.length = LE_INT(16);
  printf("cf.length=%d", cf.length);
  endl;
  f.format = LE_SHORT(WAV_PCM_CODE);
  f.modus = LE_SHORT(hwparams.channels);
  f.sample_fq = LE_INT(hwparams.rate);
#if 0
  tmp2 = (samplesize == 8) ? 1 : 2;
  f.byte_p_spl = LE_SHORT(tmp2);
  tmp = dsp_speed * hwpa__attribute__((unused) rams.channels * (u_int) tmp2;
#else
				      tmp2 =
				      hwparams.channels * snd_pcm_format_physical_width(hwparams.format) /
				      8;
				      f.byte_p_spl = LE_SHORT(tmp2);
				      tmp = (u_int) tmp2 *hwparams.rate;
#endif
				      f.byte_p_sec = LE_INT(tmp);
				      f.bit_p_spl = LE_SHORT(bits);

				      cd.type = WAV_DATA;
				      cd.length = LE_INT(cnt);


				      printf("********cd.length=%d", cd.length);
				      if (write(fd, &h, sizeof(WaveHeader)) != sizeof(WaveHeader) ||
					  write(fd, &cf, sizeof(WaveChunkHeader)) != sizeof(WaveChunkHeader) ||
					  write(fd, &f, sizeof(WaveFmtBody)) != sizeof(WaveFmtBody) ||
					  write(fd, &cd, sizeof(WaveChunkHeader)) != sizeof(WaveChunkHeader))
				      {
					error("write error");
					exit(EXIT_FAILURE);
				      }
#endif
				      }

/* write a Au-header */
    void begin_au(int fd, size_t cnt)
    {
#ifndef NO_ALSA
      AuHeader ah;

      ah.magic = AU_MAGIC;
      ah.hdr_size = BE_INT(24);
      ah.data_size = BE_INT(cnt);
      switch ((unsigned long) hwparams.format)
      {
      case SND_PCM_FORMAT_MU_LAW:
        ah.encoding = BE_INT(AU_FMT_ULAW);
        break;
      case SND_PCM_FORMAT_U8:
        ah.encoding = BE_INT(AU_FMT_LIN8);
        break;
      case SND_PCM_FORMAT_S16_BE:
        ah.encoding = BE_INT(AU_FMT_LIN16);
        break;
      default:
        error("Sparc Audio doesn't support %s format...",
              snd_pcm_format_name(hwparams.format));
        exit(EXIT_FAILURE);
      }
      ah.sample_rate = BE_INT(hwparams.rate);
      ah.channels = BE_INT(hwparams.channels);
      if (write(fd, &ah, sizeof(AuHeader)) != sizeof(AuHeader))
      {
        error("write error");
        exit(EXIT_FAILURE);
      }
#endif
    }

/* closing .VOC */
  void end_voc(int fd)
  {
#ifndef NO_ALSA
    off64_t length_seek;
    VocBlockType bt;
    size_t cnt;
    char dummy = 0;             /* Write a Terminator */
    int ret=-1;

    if (write(fd, &dummy, 1) != 1)
    {
      error("write error");
      exit(EXIT_FAILURE);
    }
    length_seek = sizeof(VocHeader);
    if (hwparams.channels > 1)
      length_seek += sizeof(VocBlockType) + sizeof(VocExtBlock);
    bt.type = 1;
    cnt = fdcount;
    cnt += sizeof(VocVoiceData);    /* Channel_data block follows */
    if (cnt > 0x00ffffff)
      cnt = 0x00ffffff;
    bt.datalen = (u_char) (cnt & 0xFF);
    bt.datalen_m = (u_char) ((cnt & 0xFF00) >> 8);
    bt.datalen_h = (u_char) ((cnt & 0xFF0000) >> 16);
    if (lseek64(fd, length_seek, SEEK_SET) == length_seek)
      ret=write(fd, &bt, sizeof(VocBlockType));
    if (fd != 1)
      close(fd);
#endif
  }

  void end_raw(int fd)
  {                               /* REALLY only close output */
    if (fd != 1)
      close(fd);
  }

  void end_wave(int fd)
  {                               /* only close output */
#ifndef NO_ALSA
    WaveChunkHeader cd;
    off64_t length_seek;
    u_int rifflen;
    int ret=-1;
    printf("end wave\n");
    length_seek = sizeof(WaveHeader) +
      sizeof(WaveChunkHeader) + sizeof(WaveFmtBody);
    cd.type = WAV_DATA;
    cd.length = fdcount > 0x7fffffff ? 0x7fffffff : LE_INT(fdcount);
    rifflen = fdcount + 2 * sizeof(WaveChunkHeader) + sizeof(WaveFmtBody) + 4;
    rifflen = rifflen > 0x7fffffff ? 0x7fffffff : LE_INT(rifflen);

    if (lseek64(fd, 4, SEEK_SET) == 4)
      ret=write(fd, &rifflen, 4);


    if (lseek64(fd, length_seek, SEEK_SET) == length_seek)
      ret=write(fd, &cd, sizeof(WaveChunkHeader));


    if (fd != 1)
      close(fd);
#endif
  }

  void end_au(int fd)
  {                               /* only close output */
#ifndef NO_ALSA
    AuHeader ah;
    off64_t length_seek;
    int ret=-1;

    length_seek = (char *) &ah.data_size - (char *) &ah;
    ah.data_size = fdcount > 0xffffffff ? 0xffffffff : BE_INT(fdcount);
    if (lseek64(fd, length_seek, SEEK_SET) == length_seek)
      ret=write(fd, &ah.data_size, sizeof(ah.data_size));
    if (fd != 1)
      close(fd);
#endif
  }

  void header(int rtype, char *name)
  {
#ifndef NO_ALSA
    if (!quiet_mode)
    {
      fprintf(stderr, "%s %s '%s' : ",
	      (stream == SND_PCM_STREAM_PLAYBACK) ? "Playing" : "Recording",
	      fmt_rec_table[rtype].what, name);
      fprintf(stderr, "%s, ", snd_pcm_format_description(hwparams.format));
      fprintf(stderr, "Rate %d Hz, ", hwparams.rate);
      if (hwparams.channels == 1)
	fprintf(stderr, "Mono");
      else if (hwparams.channels == 2)
	fprintf(stderr, "Stereo");
      else
	fprintf(stderr, "Channels %i", hwparams.channels);
      fprintf(stderr, "\n");
    }
#endif
  }

/* playing raw data */

  void playback_go(int fd, size_t loaded, size_t count, int rtype, char *name)
  {
#ifndef NO_ALSA
    int l, r;
    size_t written = 0;
    size_t c;

    header(rtype, name);
    set_params();

    while (loaded > chunk_bytes && written < count)
    {
      if (pcm_write(audiobuf + written, chunk_size) <= 0)
	return;
      written += chunk_bytes;
      loaded -= chunk_bytes;
    }
    if (written > 0 && loaded > 0)
      memmove(audiobuf, audiobuf + written, loaded);

    l = loaded;
    while (written < count)
    {
      do
      {
	c = count - written;
	if (c > chunk_bytes)
	  c = chunk_bytes;
	c -= l;

	if (c == 0)
	  break;
	r = safe_read(fd, audiobuf + l, c);
	if (r < 0)
	{
	  perror(name);
	  exit(EXIT_FAILURE);
	}
	fdcount += r;
	if (r == 0)
	  break;
	l += r;
      }
      while (sleep_min == 0 && (size_t) l < chunk_bytes);
      l = l * 8 / bits_per_frame;
      r = pcm_write(audiobuf, l);
      if (r != l)
	break;
      r = r * bits_per_frame / 8;
      written += r;
      l = 0;
    }
    snd_pcm_drain(handle);
#endif
  }

/* capturing raw data, this proc handels WAVE files and .VOCs (as one block) */

  void capture_go(int fd, size_t count, int rtype, char *name)
  {
#ifndef NO_ALSA
    header(rtype, name);
    printf("rtype %d, name %s\n", rtype, name);
    endl;
    set_params();
    /*buf = realloc(buf, chunk_bytes); removed from input parameters
     * of the function add it back if needed*/
    if (write_dat == 1)
    {
      if ((f = fopen("dat", "w+")) == NULL)
      {
	perror("dat file open error\n");
	exit(-1);
      }

      if (fclose(f) == EOF)
      {
	perror("dat file closing error\n");
	exit(-1);
      }
    }
#ifdef DEBUG
    pr("dans capture_go");
    endl;
    printf(" fd=%d,count =%d,rtype=%d,name=%s", fd, count, rtype, name);
    endl;
#else
    (void)fd;
#endif
    pbrec_count = count;
    mrtype = rtype;
    cur = pbrec_count;
#endif
  }

  void capture_end()
  {
#ifndef NO_ALSA
    if (write_wav == 1)
    {
      fmt_rec_table[file_type].end(fd);
      printf("closing file %s", name);
      endl;
      printf("which type is %d", file_type);
    }
#endif
  }
  void getbuf(char *buf)
  {
#ifndef NO_ALSA
    size_t c;
    ssize_t r, err = 0;

    if (write_dat == 1)
    {
      if ((f = fopen("dat", "a")) == NULL)
      {
	perror("dat file open error\n");
	exit(-1);
      }
    }
    c = cur;

    if (c > chunk_bytes)

      c = chunk_bytes;

    c = c * 8 / bits_per_frame;
    if (buf == NULL)
    {

      perror("buffer not initialized");
      exit(-1);

    }
    if ((size_t) (r = pcm_read((u_char*)buf, c)) != c)
      perror("audio reading error\n");
    endl;
    r = r * bits_per_frame / 8;
    if (write_dat == 1)
    {
      saveto(f, buf);
    }
    if (write_wav == 1)
    {
      if ((err = write(fd, buf, r)) != r)
      {
	perror("wav writing error\n");
	exit(EXIT_FAILURE);
      }
    }
    if (err > 0)
      fdcount += err;
    if (write_dat == 1)
    {
      if (fclose(f) == EOF)
      {
	perror("dat file closing error\n");
	exit(-1);
      }
    }

    cur -= r;
#endif

  }


/*
 *  let's play or capture it (capture_type says VOC/WAVE/raw)
 */

  void playback(char *name)
  {
#ifndef NO_ALSA
    int ofs;
    size_t dta;
    ssize_t dtawave;

    pbrec_count = (size_t) - 1;
    fdcount = 0;
    if (!name || !strcmp(name, "-"))
    {
      fd = fileno(stdin);
      /** warning : no check of size of name buffer */
      strncpy(name,"stdin",5);
      name[5]='\0';
    }
    else
    {
      if ((fd = open64(name, O_RDONLY, 0)) == -1)
      {
	perror(name);
	exit(EXIT_FAILURE);
      }
    }
    /* read the file header */
    dta = sizeof(AuHeader);
    if ((size_t) safe_read(fd, audiobuf, dta) != dta)
    {
      error("read error");
      exit(EXIT_FAILURE);
    }
    if (test_au(fd, audiobuf) >= 0)
    {
      rhwparams.format = SND_PCM_FORMAT_MU_LAW;
      pbrec_count = calc_count();
      playback_go(fd, 0, pbrec_count, FORMAT_AU, name);
      goto __end;
    }
    dta = sizeof(VocHeader);
    if ((size_t) safe_read(fd, audiobuf + sizeof(AuHeader),
                           dta - sizeof(AuHeader)) != dta - sizeof(AuHeader))
    {
      error("read error");
      exit(EXIT_FAILURE);
    }
    if ((ofs = test_vocfile(audiobuf)) >= 0)
    {
      pbrec_count = calc_count();
      voc_play(fd, ofs, name);
      goto __end;
    }
    /* read bytes for WAVE-header */
    if ((dtawave = test_wavefile(fd, audiobuf, dta)) >= 0)
    {
      pbrec_count = calc_count();
      playback_go(fd, dtawave, pbrec_count, FORMAT_WAVE, name);
    }
    else
    {
      /* should be raw data */
      init_raw_data();
      pbrec_count = calc_count();
      playback_go(fd, dta, pbrec_count, FORMAT_RAW, name);
    }
  __end:
    if (fd != 0)
      close(fd);
#endif
  }

  void capture(const char *fname)
  {
#ifndef NO_ALSA
    int length=strlen(fname)+1;
    if(length<8) /* ensure enough room for "stdout" */
      length=8;
    name = (char *) malloc(length);
    strcpy(name, fname);
  

    /*printf(" dans Capture");endl; */
    pbrec_count = (size_t) - 1;
    if (write_wav == 1)
    {
      if (!name || !strcmp(name, "-"))
      {
	fd = fileno(stdout);
	strncpy(name,"stdout",6);
	name[6]='\0';
      }
      else
      {
	remove(name);
	if ((fd =
	     open64(name, O_WRONLY | O_CREAT | O_APPEND, 0644)) == -1)
	{
	  perror(name);
	  exit(EXIT_FAILURE);
	}
      }
    }
    fdcount = 0;
    pbrec_count = calc_count();
    /* WAVE-file should be even (I'm not sure), but wasting one byte
       isn't a problem (this can only be in 8 bit mono) */
    pbrec_count += pbrec_count % 2;
    if (pbrec_count == 0)
      pbrec_count -= 2;
    if (write_wav == 1)
    {
      if (fmt_rec_table[file_type].start)
	fmt_rec_table[file_type].start(fd, pbrec_count);
    }
    capture_go(fd, pbrec_count, file_type, name); 
    /* before char *bufin input parameters and used in capture_go*/
#endif
  }

  void playbackv_go(int *fds, unsigned int channels, size_t loaded,
		    size_t count, int rtype, char **names)
  {
#ifndef NO_ALSA
    int r;
    size_t vsize;

    unsigned int channel;
/*  u_char *bufs[channels]; // berk !*/    
    u_char **bufs = NULL;
    bufs=MANY_ALLOCATIONS(channels,u_char*);

    header(rtype, names[0]);
    set_params();

    vsize = chunk_bytes / channels;

    /* Not yet implemented */
    assert(loaded == 0);

    for (channel = 0; channel < channels; ++channel)
      bufs[channel] = audiobuf + vsize * channel;

    while (count > 0)
    {
      size_t c = 0;
      size_t expected = count / channels;
      if (expected > vsize)
	expected = vsize;
      do
      {
	r = safe_read(fds[0], bufs[0], expected);
	if (r < 0)
	{
	  perror(names[channel]);
	  exit(EXIT_FAILURE);
	}
	for (channel = 1; channel < channels; ++channel)
	{
	  if (safe_read(fds[channel], bufs[channel], r) != r)
	  {
	    perror(names[channel]);
	    exit(EXIT_FAILURE);
	  }
	}
	if (r == 0)
	  break;
	c += r;
      }
      while (sleep_min == 0 && c < expected);
      c = c * 8 / bits_per_sample;
      r = pcm_writev(bufs, channels, c);
      if ((size_t) r != c)
	break;
      r = r * bits_per_frame / 8;
      count -= r;
    }
    snd_pcm_drain(handle);

    if(bufs!=NULL)
      free(bufs);
#endif
  }

  void capturev_go(int *fds, unsigned int channels, size_t count, int rtype,
		   char **names)
  {
#ifndef NO_ALSA
    size_t c;
    ssize_t r;
    unsigned int channel;
    size_t vsize;
/*  u_char *bufs[channels]; // berk !*/    
    u_char **bufs = NULL;
    bufs=MANY_ALLOCATIONS(channels,u_char*);

    header(rtype, names[0]);
    set_params();

    vsize = chunk_bytes / channels;

    for (channel = 0; channel < channels; ++channel)
      bufs[channel] = audiobuf + vsize * channel;

    while (count > 0)
    {
      size_t rv;
      c = count;
      if (c > chunk_bytes)
	c = chunk_bytes;
      c = c * 8 / bits_per_frame;
      if ((size_t) (r = pcm_readv(bufs, channels, c)) != c)
	break;
      rv = r * bits_per_sample / 8;
      for (channel = 0; channel < channels; ++channel)
      {
	if ((size_t) write(fds[channel], bufs[channel], rv) != rv)
	{
	  perror(names[channel]);
	  exit(EXIT_FAILURE);
	}
      }
      r = r * bits_per_frame / 8;
      count -= r;
      fdcount += r;
    }

    if(bufs!=NULL)
      free(bufs);
#endif
  }

  void playbackv(char **names, unsigned int count)
  {
#ifndef NO_ALSA
    int ret = 0;
    unsigned int channel;
    unsigned int channels = rhwparams.channels;
    int alloced = 0;
/*    int fds[channels]; // berk !*/    
    int *fds = NULL;
    fds=MANY_ALLOCATIONS(channels,int);

    for (channel = 0; channel < channels; ++channel)
      fds[channel] = -1;

    if (count == 1 && channels > 1)
    {
      size_t len = strlen(names[0]);
      char format[1024];
      memcpy(format, names[0], len);
      strcpy(format + len, ".%d");
      len += 4;
      names = malloc(sizeof(*names) * channels);
      for (channel = 0; channel < channels; ++channel)
      {
	names[channel] = malloc(len);
	sprintf(names[channel], format, channel);
      }
      alloced = 1;
    }
    else if (count != channels)
    {
      error("You need to specify %d files", channels);
      exit(EXIT_FAILURE);
    }

    for (channel = 0; channel < channels; ++channel)
    {
      fds[channel] = open(names[channel], O_RDONLY, 0);
      if (fds[channel] < 0)
      {
	perror(names[channel]);
	ret = EXIT_FAILURE;
	goto __end;
      }
    }
    /* should be raw data */
    init_raw_data();
    pbrec_count = calc_count();
    playbackv_go(fds, channels, 0, pbrec_count, FORMAT_RAW, names);

  __end:
    for (channel = 0; channel < channels; ++channel)
    {
      if (fds[channel] >= 0)
	close(fds[channel]);
      if (alloced)
	free(names[channel]);
    }

    if(fds!=NULL)
      free(fds);
    if (alloced)
      free(names);
    if (ret)
      exit(ret);
#endif
  }

  void capturev(char **names, unsigned int count)
  {
#ifndef NO_ALSA
    int ret = 0;
    unsigned int channel;
    unsigned int channels = rhwparams.channels;
    int alloced = 0;
/*    int fds[channels]; // berk !*/    
    int *fds = NULL;
    fds=MANY_ALLOCATIONS(channels,int);

    for (channel = 0; channel < channels; ++channel)
      fds[channel] = -1;

    if (count == 1)
    {
      size_t len = strlen(names[0]);
      char format[1024];
      memcpy(format, names[0], len);
      strcpy(format + len, ".%d");
      len += 4;
      names = malloc(sizeof(*names) * channels);
      for (channel = 0; channel < channels; ++channel)
      {
	names[channel] = malloc(len);
	sprintf(names[channel], format, channel);
      }
      alloced = 1;
    }
    else if (count != channels)
    {
      error("You need to specify %d files", channels);
      exit(EXIT_FAILURE);
    }

    for (channel = 0; channel < channels; ++channel)
    {
      fds[channel] = open(names[channel], O_WRONLY + O_CREAT, 0644);
      if (fds[channel] < 0)
      {
	perror(names[channel]);
	ret = EXIT_FAILURE;
	goto __end;
      }
    }
    /* should be raw data */
    init_raw_data();
    pbrec_count = calc_count();
    capturev_go(fds, channels, pbrec_count, FORMAT_RAW, names);

  __end:
    for (channel = 0; channel < channels; ++channel)
    {
      if (fds[channel] >= 0)
	close(fds[channel]);
      if (alloced)
	free(names[channel]);
    }

    if(fds!=NULL)
      free(fds);
    if (alloced)
      free(names);
    if (ret)
      exit(ret);
#endif
  }



/* Affiche sur stdout les valeurs de la trame*/
  int showbuf(char *buf)
  {
    int i = 0;

    printf("length buf=%d", (int)strlen(buf));
    endl;
    for (i = 0; (unsigned int)i < strlen(buf); i++)
    {
      printf("%d, ", (int) buf[i]);
    }
    endl;
    return i;
  }

/* sauve dans un fichier nomme dat les trames acquises
 
*/
  int saveto(FILE * file, char *buf)
  {

    int i = 0;
    int N = strlen(buf);

    for (i = 0; i < N; i++)
    {

      if (fprintf(file, "%f\n", (float) buf[i] / 128) == 0)
      {
	perror("nothing written");
      }
    }

    return i;

  }
