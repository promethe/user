/** ***********************************************************
\file  read_audio.c
\brief calculate distance between two points

Author: Oriane Dermy
Created: 01/06/15

Theoritical description:

Description:
 This file contains a little function able to read audio save thanks to f_sound_get_signal (in fact alsa_sound.c called by f_sound_get_signal).
 * Add : if we add a group of supervision, there were "n" = size of supervision group wich will be read according to the activate neuron
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: none
- description: none
- input expected group: none
- where are the data?: none

\section Options
- neurone : will be write in the link of the supervision group.
- fName : will be write to allow to know were we need to read the file (file named Name or Name0 NameI NameA and NameN if we have had thhe supervision group.


Comments: none

Known bugs: none

Todo: see the author for comments


http://www.doxygen.org
 ************************************************************/
//#define DEBUG 1
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <alsa/asoundlib.h>
#define EXIT_ON_SOUND_ERROR(err, msg, ...) EXIT_ON_ERROR("ALSA:%s\n\t"msg, snd_strerror(err), ##__VA_ARGS__)
#define size 128
#define send 1024

typedef struct read_audio{
	FILE *fp; //*fpa, *fpi, *fpo;
	int nb_it, ita, iti, ito, itTot;
	int tall, talla, talli, tallo;
	//int sound_frame_number;
	int16_t *buf, *bufa, *bufo, *bufi, bufTot[send];
	snd_pcm_t *playback_handle;
	int debutCtrlGpe, choixPrec;
} type_read_audio;


snd_pcm_t *init_sound_output(char const *device_name)
{
	int err, dir=0;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *playback_handle = NULL;
   unsigned int sample_rate = 44100, exact_rate;

   //Infos sortie
   	if ((err = snd_pcm_open (&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0) 
   	{
		EXIT_ON_SOUND_ERROR(err, "cannot open audio device %s (%s)\n", device_name);
	}
		if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
		{
			EXIT_ON_SOUND_ERROR(err, "cannot allocate hardware parameter structure (%s)\n");
		}
				 
		if ((err = snd_pcm_hw_params_any (playback_handle, hw_params)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot initialize hardware parameter structure (%s)\n");
		}
	
		if ((err = snd_pcm_hw_params_set_access (playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set access type (%s)\n");
		}
	
		if ((err = snd_pcm_hw_params_set_format (playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set sample format (%s)\n");
		}
	
	
		if ((err = snd_pcm_hw_params_set_channels (playback_handle, hw_params, 2)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set channel count (%s)\n");
		}
		exact_rate=sample_rate;
		if ((err = snd_pcm_hw_params_set_rate_near (playback_handle, hw_params, &exact_rate, &dir)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set sample rate (%s)\n");
		}
		if(exact_rate != sample_rate)
		{
			printf("\nThe rate %d Hz is not supported by your hardware.\n ==> Using %d Hz instead.\n\n", sample_rate, exact_rate);
		}else printf("\nThe rate %d Hz is supported by your hardware.\n\n\n", exact_rate);


		if ((err = snd_pcm_hw_params (playback_handle, hw_params)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set parameters (%s)\n");
		}

		
		snd_pcm_hw_params_free (hw_params);
		if ((err = snd_pcm_prepare (playback_handle)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot prepare audio interface for use (%s)\n");
		}

		return  playback_handle;
}



void new_read_audio(int Gpe)
{
	int l,i, taille, result;
	FILE *fp=NULL, *fpa=NULL, *fpi=NULL, *fpo=NULL;
	type_read_audio *my_read_audio=NULL;
	char param[256], string[256];
	char *nameFile= NULL;
	
	dprints("Begining of new_read_audio\n");

	my_read_audio=(type_read_audio*)malloc(sizeof(type_read_audio));

	if(my_read_audio==NULL)
	{
		dprints("my_read_audio is NULL!\n");
		EXIT_ON_ERROR("Problem with malloc in %d\n",Gpe);
	}
	
	/**Initialisation **/
	my_read_audio->nb_it=0;
	my_read_audio->itTot=0;
	my_read_audio->fp = NULL;
	//my_read_audio->bufTot= {0};
	/*Ajout pour le cas où plusieurs fichiers audio*/
	my_read_audio->ita = 0;
	my_read_audio->ito = 0;
	my_read_audio->iti = 0;
	my_read_audio->debutCtrlGpe = -1;
	/*Fin ajout*/
	
	my_read_audio->playback_handle = init_sound_output("default");
	if(my_read_audio->playback_handle ==NULL) dprints("playback no activate\n\n\n"); 
	//my_read_audio->sound_frame_number = def_groupe[Gpe].taillex;
	//my_read_audio->buf = malloc(def_groupe[Gpe].taillex*sizeof(int16_t));
	/******************************************************Links Treatments*************************************/
	dprints("Begin the reading of links\n");

	if (def_groupe[Gpe].ext == NULL)
	{
		i=0;
		l = find_input_link(Gpe, i);
		while(l!=-1)
		{
				if (prom_getopt(liaison[l].nom, "-f", param) != 0)
				{
					if (param != NULL)
					{		
						sprintf(string, "%s", param);
					}
				}
				// ajout dans le cas où plusieurs fichiers audios
				if(prom_getopt(liaison[l].nom, "-neurone", param) != 0)
				{
					my_read_audio->debutCtrlGpe = def_groupe[liaison[l].depart].premier_ele;
					printf("groupe de supervision : %d\n", my_read_audio->debutCtrlGpe);
				}
				//FIN AJOUT
				i++;
				l = find_input_link(Gpe, i);
		}
		/*If correspondant aà l'ajout de plusieurs fichiers audios*/		
		if(my_read_audio->debutCtrlGpe != -1)
		{
			//we opened the other files
			nameFile = (char *) malloc(sizeof(char)*strlen(string)+2);
			strcat(nameFile, string);
			nameFile[strlen(string)] = 'N';
			nameFile[strlen(string) +1] = '\0';
			printf("\nname for N %s\n\n", nameFile);
			if(nameFile !=NULL)
			{
				fp = fopen(nameFile, "rb");
				if ((fp == NULL))
				{
					EXIT_ON_ERROR("ERROR in read_audio(%s): Impossible to read file %s\n", def_groupe[Gpe].no_name, nameFile);
				}
				fseek(fp, 0, SEEK_END);
				taille = ftell(fp)/sizeof(int16_t);
				my_read_audio->buf = (int16_t *) malloc(ftell(fp));
				fseek(fp, 0, SEEK_SET);
				if((result = fread(my_read_audio->buf, sizeof(int16_t), taille, fp)) != taille)
				{
					EXIT_ON_ERROR("lecture fichier %s mal lu : lu uniquement %d valeurs\n", nameFile, result);
				}
				my_read_audio->tall = taille;//*sizeof(int16_t);
				
			}
			nameFile[strlen(string)] = 'A';

			printf("name for A %s\n", nameFile);
			if(nameFile !=NULL)
			{
				fpa = fopen(nameFile, "rb");
				if ((fpa == NULL))
				{
					EXIT_ON_ERROR("ERROR in read_audio(%s): Impossible to read file %s\n", def_groupe[Gpe].no_name, nameFile);
				}
				fseek(fpa, 0, SEEK_END);
				taille = ftell(fpa)/sizeof(int16_t);
				my_read_audio->bufa = (int16_t *) malloc(taille*sizeof(int16_t));
				fseek(fpa, 0, SEEK_SET);
				if((result = fread(my_read_audio->bufa, sizeof(int16_t), taille, fpa)) != taille)
				{
					EXIT_ON_ERROR("lecture fichier %s mal lu : lu uniquement %d valeurs\n", nameFile, result);
				}
				my_read_audio->talla = taille;
				
			}
			
			nameFile[strlen(string)] = 'O';

			printf("name for O %s\n", nameFile);
			if(nameFile !=NULL)
			{
				fpo = fopen(nameFile, "rb");
				if ((fpo == NULL))
				{
					EXIT_ON_ERROR("ERROR in read_audio(%s): Impossible to read file %s\n", def_groupe[Gpe].no_name, nameFile);
				}
				fseek(fpo, 0, SEEK_END);
				taille = ftell(fpo)/sizeof(int16_t);
				my_read_audio->bufo = (int16_t *) malloc(taille*sizeof(int16_t));
				fseek(fpo, 0, SEEK_SET);
				if((result = fread(my_read_audio->bufo, sizeof(int16_t), taille, fpo)) != taille)
				{
					EXIT_ON_ERROR("lecture fichier %s mal lu : lu uniquement %d valeurs\n", nameFile, result);
				}
				my_read_audio->tallo = taille;
				
			}
			nameFile[strlen(string)] = 'I';

			printf("name for I %s\n", nameFile);
			if(nameFile !=NULL)
			{
				fpi = fopen(nameFile, "rb");
				if ((fpi == NULL))
				{
					EXIT_ON_ERROR("ERROR in read_audio(%s): Impossible to read file %s\n", def_groupe[Gpe].no_name, nameFile);
				}
				fseek(fpi, 0, SEEK_END);
				taille = ftell(fpi)/sizeof(int16_t);
				my_read_audio->bufi = (int16_t *) malloc(taille*sizeof(int16_t));
				fseek(fpi, 0, SEEK_SET);
				if((result = fread(my_read_audio->bufi, sizeof(int16_t), taille, fpi)) != taille)
				{
					EXIT_ON_ERROR("lecture fichier %s mal lu : lu uniquement %d valeurs\n", nameFile, result);
				}
				my_read_audio->talli = taille;
				
			}
			fclose(fp);
			fclose(fpa);
			fclose(fpi);
			fclose(fpo);
		}
		/*fin ajout*/
		else
		{
			my_read_audio->fp = fopen(string, "rb");
			if ((my_read_audio->fp == NULL))
			{
				EXIT_ON_ERROR("ERROR in read_audio(%s): Impossible to read file %s\n", def_groupe[Gpe].no_name, string);
			}
			//fseek(fp, 0, SEEK_END);
			//taille = ftell(fp)/sizeof(int16_t);
			//my_read_audio->buf = (int16_t *) malloc(taille*sizeof(int16_t));
			//fseek(fp, 0, SEEK_SET);
			//if((result = fread(my_read_audio->buf, sizeof(int16_t), taille, fp)) != taille)
			//{
				//EXIT_ON_ERROR("lecture fichier %s mal lu : lu uniquement %d valeurs\n", nameFile, result);
			//}
			
			//my_read_audio->tall = taille;//*sizeof(int16_t);
			//fclose(fp);
		}
	}
	//my_read_audio->choixPrec = -1;
	def_groupe[Gpe].data= my_read_audio; /*Save of My_Data*/
	dprints("End of new_read_audio\n");
}


void destroy_read_audio(int gpe)
{
	type_read_audio *my_read_audio= def_groupe[gpe].data;
	if(my_read_audio->fp !=NULL)
	{
		free(my_read_audio->fp);
		my_read_audio->fp = NULL;
	}
	//ajout fichiers multiples
	/*if(my_read_audio->fpa !=NULL)
	{
		free(my_read_audio->fpa);
		my_read_audio->fpa = NULL;
	}
	if(my_read_audio->fpo !=NULL)
	{
		free(my_read_audio->fpo);
		my_read_audio->fpo = NULL;
	}
	if(my_read_audio->fpi !=NULL)
	{
		free(my_read_audio->fpi);
		my_read_audio->fpi = NULL;
	}*/
	/*fin ajout*/
	free(def_groupe[gpe].data);
	def_groupe[gpe].data=NULL;
	def_groupe[gpe].ext=NULL;
}

void function_read_audio(int Gpe)
{
	int result, debut, size2;
	int16_t *buf;
	type_read_audio *my_read_audio=NULL;
	
	dprints("At the begining of the function read_audio\n");
	
	//
	my_read_audio = def_groupe[Gpe].data;
	if(my_read_audio == NULL) EXIT_ON_ERROR("error allocation structure in f_read_audio\n");
	
	//if we have to read only one input file
	if(my_read_audio->debutCtrlGpe == -1)
	{ 
			debut = def_groupe[Gpe].premier_ele;
			size2 = def_groupe[Gpe].taillex;
			//on continue juste la lecture du fichier audio 
			buf = (int16_t *) malloc(size2*2*sizeof(int16_t));
				//fseek(my_read_audio->fp, my_read_audio->nb_it*(2*size*sizeof(int16_t)), SEEK_SET);
				//my_read_audio->nb_it++; 
			if((result = fread(buf,sizeof(int16_t), size2*2, my_read_audio->fp)) != size2*2)
			{
				if(feof(my_read_audio->fp))
				{
					fseek(my_read_audio->fp, 0, SEEK_SET);
					printf("retour à 0 feof\n");
				}
					else printf("erreur DANS LA TAILLE !\n\n");
			}	
			else
			{
			/*for (j=0;j < size2; j++)
			{
				//We normalize the data. Max  is 1 << 15
				// Pour optimiser il ne faudrait peut être pas normalise et ne travailler que sur des entiers. Arnaud B.	
				neurone[j +debut].s1 = (float)  buf[j * 2] / (1 << 15);
				neurone[size2 +j + debut].s1 = (float) buf[j * 2 + 1] / (1 << 15);
			}*/
				if ((result = snd_pcm_writei (my_read_audio->playback_handle, buf, size2)) != size2) 
				{
					if( result == -EPIPE)
					{
						snd_pcm_recover(my_read_audio->playback_handle, result, 0);
						PRINT_WARNING("Don't right sound sample");
					}
					else dprints("write to audio interface failed (default)\n");			
				}
			}

	  }
	 /* else
	  {
	     if(neurone[my_read_audio->debutCtrlGpe].s1 > 0.5)
		  {
			 &(my_read_audio->bufTot[(my_read_audio->itTot)]) = &(my_read_audio->buf[(my_read_audio->nb_it)]);
			 //new placement of the cursor
			 my_read_audio->nb_it += size;
			 if(my_read_audio->nb_it + size > my_read_audio->tall)
			 {
				my_read_audio->nb_it = 0;
			 }
			 if(my_read_audio->itTot + size == send)
			 {
			  if ((result = snd_pcm_writei (my_read_audio->playback_handle, my_read_audio->bufTot, send)) != send) 
			  {
				if( result == -EPIPE)
				{
					snd_pcm_recover(my_read_audio->playback_handle, result, 0);
					PRINT_WARNING("Don't right sound sample");
				 }
				 else dprints("write to audio interface failed (default)\n");			
			  }
			  my_read_audio->itTot = 0;
			}
		    else my_read_audio->itTot += size;	
		  }
	  }*/
	
	  //~ else
	  //~ {
		  //~ if(neurone[my_read_audio->debutCtrlGpe].s1 > 0.5)
		  //~ {
			  //~ for(j=0;j<size; j++)
			  //~ {
				  //~ my_read_audio->bufTot[my_read_audio->itTot + j] = my_read_audio->bufi[my_read_audio->iti +j] ;
			  //~ }
			  //~ 
			  //~ ///*if ((result = snd_pcm_writei (my_read_audio->playback_handle, &my_read_audio->buf[my_read_audio->iti], size)) != size) 
			  //~ //{
					  //~ //if( result == -EPIPE)
					  //~ //{
						  //~ //snd_pcm_recover(my_read_audio->playback_handle, result, 0);
						  //~ //PRINT_WARNING("Don't right sound sample");
					  //~ //}
					  //~ //else dprints("write to audio interface failed (default)\n");			
			  //~ //} */
			  //~ //new placement of the cursor
			  //~ my_read_audio->iti += size;
			  //~ if(my_read_audio->iti + size > my_read_audio->talli)
			  //~ {
				  //~ my_read_audio->iti = 0;
				  //~ printf("retour\n");
			  //~ }
		  //~ }
		  //~ else if(neurone[my_read_audio->debutCtrlGpe +2].s1 > 0.5)
		  //~ {
			  //~ for(j=0;j<size; j++)
			  //~ {
				  //~ my_read_audio->bufTot[my_read_audio->ita + j] = my_read_audio->bufi[my_read_audio->ita +j] ;
			  //~ }
			  //~ //new placement of the cursor
			  //~ my_read_audio->ita += size;
			  //~ if(my_read_audio->ita + size > my_read_audio->talla)
			  //~ {
				  //~ my_read_audio->ita = 0;
			  //~ }
		  //~ }
		  //~ else if(neurone[my_read_audio->debutCtrlGpe +3].s1 > 0.5)
		  //~ {
			  //~ for(j=0;j<size; j++)
			  //~ {
				  //~ my_read_audio->bufTot[my_read_audio->ito + j] = my_read_audio->bufi[my_read_audio->ito +j] ;
			  //~ }
			  //~ //new placement of the cursor
			  //~ my_read_audio->ito += size;
			  //~ if(my_read_audio->ito + size > my_read_audio->tallo)
			  //~ {
				  //~ my_read_audio->ito = 0;
			  //~ }
		  //~ }
		  //~ else
		  //~ {
			  //~ for(j=0;j<size; j++)
			  //~ {
				  //~ my_read_audio->bufTot[my_read_audio->nb_it + j] = my_read_audio->bufi[my_read_audio->nb_it +j] ;
			  //~ }
			  //~ //new placement of the cursor
			  //~ my_read_audio->nb_it += size;
			  //~ if(my_read_audio->nb_it + size > my_read_audio->tall)
			  //~ {
				  //~ my_read_audio->nb_it = 0;
			  //~ }
		  //~ }
  //~ /* // we write in neurons the values		
	  //~ for (i=0;i < size/2; i++)
	  //~ {
	  //~ //We normalize the data. Max  is 1 << 15
	  //~ // Pour optimiser il ne faudrait peut être pas normalise et ne travailler que sur des entiers. Arnaud B.	
		  //~ neurone[i +debut].s1 = (float)  my_read_audio->buf[my_read_audio->itTot + i * 2] / (1 << 15);
		  //~ neurone[size +i + debut].s1 = (float) my_read_audio->buf[my_read_audio->itTot +i * 2 + 1] / (1 << 15);
	  //~ }
  //~ */		
		  //~ //writing if the buffer is totally filled
		  //~ if(my_read_audio->itTot + size == send)
		  //~ {
			  //~ if ((result = snd_pcm_writei (my_read_audio->playback_handle, my_read_audio->bufTot, send)) != send) 
			  //~ {
				  //~ if( result == -EPIPE)
				  //~ {
					  //~ snd_pcm_recover(my_read_audio->playback_handle, result, 0);
					  //~ PRINT_WARNING("Don't right sound sample");
				  //~ }
				  //~ else dprints("write to audio interface failed (default)\n");			
			  //~ }
			  //~ my_read_audio->itTot = 0;
		  //~ }
		  //~ else my_read_audio->itTot += size;		
	//~ }
	
	dprints("At the end of the function read_audio\n");
}

