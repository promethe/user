/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_init_fft_audio.c 
\brief initialise a 1 dimension fft with fftw3

Author: A.HIOLLE
Created: 19/04/2005
Modified:
- author: 
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:  mettre -Xtaille pour allouer un plan de fft de cette taille

Known bugs: si il y a plusieurs boites en parallele utilisant cette fonction 
	    probleme avec fftw

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#ifndef NO_ALSA
#include <Sound/Sound.h>
#endif
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/

void function_init_fft_audio(int numero)
{
#ifndef NO_ALSA
    char *chaine = NULL, *info = NULL;

    int i = 0, taillex = -1, l, link = -1, Gpe_sound = -1;


    /* Recuperation du lien entrant
       mettre -XN pour un tableau de N echantillons pour initialiser la 
       taille de la fft     
     */
#ifdef DEBUG
    pr("fft_init_audio\n");
#endif
    l = find_input_link(numero, i);
    while (l != -1)
    {
        if (strncmp(liaison[l].nom, "-X", 2) == 0)
        {
            Gpe_sound = liaison[l].depart;
            link = l;
        }
        i++;
        l = find_input_link(numero, i);
    }

    if (Gpe_sound == -1)
    {
        pr("liaison avec groupe sound non trouve");
        endl;
        exit(0);
    }
    else
    {

        chaine = liaison[link].nom;

        if (chaine == NULL)
        {
            printf("numero : %d : %s : pas de lien entrant...\n", numero,
                   __FUNCTION__);
            exit(0);
        }                       /* recuperation des infos du lien */
        info = NULL;

        info = strstr(chaine, "-X");

        if (info != NULL)
        {

            taillex = atoi(&info[2]);

            printf("taillex %d\n", taillex);

        }
        else
        {
            printf
                ("%s : il faut definir les dimensions des FFT (-X### )\n",
                 __FUNCTION__);
            exit(0);
        }

        if (taillex < 1)
        {
            printf
                ("%s : il faut definir les dimensions des FFT (-X### -Y###)\n",
                 __FUNCTION__);
            exit(0);
        }

    }

    /*      Initialisation de la structure audio */
    if (def_groupe[numero].audio_ext == NULL)
    {

        if ((def_groupe[numero].audio_ext =
             (void *) malloc(sizeof(fft_1d_audio))) == NULL)
        {
#ifdef DEBUG
            pr("allocation de la structure audio\n");
#endif
            perror("fft_1d_audio allocation error\n");

            printf(" dans %s \n", __FUNCTION__);
        }

        ((fft_1d_audio *) def_groupe[numero].audio_ext)->taille = taillex;

        /*  allouer la trame dans votre code selon le type de donnees
           exemple : 
           recopier cette ligne dans votre code
           ((fft_1d_audio *)def_groupe[numero].audio_ext)->trame=(void *)malloc(taillex*sizeof(type_donnee);

         */
        if ((((fft_1d_audio *) def_groupe[numero].audio_ext)->in =
             (float *) malloc(taillex * sizeof(float))) == NULL)
        {

            perror("fft_1d_audio->in allocation error\n");
            printf(" dans %s \n", __FUNCTION__);
            exit(-1);
        }
        if ((((fft_1d_audio *) def_groupe[numero].audio_ext)->out =
             (fftwf_complex *) malloc(taillex * sizeof(fftwf_complex))) ==
            NULL)
        {

            perror("fft_1d_audio->out allocation error\n");
            printf(" dans %s \n", __FUNCTION__);
            exit(-1);
        }


            /******creation du plan pour la fft*****************/

        if ((((fft_1d_audio *) def_groupe[numero].audio_ext)->fft_plan =
             (fftwf_plan *) malloc(sizeof(fftwf_plan))) == NULL)
        {

            perror("fft_1d_audio->fft_plan allocation error\n");
            printf(" dans %s \n", __FUNCTION__);
            exit(-1);
        }

        *((fft_1d_audio *) def_groupe[numero].audio_ext)->fft_plan =
            fftwf_plan_dft_r2c(1, &taillex,
                               ((fft_1d_audio *) def_groupe[numero].
                                audio_ext)->in,
                               ((fft_1d_audio *) def_groupe[numero].
                                audio_ext)->out,
                               FFTW_FORWARD | FFTW_ESTIMATE);
    }
    else
    {
        printf
            ("l'initialisation de la fft_1d ne doit se faire qu'une fois\n ");

    }
#else
    printf("PAS de GESTION de ALSA prevue !!! \n");
#endif
}
