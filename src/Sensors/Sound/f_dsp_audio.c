/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_dsp_audio f_dsp_audio
\ingroup libSensors

\brief calcul de la dsp 1d

\file
\ingroup f_dsp_audio

\brief calcul de la dsp 1d

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

**/
/*#define DEBUG*/ 
#include <libx.h>
#include <stdlib.h>
#include <string.h>


#ifndef NO_FFT
#include <Sound/Sound.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>


/*#define SAVE*/
/*SAVE a decommenter si vous souhaiter stocker les dsp de chaque trame dans un fichier du nom
 "fft+numero_iteration"(ici tps);*/
static int tps = 0;




void function_dsp_audio(int numero)
{
#ifdef SAVE
    char filename[10];
    FILE *f;
    char str[10];
#endif
    int k, taille = 0;
    int l = -1;
    int i = 0, cpt = 0;
    int Gpe_sound = -1;
    double *power_spectrum;
    int winner = -1;
    float tempspec = 0, meanspec = 0;
    int temp = 0;
    int maxfreq, gap;

    tps++;

#ifdef DEBUG
    pr("Dsp_audio\n");
#endif
    if (def_groupe[numero].ext == NULL)
    {
        l = find_input_link(numero, i);
        /** recherche du lien entrant     */
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "dsp") == 0)
            {
                Gpe_sound = liaison[l].depart;
            }
            i++;
            l = find_input_link(numero, i);
        }
#ifdef DEBUG
        printf("link sound=%d", Gpe_sound);
        endl;
#endif

        /** Sortie si le lien n'est pas trouve */
        if (Gpe_sound == -1)
        {
            pr("liaison avec groupe fft non trouve");
            endl;
            exit(0);
        }

        /** recopie la structure fft_1d_audio du groupe precedent */
        if ((fft_1d_audio *) def_groupe[Gpe_sound].audio_ext != NULL)
        {
            def_groupe[numero].audio_ext =
                (void *) def_groupe[Gpe_sound].audio_ext;
        }
        else
        {

            printf("no audio data in groupe : %d\n", Gpe_sound);
            exit(-1);
        }

        def_groupe[numero].ext = (void *) malloc(sizeof(int));

        /** stockage du numero de groupeee dans ext */
        *(int *) def_groupe[numero].ext = Gpe_sound;
    }
    else
    {
#ifdef DEBUG
        pr("already initialized\n");
#endif
        if (def_groupe[numero].ext != NULL)
        {
            Gpe_sound = *((int *) def_groupe[numero].ext);
        }
        else
        {
            printf("no group stored\n");
            exit(-1);

        }
#ifdef DEBUG
        printf("Gpe_fft=%d\n", Gpe_sound);
#endif
    }

    if ((fft_1d_audio *) def_groupe[numero].audio_ext == NULL)
    {
        perror("audio_ext is null\n");
    }

    taille = (int) ((fft_1d_audio *) def_groupe[numero].audio_ext)->taille;


    /** allocation du tableau qui contiendra les valeurs de la dsp */
    if ((power_spectrum =
         (double *) malloc(((taille + 10) / 2) * sizeof(double))) == NULL)
    {

        perror("power_spectrum Allocation error in f_dsp_audio\n");
        exit(-1);
    }

#ifdef DEBUG
    printf("taille_power spec=%d\n", (taille + 10) / 2);
#endif



#ifdef SAVE
    /*******definition du fichier de sauvegarde des dsp**************/
    /** creation du nom du fichier de sauvegarde      */

    strcpy(filename, "fft");

    /**ajout du numero de l'iteration en cours au nom du fichier */
    sprintf(str, "%d", tps);
    strcat(filename, str);

    /**ouverture du fichier de sauvegarde            */
    if ((f = fopen(filename, "w+")) == NULL)
    {
        perror("Error during file opening :");
        endl;
        exit(-1);
    }
#endif



    /** ***********************Calcul de la DSP***********/
        /** ****sq(X) donne X au carre***********/


    power_spectrum[0] = 0.0;
#ifdef SAVE
    fprintf(f, "%f\n", power_spectrum[0]);
#endif
    for (k = 1; k < (taille + 1) / 2; ++k)  /* (k < N/2 rounded up) */
    {
        power_spectrum[k] =
            sq((((fft_1d_audio *) def_groupe[numero].audio_ext)->out[k][0])) +
            sq(((fft_1d_audio *) def_groupe[numero].audio_ext)->out[k][1]) +
            sq(((fft_1d_audio *) def_groupe[numero].audio_ext)->
               out[taille - k][0]) +
            sq(((fft_1d_audio *) def_groupe[numero].audio_ext)->
               out[taille - k][1]);
#ifdef SAVE
        fprintf(f, "%f\n", power_spectrum[k]);
#endif
    }

    if (taille % 2 == 0)
    {                           /* N is even */

#ifdef DEBUG
        printf("tailledsp= %d\n", taille);
#endif


        power_spectrum[taille / 2] =
            sq(((fft_1d_audio *) def_groupe[numero].audio_ext)->
               out[taille / 2][0]) +
            sq(((fft_1d_audio *) def_groupe[numero].audio_ext)->
               out[taille / 2][1]);
    }

#ifdef SAVE
    if (fprintf(f, "%f\n", power_spectrum[taille / 2]) < 0)
    {
        perror("error writing\n");
        exit(-1);

    }


/**	fermeture du fichier de sauvegarde*/
    if (fclose(f) != 0)
    {

        perror("Error on closing file\n");
        exit(-1);
    }
#endif

#ifdef DEBUG
    printf(" tp= %d\n", tps);
#endif
    /** frequence maximale d'observation */
    maxfreq = 1300;

    /** largeur de la bande de frequence par neurone         */
    gap = maxfreq / (16 * def_groupe[numero].nbre);

    /**affectation de la valeur des neurones
       chacun recoit la valeur de la moyenne d'une bande de fr�uence
       de largeur gap*maxfreq/nbre neurone */
    for (i = 0; i < def_groupe[numero].nbre; i++)
    {
        tempspec = 0;
        temp = cpt;
        /**on incremente de gap */
        for (cpt = temp; cpt < temp + gap; cpt++)
        {
            if ((power_spectrum + cpt) != NULL)
            {
                tempspec += power_spectrum[cpt];
                /*printf("cpt=%d\n",cpt); */
            }
            else
            {
                perror("dsp null\n");
            }
        }

        /**chaque neurone recoit la valeur moyenne sur sa bande */
        if (tempspec > def_groupe[numero].seuil)
        {
            neurone[def_groupe[numero].premier_ele + i].s = tempspec;
            neurone[def_groupe[numero].premier_ele + i].s1 = tempspec;
            neurone[def_groupe[numero].premier_ele + i].s2 = tempspec;
        }
        else
        {
            neurone[def_groupe[numero].premier_ele + i].s = 0;
            neurone[def_groupe[numero].premier_ele + i].s1 = 0;
            neurone[def_groupe[numero].premier_ele + i].s2 = 0;
        }
        /** on retient la bande de moyenne la + elevee pour normaliser */
        if (tempspec > meanspec)
        {
            winner = i;
            meanspec = tempspec;
        }
    }
    /**desallocation du tableau de la dsp */
    free(power_spectrum);


    /**normalisation de la sortie */
    for (i = 0; i < def_groupe[numero].nbre; i++)
    {


        neurone[def_groupe[numero].premier_ele + i].s /= meanspec;
        neurone[def_groupe[numero].premier_ele + i].s1 /= meanspec;
        neurone[def_groupe[numero].premier_ele + i].s2 /= meanspec;


    }

}

#else
void function_dsp_audio(int numero)
{
    printf
        ("ERREUR: function_dsp_audio non supportee si compilation avec NO_FFT \n");
}
#endif
