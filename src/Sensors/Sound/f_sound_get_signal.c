/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_sound_get_signal f_sound_get_signal

\author previous : Oriane Dermy
\date  20/05/2015

\section Description
This function is based on the copy of the f_sound_get_energy but there, we only give the intensity for 1024 samples in two hears (So 512 samples per hear)

The x size is the number of data receive by the sound (1024)
The y size is either 2 (for left and right) or 1 (mono).
* 
* 
* This function allows the save of data in a file with the link "-f". If this file already exists, it is rewriting.

The function requires a device definition in the .dev file:

\verbatim
<device>
<sound type="alsa" position="left" />
<sound type="alsa" position="right" />
</device>
\endverbatim

\section Internal Tools
-new_sound_get_signal(int)
-function_sound-get_signal(int)
-destroy_sound_get_signal(int)
*/

#include <libx.h>
#include <net_message_debug_dist.h>
#include <dev.h>
#include <device.h>
#include <Components/sound.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define MAXIMUM_SIZE_OF_OPTION 128
#define SOUND_FRAMES_NB 1024

typedef struct box_sound
{
	Device *device;
	Sound **sounds;
	int number_of_sounds;
	int number_of_freq_bands;
    float* spectrum_l;
    float* spectrum_r;
    type_neurone* neurons;
    int enable_itd;
    FILE *fp;
    int nbIt, gpe_supervision, tabs;
}Box_sound;

//~ 
//~ /* Taille de l'en-tête 44 octets */
//~ struct wave {
    //~ /* Le mot "RIFF"
     //~ * La taille du fichier - 8
     //~ * Le mot "WAVE" */
    //~ char riff[4];
    //~ int32_t taille;
    //~ char wave[4];
     //~ 
    //~ /* Le mot "fmt " */
    //~ char fmt[4];
    //~ /* Taille du header jusqu'à "data" */
    //~ int32_t subTaille1;
    //~ /* Format du fichier */
    //~ int16_t formatAudio;
    //~ /* Nombres de canaux */
    //~ int16_t nombreCanaux;
    //~ /* Fréquence d'échantillonnage */
    //~ int32_t freqEch;
    //~ /* ByteRate */
    //~ int32_t ByteRate;
    //~ /* Alignement */
    //~ int16_t align;
    //~ /* Bits par échantillon */
    //~ int16_t bitsParEch;
     //~ 
    //~ /* Le mot "data" et la
     //~ * taille des données */
    //~ char Ndata[4];
    //~ /* Taille des données */
    //~ int32_t subTaille2;
//~ };

void new_sound_get_signal(int index_of_group)
{
	
   	Box_sound *box;
   	char id_of_device[MAXIMUM_SIZE_OF_OPTION], string[255];
	Device *device = NULL;
   	int success, l, i;
	char param[256], *file_name = NULL;
	type_groupe *group;
	//struct wave wav = { "RIFF",  (int32_t) (44-8) /*taille du fichier : il faudra modifier ce nombre à chaque fois*/, "WAVE", "fmt ",
		//(int32_t) 16/*taille header*/,(int16_t)1/*format audio PCM*/,  (int16_t)/*1*/2 /*nb canaux*/, (int32_t) 44100 /*freq echantillonage */, 
		//(int32_t) ((44100*2*16)/8) /*oct/sec = freq*cannaux*bitsparech / 8*/, 
		//(int16_t)((2*16)/ 8)/* alignement = nbOct/ech = nbCan*bit/ech / 8*/, 
		//(int16_t) 16/*nb Bit par ech*/,
		 //"data", (int32_t) 0/* (filesize -44) taille données :nbCan*nbEch*bitParEch /8*/};
	cprints("New_sound_get_signal\n");

	/** On retrouve le groupe de neurones */
   	group = &def_groupe[index_of_group];

	/** On cree la boite et l'associe au groupe */
   	box = ALLOCATION(Box_sound);
	group->data = box;

/**Initialisation **/
	box->fp=NULL;
	box->nbIt= 0;
	box->gpe_supervision = -1;
	box->tabs = -1;
	/******END Init********/
	/******************************************************Links Treatments*************************************/
	dprints("Begin the reading of links\n");

	if (def_groupe[index_of_group].ext == NULL)
	{
		i=0;
		l = find_input_link(index_of_group, i);
		if(l == -1)
		{
			success = 0;
		}
		else
		{
			while(l!= -1)
			{
				if((success = prom_getopt(liaison[l].nom, "id=", id_of_device)) !=0)
				{
					if(success == 2)
					{
						device = dev_get_device("sound", id_of_device);
					}
					else if(success == 1)
					{
						EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, liaison[l].nom);
					}
				}
				if (prom_getopt(liaison[l].nom, "-f", param) != 0)
				{
					sprintf(string, "%s", param);
					file_name = (char *) malloc(sizeof(char) * strlen(string) + 1);
					strcpy(file_name, string);

					/*------------------------------*/
					/* Opening the file for writing (delete previous file with the same name)*/

					if (file_name != NULL)
					{		
						box->fp = fopen(file_name, "wb+");
						if ((box->fp == NULL))
						{
							EXIT_ON_ERROR("ERROR in sound_get_signal(%s): Impossible to create file %s\n", def_groupe[index_of_group].no_name, file_name);
						}
					/*	else
						{
					//		 fwrite(&wav, sizeof(wav), 1, box->fp);
					//		 printf("Fin création entête des fichiers .wav\n");
						}*/
					}
				}
				/* permet d'écrire le numéro du neurone actif du groupe de supervision pour pouvoir étiqueter les données*/
				if(prom_getopt(liaison[l].nom, "-tabs", param) != 0)
				{
					box->tabs =1;
				}
				if(prom_getopt(liaison[l].nom, "-neurone", param) != 0)
				{
					box->gpe_supervision=liaison[l].depart; 
					printf("groupe de supervision : %d\n", box->gpe_supervision);
				}
				l = find_input_link(index_of_group, ++i);

			}
			
		}
		
	}
	/****************************************End Links Treatments**********************************/


	/** On verifie que le nombre de sons de l'appareil equivaut a celui du nombre de neurones du groupe*/
//	if (device->number_of_components != 2) EXIT_ON_ERROR("Device must have 2 sound components (binaural)\n",index_of_group);
	if (group->tailley != 2 && group->tailley != 3) EXIT_ON_ERROR("Group %d, Y size must be 2 (left / right energy per freq. band) or 3 (left/right energy per freq. band + itd)\n",index_of_group);
	box->number_of_sounds = device->number_of_components;
	if (group->tailley == 3)
	{
		box->enable_itd = 1;
    }
	else
	{
		box->enable_itd = 0;
	}


	//cprints("Group %d, X Size : %d -> %d frequency bands\n",index_of_group,group->taillex,group->taillex);
	box->number_of_freq_bands=group->taillex;
	//TODO remplacer par buffersize ill faut que ce soit le même que alsasound !
	if(group->taillex!= SOUND_FRAMES_NB) EXIT_ON_ERROR("Group %d, X size must be 1024 to have the intensity", index_of_group);
	/** On defini les neurones de la boite. */
	box->neurons = &neurone[group->premier_ele];

	/** On defini les moteurs de la boite*/
	box->sounds = (Sound**)device->components;
	/*printf("End of new_sound_get_freq\n");*/

    box->spectrum_l = MANY_ALLOCATIONS(box->number_of_freq_bands,float);
    box->spectrum_r = MANY_ALLOCATIONS(box->number_of_freq_bands,float);

}

void function_sound_get_signal(int index_of_group)
{
	/*printf("f_sound_get_signal\n");*/
	Box_sound *box;
   	void (*get_signal_p)(Sound *,float*,float*, FILE*,  int16_t);
	float* spectrum_l, *spectrum_r, max;
	int j, expression, debut;
   	Sound **sounds;
  	type_neurone *neurons_of_the_box;

	/*printf("	Getting Box_sound\n");*/
	/** On recupere la boite a partir du groupe*/
   	box = (Box_sound*)def_groupe[index_of_group].data;

	/*printf("	Getting Neurons\n");*/
	/** On recupere les neurones de la boite */
	neurons_of_the_box = box->neurons;
	
	/*printf("	Getting Sounds\n");*/
	sounds = box->sounds;
	
    spectrum_l = box->spectrum_l;
    spectrum_r = box->spectrum_r;

    memset(spectrum_l,0,box->number_of_freq_bands*sizeof(float));
    memset(spectrum_r,0,box->number_of_freq_bands*sizeof(float));
	/*printf("	Getting freq. energies:\n");*/
	/** On recupere la position pour chaque moteur */
	
		/*printf("	Getting get_signal function\n");*/
		
		get_signal_p =  sounds[0]->get_signal;
		//on enregistre les données brut
		if(box->gpe_supervision == -1 && box->tabs == -1)
		{
			get_signal_p(sounds[0], spectrum_l, spectrum_r, box->fp, (int16_t) box->gpe_supervision);
		}
		//on enregistre le type de voyelle (stage oriane)
		else /*if(box->gpe_supervision == 1)*/
		{
			debut=def_groupe[box->gpe_supervision].premier_ele;
			max=0.;  	
			expression= -1;
			for(j=0;j<4;j++)
			{
				if(neurone[debut+j].s>max)
				{
					max=neurone[debut+j].s;
					expression=j;
				}
			}
			get_signal_p(sounds[0], spectrum_l, spectrum_r, box->fp, (int16_t) expression);
		}
		//on enregistre le temps de simulation
		/*else
		{
			gettimeofday(&CurrentTime, (void *) NULL);
            fprintf(fp, "%li.%06li ", CurrentTime.tv_sec, CurrentTime.tv_usec);
		}*/
		box->nbIt++;
		if(spectrum_l&&spectrum_r){
			/*printf("Spectrum for sound %d\n",i);*/
			for(j=0;j<def_groupe[index_of_group].taillex;j++){
				neurons_of_the_box[j].s1 = spectrum_l[j];
				neurons_of_the_box[def_groupe[index_of_group].taillex+j].s1 = spectrum_r[j];
			}
		}
}

void destroy_sound_get_signal(int index_of_group)
{
   	Box_sound* box = (Box_sound*)def_groupe[index_of_group].data;    
  //  int32_t val;

	if (box->fp != NULL)
    {
			//val = (int32_t) ((box->nbIt)*1024*2*sizeof(int16_t));
		   ////Il y a le nombre de données qui change
		  //fseek(box->fp, 40, SEEK_SET);
		  ////val= (int32_t) (2*(box->nbIt)*1024*2*sizeof(int16_t));//nbCan*nbEch*bitParEch/8
		  //fwrite(&val, sizeof(int32_t), 1, box->fp);
		  ////idem
		  //fseek(box->fp, 4, SEEK_SET);
		  //val =(int32_t)  (val + 44 -8);
		  //fwrite(&val, sizeof(int32_t), 1, box->fp);
	    fclose(box->fp);
	    box->fp = NULL;
    }


    free(box->spectrum_l);
    free(box->spectrum_r);
   	free(def_groupe[index_of_group].data);
}
