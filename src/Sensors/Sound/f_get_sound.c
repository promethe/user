/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_get_sound.c
\brief

Author: A.HIOLLE
Created: 10/02/2005
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: passer -Xtaille sur le lien pour la taille de la trame
	  non teste encore, fonctionne par defaut avec une taille de 1000
Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>
#include <Sound/Sound.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/


void function_get_sound(int numero)
{
   char *chaine = NULL, *info = NULL;
   int i = 0, taillex = -1, l, link = -1, Gpe_sound = -1;

   dprints("get_sound");
   if (def_groupe[numero].data == NULL)
   {
      l = find_input_link(numero, i);
      while (l != -1)
      {
         if (strncmp(liaison[l].nom, "-X", 2) == 0)
         {
            Gpe_sound = liaison[l].depart;
            link = l;
         }
         i++;
         l = find_input_link(numero, i);
      }

      if (Gpe_sound == -1)
      {
         EXIT_ON_ERROR("liaison avec groupe precedent non trouve");
      }
      else
      {
         chaine = liaison[link].nom;
         if (chaine == NULL)
         {
            printf("getsound : utilisation par defaut, trame de 1000 echantillons\n");
         }                   /* recuperation des infos du lien */
         else
         {
            info = NULL;
            info = strstr(chaine, "-X");
            cprints("info=%s\n", info);

            cprints("length_info=%d", (int) strlen(info));
            if (strlen(info) > 2)
            {
               taillex = atoi(&info[2]);
               cprints("taillex %d\n", taillex);
            }
            else
            {
               cprints("fonctionnement par defaut taillex=1000\n");
               cprints("info=%s\n", info);
               taillex = 1000;
            }

            if (taillex < 1)
            {
               EXIT_ON_ERROR("%s : il faut definir les dimensions des FFT (-X### -Y###)\n",__FUNCTION__);
            }
         }
      }                       /*allocation de la structure audio */
      if ((prom_audio_struct *) def_groupe[numero].audio_ext == NULL)
      {
         cprints("allocation de la structure audio : \n");
         if ((def_groupe[numero].audio_ext =(void *) malloc(sizeof(prom_audio_struct))) == NULL)
         {
            EXIT_ON_ERROR("prom_audio_struct allocation error dans %s \n", __FUNCTION__);
         }
         else
         {
            cprints("structure allouee!\n");
         }
      }

      ((prom_audio_struct *) def_groupe[numero].audio_ext)->taille = taillex;
      ((prom_audio_struct *) def_groupe[numero].audio_ext)->trame = (char *) malloc((taillex) * sizeof(char));
      if (((prom_audio_struct *) def_groupe[numero].audio_ext)->trame == NULL)
      {
         EXIT_ON_ERROR("prom_audio_struct->trame allocation error dans %s \n", __FUNCTION__);
      }


      if ((def_groupe[numero].data = (void *) malloc(sizeof(int))) == NULL)
      {
         EXIT_ON_ERROR("groupe.data allocation error dans %s \n", __FUNCTION__);
      }

      /*stockage du numero de groupeee dans data */
      *(int *) def_groupe[numero].data = Gpe_sound;
   }
   else
   {
      Gpe_sound = *((int *) def_groupe[numero].data);
      dprints("Gpe_sound=%d\n", Gpe_sound);
      if (Gpe_sound == -1)
      {
         EXIT_ON_ERROR("precedent group not stored, ending....\n");
      }
   }

   /*acquisition de la trame audio */

   getbuf(((char *) ((prom_audio_struct *) def_groupe[numero].audio_ext)->trame));

   if ((((prom_audio_struct *) def_groupe[numero].audio_ext)->trame) == NULL)
   {
      EXIT_ON_ERROR("no buffer  null\n");
   }
}
