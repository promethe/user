/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_play_sound
\brief 
\file  
/section Author
Name: Adrien Jauffret
Created: 16/04/2015

\section Description

Permet de jouer des sons avec gstreamer.  
Les sons doivent être au format Ogg/Vorbis pour pouvoir être lu par gstreamer. La prise en compte des .wav et .mp3 sera ajoutée à l'avenir.

Fonctionnement: 

* Si le groupe en entrée ne contient qu'un seul neurone, alors la fonction f_play_sound joue un son spécifique dont le chemin est précisé sur le lien algo avec l'option "-f".
La fonction joue un son lorsque l'activité du neurone d'entrée passe au dessus de 0.5 (détecteur de front montant).
Sur le lien:  -f<path_et_nom_du_fichier>


* Si le groupe en entrée contient plusieurs neurones, alors la fonction f_play_sound joue le son dans la playlist correspondant à l'index du neurone actif dans le groupe d'entrée.
La fonction joue un son lorsque l'activité du neurone d'entrée passe au dessus de 0.5 (détecteur de front montant).
Si le neurone N du groupe d'entrée s'active, alors le Nième son de la playlist est joué.
Le chemin du fichier de playlist est précisé sur le lien algo avec l'option "-f".  
Sur le lien:  -f<path_et_nom_du_fichier>
Le fichier de playlist est simplement un fichier texte listant les chemins d'accès des différents sons, à l'instar du standard de playlist .m3u
Exemple: 
../my_path/my_sound_1.ogg 
../my_path/my_sound_2.ogg 
../my_path/my_sound_3.ogg 
../my_path/my_sound_4.ogg 

etc..


* Si Gstreamer n'arrive pas à lire de sons et tente de passer par jack, c'est qu'il faut installer gstreamer1.0-alsa
sudo apt-get install gstreamer1.0-alsa 

* Si Gstreamer n'arrive pas à lire certains .ogg et renvoi le message "Error: Internal data stream error." c'est que l'encodage du fichier Ogg/Vorbis est mauvais (tags ou bitrate non supporté).
Il faut reconvertir le fichier en .ogg simple au bitrate : 128 ko/s

* Attention, la fonction f_play_sound se sert de ses propres neurones comme mémoire de l'etat précédent des neurones du groupe d'entrée pour réaliser une détection de front montant. 
Pour cela, il faut que le groupe f_play_sound comporte autant de neurones que le groupe d'entrée !
* 


\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <unistd.h>
#include <pthread.h>


/* La lib gstreamer gst.h utilise une variable epsilon comme promethe (conflit). 
 En attendant une solution plus propre, on effectue un "undef" temporaire de la variable, puis on la redéfini juste après l'include de la lib. */
#undef epsilon
#include <gst/gst.h>
#include <glib.h>
#define epsilon 0.00001


/********************************************************************/
typedef struct MyData{
	char file_name[256];
	char playlist_name[256];
	int input_first_ele;
	int input_nbre;
		
	pthread_mutex_t mutex;
		
} MyData; 


/********************************************************************/
static gboolean
bus_call (GstBus     *bus,
          GstMessage *msg,
          gpointer    data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

/********************************************************************/
static void 
on_pad_added (GstElement *element,
              GstPad     *pad,
              gpointer    data)
{
  GstPad *sinkpad;
  GstElement *decoder = (GstElement *) data;

  /* We can now link this pad with the vorbis-decoder sink pad */
  g_print ("Dynamic pad created, linking demuxer/decoder\n");

  sinkpad = gst_element_get_static_pad (decoder, "sink");

  gst_pad_link (pad, sinkpad);

  gst_object_unref (sinkpad);
}

/********************************************************************/
void 
play_sound(int gpe)
{	
	
			MyData *mydata = def_groupe[gpe].data;			
			
			GMainLoop *loop;
			GstElement *pipeline, *source, *demuxer, *decoder, *conv, *sink;
			GstBus *bus;
			guint bus_watch_id;
			
			
			/***** Init Gstreamer *****/
			/* Initialisation */
			gst_init (NULL, &mydata->file_name);

			loop = g_main_loop_new (NULL, FALSE);
			
			/* Create gstreamer elements */
			pipeline = gst_pipeline_new ("audio-player");
			source   = gst_element_factory_make ("filesrc",       "file-source");
			demuxer  = gst_element_factory_make ("oggdemux",      "ogg-demuxer");
			decoder  = gst_element_factory_make ("vorbisdec",     "vorbis-decoder");
			conv     = gst_element_factory_make ("audioconvert",  "converter");
			sink     = gst_element_factory_make ("autoaudiosink", "audio-output");

			if (!pipeline || !source || !demuxer || !decoder || !conv || !sink) {
			 g_printerr ("One element could not be created. Exiting.\n");
			 return -1;
			}
			/**************************/
			

			/* Set up the pipeline */

			/* we set the input filename to the source element */
			g_object_set (G_OBJECT (source), "location", mydata->file_name, NULL);

			/* we add a message handler */
			bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
			bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
			gst_object_unref (bus);


			/* we add all elements into the pipeline */
			/* file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output */
			gst_bin_add_many (GST_BIN (pipeline),
							     source, demuxer, decoder, conv, sink, NULL);

			/* we link the elements together */
			/* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
			gst_element_link (source, demuxer);
			gst_element_link_many (decoder, conv, sink, NULL);
			g_signal_connect (demuxer, "pad-added", G_CALLBACK (on_pad_added), decoder);


			/* Set the pipeline to "playing" state*/
			g_print ("Now playing: %s\n", mydata->file_name);
			gst_element_set_state (pipeline, GST_STATE_PLAYING);


			/* Iterate */
			g_print ("Running...\n");
			g_main_loop_run (loop);
			

			/* Out of the main loop, clean up nicely */
			g_print ("Returned, stopping playback\n");
			gst_element_set_state (pipeline, GST_STATE_NULL);
						
			g_print ("Deleting pipeline\n");
			gst_object_unref (GST_OBJECT (pipeline));
			g_source_remove (bus_watch_id);
			g_main_loop_unref (loop);


			/* On rend la main pour lancer un autre thread */
			pthread_mutex_unlock(&mydata->mutex);
			pthread_exit (0);		

}



/********************************************************************/
void new_play_sound(int gpe)
{	
	MyData *mydata=NULL;

	char param_link[256];

	int deb;
	int dli = -1;
	int i;
	
	deb = def_groupe[gpe].premier_ele;
	dli = find_input_link(gpe, 0);  /* recherche le 1er lien */
	i = 0;


	mydata=(MyData*)malloc(sizeof(MyData));
	if(mydata==NULL) 
	{
		EXIT_ON_ERROR("pb malloc dans %d\n",gpe);
	}

	mydata->input_first_ele = -1;
	mydata->input_nbre = -1;
	mydata->input_nbre = def_groupe[liaison[dli].depart].nbre;
	

	if(mydata->input_nbre == 1)
	{
		while (dli != -1)
		{
			if (prom_getopt(liaison[dli].nom, "-f", param_link) == 2 || prom_getopt(liaison[dli].nom, "-F", param_link) == 2) /* -fpath_et_nom_du_fichier */
			{
				 strncpy(mydata->file_name,param_link, 256);
				 mydata->input_first_ele = def_groupe[liaison[dli].depart].premier_ele;
			}
				
			i++;
			dli = find_input_link(gpe, i);  
		}

		if(mydata->input_first_ele == -1)
			EXIT_ON_ERROR("No input link with option -f (give the name of file to be played) !\n");		
	}
	else
	{
		while (dli != -1)
		{
			if (prom_getopt(liaison[dli].nom, "-f", param_link) == 2 || prom_getopt(liaison[dli].nom, "-F", param_link) == 2) /* -fpath_et_nom_du_fichier */
			{
				 strncpy(mydata->playlist_name,param_link, 256);
				 mydata->input_first_ele = def_groupe[liaison[dli].depart].premier_ele;
				 
			}
			
			i++;
			dli = find_input_link(gpe, i);  
		}

		if(mydata->input_first_ele == -1 || mydata->input_nbre == -1)
			EXIT_ON_ERROR("No input link with option -f (give the name of file to be played) !\n");			
	}
  	   	
  	def_groupe[gpe].data=mydata; /* sauvegarde de My_Data*/
}


/********************************************************************/
void destroy_play_sound(int gpe)
{	
		free(def_groupe[gpe].data);
		def_groupe[gpe].data=NULL;
}


/********************************************************************/
void function_play_sound(int gpe)
{  	
	MyData *mydata = def_groupe[gpe].data;

	int i,j;
	int deb = def_groupe[gpe].premier_ele;
	int loop;
	
	pthread_t gst_thread;
	
	FILE *f;


	if(mydata->input_nbre == 1)
	{
		if(neurone[mydata->input_first_ele].s1 > 0.5)      /* Si l'activité du neurone est supérieure à 0.5 ... */
		{
			if( neurone[deb].s < 0.5 )					   /* ...et que son activité précédente était inférieure à 0.5 (détecteur de front montant).  */
			{
				if(pthread_mutex_trylock(&mydata->mutex) == 0)	   /* Si aucun thread gstreamer n'est déjà lancé */
				{
					/* Creation d'un thread pour executer gstreamer. */
					printf ("Gstreamer thread creation...\n");
					pthread_create(&gst_thread, NULL, play_sound, gpe);	
					
				}
				else
				{
					printf("Gstreamer thread mutex already locked...");
				}					
			}
		}		
	}
	else
	{
		for(i=0 ; i<mydata->input_nbre ; i++)		
		{
			if(neurone[mydata->input_first_ele+i].s1 > 0.5)      /* Si l'activité du neurone est supérieure à 0.5 ... */
			{
				if( neurone[deb+i].s < 0.5 )					   /* ...et que son activité précédente était inférieure à 0.5 (détecteur de front montant).  */
				{
						/************************************************************/ 	 
						f = fopen(mydata->playlist_name, "r");		/* ouverture du fichier .m3u de playlist  */
						if (f == NULL)
						{
								EXIT_ON_ERROR("unable to open file %s \n", mydata->playlist_name);
						}
						
						loop = 1;
						int j=1;
						
						do
						{
							fgets(mydata->file_name, sizeof mydata->file_name, f); /* lecture d'une ligne dans le fichier .m3u */

							if (j == i+1)		/* si le numéro de la ligne extraite est identique a l'index du neurone actif */
							{
								loop = 0;
								
								/*******************************************/
								int k=0;
								for (k=0;k<strlen(mydata->file_name); k++){		/* Suppression du retour chariot en fin de ligne (le retour chariot pose probleme à gstreamer pour la lecture du fichier. */
									if(isspace(mydata->file_name[k]))
										mydata->file_name[k]='\0';
								}								
								/*******************************************/
								
								if(pthread_mutex_trylock(&mydata->mutex) == 0)	   /* Si aucun thread gstreamer n'est déjà lancé */
								{
									/* Creation d'un thread pour executer gstreamer. */
									printf ("Gstreamer thread creation...\n");
									pthread_create(&gst_thread, NULL, play_sound, gpe);
								}													
								else
								{
									printf("Gstreamer thread mutex already locked..."); 	/* Si un thread est déjà lancé, on abandonne la création d'un nouveau thread */
								}
							}
							j++;

						}while(loop);
						
						fclose(f);  
						/************************************************************/ 	
				}
			}
		}	
	}
	
	for(i=0 ; i<mydata->input_nbre ; i++)		
	{	
		neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = neurone[mydata->input_first_ele+i].s1;	/* mise en mémoire de l'état précédent des neurones d'entrée dans les neurones du groupe */
	}

}

