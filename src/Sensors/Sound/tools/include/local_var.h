/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _SOUND_LOCAL_VAR_H
#define _SOUND_LOCAL_VAR_H
#include <Sound/Sound.h>
#include <sys/types.h>


extern int mrtype;
extern char *name;
extern int __attribute__ ((unused)) timelimit;
extern int __attribute__ ((unused)) quiet_mode;
extern int __attribute__ ((unused)) file_type;
extern unsigned int __attribute__ ((unused)) sleep_min;
extern int __attribute__ ((unused)) open_mode;

extern snd_pcm_stream_t __attribute__ ((unused)) stream;
extern int __attribute__ ((unused)) mmap_flag;
extern int __attribute__ ((unused)) interleaved;
extern int __attribute__ ((unused)) nonblock;
extern u_char * __attribute__ ((unused)) audiobuf;
extern snd_pcm_uframes_t __attribute__ ((unused)) chunk_size;
extern unsigned __attribute__ ((unused)) period_time;
extern unsigned __attribute__ ((unused)) buffer_time;
extern snd_pcm_uframes_t __attribute__ ((unused)) period_frames;
extern snd_pcm_uframes_t __attribute__ ((unused)) buffer_frames;
extern int __attribute__ ((unused)) avail_min;
extern int __attribute__ ((unused)) start_delay;
extern int __attribute__ ((unused)) stop_delay;
extern int __attribute__ ((unused)) verbose;
extern int __attribute__ ((unused)) buffer_pos;
extern size_t
    __attribute__ ((unused)) bits_per_sample,
    __attribute__ ((unused)) bits_per_frame;
extern size_t __attribute__ ((unused)) chunk_bytes;
extern snd_output_t *mylog;

extern int sample_size;
extern int __attribute__ ((unused)) fd;
extern off64_t
    __attribute__ ((unused)) pbrec_count, __attribute__ ((unused)) fdcount;
extern int
    __attribute__ ((unused)) vocmajor, __attribute__ ((unused)) vocminor;

#endif
