/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file  
\brief 

Author: A.HIOLLE
Created:03/2005
Modified:
- author: A.HIOLLE
- description: ajout de st��
- date: 05/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:  execute une fft 1d sur la trame du groupe pr��ent

	Si audio st��, le groupe pr�e�ent poss�e 2 structures audio
	mettre S0 ou S1 pour executer sur la premiere ou la deuxi�e....

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#include <string.h>
#ifndef NO_ALSA
#include <Sound/Sound.h>
#endif
#include <public_tools/Vision.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>

/* #define DEBUG */


void function_fft_audio_execute(int numero)
{
#ifndef NO_ALSA
    int l = -1, link = -1;
    int i = 0;
    int Gpe_sound = -1, Gpe_init = -1;  /*groupe precedent */
    float mean = 0.;            /*moyenne de la trame */
    float temp = 0.;            /*variable temporaire pour �iter les cats multiples */
    char *buf;                  /*pointeur intermediaire pour plus de lisibilite du code */
    char *chaine = NULL;

#ifdef DEBUG
    printf("fft execute\n");
#endif
    if (def_groupe[numero].data == NULL)
    {
        /* recherche du lien en entree */
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strncmp(liaison[l].nom, "sound", 5) == 0)
            {
                Gpe_sound = liaison[l].depart;
                link = l;
            }
            if (strncmp(liaison[l].nom, "fft_1d", 6) == 0)
            {
                Gpe_init = liaison[l].depart;
            }
            i++;
            l = find_input_link(numero, i);
        }
#ifdef DEBUG
        printf("link sound=%d", Gpe_sound);
        endl;
        printf("link init=%d", Gpe_init);
        endl;
#endif

        /*Sortie si le lien n'est pas trouve */
        if (Gpe_sound == -1)
        {
            printf("liaison avec groupe sound non trouve");
            endl;
            exit(-1);
        }
        chaine = liaison[link].nom;
        if (chaine == NULL)
        {
            printf("numero : %d : %s : pas de lien entrant...\n", numero,
                   __FUNCTION__);
            exit(0);
        }

        /* recuperation du numero de port sur le lien */
        /*recopie du pointeur sur la structure audio du groupe precedent */
        if (def_groupe[Gpe_init].audio_ext != NULL)
        {
#ifdef	DEBUG
            printf("recopie de la structure audio du groupe %d\n", Gpe_sound);
#endif
            if ((def_groupe[numero].audio_ext =
                 (void *) malloc(sizeof(fft_1d_audio))) == NULL)
            {
#ifdef DEBUG
                pr("allocation de la structure fft_1d\n");
#endif
                perror("fft_1d_audio allocation error\n");
                printf(" dans %s \n", __FUNCTION__);
            }

            /* Recopie de la structure fft_1d du groupe init_fft_audio*** */
            if (memcpy
                (def_groupe[numero].audio_ext, def_groupe[Gpe_init].audio_ext,
                 sizeof(fft_1d_audio)) == NULL)
            {
                perror("impossible de copier la structure fft_1d_audio");
                printf("dans %s \n", __FUNCTION__);
                exit(-1);
            }
            else
            {
#ifdef DEBUG
                pr("recopie reussie");
                endl;
#endif
            }
        }
        else
        {
            printf("no fft_1d structure in groupe : %d\n", Gpe_init);
            exit(-1);
        }
        if ((def_groupe[numero].data =
             (void *) malloc(2 * sizeof(int))) == NULL)
        {
            perror("allocation error in fft_audio_excute\n");
            exit(-1);
        }

        printf("...1\n");
        /*stockage des numeros de groupe dans data */
        ((int *) (def_groupe[numero].data))[0] = Gpe_sound;
        ((int *) (def_groupe[numero].data))[1] = Gpe_init;
        printf("...2\n");
    }
    else
    {                           /* les donnees on normalement  etee initialisees */
        Gpe_sound = ((int *) def_groupe[numero].data)[0];
        Gpe_init = ((int *) def_groupe[numero].data)[1];
#ifdef DEBUG
        printf("Gpe_get_sound=%d\n", Gpe_sound);
#endif
        if (Gpe_sound == -1)
        {
            perror("Gpe_sound not stored, ending....\n");
            exit(-1);
        }
        if (Gpe_init == -1)
        {
            perror("Gpe_init fft not stored, ending....\n");
            exit(-1);
        }
    }

    /*pour plus de lisibilite et eviter les cast multiples buf pointe vers la trame */
    buf = ((prom_audio_struct *) def_groupe[Gpe_sound].audio_ext)->trame;
    if (buf == NULL)
    {
        printf("trame is NULL in %s\n", __FUNCTION__);
        exit(-1);
    }
    for (i = 0;
         i < ((fft_1d_audio *) def_groupe[numero].audio_ext)->taille - 1; i++)
    {                           /*conversion de la trame en float       */
        temp = buf[i];
        (((fft_1d_audio *) def_groupe[numero].audio_ext)->in[i]) =
            (float) (temp);

        /*calcul de la moyenne sur la trame */
        mean += ((fft_1d_audio *) def_groupe[numero].audio_ext)->in[i];
    }
    /*normalisation de la moyenne */
    mean = mean / ((fft_1d_audio *) def_groupe[numero].audio_ext)->taille;
    /*On soustrait la moyenne pour diminuer la composante principale de la fft */

    for (i = 0;
         i < ((fft_1d_audio *) def_groupe[numero].audio_ext)->taille - 1; i++)
    {
        ((fft_1d_audio *) def_groupe[numero].audio_ext)->in[i] -= mean;
    }
    /*test de l'initialisation du plan */
    if ((((fft_1d_audio *) def_groupe[numero].audio_ext)->fft_plan) == NULL)
    {
        perror("fft_plan is not initialised\n");
        exit(-1);
    }
    else
    {
        fftwf_execute(*
                      (((fft_1d_audio *) def_groupe[numero].audio_ext)->
                       fft_plan));
    }
#else
    printf("pas de GESTION de ALSA prevue \n");
#endif
}
