/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_assoc_timing.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
fonction gardant sur les neurones les dernieres activites les plus importantes
pour eviter les probleme de simultane�te entre stimulis
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Sound/Sound.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <math.h>
/*#define DEBUG*/
static int tps = 0;
static int gap = 0;
static int l = -1;
void function_assoc_timing(int numero)
{
    int i = 0;
    float alpha = 0.8;
    int Gpe_sound = -1, Gpe_mean = -1;

    float tau = exp(-(gap / def_groupe[numero].seuil));


    printf("assoc timing %d\n", tps);
    tps++;
    if (l == -1)
    {
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "memory") == 0)
            {
                Gpe_sound = liaison[l].depart;
            }
            /*if(strcmp(liaison[l].nom,"mean")==0)
               {
               Gpe_mean=liaison[l].depart;
               } */
            i++;
            l = find_input_link(numero, i);

        }
    }
    if (Gpe_sound == -1)
    {
        printf("groupe mem_non trouve\n");
        exit(0);
    }

    /*
       if(Gpe_mean==-1){

       printf("groupe mean_non trouve");
       //exit(0);
       } */

    for (i = 0; i < def_groupe[numero].nbre; i++)
    {

        if ((neurone[def_groupe[Gpe_sound].premier_ele + i].s >
             neurone[def_groupe[numero].premier_ele + i].s)
            || (gap > def_groupe[numero].seuil))
        {
            if (Gpe_mean != -1)
            {
                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s -
                    (alpha * neurone[def_groupe[Gpe_mean].premier_ele + i].s);
                if (neurone[def_groupe[numero].premier_ele + i].s < 0)
                    neurone[def_groupe[numero].premier_ele + i].s = 0;
                neurone[def_groupe[numero].premier_ele + i].s1 =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s1 -
                    (alpha *
                     neurone[def_groupe[Gpe_mean].premier_ele + i].s1);
                if (neurone[def_groupe[numero].premier_ele + i].s1 < 0)
                    neurone[def_groupe[numero].premier_ele + i].s1 = 0;
                neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s2 -
                    (alpha *
                     neurone[def_groupe[Gpe_mean].premier_ele + i].s2);
                if (neurone[def_groupe[numero].premier_ele + i].s2 < 0)
                    neurone[def_groupe[numero].premier_ele + i].s2 = 0;
            }
            else
            {
                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s;
                neurone[def_groupe[numero].premier_ele + i].s1 =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s1;
                neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[Gpe_sound].premier_ele + i].s2;
            }

        }
        else
        {

            neurone[def_groupe[numero].premier_ele + i].s =
                tau * (neurone[def_groupe[numero].premier_ele + i].s);
            neurone[def_groupe[numero].premier_ele + i].s1 =
                tau * neurone[def_groupe[numero].premier_ele + i].s1;
            neurone[def_groupe[numero].premier_ele + i].s2 =
                tau * neurone[def_groupe[numero].premier_ele + i].s2;


        }
    }

    if (gap > def_groupe[numero].seuil)
    {

        gap = 0;
    }
    else
    {
        gap++;
    }

}
