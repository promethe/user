/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 

TODO DOC

PAS DU TOUT SAFE !! NE PAS UTILISER

\file  

************************************************************/
/*#define DEBUG*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Sound/Sound.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>

static int tps = 0;
void function_traite_context(int gpe)
{
    type_coeff *synapse;

    float dc = 0. /*, act */ ;
    float input_value;
    int size_Y = def_groupe[gpe].tailley;
    int size_X = def_groupe[gpe].taillex;
    int first_neuron = def_groupe[gpe].premier_ele;
    int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
    int i=0, j = 1, Gpe_ir=-1, Gpe_cr=-1;
    int l = -1;
    int rep[4];

    tps++;
    if (l == -1)
    {
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "IR") == 0)
            {
                Gpe_ir = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "CR") == 0)
            {
                Gpe_cr = liaison[l].depart;
            }
            i++;
            l = find_input_link(gpe, i);

        }
    }
    if( Gpe_cr==-1 || Gpe_ir == -1 )
      EXIT_ON_ERROR("Gpe_cr or Gpr_ir not initialized\n");

    for (i = 0; i < 4; i++)
    {

        rep[i] =
            neurone[def_groupe[Gpe_cr].premier_ele + i].s -
            neurone[def_groupe[Gpe_ir].premier_ele + i].s;

    }
    j = 0;
    for (i = first_neuron; i < last_neuron; i++)
    {
        /*mise a jour des poids sur les entrees C */
        synapse = neurone[i].coeff;

        while (synapse != NULL)
        {
          input_value = neurone[synapse->entree].s1;

            if (synapse->evolution == 1)
            {
                switch (liaison[synapse->gpe_liaison].mode)
                {

                case 0:        /*liens produit */
                    dc = eps * rep[j] * input_value;
                    j++;

                    break;

                case 2:        /*liens produit */
                    dc = eps * rep[j] * input_value;
                    j++;
                    break;
                }

                synapse->val = synapse->val + dc;


            }
            synapse = synapse->s;
        }
        synapse = neurone[i].coeff;

        /*while (synapse != NULL)
           {
           save_weight(synapse->val,j);
           synapse=synapse->s;
           j++;
           }
           synapse=neurone[i].coeff;
         */
    }
}
