/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_IR_simul.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
simulation des IR sur 4 neurones representant droite gauche avant arriere
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Sound/Sound.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/
static int tps = 0;
void function_compute_cr(int numero)
{
   int i = 0, Gpe_asso=-1, Gpe_context=-1;
   int l = -1;
   int rep[4];
   int numsynapse = 0;         /* numero de synapse pour la sauvegarde */

   tps++;
   numsynapse = 0;
   if (l == -1)                             /* PG: attention ecriture inefficace: utiliser un new a la place */
   {
      l = find_input_link(numero, i);
      while (l != -1)
      {
         if (strcmp(liaison[l].nom, "assoc") == 0)
         {
            Gpe_asso = liaison[l].depart;
         }
         if (strcmp(liaison[l].nom, "context") == 0)
         {
            Gpe_context = liaison[l].depart;
         }
         i++;
         l = find_input_link(numero, i);

      }
   }


   if (Gpe_asso==-1) EXIT_ON_ERROR("no link assoc\n");
   if (Gpe_context==-1) EXIT_ON_ERROR("no link context\n");


   for (i = 0; i < def_groupe[numero].nbre; i++)
   {
      rep[i] =neurone[def_groupe[Gpe_asso].premier_ele + i].s - neurone[def_groupe[Gpe_context].premier_ele + i].s;

      neurone[def_groupe[numero].premier_ele + i].s =  neurone[def_groupe[numero].premier_ele + i].s1 = neurone[def_groupe[numero].premier_ele + i].s2 = rep[i];
   }

}
