/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number

Macro:
-TAILLEX

Local varirables:
-float posx
-float pos.y

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>
#include <Struct/print_info_data.h>
#define DECALAGE_X 0
#define DECALAGE_Y 550
#define DECALAGE_X1 550
#define DECALAGE_Y1 550
#ifndef AVEUGLE
void draws_vector_and_act(TxDonneesFenetre ima, TxPoint org, TxPoint dest)
{
    TxDessinerSegment(&ima, /*colour */ 4000,
                      org, dest, 1 /*epaisseur */ );
    TxDessinerCercle(&ima, 4000, 1 /*plein */ , dest, 2 /*rayon */ ,
                     1 /*epaisseur */ );
}
#endif


void function_print_vector_explo_from_print_info(int gpe)
{
#ifndef AVEUGLE
    TxPoint point, point2;
    char retour[255];
    int i, k, colour, posxx, posyy, num, nx, ny, posxxx, posyyy, tnum, posxxxx,
        posyyyy;
    int val;
    int va[1200][8];
    FILE *f;
    /* print_info_data *p = 0;*/
    val = trouver_entree(gpe, "print_info");
    if (val != -1)
    {

        /*initialise le tableau */
        for (i = 0; i < 1200; i++)
            for (k = 0; k < 4; k++)
                va[i][k] = -1;

        /*Redessine l'env */
        strcpy(retour, "robot_pos");
        f = fopen(retour, "r");
        if (f != NULL)
        {
            do
            {
                val =
                    fscanf(f, "	 %d, %d, %d,%d,%d,%d,%d,%d", &posxxx,
                           &posyyy, &colour, &posxx, &posyy, &num, &nx, &ny);
                /* decommentez si utilise fichier genere par print_place au lieu de print_info
                   val=  fscanf(f,"%d, %d, %d,%d,%d",&posxx,&posyy,&colour,  &posxx,  &posyy);
                 */
                /*affiche 1ere image pour vecteur a partir de l'org de la cel */
                point.x = posxx + DECALAGE_X;
                point.y = posyy + DECALAGE_Y;
                TxDessinerRectangle_rgb(&image1, colour, TxPlein, point, pas,
                                        pas, 1);
                /*affiche 1eme image pour vecteur a partir de la pos de creation */
                point.x = posxx + DECALAGE_X1;
                point.y = posyy + DECALAGE_Y1;
                TxDessinerRectangle_rgb(&image1, colour, TxPlein, point, pas,
                                        pas, 1);
            }
            while (val != EOF);
            printf("fini de charger l'image1\n");
            fclose(f);
            TxFlush(&image1);
        }

        /*affiche par dessus les vecteurs appris */
        strcpy(retour, "robot_vect");
        f = fopen(retour, "r");
        if (f != NULL)
        {
            printf("fichier robot_vect found\n");
            do
            {
                /*on stoque dans un tableau les coordonnees des vecteurs a l'indice de la transition correspondante.
                   Comme le vecteur d'une trans change au cours du temps on ecrase au fur et a mesure les valeur precedente et on ne conserve que la
                   derniere valeur connu pour ce vecteur... */
                val =
                    fscanf(f, "	 %d, %d, %d, %d,%d,%d,%d", &tnum,
                           &posxx, &posyy, &posxxx, &posyyy, &posxxxx,
                           &posyyyy);
                /*le point d'origine est le lieux d'apprentissage de la cel de lieu d'origine... */
                va[tnum][0] = posxx + DECALAGE_X;
                va[tnum][1] = posyy + DECALAGE_Y;
                va[tnum][2] = posxxx + DECALAGE_X;
                va[tnum][3] = posyyy + DECALAGE_Y;
                /*le point d'origine du vecteur est la pos du robot */
                va[tnum][4] = posxxxx + DECALAGE_X1;
                va[tnum][5] = posyyyy + DECALAGE_Y1;
                /*meme destination */
                va[tnum][6] = posxxx + DECALAGE_X1;
                va[tnum][7] = posyyy + DECALAGE_Y1;
            }
            while (val != EOF);
            if (f != NULL)
                fclose(f);
            printf("fini de charger vect1\n");
            /*on stoque dans ce fichier les vecteurs stabilises (dernier connu) */
            strcpy(retour, "robot_finalv");
            f = fopen(retour, "a");
            for (i = 0; i < 1200; i++)
            {
                if (va[i][0] != -1)
                {
                    point.x = va[i][0];
                    point.y = va[i][1];
                    point2.x = va[i][2];
                    point2.y = va[i][3];
                    draws_vector_and_act(image1, point, point2);
                    if (f != NULL)
                    {
                        fprintf(f, "	 %d, %d, %d,%d\n", point.x,
                                point.y, point2.x, point2.y);
                    }
                    point.x = va[i][4];
                    point.y = va[i][5];
                    point2.x = va[i][6];
                    point2.y = va[i][7];
                    draws_vector_and_act(image1, point, point2);
                    if (f != NULL)
                    {
                        fprintf(f, "	 %d, %d, %d,%d\n", point.x,
                                point.y, point2.x, point2.y);
                    }
                }
            }
            if (f != NULL)
                fclose(f);
            TxFlush(&image1);
        }
    }
#else
    (void)gpe;
#endif
    printf("fini_fvect\n");
}
