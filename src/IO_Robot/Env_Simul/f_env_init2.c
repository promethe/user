/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_env_init2 f_env_init2
\brief


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$

\section Description
 L'init de l'environnement n'est faite qu'a la premiere iter des simul avec promethe


\section Macro
-TAILLE_X

\section Local variables
-float pas
-int NBLAND;			Nbre de landmarks
-char nom_fichier_cfg[32];
-int mode;

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "tools/include/macro.h"

#include "tools/include/local_var.h"

#include "tools/include/calcule_nbptessentiels_dans_fichier.h"
#include "tools/include/alloue.h"
#include "tools/include/init_obstacle.h"
#include "tools/include/init_aleat_table_terrain.h"
#include "tools/include/mise_a_echelle_cases_vers_pix.h"

#include "tools/include/nomprs.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG
void init2(int);
void init_position_de_depart2(float *px, float *py,int);


#define STRING_SIZE 256
void function_env_init2(int numero)
{
   char st[STRING_SIZE],param_link[STRING_SIZE];
   FILE *f;
   int i=0;
   int init_pos=0;
   int l,gpe_init=-1;
   int test;

   if (def_groupe[numero].data==NULL)
   {
      dprints("Environment init taillex= %d\n", TAILLEX);
      mode = 1;
      NBLAND = 0;
      l=find_input_link(numero,i);
      while (l!=-1)
      {
         if (prom_getopt(liaison[l].nom, "c", param_link) == 2)
         {
            strcpy(nom_fichier_cfg, param_link);
            nomprs();
            dprints(" nom du fichier de config  = %s \n", nom_fichier_cfg);

         }
         if (strcmp(liaison[l].nom,"init_position")==0)
         {
            gpe_init=liaison[l].depart;
         }
         i++;
         l=find_input_link(numero,i);
      }

      /*-----------------------------------------------------------------*/
      /*ouverture du fichier qui contient l'information                  */
      /* dans une grille 40x40 avec comme valeur N ou E pour coder       */
      /* un pt de Nourriture ou un point d'eau                           */
      /*-----------------------------------------------------------------*/
      if (gpe_init!=-1)
         if (neurone[def_groupe[gpe_init].premier_ele].s1>0.5)
         {
            init_pos=1;
         }

      f = fopen(nom_fichier_cfg, "r");
      if (f == NULL)
      {
         EXIT_ON_ERROR("f_init_env2: cannot open %s\n", nom_fichier_cfg);
      }

      while (feof(f))
      {
         test = fscanf(f, "%s", st);
         if (test != 1) {cprints("Erreur de lecture fscanf");}
         i++;
      }
      dprints("nbcase = %d\n", i);
      fclose(f);
      f = fopen(nom_fichier_cfg, "r");
      if (fgets(st, STRING_SIZE, f))
      {nb_cases = strlen(st) / 2;}
      else {cprints("Erreur de lecture fgets");}
      fclose(f);
      dprints("nb cases: %d\n", nb_cases);
      pas = (float) TAILLEX / nb_cases;

      /*on lance alors la fonction 'init' */

      init2(init_pos);
   }
}

void init2(int init_pos)
{
   int i = 0, p;
   FILE * fp;
   int test=-1;

   /*-------------------------------------*/
   /*Calcul du nombre de points essentiels */
   i = calcule_nbptessentiels_dans_fichier();

   /*i = nb_pts_essentiels_max; */

   /*si il existe un seul point defini dans le fichier texte */
   /* on ne met plus les points aleatoirement dans le terrain */
   if (i != 0)
   {
      nbptessentiel = i;
      cprints("%d variables essentiels sur le terrain\n", nbptessentiel);
   }
   else
   {
      nbptessentiel = 0;
      cprints("Aucune ressource sur le terrain\n");
   }

   /*Le placement des sources ne sera pas aleatoire*/
   aleat_table_terrain = 0;

   /*------------------------*/
   /*allouer toute la memoire */
   alloue((char*)"tout");
   dprints("table_land et table_terrain alloued\n");

   /*  printf("entree une valeur: ");
      scanf("%d",&seed); */
//   srand48((unsigned) time(NULL));

   /*Rempli table_land*/
   init_obstacle();

   if (continue_simulation_status==START)
   {
      /*rempli table_terrain*/
      /*force init dans init_ptessentiel*/
      for (i = 0; i < nbptessentiel; i++)
      {
         /*met la cellule associe et le y a 0 */
         table_terrain[i][4] = 0.;
         table_terrain[i][1] = 0.;
      }
      init_aleat_table_terrain();
      /*Aucune source connue (si online_learning)*/
      for (i = 0; i < NBVARESSENTIEL; i++)
         type_src_known[i] = 0;
   }
   else
   {
      fp=fopen("sources.def","r");
      if (fp==NULL)
      {
         EXIT_ON_ERROR("impossible d'ouvrir sources.def\n");
      }
      for (i=0; i<NBVARESSENTIEL; i++)
         test=fscanf(fp,"%d ",&(type_src_known[i]));

      for (i=0; i<nbptessentiel; i++)
      {
         test=fscanf(fp,"%f %f %f %f %f \n",&(table_terrain[i][0]),&(table_terrain[i][1]),&(table_terrain[i][2]),&(table_terrain[i][3]),&(table_terrain[i][4]));
         if (test == 0) {cprints("Erreur de lecture fscanf");}
      }
      for (i=0; i<NBVARESSENTIEL; i++)
      {
         test=fscanf(fp,"%f %f %f %f %f %f \n",&(param_varessentiel[i][0]),&(param_varessentiel[i][1]),&(param_varessentiel[i][2]),&(param_varessentiel[i][3]),&(param_varessentiel[i][4]),&(param_varessentiel[i][5]));
         if (test == 0) {cprints("Erreur de lecture fscanf");}
      }
   }

   if (test != 1) {cprints("Erreur de lecture fscanf");}
   init_position_de_depart2(&posx, &posy, init_pos);
   dprints("INIT2 POS DEPART \n");

   /*initialisation mem pour graphe */
   for (i = 0; i < NBVARESSENTIEL; i++)
      for (p = 0; p < NBMEM; p++)
      {
         mem[i][p] = param_varessentiel[i][0];
      }

}

void init_position_de_depart2(float *px, float *py,int init_pos)
{
   int i, j;
   char st[2];
   FILE *f;
   int test;

   /*----------------------------------------------- ----------------------------*/
   /*ouverture du fichier qui contient l'information sur l environnement        */
   /* dans une grille 40x40 avec comme valeur X pour coder la position de depart */
   /*---------------------------------------------------------------------------*/

   f = fopen("robot_position.def", "r");
   if (continue_simulation_status==CONTINUE && f!=NULL && init_pos!=1)
   {
      test=fscanf(f,"%f %f",px,py);
      if (test)
      {dprints("px=%f py=%f\n",*px,*py);}
      fclose(f);
   }
   else
   {
      f = fopen(nom_fichier_cfg, "r");
      if (f == NULL)
      {
         EXIT_ON_ERROR("Environment map for init config %s is not in the current directory in %s\n",nom_fichier_cfg, __FUNCTION__);
      }

      for (i = 0; i < nb_cases; i++)
      {
         for (j = 0; j < nb_cases; j++)
         {
            test=fscanf(f, "%s ", st);
            if (test == 0) {cprints("Erreur de lecture fscanf");}
            /*landmark */
            if ((strcmp(st, "X")) == 0)
            {
               (*px) = mise_a_echelle_cases_vers_pix(j);
               (*py) = mise_a_echelle_cases_vers_pix(i);
               break;
            }
         }
         test=fscanf(f, "\n");
         if (test == 0) {cprints("Erreur de lecture fscanf");}
      }
      fclose(f);
   }
}

