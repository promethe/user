/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_simul_robot_perception f_simul_robot_perception
 

\section Modified 
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description




\section Macro
-none 
\section Local variables
-float posx
-float posy
-float pas
-float dx_mem
-int nb_pts_essentiel_courant
-int nbptessentiel
-float temps
-int graphiques


\section Global variables
-none

\section Internal Tools
-init_aleat_table_terrain()
-affiche_obstacle()
-mise_a_echelle_cases_vers_pix()
-affiche_simul()
-affiche_robot()
-checker_landmarks_visibles.()
-reconnaissance()
-mise_a_jour_var_essentiel()
-mise_a_jour_graph_eq()

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/

#include <libx.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <Kernel_Function/trouver_entree.h>
#include "tools/include/mise_a_jour_var_essentiel.h"
#include "tools/include/mise_a_jour_graph_eq.h"
/*-------------------------------------------------------------------------*/
/*#define DEBUG*/

void function_simul_drive_expected(int numero)
{
    int deb, nbre, i, test;
    deb = def_groupe[numero].premier_ele;
    nbre = def_groupe[numero].nbre;
    if (nbre != NBVARESSENTIEL)
    {
        EXIT_ON_ERROR
            ("Erreur le groupe %d ne contient pas le bon nombre de neurone: %d, souhaite:%d\n",
             numero, nbre, nbptessentiel);

    }

    cprints("entre ds expect\n");


    /*l'activite des neurones refletes la motivation */
    for (i = deb; i < deb + nbre; i++)
    {
        /*si au moins une src de ce type est connue... */
        if (type_src_known[i - deb] > 0)
        {
            if (param_varessentiel[i - deb][0] <
                param_varessentiel[i - deb][3])
            {
                neurone[i].s = neurone[i].s1 = neurone[i].s2 = 1;
                cprints
                    ("\n\n		ENTREZ EN PLANIF: val:%f, seuil:%f, i:%d\n",
                     param_varessentiel[i - deb][0],
                     param_varessentiel[i - deb][3], i);
                test=scanf("%d", &deb);
                if(test == 0){cprints("Erreur de lecture du scanf dans f_simul_drive_expected");}
            }
            else
                neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
        }
        else
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
    }
    if (graphiques == 1)
    {
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
        mise_a_jour_graph_eq(temps);
#endif

    }
    cprints("sortie ds expect\n");
}
