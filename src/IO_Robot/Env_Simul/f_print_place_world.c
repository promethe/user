/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libIO_Robot
\defgroup f_print_place_world f_print_place_world
\brief 


\section Author
- name: xxxxxxxx
- Created: XX/XX/XXXX
\section Modified
- author: J. Hirel
- description: specific file creation
- date: 16/02/2009

\section Modified
- author: S. Hanoune
- description: Modification de printf, rajout de l'affichage "Door" dans un World. A "Door" est un obstacle, forcement horizontal ou verticla (A modifier), que l'on peut traverser que dans une seule direction: 1 nort->sud (horizontale), 2 est->ouest (verticale),  sud->nord(horizontale),ouest->est (verticale). Les IR ne detecte la porte que dans le sens inverse de l'ouverture
- date: 24/08/2014

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Prints the current robot location in with a color associated with the winning place cell.
 An optionnal argument can be given to specify the simulated world (if any) to draw. 

\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/



/*#define DEBUG*/
#include <libx.h>
#include <stdio.h>
#include <string.h> 
#include <stdlib.h>


#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif
#include "tools/include/display_world.h"



#ifndef AVEUGLE
typedef struct mydata_print_place_world
{
      int erase;
      int gpe_PC;
      int gpe_pos;
      World *world;
      TxDonneesFenetre *window;
} mydata_print_place_world;
#endif

void new_print_place_world(int gpe)
{
#ifndef AVEUGLE
   char world_name[255] = "\0", retour[255];
   int i, l, gpe_PC = -1, gpe_pos = -1;
   int erase = 0;
   mydata_print_place_world *my_data = NULL;
   World *world = NULL;
   TxDonneesFenetre *window = &image1;

   if (def_groupe[gpe].data == NULL)
   {
      
      l = 0;
      i = find_input_link(gpe, l);
      while (i != -1)
      {
	 if (prom_getopt(liaison[i].nom, "r",retour) == 1)
	 {
	    erase = 1;
	 }

	 if (strcmp(liaison[i].nom, "PC") == 0)
	 {
	    gpe_PC = liaison[i].depart;
	 }

	 if (strcmp(liaison[i].nom, "pos") == 0)
	 {
	    gpe_pos = liaison[i].depart;
	 }

	 if (prom_getopt(liaison[i].nom, "w", retour) == 2)
	 {
	    sprintf(world_name, "%s", retour);
	 }

	 if (prom_getopt(liaison[i].nom, "i", retour) == 2)
	 {
	    i = atoi(retour);
	    if (i == 1)
	    {
	       window = &image1;
	    }
	    else if (i == 2)
	    {
	       window = &image2;
	    }
	 }

	 l++;
	 i = find_input_link(gpe, l);
	    
      }
      
      if (gpe_pos == -1)
      {
	 EXIT_ON_ERROR("Missing 'pos' input link");
      }

      my_data = ALLOCATION(mydata_print_place_world);

      if (strlen(world_name) > 0)
      {
	 world = world_get_world_by_name(world_name);
	 
	 dprints("new_print_place_world(%s): Getting world '%s'\n", def_groupe[gpe].no_name, world_name);
	 if (world == NULL)
	 {
	    EXIT_ON_ERROR("ERROR in new_print_place_world(%s): Could not find world '%s'\n", def_groupe[gpe].no_name, world_name);
	 }

	 display_world(window, world);
      }
      
      my_data->erase = erase;
      my_data->world = world;
      my_data->gpe_PC = gpe_PC;
      my_data->gpe_pos = gpe_pos;
      my_data->window = window;
      def_groupe[gpe].data = my_data;
   }
#endif
(void) gpe;
}

void function_print_place_world(int gpe)
{
#ifndef AVEUGLE
    TxPoint point;
    float max;
    float robot_posx, robot_posy;
    int i, deb_entree = -1, nbre_entree, no_max = -1, gpe_PC = -1, gpe_pos = -1;
    TxCouleur color = {0, 0, 0, 0};
    TxDonneesFenetre *window = NULL;
    mydata_print_place_world *my_data = (mydata_print_place_world *) def_groupe[gpe].data;;

    if (my_data == NULL)
    {
       EXIT_ON_ERROR("Cannot retrieve data");
    }

    gpe_PC = my_data->gpe_PC;
    gpe_pos = my_data->gpe_pos;
    window = my_data->window;

    robot_posx = neurone[def_groupe[gpe_pos].premier_ele].s1;
    robot_posy = neurone[def_groupe[gpe_pos].premier_ele+1].s1;;

    point.x = (int) robot_posx - 5;
    point.y = (int) robot_posy - 5;

    dprints("f_print_place_world(%s): Position of robot to print : %f, %f\n", def_groupe[gpe].no_name, robot_posx, robot_posy);

    if (gpe_PC >= 0)
    {
       deb_entree = def_groupe[gpe_PC].premier_ele;
       nbre_entree = def_groupe[gpe_PC].nbre;

       max = -1;
       for (i = deb_entree; i < (deb_entree + nbre_entree); i++)
       {
	  if (neurone[i].s1 > def_groupe[gpe_PC].seuil && neurone[i].s1 > max)
	  {
	     max = neurone[i].s1;
	     no_max = i;
	  }
       }

       if (no_max >= 0)
       {
	  memcpy(&color, &couleurs[17 + (no_max % 20)], sizeof(GdkColor));
       }

       if (max > 1)
       {
	  max = 1;
       }

       if (max < 0)
       {
	  max = 0;
       }

       color.red += (1 - max) * (MAX_VALUE_GDKCOLOR - color.red);
       color.blue += (1 - max) * (MAX_VALUE_GDKCOLOR - color.blue);
       color.green += (1 - max) * (MAX_VALUE_GDKCOLOR - color.green);
    }

    TxDessinerRectangle_color(window, &color, TxPlein,point, 5, 5, 1);

    if (no_max >= 0 && neurone[no_max].s1 > 0.995)
    {
        TxDessinerRectangle(window, noir, TxVide, point, 2, 2, 2);
    }

    TxFlush(window);
#endif
(void) gpe;
}

void destroy_print_place_world(int gpe)
{
#ifndef AVEUGLE
   if (def_groupe[gpe].data != NULL)
   {
      free(((mydata_print_place_world*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_print_place_world(%s): Leaving function\n", def_groupe[gpe].no_name);
   
#endif
(void) gpe;
}

