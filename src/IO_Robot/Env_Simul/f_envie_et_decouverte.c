/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
___NO_COMMENT___
___NO_SVN___

\file  f_envie_et_decouverte.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction VB : Cette fonction demande a l'utilisateur d'indiquer quelle
 motivation a ete detectee. Il doit y avoir autant de neurone que de motivations
 possibles. Le neurone corespondant a la motivation detectee est mis a 1.
 Remarque:  Cette fonction ne fait rien en planification
 A terme, cette detection de motivation doit etre totalement autonome.
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#include "tools/include/local_var.h"
#include "tools/include/reconnaissance.h"
	#include <Kernel_Function/trouver_entree.h>
#include "tools/include/mise_a_jour_var_essentiel.h"
/*#define DEBUG*/
/*#define PAUSE_when_FOUND*/
/*rappel: source type nourriture =0 et type eau =1 !*/
void function_envie_et_decouverte(int gpe_sortie)
{

    static int deb_s, taille_s;
    int i, deb_e = -1, gpe_entree = -1, nb_e = -1, cell = -1,  nn =
        -1, ii = -1;
    static int cell_prec = -1;
    float max = -1;
    int gpe_envie;
    int gpe_entree_inhib;

    deb_s = def_groupe[gpe_sortie].premier_ele;
    taille_s = def_groupe[gpe_sortie].nbre;
#ifdef DEBUG
    printf("entre ds detect\n");
#endif    
/*recherche du lieux actuel */
    gpe_entree = trouver_entree(gpe_sortie,(char*) "EC");
    if (gpe_entree == -1)
    {
        printf
            ("Erreur limpossible de trouver le lien 'EC'  en entree du groupe: %d\n",
             gpe_sortie);
        exit(-1);
    }
    else
    {
        nb_e = def_groupe[gpe_entree].nbre;
        deb_e = def_groupe[gpe_entree].premier_ele;
        for (i = deb_e; i < deb_e + nb_e; i++)
        {
            if (neurone[i].s2 > max)
            {
                cell = i - deb_e;
                max = neurone[i].s2;
            }
        }

    }
    gpe_entree_inhib = trouver_entree(gpe_sortie, (char*)"inhib_learning");
    
    
    
    for (i = deb_s; i < deb_s + taille_s; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;

    if(gpe_entree_inhib>=0)
        if(neurone[def_groupe[gpe_entree_inhib].premier_ele].s1>0.5)
            return;
            
    /*test si la src est deja connue et recupere le type de source en fonction du lieu actuel, limite a 1 src par cell de lieux avec cet algo...(nav a vue)) */
    for (ii = 0; ii < nbptessentiel; ii++)
    {
#ifdef DEBUG
        printf("av reco:%d\n", ii);
#endif

        if (isequal(table_terrain[ii][5], cell) && cell != cell_prec)
        {
            cell_prec = cell;
            /*i=le type de src */
            i = table_terrain[ii][0];
            nn = ii;
#ifdef DEBUG
            printf("cell: %d, i:%d\n", cell, i);
#endif
            break;
        }
    }
/*on est pas sur une cellule qui contient une src connue, reconnaissance classique: recherche du type de lieux*/
    if (nn == -1)
        i = reconnaissance(&nn);
#ifdef DEBUG
	printf("ap reco:%d\n", i);
#endif
    /*mise a jour variable essentielle en fonction de la reconnaissance */
    mise_a_jour_var_essentiel(nn);

#ifdef DEBUG
    printf("\n\n+++++++++++++++I: %d *******************\n\n", i);
#endif
    if (i > -1)
    {
        /*sinon, son indice a ete trouve avant... */
        if (nn > -1)
        {
            /*on rempli les infos de la nnieme src de type i */
            table_terrain[nn][5] = cell;
            table_terrain[nn][1] = 1;
            type_src_known[i] = 1;
            neurone[deb_s + i].s  =
                neurone[deb_s + i].s2 = 1.;
#ifdef PAUSE_when_FOUND
            scanf("%d", &i);
#endif
        }
        else
        {
            printf("erreur pas de source trouve de ce type: %d\n", i);
            exit(-1);
        }
    }
#ifdef DEBUG
    else
        printf("pas de source\n");
#endif
	

	gpe_envie=trouver_entree(gpe_sortie,(char*)"envie");
	if(def_groupe[gpe_envie].nbre!=def_groupe[gpe_sortie].nbre)
	{
		printf("probl�me dans %s \n",__FUNCTION__);
		exit(0);
	}
	for(i=0;i<taille_s;i++)
		neurone[i+deb_s].s=neurone[i+deb_s].s1=neurone[i+def_groupe[gpe_envie].premier_ele].s1;
}
