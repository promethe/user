/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file
 \brief

 DEPRECATED : use f_extract_val_to_vector instead.

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: M. Maillard
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Fonction permettant de traduire une valeur analogique d'un vecteur en l'activation d'une
 valeur sur une population de neurones.
 le premier parametre du lien doit etre le numero de la composante et le deuxieme le max
 de la dynamique de cette grandeur de maniere a la ramener sur le bon intervale.
 par exemple si le premier parametre est le numero du landmark reconnu,
 il faut ecrire: 0,100 si l'on a 100 label max.
 Pour l'azimuth, il faut ecrire par ex: 1,6.28 pour definir que les valeurs sont entre 0 et 2pi pour le neurone "1" du groupe en entrée (correspondant à l'azimuth).


 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -discr ?

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include "tools/include/local_var.h"
#include "Kernel_Function/find_input_link.h"

struct this_data {
	float max;
	int p;
	int groupe_entree;
};

void function_extract_val_to_vector_circular(int gpe)
{
	int no, deb, deb_entree, nbre, nbre_entree, i = -1, groupe_entree, p;
	float max;
	int compt = 0; /*ajout nizar */
	struct this_data *_this_data = NULL;

	deb = def_groupe[gpe].premier_ele;
	nbre = def_groupe[gpe].nbre;

	if (discr == 0)
	{
		compt++;
		if (compt == 1)
		{
			discr = nbre; /*printf("discretisation :%d \n",discr); */
		}

	} /*est ce encore util? */

	/* printf("function_extract_val_to_vector \n"); */

	if (def_groupe[gpe].data == NULL)
	{
		PRINT_WARNING("Deprecated, use f_extract_val_to_vector instead. The difference is that the vector is centered no [0, max+max/n[ but [0, max]");

		i = find_input_link(gpe, 0);
		if (i == -1)
		{
			printf("ERROR the group f_extract_val_to_vector must have a link (%d)\n", gpe);
			exit(0);
		}

		sscanf(liaison[i].nom, "%d,%f", &p, &max);
		if (p < 0 || max < 0.)
		{
			printf("ERROR the group f_extract_val_to_vector must have a link with int,float informations (no and max value of no)\n");
			printf("The current name of the link is %s \n", liaison[i].nom);
			exit(0);
		}

		groupe_entree = liaison[i].depart;
		if (p > def_groupe[groupe_entree].nbre)
		{
			printf("ERROR argument of f_extract_val_to_vector for group %d is too high \n", gpe);
			exit(0);
		}
		def_groupe[gpe].data = malloc(sizeof(struct this_data));
		_this_data = (struct this_data *) def_groupe[gpe].data;
		if (_this_data == NULL)
		{
			printf("Erreur d'allocation memoire dans function_extract_val_to_vector Gpe %d\n", gpe);
			exit(0);
		}
		_this_data->p = p;
		_this_data->max = max;
		_this_data->groupe_entree = groupe_entree;
	}
	else
	{
		_this_data = (struct this_data *) def_groupe[gpe].data;
		p = _this_data->p;
		max = _this_data->max;
		groupe_entree = _this_data->groupe_entree;
	}

	deb_entree = def_groupe[groupe_entree].premier_ele;
	nbre_entree = def_groupe[groupe_entree].nbre;

	for (i = deb; i < (deb + nbre); i++)
	{
		neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
	} /*reset group activity */

	no = (int) ((float) nbre * (neurone[deb_entree + p].s1 / max));

	/*Si on est sur le max, alors c'est le dernier neurone qui s'active*/
	if (no == nbre) no = nbre - 1;

	if (no > nbre) no -= nbre;

	if (no < 0)
	{
		no += nbre;
		/*Modif M.M. : si c'est pas dans la range acceptable c'est que j'avais aps de point de focalisation donc je sors pas de promethe mais je fais rien (je met -1 si j'ai pas de focalisation */
		/*         printf("ERROR during the execution of group %d \n",gpe); */
		/*            printf("The activity on the input group must be positive and you shouls set max to ensure that you stay in the population of the groupe\n");  */

		/* return;*/
		/* 	exit(0);  */
	}

	/* printf("no=%d,nbre=%d,max=%f,p=%d,val=%f \n",no,nbre,max,p,neurone[deb_entree+p].s); */
	neurone[deb + no].s = neurone[deb + no].s1 = neurone[deb + no].s2 = 1.;
}

void function_extract_val_to_vector_non_circular(int gpe)
{
	int no, deb, deb_entree, nbre, nbre_entree, i = -1, groupe_entree, p;
	float max;
	int compt = 0; /*ajout nizar */
	struct this_data *_this_data = NULL;

	deb = def_groupe[gpe].premier_ele;
	nbre = def_groupe[gpe].nbre;

	if (discr == 0)
	{
		compt++;
		if (compt == 1)
		{
			discr = nbre; /*printf("discretisation :%d \n",discr); */
		}

	} /*est ce encore util? */

	/* printf("function_extract_val_to_vector \n"); */

	if (def_groupe[gpe].data == NULL)
	{
		PRINT_WARNING("Deprecated, use f_extract_val_to_vector instead with option -'-circular'. The difference is that the vector is centered no [0, max+max/n[ but [0, max[");

		i = find_input_link(gpe, 0);
		if (i == -1)
		{
			printf("ERROR the group f_extract_val_to_vector must have a link (%d)\n", gpe);
			exit(0);
		}

		sscanf(liaison[i].nom, "%d,%f", &p, &max);
		if (p < 0 || max < 0.)
		{
			printf("ERROR the group f_extract_val_to_vector must have a link with int,float informations (no and max value of no)\n");
			printf("The current name of the link is %s \n", liaison[i].nom);
			exit(0);
		}

		groupe_entree = liaison[i].depart;
		if (p > def_groupe[groupe_entree].nbre)
		{
			printf("ERROR argument of f_extract_val_to_vector for group %d is too high \n", gpe);
			exit(0);
		}
		def_groupe[gpe].data = malloc(sizeof(struct this_data));
		_this_data = (struct this_data *) def_groupe[gpe].data;
		if (_this_data == NULL)
		{
			printf("Erreur d'allocation memoire dans function_extract_val_to_vector Gpe %d\n", gpe);
			exit(0);
		}
		_this_data->p = p;
		_this_data->max = max;
		_this_data->groupe_entree = groupe_entree;
	}
	else
	{
		_this_data = (struct this_data *) def_groupe[gpe].data;
		p = _this_data->p;
		max = _this_data->max;
		groupe_entree = _this_data->groupe_entree;
	}

	deb_entree = def_groupe[groupe_entree].premier_ele;
	nbre_entree = def_groupe[groupe_entree].nbre;

	for (i = deb; i < (deb + nbre); i++)
	{
		neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
	} /*reset group activity */

	no = (int) ((float) (nbre - 1) * (neurone[deb_entree + p].s1 / max));
	/*
	 Tr�s bizarre ce fonctionnement. Par exemple, on demande quatre neurones mais la fonction divise l'intervalle entre  [0,max[ en trois iontervalle, et met l'intervalle [max, max+1/nbre-1[ sur le dernier neurone.
	 Par exmple: max = 1 et nbre = 4:

	 [0,0.33[ 	-> neurone 0
	 [0.33,0.66[ 	-> neurone 1
	 [0.66,1[ 	-> neurone 2
	 [1,1.33[ 	-> neurone 3

	 Pourtant, le max demand� �tait 1.

	 Je pense que le code ne fait pas ce qu'attend le codeur.*/

	/*deb_entree+p :endroit ou se trouve l'info sur l'angle car elle se trouve a deb+1 et p=1 */

	if (no < 0 || no >= nbre)
	{
		/*Modif M.M. : si c'est pas dans la range acceptable c'est que j'avais aps de point de focalisation donc je sors pas de promethe mais je fais rien (je met -1 si j'ai pas de focalisation */
		/*printf("ERROR during the execution of group %d \n",gpe);
		 printf("The activity on the input group must be positive and you shouls set max to ensure that you stay in the population of the groupe\n"); */

		return;
		/*exit(0); */
	}

	/* printf("no=%d,nbre=%d,max=%f,p=%d,val=%f \n",no,nbre,max,p,neurone[deb_entree+p].s); */
	neurone[deb + no].s = neurone[deb + no].s1 = neurone[deb + no].s2 = 1.;

}

