/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_explo_alea.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: Generation aleatoire d'un neurone codant une direction
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float vigil

Global variables:
-none

Internal Tools:
-teleguide()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>
#include <time.h>
#include <string.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
/*#define TEST*/
#define USE_FOR_EXPLO
void function_explo_alea(int numero)
{
int deb,nbre,i,cpt=0;
    int mod;
	float val;
	int gpe=-1,deb_e;
   static int test=0;
 
#ifdef USE_FOR_EXPLO
	int gpe_reset;
	int deb_reset;
	float reset ;
	 gpe_reset=trouver_entree(numero,(char*) "reset");
    if(gpe_reset ==-1) {printf("Error group (%d) should have a link named reset!\n",numero);exit(-1);}
    else{
	 deb_reset=def_groupe[gpe_reset].premier_ele;
	 reset=neurone[deb_reset].s2;
	 } 
#endif 
 
  deb=def_groupe[numero].premier_ele;
  nbre=def_groupe[numero].nbre;
 mod=(int)def_groupe[numero].seuil;
/* if(mod<=0)mod=4;*/
mod=8;
   if(test==0){
  srand48(/*clock()*/(unsigned)time(NULL));
test=1;
  }
    /* test si on est en planification*/
    /*nlink=find_input_link(numero,0);
    if(strcmp(liaison[nlink].nom,"planif")>=0)
    	gpe=liaison[nlink].depart;*/
    gpe=trouver_entree(numero,(char*) "planif");
    if(gpe ==-1) {printf("Error group (%d) should have a link named planif!\n",numero);exit(-1);}
    else{deb_e=def_groupe[gpe].premier_ele;} 
 
	for(i=deb;i<nbre+deb;i++)
		neurone[i].s=neurone[i].s1=neurone[i].s2=0;
   
 if(neurone[deb_e].s2<=0){
#ifndef USE_FOR_EXPLO
	/*version ameliore pour explo*/
#ifdef DEBUG
	printf("alea: %d, neurone %d , modulea:%d, l_traject: %d\n",alea_number,deb+alea_number,modulea,l_traject_donea);
#endif
	if(l_traject_donea>modulea)  {
		check2=0;
		#ifdef DEBUG
			printf("new trject normally generated\n");
		#endif
	}	
	
  	/*on test si on doit regenerer un mvt*/
   	if(check2==0){
	
		#ifdef DEBUG
			printf("check2 changed!\n");
		#endif
		
	#ifndef TEST
		val=drand48();
		if(val<0.5)val=-val;
#ifdef DEBUG
printf("val: %f\n",val);
#endif	
		alea_number+=(int)(val*(nbre/4));
#ifdef DEBUG
		printf("val4: %f , %d\n",(val*(nbre/4)),(int)(val*(nbre/4)));
#endif
		do
		  modulea =(int)(drand48()*(mod));
	  	while(modulea<3);/*3*/
	  	l_traject_donea=0;
			/*while (abs(alea_number-prec_dir)>(nbre/4) ){
				alea_number=(int)(drand48()*(nbre-1));
			}*/
	#else	
			scanf("%d",&alea_number);
	#endif
		
		if(alea_number >= nbre)
	 			alea_number -=nbre;
      		 if(alea_number < 0)
			alea_number +=nbre;
		prec_dir=alea_number;
	
		check2=1;
	}
	
	l_traject_donea++;
	neurone[deb+alea_number].s=neurone[deb+alea_number].s1=neurone[deb+alea_number].s2=1;
	#ifdef DEBUG
	printf("alea: %d, neurone %d , modulea:%d, l_traject: %d\n",alea_number,deb+alea_number,modulea,l_traject_donea);
	#endif
#else
	/*version basic pour test vs explo*/
#ifdef DEBUG	
printf("alea: %d, neurone %d , modulea:%d, l_traject: %d\n",alea_number,deb+alea_number,modulea,l_traject_donea);
#endif
	
  	/*on test si on doit regenerer un mvt*/
   	cpt++;
	if(check2==0 ||reset>0||cpt>800){
		
		cpt=0;
	
		#ifdef DEBUG
			printf("check2 changed!\n");
		#endif
		
		val=drand48();
		if(val<0.5)val=-val;
#ifdef DEBUG
		printf("val: %f\n",val);
#endif		
		alea_number+=(int)(val*(nbre/4));
#ifdef DEBUG
printf("Rval4: %f , %d\n",(val*(nbre/4)),(int)(val*(nbre/4)));
#endif
		if(alea_number >= nbre)
	 			alea_number -=nbre;
      		 if(alea_number < 0)
			alea_number +=nbre;
		prec_dir=alea_number;
	
		check2=1;
	}
	
	
	neurone[deb+alea_number].s=neurone[deb+alea_number].s1=neurone[deb+alea_number].s2=1;
#ifdef DEBUG
	printf("alea: %d, neurone %d , modulea:%d, l_traject: %d\n",alea_number,deb+alea_number,modulea,l_traject_donea);
#endif
#endif

	}
	else{
	
	#ifdef DEBUG
	printf("We are in planifictaion : no mvt will be generated from this group!\n");
	#endif
	}


}
