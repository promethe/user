/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  reconnaissance.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float posx
-float posy
-float pas
-int nbptessentiel;
-float **table_terrain
-float buts_trouves

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: 
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include "tools/include/local_var.h"
#include <math.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include "tools/include/rencontre_mur.h"
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
 /*-------------------------------------*/
  /*Reconnaissance si on passe sur un but */
int on_src(float x, float y, int i)
{
    int numero_pt = -1;
    float delta = 1.;
    printf
        ("src_x:%f, src_y:%f, x:%f, y:%f, inf_x:%f sup_x:%f,inf_y:%f,sup_y:%f\n",
         table_terrain[i][2], table_terrain[i][3], x, y,
         (table_terrain[i][2] - (delta * pas / 2.)),
         (table_terrain[i][2] + (delta * pas / 2.)),
         (table_terrain[i][3] - (delta * pas / 2.)),
         (table_terrain[i][3] + (delta * pas / 2.)));
    if ((x >= (table_terrain[i][2] - (delta * pas / 2.)))
        && (x < (table_terrain[i][2] + (delta * pas / 2.)))
        && (y >= (table_terrain[i][3] - (delta * pas / 2.)))
        && (y < (table_terrain[i][3] + (delta * pas / 2.))))
    {
        if (table_terrain[i][4] > 0.)
        {
            numero_pt = (int) table_terrain[i][0];  /*memorisation du type de but trouve */
        }

    }
    return (numero_pt);         /*(renvoie -1 dans le cas ou rien n'a ete reconnu) */
}

   /** calcule l'angle (par rapport au nord) fait par de vecteur de
     * coordonnees (dx,dy);
     * @param dx l'abscisse du vecteur
     * @param dy l'ordonnee du vecteur
     * @return l'angle par rapport au nord (dans [0, 2pi[)
     */
float angle(float dx, float dy)
{
    float theta;
    if (isequal(dx, 0))
    {
      if (isequal(dy, 0)) theta = 0;
        else if (dy > 0)  theta = M_PI / 2;
        else              theta = -M_PI / 2;
    }
    else
        theta = (float) atan2(dy, dx);

/*	if (dx>=0) theta += (M_PI / 2);
	else theta += (3 *M_PI / 2);*/
    return theta;
}



int checkForDirection(float x, float y, float theta, int type, float dist)
{
    int s;
    float xp, yp;
    yp = sin(theta);
    xp = cos(theta);
    while (dist <= perimetre * pas)
    {
        s = on_src(x, y, type);
        printf("s:%d\n", s);
        if (s > -1)
            return s;

        x += xp;
        y += yp;
        dist += sqrt(xp * xp + yp * yp);
        if (rencontre_mur(x, y))
            break;
    }
    return -1;
}







void function_nav_a_vue(int numero)
{
    int i, type;
    float dx, dy, theta, dist;
    int deb, nbre, nlink;
    int mem, found = 0;
    float max = -1;
    int gpe = -1, deb_e = -1, nb_e = -1;

    (void) mem; // (unused)

    deb = def_groupe[numero].premier_ele;
    nbre = def_groupe[numero].nbre;

    /* test si on est en planification */
    nlink = find_input_link(numero, 0);
    if (strcmp(liaison[nlink].nom, "mvt") >= 0)
        gpe = liaison[nlink].depart;
    /*gpe=trouver_entree(numero, "-pplanif"); */
    if (gpe == -1)
    {
        printf("Error group (%d) should have a link named mvt!\n", numero);
        exit(-1);
    }
    else
    {
        deb_e = def_groupe[gpe].premier_ele;
        nb_e = def_groupe[gpe].nbre;
    }


    for (i = deb; i < nbre + deb; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;

    /*recherche du max en entree */
    for (i = deb_e; i < nb_e + deb_e; i++)
    {
        if (neurone[i].s2 > max)
        {
            max = neurone[i].s2;
            mem = i;
            alea_number = i - deb_e;
/*            printf("max nav_a_vue: %d , %f\n", i - deb_e, max);*/
        }
    }
    /* recherche individuelle des sources */
    for (i = 0; i < nbptessentiel; i++)
    {
        dx = table_terrain[i][2] - posx;
        dy = table_terrain[i][3] - posy;
        theta = atan2(dy, dx);  /* theta = angle(dx,dy); */
        type = table_terrain[i][0];
        dist = sqrt(dx * dx + dy * dy);
        /*found= checkForDirection(posx,posy,theta, i,dist); */
#ifdef DEBUG
	printf("theta:%f, dist:%f, perimetre:%f,alea_number:%d\n", theta,
               dist, perimetre * pas, alea_number);
#endif        
 /*if(dist<perimetre*pas)         scanf("%f",&max); */
        if (theta >= M_PI)
            theta = -M_PI;
        /*attraction si src en vue et soit on ne connait pas de src de ce type ou on la connait et on en a besoin */
        if (dist < rayon_vision 
            && ((type_src_known[type] < 1)
                || (param_varessentiel[type][0] <
                    param_varessentiel[type][3])))
        {
            /* scanf("%f",&max); */
            found = 1;
            alea_number =
                (int) (((float) nbre * theta / (2 * M_PI)) + nbre / 2);
#ifdef DEBUG      
      printf("theta:%f, alea_number:%d\n", theta, alea_number);
#endif            
      /*      scanf("%d",&theta);  */
        }
    }
    for (i = 0; i < nb_e; i++)
    {
        /*si on a modifie la direction on conserve la modif */
        if (found > 0 /*i==mem-deb_e||i==alea_number */ )
            neurone[deb + alea_number].s = neurone[deb + alea_number].s1 =
                neurone[deb + alea_number].s2 = 1;
        /*sinon on recopie l'entree */
        else
        {
            neurone[deb + i].s = neurone[deb_e + i].s;
            neurone[deb + i].s1 = neurone[deb_e + i].s1;
            neurone[deb + i].s2 = neurone[deb_e + i].s2;
        }
    }


}
