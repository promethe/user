/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_show_env_and_robot f_show_env_and_robot
 

\section Modified 
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description




\section Macro
-none 
\section Local variables
-float posx
-float posy
-float dx_mem
-float dy_mem

\section Global variables
-none

\section Internal Tools
-affiche_robot()
-affiche_obstacle()
-affiche_simul()
-affiche_temps()

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http:../masks/www.doxygen.org
*/

#include <libx.h>
#include <Typedef/boolean.h>

#include "tools/include/local_var.h"
#include "tools/include/macro.h"

 /*#include "tools/include/affiche_robot.h" */
#include "tools/include/affiche_obstacle.h"
#include "tools/include/affiche_landmarks.h"
#include "tools/include/affiche_ptessentiel.h"
#include "tools/include/affiche_eq.h"
void affiche_temps_position(void);

void affiche_temps_position(void)
{

#ifndef AVEUGLE
    TxPoint pointxt;
    char chaine1[50] = "Time = ";
    pointxt.x = 0;
    pointxt.y = 500;
    /*Le rectangle noir ne sera dessine qu'une fois avec ce if */
    TxDessinerRectangle(&image1, gris, TxPlein, pointxt, 1200, 15, 2);
    sprintf(chaine1, "Time = %d, X=%3.2f, Y=%3.2f", (int) temps, posx, posy);
    pointxt.x = 5;
    pointxt.y = 512;
    TxEcrireChaine(&image1, rouge, pointxt, chaine1, 0);
#endif
}

void function_show_env_and_robot(int numero)
{
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
    /* efface_terrain(); */

    /* ligne a rajouter si la fonction teleguide ne dessine pas le robot : */

    /* affiche_robot(posx,posy,dx_mem,dy_mem,rouge); */



    if (is_graphic_init == false)
    {
        affiche_obstacle();
        affiche_landmarks();
        affiche_ptessentiel();
    }
    /*affiche_temps_position();
       affiche_eq(); */
    TxFlush(&image1);

    /*Cet variable est au premier tour a false. Elle permet de savoir si le fond de l'environnement 
       est deja dessiner ou pas. */
    is_graphic_init = true;
#endif

(void) numero;
}
