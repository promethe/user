/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_env_init f_env_init


\section Modified
-Name: C.Giovannangeli
-Created: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description
 L'init de l'environnement n'est faite qu'a la premiere iter des simul avec promethe



\section Macro
-TAILLE_X

\section Local variables
-float pas
-int NBLAND;			Nbre de landmarks
-char nom_fichier_cfg[32];
-int mode;

\section Global variables
-none

\section Internal Tools
-init()

\section External Tools
-none

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


/*#define DEBUG*/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/nomprs.h"

#include <time.h>
#include "tools/include/alloue.h"
#include "tools/include/init_obstacle.h"
#include "tools/include/init_aleat_table_terrain.h"
#include "tools/include/init_position_de_depart.h"
#include "tools/include/calcule_nbptessentiels_dans_fichier.h"

void init(void);

void init(void)
{
   int i = 0, p;

   /*-------------------------------------*/
   /*Calcul du nombre de points essentiels */
   i = calcule_nbptessentiels_dans_fichier();
   /*i = nb_pts_essentiels_max; */

   /*si il existe un seul point defini dans le fichier texte */
   /* on ne met plus les points aleatoirement dans le terrain */
   if (i != 0)
   {
      nbptessentiel = i;
      aleat_table_terrain = 0;
      cprints("%d variables essentiels sur le terrain\n", nbptessentiel);
   }
   else
   {
      aleat_table_terrain = 0;
      nbptessentiel = 0;
      cprints("Aucune ressource sur le terrain\n");
   }
   /*------------------------*/
   /*allouer toute la memoire */
   alloue((char*)"tout");
   cprints("table_land et table_terrain alloued\n");

   /*  printf("entree une valeur: ");
      scanf("%d",&seed); */
   srand48((unsigned) time(NULL));

   for (i = 0; i < nbptessentiel; i++)
   {
      /*met la cellule associe et le y a 0 */
      table_terrain[i][4] = 0.;
      table_terrain[i][1] = 0.;
   }
   init_obstacle();
   init_aleat_table_terrain();
   /*Aucune source connue*/
   for (i = 0; i < NBVARESSENTIEL; i++)
      type_src_known[i] = 0;

   /*position du robot case de depart 10 10 */
   /* posx=mise_a_echelle_cases_vers_pix(POS_DEPARTX);
      posy=mise_a_echelle_cases_vers_pix(POS_DEPARTY); */
   init_position_de_depart(&posx, &posy);
   cprints("INIT POS DEPART \n");
   /*initialisation mem pour graphe */
   for (i = 0; i < NBVARESSENTIEL; i++)
      for (p = 0; p < NBMEM; p++)
      {
         mem[i][p] = param_varessentiel[i][0];
      }
}

#define STRING_SIZE 256
void function_env_init(int numero)
{
   char st[STRING_SIZE];
   FILE *f;
   int i,test;
   if (first_init == 1)
   {
      cprints("Environment init taillex= %d\n", TAILLEX);
      mode = 1;
      NBLAND = 0;

      for (i = 0; i < nbre_liaison; i++)  /*Dans toutes les liaisons exitantes */
         if (liaison[i].arrivee == numero)   /*on cherche celle dont l'arrivee est le groupe que l'on traite */
         {
            strcpy(nom_fichier_cfg, liaison[i].nom);    /*une fois la liaison trouvee, on copie son nom dans la variable 'nom_fichier_cfg' */
            nomprs();       /*ajout de fonction de nizar               si ('nom_fichier_cfg'=file.cfg) alors 'nom_fichier_prs'=file.prs */
            break;              /**on peut alors casser la boucle*/
         }
      dprints(" nom du fichier de config  = %s \n", nom_fichier_cfg);


      /*-----------------------------------------------------------------*/
      /*ouverture du fichier qui contient l'information                  */
      /* dans une grille 40x40 avec comme valeur N ou E pour coder       */
      /* un pt de Nourriture ou un point d'eau                           */
      /*-----------------------------------------------------------------*/
      f = fopen(nom_fichier_cfg, "r");
      if (f == NULL)
      {
         EXIT_ON_ERROR("f_init_env: cannot open %s\n", nom_fichier_cfg);
      }

      while (feof(f))
      {
         test=fscanf(f, "%s", st);
         if (test ==0) {PRINT_WARNING("Erreur de lecture du fichier");}
         i++;
      }
      cprints("nbcase = %d\n", i);
      fclose(f);
      f = fopen(nom_fichier_cfg, "r");
      if (fgets(st, STRING_SIZE, f)!=NULL)
      {
         nb_cases = strlen(st) / 2;
         fclose(f);
         cprints("nb cases: %d\n", nb_cases);
         pas = (float) TAILLEX / nb_cases;
      }
      init();                 /*on lance alors la fonction 'init' */
   }
   first_init = 0;             /*'first_init' est maintenant different de 1 (il n'y aura pas d'autre initialisation de l'environneemnt) */
}
