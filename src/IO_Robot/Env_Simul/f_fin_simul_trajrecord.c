/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libSensors
\defgroup f_fin_simul_trajrecord f_fin_simul_trajrecord
\brief 
\section Author
Name xxxxxxxx
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description

\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include "tools/include/init_position_de_depart.h"
#include "tools/include/init_obstacle.h"
#include "tools/include/local_var.h"

void function_fin_simul_trajrecord(int numero)
{
    char chaine[255];
    float x, y;
    int nb_cell, i;
    FILE *traject = fopen("traject.SAVE", "r");
    prom_images_struct *im_ext = calloc_prom_image(1,
                                                   nb_cases,
                                                   nb_cases,
                                                   1);
    if (traject == NULL)
    {
        dprints("Impossible to create file traject.SAVE\n");
        exit(EXIT_FAILURE);
    }
    def_groupe[numero].ext = im_ext;
    for (i = 0; i < nb_cases * nb_cases; i++)
    {
        if(fscanf(traject, "%f %f %d\n", &x, &y, &nb_cell))
			{im_ext->images_table[0][i] = (unsigned char) (nb_cell * 7);}
		else {dprints("Erreur de lecture sur le fichier dans traject.SAVE");}
    }
    do
    {
        cprints("pour revenir au menu taper: oui \n");
        cprints("fin simul (oui/non) : ");
        if(scanf("%s", chaine)){}
        else{dprints("Erreur lecture scanf, debut/fin simul");}
    }
    while (strcmp(chaine, "oui") != 0);
    cprints("%s\n", chaine);

    cprints("repartir de la position indiqu�e par le script: oui \n");
    cprints("(oui/non) : ");
    if(scanf("%s", chaine))
		{if (strcmp(chaine, "oui") == 0)
			{
			init_position_de_depart(&posx, &posy);
			}
		}
	else {dprints("Erreur lecture scanf, position depart");}
    /*init_presence(); */
    init_obstacle();
    _initialisation = 1;        /*variable globale */
    first_init = 1;
     fclose(traject);
}
