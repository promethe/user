/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \ingroup libIO_Robot
 \defgroup f_extract_vector_to_val f_extract_vector_to_val
 \brief

 \author M. Maillard

 \details
 Set the neuron of the group accordingly on the position of the maximum of activity in the input group.
 First input neuron imply 0, last one imply 1.
 If the input is null the output is -1.

 \file
 \ingroup f_extract_vector_to_val

 */
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools/include/local_var.h"
#include "Kernel_Function/find_input_link.h"

typedef struct extract_vector_to_val {
  float x_min, i_to_x;
  float y_min, j_to_y;
  int lineaire;

  type_groupe *input_group;

} type_extract_vector_to_val;

void new_extract_vector_to_val(int gpe)
{
  char *link_name;
  char *string = NULL;
  float x_max, y_max;
  int link_id, full_range, nb_links;
  type_extract_vector_to_val *my_data;

  my_data = ALLOCATION(type_extract_vector_to_val);

  my_data->lineaire = 0;

  nb_links = 0;
  for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, nb_links))
  {
    link_name = liaison[link_id].nom;
    my_data->input_group = &def_groupe[liaison[link_id].depart];

    if (prom_getopt(link_name, "full_range", NULL) == 1) full_range = 1;
    else full_range = 0;

    if (prom_getopt_float(link_name, "-xm", &my_data->x_min) != 2)
    {
      if (full_range) my_data->x_min = 0;
      else my_data->x_min = 0.5 / (my_data->input_group->taillex); /* C'était 1 / ... */
    }

    if (prom_getopt_float(link_name, "-ym", &my_data->y_min) != 2)
    {
      if (full_range) my_data->y_min = 0;
      else my_data->y_min = 0.5 / (my_data->input_group->tailley); /* C'était 1 / ... */
    }

    if (prom_getopt(link_name, "-lineaire", string) >= 1)
    {
      my_data->lineaire = 1;
    }

    if (prom_getopt_float(link_name, "-xM", &x_max) != 2) x_max = 1;
    if (prom_getopt_float(link_name, "-yM", &y_max) != 2) y_max = 1;

    if (my_data->input_group->taillex > 1)
    {
      if (full_range) my_data->i_to_x = (x_max - my_data->x_min) / (my_data->input_group->taillex - 1);
      else my_data->i_to_x = (x_max - my_data->x_min) / (my_data->input_group->taillex);
      printf("my_data->i_to_x=%f\n", my_data->i_to_x);
    }
    else if (my_data->input_group->tailley > 1)
    {
      if (full_range) my_data->j_to_y = (y_max - my_data->y_min) / (my_data->input_group->tailley - 1);
      else my_data->j_to_y = (y_max - my_data->y_min) / (my_data->input_group->tailley);
      printf("my_data->j_to_y=%f\n", my_data->j_to_y);
    }
    else EXIT_ON_GROUP_ERROR(gpe, "You need a vector as input");

    nb_links++;
  }
  def_groupe[gpe].data = my_data;

}

void function_extract_vector_to_val(int gpe)
{
  float max = 0.0, barycentre = 0.0, sum = 0.0;
  int deb, input_deb, i, max_id = -1;
  type_extract_vector_to_val *my_data = NULL;

  deb = def_groupe[gpe].premier_ele;
  my_data = def_groupe[gpe].data;
  input_deb = my_data->input_group->premier_ele;

  max = 0.;
  max_id = -1;
  if (my_data->input_group->taillex > 1)
  {
    if (my_data->lineaire != 1)
    {
      for (i = 0; i < my_data->input_group->taillex; i++)
      {
        if (neurone[input_deb + i].s1 > max)
        {
          max = neurone[input_deb + i].s1;
          max_id = i;
        }
      }
    }
    else
    {
      for (i = 0; i < my_data->input_group->taillex; i++)
      {
        sum += neurone[input_deb + i].s1;
        barycentre += neurone[input_deb + i].s1 * i;
      }
      if (sum > 0.000000001 || sum < -0.000000001)
      {
        barycentre = barycentre / sum;
      }
      else barycentre = 0.0;
    }

    if (max_id != -1 && my_data->lineaire != 1)
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = max_id * my_data->i_to_x + my_data->x_min;
      /*
       ((float) (max_id  + 1) / nbre_entree)  = max_id * 1/nbre_entre + 1/nbre_entree);

       full_range
       else
       {
       if (nbre_entree > 1) neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = ((float) (max_id) / (nbre_entree - 1));
       else
       }*/
    }
    else if (my_data->lineaire == 1)
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = barycentre * my_data->i_to_x + my_data->x_min;
    }
    else
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = -1.;
    }
  }
  else
  {
    if (my_data->lineaire != 1)
    {
      for (i = 0; i < my_data->input_group->tailley; i++) /* We suppose taillex == 1 */
      {
        if (neurone[input_deb + i].s1 > max)
        {
          max = neurone[input_deb + i].s1;
          max_id = i;
        }
      }
    }
    else
    {
      for (i = 0; i < my_data->input_group->tailley; i++)
      {
        sum += neurone[input_deb + i].s1;
        barycentre += neurone[input_deb + i].s1 * i;
      }
      if (sum > 0.000001 || sum < -0.000001)
      {
        barycentre = barycentre / sum;
      }
      else barycentre = 0.0;
    }

    if (max_id != -1 && my_data->lineaire != 1)
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = max_id * my_data->j_to_y + my_data->y_min;
    }
    else if (my_data->lineaire == 1)
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = barycentre * my_data->j_to_y + my_data->y_min;
    }
    else
    {
      neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = -1.;
    }
  }
}
