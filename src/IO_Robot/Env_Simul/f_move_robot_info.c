/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_move_robot_info.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:  Traitement des informations des cellules de lieux acquises lors de la premiere phase (exploration aleatoire).
 Creation de deux fichiers dans le repertoire results: - lieux_vig_x_disc_y (contient pour chaque position le numero du neurone vainqueur de DG)
                                                       - act_vig_x_disc_y (contient pour chaque position le vecteur des activites de tous les neuones)
x et y sont modifies selon la vigilence  et le pas de discretisation utilises dans la simulation.

Macro:
-TAILLE_X
-TAILLE_Y 

Local variables:
-int _initialisation
-float vigil
-char chemin[254]
-int discr
-char chemin2[254]
-float pas
-float posx
-float posy

Global variables:
-none

Internal Tools:
-rencontre_obstacle()
-rencontre_landmark()
-rencontre_petit_mur()
-arrange_echelle()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>

#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
#include "tools/include/rencontre_obstacle.h"
#include "tools/include/rencontre_landmark.h"
#include "tools/include/rencontre_petit_mur.h"
#include "tools/include/arrange_echelle.h"

void function_move_robot_info(int gpe)
{
    static int abs = 0;
    static int ord = 0;

    int absmax;
    int ordmax;

    int n;

    FILE *file_lieux, *file_act;    /* 2 fichiers de sorties */

    /* string pour la formation du nom des fichiers         */
    char vig[20] = "";
    char discretisation[20] = "";
    char vig2[20] = "";
    char discretisation2[20] = "";

    /* la variable _initialisation vaut 1 lors du premier appel de la fonction: initialisation du nom des fichiers de sortie */
    if (_initialisation == 1)
    {
        abs = 0;
        ord = 0;                /* coordonnees du depart */

        sprintf(vig, "%f", vigil);
        strcpy(chemin, "results/lieux_vig_");
        strcat(chemin, vig);

        sprintf(discretisation, "%d", discr);
        strcat(chemin, "_discr_");
        strcat(chemin, discretisation);


        sprintf(vig2, "%f", vigil); /* on est oblige de redeclarer des variables differentes -> probleme de pointeurs sinon */
        strcpy(chemin2, "results/activ_vig_");
        strcat(chemin2, vig2);

        sprintf(discretisation2, "%d", discr);
        strcat(chemin2, "_discr_");
        strcat(chemin2, discretisation2);

        _initialisation = 0;
    }
    /* Nombre de cases */
    /*mesure m[(int)(TAILLEX/pas)][(int)(TAILLEY/pas)]; */
    absmax = (int) ((TAILLEX - 100) / pas);

    ordmax = (int) ((TAILLEY - 100) / pas);
    if (ord > ordmax - 2)
    {
    }
    else
    {

#ifdef DEBUG
        printf("DEBUG ACTIF");
#endif
    /*****PARTIE DU CODE POUR EXTRAIRE LES INFOS DES NEURONES*****/
        float max;
        int i, deb_entree, nbre_entree, no_max = 0, groupe_entree;

        groupe_entree = -1;
        groupe_entree = liaison[find_input_link(gpe, 0)].depart;
        /*old code
           for(i=0;i<nbre_liaison;i++)
           if(liaison[i].arrivee==gpe) 
           { 
           groupe_entree=liaison[i].depart; 
           break; 
           }
         */

        if (groupe_entree < 0)
        {
            printf("error no input group for group %d\n", gpe);
            return;
        }

        deb_entree = def_groupe[groupe_entree].premier_ele;
        nbre_entree = def_groupe[groupe_entree].nbre;

        /* recherche de l'activite max dans le groupe d'entree   */
        max = -1;
        for (i = deb_entree; i < (deb_entree + nbre_entree); i++)
        {
            if (neurone[i].s > max)
            {
                max = neurone[i].s;
                no_max = i;
            }
#ifdef DEBUG
            printf("%f", neurone[i].s);
#endif
        }
        /*index en relatif */
        no_max = no_max - deb_entree;
        /*point.x = (int)posx ; point.y=(int)posy ;  */

        /*sprintf(chaine1,"%d",no_max); */
        /*printf("%d",no_max); */


    /***********************************************/


    /*****PARTIE DU CODE POUR ENREGISTRER DANS UN FICHIER**********/



        file_lieux = fopen(chemin, "a");
        file_act = fopen(chemin2, "a");

        if ((abs == 0) && (ord == 0))
        {                       /* initialisation */
            /* obligation de faire cela d� au fait que je ne peux intitialiser le robot en 0,0 dans le script cfg */
            fprintf(file_lieux, "-%d ", -2);    /*en fait ce point n'est pas defini lors du premier pas donc chiffre arbitraire */
            for (n = 0; n < nbre_entree; n++)
            {
                fprintf(file_act, "%d ", -2);   /* activite=-2 pour toutes les activites pour la position de depart */
            }

        }
        else
        {                       /* traitement du decalage a l'affichage pour chiffre < 10 affiche 0X au lieu de X */
            if (no_max < 10)
                fprintf(file_lieux, "00%d ", no_max);   /* inscrit le numero du neurone vainqueur */
            else if (no_max < 100)
                fprintf(file_lieux, "0%d ", no_max);    /* inscrit le numero du neurone vainqueur */
            else
                fprintf(file_lieux, "%d ", no_max); /* inscrit le numero du neurone vainqueur */

            for (n = 0; n < nbre_entree; n++)
                fprintf(file_act, "%f ", neurone[n + deb_entree].s);    /*activites de tous les neurones en ce lieu */


        }

        abs++;                  /*incrementation de la position (progression sur une ligne) */
        /* verification si on ne se trouve pas dans un mur, un obstacle, un landmark ou hors de l'environnement, bref dans la zone de simulation! */
        while ((rencontre_obstacle(abs * pas, ord * pas)) || (abs >= absmax)
               || (rencontre_landmark(abs * pas, ord * pas)
                   || (rencontre_petit_mur(abs * pas, ord * pas))))
        {

            /*test si on rencontre un landmark ou un mur */
            printf("mur ou hors champs. posx=%d\t posy=%d\n", abs, ord);
            if ((rencontre_obstacle(abs * pas, ord * pas))
                || (rencontre_landmark(abs * pas, ord * pas))
                || (rencontre_petit_mur(abs * pas, ord * pas)))
            {
                fprintf(file_lieux, "-%d ", -1);    /* si on rencontre un obstacle -1 dans mle fichier de lieux */
                for (n = 0; n < nbre_entree; n++)
                {
                    fprintf(file_act, "-%d ", -1);  /* et -1 pour l'activite de tous les neurones en ce lieux */
                }

            }
            abs++;              /*incrementation supplementaire (car peut etre pas au bout de la zone d'environnement, ex: obstacle au milieux */

            /* si on est au bord de la zone de simulation... */
            if (abs > absmax - 1)
            {
                fprintf(file_lieux, "\n");  /*on passe a la ligne suivante dans les fichier */
                fprintf(file_act, "\n");
                abs = 0;
                ord++;
            }

            if (ord > ordmax - 2)
            {
                break;
            }


        }
        /*fermeture des fichiers */
        fclose(file_lieux);
        fclose(file_act);

        /* deplacement du robot */
        posx = abs * pas;
        posy = ord * pas;

        posx = arrange_echelle(posx);
        posy = arrange_echelle(posy);

    }

}
