/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_print_place f_print_place
\brief 
 


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number
 
\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <libx.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>
#include <Struct/print_info_data.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*affiche ou non les infos d'activites sur image2...*/
#define im2
#define MAX_WINNER 1200
/*module max d'un vecteur du sub */
#define VECT_MOD 10
#define DISPLAY_GRAPH
#ifndef AVEUGLE

#define LINE_WIDTH 3000
#define LINE_HEIGHT 20
#define LINE_START_X 10
#define LINE_START_Y 20
#define COL_SIZE 28
void draw_vector(TxDonneesFenetre ima, TxPoint org, TxPoint dest, int colour)
{
    TxDessinerSegment(&ima, colour * 4000 + 5, org, dest, 1 /*epaisseur */ );
    TxDessinerCercle(&ima, colour * 4000 + 5 /*4000 */ , 1 /*plein */ , dest,
                     2 /*rayon */ , 1 /*epaisseur */ );
}

void draw_bord_tableau()
{
    static TxPoint point, pt;
/*horiz*/
    point.x = LINE_START_X;
    point.y = LINE_START_Y + LINE_HEIGHT * 3;
    pt.x = LINE_WIDTH + point.x;
    pt.y = point.y;
    TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
    point.x = LINE_START_X;
    point.y = LINE_START_Y + LINE_HEIGHT * 8;
    pt.x = point.x + LINE_WIDTH;
    pt.y = point.y;
    TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
    /*vert */
    point.x = LINE_START_X;
    point.y = LINE_START_Y + LINE_HEIGHT * 3;
    pt.x = point.x;
    pt.y = LINE_START_Y + LINE_HEIGHT * 8;
    TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
    point.x = LINE_START_X + LINE_WIDTH;
    point.y = LINE_START_Y + LINE_HEIGHT * 3;
    pt.x = point.x;
    pt.y = LINE_START_Y + LINE_HEIGHT * 8;;
    TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
    /*1er col */
    point.x = LINE_START_X + 28 * 2;
    point.y = LINE_START_Y + LINE_HEIGHT * 3;
    pt.x = point.x;
    pt.y = LINE_START_Y + LINE_HEIGHT * 8;
    TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
}
void draw_a_textline(int line_number /*, int col_number, */ , char *string,
                     int colour)
{
    static int line_nb = 0;
    static TxPoint point, pt;
    point.x = LINE_START_X;
    point.y = LINE_START_Y + LINE_HEIGHT * (line_number - 1);
    TxDessinerRectangle_rgb(&image2, 3, TxPlein, point, 3000, 20, 1);
    point.y = LINE_START_Y + LINE_HEIGHT * line_number;
    if (string != NULL)
        TxEcrireChaine(&image2, vert, point, string, 0);
    if (colour > -1)
        TxDessinerRectangle_rgb(&image2, 3, TxPlein, point, 3000, 20, 1);
    /*draw col */
    for (line_nb = 0; line_nb < 8; line_nb++)
    {
        point.x = LINE_START_X + 28 * 2 + line_nb * 28 * 7;
        point.y = LINE_START_Y + LINE_HEIGHT * 3;
        pt.x = point.x;
        pt.y = LINE_START_Y + LINE_HEIGHT * 8;
        TxDessinerSegment(&image2, vert, point, pt, 2 /*epaisseur */ );
    }
}

#endif


typedef struct neur_dta_def
{
    int index;
    float val;
} neur_dta;

void Quicksort(neur_dta * vec, int loBound, int hiBound)
{
    neur_dta pivot, temp;
    int loSwap, hiSwap;
    if (hiBound - loBound == 1)
    {
        if (vec[loBound].val > vec[hiBound].val)
        {
            temp = vec[loBound];
            vec[loBound] = vec[hiBound];
            vec[hiBound] = temp;
        }
        return;
    }
    pivot = vec[((loBound + hiBound) / 2)];
    vec[((loBound + hiBound) / 2)] = vec[loBound];
    vec[loBound] = pivot;
    loSwap = loBound + 1;
    hiSwap = hiBound;
    do
    {
        while (loSwap <= hiSwap && vec[loSwap].val <= pivot.val)
            loSwap++;
        while (vec[hiSwap].val > pivot.val)
            hiSwap--;
        if (loSwap < hiSwap)
        {
            temp = vec[loSwap];
            vec[loSwap] = vec[hiSwap];
            vec[hiSwap] = temp;
        }
    }
    while (loSwap < hiSwap);
    vec[loBound] = vec[hiSwap];
    vec[hiSwap] = pivot;
    if (loBound < hiSwap - 1)
        Quicksort(vec, loBound, hiSwap - 1);
    if (hiSwap + 1 < hiBound)
        Quicksort(vec, hiSwap + 1, hiBound);
}


int class_by_order(int gp, neur_dta * tab, float seuil)
{
    int i, deb_entree, nbre_entre, increment, k;
    deb_entree = def_groupe[gp].premier_ele;
    nbre_entre = def_groupe[gp].taillex * def_groupe[gp].tailley;
    increment = def_groupe[gp].nbre / nbre_entre;

    k = 0;
    for (i = deb_entree + increment - 1; i < (deb_entree + nbre_entre);
         i += increment)
    {
        if (neurone[i].s1 > seuil)
        {
            tab[k].val = neurone[i].s1;
            tab[k].index = i;
            k++;
        }
    }
    Quicksort(tab, 0, k - 1);
    return k - 1;
}


void function_print_infos(int gpe)
{
#ifndef AVEUGLE
   static TxPoint point, pos;
    TxPoint org, dest;
#endif
    char chaine1[50], chaine2[500], chaine4[500], chaine5[250000], vale[2500],
        chaine6[250000], valb[250000], retour[255];
    float max, angle, max1, max2;
    int test, i, p, deb_entree, nbre_entre, no_max =
        0, no_max1, no_max2, n, increment = -20000, increment_ca3 =
        -20000, deb_entree3 = -2000;
    int val, val_ca3 = -1, k, fp = 0 /*var bool pour sortie ds un fichier */ ;
    static int max_ec_prec = -1, max_dg_prec = -1;
    static int orgx, orgy, DGx, DGy, ECx, ECy, pos_precx, pos_precy,
        pos_org1x, pos_org1y, pointx, pointy;
    static int *EC_cell_pos = 0;
    static FILE *f = NULL;
    char *buff;
    static int count = 0, max_ca3_prec = -1;
    static int dg_winners_pos[4];
    static int ca3_trans_mem[800];
    static neur_dta *winner_ord = NULL;
    static int plan = -1, deb_entree_acc;
    static int init = -1, n_mvt = 0, ct = 0, fff = 0;
    static int mvt_reco[6][6];  /*n�reco,n�mvt,posx,posy,colour */

    val = trouver_entree(gpe, (char*)"plan");
    if (val != -1)
    {
        plan = (int) neurone[def_groupe[val].premier_ele].s2;
    }

    /*recherche le groupe donnant la position */
    if (init == -1)
    {
#ifndef AVEUGLE
#ifdef DISPLAY_GRAPH
        fff = open("mapi.dot", O_WRONLY | O_CREAT | O_EXCL, 0666);  /*    ff=fopen("mapi.dot","a"); */
        if (fff != -1)
        {
            /*      if(ff==NULL){
               printf("cannot open map.dot\n");
               return;
               } */
            buff = (char *) malloc(955 * sizeof(char));
            sprintf(buff, "%s",
                    "digraph out {\nnode [style=\"filled\", color=\"black\", shape=\"box\"];\nsize=\"20,7\"; \nfontsize=\"8\"; \nfontname=\"Times\"; \nratio=compress; \noverlap=scale;\n");

            dprints("write: %d\n",
                   (int)write(fff, buff, strlen(buff) * sizeof(char)) ) ;
            perror(0);
            dprints("closed: %d\n", close(fff));
            free(buff);
        }
        else
            perror(0);
        /*      
           fprintf(ff,"digraph out {\n  ");
           fprintf(ff,   "node [style=\"filled\", color=\"black\", shape=\"box\"];\n");
           fprintf(ff,   "size=\"20,7\"; \n");
           fprintf(ff,   "fontsize=\"8\"; \n");
           fprintf(ff,   "fontname=\"Times\"; \n");
           fprintf(ff,   "ratio=compress; \n");
           fprintf(ff,   "overlap=scale;\n");
           fclose(ff);

         */ /*    system("scp ./mapi.dot  goldorak:~/script/nav/"); */
#endif
        draw_bord_tableau();
#endif
        /*la position est elle fournit par une boite specifique (utilisee pour robot */
        val = trouver_entree(gpe,(char*) "pos");
        if (val != -1)
        {
            /*      pos.x=neurone[def_groupe[val].premier_ele].s2;
               pos.y=neurone[def_groupe[val].premier_ele+1].s2; */
        }
        /*ou utilise t on les position de l'environnement simule */
        else
        {
            /*sinon on est en simulation */
            /*initialise la structure point avec les coord courantes de l'agent */
            /*      pos.x=(int)posx;
               pos.y=(int)posy; */
            pos_precx = -1;
            pos_precy = -1;     /*pos precedente */
        }
        /*printf("posx: %d,posy: %d\n",(int)pos.x,(int)pos.y);
           printf("posx: %d ,posy: %d\n",(int)posx,(int)posy); */
        init = 1;
    }
    /*pos actuelle */
    pos_org1x = posx;
    pos_org1y = posy;

    /*pos utilisee selon qu'on predit ou non */
    if (plan > 0)
    {
        /*affiche le mvt propose */
        orgx = (int) posx;
        orgy = (int) posy;
    }
    else
    {
        /*affiche la mvt desiree */
        orgx = (int) pos_precx;
        orgy = (int) pos_precy;
    }




    /*enregistrement des positions du robot ds un fichier */
    if (fp)
    {
        if (f == NULL)
            f = fopen("ca3.txt", "w");
        else
            f = fopen("ca3.txt", "a");
    }

    /*cellule de EC */
    val = trouver_entree(gpe,(char*) "EC");
    if (val != -1)
    {
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].nbre;

        /*initialisation si besoin, du tableau des coordonne des cells */
        if (EC_cell_pos == 0)
        {
            EC_cell_pos = (int *) malloc(nbre_entre * 2 * sizeof(int));
            if (EC_cell_pos == 0)
            {
                EXIT_ON_ERROR("imossible d'allouer le tab EC\n");
                exit(-1);
            }
            for (i = 0; i < 2 * nbre_entre; i++)
            {
                EC_cell_pos[i] = -1;
            }
        }
        no_max = no_max1 = no_max2 = 0;
        max = max1 = max2 = -1;
        for (i = deb_entree; i < (deb_entree + nbre_entre); i++)
        {
            if (neurone[i].s1 > max)
            {
                max2 = max1;
                no_max2 = no_max1;
                max1 = max;
                no_max1 = no_max;
                max = neurone[i].s;
                no_max = i;
            }
            else if (neurone[i].s1 > max1)
            {
                no_max2 = no_max1;
                max2 = max1;
                max1 = neurone[i].s;
                no_max1 = i;
            }
            else if (neurone[i].s1 > max2)
            {
                max2 = neurone[i].s;
                no_max2 = i;
            }
        }
        no_max = no_max - deb_entree;
        no_max1 = no_max1 - deb_entree;
        no_max2 = no_max2 - deb_entree;
#ifdef DEBUG
        dprints("EC: RECOGNIZED place = %d ,posx=%d, posy=%d\n", no_max,
               point.x, point.y);
#endif
        sprintf(chaine1, "EC: %d( %f) | %d( %f) | %d( %f) ", no_max, max,
                no_max1, max1, no_max2, max2);
        cprints("%s\n", chaine1);
        if (fp)
            fprintf(f, "EC: %d( %f), %d( %f), %d( %f) ", no_max, max, no_max1,
                    max1, no_max2, max2);
#ifndef AVEUGLE
#ifdef im2
        draw_a_textline(1, chaine1, -1);
#endif
#endif
        /*memorise, si besoin, les coordonnees ou ont ete appris la cel */
        if (EC_cell_pos[no_max] == -1
            && EC_cell_pos[no_max + nbre_entre] == -1)
        {
            EC_cell_pos[no_max] = (int) posx;
            EC_cell_pos[no_max + nbre_entre] = (int) posy;
        }
        /*      Affichage de la cell sur l'environement         */
        /*utilise les coordonnees ou ont ete appris la cel */
        ECx = /*(int)posx= */ EC_cell_pos[no_max];
        ECy = /*(int)posy= */ EC_cell_pos[no_max + nbre_entre];
#ifndef AVEUGLE
        TxDessinerCercle(&image1, no_max * 40, 1, pos, 5, 3);
#endif
        /*efface la precedente */
        if (max_ec_prec != no_max)
        {
#ifndef AVEUGLE
            TxDessinerCercle(&image1, /*max_ec_prec*4000+5 */ 4000, 1, pos, 5,
                             3);
#endif
            max_ec_prec = no_max;
        }
        /*      #endif*/


        strcpy(retour, "robot_pos");
        f = fopen(retour, "a");
        pointx = (int) orgx - pas / 2;
        pointy = (int) orgy - pas / 2;
        fprintf(f, "%d, %d, %d,%d,%d,%d,%d,%d\n", (int) posx, (int) posy,
                no_max * 4000 + 5, pointx, pointy, no_max, ECx, ECy);
        fclose(f);

    }

/*cellule de DG*/
    val = trouver_entree(gpe, (char*)"DG");
    if (val != -1)
    {
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].nbre;
        no_max = no_max1 = no_max2 = 0;
        max = max1 = max2 = -1;
        n = 0;
        for (i = deb_entree; i < (deb_entree + nbre_entre); i++)
        {
            if (neurone[i].s1 > max)
            {
                max2 = max1;
                no_max2 = no_max1;
                max1 = max;
                no_max1 = no_max;
                max = neurone[i].s;
                no_max = i;
            }
            else if (neurone[i].s1 > max1)
            {
                no_max2 = no_max1;
                max2 = max1;
                max1 = neurone[i].s;
                no_max1 = i;
            }
            else if (neurone[i].s1 > max2)
            {
                max2 = neurone[i].s;
                no_max2 = i;
            }

            if (neurone[i].s1 > 0.)
            {
                dg_winners_pos[n] = i;
                n++;
            }
        }
        if (max < 0 || no_max == -1)
        {
            dprints("hiumm Dg\n");
            DGx = DGy = -1;
#ifndef AVEUGLE
#ifdef im2
            sprintf(chaine2, "DG: %d  vide ou pas d'act", no_max);
            draw_a_textline(2, chaine2, -1);
#endif
#endif
        }
        else
        {
            no_max = no_max - deb_entree;
            no_max1 = no_max1 - deb_entree;
            no_max2 = no_max2 - deb_entree;
#ifndef AVEUGLE

            point.x = 0;
            point.y = 20;
#ifdef DEBUG
            dprints("DG: RECOGNIZED place = %d ,posx=%d, posy=%d\n", no_max,
                   point.x, point.y);
#endif
            sprintf(chaine2, "DG:  %d( %f) | %d( %f) | %d( %f) ", no_max, max,
                    no_max1, max1, no_max2, max2);
            dprints("%s\n", chaine2);
            if (fp)
                fprintf(f, "_________\n%s \n", chaine2);

#ifdef im2
            draw_a_textline(2, chaine2, -1);
#endif
#endif
            /*utilise les coordonnees ou ont ete appris la cel */
            DGx = EC_cell_pos[no_max];
            DGy = EC_cell_pos[no_max + nbre_entre];
            max_dg_prec = no_max;
        }
    }




/*cellule de CA1*/
    /*
       val=trouver_entree(gpe, "CA1"); if(val!=-1)
       {
       deb_entree=def_groupe[val].premier_ele;
       nbre_entre=def_groupe[val].taillex*def_groupe[val].tailley;
       increment=def_groupe[val].nbre/nbre_entre;
       max=-1;
       for(i=deb_entree+increment-1;i < (deb_entree+nbre_entre);i+=increment)
       {
       if(neurone[i].s>max) {max=neurone[i].s;no_max=i;}
       } 
       if(max==0||no_max==-1)printf("hiumm\n");
       else
       {
       no_max=no_max-deb_entree;
       point.x = 0  ; point.y=40  ;
       #ifdef DEBUG 
       printf("CA1 RECOGNIZED place = %d ,posx=%d, posy=%d\n",no_max,point.x,point.y);
       #endif
       sprintf(chaine1,"CA1: %d [%d-%d]",no_max,max_dg_prec,no_max/def_groupe[val].taillex);printf("%s\n",chaine1); if(fp)fprintf(f,"CA1: %d [%d-%d]\n",no_max,max_dg_prec,no_max/def_groupe[val].taillex);
       #ifndef AVEUGLE 
       #ifdef im2
       draw_a_textline(3,chaine1 ,-1);
       if(DGx!=-1&&DGy!=-1)
       TxDessinerSegment(&image1, 6, 
       DGp, ECp,2);
       #endif
       #endif
       }
       }
     */
#ifndef AVEUGLE
    sprintf(vale,
            "%-10.10s|%28.28s|%28.28s|%28.28s|%28.28s|%28.28s|%28.28s| ",
            "WINNERS", "1", "2", "3", "4", "5", "6");
    draw_a_textline(4, vale, -1);

    val = trouver_entree(gpe,(char*) "ACC");
    if (val != -1)
    {
        deb_entree_acc = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].taillex * def_groupe[val].tailley;
        increment = def_groupe[val].nbre / nbre_entre;
        max = -1;

        sprintf(chaine6, "%-10.10s", "ACC: ");
        if (winner_ord == NULL)
            winner_ord = malloc(sizeof(neur_dta) * nbre_entre);
        if (winner_ord == NULL)
        {
            EXIT_ON_ERROR("error ACC\n");
            exit(-1);
        }
        ct = class_by_order(val, winner_ord, 0.01); /*printf("passed01, %p\n",winner_ord); */
        cprints("\nct :%d\n", (int) ct);
        for (i = ct; i >= 0; i--)
        {
            p = winner_ord[i].index - deb_entree_acc;
            /*      printf("ok, max:%d\n",(int)i); */
            sprintf(valb, " %d , act (%6f)   |   ", p,
                    neurone[winner_ord[i].index].s2);
            /*                      printf("ok, amax:%d\n-------------\n",(int)ct); */
            sprintf(chaine5, "%-28.28s", valb);
            strcat(chaine6, chaine5);
        }
        if (fp)
            fprintf(f, "%s\n", chaine6);
#ifdef im2
        draw_a_textline(5, chaine6, -1);
#endif
    }
    else
        cprints("noACC\n");
    cprints("%s\n", chaine6);
    cprints("endACC\n");

    /*cellule de CA3 */
    val_ca3 = trouver_entree(gpe,(char*) "CA3");
    if (val_ca3 != -1)
    {
        deb_entree3 = def_groupe[val_ca3].premier_ele;
        nbre_entre =
            def_groupe[val_ca3].taillex * def_groupe[val_ca3].tailley;
        increment_ca3 = def_groupe[val_ca3].nbre / nbre_entre;
#ifdef DISPLAY_GRAPH
        if (plan < 1)
        {
            max = 0.0;
            no_max = -1;
            for (i = deb_entree3 + increment_ca3 - 1;
                 i < (deb_entree3 + nbre_entre); i += increment_ca3)
            {
                if (neurone[i].s1 > max && i - deb_entree3 != max_ca3_prec)
                {
                    max = neurone[i].s1;
                    no_max = i - deb_entree3;
                }
            }
            if (no_max != max_ca3_prec && no_max != -1)
            {

                /*      system("scp goldorak:~/script/nav/mapi.dot ."); */
                if (ca3_trans_mem[no_max] != 1)
                {
                    do
                    {
                        /*      ff=fopen("mapi.dot","a"); */
                        fff =
                            open("mapi.dot", O_WRONLY | O_APPEND | O_EXCL,
                                 0666);
                        if (fff == -1)
                        {
                            dprints("cannot open mapi.dot\n");   /*exit(-1); */
                            perror(0);
                        }
                    }
                    while (fff == -1);
                    cprints("%d         %d %d [%d,%d]  act[%.3f]\n",
                           no_max + deb_entree3,
                           (int) (no_max / increment_ca3),
                           (int) (no_max / increment_ca3),
                           (int) (neurone[no_max + deb_entree3].cste),
                           (int) (neurone[no_max + deb_entree3].posy),
                           neurone[no_max + deb_entree3].s1);
                    cprints
                        ("%d         \"%d\" [label=\"%d [%d,%d]  act[%.3f]\",fillcolor=\"white\",fontcolor=\"black\",shape=box ];\n",
                         no_max + deb_entree3, (int) (no_max / increment_ca3),
                         (int) (no_max / increment_ca3),
                         (int) (neurone[no_max + deb_entree3].cste),
                         (int) (neurone[no_max + deb_entree3].posy),
                         neurone[no_max + deb_entree3].s1);
                    buff = (char *) malloc(255 * sizeof(char));
                    sprintf(buff,
                            "\"%d\" [label=\"%d [%d,%d] [%.3f]\",fillcolor=\"white\",fontcolor=\"black\",shape=box ];\n",
                            (int) (no_max / increment_ca3),
                            (int) (no_max / increment_ca3),
                            (int) (neurone[no_max + deb_entree3].cste),
                            (int) (neurone[no_max + deb_entree3].posy),
                            neurone[no_max + deb_entree3].s1);
                    cprints("write: %d\n",
                           (int) write(fff, buff, strlen(buff) * sizeof(char)));
                    cprints("closed: %d\n", close(fff));
                    free(buff);
                    /*              system("cat mapi.dot mapia.dot >mapu.dot"); */
                    /*      system("neato -Tpng mapi.dot>toto");   */ /*system("scp ./mapi.dot goldorak:~/script/nav/"); */

                    /*      system("scp ./toto goldorak:~/script/nav/"); */
                    /*scanf("%f",&max); */
                }
                max_ca3_prec = no_max;
                ca3_trans_mem[no_max] = 1;
            }
        }
#endif
        sprintf(chaine4, "%-10.10s", "CA3: ");
        /*      printf(" deb%3d, inc:%d\n" ,(deb_entree3),increment_ca3); */
        for (i = ct; i >= 0; i--)
        {
            p = winner_ord[i].index - deb_entree_acc;   /*printf("passed01, %p\n",winner_ord); */
            sprintf(chaine5, " %3d [%d-%d], (%6f)  | ", (p),
                    (int) (neurone
                           [((p + 1) * increment_ca3) + deb_entree3 -
                            1].cste),
                    (int) (neurone
                           [((p + 1) * increment_ca3) + deb_entree3 -
                            1].posy),
                    neurone[((p + 1) * increment_ca3) + deb_entree3 - 1].s2);
            sprintf(vale, "%28.28s", chaine5);
            strcat(chaine4, vale);
        }
        /*printf("%s\n",chaine4); */
        if (fp)
            fprintf(f, "%s\n", chaine4);
#ifdef im2
        draw_a_textline(7, chaine4, -1);
#endif
    }
    else
        cprints("noCA3\n");
#endif

    cprints("passed6\n");


    val = trouver_entree(gpe, (char*)"RECO_ORD");
    if (val != -1)
    {
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].taillex * def_groupe[val].tailley;
        increment = def_groupe[val].nbre / nbre_entre;
        n = 0;
        for (i = deb_entree + increment - 1; i < (deb_entree + nbre_entre);
             i += increment)
        {
            if (neurone[i].s2 >= 0.)
            {
                mvt_reco[n][0] = (int) neurone[i].s2;   /*printf("i:%d,val:%d\n",i-deb_entree,(int)neurone[i].s2); */
                n++;
            }
        }
        n_mvt = n;
        cprints("n_reco:%d\n", n_mvt);
    }

    val = trouver_entree(gpe, (char*)"MVT_ORD");
    if (val != -1)
    {
        n_mvt = 0;
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].taillex * def_groupe[val].tailley;
        increment = def_groupe[val].nbre / nbre_entre;
        n = 0;
        for (i = deb_entree + increment - 1; i < (deb_entree + nbre_entre);
             i += increment)
        {
            if (neurone[i].s2 >= 0.)
            {
                mvt_reco[n][1] = (int) neurone[i].s2;   /*printf("i:%d,val:%d\n",i-deb_entree,(int)neurone[i].s2); */
                n++;
            }
        }
        n_mvt = n;
        cprints("n_mvt:%d\n", n_mvt);
    }


    /*cellule du sub */
    val = trouver_entree(gpe, (char*)"SUB");
    if (val != -1)
    {
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].taillex * def_groupe[val].tailley;
        increment = def_groupe[val].nbre / nbre_entre;
        max = -1;
        n = 0;
        /*recherche du max pour normalisation du vecteur */
        for (i = deb_entree + increment - 1; i < (deb_entree + nbre_entre);
             i += increment)
        {
            if (neurone[i].s1 > max)
            {
                max = neurone[i].s;
                no_max = i;
            }
            /*printf("act_sub :%d: %f\n",i-deb_entree,neurone[i].s1); */ }
        /*calcule les vecteurs correspondants au entrees, pos dest */
        for (i = deb_entree + increment - 1; i < (deb_entree + nbre_entre);
             i += increment)
        {
            if (neurone[i].s1 > 0.)
            {
                if (plan < 1)
                    max = 1;
                angle = ((i - deb_entree) * (2 * M_PI / nbre_entre)) - M_PI;
                mvt_reco[n][2] =
                    (int) ((float) (pas * neurone[i].s1 / max) * cos(angle));
                mvt_reco[n][3] =
                    (int) ((float) (pas * neurone[i].s1 / max) * sin(angle));
                /*      printf(" print infos: angle:%f, destx:%d,desty:%d,orgx: %d,orgy: %d, n:%d, act:%f\n",angle,mvt_reco[n][2],mvt_reco[n][3],(orgx),(orgy),n,neurone[i].s1); */
                n++;
            }
        }
        if (max < 0 || no_max == -1)
        {
            dprints("hiumm\n");
            test = scanf("%d", &no_max);
            if(test == 0){dprints("Echec du scanf");}
        }
        else
        {
            dprints("passed0\n");
            /*affiche le vecteur a partire de la pos courante du robot */
            /*affiche tous les vevteurs trouves */
            if ((orgx != -1 && orgy != -1)
                && ((plan > 0) || (plan < 1 && max_dg_prec != max_ec_prec)))
            {
                /*recherche la cellule de lieux d'origine de chaque mvt a partir de la transition de reconnaissance corresponadante... */
                for (i = deb_entree + increment - 1;
                     i < (deb_entree + nbre_entre); i += increment)
                {
                    for (k = 0; k < n_mvt; k++)
                    {
                        if (i == mvt_reco[k][1] + deb_entree)
                        {
                            mvt_reco[k][4] =
                                (int) neurone[(mvt_reco[k][0] + 1) *
                                              increment_ca3 +
                                              def_groupe[val_ca3].
                                              premier_ele - 1].cste;
                                         /*printf("vect n�%d,winner n�:%d,cell_org:%d,acc:%d,ca3:%d\n",i-deb_entree,k,mvt_reco[k][4],mvt_reco[k][0], (mvt_reco[k][0]+1)*increment_ca3 */ /*+def_groupe[val_ca3].premier_ele*-1    ); */
                            mvt_reco[k][5] = i;
                            if (orgx > 0 && orgy > 0)
                            {
                                /*affecte les coord d'org et de dest du vecteur a l'indice de la trans */
                                strcpy(retour, "robot_vect");
                                f = fopen(retour, "a");
                                fprintf(f, "	 %d, %d, %d, %d,%d,%d,%d\n",
                                        mvt_reco[k][0], DGx, DGy,
                                        mvt_reco[k][2] + orgx,
                                        mvt_reco[k][3] + orgy, orgx, orgy);
                                fclose(f);
                            }
                        }
                    }
                }
#ifndef AVEUGLE
                for (i = 0; i < n_mvt; i++)
                {
                    dest.x = mvt_reco[i][2] + orgx;
                    dest.y = mvt_reco[i][3] + orgy;
                    org.x = orgx;
                    org.y = orgy;
                    cprints
                        ("drawing now at:dest.x:%d ,dest.y:%d, mvt_reco.x=%d,mvt_reco.y:%d,colour:%d \n",
                         dest.x, dest.y, mvt_reco[i][2], mvt_reco[i][3],
                         mvt_reco[i][4]);
                    draw_vector(image1, org, dest, mvt_reco[i][4]);
                    cprints("passed1\n");
                }
#endif
            }
#ifndef AVEUGLE
            dprints("ok, max:%d\n", ct);
            sprintf(chaine6, "%-10.10s", "SUB: ");
            for (i = ct; i >= 0; i--)
            {
                for (k = 0; k < n_mvt; k++)
                {
                    if (winner_ord[i].index - deb_entree_acc ==
                        mvt_reco[k][0])
                    {
                        /*                      printf("okm, k:%d win: %d, acc:%d,mvt_reco:%d,k[5]:%d\n",k,i,mvt_reco[k][0],mvt_reco[k][1],mvt_reco[k][5]-deb_entree); */
                        p = mvt_reco[k][5] - deb_entree;
                        sprintf(valb, " %d , act (%f)   |   ", p,
                                neurone[mvt_reco[k][5]
                                        /*winner_ord[i].index+deb_entree */ ].
                                s2);
                        sprintf(chaine5, "%-28.28s", valb);
                        strcat(chaine6, chaine5);

                    }
                }
            }

            /*      printf("%s\n",chaine6); */ if (fp)
                fprintf(f, "SUB: %d", no_max);

#ifdef im2
            draw_a_textline(6, chaine6, -1);
#endif
#endif
        }
    }
    cprints("passed6\n");



#ifndef AVEUGLE
/*cellule du but*/
    val = trouver_entree(gpe,(char*) "BUT");
    if (val != -1)
    {
        deb_entree = def_groupe[val].premier_ele;
        nbre_entre = def_groupe[val].taillex * def_groupe[val].tailley;
        increment = def_groupe[val].nbre / nbre_entre;
        max = -1;
        cprints("passed9\n");
        sprintf(chaine6, "BUT: ");

        for (i = ct; i >= 0; i--)
        {
            p = winner_ord[i].index - deb_entree_acc;
            sprintf(valb, " %3d [%d-%d], (%6f)  | ", (p),
                    (int) (neurone
                           [((p + 1) * increment_ca3) + deb_entree3 -
                            1].cste),
                    (int) (neurone
                           [((p + 1) * increment_ca3) + deb_entree3 -
                            1].posy),
                    neurone[(p + 1) * increment + deb_entree - 1].s2);
            /*printf( " %3d [%d-%d], (%6f)  | " ,(p),(int)neurone[((p+1)*increment_ca3)+deb_entree3-1].cste,(int)neurone[((p+1)*increment_ca3)+deb_entree3-1].posy,
               neurone[(p+1)*increment+deb_entree-1].s2); */
            sprintf(chaine5, "%-28.28s", valb);
            strcat(chaine6, chaine5);
        }
        if (fp)
            fprintf(f, "%s\n", chaine6);

#ifdef im2
        draw_a_textline(8, chaine6, -1);
#endif

        cprints("passed9\n");
        cprints("passed9end\n");
    }
    else
        cprints("noBUT\n");
#endif
    cprints("passedend6\n");
/*cellule du but*/



/*
   if(fp){
	fprintf(f,"________\n");
fclose(f);
  }*/
    count++;
    pos_precx = pos_org1x;
    pos_precy = pos_org1y;
    printf("passedend6\n");
#ifndef AVEUGLE
    TxFlush(&image1);
    TxFlush(&image2);
#endif
    cprints("passedend6\n");
    cprints("gpe:%d\n", gpe);
    cprints("end\n");
    
    
    (void) fff;
    (void) ct;
    (void) deb_entree_acc;
    (void) winner_ord;
    (void) ca3_trans_mem;
    (void) max_ca3_prec;
    (void) buff;
    (void) chaine6;
    (void) deb_entree3;
    (void) p;
    (void) valb; 
	(void) vale;
	(void) chaine5;
	(void) chaine4;
	(void) chaine2;
    
    
    }
