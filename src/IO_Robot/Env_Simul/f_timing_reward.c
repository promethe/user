/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_timing_reward.c 
    \brief 

    Author: Julien Hirel
    Created: 11/06/2008
    Modified:
    - author: 
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
    - Saves information about the entry and exit times at the goal location, and about the timing of the movement inhibition and reward 

    Macro:

    Local varirables:

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    Takes the following links as input :
    - EC : link from the group representing EC (after WTA)
    - sound : link from the group representing the hearing of the sound
    - auto_reward : link from a group whose activity enables the automatic release of the reward after a specific time delay
    - time_to_sound : link from a group whose activity regulate the timing for the automatic reward release (in sec)
    - inhib_mvt : link from the group controlling the inhibition of the movement
    - time_generator : if not in REAL_TIME mode, link from the group simulating the time (TimeGenerator)

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

#define DEBUG
#define DEFAULT_TIME_TO_SOUND 2.0

typedef struct mydata_timing_reward
{
      int gpe_EC;
      int gpe_auto_reward;
      int gpe_sound;
      int gpe_time_to_sound;
      int gpe_inhib_mvt;
      int goal_index_EC;
      int last_place_index;
      struct timeval sound_time;
      struct timeval simulation_start_time;
      struct timeval place_entered_time;
      struct timeval inhib_mvt_start_time;
      struct timeval inhib_mvt_stop_time;
#ifndef REAL_TIME
      int gpe_time_generator;
#endif
      
}mydata_timing_reward;


static void timeval_subtract(struct timeval * result, struct timeval * x, struct timeval * y)
{
   long nsec = 0;

   /* subtracts timeval x by y, and stores the result in result */
   if (x->tv_usec < y->tv_usec) 
   {
      result->tv_usec = 1000000 + x->tv_usec - y->tv_usec;
      nsec = -1;
   }
   else
      result->tv_usec = x->tv_usec - y->tv_usec;
   
   result->tv_sec = nsec + x->tv_sec - y->tv_sec;
}


void new_timing_reward(int gpe)
{ 
   int gpe_EC = -1, gpe_sound = -1, gpe_auto_reward = -1, gpe_time_to_sound = -1, gpe_inhib_mvt = -1;
#ifndef REAL_TIME   
   int gpe_time_generator = -1;
#endif
   int i, l;
   FILE *fp;
   mydata_timing_reward *my_data = NULL;

   if (def_groupe[gpe].data == NULL)
   {
      l = 0;
      i = find_input_link(gpe, l);    /* recherche les liens en entree */
      while (i != -1)
      {
	 if (strcmp(liaison[i].nom, "EC") == 0)
	    gpe_EC = liaison[i].depart;
	 if (strcmp(liaison[i].nom, "sound") == 0)
	    gpe_sound = liaison[i].depart;
	 if (strcmp(liaison[i].nom, "auto_reward") == 0)
	    gpe_auto_reward = liaison[i].depart;
	 if (strcmp(liaison[i].nom, "time_to_sound") == 0)
	    gpe_time_to_sound = liaison[i].depart;
	 if (strcmp(liaison[i].nom, "inhib_mvt") == 0)
	    gpe_inhib_mvt = liaison[i].depart;

#ifndef REAL_TIME
	 if (strcmp(liaison[i].nom, "time_generator") == 0)
	     gpe_time_generator = liaison[i].depart;
#endif

	 l++;
	 i = find_input_link(gpe, l);

      }


#ifndef REAL_TIME
      if (gpe_time_generator == -1)
      {
	 fprintf(stderr, "ERROR in new_timing_reward(%s) : Missing 'time_generator' input link\n", def_groupe[gpe].no_name);
	 exit(1);
      }
#endif

      if (gpe_EC == -1 || gpe_sound == -1 || gpe_inhib_mvt == -1)
      {
	 fprintf(stderr, "ERROR in new_timing_reward(%s) : Missing input links\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data = (mydata_timing_reward*) malloc(sizeof(mydata_timing_reward));
      if (my_data == NULL)
      {
	 fprintf(stderr, "ERROR in new_timing_reward(%s): malloc failed for data\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data->gpe_EC = gpe_EC;
      my_data->gpe_sound = gpe_sound;
      my_data->gpe_auto_reward = gpe_auto_reward;
      my_data->gpe_time_to_sound = gpe_time_to_sound;
      my_data->gpe_inhib_mvt = gpe_inhib_mvt;
      my_data->goal_index_EC = -1;
      my_data->last_place_index = -1;
      my_data->place_entered_time.tv_sec = my_data->place_entered_time.tv_usec = 0;
      my_data->sound_time.tv_sec = my_data->sound_time.tv_usec = 0;
      my_data->inhib_mvt_start_time.tv_sec = my_data->inhib_mvt_start_time.tv_usec = 0;
      my_data->inhib_mvt_stop_time.tv_sec = my_data->inhib_mvt_stop_time.tv_usec = 0;

      /* Saving the initial time */
#ifdef REAL_TIME
      gettimeofday(&my_data->simulation_start_time, NULL);
#else
      my_data->gpe_time_generator = gpe_time_generator;
      my_data->simulation_start_time.tv_sec = my_data->simulation_start_time.tv_usec = 0;
#endif

      dprints("new_timing_reward(%s): simulation start time : %d sec, %d usec\n", 
	      def_groupe[gpe].no_name, my_data->simulation_start_time.tv_sec, my_data->simulation_start_time.tv_usec);
      def_groupe[gpe].data = my_data;

      fp = fopen("timing.SAVE", "w+");
      if (fp == NULL)
      {
	 fprintf(stderr, "ERROR in new_timing_reward(%s): Cannot open file timing.SAVE\n", def_groupe[gpe].no_name);
	 exit(1);
      }
      fprintf(fp, "PC_index time_entered time_left\n");
      fclose(fp);

      fp = fopen("goal_timing.SAVE", "w+");
      if (fp == NULL)
      {
	 fprintf(stderr, "ERROR ni new_timing_reward(%s): Cannot open file goal_timing.SAVE\n", def_groupe[gpe].no_name);
	 exit(1);
      }
      fprintf(fp, "PC_index time_entered inhib_mvt_start_time inhib_mvt_stop_time sound_time time_left\n");
      fclose(fp);

   }
}

 
void function_timing_reward(int gpe)
{
   int i, ind_max = -1, ind_second = -1;
   int gpe_EC = -1;
   int gpe_sound = -1;
   int goal_index_EC = -1;
   int gpe_inhib_mvt = -1;
   int gpe_auto_reward = -1;
   int gpe_time_to_sound = -1;
   int last_place_index = -1;
   double max, second_max, time_double, time_to_sound = DEFAULT_TIME_TO_SOUND;
   struct timeval current_time, time_since_place_entered;
   mydata_timing_reward *my_data = NULL;
#ifndef REAL_TIME
   struct timeval *ref_time = NULL;
#endif    
   FILE *fp;
   char buffer[30];

   my_data = (mydata_timing_reward *) def_groupe[gpe].data;
   gpe_sound = my_data->gpe_sound;
   gpe_EC = my_data->gpe_EC;
   gpe_auto_reward = my_data->gpe_auto_reward;
   gpe_inhib_mvt = my_data->gpe_inhib_mvt;
   gpe_time_to_sound = my_data->gpe_time_to_sound;
   goal_index_EC = my_data->goal_index_EC;
   last_place_index = my_data->last_place_index;

   if (gpe_time_to_sound > -1)
   {
      time_to_sound = neurone[def_groupe[gpe_time_to_sound].premier_ele].s1;
   }

#ifndef REAL_TIME
   ref_time = (struct timeval *) def_groupe[my_data->gpe_time_generator].ext;

   if (ref_time == NULL)
   {
      fprintf(stderr, "ERROR in f_timing_reward(%s) : NULL pointer for ext in time_generator group\n", def_groupe[gpe].no_name);
      exit(1);
   }
#endif

   neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = 0;

   /* Gets the current place (Winning place cell index in EC) */
   max = -1.;
   second_max = -1.;
   for (i = 0; i < def_groupe[gpe_EC].nbre; i++)
   {
      if (neurone[def_groupe[gpe_EC].premier_ele + i].s > max)
      {
	 second_max = max;
	 ind_second = ind_max;

	 max = neurone[def_groupe[gpe_EC].premier_ele + i].s;
	 ind_max = i;
      }
      else if (neurone[def_groupe[gpe_EC].premier_ele + i].s > second_max)
      {
	 second_max = neurone[def_groupe[gpe_EC].premier_ele + i].s;
	 ind_second = i;
      }
   }

#ifdef REAL_TIME
   gettimeofday(&current_time, NULL);
#else
   current_time.tv_sec = ref_time->tv_sec;
   current_time.tv_usec = ref_time->tv_usec;
#endif
   timeval_subtract(&current_time, &current_time, &my_data->simulation_start_time);
   timeval_subtract(&time_since_place_entered, &current_time, &my_data->place_entered_time);

   /* Detects the hearing of the sound (pellet released) */
   if (neurone[def_groupe[gpe_sound].premier_ele].s1 > 0.5)
   {
      my_data->sound_time.tv_sec = current_time.tv_sec;
      my_data->sound_time.tv_usec = current_time.tv_usec;

      /* Saves the index of the goal place in EC */
      if (goal_index_EC == -1)
      {
	 goal_index_EC = ind_second;
 	 my_data->goal_index_EC = goal_index_EC;

	 dprints("f_timing_reward(%s) : neurone coding for goal PC located : index = %d\n", def_groupe[gpe].no_name, goal_index_EC);
      }
   }
 
   /* Checking if the current place has changed */
   if (last_place_index != -1 && ind_max != last_place_index)
   {
      fp = fopen("timing.SAVE", "a");
      if (fp == NULL)
      {
	 fprintf(stderr, "ERROR in f_timing_reward(%s) : Cannot open file timing.SAVE\n", def_groupe[gpe].no_name);
	 exit(1);
      }
      fprintf(fp, "%d %d.%d %d.%d\n", last_place_index, 
	      (int) my_data->place_entered_time.tv_sec, (int)my_data->place_entered_time.tv_usec, (int)current_time.tv_sec, (int)current_time.tv_usec);
      fclose(fp);

      if (last_place_index == goal_index_EC)
      {
	 dprints("f_timing_reward(%s): leaving goal location\n", def_groupe[gpe].no_name);

	 fp = fopen("goal_timing.SAVE", "a");
	 if (fp == NULL)
	 {
	    fprintf(stderr, "ERROR in f_timing_reward(%s) : Cannot open file goal_timing.SAVE\n", def_groupe[gpe].no_name);
	    exit(1);
	 }
	 fprintf(fp, "%d %d.%d %d.%d %d.%d %d.%d %d.%d\n", last_place_index, (int) my_data->place_entered_time.tv_sec, (int)my_data->place_entered_time.tv_usec, 
		 (int)my_data->inhib_mvt_start_time.tv_sec, (int)my_data->inhib_mvt_start_time.tv_usec, (int)my_data->inhib_mvt_stop_time.tv_sec, 
		 (int)my_data->inhib_mvt_stop_time.tv_usec, (int)my_data->sound_time.tv_sec, (int)my_data->sound_time.tv_usec, 
		 (int)current_time.tv_sec, (int)current_time.tv_usec);
	 fclose(fp);

      my_data->sound_time.tv_sec = my_data->sound_time.tv_usec = 0;
      my_data->inhib_mvt_start_time.tv_sec = my_data->inhib_mvt_start_time.tv_usec = 0;
      my_data->inhib_mvt_stop_time.tv_sec = my_data->inhib_mvt_stop_time.tv_usec = 0;
      }

      my_data->place_entered_time.tv_sec = current_time.tv_sec;
      my_data->place_entered_time.tv_usec = current_time.tv_usec;
   }

   /* Automatic reward delivery */
   if(ind_max == goal_index_EC && gpe_auto_reward > -1 && neurone[def_groupe[gpe_auto_reward].premier_ele].s1 > 0.5)
   {
      sprintf(buffer, "%d.%d", (int)time_since_place_entered.tv_sec, (int)time_since_place_entered.tv_usec);
      time_double = atof(buffer);
      if (my_data->sound_time.tv_sec == 0 && time_double > time_to_sound)
      {
	 dprints("f_timing_reward(%s): time_to_sound is %f sec, time spent in goal location is %f sec, releasing pellet\n", 
		 def_groupe[gpe].no_name, time_to_sound, time_double);

	 neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = 1;
      }
   }

   /* Saves information about the timing of the movement inhibition */
   if (neurone[def_groupe[gpe_inhib_mvt].premier_ele].s1 > 0)
   {
      if (my_data->inhib_mvt_start_time.tv_sec == 0)
      {
	 my_data->inhib_mvt_start_time.tv_sec = current_time.tv_sec;
	 my_data->inhib_mvt_start_time.tv_usec = current_time.tv_usec;
      }
   }
   else if (my_data->inhib_mvt_start_time.tv_sec > 0)
   {
      my_data->inhib_mvt_stop_time.tv_sec = current_time.tv_sec;
      my_data->inhib_mvt_stop_time.tv_usec = current_time.tv_usec;
   }

   my_data->last_place_index = ind_max;
}


void destroy_timing_reward(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((mydata_timing_reward*)def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_timing_reward(%s): Leaving function\n", def_groupe[gpe].no_name);
}
