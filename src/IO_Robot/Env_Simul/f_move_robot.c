/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_move_robot f_move_robot
\brief 

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 L'init de l'environnement n'est faite qu'a la premiere iter des simul avec promethe


\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-teleguide()

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <stdio.h>
#include <IO_Robot/Env_Simul.h>
#include "tools/include/teleguide.h"
#include "tools/include/local_var.h"

#include <Global_Var/IO_Robot.h>

void function_move_robot(int numero)
{
    int dir;

    if (choix_mov == 0)
    {
        cprints("movement direction : ");
        if(scanf("%d", &dir))
			{teleguide(dir);}
		else{EXIT_ON_ERROR("Erreur de lecture (scanf) direction");}
    }
    else if (choix_mov == 1)
    {
        function_move_robot_alea(numero);
    }
    else if (choix_mov == 2)
    {
        function_move_robot_info(numero);
    }
}
