/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_trajectory_XY_teacher.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Fonction VB  :   Met sur le neurone de la boite le max de l'activite  
  analogique ( .s) de la boite precedante.				 
  Cette fonction est utilise dans la planification pour recuperer	 
  l'intensite de la reconnaissance du lieux le mieux reconnu.	

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include "tools/include/local_var.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tools/include/mise_a_echelle_cases_vers_pix.h"
void affiche_target(float x, float y, float dx, float dy, int coul);

typedef struct MyData_f_trajectory_XY_teacher
{
    int gpe_teaching;
    char file_name[255];
    FILE *file_desc;
    int pt[56000][2];
    int nb_pt;
    float d_min_prec;
    float Rmax;

} MyData_f_trajectory_XY_teacher;
/* #define DEBUG*/
void function_trajectory_XY_teacher(int gpe)
{
    char param_link[255];
    char file_name[255];
    int i, flag_file = 0, l;
    MyData_f_trajectory_XY_teacher *mydata = NULL;
    FILE *file_desc;
    int gpe_teaching = -1;
    int pt[56000][2];
    int nb_pt;
    int ind_min;
    float d_min;
    float Rmax = 20.;
    float alpha, d_min_prec = -1000.;
    float dist;
    int cpt, j;
    char st[10];
    TxPoint point;
    int ret;

    (void) ret; // (unused)
    (void) point; // (unused)
    (void) d_min_prec; // (unused)


    if (def_groupe[gpe].data == NULL)
    {

        if (def_groupe[gpe].nbre != 2)
        {
            printf("il faut 2 neuron pour %s(%d) au lieu de %d neurons \n", __FUNCTION__,gpe,def_groupe[gpe].nbre);
            exit(0);
        }
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "f", param_link) == 2)
            {
                sprintf(file_name, "%s", param_link);
                flag_file = 1;
            }
            if (prom_getopt(liaison[l].nom, "r", param_link) == 2)
            {
                Rmax = atof(param_link);
            }
            if (prom_getopt(liaison[l].nom, "t", param_link) >= 1)
            {
                gpe_teaching = liaison[l].depart;
            }

            i++;
            l = find_input_link(gpe, i);
        }

        if (flag_file == 0)
        {
            printf
                ("probleme: pas de fichier trouve avec l'optio -f dans %s\n",
                 __FUNCTION__);
            exit(0);
        }


        file_desc = fopen(file_name, "r");
        if (file_desc == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name);
            exit(0);
        }

        cpt = 0;
        for (i = 0; i < nb_cases; i++)
        {
            for (j = 0; j < nb_cases; j++)
            {
                ret = fscanf(file_desc, "%s", st);
                /*landmark */
                if (st[0] == 'T')
                {
                    pt[atoi(st + 1)][0] =
                        (int) mise_a_echelle_cases_vers_pix(j);
                    pt[atoi(st + 1)][1] =
                        (int) mise_a_echelle_cases_vers_pix(i);
                    cpt++;
                    printf("%d:%s\n", cpt, st);
                }
            }
            ret = fscanf(file_desc, "\n");
        }
        fclose(file_desc);
        i = cpt;


        nb_pt = i;
        printf("nb_pt = %d\n", nb_pt);

        mydata =
            (MyData_f_trajectory_XY_teacher *)
            malloc(sizeof(MyData_f_trajectory_XY_teacher));
        if (mydata == NULL)
        {
            printf("probleme malloc %s %d\n", __FUNCTION__, __LINE__);
            exit(0);
        }
        sprintf(mydata->file_name, "%s", file_name);
        mydata->file_desc = file_desc;
        mydata->gpe_teaching = gpe_teaching;
        mydata->Rmax = Rmax;

        for (i = 0; i < nb_pt; i++)
        {
            mydata->pt[i][0] = pt[i][0];
            mydata->pt[i][1] = pt[i][1];
        }

#ifndef AVEUGLE
        for (i = 0; i < nb_pt; i++)
        {
            point.x = pt[i][0] - 1;
            point.y = (int) pt[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, -3000, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
            TxEcrireChaine(&image1, noir, point, st, 0);
#endif
        }
#endif
        mydata->nb_pt = nb_pt;
        def_groupe[gpe].data = (MyData_f_trajectory_XY_teacher *) mydata;
    }
    else
    {
        mydata = (MyData_f_trajectory_XY_teacher *) def_groupe[gpe].data;
        gpe_teaching = mydata->gpe_teaching;
        nb_pt = mydata->nb_pt;
        for (i = 0; i < nb_pt; i++)
        {
            pt[i][0] = mydata->pt[i][0];
            pt[i][1] = mydata->pt[i][1];
        }
        d_min_prec = mydata->d_min_prec;
        Rmax = mydata->Rmax;
    }


    /*Chercher le point le plus proche, calculer rayon, si > valeur, reorienter vers le prochain point */
    if (gpe_teaching != -1)
    {
        if (neurone[def_groupe[gpe_teaching].premier_ele].s1 < 0.5)
        {
            neurone[def_groupe[gpe].premier_ele].s =
                neurone[def_groupe[gpe].premier_ele].s1 =
                neurone[def_groupe[gpe].premier_ele].s2 = 0.;
            neurone[def_groupe[gpe].premier_ele + 1].s =
                neurone[def_groupe[gpe].premier_ele + 1].s1 =
                neurone[def_groupe[gpe].premier_ele + 1].s2 = 0.;
            return;

        }
    }

    d_min = 999999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));
        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }


    if (fabs(pt[ind_min][0] - posx) > fabs(pt[ind_min][1] - posy))
        d_min = fabs(pt[ind_min][0] - posx);
    else
        d_min = fabs(pt[ind_min][1] - posy);
#ifdef DEBUG
    printf("nd_courant : %d,", ind_min);
#endif    
    if (ind_min >= nb_pt - 5)
        ind_min = ind_min - nb_pt;
#ifdef DEBUG
    printf("target:%d \n", ind_min + 5);
#endif
    mydata->d_min_prec = d_min;
#ifndef ROUTEUR
    alpha =
        atan2f(pt[ind_min + 5][1] - posy, pt[ind_min + 5][0] - posx) + M_PI;
#else
    printf
        ("atan2f n'est pas disponible sur le routeur... le code de f_trajectory_XY_teacher ne fct probablement pas...\n");
#endif
    if (d_min <= Rmax)
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
    }
    else
    {
#ifdef DEBUG
        printf("d_min= %f  d_min_prec = %f \n ", d_min, d_min_prec);
#endif        
        if (d_min <= d_min_prec)
        {
            neurone[def_groupe[gpe].premier_ele].s =
                neurone[def_groupe[gpe].premier_ele].s1 =
                neurone[def_groupe[gpe].premier_ele].s2 = 0.;
            neurone[def_groupe[gpe].premier_ele + 1].s =
                neurone[def_groupe[gpe].premier_ele + 1].s1 =
                neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
        }
        else
        {
            neurone[def_groupe[gpe].premier_ele].s =
                neurone[def_groupe[gpe].premier_ele].s1 =
                neurone[def_groupe[gpe].premier_ele].s2 = 1.;
            neurone[def_groupe[gpe].premier_ele + 1].s =
                neurone[def_groupe[gpe].premier_ele + 1].s1 =
                neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
        }

    }

}

/*3 modes pour cette fonction: 0, 1 et 2*/

typedef struct MyData_f_trajectory_XY_teacher_2
{
    int gpe_teaching;
    char file_name[255];
    FILE *file_desc;
    int pt[56000][2];
    int nb_pt;
    float d_min_prec;
    float Rmax;
    int guidage;

} MyData_f_trajectory_XY_teacher_2;

void function_trajectory_XY_teacher_2(int gpe)
{
    char param_link[255];
    char file_name[255];
    int i, flag_file = 0, l;
    MyData_f_trajectory_XY_teacher_2 *mydata = NULL;
    FILE *file_desc;
    int gpe_teaching = -1;
    int pt[56000][2];
    int nb_pt;
    int ind_min;
    float d_min;
    float Rmax = 20.;
    float alpha, d_min_prec = -1000.;
    float dist;
    int cpt, j;
    char st[10];
    int guidage;
    TxPoint point;
    int ret;

    (void) ret; // (unused)
    (void) point; // (unused)
    (void) d_min_prec; // (unused)

    if (def_groupe[gpe].data == NULL)
    {

        if (def_groupe[gpe].nbre != 2)
        {
            printf("il faut 2 neuron pour %s(%d) au lieu de %d neurons \n", __FUNCTION__,gpe,def_groupe[gpe].nbre);
            exit(0);
        }
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "f", param_link) == 2)
            {
                sprintf(file_name, "%s", param_link);
                flag_file = 1;
            }
            if (prom_getopt(liaison[l].nom, "r", param_link) == 2)
            {
                Rmax = atof(param_link);
            }
            if (prom_getopt(liaison[l].nom, "t", param_link) >= 1)
            {
                gpe_teaching = liaison[l].depart;
            }

            i++;
            l = find_input_link(gpe, i);
        }

        if (gpe_teaching == -1)
        {
            printf("pas de groupe -t dans %s\n", __FUNCTION__);
            exit(0);
        }

        if (flag_file == 0)
        {
            printf
                ("probleme: pas de fichier trouve avec l'optio -f dans %s\n",
                 __FUNCTION__);
            exit(0);
        }


        file_desc = fopen(file_name, "r");
        if (file_desc == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name);
            exit(0);
        }

        cpt = 0;
        for (i = 0; i < nb_cases; i++)
        {
            for (j = 0; j < nb_cases; j++)
            {
                ret = fscanf(file_desc, "%s ", st);
                /*landmark */
                if (st[0] == 'T')
                {
                    pt[atoi(st + 1)][0] =
                        (int) mise_a_echelle_cases_vers_pix(j);
                    pt[atoi(st + 1)][1] =
                        (int) mise_a_echelle_cases_vers_pix(i);
                    cpt++;
                }
            }
            ret = fscanf(file_desc, "\n");
        }
        fclose(file_desc);
        i = cpt;

       
        nb_pt = i;
        printf("nb_pt = %d\n", nb_pt);

        mydata =
            (MyData_f_trajectory_XY_teacher_2 *)
            malloc(sizeof(MyData_f_trajectory_XY_teacher_2));
        if (mydata == NULL)
        {
            printf("probleme malloc %s %d\n", __FUNCTION__, __LINE__);
            exit(0);
        }
        sprintf(mydata->file_name, "%s", file_name);
        mydata->file_desc = file_desc;
        mydata->gpe_teaching = gpe_teaching;
        mydata->Rmax = Rmax;
        guidage = 0;
        mydata->guidage = guidage;
        for (i = 0; i < nb_pt; i++)
        {
            mydata->pt[i][0] = pt[i][0];
            mydata->pt[i][1] = pt[i][1];
        }

#ifndef AVEUGLE
        for (i = 0; i < nb_pt; i++)
        {
            point.x = pt[i][0] - 1;
            point.y = (int) pt[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, -3000, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
/*		TxEcrireChaine(&image1,noir,point,st,0);*/
#endif
        }
#endif
        mydata->nb_pt = nb_pt;
        def_groupe[gpe].data = (MyData_f_trajectory_XY_teacher_2 *) mydata;
    }
    else
    {
        mydata = (MyData_f_trajectory_XY_teacher_2 *) def_groupe[gpe].data;
        gpe_teaching = mydata->gpe_teaching;
        nb_pt = mydata->nb_pt;
        for (i = 0; i < nb_pt; i++)
        {
            pt[i][0] = mydata->pt[i][0];
            pt[i][1] = mydata->pt[i][1];
        }
        d_min_prec = mydata->d_min_prec;
        Rmax = mydata->Rmax;
        guidage = mydata->guidage;
    }


    /*Chercher le point le plus proche, calculer rayon, si > valeur, reorienter vers le prochain point */

    /*aucun guidage */
    if (neurone[def_groupe[gpe_teaching].premier_ele].s1 < 0.5)
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = 0.;
#ifdef DEBUG   
printf("%s: pas de guidage\n", __FUNCTION__);
#endif
        return;
    }


    d_min = 999999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));
        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }


    if (fabs(pt[ind_min][0] - posx) > fabs(pt[ind_min][1] - posy))
        d_min = fabs((float) (pt[ind_min][0] - posx));
    else
        d_min = fabs((float) (pt[ind_min][1] - posy));
#ifdef DEBUG
    printf("dmin:%f\n", d_min);
    printf("nd_courant : %d,", ind_min);
#endif
    if (ind_min >= nb_pt - 5)
        ind_min = ind_min - nb_pt;
#ifdef DEBUG
    printf("target:%d \n", ind_min + 5);
#endif     
    mydata->d_min_prec = d_min;
#ifndef ROUTEUR
    alpha =
        atan2f(pt[ind_min + 5][1] - posy, pt[ind_min + 5][0] - posx) + M_PI;
#else
    printf
        ("atan2f n'est pas disponible sur le routeur... le code de f_trajectory_XY_teacher ne fct probablement pas...\n");
#endif
/*Mode correct*/
    if (d_min >= Rmax || guidage > 0
        || neurone[def_groupe[gpe_teaching].premier_ele].s1 > 1.5)
    {
#ifdef DEBUG
        printf("%s: guidage correctif\n", __FUNCTION__);
#endif        
        if (guidage == 0)
        {
            guidage = 1;
        }

        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 1.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;

        if (d_min <= Rmax / 4.)
        {
            guidage = 0;
        }
    }
    else
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
    }

    mydata->guidage = guidage;
}

typedef struct MyData_f_trajectory_XY_teacher_3
{
    int gpe_teaching;

    char file_name[255];
    FILE *file_desc;
    int pt[56000][2];
    int nb_pt;

    float d_min_prec;
    float Rmax;
    int guidage;
/*****************************/
    char file_name_eval[255];
    FILE *file_desc_eval;
    int pt_eval[56000][2];
    int nb_pt_eval;

    float dmin_pt[56000];
    float sum_pres;
    float sum_loin;
    int nb_mesure;
    int gpe_reset_eval;
} MyData_f_trajectory_XY_teacher_3;

void function_trajectory_XY_teacher_3(int gpe)
{
    char param_link[255];
    char file_name[255], file_name_eval[255];
    int i, flag_file = 0, flag_file_2 = 0, l;
    MyData_f_trajectory_XY_teacher_3 *mydata = NULL;
    FILE *file_desc, *file_desc_eval;
    int gpe_teaching = -1;
    int pt[56000][2];
    float pt_eval[56000][2];
    int nb_pt, nb_pt_eval;
    int ind_min;
    float d_min;
    float Rmax = 20.;
    float alpha, d_min_prec = -1000.;
    float dist;
    int cpt, j;
    char st[10];
    int guidage;
    int gpe_reset_eval = -1;
    TxPoint point;
    int ret;

    (void) point; // (unused);
    (void) d_min_prec; // (unused)
    (void) ret; // (unused)

    if (def_groupe[gpe].data == NULL)
    {
        printf("init %s\n", __FUNCTION__);
        if (def_groupe[gpe].nbre != 4)
        {
            printf("il faut 4 neuron pour %s\n", __FUNCTION__);
            exit(0);
        }
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "f", param_link) == 2)
            {
                sprintf(file_name, "%s", param_link);
                flag_file = 1;
            }
            if (prom_getopt(liaison[l].nom, "e", param_link) == 2)
            {
                sprintf(file_name_eval, "%s", param_link);
                flag_file_2 = 1;
            }
            if (prom_getopt(liaison[l].nom, "r", param_link) == 2)
            {
                Rmax = atof(param_link);
            }
            if (prom_getopt(liaison[l].nom, "t", param_link) >= 1)
            {
                gpe_teaching = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "reset_eval") == 0)
            {
                gpe_reset_eval = liaison[l].depart;
            }
            i++;
            l = find_input_link(gpe, i);
        }

        if (gpe_teaching == -1)
        {
            printf("pas de groupe -t dans %s\n", __FUNCTION__);
            exit(0);
        }

        if (flag_file == 0 || flag_file_2 == 0)
        {
            printf
                ("probleme: pas de fichier trouve avec l'optio -f ou -e dans %s\n",
                 __FUNCTION__);
            exit(0);
        }


        file_desc = fopen(file_name, "r");
        if (file_desc == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name);
            exit(0);
        }
        cpt = 0;
        for (i = 0; i < nb_cases; i++)
        {
            for (j = 0; j < nb_cases; j++)
            {
                ret = fscanf(file_desc, "%s ", st);
                /*landmark */
                if (st[0] == 'T')
                {
                    pt[atoi(st + 1)][0] =
                        (int) mise_a_echelle_cases_vers_pix(j);
                    pt[atoi(st + 1)][1] =
                        (int) mise_a_echelle_cases_vers_pix(i);
                    cpt++;
                }
            }
            ret = fscanf(file_desc, "\n");
        }
        fclose(file_desc);
        nb_pt = cpt;
        printf("nb_pt = %d\n", nb_pt);

        file_desc_eval = fopen(file_name_eval, "r");
        if (file_desc_eval == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name_eval);
            exit(0);
        }
        cpt = 0;
        do
        {
            ret = fscanf(file_desc_eval, "%f %f", &(pt_eval[cpt][0]),
                   &(pt_eval[cpt][1]));
            cpt++;
        }
        while (!feof(file_desc_eval));
        fclose(file_desc_eval);
        nb_pt_eval = cpt;
        printf("nb_pt_eval = %d\n", nb_pt_eval);
        mydata =
            (MyData_f_trajectory_XY_teacher_3 *)
            malloc(sizeof(MyData_f_trajectory_XY_teacher_3));
        if (mydata == NULL)
        {
            printf("probleme malloc %s %d\n", __FUNCTION__, __LINE__);
            exit(0);
        }
        sprintf(mydata->file_name, "%s", file_name);
        mydata->file_desc = file_desc;
        sprintf(mydata->file_name_eval, "%s", file_name_eval);
        mydata->file_desc_eval = file_desc_eval;
        mydata->gpe_teaching = gpe_teaching;
        mydata->Rmax = Rmax;
        guidage = 0;
        mydata->guidage = guidage;
        mydata->nb_mesure = 0;
        mydata->sum_loin = 0.;
        mydata->gpe_reset_eval = gpe_reset_eval;

        for (i = 0; i < nb_pt; i++)
        {
            mydata->pt[i][0] = pt[i][0];
            mydata->pt[i][1] = pt[i][1];
        }
        for (i = 0; i < nb_pt_eval; i++)
        {
            mydata->pt_eval[i][0] = pt_eval[i][0];
            mydata->pt_eval[i][1] = pt_eval[i][1];
            mydata->dmin_pt[i] = 99999999;
        }

#ifndef AVEUGLE
        for (i = 0; i < nb_pt; i++)
        {
            point.x = pt[i][0] - 1;
            point.y = (int) pt[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, -3000, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
/*		TxEcrireChaine(&image1,noir,point,st,0);*/
#endif
        }
#endif
#ifndef AVEUGLE
        for (i = 0; i < nb_pt_eval; i++)
        {
            point.x = pt_eval[i][0] - 1;
            point.y = (int) pt_eval[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, 200, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
/*		TxEcrireChaine(&image1,noir,point,st,0);*/
#endif
        }
#endif
        mydata->nb_pt = nb_pt;
        mydata->nb_pt_eval = nb_pt_eval;
        def_groupe[gpe].data = (MyData_f_trajectory_XY_teacher_3 *) mydata;
    }
    else
    {
        mydata = (MyData_f_trajectory_XY_teacher_3 *) def_groupe[gpe].data;
        gpe_teaching = mydata->gpe_teaching;
        nb_pt = mydata->nb_pt;
        nb_pt_eval = mydata->nb_pt_eval;
        for (i = 0; i < nb_pt_eval; i++)
        {
            pt[i][0] = mydata->pt[i][0];
            pt[i][1] = mydata->pt[i][1];
        }
        for (i = 0; i < nb_pt_eval; i++)
        {
            mydata->pt_eval[i][0] = pt_eval[i][0];
            mydata->pt_eval[i][1] = pt_eval[i][1];
        }
        d_min_prec = mydata->d_min_prec;
        Rmax = mydata->Rmax;
        guidage = mydata->guidage;
        gpe_reset_eval = mydata->gpe_reset_eval;
    }
    printf("debut %s\n", __FUNCTION__);
    if (neurone[def_groupe[gpe_reset_eval].premier_ele].s > 0.99)
    {
        for (i = 0; i < nb_pt_eval; i++)
            mydata->dmin_pt[i] = 10000.;
        mydata->sum_loin = 0.;
        mydata->nb_mesure = 0;
    }
    /*Chercher le point le plus proche, calculer rayon, si > valeur, reorienter vers le prochain point */

    d_min = 9999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));

        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }
    (mydata->nb_mesure)++;
    mydata->sum_loin = mydata->sum_loin + d_min;

    mydata->sum_pres = 0.;
    for (i = 0; i < nb_pt_eval; i++)
    {
        dist =
            sqrt((pt_eval[i][0] - posx) * (pt_eval[i][0] - posx) +
                 (posy - pt_eval[i][1]) * (posy - pt_eval[i][1]));

        if (dist <= mydata->dmin_pt[i])
            mydata->dmin_pt[i] = dist;
        mydata->sum_pres = mydata->sum_pres + mydata->dmin_pt[i];
    }
    (mydata->nb_mesure)++;
    mydata->sum_loin = mydata->sum_loin + d_min;

#ifdef DEBUG
    printf("======== Sum pres =======\n");
    printf("========   %f  =======\n", mydata->sum_pres / (float) nb_pt_eval);
    printf("======== Sum loin =======\n");
    printf("========   %f  =======\n",
           mydata->sum_loin / (float) (mydata->nb_mesure));
#endif
    neurone[def_groupe[gpe].premier_ele + 2].s =
        neurone[def_groupe[gpe].premier_ele + 2].s1 =
        neurone[def_groupe[gpe].premier_ele + 2].s2 = mydata->sum_pres;
    neurone[def_groupe[gpe].premier_ele + 3].s =
        neurone[def_groupe[gpe].premier_ele + 3].s1 =
        neurone[def_groupe[gpe].premier_ele + 3].s2 = mydata->sum_loin;

    /*aucun guidage */
    if (neurone[def_groupe[gpe_teaching].premier_ele].s1 < 0.5)
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = 0.;
#ifdef DEBUG        
	printf("%s: pas de guidage\n", __FUNCTION__);
#endif        
return;
    }


    d_min = 999999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));
        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }


    if (fabs(pt[ind_min][0] - posx) > fabs(pt[ind_min][1] - posy))
        d_min = fabs((float) (pt[ind_min][0] - posx));
    else
        d_min = fabs((float) (pt[ind_min][1] - posy));
#ifdef DEBUG
    printf("dmin:%f\n", d_min);
    printf("nd_courant : %d,", ind_min);
#endif    
    if (ind_min >= nb_pt - 5)
        ind_min = ind_min - nb_pt;
#ifdef DEBUG
    printf("target:%d \n", ind_min + 5);
#endif     
    mydata->d_min_prec = d_min;
#ifndef ROUTEUR
    alpha =
        atan2f(pt[ind_min + 5][1] - posy, pt[ind_min + 5][0] - posx) + M_PI;
#else
    printf
        ("atan2f n'est pas disponible sur le routeur... le code de f_trajectory_XY_teacher ne fct probablement pas...\n");
#endif
/*Mode correct*/
    if (d_min >= Rmax || guidage > 0
        || neurone[def_groupe[gpe_teaching].premier_ele].s1 > 1.5)
    {
#ifdef DEBUG
        printf("%s: guidage correctif\n", __FUNCTION__);
#endif
	if (guidage == 0)
        {
            guidage = 1;
        }

        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 1.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;

        if (d_min <= Rmax / 4.)
        {
            guidage = 0;
        }
    }
    else
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
    }

    mydata->guidage = guidage;
}


typedef struct MyData_f_trajectory_XY_teacher_4
{
    int gpe_teaching;

    char file_name[255];
    FILE *file_desc;
    int pt[56000][2];
    int nb_pt;

    float d_min_prec;
    float Rmax;
    int guidage;
/*****************************/
    char file_name_eval[255];
    FILE *file_desc_eval;
    int pt_eval[56000][2];
    int nb_pt_eval;

    float dmin_pt[56000];
    float sum_pres;
    float sum_loin;
    int nb_mesure;
    int gpe_reset_eval;
    int gpe_interaction;
} MyData_f_trajectory_XY_teacher_4;

void function_trajectory_XY_teacher_4(int gpe)
{
    char param_link[255];
    char file_name[255], file_name_eval[255];
    int i, flag_file = 0, flag_file_2 = 0, l;
    MyData_f_trajectory_XY_teacher_4 *mydata = NULL;
    FILE *file_desc, *file_desc_eval;
    int gpe_teaching = -1;
    int pt[56000][2];
    float pt_eval[56000][2];
    int nb_pt, nb_pt_eval;
    int ind_min;
    float d_min;
    float Rmax = 20.;
    float alpha, d_min_prec = -1000.;
    float dist;
    int cpt, j;
    char st[10];
    int guidage;
    int gpe_reset_eval = -1;
    int gpe_interaction = -1;
    TxPoint point;   
    int ret;
 
    (void) d_min_prec; // (unused)
    (void) point; // (unused)
    (void) ret; // (unused)

    if (def_groupe[gpe].data == NULL)
    {
        printf("init %s\n", __FUNCTION__);
        if (def_groupe[gpe].nbre != 4)
        {
            printf("il faut 4 neuron pour %s\n", __FUNCTION__);
            exit(0);
        }
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (prom_getopt(liaison[l].nom, "f", param_link) == 2)
            {
                sprintf(file_name, "%s", param_link);
                flag_file = 1;
            }
            if (prom_getopt(liaison[l].nom, "e", param_link) == 2)
            {
                sprintf(file_name_eval, "%s", param_link);
                flag_file_2 = 1;
            }
            if (prom_getopt(liaison[l].nom, "r", param_link) == 2)
            {
                Rmax = atof(param_link);
            }
            if (prom_getopt(liaison[l].nom, "t", param_link) >= 1)
            {
                gpe_teaching = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "reset_eval") == 0)
            {
                gpe_reset_eval = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "interaction") == 0)
            {
                gpe_interaction = liaison[l].depart;
            }

            i++;
            l = find_input_link(gpe, i);
        }

        if (gpe_teaching == -1 || gpe_interaction == -1)
        {
            printf("pas de groupe -t ou interaction dans %s\n", __FUNCTION__);
            exit(0);
        }

        if (flag_file == 0 || flag_file_2 == 0)
        {
            printf
                ("probleme: pas de fichier trouve avec l'optio -f ou -e dans %s\n",
                 __FUNCTION__);
            exit(0);
        }


        file_desc = fopen(file_name, "r");
        if (file_desc == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name);
            exit(0);
        }
        cpt = 0;
        for (i = 0; i < nb_cases; i++)
        {
            for (j = 0; j < nb_cases; j++)
            {
               ret =  fscanf(file_desc, "%s ", st);
                /*landmark */
                if (st[0] == 'T')
                {
                    pt[atoi(st + 1)][0] =
                        (int) mise_a_echelle_cases_vers_pix(j);
                    pt[atoi(st + 1)][1] =
                        (int) mise_a_echelle_cases_vers_pix(i);
                    cpt++;
                }
            }
            ret = fscanf(file_desc, "\n");
        }
        fclose(file_desc);
        nb_pt = cpt;
        printf("nb_pt = %d\n", nb_pt);

        file_desc_eval = fopen(file_name_eval, "r");
        if (file_desc_eval == NULL)
        {
            printf("impossible d'ouvrir '%s'\n", file_name_eval);
            exit(0);
        }
        cpt = 0;
        do
        {
            ret = fscanf(file_desc_eval, "%f %f", &(pt_eval[cpt][0]),
                   &(pt_eval[cpt][1]));
            cpt++;
        }
        while (!feof(file_desc_eval));
        fclose(file_desc_eval);
        nb_pt_eval = cpt;
        printf("nb_pt_eval = %d\n", nb_pt_eval);
        mydata =
            (MyData_f_trajectory_XY_teacher_4 *)
            malloc(sizeof(MyData_f_trajectory_XY_teacher_4));
        if (mydata == NULL)
        {
            printf("probleme malloc %s %d\n", __FUNCTION__, __LINE__);
            exit(0);
        }
        sprintf(mydata->file_name, "%s", file_name);
        mydata->file_desc = file_desc;
        sprintf(mydata->file_name_eval, "%s", file_name_eval);
        mydata->file_desc_eval = file_desc_eval;
        mydata->gpe_teaching = gpe_teaching;
        mydata->Rmax = Rmax;
        guidage = 0;
        mydata->guidage = guidage;
        mydata->nb_mesure = 0;
        mydata->sum_loin = 0.;
        mydata->gpe_reset_eval = gpe_reset_eval;
        mydata->gpe_interaction = gpe_interaction;
        for (i = 0; i < nb_pt; i++)
        {
            mydata->pt[i][0] = pt[i][0];
            mydata->pt[i][1] = pt[i][1];
        }
        for (i = 0; i < nb_pt_eval; i++)
        {
            mydata->pt_eval[i][0] = pt_eval[i][0];
            mydata->pt_eval[i][1] = pt_eval[i][1];
            mydata->dmin_pt[i] = 99999999;
        }

#ifndef AVEUGLE
        for (i = 0; i < nb_pt; i++)
        {
            point.x = pt[i][0] - 1;
            point.y = (int) pt[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, -3000, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
/*		TxEcrireChaine(&image1,noir,point,st,0);*/
#endif
        }
#endif
#ifndef AVEUGLE
        for (i = 0; i < nb_pt_eval; i++)
        {
            point.x = pt_eval[i][0] - 1;
            point.y = (int) pt_eval[i][1] - 1;
            TxDessinerRectangle_rgb(&image1, 200, TxPlein, point, 2, 2, 1);
            sprintf(st, "%d", i);
#ifdef DEBUG
/*		TxEcrireChaine(&image1,noir,point,st,0);*/
#endif
        }
#endif
        mydata->nb_pt = nb_pt;
        mydata->nb_pt_eval = nb_pt_eval;
        def_groupe[gpe].data = (MyData_f_trajectory_XY_teacher_4 *) mydata;
    }
    else
    {
        mydata = (MyData_f_trajectory_XY_teacher_4 *) def_groupe[gpe].data;
        gpe_teaching = mydata->gpe_teaching;
        nb_pt = mydata->nb_pt;
        nb_pt_eval = mydata->nb_pt_eval;
        for (i = 0; i < nb_pt_eval; i++)
        {
            pt[i][0] = mydata->pt[i][0];
            pt[i][1] = mydata->pt[i][1];
        }
        for (i = 0; i < nb_pt_eval; i++)
        {
            mydata->pt_eval[i][0] = pt_eval[i][0];
            mydata->pt_eval[i][1] = pt_eval[i][1];
        }
        d_min_prec = mydata->d_min_prec;
        Rmax = mydata->Rmax;
        guidage = mydata->guidage;
        gpe_reset_eval = mydata->gpe_reset_eval;
        gpe_interaction = mydata->gpe_interaction;
    }
/*    printf("debut %s\n", __FUNCTION__);*/
    if (neurone[def_groupe[gpe_reset_eval].premier_ele].s > 0.99)
    {
        for (i = 0; i < nb_pt_eval; i++)
            mydata->dmin_pt[i] = 10000.;
        mydata->sum_loin = 0.;
        mydata->nb_mesure = 0;
    }
    /*Chercher le point le plus proche, calculer rayon, si > valeur, reorienter vers le prochain point */

    d_min = 9999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));

        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }
    (mydata->nb_mesure)++;
    mydata->sum_loin = mydata->sum_loin + d_min;

    mydata->sum_pres = 0.;
    for (i = 0; i < nb_pt_eval; i++)
    {
        dist =
            sqrt((pt_eval[i][0] - posx) * (pt_eval[i][0] - posx) +
                 (posy - pt_eval[i][1]) * (posy - pt_eval[i][1]));

        if (dist <= mydata->dmin_pt[i])
            mydata->dmin_pt[i] = dist;
        mydata->sum_pres = mydata->sum_pres + mydata->dmin_pt[i];
    }
    (mydata->nb_mesure)++;
    mydata->sum_loin = mydata->sum_loin + d_min;
#ifdef DEBUG
    printf("======== Sum pres =======\n");
    printf("========   %f  =======\n", mydata->sum_pres / (float) nb_pt_eval);
    printf("======== Sum loin =======\n");
    printf("========   %f  =======\n",
           mydata->sum_loin / (float) (mydata->nb_mesure));
#endif
    neurone[def_groupe[gpe].premier_ele + 2].s =
        neurone[def_groupe[gpe].premier_ele + 2].s1 =
        neurone[def_groupe[gpe].premier_ele + 2].s2 = mydata->sum_pres;
    neurone[def_groupe[gpe].premier_ele + 3].s =
        neurone[def_groupe[gpe].premier_ele + 3].s1 =
        neurone[def_groupe[gpe].premier_ele + 3].s2 = mydata->sum_loin;

    /*aucun guidage */
    if (neurone[def_groupe[gpe_teaching].premier_ele].s1 < 0.5)
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = 0.;
#ifdef DEBUG        
	printf("%s: pas de guidage\n", __FUNCTION__);
#endif        
	return;
    }

    d_min = 999999.;
    ind_min = -1;
    for (i = 0; i < nb_pt; i++)
    {
        dist =
            sqrt((pt[i][0] - posx) * (pt[i][0] - posx) +
                 (posy - pt[i][1]) * (posy - pt[i][1]));
        if (dist <= d_min)
        {
            d_min = dist;
            ind_min = i;
        }
    }


    if (fabs(pt[ind_min][0] - posx) > fabs(pt[ind_min][1] - posy))
        d_min = fabs((float) (pt[ind_min][0] - posx));
    else
        d_min = fabs((float) (pt[ind_min][1] - posy));
#ifdef DEBUG
    printf("dmin:%f\n", d_min);
    printf("nd_courant : %d,", ind_min);
#endif
    if (ind_min >= nb_pt - 5)
        ind_min = ind_min - nb_pt;
#ifdef DEBUG
    printf("target:%d \n", ind_min + 5);
#endif
    mydata->d_min_prec = d_min;
#ifndef ROUTEUR
    alpha =
        atan2f(pt[ind_min + 5][1] - posy, pt[ind_min + 5][0] - posx) + M_PI;
#else
    printf
        ("atan2f n'est pas disponible sur le routeur... le code de f_trajectory_XY_teacher ne fct probablement pas...\n");
#endif
/*Mode correct*/
    if (neurone[def_groupe[gpe_interaction].premier_ele].s1 > 0.5)
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 1.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;

    }
    else
    {
        neurone[def_groupe[gpe].premier_ele].s =
            neurone[def_groupe[gpe].premier_ele].s1 =
            neurone[def_groupe[gpe].premier_ele].s2 = 0.;
        neurone[def_groupe[gpe].premier_ele + 1].s =
            neurone[def_groupe[gpe].premier_ele + 1].s1 =
            neurone[def_groupe[gpe].premier_ele + 1].s2 = alpha;
    }

    mydata->guidage = guidage;
}
