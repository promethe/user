/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_reco_info.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N. Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: Renvoie les indices de neurones gagnant en entree. Utiliser dans des portion de script ou l'on isole un a un les gagnants d'une couche de neurones

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
extern int echelle_temps[];
typedef struct
{
    int count;
    int deb;
    int longueur;
    int x;
    int nlink;
    int input;
    int input_nb;


} info_data;

void function_reco_info(int numero)
{
    info_data *d;
    int i;

    #ifdef DEBUG
    printf("~~~~~~~~~~~entree dans %s (%d)\n", __FUNCTION__, numero);
#endif  

    /*get the string of the input link */
    if (def_groupe[numero].data == NULL)
    {
        d = malloc(sizeof(info_data));
        if (d == NULL)
        {
            printf("pb!\n");
            exit(-1);
        }
        d->nlink = find_input_link(numero, 0);
        d->deb = def_groupe[numero].premier_ele;
        d->input = liaison[d->nlink].depart;
        d->longueur = def_groupe[numero].nbre;
        d->input_nb = def_groupe[d->input].premier_ele;
        d->x = 0;
        d->count = 0;
        def_groupe[numero].data = d;
        printf("found input grp:%d\n", d->input);
    }
    else
        d = def_groupe[numero].data;


/*initialisation a chaque tour complet de l'echelle de temps*/
    if (d->count >= echelle_temps[def_groupe[numero].ech_temps]
        || d->count == 0)
    {
        printf("----------------------------reset-count:%d, ech_tps:%d\n",
               d->count, echelle_temps[def_groupe[numero].ech_temps]);
        d->x = 0;
        d->count = 0;
        for (i = 0; i < def_groupe[numero].nbre; i++)
            neurone[i + d->deb].s2 = neurone[i + d->deb].s1 = -1;
    }
    for (i = 0; i < def_groupe[d->input].nbre; i++)
    {
        /*memorise l'index du vainqueur */
        if (neurone[i + d->input_nb].s2 > 0)
        {
            neurone[d->x + d->deb].s2 = neurone[d->x + d->deb].s1 = i;
            d->x++;
            printf("found an input:%d,%d\n", i + d->input_nb, i);
            break;
        }
    }
    d->count++;
    printf("~~~~~~~~~~~sortie de %s (%d)\n", __FUNCTION__, numero);
}
