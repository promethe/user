/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_print_place.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: P. Gaussier
- description: specific file creation
- date: 01/2006

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number

Macro:
-TAILLEX

Local varirables:
-float posx
-float pos.y

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>

/*#define DEBUG*/
#define POSITION_PERCUE 1

#define max_nb_cases 200

extern int posx_percue, posy_percue;

typedef struct struct_print_one_place
{
    int groupe_entree;
    int nx, ny;                 /* taille en x et en y pour l'affichage */
    int x0, y0;                 /* position de l'affichage / offset dans la fenetre */
    int nbre_neurones_affiches;
    int *pos_x;
    int *pos_y;
    int *no_neurone;
    int print_in_file;
    int info_pos_robot;
    int image;                  /* no de l'image utilisee */
    int no_sortie;              /* 0 pour s, 1 por s1 et 2 pour s2 */
    float *sum;
    float *sum2;
    float *nbre;
    char *file_name_prefix;
} struct_print_one_place;


void fin_print_one_place(int gpe)
{
    int i, j, p, p2;
    int px, py, nx, ny;
    FILE *f_sum, *f_sum2, *f_nbre;
    struct_print_one_place *ptr;
    char file_name[255];

    printf("Fin print_one_place \n");


    ptr = (struct_print_one_place *) def_groupe[gpe].data;
    if (ptr == NULL)
    {
        printf
            ("fin_print_one_place: L'initialisation pour le groupe %d f_print_one_place absente !!!\n",
             gpe);
        printf
            ("Penser a appeler new_print_one_place comme methode new de f_print_one_place.\n");
        exit(1);
    }
    nx = ptr->nx;
    ny = ptr->ny;


    for (py = 0; py < ny; py++)
        for (px = 0; px < nx; px++)
        {
            p = (px + py * nx) * (nb_cases * nb_cases);
            sprintf(file_name, "%s_%d_%d_sum.m", ptr->file_name_prefix, px,
                    py);
            f_sum = fopen(file_name, "w");
            sprintf(file_name, "%s_%d_%d_sum2.m", ptr->file_name_prefix, px,
                    py);
            f_sum2 = fopen(file_name, "w");
            sprintf(file_name, "%s_%d_%d_nbre.m", ptr->file_name_prefix, px,
                    py);
            f_nbre = fopen(file_name, "w");
            /*     printf("p0= %d p=%d nx=%d ny=%d nb_cases=%d\n",p/(nb_cases*nb_cases),p,nx,ny,nb_cases); */
            for (j = 0; j < nb_cases; j++)
            {
                for (i = 0; i < nb_cases; i++)
                {
                    p2 = p + i + j * nb_cases;
                    fprintf(f_sum, "%f ", ptr->sum[p2]);
                    fprintf(f_sum2, "%f ", ptr->sum2[p2]);
                    fprintf(f_nbre, "%f ", ptr->nbre[p2]);
                }
                fprintf(f_sum, "\n");
                fprintf(f_sum2, "\n");
                fprintf(f_nbre, "\n");
            }
            fclose(f_sum);
            fclose(f_sum2);
            fclose(f_nbre);
        }

}

/* fonction appelee au demarrage */
void new_print_one_place(int gpe)
{
    int i, j, n, n0, nx, ny, x0, y0, no_sortie;
    int image;
    int nbre_neurones_affiches;
    int val;
    int groupe_entree, deb_entree, nbre_entree, no_neurone;
    int print_in_file = -1;
    struct_print_one_place *ptr;
    char retour[255];
    char file_name_prefix[255];
    int taille_memoire;

#define DEBUG
#ifdef DEBUG
    printf("new print_one_place du groupe %d \n", gpe);
#endif

    if (def_groupe[gpe].data != NULL)
    {
        printf("new_print_one_place deja initialise \n");
        return;
    }

    /*apres ce traitement la liason i doit etre celle qui lit ce groupe est celui qui contient les infos de cellules de lieux */
    /* pas propre a changer PG : na pas imposer d'ordre dans les liens !!!!!!!!!!!!!!!!!!!!!!  */
    i = find_input_link(gpe, 0);    /* recherche le 1er lien */
    if (i == -1)
    {
        printf("error no input group for group %d\n", gpe);
        return;
    }
    else if (strcmp(liaison[i].nom, "pos") >= 0)
    {
        i = find_input_link(gpe, 1);    /* recherche le 1er lien */
        if (i == -1)
        {
            printf("error no input group for group %d\n", gpe);
            return;
        }
    }
    groupe_entree = liaison[i].depart;

/*n pour number of the first neuron */
    val = prom_getopt(liaison[i].nom, "n", retour);
    if (val != 2)
    {
        printf
            ("il manque le numero du neurone a selectionner dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        n0 = 0;
    }
    else
        n0 = atoi(retour);
    printf("liaison %s %s, premier neurone selectionne %d \n", liaison[i].nom,
           retour, val);

  /*-f pour le prefixe des fichiers dans lesquels seront mis les resultats prefix_sum.m prefix_sum2.m prefix_nbre.m */
    val = prom_getopt(liaison[i].nom, "f", retour);
    if (val != 2)
    {
        printf
            ("il manque le prefixe pour les noms de fichiers a utiliser: on prend -fdefault\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        strcpy(file_name_prefix, "default");
    }
    else
        strcpy(file_name_prefix, retour);

    val = prom_getopt(liaison[i].nom, "I", retour);
    if (val != 2)
    {
        printf
            ("il manque le numero de l'image a selectionner dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        image = 1;
    }
    else
        image = atoi(retour);

    val = prom_getopt(liaison[i].nom, "x", retour);
    if (val != 2)
    {
        printf("il manque la taille x dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        nx = 1;
    }
    else
        nx = atoi(retour);

    val = prom_getopt(liaison[i].nom, "y", retour);
    if (val != 2)
    {
        printf("il manque la taille y dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        ny = 1;
    }
    else
        ny = atoi(retour);

    val = prom_getopt(liaison[i].nom, "X", retour);
    if (val != 2)
    {
        printf("il manque le decalage X (x0 affichage) dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        x0 = 0;
    }
    else
        x0 = atoi(retour);

    val = prom_getopt(liaison[i].nom, "Y", retour);
    if (val != 2)
    {
        printf("il manque le decalage Y (y0 affichage) dans la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        y0 = 0;
    }
    else
        y0 = atoi(retour);
/*s pour kind of output s, s1 or s2 */
    val = prom_getopt(liaison[i].nom, "s", retour);
    if (val != 2)
    {
        printf
            ("il manque le numero du type de sortie selectionnee pour la liaison\n");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        no_sortie = 1;
    }
    else
        no_sortie = atoi(retour);
    printf("liaison %s %s, type sortie neurone selectionne %d \n",
           liaison[i].nom, retour, val);


    print_in_file = prom_getopt(liaison[i].nom, "f", retour);   /* valeur numerique sinon couleur par defaut */

    deb_entree = def_groupe[groupe_entree].premier_ele;
    nbre_entree = def_groupe[groupe_entree].nbre;

    nbre_neurones_affiches = nx * ny;
    ptr = (struct_print_one_place *) malloc(sizeof(struct_print_one_place));
    /*recherche le groupe donnant la position */
    ptr->info_pos_robot = trouver_entree(gpe, (char*)"pos");
    ptr->no_sortie = no_sortie;
    ptr->nx = nx;
    ptr->ny = ny;
    ptr->x0 = x0;
    ptr->y0 = y0;
    ptr->image = image;
    ptr->pos_x = (int *) malloc(nbre_neurones_affiches * sizeof(int));
    if (ptr->pos_x == NULL)
    {
        printf("pb allocation dans new_print_one_place pos_x \n");
        exit(1);
    }
    ptr->pos_y = (int *) malloc(nbre_neurones_affiches * sizeof(int));
    if (ptr->pos_y == NULL)
    {
        printf("pb allocation dans new_print_one_place pos_y \n");
        exit(1);
    }
    ptr->no_neurone = (int *) malloc(nbre_neurones_affiches * sizeof(int));
    if (ptr->no_neurone == NULL)
    {
        printf("pb allocation dans new_print_one_place no_neurone \n");
        exit(1);
    }
    ptr->nbre_neurones_affiches = nbre_neurones_affiches;
    ptr->groupe_entree = groupe_entree;
    ptr->print_in_file = print_in_file;
    ptr->file_name_prefix = (char *) malloc(255 * sizeof(char));
    if (ptr->file_name_prefix == NULL)
    {
        printf("pb allocation dans new_print_one_place file_name_prefix \n");
        exit(1);
    }
    strcpy(ptr->file_name_prefix, file_name_prefix);

    /*  printf("max_nb_cases = %d nx = %d ny = %d \n",max_nb_cases,nx,ny); */
    taille_memoire = max_nb_cases * max_nb_cases * nx * ny;
    /* printf("new_print_one_place taille_memoire allocation = %d \n",taille_memoire); */
    ptr->sum = (float *) malloc(taille_memoire * sizeof(float));
    if (ptr->sum == NULL)
    {
        printf("pb allocation dans new_print_one_place  sum \n");
        exit(1);
    }
    ptr->sum2 = (float *) malloc(taille_memoire * sizeof(float));
    if (ptr->sum2 == NULL)
    {
        printf("pb allocation dans new_print_one_place  sum \n");
        exit(1);
    }
    ptr->nbre = (float *) malloc(taille_memoire * sizeof(float));
    if (ptr->nbre == NULL)
    {
        printf("pb allocation dans new_print_one_place  sum \n");
        exit(1);
    }

    def_groupe[gpe].data = (void *) ptr;    /* sauvegarde des donnees dans le champs data du groupe */

    n = 0;                      /* compteur de neurones selectionnes */
    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++)
        {
            no_neurone = n0 + i + j * nx;
            if (no_neurone < 0 || no_neurone > nbre_entree)
            {
                printf("ERROR: function_print_one_place(%d)", gpe);
                printf
                    ("selected neurone %d > max number neurons in the group (or val<0) ...\n",
                     no_neurone);
                exit(1);
            }
            ptr->pos_x[n] = i;
            ptr->pos_y[n] = j;
            ptr->no_neurone[n] = no_neurone + deb_entree;
            n++;
        }

    /*  printf("max_nb_cases = %d ,  Taillex %d tailley %d \n",nb_cases,TAILLEX,TAILLEY); */
    for (i = 0; i < taille_memoire; i++)
    {
        ptr->sum[i] = ptr->sum2[i] = ptr->nbre[i] = 0.;
    }
}

void function_print_one_place(int gpe)
{
    TxPoint point, point_percu, pos, pos_percue;
    float max = -1;
    int n, no_max = 0;
    int no_sortie;
#ifdef PRINT_FILE
    FILE *f;
    char retour[255];
    int print_in_file = -1
#endif
    int i, j, p, p0, nx, ny, x0, y0, reduction;
    int local_pas, longueur;
    float fpas;
    struct_print_one_place *ptr;
    int taille_memoire;
#ifndef AVEUGLE
    TxDonneesFenetre *image;
    int image_refresh = 0;
#define periode_image_refresh 100
#endif

    (void) local_pas; // (unused)
    (void) point_percu; // (unused)
    (void) point; // (unused)

    ptr = (struct_print_one_place *) def_groupe[gpe].data;
    if (ptr == NULL)
    {
        printf
            ("L'initialisation pour le groupe %d f_print_one_place absente !!!\n",
             gpe);
        printf
            ("Penser a appeler new_print_one_place comme methode new de f_print_one_place.\n");
        exit(1);
    }

    x0 = ptr->x0;
    y0 = ptr->y0;
    nx = ptr->nx;
    ny = ptr->ny;
    no_sortie = ptr->no_sortie;
    if (no_sortie < 0 || no_sortie > 2)
    {
        printf
            ("Error the output type %d does not belong to [0,2] s, s1 or s2 \n",
             no_sortie);
        printf("print_one_place group = %d \n", gpe);
        exit(1);
    }

    if (nx > ny)
        reduction = nx;
    else
        reduction = ny;
    if (nb_cases > max_nb_cases)
    {
        printf
            ("erreur dans f_print_one_place nb_cases>max_nb_cases (%d>%d)\n",
             nb_cases, max_nb_cases);
        printf
            ("Il faut augmenter le define max_nb_cases au debut de f_print_one_place.c \n");
        exit(1);
    }
    local_pas = pas / reduction;
    longueur = (TAILLEX) / reduction + 1;

    if (ptr->info_pos_robot != -1)
    {
        pos.x = neurone[def_groupe[ptr->info_pos_robot].premier_ele].s2;
        pos.y = neurone[def_groupe[ptr->info_pos_robot].premier_ele + 1].s2;
    }
    else
    {
        /*sinon on est en simulation */
        /*initialise la structure point avec les coord courantes de l'agent */
        pos.x = posx;
        pos.y = posy;
    }
    pos_percue.x = posx_percue;
    pos_percue.y = posy_percue;
#ifndef AVEUGLE
    if (ptr->image == 2)
        image = &image2;
    else if (ptr->image == -1)
        image = NULL;
    else
        image = &image1;        /*par defaut pour rester compatible avec autres scripts */
#endif

    taille_memoire = nb_cases * nb_cases;
    /* printf("TAILLEX %d nb_cases %d pas %f \n",TAILLEX,nb_cases,pas); */
    fpas = ((float) nb_cases) / ((float) TAILLEX);
    nbre_entree = def_groupe[ptr->groupe_entree].nbre;
    /* printf("nbre_neurones_affiches = %d \n",ptr->nbre_neurones_affiches); */
    for (n = 0; n < ptr->nbre_neurones_affiches; n++)
    {
        /*printf("traite affichage neurone %d \n",n); */
        i = ptr->pos_x[n];
        j = ptr->pos_y[n];
        no_max = ptr->no_neurone[n];
        if(no_sortie==0) max = neurone[no_max].s;
        else if(no_sortie==1) max = neurone[no_max].s1;
        else if(no_sortie==2) max = neurone[no_max].s2;

        /*f(max>0.5 && max<1.) printf("neurone %d val=%f \n",no_max,max); */

        point.x = x0 + longueur * i + ((int) pos.x - pas / 2) / nx;
        point.y = y0 + longueur * j + ((int) pos.y - pas / 2) / ny;

        point_percu.x = x0 + longueur * i + ((int) pos_percue.x - pas / 2) / nx;
        point_percu.y = y0 + longueur * j + ((int) pos_percue.y - pas / 2) / ny;

        /*fpas=((float)nb_cases)/((float)TAILLEX); */
        p0 = ((int) (pos.x * fpas)) + ((int) (pos.y * fpas)) * nb_cases;
        if (((int) (pos.x * fpas)) >= nb_cases)
        {
            printf("erreur pos.x \n"), exit(1);
        }
        if (((int) (pos.y * fpas)) >= nb_cases)
        {
            printf("erreur pos.y \n"), exit(1);
        }
        /*printf("p0=%d, pos.x = %d , pos.y = %d , fpas =%f, nb_cases= %d \n",p0,pos.x,pos.y,fpas,nb_cases); */
        if (p0 < 0 || p0 >= taille_memoire)
        {
            printf("Erreur dans f_print_N_places p0=%d hors bornes [0,%d]\n",
                   p0, taille_memoire - 1);
            printf("pos.x = %d , pos.y = %d , fpas =%f, nb_cases= %d \n",
                   pos.x, pos.y, fpas, nb_cases);
            exit(1);
        }
        p = n * taille_memoire + p0;

        /* PROVISOIRE BUG */
        /*printf("p=%d p0=%d n=%d taille_memoire=%d \n",p,p0,n,taille_memoire); */
        ptr->sum[p] = ptr->sum[p] + max;
        ptr->sum2[p] = ptr->sum2[p] + max * max;
        ptr->nbre[p] = ptr->nbre[p] + 1.;


        /*enregistrement de la position de l'animat dans un fichier */
#ifdef PRINT_FILE
        strcpy(retour, "robot_pos");
        f = fopen(retour, "a");
        fprintf(f, "%d, %d, %d,%d,%d,%d,%f\n", (int) posx, (int) posy,
                no_max * 4000 + 5, point.x, point.y, no_max, max);
        fclose(f);
#endif

#ifndef AVEUGLE
        /*if(max>vigilence) max=max-vigilence; else max=0.; */
        /*printf("dessine %d %d %d\n",point.x,point.y,(int)(max*1000000.)); */
        if (image != NULL)
            TxDessinerRectangle_rgb(image, (int) (max * 1000000.), TxPlein,
                                    point, local_pas, local_pas, 1);
/* utile pour montrer la position predite par le neural field de l'integration de chemin calculee dans f_stat_on_position_error.c */
#ifdef POSITION_PERCUE
          if (image != NULL)
            TxDessinerRectangle(image, rouge , TxPlein,
                                    point_percu, local_pas, local_pas, 1);
#endif
        /*    point.x=p0=((int)(pos.x*fpas));point.y=((int)(pos.y*fpas));
           TxDessinerRectangle_rgb(image, (int)(max*1000000.) ,TxPlein,point,1,1,1); */
        image_refresh = image_refresh + 1;
        if (image != NULL && image_refresh == periode_image_refresh)
            TxFlush(image);
        if (image_refresh >= periode_image_refresh)
            image_refresh = 0;
#endif
    }
}
