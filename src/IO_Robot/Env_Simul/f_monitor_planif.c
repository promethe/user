/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 \defgroup f_monitor_planif f_monitor_planif
 \ingroup libIO_Robot

\brief for the debugging of cognitive map architectures

\details

    \section Description 

    Print at the current robot location in the environment map,
    the number of the winner neuron associated to
    the input group => place number

    \section Links

    - EC ou -ECi : cellule de lieu courante en entree pour la carte cognitive i

    - DG ou -DGi

    - pos : position absolue du robot dans l'espace

    - vigilence : vigilance

    - transition_lieu

    - CA3 ou -CA3i: les transitions de lieux pour la carte cognitive i

    - reward : recompence

    - trans_biased ou -trans_biasedi pour la carte cognitive i

    - but ou -gi

    - -wnom_du_monde : pour la simulation

    - mvt_predit

    - DG : cellule de lieu precedente

    - -recrutbuti : signal de recrutement pour la carte cognitive i

    - -vali : affiche les valeurs des neurones dans la carte cognitive

    - -b

    - author: J.Hirel
    - description: * Ajout d'une option -val pour afficher les valeurs des neurones dans la carte cognitive.
				   * Affichage des mouvements associes a chaque transitions a l'aide d'une fleche.

    - author: J.Bonhoure
    - description: * prend en compte plusieurs buts (-gi avec i entier). 
                   * retrocompatible, fonctionne toujours avec le lien "but".
                   * ajout du lien -recrutbuti avec i entier (pas obligatoire).
    - date: 23/05/2011

    \file
    \ingroup f_monitor_planif
    \brief 

    Author: xxxxxxxx
    Created: XX/XX/XXXX
    Modified:
    - author: C.Giovannangeli
    - description: specific file creation
    - date: 11/08/2004
    
    - author: J.Hirel
    - description: * Ajout d'une option -val pour afficher les valeurs des neurones dans la carte cognitive.
				   * Affichage des mouvements associes a chaque transitions a l'aide d'une fleche.

    - author: J.Bonhoure
    - description: * prend en compte plusieurs buts (-gi avec i entier). 
                   * retrocompatible, fonctionne toujours avec le lien "but".
                   * ajout du lien -recrutbuti avec i entier (pas obligatoire).
    - date: 23/05/2011

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
    Print at the current robot location in the environment map,
    the number of the winner neuron associated to
    the input group => place number

    Liens en entree :

    * EC ou -ECi : cellule de lieu courante en entree pour la carte cognitive i

    * DG ou -DGi

    * pos : position absolue du robot dans l'espace

    * vigilence : vigilance

    * transition_lieu

    * CA3 ou -CA3i: les transitions de lieux pour la carte cognitive i

    * reward : recompence

    * trans_biased ou -trans_biasedi pour la carte cognitive i

    * but ou -gi

    * -wnom_du_monde : pour la simulation

    * mvt_predit

    * DG : cellule de lieu precedente

    * -recrutbuti : signal de recrutement pour la carte cognitive i

    * -vali : affiche les valeurs des neurones dans la carte cognitive

    * -b

    Macro:
    -TAILLEX

    Local varirables:
    -float posx
    -float posy

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/

/* #define DEBUG */
 
#include <libx.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include <libhardware.h>
#include "tools/include/affiche_ptessentiel.h"
#include "tools/include/display_world.h"
#include "tools/include/save_debug.h"
#include "tools/include/macro.h"
#include "tools/include/local_struct.h"
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

#ifndef AVEUGLE
void create_image3(TxDonneesFenetre * fenetre);
gboolean image3_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data);

gboolean image3_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data);

void create_image4(TxDonneesFenetre * fenetre, int k);
gboolean image4_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data);

gboolean image4_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data);

gboolean fenetre_expose_event(TxDonneesFenetre * fenetre, GtkWidget * widget,
                              GdkEventExpose * event, gpointer data);

gboolean fenetre_configure_event(TxDonneesFenetre * fenetre,
                                 GtkWidget * widget,
                                 GdkEventConfigure * event, gpointer data);
gboolean cb_destroy_fenetre(GtkWidget * widget, gpointer data);

TxDonneesFenetre image3;
TxDonneesFenetre *image4;

static void draw_transition(es_trans *trans, es_lieu *lieux, TxDonneesFenetre *image, int color, int width)
{
   TxPoint point1, point2;

   if(trans->autotrans == 0)
   {
      point1.x = lieux[trans->depart].x;
      point1.y = lieux[trans->depart].y;
      point2.x = lieux[trans->arrive].x;
      point2.y = lieux[trans->arrive].y;
      TxDessinerSegmentPointille_rgb16M(image, color, point1, point2, width);
      
      point2.x = (point2.x + point1.x)/2;
      point2.y = (point2.y + point1.y)/2;
      TxDessinerSegment_rgb16M(image, color, point1, point2, width + 1);
   }
   else
   {
      point1.x = 5 + lieux[trans->depart].x;
      point1.y = 5 + lieux[trans->depart].y;
      TxDessinerCercle_rgb16M(image, color, TxVide, point1, 5, width + 1);
   }
}
#endif

static int neurone_max(int gpe)
{
   float max = -1;
   int i;
   int DebutGpe, NbreTotalGpe, IncrementGpe;
   int ind_max = -1;
   DebutGpe = def_groupe[gpe].premier_ele;
   NbreTotalGpe = def_groupe[gpe].nbre;
   IncrementGpe = NbreTotalGpe / (def_groupe[gpe].taillex * def_groupe[gpe].tailley);

   for (i = DebutGpe + IncrementGpe - 1; i < DebutGpe + NbreTotalGpe; i += IncrementGpe)
   {
      if (neurone[i].s1 > max)
      {
	 max = neurone[i].s1;
	 ind_max = i;
      }
   }

   return ind_max;
}


void new_monitor_planif(int gpe)
{ 
   int gpe_EC = -1;
   int gpe_CA3 = -1;
   int gpe_DG = -1;
   int gpe_vigilence=-1;
   int gpe_transition_lieu = -1;
   int *gpe_but;
   int *debut_but;
   int *inc_but;
   int *gpe_recrut;
   int gpe_mode = -1;
   int gpe_mvt_predit = -1;
   int gpe_trans_biased = -1;
   int gpe_reward = -1;
   int gpe_pos = -1;
   int print_vals = 0;
   int trans_name = 0;
   int size_but = 0;
   int i;
   int l,k;
   int ret, newfmp;
   MyData_f_monitor_planif *my_data = NULL;
   int *mode_but;
   char string[255];
   char world_name[255] = "\0";
   char param[2];
   FILE * fp;
   int index, neuron, place_x, place_y, from, to, autotrans, recrutement;
   World *world = NULL;
   int but=0;
   
   dprints("new_monitor_planif(%s): Entering function\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data == NULL)
   {
      l = 0;
      i = find_input_link(gpe, l);    /* recherche le 1er lien */
      while (i != -1)
      {
		if (strcmp(liaison[i].nom, "EC") == 0)
		{
			gpe_EC = liaison[i].depart;
		}

		if (strcmp(liaison[i].nom, "CA3") == 0)
		{
			gpe_CA3 = liaison[i].depart;
		}

		if (strcmp(liaison[i].nom, "DG") == 0)
		{
			gpe_DG = liaison[i].depart;
		}
	 
		if (prom_getopt(liaison[i].nom, "g", param) == 2)
		{
			size_but++;
		}
	 
		if (strcmp(liaison[i].nom, "reward") == 0)
		{
			gpe_reward = liaison[i].depart;
		}

		if (strcmp(liaison[i].nom, "transition_lieu") == 0)
		{
			gpe_transition_lieu = liaison[i].depart;
		}

		if (strcmp(liaison[i].nom, "pos") == 0 || strcmp(liaison[i].nom, "position") == 0)
		{
			gpe_pos = liaison[i].depart;
		}
		
		if (prom_getopt(liaison[i].nom, "w", string) == 2)
		{
			sprintf(world_name, "%s", string);
		}

		if (strcmp(liaison[i].nom, "mvt_predit") == 0)
		{
			gpe_mvt_predit = liaison[i].depart;
		}

		if (strcmp(liaison[i].nom, "trans_biased") == 0)
		{
			gpe_trans_biased = liaison[i].depart;
		}
	 
		if (strcmp(liaison[i].nom, "but") == 0)
		{
			size_but ++;
		}

		l++;
		i = find_input_link(gpe, l);    /* recherche le 1er lien */
      }

		gpe_but = MANY_ALLOCATIONS(size_but,int);
		gpe_recrut = MANY_ALLOCATIONS(size_but,int);
		inc_but = MANY_ALLOCATIONS(size_but,int);
		debut_but = MANY_ALLOCATIONS(size_but,int);
		mode_but = MANY_ALLOCATIONS(size_but,int);

		for(i=0; i<size_but; i++){
			gpe_but[i]=-1;
			gpe_recrut[i] = mode_but[i] = -1;
			debut_but[i] = inc_but[i] = -1;
		}
 
		l = 0;
		i = find_input_link(gpe, l);
      
    while (i != -1)
	{
      	 if (prom_getopt(liaison[i].nom, "g", param) == 2) 
		{
			but = atoi(param);
			if (but >= size_but)
			{
				EXIT_ON_ERROR("Index of but (%i) out of range (max = %i)", but, size_but);
			}
			
			if (gpe_but[but] != -1){
				EXIT_ON_ERROR("ERROR: in f_monitor_planif: goal(%s) already defined\n",param);
			}
			
			gpe_but[but] = liaison[i].depart;
		}
		
		if (strcmp(liaison[i].nom, "but") == 0)
		{	    
			if (gpe_but[0] != -1){
				EXIT_ON_ERROR("ERROR: in f_monitor_planif: goal(%s) already defined\n",param);
			}
			gpe_but[0] = liaison[i].depart;
		}
		
		if (prom_getopt(liaison[i].nom, "recrutbut",param) == 2)
		{
			but = atoi(param);
			if(gpe_recrut[but]==-1){
				gpe_recrut[but] = liaison[i].depart;
			}
			else{
				EXIT_ON_ERROR("ERROR: in f_monitor_planif: recrutbut(%s) already defined\n",param);
			}
		}

		if (strcmp(liaison[i].nom, "vigilence") == 0)
		{
			gpe_vigilence = liaison[i].depart;
		}
		
		if (prom_getopt(liaison[i].nom, "val", param) == 1)
        {
			print_vals = 1;
        }
        
        if (prom_getopt(liaison[i].nom, "trans_name", param) == 1)
        {
			trans_name = 1;
        }
        
        if (prom_getopt(liaison[i].nom, "b",string) == 2)
		{
	    	for(k=0;k<size_but;k++)	mode_but[k] = atoi(string);
		 }
		else if (prom_getopt(liaison[i].nom, "b",string) == 1)
		{
			gpe_mode = liaison[i].depart;
		}

        if (prom_getopt(liaison[i].nom, "mb",string) == 2)
		{			
			but = atoi(string);
			
			if (but >= size_but)
			{
				EXIT_ON_ERROR("Index of but (%i) out of range (max = %i)", but, size_but);
			}
			
			if (mode_but[but] != -1){
				EXIT_ON_ERROR("ERROR: in f_monitor_planif: goal(%s) already defined\n",param);
			}
			mode_but[but] = 1;
		}
        
		l++;
		i = find_input_link(gpe, l);
	}


	if (gpe_EC == -1 || gpe_CA3 == -1 || gpe_DG == -1 ||  gpe_transition_lieu == -1)
	{
		EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Missing input links\n", def_groupe[gpe].no_name);
    }

	if(gpe_vigilence == -1) EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Missing vigilence input links\n", def_groupe[gpe].no_name);
	
	if(gpe_but[0] == -1)
	{
		EXIT_ON_ERROR("ERROR in new_monitor_planif: First Goal undefined\n");
	} 
     
	if (def_groupe[gpe_CA3].nbre > 10000 || def_groupe[gpe_EC].nbre > 1000)
	{
		EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Too many neurons in CA3 or EC\n", def_groupe[gpe].no_name);
	}

	if (gpe_pos >= 0 && def_groupe[gpe_pos].nbre != 3)
	{
		EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): input group 'pos' must have 3 neurons\n", def_groupe[gpe].no_name);
	}

	my_data = ALLOCATION(MyData_f_monitor_planif);
	memset(my_data,0,sizeof(MyData_f_monitor_planif));


	for (i = 0; i < 1000; i++)
	{
		my_data->lieu[i].neurone = -1;
		my_data->lieu[i].recrutement=NULL; /* init recrutement */
	}

	for (i = 0; i < 10000; i++)
	{
		my_data->trans[i].neurone = -1;
		my_data->trans[i].recrutement=NULL;  /* init recrutement */
	}
	if (strlen(world_name) > 0)
	{
		world = world_get_world_by_name(world_name);
	
		dprints("new_monitor_planif(%s): Getting world '%s'\n", def_groupe[gpe].no_name, world_name);
		if (world == NULL)
		{	
			EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Could not find world '%s'\n", def_groupe[gpe].no_name, world_name);
		}
	}
	
	my_data->world = world;
	my_data->gpe_transition_lieu = gpe_transition_lieu;
	my_data->gpe_EC = gpe_EC;
	my_data->gpe_DG = gpe_DG;
	my_data->gpe_CA3 = gpe_CA3;
	my_data->gpe_but = gpe_but;
	my_data->inc_but = inc_but;
	my_data->debut_but = debut_but;
	my_data->gpe_vigilence = gpe_vigilence;
	my_data->gpe_recrut = gpe_recrut;
	my_data->nbr_de_lieu = 0;
	my_data->mode_but = mode_but;
	my_data->gpe_mode = gpe_mode;
	my_data->gpe_reward = gpe_reward;
	my_data->gpe_pos = gpe_pos;
	my_data->size_but = size_but;
	my_data->gpe_mvt_predit = gpe_mvt_predit;
	my_data->gpe_trans_biased = gpe_trans_biased;
	my_data->print_vals = print_vals;
	my_data->trans_name = trans_name;

	
	for(k=0;k<size_but;k++)	
	{
		if (gpe_but[k] >= 0)
		{
			debut_but[k] = def_groupe[gpe_but[k]].premier_ele;
			inc_but[k] = def_groupe[gpe_but[k]].nbre / (def_groupe[gpe_but[k]].taillex * def_groupe[gpe_but[k]].tailley);
		}
	}

	if (continue_simulation_status == CONTINUE)
	{		
		dprints("new_monitor_planif(%s): Loading previously saved debugging information\n", def_groupe[gpe].no_name);
		fp = fopen("transition.def","r");
		if(fp != NULL)
		{
			ret = fscanf(fp,"%s\n",string);
			if (ret != 1)
			{
				EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (leading string => ret = %i)\n", def_groupe[gpe].no_name, ret);
			}	    

			if (strcmp(string,"configuration")==0)
			{
				ret = fscanf(fp,"nbl = %d\nnbt = %d\n", &(my_data->nbr_de_lieu), &(my_data->nbr_de_trans));
				if (ret != 2)
				{
					EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (nbs => ret = %i)\n", def_groupe[gpe].no_name, ret);
				}	    
				ret = fscanf(fp,"nbcm = %d\n", &(my_data->size_but));
				if(ret != 1) newfmp = 0;
				else newfmp = 1;
				for(i = 0; i < my_data->nbr_de_lieu; i++)
				{
					if(newfmp == 1) ret = fscanf(fp,"%i %i %i %i",&index, &neuron, &place_x, &place_y);
					else ret = fscanf(fp,"%i %i %i %i\n",&index, &neuron, &place_x, &place_y);
					if (ret != 4)
					{
						EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (place => ret = %i)\n", def_groupe[gpe].no_name, ret);
					}	    

					my_data->coded_lieu[i] = index;
					my_data->lieu[index].neurone = neuron;
					my_data->lieu[index].x = place_x;
					my_data->lieu[index].y = place_y;
					if(my_data->lieu[index].recrutement==NULL) my_data->lieu[index].recrutement = MANY_ALLOCATIONS(my_data->size_but,int);
					if(newfmp == 1){
						for(k = 0; k < my_data->size_but; k++)
						{
							if(k != my_data->size_but)
							{
								ret = fscanf(fp,"%i",&recrutement);
								if (ret != 1)
								{
									EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (recrutement_place => ret = %i)\n", def_groupe[gpe].no_name, ret);
								}	
							}
							else
							{
								ret = fscanf(fp,"%i\n",&recrutement);
								if (ret != 1)
								{
									EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (recrutement_place => ret = %i)\n", def_groupe[gpe].no_name, ret);
								}
							}
							my_data->lieu[index].recrutement[k] = recrutement;
						}
					}
					else
					   for(k = 0; k < my_data->size_but; k++) {
					      my_data->lieu[index].recrutement[k] = 1; /** init recrutement even if not used*/
					   }
					   
				}
				
				for(i = 0; i < my_data->nbr_de_trans; i++)
				{					
					if(newfmp == 1) ret = fscanf(fp,"%i %i %i %i %i",&index, &neuron, &from, &to, &autotrans);
					else ret = fscanf(fp,"%i %i %i %i %i\n",&index, &neuron, &from, &to, &autotrans);
					if (ret != 5)
					{
						EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (trans => ret = %i)\n", def_groupe[gpe].no_name, ret);
					}
						    
		  			my_data->coded_trans[i] = index;
					my_data->trans[index].neurone = neuron;
					my_data->trans[index].depart = from;
					my_data->trans[index].arrive = to;
					my_data->trans[index].autotrans = autotrans;
					
					if(my_data->trans[index].recrutement==NULL) my_data->trans[index].recrutement = MANY_ALLOCATIONS(my_data->size_but,int);
					if(newfmp == 1){
						for(k = 0; k < my_data->size_but; k++)
						{
							if(k != my_data->size_but)
							{
								ret = fscanf(fp,"%i",&recrutement);
								if (ret != 1)
								{
									EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (recrutement_trans => ret = %i)\n", def_groupe[gpe].no_name, ret);
								}	
							}
							else
							{
								ret = fscanf(fp,"%i\n",&recrutement);
								if (ret != 1)
								{
									EXIT_ON_ERROR("ERROR in new_monitor_planif(%s): Invalid format for file transition.def (recrutement_trans => ret = %i)\n", def_groupe[gpe].no_name, ret);
								}
							}
							my_data->trans[index].recrutement[k] = recrutement;
						}
					}
					else {
					   for(k = 0; k < my_data->size_but; k++) {
					      my_data->trans[index].recrutement[k] = 1;
					   }
					}
				}
				fclose(fp);
			}
		}
		else
		{
			dprints("new_monitor_planif(%s): No transition.def => Starting debug from scratch\n", def_groupe[gpe].no_name);			
		}	
	}
	def_groupe[gpe].data = my_data;
	
#ifndef AVEUGLE
	image4 = MANY_ALLOCATIONS(size_but,TxDonneesFenetre);
	memset(image4,0,size_but*sizeof(TxDonneesFenetre));
	if(strcmp(image3.name,"debug CA3") != 0)
	{
	   sprintf(image3.name, "debug CA3");
	   create_image3(&image3);
	   gtk_widget_show_all(image3.window);
	}
	
	for(i=0;i<size_but;i++){
	   sprintf(image4[i].name, "debug but %d",i);
	   create_image4(&(image4[i]),i);
	   gtk_widget_show_all(image4[i].window);
	}
#endif	
   }
   dprints("new_monitor_planif(%s): Leaving function\n", def_groupe[gpe].no_name);   
}

void new_monitor_planif_real(int gpe)
{
   MyData_f_monitor_planif *my_data = NULL;

   new_monitor_planif(gpe);
   my_data = (MyData_f_monitor_planif *) def_groupe[gpe].data;
   if (my_data == NULL)
   {
      EXIT_ON_ERROR("ERROR in new_monitor_planif_real(%s): Cannot load data\n", def_groupe[gpe].no_name);
   }
   
   if (my_data->gpe_reward == -1)
   {
      /* No default printing of the sources for f_monitor_planif_real */
      my_data->gpe_reward = -2;
   }

}
 
void function_monitor_planif(int gpe)
{

   int gpe_EC = -1;
   int gpe_CA3 = -1;
   int gpe_DG = -1;
   int gpe_vigilence;
   int size_but;
   int *gpe_but;
   int *gpe_recrut;
   int gpe_mode=-1;
   int gpe_pos = -1;
   int gpe_reward = -1;
   int gpe_transition_lieu = -1;
   int gpe_mvt_predit=-1;
   int gpe_trans_biased = -1;
   int inc_EC, inc_CA3;
   int *inc_but;
   MyData_f_monitor_planif *my_data = NULL;
   int trans_max = -1, trans_max_ind = -1, lieu_max = -1, lieu_max_ind = -1, DG_max_ind = -1;
   int debut_EC = -1, debut_CA3 = -1;
   int *debut_but;
   int debut_trans_biased = -1, inc_trans_biased = -1;
   int *mode_but;
   float robot_posx, robot_posy;
   int k;
   es_lieu *lieu = NULL;
   es_trans *trans = NULL;
   int *coded_trans = NULL;
   int *coded_lieu = NULL;
   
#ifndef AVEUGLE
   int i;
   int trans_biased_win = -1;
   int imax, j;
   float act_trans_biased = 0, act_but = 0., mvt;
   float maxt = -100.,max;
   float mint = 100.;
   float couleur, epaisseur;
   int tmax[1000];
   float mvt_predit = 0., orientation = 0.;
   TxPoint point1, point2, point;
   char value[6];
   int color_place = rgb16M(31, 119, 36);
   int color_current_place = rgb16M(118,244,118);
#endif

   (void) gpe_but; // (unused)
   (void) gpe_reward; // (unused)
   (void) inc_but; // (unused)
   (void) debut_but; // (unused)
   (void) debut_trans_biased; // (unused)
   (void) inc_trans_biased; // (unused)

   my_data = (MyData_f_monitor_planif *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("ERROR in f_monitor_planif(%s): Cannot load data\n", def_groupe[gpe].no_name);
   }

   lieu = my_data->lieu;
   trans = my_data->trans;
   coded_trans = my_data->coded_trans;
   coded_lieu = my_data->coded_lieu;
   gpe_vigilence = my_data->gpe_vigilence;
   gpe_but = my_data->gpe_but;
   gpe_recrut = my_data->gpe_recrut;
   debut_but = my_data->debut_but;
   inc_but = my_data->inc_but;
   gpe_EC = my_data->gpe_EC;
   gpe_CA3 = my_data->gpe_CA3;
   gpe_DG = my_data->gpe_DG;
   gpe_transition_lieu = my_data->gpe_transition_lieu;
   gpe_pos = my_data->gpe_pos;
   gpe_reward = my_data->gpe_reward;
   mode_but = my_data->mode_but;
   gpe_mode = my_data->gpe_mode;
   gpe_mvt_predit = my_data->gpe_mvt_predit; 
   gpe_trans_biased = my_data->gpe_trans_biased; 
   size_but = my_data->size_but; 

   debut_EC = def_groupe[gpe_EC].premier_ele;
   debut_CA3 = def_groupe[gpe_CA3].premier_ele;

   if (gpe_trans_biased >= 0)
   {
      debut_trans_biased = def_groupe[gpe_trans_biased].premier_ele;
      inc_trans_biased = def_groupe[gpe_trans_biased].nbre / (def_groupe[gpe_trans_biased].taillex * def_groupe[gpe_trans_biased].tailley);
   }

   /* Handling of groups containing micro/macro neurons */
   inc_EC = def_groupe[gpe_EC].nbre / (def_groupe[gpe_EC].taillex * def_groupe[gpe_EC].tailley);
   inc_CA3 = def_groupe[gpe_CA3].nbre / (def_groupe[gpe_CA3].taillex * def_groupe[gpe_CA3].tailley);

   /* position du robot */
   if (gpe_pos == -1)
   {
      robot_posx = posx;
      robot_posy = posy;
   }
   else
   {
      robot_posx = neurone[def_groupe[gpe_pos].premier_ele].s1;
      robot_posy = neurone[def_groupe[gpe_pos].premier_ele + 1].s1;
   }

/*calcul du mode d'affiche du but*/
   if(gpe_mode != -1)
   {
      mode_but[0] = (int)(neurone[def_groupe[gpe_mode].premier_ele].s1);	
   }

   /*regarde nouveau lieu*/
   lieu_max = neurone_max(gpe_EC);
   lieu_max_ind = (lieu_max - inc_EC + 1 - debut_EC) / inc_EC;

   dprints("f_monitor_planif(%s): Vigilence = %f\n", def_groupe[gpe].no_name, neurone[def_groupe[gpe_vigilence].premier_ele].s1);
   if (neurone[def_groupe[gpe_vigilence].premier_ele].s1 < 0.5 || neurone[lieu_max].s1 <= def_groupe[gpe_EC].seuil)
   {
      dprints("f_monitor_planif(%s): No new place cell (activity too low)\n", def_groupe[gpe].no_name);
   }
   else
   {
      dprints("f_monitor_planif(%s): Place cell max = %i (act = %f)\n", def_groupe[gpe].no_name, lieu_max, neurone[lieu_max].s1);
      if (lieu[lieu_max_ind].neurone == -1)
      {
		coded_lieu[my_data->nbr_de_lieu] = lieu_max_ind;
		my_data->nbr_de_lieu++;
		lieu[lieu_max_ind].neurone = lieu_max;
		if(lieu[lieu_max_ind].recrutement==NULL) {
		   lieu[lieu_max_ind].recrutement = MANY_ALLOCATIONS(size_but,int);
		   /** a la creation si gpe_recrut pas gere alors recrutement=1, sinon 0*/
		   for(k=0;k<size_but;k++){
		      if (gpe_recrut[k] == -1){
			 lieu[lieu_max_ind].recrutement[k]=1;
		      }
		      else{
			 lieu[lieu_max_ind].recrutement[k]=0;
		      }
		   }
		}
		lieu[lieu_max_ind].x = (int) robot_posx;
		lieu[lieu_max_ind].y = (int) robot_posy;
	
	    dprints("f_monitor_planif(%s): update place x = %d, y = %d\n", def_groupe[gpe].no_name, lieu[lieu_max_ind].x,  lieu[lieu_max_ind].y);
      }
	  lieu[lieu_max_ind].x = (int) robot_posx;
	  lieu[lieu_max_ind].y = (int) robot_posy;
	  
      dprints("f_monitor_planif(%s): update place x = %d, y = %d\n", def_groupe[gpe].no_name, lieu[lieu_max_ind].x,  lieu[lieu_max_ind].y);
   }

    DG_max_ind = neurone_max(gpe_DG);

    for(k=0;k<size_but;k++){
       if(gpe_recrut[k] >= 0 && neurone[def_groupe[gpe_recrut[k]].premier_ele].s1 > 0.5 && lieu[lieu_max_ind].neurone >= 0) 
       {
	  lieu[lieu_max_ind].recrutement[k]=1;
	  dprints("Le lieu %d a ete mis a jour pour la carte %d \n",lieu_max_ind,k);
	  /*if(DG_max_ind != -1){
	    if(lieu[DG_max_ind].recrutement!=NULL && lieu[DG_max_ind].neurone >= 0){
	    lieu[DG_max_ind].recrutement[k]=1;
	    dprints("Le lieu %d de DG a ete mis a� jour\n",DG_max_ind);
	    }
	    }*/
       }
       else dprints("Le lieu %d n'a pas ete mis a jour pour la carte %d \n",lieu_max_ind,k); 
       /** carte qui n'existe pas forcement - si gpe_recrut n'est pas
	* defini*/
    }

   trans_max = neurone_max(gpe_CA3);
   trans_max_ind = (trans_max - inc_CA3 + 1 - debut_CA3) / inc_CA3;
   
   dprints("f_monitor_planif(%s): Transitions_lieu = %f, neurone trans_max : %f\n", def_groupe[gpe].no_name,
	   neurone[def_groupe[gpe_transition_lieu].premier_ele].s1,neurone[trans_max].s1);

   if (neurone[def_groupe[gpe_transition_lieu].premier_ele].s1 < 0.5 || neurone[trans_max].s1 <= def_groupe[gpe_CA3].seuil)
   {
      dprints("f_monitor_planif(%s): No new transition (activity too low)\n", def_groupe[gpe].no_name);
   }
   else
   {
      dprints("f_monitor_planif(%s): Transition max = %d (act = %f)\n",def_groupe[gpe].no_name, trans_max, neurone[trans_max].s1);

      if (trans[trans_max_ind].neurone == -1)
      {
		trans[trans_max_ind].neurone = trans_max;

		coded_trans[my_data->nbr_de_trans] = trans_max_ind;
		my_data->nbr_de_trans++;
		dprints("f_monitor_planif(%s): New transition => ind = %d\n", def_groupe[gpe].no_name, trans_max_ind);
      }

      if(trans[trans_max_ind].recrutement == NULL) {
	 trans[trans_max_ind].recrutement = MANY_ALLOCATIONS(size_but,int);
	 /** a la creation si gpe_recrut pas gere alors recrutement=1, sinon 0*/
	 /** rq : doit on vraiment tester gpe_recrut pour chaque k ? */
	 for(k=0;k<size_but;k++){
	    if (gpe_recrut[k] == -1){
	       trans[trans_max_ind].recrutement[k]=1;
	    }
	    else{
	       trans[trans_max_ind].recrutement[k]=0;
	    }
	 }
      }

      /* Handling of memory and precise timing versions of DG */

      if (def_groupe[gpe_DG].taillex > 1 && def_groupe[gpe_DG].tailley > 1)
      {
		trans[trans_max_ind].depart = (int) (DG_max_ind - def_groupe[gpe_DG].premier_ele) / def_groupe[gpe_DG].taillex;
      }
      else
      {
		trans[trans_max_ind].depart = DG_max_ind - def_groupe[gpe_DG].premier_ele;
      }
      
      trans[trans_max_ind].arrive = lieu_max_ind;
      
      /* Handling of place to place transitions or auto-transitions */
      if(trans[trans_max_ind].depart == trans[trans_max_ind].arrive)
      {
		trans[trans_max_ind].autotrans = 1;
      }
      else
      {
		trans[trans_max_ind].autotrans = 0;
      }

      if (gpe_mvt_predit >= 0)
      {
        trans[trans_max_ind].mvt = neurone[def_groupe[gpe_mvt_predit].premier_ele].s1 * 2 * M_PI + M_PI;
	  }
      else
      {
        trans[trans_max_ind].mvt = -99999;
      }
      dprints("f_monitor_planif(%s): update transition from = %d, to = %d\n", def_groupe[gpe].no_name, trans[trans_max_ind].depart,  trans[trans_max_ind].arrive);
	
      for(k=0;k<size_but;k++){
	 if(gpe_recrut[k] >= 0 && neurone[def_groupe[gpe_recrut[k]].premier_ele].s1 > 0.5 && trans[trans_max_ind].neurone >= 0) 
	 {
	    trans[trans_max_ind].recrutement[k]=1;
	 }
	 else dprints("La transition %d n'a pas ete mis a� jour pour la carte %d \n",trans_max_ind,k);
      }
      if(trans[trans_max_ind].recrutement[1]==1) dprints("La transition %d est recrutee pour la carte 1\n",trans_max_ind);
      else dprints("La transition %d n'est pas recrutee pour la carte 1\n",trans_max_ind);
	  
      dprints("f_monitor_planif(%s): update transition from = %d, to = %d\n", def_groupe[gpe].no_name, trans[trans_max_ind].depart,  trans[trans_max_ind].arrive);
   }

   dprints("f_monitor_planif(%s): Number of transitions = %d \n",def_groupe[gpe].no_name, my_data->nbr_de_trans);


#ifndef AVEUGLE
   /* Clears the drawing area */
   point.x = 0;
   point.y = 0;
   TxDessinerRectangle(&image3, blanc, TxPlein, point, 1000, 1000, 1);

   if (my_data->world != NULL)
      display_world(&image3, my_data->world);


   /*Affiche tous les lieux */
   for (i = 0; i < my_data->nbr_de_lieu; i++)
   {
      point.x = lieu[coded_lieu[i]].x;
      point.y = lieu[coded_lieu[i]].y;
      TxDessinerCercle_rgb16M(&image3, color_place, TxPlein, point, 4, 1);
   }

   /*Affiche toutes les transitions */
   for (i = 0; i < my_data->nbr_de_trans; i++)
   {
      if (trans[coded_trans[i]].neurone == trans_max)
      {
	 /*affiche transition max*/
		if (neurone[def_groupe[gpe_transition_lieu].premier_ele].s > 0.5)
		{
			couleur = rgb16M(255,0,0);;
		}
		else
		{
			couleur = rgb16M(0,255,0);	 
		}

		draw_transition(&(trans[coded_trans[i]]), lieu, &image3, couleur, 7);
      }

      couleur = colormap_2(sqrt(neurone[trans[coded_trans[i]].neurone].s1));
      draw_transition(&(trans[coded_trans[i]]), lieu, &image3, couleur, 2);
      mvt = trans[coded_trans[i]].mvt;

      if (mvt >= -1000)
      {
        point1.x = 0.75 * lieu[trans[coded_trans[i]].depart].x + 0.25 * lieu[trans[coded_trans[i]].arrive].x;
        point1.y = 0.75 * lieu[trans[coded_trans[i]].depart].y + 0.25 * lieu[trans[coded_trans[i]].arrive].y;

        point2.x = point1.x + 15. * cos(mvt);
        point2.y = point1.y + 15. * sin(mvt);

        TxDessinerFleche_rgb16M(&image3, 0, point1, point2, 1);
      }
      
   }

   /*affiche robot */
   dprints("f_monitor_planif(%s): Position of robot x = %f, y = %f\n", def_groupe[gpe].no_name, robot_posx, robot_posy);
   point.x = robot_posx;
   point.y = robot_posy;
   TxDessinerRectangle_rgb16M(&image3, 0, TxPlein, point, 5, 5, 1);
    
   if (gpe_pos >= 0 && def_groupe[gpe_pos].taillex * def_groupe[gpe_pos].tailley > 2)
   {
      orientation = neurone[def_groupe[gpe_pos].premier_ele+2].s1;
      point1.x = robot_posx + 2;
      point1.y = robot_posy + 2;
      point2.x = robot_posx + 2 + 10. * cos(orientation);
      point2.y = robot_posy + 2 + 10. * sin(orientation);
      TxDessinerSegment_rgb16M(&image3, 0, point1, point2, 2);
   }

   /*affiche lieu */
   point.x = lieu[lieu_max_ind].x;
   point.y = lieu[lieu_max_ind].y;
   TxDessinerCercle_rgb16M(&image3, color_current_place, TxPlein, point, 4, 1);
	
	
   TxFlush(&image3);

	
/********************************************************************************************   
	Affiche le gradient du but sur Im4.
********************************************************************************************/	
   for(k=0;k<size_but;k++){
	if (gpe_but[k] != -1)
	{
      /*efface image4*/
      point.x = 0;
      point.y = 0;
      TxDessinerRectangle(&(image4[k]), blanc, TxPlein, point, 1000, 1000, 1);

      if (my_data->world != NULL)
		display_world(&(image4[k]), my_data->world);

      if (mode_but[k] == 1)
      {
		/* Prints only the best transitions for each place */
		maxt = -100.;
		mint = 100.;
		for(i = 0; i < my_data->nbr_de_lieu; i++)
		{
			imax = -1;
			max = -100.;
			for(j = 0; j < my_data->nbr_de_trans; j++)
			{
				act_but = neurone[coded_trans[j] * inc_but[k] + debut_but[k] + inc_but[k] - 1].s1;

				if (trans[coded_trans[j]].depart == coded_lieu[i] && act_but > max && trans[coded_trans[j]].autotrans == 0)
				{
					max = act_but;
					imax = j;
				}
			}
			tmax[i] = imax;

			if (imax >= 0)
			{					
				if (max >= maxt)
				maxt = neurone[imax * inc_but[k] + debut_but[k] + inc_but[k] - 1].s1;
				if (max <= mint)
				mint = neurone[imax * inc_but[k] + debut_but[k] + inc_but[k] - 1].s1;
			}
		}
		dprints("f_monitor_planif(%s): activity of transitions => max = %f, min = %f\n", def_groupe[gpe].no_name, maxt, mint);	

		/*affiche les trans_max*/
		for (i = 0; i < my_data->nbr_de_lieu; i++)
		{
			imax = tmax[i];
			if (imax >= 0 && trans[coded_trans[i]].recrutement[k]==1)
			{
				imax = coded_trans[imax];
				act_but = neurone[imax * inc_but[k] + debut_but[k] + inc_but[k] - 1].s1;
				
				epaisseur = pow(((act_but - mint) / (maxt - mint)), 3);
				couleur = colormap_1((act_but - mint) / (maxt - mint));

				dprints("f_monitor_planif(%s): Printing transition %i from %i to %i (act = %f)\n", def_groupe[gpe].no_name, imax, i, trans[imax].arrive, act_but);

				draw_transition(&(trans[imax]), lieu, &(image4[k]), couleur, epaisseur*4+1);
				
				point1.x = 0.75 * lieu[trans[imax].depart].x + 0.25 * lieu[trans[imax].arrive].x;
				point1.y = 0.75 * lieu[trans[imax].depart].y + 0.25 * lieu[trans[imax].arrive].y;
				
				if (my_data->trans_name > 0)
              	{
					sprintf(value,"Transition %d",trans[imax].neurone);
					TxEcrireChaineSized(&(image4[k]), noir, point1, value, 12., NULL);
				}
				
				if (my_data->print_vals > 0)
              	{
					sprintf(value, "%.2f", act_but);
					TxEcrireChaineSized(&(image4[k]), vert, point1, value, 12., NULL);
				}
			}
		}    
      }
      else
      {
		/* Affiche trans predite par but */
		for (i = 0; i < my_data->nbr_de_trans; i++)
		{
			act_but = neurone[coded_trans[i] * inc_but[k] + debut_but[k] + inc_but[k] - 1].s1;
/*!! 			printf("Neurone [%d] : act_but : %f -- recrutement [%d] : %d\n",i,act_but,k,trans[coded_trans[i]].recrutement[k]); */
			if(act_but > 0.	&& trans[coded_trans[i]].recrutement[k]==1)
			{
				dprints("f_monitor_planif(%s): Printing transition %i from %i to %i (act = %f)\n", def_groupe[gpe].no_name, i, trans[i].depart, trans[i].arrive, act_but);

				couleur = colormap_1(act_but);
				draw_transition(&(trans[coded_trans[i]]), lieu, &(image4[k]), couleur, 2);

				point1.x = 0.75 * lieu[trans[coded_trans[i]].depart].x + 0.25 * lieu[trans[coded_trans[i]].arrive].x;
				point1.y = 0.75 * lieu[trans[coded_trans[i]].depart].y + 0.25 * lieu[trans[coded_trans[i]].arrive].y;
				
				if (my_data->trans_name > 0)
              	{
					sprintf(value,"Transition %d",trans[coded_trans[i]].neurone);
					TxEcrireChaineSized(&(image4[k]), noir, point1, value, 12., NULL);
				}

				if (my_data->print_vals > 0)
              	{
					sprintf(value, "%.2f", act_but);
					TxEcrireChaineSized(&(image4[k]), vert, point1, value, 12., NULL);
				}
			}
		}
      }
      
      if (gpe_trans_biased >= 0)
      {
	 trans_biased_win = neurone_max(gpe_trans_biased);
	 if(trans_biased_win >=0)
	 {
	    act_trans_biased = neurone[trans_biased_win].s1;
/*!! 	    printf("recrutement [%d] : act_trans_biased : %f, recrutement : %p\n",trans_biased_win,act_trans_biased,(void*)trans[trans_biased_win].recrutement); */
	    if (act_trans_biased > 0 && trans[(trans_biased_win - inc_trans_biased + 1 - debut_trans_biased) / inc_trans_biased].recrutement[k] == 1)
	    {
	       draw_transition(&(trans[(trans_biased_win - inc_trans_biased + 1 - debut_trans_biased) / inc_trans_biased]), lieu, &(image4[k]), rgb16M(0,255,0), 3);
	    }
	 }
      }
      dprints("On affiche les %d lieux pertinents pour la carte %d\n", my_data->nbr_de_lieu,k);

	  /*Affiche les lieux importants*/
      for (i = 0; i < my_data->nbr_de_lieu; i++)
	  {      
		point.x = lieu[coded_lieu[i]].x;
		point.y = lieu[coded_lieu[i]].y;
		if(lieu[coded_lieu[i]].recrutement[k]==1)
		{
				TxDessinerCercle_rgb16M(&(image4[k]), color_place, TxPlein, point, 4, 1);
		}
		else dprints("Lieu non pertinent, val : %d\n",(lieu[coded_lieu[i]]).recrutement[k]);
      }

      /*affiche robot */
      point.x = robot_posx - 2;
      point.y = robot_posy - 2;
      TxDessinerRectangle_rgb16M(&(image4[k]), 0, TxPlein, point, 5, 5, 1);

      if (gpe_mvt_predit >= 0)
      {
		mvt_predit = neurone[def_groupe[gpe_mvt_predit].premier_ele].s1*2*M_PI + M_PI;
		point1.x = robot_posx;
		point1.y = robot_posy;
		point2.x = robot_posx + 10. * cos(mvt_predit);
		point2.y = robot_posy + 10. * sin(mvt_predit);
		TxDessinerSegment_rgb16M(&(image4[k]), 0, point1, point2, 2);
      }	

      /*affiche lieu */
		
      if(lieu[lieu_max_ind].neurone >= 0 && lieu[lieu_max_ind].recrutement[k]==1)
      {
		point.x = lieu[lieu_max_ind].x;
		point.y = lieu[lieu_max_ind].y;
		
		/*lieu ou se trouve actuellement le robot*/
		TxDessinerRectangle_rgb16M(&(image4[k]), color_current_place, TxPlein, point, 5, 5, 1);
      }
	  
      /*affiche source*/
      if (gpe_reward == -1)
      {
		affiche_ptessentiel1(&(image4[k]));
      }
      else
      {
		/* TODO: Gestion sources (hors environnement simule) */
      }
      TxFlush(&(image4[k]));
	}
   }
#endif
}

void destroy_monitor_planif(int gpe)
{
   dprints("destroy_monitor_planif(%s): Entering function\n", def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
      free(((MyData_f_monitor_planif*)def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_monitor_planif(%s): Leaving function\n", def_groupe[gpe].no_name);
}


void save_monitor_planif(int gpe)
{
   MyData_f_monitor_planif *mydata = NULL;

   printf("save_monitor_planif(%s): Entering function\n", def_groupe[gpe].no_name);
   mydata = (MyData_f_monitor_planif *) def_groupe[gpe].data;

   /* Saving all debug information */
   if (mydata != NULL)
   {
      save_transitions_monitor_planif(mydata);
      save_robot_position_monitor_planif(mydata);
      save_resources_monitor_planif(mydata);
   }
   printf("save_monitor_planif(%s): Leaving function\n", def_groupe[gpe].no_name);
}

#ifndef AVEUGLE
void create_image3(TxDonneesFenetre * fenetre)
{
   GtkWidget *vbox1;
   GtkWidget *scrolledwindow1;
   GtkWidget *viewport1;

   gdk_threads_enter();
   fenetre->region_rectangle = NULL;
   fenetre->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",
		    G_CALLBACK(cb_destroy_fenetre), fenetre);

   gtk_widget_set_size_request(fenetre->window, 300, 350);
   gtk_object_set_data(GTK_OBJECT(fenetre->window), fenetre->name,
		       fenetre->window);
   gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);

   vbox1 = gtk_vbox_new(FALSE, 1);
   gtk_widget_ref(vbox1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "vbox3", vbox1,
			    (GtkDestroyNotify) gtk_widget_unref);

   gtk_widget_show(vbox1);
   gtk_container_add(GTK_CONTAINER(fenetre->window), vbox1);

   /* Pack it all together */


   scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
   gtk_widget_ref(scrolledwindow1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "scrolledimage3",
			    scrolledwindow1,
			    (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(scrolledwindow1);
   gtk_box_pack_start(GTK_BOX(vbox1), scrolledwindow1, TRUE, TRUE, 0);

   viewport1 = gtk_viewport_new(NULL, NULL);
   gtk_widget_ref(viewport1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "viewportimage3",
			    viewport1, (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(viewport1);
   gtk_container_add(GTK_CONTAINER(scrolledwindow1), viewport1);



   /*********************/

   fenetre->da = gtk_drawing_area_new();
   fenetre->width = 3204;
   fenetre->height = 900;
   gtk_widget_set_size_request(fenetre->da, fenetre->width, fenetre->height);
   TxResetDisplayPosition(fenetre);

   fenetre->font = gdk_font_load("-*-courier-medium-r-*-*-*-110-*-*-*-*-*-*");

   gtk_widget_ref(fenetre->da);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "drawingareaimage3",
			    fenetre->da,
			    (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(fenetre->da);
   gtk_container_add(GTK_CONTAINER(viewport1), fenetre->da);

   /***************************************************/
   fenetre->pixmap = NULL;     /* voir configure_event pour l'init et l'affichage */


   fenetre->filew = NULL;
   fenetre->box1 = vbox1;      /* peut etre que NULL aurait ete mieux - pas de vumetre possible */

   /* Signals used to handle backing pixmap */

   g_signal_connect(fenetre->da, "expose_event",
		    G_CALLBACK(image3_expose_event), NULL);
   g_signal_connect(fenetre->da, "configure_event",
		    G_CALLBACK(image3_configure_event), NULL);

   gtk_widget_show_all(fenetre->window);
   gdk_threads_leave();
}

gboolean image3_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data)
{

   return fenetre_configure_event(&image3, widget, event, data);
}

gboolean image3_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data)
{
   return fenetre_expose_event(&image3, widget, event, data);
}


void create_image4(TxDonneesFenetre * fenetre, int i)
{
   GtkWidget *vbox1;
   GtkWidget *scrolledwindow1;
   GtkWidget *viewport1;

   gdk_threads_enter();
   fenetre->region_rectangle = NULL;
   fenetre->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",
		    G_CALLBACK(cb_destroy_fenetre), fenetre);

	gtk_widget_set_size_request(fenetre->window, 300, 350);
	gtk_object_set_data(GTK_OBJECT(fenetre->window), fenetre->name,
			fenetre->window);
	gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);

	vbox1 = gtk_vbox_new(FALSE, 1);
	gtk_widget_ref(vbox1);
	gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "vbox3", vbox1,
			(GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_show(vbox1);
	gtk_container_add(GTK_CONTAINER(fenetre->window), vbox1);

	/* Pack it all together */

	scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_ref(scrolledwindow1);
	gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "scrolledimage4",
			scrolledwindow1,
			(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(scrolledwindow1);
	gtk_box_pack_start(GTK_BOX(vbox1), scrolledwindow1, TRUE, TRUE, 0);

	viewport1 = gtk_viewport_new(NULL, NULL);
	gtk_widget_ref(viewport1);
	gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "viewportimage4",
			viewport1, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(viewport1);
	gtk_container_add(GTK_CONTAINER(scrolledwindow1), viewport1);


	/*********************/

	fenetre->da = gtk_drawing_area_new();
	fenetre->width = 3204;
	fenetre->height = 900;
	gtk_widget_set_size_request(fenetre->da, fenetre->width, fenetre->height);
	TxResetDisplayPosition(fenetre);
	fenetre->font = gdk_font_load("-*-courier-medium-r-*-*-*-110-*-*-*-*-*-*");

	gtk_widget_ref(fenetre->da);
	gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "drawingareaimage4",
			fenetre->da,
			(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(fenetre->da);
	gtk_container_add(GTK_CONTAINER(viewport1), fenetre->da);


	/***************************************************/
	fenetre->pixmap = NULL;     /* voir configure_event pour l'init et l'affichage */

	fenetre->filew = NULL;
	fenetre->box1 = vbox1;      /* peut etre que NULL aurait ete mieux - pas de vumetre possible */

	/* Signals used to handle backing pixmap */

	g_signal_connect(fenetre->da, "expose_event",
			 G_CALLBACK(image4_expose_event), (gpointer) (long) i);
	g_signal_connect(fenetre->da, "configure_event",
			 G_CALLBACK(image4_configure_event), (gpointer) (long) i);
		
	gtk_widget_show_all(fenetre->window);
   gdk_threads_leave();
}

gboolean image4_configure_event(GtkWidget * widget, GdkEventConfigure * event,
                                gpointer data)
{
   return fenetre_configure_event(&(image4[(long)data]), widget, event, data);
}

gboolean image4_expose_event(GtkWidget * widget, GdkEventExpose * event,
                             gpointer data)
{
   return fenetre_expose_event(&(image4[(long)data]), widget, event, data);
}
#endif
