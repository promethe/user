/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\ingroup libIO_Robot
\defgroup f_print_place f_print_place
\brief 

\section Author
- name: xxxxxxxx
- Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number

\section Macro
-TAILLEX

\section Local varirables
-float posx
-float pos.y

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>

void function_print_place(int gpe)
{
    TxPoint point, pos;
    char chaine1[50], retour[255];
    float max;
    int i, deb_entree, nbre_entree, no_max = 0, groupe_entree, print_in_file =
        -1;
    int val, imag = 1;

    (void) print_in_file; // (unused)
    (void) point; // (unused)


    /*recherche le groupe donnant la position */
    val = trouver_entree(gpe,(char*) "pos");
    if (val != -1)
    {
        pos.x = neurone[def_groupe[val].premier_ele].s2;
        pos.y = neurone[def_groupe[val].premier_ele + 1].s2;
    }
    else
    {
        /*sinon on est en simulation */
        /*initialise la structure point avec les coord courantes de l'agent */
        pos.x = posx;
        pos.y = posy;
    }

    /*apres ce traitement la liason i doit etre celle qui lit ce groupe a celui qui contient les infos de cellules de lieux */
    i = find_input_link(gpe, 0);    /* recherche le 1er lien */
    if (i == -1)
    {
        EXIT_ON_ERROR("error no input group for group %d\n", gpe);
        return;
    }
    else if (strcmp(liaison[i].nom, "pos") >= 0)
    {
        i = find_input_link(gpe, 1);    /* recherche le 1er lien */
        if (i == -1)
        {
            EXIT_ON_ERROR("error no input group for group %d\n", gpe);
            return;
        }
    }
    groupe_entree = liaison[i].depart;

    val = prom_getopt(liaison[i].nom, "n", retour); /* valeur numerique sinon couleur par defaut */
    print_in_file = prom_getopt(liaison[i].nom, (char*)"f", retour);   /* valeur numerique sinon couleur par defaut */
    /*determine la fenetre d'affichage (image1 (-I1)) ou image2(-I2)) */
    /*im=()*/prom_getopt(liaison[i].nom,(char*) "I",retour);
    if (imag == 2)
        imag = 2;
    else
        imag = 1;               /*par defaut pour rester compatible avec autres scripts */
    deb_entree = def_groupe[groupe_entree].premier_ele;
    nbre_entree = def_groupe[groupe_entree].nbre;

    max = -1;

    for (i = deb_entree; i < (deb_entree + nbre_entree); i++)
    {
        if (neurone[i].s > max)
        {
            max = neurone[i].s;
            no_max = i;
        }
    }



    no_max = no_max - deb_entree;
    point.x = (int) pos.x - pas / 2;
    point.y = (int) pos.y - pas / 2;
/*  point.x=5*(point.x/5);
  point.y=5*(point.y/5);*/
    /*enregistrement de la position de l'animat dans un fichier */
#ifdef PRINT_FILE
    strcpy(retour, "robot_pos");
    f = fopen(retour, "a");
    fprintf(f, "%d, %d, %d,%d,%d,%d\n", (int) posx, (int) posy,
            no_max * 4000 + 5, point.x, point.y, no_max,);
    fclose(f);
#endif
    /*printf("RECOGNIZED place = %d ,pos.x=%d, pos.y=%d\n",no_max,point.x,point.y); */
    sprintf(chaine1, "%3d", no_max);
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
    if (imag == 1)
    {
        if (val > 0)
            TxEcrireChaine(&image1, vert, point, chaine1, 0);
        else
            TxDessinerRectangle_rgb(&image1, no_max * 4000 + 5, TxPlein,
                                    point, pas, pas, 1);
        /*  TxEcrireChaine(&image2,noir,point,chaine1,0); */

        TxFlush(&image1);
    }
    else
    {
        if (val > 0)
            TxEcrireChaine(&image2, vert, point, chaine1, 0);
        else
            TxDessinerRectangle_rgb(&image2, no_max * 4000 + 5, TxPlein,
                                    point, pas, pas, 1);
        /*  TxEcrireChaine(&image2,noir,point,chaine1,0); */

        TxFlush(&image2);
    }
#endif
}
