/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: P. Gaussier
- description: specific file creation
- date: 01/2006

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Compute the error about the absolute position of the robot from the neural field
used for path integration (difference between know position in the simulator and
position computed from the field

Macro:
-TAILLEX

Local varirables:
-float posx
-float pos.y

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: tool for robot simulation analysis (navigation)
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "tools/include/macro.h"
#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/trouver_entree.h>

/*#define DEBUG */

#define max_nb_cases 200

extern TxPoint nid;             /* defini dans f_explo_alea_ou_dirige.c */

int posx_percue, posy_percue;

typedef struct struct_stat_on_position
{
    int groupe_entree_field;    /* field pour le champs contenant l'integration de chemin */
    int groupe_entree_pos;      /* pos pour la position du robot sur des 2 neurones */
    char *name;                 /* nom du fichier utilise pour la sauvegarde */
    FILE *fd;
} struct_stat_on_position;


void destroy_stat_on_position_error(int gpe)
{
    struct_stat_on_position *ptr;

    printf("Fin destroy_stat_on_position_error \n");
    ptr = (struct_stat_on_position *) def_groupe[gpe].data;
    if (ptr == NULL)
    {
        printf
            ("fin_stat_on_position: L'initialisation pour le groupe %d f_stat_on_position_error absente !!!\n",
             gpe);
        printf
            ("Penser a appeler new_stat_on_position_error comme methode new de f_stat_on_position_error.\n");
        exit(1);
    }

    fclose(ptr->fd);
}

/* fonction appelee au demarrage */
void new_stat_on_position_error(int gpe)
{
    int i, j, val;
    int groupe_entree_field = -1;
    int groupe_entree_pos = -1;
    struct_stat_on_position *ptr;
    char retour[255];
    char name[255];

#ifdef DEBUG
    printf("new stat_on_position_error du groupe %d \n", gpe);
#endif

    if (def_groupe[gpe].data != NULL)
    {
        printf("new_stat_on_position_error deja initialise \n");
        return;
    }

    ptr = (struct_stat_on_position *) malloc(sizeof(struct_stat_on_position));

    /*apres ce traitement la liason i doit etre celle qui lit ce groupe est celui qui contient les infos de cellules de lieux */
    name[0] = (char) 0;
    j = 0;
    i = find_input_link(gpe, j);    /* recherche le 1er lien */
    if (i == -1)
    {
        printf("error no input group for group %d\n", gpe);
        return;
    }

    do
    {
        printf("teste la liaison %d  = %s \n", j, liaison[i].nom);
        if (strcmp(liaison[i].nom, "pos") >= 0) /* gpe en entree contenant la position du robot sur ses neurones */
        {
            groupe_entree_pos = liaison[i].depart;
        }
        else
        {
            val = prom_getopt(liaison[i].nom, "f", retour);
            if (val == 2)
            {
                strcpy(name, retour);
                groupe_entree_field = liaison[i].depart;
            }
        }
        j++;
    } while ((i = find_input_link(gpe, j)) != -1);

  /*-f pour le nom du fichier dans lesquels seront mis les resultats  */

    if (groupe_entree_field == -1)
    {
        printf
            ("ERROR the group %d f_stat_position_error has no link with -f \n",
             gpe);
        printf
            ("This link should be present at least to indicate the input field. \n");
        printf
            ("(also useful to provide optional filename to store the results. \n");
        exit(1);
    }

    if (strlen(name) < 2)
    {
        printf("il manque le nom du fichier a utiliser: on prend ");
        printf("liaison %d s=%s \n", i, liaison[i].nom);
        sprintf(name, "default_error_pos_gpe_%d", gpe);
        printf("%s \n", name);
    }

    /*recherche le groupe donnant la position */

    ptr->name = (char *) malloc(255 * sizeof(char));
    if (ptr->name == NULL)
    {
        printf
            ("pb allocation dans new_stat_on_position_error name malloc \n");
        exit(1);
    }
    strcpy(ptr->name, name);
    ptr->fd = fopen(name, "w");
    if (ptr->fd == NULL)
    {
        printf
            ("ERROR file %s cannot be opened in new_stat_on_position_error \n",
             name);
        exit(1);
    }

    ptr->groupe_entree_pos = groupe_entree_pos;
    ptr->groupe_entree_field = groupe_entree_field;
    def_groupe[gpe].data = (void *) ptr;    /* sauvegarde des donnees dans le champs data du groupe */
    printf("fin init pour f_stat_position_error name = %s \n", name);
}

/*--------------------------------------------------------------*/
void function_stat_on_position_error(int gpe)
{
    TxPoint pos;
    float max, angle, distance, erreur;
    int n, deb, no_max = 0;
    int x, y;
    int ex, ey;
    struct_stat_on_position *ptr;
  
#ifdef DEBUG
    printf("mise a jour function_stat_on_position_error(%d) \n", gpe);
#endif
    ptr = (struct_stat_on_position *) def_groupe[gpe].data;
    if (ptr == NULL)
    {
        printf
            ("L'initialisation pour le groupe %d f_stat_on_position_error absente !!!\n",
             gpe);
        printf
            ("Penser a appeler new_stat_on_position_error comme methode new de f_stat_on_position_error.\n");
        exit(1);
    }

    /* printf("ptr->groupe_entree_pos =%d \n",ptr->groupe_entree_pos); */
    if (ptr->groupe_entree_pos != -1)
    {
        pos.x = neurone[def_groupe[ptr->groupe_entree_pos].premier_ele].s2;
        pos.y =
            neurone[def_groupe[ptr->groupe_entree_pos].premier_ele + 1].s2;
    }
    else
    {
        /*sinon on est en simulation */
        /*initialise la structure point avec les coord courantes de l'agent */
        pos.x = posx;
        pos.y = posy;
    }

    /*   printf("groupe_entree_field = %d \n",ptr->groupe_entree_field); */
    nbre_entree = def_groupe[ptr->groupe_entree_field].nbre;
    deb = no_max = def_groupe[ptr->groupe_entree_field].premier_ele;
    max = neurone[no_max].s1;
    for (n = deb; n < deb + nbre_entree; n++)
    {
        /*printf("n=%d s=%f \n",n,neurone[n].s1); */
        if (neurone[n].s1 > max)
        {
            max = neurone[n].s1;
            no_max = n;
        }

    }
    angle =
        ((float) (no_max - deb)) / ((float) nbre_entree) * 2 *
        M_PI /*-M_PI-*/ ;
    distance = max * 5.;        /* rapport d'echelle entre pas et dist simul, pas tres propre !!! */
    /*printf("angle =%f distance= %f\n",angle,distance); */
    posx_percue = x = (int) (cos(angle) * distance) + nid.x;
    posy_percue = y = (int) (sin(angle) * distance) + nid.y;

    ex = posx - x;
    ey = posy - y;
    erreur = sqrt(ex * ex + ey * ey);
    fprintf(ptr->fd, "%f \n", erreur);
    /* printf("ex = %d ey = %d / x = %d posx = %d / y = %d posy = %d \n",ex,ey,x,pos.x,y,pos.y);
       printf("appuyer sur une touche \n");
       scanf("%c",&c); */
    (void) pos;

}
