/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_print_place f_print_place
\file f_print_place.c
\brief 
 


\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
 Print at the current robot location in the environment map,
 the number of the winner neuron associated to
 the input group => place number
 
\section Macro
-TAILLEX

\section Local varirables
-float posx
-float posy

\section Global variables
-none

\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


typedef struct MyData_f_print_place_elegant
{
    int val;
    int groupe_entree;
    int gpe_mvt;
    int gpe_guidance;
    int gpe_adaptation;
} MyData_f_print_place_elegant;

void function_print_place_elegant(int gpe)
{

    TxPoint point, point2, point_dep, point_dest;
    char chaine1[50], retour[255];
    float max, orient;
    int i_max;
    int i, deb_entree, nbre_entree, no_max = -1, groupe_entree = -1;
    int val = 0;
    int gpe_mvt = -1, gpe_guidance = -1, gpe_adaptation = -1, l;
    MyData_f_print_place_elegant *my_data = NULL;
    float inseuil;
    FILE *pc_learned = NULL, *traject = NULL;

    if (def_groupe[gpe].data == NULL)
    {
        if (def_groupe[gpe].nbre != 2)
        {
            EXIT_ON_ERROR("il faut deux groupe pour %s\n", __FUNCTION__);
            exit(0);
        }

        l = 0;
        i = find_input_link(gpe, l);    /* recherche le 1er lien */
        while (i != -1)
        {
            if (strcmp(liaison[i].nom, "PC") == 0)            groupe_entree = liaison[i].depart;
            if (prom_getopt(liaison[i].nom, "n", retour) >= 1)  val = 1;
            if (strcmp(liaison[i].nom, "mvt") == 0)          gpe_mvt = liaison[i].depart;
            if (strcmp(liaison[i].nom, "adaptation") == 0)   gpe_adaptation = liaison[i].depart;
            if (strcmp(liaison[i].nom, "guidance") == 0)     gpe_guidance = liaison[i].depart;

            l++;
            i = find_input_link(gpe, l);    /* recherche le 1er lien */

        }
        if (groupe_entree == -1)
        {
            EXIT_ON_ERROR("error no input group for group %d\n", gpe);
        }

        my_data = (MyData_f_print_place_elegant *) malloc(sizeof(MyData_f_print_place_elegant));
        if (my_data == NULL)
        {
            EXIT_ON_ERROR("error malloc: %s (%d ) : %d\n", __FUNCTION__, gpe,__LINE__);
        }
        my_data->groupe_entree = groupe_entree;
        my_data->gpe_mvt = gpe_mvt;
        my_data->gpe_guidance = gpe_guidance;
        my_data->gpe_adaptation = gpe_adaptation;
        my_data->val = val;
        def_groupe[gpe].data = my_data;
    }
    else
    {
        my_data = (MyData_f_print_place_elegant *) def_groupe[gpe].data;

        groupe_entree = my_data->groupe_entree;
        gpe_mvt = my_data->gpe_mvt;
        gpe_guidance = my_data->gpe_guidance;
        gpe_adaptation = my_data->gpe_adaptation;
        val = my_data->val;

    }
    dprints("debut effectif %s\n", __FUNCTION__);
    deb_entree = def_groupe[groupe_entree].premier_ele;
    nbre_entree = def_groupe[groupe_entree].nbre;


/* printf("debut %s(%d)\n",__FUNCTION__,gpe);*/

    max = -1;
    inseuil = def_groupe[gpe].seuil;
    for (i = deb_entree; i < (deb_entree + nbre_entree); i++)
    {
        if (neurone[i].s > inseuil)
            if (neurone[i].s > max)
            {
                max = neurone[i].s;
                no_max = i;
            }
    }



    no_max = no_max - deb_entree;
    if (pas > 5)
    {
        point.x = (int) posx - pas / 2;
        point.y = (int) posy - pas / 2;
    }
    else
    {
        point.x = (int) posx - 5;
        point.y = (int) posy - 5;
    }
    /*point.x=5*(point.x/5); */
    /*point.y=5*(point.y/5); */
    /*printf("RECOGNIZED place = %d ,posx=%d, posy=%d\n",no_max,point.x,point.y); */
    sprintf(chaine1, "%3d", no_max);
    
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
    if (val > 0)   TxEcrireChaine(&image1, vert, point, chaine1, 0);
    else if (gpe_mvt != -1)
    {
        TxDessinerRectangle_rgb(&image1, no_max * 4000 + 5, TxPlein, point, pas, pas, 1);
        max = 0.;
        i_max = -1;
        for (i = 0; i < def_groupe[gpe_mvt].nbre; i++)
        {
            if (neurone[def_groupe[gpe_mvt].premier_ele + i].s1 > max)
            {
                max = neurone[def_groupe[gpe_mvt].premier_ele + i].s1;
                i_max = i;
            }
        }

        orient = (float) (2. * M_PI * i_max) / (float) (def_groupe[gpe_mvt].nbre) + M_PI;
        point_dep.x = (int) (posx) - 1;
        point_dep.y = (int) (posy) - 1;
        TxDessinerRectangle(&image1, noir, TxPlein, point_dep, 3, 3, 1);
        point_dep.x = (int) (posx);
        point_dep.y = (int) (posy);
        point_dest.x = point_dep.x + (int) (10. * cos(orient));
        point_dest.y = point_dep.y + (int) (10. * sin(orient));
        TxDessinerSegment(&image1, noir, point_dep, point_dest, 1);
    }
    else
    {
        if (pas > 5)
        {
            TxDessinerRectangle_rgb(&image1, no_max * 4000 + 5, TxPlein, point, pas, pas, 1);
        }
        else
        {
            TxDessinerRectangle_rgb(&image1, no_max * 4000 + 5, TxPlein, point, 5, 5, 1);
        }
    }
    if (neurone[no_max + deb_entree].s > 0.995) TxDessinerRectangle_rgb(&image1, -3000, TxVide, point, pas, pas, 1);
    if (gpe_adaptation != -1)
    {
        point.x = 0;
        point.y = 0;
        if (neurone[def_groupe[gpe_adaptation].premier_ele + no_max].s1 > 0.99) TxDessinerRectangle(&image1, rouge, TxPlein, point, 10, 10, 1);
        else                                                                    TxDessinerRectangle(&image1, blanc, TxPlein, point, 10, 10, 1);
        TxDessinerRectangle(&image1, noir, TxVide, point, 10, 10, 1);
    }

    if (isequal(neurone[no_max + deb_entree].s, 1.0))
    {
        pc_learned = fopen("pc_learned.SAVE", "a");
        if (pc_learned == NULL)
        {
            EXIT_ON_ERROR("Impossible to create file pc_learned.SAVE\n");
        }
        fprintf(pc_learned, "%d %f %f\n", no_max, posx, posy);
        fclose(pc_learned);
    }
    traject = fopen("traject.SAVE", "a");
    if (traject == NULL)
    {
        EXIT_ON_ERROR("Impossible to create file traject.SAVE\n");
        exit(EXIT_FAILURE);
    }
    fprintf(traject, "%f %f %d\n", posx, posy, no_max);
    fclose(traject);

/*   if(neurone[no_max+deb_entree].s>0.9999)
      TxDessinerRectangle_rgb(&image1, -3000 ,TxVide,point,pas,pas,1);
    */
    if (gpe_guidance != -1)
    {
        point.x = 250;
        point.y = 200;
        TxDessinerRectangle(&image1, blanc, TxPlein, point, 10, 100, 1);
        TxDessinerRectangle_rgb(&image1, 4005, TxPlein, point, 10,100 * neurone[def_groupe[gpe_guidance].premier_ele].  s1, 1);
        cprints("difference angle:%f\n", neurone[def_groupe[gpe_guidance].premier_ele].s1 * 180.);
    }

/*   if(pas>5)
   {
	point.x=point.x-10;
	point.y=point.y+10;
	TxEcrireChaine(&image1,noir,point,chaine1,0);
   }*/
    point2.x = TAILLEX + 10;
    point2.y = 100;

    TxFlush(&image1);
#else
    UNUSED_PARAM traject; UNUSED_PARAM pc_learned; UNUSED_PARAM i_max;
    UNUSED_PARAM orient; UNUSED_PARAM point_dest; UNUSED_PARAM point_dep;
    UNUSED_PARAM point2;  
#endif
    neurone[def_groupe[gpe].premier_ele].s2 = neurone[def_groupe[gpe].premier_ele].s1 =neurone[def_groupe[gpe].premier_ele].s = posx;
    neurone[def_groupe[gpe].premier_ele + 1].s2 = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s = posy;
}
