/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_extract_multi_vals_to_vectors_circular f_extract_multi_vals_to_vectors_circular
\ingroup libIO_Robot
\brief Concatenation de plusieurs f_extract_val_to_vector_circular dans un seul groupe.
\details

\section Description
On considere un vector de scalaire que l'on va projeter sur un
ensemble de population de neurone selon une dimension bien definie
(Ex: un vecteur colonne de 3 neurones sera projete sur 3 lignes de 100
neurones. Chaque valeur scalaire est donc projetee sur 100
neurones). Cette projection correspond a traduire une valeur scalaire
en une distribution (position du neurone actif) des activites sur
chaque groupe de neurone (Ex: sur chaque ligne).

\section Options 

* x avec x un reel 

Le parametre du lien 'x' doit etre le max de la dynamique de cette
grandeur de maniere a la ramener sur le bon intervalle. Par exemple si
le scalaire peut varier entre 0 et 1, on prendra 1 comme
parametre. Dans ce cas, si la valeur d'entree est 0.5, le neurone au
milieu de la population sera actif et les autres seront a 0.


\defgroup f_extract_multi_vals_to_vectors_non_circular f_extract_multi_vals_to_vectors_non_circular
\ingroup libIO_Robot
\brief Concatenation de plusieurs f_extract_val_to_vector_non_circular dans un seul groupe.

\details

\section Description
On considere un vector de scalaire que l'on va projeter sur un
ensemble de population de neurone selon une dimension bien definie
(Ex: un vecteur colonne de 3 neurones sera projete sur 3 lignes de 100
neurones. Chaque valeur scalaire est donc projetee sur 100
neurones). Cette projection correspond a traduire une valeur scalaire
en une distribution (position du neurone actif) des activites sur
chaque groupe de neurone (Ex: sur chaque ligne).

\section Options 

* x avec x un reel 

Le parametre du lien 'x' doit etre le max de la dynamique de cette
grandeur de maniere a la ramener sur le bon intervalle. Par exemple si
le scalaire peut varier entre 0 et 1, on prendra 1 comme
parametre. Dans ce cas, si la valeur d'entree est 0.5, le neurone au
milieu de la population sera actif et les autres seront a 0.

\file f_extract_multi_vals_to_vectors.c
\ingroup f_extract_multi_vals_to_vectors_circular
\ingroup f_extract_multi_vals_to_vectors_non_circular
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A. de RENGERVE
- description: specific file creation
- date: 11/08/2010

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
  voir module libIO_Robot/f_extract_multi_vals_to_vectors


Macro:
-none 

Local variables:
-none

Global variables:
-discr ?

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdlib.h>
#include "tools/include/local_var.h"
#include "Kernel_Function/find_input_link.h"

/*#define DEBUG*/

#include <net_message_debug_dist.h>

struct this_data
{
  float max;
  int groupe_entree;
  int incr_neurons;
  int incr_groups;
  int nb_neurons;
  int nb_groups;
};

void function_extract_multi_vals_to_vectors(int gpe)
{
  EXIT_ON_ERROR("In (%s) : Pour correspondre aux fonctions function_extract_val_to_vector_circular (fonction originelle) ou function_extract_val_to_vector_circular_non_circular (fonction de mickael maillard) il faut maintenant choisir entre les deux versions multi\n",def_groupe[gpe].no_name); 
}

/* correct selon le modele non_circular, non teste directement !!*/
void function_extract_multi_vals_to_vectors_circular(int gpe)
{
  int no, deb, deb_entree, nbre, nbre_entree, i = -1, j=-1, groupe_entree;
  float max;
  struct this_data *_this_data = NULL;
  int sx, sy;
  int pos;
  int incr_neurons,incr_groups,nb_neurons,nb_groups;
  
  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;
  
  
  dprints("function_extract_multi_val_to_vector (%s) \n", def_groupe[gpe].no_name);
  
  if (def_groupe[gpe].data == NULL)
    {
      
      i = find_input_link(gpe, 0);
      if (i == -1)
        {
	  EXIT_ON_ERROR("ERROR the group f_extract_multi_vals_to_vectors_circular must have a link (%s)\n",def_groupe[gpe].no_name);
        }
      
      sscanf(liaison[i].nom, "%f", &max);
      if (max < 0.)
        {
	  EXIT_ON_ERROR("ERROR the group f_extract_multi_vals_to_vectors_circular must have a link with float informations (max value)\nThe current name of the link is %s \n", liaison[i].nom);
        }
      
      groupe_entree = liaison[i].depart;
      
      sx = def_groupe[gpe].taillex;
      sy = def_groupe[gpe].tailley;
      
      if(def_groupe[groupe_entree].tailley==1 && sx==def_groupe[groupe_entree].taillex) {
	/* groupe de colonnes en sortie */
	incr_neurons = sx;
	incr_groups = 1;
	nb_neurons=sy;
	nb_groups=sx;
      }
      else if(def_groupe[groupe_entree].taillex==1 && sy==def_groupe[groupe_entree].tailley) {
	/* groupe de lignes en sortie */
	incr_neurons = 1;
	incr_groups = sx;
	nb_neurons=sx;
	nb_groups=sy;
      }
      else {
	/* groupe considere comme un seul boc */
	cprints("In (%s) : ATTENTION : mode classique pris en compte avec p=0\n", def_groupe[gpe].no_name);
	incr_neurons = 1;
	incr_groups = sx*sy;
	nb_neurons=sx*sy;
	nb_groups=1;
      }
      
      def_groupe[gpe].data = malloc(sizeof(struct this_data));
      _this_data = (struct this_data *) def_groupe[gpe].data;
      if (_this_data == NULL)
        {
	  EXIT_ON_ERROR("Erreur d'allocation memoire dans f_extract_multi_vals_to_vectors_circular Gpe %s\n",def_groupe[gpe].no_name);
        }
      _this_data->max = max;
      _this_data->groupe_entree = groupe_entree;
      _this_data->incr_neurons=incr_neurons;
      _this_data->incr_groups=incr_groups;
      _this_data->nb_neurons=nb_neurons;
      _this_data->nb_groups=nb_groups;
    }
  else
    {
      _this_data = (struct this_data *) def_groupe[gpe].data;
      max = _this_data->max;
      groupe_entree = _this_data->groupe_entree;
      incr_neurons = _this_data->incr_neurons;
      incr_groups = _this_data->incr_groups;
      nb_neurons = _this_data->nb_neurons;
      nb_groups = _this_data->nb_groups;
    }
  
  
  deb_entree = def_groupe[groupe_entree].premier_ele;
  nbre_entree = def_groupe[groupe_entree].nbre;

  dprints("incr_neurons %d, nb_neurons %d, incr_groups %d, nb_groups %d\n");
  
  /* pour chaque groupe*/
  for(j=0; j<nb_groups; j++) {
    
    
    for (i = 0; i < nb_neurons; i++)
      {
	pos = deb + j*incr_groups + i*incr_neurons; 
	neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = 0.;
      }                           /*reset group activity */
    
    pos= deb_entree + j;
    no = (int) ((float) nb_neurons * (neurone[pos].s1 / max)); 
    
    /*Si on est sur le max, alors c'est le dernier neurone qui s'active*/
    if (no==nb_neurons)
      no=nb_neurons-1;
    
    if (no > nb_neurons)
      no -= nb_neurons;
    
    if (no < 0)
      {
	no += nb_neurons;
      }
    
    /* printf("no=%d,nbre=%d,max=%f,p=%d,val=%f \n",no,nbre,max,p,neurone[deb_entree+p].s); */
    pos = deb + j*incr_groups+no*incr_neurons;
    neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = 1.;
  }
}

void function_extract_multi_vals_to_vectors_non_circular(int gpe)
{
  int no, deb, deb_entree, nbre, nbre_entree, i = -1, j=-1, groupe_entree;
  float max;
  struct this_data *_this_data = NULL;
  int sx, sy;
  int pos;
  int incr_neurons,incr_groups,nb_neurons,nb_groups;
  
  
  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;


  dprints("Entree dans function_extract_multi_vals_to_vectors_non_circular (%s) \n",def_groupe[gpe].no_name);
  
  if (def_groupe[gpe].data == NULL)
    {
      
      i = find_input_link(gpe, 0);
      if (i == -1)
        {
	  EXIT_ON_ERROR("ERROR the group f_extract_multi_vals_to_vectors_non_cicular must have a link (%s)\n",def_groupe[gpe].no_name);
        }
      
      sscanf(liaison[i].nom, "%f",&max);      if (max < 0.)
        {
	  EXIT_ON_ERROR("ERROR the group f_extract_multi_vals_to_vectors_non_circular must have a link with float informations (max value)\nThe current name of the link is %s \n", liaison[i].nom);
        }

      
      groupe_entree = liaison[i].depart;
      
      sx = def_groupe[gpe].taillex;
      sy = def_groupe[gpe].tailley;
      
      if(def_groupe[groupe_entree].tailley==1 && sx==def_groupe[groupe_entree].taillex) {
	/* groupe de colonnes en entree */
	incr_neurons = sx;
	incr_groups = 1;
	nb_neurons=sy;
	nb_groups=sx;
      }
      else if(def_groupe[groupe_entree].taillex==1 && sy==def_groupe[groupe_entree].tailley) {
	/* groupe de lignes en entree */
	incr_neurons = 1;
	incr_groups = sx;
	nb_neurons=sx;
	nb_groups=sy;
      }
      else {
	/* entree consideree comme un seul groupe */
	cprints("In (%s) : ATTENTION : mode classique pris en compte avec p=0\n", def_groupe[gpe].no_name);
	incr_neurons = 1;
	incr_groups = sx*sy;
	nb_neurons=sx*sy;
	nb_groups=1;
      }

      
      
      def_groupe[gpe].data = ALLOCATION(struct this_data);
      _this_data = (struct this_data *) def_groupe[gpe].data;
      if (_this_data == NULL)
	{
	  EXIT_ON_ERROR("Erreur d'allocation memoire dans function_extract_multi_vals_to_vectors_non_circular Gpe %s\n",def_groupe[gpe].no_name);
	}
      _this_data->max = max;
      _this_data->groupe_entree = groupe_entree;
      _this_data->incr_neurons=incr_neurons;
      _this_data->incr_groups=incr_groups;
      _this_data->nb_neurons=nb_neurons;
      _this_data->nb_groups=nb_groups;
    }
  else
    {
      _this_data = (struct this_data *) def_groupe[gpe].data;
      max = _this_data->max;
      groupe_entree = _this_data->groupe_entree;
      incr_neurons = _this_data->incr_neurons;
      incr_groups = _this_data->incr_groups;
      nb_neurons = _this_data->nb_neurons;
      nb_groups = _this_data->nb_groups;
    }
  
  
  deb_entree = def_groupe[groupe_entree].premier_ele;
  nbre_entree = def_groupe[groupe_entree].nbre;

  dprints("In (%s): incr_neurons %d, nb_neurons %d, incr_groups %d, nb_groups %d\n",def_groupe[gpe].no_name, incr_neurons, nb_neurons, incr_groups, nb_groups);
  
  /* pour chaque groupe*/
  for(j=0; j<nb_groups; j++) {
    
    for (i = 0; i < nb_neurons; i++)
      {
	pos = deb + j*incr_groups + i*incr_neurons; 
	neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = 0.;
      }                           /*reset group activity */
    
    pos= deb_entree + j;
    
    no = (int) ((float) (nb_neurons-1) * (neurone[pos].s1 / max)); 
    
    if (no < 0 || no >= nb_neurons)
      {
	return;
      }
    
    /* printf("no=%d,nbre=%d,max=%f,val=%f \n",no,nbre,max,neurone[pos].s1);*/
    pos = deb + j*incr_groups+no*incr_neurons;
    neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = 1.;
  }
}
