/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_mesure_planif f_mesure_planif
 

\section Modified 
-Name: C.Giovannangeli
-Created: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description



\section Macro
-none

\section Local varirables
-none


\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include "tools/include/local_struct.h"
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

void function_mesure_planif(int numero)
{
	int gpe_monitor=-1,gpe_image_ta=-1,i,j,l,k,indmax;
	float vrai_angle,learned_angle;
	float dep_x=-1, dep_y=-1, arr_y=-1,arr_x=-1;
	float max;
	float mesure=0.;
	prom_images_struct * image_ta;
	MyData_f_monitor_planif * mydata;
	type_coeff*coeff;
	int cpt;

	if(def_groupe[numero].nbre!=4)
	{
	   fprintf(stderr, "nbre de neurone errone dans f_mesure_planif(%d)\n",numero);
	   exit(0);
	}

	i=0;
	l=find_input_link(numero,i);
	while(l!=-1)
	{
		if(strcmp(liaison[l].nom,"f_monitor_planif")==0)
		{
			gpe_monitor=liaison[l].depart;
		}
		if(strcmp(liaison[l].nom,"image_ta")==0)
		{
			gpe_image_ta=liaison[l].depart;
		}	
		i++;
		l=find_input_link(numero,i);
	}
	
	if(gpe_monitor==-1||gpe_image_ta==-1)
	{
	   fprintf(stderr, "f_mesure_planif: erreur de cablage\n");
	   exit(0);
	}

	image_ta=(prom_images_struct*)def_groupe[gpe_image_ta].ext;
	mydata=(MyData_f_monitor_planif*)(def_groupe[gpe_monitor].data);
	cpt=0;
	for(i=0;i<mydata->nbr_de_trans;i++)
	{	
#ifdef DEBUG
		dprints("=======\n");
		dprints("trans:%d\n",i);
#endif
		if(mydata->trans[mydata->coded_trans[i]].autotrans==1)
		{
			vrai_angle=-1;
			learned_angle=-1;
#ifdef DEBUG
			dprints("AUTO\n");
#endif
		}
		else
		{
			cpt++;
			dep_x=mydata->lieu[mydata->trans[mydata->coded_trans[i]].depart].x;
			dep_y=mydata->lieu[mydata->trans[mydata->coded_trans[i]].depart].y;
			arr_x=mydata->lieu[mydata->trans[mydata->coded_trans[i]].arrive].x;
			arr_y=mydata->lieu[mydata->trans[mydata->coded_trans[i]].arrive].y;	
			vrai_angle=atan2(arr_y-dep_y,arr_x-dep_x);
			
			max=0.;
			indmax=-1000;
			
			for (j = 0; j < (int)image_ta->sx; j++)
			{
				coeff = neurone[def_groupe[gpe_image_ta].premier_ele + j].coeff;
			
				for (k = 0;k <(int)image_ta->sy/*trans*/; k++)
				{
					if (coeff->evolution == 0)
						coeff = coeff->s;
					if (coeff->evolution == 1 && /*si c bonne trans*/k==mydata->coded_trans[i])
						{
							if(coeff->val>max)
							{
								max=coeff->val;
								indmax=j;
							}
									
						}
						
					coeff = coeff->s;	
				}
			}
#ifdef DEBUG
			dprints("indmax=%d\n",indmax);
#endif
			learned_angle=((float)indmax / (float) (image_ta->sx) )* 2*M_PI - M_PI;
		}
		mesure=mesure+fabs(vrai_angle-learned_angle);
#ifdef DEBUG
		dprints("dx=%f dy=%f\nax=%f ay=%f\n",dep_x,dep_y,arr_x,arr_y);
		dprints("vrai_angle=%f\n",vrai_angle);
		dprints("angle_appris=%f\n",learned_angle);
		dprints("=======\n");
#endif
		
	}
#ifdef DEBUG
	dprints("nb_trans_mes=%d\nmesure=%f\n",cpt,mesure);
#endif
	
	if(cpt!=0)
	{	
	neurone[def_groupe[numero].premier_ele  ].s=
	neurone[def_groupe[numero].premier_ele  ].s1=
	neurone[def_groupe[numero].premier_ele  ].s2=mesure/cpt;
	}
	else
	{
	neurone[def_groupe[numero].premier_ele].s=
	neurone[def_groupe[numero].premier_ele].s1=
	neurone[def_groupe[numero].premier_ele].s2=0.;
	}
	neurone[def_groupe[numero].premier_ele+ 1].s=
	neurone[def_groupe[numero].premier_ele+ 1].s1=
	neurone[def_groupe[numero].premier_ele+ 1].s2=cpt;
	
	neurone[def_groupe[numero].premier_ele+ 2].s=
	neurone[def_groupe[numero].premier_ele+ 2].s1=
	neurone[def_groupe[numero].premier_ele+ 2].s2=mesure;
	
	neurone[def_groupe[numero].premier_ele+ 3].s=
	neurone[def_groupe[numero].premier_ele+ 3].s1=
	neurone[def_groupe[numero].premier_ele+ 3].s2=mydata->nbr_de_lieu;

}

