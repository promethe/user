/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_fin_planif f_fin_planif
 

\section Modified 
-Name: C.Giovannangeli
-Created: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description



\section Macro
-none

\section Local varirables
-none


\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libx.h>
#include "tools/include/save_debug.h"
#include "tools/include/local_struct.h"
#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>

void function_fin_planif(int numero)
{
   int gpe_monitor = -1, i, l;
   MyData_f_monitor_planif * mydata;
   FILE * fp;

   i = 0;
   l = find_input_link(numero, i);
   while (l != -1)
   {
      if (strcmp(liaison[l].nom, "f_monitor_planif") == 0)
      {
	 gpe_monitor = liaison[l].depart;
      }	

      i++;
      l = find_input_link(numero,i);
   }
   /*init_obstacle();
     _initialisation = 1;
     first_init = 1;*/

   /*sauvegarde lieu, graphe des transition, carte_co*/
   if (gpe_monitor != -1)
   {
      mydata = (MyData_f_monitor_planif *) def_groupe[gpe_monitor].data;
      save_transitions_monitor_planif(mydata);
      save_robot_position_monitor_planif(mydata);
      save_resources_monitor_planif(mydata);
   }
   
  (void) fp;
}
