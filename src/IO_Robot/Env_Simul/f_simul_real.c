/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_simul_real f_simul_real
\brief 
\section Author
Name: XXXXXXXXX
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
  Nouvelle fonction qui permet de faire une simulation 
  en reel, capture d'image                            
  Demande le niveau de vigilence

\section Macro
-none 

\section Local variables
-char nom_image[256]
-int start

\section Global variables
-int learn_NN
-int NbrFeaturesPoints

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: none
- description: none
- input expected group: Image of real point
- where are the data?: in the image to convert

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <string.h>
#include <stdio.h>
#include <libx.h>

#include <Global_Var/NN_IO.h>
#include <Global_Var/IO_Robot.h>

void function_simul_real(int gpe_sortie)
{

    char nom_image[256];
    int start = 0;

    int i /*,j */ , n, deb;
    /*   static char nf[80], command[256]; */
    /*    static FILE *f; */
    /*   char nom_batch[20],stmp[100],*string,*string2; */
    /*   float angle=0.0; */

    cprints("------------------------------------------------------------\n");
    cprints("                   Function simul_real\n");

    deb = def_groupe[gpe_sortie].premier_ele;

  /*----------------------------*/
    /*Reset des valeurs de neurone */
    for (n = 0; n < def_groupe[gpe_sortie].nbre; n++)
        neurone[deb + n].s = neurone[deb + n].s1 = neurone[deb + n].s2 = 0.;


  /*------------------------------*/
    /*Compte le nombre de ptc actuel */
    NbrFeaturesPoints = 0;

    i = 0;
    strcpy(nom_image, "image.lena");


/*
  printf("Boussole donne l'angle %f\n",AngleBoussole());
  printf("De combien de degres voulez-vous tourner le robot?\n");
  scanf("%f",&angle);
  printf("tourner de %f\n",angle);
  turn_right(angle);
  printf("=> Boussole donne l'angle %f\n",AngleBoussole());
  TxEffacerAireDessin(&image2);
*/

    /*fflush(stdout);
       fflush(stdin); */
    cprints("\nAPPRENTISSAGE :1 TEST:0 :\n");
    
    if(scanf("%d", &i)){
    /*fflush(stdin); */

		if (i == 1)
		{
		  /*------------------------*/
			/*apprentissage de l'objet */
			learn_NN = 1;
			vigilence = 1.0;        /*se regle dans le fichier config */
			cprints("Entrez la vigilence APPRENTISSAGE:\n");
			if(scanf("%f", &vigilence))
				{cprints("APPRENTISSAGE(vigilance=%3.2f du config)\n", vigilence);}
			else {dprints("Erreur lecture (scanf) vigilence apprentissage dans %d",def_groupe[gpe_sortie].no_name);}
			start++;


		}
		else
		{
		  /*----------------------*/
			/*test de reconnaissance */
			learn_NN = 0;
			vigilence = 0;
			cprints("Entrez la vigilence TEST:\n");
			if(scanf("%f", &vigilence))
				{cprints("TEST de reconnaissance(vigilance=%3.2f fixe)\n", vigilence);}
			else {dprints("Erreur lecture (scanf) vigilence test dans %d",def_groupe[gpe_sortie].no_name);}
			start++;

		}}
		else
		{EXIT_ON_ERROR("Erreur de lecture (scanf) type fonctionnement %d",def_groupe[gpe_sortie].no_name);}
    /*  do{
       printf("Quel numero de neurone vous voulez forcer? 0 a %d (-1 break)\n",def_groupe[gpe_sortie].nbre-1);
       scanf("%d",&n);

       if(n<0.)
       break;
       if(n<def_groupe[gpe_sortie].nbre)
       neurone[deb+n].s= neurone[deb+n].s1= neurone[deb+n].s2=1.;
       else
       printf("Il n'y a que %d neurone sur ce groupe %d, RECOMMENCEZ\n"
       ,def_groupe[gpe_sortie].nbre,gpe_sortie);

       }while(n>=def_groupe[gpe_sortie].nbre); */

/*  fflush(stdout);
  fflush(stdin);
  printf("Donnez un nom a l'objet courant: \n");
  fflush(stdin);
  scanf("%s",vrai_nom_image);
  fflush(stdout);
  strcpy( vrai_nom_image_lena, vrai_nom_image);
  strcat( vrai_nom_image_lena, ".lena");
*/
    start = 0;


    NbrFeaturesPoints = 0;

    cprints("----------------------------------------------------------\n");

}
