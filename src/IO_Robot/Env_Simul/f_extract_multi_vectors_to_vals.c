/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup f_extract_multi_vectors_to_vals  f_extract_multi_vectors_to_vals 
\ingroup libIO_Robot

\brief Concatenation de plusieurs f_extract_vector_to_val dans un seul groupe.

\details

\section Description

On considere un ensemble de population de neurone selon une dimension
bien definie (Ex: 3 lignes de 100 neurones). Cette fonction permet de
traduire les distributions (position du max) des activites sur chaque
groupe de neurone (Ex: chaque ligne) en une valeur analogique dans un
vecteur correspondant (Dans l'exemple : on passerait de 3 lignes de
100 neurones a un vecteur en colonne de 3 neurones chacun codant pour
la ligne qui correspondait.

\section Options 

* full_range    (const string)

"full_range" : 
 - si present : valeur scalaire comprise entre 0 et 1-1/N
 - si absent : valeur scalaire comprise entre 1/N et 1


\file f_extract_multi_vectors_to_vals.c
\ingroup f_extract_multi_vectors_to_vals
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A. de RENGERVE
- description: specific file creation
- date: 11/08/2010

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
  voir module libIO_Robot/f_extract_multi_vectors_to_vals


Macro:
-none 

Local variables:
-none

Global variables:
-discr ?

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools/include/local_var.h"
#include "Kernel_Function/find_input_link.h"

/*#define DEBUG*/

#include <net_message_debug_dist.h>

struct this_data
{
  int groupe_entree;
  int full_range;
  int incr_neurons;
  int incr_groups;
  int nb_neurons;
  int nb_groups;
};

void function_extract_multi_vectors_to_vals(int gpe)
{
  int  deb, deb_entree, nbre, nbre_entree, i = -1, j=-1, groupe_entree;
  float max;
  struct this_data *_this_data = NULL;
  int i_max;
  int full_range = 0;
  int sx, sy;
  int pos;
  int incr_neurons,incr_groups,nb_neurons,nb_groups;

  (void) nbre; // (unused)

  deb = def_groupe[gpe].premier_ele;
  nbre = def_groupe[gpe].nbre;


  dprints("function_extract_multi_vectors_to_vals \n");

  if (def_groupe[gpe].data == NULL)
  {

    i = find_input_link(gpe, 0);
    if (i == -1)
      EXIT_ON_ERROR("ERROR the group f_extract_vector_to_val must have a link \n");


    if (strcmp(liaison[i].nom, "full_range") == 0)
    full_range = 1;

    groupe_entree = liaison[i].depart;

    sx = def_groupe[gpe].taillex;
    sy = def_groupe[gpe].tailley;

    if(sy==1 && sx==def_groupe[groupe_entree].taillex)
    {
      /* groupe de colonnes en entree */
      incr_neurons = def_groupe[groupe_entree].taillex;
      incr_groups = 1;
      nb_neurons=def_groupe[groupe_entree].tailley;
      nb_groups=sx;
    }
    else if(sx==1 && sy==def_groupe[groupe_entree].tailley)
    {
      /* groupe de lignes en entree */
      incr_neurons = 1;
      incr_groups = def_groupe[groupe_entree].taillex;
      nb_neurons=def_groupe[groupe_entree].taillex;
      nb_groups=sy;
    }
    else
    {
      /* entree consideree comme un seul groupe */
      printf("Input considered as a unique group\n");
      incr_neurons = 1;
      incr_groups = def_groupe[groupe_entree].nbre;
      nb_neurons=def_groupe[groupe_entree].nbre;
      nb_groups=1;
    }

    def_groupe[gpe].data = ALLOCATION(struct this_data);
    _this_data = (struct this_data *) def_groupe[gpe].data;
    if (_this_data == NULL)
      EXIT_ON_ERROR("Erreur d'allocation memoire dans function_extract_vector_to_val Gpe %s\n",def_groupe[gpe].no_name);
      
    _this_data->groupe_entree = groupe_entree;
    _this_data->full_range = full_range;
    _this_data->incr_neurons=incr_neurons;
    _this_data->incr_groups=incr_groups;
    _this_data->nb_neurons=nb_neurons;
    _this_data->nb_groups=nb_groups;
  }
  else
  {
    _this_data = (struct this_data *) def_groupe[gpe].data;
    groupe_entree = _this_data->groupe_entree;
    full_range = _this_data->full_range;
    incr_neurons = _this_data->incr_neurons;
    incr_groups = _this_data->incr_groups;
    nb_neurons = _this_data->nb_neurons;
    nb_groups = _this_data->nb_groups;
  }

  dprints("incr_neurons %d, nb_neurons %d, incr_groups %d, nb_groups %d\n");    

  deb_entree = def_groupe[groupe_entree].premier_ele;
  nbre_entree = def_groupe[groupe_entree].nbre;


  /* pour chaque groupe*/
  for(j=0; j<nb_groups; j++)
  {

    /* cherche le max sur le groupe */

    max = 0.;
    i_max = -1;
    for (i = 0; i < nb_neurons; i++)
    {
      pos = deb_entree + j*incr_groups + i*incr_neurons; 
      /*    printf("pos %d, val %f\n", pos,neurone[pos].s1);*/
      if (neurone[pos].s1 > max)
      {
        max = neurone[pos].s1;
        i_max = i;
      }
    }

    /*      printf("j %d, max %f\n",j,max);*/

    pos = deb+ j; /* il n'y a qu'un vecteur (ligne ou colonne) pas d'ambiguite sur pos*/
    if (i_max != -1)
    {
      if (full_range == 0)
        neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = ((float) (i_max + 1) / nb_neurons);
      else
      {
        if (nbre_entree > 1)
          neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = ((float) (i_max) / (nb_neurons - 1));
        else
          neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = 0.5;
      }
    }
    else
      neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = -1.;
  }
}
