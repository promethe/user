/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file  
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Simulation of the sequential perception of distant landmarks             
   possible problem when the system cannot analyze the whole set
   of landmarks. Not enough iterations... 

   Problem solved. The function refuses to take into account more
landmarks than the number of iterations at the function time scale.
Yet, this functioning mode induces a particular bias in the case of hidden landmarks
(the first landmarks are prefered  but it is not a big issue in a simple simulation).
PG.

Macro:
-none 

Local variables:
-float posx
-float posy
-int NBLAND
-float ** table_land

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include "tools/include/local_var.h"
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#define PI 	3.1415

   /* tableau normalement non visible du sequenceur du kernel de promethe... */
extern int echelle_temps[];

typedef struct MyData_f_sequential_perception
{
    int gpe_azimut_robot;
} MyData_f_sequential_perception;

void function_sequential_perception(int gpe)
{
    float angle1 = 0.;
    int deb, nbre;
    float b, c;
    static int nb_of_tested_landmark = 0;   /* need of several loops before a complete exploration */
    MyData_f_sequential_perception *my_data;
    int gpe_azimut_robot = 0, i = 0;
    int l = 0, neur_orient = 0;
    float orientation = 0., max = -1., erreur, angle_miroir;
    float max_err_miroir = 10. * 2. * pi / 360;

    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
#ifdef DEBUG
    printf("debut f_sequential_perception\n");
#endif

    if (def_groupe[gpe].data == NULL)
    {
        i = 0;
        l = find_input_link(gpe, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "azimut_robot") == 0)
            {
                gpe_azimut_robot = liaison[l].depart;
            }
            i++;
            l = find_input_link(gpe, i);

        }
        my_data =
            (MyData_f_sequential_perception *)
            malloc(sizeof(MyData_f_sequential_perception));
        my_data->gpe_azimut_robot = gpe_azimut_robot;
        def_groupe[gpe].data = (MyData_f_sequential_perception *) my_data;
    }
    else
    {
        my_data = (MyData_f_sequential_perception *) (def_groupe[gpe].data);
        gpe_azimut_robot = my_data->gpe_azimut_robot;
    }

    if (gpe_azimut_robot != 0)
    {
        max = -1;
        neur_orient = -1;
        for (i = def_groupe[gpe_azimut_robot].premier_ele;
             i <
             def_groupe[gpe_azimut_robot].nbre +
             def_groupe[gpe_azimut_robot].premier_ele; i++)
        {
            if (neurone[i].s1 > max)
            {
                max = neurone[i].s1;
                neur_orient = i - def_groupe[gpe_azimut_robot].premier_ele;
            }
        }
        orientation =
            ((float) neur_orient /
             (float) def_groupe[gpe_azimut_robot].nbre) * 2 * pi;
        printf("orinetation:%f\n", orientation);
    }

    if (nbre != 2)
    {
        printf
            ("ERROR the group f_sequential_perception must have 2 neurons (landmark id and azimuth) \n");
        exit(0);
    }

    while (isequal(table_land[nb_of_tested_landmark][2], 0))
    {
        /*printf("sequential perception %d \n",nb_of_tested_landmark); */
        nb_of_tested_landmark = nb_of_tested_landmark + 1;
        if (nb_of_tested_landmark >= NBLAND)
            nb_of_tested_landmark = 0;

    }                           /*cette boucle sert a tester si le landmark est visible a passer au landmark suivant dans le cas contraire et revenir a zero quand on les a tous teste */

    /* printf("sequential perception OK %d \n",nb_of_tested_landmark); */

    if (isequal(table_land[nb_of_tested_landmark][2], 1.)) /*landmark visible */
    {
        b = table_land[nb_of_tested_landmark][0] - posx;    /*distance sur l'axe x entre la position du robot et le landmark */
        c = table_land[nb_of_tested_landmark][1] - posy;    /*distance sur l'axe y entre la position du robot et le landmark */
        c = -c;
        if (isequal(b, 0.) && isequal(c, 0.))
            angle1 = 0.;
        else
            angle1 = atan2(c, b);

        if (angle1 < 0)
            angle1 = angle1 + 2. * pi;
        /* printf("landmark %d , b=%f, c=%f, angle abs=%f \n",nb_of_tested_landmark,b,c,angle1); */
    }

    if (gpe_azimut_robot != 0)
    {
        angle_miroir = angle1 - orientation;

        if (angle_miroir < 0)
            angle_miroir = angle_miroir + 2. * pi;

        erreur = max_err_miroir * (cos(angle_miroir) - 1) / 2.;

        printf("erreur en deg:%f\n", erreur * 360. / 2. / pi);
        angle1 = angle1 + erreur;

        if (angle1 < 0)
            angle1 = angle1 + 2. * pi;
        if (angle1 >= 2. * pi)
            angle1 = angle1 - 2. * pi;
    }

    neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = (float) nb_of_tested_landmark; /* landmark id */
    neurone[deb + 1].s = neurone[deb + 1].s1 = neurone[deb + 1].s2 = angle1;    /* landmark azimuth */
#ifdef DEBUG
    printf("%s: \n\tid landmark:%f\n\tangle land:%f\n", __FUNCTION__,
           (float) nb_of_tested_landmark, angle1);
#endif


    nb_of_tested_landmark = nb_of_tested_landmark + 1;
    if (nb_of_tested_landmark >= NBLAND
        || nb_of_tested_landmark >= echelle_temps[def_groupe[gpe].ech_temps])
        nb_of_tested_landmark = 0;

#ifdef DEBUG
    printf("fin f_sequential_perception\n");
#endif


}
