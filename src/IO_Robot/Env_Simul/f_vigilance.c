/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_vigilance.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 23/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   	Gere le changement du niveau de vigilance en fonction
de l'activite des neurones en entree.
Les entrees sont des groupes de neurones contenant en general
1 neurone (possibilite d'en mettre plus).

	Le groupe vigilance est une fonction
a seuil appliquee a chaque entree. 
La valeur du lien correspond au seuil de declenchement
de la fonction.

	Si une entrees active le dirac alors il y a un dirac
de vigilance en sortie sinon la vig reste a son niveau
de base (lu dans le fichier de config).

 	* Ajout Nizar : 
On a un seuil de vigilance sur l'activit�globale de DG (lu dans le script),ce seuil si ce seuil est d�ass�par en dessous) alors la vigilance de reconnaisssancede lieux estmise �1. 
En temps normal cette vigilance de reconnaissance reste �la valeur indiqu�dans le config.
Pour un lieu donn�son activit�va aller de 1 jusqu'au niveau de vigilance du config. 
Quand on d�asse ce niveau dans un endroit inconnu, l'activit�globale de DG diminue et donc entraine une vigilance haute (1) sur la vigilance de reconnaissance.
Ceci cr� donc un nouveau lieu dont le centre �une activit��1.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/local_var.h"

/*#define DEBUG*/
void function_vigilance(int numero)
{
    type_coeff *coeff;
    int deb;
    int no_lien;
    float input;
    float seuil;
    /*ligne pour CG: vigilance_de_base=0.01; */

    deb = def_groupe[numero].premier_ele;
#ifdef DEBUG
    printf
        ("-----------------------------------%d mise a jour vigilance %f \n",
         numero, vigilence);
#endif
    if (vigilance_de_base < 0)
    {
        vigilance_de_base = vigilence;
#ifdef DEBUG
        printf("vigilance_de_base = %f \n", vigilance_de_base);
#endif
    }

    no_lien = find_input_link(numero, 0);
    seuil = liaison[no_lien].norme;
#ifdef DEBUG
    printf("seuil pour d�lenchement vigilence: %f\n", seuil);
#endif

    vigilence = vigilance_de_base;
    /*printf("vigilance_de_base = %f \n",vigilance_de_base); */

    coeff = neurone[deb].coeff;
    while (coeff != NULL)
    {
        input = neurone[coeff->entree].s1 /*coeff->moy */ ;
#ifdef DEBUG
        printf("input= %f , input-vig = %f , seuil = %f \n", input,
               input - vigilence, seuil);
#endif
        if (input /*-vigilence */  < seuil /*coeff->val */ )
        {
            vigilence = 0.99;
            break;
        }
        else
        {
            vigilance_de_base = -1;
        }
        coeff = coeff->s;
    }

#ifdef DEBUG
    printf("VIGILENCE=%f\n", vigilence);
#endif
    neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = vigilence;

}
