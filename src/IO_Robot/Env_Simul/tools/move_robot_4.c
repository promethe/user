/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include "include/local_var.h"
#include "include/mise_a_echelle_cases_vers_pix.h"
#include "include/rencontre_tout.h"
#include "include/arrange_echelle.h"
#include "include/affiche_landmarks.h"
#include "include/affiche_ptessentiel.h"
#include "include/affiche_eq.h"
#include "include/checker_landmarks_visibles.h"
#include "include/affiche_robot.h"
#include "include/affiche_obstacle.h"
#include "include/affiche_temps.h"
#include "include/rencontre_quel_mur.h"
#include "include/rencontre_landmark.h"
#include <stdlib.h>
#include <stdio.h>
#include <libx.h>

#include <math.h>
#include "reseau.h" 

/*#define DEBUG*/
void evitement_4(int *check, float *x, float *y, float *dirx_mem, float *diry_mem, float *dirx, float *diry, int *s, int *compt, int *tt, float dx, float dy);

/*-------------------------------------*/
/*Evitement d'obstacle a la BRAITENBERG*/
/*par suivi de mur                     */
/*-------------------------------------*/
void evitement_4(int *check, float *x, float *y, float *dirx_mem, float *diry_mem, float *dirx, float *diry, int *s, int *compt, int *tt, float dx, float dy)
{
  float tab[5] =
    { -1., -1., -1., -1., -1. }, xx, yy, xx_org = 0., yy_org = 0., pas_avancement;
  int i;
  int temps_evitement = 40;
  pas_avancement = pas;

  cprints("Suivi dobstacle\n");
  (*tt) = (*tt) + 1;
  (*dirx_mem) = dx;
  (*diry_mem) = dy;
  cprints("dddxx_org:%f,yy_org:%f, dirx_mem:%f, diry_mem:%f,tab[2]:%f\n", xx_org, yy_org, *dirx_mem, *diry_mem, tab[2]);
  if ((*check) == 0)
  {
    cprints("=>Debut evitement\n");
    (*check) = 1;
    if (isdiff((*dirx), 0) && isdiff((*diry), 0))
    { /*pas de mvment en diag */
      /*	printf ("pas de mvment en diag\n");*/
      /*if(drand48()>.5)(*dirx)=0;
       else (*diry)=0; */

      /*	printf ("suivi a D\n");*/

      if (*dirx > *diry)
      {
        *diry = 0;
        *dirx = fabs(*dirx) / (*dirx);
      }
      else
      {
        *dirx = 0;
        *diry = fabs(*diry) / (*diry);
      }
    }
    if (drand48() > .5) /* suivi tire au hazard a G ou a D */
    {
      (*s) = -1;
      /*	printf ("suivi a G\n");*/
    }
    else
    {
      (*s) = 1;
    }
    /*
     xx = (*x) + pas_avancement * (*dirx);
     yy = (*y) + pas_avancement * (*diry);*/

    /*	(*dirx_mem) = (*dirx);
     (*diry_mem) = (*diry);*/

    /* if ( rencontre_tout(xx,yy)==0 )
     {
     (*check)=0;     printf("=>Fin de l'evitement\n\n");
     return;
     } */
  }
  for (i = 0; i < 5; i++)
    tab[i] = 0.;
  /*suivi horizontal*/
  /*
   0 1
   > 2
   4 3
   */
  if (isdiff((*dirx), 0.))
  {
    cprints("cas1\n");
    xx = (*x);
    yy = (*y) - pas_avancement * (*dirx);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[0] = 1.;

    xx = (*x) + pas_avancement * (*dirx);
    yy = (*y) - pas_avancement * (*dirx);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[1] = 1.;

    xx = (*x) + pas_avancement * (*dirx);
    yy = (*y);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[2] = 1.;

    xx = (*x) + pas_avancement * (*dirx);
    yy = (*y) + pas_avancement * (*dirx);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[3] = 1.;

    xx = (*x);
    yy = (*y) + pas_avancement * (*dirx);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[4] = 1.;
  }
  /*suivi vertical*/
  else
  {
    cprints("cas2\n");
    xx = (*x) + pas_avancement * (*diry);
    yy = (*y);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[0] = 1.;

    xx = (*x) + pas_avancement * (*diry);
    yy = (*y) + pas_avancement * (*diry);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[1] = 1.;

    xx = (*x);
    yy = (*y) + pas_avancement * (*diry);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[2] = 1.;

    xx = (*x) - pas_avancement * (*diry);
    yy = (*y) + pas_avancement * (*diry);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[3] = 1.;

    xx = (*x) - pas_avancement * (*diry);
    yy = (*y);
    if (rencontre_quel_mur(xx, yy) >= 1) tab[4] = 1.;
  }

  for (i = 0; i < 5; i++)
  {
    cprints("tab[%i]=%f \n", i, tab[i]);
  }

  /*obstacle present devant soi*/
  if (isequal(tab[2], 1))
  {
    /* Eviter obstacle tourner sur place */
    /*	printf ("Eviter obstacle tourner sur place\n");*/
    if (isequal((tab[0] + tab[1]), (tab[3] + tab[4])))
    {
      /*tourner sur place au hazard */
      cprints("tourner sur place au hazard\n");
      if ((*s) > .5)
      {
        /*tourner sur place vers la droite */
        if (isequal((*dirx), 0))
        {
          (*diry) = (*dirx);
          (*dirx) = 0;
        }
        else
        {
          (*dirx) = -(*diry);
          (*diry) = 0;
        }
      }
      else
      {
        /*tourner sur place vers la gauche */
        if (isdiff((*dirx), 0))
        {
          (*diry) = -(*dirx);
          (*dirx) = 0;
        }
        else
        {
          (*dirx) = (*diry);
          (*diry) = 0;
        }
      }
    }
    else if ((tab[0] + tab[1]) > (tab[3] + tab[4]))
    {
      /*tourner sur place vers la droite */
      cprints("tourner sur place vers la droite\n");
      if (isdiff((*dirx), 0))
      {
        (*diry) = (*dirx);
        (*dirx) = 0;
      }
      else
      {
        (*dirx) = -(*diry);
        (*diry) = 0;
      }
    }
    else
    {
      /*tourner sur place vers la gauche */
      cprints("tourner sur place vers la gauche\n");
      if (isdiff((*dirx), 0.))
      {
        (*diry) = -(*dirx);
        (*dirx) = 0;
      }
      else
      {
        (*dirx) = (*diry);
        (*diry) = 0;
      }
    }
  }
  /*pas d'obstacle devant soi mais direction initialement choisi obstruée par obstacle, suivi... */
  else
  {
    /*verification si l'on peut utiliser la direction demandée...*/
    if ((*dirx_mem) > (*diry_mem))
    {
      xx_org = (*x) + pas_avancement * (*dirx_mem);
      yy_org = (*y) + pas_avancement * (*diry_mem);
      if (rencontre_quel_mur(xx_org, yy_org) == 0) tab[2] = 0.;
      else tab[2] = 1;
    }
    else
    {
      yy_org = (*y) + pas_avancement * (*diry_mem);
      xx_org = (*x) + pas_avancement * (*dirx_mem);
      if (rencontre_quel_mur(xx_org, yy_org) == 0) tab[2] = 0.;
      else tab[2] = 1;
    }
    cprints("xx_org:%f,yy_org:%f, dirx_mem:%f, diry_mem:%f,tab[2]:%f\n", xx_org, yy_org, *dirx_mem, *diry_mem, tab[2]);

    /*suivi selon s(G ou D) si dir d'origine encore obstruee */
    if (isequal(tab[2], 1.))
    {
      if ((*s) == -1) /* suivi a G */
      {
        /*	printf ("suivi selon s(G)\n");
         if ((tab[0] == 0) && (tab[1] == 0))
         {
         *tourner sur place vers la G *
         printf ("tourner sur place vers la G\n");
         if ((*dirx) != 0)
         {
         (*diry) = -(*dirx);
         (*dirx) = 0;
         }
         else
         {
         (*dirx) = (*diry);
         (*diry) = 0;
         }
         }
         else
         {*/
        /*avancer d'un pas */
        /*printf ("avancer d'un pas\n");*/
        (*x) = (*x) + pas_avancement * (*dirx);
        (*y) = (*y) + pas_avancement * (*diry);
        (*x) = arrange_echelle(*x);
        (*y) = arrange_echelle(*y);
        /*}*/
      }
      else /* suivi a D */
      {
        /*printf ("suivi selon s(D)\n");*/
        /*	if ((tab[4] == 0) && (tab[3] == 0))
         {
         *tourner sur place vers la D *
         printf ("tourner sur place vers la D\n");
         if ((*dirx) != 0)
         {
         (*diry) = (*dirx);
         (*dirx) = 0;
         }
         else
         {
         (*dirx) = -(*diry);
         (*diry) = 0;
         }
         }
         else
         {*/
        /*avancer d'un pas *//*printf ("avancer d'un pas\n");*/
        (*x) = (*x) + pas_avancement * (*dirx);
        (*y) = (*y) + pas_avancement * (*diry);
        (*x) = arrange_echelle(*x);
        (*y) = arrange_echelle(*y);
        /*}*/
      }
    }

  }
  /*if( (*dirx_mem)==(*dirx) && (*diry_mem)==(*diry)  )
   {
   (*compt) = (*compt)+1;
   } */
  if ( /*(*compt)==2  || */(*tt) == temps_evitement || isequal(tab[2], 0.))
  {
    (*compt) = 0;
    /*Fin de l'evitement d'obstacle */
    cprints("=>Fin de l'evitement d'obstacle\n\n");
    *check = 0;
    /*avancer d'un pas */cprints("avancer d'un pas\n");
    if (isequal(tab[2], 0.))
    {
      (*x) = xx_org;
      (*y) = yy_org;
      (*x) = arrange_echelle(*x);
      (*y) = arrange_echelle(*y);
    }
    /*-------------------------------------------------*/
    /*le temps de suivi d'obstacle a sa valeur minimale */
    /*  temps_evitement=2; */
  }
  else
  {
    /*---------------------------------------*/
    /*le temps de suivi d'obstacle incremente */
    temps_evitement = temps_evitement + 1;
    /*avancer d'un pas *//*printf ("avancer d'un pas\n");*/
  }
}

void move_robot_4(float *dx, float *dy)
{
  float dx_m_prec = 0, dy_m_prec = 0;
  static int tt = 0, check3 = 0, s = -1, cpt = 0;
  static float dirx_mem, diry_mem, dirx, diry, posx_prec, posy_prec;
  TxPoint pointxt;

  float pas_avancement = pas;

  /*printf("DEPLACEMENT du robot \n");*/

  /*-----------------------------------------------------------*/
  /*     Mise en route de la marche aleatoire specifique       */
  /* si l'activite maximale des neurone ne depasse pas un seuil*/
  pointxt.x = TAILLEX + 20;
  pointxt.y = 300;

#ifndef AVEUGLE
  if (graphiques == 1)
  {
    TxEcrireChaine(&image1, lut_g[2], pointxt, (char*) "MOTOR COMMAND: random walk", 0);
    pointxt.x = TAILLEX + 15;
    pointxt.y = 288;
    TxDessinerRectangle(&image1, lut_g[2], TxVide, pointxt, 210, 15, 2);
    pointxt.x = TAILLEX + 20;
    pointxt.y = 280;
    TxEcrireChaine(&image1, blanc, pointxt, (char*) "BRAIN COMMAND: neural walk", 0);
    pointxt.x = TAILLEX + 15;
    pointxt.y = 268;
    TxDessinerRectangle(&image1, blanc, TxVide, pointxt, 210, 15, 2);
  }
#endif
  dx_mem = *dx;
  dy_mem = *dy;
  posx_prec = posx;
  posy_prec = posy;
  /*
   posx2 = posx + pas_avancement *3* (*dx);
   posy2 = posy + pas_avancement *3*(*dy);
   mirror=rencontre_quel_mur(posx2,posy2);
   if(mirror<1){
   posx2 = posx + pas_avancement *2* (*dx);
   posy2 = posy + pas_avancement *2*(*dy);
   mirror=rencontre_quel_mur(posx2,posy2);
   if(mirror<1){
   mirror=rencontre_quel_mur(posx2,posy2);
   posx2 = posx + pas_avancement * (*dx);
   posy2 = posy + pas_avancement *(*dy);
   }
   }
   */

#ifdef DEBUG
  dprints("x:%f, y:%f, dx:%f, dy:%f,x2:%f,y2:%f, obstacle:%d,check3:%d\n",posx,posy,*dx,*dy,posx2,posy2,rencontre_quel_mur(posx2,posy2),check3);
#endif
  if (mirror >= 100 && check3 == 0)
  {
    /*    if(rencontre_quel_mur(posx2,posy2)||rencontre_landmark(posx2,posy2)==1){*/
    /*check2=0;*/
    cprints("\n===================\n!!!!!!!MIRROR!!!!!!!!!!!!!!!!!!        %d\n", mirror);
    if (mirror == 1)
    {
      dirx = dx_mem;
      diry = dy_mem;
      /*on rencontre un obstacle*/
      /*debut de suivi d'obstacle*/
      /*	#ifdef DEBUG
       printf("entree ds evitement obstacle\n");
       #endif
       dirx=dx_mem; diry=dy_mem; cpt=0;tt=0;  dirx=*dx; diry=*dy;
       evitement(&check3, &posx,&posy,&dirx_mem,&diry_mem, &dirx,&diry,&s,&cpt,&tt,*dx,*dy);
       dx_mem=dirx; dy_mem=diry;
       *dx = dx_mem;
       *dy = dy_mem;
       */

      /*regarde dans les 4dir ou se trouve l'obstacle
       *dx=1;*dy=0;
       posx2 = posx + pas_avancement *3* (*dx);
       posy2 = posy + pas_avancement *3*(*dy);
       dirx=rencontre_quel_mur(posx2,posy2);
       if(dirx<1){
       posx2 = posx + pas_avancement *2* (*dx);
       posy2 = posy + pas_avancement *2*(*dy);
       dirx=rencontre_quel_mur(posx2,posy2);
       if(dirx<1){
       dirx=rencontre_quel_mur(posx2,posy2);
       posx2 = posx + pas_avancement * (*dx);
       posy2 = posy + pas_avancement *(*dy);
       }
       }


       *dx=-1;*dy=0;
       posx2 = posx + pas_avancement *3* (*dx);
       posy2 = posy + pas_avancement *3*(*dy);
       dirx_mem=rencontre_quel_mur(posx2,posy2);
       if(dirx_mem<1){
       posx2 = posx + pas_avancement *2* (*dx);
       posy2 = posy + pas_avancement *2*(*dy);
       dirx_mem=rencontre_quel_mur(posx2,posy2);
       if(dirx_mem<1){
       dirx_mem=rencontre_quel_mur(posx2,posy2);
       posx2 = posx + pas_avancement * (*dx);
       posy2 = posy + pas_avancement *(*dy);
       }
       }
       *dx=0;*dy=1;
       posx2 = posx + pas_avancement *3* (*dx);
       posy2 = posy + pas_avancement *3*(*dy);
       diry=rencontre_quel_mur(posx2,posy2);
       if(diry<1){
       posx2 = posx + pas_avancement *2* (*dx);
       posy2 = posy + pas_avancement *2*(*dy);
       diry=rencontre_quel_mur(posx2,posy2);
       if(diry<1){
       diry=rencontre_quel_mur(posx2,posy2);
       posx2 = posx + pas_avancement * (*dx);
       posy2 = posy + pas_avancement *(*dy);
       }
       }
       *dx=0;*dy=-1;
       posx2 = posx + pas_avancement *3* (*dx);
       posy2 = posy + pas_avancement *3*(*dy);
       diry_mem=rencontre_quel_mur(posx2,posy2);
       if(diry_mem<1){
       posx2 = posx + pas_avancement *2* (*dx);
       posy2 = posy + pas_avancement *2*(*dy);
       diry_mem=rencontre_quel_mur(posx2,posy2);
       if(diry_mem<1){
       diry_mem=rencontre_quel_mur(posx2,posy2);
       posx2 = posx + pas_avancement * (*dx);
       posy2 = posy + pas_avancement *(*dy);
       }
       }

       if(dirx>0)mirror=2;
       else if(dirx_mem>0)mirror=3;
       else if(diry>0)mirror=5;
       else if(diry_mem>0)mirror=4;
       else mirror=2;*/

    }
    else
    {
      /*	  posx += pas_avancement * (*dx);
       posy += pas_avancement * (*dy);*/
    }
  }
  else if (check3 == 1)
  {
#ifdef DEBUG
    cprints("poursuit ds evitement obstacle\n");
#endif
    /*-------------------------*/
    /*Suite du suivi d'obstacle*/
    evitement_4(&check3, &posx, &posy, &dirx_mem, &diry_mem, &dirx, &diry, &s, &cpt, &tt, *dx, *dy);
    dx_mem = dirx;
    dy_mem = diry;
    check2 = 0;
    *dx = dx_mem;
    *dy = dy_mem;
  }
  else
  {
#ifdef DEBUG
    cprints("Mvt normal\n");
#endif
    posx += pas_avancement * (*dx);
    posy += pas_avancement * (*dy);
  }

#ifdef DEBUG		
  dprints("x:%f, y:%f, dx:%f, dy:%f,dx_mem:%f, dy_mem:%f,posx_prec:%f,posy_prec:%f,dx_prec:%f,dy_prec:%f\n",posx,posy,*dx,*dy,dx_mem,dy_mem,posx_prec,posy_prec,dx_m_prec,dy_m_prec);
#endif

#ifndef AVEUGLE

  affiche_robot(posx_prec, posy_prec, dx_m_prec, dy_m_prec, -1);
  affiche_robot(posx, posy, dx_mem, dy_mem, 1);
  posx_prec = posx;
  posy_prec = posy;
  dx_m_prec = dx_mem;
  dy_m_prec = dy_mem;
  affiche_landmarks();
  affiche_ptessentiel();
  affiche_eq();
  /*Affichage des obstacles*/
  affiche_obstacle();
  /*afficher(texte) le temps*/
  affiche_temps();

  /*Liberer le buffer*/
  TxFlush(&image1);

#endif
  (void) dy_m_prec;
  (void) dx_m_prec;
  (void) posy_prec;
  (void) pointxt;
  (void) posx_prec;
}

