/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** 
___NO_COMMENT___
___NO_SVN___

\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N. Cuperlier
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include "include/local_var.h"
#include "include/mise_a_echelle_cases_vers_pix.h"
/*#define nav_simul_old_ways*/
#include "include/rencontre_tout.h"
#include "include/arrange_echelle.h"
#include "include/affiche_landmarks.h"
#include "include/affiche_ptessentiel.h"
#include "include/affiche_eq.h"
#include "include/checker_landmarks_visibles.h"
#include "include/affiche_robot.h"
#include "include/affiche_obstacle.h"
#include "include/affiche_temps.h"
#include "include/reconnaissance.h"
#include "include/rencontre_quel_mur.h"
#include "include/rencontre_landmark.h"
#include <stdio.h>
#include "math.h"
#include "reseau.h"
/**************************************************************************************/

/* attention turn ne devrait pas faire avancer le robot pour etre
completement compatible avec la manip sur le vrai robot
(il faudrait alors rajouter le braitenberg... ).
L'angle est en degres (direction absolue).  */

/*#define DEBUG*/

#ifndef AVEUGLE
#define AVEUGLE
#endif

/*-------------------------------------*/
/*Evitement d'obstacle a la BRAITENBERG*/
/*par suivi de mur                     */
/*-------------------------------------*/
void evitement(int *check, float *x, float *y, float *dirx_mem,
               float *diry_mem, float *dirx, float *diry, int *s, int *compt,
               int *tt)
{
    float tab[5], xx, yy, pas_avancement;
    int i;
    int temps_evitement = 5;
    pas_avancement = pas;

    /*printf("Suivi dobstacle\n"); */

    (*tt) = (*tt) + 1;

    if ((*check) == 0)
    {
        /* printf("=>Debut evitement\n"); */
        (*check) = 1;
        if (isdiff((*dirx), 0) && isdiff((*diry), 0))
        {                       /*pas de mvment en diag */
            /* printf("pas de mvment en diag\n"); */
            if (drand48() > .5)
                (*dirx) = 0;
            else
                (*diry) = 0;
            xx = (*x) + pas_avancement * (*dirx);
            yy = (*y) + pas_avancement * (*diry);
            if (rencontre_tout(xx, yy) == 0)
            {
                (*x) = xx;
                (*y) = yy;
            }
        }
        (*dirx_mem) = (*dirx);
        (*diry_mem) = (*diry);
        xx = (*x) + pas_avancement * (*dirx);
        yy = (*y) + pas_avancement * (*diry);
        if (rencontre_tout(xx, yy) == 0)
        {
            (*check) = 0;       /* printf("=>Fin de l'evitement\n\n"); */
            return;
        }
        if (drand48() > .5)     /* suivi tire au hazard a G ou a D */
        {
            (*s) = -1;          /*printf("suivi a G\n"); */
        }
        else
        {
            (*s) = 1;           /*printf("suivi a D\n"); */
        }

    }
    for (i = 0; i < 5; i++)
        tab[i] = 0.;

    if (isdiff((*dirx), 0.))
    {
        /*printf("cas1\n"); */
        xx = (*x);
        yy = (*y) - pas_avancement * (*dirx);;
        if (rencontre_tout(xx, yy) == 1)
            tab[0] = 1.;

        xx = (*x) + pas_avancement * (*dirx);
        yy = (*y) - pas_avancement * (*dirx);;
        if (rencontre_tout(xx, yy) == 1)
            tab[1] = 1.;

        xx = (*x) + pas_avancement * (*dirx);
        yy = (*y);
        if (rencontre_tout(xx, yy) == 1)
            tab[2] = 1.;

        xx = (*x) + pas_avancement * (*dirx);
        yy = (*y) + pas_avancement * (*dirx);
        if (rencontre_tout(xx, yy) == 1)
            tab[3] = 1.;

        xx = (*x);
        yy = (*y) + pas_avancement * (*dirx);
        if (rencontre_tout(xx, yy) == 1)
            tab[4] = 1.;
    }
    else
    {
        /*printf("cas2\n"); */
        xx = (*x) + pas_avancement * (*diry);
        yy = (*y);
        if (rencontre_tout(xx, yy) == 1)
            tab[0] = 1.;

        xx = (*x) + pas_avancement * (*diry);
        yy = (*y) + pas_avancement * (*diry);
        if (rencontre_tout(xx, yy) == 1)
            tab[1] = 1.;

        xx = (*x);
        yy = (*y) + pas_avancement * (*diry);
        if (rencontre_tout(xx, yy) == 1)
            tab[2] = 1.;

        xx = (*x) - pas_avancement * (*diry);
        yy = (*y) + pas_avancement * (*diry);
        if (rencontre_tout(xx, yy) == 1)
            tab[3] = 1.;

        xx = (*x) - pas_avancement * (*diry);
        yy = (*y);
        if (rencontre_tout(xx, yy) == 1)
            tab[4] = 1.;
    }
    /*
    for(i=0;i<5;i++)
    {
      printf("tab[%i]=%f \n",i,tab[i]);
    }
    */
    if (isequal(tab[2], 1))
    {
        /* Eviter obstacle tourner sur place */
        /* printf("Eviter obstacle tourner sur place\n"); */
      if (isequal((tab[0] + tab[1]), (tab[3] + tab[4])))
        {
            /*tourner sur place au hazard */
            /*printf("tourner sur place au hazard\n"); */
            if (drand48() > .5)
            {
                /*tourner sur place vers la droite */
	      if (isdiff((*dirx), 0.))
                {
                    (*diry) = (*dirx);
                    (*dirx) = 0;
                }
                else
                {
                    (*dirx) = -(*diry);
                    (*diry) = 0;
                }
            }
            else
            {
                /*tourner sur place vers la gauche */
	      if (isdiff((*dirx), 0.))
                {
                    (*diry) = -(*dirx);
                    (*dirx) = 0;
                }
                else
                {
                    (*dirx) = (*diry);
                    (*diry) = 0;
                }
            }
        }
        else if ((tab[0] + tab[1]) > (tab[3] + tab[4]))
        {
            /*tourner sur place vers la droite */
            /*printf("tourner sur place vers la droite\n"); */
	  if (isdiff((*dirx), 0.))
            {
                (*diry) = (*dirx);
                (*dirx) = 0;
            }
            else
            {
                (*dirx) = -(*diry);
                (*diry) = 0;
            }
        }
        else
        {
            /*tourner sur place vers la gauche */
            /*printf("tourner sur place vers la gauche\n"); */
	  if (isdiff((*dirx), 0.))
            {
                (*diry) = -(*dirx);
                (*dirx) = 0;
            }
            else
            {
                (*dirx) = (*diry);
                (*diry) = 0;
            }
        }
    }
    else
    {
        /*suivi selon s(G ou D) */
        if ((*s) == -1)         /* suivi a G */
        {                       /*printf("suivi selon s(G)\n"); */
	  if (isdiff(tab[0],0.) && isdiff(tab[1], 0.))
            {
                /*tourner sur place vers la G */
                /*printf("tourner sur place vers la G\n"); */
	      if (isdiff((*dirx), 0.))
                {
                    (*diry) = -(*dirx);
                    (*dirx) = 0;
                }
                else
                {
                    (*dirx) = (*diry);
                    (*diry) = 0;
                }
            }
            else
            {
                /*avancer d'un pas printf("avancer d'un pas\n"); */
                (*x) = (*x) + pas_avancement * (*dirx);
                (*y) = (*y) + pas_avancement * (*diry);
                (*x) = arrange_echelle(*x);
                (*y) = arrange_echelle(*y);
            }
        }
        else                    /* suivi a D */
        {                       /*printf("suivi selon s(D)\n"); */
	  if (isequal(tab[4], 0.) && isequal(tab[3], 0.))
            {
                /*tourner sur place vers la D */
                /*printf("tourner sur place vers la D\n"); */
	      if (isequal((*dirx), 0.))
                {
                    (*diry) = (*dirx);
                    (*dirx) = 0;
                }
                else
                {
                    (*dirx) = -(*diry);
                    (*diry) = 0;
                }
            }
            else
            {
                /*avancer d'un pas printf("avancer d'un pas\n"); */
                (*x) = (*x) + pas_avancement * (*dirx);
                (*y) = (*y) + pas_avancement * (*diry);
                (*x) = arrange_echelle(*x);
                (*y) = arrange_echelle(*y);
            }
        }
    }
    if (isequal((*dirx_mem), (*dirx)) && isequal((*diry_mem), (*diry)))
    {
        (*compt) = (*compt) + 1;
    }
    if ((*compt) == 2 || (*tt) == temps_evitement)
    {
        (*compt) = 0;
        /*Fin de l'evitement d'obstacle */
        /*printf("=>Fin de l'evitement d'obstacle\n\n"); */
        *check = 0;

      /*-----------------------------------------------------------*/
        /*Apprendre les endroits ou le robot sort du suivi d'obstacle */
        if ((*tt) != temps_evitement)
        {
            if (mode == 1)
            {
                /* apprendre_PlaceCell(*x,*y,&neur); */

            }

      /*-------------------------------------------------*/
            /*le temps de suivi d'obstacle a sa valeur minimale */
            temps_evitement = 2;
        }
        else
        {
      /*---------------------------------------*/
            /*le temps de suivi d'obstacle incremente */
            temps_evitement = temps_evitement + 1;
        }
    }
}

/* simulation du deplacement du robot a partir du vecteur mouvement (dx,dy) */

void simul_deplacement_robot(float dx, float dy)
{
    /*int choix=10; */
    /* int type,n,ngagnant=0,s=-1; */
    float dx_mem = 1., dy_mem = 0., posx2, posy2;
    int  n;
#ifndef AVEUGLE
    float posx_prec, posy_prec, dx_m_prec = 1., dy_m_prec = 0.;
    (void) numero_pt; // (unused)
    (void) posx_prec; // (unused)
    (void) posy_prec; // (unused)
    (void) dx_m_prec; // (unused)
    (void) dy_m_prec; // (unused)


    /*intitilisation pour le graphique (afficher un robot en noir) */
    posx_prec = posx;
    posy_prec = posy;
#endif

    /* char ch; */




    /*Bouger le robot     */
    dx_mem = dx;
    dy_mem = dy;

    posx2 = posx + pas * dx_mem;
    posy2 = posy + pas * dy_mem;

    /*si on rencontre un obstacle sur le chemin en mode marche aleat specifique */
    /* alors on revient sur ses pas !                                          */
    if (rencontre_tout(posx2, posy2) == 1)
    {
        dx_mem = -dx_mem;
        dy_mem = -dy_mem;
    }
    else
    {
        posx = posx + pas * dx_mem;
        posy = posy + pas * dy_mem;
    }
    posx = arrange_echelle(posx);
    posy = arrange_echelle(posy);


    affiche_landmarks();
    affiche_ptessentiel();
    affiche_eq();
    /*checker les landmarks visible a la position posx posy */
    checker_landmarks_visibles(posx, posy);

    if (mode == 1)
    {
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
        /* efface_terrain(); */
        affiche_robot(posx_prec, posy_prec, dx_m_prec, dy_m_prec, -1);
        affiche_robot(posx, posy, dx_mem, dy_mem, 1);
        posx_prec = posx;
        posy_prec = posy;
        dx_m_prec = dx_mem;
        dy_m_prec = dy_mem;
#endif



#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
        /*Affichage des Landmarks(croix) */
        /*    des pts essentiels(ronds) */
        /*    des equations             */
        affiche_landmarks();
        affiche_ptessentiel();
        affiche_eq();
        /*Affichage des obstacles */
        affiche_obstacle();
        /*afficher(texte) le temps */
        affiche_temps();

        /*Liberer le buffer */
        TxFlush(&image1);
#endif
    }
}

void move_robot_3(float dx, float dy)
{
    
    int tt = 0, check3 = 0, s = -1, cpt = 0;
    float posx2, posy2, dirx_mem, diry_mem, dirx, diry, posx_prec, posy_prec;
    TxPoint pointxt;


    /*Ajout Mathias */

    float pas_avancement = pas;

    (void) posy_prec; // (unused)
    (void) posx_prec; // (unused)
    (void) pointxt; // (unused)


    /*printf("DEPLACEMENT du robot \n"); */

    /*-----------------------------------------------------------*/
    /*     Mise en route de la marche aleatoire specifique       */
    /* si l'activite maximale des neurone ne depasse pas un seuil */
    pointxt.x = TAILLEX + 20;
    pointxt.y = 300;

#ifndef AVEUGLE
    if (graphiques == 1)
    {
        TxEcrireChaine(&image1, lut_g[2], pointxt,
                       "MOTOR COMMAND: random walk", 0);
        pointxt.x = TAILLEX + 15;
        pointxt.y = 288;
        TxDessinerRectangle(&image1, lut_g[2], TxVide, pointxt, 210, 15, 2);
        pointxt.x = TAILLEX + 20;
        pointxt.y = 280;
        TxEcrireChaine(&image1, blanc, pointxt, "BRAIN COMMAND: neural walk",
                       0);
        pointxt.x = TAILLEX + 15;
        pointxt.y = 268;
        TxDessinerRectangle(&image1, blanc, TxVide, pointxt, 210, 15, 2);
    }
#endif
    dx_mem = dx;
    dy_mem = dy;
    posx_prec = posx;
    posy_prec = posy;
    posx2 = posx + pas_avancement * dx;
    posy2 = posy + pas_avancement * dy;
#ifdef DEBUG
    printf("x:%f, y:%f, dx:%f, dy:%f,x2:%f,y2:%f\n", posx, posy, dx, dy,
           posx2, posy2);
#endif
    if (rencontre_quel_mur(posx2, posy2) >= 1 && check3 == 0)
    {
        /*    if(rencontre_quel_mur(posx2,posy2)||rencontre_landmark(posx2,posy2)==1){ */
        /*check2=0; */
        mirror = rencontre_quel_mur(posx2, posy2);
#ifdef DEBUG
        printf
            ("\n===================\n!!!!!!!MIRROR!!!!!!!!!!!!!!!!!!        %d\n",
             mirror);
#endif
        /*} */
        /*else{ */
         /*-------------------------*/
        /*debut de suivi d'obstacle */
        /* #ifdef DEBUG
           printf("entree ds evitement obstacle\n");
           #endif
           dirx=dx_mem; diry=dy_mem; cpt=0;tt=0; check2=0; 
           evitement(&check3, &posx,&posy,&dirx_mem,&diry_mem, &dirx,&diry,&s,&cpt,&tt);
           dx_mem=dirx; dy_mem=diry; check2=0;
           dx = dx_mem;
           dy = dy_mem;
           } */

    }
    else if (check3 == 1)
    {
#ifdef DEBUG
        printf("poursuit ds evitement obstacle\n");
#endif
        /*-------------------------*/
        /*Suite du suivi d'obstacle */
        evitement(&check3, &posx, &posy, &dirx_mem, &diry_mem, &dirx, &diry,
                  &s, &cpt, &tt);
        dx_mem = dirx;
        dy_mem = diry;
        check2 = 0;
        dx = dx_mem;
        dy = dy_mem;
    }
    else
    {
#ifdef DEBUG
        printf("Mvt normal\n");
#endif
        posx += pas_avancement * dx;
        posy += pas_avancement * dy;
    }

#ifdef DEBUG
    printf
        ("x:%f, y:%f, dx:%f, dy:%f,dx_mem:%f, dy_mem:%f,posx_prec:%f,posy_prec:%f,dx_prec:%f,dy_prec:%f\n",
         posx, posy, dx, dy, dx_mem, dy_mem, posx_prec, posy_prec, dx_m_prec,
         dy_m_prec);
#endif

#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
    affiche_robot(posx_prec, posy_prec, dx_m_prec, dy_m_prec, -1);
    affiche_robot(posx, posy, dx_mem, dy_mem, 1);
    posx_prec = posx;
    posy_prec = posy;
    dx_m_prec = dx_mem;
    dy_m_prec = dy_mem;
    affiche_landmarks();
    affiche_ptessentiel();
    affiche_eq();
    /*Affichage des obstacles */
    affiche_obstacle();
    /*afficher(texte) le temps */
    affiche_temps();

    /*Liberer le buffer */
    TxFlush(&image1);
#endif
}


void turn_simul(float angle, float distance)
{
    float dx, dy;

    static int case_x = 0;
    static int case_y = 0;

    /* printf("choix mov = %d\n", choix_mov); */
    if (choix_mov != 2)
    {
        distance = distance * 1.;
        dx = cos(angle) * distance;
        dy = sin(angle) * distance;
#ifdef simple
        posx = posx + dx;
        posy = posy + dy;
#endif
#ifndef simple
#ifdef nav_simul_old_ways
        simul_deplacement_robot(dx, dy);
#else
        move_robot_3(dx, dy);
#endif
#endif
    }
    else
    {
        case_x++;
        if (case_x == nb_cases)
        {
            case_x = 0;
            case_y++;
        }
        if (case_y < nb_cases)
        {
            posx = case_x * pas;
            posy = case_y * pas;
        }
        else
        {
            printf("fin de l'environnement\n");
            exit(0);
        }
    }

}
