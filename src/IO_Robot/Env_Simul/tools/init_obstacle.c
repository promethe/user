/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup init_obstacle init_obstacle
\brief 
 

\section Author 
-Name: XXXXXXX
-Created: xx/xx/xxxx

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description


\section Macro
-none

\section Local variables
-char nom_fichier_cfg[32]
-float **table_land
-float obstacle[40][40]

\section Global variables
-none

\section Internal Tools
-mise_a_echelle_cases_vers_pix()

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include "include/local_var.h"

#include "include/mise_a_echelle_cases_vers_pix.h"

void init_obstacle(void)
{
    int i, j, test;
    int val;
    int max = -1;

    char st[2];
    FILE *fcfg, *fprs;

  /*-----------------------------------------------------------------*/
    /*choix de la fonction de mouvement  que l'on desire utiliser      */
    /*                                                                 */
  /*-----------------------------------------------------------------*/

    cprints("what kind of movement... directed(0) aleat(1) info (2) \n");
    cscans("%d", &choix_mov);

  /*-----------------------------------------------------------------*/
    /*ouverture du fichier qui contient l'information sur les obstacles */
    /* dans une grille 40x40 avec comme valeur M pour coder un mur    */
  /*-----------------------------------------------------------------*/
    fcfg = fopen(nom_fichier_cfg, "r");
    if (fcfg == NULL)
    {
        EXIT_ON_ERROR
            ("Environment map for init config %s is not in the current directory in %s\n",
             nom_fichier_cfg, __FUNCTION__);
        exit(0);
    }

    fprs = fopen(nom_fichier_prs, "r");
    if (fprs == NULL)
    {
        EXIT_ON_ERROR
            ("Environment map for init config %s is not in the current directory\n",
             nom_fichier_prs);
        exit(0);
    }
    /*reset de parametre de landmarks */
    for (i = 0; i < NBLAND; i++)
    {
        presence[i] = -1;
        tjr_visibl[i] = 0;
    }                           /* ajout Nizar */


  /*-------------------------------------------*/
    /*initialisation de la matrice obstacle[i][j] */
  /*-------------------------------------------*/
    /*reset de la carte des obstacles */
    for (i = 0; i < nb_cases; i++)
        for (j = 0; j < nb_cases; j++)
            obstacle[j][i] = 0.;

/*for(i=0;i<nb_cases;i++)
  {
   for(j=0;j<nb_cases;j++)
      {
	if(obstacle[j][i]==0.)
		printf("*");
	else if(obstacle[j][i]==1.)
		printf("M");
	else if(obstacle[j][i]==2.)
		printf("D");
	else if(obstacle[j][i]==3.)
		printf("m");
	}
	printf("\n");
}	*/
  /*-------------------------------------------*/
    /*remplissage de la matrice obstacle[i][j] */
  /*-------------------------------------------*/
    cprints("nbcases %d \n", nb_cases);
    for (i = 0; i < nb_cases; i++)
    {
        for (j = 0; j < nb_cases; j++)
        {
            test=fscanf(fprs, "%d ", &val);
			if(test == 0){kprints("Erreur de lecture du fichier dans init_obstacle");}
            test=fscanf(fcfg, "%s ", st);
   			if(test == 0){kprints("Erreur de lecture du fichier dans init_obstacle");}


            obstacle[j][i] = 0.;
            if (strcmp(st, "D") == 0)
                obstacle[j][i] = 2.;    /*danger */
            else if (strcmp(st, "M") == 0)
                obstacle[j][i] = 1.;    /*gros mur */
            else if (strcmp(st, "m") == 0)
                obstacle[j][i] = 3.;    /*petit mur */
            else if (strcmp(st, "A") == 0 || strcmp(st, "a") == 0)
            {
                obstacle[j][i] = 0.;
                if (val < 0)
                {
                    EXIT_ON_ERROR
                        ("erreur dans %s: la lettre Aa est trouve mais pas le num correspondant: case %d %d\n",
                         __FUNCTION__, j, i);
                    exit(0);
                }
                else
                {
                    if (val > max)
                        max = val;

                    presence[val] = 1;
                    if (strcmp(st, "A") == 0)
                    {
                        table_land[val][0] =
                            mise_a_echelle_cases_vers_pix((float) j);
                        table_land[val][1] =
                            mise_a_echelle_cases_vers_pix((float) i);
                        table_land[val][2] = 1.;
                        tjr_visibl[val] = 0;
                    }
                    else
                    {
                        table_land[val][0] =
                            mise_a_echelle_cases_vers_pix((float) j);
                        table_land[val][1] =
                            mise_a_echelle_cases_vers_pix((float) i);
                        table_land[val][2] = 1.;
                        tjr_visibl[val] = 1;
                    }

                }
            }
            else if (strcmp(st, "X") != 0 && strcmp(st, "H") != 0
                     && strcmp(st, "E") != 0 && strcmp(st, "N") != 0
                     && strcmp(st, "0") != 0)
            {
                EXIT_ON_ERROR("la lettre %s ne sert pas: corrigez la case %d %d\n",
                       st, j, i);
                exit(0);
            }

/*		printf("%d",(int)(obstacle[j][i]));*/
        }
        test=fscanf(fcfg, "\n");
		if(test == 0){kprints("Erreur de lecture du fichier dans init_obstacle");}

/*	printf("%d\n",i);*/

    }

  /*-----------------*/
    /*Fermeture fichier */
  /*-----------------*/

/*for(i=0;i<nb_cases;i++)
  {
   for(j=0;j<nb_cases;j++)
      {
	printf("%1.0f ",obstacle[j][i]);
	}
	printf("\n");
}*/
    fclose(fcfg);
    fclose(fprs);

    for (i = 0; i < NBLAND; i++)
    {
        printf("landmark %d : %f \n presence : %d\n", i, table_land[i][2],
               presence[i]);
        if (presence[i] < 1)
        {
            table_land[i][0] = -1;
            table_land[i][1] = -1;
            table_land[i][2] = -1;
        }


    }

}
