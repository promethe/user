/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  mise_a_jour_var_essentiel.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float** table_terrain
-float quant
-int satisfaction

Global variables:
-none

Internal Tools:
-mise_a_jour_nutrition_percue()
-mise_a_jour_hidratation_percue()
-mise_a_jour_retour_au_nid()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include "include/local_var.h"

#include "include/mise_a_jour_hidratation_percue.h"

#include "math.h"
#include "reseau.h"

void mise_a_jour_retour_au_nid(float retour_nid)
{
    float rn, alpha_rn, rnmax, rnmin;

  /*------------------------------------------------------------------------------*/
    /* Niveau de retour nid(plus la peur du robot est grande plus le niveau est fort */
    /* la peur augmente lorsque le robot a quitte trop longtemps son nid            */
    alpha_rn = param_varessentiel[2][1];
    rnmax = param_varessentiel[2][5];
    rnmin = 0.;
    rn = param_varessentiel[2][0];

    rn = rn - alpha_rn * rn + beta_rn * (rnmax - rn) * retour_nid -
        beta_consommation_nid * (rn - rnmin) * (1. - retour_nid);
  /*---------------------*/
    /*permet une saturation */
    if (rn > rnmax)
        rn = rnmax;
    param_varessentiel[2][0] = rn;

}

void mise_a_jour_nutrition_percue(float ingestion_n, float ingestion_h)
{
  float alpha_n;
  float nmax, nmin, n;
  
  /*Niveau de nourriture mis a jour */
  alpha_n = param_varessentiel[0][1];
  nmax = param_varessentiel[0][5];
  nmin = 0.;
  
  n = param_varessentiel[0][0];
  
  n = n - alpha_n * n
    + beta_ingestion_n * (nmax - n) * ingestion_n
    - beta_consommation_n * (n - nmin) * (1. - ingestion_n)
    + gamma_couplage_n * ingestion_h * n; /*couplage: Boire coupe la faim */
  
  /*permet une saturation */
  if (n > nmax)
    n = nmax;
  param_varessentiel[0][0] = n;
}

void mise_a_jour_var_essentiel(int numero_pt
                   /*-1(aucune ingestion) ou numero*/ )
{
    float ingestion_n = 0., ingestion_h = 0., retour_nid = 0.;


    if (numero_pt != -1)
    {
        if (table_terrain[numero_pt][4] <= 0)
        {
            ingestion_n = 0.;
            ingestion_h = 0.;
        }
        else
        {
            /*On vient de manger ou de boire */
            /*disparition de la source supprimer momentanement */
            /*  table_terrain[numero_pt][4]= table_terrain[numero_pt][4] - quant; */
	  if (isequal(table_terrain[numero_pt][0], 0.))
                ingestion_n = 1;
            else
                ingestion_n = 0.;
	  if (isequal(table_terrain[numero_pt][0], 1.))
                ingestion_h = 1.;
            else
                ingestion_h = 0.;
	  if (isequal(table_terrain[numero_pt][0], 2.))
                retour_nid = 1.;
            else
                retour_nid = 0.;

            /*le robot est alors satisfait */
            satisfaction = 1;


        }
    }
    else
    {
        /*Le robot n'a rien trouve a manger ni a boire, n'est pas au nid */
        ingestion_n = 0.;
        ingestion_h = 0.;
        retour_nid = 0.;
        /*le robot n'est alors pas satisfait */
        satisfaction = 0;
    }
    /*Mise a jour des variables essentielles */
    /*Avec equations percues */
    mise_a_jour_nutrition_percue(ingestion_n, ingestion_h);
    mise_a_jour_hidratation_percue(ingestion_n, ingestion_h);
    mise_a_jour_retour_au_nid(retour_nid);

    /*Avec equations objectives */
    /* mise_a_jour_nutrition_objective(ingestion_n,ingestion_h);
       mise_a_jour_hidratation_objective(ingestion_n,ingestion_h); */

    /*mise a jour de la vigilence du systeme ainsi que de la variation de vigilence */
    /*mise_a_jour_vigilence();  modif pg la vigilence est controlee explicitement */
}
