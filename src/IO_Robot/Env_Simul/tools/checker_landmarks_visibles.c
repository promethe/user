/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float pas
-int NBLANF_vis
-int NBLAND
-float ** table_land

Global variables:
-none

Internal Tools:
-arrange_echelle()
-rencontre_tout()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <math.h>

#include "include/local_var.h"

#include "include/arrange_echelle.h"
#include "include/rencontre_tout.h"
#include "reseau.h"

void checker_landmarks_visibles(float x, float y)
{
    float xl, yl, x2, y2, x3, y3, difx, dify, vect_angle, p;
    float dx, dy;
    int l;

    NBLAND_vis = 0;
    p = pas;

  /*-----------------------------------------------*/
    /*A partir de la position x,y du robot actuelle  */
    /* on defini si tel ou tel landmark est visible  */
    /* c'est a dire si on ne rencontre pas d'obstacle */
    x = arrange_echelle(x);
    y = arrange_echelle(y);
    for (l = 0; l < NBLAND; l++)
    {
        if (presence[l] == -1)
            table_land[l][2] = 0.;  /*non visible */
        else
        {
            xl = arrange_echelle(table_land[l][0]);
            yl = arrange_echelle(table_land[l][1]);
            x2 = x;
            y2 = y;
            difx = xl - x;
            dify = yl - y;
            if (isequal(difx, 0.) && isequal(dify, 0.))
                vect_angle = 0.;
            else
                vect_angle = atan2(dify, difx);
            dx = p * cos(vect_angle);
            dy = p * sin(vect_angle);
            do
            {
                x2 = x2 + dx;
                y2 = y2 + dy;
                x3 = arrange_echelle(x2);
                y3 = arrange_echelle(y2);
            }
            while (rencontre_tout(x3, y3) != 1);

            if (isequal(x3, xl) && isequal(y3, yl))
            {
                table_land[l][2] = 1.;  /*visible */
                NBLAND_vis = NBLAND_vis + 1;    /*mise a jour de la var globale */
            }
            else
                table_land[l][2] = 0.;  /*non visible */
            if (tjr_visibl[l] == 1)
            {
                table_land[l][2] = 1;
                NBLAND_vis = NBLAND_vis + 1;
            }
        }
    }
/*printf("pres:%p,table_land:%p\n",presence,table_land);*/
}
