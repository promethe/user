/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  alloue.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-int NBLAND
-int NBLAND_vis
-int NBLAND_MAX
-int nbptessentiel
-float ** table_land
-float ** table_terrain

Global variables:
-none

Internal Tools:
-existance_du_fichier_de_configuration()
-calcule_NBLAND()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "include/local_var.h"

#include "include/existance_du_fichier_de_configuration.h"

int calcule_NBLAND(void)
{
    int i, j, n = 0;
    int max = -2;
    int val = -3;
    char st[2];
    FILE *f;

  /*-----------------------------------------------------------------*/
    /*ouverture du fichier qui contient l'information sur les landmarks */
    /* dans une grille 40x40 avec comme valeur A pour coder un Amer    */
  /*-----------------------------------------------------------------*/

    f = fopen(nom_fichier_prs, "r");    /*j'ai remplace cfg par prs */
    if (f == NULL)
    {
        EXIT_ON_ERROR("Environment map for init config %s is not in the current directory in %s\n",nom_fichier_prs, __FUNCTION__);
    }

    for (i = 0; i < nb_cases; i++)
    {
        for (j = 0; j < nb_cases; j++)
        {
            /*landmark-Amer */
            TRY_FSCANF(1, f, "%s ", st);
            val = atoi(st);
            if (max < val) max = val;      /* le nombre de landmark utile pour les boucles est le nombre maximum */
            /*printf("ATOI %d\n",atoi(st)); */
            /*
               if((strcmp(st,"A"))==0)
               {
               n++;
               }
             */
        }
        if(fscanf(f, "\n")!=0) cprints("in %s: pb reading eol in file %s \n",__FUNCTION__,nom_fichier_prs);
    }
    fclose(f);
    max = max + 1;              /* car les numeros de landmark commence a  0 et le nombre de landmark commence a 1 */
    printf("MAX %d\n", max);
    if (max > 0)
    {
        n = max;
        cprints("Valeur de NBLAND %d\n", n);
    }
    else
    {
        EXIT_ON_ERROR("Erreur dans le fichier de presence *.prs \n");
    }
    return (n);
}



void alloue(char *s)
{
    int i;

  /*----------------------------------------------------------------*/
    /*Teste si le fichier contenant l environnement de l ANIMAT existe */
    /* sinon quitter                                                  */
    existance_du_fichier_de_configuration();

  /*------------------------------------------*/
    /*allocation des positions des Landmarks    */
    /*tableau des landmarks x,y,visible         */
  /*------------------------------------------*/
    if (memcmp(s, "tout", 4) == 0 || memcmp(s, "table_land", 4) == 0)
    {
      /*----------------*/
        /*calcul du NBLAND */
        NBLAND = calcule_NBLAND();
        NBLAND_vis = NBLAND;

        if (NBLAND > NBLAND_MAX)
        {
            EXIT_ON_ERROR("Plus de landmarks que le max autorise par le code (parametre en dur)\n");
        }

        table_land =(float **) malloc((size_t) ((NBLAND_MAX) * sizeof(float *)));
        for (i = 0; i < NBLAND_MAX; i++)
        {
            table_land[i] = (float *) malloc((size_t) (3 * sizeof(float)));
        }
    }
  /*------------------------------------------*/
    /*allocation des positions des pt essentiels */
    /*type lie(vrai ou faux) x y  quantite     */
  /*------------------------------------------*/
    if (memcmp(s, "tout", 4) == 0 || memcmp(s, "table_terrain", 4) == 0)
    {

        table_terrain =(float **) malloc((size_t) ((nbptessentiel) * sizeof(float *)));
        for (i = 0; i < nbptessentiel; i++)
        {
            cprints("alloue table terrain\n");
            table_terrain[i] = (float *) malloc((size_t) (6 * sizeof(float)));
        }
    }


}
