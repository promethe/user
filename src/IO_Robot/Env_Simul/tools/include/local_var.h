/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IOROBOT_ENVSIMUL_LOCAL_VAR_H
#define _IOROBOT_ENVSIMUL_LOCAL_VAR_H
#include <Typedef/boolean.h>
#include "macro.h"


extern float posx;
extern float posy;              /*position du robot */
extern float pas;               /*pasmaximal d'avance du robot */


extern int perimetre;
extern int nb_cases;
extern int NBLAND;              /*Nbre de landmarks */
extern char nom_fichier_cfg[32];
extern int mode;

extern int nbptessentiel;       /*nbre de pts essentiels ptnou + pteau + ... */
extern int aleat_table_terrain; /*par defaut si il n y a pas de point N ou E defini dans   */
                /* le fichier area.cfg on fait une initialisation aleatoire */
extern float mem[NBVARESSENTIEL][NBMEM];    /*type;mem... */
extern float param_varessentiel[NBVARESSENTIEL][6];
extern float **table_terrain;
/*table_terrain[nbptessentiel][type;liaison(1vrai ou 0faux);x;y;quantite];*/
                /*      =       {{0.,0.,30.,10.,100.}, nourriture:type=0
                   {1.,0.,10.,30.,100.}, eau: type=1 */
/*intitialisation du terrain en numero de cases 0 a 39*/
extern float **table_land;      /*tableau des landmarks x,y,visible */
               /*  = {{33,18}, {31,34},{14,36},{12,16},
                  {39,0},{39,39},{0,39},{0,0},
                  {36,3},{39,35},{8,33},{3,7}}; */
               /*  = {{39,18,1}, {39,34,1},{14,39,1},{0,16,1},
                  {39,0,1},{39,39,1},{0,39,1},{0,0,1},
                  {39,3,1},{39,35,1},{8,39,1},{0,7,1}}; */

extern int type_src_known[NBVARESSENTIEL];  /*indique si au moins une src du type correspondant est connue */

extern int NBLAND_vis;          /*nbre de landmarks visibles */
extern int NBLAND_MAX;
extern int prec_dir;
extern int alea_number;
extern int module;
extern int l_traject_done;
extern float temps;
/* Obstacles*/
extern float obstacle[500][500];
extern float buts_trouves;      /*nbre be buts trouves */
extern int nb_pts_essentiels_courant;
extern int mirror;
/*Cte eau*/
extern float beta_ingestion_h;
extern float beta_consommation_h;
extern float gamma_couplage_h;

/*Cte nourr*/
extern float beta_ingestion_n;
extern float beta_consommation_n;
extern float gamma_couplage_n;


/*Cte retour au nid*/
extern float beta_rn;
extern float beta_consommation_nid;

extern float quant;             /*Quantite absorbee */
/*Variables resultas simul*/
extern int satisfaction;        /*Le robot est satisfait(=1) lorqu'il vient de manger ou de boire */

extern float dx_mem;
extern float dy_mem;
extern int graphiques;

extern float vigilance_de_base;

extern int discr;               /*nombre de pas de discr�isation sur les angles Nizar */

extern char nom_fichier_prs[32];    /*fichier qui indique si un amer est present ou non et son num�o */

extern int choix_mov;

extern float vigil;             /*sauvegarde de la vigilance utilis�pendant l'exploration al�toire(Nizar) */

extern int _initialisation;     /*Nizar */

extern char chemin[254];        /*nom du chemin de sauvegarde Nizar */
extern char chemin2[254];
extern int presence[50];        /*j'ai mis 50 car c'est le nombre de landmark maximum d'ailleurs on fait un chose comparable dans aloue pour table_land */
extern int tjr_visibl[50];
extern boolean is_graphic_init;
extern int check2;
extern int first_init;



extern float pas_avancement;
extern float rayon_vision;

#endif
