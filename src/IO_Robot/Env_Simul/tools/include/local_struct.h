/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IOROBOT_ENVSIMUL_LOCAL_STRUCT_H
#define _IOROBOT_ENVSIMUL_LOCAL_STRUCT_H
#include <Typedef/boolean.h>
#include "macro.h"

typedef struct es_lieu
{
    int neurone;
    int x;
    int y;
    int *recrutement;
} es_lieu;

typedef struct es_tran
{
    int neurone;
    int depart;
    int arrive;
    int autotrans;
    float mvt;
    int *recrutement;
} es_trans;

typedef struct MyData_f_monitor_planif
{
    int gpe_EC;
    int gpe_vigilence;
    int gpe_CA3;
    int gpe_DG;
    int gpe_transition_lieu;
    int *gpe_but;
    int *gpe_recrut;
    int *inc_but;
    int *debut_but;
    int size_but;
    int gpe_reward;
    int gpe_pos;
    int gpe_trans_biased;

    es_lieu lieu[1000];     /*tableau de lieux dans l'ordre*/
    int coded_lieu[1000]; /*tableau des lieux coded dans l'ordre (donne le num du lieu dans le tab lieu)*/
    es_trans trans[10000];  /*tableau de trans =  meme topo que le groupe*/
    int coded_trans[10000]; /*tableau des transition coded dans l'ordre (donne le num de la transition dans le tab trans)*/
    
    int nbr_de_lieu;
    int nbr_de_trans; 
    int *mode_but;
    int gpe_mode;
    int gpe_mvt_predit;

    int print_vals;
	int trans_name;


    struct World *world;
} MyData_f_monitor_planif;

typedef struct MyData_f_monitor_planif_real
{
    int gpe_EC;
    int gpe_vigilence;
    int gpe_CA3;
    int gpe_DG;
    int gpe_transition_lieu;
    int gpe_but;
    int gpe_planif;

    es_lieu lieu[1000];     /*tableau de lieux dans l'ordre*/
    es_trans trans[10000];  /*tableau de trans =  meme topo que le groupe*/
    int coded_trans[10000]; /*tableau des transition coded dans l'ordre (donne le num de la transition dans le tab trans)*/
    
    int connec[1000000][2];
    int nbr_de_connec;

    int nbr_de_lieu;
    int nbr_de_trans; 
    int mode_but;
    int gpe_mode;
	int gpe_image;
	int gpe_position;
    int gpe_mvt_predit;
} MyData_f_monitor_planif_real;

typedef struct Env_Simul
{
/*position du robot */
float posx;
float posy;              

} Env_Simul;

#endif
