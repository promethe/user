/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Typedef/boolean.h>
#include "include/macro.h"

float posx;
float posy;                     /*position du robot */
float pas;                      /*pasmaximal d'avance du robot */

int nb_cases = 40;
int NBLAND;                     /*Nbre de landmarks */
char nom_fichier_cfg[32] = "area.cfg";
int mode = 0;
int nbptessentiel = 2;          /*nbre de pts essentiels ptnou + pteau + ... */
int aleat_table_terrain = 1;    /*par defaut si il n y a pas de point N ou E defini dans   */
                                /* le fichier area.cfg on fait une initialisation aleatoire */
float mem[NBVARESSENTIEL][NBMEM];   /*type;mem... */
float param_varessentiel[NBVARESSENTIEL][6] =
    { {200., 0.0001, 200., 30., 70., 200.},
{200., 0.0001, 200., 30., 70., 200.}
};
float **table_terrain;
/*table_terrain[nbptessentiel][type;liaison(1vrai ou 0faux);x;y;quantite,cellule associe];*/
                /*      =       {{0.,0.,30.,10.,100.}, nourriture:type=0
                   {1.,0.,10.,30.,100.}, eau: type=1 */
int type_src_known[NBVARESSENTIEL]; /*indique si au moins une src du type correspondant est connue */
/*intitialisation du terrain en numero de cases 0 a 39*/
float **table_land;             /*tableau des landmarks x,y,visible */
               /*  = {{33,18}, {31,34},{14,36},{12,16},
                  {39,0},{39,39},{0,39},{0,0},
                  {36,3},{39,35},{8,33},{3,7}}; */
               /*  = {{39,18,1}, {39,34,1},{14,39,1},{0,16,1},
                  {39,0,1},{39,39,1},{0,39,1},{0,0,1},
                  {39,3,1},{39,35,1},{8,39,1},{0,7,1}}; */

int NBLAND_vis;                 /*nbre de landmarks visibles */
int NBLAND_MAX = 100;
int prec_dir = 60;
int alea_number = -1;
int module = -1;
int l_traject_done = 0;
int perimetre = 50;              /*perimetre de vision */
int check2 = 0;                 /*flag pour la generation d'un nouveau mouvement aleatoire */
int mirror = 0;
/* Obstacles*/
float obstacle[500][500];
float buts_trouves = 0.;        /*nbre be buts trouves */
int nb_pts_essentiels_courant = 0;

/*Cte eau*/
float beta_ingestion_h = 0.3;
float beta_consommation_h = 0.003;
float gamma_couplage_h = 0.003;

/*Cte nourr*/
float beta_ingestion_n = 0.3;
float beta_consommation_n = 0.003;
float gamma_couplage_n = 0.003;


/*Cte retour au nid*/
float beta_rn = 0.3;
float beta_consommation_nid = 0.003;

float quant = 0.;               /*Quantite absorbee */
/*Variables resultas simul*/
int satisfaction = 0;           /*Le robot est satisfait(=1) lorqu'il vient de manger ou de boire */

float dx_mem = 0.;
float dy_mem = 0.;

int graphiques = 1;
/*----------------------------------------------------------------------*/
float temps = 0.;

float vigilance_de_base = 0.01;


int discr = 0;                  /*nombre de pas de discreisation sur les angles Nizar */

char nom_fichier_prs[32] = "area.prs";  /*fichier qui indique si un amer est present ou non et son numero */
int choix_mov = -1;

int _initialisation = 0;        /*Nizar */

float vigil = 0;                /*sauvegarde de la vigilance utilisee pendant l'exploration aleatoire(Nizar) */
char chemin[254] = "Mai_Marseille/lieu";    /*nom du chemin de sauvegarde Nizar */
char chemin2[254] = "Mai_Marseille/activ";
int presence[100];              /*j'ai mis 50 car c'est le nombre de landmark maximum d'ailleurs on fait un chose comparable dans aloue pour table_land */
/*quand un landmark est visible quelque soit l'endroit vaut 1 sinon 0*/
int tjr_visibl[100];

boolean is_graphic_init = false;
int first_init = 1;


float pas_avancement=25;
float rayon_vision=30;
