/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004



Description:

Macro:
-none

Local variables:
-int aleat_table_terrain
-int NBLAND
-float ** table_terrain
-float ** table_land
-float obstacle[40][40]
-int nbptessentiel

Global variables:
-none

Internal Tools:
-init_ptsessentiels()
-mise_a_echelle_cases_vers_pix()

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>
#include <string.h>

#include "include/local_var.h"

#include "include/mise_a_echelle_cases_vers_pix.h"
#include "math.h"
#include "reseau.h"

void init_ptsessentiels(void)
{
   int i, j, n = 0;
   char st[2];
   FILE *f;

   /*-----------------------------------------------------------------*/
   /*ouverture du fichier qui contient l'information                  */
   /* dans une grille 40x40 avec comme valeur N ou E pour coder       */
   /* un pt de Nourriture ou un point d'eau                           */
   /*-----------------------------------------------------------------*/
   f = fopen(nom_fichier_cfg, "r");
   if (f == NULL) EXIT_ON_ERROR("Environment map for init config %s is not in the current directory in %s\n", nom_fichier_cfg, __FUNCTION__);

   for (i = 0; i < nb_cases; i++)
   {
      for (j = 0; j < nb_cases; j++)
      {
         TRY_FSCANF(1,f, "%s ", st);
         if ((strcmp(st, "N")) == 0)
         {
            if (table_terrain[n][4] <= 0.)
            {
               table_terrain[n][0] = 0;
               table_terrain[n][1] = 0;
               table_terrain[n][2] = mise_a_echelle_cases_vers_pix(j);
               table_terrain[n][3] = mise_a_echelle_cases_vers_pix(i);
               table_terrain[n][4] = 100.;
               table_terrain[n][5] = -1.;
            }
            n++;
         }
         if ((strcmp(st, "E")) == 0)
         {
            if (table_terrain[n][4] <= 0.)
            {
               table_terrain[n][0] = 1;
               table_terrain[n][1] = 0;
               table_terrain[n][2] = mise_a_echelle_cases_vers_pix(j);
               table_terrain[n][3] = mise_a_echelle_cases_vers_pix(i);
               table_terrain[n][4] = 100.;
               table_terrain[n][5] = -1.;
            }
            n++;
         }
         if ((strcmp(st, "H")) == 0)
         {
            if (table_terrain[n][4] <= 0.)
            {
               table_terrain[n][0] = 2;
               table_terrain[n][1] = 0;
               table_terrain[n][2] = mise_a_echelle_cases_vers_pix(j);
               table_terrain[n][3] = mise_a_echelle_cases_vers_pix(i);
               table_terrain[n][4] = 100.;
               table_terrain[n][5] = -1.;
            }
            n++;
         }
      }
      if(fscanf(f, "\n")!=0) cprints("in %s: pb reading eol in file %s \n",__FUNCTION__,nom_fichier_cfg);
   }
   fclose(f);
}

void init_aleat_table_terrain(void)
{
   int i, j, ii, jj, check = 0;
   float x, y;

   /*On verifie si on fait une initialisation aleatoire ou pas*/
   if (aleat_table_terrain == 0)
   {
      /*pas d initialisation aleatoire des pts essentiels */
      init_ptsessentiels();
   }
   else                        /*initialisation aleat des pts essentiels */
      for (i = 0; i < nbptessentiel; i++)
         if (table_terrain[i][4] <= 0.)
         {
            do
            {
               x = (int) (39. * drand48());
               y = (int) (39. * drand48());
               x = mise_a_echelle_cases_vers_pix(x);
               y = mise_a_echelle_cases_vers_pix(y);
               /*
               	     x=mise_a_echelle_cases_vers_pix(15+10*i);
                	     y=mise_a_echelle_cases_vers_pix(15+10*i);*/
               check = 0;
               for (j = 0; j < i; j++)
               {
                  if (isequal(table_terrain[j][2], x)  && isequal(table_terrain[j][3], y))
                  {
                     check = 1;
                     break;  /*on a deja un pt etabli a cette position */
                  }
               }
               for (j = 0; j < NBLAND; j++)
               {
                  if (isequal(table_land[j][0], x) && isequal(table_land[j][1], y))
                  {
                     check = 1;
                     break;  /*on a deja un landmark  a cette position */
                  }
               }
               for (ii = 0; ii < nb_cases; ii++)
                  for (jj = 0; jj < nb_cases; jj++)
                     if (isequal(obstacle[ii][jj], 1) && isequal(x, mise_a_echelle_cases_vers_pix(ii))  && isequal(y, mise_a_echelle_cases_vers_pix(jj)))
                     {
                        check = 1;
                        break;  /*on a deja un obstacle  a cette position */
                     }
            }
            while (check == 1);
            table_terrain[i][2] = x;
            table_terrain[i][3] = y;
            table_terrain[i][4] = 100.;
            if (i < (nbptessentiel / 3.))
               table_terrain[i][0] = 0.;   /*nourriture:type=0* */
            else if (i < (2. * nbptessentiel / 3.)) table_terrain[i][0] = 1.;   /*eau: type=1 */
            else                                    table_terrain[i][0] = 2.;   /*Nid: type=2 */
         }


}
