/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup group_tools
\defgroup affiche_robot affiche_robot
\brief 
\section Author
Name: xxxxxxxx
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Cette fonction permet de faire l'affichage d'un robot simulé. Elle est utilisée dans plusieurs autres fonctions.
\section Macro
-none 

\section Local variables
-float pas

\section Global variables
-none

\section Internal Tools
-dessine_robot()

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>

#include "include/local_var.h"

void dessine_robot(TxPoint point1, TxPoint point2, TxPoint point3, int coul)
{
#ifndef AVEUGLE
    static TxPoint tab_points[3];

    tab_points[0] = point2;
    tab_points[1] = point1;
    tab_points[2] = point3;
    /*printf("-------x1=%d ,y1=%d,x2=%d,y2=%d,x3=%d,y3=%d \n",point1.x,point1.y,point2.x,point2.y,point3.x,point3.y); */
    if (couleur > 0)
        TxDessinerPolygone(&image1, coul, TxPlein, tab_points, 3, 1);
    else
        TxDessinerPolygone(&image1, -coul, TxVide, tab_points, 3, 1);
    TxFlush(&image1);
#endif

(void) point1;
(void) point2;
(void) point3;
(void) coul;
}


void affiche_robot(float x, float y, float dx, float dy, int coul)
{
    /*TxPoint point1;

       point1.x = x;
       point1.y = y;
       TxAfficherPixmap(&image1,&pix,point1); */

    TxPoint point1, point2, point3;
/*  if (coul==-1)
coul=gdk_gc_get_rgb_fg_color (image1,& couleurs[numero_couleur]);*/
#ifdef DEBUG
    dprints("AFFICHAGE ROBOT x=%f, y=%f ,dx=%ef,dy=%ef, coul =%d\n", x, y, dx,
           dy, coul);
    dprints("pas=%f \n", pas);
#endif
/*horizontale*/
/*
pt3 
|	(x,y)	pt1	
pt2 
*/
    if (isdiff(dx, 0.f) && isequal(dy, 0.f))
    {
        point1.x = (int) (x + pas * dx / 2.);
        point1.y = (int) y;
        point2.x = (int) (x - pas * dx / 2.);
        point2.y = (int) (y + pas / 3.);
        point3.x = (int) (x - pas * dx / 2.);
        point3.y = (int) (y - pas / 3.);
#ifdef DEBUG
        dprints("C1 x1=%d ,y1=%d,x2=%d,y2=%d,x3=%d,y3=%d \n", point1.x,
               point1.y, point2.x, point2.y, point3.x, point3.y);
#endif
        dessine_robot(point1, point2, point3, coul);
    }
    /*verticale */
    /*
       pt2 - pt3 
       (x,y)               
       pt1
     */
    else if (isequal(dx, 0.f) && isdiff(dy, 0.f))
    {
        point1.x = (int) x;
        point1.y = (int) (y + pas * dy / 2.);
        point2.x = (int) (x - pas / 3.);
        point2.y = (int) (y - pas * dy / 2.);
        point3.x = (int) (x + pas / 3.);
        point3.y = (int) (y - pas * dy / 2.);
#ifdef  DEBUG
        dprints("C2 x1=%d ,y1=%d,x2=%d,y2=%d,x3=%d,y3=%d \n", point1.x,
               point1.y, point2.x, point2.y, point3.x, point3.y);
#endif
        dessine_robot(point1, point2, point3, coul);
    }
    /*quelquonue */
    else if (isdiff(dx, 0.f) && isdiff(dy, 0.f))
    {
        point1.x = (int) (x + pas * dx / 2.);
        point1.y = (int) (y + pas * dy / 2.);
        point2.x = (int) (x - pas * dx / 2.);
        point2.y = (int) y;
        point3.x = (int) x;
        point3.y = (int) (y - pas * dy / 2.);
#ifdef  DEBUG
        dprints("C3 x1=%d ,y1=%d,x2=%d,y2=%d,x3=%d,y3=%d \n", point1.x,
               point1.y, point2.x, point2.y, point3.x, point3.y);
#endif
        dessine_robot(point1, point2, point3, coul);
    }
    else
    {
        /*printf("Robot ne bouge pas: affiche cercle \n"); */
        point1.x = (int) x;
        point1.y = (int) y;
#ifndef AVEUGLE                 /* 18/06/2003 Olivier.Ledoux */
        /*   TxDessinerCercle(&image1,coul,TxPlein,point1,8,1); */
        /*printf("else x1=%d ,y1=%d,x2=%d,y2=%d,x3=%d,y3=%d \n",point1.x,point1.y,point2.x,point2.y,point3.x,point3.y); */
#endif
    }

}
