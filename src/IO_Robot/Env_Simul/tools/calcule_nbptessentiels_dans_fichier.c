/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  calcule_nbptessentiels_dans_fichier.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -none

 Local variables:
 -char nom_fichier_cfg[32]

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include "include/local_var.h"

int calcule_nbptessentiels_dans_fichier(void)
{
	int i, j, n = 0;
	char st[2];
	FILE *f;

	/*-----------------------------------------------------------------*/
	/*ouverture du fichier qui contient l'information                  */
	/* dans une grille 40x40 avec comme valeur N ou E pour coder       */
	/* un pt de Nourriture ou un point d'eau                           */
	/*-----------------------------------------------------------------*/
	f = fopen(nom_fichier_cfg, "r");
	if (f == NULL ) EXIT_ON_ERROR("Environment map for init config %s is not in the current directory in %s\n", nom_fichier_cfg, __FUNCTION__);

	for (i = 0; i < nb_cases; i++)
	{
		for (j = 0; j < nb_cases; j++)
		{
			TRY_FSCANF(1,f, "%s ", st);
			if ((strcmp(st, "N")) == 0 || (strcmp(st, "E")) == 0 || (strcmp(st, "H")) == 0)
			{
				n++;
				if (strcmp(st, "N") == 0) printf("Nourriture found in %d,%d\n", j, i);
				if (strcmp(st, "E") == 0) printf("Eau found in %d,%d\n", j, i);
				if (strcmp(st, "H") == 0) printf("Home found in %d,%d\n", j, i);
			}
		}
      if(fscanf(f, "\n")!=0) cprints("in %s: pb reading eol in file %s \n",__FUNCTION__,nom_fichier_cfg);
	}
	fclose(f);
	return (n);

}

/*----------------------------------------------------------------------------------*/
