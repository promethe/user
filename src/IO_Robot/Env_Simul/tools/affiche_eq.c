/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  affiche_eq.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Affichage des courbes representant l'evolution des variables essentielles 
  dans le temps

Macro:
-NBMEM
-NBVARESSENTIEL
-TAILLEX

Local variables:
-float mem[NBVARESSENTIEL][NBMEM]
-float param_varessentiel[NBVARESSENTIEL][6]

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

#include "include/local_var.h"

#include "include/macro.h"

/*----------------------------------------------------------------------------------*/
/* Affichage des courbes representant l'evolution des variables essentielles 
dans le temps */
/*----------------------------------------------------------------------------------*/


void affiche_eq(void)
{
#ifndef AVEUGLE
    int i, type;
    float repx, repy, taille = 200.;
    TxPoint point1, point2, point3, point4, pointxt;

    repx = TAILLEX + 35;
    repy = taille;

    point1.x = repx;
    point1.y = 0;

    TxDessinerRectangle(&image1, blanc, TxPlein, point1, (int) taille,
                        (int) taille, 1);
    TracerAxe(&image1, bleu, bleu, (int) repx, (int) repy,
              (int) (repx + taille), 0, 0, 0);


    for (type = 0; type < NBVARESSENTIEL; type++)
    {
        point1.x = repx;
        point1.y =
            repy -
            (param_varessentiel[type][3] * taille /
             param_varessentiel[type][5]);
        point2.x = repx + taille;
        point2.y =
            repy -
            (param_varessentiel[type][3] * taille /
             param_varessentiel[type][5]);
        point3.x = repx;
        point3.y =
            repy -
            (param_varessentiel[type][4] * taille /
             param_varessentiel[type][5]);
        point4.x = repx + taille;
        point4.y =
            repy -
            (param_varessentiel[type][4] * taille /
             param_varessentiel[type][5]);
        pointxt.x = point1.x - 30;
        if (type == 0)
        {
            pointxt.y = repy - (taille) + 10;
            TxEcrireChaine(&image1, vert, pointxt,
                           Int2Str((int) param_varessentiel[type][5]), 0);
            pointxt.y = point1.y;
            TxEcrireChaine(&image1, vert, pointxt,
                           Int2Str((int) param_varessentiel[type][3]), 0);
            pointxt.y = point3.y;
            TxEcrireChaine(&image1, vert, pointxt,
                           Int2Str((int) param_varessentiel[type][4]), 0);
            TxDessinerSegment(&image1, vert, point1, point2, 1);
            TxDessinerSegment(&image1, vert, point3, point4, 1);
        }
        if (type == 1)
        {
            pointxt.y = repy - (taille) + 10;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][5]), 0);
            pointxt.y = point1.y;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][3]), 0);
            pointxt.y = point3.y;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][4]), 0);
            TxDessinerSegment(&image1, bleu, point1, point2, 1);
            TxDessinerSegment(&image1, bleu, point3, point4, 1);
        }
        if (type == 2)
        {
            pointxt.y = repy - (taille) + 10;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][5]), 0);
            pointxt.y = point1.y;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][3]), 0);
            pointxt.y = point3.y;
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) param_varessentiel[type][4]), 0);
            TxDessinerSegment(&image1, rouge, point1, point2, 1);
            TxDessinerSegment(&image1, rouge, point3, point4, 1);
        }

        for (i = 0; i < (NBMEM - 1); i++)
        {
            point1.x = repx + ((float) i) * (taille / (float) NBMEM);
            point1.y =
                repy - (mem[type][i] * taille / param_varessentiel[type][5]);
            point2.x = repx + ((float) (i + 1)) * (taille / (float) NBMEM);
            point2.y =
                repy -
                (mem[type][i + 1] * taille / param_varessentiel[type][5]);
            if (type == 0)      /*Energie de nourriture */
            {
                TxDessinerSegment(&image1, vert, point1, point2, 3);
            }
            if (type == 1)      /*Energie d'eau */
            {
                TxDessinerSegment(&image1, bleu, point1, point2, 3);
            }
            if (type == 2)      /*retour_nid */
            {
                TxDessinerSegment(&image1, rouge, point1, point2, 3);
            }
        }
        pointxt.x = point2.x - 20;
        pointxt.y = point2.y - 5;
        if (type == 0)
        {
            TxEcrireChaine(&image1, vert, pointxt,
                           Int2Str((int) mem[type][NBMEM - 1]), 0);
        }
        if (type == 1)
        {
            TxEcrireChaine(&image1, bleu, pointxt,
                           Int2Str((int) mem[type][NBMEM - 1]), 0);
        }
        if (type == 2)
        {
            TxEcrireChaine(&image1, rouge, pointxt,
                           Int2Str((int) mem[type][NBMEM - 1]), 0);
        }

    }
#endif
}
