/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup init_position_de_depart init_position_de_depart
\brief 
 

\section Author 
-Name: XXXXXXX
-Created: xx/xx/xxxx

\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description


\section Macro
-none

\section Local variables
-char nom_fichier_cfg[32]

\section Global variables
-none

\section Internal Tools
-mise_a_echelle_cases_vers_pix()

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include "include/local_var.h"

#include "include/mise_a_echelle_cases_vers_pix.h"

void init_position_de_depart(float *px, float *py)
{
    int i, j, test;
    char st[2];
    FILE *f;

  /*----------------------------------------------- ----------------------------*/
    /*ouverture du fichier qui contient l'information sur l environnement        */
    /* dans une grille 40x40 avec comme valeur X pour coder la position de depart */
  /*---------------------------------------------------------------------------*/
	
	f = fopen("robot_position.def", "r");
	if(continue_simulation_status==CONTINUE && f!=NULL)
	{
		
			test=fscanf(f,"%f %f",px,py);
			if(test == 0){kprints("Erreur de lecture du fichier dans init_position_de_depart");}
			cprints("px=%f py=%f\n",*px,*py);
			fclose(f);
		
	}   
	else
	{
		f = fopen(nom_fichier_cfg, "r");
		if (f == NULL)
		{
			EXIT_ON_ERROR
			("Environment map for init config %s is not in the current directory in %s\n",
			nom_fichier_cfg, __FUNCTION__);
			exit(0);
		}
		
		for (i = 0; i < nb_cases; i++)
		{
			for (j = 0; j < nb_cases; j++)
			{
			test=fscanf(f, "%s ", st);
			if(test == 0){kprints("Erreur de lecture du fichier dans init_position_de_depart");}
			/*landmark */
			if ((strcmp(st, "X")) == 0)
			{
				(*px) = mise_a_echelle_cases_vers_pix(j);
				(*py) = mise_a_echelle_cases_vers_pix(i);
				break;
			}
			}
			test=fscanf(f, "\n");
			if(test == 0){kprints("Erreur de lecture du fichier dans init_position_de_depart");}
		}
		fclose(f);
	}
}

