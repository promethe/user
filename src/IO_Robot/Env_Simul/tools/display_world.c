/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  display_world.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: J. Hirel
- description: specific file creation
- date: 01/04/2009

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Displays world object (landmarks, obstacles)
  
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

#include <libhardware.h>
#include <net_message_debug_dist.h>

#ifndef AVEUGLE
void display_world(TxDonneesFenetre *window, World *world)
{

   TxPoint start, end;
   int i, width = 1;

   if (window == NULL || world == NULL)
   {
      EXIT_ON_ERROR("ERROR in display_world: NULL pointer for window or world\n");
   }

   for(i = 0 ; i < world->nb_obstacles; i++)
   {
      start.x = world->obstacles[i].xa;
      start.y = world->obstacles[i].ya;
      end.x = world->obstacles[i].xb;
      end.y = world->obstacles[i].yb;
      width = 2 + world->obstacles[i].high * 3;	
      dprints("display_world: Printing obstacles %d (from %d,%d to %d,%d ; high = %d)\n", 
	      i, start.x, start.y, end.x, end.y, world->obstacles[i].high);
      TxDessinerSegment(window, bleu, start, end, width);
      
   }

   for(i = 0 ; i < world->nb_doors; i++)
   {
      start.x = world->doors[i].xa;
      start.y = world->doors[i].ya;
      end.x = world->doors[i].xb;
      end.y = world->doors[i].yb;
      width = 2 + world->doors[i].high * 3;	
      dprints("display_world: Printing obstacles %d (from %d,%d to %d,%d ; high = %d)\n", 
	      i, start.x, start.y, end.x, end.y, world->doors[i].high);
	  
      TxDessinerSegmentPointille(window, bleu, start, end, width);
   }
	
   for(i = 0 ; i < world->nb_landmarks; i++)
   {
      width = 1 + world->landmarks[i].always_visible * 2;
      start.x = world->landmarks[i].x + 5;
      start.y = world->landmarks[i].y;
      end.x = world->landmarks[i].x - 4;
      end.y = world->landmarks[i].y;
      dprints("display_world: Printing Landmark %d (at %d,%d ; always_visible = %d)\n", 
	      i, world->landmarks[i].x, world->landmarks[i].y, world->landmarks[i].always_visible);
      TxDessinerSegment(window, rouge, start, end, width);	 	
      
      start.x = world->landmarks[i].x;
      start.y = world->landmarks[i].y + 5;
      end.x = world->landmarks[i].x;
      end.y = world->landmarks[i].y - 4;
      TxDessinerSegment(window, rouge, start, end, width);
   }

   for(i = 0 ; i < world->nb_resources; i++)
   {
      start.x = world->resources[i].x;
      start.y = world->resources[i].y;
      dprints("display_world: Printing Resource %d (at %d,%d ; color = [%d,%d,%d])\n", 
              i, world->resources[i].x, world->resources[i].y, 
              world->resources[i].red, world->resources[i].green, world->resources[i].blue);
      TxDessinerCercle_rgb16M(window,
                              world->resources[i].red + 256 * world->resources[i].green + 256 * 256 * world->resources[i].blue, 
                              TxPlein, start, world->resources[i].radius, 1);	 	
   }
}
#endif
