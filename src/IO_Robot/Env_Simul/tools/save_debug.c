/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup save_debug save_debug
\brief 
 

\section Author 
-Name: J. Hirel
-Created: 12/06/2009

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Fonctions de sauvegarde des informations de debug. Peuvent �tre utilisees pendant la sauvegarde du reseau ou en fin de manip.


\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
- type: none
- description: none
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include "include/local_struct.h"
#include "include/local_var.h"
#include <libx.h>
#include <stdlib.h>

void save_transitions_monitor_planif(MyData_f_monitor_planif *mydata)
{
   FILE * fp;
   int i = 0;
   int k;

   if (mydata == NULL)
   {
      fprintf(stderr, "ERROR in save_transitions_monitor_planif(tools): mydata is NULL\n");
      exit(1);
   }
   
   fp = fopen("transition.def","w+");
   if (fp == NULL)
   {
      fprintf(stderr, "ERROR in save_transitions_monitor_planif(tools): Cannot create file transition.def\n");
      exit(1);
   }
   
   fprintf(fp, "configuration\n");
   fprintf(fp, "nbl = %d\nnbt = %d\n", mydata->nbr_de_lieu, mydata->nbr_de_trans);
   fprintf(fp, "nbcm = %d\n", mydata->size_but);

   for (i = 0; i < mydata->nbr_de_lieu; i++)
   {
      fprintf(fp, "%d %d %d %d", mydata->coded_lieu[i], mydata->lieu[mydata->coded_lieu[i]].neurone, mydata->lieu[mydata->coded_lieu[i]].x, mydata->lieu[mydata->coded_lieu[i]].y);
      for(k = 0; k < mydata->size_but; k++) fprintf(fp, " %d", mydata->lieu[mydata->coded_lieu[i]].recrutement[k]);
      fprintf(fp, "\n");
   }

   for ( i = 0; i < mydata->nbr_de_trans; i++)
   {
      fprintf(fp, "%d %d %d %d %d", mydata->coded_trans[i], mydata->trans[mydata->coded_trans[i]].neurone, mydata->trans[mydata->coded_trans[i]].depart, mydata->trans[mydata->coded_trans[i]].arrive, mydata->trans[mydata->coded_trans[i]].autotrans);
      for(k = 0; k < mydata->size_but; k++) fprintf(fp, " %d", mydata->trans[mydata->coded_trans[i]].recrutement[k]);
      fprintf(fp, "\n");
   }

   fclose(fp);
}


void save_robot_position_monitor_planif(MyData_f_monitor_planif *mydata)
{
   FILE * fp;
   /*int i = 0;*/
   float robot_posx, robot_posy;

   if (mydata == NULL)
   {
      fprintf(stderr, "ERROR in save_robot_position_monitor_planif(tools): mydata is NULL\n");
      exit(1);
   }

   fp = fopen("robot_position.def","w+");
   if (fp == NULL)
   {
      fprintf(stderr, "ERROR in save_robot_position_monitor_planif(tools): Cannot create file robot_position.def\n");
      exit(1);
   }

   if (mydata->gpe_pos == -1)
   {
      robot_posx = posx;
      robot_posy = posy;
   }
   else
   {
      robot_posx = neurone[def_groupe[mydata->gpe_pos].premier_ele].s1;
      robot_posy = neurone[def_groupe[mydata->gpe_pos].premier_ele + 1].s1;
   }

   fprintf(fp, "%f %f\n", robot_posx, robot_posy);
   fclose(fp);
}



void save_resources_monitor_planif(MyData_f_monitor_planif *mydata)
{
   FILE * fp;
   int i = 0;

   if (mydata == NULL)
   {
      fprintf(stderr, "ERROR in save_resources_monitor_planif(tools): mydata is NULL\n");
      exit(1);
   }
   
   if (mydata->gpe_reward == -1)
   { 
      fp = fopen("sources.def","w+");
      if (fp == NULL)
      {
	 fprintf(stderr, "ERROR in save_resources_monitor_planif(tools): Cannot create file sources.def\n");
	 exit(1);
      }
      
      for (i = 0; i < NBVARESSENTIEL; i++)
      {
	 fprintf(fp, "%d ", type_src_known[i]);
      }
      fprintf(fp, "\n"); 
      
      for (i = 0; i < nbptessentiel; i++)
      {	    
	 fprintf(fp, "%f %f %f %f %f \n", table_terrain[i][0], table_terrain[i][1], table_terrain[i][2], table_terrain[i][3], table_terrain[i][4]);
      }
      printf("\n");
      
      for ( i = 0; i < NBVARESSENTIEL; i++)
      {	    
	 fprintf(fp, "%f %f %f %f %f %f \n", param_varessentiel[i][0], param_varessentiel[i][1], param_varessentiel[i][2], param_varessentiel[i][3], param_varessentiel[i][4], param_varessentiel[i][5]);
      }
      
      fclose(fp);
   }
}
