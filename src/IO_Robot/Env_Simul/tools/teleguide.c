/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  teleguide.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Pour teleguider le robot

Macro:
-none 

Local variables:
-float posx
-float posy
-float pas
-float temps
-int mode

Global variables:
-none

Internal Tools:
-rencontre_tout()
-arrange_echelle()
-affiche_simul()
-checker_landmarks_visibles()
-reconnaissance()
-affiche_robot()
-affiche_obstacle()
-affiche_temps()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

#include "include/local_var.h"

#include "include/rencontre_tout.h"
#include "include/arrange_echelle.h"
#include "include/affiche_eq.h"
#include "include/affiche_ptessentiel.h"
#include "include/affiche_landmarks.h"
#include "include/checker_landmarks_visibles.h"
#include "include/reconnaissance.h"
#include "include/affiche_robot.h"
#include "include/affiche_obstacle.h"
#include "include/affiche_temps.h"
#include "include/rencontre_petit_mur.h"

void teleguide(int choix)
{
    /*int choix=10; */

    float dx = 1., dy = 0., dx_mem = 1., dy_mem =
        0., posx2, posy2, posx_prec, posy_prec, dx_m_prec = 1., dy_m_prec =
        0.;
    int numero_pt, n;

    (void) posx_prec; // (unused)
    (void) dx_m_prec; // (unused)
    (void) dy_m_prec; // (unused)
    (void) posy_prec; // (unused)
    (void) numero_pt; // (unused)

#ifdef DEBUG
    printf("%s = entree\n", __FUNCTION__);
#endif
    /*intitilisation pour le graphique (afficher un robot en noir) */
    posx_prec = posx;
    posy_prec = posy;

    switch (choix)
    {
    case 0:
        /*printf("\n");
           printf("* exit remote  *\n");
           printf("\n");
           printf("\a");fflush(stdout);
           printf("\n");return; */ break;
    case 1:
        dx = -1.;
        dy = 1.;
        break;
    case 2:
        dx = 0.;
        dy = 1.;
        break;
    case 3:
        dx = 1.;
        dy = 1.;
        break;
    case 4:
        dx = -1.;
        dy = 0.;
        break;
    case 6:
        dx = 1.;
        dy = 0.;
        break;
    case 7:
        dx = -1.;
        dy = -1.;
        break;
    case 8:
        dx = 0.;
        dy = -1.;
        break;
    case 9:
        dx = 1.;
        dy = -1.;
        break;
    }

    /*Bouger le robot     */
    dx_mem = dx;
    dy_mem = dy;

    posx2 = posx + pas * dx_mem;
    posy2 = posy + pas * dy_mem;

    /*si on rencontre un obstacle sur le chemin en mode marche aleat specifique */
    /* alors on revient sur ses pas !                                          */
    if ((rencontre_tout(posx2, posy2) == 1)
        || (rencontre_petit_mur(posx2, posy2) == 1))
    {
        dx_mem = -dx_mem;
        dy_mem = -dy_mem;
    }
    else
    {
        posx = posx + pas * dx_mem;
        posy = posy + pas * dy_mem;
    }
    posx = arrange_echelle(posx);
    posy = arrange_echelle(posy);

    /* affiche_landmarks(); */
    /* affiche_ptessentiel(); */
    /* affiche_eq(); */


    /*checker les landmarks visible a la position posx posy */
    checker_landmarks_visibles(posx, posy);
    /*Reconnaissance? si on passe sur un pt de Nour ou d'Eau */
    numero_pt = reconnaissance(&n);

    if (mode == 1)
    {
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
        /* efface_terrain(); */
        if (debug > 0)
        {
            affiche_robot(posx_prec, posy_prec, dx_m_prec, dy_m_prec, -1);
            affiche_robot(posx, posy, dx_mem, dy_mem, 1);
        }
#endif
        posx_prec = posx;
        posy_prec = posy;
        dx_m_prec = dx_mem;
        dy_m_prec = dy_mem;


#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */
        /*Affichage des Landmarks(croix) */
        /*    des pts essentiels(ronds) */
        /*    des equations             */
        /* affiche_landmarks(); */
        /*  affiche_ptessentiel(); */
        /*  affiche_eq(); */
        /*Affichage des obstacles */
        /*  affiche_obstacle(); */
        /*afficher(texte) le temps */
        /*   affiche_temps(); */

        /*Liberer le buffer */
        /* TxFlush(&image1); */
#endif
    }
}
