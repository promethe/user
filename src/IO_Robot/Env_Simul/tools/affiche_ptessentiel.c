/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  affiche_ptessentiel.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float pas
-int nbptessentiel
-float ** table_terrain

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>

#include "include/local_var.h"

void affiche_ptessentiel(void)
{
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */

    int i, choix;
    TxPoint point1;
    TxPoint sup_gauche;

    /*Dessin de grand cercles avec une couleur differente pour le type d'objet rencontre */
    for (i = 0; i < nbptessentiel; i++)
    {
        point1.x = table_terrain[i][2];
        point1.y = table_terrain[i][3];
        choix = table_terrain[i][0];
        if (table_terrain[i][4] > 0.)
            switch (choix)
            {
            case 0:            /*pt de nourriture */
	      if (isequal(table_terrain[i][1], 0.))
                    TxDessinerCercle(&image1, lut_g[8], TxVide, point1,
                                     rayon_vision, 3);
                else
                    TxDessinerCercle(&image1, lut_g[8], TxPlein, point1,
                                     rayon_vision, 3);
                sup_gauche.x=point1.x-pas_avancement;
		sup_gauche.y=point1.y-pas_avancement;
                TxDessinerRectangle(&image1,
                                noir, TxVide,
                                sup_gauche,pas_avancement*2,
                                pas_avancement*2, 1);
		point1.x = point1.x + 5;
                point1.y = point1.y - 5;
/*                 TxEcrireChaine(&image1, lut_g[8], point1,(char*) "Nour", 0);*/
                break;

            case 1:            /*pt d'eau */
	      if (isequal(table_terrain[i][1], 0))
                    TxDessinerCercle(&image1, bleu, TxVide, point1,
                                     rayon_vision, 3);
                else
                    TxDessinerCercle(&image1, bleu, TxPlein, point1,
                                     rayon_vision, 3);

                sup_gauche.x=point1.x-pas_avancement;
		sup_gauche.y=point1.y-pas_avancement;
                TxDessinerRectangle(&image1,
                                noir, TxVide,
                                sup_gauche,pas_avancement*2,
                                pas_avancement*2, 1);

                point1.x = point1.x + 5;
                point1.y = point1.y - 5;
/*                 TxEcrireChaine(&image1, bleu, point1,(char*) "Eau", 0);*/
                break;

            case 2:            /*pt nid */
	      if (isequal(table_terrain[i][1], 0.))
                    TxDessinerCercle(&image1, lut_g[19], TxVide, point1,
                                     rayon_vision, 3);
                else
                    TxDessinerCercle(&image1, lut_g[19], TxPlein, point1,
                                    rayon_vision, 3);

                sup_gauche.x=point1.x-pas_avancement;
		sup_gauche.y=point1.y-pas_avancement;
                TxDessinerRectangle(&image1,
                                noir, TxVide,
                                sup_gauche,pas_avancement*2,
                                pas_avancement*2, 1);

                point1.x = point1.x + 5;
                point1.y = point1.y - 5;
/*                 TxEcrireChaine(&image1, lut_g[19], point1,(char*) "Maison", 0);*/
                break;
            }
    }

#endif

}

void affiche_ptessentiel1(void*image_e)
{
#ifndef AVEUGLE                 /* 18/06/2003 Olivier Ledoux */

    int i, choix;
    TxPoint point1;
    TxDonneesFenetre*image_x=(TxDonneesFenetre*)image_e;
    /*Dessin de grand cercles avec une couleur differente pour le type d'objet rencontre */
    for (i = 0; i < nbptessentiel; i++)
    {
        point1.x = table_terrain[i][2];
        point1.y = table_terrain[i][3];
        choix = table_terrain[i][0];
        if (table_terrain[i][4] > 0.)
            switch (choix)
            {
            case 0:            /*pt de nourriture */
	      if (isequal(table_terrain[i][1], 0))
                    TxDessinerCercle(image_x, lut_g[8], TxVide, point1,
                                     (int) (pas / 2.), 3);
                else
                    TxDessinerCercle(image_x, lut_g[8], TxPlein, point1,
                                     (int) (pas / 2.), 3);
                point1.x = point1.x + 5;
                point1.y = point1.y - 5;
                TxEcrireChaine(image_x, lut_g[8], point1,(char*) "Nour", 0);
                break;

            case 1:            /*pt d'eau */
	      if (isequal(table_terrain[i][1], 0))
                    TxDessinerCercle(image_x, bleu, TxVide, point1,
                                     (int) (pas / 2.), 3);
                else
                    TxDessinerCercle(image_x, bleu, TxPlein, point1,
                                     (int) (pas / 2.), 3);
                point1.x = point1.x + 5;
                point1.y = point1.y - 5;
                TxEcrireChaine(image_x, bleu, point1,(char*) "Eau", 0);
                break;

            case 2:            /*pt nid */
	      if (isequal(table_terrain[i][1], 0.))
                    TxDessinerCercle(image_x, lut_g[19], TxVide, point1,
                                     (int) (pas / 2.), 3);
                else
                    TxDessinerCercle(image_x, lut_g[19], TxPlein, point1,
                                     (int) (pas / 2.), 3);
                point1.x = point1.x + 5;
                point1.y = point1.y - 5;
                TxEcrireChaine(image_x, lut_g[19], point1, (char*)"Maison", 0);
                break;
            }
    }

#endif
    (void) image_e; // (unused)
}
