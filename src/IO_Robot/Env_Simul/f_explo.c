/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_explo.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: N. Cuperlier
 - description: propose des mouvements � partir de ceux deja codes par les transitions
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -float vigil

 Global variables:
 -none

 Internal Tools:
 -teleguide()

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <stdlib.h>
#include <libx.h>
#include <time.h>
#include <string.h>
#include "tools/include/local_var.h"
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
/*#define TEST*/
#define DEBUG
void function_explo(int numero)
{
  int deb, i, nb_e;
  float max;
  int gpe = -1, deb_e, count_zero = 0;

  deb = def_groupe[numero].premier_ele;
  /* test si on est en planification*/

  gpe = trouver_entree(numero, (char*) "mvt");
  if (gpe == -1)
  {
    printf("Error group (%d) should have a link named mvt!\n", numero);
    exit(-1);
  }
  else
  {
    deb_e = def_groupe[gpe].premier_ele;
    nb_e = def_groupe[gpe].nbre;
  }

  /*RAZ*/
  max = -10.;
  /*search max*/
  for (i = deb_e; i < nb_e + deb_e; i++)
  {
    if (neurone[i].s1 > max) max = neurone[i].s1;

  }
  /*negatif*/
  for (i = 0; i < nb_e; i++)
  {
    neurone[i + deb].s = neurone[i + deb].s1 = neurone[i + deb].s2 = max - neurone[i + deb_e].s1;
    if (neurone[i + deb].s < max / 4) count_zero++;
  }
  /*if(count_zero>nbre/4)
   for(i=deb;i<nbre+deb;i++)
   neurone[i].s=neurone[i].s1=neurone[i].s2=0;
   */

}
