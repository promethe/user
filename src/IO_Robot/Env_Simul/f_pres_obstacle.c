/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
 /** ***********************************************************
\file  f_move_robot_alea.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-float vigil

Global variables:
-none

Internal Tools:
-teleguide()

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdlib.h>
#include <libx.h>

#include "tools/include/local_var.h"
#include "tools/include/rencontre_quel_mur.h"


void function_pres_obstacle(int numero)
{
float  dx ,dy,posx2,posy2,PI=3.141592653589793238512808959406186204433;
int m=0;
 int deb,nbre,i;
	float angle,scale;
	
float k;

  deb=def_groupe[numero].premier_ele;
  nbre=def_groupe[numero].nbre;

  	for(i=deb;i<nbre+deb;i++)
	neurone[i].s=neurone[i].s1=neurone[i].s2=0;
  	scale=2*PI/nbre;
  	m=0;mirror=0;
     /*recherche du max en entree*/
	for(i=deb;i<nbre+deb;i++)
	{
	
	
 k=0;
/*on attribue au capteur a la position relative i la valeur du lancer de rayon a la pos i*/
  
	angle=  (i-deb) * scale -PI;
		dx=cos((angle))*pas;
  dy=sin((angle))*pas;
 
	posx2 = posx +  (dx);
	posy2 = posy + (dy);
	m=rencontre_quel_mur(posx2,posy2);
	if(m>0)k+=1;
	/* printf("posx:%d,posy:%d,p2x:%d,p2y:%d,angle:%f\n",(int)posx,(int)posy,(int)posx2,(int)posy2,angle);
	if(m>0)
	printf("obstacle found at:%d,%d\n",1,m);*/
	if(m<1){
	posx2 = posx + 2* (dx);
	posy2 = posy + 2*(dy);
	m=rencontre_quel_mur(posx2,posy2);
	if(m>0)k=2;
	/*	printf("obstacle found at:%d,%d\n",2,m);*/
	if(m<1){
	posx2 = posx + 3* (dx);
	posy2 = posy + 3*(dy);
	m=rencontre_quel_mur(posx2,posy2);
	if(m>0)k=3;
	/*printf("obstacle found at:%d,%d\n",3,m);*/
	if(m<1){
	posx2 = posx + 4* (dx);
	posy2 = posy + 4*(dy);
	m=rencontre_quel_mur(posx2,posy2);
	if(m>0)k=4;
	/*printf("obstacle found at:%d,%d\n",3,m);*/
	if(m<1){
	posx2 = posx + 5* (dx);
	posy2 = posy + 5*(dy);
	m=rencontre_quel_mur(posx2,posy2);
	if(m>0)k=5;
	/*printf("obstacle found at:%d,%d\n",3,m);*/
	}
	}
	}
	}
	if(m>0){
if(i<deb+45&&i>deb+15)
mirror=1;
/*	printf("obstacle found at:%d\n",i-deb);*/
		neurone[i].s2=neurone[i].s1=neurone[i].s=1/k;

		/*
		point.x=posx;point.y=posy;pt.x=posx2;pt.y=posy2;
		TxDessinerSegment(&image1,vert, 
										point, pt,2);	*/
/*	printf("posx:%d,posy:%d,p2x:%d,p2y:%d,angle:%f,i:%d,i+offset:%d\n",(int)posx,(int)posy,(int)posx2,(int)posy2,angle,i-deb,i-deb+offset);*/
}	

	m=0;
	}


  
  	
	
}
