/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_move_robot_alea f_move_robot_alea
 

\section Modified 
-Name: C.Giovannangeli
-Created: 01/09/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description



\section Macro
-none

\section Local varirables
-none


\section Global variables
-none

\section Internal Tools
-teleguide()
* 
\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <stdlib.h>
#include <libx.h>

#include "tools/include/local_var.h"

#include "tools/include/teleguide.h"

void function_move_robot_alea(int numero)
{

    int alea_number;
    int compt = 0;
    if (isequal(vigil, 0))
    {
        compt++;
        if (compt == 1)
        {
            vigil = vigilence;
        }
        printf("Vigilance :%f\n", vigil);
    }
    alea_number = (int) (drand48() * 10);
    while ((alea_number == 0) || (alea_number == 5))
    {
        alea_number = (int) (drand48() * 10);
    }
    teleguide(alea_number);
	(void) numero;
}
