/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_variables_essentielles f_variables_essentielles
\brief 
 

\section Modified
- author: C.Hasson
- description: specific file creation
- date: 15/09/2008

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Fonction calculant les niveaux des variables essentielles (pour le moment faim et soif seulement).
Ces niveaux decroissent naturellement dans le temps (consommation de ressources) et remontent lorsque les ressources correspondantes sont detectees.

\section Macro
-none 

\section Local variables
-none


\section Global variables
-none

\section Internal Tools
-mise_a_jour_nutrition_percue()
-mise_a_jour_hidratation_percue()
-mise_a_jour_retour_au_nid()

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
/*#include "include/local_var.h"*/

/*#include "include/mise_a_jour_nutrition_percue.h"*/
#include "tools/include/mise_a_jour_hidratation_percue.h"
#include "tools/include/mise_a_jour_var_essentiel.h"
/*#include "include/mise_a_jour_retour_au_nid.h"*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>

#include <Struct/prom_images_struct.h>
#include <Struct/hough_struct.h>
#include <Struct/decoupage.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*#include <NN_Core/macro_colonne.h>*/

typedef struct
{
  int gpe;/*sauvegarde le numero du groupe precedent*/
} data;


/*fonction d initialisation*/
/*recuperation des parametres*/

void new_variables_essentielles(int Gpe)
{
  int n, link;
  data *my_data;
  my_data = (data *) malloc(sizeof(data));
  /* initialisation de la structure */

#ifdef DEBUG
  dprints("initialisation structure %s %d\n",__FUNCTION__,Gpe);
#endif

  /* Le groupe doit obligatoirement avoir une entree :*/
  n = -1;
  do
    {
      n++;
      link = find_input_link(Gpe, n);
      if (link != -1)
        {
	  if (liaison[link].nom[0] == '-')
            {
	      switch (liaison[link].nom[1])
                {
		  /* definie le numero du groupe en entree */
                case 'r':
		  my_data->gpe = liaison[link].depart;

		  #ifdef DEBUG
		  dprints("supervision input group : %d \n", liaison[link].depart);
		  #endif

		  break;
	
                default:
		  cprints(" link %s not recognized %s \n",
			 liaison[link].nom,__FUNCTION__);
                }
            }
	  else
	    cprints(" link %s not recognized %s \n",
		   liaison[link].nom,__FUNCTION__);
        }
    }
  while (link != -1);
  
  /*on sauvegarde les donnees dans le champ data*/
  def_groupe[Gpe].data = my_data;
  
}

/********************************** fin new_variables_essentielles **********************************/

void function_variables_essentielles(int Gpe)
{
	cprints("function_variables_essentielles: fonction a revoir car mise_a_jour_nutrition_percue et mise_a_jour_hidratation_percue retournent void et non float.\n");
/*
	data *my_data = NULL;
	float max=0.;
	int neurone_gagnant=-1;
	int deb_gpe=-1;
	int taille_gpe=-1;
	int i=0;
	int deb;

	float ingestion_n = 0., ingestion_h = 0. */ /*, retour_nid = 0.*/;
	/*float var_h=0., var_n=0.;*/

	/* getting data from current group structure */
/*	my_data = (data *) def_groupe[Gpe].data;

	if (my_data == NULL)
	{
		EXIT_ON_ERROR("error while loading data in group %d (f_mise_a_jour_var_ess.c)\n", Gpe);
		exit(EXIT_FAILURE);
	}*/

	/*debut et taille du groupe precedent*/
/*	deb_gpe = def_groupe[my_data->gpe].premier_ele;
	taille_gpe = def_groupe[my_data->gpe].nbre;*/

	/*debut du groupe courant*/
/*	deb = def_groupe[Gpe].premier_ele;*/

	/*determine quelle ressource a ete detectee*/
/*	for(i=deb_gpe ; i<deb_gpe+taille_gpe ; i++)
	{
		if(max < neurone[i].s1)
		{
			max = neurone[i].s1;
			neurone_gagnant = i - deb_gpe;
		}
	}
	#ifdef DEBUG
	dprints("neurone_gagnant=%d\n",(int)neurone_gagnant);
	#endif

	if (neurone_gagnant == 0)
	{
		ingestion_h = 1.;
		#ifdef DEBUG
		dprints("\n\ningestion E\n\n");
		#endif
	}
	else
	ingestion_h = 0.;

	if (neurone_gagnant == 1)
	{
		ingestion_n = 1.;
		#ifdef DEBUG
		dprints("\n\ningestion N\n\n");
		#endif
	}
	else
	ingestion_n = 0.;
*/
	/*if (neurone_gagnant == 2)
	{
		retour_nid = 1.;
		#ifdef DEBUG
		dprints("\n\nretour nid\n\n");
		#endif
	}
	else
	retour_nid = 0.;*/

	/*Mise a jour des variables essentielles avec equations percues */
/*	var_n = mise_a_jour_nutrition_percue(ingestion_n, ingestion_h);
	var_h = mise_a_jour_hidratation_percue(ingestion_n, ingestion_h);*/
	/*cprints("\n\nNiveau de nutrition = %f\n\n",var_n);*/
	/*cprints("\n\nNiveau d'hydratation = %f\n\n",var_h);*/
/*
	neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = var_h/200.;
	neurone[deb+1].s = neurone[deb+1].s1 = neurone[deb+1].s2 = var_n/200.;

*/
(void) Gpe;

}
