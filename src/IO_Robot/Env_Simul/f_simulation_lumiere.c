/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
 /** ***********************************************************
\file  f_simulation_lumiere.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/


static int tps;
void function_simulation_lumiere(int numero)
{
    int i = 0;
    int Gpe_X = -1;
    int l = -1;
    char *info = NULL;
    float min = def_groupe[numero].seuil;
    float step = def_groupe[numero].learning_rate;
    float max = 0.;
    float val = 0.;
    int r = 0;
    FILE *ur;


    printf("------------------f_simulation_lumiere.------------------------------\n");

    if (def_groupe[numero].data == NULL)
    {

        tps = 0;
        neurone[def_groupe[numero].premier_ele].d = 0.;
        if (l == -1)
        {
            l = find_input_link(numero, i);
            while (l != -1)
            {
                if (strncmp(liaison[l].nom, "-X", 2) == 0)
                {
                    Gpe_X = liaison[l].depart;
                    info = strstr(liaison[l].nom, "-X-");
                    max = atof(&info[3]);
                    /*printf("max char= %s,max %f \n",&info[3],max); */
                }

                i++;
                l = find_input_link(numero, i);

            }
        }
        if (Gpe_X == -1)
        {
            printf("groupe  Xpostion non trouve\n");
            exit(0);
        }

        def_groupe[numero].data = malloc(sizeof(int));


        *(int *) def_groupe[numero].data = Gpe_X;
        def_groupe[numero].ext = malloc(sizeof(float));


        *(float *) def_groupe[numero].ext = max;
    }
    else
    {

        Gpe_X = *(int *) def_groupe[numero].data;
        max = *(float *) def_groupe[numero].ext;

    }
    tps++;

    printf("ITERATION No %d\n", tps);

    min = def_groupe[numero].seuil;
    /*step=step-min; */


    {
        if (neurone[def_groupe[Gpe_X].premier_ele].s1 >= step)
        {

            neurone[def_groupe[numero].premier_ele].s =
                neurone[def_groupe[numero].premier_ele].s1 =
                neurone[def_groupe[numero].premier_ele].s2 = 0.2;
            printf(" ZONE PREDICTIBLE lumiere predictible\n");

        }
        else if (neurone[def_groupe[Gpe_X].premier_ele].s1 <= min)
        {

            ur = fopen("/dev/urandom", "r");
            r = fgetc(ur);
            fclose(ur);
            val = (float) r / 500;
            neurone[def_groupe[numero].premier_ele].s =
                neurone[def_groupe[numero].premier_ele].s1 =
                neurone[def_groupe[numero].premier_ele].s2 = val;
            printf("ZONE BRUITEE : random=%d , val=%f\n", r,
                   neurone[def_groupe[Gpe_X].premier_ele].s1);

        }
        else
        {
            ur = fopen("/dev/urandom", "r");
            r = fgetc(ur);
            fclose(ur);

            if (r % 2 == 0)
                neurone[def_groupe[numero].premier_ele].s =
                    neurone[def_groupe[numero].premier_ele].s1 =
                    neurone[def_groupe[numero].premier_ele].s2 = 0.2;
            else
                neurone[def_groupe[numero].premier_ele].s =
                    neurone[def_groupe[numero].premier_ele].s1 =
                    neurone[def_groupe[numero].premier_ele].s2 = 0.6;

        }
    }

    printf("lum=%f\n", neurone[def_groupe[numero].premier_ele].s1);
}
