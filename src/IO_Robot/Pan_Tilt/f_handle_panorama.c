/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_handle_panorama.c
\brief

Author: Marwen Belkaid
Created: 15/09/2014

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
The function is used to scan the panorama as it is done in navigation applications (generally in the head_control script).
It returns the next position set to 1. For visual debug, the mask being used is copied to the output, but the activities divided by 2 so that we ensure the next position wins.
Mandatory links:
- -explo-pano: gives the mask used for exploration, i.e. the defaults behavior.
- -curr_pos: gives the current position of the camera in the panorama.
Optional links:
- -flip or -flip1: specifies that during exploration, -explo_mask is flipped horizontally from a scan to another (default = -flip0)
- -learn_mask: gives the mask used to learn
- -vig: specifies the learning phases (when =1)
Warning:
-explo_mask, -learn_mask and -curr_pos must have the same size as the group !
if -learn_mask is used, -vig also should be

Known bugs: none (yet!)

Todo:see authored fortestingreen and commentingreen the function

http://www.doxygen.org
************************************************************/


//#define DEBUG

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_handle_panorama
{
   float *explo_mask;
   float *learn_mask;
   float *prev_mask;
   int gpe_size;
   int explo_mask_gpe;
   int learn_mask_gpe;
   int curr_pos_gpe;
   int vig_gpe;
   int vig_signal;
   int flip ;
   int *done_mask;
   int explo_mask_final_pos ;
   int learn_mask_final_pos ;
} data_handle_panorama;


void new_handle_panorama(int gpe)
{
   data_handle_panorama* mydata ;
   float *tmp_explo_mask, *tmp_learn_mask;
   int *tmp_done_mask;
   int tmp_explo_mask_gpe, tmp_learn_mask_gpe, tmp_gpe_size, tmp_curr_pos_gpe, tmp_vig_gpe, tmp_vig_signal, tmp_flip;
   int i, links_in ;
   char tmp_input_value[1] ;

   tmp_explo_mask = tmp_learn_mask = NULL ;
   tmp_explo_mask_gpe = tmp_learn_mask_gpe = tmp_gpe_size = tmp_vig_gpe = tmp_curr_pos_gpe = -1 ;
   tmp_vig_signal = tmp_flip = 0 ;

   /* Get Input group */
   i = 0 ;
   while((links_in = find_input_link (gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "-explo_mask", NULL) != 0)
         tmp_explo_mask_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-learn_mask", NULL) != 0)
         tmp_learn_mask_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-curr_pos", NULL) != 0)
         tmp_curr_pos_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-vig", NULL) != 0)
         tmp_vig_gpe = liaison[links_in].depart ;
      if(prom_getopt(liaison[links_in].nom, "-flip", tmp_input_value) == 2)
         tmp_flip = atoi(tmp_input_value) ;
      if(prom_getopt(liaison[links_in].nom, "-flip", tmp_input_value) == 1)
         tmp_flip = 1 ;

      i++ ;
   }

   /* Check input groups */
   if(tmp_explo_mask_gpe == -1)
      EXIT_ON_ERROR("The group must have an input for the exploration mask !\n") ;
   if(tmp_curr_pos_gpe == -1)
      EXIT_ON_ERROR("The group must have an input for the current position !\n") ;
   if(tmp_learn_mask_gpe*tmp_vig_gpe < 0)
      EXIT_ON_ERROR("The group must have either both or none of the learn_mask and vig groups !\n") ;

   /* Check groups sizes and allocate temporary masks tables */
   tmp_gpe_size = def_groupe[gpe].nbre ;
   if((def_groupe[tmp_explo_mask_gpe].nbre != tmp_gpe_size)
         || (def_groupe[tmp_curr_pos_gpe].nbre != tmp_gpe_size)
         || (tmp_learn_mask_gpe != -1 && def_groupe[tmp_learn_mask_gpe].nbre != tmp_gpe_size))
      EXIT_ON_ERROR("The group size must equal the mask(s) and the curr_pos groups !\n") ;
   tmp_explo_mask = MANY_ALLOCATIONS(tmp_gpe_size, float);
   tmp_done_mask = MANY_ALLOCATIONS(tmp_gpe_size, int);
   if(tmp_learn_mask_gpe != -1)
      tmp_learn_mask = MANY_ALLOCATIONS(tmp_gpe_size, float);

   /* Create object */
   mydata = ALLOCATION(data_handle_panorama);
   mydata->explo_mask = tmp_explo_mask ;
   mydata->learn_mask = tmp_learn_mask ;
   mydata->prev_mask = NULL ;
   mydata->done_mask = tmp_done_mask ;
   mydata->gpe_size = tmp_gpe_size ;
   mydata->vig_gpe = tmp_vig_gpe ;
   mydata->vig_signal = tmp_vig_signal ;
   mydata->explo_mask_gpe = tmp_explo_mask_gpe ;
   mydata->learn_mask_gpe = tmp_learn_mask_gpe ;
   mydata->curr_pos_gpe = tmp_curr_pos_gpe ;
   mydata->flip = tmp_flip ;
   mydata->explo_mask_final_pos = -1 ;
   mydata->learn_mask_final_pos = -1 ;
   def_groupe[gpe].data = (data_handle_panorama *) mydata;
}



void function_handle_panorama(int gpe)
{
   data_handle_panorama* mydata ;
   float *tmp_explo_mask, *tmp_learn_mask, *tmp_prev_mask;
   int *tmp_done_mask ;
   int tmp_explo_mask_gpe, tmp_learn_mask_gpe, tmp_gpe_size, tmp_curr_pos_gpe, tmp_vig_gpe, tmp_vig_signal, tmp_flip;
   int i, max_i, offset, all_done ;
   float tmp_val ;
   int *tmp_explo_mask_final_pos, *tmp_learn_mask_final_pos ;
   int end_pano ;
   char dynamic_masks = 0 ; // TODO: Handle dynamic masks

   /* init local variables */
   mydata = (data_handle_panorama *) def_groupe[gpe].data ;
   tmp_done_mask = mydata->done_mask ;
   tmp_prev_mask = mydata->prev_mask ;
   tmp_gpe_size = mydata->gpe_size ;
   tmp_vig_gpe = mydata->vig_gpe ;
   tmp_vig_signal = mydata->vig_signal ;
   tmp_explo_mask_gpe = mydata->explo_mask_gpe ;
   tmp_learn_mask_gpe = mydata->learn_mask_gpe ;
   tmp_curr_pos_gpe = mydata->curr_pos_gpe ;
   tmp_flip = mydata->flip ;
   tmp_explo_mask_final_pos = &(mydata->explo_mask_final_pos) ;
   tmp_learn_mask_final_pos = &(mydata->learn_mask_final_pos) ;
   i = max_i = offset = all_done = end_pano = -1 ;

   if(!dynamic_masks)
   {
      /* Complete local variables initialization */
      tmp_explo_mask = mydata->explo_mask ;
      tmp_learn_mask = mydata->learn_mask ;

      /* Copy mask(s) */
      if(tmp_prev_mask == NULL)     // Only done once
      {
         offset = def_groupe[tmp_explo_mask_gpe].premier_ele;
         for(i = 0, tmp_val=99 ; i < tmp_gpe_size ; i++)
         {
            tmp_explo_mask[i] = neurone[offset+i].s1 ;
            if((tmp_explo_mask[i] < tmp_val) && (tmp_explo_mask[i]>0.00001))
            {
               (*tmp_explo_mask_final_pos) = i ;
               tmp_val = tmp_explo_mask[i] ;
            }
         }
#ifdef DEBUG
dprints("%s : explo_mask = ",__FUNCTION__) ;
for(i = 0 ; i < tmp_gpe_size ; i++)
   dprints("%f ", tmp_explo_mask[i]) ;
dprints("\n") ;
#endif
         if(tmp_learn_mask_gpe != -1)
         {
            offset = def_groupe[tmp_learn_mask_gpe].premier_ele;
            for(i = 0, tmp_val=99 ; i < tmp_gpe_size ; i++)
            {
               tmp_learn_mask[i] = neurone[offset+i].s1 ;
               if((tmp_learn_mask[i] < tmp_val) && (tmp_learn_mask[i]>0.00001))
               {
                  (*tmp_learn_mask_final_pos) = i ;
                  tmp_val = tmp_learn_mask[i] ;
               }
            }
#ifdef DEBUG
dprints("%s : learn_mask = ",__FUNCTION__) ;
for(i = 0 ; i < tmp_gpe_size ; i++)
   dprints("%f ", tmp_learn_mask[i]) ;
dprints("\n") ;
#endif
         }
      }

      /* Set learning phase on */
      if((tmp_vig_gpe != -1) && (neurone[def_groupe[tmp_vig_gpe].premier_ele].s1 > 0.5))
         tmp_vig_signal = 1 ;
#ifdef DEBUG
dprints("%s : vig_signal = %d; input vigilance = %f\n",__FUNCTION__, tmp_vig_signal, neurone[def_groupe[tmp_vig_gpe].premier_ele].s1) ;
#endif

      /* Handle panorama */
      if(tmp_vig_signal == 1)   // Learning phase
      {
         /* Check which part of learn_mask has already been done */
         if(tmp_prev_mask != tmp_learn_mask)    // if we switch from exploration to learning, just re-initialize done_mask and reset prev_mask
         {
            for(i = 0 ; i < tmp_gpe_size ; i++)
                  tmp_done_mask[i] = 0 ;
            tmp_prev_mask = tmp_learn_mask ;
         }
         else                                   //otherwise, update done_mask with current position
         {
            offset = def_groupe[tmp_curr_pos_gpe].premier_ele;
            for(i = 0 ; i < tmp_gpe_size ; i++)
               if(neurone[offset+i].s1 > 0.99)
                  tmp_done_mask[i] = 1 ;
         }
         /* Set group output */
         offset = def_groupe[gpe].premier_ele;
         for(i = 0 ; i < tmp_gpe_size ; i++) // Copy learn_mask rescaled between 0 and 0.5 for visual debug
            neurone[offset+i].s1 = tmp_learn_mask[i]/2 ;
         for(i = 0, max_i = -1 ; i < tmp_gpe_size ; i++) // Look for next target position
            //if((tmp_learn_mask[i] > tmp_learn_mask[max_i]) && (tmp_done_mask[i] != 1))
            //   max_i = i ;
            if((tmp_learn_mask[i] > 0.00001) && (tmp_done_mask[i] != 1))
               if((max_i < -0.00001) || (tmp_learn_mask[i] > tmp_learn_mask[max_i]))
                  max_i = i ;
         if((max_i < tmp_gpe_size) && (tmp_learn_mask[max_i] > 0.00001) && (tmp_done_mask[max_i] == 0))
            neurone[offset+max_i].s1 = 1. ; // Set next target position to 1
         else
            tmp_vig_signal = 0 ;            // Switch to exploration phase (learning is completed)
         if (max_i == (*tmp_learn_mask_final_pos))
            end_pano = 1 ;
         else
            end_pano = 0 ;
      }
      if(tmp_vig_signal == 0)   // Exploration phase
      {
         /* Check which part of explo_mask has already been done */
         if(tmp_prev_mask != tmp_explo_mask)    // if we switch from learning to exploration, just re-initialize done_mask and reset prev_mask
         {
            for(i = 0 ; i < tmp_gpe_size ; i++)
                  tmp_done_mask[i] = 0 ;
            tmp_prev_mask = tmp_explo_mask ;
         }
         else                                   // otherwise, check if we have to flip explo_mask or update done_mask
         {
            offset = def_groupe[tmp_curr_pos_gpe].premier_ele;
            for(i = 0 ; i < tmp_gpe_size ; i++)
               if(neurone[offset+i].s1 > 0.99)
                  tmp_done_mask[i] = 1 ;
            for(i = 0, all_done = 1 ; (i < tmp_gpe_size) && (all_done == 1) ; i++)
               if((tmp_done_mask[i] == 0) && (tmp_explo_mask[i] > 0.00001)) // If there's still a position in the mask that's hasn't been done yet
                  all_done = 0 ;
            if(all_done)
            {
               for(i = 0 ; i < tmp_gpe_size/2 ; i++)
               {
                  /* Re-initialize done_mask  */
                  tmp_done_mask[i] = tmp_done_mask[tmp_gpe_size-i-1] = 0 ;
                  tmp_done_mask[(int)tmp_gpe_size/2] = 0 ; // handle center when tmp_gpe_size is odd

                  if(tmp_flip == 1)
                  {
                     /* Permute values */
                     tmp_val = tmp_explo_mask[i] ;
                     tmp_explo_mask[i] = tmp_explo_mask[tmp_gpe_size-i-1] ;
                     tmp_explo_mask[tmp_gpe_size-i-1] = tmp_val ;
                  }
               }

               /* Change final pos */
               (*tmp_explo_mask_final_pos) = tmp_gpe_size - (*tmp_explo_mask_final_pos) - 1 ;
            }
#ifdef DEBUG
dprints("%s : all_done = %i\n",__FUNCTION__, all_done) ;
for(i = 0 ; i < tmp_gpe_size ; i++)
   dprints("%i ", tmp_done_mask[i]) ;
dprints("\n") ;
#endif
         }
         /* Set group output */
         offset = def_groupe[gpe].premier_ele;
         for(i = 0 ; i < tmp_gpe_size ; i++) // Copy explo_mask rescaled between 0 and 0.5 for visual debug
            neurone[offset+i].s1 = tmp_explo_mask[i]/2 ;
         for(i = 0, max_i = -1 ; i < tmp_gpe_size ; i++) // Look for next target position
            if((tmp_explo_mask[i] > 0.00001) && (tmp_done_mask[i] != 1))
               if((max_i < -0.00001) || (tmp_explo_mask[i] > tmp_explo_mask[max_i]))
                  max_i = i ;
         if((max_i < tmp_gpe_size) && (tmp_explo_mask[max_i] > 0.00001))
            neurone[offset+max_i].s1 = 1. ;// Set next target position to 1
         if (max_i == (*tmp_explo_mask_final_pos))
            end_pano = 1 ;
         else
            end_pano = 0 ;
      }
      /* Copy output to s and s2 */
      for(i = 0 ; i < tmp_gpe_size ; i++)
         neurone[offset+i].s = neurone[offset+i].s2 = neurone[offset+i].s1 ;
      neurone[offset].d = (float)end_pano;
   }

  /* Update object */
   mydata->prev_mask = tmp_prev_mask ;
   def_groupe[gpe].data = (data_handle_panorama *) mydata;
}


void destroy_handle_panorama(int gpe)
{
   dprints("destroy %s(%s) \n", __FUNCTION__,def_groupe[gpe].no_name);
   if (def_groupe[gpe].data != NULL)
   {
     free(((data_handle_panorama *) def_groupe[gpe].data));
     def_groupe[gpe].data = NULL;
   }
}


