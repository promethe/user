/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_activ_joint.c 
 \brief 

 Author: Antoine de Rengerve
 Created: 
 Modified: 
 - author:
 - description: 
 - date: 

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 
 - active ou desactive les moteurs du bras Katana
 Attention a la securite du bras !
 -s0/1 (1 par defaut) : 1 securite active : on demande confirmation avant 
 activation ou desactivation
 0 securite desactive : la transition est effective
 directement
 Les moteurs sont actives ou desactives selon les liens en entree. 
 -m0/1 (0 par defaut) : 1 avec memoire : le groupe se souvient du dernier ordre passe et 
 s'autorise a ne pas relancer deux fois 
 le meme ordre.
 0 sans memoire : le groupe passera toujours la commande  
 d'activation / desactivation sans se poser de questions.
 Rq : On n'a pas implemente de fonction dans le broker pour recuperer l'etat actif ou 
 inactif des moteurs plutôt que d'utiliser une memoire. 
 (pour le bras Katana : Une telle fonction existe-t-elle dans KNI ?)


 elargissement a tous les joints : on doit passer dans un fichier (-fxxx) les noms des joints qui sont controlles. Ex: legLF3, moteur1,...

 Interface utilisateur :
 y : autorise l'envoi de la commande en activation pour le moteur a l'index cite
 n : refuse l'envoi de la commande en activation pour le moteur a l'index cite
 yAll :  autorise l'envoi de commande en activation pour tous les moteurs suivants
 nAll : refuse l'envoi de commande en activation pour tous les moteurs suivants


 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx


 Known bugs: none found yet...

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>

#define DEBUG

#include <net_message_debug_dist.h>

/* precision pour la memoire */
#define EQUAL_EPS 0.05

typedef struct {
  int longueur; /* longueur de la chaine de neurones (chaque neurone est une expression) */
  int deb; /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int inputGpe; /* groupe d'entree pour la liaison en arriere */
  Joint *joint_servo; /* structure de promethe */
  float *neurone_passe;
  int security;
  int memoire; /* se souvient des etats passe et agit en consequence */
  char **joints;
  int nb_joints;
} myData;

void new_activ_joint(int Gpe)
{
  FILE *f;
  myData *data; /* structure de cette fonction */
  int i = 0;
  int security = 1;
  int memoire = 0;
  char param[256];
  char file_name[256];
  int error;

  dprints("enter in %s\n", __FUNCTION__);

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = (myData *) malloc(sizeof(myData));

  if (data == NULL)
  {
    printf("error in memory allocation in group %d %s\n", Gpe, __FUNCTION__);
    exit(EXIT_FAILURE);
  }

  i = find_input_link(Gpe, 0);

  if (i == -1)
  {
    printf(" Error, %s group %i doesn't have an input\n", __FUNCTION__, Gpe);
    exit(EXIT_FAILURE);
  }

  if (prom_getopt(liaison[i].nom, "-s", param) == 2)
  {
    security = atoi(param);

    if (security != 0 && security != 1)
    {
      security = 1;
      dprints("f_activ_joint (%d) : securite activee par defaut\n", Gpe);
    }
  }
  else
  {
    security = 1;
    dprints("f_activ_joint (%d) : securite activee par defaut\n", Gpe);
  }

  if (prom_getopt(liaison[i].nom, "-m", param) == 2)
  {
    memoire = atoi(param);

    if (memoire != 1)
    {
      memoire = 0;
      dprints("f_activ_joint (%d) : memoire activee\n", Gpe);
    }
    else
    {
      dprints("f_activ_joint (%d) : memoire desactivee\n", Gpe);
      memoire = 1;
    }
  }
  else
  {
    memoire = 0;
    dprints("f_activ_joint (%d) : pas de memoire\n", Gpe);
  }

  if (prom_getopt(liaison[i].nom, "-f", param) == 2)
  {
    strcpy(file_name, param);
  }
  else
  {
    cprints("f_activ_joint (%d) : pas de fichier pour les noms des joints geres !!\n", Gpe);
    exit(1);
  }

  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->inputGpe = liaison[i].depart;
  data->deb = def_groupe[data->inputGpe].premier_ele;
  data->longueur = def_groupe[data->inputGpe].nbre;
  data->security = security;
  data->memoire = memoire;

  data->nb_joints = data->longueur;

  data->neurone_passe = (float*) malloc(data->longueur * (sizeof(float)));
  if (data->neurone_passe == NULL)
  {
    printf("Erreur d'allocation neurone_passe\n");
    exit(1);
  }

  data->joints = (char**) malloc(data->nb_joints * (sizeof(char*)));
  if (data->joints == NULL)
  {
    printf("Erreur d'allocation joints\n");
    exit(1);
  }
  /* ouverture du fichier contenant les noms des joints */
  f = fopen(file_name, "r");
  if (f == NULL)
  {
    printf("f_activ_joint(%s):ERROR unable to open file %s \n", def_groupe[Gpe].no_name, file_name);
    exit(0);
  }

  /*on alloue le tableau qui memorise les noms des joints supervises */
  for (i = 0; i < data->nb_joints; i++)
  {
    error = fscanf(f, "%s", param);
    if (error == EOF || error == 0)
    {
      printf("Not enough joint name \n");
      printf("error after reading %d values \n", i);
      exit(1);
    }
    printf("%s!\n", param);
    data->joints[i] = strdup(param);
    if (data->joints[i] == NULL)
    {
      printf("Failure while duplicating the joint name\n");
      exit(1);
    }

    printf("%s!\n", data->joints[i]);
  }

  /*on alloue le tableau qui memorise la valeur de la position passee */
  for (i = 0; i < data->longueur; i++)
    data->neurone_passe[i] = 1.; /* après la calibration, tous les moteurs sont actifs*/

  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;

  fclose(f);

  dprints("end of %s\n", __FUNCTION__);

}

/*
 *
 *FIN DU CONSTRUCTEUR
 *
 */

void function_activ_joint(int Gpe)
{
  /* initialisation des variables */
  int i;
  myData *my_data = NULL; /* la structure de cette fonction */
  /*  char moteur[NUM_SERVOS][256]={"motor1","motor2","motor3","motor4","motor5","motor6"}; */
  int deb;
  int pass, ask;
  float activation;
  int rep;
  char answ[10];
  /* fin de l'initialisation des variables */

  dprints("enter in %s, group %d\n", __FUNCTION__, Gpe);

  my_data = (myData *) def_groupe[Gpe].data; /* recuperer les donnes du constructeur */
  if (my_data == NULL)
  {
    cprints("error while loading data in group %d %s\n", Gpe, __FUNCTION__);
    exit(EXIT_FAILURE);
  }
  deb = my_data->deb;

  /* demande d'autorisation */
  ask = 1;
  pass = -1;

  if (my_data->security == 0)
  {
    pass = 1; /* pas de securite, autorisation accordee */
    ask = 0;
  }

  /* donner des commandes a chaque servo */
  for (i = 0; i < my_data->nb_joints; i++)
  {

    printf("[%s](%d)\n", my_data->joints[i], i);

    my_data->joint_servo = NULL; /*inutile ?*/
    /* mis en forme (structure Joint) pour appeller la fonction joint_servo_command */
    my_data->joint_servo = joint_get_serv_by_name(my_data->joints[i]);

    activation = neurone[deb + i].s;

    printf("n passe : %f activation : %f\n", my_data->neurone_passe[i], activation);

    if ((my_data->memoire == 0 || fabs(activation - my_data->neurone_passe[i]) > EQUAL_EPS) && ask == 1)
    /* (sans memoire ou modif) + autorisation en attente */
    {
      /* autorisation de modification */
      if (my_data->memoire) cprints("(Groupe %d) Attention ! : voulez vous changer l'activation de %s \n(index %f - memoire %d -> %f : 0->OFF, 1->ON) ? (y/yAll/n/nAll)\n", Gpe, my_data->joints[i], i, my_data->neurone_passe[i], activation);
      else cprints("(Groupe %d) Attention ! : voulez vous changer l'activation de %s \n(index %d -> %f : 0->OFF, 1->ON) ? (y/yAll/n/nAll)\n", Gpe, my_data->joints[i], i, activation);

      rep = 0;
      while (rep == 0)
      {
        rep = 1;
        cprints(">");
        cscans("%s", answ);
        if (strcmp(answ, "y") == 0) pass = 1; /* autorisation accordee*/
        else if (strcmp(answ, "n") == 0) pass = 0; /* autorisation refusee */
        else if (strcmp(answ, "yAll") == 0)
        {
          pass = 1; /* autorisation accordee pour tous ceux qui restent */
          ask = 0;
        }
        else if (strcmp(answ, "nAll") == 0)
        {
          pass = 0; /* autorisation refusee pour tous ceux qui restent*/
          ask = 0;
        }
        else
        {
          cprints("(Groupe %d) : reponse non reconnue\n", Gpe);
          rep = 0;
        }
      }
    }

    if ((my_data->memoire == 0 || fabs(activation - my_data->neurone_passe[i]) > EQUAL_EPS) && pass == 1)
    /* (sans memoire ou modif) + passage autorise */
    {
      printf("activation %f\n", activation);
      joint_activate_servo(my_data->joint_servo, activation);
      my_data->neurone_passe[i] = activation;
    }

    dprints("neurone_input[%d]=%f, pass %d, activ %d\n", i, neurone[deb + i].s, pass, (int) my_data->neurone_passe[i]);
  }

  if (pass != -1 && my_data->security != 0) cprints("..\n");

  dprints("end of %s, group %d\n", __FUNCTION__, Gpe);
}
