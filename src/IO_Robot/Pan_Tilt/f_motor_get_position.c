/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************

 Author: Arnaud Blanchard
 Created: 20/07/2009

 Description:
 - Send a vector of position to the motors depending definition in .dev.
 Incoming links:
 - position

 */

#include <libx.h>
#include <net_message_debug_dist.h>
#include <dev.h>
#include <device.h>
#include <Components/motor.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define MAXIMUM_SIZE_OF_OPTION 128

typedef struct box_motor_get_position {
  Device *device;
  type_motor **motors;
  int number_of_motors;
  type_neurone* neurons;
} Box_motor_get_position;

void new_motor_get_position(int index_of_group)
{
  Box_motor_get_position *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
  Device *device;
  int index_of_link, success;

  type_groupe *group;
  type_liaison *link;

  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box_motor_get_position);
  group->data = box;

  /** On recherche le lien entrant s'il existe. */
  index_of_link = find_input_link(index_of_group, 0);
  if (index_of_link == -1) link = NULL;
  else link = &liaison[index_of_link];
  if (find_input_link(index_of_group, 1) != -1) EXIT_ON_ERROR("Group: %d, has more than one incoming link !", index_of_group);

  /** On recherche le parametre id sur le lien  s'il y en a un.*/
  if (link != NULL)
  {
    success = prom_getopt(link->nom, "id=", id_of_device);
    if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);
  }
  else success = 0;

  /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
  device = 0;
  if (success == 2) device = dev_get_device("motor", id_of_device);
  else if (success == 0) device = dev_get_device("motor", NULL);
  else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

  /** On verifie que le nombre de moteurs de l'appareil equivaut a celui du nombre de neurones du groupe*/
  if (device->number_of_components != group->nbre) EXIT_ON_ERROR("Number of motors (%d) different from number of neurons (%d).", device->number_of_components, group->nbre);
  else box->number_of_motors = device->number_of_components;

  /** On defini les neurones de la boite. */
  box->neurons = &neurone[group->premier_ele];

  /** On defini les moteurs de la boite*/
  box->motors = (type_motor**) device->components;
}

void function_motor_get_position(int index_of_group)
{
  Box_motor_get_position *box;
  int i;
  type_motor **motors;
  type_neurone *neurons_of_the_box;

  /** On recupere la boite a partir du groupe*/
  box = (Box_motor_get_position*) def_groupe[index_of_group].data;

  /** On recupere les neurones de la boite et les moteurs*/
  neurons_of_the_box = box->neurons;
  motors = box->motors;

  /** On recupere la position pour chaque moteur */
  for (i = 0; i < box->number_of_motors; i++)
  {
    motors[i]->get_position(motors[i], &neurons_of_the_box[i].s1);
    neurons_of_the_box[i].s = neurons_of_the_box[i].s1;
    neurons_of_the_box[i].s2 = neurons_of_the_box[i].s1;
  }
}

void destroy_motor_get_position(int index_of_group)
{
  free(def_groupe[index_of_group].data);
}

