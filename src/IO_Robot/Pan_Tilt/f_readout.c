/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*#define DEBUG 1*/
#include <libx.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libhardware.h>
#include "net_message_debug_dist.h"

struct READOUT_STRUCT {
  int gpe_pos;
  int gpe_dphi;
};

void function_readout(int num)
{
  int lien;
  struct READOUT_STRUCT *data = NULL;
  int gpe_pos = 0;
  int gpe_dphi = 0;
  int val_pos = 0;
  int i = 0;

  if (def_groupe[num].data == NULL)
  {
    if ((data = def_groupe[num].data = malloc(sizeof(struct READOUT_STRUCT))) == NULL)
    {
      fprintf(stderr, "f_readout : allocation de la structure de donnee impossible.\n");
      exit(-1);
    }
    for (lien = 0; lien < nbre_liaison; lien++)
      if (liaison[lien].arrivee == num)
      {
        if (!strcmp(liaison[lien].nom, "pos")) gpe_pos = data->gpe_pos = liaison[lien].depart;
        else if (!strcmp(liaison[lien].nom, "dphi")) gpe_dphi = data->gpe_dphi = liaison[lien].depart;
      }
  }
  else
  {
    data = (struct READOUT_STRUCT *) def_groupe[num].data;
    gpe_pos = data->gpe_pos;
    gpe_dphi = data->gpe_dphi;
  }

  for (i = 0; i < def_groupe[gpe_pos].nbre; i++)
    if ((int) neurone[def_groupe[gpe_pos].premier_ele + i].s2 == 1) break;

  val_pos = i;

  dprints("f_readout : pos_absolu = %i\n", val_pos);

  neurone[def_groupe[num].premier_ele].s = neurone[def_groupe[num].premier_ele].s1 = neurone[def_groupe[num].premier_ele].s2 = neurone[(def_groupe[gpe_dphi].premier_ele + val_pos)].s2;

  dprints("f_readout : vitesse = %f\n", neurone[def_groupe[num].premier_ele].s2);
}
