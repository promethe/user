/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************

 Author: Raphaël Braud
 Created: 07/06/2012

 Description:
 - Send a vector of activation to the motors depending definition in .dev.
 Incoming links:
 - position

 */

#include <net_message_debug_dist.h>
#include <libx.h>
#include <dev.h>
#include <device.h>
#include <Components/motor.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#define MAXIMUM_SIZE_OF_OPTION 128

typedef struct box_motor_set_activation {
  Device *device;
  type_motor **motors;
  int number_of_motors;
  type_neurone *neurons_of_command, *neurons_of_enable;
} Box_motor_set_activation;

/** Fonctions static car non accessible de l'exterieur (sorte de private) */
static int link_start_with_name(type_liaison *link, const char *name)
{
  if (strncmp(link->nom, name, strlen(name)) == 0) return 1;
  else return 0;
}

void new_motor_set_activation(int index_of_group)
{
  Box_motor_set_activation *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
  int index_of_link, success, number_of_links;

  type_groupe *group;
  type_liaison *link;

  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box_motor_set_activation);
  group->data = box;

  box->neurons_of_enable = NULL;
  box->neurons_of_command = NULL;

  /** On recherche les liens entrants */
  for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];

    if (link_start_with_name(link, "enable"))
    {
      box->neurons_of_enable = &neurone[def_groupe[link->depart].premier_ele];
    }
    else if (link_start_with_name(link, "command"))
    {
      box->neurons_of_command = &neurone[def_groupe[link->depart].premier_ele];

      /** On recherche le parametre id sur le lien */
      success = prom_getopt(link->nom, "id=", id_of_device);
      if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

      /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
      if (success == 2) box->device = dev_get_device("motor", id_of_device);
      else if (success == 0) box->device = dev_get_device("motor", NULL);
      else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

      /** On verifie que le nombre de moteurs de l'appareil equivaut a LA MOITIE celui du nombre de neurones entrant : a1 et a2 pour chaque moteur*/
      if (2 * box->device->number_of_components != def_groupe[link->depart].nbre) EXIT_ON_ERROR("Number of motors (%d) different from number of input neurons (%d).", box->device->number_of_components, def_groupe[link->depart].nbre);
      else box->number_of_motors = box->device->number_of_components;
    }
    else if (!link_start_with_name(link, "sync")) EXIT_ON_ERROR("Group: %s, type of link unknowm: %s. The possibilities are :'command', 'enable' and 'sync'.", group->no_name, link->nom);
  }
  if (box->neurons_of_command == NULL) EXIT_ON_ERROR("Group: %s there is no 'command' link.", group->no_name);

  /** On defini les moteurs de la boite*/
  box->motors = (type_motor**) box->device->components;
}

void function_motor_set_activation(int index_of_group)
{
  Box_motor_set_activation *box = NULL;

  int enabled, i;
  type_motor **motors;
  type_neurone *neurons_of_command;

  /** On recupere la boite a partir du groupe*/
  box = (Box_motor_set_activation*) def_groupe[index_of_group].data;

  if (box->neurons_of_enable == NULL) enabled = 1;
  else if (box->neurons_of_enable[0].s1 >= 0.5) enabled = 1;
  else
  {
    enabled = 0;
  }

  if (enabled)
  {
    /** On recupere les neurones entrant dans la boite et les moteurs*/
    neurons_of_command = box->neurons_of_command;
    motors = box->motors;

    /** On defini l'activite antagoniste pour chaque moteur */
    for (i = 0; i < box->number_of_motors; i++)
    {
      motors[i]->set_activation(motors[i], neurons_of_command[2 * i].s1, neurons_of_command[2 * i + 1].s1);
    }

    /** On demande a l'appareil d'envoyer tout le buffer */
    box->device->flush(box->device);
  }

}

void destroy_motor_set_activation(int index_of_group)
{
  free(def_groupe[index_of_group].data);
}

