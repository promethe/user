/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_joint.c 
 \brief 

 Author: Benoit Mariat
 Created: 03/03/2005
 Modified:
 - author: -
 - description: -
 - date: -

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 
 -send a command to a servo,
 the command is a percentage, it is define by the neuron of the precedent group
 the servo is identify by its name, this name is on the link

 Macro:
 -none 

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <Struct/prom_images_struct.h>*/

#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

void function_joint_population(int Gpe)
{
  int lien = -1;
  int inputGpe, deb_input, deb;
  int i;
  int nb;
  float val_max = -999999, pos = -99999;
  Joint *servo = NULL;

  lien = find_input_link(Gpe, 0);
  if (lien == -1)
  {
    printf("%s : Gpe : %d : Absence de  lien entrants...\n", __FUNCTION__, Gpe);
    exit(0);
  }

  servo = joint_get_serv_by_name(liaison[lien].nom);
  inputGpe = liaison[lien].depart;
  deb_input = def_groupe[inputGpe].premier_ele;
  nb = def_groupe[inputGpe].nbre;
  deb = def_groupe[Gpe].premier_ele;

  for (i = 0; i < nb; i++)
  {
    if (neurone[deb_input + i].s1 > val_max)
    {
      val_max = neurone[deb_input + i].s1;
      pos = i;
    }
  }
  if (val_max > neurone[deb].seuil)
  {
    joint_servo_command(servo, pos / nb, -1);
    neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = pos / nb;
  }
#ifdef DEBUG
  printf("position demandee: %f\n", pos);
#endif
  return;
}
