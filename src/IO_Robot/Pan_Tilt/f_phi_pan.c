/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/mem_entree.h>
#include <libhardware.h>
#include <Global_Var/IO_Robot.h>
void function_phi_pan(int num)
{

  int i, num_e = 0, cpt, entree;
  /*int size_Y = def_groupe[num].tailley;
   int size_X = def_groupe[num].taillex;
   int size = (size_X * size_Y);
   int first_neuron = def_groupe[num].premier_ele;
   int last_neuron  = def_groupe[num].premier_ele + size;
   */int /*centre, pos_d_phi,*/delta;
  char *string;
  mem_entree *data_g;

  printf("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ", __FUNCTION__);
  exit(0);

  /*! recherche  des liens */
  if (def_groupe[num].ext == NULL)
  {
    cpt = 0;
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == num)
      {
        string = liaison[i].nom;
        if (strstr(string, "pan") != NULL)
        {
          num_e = i;
          cpt++;
        }
      }
    if (cpt != 1)
    {
      printf("\n function_phi_pan() erreur: pas de  lien corrects detecte !\n ");
      exit(0);
    }

    data_g = (mem_entree *) malloc(sizeof(mem_entree));
    (*data_g).entree_1 = entree = liaison[num_e].depart;
    def_groupe[num].ext = (void *) data_g;
  }
  else
  {
    data_g = (mem_entree *) def_groupe[num].ext;
    entree = (*data_g).entree_1;
  }

  /* position de l'origine sur d_phi */
  /*
   size_d_phi= def_groupe[entree].taillex *def_groupe[entree].tailley;
   centre = (int) (float)((float)(254-(get_PTposition()).servo_pan)+(float)(get_movepan()/2))*size_d_phi /((float)get_movepan()+255);
   */
  /* on recupere la valeur du neurone  de d_phi correspondant */

  /*pos_d_phi = def_groupe[entree].premier_ele + centre;*/

  /*printf("phi_pan: neurone %d  valeur %f ",centre,neurone[pos_d_phi].s2 ) ; */
  /*delta =(int) (-1.0*amplitude*(neurone[pos_d_phi].s2)*get_pan_max_delta()); */
  if (abs(delta) > 10)
  {
    /* new_pos= (get_PTposition()).servo_pan+delta;*/
    /*pan(new_pos); */}

}
