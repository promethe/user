/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************

 Author: Arnaud Blanchard
 Created: 20/07/2009

 Description:
 - Send a vector of position to the motors depending definition in .dev.
 Incoming links:
 - position

 */
/* #define DEBUG */
#include <net_message_debug_dist.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <dev.h>
#include <Components/motor.h>
#include <time.h>

#define MAXIMUM_SIZE_OF_OPTION 128



typedef struct box {
  Device *device;
  type_motor **motors;
  int number_of_motors;
  type_neurone *neurons_of_command, *neurons_of_enable;
  int gpe_wait;
  float *float_buffer;
  int float_buffer_size;
} Box;

/** Fonctions static car non accessible de l'exterieur (sorte de private) */
static int link_start_with_name(type_liaison *link, const char *name)
{
  if (strncmp(link->nom, name, strlen(name)) == 0) return 1;
  else return 0;
}

void new_motor_set_position(int index_of_group)
{
  Box *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
  int index_of_link, success, number_of_links, component_id;

  type_groupe *group;
  type_liaison * link;

  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box);
  group->data = box;

  /** Valeurs par defaut */
  box->neurons_of_enable = NULL;
  box->neurons_of_command = NULL;
  box->gpe_wait = -1;

  /** On recherche les liens entrants */
  for (number_of_links = 0; (index_of_link = find_input_link(index_of_group, number_of_links)) != -1; number_of_links++)
  {
    link = &liaison[index_of_link];

    if (link_start_with_name(link, "wait"))
    {
      box->gpe_wait = link->depart;
    }
    else if (link_start_with_name(link, "enable"))
    {
      box->neurons_of_enable = &neurone[def_groupe[link->depart].premier_ele];
    }
    else if (link_start_with_name(link, "command"))
    {
      box->neurons_of_command = &neurone[def_groupe[link->depart].premier_ele];

      /** On recherche le parametre id sur le lien */
      success = prom_getopt(link->nom, "id=", id_of_device);
      if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

      /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
      if (success == 2) box->device = dev_get_device("motor", id_of_device);
      else if (success == 0) box->device = dev_get_device("motor", NULL);
      else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

      /** On verifie que le nombre de moteurs de l'appareil equivaut a celui du nombre de neurones entrant*/

      if (box->device->total_elements_nb != def_groupe[link->depart].nbre) EXIT_ON_ERROR("Number of motors (%d) different from number of input neurons (%d).", box->device->number_of_components, def_groupe[link->depart].nbre);
      else box->number_of_motors = box->device->number_of_components;
    }
    else if (!link_start_with_name(link, "sync")) EXIT_ON_ERROR("Group: %s, type of link unknowm: %s. The possibilities are :'command', 'enable' and 'sync'.", group->no_name, link->nom);
  }
  if (box->neurons_of_command == NULL) EXIT_ON_ERROR("Group: %s there is no 'command' link.", group->no_name);

  /** On defini les moteurs de la boite qui sont tous les composants du peripherique*/
  box->motors = (type_motor**) box->device->components;

  /** On s'assure que le buffer de float sera suffisant pour n'importe quel composant. A generaliser. */
  for (component_id = 0; component_id < box->device->number_of_components; component_id++)
  {
    if (box->motors[component_id]->elements_nb > box->float_buffer_size)
    {
      free(box->float_buffer);
      box->float_buffer_size = box->motors[component_id]->elements_nb;
      box->float_buffer = MANY_ALLOCATIONS(box->float_buffer_size, float);
    }
  }
}

void function_motor_set_position(int index_of_group)
{
  Box *box;
  int enabled, i, element_id, neuron_id = 0;
  float pos, proprio;
  type_motor **motors;
  type_neurone *neurons_of_command;
  struct timespec duree_nanosleep;
  double wait_nano = 0, wait_nano_max = 0, wait_factor, servo_speed = -1;
  float *float_buffer=NULL;
  /** On recupere la boite a partir du groupe */
  box = (Box*) def_groupe[index_of_group].data;

  if (box->neurons_of_enable == NULL) enabled = 1;
  else if (box->neurons_of_enable[0].s1 >= 0.5) enabled = 1;
  else enabled = 0;

  if (enabled)
  {
    /** On recupere les neurones entrant dans la boite et les moteurs*/
    neurons_of_command = box->neurons_of_command;
    motors = box->motors;
	float_buffer=box->float_buffer;
    /** On defini la position pour chaque type de moteur */
    for (i = 0; i < box->number_of_motors; i++)
    {
      if (box->gpe_wait != -1) /* Tout ça devrait être gérer ailleurs */
      {
        /* La relation lineaire entre temps d'attente et distance a parcourir n'est peut etre pas la meilleure (log ?) */

        pos = neurons_of_command[i].s1;
        motors[i]->get_position(motors[i], &proprio);

        wait_nano = sqrt(fabs(pos - proprio)); /*PK sqrt(fabs(pos - proprio))?? soit fabs(pos - proprio) soit sqrt((pos - proprio)*(pos - proprio))*/

        dprints("wait nano is %f\n", wait_nano);

        /** on attend le temps le plus long pour les differents moteurs controlles */
        if (wait_nano > wait_nano_max) wait_nano_max = wait_nano;
      }

      for (element_id = 0; element_id < motors[i]->elements_nb; element_id++)
      {
        float_buffer[element_id] = neurons_of_command[neuron_id].s1;
        neuron_id++;
      }

      motors[i]->set_position(motors[i], float_buffer);
    }

    /** On demande a l'appareil d'envoyer tout le buffer */
    box->device->flush(box->device);

    /* Attente de fin du mouvement basee sur la vitesse du servo */
    if (box->gpe_wait != -1)
    {
      servo_speed = 4;/** TODO get Vdefault from .dev : servo->Vdefault; */
      dprints("%s (%s): Speed of joint is %i\n", __FUNCTION__, def_groupe[index_of_group].no_name, servo_speed);
      if (servo_speed <= 0)
      {
        cprints("WARNING in f_motor_set_position(%s): Wait option active but Vdefault not set for the joint => cannot calculate wait time\n", def_groupe[index_of_group].no_name);
      }
      else
      {
        /* test avec une racine carre */
        wait_factor = neurone[def_groupe[box->gpe_wait].premier_ele].s1;
        wait_nano = wait_nano_max * 4000000000 * wait_factor / servo_speed;
        duree_nanosleep.tv_sec = (long) wait_nano / 1000000000;
        duree_nanosleep.tv_nsec = (long) wait_nano % 1000000000;

        dprints("f_servo_motor(%s): Waiting for %i seconds, %i nanoseconds\n", def_groupe[index_of_group].no_name, duree_nanosleep.tv_sec, duree_nanosleep.tv_nsec);
        nanosleep(&duree_nanosleep, NULL);
      }
    }
  }
}

void destroy_motor_set_position(int index_of_group)
{
  Box *box;
  box = (Box*) def_groupe[index_of_group].data;
  box->float_buffer_size=0; //pour le principe
  free(box->float_buffer);
  box->float_buffer=NULL;
  free(box);
  def_groupe[index_of_group].data=NULL;
}
