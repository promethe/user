/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_pantilt_move_position.c
 \brief 

 Author: J. Hirel
 Created: 01/05/2009
 Modified:
 - author:
 - description: 
 - date: 

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 

 Macro:
 -none 

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()

 Links:
 - type: algo
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

/*#define DEBUG*/

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <libhardware.h>

typedef struct data_pantilt_move_position {
  int nb_axes;
  type_liaison *axes[10];
  PanTilt *pantilt;
} data_pantilt_move_position;

void new_pantilt_move_position(int gpe)
{
  int i, l;
  char string[256];
  PanTilt *pantilt = NULL;
  char pantilt_name[256] = "\0";
  data_pantilt_move_position *my_data = NULL;

  dprints("new_pantilt_move_position(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data == NULL)
  {
    my_data = (data_pantilt_move_position *) malloc(sizeof(data_pantilt_move_position));

    if (my_data == NULL)
    {
      fprintf(stderr, "ERROR in new_pantilt_move_position(%s): malloc failed for data\n", def_groupe[gpe].no_name);
      exit(1);
    }

    my_data->nb_axes = 0;

    l = 0;
    i = find_input_link(gpe, l);
    while (i != -1)
    {
      if (prom_getopt(liaison[i].nom, "p", string) == 2)
      {
        dprints("new_pantilt_move_position(%s): Pantilt name is %s\n", def_groupe[gpe].no_name, string);
        strcpy(pantilt_name, string);
      }
      else
      {
        dprints("new_pantilt_move_position(%s): Move position for axis %s\n", def_groupe[gpe].no_name, liaison[i].nom);
        my_data->axes[my_data->nb_axes] = &(liaison[i]);
        my_data->nb_axes++;
      }

      l++;
      i = find_input_link(gpe, l);
    }

    if (strlen(pantilt_name) == 0)
    {
      pantilt = pantilt_get_first_pantilt();
    }
    else
    {
      pantilt = pantilt_get_pantilt_by_name(pantilt_name);
    }

    if (pantilt == NULL)
    {
      fprintf(stderr, "ERROR in f_pantilt_move_position(%s): Cannot find pantilt\n", def_groupe[gpe].no_name);
      exit(1);
    }

    my_data->pantilt = pantilt;
    def_groupe[gpe].data = my_data;
  }

  dprints("new_pantilt_move_position(%s): Leaving function\n", def_groupe[gpe].no_name);
}

void function_pantilt_move_position(int gpe)
{
  int i;
  data_pantilt_move_position *my_data;

  printf("In pantilt_move_position\n");

  my_data = (data_pantilt_move_position *) def_groupe[gpe].data;
  if (my_data == NULL)
  {
    fprintf(stderr, "ERROR in f_pantilt_move_position(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
    exit(1);
  }

  for (i = 0; i < my_data->nb_axes; i++)
  {
    dprints("f_pantilt_move_position(%s): Set axis %s to position %f\n", def_groupe[gpe].no_name, my_data->axes[i]->nom, neurone[def_groupe[my_data->axes[i]->depart].premier_ele].s1);
    pantilt_set_axis_position(my_data->pantilt, my_data->axes[i]->nom, neurone[def_groupe[my_data->axes[i]->depart].premier_ele].s1);
  }

  if (my_data->nb_axes == 1)
  {
    pantilt_move_axis(my_data->pantilt, my_data->axes[0]->nom, 1);
    dprints("f_pantilt_move_position(%s): One axis movement done\n", def_groupe[gpe].no_name);
  }
  else
  {
    pantilt_move_multiple(my_data->pantilt, 100);
    dprints("f_pantilt_move_position(%s): Multiple axes movement done\n", def_groupe[gpe].no_name);
  }
}

void destroy_pantilt_move_position(int gpe)
{
  data_pantilt_move_position *my_data = NULL;

  dprints("destroy_pantilt_move_position(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data != NULL)
  {
    my_data = (data_pantilt_move_position *) def_groupe[gpe].data;

    free(my_data);
    def_groupe[gpe].data = NULL;
  }

  dprints("destroy_pantilt_move_position(%s): Entering function\n", def_groupe[gpe].no_name);
}
