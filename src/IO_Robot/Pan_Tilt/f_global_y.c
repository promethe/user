/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_global_y.c 
 \brief 

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -int MOVE_TILT
 -cam_pos PTposition

 Macro:
 -none 

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/mem_entree.h>
#include <libhardware.h>
/*#include <Global_Var/IO_Robot.h>*/

void function_global_y(int num)
{
  int i, /*j, */num_V = 0, cpt, e_V;
  int size_Y = def_groupe[num].tailley;
  int size_X = def_groupe[num].taillex;
  int size = (size_X * size_Y);
  int first_neuron = def_groupe[num].premier_ele;
  int last_neuron = def_groupe[num].premier_ele + size;
  int local_y;
  int suggested_size_y;
  int /*centre, */origine;
  int map, global_o, local_o;
  char *string;
  mem_entree *data_g;
  /*unused: FILE * posactiv2; */

  printf("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ", __FUNCTION__);
  exit(0);

  if (def_groupe[num].ext == NULL)
  {
    cpt = 0;
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == num)
      {
        string = liaison[i].nom;
        if (strstr(string, "Vision_y") != NULL)
        {
          num_V = i;
          cpt++;
        }
      }
    if (cpt != 1)
    {
      printf("\n function_global_space() erreur: pas de  lien corrects detecte !\n "
      /*, cpt */);
      exit(0);
    }

    data_g = (mem_entree *) malloc(sizeof(mem_entree));
    (*data_g).entree_1 = e_V = liaison[num_V].depart;
    def_groupe[num].ext = (void *) data_g;
  }
  else
  {
    data_g = (mem_entree *) def_groupe[num].ext;
    e_V = (*data_g).entree_1;
  }

  local_y = def_groupe[e_V].tailley;

  /*suggested_size_y=(int)   ((float)(255.+get_movetilt())/((float)get_movetilt()/local_y));*/

  if ((size_Y != suggested_size_y))
  {
    printf(" function_global_y(): les tailles de la carte semblent etre inapropriees au mapping de la carte visuelle d'entree.\n    taille recomandee: %d \n    taille actuelles: %d\n", suggested_size_y, size_Y);
  }

  /*positionement du coin superieur gauche de la vue locale sur la carte globale */

  /* origine=   first_neuron + (int) ((float)((get_PTposition()).servo_tilt)/((float)get_movetilt()/local_y));*/
  /*mapping de l'activite des neurones de la vue locale sur l'espace global*/

  for (i = first_neuron; i < last_neuron; i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;

  for (i = 0; i < local_y; i++)
  {

    map = i;
    global_o = origine + map;
    local_o = (def_groupe[e_V].premier_ele) + i;
    neurone[global_o].s = neurone[global_o].s1 = neurone[global_o].s2 = neurone[local_o].s2;

  }

  /*
   posactiv2=fopen ("./global_y.txt", "a");
   for (i=first_neuron;i<last_neuron;i++)
   if (neurone[i].s2== 1.0)  fprintf(posactiv2,"%d \n",i-first_neuron);

   fclose(posactiv2);  
   */
}
