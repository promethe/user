/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_merge.c 
 \brief 

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 
 Carte 2D representant l'enssemble de l'espace visuel du robot.  Il
 s'agit de la concatenation des espaces visuels accessible par la
 camera "tant donnees les differentes positions pan tilt.  Cette carte
 possede en entrees: la carte 2D representant le champ de vision local
 de la camera.  un lien algo nomme "Vision" la relie. Le poids de ce
 lien joue sur l'amplitude de la duplication des activites

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Struct/mem_entree.h>
#include <string.h>

void function_merge(int num)
{
  int i, j, num_X = 0, num_Y = 0, cpt, e_X, e_Y, nx, ny;
  int size_Y = def_groupe[num].tailley;
  int size_X = def_groupe[num].taillex;
  int size = (size_X * size_Y);
  int deb = def_groupe[num].premier_ele;
  int last = def_groupe[num].premier_ele + size;
  int local_x, deb_x;
  int local_y, deb_y;
  char *string;
  mem_entree *data_g;
  /*! recherche  des liens */

  if (def_groupe[num].ext == NULL)
  {
    cpt = 0;
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == num)
      {
        string = liaison[i].nom;
        if (strstr(string, "x_coord") != NULL)
        {
          num_X = i;
          cpt++;
        }
        if (strstr(string, "y_coord") != NULL)
        {
          num_Y = i;
          cpt++;
        }
      }
    if (cpt != 2)
    {
      printf("\n function_merge() erreur: pas de  lien corrects detecte !\n "
      /*, cpt */);
      exit(0);
    }
    data_g = (mem_entree *) malloc(sizeof(mem_entree));
    (*data_g).entree_1 = e_X = liaison[num_X].depart;
    (*data_g).entree_2 = e_Y = liaison[num_Y].depart;
    def_groupe[num].ext = (void *) data_g;

  }
  else
  {
    data_g = (mem_entree *) def_groupe[num].ext;
    e_X = (*data_g).entree_1;
    e_Y = (*data_g).entree_2;

  }

  local_x = def_groupe[e_X].taillex;
  local_y = def_groupe[e_Y].tailley;
  deb_x = def_groupe[e_X].premier_ele;
  deb_y = def_groupe[e_Y].premier_ele;

  printf("groupeX %d ", deb_x);
  printf("groupeY %d ", deb_y);

  for (i = deb; i < last; i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;

  for (i = deb_x; i < deb_x + local_x; i++)
  {
    if (neurone[i].s2 > 0.2)
    {
      nx = i - deb_x;
      for (j = deb; j < last; j++)
      {
        if ((j - deb) % local_x == nx) neurone[j].s += 1.0;

      }
    }
  }

  for (i = deb_y; i < deb_y + local_y; i++)
  {
    if (neurone[i].s2 > 0.2)
    {
      ny = i - deb_y;
      for (j = deb; j < last; j++)
      {
        if ((j - deb) / local_x == ny) neurone[j].s += 1.0;
      }
    }
  }

  for (j = deb; j < last; j++)
  {
    if (neurone[j].s > 1.0) neurone[j].s2 = neurone[j].s1 = 1.0;

  }

}
