/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_joint.c 
 \brief 

 Author: Benoit Mariat
 Created: 03/03/2005
 Modified:
 - author: -
 - description: -
 - date: -

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 
 -send a command to a servo,
 the command is a percentage, it is define by the neuron of the precedent group
 the servo is identify by its name, this name is on the link

 Macro:
 -none 

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*#define DEBUG*/

#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <net_message_debug_dist.h>

struct F_JOINT_STRUCT {
  int no_neur;
  Joint *servo;
  int gpe_wait;
  int proprio;
  float seuil_error;
  int gpe_of_enable;
};

void new_joint(int gpe)
{
  int gpe_wait = -1, proprio = -1;
  char joint_name[256] = "\0";
  int input_group = -1;
  int index, link;
  Joint *servo = NULL;
  struct F_JOINT_STRUCT *this_struct = NULL;
  float seuil_error = 0.;
  int gpe_of_enable = -1;

  if (def_groupe[gpe].data == NULL)
  {
    index = 0;
    link = find_input_link(gpe, index);
    while (link != -1)
    {

      if (prom_getopt_float(liaison[link].nom, "-proprio", &seuil_error) == 2) proprio = 1;
      if (strcmp(liaison[link].nom, "wait") == 0)
      {
        gpe_wait = liaison[link].depart;
      }
      else
      {
        if (strcmp(liaison[link].nom, "enable") == 0)
        {
          gpe_of_enable = liaison[link].depart;
        }
        else /* true input link */
        {
          input_group = liaison[link].depart;
          strcpy(joint_name, liaison[link].nom);
        }
      }

      index++;
      link = find_input_link(gpe, index);
    }

    if (strlen(joint_name) == 0 || input_group < 0)
    {
      fprintf(stderr, "ERROR in new_joint(%s): No joint specified as input link\n", def_groupe[gpe].no_name);
      exit(1);
    }
    servo = joint_get_serv_by_name(joint_name);

    if (servo == NULL)
    {
      fprintf(stderr, "ERROR in new_joint(%s): No joint found with name %s\n", def_groupe[gpe].no_name, joint_name);
      exit(1);
    }

    this_struct = malloc(sizeof(struct F_JOINT_STRUCT));
    if (this_struct == NULL)
    {
      fprintf(stderr, "ERROR in new_joint(%s): malloc failed for data\n", def_groupe[gpe].no_name);
      exit(1);
    }

    def_groupe[gpe].data = this_struct;
    this_struct->no_neur = def_groupe[input_group].premier_ele;
    this_struct->servo = servo;
    this_struct->gpe_wait = gpe_wait;
    this_struct->proprio = proprio;
    this_struct->seuil_error = seuil_error;
    this_struct->gpe_of_enable = gpe_of_enable;

  }
}

void function_joint(int gpe)
{
  int deb, servo_speed;
  float pos = 0, proprio = 0, wait_factor;
  struct F_JOINT_STRUCT *this_struct = NULL;
  Joint *servo;
  int deb_gpe;
  struct timespec duree_nanosleep;
  long wait_nano = 0;
  int enabled;
  float value_of_enable = 0;

  this_struct = (struct F_JOINT_STRUCT *) def_groupe[gpe].data;
  if (this_struct == NULL)
  {
    fprintf(stderr, "ERROR in f_joint(%s): Cannot retreive data\n", def_groupe[gpe].no_name);
    exit(1);
  }
  value_of_enable = neurone[def_groupe[this_struct->gpe_of_enable].premier_ele].s1;
  if (this_struct->gpe_of_enable == -1) enabled = 1;
  else if (this_struct->gpe_of_enable != -1 && value_of_enable >= 0.5) enabled = 1;
  else enabled = 0;

  if (!enabled) return;

  servo = this_struct->servo;
  deb = this_struct->no_neur;
  deb_gpe = def_groupe[gpe].premier_ele;

  pos = neurone[deb].s1;

  dprints("f_joint(%s): Setting joint %s to pos %f\n", def_groupe[gpe].no_name, servo->name, pos);

  if (this_struct->gpe_wait != -1)
  {
    joint_get_proprio(servo);
    proprio = servo->proprio;
  }

  if (pos >= neurone[deb_gpe].seuil) /* ************************** Attention ! ******************************* */
  joint_servo_command(servo, pos, -1); /* il faut ici pos>=seuil et pas seulement pos>seuil afin que la position */
  /* 0 soit prise en compte lorsque le seuil vaut 0 (valeur par defaut).   */
  if (this_struct->proprio != -1)
  {
    do
    {
      joint_get_proprio(servo);
      dprints("Wait till the motor reaches the consigne\n");
    } while (abs(servo->proprio - pos) > this_struct->seuil_error);
  }

  /* Attente de fin du mouvement basee sur la vitesse du servo */
  if (this_struct->gpe_wait != -1)
  {
    servo_speed = servo->Vdefault;
    dprints("f_joint(%s): Speed of joint is %i\n", def_groupe[gpe].no_name, servo_speed);
    if (servo_speed <= 0)
    {
      fprintf(stderr, "WARNING in f_joint(%s): Wait option active but Vdefault not set for the joint => cannot calculate wait time\n", def_groupe[gpe].no_name);
    }
    else
    {
      /* La relation lineaire entre temps d'attente et distance a parcourir n'est peut etre pas la meilleure (log ?) */
      /* test avec une racine carre */
      wait_factor = neurone[def_groupe[this_struct->gpe_wait].premier_ele].s1;
      wait_nano = sqrt(fabs(pos - proprio)) * 4000000000 * wait_factor / servo_speed;
      duree_nanosleep.tv_sec = wait_nano / 1000000000;
      duree_nanosleep.tv_nsec = wait_nano % 1000000000;
      dprints("f_joint(%s): Waiting for %i seconds, %i nanoseconds\n", def_groupe[gpe].no_name, duree_nanosleep.tv_sec, duree_nanosleep.tv_nsec);
      nanosleep(&duree_nanosleep, NULL);
    }
  }

  neurone[deb_gpe].s2 = neurone[deb_gpe].s1 = neurone[deb_gpe].s = pos; /*proprio mais faudrait mettre la vraie... */
}

void destroy_joint(int gpe)
{
  struct F_JOINT_STRUCT *my_data = NULL;

  dprints("destroy_joint(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data != NULL)
  {
    my_data = (struct F_JOINT_STRUCT *) def_groupe[gpe].data;

    free(my_data);
    def_groupe[gpe].data = NULL;
  }

  dprints("destroy_joint(%s): Entering function\n", def_groupe[gpe].no_name);
}
