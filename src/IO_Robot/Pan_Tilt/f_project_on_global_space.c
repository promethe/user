/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_project_on_global_space.c
 \brief

 Author: Mickael Maillard
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 23/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -COM_D
 -MOTOR_MAX

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -Kernel_Function/find_input_link()
 -tools/IO_Robot/Com_Koala/koala_ir()
 -tools/IO_Robot/Com_Koala/commande_koala()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <libhardware.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #define DEBUG 1  */

struct proprio_data {
  int gpe;
  float joint_start;
  float joint_stop;
};

struct project_on_global_space_data {
  float cam_fov_x;
  float cam_fov_y;
  float facteur_pixel_to_pos_x;
  float facteur_pixel_to_pos_y;
  int prev_gpe;
  struct proprio_data *gpe_proprios_pan;
  int nr_proprio_pan;
  struct proprio_data *gpe_proprios_tilt;
  int nr_proprio_tilt;
  int inversion;
};

void function_project_on_global_space(int Gpe)
{
  int i = 0;
  int j = 0;

  int k = 0;
  int l = 0;

  int lien = 0;

  char param[128];
  char *param_link = NULL;

  int gpe_input = 0;
  struct proprio_data *gpe_proprios_pan = NULL;
  struct proprio_data *gpe_proprios_tilt = NULL;
  int nr_proprio_pan = 0;
  int nr_proprio_tilt = 0;
  float facteur_pixel_to_pos_x = 0;
  float facteur_pixel_to_pos_y = 0;
  struct project_on_global_space_data *struct_global_data = NULL;
  float cam_fov_x = 0;
  float cam_fov_y = 0;
  int prev_gpe = 0;

  int deb_prev = 0;
  int deb_gpe = 0;
  int gpe_nbr = 0;

  int gpe_taillex = 0;

  int prev_gpe_taillex = 0;
  int prev_gpe_tailley = 0;

  float new_stop_x = 0;
  float new_start_x = 0;

  float new_stop_y = 0;
  float new_start_y = 0;

  int pos_x = 0;
  int pos_y = 0;

  int cur_pos = -1;
  int cur_prev_pos = -1;

  memset(param, 0, 128);
  if (def_groupe[Gpe].data == NULL)
  {
#ifdef DEBUG
    printf("function_project_on_global_space : 1er passage dans f_global_2D\n");
#endif	
    struct_global_data = def_groupe[Gpe].data = malloc(sizeof(struct project_on_global_space_data));
    if (struct_global_data == NULL)
    {
      printf("function_project_on_global_space : Gpe : %d : Erreur d'allocation memoire...\n", Gpe);
      exit(1);
    }
    memset(struct_global_data, 0, sizeof(struct project_on_global_space_data));

    struct_global_data->inversion = 1; /*initialisation inversion */

    for (lien = 0; lien < nbre_liaison; lien++)
    {
      if (liaison[lien].arrivee == Gpe)
      {
        if ((prom_getopt(liaison[lien].nom, "-t", param) == 2) || (prom_getopt(liaison[lien].nom, "-T", param) == 2))
        {
          if (!strcmp(param, "proprio"))
          {
            int type = -1; /* 0 = PAN ; 1 = TILT */
            int num_proprio = -1;
            int nr_proprio = -1;
            float *new_start = NULL;
            float *new_stop = NULL;
            struct proprio_data **gpe_proprios = NULL;

            if ((prom_getopt(liaison[lien].nom, "-a", param) == 2) || (prom_getopt(liaison[lien].nom, "-A", param) == 2))
            {
              type = atoi(param);
              switch (type)
              {
              case 0: /* Pan (axe X)*/
                gpe_proprios = &gpe_proprios_pan;
                nr_proprio = nr_proprio_pan;
                new_start = &new_start_x;
                new_stop = &new_stop_x;
                break;
              case 1: /* Tilt (axe Y) */
                gpe_proprios = &gpe_proprios_tilt;
                nr_proprio = nr_proprio_tilt;
                new_start = &new_start_y;
                new_stop = &new_stop_y;
                break;
              default:
                fprintf(stderr, "function_project_on_global_space : erreur de type sur le lien de proprioception : %s\n", liaison[lien].nom);
                exit(1);
              }
            }
            else
            {
              fprintf(stderr, "function_project_on_global_space : erreur de type sur le lien de proprioception : %s\n", liaison[lien].nom);
              exit(1);
            }

            if ((prom_getopt(liaison[lien].nom, "-p", param) == 2) || (prom_getopt(liaison[lien].nom, "-P", param) == 2))
            {
              num_proprio = atoi(param);
            }
            else
            {
              fprintf(stderr, "function_project_on_global_space : erreur de nom sur le lien de proprioception : %s\n", liaison[lien].nom);
              exit(1);
            }

            if ((num_proprio + 1) > nr_proprio)
            {
              if (type == 0) struct_global_data->nr_proprio_pan = nr_proprio_pan = nr_proprio = num_proprio + 1;
              else if (type == 1) struct_global_data->nr_proprio_tilt = nr_proprio_tilt = nr_proprio = num_proprio + 1;
              if (((*gpe_proprios) = realloc(*gpe_proprios, nr_proprio * sizeof(struct proprio_data))) == NULL)
              {
                fprintf(stderr, "function_project_on_global_space : erreur d'allocation memoire pour les liens proprios\n");
                exit(-1);
              }
            }
            if (num_proprio >= 0) (*gpe_proprios)[num_proprio].gpe = liaison[lien].depart;
            else
            {
              fprintf(stderr, "function_project_on_global_space : num_proprio invalide : %i\n", num_proprio);
              exit(-1);
            }

            /*INVERSION DES SENS de parcours en proprio ou pas*/
            if ((prom_getopt(liaison[lien].nom, "-noinv", param) >= 1) || (prom_getopt(liaison[lien].nom, "-NOINV", param) >= 1))
            {
              struct_global_data->inversion = 0;
            }

            if ((param_link = strstr(liaison[lien].nom, "-B")) != NULL)
            {
              (*gpe_proprios)[num_proprio].joint_start = atof(param_link + 2);
              (*new_start) += (*gpe_proprios)[num_proprio].joint_start;
            }
            else
            {
              printf("function_project_on_global_space : Erreur de formatage des liens (servo start) de f_global_2D gpe %d\n", Gpe);
              exit(1);
            }

            if ((param_link = strstr(liaison[lien].nom, "-E")) != NULL)
            {
              (*gpe_proprios)[num_proprio].joint_stop = atof(param_link + 2);
              (*new_stop) += (*gpe_proprios)[num_proprio].joint_stop;
            }
            else
            {
              printf("function_project_on_global_space : Erreur de formatage des liens (servo stop) de f_global_2D gpe %d\n", Gpe);
              exit(1);
            }
          }
        }
        else gpe_input = lien;
      }
    }
    struct_global_data->gpe_proprios_pan = gpe_proprios_pan;
    struct_global_data->gpe_proprios_tilt = gpe_proprios_tilt;

#ifdef DEBUG
    printf("function_project_on_global_space : apres find\n");
#endif	

    if ((prom_getopt(liaison[gpe_input].nom, "-x", param) == 2) || (prom_getopt(liaison[gpe_input].nom, "-X", param) == 2))
    {
      struct_global_data->cam_fov_x = cam_fov_x = atof(param);
    }
    else
    {
      printf("function_project_on_global_space : Erreur de formatage des liens (camera en x) de f_global_2D gpe %d\n", Gpe);
      exit(1);
    }

    if ((prom_getopt(liaison[gpe_input].nom, "-y", param) == 2) || (prom_getopt(liaison[gpe_input].nom, "-Y", param) == 2))
    {
      struct_global_data->cam_fov_y = cam_fov_y = atof(param);
    }
    else
    {
      printf("function_project_on_global_space : Erreur de formatage des liens (camera en y) de f_global_2D gpe %d\n", Gpe);
      exit(1);
    }

#ifdef DEBUG
    printf("function_project_on_global_space : Apres joint et camera\n");
#endif	
    prev_gpe = struct_global_data->prev_gpe = liaison[gpe_input].depart;

    deb_prev = def_groupe[prev_gpe].premier_ele;
    deb_gpe = def_groupe[Gpe].premier_ele;
    gpe_nbr = def_groupe[Gpe].nbre;

    if ((def_groupe[Gpe].ext = malloc(2 * sizeof(int))) == NULL)
    {
      fprintf(stderr, "function_project_on_global_space : allocation impossible\n");
      exit(-1);
    }
    memset(def_groupe[Gpe].ext, 0, 2 * sizeof(int));

    struct_global_data->facteur_pixel_to_pos_x = facteur_pixel_to_pos_x = ((float) def_groupe[prev_gpe].taillex) / cam_fov_x;
    struct_global_data->facteur_pixel_to_pos_y = facteur_pixel_to_pos_y = ((float) def_groupe[prev_gpe].tailley) / cam_fov_y;

    if ((def_groupe[Gpe].taillex != (int) ((new_stop_x - new_start_x + cam_fov_x) * facteur_pixel_to_pos_x)) || (def_groupe[Gpe].tailley != (int) ((new_stop_y - new_start_y + cam_fov_y) * facteur_pixel_to_pos_y)))
    {
      printf("function_project_on_global_space : nombre de neuronne connseille pour le groupe %d = %f x %f\n", Gpe, ((new_stop_x - new_start_x + cam_fov_x) * facteur_pixel_to_pos_x), (new_stop_y - new_start_y + cam_fov_y) * facteur_pixel_to_pos_y);
      exit(1);
    }

#ifdef DEBUG		
    printf("function_project_on_global_space : gpe_nbr %d\n",gpe_nbr);
#endif
  }

#ifdef DEBUG
  printf("function_project_on_global_space : autre passage dans f_global_2D\n");
#endif	
  struct_global_data = (struct project_on_global_space_data*) def_groupe[Gpe].data;

  facteur_pixel_to_pos_x = struct_global_data->facteur_pixel_to_pos_x;
  facteur_pixel_to_pos_y = struct_global_data->facteur_pixel_to_pos_y;

  prev_gpe = struct_global_data->prev_gpe;
  prev_gpe_taillex = def_groupe[prev_gpe].taillex;
  prev_gpe_tailley = def_groupe[prev_gpe].tailley;

  gpe_taillex = def_groupe[Gpe].taillex;

  deb_prev = def_groupe[prev_gpe].premier_ele;
  deb_gpe = def_groupe[Gpe].premier_ele;

  gpe_nbr = def_groupe[Gpe].nbre;

  gpe_proprios_pan = struct_global_data->gpe_proprios_pan;
  gpe_proprios_tilt = struct_global_data->gpe_proprios_tilt;

  nr_proprio_pan = struct_global_data->nr_proprio_pan;
  nr_proprio_tilt = struct_global_data->nr_proprio_tilt;

#ifdef DEBUG
  printf("function_project_on_global_space : partie commune dans f_global\n");
#endif

  for (i = deb_gpe; i < (deb_gpe + gpe_nbr); i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;

  new_start_x = 0;
  new_stop_x = 0;

  new_start_y = 0;
  new_stop_y = 0;

  /*
   ** ATTENTION : Valable si les reperes de toutes les proprioceptions sont identiques
   */
  for (pos_x = 0, i = 0; i < nr_proprio_pan; i++)
  {
    new_start_x += gpe_proprios_pan[i].joint_start * facteur_pixel_to_pos_x;
    new_stop_x += gpe_proprios_pan[i].joint_stop * facteur_pixel_to_pos_x;
    pos_x = pos_x + (int) ((facteur_pixel_to_pos_x * (int) (neurone[def_groupe[gpe_proprios_pan[i].gpe].premier_ele].s2 * (gpe_proprios_pan[i].joint_stop - gpe_proprios_pan[i].joint_start))));
  }

  for (pos_y = 0, i = 0; i < nr_proprio_tilt; i++)
  {
    new_start_y += gpe_proprios_tilt[i].joint_start * facteur_pixel_to_pos_y;
    new_stop_y += gpe_proprios_tilt[i].joint_stop * facteur_pixel_to_pos_y;
    pos_y = pos_y + (int) ((facteur_pixel_to_pos_y * (int) (neurone[def_groupe[gpe_proprios_tilt[i].gpe].premier_ele].s2 * (gpe_proprios_tilt[i].joint_stop - gpe_proprios_tilt[i].joint_start))));
  }

  /* 	printf("factor pan = %f factor tilt = %f\n", facteur_pixel_to_pos_x, facteur_pixel_to_pos_y); */
  /* 	printf("pan = %i tilt = %i\n", pos_x, pos_y); */

  /* Centre horizontal */
  ((int *) def_groupe[Gpe].ext)[0] = pos_x + (prev_gpe_taillex / 2);
  /* Centre vertical */
  ((int *) def_groupe[Gpe].ext)[1] = pos_y + (prev_gpe_tailley / 2);

  /*printf("In groupe %s struct_global_data->inversion == %d\n",def_groupe[Gpe].no_name,struct_global_data->inversion);*/
  if (struct_global_data->inversion == 1)
  {
    for (l = pos_y, j = (prev_gpe_tailley - 1); j >= 0; j--, l++)
      for (k = pos_x, i = (prev_gpe_taillex - 1); i >= 0; i--, k++)
      {
        cur_pos = ((l * gpe_taillex) + k);
        cur_prev_pos = ((j * prev_gpe_taillex) + i);
        if ((cur_pos >= 0) && (cur_pos < gpe_nbr))
        {
          /*printf("pos prev = %i - pos = %i\n",  (j * prev_gpe_taillex) + i, (l * gpe_taillex) + k);*/
          neurone[deb_gpe + cur_pos].s = neurone[deb_prev + cur_prev_pos].s;
          neurone[deb_gpe + cur_pos].s1 = neurone[deb_prev + cur_prev_pos].s1;
          neurone[deb_gpe + cur_pos].s2 = neurone[deb_prev + cur_prev_pos].s2;
        }
      }

  }
  else
  {
    /*	   printf("NOINV activated in groupe %s !\n",def_groupe[Gpe].no_name);*/
    for (l = pos_y, j = 0; j < prev_gpe_tailley; j++, l++)
      for (k = pos_x, i = 0; i < prev_gpe_taillex; i++, k++)
      {
        cur_pos = ((l * gpe_taillex) + k);
        cur_prev_pos = ((j * prev_gpe_taillex) + i);
        if ((cur_pos >= 0) && (cur_pos < gpe_nbr))
        {
          /*printf("pos prev = %i - pos = %i\n",  (j * prev_gpe_taillex) + i, (l * gpe_taillex) + k);*/
          neurone[deb_gpe + cur_pos].s = neurone[deb_prev + cur_prev_pos].s;
          neurone[deb_gpe + cur_pos].s1 = neurone[deb_prev + cur_prev_pos].s1;
          neurone[deb_gpe + cur_pos].s2 = neurone[deb_prev + cur_prev_pos].s2;
        }
      }
  }

#ifdef DEBUG
  printf("function_project_on_global_space : sortie de  f_global_2D\n");
#endif
  return;
}
