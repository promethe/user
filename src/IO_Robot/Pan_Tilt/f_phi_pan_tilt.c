/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_phi_pan_tilt.c 
 \brief 

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -cam_pos PTposition
 -int MOVE_TILT
 -int OFFSET_PAN
 -int MOVE_PAN
 -int TILT_MAX_DELTA
 -int PAN_MAX_DELTA
 -prom_images_struct images_captures   		=> dans Global_Var/Vision

 Internal Tools:
 -none

 External Tools: 
 -tools/IO_Robot/Pan_Tilt/tilt()
 -tools/IO_Robot/Pan_Tilt/pan()
 -tools/IO_Robot/Pan_Tilt/CaptureImage()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>

#include <Global_Var/IO_Robot.h>
#include <Global_Var/Vision.h>

#include <libhardware.h>

void function_phi_pan_tilt(int num)
{

  int i, j, num_e1 = 0, num_e2 = 0, cpt, entree_p, entree_t /*,ask */;
  /*unused: int size_Y = def_groupe[num].tailley; */
  /*unused: int size_X = def_groupe[num].taillex;  */
  /*unused: int size = (size_X * size_Y); */
  /*unsued :int first_neuron = def_groupe[num].premier_ele; */
  /*unused: int last_neuron  = def_groupe[num].premier_ele + size; */
  int centre_t = 0, centre_p = 0, pos_d_phi_t, pos_d_phi_p,
  /*new_pos,delta_t,delta_p, */size_d_phi_t, size_d_phi_p;
  char *string;
  mem_entree *data_g;
  int sum = 0;
  int delai = 1;
  float amplitude;

  (void) amplitude; // (unused)
  (void) size_d_phi_p; // (unused)
  (void) size_d_phi_t; // (unused)
  (void) pos_d_phi_p; // (unused)
  (void) pos_d_phi_t; // (unused)

  printf("La fonction %s doit erte revues car les appel hardware ne sont plus bons\n", __FUNCTION__);

  if (1 /*get_cammode() == 180 */) amplitude = 8.0; /* P.A peu propre! ne tiens pas compte de la taille ni de la pente du filtre du nf */
  else amplitude = 4.0;

  /*! recherche  des liens */
  if (def_groupe[num].ext == NULL)
  {
    cpt = 0;
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == num)
      {
        string = liaison[i].nom;
        if (strstr(string, "tilt") != NULL)
        {
          num_e1 = i;
          cpt++;
        }
        if (strstr(string, "pan") != NULL)
        {
          num_e2 = i;
          cpt++;
        }
      }
    if (cpt != 2)
    {
      printf("\n function_phi_pan tilt() erreur: nombre de liens amont incorects !\n "
      /*, cpt */);
      exit(0);
    }

    data_g = (mem_entree *) malloc(sizeof(mem_entree));
    (*data_g).entree_1 = entree_t = liaison[num_e1].depart;
    (*data_g).entree_2 = entree_p = liaison[num_e2].depart;
    def_groupe[num].ext = (void *) data_g;
  }
  else
  {
    data_g = (mem_entree *) def_groupe[num].ext;
    entree_t = (*data_g).entree_1;
    entree_p = (*data_g).entree_2;
  }

  /* position de l'origine sur d_phi */
  size_d_phi_t = def_groupe[entree_t].nbre;
  size_d_phi_p = def_groupe[entree_p].nbre;

  /*  centre_t = (int) (float)((get_PTposition()).servo_tilt+(float)(get_movetilt()/2))*size_d_phi_t /((float)get_movetilt()+255);
   centre_p = (int) (float)((float)((get_PTposition()).servo_pan+get_offsetpan())+(float)(get_movepan()/2))*size_d_phi_p /((float)get_movepan()+255);*/
  /* on recupere la valeur du neurone  de d_phi correspondant */

  pos_d_phi_t = def_groupe[entree_t].premier_ele + centre_t;
  pos_d_phi_p = def_groupe[entree_p].premier_ele + centre_p;

  /*  delta_t =(int) (rint(amplitude*(neurone[pos_d_phi_t].s2)*get_tilt_max_delta()));
   delta_p =(int) (rint((amplitude*(neurone[pos_d_phi_p].s2)*get_pan_max_delta())));
   
   if (abs(delta_t)> 2)
   { 
   new_pos= (get_PTposition()).servo_tilt+ delta_t ;
   tilt(new_pos);
   sum+=abs(delta_t);
   }

   if (abs(delta_p)> 2)
   { 
   new_pos= (get_PTposition()).servo_pan+ delta_p ;
   
   pan(new_pos);
   sum+=abs(delta_p);
   }
   */
  if (sum > 1)
  {
    for (j = 0; j < delai; j++)
    {
      for (i = 0; i < (int) images_capture.image_number; i++)
      {
        /*CaptureImage(images_capture.images_table[i]); */
      }
    }
  }

}
