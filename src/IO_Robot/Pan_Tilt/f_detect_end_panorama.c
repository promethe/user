//#define DEBUG

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

typedef struct data_detect_end_panorama {
  int handle_panorama_gpe;
} data_detect_end_panorama;

void new_detect_end_panorama(int gpe)
{
  data_detect_end_panorama *mydata;
  int tmp_handle_panorama_gpe=-1;
  int i, links_in;

  /* Get Input group */
  i = 0;
  while ((links_in = find_input_link(gpe, i)) != -1)
  {
    if (prom_getopt(liaison[links_in].nom, "-handle_panorama", NULL) != 0) tmp_handle_panorama_gpe = liaison[links_in].depart;
    i++;
  }

  /* Check input groups */
  if (tmp_handle_panorama_gpe == -1)
  EXIT_ON_ERROR("The group must have be linked to f_handle_panorama !\n");

  /* Check group size */
  if (def_groupe[gpe].nbre != 1)
  EXIT_ON_ERROR("The group size must have a single neuron !\n");

  /* Create object */
  mydata = ALLOCATION(data_detect_end_panorama);
  mydata->handle_panorama_gpe = tmp_handle_panorama_gpe;
  def_groupe[gpe].data = (data_detect_end_panorama *) mydata;
}

void function_detect_end_panorama(int gpe)
{
  data_detect_end_panorama *mydata;
  int offset;
  int tmp_handle_panorama_gpe;

  /* init local variables */
  mydata = (data_detect_end_panorama *) def_groupe[gpe].data;
  tmp_handle_panorama_gpe = mydata->handle_panorama_gpe;

  /* set group output */
  offset = def_groupe[gpe].premier_ele;
  neurone[offset].s = neurone[offset].s1 = neurone[offset].s2 = neurone[def_groupe[tmp_handle_panorama_gpe].premier_ele].d;

}

void destroy_detect_end_panorama(int gpe)
{
  dprints("destroy %s(%s) \n", __FUNCTION__, def_groupe[gpe].no_name);
  if (def_groupe[gpe].data != NULL)
  {
    free(((data_detect_end_panorama *) def_groupe[gpe].data));
    def_groupe[gpe].data = NULL;
  }
}
