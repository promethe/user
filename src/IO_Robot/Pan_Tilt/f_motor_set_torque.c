/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************

 Author: Arnaud Blanchard
 Created: 20/07/2009

 Description:
 - Send a vector of position to the motors depending definition in .dev.
 Incoming links:
 - position

 */
#define DEBUG
#include <net_message_debug_dist.h>
#include <unistd.h>
#include <libx.h>
#include <outils.h>
#include <dev.h>
#include <device.h>
#include <Components/motor.h>

#define MAXIMUM_SIZE_OF_OPTION 128

typedef struct box {
  Device *device;
  type_motor **motors;
  int number_of_motors;
  type_neurone* input_neurons;
} Box;

void new_motor_set_torque(int index_of_group)
{
  Box *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
  int index_of_link, success;

  type_groupe *group, *group_of_inputs;
  type_liaison *link;

  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box);
  group->data = box;

  /** On recherche le lien entrant qui doit etre unique */
  index_of_link = find_input_link(index_of_group, 0);
  if (index_of_link == -1) EXIT_ON_ERROR("Group: %d, no incoming link !", index_of_group);
  if (find_input_link(index_of_group, 1) != -1) EXIT_ON_ERROR("Group: %d, has more than one incoming link !", index_of_group);
  link = &liaison[index_of_link];

  /** On recherche le parametre id sur le lien */
  success = prom_getopt(link->nom, "id=", id_of_device);
  if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

  /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
  if (success == 2) box->device = dev_get_device("motor", id_of_device);
  else if (success == 0) box->device = dev_get_device("motor", NULL);
  else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

  /** On recupere le groupe entrant */
  group_of_inputs = &def_groupe[link->depart];

  /** On verifie que le nombre de moteurs de l'appareil equivaut a celui du nombre de neurones entrant*/
  if (box->device->number_of_components != group_of_inputs->nbre) EXIT_ON_ERROR("Number of motors (%d) different from number of input neurons (%d).", box->device->number_of_components, group_of_inputs->nbre);
  else box->number_of_motors = box->device->number_of_components;

  /** On defini les neurones entrant de la boite. */
  box->input_neurons = &neurone[group_of_inputs->premier_ele];

  /** On defini les moteurs de la boite*/
  box->motors = (type_motor**) box->device->components;

}

void function_motor_set_torque(int index_of_group)
{
  Box *box;
  int i;
  type_motor **motors;
  type_neurone *input_neurons;

  /** On recupere la boite a partir du groupe*/
  box = (Box*) def_groupe[index_of_group].data;

  /** On recupere les neurones entrant dans la boite et les moteurs*/
  input_neurons = box->input_neurons;
  motors = box->motors;

  /** On defini le couple pour chaque moteur */
  for (i = 0; i < box->number_of_motors; i++)
  {
    motors[i]->set_torque(motors[i], &input_neurons[i].s1);
  }

  /** On demande a l'appareil d'envoyer tout le buffer */
  box->device->flush(box->device);

}

void destroy_motor_set_torque(int index_of_group)
{
  free(def_groupe[index_of_group].data);
}

