/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS 

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 and, more generally, to use and operate it in the same conditions as regards security. 
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_motor_is_moving.c 
 \brief 

 Author: Julien Hirel
 Created: 25/05/2010
 Modified:
 - author: -
 - description: -
 - date: -

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: 
 Gets information about whether the motor is moving or not.

 Macro:
 -none 

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools: 
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG

#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <net_message_debug_dist.h>

typedef struct data_motor_is_moving {
  Motor *servo;
} data_motor_is_moving;

void new_motor_is_moving(int gpe)
{
  int link;
  data_motor_is_moving *my_data = NULL;
  Motor *servo = NULL;
  int index;

  if (def_groupe[gpe].data == NULL)
  {

    index = 0;
    link = find_input_link(gpe, index);

    while (link != -1)
    {
      servo = motor_get_serv_by_name(liaison[link].nom);

      if (servo != NULL)
      {
        break;
      }

      index++;
      link = find_input_link(gpe, index);
    }

    if (servo == NULL)
    {
      EXIT_ON_ERROR("No valid motor name found");
    }

    my_data = ALLOCATION(data_motor_is_moving);
    my_data->servo = servo;
    def_groupe[gpe].data = (void *) my_data;
  }
}

void function_motor_is_moving(int gpe)
{
  data_motor_is_moving *my_data = NULL;
  int first = def_groupe[gpe].premier_ele;

  my_data = (data_motor_is_moving *) def_groupe[gpe].data;

  if (my_data == NULL)
  {
    EXIT_ON_ERROR("Cannot retreive data");
  }

  dprints("f_motor_is_moving(%s): reading value\n", def_groupe[gpe].no_name);
  neurone[first].s2 = neurone[first].s1 = neurone[first].s = motor_is_moving(my_data->servo);
  dprints("f_motor_is_moving(%s): motor %s movement is %.0f\n", def_groupe[gpe].no_name, my_data->servo->name, neurone[first].s1);
}

void destroy_motor_is_moving(int gpe)
{
  dprints("destroy_motor_is_moving(%s): Entering function\n", def_groupe[gpe].no_name);

  if (def_groupe[gpe].data != NULL)
  {
    free((data_motor_is_moving *) def_groupe[gpe].data);
    def_groupe[gpe].data = NULL;
  }

  dprints("destroy_motor_is_moving(%s): Leaving function\n", def_groupe[gpe].no_name);
}
