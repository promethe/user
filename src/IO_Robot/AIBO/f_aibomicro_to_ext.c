/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief affect a chaque neurone du groupe la valeur normalisa��e d'une device
	(dans l'ordre oa�� on les a initialisa��es)

Author: A.HIOLLE
Created: 19/04/2005
Modified:
- author:
- description:
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:  le groupe pra��cedent est lia�� a�� celui-ci par un lien nomma�� device( chose inutils dans la configuration actuelle, mais pour plus tard c fait)

Known bugs:
Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Sound/Sound.h>


#include <IO_Robot/Aibo.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/
/*#define SAVE*/
void function_aibomicro_to_ext(int numero)
{
#ifdef SAVE
    char filename[25];
    FILE *fd;
#endif
    int i = 0, l = -1;
    long offset;                 /* ou le numero du canal pour demelanger les 2 voies */

    prom_audio_struct *temp_audio;
#ifdef DEBUG
    printf("*****************************************************\n");
    printf("AIBOMICROto ext \n**********************************\n");
#endif

    if (def_groupe[numero].audio_ext == NULL)
    {
        printf("init de aibomicro_to_ext \n");
        def_groupe[numero].data = (void *) malloc(sizeof(int));
        i = 0;
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strncmp(liaison[l].nom, "-1", 2) == 0)
            {
                def_groupe[numero].data = (void *) 1;
            }
            else if (strncmp(liaison[l].nom, "-2", 2) == 0)
            {
                def_groupe[numero].data = (void *) 2;
            }
            i++;
            l = find_input_link(numero, i);
        }
        printf("Init groupe aibomicro_to_ext No : %d for channel No : %p\n",
               numero, (void*) def_groupe[numero].data);

#ifdef SAVE
        strcpy(filename, "AiboSound");
        if ((int) def_groupe[numero].data == 1)
            strcat(filename, "1.snd");
        else
            strcat(filename, "2.snd");
#endif

        temp_audio = (prom_audio_struct *) malloc(sizeof(prom_audio_struct));

        if (temp_audio == NULL)
        {
            printf("Allocation error in %s  \n", __FUNCTION__);
            perror
                ("prom_audio_struct isn't allocated\nWaiting 5 seconds  \n");
            exit(1);
        }

        def_groupe[numero].audio_ext = (type_prom_data *) temp_audio;
        temp_audio->taille = 1024;
        temp_audio->trame =
            (char *) malloc(temp_audio->taille * sizeof(char));
        if (temp_audio->trame == NULL)
        {
            perror("prom_audio_struct->trame allocation error\n");
            printf(" dans %s \n", __FUNCTION__);
            exit(-1);
        }
    }

    temp_audio = (prom_audio_struct *) def_groupe[numero].audio_ext;

  /***Recopie de la partie droite ou gauche de la trame capturee par la structure Aibo_Micro********/
#ifdef SAVE
    fd = fopen(filename, "a");
#endif
    offset = ((long) def_groupe[numero].data) / 2;
    printf("canal sound = %ld \n", offset);
    for (i = 0; i < temp_audio->taille; i += 2)
    {
        temp_audio->trame[i / 2] = get_Aibo_Micro()->trame[i + offset];
#ifdef SAVE
        fprintf(fd, "%d\n", (int) temp_audio->trame[i / 2]);
#endif
    }

#ifdef SAVE
    fclose(fd);
#endif
}
