/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_smooth_command.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/

static int l = -1;

void function_smooth_command(int numero)
{
    int i = 0;
    float alpha = 0.2;
    int Gpe_asso = -1, Gpe_joint = -1;
    int deb, deb_joint, deb_asso;

    /*  printf("assoc timing \n"); */

    if (l == -1)
    {
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "asso") == 0)
            {
                Gpe_asso = liaison[l].depart;
            }
            if (strcmp(liaison[l].nom, "joint") == 0)
            {
                Gpe_joint = liaison[l].depart;
            }
            i++;
            l = find_input_link(numero, i);
        }
    }
    if (Gpe_asso == -1)
    {
        printf("groupe mem_non trouve\n");
        exit(0);
    }

    if (Gpe_joint == -1)
    {
        printf("groupe mean_non trouve\n");
        exit(0);
    }

    deb = def_groupe[numero].premier_ele;
    deb_joint = def_groupe[Gpe_joint].premier_ele;
    deb_asso = def_groupe[Gpe_asso].premier_ele;

    /*PG: Attention il n'y a apas de verification sur la taille des groupes !!!! risque core dump */
    for (i = 0; i < def_groupe[numero].nbre; i++)
    {
        neurone[deb + i].s =
            neurone[deb_joint + i].s + alpha * (neurone[deb_asso + i].s -
                                                neurone[deb_joint + i].s);
        if (neurone[deb + i].s > 1.0)
            neurone[deb + i].s = 1.0;
        else if (neurone[deb + i].s < 0.)
            neurone[deb + i].s = 0.;

        /*   printf("smooth command [%d]=%f\n",i,neurone[deb+i].s); */
        neurone[deb + i].s2 = neurone[deb + i].s1 = neurone[deb + i].s;
    }
}
