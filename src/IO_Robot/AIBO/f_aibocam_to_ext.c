/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file  
\brief recopie l'image capturee dans video_ext du groupe 
	

Author: A.HIOLLE modif P. Gaussier
Created: 19/04/2005
Modified:
- author:
- description:
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools: 
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:  
Known bugs:
Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <IO_Robot/Aibo.h>
#include <public_tools/Vision.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/*#define DEBUG*/
/*#define SAVE*/
#ifdef SAVE
static int cpt;
#endif
void function_aibocam_to_ext(int numero)
{

    int size = 0;
    prom_images_struct *image;
    int success;

#ifdef SAVE
    char filename[25];
    char str[25];
    cpt++;
#endif
#ifdef DEBUG
    printf("****** AIBOCAM to ext *************************\n");
#endif
    if (def_groupe[numero].ext == NULL)
    {

        def_groupe[numero].ext =
            (void *) calloc_prom_image(1, get_Aibo_Cam()->sx,
                                       get_Aibo_Cam()->sy, 3);

    }

    image = (prom_images_struct *) def_groupe[numero].ext;

    size = get_Aibo_Cam()->sx * get_Aibo_Cam()->sy * 3;
#ifdef DEBUG
    printf("sx=%d,sy=%d,size=%d  \n", get_Aibo_Cam()->sx, get_Aibo_Cam()->sy,
           size);
#endif
#ifdef SAVE

    strcpy(filename, "AiboIm");
    sprintf(str, "%d.png", cpt);
    strcat(filename, str);
#endif
#ifdef DEBUG
    printf("format image = %d \n", get_Aibo_Cam()->format);
#endif
    if (get_Aibo_Cam()->format == 0)
        success =
            convertYCrCbtoRGB(get_Aibo_Cam()->image, size,
                              (char*)( image->images_table[0]));
    else
        success =
            convertJPEGtoRGB(get_Aibo_Cam()->image, get_Aibo_Cam()->size,
                             (char*)image->images_table[0], &size);
    /*      printf("conversion jpeg taille retour=%d \n",size); */



#ifdef SAVE

    save_png_to_disk(filename, *image, 0);
#endif
}
