/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include<IO_Robot/Aibo.h>
#include<Struct/prom_images_struct.h>




void aibo_cam_to_ext()
{


    /*

       (cam->image)->image_number=0;
       ((prom_images_struct *)cam->image)->sx=p2;
       ((prom_images_struct *)cam->image)->sy=p3;
       ((prom_images_struct *)cam->image)->nb_band=3;
       if (!strcmp(type, "YCbCr")) {

       if((((prom_images_struct *)cam->image)->images_table[((prom_images_struct*)cam->image)->image_number]=(char *)malloc(p1))==NULL){

       perror("Allocation error in ");
       printf("%s at   \n",__FUNCTION__);
       return -1;

       }

       if(!memcpy(((prom_images_struct *)cam->image)->images_table[((prom_images_struct*)cam->image)->image_number],client->buffer_binaire,p1)){

       perror("copying binary image into prom_images_struct failed  ...");
       printf("In %s : reponse is %p\n",__FUNCTION__,reponse);
       return -1;
       }

       } */


}

void aibo_micro_to_ext()
{


    /*

       if((liste_device->data=malloc(sizeof(prom_audio_struct)))==NULL){

       perror("Allocation error in ");
       printf("%s at   \n",__FUNCTION__);
       return -1;

       }

       headerpos=strlen("BIN ")+strlen(itoa(p1,10))+1+strlen(type)+1+strlen(itoa(p2,10))+1+strlen(itoa(p3,10));*/

/*	((prom_audio_struct *)micro->data)->taille=p1;
	((prom_audio_struct *)micro->data)->channels=p2;
	((prom_audio_struct *)micro->data)->sample_rate=p3;
	((prom_audio_struct *)micro->data)->sample_rate=p4;
	((prom_audio_struct *)micro->data)->sample_format=p5;
	
	
	if((((prom_audio_struct *)micro->data)->trame=(char *)malloc(p1))==NULL){

		perror("Allocation error in ");
		printf("%s at   \n",__FUNCTION__);
		return -1;

	}
	
	if(!memcpy(((prom_audio_struct *)micro->data)->trame,client->buffer_binaire,p1)){

		perror("copying binary data into prom_audio_struct failed  ...");
		printf("In %s : reponse is %p\n",__FUNCTION__,reponse);
		return -1;
	}
*/
}
