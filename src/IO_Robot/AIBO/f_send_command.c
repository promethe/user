/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief Envoi les valeurs des neurones du groupe precedent au serveur pour les affecter aux devices

Author: A.HIOLLE
Created: 19/04/2005
Modified:
- author: P. Gaussier
- description:
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs:
Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include<IO_Robot/Aibo.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/

void function_send_command(int numero)
{

    int l = -1, i = 0;

    int Gpe_joint;

#ifdef DEBUG
    printf
        ("\n***************************************\n\tSend command\n*************************************\n");
#endif



    if (def_groupe[numero].ext == NULL)
    {

        Gpe_joint = -1;

        l = find_input_link(numero, i);

        while (l != -1)
        {
            if (strncmp(liaison[l].nom, "joint", 2) == 0)
            {
                Gpe_joint = liaison[l].depart;
            }

            i++;

            l = find_input_link(numero, i);
        }

        if (Gpe_joint == -1)
        {

            printf("liaison avec groupe connection non trouve dans %s at %d",
                   __FUNCTION__, __LINE__);
            exit(0);
        }


        if ((def_groupe[numero].ext = (void *) malloc(sizeof(int))) == NULL)
        {


            perror("allocation error in set_device_values");
            exit(-1);
        }



        *(int *) def_groupe[numero].ext = Gpe_joint;



    }
    else
    {


        Gpe_joint = *(int *) def_groupe[numero].ext;
    }

    memset(get_Aibo_Client()->buffer, 0, 4000);


    for (i = 0; i < def_groupe[Gpe_joint].nbre; i++)
    {
        /*    set_valn(i,neurone[def_groupe[Gpe_joint].premier_ele+i].s>0.01?neurone[def_groupe[Gpe_joint].premier_ele+i].s/1.5 : 0.99); */
        set_valn(i, neurone[def_groupe[Gpe_joint].premier_ele + i].s);

    }
    /*      printf("buffer = %s \n",get_Aibo_Client()->buffer); */
    Aibo_Client_send();
    /*PG: les messages en retour ne sont pas traites. risque de collision avec f_update_robot ??? */


}
