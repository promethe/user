/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   defgroup f_sim2d_init f_sim2d_init
   ingroup libIO_robot

   \file
   ingroup f_sim2d_init
   \brief

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 11/08/2004

   Theoritical description:
   - \f$  LaTeX equation: none \f$

   Description:
   L'init de l'environnement n'est faite qu'a la premiere iter des simul avec promethe

   Macro:
   -TAILLE_X

   Local variables:
   -float pas
   -int NBLAND;			Nbre de landmarks
   -char nom_fichier_cfg[32];
   -int mode;

   Global variables:
   -none

   Internal Tools:
   -init()

   External Tools:
   -none

   Links:
   - type: algo / biological / neural
   - description: none/ XXX
   - input expected group: none/xxx
   - where are the data?: none/xxx

   Comments:

   Known bugs: none (yet!)

   Todo:see author for testing and commenting the function

   http://www.doxygen.org
***********************************************************/
#ifdef SIM2D


#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <mxml.h>
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>


#include <IO_Robot/Simulateur_2D.h>

/* #define DEBUG*/
#ifndef AVEUGLE
void create_fenetre_sim2d(TxDonneesFenetre * fenetre);
gboolean cb_destroy_fenetre(GtkWidget * widget, gpointer data);
gboolean fenetre_expose_event(TxDonneesFenetre * fenetre, GtkWidget * widget, GdkEventExpose * event, gpointer data);

gboolean fenetre_configure_event(TxDonneesFenetre * fenetre, GtkWidget * widget, GdkEventConfigure * event, gpointer data);

TxDonneesFenetre sim2d_fenetre;
void sim2d_display();
#endif

Sim2d * prom_sim2d=NULL;


void new_function_sim2d_init(int gpe)
{
   int i=0,j,l;
   int nb_landmarks=0,nb_agents=0,nb_obstacles=0;
   char filename[255],param[255];

   FILE *fp;
   mxml_node_t *tree=NULL;
   mxml_node_t *node=NULL;
   mxml_node_t *node_sec=NULL;
   mxml_node_t * child = NULL;

   printf("new_function_sim2d_init\n");

   /*Alloue le pointeur vers Sim2d*/
   prom_sim2d=(Sim2d *)malloc(sizeof(Sim2d));
   if (prom_sim2d==NULL)
   {
      printf("error malloc dans sim2d_init\n");
      exit(-1);
   }

   prom_sim2d->step = 0;

   /*recupere le nom du fichier de descrption*/
   i = 0;
   l = find_input_link(gpe, i);
   while (l != -1)
   {

      if (prom_getopt(liaison[l].nom, "f", param) > 1)
      {
         sprintf(filename,"%s",param);
      }
      i++;
      l = find_input_link(gpe, i);

   }

   /*Cree l'arbre xml*/
   printf("XML loadFile %s \n",filename);
   if (filename == NULL)
   {
      fprintf(stderr,"%s \n",__FUNCTION__);
      exit(-1);
   }

   fp = fopen(filename, "r");
   if (fp != NULL)
   {
      tree = mxmlLoadFile(NULL, fp, MXML_OPAQUE_CALLBACK); /* "OPAQUE" pour avoir l'ensemble du texte contenu dans un noeud */
      fclose(fp);
   }
   else
   {
      fprintf(stderr, "echec ouverture fichier %s\n", filename);
      tree = NULL;
   }

   for (node = mxmlFindElement(tree, tree, "Agent", NULL, NULL, MXML_DESCEND); node != NULL; node = mxmlFindElement(node, tree, "Agent", NULL, NULL,MXML_DESCEND))
   {
      nb_agents++;
   }

   for (node = mxmlFindElement(tree, tree, "Landmark", NULL, NULL, MXML_DESCEND); node != NULL; node = mxmlFindElement(node, tree, "Landmark", NULL, NULL,MXML_DESCEND))
   {
      nb_landmarks++;
   }

   for (node = mxmlFindElement(tree, tree, "Obstacle", NULL, NULL, MXML_DESCEND); node != NULL; node = mxmlFindElement(node, tree, "Obstacle", NULL, NULL,MXML_DESCEND))
   {
      nb_obstacles++;
   }

   printf("nb_agents = %d, nb_obstacles = %d, nb_landmarks = %d\n",nb_agents,nb_obstacles,nb_landmarks);

   /*Les tailles des tableau sont dispo => allocation*/
   prom_sim2d->landmarks=(Landmark_sim2d **)malloc(nb_landmarks * sizeof(Landmark_sim2d*));
   for (i=0; i<nb_landmarks; i++)
   {
      prom_sim2d->landmarks[i]=(Landmark_sim2d *)malloc(sizeof(Landmark_sim2d));
   }
   prom_sim2d->nb_landmarks=nb_landmarks;

   prom_sim2d->agents=(Agent_sim2d **)malloc(nb_agents*sizeof(Agent_sim2d*));
   for (i=0; i<nb_agents; i++)
   {
      prom_sim2d->agents[i]=(Agent_sim2d *)malloc(sizeof(Agent_sim2d));
   }
   prom_sim2d->nb_agents=nb_agents;

   prom_sim2d->obstacles=(Obstacle_sim2d  **)malloc(nb_obstacles*sizeof(Obstacle_sim2d*));
   for (i=0; i<nb_obstacles; i++)
   {
      prom_sim2d->obstacles[i]=(Obstacle_sim2d *)malloc(sizeof(Obstacle_sim2d));
   }
   prom_sim2d->nb_obstacles=nb_obstacles;

   /*Il ne reste plus qu'a lire les champs*/

   /*    prom_sim2d->cste_temps=0.05;*/
   node = NULL;
   node = mxmlFindElement(tree, tree, "cste_temps", NULL, NULL, MXML_DESCEND);
   if (node!=NULL)
   {
      child = NULL;
      child = node->child;
      if (child == NULL)
      {
         EXIT_ON_ERROR("erreur pas de fils pour cste_temps \n");
      }
      dprints("valeur = %s \n",child->value.opaque);
      prom_sim2d->cste_temps= atof (child->value.opaque);
   }
   else   prom_sim2d->cste_temps=1.;


   /*    prom_sim2d->cste_spatial=0.2; */
   node = NULL;
   node = mxmlFindElement(tree, tree, "cste_spatial", NULL, NULL, MXML_DESCEND);
   if (node!=NULL)
   {
      child = NULL;
      child = node->child;
      if (child == NULL)
      {
         printf("erreur pas de fils pour cste_spatial \n");
         exit(-1);   /* -1 serait mieux non ??? PG */
      }
      dprints("valeur = %s \n",child->value.opaque);
      prom_sim2d->cste_spatial= atof (child->value.opaque);
   }
   else  prom_sim2d->cste_spatial=1.;


   /*    prom_sim2d->width=600;*/
   node = NULL;
   node = mxmlFindElement(tree, tree, "width", NULL, NULL, MXML_DESCEND);
   if (node!=NULL)
   {
      child = NULL;
      child = node->child;
      if (child == NULL)
      {
         EXIT_ON_ERROR("erreur pas de fils pour width \n");
      }
      dprints("valeur = %s \n",child->value.opaque);
      prom_sim2d->width= atoi (child->value.opaque);
   }
   else  prom_sim2d->width=500;

   /*  prom_sim2d->height=600;*/
   node = NULL;
   node = mxmlFindElement(tree, tree, "height", NULL, NULL, MXML_DESCEND);
   if (node!=NULL)
   {
      child = NULL;
      child = node->child;
      if (child == NULL)
      {
         EXIT_ON_ERROR("erreur pas de fils pour height\n");
      }
      dprints("valeur = %s \n",child->value.opaque);
      prom_sim2d->height= atoi (child->value.opaque);
   }
   else  prom_sim2d->height=500;

   dprints("parametre de sim2d:\n");
   dprints("\tcste_temps=%f\n\t cste_spatial=%f\n\t width=%d\n\t heigth=%d \n",prom_sim2d->cste_temps,prom_sim2d->cste_spatial,prom_sim2d->width,prom_sim2d->height);
   /*Creation agents*/

   i=0;
   for (node = mxmlFindElement(tree, tree, "Agent", NULL, NULL, MXML_DESCEND); node != NULL; node = mxmlFindElement(node, tree, "Agent", NULL, NULL,MXML_DESCEND))
   {
      /*Lit X*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "x", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->x = atof (child->value.opaque);
      }
      else   prom_sim2d->agents[i]->x =0.;

      /*Lit Y*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "y", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->y = atof (child->value.opaque);
      }
      else   prom_sim2d->agents[i]->y =0.;

      /*Lit Theta*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "theta", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->theta = atof (child->value.opaque);
      }
      else   prom_sim2d->agents[i]->theta =0.;

      /*Lit masse*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "masse", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->masse = atof (child->value.opaque);
      }
      else  prom_sim2d->agents[i]->masse =1.;

      /*lIT SPEED_MAX*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "speed_max", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->speed_max = atof (child->value.opaque);
      }
      else   prom_sim2d->agents[i]->speed_max =1.;

      /*lIT fORCE CENTRIFUGE MAX*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "FC_max", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->FC_max = atof (child->value.opaque);
      }
      else   prom_sim2d->agents[i]->FC_max =1.;

      /*lIT RADIUS PROX*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "radius_proximity", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->agents[i]->radius_proximity = atof (child->value.opaque);
      }
      else    prom_sim2d->agents[i]->radius_proximity =10.;
      dprints("Parametre agent %d:\n",i);
      dprints("\tx=%f\n\ty=%f\n\ttheta=%f\n\tmasse=%f\n\tspeedmax=%f\n\tFC_max=%f\n\tradius_prox=%f\n",prom_sim2d->agents[i]->x,prom_sim2d->agents[i]->y,prom_sim2d->agents[i]->theta,prom_sim2d->agents[i]->masse,prom_sim2d->agents[i]->speed_max,prom_sim2d->agents[i]->FC_max,prom_sim2d->agents[i]->radius_proximity);
      i++;
   }

   /*Met a jour la trace*/
   for (i=0; i<nb_agents; i++)
   {
      for (j=0; j<SIZE_TRACE; j++)
      {
         prom_sim2d->agents[i]->trace_x[j]=prom_sim2d->agents[i]->x;
         prom_sim2d->agents[i]->trace_y[j]=prom_sim2d->agents[i]->y;
         prom_sim2d->agents[i]->trace_theta[j]=prom_sim2d->agents[i]->theta;
      }
   }

   /*Creation des obstacles*/

   i=0;
   for (node = mxmlFindElement(tree, tree, "Obstacle", NULL, NULL, MXML_DESCEND);   node != NULL;  node = mxmlFindElement(node, tree, "Obstacle", NULL, NULL,MXML_DESCEND))
   {
      /*Lit XA*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "xa", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            EXIT_ON_ERROR("erreur pas de fils pour height\n");
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->obstacles[i]->xa = atoi (child->value.opaque);
      }
      else
         prom_sim2d->obstacles[i]->xa =0;

      /*Lit YA*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "ya", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->obstacles[i]->ya = atoi (child->value.opaque);
      }
      else
         prom_sim2d->obstacles[i]->ya =10;

      /*LIT XB*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "xb", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->obstacles[i]->xb = atoi (child->value.opaque);
      }
      else
         prom_sim2d->obstacles[i]->xb =10;

      /*Lit YB*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "yb", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->obstacles[i]->yb = atoi (child->value.opaque);
      }
      else
         prom_sim2d->obstacles[i]->yb =10;

      /*Lit HIGH*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "high", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->obstacles[i]->high = atoi (child->value.opaque);
      }
      else
         prom_sim2d->obstacles[i]->high = 1;

      i++;
   }


   /*Crestion des Landmarks*/

   i=0;
   for (node = mxmlFindElement(tree, tree, "Landmark", NULL, NULL, MXML_DESCEND);  node != NULL;  node = mxmlFindElement(node, tree, "Landmark", NULL, NULL,MXML_DESCEND))
   {
      /*Lit X*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "x", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->landmarks[i]->x = atoi (child->value.opaque);
      }
      else
         prom_sim2d->landmarks[i]->x =0;

      /*Lit Y*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "y", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->landmarks[i]->y = atoi (child->value.opaque);
      }
      else
         prom_sim2d->landmarks[i]->y =10;

      /*LIT visibility*/
      node_sec = NULL;
      node_sec = mxmlFindElement(node, tree, "always_visible", NULL, NULL, MXML_DESCEND);
      if (node_sec!=NULL)
      {
         child = NULL;
         child = node_sec->child;
         if (child == NULL)
         {
            printf("erreur pas de fils pour height\n");
            exit(-1);   /* -1 serait mieux non ??? PG */
         }
         dprints("valeur = %s \n",child->value.opaque);
         prom_sim2d->landmarks[i]->always_visible = atoi (child->value.opaque);
      }
      else   prom_sim2d->landmarks[i]->always_visible = 0;

      i++;
   }

   /*Creation de la fenetre*/
#ifndef AVEUGLE
   sprintf(sim2d_fenetre.name, "Env Sim2d");
   create_fenetre_sim2d(&sim2d_fenetre);
   gtk_widget_show_all(sim2d_fenetre.window);

   /*    sim2d_display();
   TxFlush(&sim2d_fenetre);*/
#endif


}


#ifndef AVEUGLE


gboolean sim2d_fenetre_configure_event(GtkWidget * widget, GdkEventConfigure * event, gpointer data)
{

   return fenetre_configure_event(&sim2d_fenetre, widget, event, data);
}

gboolean sim2d_fenetre_expose_event(GtkWidget * widget, GdkEventExpose * event, gpointer data)
{
   return fenetre_expose_event(&sim2d_fenetre, widget, event, data);
}

void create_fenetre_sim2d(TxDonneesFenetre * fenetre)
{
   GtkWidget *vbox1;
   GtkWidget *scrolledwindow1;
   GtkWidget *viewport1;

   fenetre->region_rectangle = NULL;
   fenetre->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",  G_CALLBACK(cb_destroy_fenetre), fenetre);

   gtk_widget_set_size_request(fenetre->window, 300, 350);
   gtk_object_set_data(GTK_OBJECT(fenetre->window), fenetre->name,  fenetre->window);
   gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);

   vbox1 = gtk_vbox_new(FALSE, 1);
   gtk_widget_ref(vbox1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "vbox_sim2d_fenetre", vbox1, (GtkDestroyNotify) gtk_widget_unref);

   gtk_widget_show(vbox1);
   gtk_container_add(GTK_CONTAINER(fenetre->window), vbox1);

   /* Pack it all together */

   scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
   gtk_widget_ref(scrolledwindow1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "scrolled_sim2d_fenetre", scrolledwindow1, (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(scrolledwindow1);
   gtk_box_pack_start(GTK_BOX(vbox1), scrolledwindow1, TRUE, TRUE, 0);

   viewport1 = gtk_viewport_new(NULL, NULL);
   gtk_widget_ref(viewport1);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "viewport_sim2d_fenetre", viewport1, (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(viewport1);
   gtk_container_add(GTK_CONTAINER(scrolledwindow1), viewport1);

   /*********************/

   fenetre->da = gtk_drawing_area_new();
   fenetre->width = prom_sim2d->width;
   fenetre->height = prom_sim2d->height;
   gtk_widget_set_size_request(fenetre->da, fenetre->width, fenetre->height);
   TxResetDisplayPosition(fenetre);

   fenetre->font = gdk_font_load("-*-courier-medium-r-*-*-*-110-*-*-*-*-*-*");

   gtk_widget_ref(fenetre->da);
   gtk_object_set_data_full(GTK_OBJECT(fenetre->window), "drawingarea_sim2d_fenetre",fenetre->da, (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show(fenetre->da);
   gtk_container_add(GTK_CONTAINER(viewport1), fenetre->da);

   /***************************************************/
   fenetre->pixmap = NULL;     /* voir configure_event pour l'init et l'affichage */


   fenetre->filew = NULL;
   fenetre->box1 = vbox1;      /* peut etre que NULL aurait ete mieux - pas de vumetre possible */

   /* Signals used to handle backing pixmap */

   g_signal_connect(fenetre->da, "expose_event", G_CALLBACK(sim2d_fenetre_expose_event), NULL);
   g_signal_connect(fenetre->da, "configure_event", G_CALLBACK(sim2d_fenetre_configure_event), NULL);
}
#endif


/*Cette fonction dessine tout*/
void sim2d_display()
{
#ifndef AVEUGLE
   int i;
   int nb_landmarks=prom_sim2d->nb_landmarks;
   int nb_agents=prom_sim2d->nb_agents;
   int nb_obstacles=prom_sim2d->nb_obstacles;
   TxDonneesFenetre * fenetre= &sim2d_fenetre; /*prom_sim2d->fenetre;*/
   TxPoint origine;
   TxPoint extremite;
   char sname_ele[255];
   int epaisseur;

   /*Efface tout */
   origine.x= 0;
   origine.y= 0;
   TxDessinerRectangle(fenetre,blanc,TxPlein,origine,1000,1000,1);

   /*dessine obstacle*/
   for (i=0; i<nb_obstacles; i++)
   {
      origine.x=    prom_sim2d->obstacles[i]->xa;
      origine.y=    prom_sim2d->obstacles[i]->ya;
      extremite.x=  prom_sim2d->obstacles[i]->xb;
      extremite.y=  prom_sim2d->obstacles[i]->yb;
      epaisseur=2+  prom_sim2d->obstacles[i]->high*3;
      TxDessinerSegment(fenetre,bleu,origine,extremite,epaisseur);
      origine.x=    prom_sim2d->obstacles[i]->xa+3;
      origine.y=    prom_sim2d->obstacles[i]->ya+13;
      sprintf(sname_ele,"%d",i);
      TxEcrireChaineSized(fenetre,bleu,origine,sname_ele,2,NULL);
   }

   for (i=0; i<nb_landmarks; i++)
   {
      epaisseur=1+  prom_sim2d->landmarks[i]->always_visible*2;
      origine.x=    prom_sim2d->landmarks[i]->x+5;
      origine.y=    prom_sim2d->landmarks[i]->y;
      extremite.x=  prom_sim2d->landmarks[i]->x-4;
      extremite.y=  prom_sim2d->landmarks[i]->y;
      TxDessinerSegment(fenetre,rouge,origine,extremite,epaisseur);

      origine.x=    prom_sim2d->landmarks[i]->x;
      origine.y=    prom_sim2d->landmarks[i]->y+5;
      extremite.x=  prom_sim2d->landmarks[i]->x;
      extremite.y=  prom_sim2d->landmarks[i]->y-4;
      TxDessinerSegment(fenetre,rouge,origine,extremite,epaisseur);

      origine.x=    prom_sim2d->landmarks[i]->x+3;
      origine.y=    prom_sim2d->landmarks[i]->y+13;

      sprintf(sname_ele,"%d",i);
      TxEcrireChaineSized(fenetre,rouge,origine,sname_ele,2,NULL);
   }

   for (i=0; i<nb_agents; i++)
   {
      origine.x=    (int)(prom_sim2d->agents[i]->x-5);
      origine.y=    (int)(prom_sim2d->agents[i]->y-5);
      TxDessinerRectangle(fenetre,vert,TxPlein,origine,10,10,1);

      origine.x=    (int)(prom_sim2d->agents[i]->x);
      origine.y=    (int)(prom_sim2d->agents[i]->y);
      extremite.x=  (int)(prom_sim2d->agents[i]->x + 10.*cos(prom_sim2d->agents[i]->theta));
      extremite.y=  (int)(prom_sim2d->agents[i]->y + 10.*sin(prom_sim2d->agents[i]->theta));
      TxDessinerSegment(fenetre,pelouse,origine,extremite,2);
   }
   /*Echelle*/

   origine.x=    15 ;
   origine.y=    15 ;
   extremite.x=  65 ;
   extremite.y=  15 ;
   TxDessinerSegment(fenetre,noir,origine,extremite,2);

   sprintf(sname_ele,"%.3f m",prom_sim2d->cste_spatial * 50.);
   origine.x=    15 ;
   origine.y=    25 ;
   TxEcrireChaineSized(fenetre,noir,origine,sname_ele,2,NULL);

   sprintf(sname_ele,"dt = %.3f ms", 1000.*prom_sim2d->cste_temps );
   origine.x=    15 ;
   origine.y=    35 ;
   TxEcrireChaineSized(fenetre,noir,origine,sname_ele,2,NULL);

   sprintf(sname_ele,"t = %.3f s",  prom_sim2d->cste_temps * prom_sim2d->step );
   origine.x=    15 ;
   origine.y=    45 ;
   TxEcrireChaineSized(fenetre,noir,origine,sname_ele,2,NULL);

   /*	TxFlush(fenetre); */
#endif
   prom_sim2d->step ++;
}


typedef struct MyData_f_sim2d_flushimage
{
   char filename[255];
   int savemode;
   int time_step;
} MyData_f_sim2d_flushimage;

void function_sim2d_flushimage(int gpe)
{
#ifndef AVEUGLE
   int savemode=0;
   char filename[255];
   int i,l;
   int time_step=0;
   char param[255];
   MyData_f_sim2d_flushimage * my_data;

   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */

   if (def_groupe[gpe].data == NULL)
   {
      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {
         if (prom_getopt(liaison[l].nom, "f", param) > 1)
         {
            savemode=1;
            sprintf(filename,"%s",param);
         }

         i++;
         l = find_input_link(gpe, i);
      }


      my_data =(MyData_f_sim2d_flushimage *) malloc(sizeof(MyData_f_sim2d_flushimage));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_flushimage\n");
         exit(-1);
      }
      my_data->savemode = savemode;
      my_data->time_step=time_step;
      sprintf(my_data->filename,"%s",filename);
      def_groupe[gpe].data =(MyData_f_sim2d_flushimage *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_flushimage *) (def_groupe[gpe].data);
      savemode = my_data->savemode;
      time_step=my_data->time_step;
   }

   TxFlush(fenetre);

   if (savemode==1)
   {
      sprintf(filename,"%s%6d.jpg",my_data->filename,time_step);
      TxFlush(fenetre);
      TxSaveWindowImage(fenetre, filename);
      my_data->time_step++;
   }
#else /* if blind */
   (void)gpe;
#endif

}


void function_sim2d_init(int numero)
{
   (void)numero;
#ifndef AVEUGLE
   sim2d_display();
   TxFlush(&sim2d_fenetre);
#endif
   printf("sim2d is ready !!\n");
   printf("Are you ready ??\n");
   getchar();
}

/*Cette fonction dessine tout*/
void function_sim2d_display(int numero)
{
   (void)numero;
   sim2d_display();
}

/*Cette fonction dessine la perception d'un landmark*/
void sim2d_agent_sequential_perception_display(Agent_sim2d * agent, int land_to_display)
{
#ifndef AVEUGLE
   TxPoint origine;
   TxPoint extremite;
   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */

   if (land_to_display<prom_sim2d->nb_landmarks)
   {
      origine.x=    agent->x;
      origine.y=    agent->y;
      extremite.x=  prom_sim2d->landmarks[land_to_display]->x;
      extremite.y=  prom_sim2d->landmarks[land_to_display]->y;
      TxDessinerSegment(fenetre,rouge,origine,extremite,1);
      /* 		TxFlush(fenetre);*/
   }
#else /* if blind */
   (void)agent;
   (void)land_to_display;
#endif

}

typedef struct MyData_f_sim2d_sequential_landmarks_perception
{
   int num_agent;
   int land_to_display;
   int gpe_reset;
   int graphic_display;
} MyData_f_sim2d_sequential_landmarks_perception;

/*La fonction sort sequentiellement les amers visibles*/
/**/
void function_sim2d_sequential_landmarks_perception(int gpe)
{
   float dx,dy,angle1 = 0.;
   MyData_f_sim2d_sequential_landmarks_perception *my_data=NULL;
   int i,j,l,gpe_reset=-1;
   float breaking=0.;

   int num_agent=-1,land_to_display=0,land_invisible;
   Obstacle_sim2d ** the_obstacles=prom_sim2d->obstacles;
   int nb_obstacles=prom_sim2d->nb_obstacles;
   int nb_landmarks=prom_sim2d->nb_landmarks;
   float the_agent_x;
   float the_agent_y;
   char param[255];

   float Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,r,s,den,num_r,num_s;
   float CALBG_x,CALBG_y,CALHD_x,CALHD_y,COBG_x,COBG_y,COHD_x,COHD_y;
   int graphic_display=0;

   if (def_groupe[gpe].data == NULL)
   {

      if (def_groupe[gpe].nbre != 3)
      {
         printf("il faut trois neurones a %s\n", __FUNCTION__);
         exit(-1);
      }

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         if (prom_getopt(liaison[l].nom, "d", param) > 1)
            graphic_display = atoi(param);
         if (strcmp(liaison[l].nom, "reset") ==0)
            gpe_reset = liaison[l].depart;

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      if (gpe_reset==-1)
      {
         printf("pas de groupe reset en entre de function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }


      my_data = (MyData_f_sim2d_sequential_landmarks_perception *) malloc(sizeof(MyData_f_sim2d_sequential_landmarks_perception));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->gpe_reset=gpe_reset;
      my_data->land_to_display=0;
      my_data->graphic_display=graphic_display;
      def_groupe[gpe].data =  (MyData_f_sim2d_sequential_landmarks_perception *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_sequential_landmarks_perception *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      land_to_display=my_data->land_to_display;
      gpe_reset=my_data->gpe_reset;
      graphic_display=my_data->graphic_display;
   }

   if (neurone[def_groupe[gpe_reset].premier_ele].s1>0.5)
   {
      land_to_display=0;
   }

   the_agent_x=prom_sim2d->agents[num_agent]->x;
   the_agent_y=prom_sim2d->agents[num_agent]->y;

   land_invisible=1;
   i=  land_to_display;
   while (i<nb_landmarks && land_invisible==1)
   {

      /*on suppose que le land est visible. Si un obstacle gene, on dira que le land est invisible*/
      /* printf("test land %d",i); */
      land_invisible=0;
      /*on verifie que le robot ne se trouve pas sur le landmarks*/
      if (fabs(the_agent_x - prom_sim2d->landmarks[i]->x) > 1. ||
            fabs(the_agent_y - prom_sim2d->landmarks[i]->y) > 1.)
      {
         if (prom_sim2d->landmarks[i]->always_visible==0)
         {
            /*calcul des intersection entre la droite robot-land et les droites obstacle*/
            for (j=0; j<nb_obstacles && land_invisible==0; j++)
            {
               /*obstacle  grand*/
               if (the_obstacles[j]->high==1)
               {
                  /*Intersection AB et CD ?*/
                  Ax=the_obstacles[j]->xa;
                  Ay=the_obstacles[j]->ya;
                  Bx=the_obstacles[j]->xb;
                  By=the_obstacles[j]->yb;

                  Cx=prom_sim2d->landmarks[i]->x;
                  Cy=prom_sim2d->landmarks[i]->y;
                  Dx=the_agent_x;
                  Dy=the_agent_y;

                  den=(Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
                  num_r=(Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
                  num_s=(Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);
                  if (isdiff(den,0.)) /*sinon droite parallele*/
                  {
                     r=num_r/den;
                     s=num_s/den;
                     /*Segment croises*/
                     /* printf("\t r=%f s=%f\n",r,s); */
                     if (r>=0.&&r<=1.&&s>=0.&&s<=1.)
                     {
                        land_invisible=1;
                     }
                  }
                  else
                  {
                     /*Segmant parallele*/
                     /*cohincidence*/
                     if (isequal(num_r,0.))
                     {
                        /* printf("lE CAS de deux segment cohincident obs %d\n",j); */
                        /*Si O1 ou O2 dans rect A L*/
                        if (Cx>=Dx)
                        {CALBG_x=Dx; CALHD_x=Cx;}
                        else
                        {CALBG_x=Cx; CALHD_x=Dx;}

                        if (Cy>=Dy)
                        {CALBG_y=Dy; CALHD_y=Cy;}
                        else
                        {CALBG_y=Cy; CALHD_y=Dy;}

                        if ( ( Ax>=CALBG_x && Ax<=CALHD_x ) && (Ay>=CALBG_y && Ay<=CALHD_y) )
                        {
                           /* printf("O1 dans rect AL\n"); */
                           land_invisible=1;
                        }
                        if ( ( Bx>=CALBG_x && Bx<=CALHD_x ) && (By>=CALBG_y && By<=CALHD_y) )
                        {
                           /*  printf("O2 dans rect AL\n"); */
                           land_invisible=1;
                        }
                        /*si A ou L dans rect O1 O2*/
                        if (Ax>=Bx)
                        {COBG_x=Bx; COHD_x=Ax;}
                        else
                        {COBG_x=Ax; COHD_x=Bx;}

                        if (Ay>=By)
                        {COBG_y=By; COHD_y=Ay;}
                        else
                        {COBG_y=Ay; COHD_y=By;}

                        if ( ( Cx>=COBG_x && Cx<=COHD_x ) && (Cy>=COBG_y && Cy<=COHD_y) )
                        {
                           /* printf("L dans rect OO\n"); */
                           land_invisible=1;
                        }
                        if ( ( Dx>=COBG_x && Dx<=COHD_x ) && (Dy>=COBG_y && Dy<=COHD_y) )
                        {
                           /* printf("A dans rect OO\n"); */
                           land_invisible=1;
                        }
                        getchar();

                     }
                  }

               }
               /*On ne test pas les obstale suivant si on a une invisibilite*/
               if (land_invisible==1)
                  break;

            }
         }
         /*Il ne faudra pas incrementer i si le land est visible*/
         if (land_invisible==0)
         {
            /*   printf(" .... visible \n");   */
            break;

         }
         else
         {
            /*  printf(" invisible \n"); */
         }
      }
      else
      {
         land_invisible=1;
      }
      i++;
   }

   land_to_display = i;

   /*Si on est arrivee au bout des obstacles, aucun obstacles visible donc, on donne le meme angle et un numero d'obstacle qui n'existe pas*/
   if (land_to_display==nb_landmarks)
   {
      angle1=0.;
   }
   else
   {
      dx=prom_sim2d->landmarks[land_to_display]->x -the_agent_x ;
      dy=prom_sim2d->landmarks[land_to_display]->y -the_agent_y ;

      angle1 = atan2(dy,dx);

      if (angle1 <= 0)    angle1 = angle1 + 2. * M_PI;
   }

   neurone[def_groupe[gpe].premier_ele].s = neurone[def_groupe[gpe].premier_ele].s1 = neurone[def_groupe[gpe].premier_ele].s2 = (float) land_to_display; /* landmark id */
   neurone[def_groupe[gpe].premier_ele + 1].s = neurone[def_groupe[gpe].premier_ele + 1].s1 = neurone[def_groupe[gpe].premier_ele + 1].s2 = angle1;    /* landmark azimuth */

   dprints("%s: \n\tid landmark:%d\n\tangle land:%f\n", __FUNCTION__,land_to_display, angle1);

#ifndef AVEUGLE
   if (graphic_display>0)  sim2d_agent_sequential_perception_display(prom_sim2d->agents[num_agent],land_to_display);
#endif

   if (land_to_display==nb_landmarks)
   {
      land_to_display = 0;
      breaking = 1.;
      printf("breaking now\n");
   }
   else
   {
      land_to_display++;
   }
   my_data->land_to_display=land_to_display;

   neurone[def_groupe[gpe].premier_ele + 2].s = neurone[def_groupe[gpe].premier_ele + 2].s1 = neurone[def_groupe[gpe].premier_ele + 2].s2 = (breaking);

   my_data->land_to_display=land_to_display;
   dprints("fin f_sequential_perception_break\n");
}


typedef struct MyData_f_sim2d_landmarks_perception
{
   int num_agent;
   int graphic_display;
} MyData_f_sim2d_landmarks_perception;

/*La fonction sort sequentiellement les amers visibles*/
/**/
void function_sim2d_landmarks_perception(int gpe)
{
   float dx,dy,angle1 = 0.;
   MyData_f_sim2d_landmarks_perception *my_data=NULL;
   int i,j,l;


   int num_agent=-1,land_invisible;
   Obstacle_sim2d ** the_obstacles=prom_sim2d->obstacles;
   int nb_obstacles=prom_sim2d->nb_obstacles;
   int nb_landmarks=prom_sim2d->nb_landmarks;
   float the_agent_x;
   float the_agent_y;
   char param[255];

   float Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,r,s,den,num_r,num_s;
   float CALBG_x,CALBG_y,CALHD_x,CALHD_y,COBG_x,COBG_y,COHD_x,COHD_y;
   int graphic_display=0;

   if (def_groupe[gpe].data == NULL)
   {
      if (def_groupe[gpe].taillex != 2)
      {
         printf("il faut 2 neurones en x a function_sim2d_landmarks_perception\n");
         exit(-1);
      }
      if (def_groupe[gpe].tailley < prom_sim2d->nb_landmarks)
      {
         printf("il faut nb_landmarks neurones en y function_sim2d_landmarks_perception\n");
         exit(-1);
      }

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         if (prom_getopt(liaison[l].nom, "d", param) > 1)
            graphic_display = atoi(param);

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }

      my_data =(MyData_f_sim2d_landmarks_perception *) malloc(sizeof(MyData_f_sim2d_landmarks_perception));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->graphic_display=graphic_display;
      def_groupe[gpe].data = (MyData_f_sim2d_landmarks_perception *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_landmarks_perception *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      graphic_display=my_data->graphic_display;
   }

   the_agent_x=prom_sim2d->agents[num_agent]->x;
   the_agent_y=prom_sim2d->agents[num_agent]->y;

   /*reset des neurones*/
   for (i=0; i<def_groupe[gpe].nbre; i++)
   {
      neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1= neurone[def_groupe[gpe].premier_ele+i].s2= 0. ;
   }


   for (i=0; i<nb_landmarks; i++)
   {
      /*on suppose que le land est visible. Si un obstacle gene, on dira que le land est invisible*/
      /* printf("test land %d",i); */
      land_invisible=0;
      /*on verifie que le robot ne se trouve pas sur le landmarks*/
      if (fabs(the_agent_x - prom_sim2d->landmarks[i]->x) > 1. ||
            fabs(the_agent_y - prom_sim2d->landmarks[i]->y) > 1.)
      {
         if (prom_sim2d->landmarks[i]->always_visible==0)
         {
            /*calcul des intersection entre la droite robot-land et les droites obstacle*/
            for (j=0; j<nb_obstacles && land_invisible==0; j++)
            {
               /*obstacle  grand*/
               if (the_obstacles[j]->high==1)
               {
                  /*Intersection AB et CD ?*/
                  Ax=the_obstacles[j]->xa;
                  Ay=the_obstacles[j]->ya;
                  Bx=the_obstacles[j]->xb;
                  By=the_obstacles[j]->yb;

                  Cx=prom_sim2d->landmarks[i]->x;
                  Cy=prom_sim2d->landmarks[i]->y;
                  Dx=the_agent_x;
                  Dy=the_agent_y;

                  den=(Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
                  num_r=(Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
                  num_s=(Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);
                  if (isdiff(den,0.)) /*sinon droite parallele*/
                  {
                     r=num_r/den;
                     s=num_s/den;
                     /*Segment croises*/
                     /*    printf("\t r=%f s=%f\n",r,s); */
                     if (r>=0.&&r<=1.&&s>=0.&&s<=1.)
                     {
                        land_invisible=1;
                     }
                  }
                  else
                  {
                     /*Segmant parallele*/
                     /*cohincidence*/
                     if (isequal(num_r,0.))
                     {
                        /*                         printf("lE CAS de deux segment cohincident obs %d\n",j); */
                        /*Si O1 ou O2 dans rect A L*/
                        if (Cx>=Dx)
                        {CALBG_x=Dx; CALHD_x=Cx;}
                        else
                        {CALBG_x=Cx; CALHD_x=Dx;}

                        if (Cy>=Dy)
                        {CALBG_y=Dy; CALHD_y=Cy;}
                        else
                        {CALBG_y=Cy; CALHD_y=Dy;}

                        if ( ( Ax>=CALBG_x && Ax<=CALHD_x ) && (Ay>=CALBG_y && Ay<=CALHD_y) )
                        {
                           /*                            printf("O1 dans rect AL\n"); */
                           land_invisible=1;
                        }
                        if ( ( Bx>=CALBG_x && Bx<=CALHD_x ) && (By>=CALBG_y && By<=CALHD_y) )
                        {
                           /*                            printf("O2 dans rect AL\n"); */
                           land_invisible=1;
                        }
                        /*si A ou L dans rect O1 O2*/
                        if (Ax>=Bx)
                        {COBG_x=Bx; COHD_x=Ax;}
                        else
                        {COBG_x=Ax; COHD_x=Bx;}

                        if (Ay>=By)
                        {COBG_y=By; COHD_y=Ay;}
                        else
                        {COBG_y=Ay; COHD_y=By;}

                        if ( ( Cx>=COBG_x && Cx<=COHD_x ) && (Cy>=COBG_y && Cy<=COHD_y) )
                        {
                           /*                            printf("L dans rect OO\n"); */
                           land_invisible=1;
                        }
                        if ( ( Dx>=COBG_x && Dx<=COHD_x ) && (Dy>=COBG_y && Dy<=COHD_y) )
                        {
                           /*                            printf("A dans rect OO\n"); */
                           land_invisible=1;
                        }
                        getchar();

                     }
                  }

               }
               /*On ne test pas les obstale suivant si on a une invisibilite*/
               if (land_invisible==1)   break;
            }
         }
      }
      else
      {
         land_invisible=1;
      }

      /*ici, on sait si le land i est visible*/


      if (land_invisible==0)
      {
         /*          printf(" .... visible \n");   */
         neurone[def_groupe[gpe].premier_ele+2*i].s=neurone[def_groupe[gpe].premier_ele+2*i].s1= neurone[def_groupe[gpe].premier_ele+2*i].s2= (1.-land_invisible) ;
         dx=prom_sim2d->landmarks[i]->x -the_agent_x ;
         dy=prom_sim2d->landmarks[i]->y -the_agent_y ;

         angle1 = atan2(dy,dx);

         if (angle1 < 0) angle1 = angle1 + 2. * M_PI;

         neurone[def_groupe[gpe].premier_ele+2*i+1].s=neurone[def_groupe[gpe].premier_ele+2*i+1].s1= neurone[def_groupe[gpe].premier_ele+2*i+1].s2= angle1 ;

         if (graphic_display>0)	sim2d_agent_sequential_perception_display(prom_sim2d->agents[num_agent],i);
      }
      else
      {
         /*         printf(" invisible \n");  */
      }
   }

   dprints("fin f_sequential_perception_break\n");
}



typedef struct MyData_f_sim2d_move_agent_abs
{
   int num_agent;
   int gpe_mvt;

} MyData_f_sim2d_move_agent_abs;


void function_sim2d_move_agent_abs(int gpe)
{
   int num_agent=-1;
   int gpe_mvt=-1;
   int i,l,imax;
   float max,angle,dx,dy,prec_x,prec_y,prec_theta;
   char param[255];
   MyData_f_sim2d_move_agent_abs * my_data;

   if (def_groupe[gpe].data == NULL)
   {
      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         if (strcmp(liaison[l].nom, "mvt") ==0)
            gpe_mvt = liaison[l].depart;

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_move_agent_abs\n");
         exit(-1);
      }
      if (gpe_mvt==-1)
      {
         printf("pas de groupe mvt en entre de function_sim2d_move_agent_abs\n");
         exit(-1);
      }


      my_data =(MyData_f_sim2d_move_agent_abs *)  malloc(sizeof(MyData_f_sim2d_move_agent_abs));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->gpe_mvt=gpe_mvt;
      def_groupe[gpe].data = (MyData_f_sim2d_move_agent_abs *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_move_agent_abs *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      gpe_mvt=my_data->gpe_mvt;
   }

   /*Cherche le max dans gpe_mvt.   Si tout < 0.1 => immobile*/
   max=0.1;
   imax=-1;

   for (i=0; i<def_groupe[gpe_mvt].nbre; i++)
   {
      if (neurone[def_groupe[gpe_mvt].premier_ele+i].s1>max)
      {
         max=neurone[def_groupe[gpe_mvt].premier_ele+i].s1;
         imax=i;
      }
   }

   angle=(2.*M_PI*(float)(imax))/((float)(def_groupe[gpe_mvt].nbre));

   prec_x=prom_sim2d->agents[num_agent]->x;
   prec_y=prom_sim2d->agents[num_agent]->y;
   prec_theta=prom_sim2d->agents[num_agent]->theta;

   if (imax!=-1)
   {
      dx = cos(angle) * prom_sim2d->agents[num_agent]->speed_max*prom_sim2d->cste_temps/prom_sim2d->cste_spatial;
      dy = sin(angle) * prom_sim2d->agents[num_agent]->speed_max*prom_sim2d->cste_temps/prom_sim2d->cste_spatial;

      prom_sim2d->agents[num_agent]->x = prom_sim2d->agents[num_agent]->x + dx;
      prom_sim2d->agents[num_agent]->y = prom_sim2d->agents[num_agent]->y + dy;

      prom_sim2d->agents[num_agent]->theta=angle;
   }

   /*mise a jour de la trace*/
   for (i=SIZE_TRACE-1; i>=1; i--)
   {
      prom_sim2d->agents[num_agent]->trace_x[i]=prom_sim2d->agents[num_agent]->trace_x[i-1];
      prom_sim2d->agents[num_agent]->trace_y[i]=prom_sim2d->agents[num_agent]->trace_y[i-1];
      prom_sim2d->agents[num_agent]->trace_theta[i]=prom_sim2d->agents[num_agent]->trace_theta[i-1];

   }
   prom_sim2d->agents[num_agent]->trace_x[0]=prec_x;
   prom_sim2d->agents[num_agent]->trace_y[0]=prec_y;
   prom_sim2d->agents[num_agent]->trace_theta[0]=prec_theta;
}


typedef struct MyData_f_sim2d_move_agent_rel
{
   int num_agent;
   int gpe_mvt;
   int gpe_speed_linear;
} MyData_f_sim2d_move_agent_rel;


void function_sim2d_move_agent_rel(int gpe)
{
   int num_agent=-1;
   int gpe_mvt=-1,gpe_speed_linear=-1;
   int i,l,imax;
   float max,angle,dx,dy,prec_x,prec_y,prec_theta;
   char param[255];
   MyData_f_sim2d_move_agent_rel * my_data;

   float factor_speed_linear=1.;
   float wmax,neural_speed;

   if (def_groupe[gpe].data == NULL)
   {

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         if (strcmp(liaison[l].nom, "mvt") ==0)
            gpe_mvt = liaison[l].depart;
         if (strcmp(liaison[l].nom, "speed_linear") ==0)
            gpe_speed_linear = liaison[l].depart;

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_move_agent_rel\n");
         exit(-1);
      }
      if (gpe_mvt==-1)
      {
         printf("pas de groupe mvt en entre de function_sim2d_move_agent_rel\n");
         exit(-1);
      }


      my_data = (MyData_f_sim2d_move_agent_rel *)
                malloc(sizeof(MyData_f_sim2d_move_agent_rel));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->gpe_mvt=gpe_mvt;
      my_data->gpe_speed_linear=gpe_speed_linear;
      def_groupe[gpe].data =
         (MyData_f_sim2d_move_agent_rel *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_move_agent_rel *) (def_groupe[gpe].data);
      gpe_speed_linear=my_data->gpe_speed_linear;
      num_agent = my_data->num_agent;
      gpe_mvt=my_data->gpe_mvt;
   }

   if (gpe_speed_linear != -1)
      factor_speed_linear = neurone[def_groupe[gpe_speed_linear].premier_ele].s1;

   if (factor_speed_linear>1.)
      factor_speed_linear=1.;

   /*Cherche le max dans gpe_mvt.   Si tout < 0.1 => immobile*/
   max=0.;
   imax=-1;

   for (i=0; i<def_groupe[gpe_mvt].nbre; i++)
   {
      if (neurone[def_groupe[gpe_mvt].premier_ele+i].s1>max)
      {
         max=neurone[def_groupe[gpe_mvt].premier_ele+i].s1;
         imax=i;
      }
   }

   prec_x=prom_sim2d->agents[num_agent]->x;
   prec_y=prom_sim2d->agents[num_agent]->y;
   prec_theta=prom_sim2d->agents[num_agent]->theta;

   if (imax!=-1)
   {
      wmax=prom_sim2d->agents[num_agent]->FC_max/prom_sim2d->agents[num_agent]->masse/prom_sim2d->agents[num_agent]->speed_max;

      neural_speed = imax - (def_groupe[gpe_mvt].nbre / 2) /*- imax*/;

      printf("neural speed = %f \n" , neural_speed);
      angle = /*speed_angular =*/ neural_speed * wmax /((float)(def_groupe[gpe_mvt].nbre / 2));
      /*       printf("cste_temps = %f cste_spatial = %f \n",prom_sim2d->cste_temps,prom_sim2d->cste_spatial); */

      angle = angle * prom_sim2d->cste_temps;

      printf("wmax-s= %f deg/s    wmax-step= %f deg/step\n",wmax*360./2./M_PI, wmax*prom_sim2d->cste_temps*360./2./M_PI);


      /*
        if(neural_speed>=0.)
        angle=wmax*prom_sim2d->cste_temps;
        else
        angle=-wmax*prom_sim2d->cste_temps;
      */
      printf("tourne de %f deg\n",angle*360./2./M_PI);

      dx = cos(angle+prec_theta) * factor_speed_linear * prom_sim2d->agents[num_agent]->speed_max*prom_sim2d->cste_temps/prom_sim2d->cste_spatial;
      dy = sin(angle+prec_theta) * factor_speed_linear * prom_sim2d->agents[num_agent]->speed_max* prom_sim2d->cste_temps/prom_sim2d->cste_spatial;

      prom_sim2d->agents[num_agent]->x = prom_sim2d->agents[num_agent]->x + dx;
      prom_sim2d->agents[num_agent]->y = prom_sim2d->agents[num_agent]->y + dy;


      prom_sim2d->agents[num_agent]->theta= angle + prec_theta;

      while (prom_sim2d->agents[num_agent]->theta<=0. || prom_sim2d->agents[num_agent]->theta>2.*M_PI)
      {
         if (prom_sim2d->agents[num_agent]->theta<=0.)
            prom_sim2d->agents[num_agent]->theta=prom_sim2d->agents[num_agent]->theta+2.*M_PI;
         else if (prom_sim2d->agents[num_agent]->theta>M_PI)
            prom_sim2d->agents[num_agent]->theta=prom_sim2d->agents[num_agent]->theta-2.*M_PI;
      }
   }

   /*mise a jour de la trace*/
   for (i=SIZE_TRACE-1; i>=1; i--)
   {
      prom_sim2d->agents[num_agent]->trace_x[i]=prom_sim2d->agents[num_agent]->trace_x[i-1];
      prom_sim2d->agents[num_agent]->trace_y[i]=prom_sim2d->agents[num_agent]->trace_y[i-1];
      prom_sim2d->agents[num_agent]->trace_theta[i]=prom_sim2d->agents[num_agent]->trace_theta[i-1];

   }
   prom_sim2d->agents[num_agent]->trace_x[0]=prec_x;
   prom_sim2d->agents[num_agent]->trace_y[0]=prec_y;
   prom_sim2d->agents[num_agent]->trace_theta[0]=prec_theta;
}


void function_sim2d_move_agent_rel_non_smooth(int gpe)
{
   int num_agent=-1;
   int gpe_mvt=-1,gpe_speed_linear=-1;
   int i,l,imax;
   float max,angle,dx,dy,prec_x,prec_y,prec_theta;
   char param[255];
   MyData_f_sim2d_move_agent_rel * my_data;

   float factor_speed_linear=1.;
   float wmax,neural_speed,neural_angle;

   if (def_groupe[gpe].data == NULL)
   {

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1) num_agent = atoi(param);
         if (strcmp(liaison[l].nom, "mvt") ==0) gpe_mvt = liaison[l].depart;
         if (strcmp(liaison[l].nom, "speed_linear") ==0) gpe_speed_linear = liaison[l].depart;

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_move_agent_rel\n");
         exit(-1);
      }
      if (gpe_mvt==-1)
      {
         printf("pas de groupe mvt en entre de function_sim2d_move_agent_rel\n");
         exit(-1);
      }


      my_data = (MyData_f_sim2d_move_agent_rel *) malloc(sizeof(MyData_f_sim2d_move_agent_rel));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->gpe_mvt=gpe_mvt;
      my_data->gpe_speed_linear=gpe_speed_linear;
      def_groupe[gpe].data = (MyData_f_sim2d_move_agent_rel *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_move_agent_rel *) (def_groupe[gpe].data);
      gpe_speed_linear=my_data->gpe_speed_linear;
      num_agent = my_data->num_agent;
      gpe_mvt=my_data->gpe_mvt;
   }

   if (gpe_speed_linear != -1)
      factor_speed_linear = neurone[def_groupe[gpe_speed_linear].premier_ele].s1;

   if (factor_speed_linear>1.)
      factor_speed_linear=1.;

   /*Cherche le max dans gpe_mvt.   Si tout < 0.1 => immobile*/
   max=0.;
   imax=-1;

   for (i=0; i<def_groupe[gpe_mvt].nbre; i++)
   {
      if (neurone[def_groupe[gpe_mvt].premier_ele+i].s1>max)
      {
         max=neurone[def_groupe[gpe_mvt].premier_ele+i].s1;
         imax=i;
      }
   }

   prec_x=prom_sim2d->agents[num_agent]->x;
   prec_y=prom_sim2d->agents[num_agent]->y;
   prec_theta=prom_sim2d->agents[num_agent]->theta;

   if (imax!=-1)
   {
      wmax=prom_sim2d->agents[num_agent]->FC_max/prom_sim2d->agents[num_agent]->masse/prom_sim2d->agents[num_agent]->speed_max;

      neural_speed = imax - (def_groupe[gpe_mvt].nbre / 2) /*- imax*/;

      neural_angle =  neural_speed /((float)(def_groupe[gpe_mvt].nbre / 2)) * M_PI;

      if (neural_angle > wmax * prom_sim2d->cste_temps )   angle=wmax*prom_sim2d->cste_temps;
      else  angle=neural_angle;

      printf("neural speed = %f \n" , neural_speed);
      printf("neural angle = %f \n" , neural_speed*360./2./M_PI);
      printf("angle = %f \n" , neural_speed*360./2./M_PI);

      printf("wmax-s= %f deg/s    wmax-step= %f deg/step\n",wmax*360./2./M_PI, wmax*prom_sim2d->cste_temps*360./2./M_PI);

      printf("tourne de %f deg\n",angle*360./2./M_PI);

      dx = cos(angle+prec_theta) * factor_speed_linear * prom_sim2d->agents[num_agent]->speed_max*prom_sim2d->cste_temps/prom_sim2d->cste_spatial;
      dy = sin(angle+prec_theta) * factor_speed_linear * prom_sim2d->agents[num_agent]->speed_max* prom_sim2d->cste_temps/prom_sim2d->cste_spatial;

      prom_sim2d->agents[num_agent]->x = prom_sim2d->agents[num_agent]->x + dx;
      prom_sim2d->agents[num_agent]->y = prom_sim2d->agents[num_agent]->y + dy;


      prom_sim2d->agents[num_agent]->theta= angle + prec_theta;

      while (prom_sim2d->agents[num_agent]->theta<=0. || prom_sim2d->agents[num_agent]->theta>2.*M_PI)
      {
         if (prom_sim2d->agents[num_agent]->theta<=0.) prom_sim2d->agents[num_agent]->theta=prom_sim2d->agents[num_agent]->theta+2.*M_PI;
         else if (prom_sim2d->agents[num_agent]->theta>M_PI) prom_sim2d->agents[num_agent]->theta=prom_sim2d->agents[num_agent]->theta-2.*M_PI;
      }
   }

   /*mise a jour de la trace*/
   for (i=SIZE_TRACE-1; i>=1; i--)
   {
      prom_sim2d->agents[num_agent]->trace_x[i]=prom_sim2d->agents[num_agent]->trace_x[i-1];
      prom_sim2d->agents[num_agent]->trace_y[i]=prom_sim2d->agents[num_agent]->trace_y[i-1];
      prom_sim2d->agents[num_agent]->trace_theta[i]=prom_sim2d->agents[num_agent]->trace_theta[i-1];

   }
   prom_sim2d->agents[num_agent]->trace_x[0]=prec_x;
   prom_sim2d->agents[num_agent]->trace_y[0]=prec_y;
   prom_sim2d->agents[num_agent]->trace_theta[0]=prec_theta;
}


void  sim2d_agent_display(Agent_sim2d * agent, int erase, int couleur)
{
#ifndef AVEUGLE
   TxPoint origine,extremite;
   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */

   /*Retire la position pr2cedente et la trace */
   if (erase!=0)
   {
      origine.x=    (int)(agent->trace_x[0]-5);
      origine.y=    (int)(agent->trace_y[0]-5);
      TxDessinerRectangle(fenetre ,blanc,TxPlein,origine,10,10, 1);

   }
   /*affiche position courante*/
   origine.x=    (int)(agent->x-5);
   origine.y=    (int)(agent->y-5);
   TxDessinerRectangle_rgb(fenetre,couleur,TxPlein,origine,10,10,1);

   origine.x=    (int)(agent->x);
   origine.y=    (int)(agent->y);
   extremite.x=  (int)(agent->x + 10.*cos(agent->theta));
   extremite.y=  (int)(agent->y + 10.*sin(agent->theta));
   TxDessinerSegment(fenetre,pelouse,origine,extremite,2);

   /*affiche la trace avec un gradient de couleur*/
   /*
     TxFlush(fenetre);
   */
#else /* if blind */
   (void) agent;
   (void) erase;
   (void) couleur;
#endif
}

void  sim2d_agent_trace_display(Agent_sim2d * agent)
{
#ifndef AVEUGLE
   TxPoint origine,extremite;
   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */
   int i;

   /*Retire la position pr2cedente et la trace */

   origine.x=    (int)(agent->trace_x[SIZE_TRACE-2]);
   origine.y=    (int)(agent->trace_y[SIZE_TRACE-2]);
   extremite.x=  (int)(agent->trace_x[SIZE_TRACE-1]);
   extremite.y=  (int)(agent->trace_y[SIZE_TRACE-1]);

   /*           TxDessinerPoint_rgb16M(fenetre,rgb16M(255,255,255),origine,extremite,2); */
   TxDessinerPoint_rgb16M(fenetre,rgb16M(255,255,255),extremite);
   for (i=0; i<SIZE_TRACE-2; i++)
   {
      origine.x=    (int)(agent->trace_x[i]);
      origine.y=    (int)(agent->trace_y[i]);
      extremite.x=  (int)(agent->trace_x[i+1]);
      extremite.y=  (int)(agent->trace_y[i+1]);

      /*  TxDessinerSegment_rgb16M(fenetre,colormap_1(1.-i/((float)(2*SIZE_TRACE))),origine,extremite,2); */
      TxDessinerPoint_rgb16M(fenetre,colormap_1(1.-i/((float)(2*SIZE_TRACE))),extremite);
   }

   /*       TxFlush(fenetre); */
#else /* if blind */
   (void)agent;
#endif
}


typedef struct MyData_f_sim2d_display_agent
{
   int num_agent;
   int trace;
   int erase;
   int gpe_color;
} MyData_f_sim2d_display_agent;

void function_sim2d_display_agent(int gpe)
{
#ifndef AVEUGLE
   int num_agent=-1;
   int trace=0;
   int erase=0;
   int i,l;
   char param[255];
   int gpe_color=-1;
   int couleur,imax;
   float max;

   MyData_f_sim2d_display_agent * my_data;

   if (def_groupe[gpe].data == NULL)
   {

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {
         if (prom_getopt(liaison[l].nom, "a", param) > 1)	num_agent = atoi(param);
         if (prom_getopt(liaison[l].nom, "t", param) > 0)   trace = 1;
         if (prom_getopt(liaison[l].nom, "e", param) > 0)	erase = 1;
         if (strcmp(liaison[l].nom, "color")== 0)	gpe_color = liaison[l].depart;

         i++;
         l = find_input_link(gpe, i);
      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_display_agent\n");
         exit(-1);
      }

      my_data = (MyData_f_sim2d_display_agent *) malloc(sizeof(MyData_f_sim2d_display_agent));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_display_agent\n");
         exit(-1);
      }

      my_data->num_agent = num_agent;
      my_data->trace=trace;
      my_data->erase=erase;
      my_data->gpe_color=gpe_color;
      def_groupe[gpe].data = (MyData_f_sim2d_display_agent *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_display_agent *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      trace=my_data->trace;
      erase=my_data->erase;
      gpe_color=my_data->gpe_color;
   }

   if (gpe_color==-1)
      couleur=vert;
   else
   {
      max=-1.;
      imax=0;
      for (i=0; i<def_groupe[gpe_color].nbre; i++)
      {
         if (neurone[i+def_groupe[gpe_color].premier_ele].s1>max)
         {
            max=neurone[i+def_groupe[gpe_color].premier_ele].s1;
            imax=i;
         }
      }
      couleur= imax * 4000 + 5;
   }

   sim2d_agent_display(prom_sim2d->agents[num_agent],erase,couleur);

   if (trace>0)
      sim2d_agent_trace_display(prom_sim2d->agents[num_agent]);
#else /* if blind*/
   (void)gpe;
#endif
}

void sim2d_agent_proximity_display(Agent_sim2d * agent, float dir, float act)
{
#ifndef AVEUGLE
   TxPoint origine;
   TxPoint extremite;
   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */

   float radius;

   radius=agent->radius_proximity*(1.-act)/prom_sim2d->cste_spatial;
   origine.x=    agent->x+10*cos(dir);
   origine.y=    agent->y+10*sin(dir);
   extremite.x=  agent->x+radius*cos(dir);
   extremite.y=  agent->y+radius*sin(dir);
   TxDessinerSegment(fenetre,ciel,origine,extremite,1);
#else /* if blind */
   (void)agent;
   (void) dir;
   (void) act;
#endif

}

typedef struct MyData_f_sim2d_agent_proximity
{
   int num_agent;
   int graphic_display;
} MyData_f_sim2d_agent_proximity;

void function_sim2d_agent_proximity(int gpe)
{
   int num_agent=-1;
   int graphic_display=0;
   int i,l;
   char param[255];
   MyData_f_sim2d_agent_proximity * my_data;
   float x_agent,y_agent,theta_agent;
   float Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,r,s,den,num_r,num_s,radius_proximity,px,py,dir_laser,ACT,ACT_MAX,Ix,Iy,D,CALBG_x,CALBG_y,COBG_x,COBG_y,CALHD_x,CALHD_y,COHD_x,COHD_y;
   int nb_obstacles=prom_sim2d->nb_obstacles,j,obs_invisible;
   Obstacle_sim2d ** the_obstacles = prom_sim2d->obstacles;

   (void) obs_invisible; // (unused)

   if (def_groupe[gpe].data == NULL)
   {

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         if (prom_getopt(liaison[l].nom, "d", param) > 1)
            graphic_display = atoi(param);

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_agent_proximity\n");
         exit(-1);
      }
      my_data =  (MyData_f_sim2d_agent_proximity *)  malloc(sizeof(MyData_f_sim2d_agent_proximity));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_agent_proximity\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->graphic_display = graphic_display;
      def_groupe[gpe].data = (MyData_f_sim2d_agent_proximity *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_agent_proximity *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      graphic_display = my_data->graphic_display;
   }

   x_agent=prom_sim2d->agents[num_agent]->x;
   y_agent=prom_sim2d->agents[num_agent]->y;
   theta_agent=prom_sim2d->agents[num_agent]->theta;
   radius_proximity=prom_sim2d->agents[num_agent]->radius_proximity;
   /*Pour toutes les direction, on calcule l'intersection des segment */

   for (i=0; i<def_groupe[gpe].nbre; i++)
   {
      /*calcul du point lointain*/
      dir_laser=(i -(def_groupe[gpe].nbre/2)) * 2. * M_PI / ((float)(def_groupe[gpe].nbre));

      px=x_agent+radius_proximity*cos(dir_laser+theta_agent)/prom_sim2d->cste_spatial;
      py=y_agent+radius_proximity*sin(dir_laser+theta_agent)/prom_sim2d->cste_spatial;
      ACT=0.0;
      ACT_MAX=0.;
      for (j=0; j<nb_obstacles; j++)
      {
         /*Intersection AB et CD ?*/
         Ax=the_obstacles[j]->xa;
         Ay=the_obstacles[j]->ya;
         Bx=the_obstacles[j]->xb;
         By=the_obstacles[j]->yb;

         Cx=px;
         Cy=py;
         Dx=x_agent;
         Dy=y_agent;

         den=(Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
         num_r=(Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
         num_s=(Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);
         if (isdiff(den,0.)) /*sinon droite parallele*/
         {
            r=num_r/den;
            s=num_s/den;
            /*Segment croises*/
            /*                       printf("\t r=%f s=%f\n",r,s); */
            if (r>=0.&&r<=1.&&s>=0.&&s<=1.)
            {
               Ix=Ax+r*(Bx-Ax);
               Iy=Ay+r*(By-Ay);
               D=sqrt( (Ix-Dx)*(Ix-Dx)+ (Iy-Dy)*(Iy-Dy));
               ACT=1.-D/(radius_proximity/prom_sim2d->cste_spatial);
            }
         }
         else
         {
            /*Segmant parallele*/
            /*cohincidence*/
            if (isequal(num_r,0.))
            {
               printf("lE CAS de deux segment cohincident obs %d\n",j);
               /*Si O1 ou O2 dans rect A L*/
               if (Cx>=Dx)
               {CALBG_x=Dx; CALHD_x=Cx;}
               else
               {CALBG_x=Cx; CALHD_x=Dx;}

               if (Cy>=Dy)
               {CALBG_y=Dy; CALHD_y=Cy;}
               else
               {CALBG_y=Cy; CALHD_y=Dy;}

               if ( ( Ax>=CALBG_x && Ax<=CALHD_x ) && (Ay>=CALBG_y && Ay<=CALHD_y) )
               {  printf("O1 dans rect AP\n"); obs_invisible=1;}
               if ( ( Bx>=CALBG_x && Bx<=CALHD_x ) && (By>=CALBG_y && By<=CALHD_y) )
               {  printf("O2 dans rect AP\n");  obs_invisible=1;}
               /*si A ou L dans rect O1 O2*/
               if (Ax>=Bx)
               {COBG_x=Bx; COHD_x=Ax;}
               else
               {COBG_x=Ax; COHD_x=Bx;}

               if (Ay>=By)
               {COBG_y=By; COHD_y=Ay;}
               else
               {COBG_y=Ay; COHD_y=By;}

               if ( ( Cx>=COBG_x && Cx<=COHD_x ) && (Cy>=COBG_y && Cy<=COHD_y) )
               {  printf("P dans rect OO\n"); obs_invisible=1;}
               if ( ( Dx>=COBG_x && Dx<=COHD_x ) && (Dy>=COBG_y && Dy<=COHD_y) )
               {printf("A dans rect OO\n"); obs_invisible=1;}
            }
         }
         if (ACT>=ACT_MAX)
            ACT_MAX=ACT;
      }
#ifndef AVEUGLE
      if (graphic_display==1)
         sim2d_agent_proximity_display(prom_sim2d->agents[num_agent],dir_laser+theta_agent,ACT_MAX);

      /*         TxFlush(fenetre); */
#endif
      neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2=ACT_MAX;
   }
}


typedef struct MyData_f_sim2d_agent_position
{
   int num_agent;
} MyData_f_sim2d_agent_position;

void function_sim2d_agent_position(int gpe)
{
   int num_agent=-1;
   int i,l;
   char param[255];
   MyData_f_sim2d_agent_position * my_data;
   float x_agent,y_agent,theta_agent;

   if (def_groupe[gpe].data == NULL)
   {
      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)
            num_agent = atoi(param);
         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_agent_position\n");
         exit(-1);
      }

      if (def_groupe[gpe].nbre!=3)
      {
         printf("il faut 3 neurones dans function_sim2d_agent_position\n");
         exit(-1);
      }
      my_data = (MyData_f_sim2d_agent_position *)
                malloc(sizeof(MyData_f_sim2d_agent_position));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      def_groupe[gpe].data = (MyData_f_sim2d_agent_position *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_agent_position *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
   }

   x_agent=prom_sim2d->agents[num_agent]->x;
   y_agent=prom_sim2d->agents[num_agent]->y;
   theta_agent=prom_sim2d->agents[num_agent]->theta;

   neurone[def_groupe[gpe].premier_ele].s=neurone[def_groupe[gpe].premier_ele].s1=neurone[def_groupe[gpe].premier_ele].s2=x_agent;
   neurone[def_groupe[gpe].premier_ele+1].s=neurone[def_groupe[gpe].premier_ele+1].s1=neurone[def_groupe[gpe].premier_ele+1].s2=y_agent;
   neurone[def_groupe[gpe].premier_ele+2].s=neurone[def_groupe[gpe].premier_ele+2].s1=neurone[def_groupe[gpe].premier_ele+2].s2=theta_agent;
}


typedef struct MyData_f_sim2d_place_cell_simul
{
   int gpe_vigilence;
   float rhoL;
} MyData_f_sim2d_place_cell_simul;

void function_sim2d_place_cell_simul(int gpe)
{
   int i,l;
   char param[255];
   MyData_f_sim2d_place_cell_simul * my_data;
   int gpe_vigilence=-1;
   int rhoL=1.0;
   type_coeff * coeff;
   float sortie,contrib;
   int nb_contrib;
   if (def_groupe[gpe].data == NULL)
   {

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (strcmp(liaison[l].nom, "vigilence") == 0)
            gpe_vigilence = liaison[l].depart;
         if (prom_getopt(liaison[l].nom, "r", param) > 1)
            rhoL = atof(param);
         i++;
         l = find_input_link(gpe, i);

      }
      if (gpe_vigilence==-1)
      {
         printf("pas de vigilence en entree de function_sim2d_place_cell_simul\n");
         exit(-1);
      }

      my_data =(MyData_f_sim2d_place_cell_simul *)
               malloc(sizeof(MyData_f_sim2d_place_cell_simul));

      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->gpe_vigilence = gpe_vigilence;
      my_data->rhoL=rhoL;
      def_groupe[gpe].data =(MyData_f_sim2d_place_cell_simul *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_place_cell_simul *) (def_groupe[gpe].data);
      gpe_vigilence = my_data->gpe_vigilence;
      rhoL=my_data->rhoL;

   }

   /*Apprend si vigilence*/
   if (neurone[def_groupe[gpe_vigilence].premier_ele].s1>0.5)
   {
      for (i=0; i<def_groupe[gpe].nbre; i++)
      {
         if (neurone[i+def_groupe[gpe].premier_ele].seuil<0.94)
            break;
      }
      /*          printf("APPRENTISSAGE====================: %d\n",i); */

      if (i>=def_groupe[gpe].nbre)
      {
         printf("plus de neurone libre dans f_sim2d_place_cell_simul\n");
         exit(0);
      }
      else
      {
         neurone[i+def_groupe[gpe].premier_ele].seuil=0.95;
         coeff=neurone[i+def_groupe[gpe].premier_ele].coeff;
         while (coeff!=NULL)
         {
            coeff->val=neurone[coeff->entree].s1;
            /*                printf("val = %f\n",coeff->val); */
            coeff=coeff->s;
         }
      }
   }

   for (i=0; i<def_groupe[gpe].nbre; i++)
   {
      /*       printf("Mise a jour de %d, seuil = %f\n",i,neurone[i+def_groupe[gpe].premier_ele].seuil); */
      if (neurone[i+def_groupe[gpe].premier_ele].seuil<0.94)
      {
         /*          printf("\t %d non recrute\n",i); */
         sortie=0.0;
      }
      else
      {
         /*          printf("\t %d est recrute\n",i); */
         sortie=0.0;
         nb_contrib=0;
         coeff=neurone[i+def_groupe[gpe].premier_ele].coeff;
         while (coeff!=NULL)
         {
            /* On regarde si le coeff est a un, ie si le landmark a ete appris*/
            if (coeff->val>0.5 && neurone[coeff->entree].s1>0.5)
            {
               contrib=fabs(coeff->s->val-neurone[coeff->s->entree].s1);
               if (contrib>=M_PI)
                  contrib=contrib-M_PI;
               contrib=1.-contrib/M_PI;
               nb_contrib=nb_contrib+1;
               sortie=sortie+contrib;
            }
            coeff=coeff->s->s;
         }
         if (nb_contrib>3.)
            sortie=sortie/((float)(nb_contrib));
         else
            sortie=0.0;
      }
      neurone[i+def_groupe[gpe].premier_ele].s=neurone[i+def_groupe[gpe].premier_ele].s1=neurone[i+def_groupe[gpe].premier_ele].s2=sortie;
   }
}

void sim2d_otheragent_perception_display(Agent_sim2d * agent, int agent_to_display)
{
#ifndef AVEUGLE
   TxPoint origine;
   TxPoint extremite;
   TxDonneesFenetre *  fenetre= &sim2d_fenetre;/* prom_sim2d->fenetre; */

   origine.x=    agent->x;
   origine.y=    agent->y;
   extremite.x=  prom_sim2d->agents[agent_to_display]->x;
   extremite.y=  prom_sim2d->agents[agent_to_display]->y;
   TxDessinerSegment(fenetre,vert,origine,extremite,1);
#else /* if blind */
   (void)agent;
   (void)agent_to_display;
#endif
}


typedef struct MyData_f_sim2d_otheragents_perception
{
   int num_agent;
   int graphic_display;
} MyData_f_sim2d_otheragents_perception;

/*La fonction sort sequentiellement les amers visibles*/
/**/
void function_sim2d_otheragents_perception(int gpe)
{
   float dx,dy,angle1 = 0.;
   MyData_f_sim2d_otheragents_perception *my_data=NULL;
   int i,j,l;

   int num_agent=-1,agent_invisible;
   Obstacle_sim2d ** the_obstacles=prom_sim2d->obstacles;
   int nb_obstacles=prom_sim2d->nb_obstacles;
   int nb_agents=prom_sim2d->nb_agents;
   float the_agent_x, the_agent_y, the_agent_theta, angle2;
   char param[255];

   float Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,r,s,den,num_r,num_s;
   float CALBG_x,CALBG_y,CALHD_x,CALHD_y,COBG_x,COBG_y,COHD_x,COHD_y;
   int graphic_display=0;

   if (def_groupe[gpe].data == NULL)
   {

      if (def_groupe[gpe].taillex != 4)
      {
         printf("il faut 4 neurones en x a function_sim2d_otheragents_perception\n");
         exit(-1);
      }
      if (def_groupe[gpe].tailley < prom_sim2d->nb_agents)
      {
         printf("il faut nb_agents neurones en y function_sim2d_otheragents_perception\n");
         exit(-1);
      }

      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {

         if (prom_getopt(liaison[l].nom, "a", param) > 1)	num_agent = atoi(param);
         if (prom_getopt(liaison[l].nom, "d", param) > 1)	graphic_display = atoi(param);

         i++;
         l = find_input_link(gpe, i);

      }
      if (num_agent>=prom_sim2d->nb_agents||num_agent<0)
      {
         printf("pas d agent ayant se num dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }

      my_data = (MyData_f_sim2d_otheragents_perception *) malloc(sizeof(MyData_f_sim2d_otheragents_perception));
      if (my_data==NULL)
      {
         printf("error malloc dans function_sim2d_sequential_landmarks_perception\n");
         exit(-1);
      }
      my_data->num_agent = num_agent;
      my_data->graphic_display=graphic_display;
      def_groupe[gpe].data = (MyData_f_sim2d_otheragents_perception *) my_data;
   }
   else
   {
      my_data = (MyData_f_sim2d_otheragents_perception *) (def_groupe[gpe].data);
      num_agent = my_data->num_agent;
      graphic_display=my_data->graphic_display;
   }

   the_agent_x=prom_sim2d->agents[num_agent]->x;
   the_agent_y=prom_sim2d->agents[num_agent]->y;
   the_agent_theta=prom_sim2d->agents[num_agent]->theta;
   /*reset des neurones*/
   for (i=0; i<def_groupe[gpe].nbre; i++)
   {
      neurone[def_groupe[gpe].premier_ele+i].s=neurone[def_groupe[gpe].premier_ele+i].s1= neurone[def_groupe[gpe].premier_ele+i].s2= 0. ;
   }


   for (i=0; i<nb_agents; i++)
   {

      /*on suppose que le land est visible. Si un obstacle gene, on dira que le land est invisible*/
      /*       printf("test land %d",i); */
      agent_invisible=0;
      /*on verifie que le robot ne se trouve pas sur le landmarks*/
      if (fabs(the_agent_x - prom_sim2d->agents[i]->x) > 1. || fabs(the_agent_y - prom_sim2d->agents[i]->y) > 1. || i != num_agent)
      {

         /*calcul des intersection entre la droite robot-land et les droites obstacle*/
         for (j=0; j<nb_obstacles && agent_invisible==0; j++)
         {
            /*obstacle  grand*/
            if (the_obstacles[j]->high==1)
            {
               /*Intersection AB et CD ?*/
               Ax=the_obstacles[j]->xa;
               Ay=the_obstacles[j]->ya;
               Bx=the_obstacles[j]->xb;
               By=the_obstacles[j]->yb;

               Cx=prom_sim2d->agents[i]->x;
               Cy=prom_sim2d->agents[i]->y;
               Dx=the_agent_x;
               Dy=the_agent_y;

               den=(Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
               num_r=(Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
               num_s=(Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);
               if (isdiff(den,0.)) /*sinon droite parallele*/
               {
                  r=num_r/den;
                  s=num_s/den;
                  /*Segment croises*/
                  /*                       printf("\t r=%f s=%f\n",r,s); */
                  if (r>=0.&&r<=1.&&s>=0.&&s<=1.)
                  {
                     agent_invisible=1;
                  }
               }
               else
               {
                  /*Segmant parallele*/
                  /*cohincidence*/
                  if (isequal(num_r,0.))
                  {
                     /*                         printf("lE CAS de deux segment cohincident obs %d\n",j);*/
                     /*Si O1 ou O2 dans rect A L*/
                     if (Cx>=Dx)
                     {CALBG_x=Dx; CALHD_x=Cx;}
                     else
                     {CALBG_x=Cx; CALHD_x=Dx;}

                     if (Cy>=Dy)
                     {CALBG_y=Dy; CALHD_y=Cy;}
                     else
                     {CALBG_y=Cy; CALHD_y=Dy;}

                     if ( ( Ax>=CALBG_x && Ax<=CALHD_x ) && (Ay>=CALBG_y && Ay<=CALHD_y) )
                     {
                        /*                            printf("O1 dans rect AL\n");*/
                        agent_invisible=1;
                     }
                     if ( ( Bx>=CALBG_x && Bx<=CALHD_x ) && (By>=CALBG_y && By<=CALHD_y) )
                     {
                        /*                            printf("O2 dans rect AL\n");*/
                        agent_invisible=1;
                     }
                     /*si A ou L dans rect O1 O2*/
                     if (Ax>=Bx)
                     {COBG_x=Bx; COHD_x=Ax;}
                     else
                     {COBG_x=Ax; COHD_x=Bx;}

                     if (Ay>=By)
                     {COBG_y=By; COHD_y=Ay;}
                     else
                     {COBG_y=Ay; COHD_y=By;}

                     if ( ( Cx>=COBG_x && Cx<=COHD_x ) && (Cy>=COBG_y && Cy<=COHD_y) )
                     {
                        /*                           printf("L dans rect OO\n"); */
                        agent_invisible=1;
                     }
                     if ( ( Dx>=COBG_x && Dx<=COHD_x ) && (Dy>=COBG_y && Dy<=COHD_y) )
                     {
                        /*                            printf("A dans rect OO\n"); */
                        agent_invisible=1;
                     }

                  }
               }

            }
            /*On ne test pas les obstale suivant si on a une invisibilite*/
            if (agent_invisible==1)   break;
         }
      }
      else
      {
         agent_invisible=1;
      }

      /*ici, on sait si le land i est visible*/

      if (agent_invisible==0)
      {
         /*          printf(" .... visible \n");   */

         dx=prom_sim2d->agents[i]->x -the_agent_x ;
         dy=prom_sim2d->agents[i]->y -the_agent_y ;

         angle1 = atan2(dy,dx);

         if (angle1 <= 0.)	angle1 = angle1 + 2. * M_PI;

         angle2 = angle1 - the_agent_theta;

         if (angle2 <= 0.)	angle2 = angle2 + 2. * M_PI;

         /*Visibility*/
         neurone[def_groupe[gpe].premier_ele+4*i].s=neurone[def_groupe[gpe].premier_ele+4*i].s1= neurone[def_groupe[gpe].premier_ele+4*i].s2= 1.;

         /*angle abs*/
         neurone[def_groupe[gpe].premier_ele+4*i+1].s=neurone[def_groupe[gpe].premier_ele+4*i+1].s1= neurone[def_groupe[gpe].premier_ele+4*i+1].s2= angle1 ;

         /*angle rel*/
         neurone[def_groupe[gpe].premier_ele+4*i+2].s=neurone[def_groupe[gpe].premier_ele+4*i+2].s1= neurone[def_groupe[gpe].premier_ele+4*i+2].s2= angle2 ;

         /*distance*/
         neurone[def_groupe[gpe].premier_ele+4*i+3].s=neurone[def_groupe[gpe].premier_ele+4*i+3].s1= neurone[def_groupe[gpe].premier_ele+4*i+3].s2= sqrt(dx*dx+dy*dy)*prom_sim2d->cste_spatial;

         if (graphic_display>0)
            sim2d_otheragent_perception_display(prom_sim2d->agents[num_agent],i);

      }
      else
      {
         /*         printf(" invisible \n"); */
      }
   }

   /*    printf("%s: %f %f %f %f\n",); */

   dprints("fin f_sequential_perception_break\n");
}

#endif








/*
  void new_function_sim2d_init_0(int gpe)
  {
  int i=0,j;
  int nb_landmarks=10,nb_agents=2,nb_obstacles=6;

  printf("new_function_sim2d_init\n");

  prom_sim2d=(Sim2d *)malloc(sizeof(Sim2d));
  if(prom_sim2d==NULL)
  {
  printf("error malloc dans sim2d_init\n");
  exit(-1);
  }

  prom_sim2d->cste_temps=0.05;
  prom_sim2d->cste_spatial=0.2;
  prom_sim2d->width=600;
  prom_sim2d->height=600;
  * Creation de la fenetre*
  #ifndef AVEUGLE
  sprintf(sim2d_fenetre.name, "Env Sim2d");
  create_fenetre_sim2d(&sim2d_fenetre);
  gtk_widget_show_all(sim2d_fenetre.window);
  prom_sim2d->fenetre=(TxDonneesFenetre*)malloc(sizeof(TxDonneesFenetre));
  sprintf(prom_sim2d->fenetre->name, "Environment Sim2d");
  create_fenetre_sim2d(prom_sim2d->fenetre);
  gtk_widget_show_all(prom_sim2d->fenetre->window);
  #endif

  prom_sim2d->landmarks=(Landmark_sim2d **)malloc(nb_landmarks * sizeof(Landmark_sim2d*));
  for(i=0;i<nb_landmarks;i++)
  {
  prom_sim2d->landmarks[i]=(Landmark_sim2d *)malloc(sizeof(Landmark_sim2d));
  }
  prom_sim2d->nb_landmarks=nb_landmarks;

  prom_sim2d->agents=(Agent_sim2d **)malloc(nb_agents*sizeof(Agent_sim2d*));
  for(i=0;i<nb_agents;i++)
  {
  prom_sim2d->agents[i]=(Agent_sim2d *)malloc(sizeof(Agent_sim2d));
  for(j=0;j<SIZE_TRACE;j++)
  {
  prom_sim2d->agents[i]->trace_x[j]=-100.;
  prom_sim2d->agents[i]->trace_y[j]=-100.;
  prom_sim2d->agents[i]->trace_theta[j]=-100.;
  }
  }
  prom_sim2d->nb_agents=nb_agents;

  prom_sim2d->obstacles=(Obstacle_sim2d  **)malloc(nb_obstacles*sizeof(Obstacle_sim2d*));
  for(i=0;i<nb_obstacles;i++)
  {
  prom_sim2d->obstacles[i]=(Obstacle_sim2d *)malloc(sizeof(Obstacle_sim2d));
  }
  prom_sim2d->nb_obstacles=nb_obstacles;

  prom_sim2d->obstacles[0]->xa=0;
  prom_sim2d->obstacles[0]->ya=0;
  prom_sim2d->obstacles[0]->xb=0;
  prom_sim2d->obstacles[0]->yb=500;
  prom_sim2d->obstacles[0]->high=1;

  prom_sim2d->obstacles[1]->xa=0;
  prom_sim2d->obstacles[1]->ya=0;
  prom_sim2d->obstacles[1]->xb=500;
  prom_sim2d->obstacles[1]->yb=0;
  prom_sim2d->obstacles[1]->high=1;

  prom_sim2d->obstacles[2]->xa=0;
  prom_sim2d->obstacles[2]->ya=500;
  prom_sim2d->obstacles[2]->xb=500;
  prom_sim2d->obstacles[2]->yb=500;
  prom_sim2d->obstacles[2]->high=1;

  prom_sim2d->obstacles[3]->xa=500;
  prom_sim2d->obstacles[3]->ya=0;
  prom_sim2d->obstacles[3]->xb=500;
  prom_sim2d->obstacles[3]->yb=500;
  prom_sim2d->obstacles[3]->high=1;

  prom_sim2d->obstacles[4]->xa=250;
  prom_sim2d->obstacles[4]->ya=125;
  prom_sim2d->obstacles[4]->xb=250;
  prom_sim2d->obstacles[4]->yb=375;
  prom_sim2d->obstacles[4]->high=0;

  prom_sim2d->obstacles[5]->xa=100;
  prom_sim2d->obstacles[5]->ya=250;
  prom_sim2d->obstacles[5]->xb=375;
  prom_sim2d->obstacles[5]->yb=250;
  prom_sim2d->obstacles[5]->high=1;
  *
  prom_sim2d->obstacles[6]->xa=120;
  prom_sim2d->obstacles[6]->ya=120;
  prom_sim2d->obstacles[6]->xb=130;
  prom_sim2d->obstacles[6]->yb=130;
  prom_sim2d->obstacles[6]->high=0;

  prom_sim2d->obstacles[7]->xa=140;
  prom_sim2d->obstacles[7]->ya=140;
  prom_sim2d->obstacles[7]->xb=150;
  prom_sim2d->obstacles[7]->yb=150;
  prom_sim2d->obstacles[7]->high=0;

  prom_sim2d->obstacles[8]->xa=160;
  prom_sim2d->obstacles[8]->ya=160;
  prom_sim2d->obstacles[8]->xb=170;
  prom_sim2d->obstacles[8]->yb=170;
  prom_sim2d->obstacles[8]->high=0;

  prom_sim2d->obstacles[9]->xa=180;
  prom_sim2d->obstacles[9]->ya=180;
  prom_sim2d->obstacles[9]->xb=200;
  prom_sim2d->obstacles[9]->yb=200;
  prom_sim2d->obstacles[9]->high=0;*

  prom_sim2d->agents[0]->x=150.;
  prom_sim2d->agents[0]->y=20.;
  prom_sim2d->agents[0]->theta=0.;
  prom_sim2d->agents[0]->masse=1.0;
  prom_sim2d->agents[0]->speed_max=3.;
  prom_sim2d->agents[0]->FC_max=10.0;
  prom_sim2d->agents[0]->radius_proximity=25.;

  prom_sim2d->agents[1]->x=50.;
  prom_sim2d->agents[1]->y=300.;
  prom_sim2d->agents[1]->theta=0.2;
  prom_sim2d->agents[1]->masse=20.0;
  prom_sim2d->agents[1]->speed_max=1.;
  prom_sim2d->agents[1]->FC_max=10.0;
  prom_sim2d->agents[1]->radius_proximity=25.;


  prom_sim2d->agents[2]->x=100.;
  prom_sim2d->agents[2]->y=90.;
  prom_sim2d->agents[2]->theta=0.4;
  prom_sim2d->agents[2]->speed_max=1.0;
  prom_sim2d->agents[2]->radius_proximity=50.;

  prom_sim2d->agents[3]->x=100.;
  prom_sim2d->agents[3]->y=130.;
  prom_sim2d->agents[3]->theta=0.6;
  prom_sim2d->agents[3]->speed_max=1.0;
  prom_sim2d->agents[3]->radius_proximity=50.;

  prom_sim2d->agents[4]->x=100.;
  prom_sim2d->agents[4]->y=170.;
  prom_sim2d->agents[4]->theta=0.8;
  prom_sim2d->agents[4]->speed_max=1.0;
  prom_sim2d->agents[4]->radius_proximity=50.;

  prom_sim2d->agents[5]->x=100.;
  prom_sim2d->agents[5]->y=210.;
  prom_sim2d->agents[5]->theta=0.99;
  prom_sim2d->agents[5]->speed_max=1.0;
  prom_sim2d->agents[5]->radius_proximity=50.;

  prom_sim2d->agents[6]->x=100.;
  prom_sim2d->agents[6]->y=250.;
  prom_sim2d->agents[6]->theta=1.2;
  prom_sim2d->agents[6]->speed_max=1.0;
  prom_sim2d->agents[6]->radius_proximity=50.;

  prom_sim2d->agents[7]->x=100.;
  prom_sim2d->agents[7]->y=290.;
  prom_sim2d->agents[7]->theta=1.4;
  prom_sim2d->agents[7]->speed_max=1.0;
  prom_sim2d->agents[7]->radius_proximity=50.;

  prom_sim2d->agents[8]->x=100.;
  prom_sim2d->agents[8]->y=330.;
  prom_sim2d->agents[8]->theta=1.6;
  prom_sim2d->agents[8]->speed_max=1.0;
  prom_sim2d->agents[8]->radius_proximity=50.;

  prom_sim2d->agents[9]->x=100.;
  prom_sim2d->agents[9]->y=370.;
  prom_sim2d->agents[9]->theta=1.8;
  prom_sim2d->agents[9]->speed_max=1.0;
  prom_sim2d->agents[9]->radius_proximity=50.;

  for(i=0;i<nb_agents;i++)
  {
  for(j=0;j<SIZE_TRACE;j++)
  {
  prom_sim2d->agents[i]->trace_x[j]=prom_sim2d->agents[i]->x;
  prom_sim2d->agents[i]->trace_y[j]=prom_sim2d->agents[i]->y;
  prom_sim2d->agents[i]->trace_theta[j]=prom_sim2d->agents[i]->theta;
  }
  }

  prom_sim2d->landmarks[0]->x=5;
  prom_sim2d->landmarks[0]->y=5;
  prom_sim2d->landmarks[0]->always_visible=0;

  prom_sim2d->landmarks[1]->x=250;
  prom_sim2d->landmarks[1]->y=5;
  prom_sim2d->landmarks[1]->always_visible=0;

  prom_sim2d->landmarks[2]->x=495;
  prom_sim2d->landmarks[2]->y=5;
  prom_sim2d->landmarks[2]->always_visible=0;

  prom_sim2d->landmarks[3]->x=495;
  prom_sim2d->landmarks[3]->y=250;
  prom_sim2d->landmarks[3]->always_visible=0;

  prom_sim2d->landmarks[4]->x=495;
  prom_sim2d->landmarks[4]->y=495;
  prom_sim2d->landmarks[4]->always_visible=0;

  prom_sim2d->landmarks[5]->x=250;
  prom_sim2d->landmarks[5]->y=495;
  prom_sim2d->landmarks[5]->always_visible=0;

  prom_sim2d->landmarks[6]->x=5;
  prom_sim2d->landmarks[6]->y=495;
  prom_sim2d->landmarks[6]->always_visible=0;

  prom_sim2d->landmarks[7]->x=5;
  prom_sim2d->landmarks[7]->y=250;
  prom_sim2d->landmarks[7]->always_visible=1;

  prom_sim2d->landmarks[8]->x=10;
  prom_sim2d->landmarks[8]->y=160;
  prom_sim2d->landmarks[8]->always_visible=0;

  prom_sim2d->landmarks[9]->x=10;
  prom_sim2d->landmarks[9]->y=180;
  prom_sim2d->landmarks[9]->always_visible=0;

  }*/
