/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_AX12_test.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <libhardware.h>
#include <math.h>
#include <stdlib.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

typedef struct My_Data_f_AX12_test
{
    int id_servo;
    int gpe_command;
} My_Data_f_AX12_test;

void function_AX12_test(int gpe)
{
    int id_servo=6,i,l;
    int gpe_command=-1;
    char param[255],commande[255],retour[255];
    My_Data_f_AX12_test * my_data = NULL;
    int pos_ax12;	
    
    float position;
    char highbyte_pos,lowbyte_pos,id_servo_byte,addr;
    
   
    if (def_groupe[gpe].data == NULL)
    {

        i = 0;
        while ( (l = find_input_link(gpe, i)) != -1)
        {
            
            if (prom_getopt(liaison[l].nom, "-i", param) > 1)
                id_servo = atoi(param);
            if (strcmp(liaison[l].nom,"command")==0)
                gpe_command = liaison[l].depart;
               
            i++;
        }

	if(gpe_command==-1)
	{
	  printf("manque gpe_command en entree de f_AX12_test(%s)\n",def_groupe[gpe].no_name);
	  exit(-1);
	}

        my_data = (My_Data_f_AX12_test *)malloc(sizeof(My_Data_f_AX12_test));
        my_data->id_servo = id_servo;
	my_data->gpe_command = gpe_command;
	  def_groupe[gpe].data = (My_Data_f_AX12_test *) my_data;

        /* Ajout Vincent */
        /* Mise a zero des compteurs des roues dans le cas du premier appel a la fonction */
        /*robot_init_wheels_counter(robot) ; */
    }
    else
    {
        my_data = (My_Data_f_AX12_test *) def_groupe[gpe].data;
        id_servo = my_data->id_servo;
	gpe_command = my_data->gpe_command;
    }

   position=neurone[def_groupe[gpe_command].premier_ele].s1;
   
   printf("enter ax12\n");

   if (position>1.||position<0.)
   {
	printf("outrange de la command dans f_AX12_test(%s)\n",def_groupe[gpe].nom);
   }
   else
   {
      pos_ax12=(int)(1023*position);

      printf("goto %d\n",pos_ax12);

      lowbyte_pos=pos_ax12 & 0xff;
      highbyte_pos=(pos_ax12& 0xff00)>>8;
      id_servo_byte=6;
      addr='D';
  
      printf("a %d - i %d - h %d - l %d\n",addr,id_servo,highbyte_pos,lowbyte_pos);

      sprintf(commande,"%c%c%c%c\n",addr,id_servo_byte,highbyte_pos,lowbyte_pos);
      
      serial_send_and_receive_nchar((char*)"_ttyUSB0",commande,retour,4,5);
      printf("retour:%c%c%c%c%c\n",retour[0],retour[1],retour[2],retour[3],retour[4]);
  }


}
