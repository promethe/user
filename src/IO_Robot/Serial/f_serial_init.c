/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_serial_init.c 
\brief defines Serial communication's parameters

Author: B. Mariat
Created: 23/03/2005
Modified:
- 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
* defines Serial communication's parameters
* link parameters:
	- n: port's name
	- d: speed communication
	- b: bit's length
	- p: bit's parity
	- s: number of stop bit
 
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-

Links:
- type: algo
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <libhardware.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

void function_serial_init(int gpe_sortie)
{

    int lien_entrant = -1;
    char param_link[256], name[128], speed[128], bit[128], parite[128],
        stop[128];

    /* initialisation */
    strcpy(name, "\0");         /*name doit etre absolument defini */
    strcpy(speed, "9600");
    strcpy(bit, "8");
    strcpy(parite, "N");
    strcpy(stop, "2");

    /*Refresh(); */
    /*NC Simulation de param user passer normalement sur le lien */

    if (def_groupe[gpe_sortie].ext == NULL)
    {
        lien_entrant = find_input_link(gpe_sortie, 0);

        if (prom_getopt(liaison[lien_entrant].nom, "-n", param_link) == 2)
        {
            strcpy(name, param_link);
        }
        else if (prom_getopt(liaison[lien_entrant].nom, "-N", param_link) ==
                 2)
        {
            strcpy(name, param_link);
        }
        if (prom_getopt(liaison[lien_entrant].nom, "-d", param_link) == 2)
        {
            strcpy(speed, param_link);
        }
        else if (prom_getopt(liaison[lien_entrant].nom, "-D", param_link) ==
                 2)
        {
            strcpy(speed, param_link);
        }

        if (prom_getopt(liaison[lien_entrant].nom, "-b", param_link) == 2)
        {
            strcpy(bit, param_link);
        }
        else if (prom_getopt(liaison[lien_entrant].nom, "-B", param_link) ==
                 2)
        {
            strcpy(bit, param_link);
        }

        if (prom_getopt(liaison[lien_entrant].nom, "-p", param_link) == 2)
        {
            strcpy(parite, param_link);
        }
        else if (prom_getopt(liaison[lien_entrant].nom, "-P", param_link) ==
                 2)
        {
            strcpy(parite, param_link);
        }

        if (prom_getopt(liaison[lien_entrant].nom, "-s", param_link) == 2)
        {
            strcpy(stop, param_link);
        }
        else if (prom_getopt(liaison[lien_entrant].nom, "-S", param_link) ==
                 2)
        {
            strcpy(stop, param_link);
        }
    }

    if (strlen(name) == 0)
    {
        printf
            ("il est necessaire de definir le nom d'un port serie pour faire fonctionner f_serial_init\n");
        return;
    }

    if (serial_setparam(name, speed, bit, parite, stop) == -1)
        printf("f_serial_init: port %s inconnu!\n", name);

    return;
}
