/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

/** ***********************************************************
 \file  f_serial_init.c
 \brief defines Serial communication's parameters

 Author: B. Mariat
 Created: 23/03/2005
 Modified:
 -

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 * defines Serial communication's parameters
 * link parameters:
 - n: port's name
 - d: speed communication
 - b: bit's length
 - p: bit's parity
 - s: number of stop bit
 
 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -

 Links:
 - type: algo
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************ ************/

#include <math.h>
#include <libx.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libhardware.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

int getSoftware()
{
  int lenght;
  char retour[255], commande[255];
  sprintf(commande, "%c%c%c%c", 0x55, 0xb1, 0x0d, 0x01);
  lenght = 4;
  serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, lenght, 1);
  return retour[0];
}

unsigned long int read_enc()
{
  int lenght = 4;
  unsigned char enc6, enc7, enc9, enc8;
  char retour[255], commande[255];

  sprintf(commande, "%c%c%c%c", 0x55, 0xb1, 0x09, 0x01);
  serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, lenght, 1);
  enc9 = retour[0];

  sprintf(commande, "%c%c%c%c", 0x55, 0xb1, 0x08, 0x01);
  serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, lenght, 1);
  enc8 = retour[0];
// printf("valeur=%d\n",retour[0]);

  sprintf(commande, "%c%c%c%c", 0x55, 0xb1, 0x07, 0x01);
  serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, lenght, 1);
  enc7 = retour[0];
// printf("valeur=%d\n",retour[0]);

  sprintf(commande, "%c%c%c%c", 0x55, 0xb1, 0x06, 0x01);
  serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, lenght, 1);
  enc6 = retour[0];
// printf("valeur=%d\n",retour[0]);

  return enc9 + enc8 * 256 + enc7 * 256 * 256 + enc6 * 256 * 256 * 256;
}

void function_MD23_test(int gpe_sortie)
{
// int pc=63752; // Position du moteur � l'origine.
  float K = 0.2, vitesse;
// unsigned char vitesse;
  unsigned long int a, pc = 7000;
  long int err;
  char commande[255], retour[255], Vit;
// a=read_enc();
  while (1)
  {

    a = read_enc();
    printf("\ta=%ld\n", a);
    err = pc - a;
    printf("err=%ld\n", err);

//   if(abs(err)<100)
//     sprintf(commande,"%c%c%c%c%c",0x55,0xb0,0x01,0x01,vitesse);
//   else if (err>0)
//     sprintf(commande,"%c%c%c%c%c",0x55,0xb0,0x01,0x01,0x8F);
//   else
//     sprintf(commande,"%c%c%c%c%c",0x55,0xb0,0x01,0x01,0x71);
    if (err < 1 / K && err > -1 / K)
    {
      if (err < 0) vitesse = 127;
      else if (err > 0) vitesse = 129;
      else if (err == 0) vitesse = 128;
    }
    else
    {
      vitesse = 128 + K * err;

      if (vitesse > 255) vitesse = 255;
      else if (vitesse < 0) vitesse = 0;
    }

    Vit = (char) ((int) vitesse);

    sprintf(commande, "%c%c%c%c%c", 0x55, 0xb0, 0x01, 0x01, Vit);
    serial_send_and_receive_nchar((char*) "_ttyUSB0", commande, retour, 5, 1);

  }
  (void) gpe_sortie;
}
