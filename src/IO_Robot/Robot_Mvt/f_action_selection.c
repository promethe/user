/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/
static int Gpe_1 = -1;
static int Gpe_2 = -1;
static int Gpe_X = -1;

static int mode;


void function_action_selection(int numero)
{
    int i = 0;
    int l = -1;
    float seuil = 0.02;
    int temp = -1;

    printf("Action selection %d\n", numero);

    if (def_groupe[numero].data == NULL)
    {
        mode = 0;
        if (l == -1)
        {

            l = find_input_link(numero, i);

            while (l != -1)
            {
                if (strcmp(liaison[l].nom, "-1") == 0)
                {
                    Gpe_1 = liaison[l].depart;

                }

                if (strcmp(liaison[l].nom, "-2") == 0)
                {
                    Gpe_2 = liaison[l].depart;

                }


                if (strcmp(liaison[l].nom, "S") == 0)
                {
                    Gpe_X = liaison[l].depart;

                }
                if (strcmp(liaison[l].nom, "SM") == 0)
                {
                    Gpe_X = liaison[l].depart;
                    mode = 1;
                    printf("mode continu\n");
                }

                i++;
                l = find_input_link(numero, i);

            }
        }

        if (Gpe_1 == -1)
        {
            printf("groupe  1 non trouve dans groupe %d\n", numero);
            exit(0);
        }

        if (Gpe_2 == -1)
        {
            printf("groupe  2 non trouve dans groupe %d\n", numero);
            printf
                ("Actin selection en focntoin du reward du lieu et non de la transition...\n");
        }

        if (Gpe_X == -1)
        {
            printf("groupe  position non trouve dans groupe %d\n", numero);
            exit(0);
        }


        def_groupe[numero].data = malloc(3 * sizeof(int));

        /*memcpy(def_groupe[numero].data,groupes,3*sizeof(int)); */
        neurone[def_groupe[numero].premier_ele].s1 = 1.0;
        neurone[def_groupe[numero].premier_ele].s =
            neurone[def_groupe[numero].premier_ele].s2 =
            neurone[def_groupe[numero].premier_ele].s1;
        neurone[def_groupe[numero].premier_ele + 1].s1 = 1.0;

        neurone[def_groupe[numero].premier_ele + 1].s1 = 0.;
        return;
    }
    else
    {
        if (mode == 0)
        {

            for (i = 0; i < def_groupe[Gpe_X].nbre; i++)
            {


                if (neurone[def_groupe[Gpe_X].premier_ele + i].s1 > 0.5)
                {

                    break;
                }


            }

            if (Gpe_2 == -1)
            {

                if (i == 0)
                {

                    neurone[def_groupe[numero].premier_ele].s1 = 1.;
                    neurone[def_groupe[numero].premier_ele + 1].s1 = 0.;

                }
                else if (i == def_groupe[Gpe_X].nbre - 1)
                {

                    neurone[def_groupe[numero].premier_ele].s1 = 0.;
                    neurone[def_groupe[numero].premier_ele + 1].s1 = 1.;


                }
                else
                {
                    if (neurone[def_groupe[Gpe_1].premier_ele + i + 1].s1 >
                        seuil
                        || neurone[def_groupe[Gpe_1].premier_ele + i - 1].s1 >
                        seuil)
                    {
                        neurone[def_groupe[numero].premier_ele].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + i +
                                    1].s1 >
                            neurone[def_groupe[Gpe_1].premier_ele + i -
                                    1].s1 ? 1. : 0.;
                        neurone[def_groupe[numero].premier_ele + 1].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + i +
                                    1].s1 >
                            neurone[def_groupe[Gpe_1].premier_ele + i -
                                    1].s1 ? 0. : 1.;
                    }
                }
            }
            else
            {
                if (i == 0)
                {

                    if (neurone[def_groupe[Gpe_1].premier_ele + 1].s1 > seuil
                        || neurone[def_groupe[Gpe_2].premier_ele +
                                   def_groupe[Gpe_X].nbre - 1].s1 > seuil)
                    {
                        neurone[def_groupe[numero].premier_ele].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + 1].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele +
                                    def_groupe[Gpe_X].nbre - 1].s1 ? 1. : 0.;
                        neurone[def_groupe[numero].premier_ele + 1].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + 1].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele +
                                    def_groupe[Gpe_X].nbre - 1].s1 ? 0. : 1.;
                    }

                }
                else if (i == def_groupe[Gpe_X].nbre - 1)
                {

                    if (neurone[def_groupe[Gpe_1].premier_ele].s1 > seuil
                        || neurone[def_groupe[Gpe_2].premier_ele + i - 1].s1 >
                        seuil)
                    {
                        neurone[def_groupe[numero].premier_ele].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele + i -
                                    1].s1 ? 1. : 0.;
                        neurone[def_groupe[numero].premier_ele + 1].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele + i -
                                    1].s1 ? 0. : 1.;
                    }


                }
                else
                {
                    if (neurone[def_groupe[Gpe_1].premier_ele + i + 1].s1 >
                        seuil
                        || neurone[def_groupe[Gpe_2].premier_ele + i - 1].s1 >
                        seuil)
                    {
                        neurone[def_groupe[numero].premier_ele].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + i +
                                    1].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele + i -
                                    1].s1 ? 1. : 0.;
                        neurone[def_groupe[numero].premier_ele + 1].s1 =
                            neurone[def_groupe[Gpe_1].premier_ele + i +
                                    1].s1 >
                            neurone[def_groupe[Gpe_2].premier_ele + i -
                                    1].s1 ? 0. : 1.;
                    }
                }


            }
        }
        else
        {
            temp = neurone[def_groupe[Gpe_1].premier_ele].s1;
            for (i = 1; i < def_groupe[Gpe_X].nbre; i++)
            {

                temp =
                    (temp >
                     neurone[def_groupe[Gpe_1].premier_ele +
                             i].s1 ? temp : neurone[def_groupe[Gpe_1].
                                                    premier_ele + i].s1);

            }





        }
        neurone[def_groupe[numero].premier_ele].s =
            neurone[def_groupe[numero].premier_ele].s2 =
            neurone[def_groupe[numero].premier_ele].s1;
        neurone[def_groupe[numero].premier_ele + 1].s =
            neurone[def_groupe[numero].premier_ele + 1].s2 =
            neurone[def_groupe[numero].premier_ele + 1].s1;
    }

    if (neurone[def_groupe[numero].premier_ele].s1 > 0.5)
        printf("Action droite.....action 1 =%f, action2=%f \n",
               neurone[def_groupe[numero].premier_ele].s1,
               neurone[def_groupe[numero].premier_ele + 1].s1);
    else
        printf("Action gauche...action 1 =%f, action2=%f \n",
               neurone[def_groupe[numero].premier_ele].s1,
               neurone[def_groupe[numero].premier_ele + 1].s1);

}
