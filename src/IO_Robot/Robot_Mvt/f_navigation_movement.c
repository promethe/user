/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief Fonction generant le mouvement

\details

\section Description
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]

 Le neurone correspondant a la direction du mouvement reelement effectue prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)

 Pour le moment on prend directement le maximum, dans la suite la sortie de cette boite pourrait lire une boite du genre f_d_phi mais necessite des modifs profonde: utiliser une autre fonction que move_via_mvector_given_speed
qui devra utiliser des commande D,X,X X teant obtenu a partir de d_phi*cste ...

Macro:
- PI
- PI_2

Local variables:
- sortie_odometrie parametres_odometrie


Global variables:
- boolean EMISSION_ROBOT
- int USE_SIMULATION
- int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: Deep modification from the code of V.B
- date: 30/11/2004

************************************************************/
/*#define DEBUG*/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>

#define DEFAULT_NORME 6000
#define SPEED 5
#define MAX_ROT_SPEED 80
#define MAX_D_PHI 0.07
#define SPEED_COEFF 0.8

void function_navigation_movement(int numero)
{
    float delta_angle = 0., d_phi = 0.; /*variable pour la lecture du NF */
    int p[2];

    int gpe_entree1, gpe_entree2 = -1, gpe_entree3, gpe_entree4;
    int i, n;
    int deb, size, increment;
    int deb_e, taille_groupe_e, taille_groupe_e2, deb_e3;
    int xmax2, ymax2, num_neur;
    int pos = -1;               /*,decalage,correspondant; */
    double beta_tot = 0, teta = 0 /* NC: [-Pi;+Pi] */ , norme =
        0., norm_wanted = 0;
    float mvt_r /*NC: en degre [-180;180[ */ ,
        mvt_d /*NC en radians [-PI;PI[ */ , orientation_r /*NC: [-PI; PI[ */ ,
        orientation_d /*NC: [-180; 180[ */ , max, scale;
    float speed_cste = 4., rot_speed = 50.;
    static float speed_0;

    if (def_groupe[numero].data == NULL)
    {
        def_groupe[numero].data = (void *) (&parametres_odometrie);
        if (EMISSION_ROBOT == SET)
            robot_init_wheels_counter(robot_get_first_robot());
        gpe_entree4 = trouver_entree(numero, (char*)"speed_cst");
        if (gpe_entree4 != -1)
        {
            for (n = 0; n < 4; n++)
            {
                i = find_input_link(numero, n);
                if (strcmp(liaison[i].nom, "speed_cst") == 0)
                    break;
            }
            speed_0 = neurone[def_groupe[gpe_entree4].premier_ele].s2 * liaison[i].norme;
            printf("=====================vitesse_nulle: %f===================\n", speed_0);
        }
    }
    printf("-------------------------------------------------\n");
    printf("Creating the movement \n");
    /*Recherche des boites en entrees... */
    gpe_entree3 = trouver_entree(numero,(char*) "DP");
    gpe_entree1 = trouver_entree(numero, (char*)"NF");
    gpe_entree2 = trouver_entree(numero,(char*) "compass");
    gpe_entree4 = trouver_entree(numero, (char*)"speed_cst");
    if (gpe_entree4 != -1)
    {
        for (n = 0; n < 4; n++)
        {
            i = find_input_link(numero, n);
            if (strcmp(liaison[i].nom, "speed_cst") == 0)
                break;
        }
        speed_cste =
            neurone[def_groupe[gpe_entree4].premier_ele].s2 *
            liaison[i].norme;
        if (isdiff(speed_cste, 0.))
            printf("=====================vitesse brute: %f===================\n", speed_cste);
        if ((speed_cste <= speed_0 + 2) && (speed_cste >= speed_0 - 2))
            speed_cste = 0;
        else if (speed_cste < speed_0 - 2)
            speed_cste = -(speed_0 - speed_cste);
        else
            speed_cste = speed_cste - speed_0;
        if (isdiff(speed_cste, 0.))
        {
            /* rot_speed=SPEED_COEFF*fabs(speed_cste); */
            printf("=====================vitesse rot: %f===================\n", rot_speed);

/*if(rot_speed*MAX_D_PHI+speed_cste>MAX_ROT_SPEED)rot_speed=(MAX_ROT_SPEED-speed_cste)*(1/MAX_D_PHI);*/
            rot_speed = (1 / MAX_D_PHI) * 20.5 * (1 + ((fabs(12.5 - fabs(speed_cste / 2))) / 16.5));
            printf("=====================vitesse rot ap seuillage: %f===================\n", rot_speed);
        }
    }
    if (isdiff(speed_cste, 0))
        printf("=====================vitesse: %f===================\n", speed_cste);

    if (gpe_entree2 == 1)
        printf("WARNING: f_nav_mvt: no f_orientation  find !\n");
#ifdef DEBUG
    printf("f_nav_mvt: NF:  %d; f_orientation: %d, DP %d!\n", gpe_entree1, gpe_entree2, gpe_entree3);
    printf("f_nav_mvt: f_orientation  find at group: %d\n", gpe_entree2);
#endif
    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    size = xmax2 * ymax2;

    taille_groupe_e = def_groupe[gpe_entree1].nbre;
    taille_groupe_e2 =
        def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley;
    scale = 2 * PI / (float) taille_groupe_e2;  /*echelle, depend du nombre de neurone du groupe en entree */
    increment = taille_groupe_e / taille_groupe_e2;
    deb_e = def_groupe[gpe_entree1].premier_ele;
    deb_e3 = def_groupe[gpe_entree3].premier_ele;
/*printf("Nico: ATTENTION NEW DEVELOPMENT!\n");
printf("nico: si probleeme avec simul: decommenter le code qui traite les ordres en absolue!\n");*/
    /*NC: Recupere l'orientation courante (absolue) */
    if (gpe_entree2 != -1)      /* test si direction absolue boussole ,convention valeur du groupe -PI PI */
    {
        orientation_r = neurone[def_groupe[gpe_entree2].premier_ele].s;
        orientation_d = orientation_r * 180 / (PI); /*entre -180 et 180 */
    }
    else
    {
        orientation_r = 0.;
        orientation_d = 0.;
    }
    /* tous les neurones de sortie a 0 ... */
    for (i = 0; i < size; i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

    max = -999999.;
    pos = -1;
    for (n = deb_e;
         n <
         deb_e +
         def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley;
         n++)
    {
        if (neurone[n].s > max)
        {
            max = neurone[n].s;
            pos = (n - deb_e) / increment;
        }
    }
#ifdef DEBUG
    printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
    /*Si pas d'activite superieure a 0.1 alors pas de mouvement */
    if (max - .1 < 0 || pos == -1)
    {
#ifdef DEBUG
        printf("\nActivity is to low (max=%f) : No movement !\n", max);
#endif
        parametres_odometrie.norme = 0;
        parametres_odometrie.beta_tot = 0;
        parametres_odometrie.teta = teta;
        move_via_speed(0, 0, &norme, &beta_tot, 0);
        return;
    }
    /*sinon calcul de la norme desiree */
    else
        norm_wanted = DEFAULT_NORME * max;
#ifdef DEBUG
    printf("Max found : neuron No:%d  on:%d with a max=%f, norm_wanted=%f\n",
           pos, taille_groupe_e2, max, norm_wanted);
#endif
    /*calcul de l'angle du vecteur mouvement souhaite pour la simul only */
    mvt_r = pos * scale - PI;
    mvt_d = mvt_r * 180 / PI;
             /*-180;180*/
    /*NC: mvt_x est en absolue pour le moment */
#ifdef DEBUG
    printf("order mvt (absolute) of :%f radians or %f degre \n", mvt_r,
           mvt_d);
#endif
/* lecture de la derivee*/
    d_phi = neurone[deb_e3 + 30 /*29 */ ].s1;
    delta_angle = d_phi;
/*
printf("f_phi-2 :%f \n", neurone[deb_e+27].s1);
printf("f_phi-1 :%f \n",neurone[deb_e+28].s1 );
*/ if (isdiff(speed_cste, 0))
        printf("f_phi :%f \n", delta_angle);    /*
                                                   printf("f_phi+1 :%f \n", neurone[deb_e+30].s1);
                                                   printf("f_phi+2 :%f \n", neurone[deb_e+31].s1); */


    if (EMISSION_ROBOT == SET)  /* commande robot */
    {
#ifdef DEBUG
        /* printf("Nico ATTENTION encore en developpement!!!"); */
        printf("betat_tot: %f", beta_tot);
#endif /*demande a realiser le mouvement propose par mvt_d */
        if ((neurone[deb_e3 + 31].s1 * neurone[deb_e3 + 29].s1) <
            0 /*0.01 */ )
        {
            printf("delta:%f et %f\n", delta_angle, fabs(delta_angle));
            delta_angle = 0.;
        }
        if (fabs(delta_angle) < .009 && isdiff(delta_angle, 0.))
        {
            do
                delta_angle *= 10;
            while (fabs(delta_angle) < .015);
        }
        /*printf("dela_angle=%f\n",delta_angle); */
        p[0] = (int) (speed_cste) - (rot_speed * delta_angle);  /*inversion + -> - */
        p[1] = (int) (speed_cste) + (rot_speed * delta_angle);
        /*pour le labo bleu securite */
/*      if(abs(p[0])>25)p[0]=15;
      if(abs(p[1])>25)p[1]=15;
*/ if (isdiff(speed_cste, 0))
            printf("moot: %d,%d\n", p[0], p[1]);
/*scanf("%d",&i);*/
        move_via_speed(p[0], p[1], &norme, &beta_tot, 0);
        if (isdiff(speed_cste, 0))
            printf("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
#ifdef DEBUG
        printf("The actual direction given by the compass is : %f �\n",
               orientation_d);
        printf("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
#endif
        /*veritable angle de deplacement en relatif, grace a utilisation odometrie du robot */
        mvt_r = beta_tot;       /*teta; */
        teta = beta_tot;
#ifdef DEBUG
        printf("F_nav_mvt mvt_r ap mise a jour :%f\n", mvt_r);
        printf("F_nav_mvt teta ap mise a jour :%f\n", teta);
#endif
        /*L'activite du groupe est utilisee par l'integration de chemin,
           on repasse en orientation absolue: */
        mvt_r = mvt_r + orientation_r;
        if (mvt_r >= PI)
            mvt_r = mvt_r - 2 * PI;
        if (mvt_r < -PI)
            mvt_r = mvt_r + 2 * PI;

        num_neur = (int) ((mvt_r / (2 * PI)) * xmax2 * ymax2 + (xmax2 * ymax2 / 2));
        num_neur += deb;
        /*l'activite du neurone en sortie est fonction de la distance parcourue */
        neurone[num_neur].s = neurone[num_neur].s1 = neurone[num_neur].s2 = (norme / 1000);
        /* norme:  1=30cm */
#ifdef DEBUG
        printf("Robot's direction is now :%f, norm= %f \n", mvt_r, norme);
        printf("Neurone :%d, angle= %f, act: %f \n", num_neur - deb, mvt_r,
               norme / DEFAULT_NORME /*30000 */ );
#endif
/*scanf("%d",&i);*/
        /*Sauvegarde des donnees odmetriques */

        parametres_odometrie.norme = norme; /*longueur du vecteur deplacement realise */
        parametres_odometrie.beta_tot = beta_tot;   /*angle en relatif, dans le futur pourra etre equivalent au chemin              integre ou a la valeur de l'orientation... A definir */
        parametres_odometrie.teta = teta;   /*angle en relatif */
        def_groupe[numero].data = (void *) (&parametres_odometrie);
    }
    else if (USE_ROBOT == USE_SIMULATION)
    {

/*decommenter si mouvement en entree sont en abslue..*/
        /*calcul en relatif */
        /*mvt_r = mvt_r - orientation_r;
           if(mvt_r >= PI)        mvt_r = mvt_r -2*PI;
           if(mvt_r < -PI)        mvt_r = mvt_r +2*PI; */
                                                                                                                                                                                                                                 /*  mvt_d=mvt_r*180/PI; *//*-180;180*/

        norme = 30000.;
        /*NC: compute the new direction in absolute */
#ifdef DEBUG
        printf("The actual direction given by the compass is : %f �\n",
               orientation_d);
        printf("F_nav_mvt teta av mise a jour :%f\n", teta);
        printf("F_nav_mvt beta_tot av mise a jour :%f\n", beta_tot);
#endif
        /*mouvement relatif = mvt realise en simul */
        beta_tot = mvt_r;
        teta = mvt_r;           /*mouvement relatif pour la mise a jour du groupe f_orientation */
#ifdef DEBUG
        printf("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
#endif
        /*calcul en absolue, retour en absolue en attendant modification de la fonction de deplacement en simule... */
        mvt_r = mvt_r + orientation_r;
        if (mvt_r >= PI)
            mvt_r = mvt_r - 2 * PI;
        if (mvt_r < -PI)
            mvt_r = mvt_r + 2 * PI;
        mvt_d = mvt_r * 180 / PI;
        turn_simul(mvt_r, 1.);  /*appel du mouvement simule. La direction est en absolu - voir env_simul */
#ifdef DEBUG
        printf("Robot's direction is now :%f, norm= %f \n", mvt_r, norme);
#endif
        /*NC: recherche du neurone coreespondant et affectation */
        num_neur = (int) ((mvt_d / 360.0) * xmax2 * ymax2 + (xmax2 * ymax2 / 2));
        if (num_neur > xmax2 * ymax2)
            num_neur = 0;
        num_neur += deb;
#ifdef DEBUG
        printf("Neurone :%d, angle= %f, act: %f \n", num_neur - deb, mvt_r, norme / 30000);
#endif
        neurone[num_neur].s = neurone[num_neur].s1 = neurone[num_neur].s2 = (norme / 30000); /* norme:  1=30cm */

        /*Sauvegarde des donnees odmetriques */
        parametres_odometrie.norme = norme; /*longueur du vecteur deplacement realise */
        parametres_odometrie.beta_tot = beta_tot;   /*angle en relatif, dans le futur pourra etre equivalent au chemin integre ou a la valeur de l'orientation... A definir */
        parametres_odometrie.teta = teta;   /*angle en relatif */
        def_groupe[numero].data = (void *) (&parametres_odometrie);
    }
    else
    {
        printf("\nHum...problem in Emision_Robot, check your .dev file if USE_ROBOT!=4 then EMISSION_ROBOT should be set to 1...\n");
    }
}
