/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_location_odo f_location_odo
\brief 


\section Author 
-Name: Julien Hirel
-Created: 18/02/2009

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Gets the location of the robot, based on odometric information. The origin of the reference frame can be reset.
The function returns 3 neurons: 
 * The first is the x position of the robot (refered to the original frame)
 * The second is the y position of the robot
 * 3rd is the angular orientation (theta)

\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
Takes the following links as input :
- reset: Resets the origin of the reference frame used for the location

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */



#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

typedef struct mydata_location_odo
{
	int gpe_reset;
}mydata_location_odo;


void new_location_odo(int gpe)
{
	int i, l;
	int gpe_reset = -1;
	mydata_location_odo *my_data = NULL;

	/* Checking group size */
	if (def_groupe[gpe].nbre != 3)
		EXIT_ON_ERROR("ERROR in new_location_odo(%s): group size must be 3 neurons\n", def_groupe[gpe].no_name);

	if (def_groupe[gpe].data == NULL)
	{
		l = 0;
		i = find_input_link(gpe, l);    /* recherche les liens en entree */
		while (i != -1)
		{
			if (strcmp(liaison[i].nom, "reset") == 0)
				gpe_reset = liaison[i].depart;

			l++;
			i = find_input_link(gpe, l);
		}

		my_data = (mydata_location_odo*) malloc(sizeof(mydata_location_odo));
		if (my_data == NULL)
		{
			fprintf(stderr, "ERROR in new_location_odo(%s): malloc failed for data\n", def_groupe[gpe].no_name);
			exit(1);
		}

		my_data->gpe_reset = gpe_reset;
		def_groupe[gpe].data = my_data;
	}

}

void function_location_odo(int gpe)
{
	int first = def_groupe[gpe].premier_ele;
	Robot *robot = robot_get_first_robot();
	int gpe_reset = ((mydata_location_odo *) def_groupe[gpe].data)->gpe_reset;

	/* Resets location information */
	if (gpe_reset >= 0 && neurone[def_groupe[gpe_reset].premier_ele].s1 > 0.5)
		robot_reset_location_odo(robot);

	/* Updates information about the current robot location */
	robot_get_location_odo(robot);

	dprints("f_location_odo(%s): robot location => posx = %f, posy = %f, orientation = %f\n", def_groupe[gpe].no_name, robot->posx_odo, robot->posy_odo, robot->orientation_odo);

	/* Current robot location */
	neurone[first].s = neurone[first].s1 = neurone[first].s2 = robot->posx_odo;
	neurone[first+1].s = neurone[first+1].s1 = neurone[first+1].s2 = robot->posy_odo;

	/* Current robot orientation */
	neurone[first+2].s = neurone[first+2].s1 = neurone[first+2].s2 = robot->orientation_odo;
}

void destroy_location_odo(int gpe)
{
	if (def_groupe[gpe].data != NULL)
	{
		free(((mydata_location_odo*) def_groupe[gpe].data));
		def_groupe[gpe].data = NULL;
	}
	dprints("destroy_location_odo(%s): Leaving function\n", def_groupe[gpe].no_name);
}
