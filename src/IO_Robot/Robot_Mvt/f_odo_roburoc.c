/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_odo_roburoc f_odo_roburoc 
 

\section Author
- author: Adrien
- description: specific file creation
- date: XX/XX/XXXX

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description
Reproject sur 4 neurones les vitesses des 4 roues du roburoc. Cette valeur est entre 0 et 1.  les roues sont dans cet ordre: avant droite, avant gauche, arriere droite, arriere gauche.


\section Macro
-none

\section Local variables
-none

\section Global variables
-none


\section Internal Tools
-none


\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

/*#define DEBUG*/


#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>

void new_odo_roburoc(int gpe)
{
   int first = def_groupe[gpe].premier_ele;
   
    /* Verification du nombre de neurones */
    if (def_groupe[gpe].nbre != 4)
    {
       EXIT_ON_ERROR("group size must be 4 neurons");
    }

    /* Initialisation des valeurs */
    neurone[first].s =
       neurone[first].s1 =
       neurone[first].s2 = 0.;
    
    neurone[first+1].s =
       neurone[first+1].s1 =
       neurone[first+1].s2 = 0.;
    
    neurone[first+2].s =
       neurone[first+2].s1 =
       neurone[first+2].s2 = 0.;

    neurone[first+3].s =
       neurone[first+3].s1 =
       neurone[first+3].s2 = 0.;
}

void function_odo_roburoc(int gpe)
{
	int first = def_groupe[gpe].premier_ele;
	int gpe_reset=-1;
	Robot *robot = robot_get_first_robot();
	char commande[256];
	char resultat[256];
	float wheel_front_right, wheel_front_left, wheel_back_right, wheel_back_left=.0;
	ClientRobubox *crb;

	crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);
	sprintf(commande,"%s","GetSpeeds"); 
	memset(resultat,0,256*sizeof(char));
	clientRobubox_transmit_receive(crb,commande,resultat); /*envoie de la demande des vitesses des 4 roues a la robubox*/
	sscanf(resultat,"%s %f %f %f %f", commande, &wheel_front_left, &wheel_back_left, &wheel_front_right, &wheel_back_right);

#ifdef DEBUG
	dprints("commande : %s\n",commande);
	dprints("wheel_front_right : %f\n",wheel_front_right);
	dprints("wheel_front_left : %f\n",wheel_front_left);
	dprints("wheel_back_right : %f\n",wheel_back_right);
	dprints("wheel_back_left : %f\n",wheel_back_left);
#endif

   /* Vitesse de la roue avant-droite sur le premier neurone du groupe */
   neurone[first].s = neurone[first].s1 = neurone[first].s2 = wheel_front_right;

   /* Vitesse de la roue avant-gauche sur le deuxieme neurone du groupe */
   neurone[first + 1].s = neurone[first + 1].s1 = neurone[first + 1].s2 = wheel_front_left;

   /* Vitesse de la roue arriere-droite sur le troisieme neurone du groupe */
   neurone[first + 2].s = neurone[first + 2].s1 = neurone[first + 2].s2 = wheel_back_right;

   /* Vitesse de la roue arriere-gauche sur le quatrieme neurone du groupe */
   neurone[first + 3].s = neurone[first + 3].s1 = neurone[first + 3].s2 = wheel_back_left;
      
(void) gpe_reset;
}
