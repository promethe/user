/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup move_via_speed move_via_speed
   \ingroup group_tools

   \file
   \ingroup move_via_speed
   \brief deplacement du robot selon le vecteur deplacement donne

   \details

   \section Description
   deplacement du robot selon le vecteur deplacement donne (angle [-180;180], distance), avec la vitesse specifiee (pas de vitesse angulaire c.f.: apres). Retourne les informations d'odometries correpondant au veritable deplacement effectue.
   Le robot realise d'abord une rotation sur lui meme si necessaire et va ensuite tout droit avec la vitesse specifiee jusqu'a ce qu'il ai parcouru la distance souhaite.

   - author: N.Cuperlier
   - description: specific file creation
   - date: 01/09/2004


   \section Macro
   - ANGLE90


   Internal Tools:
   - tools/IO_Robot/GenericTools/integr_deplacement_elementaire(...)

   External Tools:
   - tools/IO_Robot/Com_Koala/Init_Robot_Position_Counter()
   - tools/IO_Robot/Com_Koala/Read_Robot_Position_Counter(int* a,ont* b)
   - tools/IO_Robot/Com_Koala/GoToLeftRightBySpeedUncond(float speed,float speed)

************************************************************/
/*#define DEBUG*/
#include <stdio.h>
#include <libx.h>
#include <Macro/IO_Robot/macro.h>
#include <libhardware.h>
#include <IO_Robot/GenericTools.h>

void move_via_speed(int l, int r, double *norme,
                    double *beta_tot, int reset)
{
  static int dsav[2] = { 0, 0 };
  double /*temp=0., */ dummy = 0; /*Variable temporaire */

  if (robot_get_first_robot()->robot_type == 1)
  {
    integr_deplacement_elementaire(dsav, norme, beta_tot, &dummy);
    /*doit on reseter le compteur des roues??? */
    if (reset > 0)
      robot_init_wheels_counter(robot_get_first_robot());
    robot_go_by_speed(robot_get_first_robot(), r, l);
  }

  if (robot_get_first_robot()->robot_type == 2)
  {
    integr_deplacement_elementaire(dsav, norme, beta_tot, &dummy);
    /*doit on reseter le compteur des roues??? */
    if (reset > 0)
      robot_init_wheels_counter(robot_get_first_robot());
    robot_go_by_speed(robot_get_first_robot(), l, r);
  }
  else if (robot_get_first_robot()->robot_type == 3)
  {
    printf("l:%d\n", l);
    robot_set_velocity(robot_get_first_robot(), 0.2, 1, (l - r) / 10., 1);
  }

  /*on ne recupere que la norme car va tout droit... */
#ifdef DEBUG
  printf("turn done\n");
  printf("beta:%f\n", *beta_tot);
#endif

#ifdef DEBUG
  printf("Norme _finale: %f\n", *norme);
  printf("------------------------------------\n");
#endif
}
