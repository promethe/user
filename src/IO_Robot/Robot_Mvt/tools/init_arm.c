/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-get_max_dof_katana_arm()

Local variables:
-int NB_LOGICAL_DOF

Global variables:
-int MOTOR_LOGICAL_STATUS [get_max_dof_katana_arm()]
-float MOTOR_1
-float MOTOR_2
-float MOTOR_3
-float MOTOR_4
-float MOTOR_5

Internal Tools:
-freeze_logical_DOF()
-free_logical_DOF()
-display_arm_state)(
-display_DOF_state()
-arm_read_info()
-calibration()

External Tools: 
-tools/IO_Robot/Arm/set_motor_velocity()
-tools/IO_Robot/Arm/arm_print_position()
-tools/IO_Robot/Arm/arm_wait()
-tools/IO_Robot/Arm/set_arm_rotation()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <unistd.h>

#include <Macro/IO_Robot/macro.h>

#include <Global_Var/IO_Robot.h>

#include "include/local_var.h"

#include <libhardware.h>

#include "include/init_arm.h"


void arm_read_info(void)
{

    int param[4];
    char *data = NULL;
    if (data == NULL)
    {
        if ((data = malloc(sizeof(char) * get_maxdata())) == NULL)
        {
            printf("erreur\n");
            exit(1);
        }
    }

    commande_katana('B', param, data);
    printf(" Katana robot arm version: %s \n", data);

}

void set_encoders_new_values(float teta0, float teta1, float teta2, float teta3, float teta4)
{
    static int param[4];
    static char *data = NULL;
    static float *teta = NULL;
    int cpt;
    if (data == NULL)
    {
        if ((teta = malloc(sizeof(char) * get_maxdata())) == NULL)
        {
            printf("erreur\n");
            exit(1);
        }
    }
    if (teta == NULL)
    {
        if ((teta = malloc(sizeof(float) * get_max_dof_katana_arm())) == NULL)
        {
            printf("erreur\n");
            exit(1);
        }
    }
    teta[0] = teta0;
    teta[1] = teta1;
    teta[2] = teta2;
    teta[3] = teta3;
    teta[4] = teta4;

    param[1] = 0;               /*< Important!!! */
    for (cpt = 0; cpt < get_max_dof_katana_arm(); cpt++)
    {
        if ((get_logical_dof(cpt) == get_free()) && (teta[cpt] >= 0.0)
            && (teta[cpt] <= 1.0))
        {
            param[0] = 128 + 1 + cpt;
            param[2] = posHigh(cpt, teta[cpt]);
            param[3] = posLow(cpt, teta[cpt]);
            commande_katana('C', param, data);
        }
    }
}

void calibration(void)
{
    /*calibrating motor 2,3,4,5 */
    printf("reaching mechanical stoppers...\n");
    set_encoders_new_values(-1, 0.0, 0.0, 1.0, 1.0);
    set_arm_rotation(-1, 1.0, 1.0, 0.0, 0.0);
    sleep(3);

    set_encoders_new_values(-1, 0.0, 0.0, 0.0, 1.0);
    set_arm_rotation(-1, 1.0, 1.0, 0.0, 0.0);
    sleep(3);

    set_encoders_new_values(-1, 1.0, 1.0, 0.0, 0.0);
    set_arm_rotation(-1, 1.0, 1.0, 0.0, 0.0);
    sleep(3);

    set_encoders_new_values(-1, 1.0, 1.0, 0.0, 0.0);

    /* ajustement, on place les valeurs maximales avant les butees */
    set_arm_rotation(-1, 1.0, 1.0, 0.0, 0.0);
    sleep(2);

    /* valeurs finales */
    set_encoders_new_values(-1, 1.0, 1.0, 0.0, 0.0);

    /*set the arm in a usually safe position */
    set_arm_rotation(-1, 0.25, 0.25, 0.0, 0.0);
    sleep(2);
    /*calibrating motor 1 */
    set_encoders_new_values(0.0, -1, -1, -1, -1);
    set_arm_rotation(1.0, -1, -1, -1, -1);
    sleep(2);
    set_encoders_new_values(0.0, -1, -1, -1, -1);
    set_arm_rotation(1.0, -1, -1, -1, -1);
    sleep(2);
    set_encoders_new_values(0.0, -1, -1, -1, -1);
    set_arm_rotation(1.0, -1, -1, -1, -1);
    sleep(2);
    set_encoders_new_values(1.0, -1, -1, -1, -1);
    set_arm_rotation(0.77, -1, -1, -1, -1);
    sleep(2);
    set_encoders_new_values(1.0, -1, -1, -1, -1);
    arm_print_position();
}

void display_DOF_state(int MotorNumber)
{
    static char state[8];

    if ((MotorNumber >= 0) && (MotorNumber < get_max_dof_katana_arm()))
    {
        if (get_logical_dof(MotorNumber) == get_freezed())
            sprintf(state, "freezed");
        else
            sprintf(state, "free");
    }
    printf("\n motor %d logical status...%s ", MotorNumber, state);
}

void display_arm_state(void)
{
    int i;
    for (i = 0; i < get_max_dof_katana_arm(); i++)
        display_DOF_state(i);
    printf("\n number of free logical dof  %d \n", NB_LOGICAL_DOF);

}

void free_logical_DOF(int MotorNumber)
{
    if ((MotorNumber >= 0) && (MotorNumber < get_max_dof_katana_arm()))
        set_logical_dof(MotorNumber, get_free());
}

void freeze_logical_DOF(int MotorNumber)
{
    if ((MotorNumber >= 0) && (MotorNumber < get_max_dof_katana_arm()))
        set_logical_dof(MotorNumber, get_freezed());
}

void init_arm(void)
{


    int i;
    char user;



    printf("____________________________________________________________\n");
    for (i = 0; i < get_max_dof_katana_arm(); i++)
    {
        if (MOTOR_LOGICAL_STATUS[i] == 0)
            freeze_logical_DOF(i);
        else
        {
            free_logical_DOF(i);
            NB_LOGICAL_DOF++;
        }
    }

    printf("Logical Setup:\n");
    display_arm_state();
    printf("\nPhysical Setup\n");
    arm_read_info();
    printf("\n Vellocity of motors: \n");
    set_motor_velocity(1, 70, 70, 16, 10);
    set_motor_velocity(2, 70, 70, 16, 10);
    set_motor_velocity(3, 70, 70, 16, 10);
    set_motor_velocity(4, 70, 70, 16, 5);
    set_motor_velocity(5, 70, 70, 16, 5);
    printf("\n Positions of motors: \n");
    arm_print_position();
    printf("\n Do you need calibration (recomended  at  Power_On) y/n \n");
    user = getchar();
    if ((user == 'y') || (user == 'Y'))
        calibration();
    arm_wait();
    /*printf("\n moving joints : 0.5,0.5,0.5,0.5,0.5\n");
       set_arm_rotation  (0.5 ,0.5,0.5,0.5,0.5); *//*< moves assuring all is ok  */
    /*arm_wait(); */
    printf("____________________________________________________________\n");

 /***********************************************/

    /*temporary configuration of motors velocity  */
    /*static values */


 /***********************************************/
    /* read the starting pos in the 'appli_name'.dev/ file */
    set_arm_rotation(MOTOR_1, MOTOR_2, MOTOR_3, MOTOR_4, MOTOR_5);
    arm_wait();
}
