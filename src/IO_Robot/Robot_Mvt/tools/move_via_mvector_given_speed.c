/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
    defgroup move_via_mvector_given_speed move_via_mvector_given_speed
    ingroup group_tools
    
    CURRENTLY NOT USED ANYWHERE - SEE move_via_speed

    \file 
    ingroup move_via_mvector_given_speed
    \brief deplacement du robot selon le vecteur deplacement donne

    \details

    \section Description
    deplacement du robot selon le vecteur deplacement donne (angle [-180;180], distance), avec la vitesse specifiee (pas de vitesse angulaire c.f.: apres). Retourne les informations d'odometries correpondant au veritable deplacement effectue.
    Le robot realise d'abord une rotation sur lui meme si necessaire et va ensuite tout droit avec la vitesse specifiee jusqu'a ce qu'il ai parcouru la distance souhaite.

    - author: N.Cuperlier
    - description: specific file creation
    - date: 01/09/2004

    \section Macro
    - ANGLE90



    Internal Tools:
    - tools/IO_Robot/GenericTools/integr_deplacement_elementaire(...)

    External Tools:
    - tools/IO_Robot/Com_Koala/Init_Robot_Position_Counter()
    - tools/IO_Robot/Com_Koala/Read_Robot_Position_Counter(int* a,ont* b)
    - tools/IO_Robot/Com_Koala/GoToLeftRightBySpeedUncond(float speed,float speed)

************************************************************/
/* #define DEBUG */
#include <stdio.h>
#include <libx.h>
#include <Macro/IO_Robot/macro.h>
#include <libhardware.h>
#include <IO_Robot/GenericTools.h>

void move_via_mvector_given_speed(float angle, float distance,
                                  double *norme, double *beta_tot)
{
  static int dsav[2] = { 0, 0 };
  double temp = 0., dummy = 0;    /*Variable temporaire */
  int wheels_start[2];
  int wheels_stop[2]={0,0};
  char *noth = NULL;
  FILE *fp;
  static float  alpha = 0.;

  /*Oriente le robot dans la direction souhaitee */
  /*Se deplacement se fait surplace: norme=0, donc on ne pet pas utiliser
    la fonction integr_deplacement_elementaire(...) */

  /*
    if(angle < 0.)  turn_left(- angle);
    else  turn_right(angle); */

  robot_init_wheels_counter(robot_get_first_robot());
  integr_deplacement_elementaire(dsav, norme, &temp, &dummy); /*on ne recupere que la norme car va tout droit... */

  robot_read_wheels_counter(robot_get_first_robot(), &wheels_start[0],
			    &wheels_start[1]);
  if (isdiff(angle, 0.))
    robot_turn_angle(robot_get_first_robot(), angle);

  /*Mais on lit quand meme l'angle reellement utilise */
  robot_read_wheels_counter(robot_get_first_robot(), &wheels_start[0],
			    &wheels_start[1]);
  *beta_tot =
    ((float)
     ((wheels_stop[0] - wheels_start[0]) -
      (wheels_stop[1] - wheels_start[1]))) / 2.;
  *beta_tot =
    *beta_tot / (float) robot_get_first_robot()->angle90 * (M_PI / 2);
  /*La, on a le beta_tot */
  fp = fopen("odometry.SAVE", "a");
  fprintf(fp, "0. %f\n ", (float) *beta_tot);

  alpha = alpha + *beta_tot;

  robot_commande_koala(robot_get_first_robot(), COM_G, wheels_start, noth);

  robot_go_by_position_with_odo(robot_get_first_robot(), (float) (distance),
				(float) (distance));
  integr_deplacement_elementaire(dsav, norme, &temp, &dummy); /*on ne recupere que la norme car va tout droit... */



  fprintf(fp, "%f %f\n ", (float) (*norme), (float) temp);
  fclose(fp);

}
