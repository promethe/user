/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_readout_nms.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>


/*#define DEBUG*/

#include <net_message_debug_dist.h>

typedef struct My_Data_f_readout_nms
{
    int gpe_nf;
    int gpe_derive;
    int type_lecture;
    int * indsmax;
    
} My_Data_f_readout_nms;


/*Realise le readout de f_navigation_movement_simple (eaquivaut au mode pseudo de f_navigation_movement_simple)*/
void function_readout_nms(int gpe)
{
    My_Data_f_readout_nms * my_data = NULL;
    int gpe_nf=-2, gpe_derive=-2;
    int i, l, type_lecture = 0.;
    char param[32];

    int indmax = -1;
    int *indsmax = NULL;
    float min;

    float previous,current,next;
    int cpt;

    if (def_groupe[gpe].data == NULL)
    {
        
        i = 0;
        while ( (l = find_input_link(gpe, i)) != -1)
        {
            if (strcmp(liaison[l].nom, "NF") == 0)
                gpe_nf = liaison[l].depart;
            else if (strcmp(liaison[l].nom, "DP") == 0)
                gpe_derive = liaison[l].depart;
            
            if (prom_getopt(liaison[l].nom, "-t", param) > 1)
                type_lecture = atoi(param);
            
            i++;
        }
        if (gpe_derive == -2 || gpe_nf ==-2)
        {
            printf("Il faut 1 groups avec  <DP> au moins sur les liens de %s\n",__FUNCTION__);
            printf("exit in %s\n", __FUNCTION__);
            exit(-1);
        }

	indsmax = (int *) malloc(sizeof(int) * def_groupe[gpe_derive].nbre);
	if (indsmax == NULL)
	{
	    printf("probleme de malloc in function %s at line %d\n", __FUNCTION__,  __LINE__);
	    exit(0);
	}

        my_data = (My_Data_f_readout_nms *)malloc(sizeof(My_Data_f_readout_nms));
        my_data->gpe_nf = gpe_nf;
        my_data->gpe_derive = gpe_derive;
        my_data->type_lecture = type_lecture;
        my_data->indsmax = indsmax;
        def_groupe[gpe].data = (My_Data_f_readout_nms *) my_data;

        /* Ajout Vincent */
        /* Mise a zero des compteurs des roues dans le cas du premier appel a la fonction */
        /*robot_init_wheels_counter(robot) ; */
    }
    else
    {
        my_data = (My_Data_f_readout_nms *) def_groupe[gpe].data;
        gpe_nf = my_data->gpe_nf;
        gpe_derive = my_data->gpe_derive;
        type_lecture = my_data->type_lecture;
	indsmax = my_data->indsmax;
    }

    
    /*init du tableau des max*/
    for (i = 0; i < def_groupe[gpe_derive].nbre; i++)
    {
        indsmax[i] = -1000;
    }
    
    /*determine le max dans le nf vers lequel on se dirige (ie les passages de neg vers pos dans la derive) */
    /*le max en quetion est choisis comme la plus proche */
    if (type_lecture == 0)
    {
        cpt = 0;
        /*on cherche les max */
        for (i = 1; i < def_groupe[gpe_derive].nbre - 1; i++)
        {
            if (neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1 > 0. && 
                neurone[i + def_groupe[gpe_derive].premier_ele + 1].s1 <= 0.)
            {
                indsmax[cpt] = i;
                /*printf("%f   -   %f    =>   max:%d\n",neurone[i+def_groupe[gpe_derive].premier_ele].s1,neurone[i+def_groupe[gpe_derive].premier_ele+1].s1,i); */
                cpt++;
            }
        }
        /*on detemine les plus proches */
        min = 1000.;
        for (i = 0; i < cpt; i++)
        {
            /*il y  un petit strabisme de 1 neurone */
            if (fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]) < min)
            {
                indmax = indsmax[i];
                min = fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]);
            }
        }

    }
    else if (type_lecture == 1)
        /*Cette foix le max est le plus proche dans la direction donne par la derive */
    {
        if (neurone[ (def_groupe[gpe_derive].nbre/2)+def_groupe[gpe_derive].premier_ele ].s1 > 0.)
        {

            for (i = def_groupe[gpe_derive].nbre / 2; i < def_groupe[gpe_derive].nbre - 1; i++)
            {
                previous = neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1;
                current = neurone[i + def_groupe[gpe_derive].premier_ele].s1;
                next = neurone[i + 1 + def_groupe[gpe_derive].premier_ele].s1;
                
                if ( previous > 0. && next < 0. && fabsf(current) <= fabsf(previous) && fabsf(current) <= fabsf(next) )
                {
                    indmax = i;
                    break;
                }
            }
            
        }
        else
        {
            for (i = def_groupe[gpe_derive].nbre / 2; i > 0; i--)
            {
                previous = neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1;
                current = neurone[i + def_groupe[gpe_derive].premier_ele].s1;
                next = neurone[i + 1 + def_groupe[gpe_derive].premier_ele].s1;
                
                if ( previous > 0. && next < 0. && fabsf(current) <= fabsf(previous) && fabsf(current) <= fabsf(next) )
                {
                    indmax = i;
                    break;
                }
            }
        }
        /*    printf("no developpe\n");exit(0); */
    }
    /* indmax est l'indice du max sur le champs NF : ce qui donne la vitesse angulaire de base */

    for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    
    neurone[def_groupe[gpe].premier_ele + indmax].s =
            neurone[def_groupe[gpe].premier_ele + indmax].s1 =
            neurone[def_groupe[gpe].premier_ele + indmax].s2 = 1.0;


}
