/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_set_robot_location f_set_robot_location
 

\section Author
Author: Julien Hirel
Created: 26/08/2011

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description
    Sets the location of the robot in an absolute reference frame.




\section Macro
-none

\section Local variables
-none

\section Global variables
-none


\section Internal Tools
-none


\section External Tools
-tools/Vision/affiche_pdv()

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/



/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>


typedef struct mydata_set_robot_location
{
      int gpe_pos;
      int gpe_cond;
} mydata_set_robot_location;


void new_set_robot_location(int gpe)
{
   int index=0, link, gpe_pos = -1, gpe_cond = -1;
   mydata_set_robot_location *my_data = NULL;

   if (def_groupe[gpe].data == NULL)
   {      
      while ((link = find_input_link(gpe, index++)) != -1)
      {
	 if (strcmp(liaison[link].nom, "pos") == 0)
	 {
	    gpe_pos = liaison[link].depart;
	 }

	 if (strcmp(liaison[link].nom, "cond") == 0)
	 {
	    gpe_cond = liaison[link].depart;
	 }
      }
      
      if (gpe_pos == -1)
      {
	 EXIT_ON_ERROR("Missing 'pos' input link");
      }

      if (def_groupe[gpe_pos].nbre < 2)
      {
	 EXIT_ON_ERROR("'pos' input group must have at least 2 neurons");
      }

      my_data = ALLOCATION(mydata_set_robot_location);
      my_data->gpe_pos = gpe_pos;
      my_data->gpe_cond = gpe_cond;
      def_groupe[gpe].data = my_data;
   }
}

void function_set_robot_location(int gpe)
{
   int first = -1, first_pos = -1;
   Robot *robot = robot_get_first_robot();
   mydata_set_robot_location *my_data = (mydata_set_robot_location *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retreive data");
   }

   first = def_groupe[gpe].premier_ele;
   first_pos = def_groupe[my_data->gpe_pos].premier_ele;

   /* Sets information about the current robot location */
   if (my_data->gpe_cond == -1 || neurone[def_groupe[my_data->gpe_cond].premier_ele].s1 > 0.5)
   {
      robot_set_location_abs(robot, neurone[first_pos].s1, neurone[first_pos+1].s1);
      neurone[first].s = neurone[first].s1 = neurone[first].s2 = 1.;
      dprints("f_set_robot_location(%s): Setting robot location to %f, %f\n", def_groupe[gpe].no_name, neurone[first_pos].s1, neurone[first_pos+1].s1);

   }
   else
   {
      neurone[first].s = neurone[first].s1 = neurone[first].s2 = 0.;
   }
}

void destroy_set_robot_location(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((mydata_set_robot_location*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_set_robot_location(%s): Leaving function\n", def_groupe[gpe].no_name);
}
