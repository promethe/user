/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_set_velocity.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: M.Lagarde
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:

Internal Tools:

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/*#define DEBUG*/

typedef	struct	s_set_velocity
{
  Robot		*robot; 
  int		gpe_linear_speed;
  int		gpe_rotational_speed;
  float 	max_angular_speed;
  float 	max_linear_speed;

  int 		mode_horaire;

}		t_set_velocity;
 
void		function_set_velocity(int gpe)
{
  t_set_velocity	*data = NULL;
  int		l = 0;
  int		i = 0;
  float		linear_speed;
  float		rotational_speed;
  float 	max_angular_speed=2.*M_PI;
  float 	max_linear_speed=1.;
  char 		param[255];
  int 		mode_horaire=0;

  if (def_groupe[gpe].data == NULL)
    {
      if ((data = malloc(sizeof(t_set_velocity))) == NULL)
	{
	  fprintf(stderr, "function_set_velocity : allocation error\n");
	  exit(2);
	}

      for (i = 0; (l = find_input_link(gpe, i)) != -1; i++)
	{
	  if (strcmp(liaison[l].nom, "linear_speed") == 0)
	    {
	      data->gpe_linear_speed = liaison[l].depart;
	    }
	  if (strcmp(liaison[l].nom, "rotational_speed") == 0)
	    {
	      data->gpe_rotational_speed = liaison[l].depart;
	    }
	 if (prom_getopt(liaison[l].nom, "-a", param) > 1)
               max_angular_speed = atof(param);
	 if (prom_getopt(liaison[l].nom, "-l", param) > 1)
               max_linear_speed = atof(param);
	 if (prom_getopt(liaison[l].nom, "-h", param) > 1)
               mode_horaire = atoi(param);

	}
	
      if(max_linear_speed<0||max_angular_speed<0)
      {
	printf("problem in f_set_velocity: the max_linear_speed and the max_angular_speed should be positive\n");
	exit(-1);
      }

      data->robot = robot_get_first_robot();
      data->max_linear_speed=max_linear_speed;
      data->max_angular_speed=max_angular_speed;
      data->mode_horaire=mode_horaire;
      
      def_groupe[gpe].data = (void *)data;
    }
  else
    {
      data = (t_set_velocity *)def_groupe[gpe].data;
      max_linear_speed=data->max_linear_speed;
      max_angular_speed=data->max_angular_speed;
      mode_horaire=data->mode_horaire;
    }
  rotational_speed = ((float)(neurone[def_groupe[data->gpe_rotational_speed].premier_ele].s1) * max_angular_speed);
  linear_speed = neurone[def_groupe[data->gpe_linear_speed].premier_ele].s1*max_linear_speed;

  if(fabs(rotational_speed)>max_angular_speed)
  {
    if(rotational_speed>0)
      rotational_speed=max_angular_speed;
    else
     rotational_speed=-max_angular_speed; 
  }

  if(fabs(linear_speed)>max_linear_speed)
  {
    if(linear_speed>0)
      linear_speed=max_linear_speed;
    else
     linear_speed=-max_linear_speed; 
  }

  if(mode_horaire>0)
    rotational_speed=-rotational_speed;
   
  robot_set_velocity(data->robot, linear_speed, 0., rotational_speed, 1);

}

