/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_get_motor_power
\brief 
 

\section Author 
-Name: Pierre Delarboulas
-Created: 28/05/2014

*/

/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>

void new_get_motor_power(int gpe)
{
   int first = def_groupe[gpe].premier_ele;
   
    /* Verification du nombre de neurones */
    if (def_groupe[gpe].nbre != 2)
    {
       EXIT_ON_ERROR("group size must be 2 neurons");
    }

    /* Initialisation des valeurs */
    neurone[first].s =
       neurone[first].s1 =
       neurone[first].s2 = 0.;
    
    neurone[first+1].s =
       neurone[first+1].s1 =
       neurone[first+1].s2 = 0.;
    
}

void function_get_motor_power(int gpe)
{
   int first = def_groupe[gpe].premier_ele;
   Robot *robot = robot_get_first_robot();
   float pow_right, pow_left;

   robot_get_motor_power(robot, &pow_left, &pow_right);
	

   neurone[first].s = neurone[first].s1 = neurone[first].s2 = pow_left ;
   neurone[first + 1].s = neurone[first + 1].s1 = neurone[first + 1].s2 = pow_right;
}
