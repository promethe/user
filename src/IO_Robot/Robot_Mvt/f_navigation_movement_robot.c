/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   defgroup f_navigation_movement_simule2 f_navigation_movement_simule2
   ingroup libIO_Robot

   \brief Fonction generant le mouvement via la lecture d'un groupe de commande

   \details

   CHECK DOC !

   \section Description
   Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
   partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
   On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
   Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
   a la norme de ce mouvement (1 veut dire 30cm)
   L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)

   Macro:
   - PI
   - PI_2

   Local variables:
   - sortie_odometrie parametres_odometrie


   Global variables:
   - boolean EMISSION_ROBOT
   - int USE_SIMULATION
   - int USE_ROBOT

   Internal Tools:
   - move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)


   \file
   \ingroup f_navigation_movement_simule2
   \brief Fonction generant le mouvement via la lecture d'un groupe de commande

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: N.Cuperlier
   - description: fonction de realisation du mouvement
   - date: 01/03/2005

************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>

#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

#define DEBUG
#define CTE_DPHI 8              /*55 */
#define linear_max_speed 20
#define linear_min_speed 1
#define linear_min_speed_obst -2
int seui(int x)
{
  if (x > 0)
    return 1;
  return 0;
}

void function_navigation_movement_robot(int numero)
{

  static int MAX_ROT_SPEED;
  static int gpe_NF, gpe_compass = -1, gpe_DP,  gp_reset, gpe_satur, gpe_test_emission,  gpe_IR, deb_IR, current_robot_orientation, pos_rel_prec;
  static int deb, size, increment, xmax2, ymax2;
  static int deb_NF, taille_groupe_e, taille_groupe_e2, deb_DP = -1;
  static float scale, old_dphi;
  int pos_target,ret;
  int read_out_pos;
  float delta_angle = 0., d_phi = 0.; /*variable pour la lecture du NF */
  int i, n, pos_rel=0, max_neg = -1, max_pos = -1;
  int p[2];
  int num_neur;
  int save_posrel;
  float bias_origin;
  int pos = -1;               /*,decalage,correspondant; */
  int pos_max_d_phi;
  double beta_tot = 0, teta = 0 /* NC: [-Pi;+Pi] */ , norme = 0.;
  float mvt_r /*NC: en degre [-180;180[ */ ,
    orientation_r /*NC: [-PI; PI[ */ ,
    orientation_d /*NC: [-180; 180[ */ , max;
  float  angle_rel;
  static float speed_cste = 8.;
  float fa,  cdir;
  int pos_180, pos_relative_attracteur, ir_forced=0;

  int g, indexp, indexpp, indexppp, indexn, indexnn, indexnnn;
  float ddp, ddpp;
  int max_pos_found = -1, max_neg_found = -1, pos_ir=-1, pos_ir_rel=-1;

/*variable pour l'affichage graphique*/
#ifndef AVEUGLE
  TxPoint pt_a, pt_b;
  TxPoint derivee[61];
  char printmotor[100];
#endif



  printf("debut f_nav_mvt_robot\n");

  if (def_groupe[numero].data == NULL)
  {
    def_groupe[numero].data = (void *) (&parametres_odometrie);
    robot_init_wheels_counter(robot_get_first_robot());
    printf("toto\n");
/*Recherche des boites en entrees...*/
    gpe_DP = trouver_entree(numero, (char*)"DP");
    gpe_NF = trouver_entree(numero,(char*) "NF");
    gpe_compass = trouver_entree(numero, (char*)"compass");
    gpe_satur = trouver_entree(numero, (char*)"satur");
    gpe_test_emission = trouver_entree(numero, (char*)"test_emission");
    gp_reset = trouver_entree(numero, (char*)"reset");
    gpe_IR = trouver_entree(numero, (char*)"IR");
    if (gpe_compass == -1)
      printf("WARNING: f_nav_mvt: no f_orientation  find !\n");
#ifdef DEBUG
    printf("f_nav_mvt: NF:  %d; f_orientation: %d, DP %d!\n", gpe_NF, gpe_compass, gpe_DP);
    printf("f_nav_mvt: f_orientation  find at group: %d\n", gpe_compass);
#endif

/*initialisation des constantes et des variables*/
    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    size = xmax2 * ymax2;
    taille_groupe_e = def_groupe[gpe_NF].nbre;
    taille_groupe_e2 = def_groupe[gpe_NF].taillex * def_groupe[gpe_NF].tailley;
    scale = 2 * PI / (float) taille_groupe_e2;  /*echelle, depend du nombre de neurone du groupe en entree*/
    increment = taille_groupe_e / taille_groupe_e2;
    deb_NF = def_groupe[gpe_NF].premier_ele;
    deb_DP = def_groupe[gpe_DP].premier_ele;
    deb_IR = def_groupe[gpe_IR].premier_ele;
    MAX_ROT_SPEED = 20;     /*20 */
    current_robot_orientation = ((int) (size + 1) / 2);

  }
  printf("-------------------------------------------------\n");
  printf("Creating the movement \n");

  /*NC: Recupere l'orientation courante (absolue) */
  if (gpe_compass != -1)      /* test si direction absolue boussole ,convention valeur du groupe -PI PI */
  {
    orientation_r = neurone[def_groupe[gpe_compass].premier_ele].s;
    orientation_d = orientation_r * 180 / (PI); /*entre -180 et 180*/
  }
  else
  {
    orientation_r = 0.;
    orientation_d = 0.;
  }

  /* tous les neurones de sortie a 0 ... */
  for (i = 0; i < size; i++)
    neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

#ifndef AVEUGLE
/*Lecture du groupe de derivee pour affichage graphique */
  for (n = deb_DP;
       n < deb_DP + def_groupe[gpe_DP].taillex * def_groupe[gpe_DP].tailley;
       n++)
  {
    /*rempli le tableau pour affichage */
    derivee[n - deb_DP].x = (int) ((n - deb_DP) * 610 / 61 + 50);
    derivee[n - deb_DP].y = (int) (500 - (int) (neurone[n].s1 * 300));

  }
#endif

  max = -999999.;
  pos = -1;
  /*Recherche du max sur le groupe NF, ainsi que sa osition relative par rapport a l'orientation du robot */
  for (n = deb_NF;
       n < deb_NF + def_groupe[gpe_NF].taillex * def_groupe[gpe_NF].tailley;
       n++)
  {
    if (neurone[n].s > max)
    {
      max = neurone[n].s;
      pos = (n - deb_NF) / increment;
      pos_rel = current_robot_orientation - pos;  /*decalage par rapport a la position courante */
      angle_rel = pos_rel * (2 * M_PI) / size;    /*delta relatif entre l'angle desire et l'angle de l'orientation courante */
    }
  }
#ifdef DEBUG
  printf("Max_dphi:%f Num:%d Pos:%d, Pos_relative:%d\n", max, n, pos, pos_rel);
#endif
  max = 0;
  pos_ir = -1;
  for (n = deb_IR; n < deb_IR + def_groupe[gpe_IR].taillex * def_groupe[gpe_IR].tailley; n++)
  {
    if (neurone[n].s > max)
    {
      max = neurone[n].s;
      pos_ir = (n - deb_IR) / increment;
      pos_ir_rel = current_robot_orientation - pos_ir;    /*decalage par rapport a la position courante */
    }
  }


/* lecture de la derivee*/
  d_phi = neurone[deb_DP + def_groupe[gpe_DP].nbre / 2 - 1 /*29 */ ].s1;
  if (fabs(d_phi) < 0.0001)
    d_phi = old_dphi;
  old_dphi = d_phi;
  delta_angle = d_phi;
  printf("f_phi :%f ,Max_ir:%d,%d \n", delta_angle, pos_ir_rel, pos_ir);



/*recherche des 2 max APPARTENANT A L'ATTRACTEUR pos et neg de D_phi*/
  max_neg = -1;
  max_pos = -1;
  for (g = 0; g < size; g++)
  {
    /*calcul des index */
    indexp = pos + deb_DP + g;
    if (indexp >= size + deb_DP)
      indexp -= size;
    indexpp = pos + deb_DP + g + 1;
    if (indexpp >= size + deb_DP)
      indexpp -= size;
    indexppp = pos + deb_DP + g + 2;
    if (indexppp >= size + deb_DP)
      indexppp -= size;
    indexn = pos + deb_DP - g;
    if (indexn < deb_DP)
      indexn += size;
    indexnn = pos + deb_DP - g - 1;
    if (indexnn < deb_DP)
      indexnn += size;
    indexnnn = pos + deb_DP - g - 2;
    if (indexnnn < deb_DP)
      indexnnn += size;


    ddp = neurone[indexpp].s - neurone[indexp].s;
    ddpp = neurone[indexppp].s - neurone[indexpp].s;
    if (ddp * ddpp < 0)
    {
      /*recherche du maxima de dphi lier a l'atracteur en pos */
      if (neurone[indexpp].s > 0 && max_pos_found == -1)
      {
	max_pos = indexpp - deb_DP;
	max_pos_found = 1;
      }
      /*recherche du minima de dphi lier a l'atracteur en pos */
      else if (neurone[indexpp].s < 0 && max_neg_found == -1)
      {
	max_neg = indexpp - deb_DP;
	max_neg_found = 1;
      }
      else
	printf("max deja trouve, celui ci iggnore: %d\n",
	       indexpp - deb_DP);
    }

    ddp = neurone[indexnn].s - neurone[indexn].s;
    ddpp = neurone[indexnnn].s - neurone[indexnn].s;
    if (ddp * ddpp < 0)
    {
      /*recherche du maxima de dphi lier a l'atracteur en pos */
      if (neurone[indexnn].s > 0 && max_pos_found == -1)
      {
	max_pos = indexnn - deb_DP;
	max_pos_found = 1;
      }
      /*recherche du minima de dphi lier a l'atracteur en pos */
      else if (neurone[indexnn].s < 0 && max_neg_found == -1)
      {
	max_neg = indexnn - deb_DP;
	max_neg_found = 1;
      }
      else
	printf("max deja trouve, celui ci iggnore: %d\n",
	       indexnn - deb_DP);
    }

    /*a ton trouver le maxima et le minima ?? si oui on arrete la recherche */
    if (max_pos != -1 && max_neg != -1)
      break;
  }

  if (!(max_pos != -1 && max_neg != -1))
  {
    printf("Un max non trouve, arret urgence!\n");
    printf("ddphi: Maxpos:%d\n", max_pos);
    printf("ddphi: Maxneg:%d \n", max_neg);
    move_via_speed(0, 0, &norme, &beta_tot, 0);
    /*scanf("%d",&pt_a); */
    /*exit(-1); */
    return;
  }

  if (d_phi < 0)
    pos_rel = max_neg;
  else
    pos_rel = max_pos;
  pos_target = pos;

  if (!(max_pos != -1 && max_neg != -1))
  {
    printf("Un max non trouve, arret urgence!\n");
    printf("ddphi: Maxpos:%d\n", max_pos);
    printf("ddphi: Maxneg:%d \n", max_neg);
    move_via_speed(0, 0, &norme, &beta_tot, 0);
    /*scanf("%d",&pt_a); */
    /*exit(-1); */
    return;
  }
#ifdef DEBUG
  printf("ddphi: Maxpos:%d\n", max_pos);
  printf("ddphi: Maxneg:%d \n", max_neg);
#endif

  printf("pos_rel choisi:%d,ir_forced:%d,produit:%d\n", pos_rel, ir_forced,
	 pos_ir_rel * (pos_rel - current_robot_orientation));

  pos_rel_prec = pos_rel;

/*si il y a un obstacle*/
  ir_forced = -1;
  printf("pos_rel choisi:%d,ir_forced:%d,produit:%d\n", pos_rel, ir_forced,
	 pos_ir_rel * (pos_rel - current_robot_orientation));

  printf("pos_rel choisi:%d,ir_forced:%d\n", pos_rel, ir_forced);

/************************************************************************************************
equation de prolongement de dphi a partir de pos_rel
le coefficient directeur de la droite est obtenu en ce placant dans le cas d'une consigne a 180° de la position courrante, avec une vitesse de rotation fixee: MAX_ROT_SPEED

f(pos)=cdir*pos + f(b)
b=pos_180 la position en nb de neurones d'un stimulus a 180°de l'orientation courrante. ex: si codage sur 60neurones. Lecture a l'index 30 de la pos courrante.pos_180=30-0=30;
f(b)=la valeur de dphi en la position du max de dphi (b)=pos_rel, offset a l'origine; 
f(a)=MAX_ROT_SPEED/CTE_DPHI cette valeur est celle qui sera utilisee pour faire appelle a move_via_speed dans le cas d'une consigne a 180°...
 cdir=(f(a)-f(b))/a avec a =pos_180
************************************************************************************************/
  /*position de l'attracteur */
  pos_relative_attracteur = (pos_target - current_robot_orientation);
  /*position du max dphi */
  pos_max_d_phi = (pos_rel - current_robot_orientation);
  /*ON SAUVEGARDE POS_REL AVANT QU'IL NE SOIT MODIFIE */
  save_posrel = pos_rel;
/*control de la vitesse lineaire*/
/*printf("maxspeed:%f,%f,%f,lowwspeed:%f,%f, obst_ir:%f,%f,suivi %f,%f; final speed:%f\n",(1-seui(pos_ir))*(1-(abs(pos-30)/30.f)),(1-(abs(pos-30)/30.f)),(linear_max_speed-speed_cste),(abs(pos-30)/30.f),(linear_min_speed-speed_cste),(1-seui(abs(pos_ir_rel)-10))*(abs(pos_ir-30)/30.f),(linear_min_speed_obst-speed_cste),0.3*(seui(pos_ir))*seui(abs(pos_ir_rel)-10),(linear_max_speed-10-speed_cste),speed_cste);

speed_cste+=0.3*(1-seui(pos_ir))*(1-(abs(pos-30)/30.f))*(linear_max_speed-speed_cste)+0.3*(abs(pos-30)/30.f)*(linear_min_speed-speed_cste)+0.3*seui(pos_ir)*(1-seui(abs(pos_ir_rel)-10))*(abs(pos_ir-30)/30.f)*(linear_min_speed_obst-speed_cste)+0.3*(seui(pos_ir))*seui(abs(pos_ir_rel)-10)*(linear_max_speed-10-speed_cste);
*/

#ifndef AVEUGLE
/* 
   Nous allons plotter a cet endroit la valeur de la fonction de consigne
   motrice, qui est une fonction par morceau : une partie provient de la derivee
   du NF, l'autre est une fonction lineaire
*/
  pt_a.x = 0;
  pt_a.y = 0;

  /*EFFACE L'IMAGE */
  TxDessinerRectangle(&image1, blanc, TxPlein, pt_a, 1000, 1000, 1);
  /*PLOT 2 carres bleu pour les max de dphi et un carre rouge sur l'attracteur */
  pt_a.x = (int) ((max_pos) * 610 / 61 + 50);
  pt_a.y = (int) (500 - (int) (neurone[max_pos + deb_DP].s1 * 300));
  TxDessinerRectangle(&image1, bleu, TxPlein, pt_a, 5, 5, 1);

  pt_a.x = (int) ((max_neg) * 610 / 61 + 50);
  pt_a.y = (int) (500 - (int) (neurone[max_neg + deb_DP].s1 * 300));
  TxDessinerRectangle(&image1, bleu, TxPlein, pt_a, 5, 5, 1);
  pt_a.x = (int) ((pos) * 610 / 61 + 50);
  pt_a.y = 500;
  TxDessinerRectangle(&image1, rouge, TxPlein, pt_a, 5, 5, 1);
#endif

/*Exploitation de la derivee du NF*/
  printf("pos_relative_attracteur:%d, pos_max_dphi:%d \n",
	 pos_relative_attracteur, pos_max_d_phi);

  if ((neurone[deb_DP + current_robot_orientation + 1].s1 * neurone[deb_DP + current_robot_orientation - 1].s1) < 0 && abs(pos - current_robot_orientation) < 3)  /*seulement pour attracteur et non repulseur donc psa target_pos */
  {
    /*test si l'on est dans la zone d'attraction (les carres bleus sont ils de part et d'autre de l'orientation courante: orientation courrante sur l'attracteur: si oui va tout droit */
    printf("delta:%f et %f\n", delta_angle, fabs(delta_angle));
    delta_angle = 0.;
    p[1] = 0 /*speed_cste */ ;
    p[0] = 0 /*speed_cste */ ;
    printf("0000000 motors result from dphi: %d,%d\n", p[0], p[1]);
  }
  else if (pos_max_d_phi * pos_relative_attracteur <= 0
	   && max_pos - current_robot_orientation <
	   max_neg - current_robot_orientation)
  {
    /*test si l'orientation du robot se trouve entre la position de l'attracteur et le max de DPHI, si oui: exploite info de la derivee */
    p[1] = (int) (delta_angle * CTE_DPHI);
    p[0] = (int) (-delta_angle * CTE_DPHI);
    printf
      ("++++motors result from dphi: %d,%d,pos_max_d_phi : %d, pos_relative_attracteur : %d\n",
       p[0], p[1], pos_max_d_phi, pos_relative_attracteur);
    if (abs(pos_relative_attracteur) > 10)
      printf("\n\n\n ERREUR pos_relative_attracteur \n \n\n");
  }
  else
  {
    if (pos_max_d_phi < 0)
      pos_180 = pos_target + 2 * current_robot_orientation;
    else
      pos_180 = pos_target - 2 * current_robot_orientation;

/*	if(neurone[deb_DP+pos_rel].s2>0)*/
    fa = (float) (((float) (MAX_ROT_SPEED) / ((float) CTE_DPHI)));
    /*else
      fa= (float) (-((float)((MAX_ROT_SPEED)/((float)CTE_DPHI)))); */

    printf("MAX_ROT_SPEED : %d, CTE_DPHI : %d, fa : %f\n", MAX_ROT_SPEED,
	   CTE_DPHI, fa);

    /*cdir=(fabsf(fb)/fb)*(-pos_max_d_phi)*(fa- fb) /pos_180; */
    /*if (pos_max_d_phi<0)
      cdir=(float)((fa-fb)/(60));
      else 
      cdir=(float)((fb-fa)/(60));
      bias_origin=0*/ /*(float)fb-(float)pos_target*cdir */ ;
    cdir = (float) (-fa / 60);
    bias_origin = 0;
    if (neurone[deb_DP + pos_rel].s2 > 0)
      bias_origin = fa;
    else
      bias_origin = 0;
    if (pos_target - current_robot_orientation > 0)
      read_out_pos = current_robot_orientation + 60 - pos_target;
    else
      read_out_pos = current_robot_orientation - pos_target;
    delta_angle = (read_out_pos) * cdir + bias_origin;
    printf
      ("fa=%f, cdir=%f,pos_target=%d,delta_angle:%f, bias_origin : %f\n",
       fa, cdir, pos_target, delta_angle, bias_origin);
    if (fabs(delta_angle) < fabs(d_phi))
      delta_angle = d_phi;
    printf
      ("fa=%f, cdir=%f,pos_target=%d,delta_angle:%f, bias_origin : %f\n",
       fa, cdir, pos_target, delta_angle, bias_origin);
    p[1] = (int) (delta_angle * CTE_DPHI + 0.5 * delta_angle / fabs(delta_angle));  /*cste=4 pour test_ir_ok val 6 non tester */
    p[0] =
      (int) (-delta_angle * CTE_DPHI +
	     0.5 * delta_angle / fabs(delta_angle));
    printf("-----motors result: %d,%d\n", p[0], p[1]);
#ifndef AVEUGLE
    /*plot de la droite a partir des point d'origine et de destination */
/*du debut du champ a la target*/
    pt_b.x = (int) (0 + 50);
    pt_b.y = 500 - (int) (((61 - pos_target) * cdir + bias_origin) * 300);
    pt_a.x = (int) ((pos_target) * 610 / 61 + 50);
    pt_a.y = 500 - (int) (((60) * cdir + bias_origin) * 300);
    TxDessinerSegment(&image1, vert, pt_b, pt_a, 2);
/*de la target a la fin du champ*/
    pt_b.x = (int) (pos_target * 610 / 61 + 50);
    pt_b.y = (int) (500 - bias_origin * 300);
    /* la droite a partir du point b et point de destination obtenu via equation */
    pt_a.x = (int) (610 + 50);
    /*pt_a.y=(int)((-(abs(pos_max_d_phi)/pos_max_d_phi)*30*cdir+fb)*300);   */
    pt_a.y = 500 - (int) (((60 - pos_target) * cdir + bias_origin) * 300);
    printf
      ("*******************************fa calcule : %f, fa const : %f\n",
       pos_180 * cdir + bias_origin, fa);
    TxDessinerSegment(&image1, vert, pt_b, pt_a, 2);
#endif

  }
  speed_cste += 0.3 * (1 - (abs(pos - 30) / 30.f)) * (linear_max_speed - speed_cste) + 0.3 * (abs(pos - 30) / 30.f) * (linear_min_speed - speed_cste);
  p[1] += speed_cste;
  p[0] += speed_cste;
#ifndef AVEUGLE
  /*affiche graphique de divers infos complementaires */
  TracerAxe(&image1, vert, vert, 355, 600, 355, 400, 1, -1);
  TracerAxe(&image1, vert, vert, 50, 500, 660, 500, 60, 0);
  pt_a.x = 355;
  pt_a.y = 680;
  sprintf(printmotor, "moteurs : Left Motor = %d, Right Motor = %d\n", p[1], p[0]);
  TxEcrireChaine(&image1, vert, pt_a, printmotor, 0);
  pt_a.x = 355;
  pt_a.y = 700;
  if (p[0] > p[1])
    sprintf(printmotor, "tourne a GAUCHE\n");
  else if (p[0] < p[1])
    sprintf(printmotor, "tourne a DROITE\n");
  else
    sprintf(printmotor, "tout droit\n");
  TxEcrireChaine(&image1, vert, pt_a, printmotor, 0);
  pt_a.x = 355;
  pt_a.y = 750;
  sprintf(printmotor, "Linear speed:%f\n", speed_cste);
  TxEcrireChaine(&image1, vert, pt_a, printmotor, 0);
  TxDessinerPoint(&image1, rouge, derivee[0]);
  for (n = 1; n < 60; n++)
    TxDessinerSegment(&image1, rouge, derivee[n - 1], derivee[n], 2);
  TxFlush(&image1);
#endif


  printf("motors av seuillage: %d,%d\n", p[0], p[1]);

  if (abs(p[1]) > (MAX_ROT_SPEED + speed_cste) || abs(p[0]) > (MAX_ROT_SPEED + speed_cste))
  {
    p[1] = 0;
    p[0] = 0;
    printf("TTTTTTTTTTTTTTTTT ARRET ERREUR CONSIGNE MOTRICE EXCEDE SEUIL  \a \a \n");
  }
  printf("delta_angle FINAL: %f\n", delta_angle);
  printf("motors FINAL: %d,%d\n", p[0], p[1]);

  /*test arret uregence si saturation du NF */
  if (gpe_satur != -1)
  {
    if (neurone[def_groupe[gpe_satur].premier_ele].s2 > 0)
    {
      p[1] = p[0] = 0;
      printf("Arret d'urgence du a la saturation\n");
    }
  }

/*test si les ordres doivent etre emis au robot ou non*/
  if (gpe_test_emission != -1)
  {
    if (neurone[def_groupe[gpe_test_emission].premier_ele].s2 > 0.5)
    {
      p[1] = p[0] = 0;
      printf("test: aucun mouvement ne sera effectue\n");
    }
  }
  /*p[1] =  p[0] =0; */

  /*doit on reseter les compteurs des roues pour limiter les erreurs cumulative de l'odometrie */
  if (gp_reset != -1)
    if (neurone[def_groupe[gp_reset].premier_ele].s2 > 0)
      /*recupere l'angle et la norme du vecteur deplavement que l'on vient de realiser (l'ordre precedent) et realise le nouveau mouvement propose tout en realisant un reset */
      move_via_speed(p[0], p[1], &norme, &beta_tot, 1);
    else
      /*recupere l'angle et la norme du vecteur deplavement que l'on vient de realiser (l'ordre precedent) et realise le nouveau mouvement propose sans realiser de reset */
      move_via_speed(p[0], p[1], &norme, &beta_tot, 0);
  else
    /*recupere l'angle et la norme du vecteur deplavement que l'on vient de realiser (l'ordre precedent) et realise le nouveau mouvement propose */
    move_via_speed(p[0], p[1], &norme, &beta_tot, 0);

  printf("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
#ifdef DEBUG
  printf("The actual direction given by the compass is : %f °\n", orientation_d);
#endif
  /*veritable angle de deplacement en relatif, grace a utilisation odometrie du robot */
  mvt_r = beta_tot;           /*teta; */
  teta = beta_tot;
  /*Le mouvement affiche en sortie est celui effectue au cycle precedent (celui sur lequel on a des infos) */
  /*L'activite du groupe est utilisee par l'integration de chemin,on repasse donc en orientation absolue: */
  mvt_r += orientation_r;
  if (mvt_r >= PI)
    mvt_r = mvt_r - 2 * PI;
  if (mvt_r < -PI)
    mvt_r = mvt_r + 2 * PI;
  num_neur =
    (int) ((mvt_r / (2 * PI)) * xmax2 * ymax2 + (xmax2 * ymax2 / 2));
  /*TEST DEBORDEMENT */
  if (num_neur >= xmax2 * ymax2 || num_neur < 0)
  {
    num_neur = 0;
    printf("debordement de l'indice du neurone du groupe de mouvement:%d\n", num_neur);
    ret=scanf("%d", &num_neur);
    exit(-1);
  }
  num_neur += deb;
  /*l'activite du neurone en sortie est fonction de la distance parcourue */
  neurone[num_neur].s = neurone[num_neur].s1 = neurone[num_neur].s2 =
    (norme / 30000);
  /* norme:  1=30cm */
#ifdef DEBUG
  printf("Robot's direction is now :%f, norm= %f \n", mvt_r, norme);
  printf("Neurone :%d, angle= %f, act: %f \n", num_neur - deb, mvt_r,
	 norme /*30000 */ );
#endif

  /*Sauvegarde des donnees odmetriques */
  parametres_odometrie.norme = norme; /*longueur du vecteur deplacement realise */
  parametres_odometrie.beta_tot = beta_tot;   /*angle en relatif, dans le futur pourra etre equivalent au chemin              integre ou a la valeur de l'orientation... A definir */
  parametres_odometrie.teta = teta;   /*angle en relatif */
  def_groupe[numero].data = (void *) (&parametres_odometrie);
  printf("...fin f_nav_mvt_robot\n");
}
