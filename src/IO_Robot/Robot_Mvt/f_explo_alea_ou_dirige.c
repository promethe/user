/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_explo_alea_ou_dirige f_explo_alea_ou_dirige


\section Modified
- author: XXXXXXXXX
- description: specific file creation
- date: XX/XX/XXXX

\section Theoritical description
 - \f$  LaTeX equation: none \f$



\section Description
cette fonction ( et les outils inclus dedans ) est utile pour la gestion de
mouvements de robots dans le simulateur. Elle permet de choisir entre de
l'exploration aleatoire et une sequence de deplacements contenue dans un
fichier.

   Le format du fichier de mouvements :
Il est d'extension .batch,
il contient un et un seul nombre par ligne, allant de 0 a la taille du groupe,
suivi d'un saut de ligne.






\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <libx.h>
#include <string.h>
#include "../Env_Simul/tools/include/local_var.h"


typedef struct
{
   short flag;                 /*0 : aleatoire, 1 : dirige */
   int *trajvector;            /*si dirige contient les mouvements */
   int traj_compteur;          /*si dirige contient l'index du prochain mouvement */
   int batch_length;           /*si dirige contient la longueur du batch */
} feaod_datas;


/* cette fonction presuppose que le champs .data du groupe est deja nitialise et que le batch existe*/
void function_feaod_load_trajvector(int gpe, char *batchname)
{
   int length;
   FILE *batchptr = NULL;
   feaod_datas *dats;
   int cpt;
   char buf;
   int test;

   dats = (feaod_datas *) def_groupe[gpe].data;

   batchptr = fopen(batchname, "r");

   cpt = 0;
   length = 0;

   /*premiere phase : compter la duree du trajet pour en deduire la taille du vecteur */
   do
   {
      /*on ne s'interesse pas encore a la valeur des variables */
      if ((buf = fscanf(batchptr, "%d\n", &cpt)) != EOF)  length++;
   }
   while (buf != EOF);

   /*deuxieme phase : initialiser le pointeur de la matrice */
   dats->trajvector = (int *) malloc(length * sizeof(int));

   /*troisieme phase : remplir la matrice */
   rewind(batchptr);

   for (cpt = 0; cpt < length; cpt++)
   {
      test=fscanf(batchptr, "%d\n", &(dats->trajvector[cpt]));
      if (test == 0) {PRINT_WARNING("Erreur de lecture dans le fscnaf");}
   }

   fclose(batchptr);

   /*derniere pahse : remplir les infos dans la structure */
   dats->traj_compteur = 0;
   dats->batch_length = length;
}


/* fonction d'exploration aleatoire */

/* Note PG: on dirait que ce groupe fait l'hypothese qu'il y a tjs 120 neurones dans le groupe */
/* A verifier et a changer par une taille variable. */
void function_explo_alea_classique(int gpe)
{
   int deb, nbre, i;
   static int alea_number = -1;
   float val;
   static int l_traject_done = 0;
   static int prec_dir = -9999;
   /*= 60*/
   static int module = -1;
   int angle3q, angle2q, angle1q;

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   angle3q = (nbre * 3) / 4;   /* 90 =120 *3/4 */
   angle2q = nbre / 2;         /* 60 =120 /2   */
   angle1q = nbre / 4;         /* 30 =120 /4   */

   if (prec_dir == -9999)        prec_dir = angle2q;
   if (l_traject_done > module)  check2 = 0;

   for (i = 0; i < nbre; i++)
      neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.0;

   /*on test si on doit regenerer un mvt */
   if (check2 == 0 || mirror > 0)
   {
      l_traject_done = 0;
      /*on efface le mvt precedent */
      neurone[deb + prec_dir].s = neurone[deb + prec_dir].s1 = neurone[deb + prec_dir].s2 = 0.0;

      if (mirror == 1)
      {
         /*effet mirroir sur les murs en x */
         if (alea_number == 0)              alea_number = angle2q;  /* 60  */
         else if (alea_number == angle2q)   alea_number = angle1q /* 30 */ ;
         else if ((alea_number >= 0 && alea_number < angle1q )  || (alea_number >= angle2q /*60 */  && alea_number < angle3q /*90 */ )) alea_number += nbre / 4;
         else  alea_number -= nbre / 4;

         mirror = 0;
      }
      else if (mirror == 2)
      {
         /*effet mirroir sur les murs en y */
         if (alea_number == angle1q /* 30 */ )       alea_number = angle3q /*90 */ ;
         else if (alea_number == angle3q /*90 */ )   alea_number = angle1q /*30 */ ;
         else if ((alea_number >= angle1q    /*30 */ && alea_number < angle2q /*60 */ ) || (alea_number >= angle3q /*90 */  && alea_number < nbre /*120 */ ))    alea_number += nbre / 4;
         else  alea_number -= nbre / 4;

         mirror = 0;
      }
      else if (mirror == 3)
      {
         /*esquive sommaire d'un landmark */
         alea_number = (alea_number + angle1q /*30 */ ) % nbre;
         mirror = 0;
      }
      else
      {
         val = drand48();
         if (val < 0.5)   val = drand48();
         else             val = -(drand48());
         alea_number += (int) (val * (nbre / 4));
      }

      /* je ne comprends pas l'interet du while dans ce qui suit PG */
      while (alea_number < 0)      alea_number = alea_number + nbre /* 119 */ ;
      while (alea_number > nbre )  alea_number = alea_number - nbre /*119 */ ;

      prec_dir = alea_number;
      do
         module = (int) (drand48() * (5));
      while (module < 2);
      check2 = 1;
   }

   l_traject_done++;
   neurone[deb + alea_number].s = neurone[deb + alea_number].s1 = neurone[deb + alea_number].s2 = 1.0;
}



/* fonction de mise a jour du neurone de direction si on est en mode dirige */
void function_set_direction(int gpe)
{
   int debut, longueur, i;
   feaod_datas *dats;

   dats = (feaod_datas *) def_groupe[gpe].data;
   debut = def_groupe[gpe].premier_ele;
   longueur = def_groupe[gpe].nbre;


   for (i = debut; i < debut + longueur; i++)
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;

   neurone[debut + dats->trajvector[dats->traj_compteur]].s = neurone[debut + dats->trajvector[dats->traj_compteur]].s1 = neurone[debut + dats->trajvector[dats->traj_compteur]].s2 = 1.0;

   dats->traj_compteur++;
   if (dats->traj_compteur == dats->batch_length) dats->traj_compteur = 0;

}

int exploration_time = -1;
TxPoint nid = { -1000, -1000 };

/* fonction declenchant automatiquement des retours au nid  (pt de depart toutes les x iterations de temps */
/* Utile pour faire des simulations simples limitant la derive de l'integration de chemin ? */
/* (simple descente de gradient - peut rester coince avec les murs) */
void function_explore_et_nid(int gpe)
{
   int dx, dy;
   int exploration_time_period = 400;
   int time_limit_for_homing = 200;
   int deb, nbre;
   int i, p;
   float angle;                /* direction de mouvement de l'agent mise sur le champs de neurones du groupe */

   deb = def_groupe[gpe].premier_ele;
   nbre = def_groupe[gpe].nbre;

   dprints("exploration time = %d \n", exploration_time);

   if (exploration_time > time_limit_for_homing)
   {

      dprints("homing behavior \n");
      dx = -(posx - nid.x);
      dy = -(posy - nid.y);
      /* printf("dx = %d dy =%d \n",dx,dy); */
      angle = atan2((double) dy, (double) dx);
      angle = angle + M_PI;
      if (angle > 2 * M_PI)  angle = angle - 2 * M_PI;
      else if (angle < 0)    angle = angle + 2 * M_PI;
      /* printf("angle1 = %f \n",angle); */
      angle = angle * nbre / (2. * M_PI);

      dprints("----angle2 = %f , nbre = %d \n", angle, nbre);

      p = deb + (int) angle;
      for (i = deb; i < deb + nbre; i++)
      {
         neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.0;
      }
      neurone[p].s = neurone[p].s1 = neurone[p].s2 = 1.0;
      if (fabs(dx) <= 1 && fabs(dy) <= 1) exploration_time = 0;   /* on arrete le retour au nid */
   }
   else
   {
      function_explo_alea_classique(gpe);
   }
   exploration_time++;
   if (exploration_time > exploration_time_period)  exploration_time = 0;
}

/* cette fonction est la fonction principale : elle auguille le programme vers l'une ou l'autre des fonctions de
mouvement */
void function_explo_alea_ou_dirige(int gpe)
{
   char batchname[250];
   char bid;
   int flag;
   FILE *batchptr;
   feaod_datas *dats;
   int test;

   if (def_groupe[gpe].data == NULL)
   {
      do
      {
         cprints("saisir le nom du batch sans extension, ou null si pas de batch ou nid si alea+ retour au nid (nid=pt de depart):");
         test=scanf("%s%c", batchname, &bid);
         if (test == 0) {PRINT_WARNING("Erreur de lecture dans le scanf");}

         if (strcmp(batchname, "null") == 0)
         {
            flag = 1;
            break;
         }
         else if (strcmp(batchname, "nid") == 0)
         {
            flag = 3;
            break;
         }

         strcat(batchname, ".batch");
         batchptr = fopen(batchname, "r");
         if (batchptr == NULL)
         {
            cprints("le batch en question n'existe pas\n");
            flag = 0;
         }
         else
         {
            fclose(batchptr);
            flag = 2;
         }

      }
      while (flag == 0);

      def_groupe[gpe].data = (feaod_datas *) malloc(sizeof(feaod_datas));
      dats = (feaod_datas *) def_groupe[gpe].data;
      switch (flag)
      {
         case 1:
            dats->flag = 0;
            break;
         case 2:
            dats->flag = 1;
            function_feaod_load_trajvector(gpe, batchname);
            break;
         case 3:
            dats->flag = 3;
            break;
         default:
            EXIT_ON_ERROR("impossible !\n");

      }

      /* on est en simulation */
      /*initialise la structure point avec les coord courantes de l'agent */
      nid.x = posx;
      nid.y = posy;

   }
   else dats = (feaod_datas *) def_groupe[gpe].data;

   switch (dats->flag)
   {
      case 0:
         function_explo_alea_classique(gpe);
         break;
      case 1:
         function_set_direction(gpe);
         break;
      case 3:
         function_explore_et_nid(gpe);
         break;
      default:
         EXIT_ON_ERROR("impossible function_explo_alea_ou_dirige gpe=%d !\n", gpe);

   }

}
