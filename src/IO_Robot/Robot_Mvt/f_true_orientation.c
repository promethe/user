/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_orientation.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

author: N.Cuperlier
- description: Modification du code de VB pour nouveau standart odometrie
- date: 06/12/2004
Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Determine l'orientation actuelle du robot par rapport a son orientation de depart
orientation initiale=0; Apres un demi-tour, orientation=-Pi
La valeur de cette orientation est calculee � partir du mouvement (relatif) reelement effectue qui est indique
dans la structure d'odometrie: via teta

Macro:
-PI
-PI_2

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo
- description: none/ XXX
- input expected group: Doit etre connecte au meme groupe en entree
et en sortie (function_navigation_movment).
- where are the data?: (sortie_odometrie*)def_groupe[gp_entre].data

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/sortie_odometrie.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>

#include "tools/include/macro.h"
/*#include <tools/include/move_via_speed.h>*/ /*PG suppression lib inexistante ! 26/12/10*/
#include "../Env_Simul/tools/include/local_var.h"
#include <math.h>
#define DEBUG
/*#define sortie*/
extern int echelle_temps[];


  


void function_true_orientation(int numero)
{
  static int		deb;
  static float posx_prec=-1.,posy_prec=-1.;
  double theta,dx,dy;
  
deb=def_groupe[numero].premier_ele;

 if(isequal(posx_prec,-1) && isequal (posy_prec,-1))
{
	neurone[deb].s=neurone[deb].s1=neurone[deb].s2=0;
		posx_prec=posx;
		posy_prec=posy;
}
else{
      	dx = posx -posx_prec;
	dy = posy -posy_prec;
	theta = atan2(dy,dx);
	if (theta>=M_PI)theta=-2*M_PI;
	if (theta<-M_PI)theta=+2*M_PI;
	posx_prec=posx;
	posy_prec=posy;
      /*... et met � jour l'orientation en cons�quence!*/
     
	neurone[deb].s=neurone[deb].s1=neurone[deb].s2=theta;
    }

    printf("\nLa Boussole interne vaut %f\n", neurone[deb].s);
    

}

void function_true_orientation_acc(int numero)
{
static int	deb_e=-1,deb_ee=-1,taille_e=-1,increment_e=-1,deb,gp_entre=-1,first=1,count=0;
  int nbr;
  float temp=0.;
  sortie_odometrie* pointeur;


	if (first)
	{
		first=0;
		deb = def_groupe[numero].premier_ele;
		neurone[deb].s=neurone[deb].s1=neurone[deb].s2=0;
		gp_entre=trouver_entree(numero,(char*) "compass");
		if(gp_entre==-1)
		{
			printf("\nFunction Orientation... Erreur pas trouv�e d'entr�e avec un lien 'compass'!!\n");
			exit(-1);
		}
		else
		{
			deb_ee=def_groupe[gp_entre].premier_ele;
			gp_entre=trouver_entree(numero,(char*) "mvt");
			if(gp_entre==-1)
			{
				printf("\nFunction Orientation... Erreur pas trouv�e d'entr�e avec un lien 'mvt'!!\n");
				exit(-1);
			}
			deb_e = def_groupe[gp_entre].premier_ele;
			taille_e=def_groupe[gp_entre].nbre;
			nbr=def_groupe[gp_entre].taillex*def_groupe[gp_entre].tailley;
			increment_e=taille_e/nbr;
		}	
	}  


#ifdef DEBUG
 /* printf("\nFunction Orientation... (valeur courante de la boussole=%f [avant mise a jour])\n",neurone[deb].s);*/
#endif
 /* recup�re les donn�es odom�triques...*/
pointeur=(sortie_odometrie*)def_groupe[gp_entre].data;


	/*recup�re le d�placement relatif effectu�...*/
    if(pointeur!=NULL)
	{  
		temp=(pointeur->teta);
/*	 #ifdef DEBUG*/
 /* printf("Le teta recupere par function_orientation est %f, orientation from compass:%f\n",pointeur->teta,neurone[deb_ee].s);*/
/*#endif*/
	}
	else 
		temp=0;

	if(count>=echelle_temps[def_groupe[numero]. ech_temps])
		count=0;
	if(count==0)
	{
      
		#ifdef DEBUG
		printf("\n Lecture a partir de f_orientation %f,%f,%f\n",neurone[deb_ee].s,temp,neurone[deb_ee].s+temp);
		#endif
		/*... et met � jour l'orientation en cons�quence!*/
		if (neurone[deb_ee].s+temp<-PI)
			neurone[deb].s=neurone[deb].s1=neurone[deb].s2=neurone[deb_ee].s+temp+2*PI;
		else if (neurone[deb_ee].s+temp>=PI)
			neurone[deb].s=neurone[deb].s1=neurone[deb].s2=neurone[deb_ee].s+temp-2*PI;
		else
			neurone[deb].s=neurone[deb_ee].s1=neurone[deb].s2=neurone[deb_ee].s+temp;
		printf("\n La valeur corigee est %f\n",neurone[deb].s);
		#ifdef sortie	
		printf("\nLa Boussole interne vaut %f\n", neurone[deb].s);
		#endif     
	}
	else
	{
		#ifdef DEBUG
		printf("\n La valeur provenant de l'accumulation est %f,%f,%f\n",neurone[deb].s,temp,neurone[deb].s+temp);
		#endif
		/*... et met � jour l'orientation en cons�quence!*/
		if (neurone[deb].s+temp<-PI)
			neurone[deb].s=neurone[deb].s1=neurone[deb].s2=neurone[deb].s+temp+2*PI;
		else if (neurone[deb].s+temp>=PI)
			neurone[deb].s=neurone[deb].s1=neurone[deb].s2=neurone[deb].s+temp-2*PI;
		else
			neurone[deb].s=neurone[deb].s1=neurone[deb].s2=neurone[deb].s+temp;
		printf("\n La valeur corigee est %f\n",neurone[deb].s);
		#ifdef sortie	
		printf("\nLa Boussole interne vaut %f\n", neurone[deb].s);
		#endif     
	}
	count++;
}
