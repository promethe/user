/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none

Local variables:
-none

Global variables:
-int USE_HEAD
-int USE_CAM
-int USE_KATANA
-boolean emission_robot

Internal Tools:
-init_pan()
-init_arm()

External Tools: 
-tools/IO_Robot/Pan_Tilt/init_pan_tilt()
-tools/IO_Robot/Com_Koala/GoToLeftRightUncond()
-tools/IO_Robot/Serial/serial_open()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/IO_Robot.h>
#include <libhardware.h>

/*#define Obj_Robot*/

void function_test_robot(int numero)
{
    int i;
    static float cpt = 1.;
    static Robot *robot = NULL;
    float IR[16];
    (void) numero;
    /*printf("debut f_test_robot.c\n"); */

    /*Compass * compass;
       compass = compass_get_first_compass();
       for(i=0;i<10;i++)
       {
       printf("compass:%f\n",compass_read(compass));
       }
       printf("read compass\n");
     */
/*	joint_servo_command(joint_get_serv_by_name("tilt"),0.8,-1);
	
	for(i=0;i<100000;i++)
	{
		printf("joint\n");
		
	}
	
	joint_servo_command(joint_get_serv_by_name("pan"),0.2,-1);
*/
    /*if(robot==NULL)
       { */
    robot = robot_get_first_robot();

    /*robot_turn_angle(robot,10.); */

    /*robot_go_by_position(robot,10.,10.);
       robot_go_by_position(robot,10.,-10.);
       robot_go_by_position(robot,-10.,10.);
       robot_go_by_position(robot,-10.,-10.);
       robot_go_by_speed(robot,103.,20.);
       for(i=0;i<100000;i++)
       {
       printf("speed%d\n",i);
       }
       robot_go_by_speed(robot,0.,0.);
     */

    /*joint_servo_command(joint_get_serv_by_name("pan"),cpt,-1); */
/*	joint_servo_command(joint_get_serv_by_name("tilt"),cpt,-1);*/
    /*printf("cpt:%f\n",cpt);
       cpt=cpt+0.05;
       if(cpt>0.9)
       cpt=0.4; */

    cpt++;
    robot_get_ir(robot, IR);
    printf("dans test_robot\n");
    for (i = 0; i < 16; i++)
    {
        printf("%f: %f ", cpt, IR[i]);
    }
    printf("\n");
    /*getchar();
       getchar();
     *//*robot_turn_angle(robot,-90.);

       robot_go_forward(robot,100);
       robot_go_forward(robot,-100);
     */
    /*robot_set_velocity(robot,0.5,0.,-0.1,1); */
    /*} */
/* 	printf("tilt,avance en speed pan\n");*/


}
