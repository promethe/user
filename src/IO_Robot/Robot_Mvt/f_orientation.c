/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_orientation.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

author: N.Cuperlier
- description: Modification du code de VB pour nouveau standart odometrie
- date: 06/12/2004
Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
  Determine l'orientation actuelle du robot par rapport a son orientation de depart
orientation initiale=0; Apres un demi-tour, orientation=-Pi
La valeur de cette orientation est calculee a partir du mouvement (relatif) reelement effectue qui est indique
dans la structure d'odometrie: via teta

Macro:
-PI
-PI_2

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo
- description: none/ XXX
- input expected group: Doit etre connecte au meme groupe en entree
et en sortie (function_navigation_movment).
- where are the data?: (sortie_odometrie*)def_groupe[gp_entre].data

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/sortie_odometrie.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "tools/include/macro.h"
/*#define DEBUG*/
#define sortie
void function_orientation(int numero)
{
    static int deb_e = -1, taille_e = -1, increment_e = -1, deb, gp_entre =
        -1, first = 1, compass_grp = -1, reset_grp = -1, compass_deb =
        -1, reset_deb = -1;
    static float compass_offset = -1;
    static int reset = -1;
    int nbr;
    float temp = 0.;
    sortie_odometrie *pointeur;


    if (first)
    {
        first = 0;
        deb = def_groupe[numero].premier_ele;
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = 0;
        /*recherche du lien vers la boussole */
        compass_grp = trouver_entree(numero,(char*) "compass");
        if (compass_grp != -1)
        {
            compass_deb = def_groupe[compass_grp].premier_ele;
            /*recupere  la valeur de la boussole. Elle coorespond dirctement au decalage avec l'orientation car a l'initialisation cette derniere vaut 0 */
            compass_offset = neurone[compass_deb].s;
            /*on repasse en radians */
            compass_offset = compass_offset * 2 * PI;

        }
        /*recherche du lien indiquant le reset de l'orientation */
        reset_grp = trouver_entree(numero, (char*)"reset");
        if (reset_grp != -1)
        {
            reset_deb = def_groupe[reset_grp].premier_ele;
        }
        gp_entre = trouver_entree(numero, (char*)"mvt");
        if (gp_entre == -1)
        {
            printf
                ("\nFunction Orientation... Erreur pas trouvee d'entree avec un lien 'mvt'!!\n");
            exit(-1);
        }

        /*i=find_input_link(numero,0);**NC test redondant */
        if (gp_entre > -1)
        {
            deb_e = def_groupe[gp_entre].premier_ele;
            taille_e = def_groupe[gp_entre].nbre;
            nbr = def_groupe[gp_entre].taillex * def_groupe[gp_entre].tailley;
            increment_e = taille_e / nbr;
        }
        else
        {
            printf
                ("\nFunction Orientation... Problems on input with link 'mvt' :%d!!\n",
                 gp_entre);
            exit(-1);
        }
    }

#ifdef DEBUG
    printf
        ("\nFunction Orientation... (valeur courante de la boussole=%f [avant mise a jour])\n",
         neurone[deb].s);
#endif
    /* recupere les donnees odometriques... */
    pointeur = (sortie_odometrie *) def_groupe[gp_entre].data;

    if (compass_grp != -1 && reset_grp != -1)
    {
        reset = neurone[reset_deb].s2;
        /*un recalage de l'orientation via la boussole est il demande?? */
        if (reset > 0)
        {
            /*si oui, l'orientation est reclcule: val_boussole-val_boussole_origine */
            temp = (neurone[compass_deb].s2 * 2 * PI) - compass_offset;

            /*modulo 2PI ... */
            if (neurone[deb].s + temp < -PI)
                neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 =
                    temp + 2 * PI;
            else if (neurone[deb].s + temp >= PI)
                neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 =
                    temp - 2 * PI;
            else
                neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = temp;
            /*
               printf("\nLa Boussole interne vaut %f\n", neurone[deb].s);
             */
            return;
        }
        else /*recupere le deplacement relatif effectue... */ if (pointeur !=
                                                                  NULL)
            temp = (pointeur->teta);
        else
            temp = 0;
    }
    else
    {
        /*recupere le deplacement relatif effectue... */
        if (pointeur != NULL)
        {
            temp = (pointeur->teta);
#ifdef DEBUG
            printf("Le teta recupere par function_orientation est %f\n",
                   pointeur->teta);
#endif
        }
        else
            temp = 0;
    }





#ifdef DEBUG
    printf("\n La valeur sans norme est %f\n", neurone[deb].s + temp);
#endif
    /*... et met a jour l'orientation en consequence! */
    if (neurone[deb].s + temp < -PI)
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 =
            neurone[deb].s + temp + 2 * PI;
    else if (neurone[deb].s + temp >= PI)
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 =
            neurone[deb].s + temp - 2 * PI;
    else
        neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 =
            neurone[deb].s + temp;
#ifdef DEBUG
    printf("\nLa Boussole interne vaut %f\n", neurone[deb].s);
#endif

}
