/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  f_speedo_angular_robot.c
 \brief
*/
#include <libx.h>
#include <string.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/*#define DEBUG*/

typedef struct s_speedo_angular_robot {
  Robot *robot;
  int gpe_linear_speed;
  int gpe_rotational_speed;
} t_speedo_angular_robot;

void function_speedo_angular_robot(int gpe)
{
  t_speedo_angular_robot *data = NULL;
  int l = 0;
  int i = 0;
  float linear_speed;
  float rotational_speed;

  if (def_groupe[gpe].data == NULL )
  {
    data = ALLOCATION(t_speedo_angular_robot);

    for (i = 0; (l = find_input_link(gpe, i)) != -1; i++)
    {
      if (strcmp(liaison[l].nom, "linear_speed") == 0)
      {
        data->gpe_linear_speed = liaison[l].depart;
      }
      else if (strcmp(liaison[l].nom, "rotational_speed") == 0)
      {
        data->gpe_rotational_speed = liaison[l].depart;
      }
    }
    data->robot = robot_get_first_robot();

    def_groupe[gpe].data = (void *) data;
  }
  else
  {
    data = (t_speedo_angular_robot *) def_groupe[gpe].data;
  }


  rotational_speed = ((float) (neurone[def_groupe[data->gpe_rotational_speed].premier_ele].s1) * 2.0 * M_PI);
  linear_speed = neurone[def_groupe[data->gpe_linear_speed].premier_ele].s1;
  robot_set_velocity(data->robot, linear_speed, 0., rotational_speed, 1);
}
