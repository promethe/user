/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/

void function_store(int numero)
{
    int i = 0, j = 0;
    int Gpe_pred = -1;
    int l = -1;
    float val=0.;
    float max = -1.;
    float alpha = def_groupe[numero].seuil;
    int mode = 0.;
    int pos = -1;
    float sum = -1.;

    /*      printf("f_store  de %d\n",numero); */

    if (def_groupe[numero].data == NULL)
    {

        if (l == -1)
        {

            l = find_input_link(numero, i);

            while (l != -1)
            {
                if (strcmp(liaison[l].nom, "-1") == 0)
                {
                    Gpe_pred = liaison[l].depart;
                    mode = 1;

                }
                if (strcmp(liaison[l].nom, "-2") == 0)
                {
                    Gpe_pred = liaison[l].depart;
                    mode = 2;

                }


                i++;
                l = find_input_link(numero, i);

            }
        }

        if (Gpe_pred == -1)
        {
            printf("groupe  1 non trouve dans groupe %d\n", numero);
            exit(0);
        }




        def_groupe[numero].data = malloc(sizeof(int));
        /*memcpy(def_groupe[numero].data,groupes,3*sizeof(int)); */
        *(int *) def_groupe[numero].data = Gpe_pred;

        def_groupe[numero].ext = malloc(sizeof(float));
        /*memcpy(def_groupe[numero].ext,groupes,3*sizeof(int)); */
        *(float *) def_groupe[numero].ext = 0.;

        neurone[def_groupe[numero].premier_ele].d = 0.;



    }
    else
    {

        Gpe_pred = *(int *) def_groupe[numero].data;
        val = *(float *) def_groupe[numero].ext;

    }

    if (mode == 1)
    {
      if (isequal(neurone[def_groupe[numero].premier_ele].d, 0))
        {
            for (i = 0; i < def_groupe[numero].nbre; i++)
            {

                neurone[def_groupe[numero].premier_ele + i].s1 =
                    neurone[def_groupe[Gpe_pred].premier_ele + i].s2;
                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[numero].premier_ele + i].s1;

            }
            neurone[def_groupe[numero].premier_ele].d = 1.;
        }
        else
        {
            for (i = 0; i < def_groupe[numero].nbre; i++)
            {

                neurone[def_groupe[numero].premier_ele + i].s1 =
                    neurone[def_groupe[Gpe_pred].premier_ele + i].s1;
                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[numero].premier_ele + i].s1;

            }
            neurone[def_groupe[numero].premier_ele].d = 0.;
        }

    }
    else
    {
        if (def_groupe[Gpe_pred].type == No_Pyramidal)
        {

	  if (isequal(neurone[def_groupe[numero].premier_ele].d, 0.))
            {


                for (i = 0; i < def_groupe[numero].nbre; i++)
                {

                    sum =
                        neurone[def_groupe[Gpe_pred].premier_ele + j].s2 +
                        neurone[def_groupe[Gpe_pred].premier_ele + j + 1].s2 +
                        neurone[def_groupe[Gpe_pred].premier_ele + 2 + j].s2;

                    if (sum > max)
                    {
                        max = sum;
                        pos = i;
                    }

                    j += 3;
                }
                i = pos;

                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[numero].premier_ele + i].s1 = max;

                neurone[def_groupe[numero].premier_ele].d = 1.;

            }
            else
            {
                for (i = 0; i < def_groupe[numero].nbre; i++)
                {

                    neurone[def_groupe[numero].premier_ele + i].s1 = 0.;
                    neurone[def_groupe[numero].premier_ele + i].s =
                        neurone[def_groupe[numero].premier_ele + i].s2 =
                        neurone[def_groupe[numero].premier_ele + i].s1;

                }
                neurone[def_groupe[numero].premier_ele].d = 0.;
            }


        }
        else if (isequal(neurone[def_groupe[numero].premier_ele].d, 0))
        {

            for (i = 0; i < def_groupe[numero].nbre; i++)
            {


                if (alpha > 0.)
                {

                    neurone[def_groupe[numero].premier_ele + i].s1 =
                        val +
                        alpha *
                        (neurone[def_groupe[Gpe_pred].premier_ele + i].s1 -
                         val);
                    val = neurone[def_groupe[numero].premier_ele + i].s1;

                }
                else
                {
                    neurone[def_groupe[numero].premier_ele + i].s1 =
                        neurone[def_groupe[Gpe_pred].premier_ele + i].s1;
                }

                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[numero].premier_ele + i].s1;

            }

            neurone[def_groupe[numero].premier_ele].d = 1.;
        }
        else
        {

            for (i = 0; i < def_groupe[numero].nbre; i++)
            {


                neurone[def_groupe[numero].premier_ele + i].s =
                    neurone[def_groupe[numero].premier_ele + i].s2 =
                    neurone[def_groupe[numero].premier_ele + i].s1 = -999999.;

            }
            neurone[def_groupe[numero].premier_ele].d = 0.;



        }

    }
/*	printf(" store position %f \n",neurone[def_groupe[numero].premier_ele].s);*/
    *(float *) def_groupe[numero].ext = val;
}
