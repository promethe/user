/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \file 
   \brief

   CHECK DOC !!

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: N.Cuperlier
   - description: fonction de realisation du mouvement
   - date: 01/03/2005

   Theoritical description:
   - \f$  LaTeX equation: none \f$

   Description:
   Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
   partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
   On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
   Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
   a la norme de ce mouvement (1 veut dire 30cm)
   L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


   Macro:
   - PI
   - PI_2

   Local variables:
   -sortie_odometrie parametres_odometrie


   Global variables:
   - boolean EMISSION_ROBOT
   - int USE_SIMULATION
   - int USE_ROBOT

   Internal Tools:
   - move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>

/*#define DEBUG*/
#define DEFAULT_NORME 6000
#define SPEED 5
#define MAX_ROT_SPEED 80
#define MAX_D_PHI 0.07
#define SPEED_COEFF 0.8

typedef struct My_Data_f_pseudo_movement_simple
{
  int gpe_derive;
  int type_lecture;
} My_Data_f_pseudo_movement_simple;

void function_pseudo_movement_simple(int gpe)
{
  My_Data_f_pseudo_movement_simple *my_data = NULL;
  int gpe_nf, gpe_derive, gpe_linear_speed, gpe_rotational_speed;
  int i, l, type_lecture = 0.;
  char param[32];
  int indmax = -1;
  int *indsmax = NULL;
  float min;

  if (def_groupe[gpe].data == NULL)
  {

    gpe_nf = gpe_derive = gpe_linear_speed = gpe_rotational_speed = -2;
    i = 0;
    l = find_input_link(gpe, i);
    while (l != -1)
    {
      if (strcmp(liaison[l].nom, "DP") == 0)
      {
	gpe_derive = liaison[l].depart;
      }
      if (prom_getopt(liaison[l].nom, "-t", param) > 0)
      {
	type_lecture = atoi(param);
      }

      i++;
      l = find_input_link(gpe, i);
    }
    if (gpe_derive == -2)
    {
      EXIT_ON_ERROR("Il faut le  groupe avec  <DP> au moins sur les liens\n");
    }
    my_data = (My_Data_f_pseudo_movement_simple *) malloc(sizeof(My_Data_f_pseudo_movement_simple));
    my_data->gpe_derive = gpe_derive;
    my_data->type_lecture = type_lecture;
    def_groupe[gpe].data = (My_Data_f_pseudo_movement_simple *) my_data;
  }
  else
  {
    my_data = (My_Data_f_pseudo_movement_simple *) def_groupe[gpe].data;
    gpe_derive = my_data->gpe_derive;
    type_lecture = my_data->type_lecture;
  }

/* lecture de la derivee*/

/*d_phi=neurone[deb_e3+def_groupe[gpe_derive].nbre/2 -1].s1+neurone[deb_e3+def_groupe[gpe_derive].nbre/2 +1].s1;
  d_phi=d_phi/2;
  delta_angle=d_phi;

  printf("f_phi :%f \n", delta_angle);
  if(robot->robot_type==1||robot->robot_type==2)
  {
  robot_go_by_speed(robot,p[0],p[1]);
  }
  else if(robot->robot_type==3)
  {
  speed_linear=0.;
  printf("%f vs %f \n", delta_angle,delta_angle_old);
  robot_set_velocity(robot,speed_linear,0.,-delta_angle,1);
  delta_angle_old=delta_angle;
		       
  }
*/

  /* le zero est en nbre/2 -1 */


  indsmax = (int *) malloc(sizeof(int) * def_groupe[gpe_derive].nbre);
  if (indsmax == NULL)
  {
    EXIT_ON_ERROR("probleme de malloc ");
  }
  for (i = 0; i < def_groupe[gpe_derive].nbre; i++)
  {
    indsmax[i] = -1000;
  }

  if (type_lecture == 0)
    /*determine le max dans le nf vers lequel on se dirige (ie les passages de neg vers pos dans la derive) */
    /*le max en quetion est choisis comme la plus proche */
  {
    int cpt = 0;
    /*oncherche les max */
    for (i = 1; i < def_groupe[gpe_derive].nbre - 1; i++)
    {
      if (neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1 > 0. && neurone[i + def_groupe[gpe_derive].premier_ele + 1].s1 <= 0.)
      {
	indsmax[cpt] = i;
	/*printf("%f   -   %f    =>   max:%d\n",neurone[i+def_groupe[gpe_derive].premier_ele].s1,neurone[i+def_groupe[gpe_derive].premier_ele+1].s1,i); */
	cpt++;
      }
    }
    /*on detemine les plus proches */
    min = 1000.;
    for (i = 0; i < cpt; i++)
    {
      /*il y  un petit strabisme de 1 neurone */
      if (fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]) < min)
      {
	indmax = indsmax[i];
	min = fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]);
      }
    }

  }
  else if (type_lecture == 1)
    /*Cette foix le max est le plus proche dans la direction donne par la derive */
  {
    if (neurone
	[def_groupe[gpe_derive].nbre / 2 +
	 def_groupe[gpe_derive].premier_ele].s1 > 0.)
    {

      for (i = def_groupe[gpe_derive].nbre / 2;
	   i < def_groupe[gpe_derive].nbre - 1; i++)
      {
	if (neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1 >
	    0.
	    && neurone[i + 1 +
		       def_groupe[gpe_derive].premier_ele].s1 <= 0.)
	{
	  indmax = i;
	  break;
	}
      }
    }
    else
    {
      for (i = def_groupe[gpe_derive].nbre / 2; i > 0; i--)
      {
	if (neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1 >
	    0.
	    && neurone[i + 1 +
		       def_groupe[gpe_derive].premier_ele].s1 <= 0.)
	{
	  indmax = i;
	  break;
	}
      }
    }                       /*   printf("no developpe\n");exit(0); */
  }

  /* indmax est l'indice du max sur le champs NF : ce qui donne la vitesse angulaire de base */

  for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; i++)
    neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
  neurone[def_groupe[gpe].premier_ele + indmax].s = neurone[def_groupe[gpe].premier_ele + indmax].s1 = neurone[def_groupe[gpe].premier_ele + indmax].s2 = 1.0;
}
