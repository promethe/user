/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Fonction utilisee uniquement en simulation!
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]

 Le neurone correspondant a la direction du mouvement reelement effectue prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)

 Pour le moment on prend directement le maximum, dans la suite la sortie de cette boite pourrait lire une boite du genre f_d_phi mais necessite des modifs profonde: utiliser une autre fonction que move_via_mvector_given_speed
qui devra utiliser des commande D,X,X X teant obtenu a partir de d_phi*cste ...
Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define DEBUG*/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>


void function_navigation_movement_simule(int numero)
{
   int gpe_mvt = -1, gpe_compass = -1, gpe_speed = -1;
    int i, n, l;
    int deb, size, increment;
    int deb_e, taille_groupe_e, taille_groupe_e2;
    int xmax2, ymax2, num_neur;
    Robot *robby;
    int pos = -1;               /*,decalage,correspondant; */
    double beta_tot = 0, teta = 0 /* NC: [-Pi;+Pi] */ , norme = 0., speed = 1.;
    float mvt_r /*NC: en degre [-180;180[ */ , mvt_d /*NC en radians [-PI;PI[ */ , orientation_r /*NC: [-PI; PI[ */ , orientation_d /*NC: [-180; 180[ */ , max, scale;

    if (def_groupe[numero].data == NULL)
    {
        def_groupe[numero].data = (void *) (&parametres_odometrie);
    }
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("Creating the movement \n");
#endif
    /*Recherche des boites en entrees... */

    l = 0;
    i = find_input_link(numero, l);    /* recherche le 1er lien */
    while (i != -1)
    {
       if (strcmp(liaison[i].nom, "compass") == 0)
	  gpe_compass = liaison[i].depart;
       if (strcmp(liaison[i].nom, "mvt") == 0)
	    gpe_mvt = liaison[i].depart;
       if (strcmp(liaison[i].nom, "speed") == 0)
	  gpe_speed = liaison[i].depart;
       
	 l++;
	 i = find_input_link(numero, l);    /* recherche le 1er lien */
	 
    }

    if (gpe_compass == -1 || gpe_mvt == -1)
    {
       fprintf(stderr, "f_nav_mvt_simule: missing compass or mvt input link\n");
       exit(1);
    }

    if( gpe_speed >= 0)
    {
       speed = neurone[def_groupe[gpe_speed].premier_ele].s1;
#ifdef DEBUG
       printf("f_nav_mvt_simule : speed = %f\n", speed);
#endif

    }

    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    size = xmax2 * ymax2;

    taille_groupe_e = def_groupe[gpe_mvt].nbre;
    taille_groupe_e2 = def_groupe[gpe_mvt].taillex * def_groupe[gpe_mvt].tailley;
    scale = 2 * PI / (float) taille_groupe_e2;  /*echelle, depend du nombre de neurone du groupe en entree */
    increment = taille_groupe_e / taille_groupe_e2;
    deb_e = def_groupe[gpe_mvt].premier_ele;
    /*NC: Recupere l'orientation courante (absolue) */
    if (gpe_compass != -1)      /* test si direction absolue boussole ,convention valeur du groupe -PI PI */
    {
        orientation_r = neurone[def_groupe[gpe_compass].premier_ele].s;
        orientation_d = orientation_r * 180 / (PI); /*entre -180 et 180 */
    }
    else
    {
        orientation_r = 0.;
        orientation_d = 0.;
    }
    /* tous les neurones de sortie a 0 ... */
    for (i = 0; i < size; i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

    max = -999999.;
    pos = -1;


    /*devrait etre adapte pour plusieurs robots : par exemple, le lien d'entree */
    /*devrait porter le nom du robot concerne  */
    robby = robot_get_first_robot();

    if (robby->robot_type == USE_SIMULATION)
    {
        /*Lecture directe du NF et non de sa derivee contrairement a la fonction du robot reel */
        for (n = deb_e + increment - 1; n < deb_e + def_groupe[gpe_mvt].taillex * def_groupe[gpe_mvt].tailley * increment; n += increment)
        {
            if (neurone[n].s1 > max)
            {
                max = neurone[n].s1;
                pos = (n - deb_e) / increment;
#ifdef DEBUG
                printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
            }
        }
#ifdef DEBUG
        printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
        /*Si pas d'activite superieure a 0.1 alors pas de mouvement */
        if (max - .0007 < 0 || pos == -1)
        {
#ifdef DEBUG
            printf("\nActivity is to low (max=%f) : No movement !\n", max);
#endif
            parametres_odometrie.norme = 0;
            parametres_odometrie.beta_tot = 0;
            parametres_odometrie.teta = teta;
            return;
        }
        /*sinon calcul de la norme desiree */

        /*calcul de l'angle du vecteur mouvement souhaite pour la simul only */
        mvt_r = pos * scale - PI;
        mvt_d = mvt_r * 180 / PI;
             /*-180;180*/
        /*NC: mvt_x est en absolue pour le moment */
#ifdef DEBUG
        printf("order mvt (absolute) of :%f radians or %f degre \n", mvt_r, mvt_d);
#endif



/*decommenter si mouvement en entree sont en abslue..*/
        /*calcul en relatif */
        mvt_r = mvt_r - orientation_r;
        if (mvt_r >= PI)
            mvt_r = mvt_r - 2 * PI;
        if (mvt_r < -PI)
            mvt_r = mvt_r + 2 * PI;
        mvt_d = mvt_r * 180 / PI;
            /*-180;180*/


        norme = 30000.;
        /*NC: compute the new direction in absolute */
#ifdef DEBUG
        printf("The actual direction given by the compass is : %f �\n", orientation_d);
        printf("F_nav_mvt teta av mise a jour :%f\n", teta);
        printf("F_nav_mvt beta_tot av mise a jour :%f\n", beta_tot);
#endif
        /*mouvement relatif = mvt realise en simul */
        beta_tot = mvt_r;
        teta = mvt_r;           /*mouvement relatif pour la mise a jour du groupe f_orientation */
#ifdef DEBUG
        printf("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
#endif
        /*calcul en absolue, retour en absolue en attendant modification de la fonction de deplacement en simule... */
        mvt_r = mvt_r + orientation_r;
        if (mvt_r >= PI)
            mvt_r = mvt_r - 2 * PI;
        if (mvt_r < -PI)
            mvt_r = mvt_r + 2 * PI;
        mvt_d = mvt_r * 180 / PI;
        turn_simul(mvt_r, speed);  /*appel du mouvement simule. La direction est en absolu - voir env_simul */
#ifdef DEBUG
        printf("Robot's direction is now :%f, norm= %f \n", mvt_r, norme);
#endif
        /*NC: recherche du neurone coreespondant et affectation */
        num_neur = (int) ((mvt_d / 360.0) * xmax2 * ymax2 + (xmax2 * ymax2 / 2));

        if (num_neur >= xmax2 * ymax2)
            num_neur = 0;
        num_neur += deb;
#ifdef DEBUG
        printf("Neurone :%d, angle= %f, act: %f \n", num_neur - deb, mvt_r,
               norme / 30000);
#endif
        neurone[num_neur].s = neurone[num_neur].s1 = neurone[num_neur].s2 = (norme / 30000);    /* norme:  1=30cm */

        /*Sauvegarde des donnees odmetriques */
        parametres_odometrie.norme = norme; /*longueur du vecteur deplacement realise */
        parametres_odometrie.beta_tot = beta_tot;   /*angle en relatif, dans le futur pourra etre equivalent au chemin              integre ou a la valeur de l'orientation... A definir */
        parametres_odometrie.teta = teta;   /*angle en relatif */
        def_groupe[numero].data = (void *) (&parametres_odometrie);
    }
    else
    {
        printf
            ("\nHum...you should check your .dev : check the set-up for simulation (robot type = 4)\n");
    }
}
