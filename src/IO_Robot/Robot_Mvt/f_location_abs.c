/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_location_abs.c 
    \brief 

    Author: Julien Hirel
    Created: 18/02/2009
    Modified:
    - author: 
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
    Gets the location of the robot in an absolute reference frame.

    Macro:

    Local varirables:

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    -none

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>


void new_location_abs(int gpe)
{
   /* Checking group size */
   if (def_groupe[gpe].nbre != 3)
   {
      fprintf(stderr, "ERROR in new_location_abs(%s): group size must be 3 neurons\n", def_groupe[gpe].no_name);
      exit(1);
   }
}

void function_location_abs(int gpe)
{
   int first = def_groupe[gpe].premier_ele;
   Robot *robot = robot_get_first_robot();

   /* Updates information about the current robot location */
   robot_get_location_abs(robot);

   dprints("f_location_abs(%s): robot location => posx = %f, posy = %f, orientation = %f\n", def_groupe[gpe].no_name, robot->posx_abs, robot->posy_abs, robot->orientation_abs);

   /* Current robot location */
   neurone[first].s = neurone[first].s1 = neurone[first].s2 = robot->posx_abs;
   neurone[first+1].s = neurone[first+1].s1 = neurone[first+1].s2 = robot->posy_abs;
   
   /* Current robot orientation */
   neurone[first+2].s = neurone[first+2].s1 = neurone[first+2].s2 = robot->orientation_abs;
}
