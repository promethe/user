/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/IO_Robot.h>
#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include <stdlib.h>

float booleanToFloat(char *str) {
    if ( strcmp(str,"True") == 0 )
        return 1.;
    else
        return 0.;
}

void applyValues(int gpe, int n, float val1, float val2, float val3) {
    int premier_ele;
    
    if ( def_groupe[gpe].nbre <  n ) {
        printf("ERROR dans %s : pas assez de neurones, il en faut 3 minimum\n",__FUNCTION__);
        exit(0);
    }    
    premier_ele = def_groupe[gpe].premier_ele;
    
    if (n>0)
        neurone[premier_ele].s=neurone[premier_ele].s1=neurone[premier_ele].s2=val1;
    if (n>1)
        neurone[premier_ele+1].s=neurone[premier_ele+1].s1=neurone[premier_ele+1].s2=val2;
    if (n>2)
        neurone[premier_ele+2].s=neurone[premier_ele+2].s1=neurone[premier_ele+2].s2=val3;
}

void function_test_robubox(int gpe)
{
    
    static ClientRobubox * robot = NULL;
    char msg[1024],reponse[1024];
    float val1=0.,val2=0.,val3=0.;
    char str1[1024],str2[1024],str3[1024];
    char cmd[1024];
    int i,j;
    int type=-1,n=0;
    
    robot = clientRobubox_get_first_clientRobubox();
    
    i=0;
    while( (j=find_input_link(gpe,i)) != -1 ) {
        if(strcmp(liaison[j].nom,"loc")==0)
            type=0;
        else if(strcmp(liaison[j].nom,"odo")==0)
            type=1;
        else if(strcmp(liaison[j].nom,"leds")==0)
            type=2;
        else if(strcmp(liaison[j].nom,"switchs")==0)
            type=3;
        else if(strcmp(liaison[j].nom,"us")==0)
            type=4;
        else if(strcmp(liaison[j].nom,"batt")==0)
            type=5;
        else if(strcmp(liaison[j].nom,"bmp")==0)
            type=6;
        else  if(strcmp(liaison[j].nom,"motorsOn")==0)
            type=7;
        else  if(strcmp(liaison[j].nom,"drive")==0)
            type=8;
        else  if(strcmp(liaison[j].nom,"resetOdo")==0)
            type=9;
        
        i++;	
    }
    
    switch ( type ) {
        case 0:
            sprintf(msg,"%s","Loc");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %f %f %f",cmd,&val1,&val2,&val3);
            
            val1/=100.;
            val2/=100.;
            val3/=100.;
            
            n=3;
            break;
        case 1:
            sprintf(msg,"%s","Odo");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %f %f %f",cmd,&val1,&val2,&val3);
            
            val1/=100.;
            val2/=100.;
            val3/=100.;
            
            n=3;
            break;
        case 2:
            sprintf(msg,"%s","GetDO");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %s %s %s",cmd,str1,str2,str3);
            
            val1=booleanToFloat(str1);
            val2=booleanToFloat(str2);
            val3=booleanToFloat(str3);
            n=3;
            break;
        case 3:
            sprintf(msg,"%s","GetDI");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %s %s %s",cmd,str1,str2,str3);
            
            val1=booleanToFloat(str1);
            val2=booleanToFloat(str2);
            val3=booleanToFloat(str3);
            n=3;
            break;
        case 4:
            sprintf(msg,"%s","GetUS");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %f %f",cmd,&val1,&val2);
            
            val1/=5.;
            val2/=5.;
            n=2;
            break;
        case 5:
            sprintf(msg,"%s","GetBattery");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %f",cmd,&val1);
            printf("--Battery: %f\n",val1);
            val1/=100.;
            n=1;
            break;
        case 6:
            sprintf(msg,"%s","GetBumpers");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %s %s",cmd,str1,str2);
            
            val1=booleanToFloat(str1);
            val2=booleanToFloat(str2);
            n=2;
            break;
        case 7:
            sprintf(msg,"%s","Motors");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %s",cmd,str1);
            
            val1=booleanToFloat(str1);
            n=1;
            break;
        case 8:
            sprintf(msg,"%s","Drive 0 0");
            clientRobubox_transmit_receive(robot,msg,reponse);
            sscanf(reponse,"%s %s",cmd,str1);
            
            val1=booleanToFloat(str1);
            n=1;
            break;
        case 9:
           if(neurone[def_groupe[liaison[find_input_link (gpe, 0)].depart].premier_ele].s1 > 0.5)
           {
              sprintf(msg,"%s","ResetOdo");
              clientRobubox_transmit_receive(robot,msg,reponse);
              sscanf(reponse,"%s",str1);

              PRINT_WARNING("> reset odo : %s \n", str1);
              val1 = 1. ;
           }
           else
            val1 = 0. ;
            break;
        default:
            printf("Erreur de nom sur le lien dans %s ?!\n",__FUNCTION__);
            exit(0);
            break;
    }
    
    applyValues(gpe,n,val1,val2,val3);
    

}
