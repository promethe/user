/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file 
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <time.h>

#include <string.h>
#include <Kernel_Function/find_input_link.h>
/*#define DEBUG*/
static int flag;
static float tps;

void function_random_mvt(int numero)
{
    float r = 0.;
    int t = 0.;

    tps += 1;

    printf("Random mvt\n");

    if (flag < 1.)
    {
        /*srand48(time(NULL));flag=1.; */
    }
    neurone[def_groupe[numero].premier_ele].s =
        neurone[def_groupe[numero].premier_ele].s1 =
        neurone[def_groupe[numero].premier_ele].s2 = 0.;
    neurone[def_groupe[numero].premier_ele + 1].s =
        neurone[def_groupe[numero].premier_ele + 1].s1 =
        neurone[def_groupe[numero].premier_ele + 1].s2 = 0.;

    t = 10. * (float) drand48();
    r = (float) t *0.1;
    printf("r*0.01=%f\n", r);

    if (tps < def_groupe[numero].seuil)
    {
        if ((t % 2) == 0)
        {

            neurone[def_groupe[numero].premier_ele].s =
                neurone[def_groupe[numero].premier_ele].s1 =
                neurone[def_groupe[numero].premier_ele].s2 = 1.;
            printf("random=droite...val= %f\n",
                   neurone[def_groupe[numero].premier_ele].s1);
        }
        else
        {


            neurone[def_groupe[numero].premier_ele + 1].s =
                neurone[def_groupe[numero].premier_ele + 1].s1 =
                neurone[def_groupe[numero].premier_ele + 1].s2 = 1.;
            printf("random=gauche... val= %f\n",
                   neurone[def_groupe[numero].premier_ele + 1].s1);
        }
    }
}
