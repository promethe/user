/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_epsilon_greedy_action f_epsilon_greedy_action
 

\section Modified 
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  



\section Description




\section Macro
-none

\section Local variables
-none

\section Global variables
-none


\section Internal Tools
-none


\section External Tools
-tools/Vision/affiche_pdv()

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/


#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <unistd.h>
#include <time.h>
/*#define DEBUG*/

#define RANDOM

void function_epsilon_greedy_action(int numero)
{
    static int tps = 0;
    int i = 0, winner = -1;
    int r = 0;
    float max = 0.;
    int t = 0;
    int first_neuron = def_groupe[numero].premier_ele;
    int last_neuron = def_groupe[numero].nbre;
    type_coeff *synapse;
    FILE *fd, *ur;
    int nb_case = (int) (def_groupe[numero].learning_rate);

    if (nb_case == 0) 
    nb_case = 10;

    cprints("-------------------Greedy action--------------------------\n");
/*		#ifdef RANDOM*/
    ur = fopen("/dev/urandom", "r");
    fd = fopen("positionX", "a");
/*		#endif*/
    if (def_groupe[numero].data == NULL)
    {

        srand48(time(NULL));
        def_groupe[numero].data = malloc(sizeof(char));
        *(char *) def_groupe[numero].data = '4';
    }
    /*#ifdef RANDOM
     */ r = fgetc(ur);
    cprints("r=%d", r);
    /*#endif
     */ for (i = first_neuron; i < first_neuron + last_neuron; i++)
    {

        synapse = neurone[i].coeff;

        neurone[i].s = neurone[i].s2 = neurone[i].s1 = 0.;

        while (synapse != NULL)
        {

            neurone[i].s1 += synapse->val * neurone[synapse->entree].s2;
            synapse = synapse->s;
        }
        if (neurone[i].s1 > max)
        {
            max = neurone[i].s1;
            winner = i;
        }
    }

    if (tps >= 4 * nb_case)
    {
        if ((max > def_groupe[numero].seuil) && max > 0.
#ifdef RANDOM
            && r > (int) (255. * 0.05)
#endif
            )
        {


            cprints("max=%f\n", max);
            for (i = first_neuron; i < first_neuron + last_neuron; i++)
            {

                neurone[i].s1 = neurone[i].s2 = neurone[i].s = 0.;
                if (i == winner)
                    neurone[i].s1 = neurone[i].s2 = neurone[i].s = max;
            }
#ifdef RANDOM
            fprintf(fd, "0. ");
#endif
        }
        else
        {


            /*              #ifdef RANDOM
             */ cprints("t=%d max=%f\n", t, max);
            t = fgetc(ur);
            if (t > 127)
            {

                neurone[first_neuron].s1 = neurone[first_neuron].s2 =
                    neurone[first_neuron].s = 0.;
                neurone[first_neuron + 1].s1 = neurone[first_neuron + 1].s2 =
                    neurone[first_neuron + 1].s = 1.;
            }
            else
            {
                neurone[first_neuron].s1 = neurone[first_neuron].s2 =
                    neurone[first_neuron].s = 1.;
                neurone[first_neuron + 1].s1 = neurone[first_neuron + 1].s2 =
                    neurone[first_neuron + 1].s = 0.;


            }

            fprintf(fd, "1. ");
            /*              #endif  
             */ }
    }
    else if (tps > 2 * nb_case)
    {
        neurone[first_neuron].s1 = neurone[first_neuron].s2 =
            neurone[first_neuron].s = 0.;
        neurone[first_neuron + 1].s1 = neurone[first_neuron + 1].s2 =
            neurone[first_neuron + 1].s = 1.;
    }
    else if (tps >= 0)
    {
        neurone[first_neuron].s1 = neurone[first_neuron].s2 =
            neurone[first_neuron].s = 1.;
        neurone[first_neuron + 1].s1 = neurone[first_neuron + 1].s2 =
            neurone[first_neuron + 1].s = 0.;
    }

/*		#ifdef RANDOM*/
    fclose(fd);
    fclose(ur);
/*		#endif*/
    tps++;
    cprints("fin %s\n", __FUNCTION__);
}
