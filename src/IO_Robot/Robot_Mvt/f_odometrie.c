/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
\ingroup libIO_Robot
\defgroup f_odometrie f_odometrie
\brief 


\section Author 
-Name: Julien Hirel
-Created: 18/02/2009

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description
Gets the distances traveled by the robot, based on odometric information. The reset of the origin of the reference frame is not implemented yet!!
	The function returns 4 neurons: 
 * 1st neuron: linear distance traveled from the last iteration
 * 2nd neuron: angular distance traveled from the last iteration
 * 3rt neuron: linear distance cumulated from the starting of the experiment (no reset implemented yet)
 * 4rt neuron: angular distance cumulated from the starting of the experiment (no reset implemented yet)


\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none 

\section Links
-none

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 */


/*#define DEBUG*/

#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>
#include <net_message_debug_dist.h>

typedef struct mydata_odo
{
   int gpe_reset;
}mydata_odo;
void new_odometrie(int gpe)
{
   int first = def_groupe[gpe].premier_ele;

   int i, l;
   mydata_odo *my_data = NULL;


   if (def_groupe[gpe].data == NULL)
   {
      my_data = (mydata_odo*) malloc(sizeof(mydata_odo));
      if (my_data == NULL)
         EXIT_ON_ERROR("ERROR in new_odo(%s): malloc failed for data\n", def_groupe[gpe].no_name);
      my_data->gpe_reset = -1;
      l = 0;
      i = find_input_link(gpe, l);    /* recherche les liens en entree */
      while (i != -1)
      {
         if (strcmp(liaison[i].nom, "reset") == 0)
            my_data->gpe_reset = liaison[i].depart;

         l++;
         i = find_input_link(gpe, l);
      }

   }

   def_groupe[gpe].data = my_data;

   /* Verification du nombre de neurones */
   if (def_groupe[gpe].nbre != 4)
      EXIT_ON_ERROR("group %s size must be 4 neurons",def_groupe[gpe].no_name);

   /* Initialisation des valeurs */
   neurone[first].s =
         neurone[first].s1 =
               neurone[first].s2 = 0.;

   neurone[first+1].s =
         neurone[first+1].s1 =
               neurone[first+1].s2 = 0.;

   neurone[first+2].s =
         neurone[first+2].s1 =
               neurone[first+2].s2 = 0.;

   neurone[first+3].s =
         neurone[first+3].s1 =
               neurone[first+3].s2 = 0.;
}

void function_odometrie(int gpe)
{
   int first = def_groupe[gpe].premier_ele;
   int gpe_reset = ((mydata_odo *) def_groupe[gpe].data)->gpe_reset;
   Robot *robot = robot_get_first_robot();

   robot_get_odometry(robot);
   if (gpe_reset >= 0 && neurone[def_groupe[gpe_reset].premier_ele].s1 > 0.5)
   {
      dprints("Reseting odometrie\n");
      neurone[first].s = neurone[first].s1 = neurone[first].s2 = 0.;
      neurone[first+1].s = neurone[first+1].s1 = neurone[first+1].s2 = 0.;
      neurone[first+2].s = neurone[first+2].s1 = neurone[first+2].s2 = 0.;
      neurone[first+3].s = neurone[first+3].s1 = neurone[first+3].s2 = 0.;
      robot_reset_odo( robot);
      robot->distance = 0.;
      robot->angle = 0.;

   }
   dprints("f_odometrie(%s): robot odometry =>\ndist = %f, angle = %f\ndist_cumul = %f, angle_cumul = %f\n\n", def_groupe[gpe].no_name, robot->distance, robot->angle, neurone[first + 2].s, neurone[first + 3].s);

      /* Affichage de la valeur d'odometrie "distance" sur le premier neurone du groupe */
      neurone[first].s = neurone[first].s1 = neurone[first].s2 = robot->distance;
      /* Affichage de la valeur d'odometrie "angle" sur le deuxieme neurone du groupe */
      neurone[first + 1].s = neurone[first + 1].s1 = neurone[first + 1].s2 = robot->angle;
      /* Affichage de la valeur d'odometrie "distance" en cumule sur le troisieme neurone du groupe */
      neurone[first + 2].s = neurone[first + 2].s1 = neurone[first + 2].s2 = neurone[first + 2].s + robot->distance;
      /* Affichage de la valeur d'odometrie "angle" en cumule sur le quatrieme neurone du groupe */
      neurone[first + 3].s = neurone[first + 3].s1 = neurone[first + 3].s2 = neurone[first + 3].s + robot->angle;


   (void) gpe_reset;
}
