/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_navigation_movement_robot.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Fonction g�n�rant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degr�...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <public_tools/erreur.h>
#include "tools/include/move_via_mvector_given_speed.h"
#include <public_tools/IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>

#define DEBUG
#define DEFAULT_NORME 6000
#define SPEED 5
#define MAX_ROT_SPEED 30
#define MAX_D_PHI 0.07
#define SPEED_COEFF 0.8


/*on n'effectue pas reellemment le mvt!*/



void function_navigation_movement_simule2(int numero)
{
float delta_angle=0.,d_phi=0.; /*variable pour la lecture du NF*/ 
int p[2];
  int	gpe_entree1, gpe_entree2=-1,gpe_entree3,gpe_entree4;
  int	i, n,pos_rel,max_neg,max_pos,pos_delta_angle;
  int	deb, size, increment;
  int	deb_e, taille_groupe_e, taille_groupe_e2,deb_e3=-1;
  int	xmax2,ymax2,num_neur;
  int	pos=-1;/*,decalage,correspondant;*/
  double beta_tot=0, teta=0 /* NC: [-Pi;+Pi]*/, norme=30000.;
  float	mvt_r /*NC: en degre [-180;180[*/, orientation_r/*NC: [-PI; PI[*/,orientation_d/*NC: [-180; 180[*/,max,scale,angle_rel;
  float speed_cste =4.,rot_speed=50.,max_dphi_angle_pos,max_dphi_angle_neg;
  static float speed_0;

if(def_groupe[numero].data==NULL){
 def_groupe[numero].data=(void*)(&parametres_odometrie);
  gpe_entree4=trouver_entree(numero, "speed_cst");
  if(gpe_entree4!=-1){
  for (n=0;n<4;n++){
  i=find_input_link(numero,n);
  if(strcmp(liaison[i].nom,"speed_cst")==0) break;
  }
   speed_0=neurone[def_groupe[gpe_entree4].premier_ele].s2*liaison[i].norme;
  printf("=====================vitesse_nulle: %f===================\n",speed_0);
  }
}
printf("-------------------------------------------------\n");
  printf("Creating the movement \n");
 /*Recherche des boites en entr�es...*/
  gpe_entree3=trouver_entree(numero,"DP");
  gpe_entree1=trouver_entree(numero,"NF");
  gpe_entree2=trouver_entree(numero, "compass");
 deb_e3=def_groupe[gpe_entree3].premier_ele; /*ajout 13mai, avait disparu ?????*/

  if(gpe_entree2==1)printf("WARNING: f_nav_mvt: no f_orientation  find !\n");
#ifdef DEBUG
	printf("f_nav_mvt: NF:  %d; f_orientation: %d, DP %d!\n",gpe_entree1,gpe_entree2,gpe_entree3);
	printf("f_nav_mvt: f_orientation  find at group: %d\n",gpe_entree2);
#endif
  deb=def_groupe[numero].premier_ele;
  xmax2=def_groupe[numero].taillex;
  ymax2=def_groupe[numero].tailley;
  size = xmax2*ymax2;

  taille_groupe_e = def_groupe[gpe_entree1].nbre;
  taille_groupe_e2 = def_groupe[gpe_entree1].taillex*def_groupe[gpe_entree1].tailley;
  scale=2*PI/(float)taille_groupe_e2; /*echelle, depend du nombre de neurone du groupe en entr�e*/ 
  increment=taille_groupe_e/taille_groupe_e2;
  deb_e = def_groupe[gpe_entree1].premier_ele;

  /*NC: Recupere l'orientation courante (absolue)*/
  if(gpe_entree2!=-1)	/* test si direction absolue boussole ,convention valeur du groupe -PI PI*/{
    orientation_r = neurone[def_groupe[gpe_entree2].premier_ele].s;
    orientation_d=orientation_r*180/(PI); /*entre -180 et 180*/ 
  }else{
    orientation_r= 0.;
    orientation_d=0.;
}
  /* tous les neurones de sortie a 0 ... */
  for (i=0; i<size; i++)
    neurone[deb+i].s=neurone[deb+i].s1=neurone[deb+i].s2=0.;

  /*recherche du max sur NF*/
  max=-999999.;
  pos=-1;
  for (n=deb_e;n<deb_e+def_groupe[gpe_entree1].taillex*def_groupe[gpe_entree1].tailley; n++)
    {
      if(neurone[n].s>max)
	{
	  max=neurone[n].s;
	  pos=(n-deb_e)/increment;
	  pos_rel=(size/2)-pos; /*decalage par rapport a la position courante*/
	  angle_rel=pos_rel*(2*M_PI)/size; /*delta relatif entre l'angle desire et l'angle de l'orientation courante*/

	}
    }
 #ifdef DEBUG
	  printf("Max:%f Num:%d Pos:%d, pos_rel:%d angle_rel:%f \n", max, n, pos,  pos_rel ,  angle_rel );
#endif
	 /*recherche des 2 max pos et neg de D_phi*/
	 max_dphi_angle_neg=9999999.;
max_dphi_angle_pos=-999999.;
  pos=-1;
  for (n=deb_e3;n<deb_e3+def_groupe[gpe_entree3].taillex*def_groupe[gpe_entree3].tailley; n++)
    {
      if(neurone[n].s>max_dphi_angle_pos)
	{
	  max_dphi_angle_pos=neurone[n].s;
	  max_pos=(n-deb_e3)/increment;

	}
	 else   if(neurone[n].s<max_dphi_angle_neg)
	{
	  max_dphi_angle_neg=neurone[n].s;
	  max_neg=(n-deb_e3)/increment;
	  
	}
    }
	 	#ifdef DEBUG
	  printf("Maxpos:%f Num:%d Pos:%d\n", max_dphi_angle_pos, n, max_pos );
#endif
          #ifdef DEBUG
	  printf("Maxneg:%f Num:%d Pos:%d \n", max_dphi_angle_neg, n, max_neg );
#endif

/*
	if(fabs(max_pos-size/2)>fabs(max_neg-size/2)) {
	angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else if(fabs(max_pos-size/2)==fabs(max_neg-size/2)){
		if(d_phi<0){
			angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else
		{pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}
		}
	else{
		pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}
		*/
		/*modif test avec max absolue...*/
		
	if(fabs(max_dphi_angle_pos)>fabs(max_dphi_angle_neg)) {
	angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else if(fabs(max_dphi_angle_pos)==fabs(max_dphi_angle_neg)){
		if(d_phi<0){
			angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else
		{pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}
		}
	else{
		pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}	
/* lecture de la deriv�e*/
d_phi=neurone[deb_e3+30/*29*/].s1;
delta_angle=d_phi;

      printf("f_phi :%f \n", delta_angle);


printf("angle_rel:%f et,delta_angle:%f\, angle_diff:%f\n",angle_rel,delta_angle,fabs(pos_rel-size/2));
if(fabs(pos_rel-size/2)>14&&delta_angle!=0.){
	do
		{
		delta_angle+=0.2*(fabs(pos_rel)/pos_rel);
		pos_delta_angle=(int)(delta_angle*(size/2)/M_PI+size/2); /*position en neurone du nouvel angle*/
		printf("angle:%f, pos:%d\n",delta_angle,pos_delta_angle);
		}
		while(pos_delta_angle<pos_rel-8 );
	/*delta_angle=  angle_rel/**1*fabs(pos_rel-size/2)*/;/*)angle_rel/2;*/
}
else if((neurone[deb_e3+31].s1*neurone[deb_e3+29].s1)<0/*0.01*/){
	printf("delta:%f et %f\n",delta_angle,fabs(pos_rel-size/2));delta_angle=0.;
	/*scanf("%d",&pos);*/
	}

		
printf("angle_rel:%f ,delta_angle:%f\, angle_diff:%f\n",angle_rel,delta_angle,fabs(pos_rel-size/2));



#ifdef DEBUG
	  printf("The actual direction given by the compass is : %f �\n",orientation_d);
#endif
	  /*veritable angle de d�placement en relatif, grace � utilisation odom�trie du robot*/
          mvt_r=delta_angle;/*teta;*/
	  teta=delta_angle;
#ifdef DEBUG
	  printf("F_nav_mvt mvt_r ap mise a jour :%f\n",mvt_r);
#endif
	/*Le mouvement affiche en sortie est celui effectue au cycle precedent (celui sur lequel on a des infos)*/
	  /*L'activit� du groupe est utilis�e par l'int�gration de chemin,
	  on repasse en orientation absolue:*/
          mvt_r=mvt_r+orientation_r;
	  if(mvt_r >= PI)mvt_r = mvt_r -2*PI;
          if(mvt_r < -PI)mvt_r = mvt_r +2*PI;

	  num_neur=(int)((mvt_r/(2*PI))*xmax2*ymax2+(xmax2*ymax2/2));
	  if(num_neur>=xmax2*ymax2)num_neur=0;
	  num_neur+=deb;
	  /*l'activit� du neurone en sortie est fonction de la distance parcourue*/
	  neurone[num_neur].s=neurone[num_neur].s1=neurone[num_neur].s2=(norme/ 30000);
	   /* norme:  1=30cm */
#ifdef DEBUG
	  printf("Robot's direction is now :%f, norm= %f \n",mvt_r,norme);
	  printf("Neurone :%d, angle= %f, act: %f \n",num_neur-deb,mvt_r,norme/DEFAULT_NORME/*30000*/);
#endif
	  /*Sauvegarde des donn�es odm�triques*/

	  parametres_odometrie.norme=norme;  /*longueur du vecteur deplacement realis�*/
	  parametres_odometrie.beta_tot=beta_tot; /*angle en relatif, dans le futur pourra etre �quivalent au chemin              int�gr� ou � la valeur de l'orientation... A definir*/
	  parametres_odometrie.teta=teta;       /*angle en relatif*/
	  def_groupe[numero].data=(void*)(&parametres_odometrie);
	
}



