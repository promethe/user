/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** 
\file  
\brief simulates winner_takes_all neural network

Author: xxxxxxxxx
Created: xxxxxxxxxx
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 20/07/2004

Theoritical description:
-gestion_groupe_winner:
-apprend_winner:


Description:******fction algo pour tester erreur maj apprentissage
This file contains the C fonction for winner_takes_all neural network.
We can find gestion_groupe_winner() and apprend_winner(),
which respectivly enables to be treated as a special group, and to learn.
The updates is done traditonaly. As it is a winner_takes_all neural network, gestion_group_winner finds
the most activated neuron, raises its activity to one and downs the other to zero. Learning is then realised.
-apprend_winner:
 * apprentissage du lien entre l'entree activee et la cellule gagnante
 * desaprentissage des liens reliant des entrees/sorties non correlles
-gestion_groupe_winner:
Generic Winner take all management.
Read the .s field of the neurons of the group.
Simply activate the max (i.e. .s1 and .s2 fields
are set to 1.0). Other neurons are inactive (i.e
.s1 and .s2 fields are set to 0.0).


Global variables:
- created: none Should seldomly happen!!!
- used: none, I think

Macro:
none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: xxxxxxxxxxx
- description: none
- input expected group: none
- where are the data?: none

Comments: none

Known bugs:

Todo: see if the bugs still happen

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdlib.h>
#define DEBUG
void function_algowinner(int gpe)
{
    int i, pos = 0;
    float max;
    type_coeff *coeff;
    int deb, nbre;
    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;

    max = -99999999.;
#ifdef DEBUG
    printf("Algo_winner %d  \n", gpe);
#endif

    if (def_groupe[gpe].data == NULL)
    {
        def_groupe[gpe].data = malloc(sizeof(char));
        *(char *) def_groupe[gpe].data = '4';
        neurone[deb].d = 0.;

    }
    if (isequal(neurone[deb].d,0.))
    {
        for (i = deb; i < deb + nbre; i++)  /* modifie les poids en consequence */
        {
            coeff = neurone[i].coeff;
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
            while (coeff != NULL)
            {
                if (coeff->evolution == 1)  /* si coeff modifiable */
                {
                    neurone[i].s2 += coeff->val * neurone[coeff->entree].s;
                }
                else
                {
                    neurone[i].s2 += coeff->val * neurone[coeff->entree].s;
                }
                coeff = coeff->s;
            }
            if (neurone[i].s2 > max)
            {
                max = neurone[i].s2;
                pos = i;
            }
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
        }
        if (max > def_groupe[gpe].seuil)
            neurone[pos].s = neurone[pos].s1 = neurone[pos].s2 = max;
        printf("winner =%d for max=%f\n", pos - deb, max);

        if (def_groupe[gpe].learning_rate >= 0.)
            neurone[deb].d = 1.;
    }
    else
    {

        neurone[deb].d = 0.;
        printf("pas d'apprentissage");

        for (i = deb; i < deb + nbre; i++)
        {
            neurone[i].s = neurone[i].s1 = neurone[i].s2 = -1.0;
        }

    }


}
