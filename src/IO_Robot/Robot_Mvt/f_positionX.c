/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_smooth_command.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
/*#define DEBUG*/
typedef struct MyData_f_positionX
{
    int gpe_action;
} MyData_f_display_image_activity;

void function_positionX(int numero)
{

    int gpe_action = -1;
    MyData_f_display_image_activity *my_data = NULL;
    int i = 0, l;
    float newpos, action;
    int maxPos = (int) (def_groupe[numero].seuil);
    if (maxPos == 0)
        maxPos = 10;

    if (def_groupe[numero].data == NULL)
    {
        l = find_input_link(numero, i);
        while (l != -1)
        {
            if (strcmp(liaison[l].nom, "action") == 0)
                gpe_action = liaison[l].depart;

            i++;
            l = find_input_link(numero, i);
        }
        if (gpe_action == -1)
        {
            printf("deplacemant circulaire\n");
        }
        my_data =
            (MyData_f_display_image_activity *)
            malloc(sizeof(MyData_f_display_image_activity));
        my_data->gpe_action = gpe_action;
        def_groupe[numero].data = my_data;
    }
    else
    {
        my_data = (MyData_f_display_image_activity *) def_groupe[numero].data;
        gpe_action = my_data->gpe_action;
    }

    if (gpe_action != -1)
    {
        if (neurone[def_groupe[gpe_action].premier_ele].s1 >
            neurone[def_groupe[gpe_action].premier_ele + 1].s1)
            action = -1.;
        else
            action = 1.;
    }
    else
        action = 1.;

    newpos = neurone[def_groupe[numero].premier_ele].s1 + action;
    if (newpos >= maxPos)
    {
        newpos = 0.;
    }
    if (newpos < 0.)
        newpos = maxPos - 1;
    neurone[def_groupe[numero].premier_ele].s2 =
        neurone[def_groupe[numero].premier_ele].s1 =
        neurone[def_groupe[numero].premier_ele].s = newpos;
}
