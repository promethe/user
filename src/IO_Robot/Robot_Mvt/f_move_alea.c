/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
----------------------------------------------------------------------
 Fonction VB : propose un mouvement au hasard
    Le Mouvement propose suis une loi gaussienne centree sur
 la dircetion 'tout droit'. Le robot a doc plus tendance a
 avance qu a revenir sur ses pas
 WARNING 1:   Pour que le robot avance plutot tout droit, il est
 necessaire d'avoir la valeur de la bousole interne
 WARNING 2:   Il y a des statics, donc ne mettre cette fonction
 qu'a UN seul exemplaire dans un reseau !
----------------------------------------------------------------------
Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void function_move_alea(int numero)
{
    static int first = 1;
    static int deb_e;
    int deb, taille, gp_sortie;
    int i;
    float x, y;
    float amplitude_x = 4;


    if (first)
    {
        first = 0;
        srand48(clock() /*(unsigned)time(NULL) */ );
        for (i = 0; i < nbre_liaison; i++)
            if ((liaison[i].depart == numero)
                && (!strcmp(liaison[i].nom, "orientation")))
            {
                gp_sortie = liaison[i].arrivee;
                deb_e = def_groupe[gp_sortie].premier_ele;
            }
    }


    taille = def_groupe[numero].nbre;
    deb = def_groupe[numero].premier_ele;
    srand48((unsigned) time(NULL));
    for (i = 0; i < taille; i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0;


    i = (int) (drand48() * taille);
    /*printf("\nRentrez une direction (>-1 et <120)=\n");
       fflush(stdout);
       scanf("%d",&i); */

    /*do
       {
       x=drand48()*4-2;
       y=drand48();
       }  
       while (y>2/(0.5*sqrt(2*3.141592))*exp(-x*x/(2*0.5*0.5)));
       x+=2;
       i=(int)(x*taille/4);
       i+=taille/2; */

    x = (drand48() - 0.5) * amplitude_x;
    y = -x * x * x;

    i = (int) (y * taille /
               (2 * amplitude_x * amplitude_x * amplitude_x / 8));
    if (i < 0)
        i += 120;

    printf
        ("\nVoici un i avant modification = %d et voici la boussole interne = %f\n",
         i, neurone[deb_e].s2);
    i += (int) (neurone[deb_e].s2 * 120);
    while (i > taille - 1)
        i -= taille;
    printf("\nEt voici un i gaussien = %d\n", i);

    neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 1.;
}
