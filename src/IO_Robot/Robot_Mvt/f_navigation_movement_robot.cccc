/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_navigation_movement_robot.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Fonction g�n�rant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degr�...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <public_tools/erreur.h>
#include "tools/include/move_via_mvector_given_speed.h"
#include <public_tools/IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include "tools/include/move_via_speed.h"
#include <libhardware.h>
#include <graphic_Tx.h>

#define DEBUG
#define DEFAULT_NORME 6000
#define SPEED 5
  /*old code =80*/
/*#define MAX_ROT_SPEED 20*/
#define CTE_DPHI 55
#define MAX_D_PHI 0.07
#define SPEED_COEFF 0.8


void function_navigation_movement_robot(int numero)
{
float delta_angle=0.,d_phi=0.; //variable pour la lecture du NF
int p[2];
  int	gpe_entree1, gpe_entree2=-1,gpe_entree3,gpe_entree4,gpe_satur,gpe_test_emission;
  int	i, n,pos_rel,max_neg,max_pos,pos_delta_angle;
  int	deb, size, increment;
  int	deb_e, taille_groupe_e, taille_groupe_e2,deb_e3=-1;
  int	xmax2,ymax2,num_neur;
  int save_posrel;
  int	pos=-1;/*,decalage,correspondant;*/
  int posm;
  double beta_tot=0, teta=0 /* NC: [-Pi;+Pi]*/, norme=0.;
  float	mvt_r /*NC: en degre [-180;180[*/, orientation_r/*NC: [-PI; PI[*/,orientation_d/*NC: [-180; 180[*/,max,scale;
  float speed_cste =4.,rot_speed=50.,max_dphi_angle_pos,max_dphi_angle_neg,angle_rel;
  static float speed_0;
  float fa,fb,cdir;
  int pos_180, posz;
  int MAX_ROT_SPEED;
  TxPoint pt_a,pt_b;

  
  TxPoint derivee[61];

	
  printf("debut f_nav_mvt_robot\n");
  
if(def_groupe[numero].data==NULL)
{
	def_groupe[numero].data=(void*)(&parametres_odometrie);
		robot_init_wheels_counter(robot_get_first_robot());

	gpe_entree4=trouver_entree(numero, "speed_cst");
	if(gpe_entree4!=-1){
	for (n=0;n<4;n++){
	i=find_input_link(numero,n);
	if(strcmp(liaison[i].nom,"speed_cst")==0) break;
	}
	speed_0=neurone[def_groupe[gpe_entree4].premier_ele].s2*liaison[i].norme;
	printf("=====================vitesse_nulle: %f===================\n",speed_0);
	}
}
printf("-------------------------------------------------\n");
  printf("Creating the movement \n");
 /*Recherche des boites en entr�es...*/
  gpe_entree3=trouver_entree(numero,"DP");
  gpe_entree1=trouver_entree(numero,"NF");
  gpe_entree2=trouver_entree(numero, "compass");
  gpe_satur=trouver_entree(numero, "satur");
gpe_test_emission =trouver_entree(numero, "test_emission");
  gpe_entree4=trouver_entree(numero, "speed_cst");
  if(gpe_entree4!=-1){
  for (n=0;n<8;n++){
  i=find_input_link(numero,n);
  if(strcmp(liaison[i].nom,"speed_cst")==0) break;
  }
MAX_ROT_SPEED= (int)(neurone[def_groupe[gpe_entree4].premier_ele].s2*liaison[i].norme);
  /*if(speed_cste!=0)
  printf("=====================vitesse brute: %f===================\n",speed_cste);
  if((speed_cste<=speed_0+2)&&(speed_cste>=speed_0-2))speed_cste=0;
  else if(speed_cste<speed_0-2)speed_cste=-(speed_0-speed_cste);
  else speed_cste=speed_cste-speed_0;
  if(speed_cste!=0){
 /* rot_speed=SPEED_COEFF*fabs(speed_cste);*/
  printf("=====================vitesse rot: %d===================\n",MAX_ROT_SPEED);
  
/*if(rot_speed*MAX_D_PHI+speed_cste>MAX_ROT_SPEED)rot_speed=(MAX_ROT_SPEED-speed_cste)*(1/MAX_D_PHI);*/
/*rot_speed=(1/MAX_D_PHI)*20.5*(1+((fabs(12.5-fabs(speed_cste/2)))/16.5));
   printf("=====================vitesse rot ap seuillage: %f===================\n",rot_speed);
   }*/
  }else
  if(speed_cste!=0)MAX_ROT_SPEED =20;
  printf("=====================vitesse: %f===================\n",speed_cste);
  
  if(gpe_entree2==1)printf("WARNING: f_nav_mvt: no f_orientation  find !\n");
#ifdef DEBUG
	printf("f_nav_mvt: NF:  %d; f_orientation: %d, DP %d!\n",gpe_entree1,gpe_entree2,gpe_entree3);
	printf("f_nav_mvt: f_orientation  find at group: %d\n",gpe_entree2);
#endif
  deb=def_groupe[numero].premier_ele;
  xmax2=def_groupe[numero].taillex;
  ymax2=def_groupe[numero].tailley;
  size = xmax2*ymax2;

  taille_groupe_e = def_groupe[gpe_entree1].nbre;
  taille_groupe_e2 = def_groupe[gpe_entree1].taillex*def_groupe[gpe_entree1].tailley;
  scale=2*PI/(float)taille_groupe_e2; //echelle, depend du nombre de neurone du groupe en entr�e
  increment=taille_groupe_e/taille_groupe_e2;
  deb_e = def_groupe[gpe_entree1].premier_ele;
  deb_e3 = def_groupe[gpe_entree3].premier_ele;
/*printf("Nico: ATTENTION NEW DEVELOPMENT!\n");
printf("nico: si probl�eme avec simul: decommenter le code qui traite les ordres en absolue!\n");
 */ /*NC: Recupere l'orientation courante (absolue)*/
  if(gpe_entree2!=-1)	/* test si direction absolue boussole ,convention valeur du groupe -PI PI*/{
    orientation_r = neurone[def_groupe[gpe_entree2].premier_ele].s;
    orientation_d=orientation_r*180/(PI); //entre -180 et 180
  }else{
    orientation_r= 0.;
    orientation_d=0.;
}
  /* tous les neurones de sortie a 0 ... */
  for (i=0; i<size; i++)
    neurone[deb+i].s=neurone[deb+i].s1=neurone[deb+i].s2=0.;
for (n=deb_e3;n<deb_e3+def_groupe[gpe_entree3].taillex*def_groupe[gpe_entree3].tailley; n++)
    {
    	derivee[n-deb_e3].x = (int)( (n-deb_e3)*610/61 + 50);
	derivee[n-deb_e3].y = (int)(500-neurone[n-deb_e3].s1*100);
	printf("x:%d,y:%d       ",derivee[n-deb_e3].x,derivee[n-deb_e3].y);
      if(neurone[n].s>max)
	{
	  max=neurone[n].s;
	  pos=(n-deb_e3)/increment;
	}
     }
	  	printf("\n");
  max=-999999.;
  pos=-1;
  for (n=deb_e;n<deb_e+def_groupe[gpe_entree1].taillex*def_groupe[gpe_entree1].tailley; n++)
    {
      if(neurone[n].s>max)
	{
	  max=neurone[n].s;
	  pos=(n-deb_e)/increment;
 pos_rel=(size/2)-pos; /*decalage par rapport a la position courante*/
	  angle_rel=pos_rel*(2*M_PI)/size; /*delta relatif entre l'angle desire et l'angle de l'orientation courante*/

	}
    }
 #ifdef DEBUG
	  printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
#ifdef DEBUG
//  printf("Max found : neuron No:%d  on:%d with a max=%f, norm_wanted=%f\n", pos, taille_groupe_e2,max,norm_wanted);
#endif
        
/* lecture de la deriv�e*/
d_phi=neurone[deb_e3+def_groupe[gpe_entree3].nbre/2 -1/*29*/].s1;
delta_angle=d_phi;

	   printf("Max_dphi:%f Num:%d Pos:%d\n", max, n, pos);
/*
printf("f_phi-2 :%f \n", neurone[deb_e3+27].s1);
printf("f_phi-1 :%f \n",neurone[deb_e3+28].s1 );
*/
      printf("f_phi :%f \n", delta_angle);/*
printf("f_phi+1 :%f \n", neurone[deb_e3+30].s1);
printf("f_phi+2 :%f \n", neurone[deb_e3+31].s1);*/
printf("emission robot: %d\n",EMISSION_ROBOT);	 /*recherche des 2 max pos et neg de D_phi*/
	 max_dphi_angle_neg=9999999.;
max_dphi_angle_pos=-999999.;
 /* pos=-1*/
  for (n=deb_e3;n<deb_e3+def_groupe[gpe_entree3].taillex*def_groupe[gpe_entree3].tailley; n++)
    {
      if(neurone[n].s>max_dphi_angle_pos)
	{
	  max_dphi_angle_pos=neurone[n].s;
	  max_pos=(n-deb_e3)/increment;

	}
	 else   if(neurone[n].s<max_dphi_angle_neg)
	{
	  max_dphi_angle_neg=neurone[n].s;
	  max_neg=(n-deb_e3)/increment;
	  
	}
    }
	 	#ifdef DEBUG
	  printf("Maxpos:%f Num:%d Pos:%d\n", max_dphi_angle_pos, n, max_pos );
#endif
          #ifdef DEBUG
	  printf("Maxneg:%f Num:%d Pos:%d \n", max_dphi_angle_neg, n, max_neg );
#endif
	if(fabs(max_pos-size/2)>fabs(max_neg-size/2)) {
	angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else if(fabs(max_pos-size/2)==fabs(max_neg-size/2)){
		if(d_phi<0){
			angle_rel=max_dphi_angle_neg;
		pos_rel= max_neg ;
		}
		else
		{pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}
		}
	else{
		pos_rel=max_pos ;
			angle_rel=max_dphi_angle_pos;
		}


  
#ifdef DEBUG
         /* printf("Nico ATTENTION encore en developpement!!!");*/
          printf("betat_tot: %f\n",beta_tot);
#endif         /*demande a realiser le nouveau mouvement propose par mvt_d*/printf("angle_rel:%f et,delta_angle:%f\, angle_diff:%f\n",angle_rel,delta_angle,fabs(pos_rel-size/2));

/*equation de prolongement de dphi � partir de pos_rel*/
/*le coefficient directeur de la droite est obtenu en ce placant dans le cas d'une consigne a 180� de la position courrante, avec une vitesse
de rotation fixee: MAX_ROT_SPEED*/
/*
f(pos)=cdir*pos + f(b)
b=pos_180 la position en nb de neurones d'un stimulus a 180�de l'orientation courrante. ex: si codage sur 60neurones. Lecture a l'index 30 de la pos courrante.pos_180=30-0=30;
f(b)=la valeur de dphi en la position du max de dphi (b)=pos_rel; 
f(a)=MAX_ROT_SPEED/CTE_DPHI cette valeur est celle qui sera utilisee pour faire appelle a move_via_speed dans le cas d'une consigne a 180�...
 cdir=(f(a)-f(b))/a avec a =pos_180
 */
 /*position de l'attracteur*/
 posz=(pos-size/2);
 /*position du max dphi*/
 posm=(pos_rel-size/2);
 
 save_posrel = pos_rel;
 
 /* Nous allons plotter � cet endroit la valeur de la fonction de consigne
 motrice, qui est une fonction par morceau : une partie provient de la d�riv�e
 du NF, l'autre est une fonction lin�aire
 */

	pt_a.x=0;
	pt_a.y=0;
 
 TxDessinerRectangle(&image1 ,blanc,TxPlein,
                          pt_a,1000,1000,1);
 
if((neurone[deb_e3+31].s1*neurone[deb_e3+29].s1)<0/*0.01*/){printf("delta:%f et %f\n",delta_angle,fabs(delta_angle));delta_angle=0.;
		 p[1]=4;
 p[0]=4;
printf("++++motors result from dphi: %d,%d\n",p[0],p[1]);
	}
else  if(posm*posz>0/*(posm<0&&pos_rel==max_neg)||(posm>0&&pos_rel==max_pos)*/){
fb= neurone[deb_e3+pos_rel].s2;
	  pos_180=size/2;
   	fa= (MAX_ROT_SPEED/CTE_DPHI)*fabs(fb)/fb;
		cdir=(fabs(fb)/fb)*(fb- fa) /pos_180;
		pos_rel=(-pos_rel+(size/2));
		delta_angle=pos_rel*cdir+fb;	 
printf("fa=%f, cdir=%f,pos_rel=%d,delta_angle:%f\n",fa,cdir,pos_rel,delta_angle);
 p[1]=(int)( delta_angle*CTE_DPHI+4);
 p[0]=(int)(-delta_angle*CTE_DPHI+4);
printf("-----motors result: %d,%d\n",p[0],p[1]);
 pt_b.x=(int)(save_posrel*610/61 + 50);
 pt_b.y=(int)(500-neurone[deb_e3+save_posrel].s*100);
pt_a.x=(int)((save_posrel+30)*610/61 + 50);
pt_a.y=(int)(500-fa*100);
TxDessinerSegment(&image1, jaune, 
                       pt_b, pt_a,2);
pt_a.x=(int)((save_posrel+30)*610/61 + 50);
pt_a.y=(int)((30*cdir+fb)*100);	
	TxDessinerSegment(&image1, 5, 
                       pt_b, pt_a,2);

}
else{
	 p[1]=(int)(delta_angle*CTE_DPHI+4);
 p[0]=(int)(-delta_angle*CTE_DPHI+4);
printf("++++motors result from dphi: %d,%d,posm : %d, posz : %d\n",p[0],p[1],posm,posz);
	} 
	

 
 TracerAxe(&image1, vert,vert, 50,600, 50, 400, 1,-1);
 TracerAxe(&image1, vert,vert, 50, 500,660,500, 60,0);

TxDessinerPoint(&image1, rouge, derivee[0]);

for (n=1;n<60;n++)
 TxDessinerSegment(&image1, rouge, derivee[n-1], derivee[n],2);
 TxFlush(&image1);

/*
if(fabs(pos_rel-size/2)>14&&delta_angle!=0.){
	delta_angle=angle_rel*(1    *+(pos-(size/2))*(fabs(angle_rel)/angle_rel)*);
	printf("delta_angle:%f\n",delta_angle);
	
	/*
		delta_angle+=0.2*(fabs(delta_angle)/delta_angle);
		pos_delta_angle=(int)(delta_angle*(size/2)/M_PI+size/2); /*position en neurone du nouvel angle*/
		/*printf("angle:%f, pos:%d\n",delta_angle,pos_delta_angle);*/
		/*		(pos_delta_angle-pos_rel-8 );*/
	/*delta_angle=  angle_rel/**1*fabs(pos_rel-size/2)*/;/*)angle_rel/2;*
}
else */
/*if(fabs(delta_angle)<fabs(max)&&delta_angle!=0.)*.009*
delta_angle=max*fabs(delta_angle)/delta_angle;*/
    printf("f_phi :%f \n", delta_angle);
/*do delta_angle*=10;
while(fabs(delta_angle)<.015);/**/
/*
      p[0] = (int) ( 4) +( 50 * delta_angle);
      p[1] = (int) ( 4) - (50* delta_angle);
      */
      
      /*modif CG: on compte les angles dans le sens trigo*/
     /* p[1] = 0/*(int) ( 4) +( CTE_DPHI * delta_angle)*/; /*50*/
     /* p[0] = 0/*(int) ( 4) - (CTE_DPHI* delta_angle)*/;
      
 printf("motors av seuillage: %d,%d\n",p[0],p[1]);
 
      if(abs(p[0])>30)p[0]=30;
      if(abs(p[1])>30)p[1]=30;
printf("delta_angle: %f\n",delta_angle);
 printf("motors: %d,%d\n",p[0],p[1]);
 
 /*test arret uregence si saturation du NF*/
  if(gpe_satur!=-1){
  	if(	neurone[def_groupe[gpe_satur].premier_ele].s2>0){
		p[1] =  p[0] =0;
		printf("Arret d'urgence du a la stauration\n");
	}
  }
   if(gpe_test_emission!=-1)
	{
		if(	neurone[def_groupe[gpe_test_emission].premier_ele].s2>0.5){
		p[1] =  p[0] =0;
		printf("test: aucun mouvement ne sera effectue\n");
	}
	}	
		
       /*recupere l'angle et la norme du vecteur deplavement que l'on vient de realiser (l'ordre precedent) et realise le nouveau mouvement propose*/
       move_via_speed(p[0],p[1],500.,&norme,&beta_tot);

	  printf("F_nav_mvt beta_tot ap mise a jour :%f\n",beta_tot);
#ifdef DEBUG
	  printf("The actual direction given by the compass is : %f �\n",orientation_d);
	  printf("F_nav_mvt beta_tot ap mise a jour :%f\n",beta_tot);
#endif
	  /*veritable angle de d�placement en relatif, grace � utilisation odom�trie du robot*/
          mvt_r=beta_tot;/*teta;*/
	  teta=beta_tot;
#ifdef DEBUG
	  printf("F_nav_mvt mvt_r ap mise a jour :%f\n",mvt_r);
	  printf("F_nav_mvt teta ap mise a jour :%f\n",teta);
#endif
	/*Le mouvement affiche en sortie est celui effectue au cycle precedent (celui sur lequel on a des infos)*/
	  /*L'activit� du groupe est utilis�e par l'int�gration de chemin,
	  on repasse en orientation absolue:*/
          mvt_r=mvt_r+orientation_r;
	  if(mvt_r >= PI)mvt_r = mvt_r -2*PI;
          if(mvt_r < -PI)mvt_r = mvt_r +2*PI;

	  num_neur=(int)((mvt_r/(2*PI))*xmax2*ymax2+(xmax2*ymax2/2));
	  if(num_neur>=xmax2*ymax2)num_neur=0;
	  num_neur+=deb;
	  /*l'activit� du neurone en sortie est fonction de la distance parcourue*/
	  neurone[num_neur].s=neurone[num_neur].s1=neurone[num_neur].s2=(norme/ 30000);
	   /* norme:  1=30cm */
#ifdef DEBUG
	  printf("Robot's direction is now :%f, norm= %f \n",mvt_r,norme);
	  printf("Neurone :%d, angle= %f, act: %f \n",num_neur-deb,mvt_r,norme/DEFAULT_NORME/*30000*/);
#endif
	  /*Sauvegarde des donn�es odm�triques*/

	  parametres_odometrie.norme=norme;  /*longueur du vecteur deplacement realis�*/
	  parametres_odometrie.beta_tot=beta_tot; /*angle en relatif, dans le futur pourra etre �quivalent au chemin              int�gr� ou � la valeur de l'orientation... A definir*/
	  parametres_odometrie.teta=teta;       /*angle en relatif*/
	  def_groupe[numero].data=(void*)(&parametres_odometrie);
	    
     
  printf("...fin f_nav_mvt_robot\n");
}



