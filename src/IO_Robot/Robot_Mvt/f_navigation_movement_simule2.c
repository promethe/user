/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_navigation_movement_simule2 f_navigation_movement_simule2


\section Author
Author: xxxxxxxx
Created: XX/XX/XXXX
\section Modified
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

\brief Fonction generant le mouvement via la lecture d'un groupe de commande

\details

CHECK DOC !



\section Description
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


\section Macro
- PI
- PI_2

\section Local variables
- sortie_odometrie parametres_odometrie


\section Global variables
- boolean EMISSION_ROBOT
- int USE_SIMULATION
- int USE_ROBOT

\section Internal Tools
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <IO_Robot/GenericTools.h>
#include "tools/include/local_var.h"
#include "tools/include/move_via_speed.h"

#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif
/*#define DEBUG*/
#define DEFAULT_NORME 6000
#define SPEED 5
  /*old code =80 */
/*#define MAX_ROT_SPEED 20*/
#define CTE_DPHI 55
#define MAX_D_PHI 0.07
#define SPEED_COEFF 0.8

void function_navigation_movement_simule2(int numero)
{
    float delta_angle = 0., d_phi = 0.; /*variable pour la lecture du NF*/
    int p[2];
    int gpe_entree1, gpe_entree2 = -1, gpe_entree3/*,gp_reset*/, gpe_satur, gpe_test_emission;
    int i, n, pos_rel, max_neg = -1, max_pos = -1;
    int deb, size, increment;
    int deb_e, taille_groupe_e, taille_groupe_e2, deb_e3 = -1;
    int xmax2, ymax2, num_neur;
    float bias_origin;
    int pos = -1, offset_deltat_angle;  /*,decalage,correspondant; */
    int posm;
    double beta_tot = 0, teta = 0 /* NC: [-Pi;+Pi] */ , norme = 30000.;
    float mvt_r /*NC: en degre [-180;180[ */ , orientation_r /*NC: [-PI; PI[ */ /*, orientation_d */ /*NC: [-180; 180[ */ , max/*, scale*/;
    float max_dphi_angle_pos, max_dphi_angle_neg/*, angle_rel*/;
    
    float fa, fb, cdir;
    int pos_180, posz;
    int MAX_ROT_SPEED = 30;
    TxPoint pt_a, pt_b;
    int g, indexp, indexpp, indexppp, indexn, indexnn, indexnnn;
    float ddp, ddpp;
    int max_pos_found = -1, max_neg_found = -1;
#ifndef AVEUGLE
    TxPoint derivee[61];
    int save_posrel;
#endif
    char printmotor[100];


    printf("debut f_nav_mvt_robot\n");


    printf("-------------------------------------------------\n");
    printf("Creating the movement \n");
    /*Recherche des boites en entrees... */
    gpe_entree3 = trouver_entree(numero, (char*)"DP");
    gpe_entree1 = trouver_entree(numero, (char*)"NF");
    gpe_entree2 = trouver_entree(numero, (char*)"compass");
    gpe_satur = trouver_entree(numero, (char*)"satur");
    gpe_test_emission = trouver_entree(numero, (char*)"test_emission");
    /*gp_reset = trouver_entree(numero, (char*)"reset");*/


    if (gpe_entree2 == 1)
        printf("WARNING: f_nav_mvt: no f_orientation  find !\n");
    dprints("f_nav_mvt: NF:  %d; f_orientation: %d, DP %d!\n", gpe_entree1,
           gpe_entree2, gpe_entree3);
    dprints("f_nav_mvt: f_orientation  find at group: %d\n", gpe_entree2);
    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    size = xmax2 * ymax2;

    taille_groupe_e = def_groupe[gpe_entree1].nbre;
    taille_groupe_e2 =
        def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley;
    /*scale = 2 * PI / (float) taille_groupe_e2; */ /*echelle, depend du nombre de neurone du groupe en entree*/
    increment = taille_groupe_e / taille_groupe_e2;
    deb_e = def_groupe[gpe_entree1].premier_ele;
    deb_e3 = def_groupe[gpe_entree3].premier_ele;

    /*NC: Recupere l'orientation courante (absolue) */
    if (gpe_entree2 != -1)      /* test si direction absolue boussole ,convention valeur du groupe -PI PI */
    {
        orientation_r = neurone[def_groupe[gpe_entree2].premier_ele].s;
        /*orientation_d = orientation_r * 180 / (PI);*/ /*entre -180 et 180*/
    }
    else
    {
        orientation_r = 0.;
        /*orientation_d = 0.;*/
    }

    /* tous les neurones de sortie a 0 ... */
    for (i = 0; i < size; i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

    for (n = deb_e3; n <
         deb_e3 + def_groupe[gpe_entree3].taillex * def_groupe[gpe_entree3].tailley; n++)
    {
#ifndef AVEUGLE
        derivee[n - deb_e3].x = (int) ((n - deb_e3) * 610 / 61 + 50);
        derivee[n - deb_e3].y = (int) (500 - (int) (neurone[n].s1 * 300));
#endif
        if (neurone[n].s > max)
        {
            max = neurone[n].s;
            pos = (n - deb_e3) / increment;
        }
    }
    dprints("Max:%f Num:%d Pos:%d\n", max, n, pos);

    max = -999999.;
    pos = -1;
    for (n = deb_e; n < deb_e + def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley; n++)
    {
        if (neurone[n].s > max)
        {
            max = neurone[n].s;
            pos = (n - deb_e) / increment;
            pos_rel = (size / 2) - pos; /*decalage par rapport a la position courante */
            /*angle_rel = pos_rel * (2 * M_PI) / size;*/    /*delta relatif entre l'angle desire et l'angle de l'orientation courante */
        }
    }
    cprints("Max_dphi:%f Num:%d Pos:%d\n", max, n, pos);


/* lecture de la derivee*/
    d_phi = neurone[deb_e3 + def_groupe[gpe_entree3].nbre / 2 - 1 /*29 */ ].s1;
    delta_angle = d_phi;
    cprints("f_phi :%f \n", delta_angle);

    /*recherche des 2 max ABSOLU pos et neg de D_phi */
    max_dphi_angle_neg = 9999999.;
    max_dphi_angle_pos = -999999.;
    for (n = deb_e3; n < deb_e3 + def_groupe[gpe_entree3].taillex * def_groupe[gpe_entree3].tailley; n++)
    {
        if (neurone[n].s > max_dphi_angle_pos)
        {
            max_dphi_angle_pos = neurone[n].s;
            max_pos = (n - deb_e3) / increment;
        }
        else if (neurone[n].s < max_dphi_angle_neg)
        {
            max_dphi_angle_neg = neurone[n].s;
            max_neg = (n - deb_e3) / increment;
        }
    }

    dprints("Maxpos:%f Num:%d Pos:%d\n", max_dphi_angle_pos, n, max_pos);
    dprints("Maxneg:%f Num:%d Pos:%d \n", max_dphi_angle_neg, n, max_neg);


/*recherche des 2 max APPARTENANT A L'ATTRACTEUR pos et neg de D_phi*/
    max_neg = -1;
    max_pos = -1;
    for (g = 0; g < size; g++)
    {
        /*calcul des index */
        indexp = pos + deb_e3 + g;
        if (indexp >= size + deb_e3)
            indexp -= size;
        indexpp = pos + deb_e3 + g + 1;
        if (indexpp >= size + deb_e3)
            indexpp -= size;
        indexppp = pos + deb_e3 + g + 2;
        if (indexppp >= size + deb_e3)
            indexppp -= size;
        indexn = pos + deb_e3 - g;
        if (indexn < deb_e3)
            indexn += size;
        indexnn = pos + deb_e3 - g - 1;
        if (indexnn < deb_e3)
            indexnn += size;
        indexnnn = pos + deb_e3 - g - 2;
        if (indexnnn < deb_e3)
            indexnnn += size;


        ddp = neurone[indexpp].s - neurone[indexp].s;
        ddpp = neurone[indexppp].s - neurone[indexpp].s;
        if (ddp * ddpp < 0)
        {
            /*recherche du maxima de dphi lier a l'atracteur en pos */
            if (neurone[indexpp].s > 0 && max_pos_found == -1)
            {
                max_pos = indexpp - deb_e3;
                max_pos_found = 1;
            }
            /*recherche du minima de dphi lier a l'atracteur en pos */
            else if (neurone[indexpp].s < 0 && max_neg_found == -1)
            {
                max_neg = indexpp - deb_e3;
                max_neg_found = 1;
            }
            else
                cprints("max deja trouve, celui ci iggnore: %d\n", indexpp - deb_e3);
        }

        ddp = neurone[indexnn].s - neurone[indexn].s;
        ddpp = neurone[indexnnn].s - neurone[indexnn].s;
        if (ddp * ddpp < 0)
        {
            /*recherche du maxima de dphi lier a l'atracteur en pos */
            if (neurone[indexnn].s > 0 && max_pos_found == -1)
            {
                max_pos = indexnn - deb_e3;
                max_pos_found = 1;
            }
            /*recherche du minima de dphi lier a l'atracteur en pos */
            else if (neurone[indexnn].s < 0 && max_neg_found == -1)
            {
                max_neg = indexnn - deb_e3;
                max_neg_found = 1;
            }
            else
                cprints("max deja trouve, celui ci iggnore: %d\n", indexnn - deb_e3);
        }

        /*a ton trouver le maxima et le minima ?? si oui on arrete la recherche */
        if (max_pos != -1 && max_neg != -1)
            break;
    }

    if (!(max_pos != -1 && max_neg != -1))
    {
        cprints("Un max non trouve, arret urgence!\n");
        cprints("ddphi: Maxpos:%d\n", max_pos);
        cprints("ddphi: Maxneg:%d \n", max_neg);
        move_via_speed(0, 0, &norme, &beta_tot, 0);
/*         scanf("%d", &pt_a);*/
        exit(-1);
    }
    dprints("ddphi: Maxpos:%d\n", max_pos);
    dprints("ddphi: Maxneg:%d \n", max_neg);

/*recherche du max locale le plus proche est affecte sa position a pos_rel*/
    if (abs(max_pos - size / 2) > abs(max_neg - size / 2))
    {
        pos_rel = max_neg;
    }
    else if (abs(max_pos - size / 2) == abs(max_neg - size / 2))
    {
        if (d_phi < 0)
        {
            pos_rel = max_neg;
        }
        else
        {
            pos_rel = max_pos;
        }
    }
    else
    {
        pos_rel = max_pos;
    }


/*equation de prolongement de dphi a partir de pos_rel*/
/*le coefficient directeur de la droite est obtenu en ce placant dans le cas d'une consigne a 180� de la position courrante, avec une vitesse
de rotation fixee: MAX_ROT_SPEED*/
/*
f(pos)=cdir*pos + f(b)
b=pos_180 la position en nb de neurones d'un stimulus a 180�de l'orientation courrante. ex: si codage sur 60neurones. Lecture a l'index 30 de la pos courrante.pos_180=30-0=30;
f(b)=la valeur de dphi en la position du max de dphi (b)=pos_rel; 
f(a)=MAX_ROT_SPEED/CTE_DPHI cette valeur est celle qui sera utilisee pour faire appelle a move_via_speed dans le cas d'une consigne a 180�...
 cdir=(f(a)-f(b))/a avec a =pos_180
 */
    /*position de l'attracteur */
    posz = (pos - size / 2);
    /*position du max dphi */
    posm = (pos_rel - size / 2);
    

    /* Nous allons plotter a cet endroit la valeur de la fonction de consigne
       motrice, qui est une fonction par morceau : une partie provient de la derivee
       du NF, l'autre est une fonction lineaire
     */
#ifndef AVEUGLE

/*ON SAUVEGARDE POS_REL AVANT QU'IL NE SOIT MODIFIE */
    save_posrel = pos_rel;

    pt_a.x = 0;
    pt_a.y = 0;

/*EFFACE L'IMAGE*/
    TxDessinerRectangle(&image2, blanc, TxPlein, pt_a, 1000, 1000, 1);
/*PLOT 2 carres bleu pour les max de dphi et un carre rouge sur l'attracteur*/
    pt_a.x = (int) ((max_pos) * 610 / 61 + 50);
    pt_a.y = (int) (500 - (int) (neurone[max_pos + deb_e3].s1 * 300));
    TxDessinerRectangle(&image2, bleu, TxPlein, pt_a, 5, 5, 1);

    pt_a.x = (int) ((max_neg) * 610 / 61 + 50);
    pt_a.y = (int) (500 - (int) (neurone[max_neg + deb_e3].s1 * 300));
    TxDessinerRectangle(&image2, bleu, TxPlein, pt_a, 5, 5, 1);
    pt_a.x = (int) ((pos) * 610 / 61 + 50);
    pt_a.y = 500;
    TxDessinerRectangle(&image2, rouge, TxPlein, pt_a, 5, 5, 1);
#endif

/*test si l'on est dans la zone d'attraction (les cxarres bleu ont ils de part et d'autre de l'orientation courante */
    if ((neurone[deb_e3 + 31].s1 * neurone[deb_e3 + 29].s1) < 0 && abs(pos - size / 2) < 3 /*0.01 */ )
    {
        cprints("delta:%f et %f\n", delta_angle, fabs(delta_angle));
        delta_angle = 0.;
        p[1] = 4;
        p[0] = 4;
        cprints("0000000 motors result from dphi: %d,%d\n", p[0], p[1]);
    }
    else if (posm * posz < 0 || abs(posz) < abs(posm)
             /*(posm<0&&pos_rel==max_neg)||(posm>0&&pos_rel==max_pos) */ )
    {
        p[1] = (int) (delta_angle * CTE_DPHI + 4);
        p[0] = (int) (-delta_angle * CTE_DPHI + 4);
        cprints("++++motors result from dphi: %d,%d,posm : %d, posz : %d\n", p[0], p[1], posm, posz);
        if (abs(posz) > 10)
            cprints("\n\n\n ERREUR posz \n \n\n");
    }
    else
    {
        fb = neurone[deb_e3 + pos_rel].s2;
        if (posm < 0)
            pos_180 = pos_rel + size / 2;
        else
            pos_180 = pos_rel - size / 2;
        if (fb > 0)
            fa = (float) (((float) (MAX_ROT_SPEED) / ((float) CTE_DPHI)));
        else
            fa = (float) (-((float) ((MAX_ROT_SPEED) / ((float) CTE_DPHI))));

        cprints("MAX_ROT_SPEED : %d, CTE_DPHI : %d, fa : %f\n", MAX_ROT_SPEED, CTE_DPHI, fa);

        /*cdir=(fabsf(fb)/fb)*(-posm)*(fa- fb) /pos_180; */
        if (posm < 0)
            cdir = (float) ((fa - fb) / (pos_180 - pos_rel));
        else
            cdir = (float) ((fb - fa) / (pos_rel - pos_180));

        bias_origin = (float) fb - (float) pos_rel *cdir;
        delta_angle = (size / 2) * cdir + bias_origin;

        cprints("fa=%f, cdir=%f,pos_rel=%d,delta_angle:%f, bias_origin : %f\n", fa, cdir, pos_rel, delta_angle, bias_origin);
        p[1] = (int) (delta_angle * CTE_DPHI + 8);  /*cste=4 pour test_ir_ok val 6 non tester */
        p[0] = (int) (-delta_angle * CTE_DPHI + 8);
        cprints("-----motors result: %d,%d\n", p[0], p[1]);
#ifndef AVEUGLE
        /*plot de la droite a partir des point d'origine et de destination */
        pt_b.x = (int) (save_posrel * 610 / 61 + 50);
        pt_b.y = (int) (500 - neurone[deb_e3 + save_posrel].s * 300);
        pt_a.x = (int) ((pos_180) * 610 / 61 + 50);
        pt_a.y = (int) (500 - fa * 300);
        TxDessinerSegment(&image2, jaune, pt_b, pt_a, 2);
        /* la droite a partir du point b et point de destination obtenu via equation */
        pt_a.x = (int) ((pos_180) * 610 / 61 + 50);
        /*pt_a.y=(int)((-(abs(posm)/posm)*30*cdir+fb)*300);     */
        pt_a.y = 500 - (int) ((pos_180 * cdir + bias_origin) * 300);
        cprints("*******************************fa calcule : %f, fa const : %f\n", pos_180 * cdir + bias_origin, fa);
        TxDessinerSegment(&image2, vert, pt_b, pt_a, 2);
#endif
    }


#ifndef AVEUGLE
    TracerAxe(&image2, vert, vert, 355, 600, 355, 400, 1, -1);
    TracerAxe(&image2, vert, vert, 50, 500, 660, 500, 60, 0);
    pt_a.x = 355;
    pt_a.y = 630;
    sprintf(printmotor, "moteurs : P0 = %d, P1 = %d\n", p[0], p[1]);

    TxEcrireChaine(&image2, vert, pt_a, printmotor, 0);
    pt_a.x = 355;
    pt_a.y = 680;
    if (p[0] > p[1])
        sprintf(printmotor, "tourne a DROITE\n");
    else if (p[0] < p[1])
        sprintf(printmotor, "tourne a GAUCHE\n");
    else
        sprintf(printmotor, "tout droit\n");
    TxEcrireChaine(&image2, vert, pt_a, printmotor, 0);
    TxDessinerPoint(&image2, rouge, derivee[0]);
    for (n = 1; n < 60; n++)
        TxDessinerSegment(&image2, rouge, derivee[n - 1], derivee[n], 2);
    TxFlush(&image2);
#endif


    cprints("motors av seuillage: %d,%d\n", p[0], p[1]);

    if (abs(p[1]) > MAX_ROT_SPEED + 8)
    {
        p[1] = 0;
        p[0] = 0;
        cprints("TTTTTTTTTTTTTTTTT ARRET ERREUR CONSIGNE  \a \a \n");
    }
    cprints("delta_angle FINAL: %f\n", delta_angle);
    cprints("motors FINAL: %d,%d\n", p[0], p[1]);

    /*test arret uregence si saturation du NF */
    if (gpe_satur != -1)
    {
        if (neurone[def_groupe[gpe_satur].premier_ele].s2 > 0)
        {
            p[1] = p[0] = 0;
            cprints("Arret d'urgence du a la stauration\n");
        }
    }
    if (gpe_test_emission != -1)
    {
        if (neurone[def_groupe[gpe_test_emission].premier_ele].s2 > 0.5)
        {
            p[1] = p[0] = 0;
            cprints("test: aucun mouvement ne sera effectue\n");
        }
    }

    offset_deltat_angle = delta_angle * size / (2 * M_PI);
    if (abs(offset_deltat_angle) > abs(posm))
    {
        cprints("delta_angle exceed value: av:%f", delta_angle);
        delta_angle = posm * (2 * M_PI) / size;
        cprints(" ap:%f, posm:%d\n", delta_angle, posm);
    }

    beta_tot = delta_angle;
    cprints("F_nav_mvt beta_tot ap mise a jour :%f\n", beta_tot);
    dprints("The actual direction given by the compass is : %f �\n",  orientation_r * 180 / (PI));
    /*veritable angle de deplacement en relatif, grace a utilisation odometrie du robot */
    mvt_r = beta_tot;           /*teta; */
    teta = beta_tot;
    /*Le mouvement affiche en sortie est celui effectue au cycle precedent (celui sur lequel on a des infos) */
    /*L'activite du groupe est utilisee par l'integration de chemin,
       on repasse en orientation absolue: */
    mvt_r = mvt_r + orientation_r;
    if (mvt_r >= PI)
        mvt_r = mvt_r - 2 * PI;
    if (mvt_r < -PI)
        mvt_r = mvt_r + 2 * PI;

    num_neur = (int) ((mvt_r / (2 * PI)) * xmax2 * ymax2 + (xmax2 * ymax2 / 2));
    /*TEST DEBORDEMENT */
    if (num_neur >= xmax2 * ymax2)
        num_neur = 0;
    num_neur += deb;
    /*l'activite du neurone en sortie est fonction de la distance parcourue */
    neurone[num_neur].s = neurone[num_neur].s1 = neurone[num_neur].s2 = (norme / 30000);
    /* norme:  1=30cm */
    dprints("Robot's direction is now :%f, norm= %f \n", mvt_r, norme);
    dprints("Neurone :%d, angle= %f, act: %f \n", num_neur - deb, mvt_r, norme / DEFAULT_NORME /*30000 */ );

    /*Sauvegarde des donnees odmetriques */
    parametres_odometrie.norme = norme; /*longueur du vecteur deplacement realise */
    parametres_odometrie.beta_tot = beta_tot;   /*angle en relatif, dans le futur pourra etre equivalent au chemin              integre ou a la valeur de l'orientation... A definir */
    parametres_odometrie.teta = teta;   /*angle en relatif */
    def_groupe[numero].data = (void *) (&parametres_odometrie);
    cprints("...fin f_nav_mvt_robot\n");
    
    (void) printmotor;
    (void) pt_b;
    (void) pt_a;
}
