/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_navigation_movement_simple.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: fonction de realisation du mouvement
- date: 01/03/2005

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Fonction generant le mouvement via la lecture d'un groupe de commande (NF ou demande_angle_degre...).
 partition de 360 degres  (si 120 neurones, alors chaque neurone code 3 degres) [-180 180]
 
 On envoie l'ordre moteur correspondant a la lecture de la derivee du NF (nouvelle consigne) tout en recuperant avant les infos d'odometrie( angle et norme) du mouvement precedent!
 Le neurone correspondant a la direction du mouvement reelement effectue (au cycle precedent) prend une intensite egale
 a la norme de ce mouvement (1 veut dire 30cm)
 L'extension de ce groupe contient les donnees odometriques a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees par des groupes en aval (au hasard f_orientation)


Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie


Global variables:
-boolean EMISSION_ROBOT
-int USE_SIMULATION
-int USE_ROBOT

Internal Tools:
- move_via_mvector_given_speed(float angle,float distance,float speed, double *norme,double *beta_tot,double *teta)

External Tools:
-

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <Struct/sortie_odometrie.h>
#include <Global_Var/IO_Robot.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
#include <libhardware.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

typedef struct My_Data_f_navigation_movement_simple
{
    int gpe_nf;
    int gpe_derive;
    int gpe_linear_speed;
    int gpe_rotational_speed;
    int gpe_gaz;
    int type_lecture;
    int mode_pseudo;
    float speed_linear_max;
    int num_agent;

} My_Data_f_navigation_movement_simple;

void function_navigation_movement_simple(int gpe)
{
    My_Data_f_navigation_movement_simple * my_data = NULL;
    int mode_pseudo = 0;
    int gpe_nf, gpe_derive, gpe_linear_speed, gpe_rotational_speed, gpe_gaz;
    int i, l, type_lecture = 0.;
    char param[32];
    Robot *robot =NULL;

    int num_agent = -1;

    int indmax = -1;
    int *indsmax = NULL;
    float min;
    float neural_speed;

    float speed_linear;
    float speed_linear_max = 0.25;  /*0.5-0.05 */

    float speed_angular;
    float speed_angular_factor; /*il faut qu'il soit entre 0.5 et 1 avec les valeur suivantes: on accelere au faible vitesse */
    float speed_angular_max = M_PI / 2;
    float speed_angular_min = 0.15; /*.25 = 15 deg.s-1 */

    float facteur;
    float facteur_gaz = 1.0;
    float vd, vg;

    if (def_groupe[gpe].data == NULL)
    {

        gpe_nf = gpe_derive = gpe_linear_speed = gpe_rotational_speed = gpe_gaz = -2;
        
        i = 0;
        while ( (l = find_input_link(gpe, i)) != -1)
        {
            if (strcmp(liaison[l].nom, "NF") == 0)
                gpe_nf = liaison[l].depart;
            else if (strcmp(liaison[l].nom, "DP") == 0)
                gpe_derive = liaison[l].depart;
            else if (strcmp(liaison[l].nom, "linear_speed") == 0)
                gpe_linear_speed = liaison[l].depart;
            else if (strcmp(liaison[l].nom, "rotational_speed") == 0)
                gpe_rotational_speed = liaison[l].depart;
            else if (strcmp(liaison[l].nom, "gaz") == 0)
                gpe_gaz = liaison[l].depart;
            
            if (prom_getopt(liaison[l].nom, "-t", param) > 1)
                type_lecture = atoi(param);
            if (prom_getopt(liaison[l].nom, "-m", param) > 1)
                speed_linear_max = atof(param);
            if (prom_getopt(liaison[l].nom, "-p", param) > 0)
                mode_pseudo = 1;
            if (prom_getopt(liaison[l].nom, "-a", param) > 1)
                num_agent = atoi(param);

            i++;
        }
        if (gpe_derive == -2)
        {
            printf("Il faut 1 groups avec  <DP> au moins sur les liens de %s\n",__FUNCTION__);
            printf("exit in %s\n", __FUNCTION__);
            exit(-1);
        }
        my_data = (My_Data_f_navigation_movement_simple *)malloc(sizeof(My_Data_f_navigation_movement_simple));
        my_data->gpe_nf = gpe_nf;
        my_data->gpe_derive = gpe_derive;
        my_data->gpe_linear_speed = gpe_linear_speed;
        my_data->gpe_rotational_speed = gpe_rotational_speed;
        my_data->gpe_gaz = gpe_gaz;
        my_data->type_lecture = type_lecture;
        my_data->mode_pseudo = mode_pseudo;
        my_data->speed_linear_max = speed_linear_max;
        my_data->num_agent = num_agent;
        def_groupe[gpe].data = (My_Data_f_navigation_movement_simple *) my_data;

        /* Ajout Vincent */
        /* Mise a zero des compteurs des roues dans le cas du premier appel a la fonction */
        /*robot_init_wheels_counter(robot) ; */
    }
    else
    {
        my_data = (My_Data_f_navigation_movement_simple *) def_groupe[gpe].data;
        gpe_nf = my_data->gpe_nf;
        gpe_derive = my_data->gpe_derive;
        gpe_linear_speed = my_data->gpe_linear_speed;
        gpe_rotational_speed = my_data->gpe_rotational_speed;
        gpe_gaz = my_data->gpe_gaz;
        type_lecture = my_data->type_lecture;
        mode_pseudo = my_data->mode_pseudo;
        speed_linear_max = my_data->speed_linear_max;
    }

    if(num_agent==-1 && mode_pseudo==0)
         robot= robot_get_first_robot();
    


    if (gpe_gaz >= 0)
    {
        facteur_gaz = neurone[def_groupe[gpe_gaz].premier_ele].s1;

        dprints("facteur gaz : %f\n", facteur_gaz);
    }
    
    /* lecture de la derivee*/
/*d_phi=neurone[deb_e3+def_groupe[gpe_derive].nbre/2 -1].s1+neurone[deb_e3+def_groupe[gpe_derive].nbre/2 +1].s1;
    d_phi=d_phi/2;
    delta_angle=d_phi;

    printf("f_phi :%f \n", delta_angle);
    if(robot->robot_type==1||robot->robot_type==2)
    {
    robot_go_by_speed(robot,p[0],p[1]);
}
    else if(robot->robot_type==3)
    {
    speed_linear=0.;
    printf("%f vs %f \n", delta_angle,delta_angle_old);
    robot_set_velocity(robot,speed_linear,0.,-delta_angle,1);
    delta_angle_old=delta_angle;
		       
}
*/

    /* le zero est en nbre/2 -1 */


    indsmax = (int *) malloc(sizeof(int) * def_groupe[gpe_derive].nbre);
    if (indsmax == NULL)
    {
        printf("probleme de malloc in function %s at line %d\n", __FUNCTION__,  __LINE__);
        exit(0);
    }
    for (i = 0; i < def_groupe[gpe_derive].nbre; i++)
    {
        indsmax[i] = -1000;
    }


    /*determine le max dans le nf vers lequel on se dirige (ie les passages de neg vers pos dans la derive) */
    /*le max en quetion est choisis comme la plus proche */
    if (type_lecture == 0)
    {
        int cpt = 0;
        /*on cherche les max */
        for (i = 1; i < def_groupe[gpe_derive].nbre - 1; i++)
        {
            if (neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1 > 0. && 
                neurone[i + def_groupe[gpe_derive].premier_ele + 1].s1 <= 0.)
            {
                indsmax[cpt] = i;
                /*printf("%f   -   %f    =>   max:%d\n",neurone[i+def_groupe[gpe_derive].premier_ele].s1,neurone[i+def_groupe[gpe_derive].premier_ele+1].s1,i); */
                cpt++;
            }
        }
        /*on detemine les plus proches */
        min = 1000.;
        for (i = 0; i < cpt; i++)
        {
            /*il y  un petit strabisme de 1 neurone */
            if (fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]) < min)
            {
                indmax = indsmax[i];
                min = fabs(def_groupe[gpe_derive].nbre / 2 - indsmax[i]);
            }
        }

    }
    else if (type_lecture == 1)
        /*Cette foix le max est le plus proche dans la direction donne par la derive */
    {
        float previous,current,next;
        
        if (neurone[ (def_groupe[gpe_derive].nbre/2)+def_groupe[gpe_derive].premier_ele ].s1 > 0.)
        {

            for (i = def_groupe[gpe_derive].nbre / 2; i < def_groupe[gpe_derive].nbre - 1; i++)
            {
                previous = neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1;
                current = neurone[i + def_groupe[gpe_derive].premier_ele].s1;
                next = neurone[i + 1 + def_groupe[gpe_derive].premier_ele].s1;
                
                if ( previous > 0. && next < 0. && fabsf(current) <= fabsf(previous) && fabsf(current) <= fabsf(next) )
                {
                    indmax = i;
                    break;
                }
            }
            
        }
        else
        {
            for (i = def_groupe[gpe_derive].nbre / 2; i > 0; i--)
            {
                previous = neurone[i - 1 + def_groupe[gpe_derive].premier_ele].s1;
                current = neurone[i + def_groupe[gpe_derive].premier_ele].s1;
                next = neurone[i + 1 + def_groupe[gpe_derive].premier_ele].s1;
                
                if ( previous > 0. && next < 0. && fabsf(current) <= fabsf(previous) && fabsf(current) <= fabsf(next) )
                {
                    indmax = i;
                    break;
                }
            }
        }
        /*    printf("no developpe\n");exit(0); */
    }
    /* indmax est l'indice du max sur le champs NF : ce qui donne la vitesse angulaire de base */

    for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
    
    neurone[def_groupe[gpe].premier_ele + indmax].s =
            neurone[def_groupe[gpe].premier_ele + indmax].s1 =
            neurone[def_groupe[gpe].premier_ele + indmax].s2 = 1.0;


    neural_speed = def_groupe[gpe_derive].nbre / 2 - indmax;

    /*real speed borne a 90 deg / s */
    speed_angular = neural_speed / def_groupe[gpe_derive].nbre / 2;
    speed_angular = speed_angular * 2. * M_PI;
/*     printf("angular speed %f\n",speed_angular);*/
    
    

    if (mode_pseudo == 0)
    {
        if(num_agent!=-1)
        {
              /*to_devel*/
        }
        else 
        {
        if (robot->robot_type == 1 || robot->robot_type == 2)
        {
/*          if (speed_angular > 0. && speed_angular < M_PI / 8.)
            speed_angular = M_PI / 8.;*/
            if (gpe_rotational_speed != -2)
            {
		      if(neurone[def_groupe[gpe_rotational_speed].premier_ele].s1<=0.)
		      {
			      if (speed_angular > 0. )
				      speed_angular = M_PI / 12.;
			      if (speed_angular < 0. )
				      speed_angular = -M_PI / 12.;
		      }
		      else
		      {
			      if (speed_angular > 0. && speed_angular < M_PI / 8.)
                              speed_angular =   M_PI / 8.;
			      else if (speed_angular < 0. && speed_angular > - M_PI / 8.)
				      speed_angular = - M_PI / 8.;
      
			      speed_angular=speed_angular*neurone[def_groupe[gpe_rotational_speed].premier_ele].s1;
		      }
            }

            if (gpe_linear_speed == -2)
            {
                speed_linear = 12.;
            }
            else
            {
                facteur = (tanh (4 * (neurone[def_groupe[gpe_linear_speed].premier_ele].s1 - 0.5)) + 1.) / 2.;
                if (facteur < 0.)
                    facteur = 0.;
                speed_linear = speed_linear_max * facteur * facteur_gaz + 2.;

            }
            dprints("koala:speed linear:%f\n", speed_linear);
            vd = speed_linear + speed_angular * 150. / 2. / M_PI;
            vg = speed_linear - speed_angular * 150. / 2. / M_PI;

            /* Changement Julien Hirel 01/2009 */
            /* remplacement de l'appel a robot_go_by_speed_odometrie par robot_go_by_speed */
	    /* La gestion de l'odometrie est faite separement de celle du mouvement */
            /*robot_go_by_speed_odometrie(robot, vg, vd); */
            robot_go_by_speed(robot, vg, vd);
        }
        else if (robot->robot_type == 3 )
        {                       /*real_speed_angular=0.; */
            if (gpe_linear_speed == -2)
            {
                speed_linear = 0.; /* c'est quoi l'interet de le mettre a O.O5 ??*/
            }
            else
            {
                /* printf("linear speed 0.05 + %f /100 =%f \n", speed_linear_max*neurone[def_groupe[gpe_linear_speed].premier_ele].s1 / 0.05 * 100., speed_angular); */

                /*Pour le pioneer */
                facteur = (tanh (4 * (neurone[def_groupe[gpe_linear_speed].premier_ele].s1 - 0.5)) + 1.) / 2.;
                if (facteur < 0.)
                    facteur = 0.;
                speed_linear = speed_linear_max * facteur * facteur_gaz;

            }
            
            if (gpe_rotational_speed != -2)
            {
                /*printf("angular speed  + %f /100 =%f \n",neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 * speed_angular_factor_max*100.,speed_angular);
                */
                facteur = (tanh(4 * (neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 - 0.5)) + 1.) / 2.;
                if (facteur < 0.)
                    facteur = 0.;
                speed_angular_factor = 0.10 + facteur * facteur_gaz * 0.25; /*entre 0.5 et 1 */
                speed_angular = speed_angular * speed_angular_factor;
            }
            
            if (speed_angular > 0.)
            {
                speed_angular = speed_angular + speed_angular_min;
            }
            else if (speed_angular < 0.)
            {
                speed_angular = speed_angular - speed_angular_min;
            }
            
            if (speed_angular > speed_angular_max) {
                speed_angular = speed_angular_max;
            }
            else if (speed_angular < -speed_angular_max)
            {
                speed_angular = -speed_angular_max;
            }

            dprints("angular ---  %f\n", speed_angular * 360. / (2.*M_PI));
            dprints("vit:%f\n", neurone[def_groupe[gpe_rotational_speed].premier_ele].s1);
/*          if (neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 < 0.15)
				{
					speed_angular=0.;
				}*/
	    if (neurone[def_groupe[gpe_linear_speed].premier_ele].s1 < 0.15)
	    {
					speed_linear = 0.;
            }
			
            /* speed_linear=0.0;*/
            dprints("envoie com robot : %f - %f\n",speed_linear, speed_angular);

            robot_set_velocity(robot, speed_linear, 0., speed_angular, 1);

            dprints("fin envoie commande_robot\n");
        }
	else if ( robot->robot_type == 6 || robot->robot_type == 5)
	{                       /*real_speed_angular=0.; */



			if (gpe_linear_speed == -2)
			{
				speed_linear = 0.; /* c'est quoi l'interet de le mettre a O.O5 ??*/
			}
			else
			{
				/* printf("linear speed 0.05 + %f /100 =%f \n", speed_linear_max*neurone[def_groupe[gpe_linear_speed].premier_ele].s1 / 0.05 * 100., speed_angular); */

				/*Pour le pioneer */
				facteur = (tanh (4 * (neurone[def_groupe[gpe_linear_speed].premier_ele].s1 - 0.5)) + 1.) / 2.;
				if (facteur < 0.)
					facteur = 0.;
				speed_linear = speed_linear_max * facteur * facteur_gaz /* + 0.2*/;

			}
	
			if (gpe_rotational_speed != -2)
			{
		/*printf("angular speed  + %f /100 =%f \n",neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 * speed_angular_factor_max*100.,speed_angular);
				*/
				facteur = (tanh(4 * (neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 - 0.5)) + 1.) / 2.;
				if (facteur < 0.)
					facteur = 0.;
				speed_angular_factor = 0.10 + facteur * facteur_gaz * 0.25; /*entre 0.5 et 1 */
				speed_angular = speed_angular * speed_angular_factor;
			}
	


			if (speed_angular > 0.)
			{
				speed_angular = speed_angular + speed_angular_min;
			}
			else if (speed_angular < 0.)
			{
				speed_angular = speed_angular - speed_angular_min;
			}
	
			if (speed_angular > speed_angular_max) 
            {
				speed_angular = speed_angular_max;
			}
			else if (speed_angular < -speed_angular_max)
			{
				speed_angular = -speed_angular_max;
			}

			dprints("angular ---  %f\n", speed_angular * 360. / (2.*M_PI));
			dprints("vit:%f\n", neurone[def_groupe[gpe_rotational_speed].premier_ele].s1);
			
/* 				if (neurone[def_groupe[gpe_rotational_speed].premier_ele].s1 < 0.01)
				{
					speed_angular=0.;
				}*/
			if (neurone[def_groupe[gpe_linear_speed].premier_ele].s1 < 0.01)
			{
					speed_linear = 0.;
			}
			
			/* speed_linear=0.0;*/
			dprints("envoie com robot : %f - %f\n",speed_linear, speed_angular);

			robot_set_velocity(robot, speed_linear, 0., speed_angular, 1);

			dprints("fin envoie commande_robot\n");
	}
    }
   }
}
