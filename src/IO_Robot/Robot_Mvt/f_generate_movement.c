/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_navigation_movement.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Modification VB :  Maintenant la taille de ce groupe peut etre superieur a 1 neurone et correspond 
 a une partition des 360 degres  (si 120 neurones, alors chaque neurone code 3 degres		      
 Le neurone correspondant a la direction du mouvement reelement effectue prend une intensite egale  
 a la norme de ce mouvement (1 veut dire 30cm)						      
 L'extension de ce groupe contient les donnees odometriques renvoyes par le Braitenberg_follow      
 a savoir:  La norme, le beta_tot et le teta. Ces 3 informations peuvent ainsi etre utilisees	      
 par des groupes en aval (au hasard f_orientation)	
 
Macro:
-PI
-PI_2

Local variables:
-sortie_odometrie parametres_odometrie
-int pos_categorie
-float Orientation

Global variables:
-boolean emission_robot

Internal Tools:
-AngleTrigo()

External Tools: 
-tools/IO_Robot/Com_Koala/turn_left()
-tools/IO_Robot/Com_Koala/turn_right()
-tools/erreur/message()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: NC: messages in english

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Struct/sortie_odometrie.h>

#include "tools/include/macro.h"

#include "tools/include/local_var.h"
#include <Global_Var/IO_Robot.h>

#include <libhardware.h>
#include "tools/include/move_via_speed.h"
#include "tools/include/move_via_mvector_given_speed.h"
#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/trouver_entree.h>
#include <Kernel_Function/find_input_link.h>
#include "../Env_Simul/tools/include/turn_simul.h"
/*#define DEBUG*/
#ifdef ROUTEUR
/* la fonction powf n a pas ete trouvee par le gcc mipsel a l'edition de liens...*/
inline float powf(float x, float y)
{
    return exp(y * log(x));
}
#endif

#define ONLINE DEBUG
void function_generate_movement(int numero)
{
    int gpe_entree1, gpe_entree2 = -1;
    int inter, n;
    int increment;
    int deb_e, taille_groupe_e, taille_groupe_e2;
    int pos = -1;               /*,decalage,correspondant; */
    float max, angle_boussole, angle_voulu, mov_angle;
    float Fx, Fy, Si, puissance, angle;
    /*char c[255]; */
#ifdef DEBUG
    printf("-------------------------------------------------\n");
    printf("Creating the movement \n");
#endif
    gpe_entree1 = liaison[find_input_link(numero, 0)].depart;
    if (gpe_entree1 > -1)
    {
        gpe_entree2 = liaison[find_input_link(numero, 1)].depart;
/* si les liaisons 1 et 2 sont inversees */
        if (gpe_entree2 > -1)
        {
            if (gpe_entree2 != trouver_entree(numero,(char*) "compass"))
            {
                inter = gpe_entree1;
                gpe_entree1 = gpe_entree2;
                gpe_entree2 = inter;
                printf("f_nav_mvt: entree1:  %d; boussole: %d!\n", gpe_entree1, gpe_entree2);
            }
            printf("f_nav_mvt: f_orientation (compass) find at group: %d\n", gpe_entree2);
        }
        else
            printf("WARNING: f_nav_mvt: no f_orientation (compass) find !\n");
    }
    else
    {
        printf("Error in f_nav_mvt: no input find !\n");
    }

    /*gpe_entree1 : entree de la direction a suivre */
    /*gpe_entree1 : entree de la direction actuelle */

    /*on recherche le neurone max du gpe_entree1, indiquant la direction a suivre */
    taille_groupe_e = def_groupe[gpe_entree1].nbre;
    taille_groupe_e2 =
        def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley;
    increment = taille_groupe_e / taille_groupe_e2;
    deb_e = def_groupe[gpe_entree1].premier_ele;
    max = -9999.;
    for (n = deb_e + increment - 1;
         n <
         deb_e +
         def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley *
         increment; n += increment)
    {
        if (neurone[n].s > max)
        {
            max = neurone[n].s;
            pos = (n - deb_e) / increment;
#ifdef DEBUG
            printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
        }
    }

    if (max < 0.00001)
        max = 0.00001;

    angle_voulu =
        (float) (pos) / def_groupe[gpe_entree1].taillex /
        def_groupe[gpe_entree1].tailley;
    printf("Angle_voulu1 : %f\n", angle_voulu);


    Fx = 0.;
    Fy = 0.;
    puissance = 1.0;

    for (n = deb_e + increment - 1;
         n <
         deb_e +
         def_groupe[gpe_entree1].taillex * def_groupe[gpe_entree1].tailley *
         increment; n += increment)
    {
        Si = neurone[n].s;
        angle =
            ((float) ((n - deb_e) / increment) /
             def_groupe[gpe_entree1].taillex /
             def_groupe[gpe_entree1].tailley * 2 * PI);
        Fx = Fx + powf(Si / max, puissance) * cos(angle + PI);
        Fy = Fy + powf(Si / max, puissance) * sin(angle + PI);
        printf("Fx:%f   Fy:%f   angle:%f    Si:%f \n", Fx, Fy, angle, Si);
    }

    angle_voulu = (atan2(Fy, Fx) + PI) / (2. * PI);


    printf("Angle_voulu2 : %f\n", angle_voulu);
    

    /*on recherche le neurone max du gpe_entree2, indiquant la direction actuelle */
    taille_groupe_e = def_groupe[gpe_entree2].nbre;
    taille_groupe_e2 =
        def_groupe[gpe_entree2].taillex * def_groupe[gpe_entree2].tailley;
    increment = taille_groupe_e / taille_groupe_e2;
    deb_e = def_groupe[gpe_entree2].premier_ele;
    max = -9999.;
    for (n = deb_e + increment - 1;
         n <
         deb_e +
         def_groupe[gpe_entree2].taillex * def_groupe[gpe_entree2].tailley *
         increment; n += increment)
    {
        if (neurone[n].s > max)
        {
            max = neurone[n].s;
            pos = (n - deb_e) / increment;
#ifdef DEBUG
            printf("Max:%f Num:%d Pos:%d\n", max, n, pos);
#endif
        }
    }
    angle_boussole =
        (float) (pos) / def_groupe[gpe_entree2].taillex /
        def_groupe[gpe_entree2].tailley;


#ifdef ONLINE_DEBUG
    printf("compass= %f \t wanted= %f\n", angle_boussole, angle_voulu);
#endif

    mov_angle = -angle_voulu + angle_boussole;

    while ((mov_angle > 0.5) || (mov_angle < -0.5))
    {
        if (mov_angle > 0.5)
            mov_angle = mov_angle - 1;
        if (mov_angle < -0.5)
            mov_angle = mov_angle + 1;
    }
    if (vigilence < 0.1)
    {
        /*robot_turn_angle(robot_get_first_robot(),mov_angle*360./2);
           robot_go_by_position(robot_get_first_robot(),10,10);

           printf("odo: %f degree, avance= 5 \n",mov_angle*360./2);
           fp=fopen("odometry.SAVE","a");      
           fprintf(fp,"%f 10\n ",mov_angle);
           fclose(fp); */

        /*Pour cam_pano */

        /*
           if(abs(mov_angle*100)>30) mean            -0.3<mov_angle>0.3       
           {
           robot_turn_angle(robot_get_first_robot(),mov_angle*360.);
           }
           else if((mov_angle<-0.0625)||(mov_angle>0.0625))
           {
           if (mov_angle>0)
           robot_go_by_speed(robot_get_first_robot(),1,4);
           else if (mov_angle<=0)
           robot_go_by_speed(robot_get_first_robot(),4,1);
           }
           else
           {
           robot_go_by_speed(robot_get_first_robot(),4,4);
           } */

        /*     move_via_mvector_given_speed(mov_angle*360.,10.*333.,10.,&norme,&beta_tot,&teta);       */

        if (robot_get_first_robot()->robot_type == 1)
        {
            robot_turn_angle(robot_get_first_robot(), mov_angle * 360.);
            robot_go_by_speed(robot_get_first_robot(), 4, 4);
        }
        else if (robot_get_first_robot()->robot_type == 3)
        {
            robot_turn_angle(robot_get_first_robot(), mov_angle * 360.);
            robot_set_velocity(robot_get_first_robot(), 0.1, 0., 0., 1);
        }
        else
        {
            printf("a devel\n");
            exit(-1);
        }

        /*
           if(abs(mov_angle*100)>30)     
           {
           if(mov_angle>0)
           move_via_speed(-4,4,500.,&norme,&beta_tot);
           else if (mov_angle<0)
           move_via_speed(4,-4,500.,&norme,&beta_tot);
           }
           else if((mov_angle<-0.0625)||(mov_angle>0.0625))
           {
           if (mov_angle>0)
           move_via_speed(0,4,500.,&norme,&beta_tot);           
           else if (mov_angle<=0)       
           move_via_speed(4,0,500.,&norme,&beta_tot);

           }
           else
           {
           move_via_speed(4,4,500.,&norme,&beta_tot); */ /*robot_go_by_speed(robot_get_first_robot(),4,4); */
        /*                 
           }
         */
    }


    /* Ancien code pour koala */
    /* 
       if (mov_angle>0)
       turn_left(360*mov_angle);
       else if (mov_angle<=0)
       turn_right(-360*mov_angle);

       while(PreciseLastTargetReached()==0){;}
       GoToLeftRightUncond(30,30); */

/*GoToLeftRightBySpeedUncond(0,2);*/
/*}*/
}
