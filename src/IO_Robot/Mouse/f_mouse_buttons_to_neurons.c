/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
ingroup libIO_Robot
defgroup  f_mouse_buttons_to_neurons f_mouse_buttons_to_neurons

Author: A.Karaouzene
Created: 07/12/2012

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Fonction recupere les valeurs des bouttons de souris.
En dur 3 bouttons sont definis.

TODO :
Automatiser le nombre de bouttons et mettre un lien en entrée pour choisir quel boutton on veut comme pour le joystick.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG*/
#define MAX_NB_MOUSE_BUTTONS 5

#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>


typedef struct data_mouse_buttons_to_neurons
{
	int nb_buttons;
	int buttons_index[MAX_NB_MOUSE_BUTTONS];
	char mouse_name[256];
} data_mouse_buttons_to_neurons;


void new_mouse_buttons_to_neurons(int gpe)
{
	int index, link, ret;
	int nb_buttons = 3;
	char retour[256] = "\0";
	char mouse_name[256] = "\0";
	char *link_parse = NULL;
	data_mouse_buttons_to_neurons *my_data = NULL;

	(void) link_parse; // (unused)
	(void) ret; // (unused)

	dprints("%s (%s): Entering function\n", __FUNCTION__,def_groupe[gpe].no_name);

	if (def_groupe[gpe].data == NULL)
	{
		my_data = ALLOCATION(data_mouse_buttons_to_neurons);
		memset(my_data, 0, sizeof(data_mouse_buttons_to_neurons));

      index = 0;
      link = find_input_link(gpe, index);
      while (link != -1)
      {
		 if (prom_getopt(liaison[link].nom, "-m", retour) == 2)
		 {
				strcpy(mouse_name, retour);
		 }

		 index++;
		 link = find_input_link(gpe, index);
      }

		if (nb_buttons != def_groupe[gpe].nbre)
		{
			EXIT_ON_ERROR("Number of buttons is different from group size");
		}

		strcpy(my_data->mouse_name, mouse_name);
		my_data->nb_buttons = nb_buttons;
		def_groupe[gpe].data = my_data;
	}
}

void function_mouse_buttons_to_neurons(int gpe)
{
	int i, first;
	int value;
	float tmp;
	Mouse *mouse = NULL;
	data_mouse_buttons_to_neurons *my_data = (data_mouse_buttons_to_neurons *) def_groupe[gpe].data;

	(void) tmp; // (unused)

	if (my_data == NULL)
	{
		EXIT_ON_ERROR("Cannot retrieve data");
	}

	if (strlen(my_data->mouse_name) == 0)
	{
		mouse = mouse_get_first_mouse();
		if (mouse == NULL)
		{
			EXIT_ON_ERROR("No mouse defined");
		}
	}
	else
	{
		mouse = mouse_get_mouse_by_name(my_data->mouse_name);
		if (mouse == NULL)
		{
			EXIT_ON_ERROR("No mouse with name %s", my_data->mouse_name);
		}
	}

	first = def_groupe[gpe].premier_ele;

	for (i = 0; i < my_data->nb_buttons; i++)
	{
		value = mouse_get_button_value(mouse, i);
		neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = value;
		/*printf("f_mouse_buttons_to_neurons(%s): Button %i has value %i\n", def_groupe[gpe].no_name, i, value);*/
	}
/*	value = mouse_get_button_value(mouse, 3);
	tmp = (float)value / 50;
	neurone[first+3].s = neurone[first+3].s + tmp;
	if (neurone[first+3].s > 1 ) neurone[first+3].s = 1;
	if (neurone[first+3].s < 0 ) neurone[first+3].s = 0;
	neurone[first+3].s = neurone[first+3].s1 = neurone[first+3].s2 = neurone[first+3].s ;

	if (tmp !=0) printf("f_mouse_buttons_to_neurons(%s): Button 4 has value %f\n", def_groupe[gpe].no_name,neurone[first+3].s1);*/
}

void destroy_mouse_buttons_to_neurons(int gpe)
{
	if (def_groupe[gpe].data != NULL)
	{
		free(((data_mouse_buttons_to_neurons*) def_groupe[gpe].data));
		def_groupe[gpe].data = NULL;
	}
	dprints("destroy_mouse_buttons_to_neurons(%s): Leaving function\n", def_groupe[gpe].no_name);
}

