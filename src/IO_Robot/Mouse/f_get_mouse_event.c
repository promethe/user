/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdlib.h>
#include <string.h>


/*#define DEBUG*/
#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <stdio.h>
#include <IO_Robot/Mouse.h>

#include <fcntl.h>

#ifndef Darwin
#include <linux/input.h>

#define RIGHT_BUTTON 273
#define LEFT_BUTTON 272
#define MOLETTE 8

char mousefile[taille_chaine]; 
//char path_to_mouse[taille_chaine]="/home/neuro/hardware_files/mouse.hwm";
char path_to_mouse[taille_chaine]="/home/ali/Appli/TESTS/F_get_mouse/mouse.hwm";

void new_get_mouse_event (int gpe)
{
  FILE *f;
  long int fd;
  int error;
  
  f=fopen(path_to_mouse,"r");
  if(f==NULL)
  {
    kprints("ERROR gpe %s : opening mouse for f_get_mouse_event: %s \n",def_groupe[gpe].no_name, path_to_mouse);
    EXIT_ON_ERROR("%s %s\n",__FILE__,__FUNCTION__);
  }
  error=fscanf(f,"mouse is %s",mousefile);
  printf ("chemin du port souris %s \n",mousefile);
  if(error!=1)
  {
    kprints("ERROR gpe %s : reading file : %s error = %d\n",def_groupe[gpe].no_name, path_to_mouse,error);
    EXIT_ON_ERROR("%s %s\n",__FILE__,__FUNCTION__);
  }
  fclose(f);
  
   if((fd = open(mousefile, O_RDONLY | O_NONBLOCK)) == -1) 
    {
    kprints("ERROR %s %s \n",__FUNCTION__,mousefile);
    EXIT_ON_ERROR("%s %s %s\n",__FILE__,__FUNCTION__,mousefile);
    }
  
  def_groupe[gpe].data=(void*)fd;

  neurone[def_groupe[gpe].premier_ele+2].s = neurone[def_groupe[gpe].premier_ele+2].s1 = neurone[def_groupe[gpe].premier_ele+2].s2 = 0;
}



void function_get_mouse_event(int gpe)
{
	long int fd;
	struct input_event ie;
	int deb;
	int value;
	float tmp;
  
  deb = def_groupe[gpe].premier_ele;
    
 fd=(long int) def_groupe[gpe].data;
 if(fd==0)
 {
    kprints("%s ERROR gpe %s : fd=0 \n",__FUNCTION__, def_groupe[gpe].no_name);
    EXIT_ON_ERROR("%s %s\n",__FILE__,__FUNCTION__);   
 }


  if (read(fd, &ie, sizeof(struct input_event)))
    {
			if (ie.code == LEFT_BUTTON )
			{
				value = ie.value;
				neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = value;
			}
			if (ie.code == RIGHT_BUTTON)
			{
				value = ie.value;
				neurone[deb+1].s = neurone[deb+1].s1 = neurone[deb+1].s2 = value;
			}
			if (ie.code == MOLETTE)
			{
				value = ie.value;
				tmp = (float)value / 20;
				neurone[deb+2].s = neurone[deb+2].s + tmp;
				dprints(" origin = %f\tvaleur = %f\n", tmp,neurone[deb+2].s1);

				if (neurone[deb+2].s > 1 ) neurone[deb+2].s = 1;
				if (neurone[deb+2].s < 0 ) neurone[deb+2].s = 0;
				neurone[deb+2].s = neurone[deb+2].s1 = neurone[deb+2].s2 = neurone[deb+2].s ;

			}
	}

}
#endif

