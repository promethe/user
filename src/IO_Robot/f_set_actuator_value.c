/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************

Author: Pierre Delarboulas
Created: 01/02/2015

Description:
- Get a vector of sensor values from the sensor depending on definition in .dev.


*/

#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <dev.h>
#include <device.h>
#include <Components/xml_actuator.h>

#define MAXIMUM_SIZE_OF_OPTION 128

#define DEBUG

typedef struct box_actuator_set_actuator_value{
	Device *device;
	Xml_actuator *xml_actuator;
	float * actuator_value;
	int number_of_actuator;	
	type_neurone* neurons;
}Box_actuator_set_actuator_value;


void new_set_actuator_value(int index_of_group)
{
  Box_actuator_set_actuator_value *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
	Device *device=NULL;
	int i, index_of_link, success;
	
	type_groupe *group;
	type_liaison *link;

	/** On retrouve le groupe de neurones */
	group = &def_groupe[index_of_group];

	/** On creer la boite et l'associe au groupe */
	box = ALLOCATION(Box_actuator_set_actuator_value);
	group->data = box;

   	/** On recherche le lien entrant s'il existe. */
	index_of_link = find_input_link(index_of_group, 0);
	if (index_of_link == -1) link = NULL;
	else link = &liaison[index_of_link];
   	
	if (find_input_link(index_of_group, 1) != -1)  EXIT_ON_ERROR("Group: %d, has more than one incoming link !", index_of_group);


	/** On recherche le parametre id sur le lien  s'il y en a un.*/
	if (link!=NULL)
	{
		success = prom_getopt(link->nom, "id=", id_of_device);
		if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);
	}
	else success = 0;

	/** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
	if (success == 2) device = dev_get_device("xml_actuator", id_of_device);
	else if (success == 0) device = dev_get_device("xml_actuator", NULL);
	else /*if (success == 1)*/ EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

	/** There should be only one component in device*/
	if (device->number_of_components != 1) EXIT_ON_ERROR("Number of actuator (%d) should be 1.", device->number_of_components);

	/** On definit le net_sensor utilise */
	box->xml_actuator = (Xml_actuator*)device->components[0];

	/** On definit les neurones de la boite. */
	box->neurons = &neurone[group->premier_ele];
	
	/** nombre de senseurs attendu = nombre de neurones */
	box->number_of_actuator = group->nbre;
	
	box->actuator_value = MANY_ALLOCATIONS(box->number_of_actuator, float );

	/** init neurons with -1 (no values) */
	for(i=0;i<box->number_of_actuator;i++) {
		box->neurons[i].s1 = 0;
		box->neurons[i].s = 0;
		box->neurons[i].s2 = 0;
	}
}

void function_set_actuator_value(int index_of_group)
{
  Box_actuator_set_actuator_value *box;
  void (*set_actuator)(Xml_actuator*,float*, int); 
  int i,deb;
  Xml_actuator *xml_actuator;
  type_neurone *neurons_of_the_box;
  
  /** On recupere la boite a partir du groupe*/
  box = (Box_actuator_set_actuator_value*)def_groupe[index_of_group].data;
  
  /** On recupere les neurones de la boite et les moteurs*/
  neurons_of_the_box = box->neurons;
  xml_actuator = box->xml_actuator;
  set_actuator = xml_actuator->set_actuator; /** meme fonction pour tous les senseurs*/
	

  deb = def_groupe[ liaison[  find_input_link(index_of_group, 0)  ].depart ].premier_ele ;
  
  /** On recupere la valeur pour chaque senseur */
  for (i=0; i < box->number_of_actuator ; i++)
	{
		dprints("promethe : valeur [%d] : %f\n",i, neurons_of_the_box[i] );
//		box->actuator_value[i] = neurons_of_the_box[i].s1 = neurons_of_the_box[i].s =  neurons_of_the_box[i].s2 = neurons_of_the_box[i].coeff->val * neurone[ neurons_of_the_box[i].coeff->entree].s1  ;
		//box->actuator_value[i] =  neurons_of_the_box[i].s1 = neurons_of_the_box[i].s =  neurons_of_the_box[i].s2 = neurons_of_the_box[i].coeff->val * neurone[ neurons_of_the_box[i].coeff->entree].s1  ;
		box->actuator_value[i] =  neurons_of_the_box[i].s1 = neurons_of_the_box[i].s =  neurons_of_the_box[i].s2 = neurone[ deb+i ].s1  ;
	}
		
  set_actuator(xml_actuator, box->actuator_value  ,  box->number_of_actuator );
    
}

void destroy_set_actuator_value(int index_of_group)
{
  Box_actuator_set_actuator_value *box;
  box = (Box_actuator_set_actuator_value*)def_groupe[index_of_group].data;

  free( box->actuator_value ) ;
  free(def_groupe[index_of_group].data);
}

