/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_test_arm.c
 \brief simulates continuous movements of the Katana arm

 Author: P.Andry
 Created: 2002
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 20/07/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description: macro.h
 A set of neurons is created so as to simulate the motor
 information comming from the Katana arm.

 Each neuron has a sawtooth-like activity through
 time [0,1]

 Macro:
 - get_max_dof_katana_arm(): number of free DOF

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo only for scheduling
 - description: none
 - input expected group: none
 - where are the data?: none

 Comments:

 Known bugs: none (yet!)

 Todo:
 The activity of the different neurons should correspond to the
 joints according to the definition of the free logical_DOF
 (logical defined Degrees Of Freedom). See .dev file.


 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <libhardware.h>
#include <Macro/IO_Robot/macro.h>

void function_test_arm(int gpe)
{

  static int *sign = NULL;
  static float *value = NULL;
  static float *pas = NULL;
  int i;
  int size_Y = def_groupe[gpe].tailley;
  int size_X = def_groupe[gpe].taillex;
  int first_neuron = def_groupe[gpe].premier_ele;
  int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);

  float var;

  if (sign == NULL || value == NULL || pas == NULL)
  {
    if ((sign = malloc(sizeof(int) * (get_max_dof_katana_arm()))) == NULL)
    {
      printf("erreur\n");
      exit(1);
    }
    if ((value = malloc(sizeof(int) * get_max_dof_katana_arm())) == NULL)
    {
      printf("erreur\n");
      exit(1);
    }
    if ((pas = malloc(sizeof(int) * get_max_dof_katana_arm())) == NULL)
    {
      printf("erreur\n");
      exit(1);
    }
    for (i = get_max_dof_katana_arm() - 1; i >= 0; i--)
    {
      sign[i] = 1;
      value[i] = 0.5;
    }
  }

  /*verification */
  if ((size_X * size_Y) != get_max_dof_katana_arm())
  {
    printf("***function_test_arm (): Le Groupe doit comporter exactement %d neurones", get_max_dof_katana_arm());
    exit(299);
  }
  /*initialisation */

  var = (float) (rand() % 100) / 3000;
  /* printf("%f \n",var); */

  pas[0] = 0.01 /*+var */;
  pas[1] = 0.02 /*+var */;
  pas[2] = 0.02 /*+var */;
  pas[3] = 0.002 /*+var */;
  pas[4] = 0.00 /*+var */;

  for (i = 0; i < get_max_dof_katana_arm(); i++)
  {
    if (value[i] > 0.95) sign[i] = -1;
    if (value[i] < 0.05) sign[i] = 1;
  }
  for (i = first_neuron; i < last_neuron - 2; i++)
  {
    value[i - first_neuron] = value[i - first_neuron] + (sign[i - first_neuron]) * pas[i - first_neuron];
    neurone[i].s = neurone[i].s2 = neurone[i].s1 = value[i - first_neuron];

  }
  (void) var;
}
