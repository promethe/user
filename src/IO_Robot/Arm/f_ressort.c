/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  f_ressort.c
 \brief

 Author: Nils Beaussé
 Created: 15/04/2014
 Modified: 15/04/2014
 - author: Nils Beaussé
 - description: Firsts tests
 - date: 15/04/2014

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 - Simule la dynamique d'un ressort selon un axes dans le but de présenter de façon plus simple et lisible les travaux réalisé pour le dynamic
 muscle Per-Ac system.
 Prends en entrée la vitesse actuelle et calcule en sortie la vitesse future.

 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx


 Known bugs: none found yet...

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>

typedef struct {
  int no_group_m;
  int no_group_gainToAlpha;
  int no_group_alpha;
  int no_group_K;
  int no_group_A2;
  int no_group_A1;
  int no_group_Vitesse;
  float no_group_norme_m;
  float no_group_norme_gainToAlpha;
  float no_group_norme_alpha;
  float no_group_norme_K;
  float no_group_norme_A2;
  float no_group_norme_A1;
  float no_group_norme_Vitesse;
} ressort_data;

void new_ressort(int gpe)
{

  ressort_data *data; /* structure de cette fonction */
  int index = 0;
  int link;
  int no_group_m = -1, no_group_gainToAlpha = -1, no_group_alpha = -1, no_group_K = -1, no_group_A2 = -1, no_group_A1 = -1, no_group_Vitesse = -1;
  float no_group_norme_m = -1, no_group_norme_gainToAlpha = -1, no_group_norme_alpha = -1, no_group_norme_K = -1, no_group_norme_A2 = -1, no_group_norme_A1 = -1, no_group_norme_Vitesse = -1;
  char string[256];

  dprints("enter in %s\n", __FUNCTION__);

  index = 0;
  link = find_input_link(gpe, index);
  while (link != -1)
  {
    if (prom_getopt(liaison[link].nom, "A1", string) >= 1)
    {
      no_group_A1 = liaison[link].depart;
      no_group_norme_A1 = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "Vitesse", string) >= 1)
    {
      no_group_Vitesse = liaison[link].depart;
      no_group_norme_Vitesse = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "A2", string) >= 1)
    {
      no_group_A2 = liaison[link].depart;
      no_group_norme_A2 = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "K", string) >= 1)
    {
      no_group_K = liaison[link].depart;
      no_group_norme_K = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "alpha", string) >= 1)
    {
      no_group_alpha = liaison[link].depart;
      no_group_norme_alpha = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "gainToAlpha", string) >= 1)
    {
      no_group_gainToAlpha = liaison[link].depart;
      no_group_norme_gainToAlpha = liaison[link].norme;
    }
    if (prom_getopt(liaison[link].nom, "m", string) >= 1)
    {
      no_group_m = liaison[link].depart;
      no_group_norme_m = liaison[link].norme;
    }
    index++;
    link = find_input_link(gpe, index);
  }

  if (no_group_m < 1 || no_group_gainToAlpha < 1 || no_group_alpha < 1 || no_group_K < 1 || no_group_A2 < 1 || no_group_A1 < 1 || no_group_Vitesse < 1)
  {
    EXIT_ON_ERROR(" L'un des groupe d'entrée est non trouve : no_group_m %d no_group_gainToAlpha %d no_group_alpha %d no_group_K %d no_group_A2 %d no_group_A1 %d no_group_Vitesse %d", no_group_m, no_group_gainToAlpha, no_group_alpha, no_group_K, no_group_A2, no_group_A1, no_group_Vitesse);
  }

  if (def_groupe[gpe].nbre!=def_groupe[no_group_Vitesse].nbre)
  {
      EXIT_ON_ERROR(" Les tailles ne correspondent pas entre le groupe d'entrée (vitesse de tout les axes) et me groupe utilisé ici");
  }

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = ALLOCATION(ressort_data);

  data->no_group_A1=no_group_A1;
  data->no_group_A2=no_group_A2;
  data->no_group_m=no_group_m;
  data->no_group_gainToAlpha=no_group_gainToAlpha;
  data->no_group_alpha=no_group_alpha;
  data->no_group_K=no_group_K;
  data->no_group_Vitesse=no_group_Vitesse;

  data->no_group_norme_A1=no_group_norme_A1;
  data->no_group_norme_A2=no_group_norme_A2;
  data->no_group_norme_m=no_group_norme_m;
  data->no_group_norme_gainToAlpha=no_group_norme_gainToAlpha;
  data->no_group_norme_alpha=no_group_norme_alpha;
  data->no_group_norme_K=no_group_norme_K;
  data->no_group_norme_Vitesse=no_group_norme_Vitesse;


  /* passe les donnees a la fonction */
  def_groupe[gpe].data = data;

  dprints("end of %s\n", __FUNCTION__);
}


void function_ressort(int gpe)
{
  #define A1       (neurone[def_groupe[my_data->no_group_A1].premier_ele+(j%def_groupe[my_data->no_group_A1].nbre)].s1*my_data->no_group_norme_A1)
  #define A2       (neurone[def_groupe[my_data->no_group_A2].premier_ele+(j%def_groupe[my_data->no_group_A2].nbre)].s1*my_data->no_group_norme_A2)
  #define Vtp      (neurone[def_groupe[my_data->no_group_Vitesse].premier_ele+(j%def_groupe[my_data->no_group_Vitesse].nbre)].s1*my_data->no_group_norme_Vitesse)
  #define K        (neurone[def_groupe[my_data->no_group_K].premier_ele+(j%def_groupe[my_data->no_group_K].nbre)].s1*my_data->no_group_norme_K)
  #define alpha    (neurone[def_groupe[my_data->no_group_alpha].premier_ele+(j%def_groupe[my_data->no_group_alpha].nbre)].s1*my_data->no_group_norme_alpha)
  #define GtAlpha  (neurone[def_groupe[my_data->no_group_gainToAlpha].premier_ele+(j%def_groupe[my_data->no_group_gainToAlpha].nbre)].s1*my_data->no_group_norme_gainToAlpha)
  #define m        (neurone[def_groupe[my_data->no_group_m].premier_ele+(j%def_groupe[my_data->no_group_m].nbre)].s1*my_data->no_group_norme_m)

  ressort_data *my_data = NULL;
  int i=0,j=0;
  float Vt=0.0;

  dprints("enter in %s, group %d\n", __FUNCTION__, Gpe);
  my_data = (ressort_data *) def_groupe[gpe].data; /* recuperer les donnees du constructeur */

  for(i=def_groupe[gpe].premier_ele;i<(def_groupe[gpe].premier_ele+def_groupe[gpe].nbre);i++)
  {
    Vt=0.0;
    j=i-def_groupe[gpe].premier_ele;

    Vt=Vtp+(m*((K*(A1-A2))-(((K*GtAlpha)+alpha)*Vtp))); // equation du ressort (chaque axe independant)

    //a tester
    //printf("K %f, alpha %f, GtAlpha %f, m %f, Vtp %f, A1 %f, A2 %f\n",K,alpha,GtAlpha,m,Vtp,A1,A2);
    //printf("K neurone valeur s1 neurone concerne : %f\n",neurone[def_groupe[my_data->no_group_K].premier_ele+(j%def_groupe[my_data->no_group_K].nbre)].s1);
    //printf("neurone K %d, alpha %d, GtAlpha %d, m %d, Vtp %d \n",(def_groupe[my_data->no_group_K].premier_ele+(j%def_groupe[my_data->no_group_K].nbre)),(def_groupe[my_data->no_group_alpha].premier_ele+(j%def_groupe[my_data->no_group_alpha].nbre)),(def_groupe[my_data->no_group_gainToAlpha].premier_ele+(j%def_groupe[my_data->no_group_gainToAlpha].nbre)),(def_groupe[my_data->no_group_m].premier_ele+(j%def_groupe[my_data->no_group_m].nbre)),(def_groupe[my_data->no_group_Vitesse].premier_ele+(j%def_groupe[my_data->no_group_Vitesse].nbre)));
    //printf("groupe entree K %d, Norme K %f, groupe rattache neurone concerne %d \n", my_data->no_group_K, my_data->no_group_norme_K,neurone[def_groupe[my_data->no_group_K].premier_ele+(j%def_groupe[my_data->no_group_alpha].nbre)].groupe);
    //printf("j pourcent def_groupe[my_data->no_group_alpha].nbre)-1 %d \n",j%def_groupe[my_data->no_group_alpha].nbre);
    //printf("j %d \n",j);


    neurone[i].s=neurone[i].s1=neurone[i].s2=Vt;
  }

  dprints("end of %s, group %d\n", __FUNCTION__, Gpe);
}


void destroy_ressort(int gpe)
{
  dprints("destroy_ressort(%s): Entering function\n", def_groupe[gpe].no_name);
    if (def_groupe[gpe].data != NULL)
    {
       free(((ressort_data *) def_groupe[gpe].data));
       def_groupe[gpe].data = NULL;
    }
    dprints("destroy_ressort(%s): Leaving function\n", def_groupe[gpe].no_name);
}


