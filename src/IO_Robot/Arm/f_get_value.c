/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_get_value.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>

void function_get_value(int num)
{
    int size_Y = def_groupe[num].tailley;
    int size_X = def_groupe[num].taillex;
    int size = (size_X * size_Y);
    int first_neuron = def_groupe[num].premier_ele;
    int last_neuron = def_groupe[num].premier_ele + size;
    char *string;
    mem_entree *data_g;
    int val = 0, ent = 0, i;
    /*! recherche  des liens */
    if (def_groupe[num].ext == NULL)
    {
        for (i = 0; i < nbre_liaison; i++)
            if (liaison[i].arrivee == num)
            {
                string = liaison[i].nom;
                val = atoi(string);
                ent = def_groupe[liaison[i].depart].premier_ele + val;
            }

        data_g = (mem_entree *) malloc(sizeof(mem_entree));
        (*data_g).entree_1 = ent;
        (*data_g).entree_2 = val;
        def_groupe[num].ext = (void *) data_g;
    }
    else
    {
        data_g = (mem_entree *) def_groupe[num].ext;
        ent = (*data_g).entree_1;
        val = (*data_g).entree_2;
    }
    for (i = first_neuron; i < last_neuron; i++)
        neurone[i].s = neurone[i].s1 = neurone[i].s2 =
            neurone[ent + (i - first_neuron)].s2;


}
