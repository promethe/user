/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_get_arm_position.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   group of neuron whose activity is 
 the angles of the motors of the arm.
 A kind of proprioception.

Macro:
-get_max_dof_katana_arm()
-Free

Local variables:
-none

Global variables:
-int logical_DOF[get_max_dof_katana_arm()]
-float Range[get_max_dof_katana_arm()]
-float Min[get_max_dof_katana_arm()]

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Macro/IO_Robot/macro.h>

#include <Global_Var/IO_Robot.h>
#include <../hardware/include/Arm.h>

#include <libhardware.h>

void function_get_arm_position(int gpe)
{
    static float *pos = NULL;

    int i;
    int d;
    int pos_temp;
    int size_Y = def_groupe[gpe].tailley;
    int size_X = def_groupe[gpe].taillex;
    int first_neuron = def_groupe[gpe].premier_ele;
    int last_neuron = def_groupe[gpe].premier_ele + (size_X * size_Y);
    if (pos == NULL)
    {
        if ((pos = malloc(sizeof(float) * get_max_dof_katana_arm())) == NULL)
        {
            printf("erreur\n");
            exit(1);
        }
    }
    for (i = 0; i < get_max_dof_katana_arm(); i++)
    {
        if (get_logical_dof(i) == get_free())
        {
            d = i + 1;

            pos_temp = motor_position(&d);
            pos[i] = ((float) (pos_temp - get_Min(i))) / get_Range(i);

        }
    }
    pos[4] = 0.0;
    pos[3] = 0.0;
    for (i = first_neuron; i < last_neuron; i++)
    {
        if (pos[i - first_neuron] <= 0.0)
            pos[i - first_neuron] = 0.0;

        neurone[i].s2 = neurone[i].s1 = neurone[i].s = pos[i - first_neuron];
    }

}
