/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_proprio_arm.c 
\brief 

Author: Sofiane Boucenna
Created: 
Modified: 
- author:
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-donne la proprioception du bras katana
-liens : -first : numero du premier servo (1<= . <= nombre total de servos)
	            seuls les servos correctement definis seront controles
	 par defaut : first == 1 
- RQ : la proprio est recuperee pour les neurones du groupe :
 normalement : 6 servos sur un bras Katana donc 6 neurones dans ce groupe.

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet...

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>



/* numero total de servos */
/*#define DEBUG_to_File*/
/*#define DEBUG*/

#include <net_message_debug_dist.h>



typedef struct
{
  int gpe_amounte;            /* groupe de liaision precedant */
  int longueur;
  int deb;                    /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int inputGpe;               /* groupe d'entree pour la liaisoin en arriere */
  Joint *joint_servo;         /* structure de promethe */
  int num_servos;
  int first_servo;
} arm_data;


void new_proprio_arm(int Gpe)
{
  arm_data *data;      /* structure de cette fonction */
  int first_servo = 1;
  char param[256];
  
  dprints("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
  
  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = ALLOCATION(arm_data);
  
  /* on charge le ficher avec la description de vitesse et position de chaque servo */
  data->gpe_amounte = find_input_link(Gpe, 0);

  if (data->gpe_amounte == -1)
    {
      EXIT_ON_ERROR(" Error, %s group %i doesn't have an input\n", __FUNCTION__,
	     Gpe);
    }

  if(prom_getopt(liaison[data->gpe_amounte].nom,"first",param)==2) {
    first_servo = atoi(param);
    if(first_servo<1) 
      EXIT_ON_ERROR("first servo incorrect (%d<1) \n",
	    first_servo);
  }
  
  
  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->inputGpe = liaison[data->gpe_amounte].depart;
  data->deb = def_groupe[data->inputGpe].premier_ele;
  data->longueur = def_groupe[data->inputGpe].nbre;
  data->num_servos = def_groupe[Gpe].nbre;
  data->first_servo = first_servo;
  
  
  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;

  dprints("~~~~~~~~~~~end of %s\n", __FUNCTION__);
}

/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/


void function_proprio_arm(int Gpe)
{
  /* initialisation des variables */
  int i;
  int deb;
  arm_data *my_data = NULL;    /* la structure de cette fonction */
  char moteur[] = "motor";
  char param[256];
  /* fin de l'initialisation des variables */
/*   char moteur[NUM_SERVOS][256]={"motor1","motor2","motor3","motor4","motor5","motor6"}; */


  dprints("~~~~~~~~~~~enter in %s, group %d\n", __FUNCTION__, Gpe);



#ifdef	DEBUG_to_File
  FILE* fp=NULL;
  fp = fopen("valeur_proprio.txt","a");
  if(fp==NULL)
    {	
      printf("I can't open the file TXT ...!\n");
      exit(1);
    }
#endif



  my_data = (arm_data *) def_groupe[Gpe].data; /* recuperer les donnes du constructeur */
  
  if (my_data == NULL)
    {
      EXIT_ON_ERROR("error while loading data in group %d %s\n", Gpe,
	     __FUNCTION__);
    }
  
  
  deb=def_groupe[Gpe].premier_ele;
  /* retourne la proprio de chaque servo */
  for (i = 0; i < my_data->num_servos; i++)
    {
/*      printf("moteur[%d]=%s\n",i,moteur[i]);*/
      /* mis en forme (structure Joint) pour appeller la fonction de proprioception */

      if(sprintf(param,"%s%d",moteur,my_data->first_servo+i)) {
	if ((my_data->joint_servo = joint_get_serv_by_name(param))!=NULL) {
	  joint_get_proprio(my_data->joint_servo);

	  neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = my_data->joint_servo->proprio;
	  
#ifdef	DEBUG_to_File
	  fprintf(fp,"%f ",my_data->joint_servo->proprio);
#endif
	  
	  /* printf("PROPRIO------->moteur_%d=%f\n",i,neurone[deb+i].s); */
	  
#ifdef DEBUG
	  printf("PROPRIO------->moteur_%d=%f\n",i,neurone[deb+i].s);
#endif
	}
      }
    }
  /*printf("\n");*/

#ifdef	DEBUG_to_File
  fprintf(fp,"\n");
  fclose(fp);
#endif


#ifdef DEBUG
  printf("~~~~~~~~~~~end of %s, group %d\n", __FUNCTION__, Gpe);
#endif
  
}
