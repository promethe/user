/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_object.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: S. Boucenna
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: Cette fonction test s il y a un object dans la pince

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments: 

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>

typedef struct
{
  int gpe_amounte;            /* groupe precedant*/
  Sensor *sens;
  float *capteur;
}arm_data;

void new_object(int gpe)
{
  arm_data *data;      /* structure de cette fonction */
  int i = 0;
  int l = -1;
  int gpe_amounte = -1;
  int valeur_capteur = -1;
  char sensor[1][256] = {"capteur_pince"};

#ifdef DEBUG
  printf("######enter in %s\n", __FUNCTION__);
#endif
  
  /*
   *on cree la structure qui contiendra 
   *les donnees
   */
  
  data = (arm_data *) malloc(sizeof(arm_data));
  if (data == NULL)
    {
      printf("error in memory allocation in group %d %s\n", gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  
  
  l = find_input_link(gpe, i);

  while (l != -1)
    {
      if (strcmp(liaison[l].nom, "pince") == 0)
	{
	  gpe_amounte = data->gpe_amounte = liaison[l].depart;
	}
      l = find_input_link(gpe, i);
      i++;
    }

  if (gpe_amounte == -1)
    {
      printf(" Error, %s group %i doesn't have an input\n", __FUNCTION__,
	     gpe);
      exit(EXIT_FAILURE);
    }
  
  
  data->sens = sensor_get_sens_by_name(sensor[0]);
  valeur_capteur = sensor_sens_command(data->sens);

  data->capteur=malloc(data->sens->nb_sensor*sizeof(float));  

  if((int)valeur_capteur != 2)
    printf("ATTENTION: pb dans la fonction f_sensor_arm\n");

  for(i=0 ; i<data->sens->nb_sensor ; i++)
    {
      data->capteur[i] = data->sens->vecteur_sensor[i]; 
      printf("user: sensor[%i]=%f\n",i,data->sens->vecteur_sensor[i]); 
    }

  /* passer les donnes a la fonction suivant */
  def_groupe[gpe].data = data;
  
#ifdef DEBUG
  printf("######end of %s\n", __FUNCTION__);
#endif
  
}

void function_object(int gpe)
{
    int i, deb, nbre;
    /* int capteur_init[16] = {166,128,255,201,11,56,85,91,143,68,255,178,70,98,99,95}; */
    float difference = 0;
    float accu = 0;
    arm_data *my_data = NULL;
    my_data = (arm_data *) def_groupe[gpe].data;

    deb = def_groupe[my_data->gpe_amounte].premier_ele;
    nbre = def_groupe[my_data->gpe_amounte].nbre;
#ifdef DEBUG
    printf("----> groupe precedent = %s\n",def_groupe[my_data->gpe_amounte].no_name );
    for(i=0 ; i<my_data->sens->nb_sensor ; i++)
      { 
	printf("objet: sensor[%i]=%f\n",i,my_data->capteur[i]); 
      }
#endif
    for (i = deb ; i < deb + nbre; i++)
      {
	difference = neurone[i].s - my_data->capteur[i-deb]/* capteur_init[i-deb] */;
	if(difference<0)
	  difference = -difference;
	
	accu=accu+difference;
    }
    
    if(accu < 3)
      {
	neurone[def_groupe[gpe].premier_ele].s = 
	  neurone[def_groupe[gpe].premier_ele].s1 = 
	  neurone[def_groupe[gpe].premier_ele].s2 = 0;
	 printf("Pas d object=%f\n",accu); 
      }
    else
      {
	neurone[def_groupe[gpe].premier_ele].s = 
	  neurone[def_groupe[gpe].premier_ele].s1 = 
	  neurone[def_groupe[gpe].premier_ele].s2 = 1;
	printf("object=%f\n",accu); 
      }
}
