/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_phi_arm.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Send a position comand to motors 1, 2 and 3 according
 * to the reading of verticle and horizontal vector of movement.
 * these vectors are suggested by two <b>d_phi</b> input groups.
 * they are linked to this group via algo links. One link
 * name is "horizontal" the other is "vertical".
 * Proprioception is read in  two other group indicated by
 * "m,1" and "m,2" links.

 Macro:
 -none

 Local varaibles:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -tools/IO_Robot/Arm/set_arm_rotation()
 -tools/IO_Robot/Arm/set_motor_velocity()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <Struct/mem_entree.h>
#include <stdlib.h>
#include <string.h>

#include <libhardware.h>

void function_phi_arm(int num)
{

  int i, num_e1 = 0, num_e2 = 0, num_e3 = 0, num_e4 = 0, num_e5 = 0, num_e6 = 0, cpt;
  int dep1, dep2;
  int pos_v, pos_h;
  int entree_v, entree_h;
  int size_h, size_v;
  int indice_h, indice_v;
  int d_h, d_v;
  int first_neuron = def_groupe[num].premier_ele;
  float teta1, teta2;
  char *string;
  mem_entree *data_g;
  float val_h, val_v;
  float Seuil_mvt = 0.005;

  /*! recherche  des liens */
  if (def_groupe[num].ext == NULL)
  {
    cpt = 0;
    for (i = 0; i < nbre_liaison; i++)
      if (liaison[i].arrivee == num)
      {
        string = liaison[i].nom;
        if (strstr(string, "horizontal") != NULL)
        {
          num_e1 = i;
          cpt++;
        }
        if (strstr(string, "vertical") != NULL)
        {
          num_e2 = i;
          cpt++;
        }
        if (strstr(string, "phi_h") != NULL)
        {
          num_e3 = i;
          cpt++;
        }
        if (strstr(string, "phi_v") != NULL)
        {
          num_e4 = i;
          cpt++;
        }
        if (strstr(string, "m,1") != NULL)
        {
          num_e5 = i;
          cpt++;
        }
        if (strstr(string, "m,2") != NULL)
        {
          num_e6 = i;
          cpt++;
        }
      }
    if (cpt != 6)
    {
      printf("\n function_phi_arm() erreur: nombre de liens amont incorects: %d !\n ", cpt);
      exit(0);
    }

    data_g = (mem_entree *) malloc(sizeof(mem_entree));
    (*data_g).entree_1 = pos_h = liaison[num_e1].depart;
    (*data_g).entree_2 = pos_v = liaison[num_e2].depart;
    (*data_g).entree_3 = entree_h = liaison[num_e3].depart;
    (*data_g).entree_4 = entree_v = liaison[num_e4].depart;
    (*data_g).entree_5 = dep1 = def_groupe[liaison[num_e5].depart].premier_ele;
    (*data_g).entree_6 = dep2 = def_groupe[liaison[num_e6].depart].premier_ele;
    def_groupe[num].ext = (void *) data_g;
    neurone[first_neuron].s2 = 0.5;
    neurone[first_neuron].s1 = 0.5;
  }
  else
  {
    data_g = (mem_entree *) def_groupe[num].ext;
    pos_h = (*data_g).entree_1;
    pos_v = (*data_g).entree_2;
    entree_h = (*data_g).entree_3;
    entree_v = (*data_g).entree_4;
    dep1 = (*data_g).entree_5;
    dep2 = (*data_g).entree_6;
  }

  /* position du bras: neurone active des representations verticales et horizontales */
  size_h = def_groupe[pos_h].nbre;
  size_v = def_groupe[pos_v].nbre;

  if ((size_h != def_groupe[entree_h].nbre) || (size_h != def_groupe[entree_h].nbre))
  {
    printf("\n function_phi_arm() erreur: les groupes en entree n'ont pas les memes tailles !\n ");
    exit(0);
  }
  /*recherche de l'indice du  neurone horizontal active */
  indice_h = -1;
  for (i = def_groupe[pos_h].premier_ele; i < def_groupe[pos_h].premier_ele + size_h; i++)
  {
    if (isequal(neurone[i].s2, 1.)) indice_h = i - def_groupe[pos_h].premier_ele;
  }
  /*printf("winner horizontal %d \n",indice_h); */
  /*recherche de l'indice du neurone vertical active */
  indice_v = -1;
  for (i = def_groupe[pos_v].premier_ele; i < def_groupe[pos_v].premier_ele + size_v; i++)
  {
    if (isequal(neurone[i].s2, 1.)) indice_v = i - def_groupe[pos_v].premier_ele;
  }
  /*printf("winner vertical %d \n",indice_v); */
  /*on recupere l'activite de d_phi sur le neurone correspondant d'indice: indice_h */

  d_h = def_groupe[entree_h].premier_ele + indice_h;
  d_v = def_groupe[entree_v].premier_ele + indice_v;

  if (indice_h >= 0) val_h = neurone[d_h].s2;
  else val_h = 0.0;
  if (indice_v >= 0) val_v = neurone[d_v].s2;
  else val_v = 0.0;

  if (fabs(val_h) <= Seuil_mvt)
  {
    if (isequal(neurone[first_neuron].s2, 1.0) || isequal(neurone[first_neuron].s2, 0.0))
    {
      neurone[first_neuron].s2 = 0.5;
      set_arm_rotation(neurone[dep1].s2, -1, -1, -1, -1);
    }
    else /*printf("on est a l'arret (but %f)!\n",neurone[first_neuron].s2) */
    {
      ;
    }
  }
  else
  {
    if ((val_h > 0) && isdiff(neurone[first_neuron].s2, 1.0))
    {
      neurone[first_neuron].s2 = 1.0;
      set_arm_rotation(1.0, -1, -1, -1, -1);
    }
    if ((val_h < 0) && isdiff(neurone[first_neuron].s2, 0.0))
    {
      neurone[first_neuron].s2 = 0.0;
      set_arm_rotation(0.01, -1, -1, -1, -1);
    }
    teta1 = (53 - (40 * fabs(val_h) * 53)) + 10;
    set_motor_velocity(1, 70, 70, 6, (int) teta1);
  }

  if (fabs(val_v) <= Seuil_mvt)
  {
    if (isequal(neurone[first_neuron].s1, 1.0) || isdiff(neurone[first_neuron].s1, 0.0))
    {
      neurone[first_neuron].s1 = 0.5;
      set_arm_rotation(-1, neurone[dep2].s2, neurone[dep2 + 1].s2, -1, -1);
    }
    else /*printf("on est a l'arret (but %f)!\n",neurone[first_neuron].s1) */
    {
      ;
    }
  }
  else
  {
    if ((val_v > 0) && isdiff(neurone[first_neuron].s1, 1.0))
    {
      neurone[first_neuron].s1 = 1.0;
      set_arm_rotation(-1, 1.0, 0.01, -1, -1);
    }
    if ((val_v < 0) && isdiff(neurone[first_neuron].s1, 0.0))
    {
      neurone[first_neuron].s1 = 0.0;
      set_arm_rotation(-1, 0.01, 1.0, -1, -1);
    }
    teta2 = (53 - (40 * fabs(val_v) * 53)) + 10;
    set_motor_velocity(2, 70, 70, 6, (int) teta2);
    set_motor_velocity(3, 70, 70, 6, (int) teta2);
  }

}
