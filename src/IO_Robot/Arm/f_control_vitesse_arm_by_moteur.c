/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_control_vitesse_arm_by_moteur.c 
\brief 

Author: Sofiane Boucenna
Created: 
Modified: 
- author:
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-fait un controle en vitesse du katana
 sur le moteur qu on lui donne en argument

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet...

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>



/* numero total de servos */
#define NUM_SERVOS 6
/*#define DEBUG*/





typedef struct
{
  int gpe_amounte;            /* groupe de liaision precedant */
  int longueur;               /* longueur de la chaine de neurones (chaque neurone est une expression */
  int deb;                    /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int inputGpe;               /* groupe d'entree pour la liaisoin en arriere */
  Joint *joint_servo;         /* structure de promethe */
  int numero_servo;           /* numero du servo actif*/ 
} arm_data;


void new_control_vitesse_arm_by_moteur(int Gpe)
{
  arm_data *data;      /* structure de cette fonction */
  int i = 0;
  int l = -1;
#ifdef DEBUG
  printf("######enter in %s\n", __FUNCTION__);
#endif
  
  /*
   *on cree la structure qui contiendra 
   *les donnees
   */
  
  data = (arm_data *) malloc(sizeof(arm_data));
  if (data == NULL)
    {
      printf("error in memory allocation in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  
  
  l = find_input_link(Gpe, i);
  while (l != -1)
    {
      if (strcmp(liaison[l].nom, "joint0") == 0)
	{
	  data->gpe_amounte = liaison[l].depart;
	  data->numero_servo = 0;
	}
      if (strcmp(liaison[l].nom, "joint1") == 0)
	{
	   data->gpe_amounte= liaison[l].depart;
	   data->numero_servo = 1;
	}
      if (strcmp(liaison[l].nom, "joint2") == 0)
	{
	  data->gpe_amounte = liaison[l].depart;
	  data->numero_servo = 2;
	}
      if (strcmp(liaison[l].nom, "joint3") == 0)
	{
	   data->gpe_amounte= liaison[l].depart;
	   data->numero_servo = 3;
	}
      if (strcmp(liaison[l].nom, "joint4") == 0)
	{
	  data->gpe_amounte = liaison[l].depart;
	  data->numero_servo = 4;
	}
      if (strcmp(liaison[l].nom, "joint5") == 0)
	{
	  data->gpe_amounte= liaison[l].depart;
	  data->numero_servo = 5;
	}
      
      
      l = find_input_link(Gpe, i);
      i++;
    }
  printf("on a recuperer les parametres\n"); 

  if (data->gpe_amounte == -1)
    {
      printf(" Error, %s group %i doesn't have an input\n", __FUNCTION__,
	     Gpe);
      exit(EXIT_FAILURE);
    }
  
  
  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  /*data->inputGpe = liaison[data->gpe_amounte].depart;*/
  data->deb = def_groupe[data->gpe_amounte].premier_ele;
  data->longueur = def_groupe[data->gpe_amounte].nbre;
  
  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;

#ifdef DEBUG
  printf("######end of %s\n", __FUNCTION__);
#endif
  
  
}

/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/


void function_control_vitesse_arm_by_moteur(int Gpe)
{
  
  int deb;
  arm_data *my_data = NULL;    
  char moteur[NUM_SERVOS][256]={"motor1","motor2","motor3","motor4","motor5","motor6"};
  

#ifdef DEBUG
  printf("##########enter in %s, group %s\n", __FUNCTION__, def_groupe[Gpe].no_name);
#endif

  
  my_data = (arm_data *) def_groupe[Gpe].data; 
  deb=my_data->deb;
  if (my_data == NULL)
    {
      printf("error while loading data in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
#ifdef DEBUG 
  printf("groupe precedent=%s\n",def_groupe[my_data->gpe_amounte].no_name);
  printf("Servo utilise=%d\n",my_data->numero_servo);
#endif

  /* mis en forme (structure Joint) pour appeller la fonction de speedo_command */
  my_data->joint_servo = joint_get_serv_by_name(moteur[my_data->numero_servo]);
  
  /*juste pour savoir dans quel sens on tourne*/
  
  if(neurone[deb].s2 < neurone[deb+1].s2)   /*avant neurone[deb].s1 < neurone[deb+1].s1*/
    {
#ifdef DEBUG
      printf("0<1,gpe=%s\n",def_groupe[Gpe].no_name);
      printf("neurone0=%f\n",neurone[deb].s2);
      printf("neurone1=%f\n",neurone[deb+1].s2);
#endif
      joint_speedo_command(my_data->joint_servo,-1);

    }
  else
    {
#ifdef DEBUG
      printf("0>1,gpe=%s\n",def_groupe[Gpe].no_name);
      printf("neurone0=%f\n",neurone[deb].s2);
      printf("neurone1=%f\n",neurone[deb+1].s2);
#endif
      joint_speedo_command(my_data->joint_servo,1);
    }
      
    
  
#ifdef DEBUG
  printf("##########end of %s, group %s\n", __FUNCTION__, def_groupe[Gpe].no_name);
#endif
  
}
