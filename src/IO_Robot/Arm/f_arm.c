/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_arm.c 
\brief 

Author: Sofiane Boucenna
Created: 
Modified: 
- author:
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-control le bras katana
-liens : action : les neurones donnant la position a atteindre
           option sur le lien action :
           -first : numero du premier servo (1<= . <= nombre total de servos)
	            seuls les servos correctement definis seront controles
	 par defaut : first == 1 
         enable : (sic)
- RQ : normalement : 6 servos sur un bras Katana donc 6 neurones en input.

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet...

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>



/* numero total de servos */
#define SEUIL_MVT 0.0001
/* #define DEBUG    1 */

#include <net_message_debug_dist.h>


typedef struct
{
  int gpe_amounte_enable;  /* enable */
  int longueur;               /* longueur de la chaine de neurones (chaque neurone est une expression */
  int deb;                    /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int input_gpe;               /* groupe d'entree */
  Joint *joint_servo;         /* structure de promethe */
  int num_servos;
  int first_servo;
  float *neurones_passes;
} arm_data;


void new_arm(int Gpe)
{

  arm_data *data;      /* structure de cette fonction */  
  int i = 0;
  int l;
  int input_gpe=-1;
  int gpe_amounte_enable=-1;
  int first_servo = 1;
  int num_servos = -1;
  char *chaine;
  char param[256];

  dprints("enter in %s\n", __FUNCTION__);

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = ALLOCATION(arm_data);
  
  l = find_input_link(Gpe, i);
  while (l != -1)
    {
      if (strcmp(liaison[l].nom, "learn") == 0 || strcmp(liaison[l].nom, "enable") == 0 ) /*position control enable*/
	{
	  gpe_amounte_enable = data->gpe_amounte_enable = liaison[l].depart;
	}
      if ((chaine = strstr(liaison[l].nom, "action")) != NULL) /*values for arm position*/
	{ 
	  /* on charge le ficher avec la description de vitesse et position de chaque servo */
	  input_gpe = data->input_gpe = liaison[l].depart;
	  num_servos = def_groupe[input_gpe].nbre;
	  
	  if(prom_getopt(chaine,"first",param)==2) {
	    first_servo = atoi(param);
	    if(first_servo<1) 
	      EXIT_ON_ERROR("first servo incorrect (%d<1) \n",
		    first_servo);
	  }
	  
	}
      l = find_input_link(Gpe, i);
      i++;
    }

  
  if (data->input_gpe == -1)
    {
      EXIT_ON_ERROR(" Error, %s group %i doesn't have an input\n", __FUNCTION__,
	     Gpe);
    }

  if (data->gpe_amounte_enable == -1)
    {
      EXIT_ON_ERROR(" Error, %s group %i doesn't have an input(enable)\n", __FUNCTION__,
	     Gpe);
    }

  dprints("%s (%d) : control %d servos beginning at  %d servo \n",__FUNCTION__,Gpe, num_servos,first_servo);

  data->neurones_passes = MANY_ALLOCATIONS(num_servos,float);


  /*on alloue le tableau qui memorise la valeur de la position passee */
  for(i = 0 ; i <num_servos  ; i++)
    data->neurones_passes[i] = -10000000;
 
  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->deb = def_groupe[data->input_gpe].premier_ele;
  data->longueur = def_groupe[data->input_gpe].nbre;
  data->num_servos = num_servos;
  data->first_servo = first_servo;
	
	    
  
  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;


  /* initialisation du bras ( en vitesse )*/
  arm_speed_init_command(); /* dans le fichier joint_speedo_command.c*/

  dprints("end of %s\n", __FUNCTION__);
}

/*
 *
 *FIN DU CONSTRUCTEUR
 *
 */


void function_arm(int Gpe)
{
  /* initialisation des variables */
  int i=-1;
  arm_data *my_data = NULL;    /* la structure de cette fonction */
  float difference;
  char moteur[] = "motor";
  char param[256];
  int deb=-1;
  /* fin de l'initialisation des variables */
  /*   char moteur[NUM_SERVOS][256]={"motor1","motor2","motor3","motor4","motor5","motor6"}; */

  dprints("enter in %s, group %d\n", __FUNCTION__, Gpe);
  
  my_data = (arm_data *) def_groupe[Gpe].data; /* recuperer les donnes du constructeur */
  deb=my_data->deb;  

   if (my_data == NULL) 
     { 
       EXIT_ON_ERROR("error while loading data in group %d %s\n", Gpe, 
 	     __FUNCTION__);
     } 

 
   if(neurone[def_groupe[my_data->gpe_amounte_enable].premier_ele].s1>0.5)
      /* donner des commandes a chaque servo */

     for (i = 0; i < my_data->num_servos; i++) {

	if(sprintf(param,"%s%d",moteur,my_data->first_servo+i)) {


	  /* mis en forme (structure Joint) pour appeller la fonction joint_servo_command */
	  if ((my_data->joint_servo = joint_get_serv_by_name(param))!=NULL) {
	    
	    dprints("neurone[%d]=%f",i,neurone[deb+i].s);
	    /* commander chaque servo */
	    
	    difference = fabs(my_data->neurones_passes[i]-neurone[deb+i].s);
	    printf("--- %f difference %f\n",neurone[deb+i].s, difference );
	    
	    if(difference > SEUIL_MVT)
	      { 
		dprints("****\n");
		joint_servo_command(my_data->joint_servo,neurone[deb+i].s,10);
		my_data->neurones_passes[i] = neurone[deb+i].s;
	      }
	    
	    /*printf("neurone[%d]=%f\n",i,neurone[deb+i].s);*/
	  }
	}
     }
  
  /* printf("---------------------------\n"); */
  dprints("end of %s, group %d\n", __FUNCTION__, Gpe);
}
