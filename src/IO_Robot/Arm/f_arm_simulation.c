/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
f_arm_simulation.c
**/

#include <libx.h>
/*#include <graphic_Tx.h>*/

#include <Kernel_Function/trouver_entree.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
#include <stdlib.h>
#include <Struct/prom_images_struct.h>
#include <public_tools/Vision.h>
#include <NN_Core/rien.h>
/*#include <NN_Core/Limbic/calcule_neuromodulation.h>*/


#define pi2 6.283185314
#define FAILURE 0
#define SUCCESS 1
#define MAXLEN 640
#define OFF 0
#define ON 1
#define LEN 120
#define OBJECT_MAX 2
#define OBJECT_MIN 1.25

#define Xcentre	400
#define Ycentre	500

int libre_totalement = -1;

/* x y dans ce moument */
int x_act;
int y_act;

typedef struct ARM_INFO{

	int nbr_links;

}arm_info;

typedef struct LINK { 
float len;
float theta;
float max_theta;
float min_theta;
float phi;
struct LINK *next;
}Link;

typedef struct POINT {
	float rho;
	float phi;
}Point;

typedef struct COMPLEX {
	float x ;
	float y ;
}complex;

typedef struct GENERAL_STRUCT
{
	Link* arm;
	Point g;
	
}general_structur;

Link *IniLinks(void);
int AddLink( Link *links, float len, float theta, float max_theta, float min_theta);
int SetLink( Link *links, int linknum, float theta_norm);
int MoveLink( Link *links, int linknum, float d_theta);
void DrawArm( Link *links);
void Circle(float x, float y, float radius);
void DrawGoal( Point *g);
void DrawObstacle( Point *p);
float Random(float min, float max);
float Distance( complex *p1,  complex *p2);
float PDistance( Point *p1,  Point *p2);
void Pol2Rec( Point *p,  complex *r);
float ReadTheta( Link *links,int n);
float Reinforcement(Link* arm,complex *g);

/*--------------------------------------------------------*/
/* Start NEW */
void new_arm_simulation(int gpe)
{
	general_structur* ge_st = NULL;	
	int length;
	float maxt,mint;
	int lien=-1;
	int i;
	FILE* fp=NULL;
	arm_info my_arm_info;
	Link *arm = NULL;
	Link *l = NULL;
	complex g;
	Point goal;
	int arm_length=0;
	float phigmax,phigmin,rhogmax,rhogmin;
	char* name,braslibre[14];	
	int ret;
	
	/*if(def_groupe[gpe].ext!=NULL) return;*/  /* P.G */

	 for(i=0;(lien = find_input_link(gpe,i)) != -1; i++){}
	 if(i != (def_groupe[gpe].nbre-1))
	 {
		 printf("Erreur in Arm sim ... the nbre of output of S_Bs and Arm_Sim \n");
		 exit(1);
	 }
	ge_st = malloc(sizeof(general_structur));
	if(ge_st==NULL) 
	{ 
		printf("Erreur memoire f_delta_input\n");
		exit(1);
	}
	
	arm = IniLinks();

/* read Arm configuration txt */
	
	name = malloc(sizeof(char)*20); /* 20 charactere au maximum */

	fp = fopen("ArmInfo.txt","r");
	if(fp==NULL)
	{	
		printf("I can't open the file Arm Info TXT ...!\n");
		exit(1);
	}
	ret = fscanf(fp,"%s",name);
	fclose(fp);


	fp = fopen(name,"r");
	if(fp==NULL)
	{	
		printf("I can't open the file Arm_Sim TXT ...!\n");
		exit(1);
	}

	ret = fscanf(fp,"%s",braslibre);

	if(strcmp(braslibre,"bras_libre_oui")==0)
		libre_totalement = 1;
	if(strcmp(braslibre,"bras_libre_non")==0)
		libre_totalement = 0;
	printf("%s\n",braslibre);

	if(libre_totalement == -1)
		{		
		printf("erreur de librete de bres (libre_totalement) \n");
		exit(0);
		}

	ret = fscanf(fp,"%d",&my_arm_info.nbr_links);
	printf("my_arm_info->nbr_links = %d\n",my_arm_info.nbr_links);
	
	for(i=0; i < my_arm_info.nbr_links;i++)
	{
		ret= fscanf(fp,"%d",&length);
		ret = fscanf(fp,"%f",&maxt);
		ret = fscanf(fp,"%f",&mint);
		AddLink(arm,length,0,maxt,mint);	
		arm_length=arm_length+length;
	}
	
	/*phigmax,phigmin,rhogmax,rhogmin*/
	
	ret = fscanf(fp,"%f",&phigmax);
	ret = fscanf(fp,"%f",&phigmin);
	ret = fscanf(fp,"%f",&rhogmax);
	ret = fscanf(fp,"%f",&rhogmin);
	
	fclose(fp);

	
/* Select random locations for goal */
	goal.rho = Random(rhogmin,rhogmax);
	goal.phi = Random(phigmin,phigmax);
	Pol2Rec(&goal,&g);
	
/* Select a random configuration of the arm */
	for(i=0,l=arm ; i < my_arm_info.nbr_links ;i++)
		{
		if(i==0)
		SetLink(arm, i, Random(l->min_theta/pi2 ,l->max_theta/pi2 ));
		else 
		SetLink(arm, i, Random(l->min_theta/pi2+0.5 ,l->max_theta/pi2+0.5 ));

		l=l->next;
		}
	x_act = Xcentre;
	y_act = Ycentre;
		
	ge_st->arm = arm;
	ge_st->g   = goal;
		
	def_groupe[gpe].ext = (void*) ge_st;
	(void) ret;
return;

}
/* End NEW */
/*--------------------------------------------------------*/
void function_arm_simulation(int gpe)
{

	general_structur* ge_st = NULL;	
	Link* arm,*l;
	Point g;
	/*TxPoint point1;*/
	int i;
	int lien=-1;
	int gpe_input;
	int deb_neuron;
	complex goal;
	float t;
	float d_theta;
	float reinf;

	ge_st = (general_structur*)def_groupe[gpe].ext;

	arm = ge_st->arm;
	g   = ge_st->g;
	
	/* lire les sortie de S_B */
	
	for(i=0,l=arm ; l != NULL ; l=l->next,i++)
	{
		
		lien = find_input_link(gpe,i);
		gpe_input = liaison[lien].depart;
		deb_neuron = def_groupe[gpe_input].premier_ele;
		
		d_theta = (liaison[lien].norme * (neurone[deb_neuron].s2 - neurone[deb_neuron+1].s2) );
		MoveLink(arm,i, d_theta);
	}

	
	/*point1.x=0;
	point1.y=0;
	TxDessinerRectangle(&image1, blanc, TxPlein, point1,Xcentre*2,Ycentre*2,1);
	 */
	for(i=0,l=arm ; l != NULL ; l=l->next,i++)
	{
		t = ReadTheta(arm,i);	
		if(t<0.0) t = 1+t;
		if(t>1.0) t = t-1;
		SetLink(arm, i,t);	
	}

	DrawGoal(&g);
	DrawArm(arm);
	/*ici
	TxFlush(&image1);
	*/

	/*usleep(10);*/
	Pol2Rec(&g,&goal);

	reinf = Reinforcement(arm,&goal);

	/* send the info teta1,2,3,... and Reinforcement  */
	
	for(i=0,l=arm ; l != NULL ; l=l->next,i++)
	{
		t = ReadTheta(arm,i);
		neurone[def_groupe[gpe].premier_ele+i].s  = t;
		neurone[def_groupe[gpe].premier_ele+i].s1 = t;
		neurone[def_groupe[gpe].premier_ele+i].s2 = t;
	}

		neurone[def_groupe[gpe].premier_ele+i].s  = reinf;
		neurone[def_groupe[gpe].premier_ele+i].s1 = reinf;
		neurone[def_groupe[gpe].premier_ele+i].s2 = reinf;

	ge_st->arm = arm;

return;

}
/* END OF FUNCTION */

/***********************/
 Link *IniLinks(void)
{
	 Link *links;
	links = malloc(sizeof( Link));
	if(links == NULL) return NULL;
	links->len = 0; /* Necessary for the AddLink function */
	links->next = NULL;
	return links;
}
/***********************/
int AddLink( Link *links, float len, float theta, float max_theta, float min_theta)
{
	 Link *l;
	 float phi = 0;
	l = links;
	if(len <= 0) {
		printf("\nError: Link lengths of zero not permitted.\n");
		return FAILURE; }
		/* This section for arms with links already existing */
		if(l->len > 0) {
			while (l->next != NULL)
				l = l->next; /* Advance pointer to last existing link */
			phi = l->phi; /* Stored to calculate new link's phi */
			l->next = malloc(sizeof( Link));
			if(l->next == NULL) return FAILURE;
			l = l->next; }
			/* Insert link parameters in to the data structure */
			l->len = len;
			l->theta = theta;
			l->phi = fmod( phi + theta, pi2);
			l->max_theta = max_theta;
			l->min_theta = min_theta;
			l->next = NULL;
			return SUCCESS;
}
/***********************/
int SetLink( Link *links, int linknum, float theta_norm)
{
	int loop;
	float temp = 0, theta;
	 Link *l;
	l = links;
	/* Advance to the 'linknum' joint and sum heighths */
	for(loop = 0; loop < linknum; loop = loop + 1) {
		temp = l->phi;
		if(l->next != NULL) l = l->next; }
		theta = theta_norm * (l->max_theta - l->min_theta) + l->min_theta;
		if(theta > l->max_theta) theta = l->max_theta;
		if(theta < l->min_theta) theta = l->min_theta;
		l->theta = theta;
		for(; l!=NULL; l=l->next) { /* Recalculate phi's all the way to the tip */
			l->phi = fmod( temp + l->theta, pi2);
			temp = l->phi; }
			return SUCCESS;
}
/***********************/
int MoveLink( Link *links, int linknum, float d_theta)
{
	int loop;
	float y = 0;
	 Link *l;
	float phi;

	l = links;
	/* Advance to the 'linknum' joint and sum heighths */
	for(loop = 0; loop < linknum; loop = loop + 1) {
		y = y + l->len * sin( l->phi );
		if(l->next != NULL) l = l->next; }
	/* pour une bras totalment libre */	
	if(libre_totalement == 1)
	l->theta = fmod( l->theta + d_theta, pi2);

	/* pour une bras avec les limites */
	if(libre_totalement == 0)
	{
	l->theta = l->theta + d_theta;
	if((l->theta) > (l->max_theta)) l->theta = l->max_theta;
	if((l->theta) < (l->min_theta)) l->theta = l->min_theta;
	}

	l=links;
	phi = 0;

	for(; l != NULL ; l=l->next )
	{
		phi = fmod( phi + l->theta , pi2);
		l->phi = phi;
	}
	/*
	for(; l!=NULL; l=l->next)
		l->phi = fmod( l->phi + d_theta, pi2);
	*/
	return SUCCESS;
}
/***********************/
void MoveTo(int x, int y)
{
	x_act = Xcentre + x;
	y_act = Ycentre - y;
	
return;
}

/***********************/
void LineTo(int x, int y)
{
	/*TxPoint p1,p2;
	
	p1.x=x_act;
	p1.y=y_act;
	
	
	p2.x=Xcentre+x;
	p2.y=Ycentre-y;
	
	
	TxDessinerSegment(&image1, 1 , p1, p2, 2);
	 */
	MoveTo(x, y);
return;
}

/***********************/
void DrawArm( Link *links)
{
	int x=0, y=0; /* Assumes base is at (0, 0) */
	Link *l;
	MoveTo(x, y);
	for(l = links; l!=NULL; l = l->next) {
		x += l->len * cos(l->phi);
		y += l->len * sin(l->phi);
		LineTo(x, y);
	}
return;
}
/***********************/
void Circle(float x, float y, float radius)
{
	/*TxPoint p;
	p.x = Xcentre+(int)x;
	p.y = Ycentre-(int)y;
	
	TxDessinerCercle(&image1, 1,0,p,radius,1);
	 */
(void) x;
(void) y;
(void) radius;
	return;
}
/***********************/
void DrawGoal( Point *g)
{
	int radius = 10;
	float x, y;
	x = cos(g->phi) * g->rho;
	y = sin(g->phi) * g->rho;
	Circle(x, y, radius);
return;
}
/***********************/
void DrawObstacle( Point *o)
{
	int radius = 5;
	float x, y;
	x = cos(o->phi) * o->rho;
	y = sin(o->phi) * o->rho;
	Circle(x, y, radius);
return;
}
/***********************/
float Random(float min, float max)
{
	float n;
	
	static int first123456789 = 0;
   
	if (first123456789 == 0)
	{
		srand (time (NULL));
		first123456789 = 1;
	}
	
	n = ((float)(rand()) / RAND_MAX) * (max - min) + min;
	
	return n;
}
/***********************/
float Distance( complex *p1, complex *p2)
{
	float d;
	d = (float)( (float)(p1->x - p2->x) * (float)(p1->x - p2->x)
			+ (float)(p1->y - p2->y) * (float)(p1->y - p2->y));
	d = sqrt(d);
	return d;
}
/***********************/
float PDistance( Point *p1,  Point *p2)
{
	 complex rp1, rp2;
	rp1.x = p1->rho * cos(p1->phi);
	rp1.y = p1->rho * sin(p1->phi);
	rp2.x = p2->rho * cos(p2->phi);
	rp2.y = p2->rho * sin(p2->phi);
	return Distance(&rp1, &rp2);
}

void Pol2Rec( Point *p,  complex *r)
{
	if(!p || !r) {printf("ERROR in programming Pol2Rec."); exit(1); }
	r->x = p->rho * cos(p->phi);
	r->y = p->rho * sin(p->phi);
return;
}

/***********************/
float ReadTheta( Link *links,int n)
{
	 Link *l;
	int i;

	for(i=0,l = links; (l!=NULL) && (i < n) ; l=l->next,i++);
		
return ( l->theta - l->min_theta ) / ( l->max_theta - l->min_theta);
}
/***********************/
float Reinforcement(Link* arm,complex *g)
{
	Link* l;
	complex p;
	float d;
	int i;
	
	p.x=0;
	p.y=0;
	
	for(i=0,l = arm; l!=NULL; l = l->next,i++)
	{
		p.x += l->len * cos(l->phi);
		p.y += l->len * sin(l->phi);	
	}
	d = Distance(g,&p);
	/*d= d / ( 2.0 * sqrt(g->x*g->x + g->y*g->y) );*/
	d=d/200;
	/*return  1/( (d/100.0)+1 );*/
	/*	return  1.0/(1+exp(10*(d-0.5)));*/
	return 1.-d;

}
/**************************/


