/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sensor_arm.c 
\brief 

Author: Sofiane Boucenna
Created: 
Modified: 
- author:
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-donne la valeur des sensor de la pince du bras katana

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet...

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>

/*#define DEBUG*/

typedef struct
{
  int gpe_amounte;            /* groupe de liaision precedant */
  int longueur;               /* longueur de la chaine de neurones (chaque neurone est une expression */
  int deb;                    /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int inputGpe;               /* groupe d'entree pour la liaisoin en arriere */
  Sensor *sens;               /* structure de promethe */
} arm_data;


void new_sensor_arm(int Gpe)
{
  arm_data *data;      /* structure de cette fonction */

#ifdef DEBUG
  printf("---------------->enter in %s\n", __FUNCTION__);
#endif
  
  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = (arm_data *) malloc(sizeof(arm_data));
  if (data == NULL)
    {
      printf("error in memory allocation in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  /* on charge le ficher avec la description de vitesse et position de chaque servo */
  data->gpe_amounte = find_input_link(Gpe, 0);
  
  if (data->gpe_amounte == -1)
    {
      printf(" Error, %s group %i doesn't have an input\n", __FUNCTION__,
	     Gpe);
      exit(EXIT_FAILURE);
    }
  

  
  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->inputGpe = liaison[data->gpe_amounte].depart;
  data->deb = def_groupe[data->inputGpe].premier_ele;
  data->longueur = def_groupe[data->inputGpe].nbre;
  
  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;

#ifdef DEBUG
  printf("---------------->end of %s\n", __FUNCTION__);
#endif
  
  
}

/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/


void function_sensor_arm(int Gpe)
{
  /* initialisation des variables */
  
  arm_data *my_data = NULL;    /* la structure de cette fonction */
  /* fin de l'initialisation des variables */
  char sensor[1][256] = {"capteur_pince"};
  float valeur_capteur = -1;
  int i;
  int deb;
#ifdef DEBUG
  printf("------------->enter in %s, group %d\n", __FUNCTION__, Gpe);
#endif

  
  deb = def_groupe[Gpe].premier_ele;
  my_data = (arm_data *) def_groupe[Gpe].data; /* recuperer les donnes du constructeur */
  
  if (my_data == NULL)
    {
      printf("error while loading data in group %d %s\n", Gpe,
	     __FUNCTION__);
      exit(EXIT_FAILURE);
    }

  my_data->sens = sensor_get_sens_by_name(sensor[0]);
  valeur_capteur = sensor_sens_command(my_data->sens);
  
  if((int)valeur_capteur != 2)
    printf("ATTENTION: pb dans la fonction f_sensor_arm\n");

  for(i=0 ; i<my_data->sens->nb_sensor ; i++)
    {
      neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = my_data->sens->vecteur_sensor[i];
#ifdef DEBUG
      printf("user: sensor[%i]=%f\n",i,neurone[deb+i].s);
#endif

    }

  
  


#ifdef DEBUG
printf("-------------->end of %s, group %d\n", __FUNCTION__, Gpe);
#endif
  
}
