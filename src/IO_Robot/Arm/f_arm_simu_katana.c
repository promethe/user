/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  f_arm_simu_katana.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: R. Braud
- description: specific file creation
- date: 05/02/2015

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
   Katana arm simulation, same code as in the broker.
Options :

INPUT :
Group with link "-joints" with 5 neurons corresponding to the positions on each joint between 0 and 1.

OUTPUT :
Coord X Y Z of the end-effector in 3 neurons  (If 4 neurons : angle of the last segment of the effector in cartesian space (in radians) )

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

/*#define DEBUG*/


#include <libx.h>
#include <stdlib.h>
#include <string.h>
//#include <Struct/prom_images_struct.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <IO_Robot/Arm.h>
//#include <float.h>

//#include <sys/time.h>


/*Arm values*/
#define NB_JOINTS 5
#define NB_OFFSETS 4
#define NB_OPERATING_RANGES 5
#define NB_SEGMENTS 4

#define PI 3.14159265358979323846

/* True one : whole possible movement */
///**range of motors**/
//#define OR0 360
//#define OR1 -139.48
//#define OR2 253.95  // webots vs reel : 148.14 vs 253.95
//#define OR3 230.48
//#define OR4 332.2 //operating range of rotation of wrist gripper,
//          //useless here
///**Offset of the angles, in degrees**/
//#define OFFSET0 -0.36 /* correction offset*/
//#define OFFSET1 124.23 /* correction offset*/
//#define OFFSET2 -122.88  /* correction offset*/
//#define OFFSET3 -121.02
//#define OFFSET4   8.5


///* Bounded for reduced babbling */
///**range of motors**/
//#define OR0 180
//#define OR1 -83.688
//#define OR2 126.975  // webots vs reel : 148.14 vs 253.95
//#define OR3 115.24
//#define OR4 332.2 //operating range of rotation of wrist gripper,
//          //useless here
///**Offset of the angles, in degrees**/
//#define OFFSET0 179.64 /* correction offset*/
//#define OFFSET1 68.438 /* correction offset*/
//#define OFFSET2 -59.3925  /* correction offset*/
//#define OFFSET3 -63.4
//#define OFFSET4   8.5

///* Bounded for reduced babbling */
///**range of motors**/
//#define OR0 108
//#define OR1 -50.2128
//#define OR2 126.975  // webots vs reel : 148.14 vs 253.95
//#define OR3 115.24
//#define OR4 332.2 //operating range of rotation of wrist gripper,
//          //useless here
///**Offset of the angles, in degrees**/
//#define OFFSET0 215.64 /* correction offset*/
//#define OFFSET1 51.7004 /* correction offset*/
//#define OFFSET2 -59.3925  /* correction offset*/
//#define OFFSET3 -63.4
//#define OFFSET4   8.5



/* Bounded for reduced babbling (when we use it on the table in A578) */
/**range of motors**/
#define OR0 108
#define OR1 -50.2128
#define OR2 42.325  // webots vs reel : 148.14 vs 253.95
#define OR3 38.413
#define OR4 332.2 //operating range of rotation of wrist gripper,
          //useless here
/**Offset of the angles, in degrees**/
#define OFFSET0 215.64 /* correction offset*/
#define OFFSET1 51.7004 /* correction offset*/
#define OFFSET2 -17.0675  /* correction offset*/
#define OFFSET3 -24.986
#define OFFSET4   8.5



/*fin modif*/

/**size of the robot arm**/
#define SEG0 .317          	/* longueur en m entre le socle et le moteur 1*/
#define SEG1 .190          	/* entre le moteur 1 et le moteur 2 : UA dans la doc Katana*/
#define SEG2 .139          	/* entre le moteur 2 et le moteur 3 : FA*/
#define SEG3 .315      		/* entre le moteur 3 et le moteur 4  + entre le moteur 4 et l'extremite de la pince : WR + GR*/



typedef struct Joint_coord
{
   float x;
   float y;
   float z;
} Joint_coord;


typedef struct My_Data_arm_simu_katana
{
   int joints_gpe;
   int seg_gpe;
   Joint_coord jcoord[6];
   float segments[NB_SEGMENTS];
   float offsets[NB_JOINTS];
   float op_ranges[NB_OPERATING_RANGES];
} My_Data_arm_simu_katana;




void new_arm_simu_katana(int Gpe)
{

   My_Data_arm_simu_katana* mydata;
   int i = 0, links_in ;
   int joints_gpe = -1;
   int seg_gpe = -1;
   float val_segments[NB_SEGMENTS] = {SEG0,SEG1,SEG2,SEG3};
   float val_offsets[NB_JOINTS]={OFFSET0,OFFSET1,OFFSET2,OFFSET3,OFFSET4};
   float val_operating_ranges[NB_OPERATING_RANGES] = {OR0,OR1,OR2,OR3,OR4};
   //char buf[255] ;

   dprints(" ~~~~~~~ DEBUT constructeur %s (num %d) ~~~~~~\n", __FUNCTION__, atoi(def_groupe[Gpe].no_name));

   /* Get Input group */
   while((links_in = find_input_link (Gpe, i)) != -1)
   {
      if(prom_getopt(liaison[links_in].nom, "-joints", NULL) == 1)
      {
         joints_gpe = liaison[links_in].depart ;
         if( def_groupe[joints_gpe].nbre != 4 )
            EXIT_ON_ERROR("GROUP %s (num coeos %s) : Input -joints (group %s) needs 4 neurons, for the 4 simulated joints able to change the end-effector coordinates", __FUNCTION__, def_groupe[Gpe].no_name, def_groupe[joints_gpe].no_name);
      }
      if(prom_getopt(liaison[links_in].nom, "-lastSegment", NULL) == 1)
      {
         seg_gpe = liaison[links_in].depart ;
         if( def_groupe[seg_gpe].nbre != 1 )
            EXIT_ON_ERROR("GROUP %s (num coeos %s) : Input -lastSegment (group %s) needs just 1 neurons, a distance in meter for the last segment lenght", __FUNCTION__, def_groupe[Gpe].no_name, def_groupe[seg_gpe].no_name);
      }
      i++ ;
   }

   /* Check group size */
   if ( joints_gpe == -1 )
      EXIT_ON_ERROR("GROUP %s (num coeos %s) : There must be a link -joints\n", __FUNCTION__, def_groupe[Gpe].no_name);

   /* Check group size */
   if ( def_groupe[Gpe].nbre != 3 && def_groupe[Gpe].nbre != 4 )
      EXIT_ON_ERROR("GROUP %s (num coeos %s) : There must be a 3 neurons for x, y, z coordinates in meters + 1 (facu), the angle of the last segment of the effector\n", __FUNCTION__, def_groupe[Gpe].no_name);




   /* Create object */
   mydata = ALLOCATION(My_Data_arm_simu_katana);
   mydata->joints_gpe = joints_gpe ;
   mydata->seg_gpe = seg_gpe ;


   for (i=0; i< NB_JOINTS;i++)
   {
      mydata->jcoord[i].x = 0.;
      mydata->jcoord[i].y = 0.;
      mydata->jcoord[i].z = 0.;
      mydata->offsets[i] = val_offsets[i];
   }

   for (i=0; i< NB_SEGMENTS;i++)
   {
      mydata->segments[i] = val_segments[i];
   }
   mydata->jcoord[1].z = mydata->segments[0];

   for (i=0; i< NB_OPERATING_RANGES;i++)
      mydata->op_ranges[i] = (val_operating_ranges[i]*PI)/180.0;




   def_groupe[Gpe].data = (My_Data_arm_simu_katana *) mydata;

   dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);
}



void function_arm_simu_katana(int Gpe)
{
   My_Data_arm_simu_katana* mydata;
   int i, j;
   float sum, angles[6];

   mydata = (My_Data_arm_simu_katana *) def_groupe[Gpe].data ;

   if( mydata->seg_gpe != -1 )
      mydata->segments[NB_SEGMENTS - 1] = neurone[def_groupe[mydata->seg_gpe].premier_ele].s1;


   for(i=0; i<NB_JOINTS - 1; i++)
   {
      angles[i] = neurone[def_groupe[mydata->joints_gpe].premier_ele + i].s1 * mydata->op_ranges[i];
      angles[i] += ((mydata->offsets[i]*PI/180.0));
   }
   /* The last DoF do not change the coordinates of the end-effector : we freeze it */
   angles[NB_JOINTS - 1] = ((mydata->offsets[NB_JOINTS - 1]*PI/180.0));


   for(i=1; i<NB_JOINTS; i++)
   {
      sum = 0;
      for (j=1;j<i;j++)
        sum += angles[j];
      //sum[i] = sum[i-1] + angles[i];
      if( i != 1 )
      {
         mydata->jcoord[i].x = mydata->jcoord[i-1].x + mydata->segments[i-1] * cos((double)sum )*cos((double)(-angles[0]));
         mydata->jcoord[i].y = mydata->jcoord[i-1].y + mydata->segments[i-1] * cos((double)sum )*sin((double)(-angles[0]));
         mydata->jcoord[i].z = mydata->jcoord[i-1].z + mydata->segments[i-1] * sin((double)sum);
      }
      if( i == NB_JOINTS -1 && def_groupe[Gpe].nbre == 4 )
         neurone[def_groupe[Gpe].premier_ele + 3].s1 = sum *180.0/PI;
   }

   neurone[def_groupe[Gpe].premier_ele].s1      = mydata->jcoord[NB_JOINTS - 1].x;
   neurone[def_groupe[Gpe].premier_ele + 1].s1  = mydata->jcoord[NB_JOINTS - 1].y;
   neurone[def_groupe[Gpe].premier_ele + 2].s1  = mydata->jcoord[NB_JOINTS - 1].z;
}



void destroy_arm_simu_katana(int Gpe)
{

   if (def_groupe[Gpe].data != NULL)
   {
     free(def_groupe[Gpe].data);
     def_groupe[Gpe].data = NULL;
   }
   dprints("exiting %s (%s line %i) (num coeos %s)\n", __FUNCTION__, __FILE__, __LINE__, def_groupe[Gpe].no_name );
}



