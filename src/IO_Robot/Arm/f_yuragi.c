/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

/** ***********************************************************
 \file  f_yuragi.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: S. Boucenna
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 Pour le moment fonction qui sert pour le controle du bras Katana.
 Controle du bras dans l espace proprioceptif.
 Il y a dans cette technique une notion de renforcement
 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
/*#define DEBUG*/

#define NB_ATTRACT 100 /*nombre d'attracteurs souhait�s*/
#define NB_DIM 6     /*nombre de dimensions*/

#define YURAGI_A 1.0

typedef struct MyData {
  int gpe_x;
  float *yuragi_X; /*attracteurs*/
  float *Q_X;
  float *D_X;
  float *yuragi_x; /*positions*/
  float *yuragi_vx; /*vitesses*/
  float *proprio_present;
  float *proprio_passe;
  int nb_dim;
  int deb;
  int gpe_a;
  int gpe_distance;
  int step;
  int gpe_attracteur;
  int gpe_reservoir;
  int nb_attract;
} MyData;

/*tirage aleatoire des attracteurs       */
/* !! utilise drand48()   - a remplacer- */

/* nb_dim est le nombre de dimentions des vesteurs x et X*/
/* nb_dim depend du groupe d'entr�e */

float* create_attractors(int nb_dim)
{
  int i;
  float *attr;
  /* printf("***************************yyyyyyyyyyy*******************\n");*/
  attr = (float *) malloc(sizeof(float) * nb_dim * NB_ATTRACT);
  srand(time(NULL));
  for (i = 0; i < (NB_ATTRACT) * nb_dim; i++)
  {
    /* tirage al??atoire des valeurs des attracteurs*/

    /*#ifdef DEBUG*/
    if (i % nb_dim == 0) printf("\n Attracteur % i : \n", i / nb_dim);
    /*#endif*/

    /*juste pour que le bras ne tourne pas!!!*/
    if (i % nb_dim == 0 || i % nb_dim == 4 || i % nb_dim == 5)
    {
      attr[i] = 1;
    }
    else
    {
      attr[i] = ((float) (rand() % 10)) / (float) 10;
      /*attr[i] = ((float) drand48());*/
    }

    /*#ifdef DEBUG*/
    printf("%.3f\n", attr[i]);
    /*#endif*/
  }
  /*#ifdef DEBUG*/
  printf("\n");
  /*#endif*/

  return attr;

}

/*ajout d une deuxieme fonction qui cree les attracteurs*/
/*on essaye de paver r�gulierement l espace proprioceptif*/

float* create_attractors_pavage(int nb_dim)
{
  int i, j, z;
  float *attr;
  int cpt = 0;

  attr = (float *) malloc(sizeof(float) * 1000 * nb_dim);

  for (i = 0; i <= 10; i = i + 5)
    for (j = 0; j <= 10; j = j + 5)
      for (z = 0; z <= 10; z = z + 5)

      {
        attr[cpt++] = 1;
        attr[cpt++] = ((float) i) / 10;
        attr[cpt++] = ((float) j) / 10;
        attr[cpt++] = ((float) z) / 10;
        attr[cpt++] = 1;
        attr[cpt++] = 1;
      }

  return attr;

}
/*affiche de tous les attracteurs*/
void affiche_attracteurs(float *tab, int taille)
{

  int cpt = 0;
  int cpt1 = 1;
  for (cpt = 0; cpt < taille; cpt++)
  {
    if (cpt % 6 == 0)
    {
      printf("\n");
      printf("attr%d: ", cpt1++);
    }
    printf("%f  ", tab[cpt]);

  }
  printf("\n");
}

/* affichage du vecteur x*/
void affiche_x(float *x, int nb_dim)
{
  int i;
  for (i = 0; i < NB_DIM; i++)
    printf("%f ", *(x + i));
  printf("\n");
  (void) nb_dim;

}

/********************************ci dessus les fonctions independantes de #define NB_DIM***/
/* il ne devrait plus rien rester sous cette ligne ****************************************/

float distance(int i, float *x, float *X)
{

  float dist = 0.;
  int j;

  for (j = 0; j < NB_DIM; j++)
  {
    /*    printf("distance de x a la composante j de X : %f \n", ((*(X+(i*NB_DIM)+j))-(*(x+j))) );*/
    /*somme du carr� des distances */
    dist = dist + (((*(X + (i * NB_DIM) + j)) - (*(x + j))) * ((*(X + (i * NB_DIM) + j)) - (*(x + j))));

  }

  return dist;

}

/* r�ponse de la gaussienne centr�e en X */

float gauss(int i, float *x, float *X)
{
  float beta = -0.1;
  float dist = 0.;
  dist = distance(i, x, X);
  return exp(beta * dist);

}

/*  calcul de la valeur Ni de l'�quation  des attracteurs  */
float calculeN(int i, float *x, float *X, int nb_attract)
{

  int num;
  float sum_gauss = 0;

  for (num = 0; num < nb_attract; num++)
    sum_gauss = sum_gauss + gauss(num, x, X);

  return (gauss(i, x, X) / sum_gauss);

}

/* Xi-x */

void Diff(int i, float *x, float *X, float*diff)
{
  int dim;

  for (dim = 0; dim < NB_DIM; dim++)
  {
    diff[dim] = X[dim + (i * NB_DIM)] - x[dim];
    /*printf("x[%i]=%f X=[%i]=%f diff=%f\n",dim,x[dim],dim,X[dim+(i*NB_DIM)],diff[dim]);*/
  }

}

/* calcul de f(x), c'est a dire la nouvelle valeur de x en fonction des attracteurs*/

void calculeF(float *x, float *X, float *result, int nb_attract)
{

  int num, val;
  float tmp[NB_DIM];
  float diff[NB_DIM];
  float N, dist;

  for (val = 0; val < NB_DIM; val++)
  {
    tmp[val] = 0;
    result[val] = 0;
  }

  for (num = 0; num < nb_attract; num++)
  {
    N = calculeN(num, x, X, nb_attract);
    dist = distance(num, x, X);

    Diff(num, x, X, diff);
    for (val = 0; val < NB_DIM; val++)
    {

      /* printf("diff[%i]=%f\n",val,diff[val]); */
      if (dist < 0.00001)
      {
        tmp[val] = 0; /* ??????*/
        printf("attention ici!!\n");

      }
      else
      {
        tmp[val] = N * diff[val] / sqrt(dist);
      }
    }

    for (val = 0; val < NB_DIM; val++)
    {
      result[val] = result[val] + tmp[val];
    }
    /*  printf("numero attracteur=%i,N=%f,dist=%f\n",num,N,dist); */

  }

}

void calcule_variation_x(float *x, float *X, float *vx, int nb_attract)
{
  int i;
  /*float yuragi_noise;*/
  float yuragi_calc[NB_DIM];

  /* calculs  de la nouvelle positionde X*/
  /* !! IL N'Y A PAS DE RUNGE KUTTA!!*/
  calculeF(x, X, yuragi_calc, nb_attract);
  /*for (i=0;i<NB_DIM;i++) {
   printf("f(x_%i)=%f\n",i,yuragi_calc[i]);
   }
   printf("\n");*/
  for (i = 0; i < NB_DIM; i++)
  {

    vx[i] = eps * (yuragi_calc[i] * YURAGI_A) /*+yuragi_noise*/;
    x[i] = x[i] + vx[i] /*+yuragi_noise*/;

  }
}

/*
 *
 *Attractors dynamiques
 *
 */

void attractors_dynamique_temps(int nb_dim, float *x, float *X, float *Q_X, float *D_X, float A, int nb_attract)
{

  int num, val;
  float N;
  printf("---->A=%f\n", A);
  /*if(A<0)
   A=-1;
   else
   A=1;
   */
  /*for (num=0;num<NB_ATTRACT;num++)
   {
   for (val=0;val<nb_dim;val++)
   {
   Q_X[nb_dim*num+val]=0;
   D_X[nb_dim*num+val]=0;
   }
   }
   */

  for (num = 0; num < NB_ATTRACT; num++)
  {
    /*calcule du Ni*/
    N = calculeN(num, x, X, nb_attract);

    for (val = 0; val < nb_dim; val++)
    {
      /*printf("N(%i)=%f\n",num,N);
       printf("x[%i]=%f\n",val,x[val]);*/
      if (val == 0 || val == 4 || val == 5)
      {
        /*X[nb_dim*num+val]=1;*/
        /*printf("X[%i]=%f ",nb_dim*num+val,X[nb_dim*num+val]);*/
      }
      else
      {
        Q_X[nb_dim * num + val] = Q_X[nb_dim * num + val] + N * A * x[val];
        D_X[nb_dim * num + val] = D_X[nb_dim * num + val] + N * A;
        /*X[nb_dim*num+val]=Q_X[nb_dim*num+val]/D_X[nb_dim*num+val];*/
        /*Q_X[nb_dim*num+val] = X[nb_dim*num+val]+(-0.001*(X[nb_dim*num+val]-x[val]));*/
        /*if(X[nb_dim*num+val]<0)
         X[nb_dim*num+val]=0;

         if(X[nb_dim*num+val]>1)
         X[nb_dim*num+val]=1;
         */
        /*X[nb_dim*num+val]=Q_X[nb_dim*num+val];*/
        /*printf("X[%i]=%f ",nb_dim*num+val,X[nb_dim*num+val]);*/
      }
    }
    /*printf("\n");*/

  }

}

/*new fonction*/

void attractors_dynamique(int nb_dim, float *x, float *X,/* float *Q_X, float *D_X,*/ float A)
{

  int num, val;

  for (num = 0; num < NB_ATTRACT; num++)
  {

    for (val = 0; val < nb_dim; val++)
    {

      if (val == 0 || val == 4 || val == 5)
      {
        X[nb_dim * num + val] = 1;

      }
      else
      {

        X[nb_dim * num + val] = X[nb_dim * num + val] + A * (-0.01 * (X[nb_dim * num + val] - x[val]));
        ;

        if (X[nb_dim * num + val] < 0) X[nb_dim * num + val] = 0;

        if (X[nb_dim * num + val] > 1) X[nb_dim * num + val] = 1;

      }
    }

  }

}

/* la taille de ce groupe (nb neurones) doit etre la m�me que 
 la taille du groupe d'entree : nombre de dimensions */

void function_yuragi(int gpe)
{

  MyData *mydata = NULL;
  int i = 0;
  int j = 0;
  char param[256];
  float *tmp;
  float yuragi_noise = 0;
  int incr;
  type_coeff *coeff;
  int z = 0;
  /* int pp; */
  /* float bruit; */
  int gpe_distance = -1;
  /* float distance=0; */
  /* float R=0; */
  /*   float bruit=0; */
  float max = -1000;
  int i_max = 10;
  float renforcement = 0;

  if (def_groupe[gpe].data == NULL)
  {
    mydata = (MyData *) malloc(sizeof(MyData));
    if (mydata == NULL)
    {
      printf("pb malloc pour mydata");
      exit(0);
    }
    /*recuperation des parametres d'entree*/
    printf("je suis la\n");
    while ((j = find_input_link(gpe, i)) != -1)
    {

      if (prom_getopt(liaison[j].nom, "-x", param) == 2)
      {
        /* recuperation des donnees du groupe d'entree*/
        mydata->gpe_x = liaison[j].depart;
        mydata->nb_dim = def_groupe[mydata->gpe_x].nbre;
        mydata->deb = def_groupe[mydata->gpe_x].premier_ele;

        /* la taille du groupe d'entree donne le nombre de dimensions*/
        /* allocation et tirage des valeurs X (les attracteurs) selon le nb de dimensions*/

        mydata->yuragi_X = create_attractors(mydata->nb_dim);
        /*affiche_attracteurs(mydata->yuragi_X,mydata->nb_dim*NB_ATTRACT);*/
        /* allocation du vecteur x */
        mydata->yuragi_x = (float*) malloc(sizeof(float) * mydata->nb_dim);
        mydata->proprio_present = (float*) malloc(sizeof(float) * mydata->nb_dim);
        mydata->proprio_passe = (float*) malloc(sizeof(float) * mydata->nb_dim);
        mydata->Q_X = (float*) malloc(sizeof(float) * mydata->nb_dim * NB_ATTRACT);
        mydata->D_X = (float*) malloc(sizeof(float) * mydata->nb_dim * NB_ATTRACT);
        mydata->step = 1;
        mydata->nb_attract = NB_ATTRACT;
        /* allocation du vecteur vx */
        mydata->yuragi_vx = (float*) malloc(sizeof(float) * mydata->nb_dim);

        if ((mydata->yuragi_x == NULL) || (mydata->yuragi_vx == NULL) || (mydata->Q_X == NULL) || (mydata->D_X == NULL) || (mydata->proprio_present == NULL) || (mydata->proprio_passe == NULL))
        {
          printf("Attention pb de malloc dans la fonction f_yuragi\n");
          exit(0);
        }

        for (incr = 0; incr < mydata->nb_dim * NB_ATTRACT; incr++)
        {
          mydata->Q_X[incr] = 0;
          mydata->D_X[incr] = 0;
        }

        for (incr = 0; incr < mydata->nb_dim; incr++)
        {
          mydata->proprio_present[incr] = 0;
          mydata->proprio_passe[incr] = 0;
        }

#ifdef DEBUG
        printf("groupe=%s\n",def_groupe[liaison[j].depart].no_name);
        printf("nb_dim=%i\n",mydata->nb_dim);
#endif

      }

      if (strcmp(liaison[j].nom, "A") == 0)
      {
        mydata->gpe_a = liaison[j].depart;
#ifdef DEBUG
        printf("je rentre dans A\n");
#endif
      }

      if (strcmp(liaison[j].nom, "attracteur") == 0)
      {
        mydata->gpe_attracteur = liaison[j].depart;
#ifdef DEBUG
        printf("je rentre dans attracteurs\n");
#endif
      }

      if (strcmp(liaison[j].nom, "reservoir") == 0)
      {
        mydata->gpe_reservoir = liaison[j].depart;
#ifdef DEBUG
        printf("je rentre dans neurone\n");
#endif
      }

      if (strcmp(liaison[j].nom, "distance") == 0)
      {
        gpe_distance = mydata->gpe_distance = liaison[j].depart;
#ifdef DEBUG
        printf("je rentre dans distance\n");
#endif
      }

      i++;

#ifdef DEBUG
      printf("on a fini de recuperer les parametres\n");
#endif

    }

    if (gpe_distance == -1)
    {
      printf("erreur de parametres\n");
      exit(1);
    }
    def_groupe[gpe].data = mydata; /*sauvegarde de Mydata*/

  }
  else
  {

    mydata = def_groupe[gpe].data;

#ifdef DEBUG
    printf("gpe entree A = %s\n", def_groupe[mydata->gpe_a].no_name);
    printf("A(t)=%f\n",neurone[def_groupe[mydata->gpe_a].premier_ele].s);
    printf("gpe entree attracteur = %s\n", def_groupe[mydata->gpe_attracteur].no_name);
#endif   

    /* printf("gpe entree reservoir = %s\n", def_groupe[mydata->gpe_reservoir].no_name); */

    /******************************************************************************/
    /******************************************************************************/
    /***************                                                 **************/
    /***************               Poids du SAW                      **************/
    /***************                                                 **************/
    /******************************************************************************/
    /******************************************************************************/

    mydata->nb_attract = 0;

    printf("----------->attracteurs\n");
    for (i = 0; i < def_groupe[mydata->gpe_reservoir].nbre; i++)
    {
      if (neurone[def_groupe[mydata->gpe_reservoir].premier_ele + i].s2 > 0.2)
      {
        max = neurone[def_groupe[mydata->gpe_reservoir].premier_ele + i].s2;
        i_max = i;
        printf("i_max=%i,max=%f\n", i_max, max);
        coeff = neurone[def_groupe[mydata->gpe_attracteur].premier_ele + i_max].coeff;
        mydata->nb_attract++;
        while (coeff != NULL)
        {
          if (z % mydata->nb_dim == 0 || z % mydata->nb_dim == 4 || z % mydata->nb_dim == 5)
          {
            mydata->yuragi_X[z] = 1;
            printf("%.3f \t", mydata->yuragi_X[z]);
          }
          else
          {
            if (coeff->val < 0)
            {
              if (coeff->val < -1) mydata->yuragi_X[z] = 1;
              else mydata->yuragi_X[z] = -coeff->val;
            }
            else
            {
              if (coeff->val > 1) mydata->yuragi_X[z] = 1;
              else mydata->yuragi_X[z] = coeff->val;
            }
            printf("%.3f \t", mydata->yuragi_X[z]);
          }
          coeff = coeff->s;
          z++;
        }
        printf("\n");
      }
    }
    printf("\n");

    /*       for(i=0;i<def_groupe[mydata->gpe_attracteur].nbre;i++) */
    /* 	{ */

    /*  	  if(neurone[def_groupe[mydata->gpe_attracteur].premier_ele+i].s2==max) */
    /* 	    { */
    /* 	      coeff = neurone[def_groupe[mydata->gpe_attracteur].premier_ele+i].coeff; */
    /* 	      mydata->nb_attract++;   */

    /* 	      while (coeff != nil) */
    /* 		{ */

    /* 		  if(z%mydata->nb_dim==0 || z%mydata->nb_dim==4 || z%mydata->nb_dim==5) */
    /* 		    { */
    /* 		      mydata->yuragi_X[z]=1; */
    /* 		      printf("%.3f \t",mydata->yuragi_X[z]);   */
    /* 		    } */
    /* 		  else  */
    /* 		    {  */
    /* 		      if(coeff->val<0) */
    /* 			{ */
    /* 			  if(coeff->val<-1) */
    /* 			    mydata->yuragi_X[z]=1; */
    /* 			  else */
    /* 			    mydata->yuragi_X[z]=-coeff->val; */
    /* 			}  */
    /* 		      else  */
    /* 			{  */
    /* 			  if(coeff->val>1) */
    /* 			     mydata->yuragi_X[z]=1; */
    /* 			  else */
    /* 			    mydata->yuragi_X[z]=coeff->val; */
    /* 			}  */
    /* 		      printf("%.3f \t",mydata->yuragi_X[z]);   */
    /* 		    }  */
    /* 		  coeff = coeff->s; */
    /* 		  z++;  */
    /* 		}  */

    /* 	      printf("\n");   */
    /* 	    }  */
    /* 	 }  */

    tmp = mydata->yuragi_x;
    /* printf("----> affichage proprio\n");  */
    for (i = 0; i < (mydata->nb_dim); i++)
    {
      tmp[i] = neurone[mydata->deb + i].s;
      /* printf("%f \t",tmp[i]);   */
    }
    /* printf("\n"); */

    /* printf("\n----> affichage attracteurs\n"); */
    /* for(i=1;i<=mydata->nb_dim*mydata->nb_attract;i++) */
    /* 	{ */
    /* 	  printf("%.3f \t",mydata->yuragi_X[i-1]); */
    /* 	  if(i%mydata->nb_dim == 0) */
    /* 	    printf("\n");  */
    /* 	} */

#ifdef DEBUG
    printf("-------avant calcule variation------\n");
    affiche_x(mydata->yuragi_x,mydata->nb_dim);
#endif

    calcule_variation_x(mydata->yuragi_x, mydata->yuragi_X, mydata->yuragi_vx, mydata->nb_attract);

#ifdef DEBUG
    printf("-------apr�s calcule variation------\n");
    affiche_x(mydata->yuragi_vx,mydata->nb_dim);
#endif

    /* printf("A(t)=%f\n",neurone[def_groupe[mydata->gpe_a].premier_ele].s); */
    /*       printf("Distance=%f\n\n",neurone[def_groupe[mydata->gpe_distance].premier_ele].s); */
    if (neurone[def_groupe[mydata->gpe_a].premier_ele].s >= 0)
    {
      renforcement = neurone[def_groupe[mydata->gpe_distance].premier_ele].s;
    }
    else
    {
      renforcement = -neurone[def_groupe[mydata->gpe_distance].premier_ele].s;
    }
    for (i = 0; i < mydata->nb_dim; i++)
    {
      yuragi_noise = ((drand48() / 5) - 0.1);
      neurone[def_groupe[gpe].premier_ele + i].s = neurone[def_groupe[gpe].premier_ele + i].s1 = neurone[def_groupe[gpe].premier_ele + i].s2 = (mydata->yuragi_vx[i] * renforcement * 10/* * neurone[def_groupe[mydata->gpe_a].premier_ele].s  * 500 */
      + yuragi_noise);

      /* printf("moteur_%i=%f\n",i, neurone[def_groupe[gpe].premier_ele+i].s2); */
      /*printf("yuragi_noise_%i=%f\n",i,yuragi_noise);*/
#ifdef DEBUG	  
      printf("v_%i*A(t)=%f\n",i,mydata->yuragi_vx[i] * neurone[def_groupe[mydata->gpe_a].premier_ele].s * 1);
      printf("yuragi_noise_%i=%f\n",i,yuragi_noise * 2);
      printf("moteur_%i=%f\n",i, neurone[def_groupe[gpe].premier_ele+i].s2);
#endif
    }

    for (i = 0; i < mydata->nb_dim; i++)
    {
      mydata->yuragi_vx[i] = (mydata->yuragi_vx[i] * neurone[def_groupe[mydata->gpe_a].premier_ele].s * 100 + yuragi_noise);
    }

  }

}
