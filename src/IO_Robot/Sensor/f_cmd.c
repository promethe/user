/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_cmd
\brief 
\file  
/section Author
Name: Delarboulas Pierre
Created: 17/09/2012

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description

Permet de manipuler les Différents équipements de Maya (réacteur, lampe, etc ...)

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/




#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/* #define DEBUG*/

void function_cmd(int gpe)
{
	int deb, nbre, nbre2, i;    
	
	int reset = 0;
	int dli = -1;
	int get_name = 0;
	char param_link[256];
	char cmd_serial_name[256]; 

	int GpeIn=-1;
	CMD_Serial *cmd = NULL;	

	dli = find_input_link(gpe, 0); 
	i = 0;
	while (dli != -1)
    	{
	
		if (prom_getopt(liaison[dli].nom, "-r", param_link) == 1 || prom_getopt(liaison[dli].nom, "-R", param_link) == 1)
      		{
			reset = 1;
			GpeIn= liaison[dli].depart;
      		}
		if (prom_getopt(liaison[dli].nom, "-n", param_link) == 2 || prom_getopt(liaison[dli].nom, "-N", param_link) == 2)
      		{
       			strncpy(cmd_serial_name,param_link, 256);
			get_name = 1;
			GpeIn= liaison[dli].depart;
		}	
		i++;
		dli = find_input_link(gpe, i);	
	}

	if(GpeIn==-1) EXIT_ON_ERROR("Unable to find a link with -N or -R option !");

	if( get_name )
	{
		cmd = cmd_serial_get_cmd_serial_by_name(cmd_serial_name);
	}
	else
	{
		cmd=cmd_serial_get_first_cmd_serial();
	}

	if( cmd == NULL ) EXIT_ON_ERROR("Unable to find cmd serial !");
	
	deb = def_groupe[GpeIn].premier_ele;
	nbre = def_groupe[GpeIn].nbre;
	nbre2 = def_groupe[GpeIn].taillex * def_groupe[GpeIn].tailley;
	
	if(reset==0)
	{
//		cmd_serial_set_cmd(cmd, (int *)(&neurone[deb].s1), (int *)(&neurone[deb+1].s1), (int *)(&neurone[deb+2].s1) );
		cmd_serial_set_cmd(cmd, (int *)(&neurone[deb].s1), (int *)(&neurone[deb+1].s1) );
	}
	else
	{
		cmd_serial_set_cmd_reset(cmd, neurone[deb].s1 );
	}
	//cprints("fin %s, led rgb = %f\n", __FUNCTION__, neurone[deb].s);
	dprints("fin %s, led rgb = %f\n", __FUNCTION__, neurone[deb].s);

}
