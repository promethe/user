/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_distance_sensor_get_distance f_distance_sensor_get_distance

\brief 


\section Modified
- author: Arnaud Blanchard - David Bailly
- description: specific file creation
- date: 20/07/2009

\details
Send a vector of position to the motors depending definition in .dev.

\section Description
Send a vector of position to the motors depending definition in .dev.
\section Macro
-none

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-tools/Vision/affiche_pdv()

\section Links
- position

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
\file 


*/


#include <libx.h>
#include <net_message_debug_dist.h>
#include <dev.h>
#include <device.h>
#include <Components/distance_sensor.h>

#define MAXIMUM_SIZE_OF_OPTION 128

typedef struct box_distance_sensor_get_distance{
  Distance_sensor *distance_sensor;
  int index_of_neuron;
  int number_of_neurons;
}Box_distance_sensor_get_distance;


void new_distance_sensor_get_distance(int index_of_group)
{
  Box_distance_sensor_get_distance *box;
  char id_of_device[MAXIMUM_SIZE_OF_OPTION];
  Device *device=NULL;
  int index_of_link, success;

  type_groupe *group;
  type_liaison *link;
#ifdef DEBUG
  dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  /** On retrouve le groupe de neurones */
  group = &def_groupe[index_of_group];

  /** On creer la boite et l'associe au groupe */
  box = ALLOCATION(Box_distance_sensor_get_distance);
  group->data = box;

  /** On recherche le lien entrant s'il existe. */
  index_of_link = find_input_link(index_of_group, 0);
  if (index_of_link == -1) link = NULL;
  else link = &liaison[index_of_link];
  if (find_input_link(index_of_group, 1) != -1)  EXIT_ON_ERROR("Group: %d, has more than one incoming link !", index_of_group);


  /** On recherche le parametre id sur le lien  s'il y en a un.*/
  if (link!=NULL)
  {
    success = prom_getopt(link->nom, "id=", id_of_device);
    if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);
  }
  else success = 0;

  /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
  if (success == 2) device = dev_get_device("distance_sensor", id_of_device);
  else if (success == 0) device = dev_get_device("distance_sensor", NULL);
  else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);
  
  box->distance_sensor = (Distance_sensor*) device->components[0];
  dprints("distance_sensor = %p\n", (void*)box->distance_sensor);

  /** On defini les neurones de la boite. */
  box->index_of_neuron = group->premier_ele;
  box->number_of_neurons = group->nbre;
#ifdef DEBUG
  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}

void function_distance_sensor_get_distance(int index_of_group)
{
  Box_distance_sensor_get_distance *box;
  Distance_sensor *distance_sensor;
  int i, deb, fin;
  
#ifdef DEBUG
  dprints("entering %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
  /** On recupere la boite a partir du groupe*/
  box = (Box_distance_sensor_get_distance*)def_groupe[index_of_group].data;

  /** On recupere les neurones de la boite et les moteurs*/
  deb = box->index_of_neuron;
  fin = deb + box->number_of_neurons;
  distance_sensor = box->distance_sensor;

  /** On recupere la position pour chaque moteur */
  for (i=deb; i < fin; i++)
  {
    neurone[i].s1 = distance_sensor->get_distance(distance_sensor);
    neurone[i].s = neurone[i].s1;
    neurone[i].s2 = neurone[i].s1;
  }
#ifdef DEBUG
  dprints("exiting %s (%s line %i)\n", __FUNCTION__, __FILE__, __LINE__);
#endif
}

void destroy_distance_sensor_get_distance(int index_of_group)
{
    free(def_groupe[index_of_group].data);
}

