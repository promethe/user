/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_compass f_compass
\brief 
\file  
/section Author
Name: XXXXXXXX
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description

 Lit la boussole et met la valeur de l'angle lu dans la
 sortie du premier neurone du groupe (entre 0 et 1)

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/




#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/* #define DEBUG*/

/* lit la boussole et met la valeur de l'angle lu dans la */
/* sortie du premier neurone du groupe (entre 0 et 1) */
void function_compass(int gpe_sortie)
{
    int deb_s;
    float Angle;
    Compass * c = NULL;
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
    dprints("debut %s\n", __FUNCTION__);
    deb_s = def_groupe[gpe_sortie].premier_ele;
    /* En mode simul sur lel ien d'entee, on met l'angle de la boussole a 0 */
    if (strcmp(liaison[find_input_link(gpe_sortie, 0)].nom, "simul") == 0)
    {
        dprints("compass simul\n");
        Angle = -180;
        Angle /= 360.0;
        Angle += 0.5;
    }
    /*else if(strcmp(liaison[find_input_link(gpe_sortie,0)].nom,"rotate")==0)
       {

       GoToLeftRightBySpeedUncond(2,0);
       Angle =  AngleBoussole();
       Angle /= 355.0;
       Angle += 0.5;
       } */
    else
    {
        /*while(!LastTargetReached()==1)  {;} */
        dprints("compass read\n");
     	c=compass_get_first_compass();

        Angle = compass_read(c);
	Angle /= 360.0;
        Angle += 0.5;
//	printf("angle obtenu = %f\n", Angle); 
        while ((Angle < 0) || (Angle >= 1))
        {
            if (Angle < 0)		 		Angle = Angle + 1;
            else if (Angle >= 1)		Angle = Angle - 1;
        }
    }

/*valeur du compass*/
    neurone[deb_s].s = neurone[deb_s].s1 = neurone[deb_s].s2 = Angle;
/*rempli les neurones de tilt et roll pour les compass de type : USE_COMPASS_ROBUROC (9)*/
	if( def_groupe[gpe_sortie].nbre==3){

	 neurone[deb_s+1].s = neurone[deb_s+1].s1 = neurone[deb_s+1].s2 = c->tilt;
	 neurone[deb_s+2].s = neurone[deb_s+2].s1 = neurone[deb_s+2].s2 = c->roll;
	}

    /*if (p_trace > 0) ;*/

    dprints("fin %s, compass = %f\n", __FUNCTION__, Angle);
    

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Time in function_compass\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}
