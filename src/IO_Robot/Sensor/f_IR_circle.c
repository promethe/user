/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_IR_circle f_IR_circle

\brief Utilisation des IR du robot Koala

\section Modified
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004

\details

\section Description
function puting the 16 koala IR sensors on a 32 neurons cercle -PI Pi
This function is usefull if obstacle avoidance should be preformed by a Neural field.
\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org

\file 



Doc similaire a celle du groupe ...
PLus de details...
*/
#include <libx.h>
#include <libhardware.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*angles des capteurs pour le moment en constante, finira dans le .dev*/
#define PI 3.14159265
/*#define DEBUG*/

#include <IO_Robot/GenericTools.h>
#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

void function_IR_circle(int gpe)
{
    int deb, nbre, nbre2, i, middle = -1;

    float res = -1, angle;
    float /*sigma, */ IR[16];   /*tableau des valeur des ir */
    /*recuperation des orientation des ir */
    float R0_angle;
    float R1_angle;
    float R2_angle;
    float R3_angle;
    float R4_angle;
    float L0_angle;
    float L1_angle;
    float L2_angle;
    float L3_angle;
    float L4_angle;
    float toto=0.;
    int dl = 1, dli = -1;
    int opt1=0, opt2=0;
    char *st;
    int gpe_reset = -1;
    int index_nbre=0;  /* Evite le débordement  */
    int choix_capteur=READ_US;
    char param_link[256];
    /* float j, psy; */
    Robot *robot =NULL;
    US_Serial * us_serial = NULL;


    deb = def_groupe[gpe].premier_ele;
    nbre = def_groupe[gpe].nbre;
    nbre2 = def_groupe[gpe].taillex * def_groupe[gpe].tailley;

    if (nbre2 != nbre)
    {
        cprints("Neurones of this group should be on one line!\n");
        exit(0);
    }

    if (nbre2 == 1)
    {
        cprints("This group should have more than 1 neuron!!\n");
        exit(0);
    }

/*     printf("debut IR_circle\n"); */
/*resolution*/
    res = 2 * PI / nbre;
    middle = (int) nbre / 2;
/*printf("Res: %f, middle:%d\n",res,middle);*/
/* sensors reading */

/*printf("apres get_ir\n");*/
/*printf("apres get_ir\n");*/
    dli = find_input_link(gpe, 0);  /* recherche le 1er lien */
    i = 0;
    while (dli != -1)
    {
        if (dli == -1)
        {
            dprints("error no input group for group, dl=1 %d\n", gpe);
            dl = 1;
            toto = 1.;
        }
        else
        {
            dl = (int) liaison[dli].norme;
            st = strstr(liaison[dli].nom, "-c");
            if (st != NULL)
                toto = atof(&st[2]);

            if (strcmp(liaison[dli].nom, "reset") == 0)
            {
                gpe_reset = liaison[dli].depart;
                dprints("IR en mode reset\n");
            }
            if (strcmp(liaison[dli].nom, "ir") == 0)
            {
    		choix_capteur=READ_IR;
                dprints("IR en mode reset\n");
            }
	    if ((opt1=prom_getopt(liaison[dli].nom, "-us_s", param_link)) >= 1 || (opt2=prom_getopt(liaison[dli].nom, "-US_S", param_link)) >= 1)
            {
    		 choix_capteur=READ_US_S;
		 if(opt1==2||opt2==2) us_serial = us_serial_get_us_serial_by_name( param_link );
		 else us_serial = us_serial_get_first_us_serial();
      	    }
            /*printf("c: %f, dl: %d\n",c,dl); */
        }
        i++;
        dli = find_input_link(gpe, i);
    }
    

    switch(choix_capteur)
    {
	case READ_US_S : 
		us_serial_get_us(us_serial, IR);
		break;
	case READ_IR :
    		robot = robot_get_first_robot();
		robot_get_ir_sensor(robot, IR); 
		break;
	case READ_US : 
	default :	
    		robot = robot_get_first_robot();
		robot_get_ir(robot, IR);	
		break;		
    }

    if (gpe_reset != -1)
        if (neurone[def_groupe[gpe_reset].premier_ele].s1 > 0.99)
        {
            for (i = def_groupe[gpe].premier_ele;
                 i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
            {
                neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0.;
            }
            return;
        }
    
    if( choix_capteur==READ_US_S) {
               for (i = 0; i <  us_serial->nb_us  ; i++)
                {
                        index_nbre=(int) ((us_serial->angle_us[i] + 180.) * (nbre-1) / 360.) + deb;
                        neurone[index_nbre].s = neurone[index_nbre].s1 = neurone[index_nbre].s2 = IR[i];

                        dprints("f_IR_circle(%s): neuron %i has IR value %f\n", def_groupe[gpe].no_name, (int) ((us_serial->angle_us[i] + 180.) * nbre / 360), IR[i]);
                }
    }
    else if (robot_get_robot_type(robot) == 3 || robot_get_robot_type(robot) == 1 || robot_get_robot_type(robot) == 6||robot_get_robot_type(robot) == 5 || robot_get_robot_type(robot) == USE_ROBUBOX_V2)
    {
      
        if( choix_capteur==READ_US) {
       		for (i = 0; i <  robot->nb_ir  ; i++)
       		{

	    		index_nbre=(int) ((robot->angle_ir[i] + 180.) * (nbre-1) / 360.) + deb;
	  	     	neurone[index_nbre].s = neurone[index_nbre].s1 = neurone[index_nbre].s2 = IR[i];

	     		dprints("f_IR_circle(%s): neuron %i has IR value %f\n", def_groupe[gpe].no_name, (int) ((robot->angle_ir[i] + 180.) * nbre / 360), IR[i]);
       		}
	}
        else if( choix_capteur==READ_IR) {
       		for (i = 0; i <  robot->nb_ir_sensor  ; i++)
       		{
	    		index_nbre=(int) ((robot->angle_ir_sensor[i] + 180.) * (nbre-1) / 360.) + deb;
	  	     	neurone[index_nbre].s = neurone[index_nbre].s1 = neurone[index_nbre].s2 = IR[i];

	     		dprints("f_IR_circle(%s): neuron %i has IR value %f\n", def_groupe[gpe].no_name, (int) ((robot->angle_ir_sensor[i] + 180.) * nbre / 360), IR[i]);
       		}
	}

    }
    else if (robot_get_robot_type(robot) == 4)
    {
       for (i = 0; i < robot->nb_ir; i++)
       {
	  if (fabs(robot->angle_ir[i]) < 170.)
	  {
	     neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.) +
	     deb].s =
		neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.)
		+ deb].s1 =
		neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.)
		+ deb].s2 = IR[i];
	  }
	  /*else
	  {
	     neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.) +
	     deb].s =
		neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.)
		+ deb].s1 =
		neurone[(int) ((robot->angle_ir[i] + 180.) * nbre / 360.)
		+ deb].s2 = 0.;
		}*/
       }
    }
    else if (robot_get_robot_type(robot) == 2)
    {
        R0_angle = robot->angle_ir[5];
        R1_angle = robot->angle_ir[6];
        R2_angle = robot->angle_ir[7];
        R3_angle = robot->angle_ir[8];
        R4_angle = robot->angle_ir[9];
        L0_angle = robot->angle_ir[4];
        L1_angle = robot->angle_ir[3];
        L2_angle = robot->angle_ir[2];
        L3_angle = robot->angle_ir[1];
        L4_angle = robot->angle_ir[0];
        dprints("R0:%f,L0:%f\n", R0_angle, L0_angle);
        for (i = 0; i < robot->nb_ir; i++)
        {
            if (IR[i] < robot->seuil_ir)
                IR[i] = 0.;
        }
        for (i = -middle; i < middle; i++)
        {
            angle = res * i;

            if (angle <= (R3_angle * PI / 2) / 90)
            {

                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[8] + IR[7] - IR[1] - IR[2];

            }
            else if (angle > (R3_angle * PI / 2) / 90
                     && angle <= (R2_angle * PI / 2) / 90)
            {

                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[7] + IR[8] + (IR[7] - IR[2]);
            }
            else if (angle <= (R1_angle * PI / 2) / 90
                     && angle > ((R2_angle * PI / 2) / 90))
            {
                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[6] + IR[7] + IR[8] + (IR[6] - IR[3]);

            }
            else if (angle > (R1_angle * PI / 2) / 90
                     && angle <= (6 * PI / 2) / 90)
            {
                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    toto * IR[5] + (IR[5] - IR[4]);

            }
            else if (angle >= -(6 * PI / 2) / 90
                     && angle < ((L1_angle * PI / 2) / 90))
            {
                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    toto * IR[4] + (IR[4] - IR[5]);

            }

            else if (angle < ((L2_angle * PI / 2) / 90)
                     && angle >= ((L1_angle * PI / 2) / 90))
            {
                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[3] + IR[2] + IR[1] + (IR[3] - IR[6]);

            }

            else if (angle < (L3_angle * PI / 2) / 90
                     && angle >= ((L2_angle * PI / 2) / 90))
            {

                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[2] + IR[1] + (IR[2] - IR[7]);

            }
            else if (angle >= (L3_angle * PI / 2) / 90)
            {

                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 =
                    IR[1] + IR[2] - IR[8] - IR[7];

            }
            else
            {
                neurone[deb + i + middle].s = neurone[deb + i + middle].s1 =
                    neurone[deb + i + middle].s2 = 0;
            }
        }
        if (neurone[deb + i + middle].s1 > 0)
            dprints("Neuron pos:%d , seuil %f  ,value: %f\n", i + middle,
                   (res * i * 90 / (PI / 2)), neurone[deb + i + middle].s1);
    }

    dprints("---------------------------End-------------------------\n");
}










#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
 /*#define DEBUG */
typedef struct My_Data_f_IR_circle_target
{
    int gpe_in;
} My_Data_f_IR_circle_target;

void function_IR_circle_target(int gpe)
{
    int gpe_in, ind;
    int i,  l;
    float angle;
    My_Data_f_IR_circle_target *my_data;

#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif
    dprints("debut %s\n", __FUNCTION__);
    /*
       if(debug>0)
       affiche_message("---- function_affiche_pdv_xy ----\n");
     */
    if (def_groupe[gpe].data == NULL)
    {
        i = 0;
        gpe_in = 0;
        l = find_input_link(gpe, i);
        if (l == -1)
        {
            cprints("probleme, pas dee groupe d'entree dans %s\n",
                   __FUNCTION__);
            exit(0);
        }
        gpe_in = liaison[l].depart;
        if (def_groupe[gpe_in].nbre != def_groupe[gpe].nbre)
        {
            cprints("pb taille ds %s\n", __FUNCTION__);
            exit(0);
        }
        my_data =
            (My_Data_f_IR_circle_target *)
            malloc(sizeof(My_Data_f_IR_circle_target));
        my_data->gpe_in = gpe_in;
        def_groupe[gpe].data = (My_Data_f_IR_circle_target *) my_data;
    }
    else
    {
        my_data = (My_Data_f_IR_circle_target *) def_groupe[gpe].data;
        gpe_in = my_data->gpe_in;
    }

    for (i = 0; i < def_groupe[gpe].nbre; i++)
    {

        neurone[i + def_groupe[gpe].premier_ele].s =
            neurone[i + def_groupe[gpe].premier_ele].s1 =
            neurone[i + def_groupe[gpe].premier_ele].s2 = 0.;
    }
    for (i = 0; i < def_groupe[gpe].nbre; i++)
    {
        angle =
            (i -
             def_groupe[gpe].nbre / 2.) * 2. * M_PI / def_groupe[gpe].nbre;      
        if (angle <= -M_PI / 2.)
           angle = angle  + 85. * 2. * M_PI / 360.;
        else if (angle < 0.)
            angle = angle + 100. * 2. * M_PI / 360.;
        else if (angle < M_PI / 2.)
            angle = angle - 100. * 2. * M_PI / 360.;
        else
            angle = angle - 85. * 2. * M_PI / 360.;

        ind =
            (int) ((angle / M_PI / 2.) * def_groupe[gpe].nbre +
                   def_groupe[gpe].nbre / 2);
        while (ind < 0 || ind >= def_groupe[gpe].nbre)
        {
            if (ind < 0)
                ind = ind + def_groupe[gpe].nbre;
            if (ind >= def_groupe[gpe].nbre)
                ind = ind - def_groupe[gpe].nbre;
        }
        neurone[ind + def_groupe[gpe].premier_ele].s =
            neurone[ind + def_groupe[gpe].premier_ele].s1 =
            neurone[ind + def_groupe[gpe].premier_ele].s2 =
            neurone[i + def_groupe[gpe_in].premier_ele].s1 + neurone[ind +
                                                                     def_groupe
                                                                     [gpe].
                                                                     premier_ele].
            s;
    }
    dprints("===============f_IR_circle\n");
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,
            "Time in function_affiche_pdv_xy\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif

}
