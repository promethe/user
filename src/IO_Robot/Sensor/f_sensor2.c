/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sensor.c 
\brief 

Author: LAGARDE Matthieu
Created: 03/07/2006
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-
Macro:
-none 

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

typedef struct
{
  int gpe_input;            /* groupe de liaision precedant */
  int nbre;               /* longueur de la chaine de neurones (chaque neurone est une expression */
  int deb;                    /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int inputGpe;               /* groupe d'entree pour la liaisoin en arriere */
  int input_deb;
  Sensor *sens;               /* structure de promethe */
  int seuil;
} sensor_data; 

 

void new_sensor2 (int gpe)
{ 
  sensor_data *data;      /* structure de cette fonction */
  char *nom_sens;

  dprints("---------------->enter in %s (%s)\n", def_groupe[gpe].no_name, __FUNCTION__);
  
  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = ALLOCATION(sensor_data);
  if (data == NULL)
    {
      EXIT_ON_ERROR("error in memory allocation in group %s %s\n", def_groupe[gpe].no_name,__FUNCTION__);
    }
  
  /* on charge le ficher avec la description de vitesse et position de chaque servo */
  data->gpe_input = find_input_link(gpe, 0);
  
  if (data->gpe_input == -1)
    {
      printf(" Error, %s group %s doesn't have an input\n", __FUNCTION__,def_groupe[gpe].no_name);
      exit(EXIT_FAILURE);
    }
  
  data->sens = (Sensor *)sensor_get_sens_by_name((char *) (nom_sens = liaison[data->gpe_input].nom));
  
  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->inputGpe = liaison[data->gpe_input].depart;
  data->input_deb = def_groupe[data->inputGpe].premier_ele;
  data->nbre = def_groupe[gpe].nbre;
  data->deb = def_groupe[gpe].premier_ele;

  if(data->nbre!= data->sens->nb_sensor) {
    EXIT_ON_ERROR("Groupe [%s] : Ill-size between hardware sensors (%d) and neural groupe (%d)\n",def_groupe[gpe].no_name,data->sens->nb_sensor, data->nbre);
  }


  sensor_init_sensor(data->sens);

  data->seuil = neurone[def_groupe[gpe].premier_ele].seuil;
  
  /* passer les donnes a la fonction suivant */
  def_groupe[gpe].data = data;

  dprints("---------------->end of %s (%s)\n", def_groupe[gpe].no_name, __FUNCTION__);

}

void function_sensor2(int gpe)
{
  int deb,i;
    sensor_data *data = NULL;
    float seuil;
    Sensor *sens;
    int deb_gpe;
    float valeur_capteur = -1.;

    (void) seuil; // (unused)
    (void) deb_gpe; // (unused)
    (void) valeur_capteur; // (unused)

    data = (sensor_data *)def_groupe[gpe].data;
    if (data == NULL)
      {
	EXIT_ON_ERROR("No data in group %s %s\n", def_groupe[gpe].no_name,__FUNCTION__);
      }
    sens = data->sens;
    deb = data->deb;
    seuil = data->seuil;
    deb_gpe = data->input_deb;
    

    valeur_capteur = sensor_sens_command(sens);

    
    for(i=0 ; i<data->sens->nb_sensor ; i++)
      {
	neurone[deb+i].s = neurone[deb+i].s1 = neurone[deb+i].s2 = data->sens->vecteur_sensor[i];
	dprints("user: sensor[%i]=%f\n",i,neurone[deb+i].s);
	
      }
    
    dprints("-------------->end of %s, group %s\n", __FUNCTION__, def_groupe[gpe].no_name);   
    return;
}
