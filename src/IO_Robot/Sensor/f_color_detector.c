/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_color_detector.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Hasson
- description: specific file creation
- date: 22/06/2010

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
Lit la valeur du detecteur de couleur et active le neurone correspondant a la couleur detectee.

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>
#include <stdlib.h>

/*#define DEBUG*/

typedef struct colordetector_color
{
   float C_r;
   float C_g;
   float C_b;
   char name[256];
}  colordetector_color;

typedef struct mydata_color_detector
{
   int nb_couleur;
   colordetector_color *colors;
   float Interval_C;
   int gpe_R;
   int gpe_G;
   int gpe_B;
} mydata_color_detector;


void new_color_detector(int gpe)
{
   int i, l,ret;
   int taille_gpe;
   char listfile_name[256] = "\0";
   FILE *listfile = NULL;
   mydata_color_detector *my_data = NULL;
   int nb_couleur;
   char param[256];

   my_data = (mydata_color_detector *) malloc(sizeof(mydata_color_detector));

   if (my_data==NULL)
   {
      cprints("Probleme d'allocation memoire du my_data (%s)\n",__FUNCTION__);
   }


   /*recuperation du fichier de parametres*/
   /* recherche les liens en entree */
   l = 0;
   i = find_input_link(gpe, l);

   while (i != -1)
   {
      if (prom_getopt(liaison[i].nom, "f", param) == 2)
      {
         sprintf(listfile_name, "%s",  param);
      }
      else cprints(" link %s not recognized %s \n", liaison[i].nom,__FUNCTION__);

      l++;
      i = find_input_link(gpe, l);
   }

   /*verifications*/
   if (strlen(listfile_name) == 0)
   {
      EXIT_ON_ERROR("%s a besoin d'un lien -f[fichier] en entree (parametrage des couleurs a detecter)\nlistfile_name\n", __FUNCTION__);
   }

   listfile = fopen(listfile_name,"r");

   if (listfile == NULL)
   {
      EXIT_ON_ERROR("impossible d'ouvrir '%s' (%s)\n", listfile_name, __FUNCTION__);
   }

   /*recuperation*/
   TRY_FSCANF(1,listfile,"Nb couleur=%d", &nb_couleur);

   cprints("\nNb couleur = %d\n\n",nb_couleur);

   my_data->nb_couleur = nb_couleur;

   my_data->colors = calloc(nb_couleur, sizeof(colordetector_color));
   if (my_data->colors == NULL)
   {
      EXIT_ON_ERROR("Probleme d'allocation memoire pour les couleurs (%s)\n",__FUNCTION__);
   }

   for (i = 0; i < nb_couleur; i++)
   {
      ret = fscanf(listfile,"\n%s\nred=%f\ngreen=%f\nblue=%f\n", my_data->colors[i].name, &(my_data->colors[i].C_r), &(my_data->colors[i].C_g), &(my_data->colors[i].C_b));

      if (ret!=4)
      {
         EXIT_ON_ERROR("%s:\nred=%f\ngreen=%f\nblue=%f\n\n", my_data->colors[i].name, my_data->colors[i].C_r, my_data->colors[i].C_g, my_data->colors[i].C_b);
      }

      cprints("%s:\nred=%f\ngreen=%f\nblue=%f\n\n", my_data->colors[i].name, my_data->colors[i].C_r, my_data->colors[i].C_g, my_data->colors[i].C_b);

   }
   TRY_FSCANF(1,listfile,"\nInterval de confiance=%f", &(my_data->Interval_C));


   cprints("Interval de confiance = %f\n\n",my_data->Interval_C);

   /*verification integrite du groupe*/
   taille_gpe = def_groupe[gpe].nbre;

   if (taille_gpe != nb_couleur)
   {
      EXIT_ON_ERROR("%s : la taille du groupe doit correspondre au nombre de couleurs pouvant etre detectees\n",__FUNCTION__);
   }

   def_groupe[gpe].data = my_data;
}

void function_color_detector(int gpe)
{
   int deb_gpe;
   int taille_gpe;
   int i;
   int colordetector_value[5];
   float red, green, blue;
   float red_rel = 0., green_rel = 0., blue_rel = 0.;
   float C_r, C_g, C_b;
   int nb_couleur;
   float Interval_C;
   mydata_color_detector *my_data = NULL;
   char* couleur;


   dprints("debut %s\n", __FUNCTION__);

   deb_gpe = def_groupe[gpe].premier_ele;
   taille_gpe = def_groupe[gpe].nbre;
   my_data = def_groupe[gpe].data;

   nb_couleur = my_data->nb_couleur;
   Interval_C = my_data->Interval_C;

   /*initialisation*/
   for (i=deb_gpe ; i<deb_gpe+taille_gpe ; i++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
   }

   dprints("color_detector read\n");

   colordetector_read(colordetector_get_first_colordetector(), colordetector_value);


   red = colordetector_value[0];
   green = colordetector_value[1];
   blue = colordetector_value[2];

   /*couleurs relatives*/
   if (isdiff(red + green + blue, 0.))
   {
      red_rel = red / (red + green + blue);
      green_rel = green / (red + green + blue);
      blue_rel = blue / (red + green + blue);
   }

   dprints("%s : red :%f, green :%f, blue :%f\n",__FUNCTION__, red, green, blue);
   dprints("%s : red_rel:%f, green_rel:%f, blue_rel:%f\n",__FUNCTION__, red_rel, green_rel, blue_rel);

   for (i=0; i<nb_couleur; i++)
   {
      C_r = my_data->colors[i].C_r;
      C_g = my_data->colors[i].C_g;
      C_b = my_data->colors[i].C_b;

      couleur = my_data->colors[i].name;

      /*comparaison couleur percue/couleurs connues*/
      if (fabs(red_rel - C_r) <= Interval_C && fabs(green_rel - C_g) <= Interval_C && fabs(blue_rel - C_b) <= Interval_C)
      {
         neurone[deb_gpe+i].s = neurone[deb_gpe+i].s1 = neurone[deb_gpe+i].s2 = 1;
         cprints("Detection d'une zone %s\n",couleur);
      }
   }

   dprints("fin %s\n\n", __FUNCTION__);
}


