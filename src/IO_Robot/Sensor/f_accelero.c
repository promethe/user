/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\ingroup libIO_Robot
\defgroup f_accelero f_accelero
\brief 
\file  
/section Author
Name: XXXXXXXX
Created: XX/XX/XXXX
\section Modified
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

\section Theoritical description
 - \f$  LaTeX equation: none \f$  

\section Description

 Lit la boussole et met la valeur de l'angle lu dans la
 sortie du premier neurone du groupe (entre 0 et 1)

\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-tools/IO_Robot/Com_Koala/AngleBoussole

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
*/

#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

#define DEBUG

/* lit la boussole et met la valeur de l'angle lu dans la */
/* sortie du premier neurone du groupe (entre 0 et 1) */
void function_accelero(int gpe_sortie)
{
    int result[4];
    
#ifdef TIME_TRACE
    gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

#ifdef DEBUG
    dprints("debut %s\n", __FUNCTION__);
#endif
	
    if(def_groupe[gpe_sortie].nbre != 4 ) EXIT_ON_GROUP_ERROR(gpe_sortie, "il faut 4 neurones dans function_accelero\n");

    /* En mode simul sur lel ien d'entee, on met l'angle de la boussole a 0 */
   
#ifdef DEBUG
        dprints("accelero read\n");
#endif
        accelero_read(accelero_get_first_accelero(),result);
        
    
	neurone[def_groupe[gpe_sortie].premier_ele].s = neurone[def_groupe[gpe_sortie].premier_ele].s1 = neurone[def_groupe[gpe_sortie].premier_ele].s2 = (result[0]-150.)/200.;
	neurone[def_groupe[gpe_sortie].premier_ele+1].s = neurone[def_groupe[gpe_sortie].premier_ele+1].s1 = neurone[def_groupe[gpe_sortie].premier_ele+1].s2 = (result[1]-150.)/200.;
	neurone[def_groupe[gpe_sortie].premier_ele+2].s = neurone[def_groupe[gpe_sortie].premier_ele+2].s1 = neurone[def_groupe[gpe_sortie].premier_ele+2].s2 = (result[2]-150.)/200.;
	neurone[def_groupe[gpe_sortie].premier_ele+3].s = neurone[def_groupe[gpe_sortie].premier_ele+3].s1 = neurone[def_groupe[gpe_sortie].premier_ele+3].s2 = (result[3]-150.)/200.;

#ifdef DEBUG
    dprints("fin %s, acceleros = %d, %d, %d, %d\n", __FUNCTION__, result[0],result[1],result[2],result[3]);
#endif

#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
        MicroSecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
        SecondesFunctionTimeTrace =
            OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec -
            1;
        MicroSecondesFunctionTimeTrace =
            1000000 + OutputFunctionTimeTrace.tv_usec -
            InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace, "Time in function_compass\t%4ld.%06d\n",
            SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    affiche_message(MessageFunctionTimeTrace);
#endif
}


void function_calibre_plateforme(int gpe_sortie)
{
	Joint *x_stab,*y_stab;
	Accelero * accelero;
	char c;
	float posx,posy;
	float posx_min=0.394119,posy_min=.374510,posx_max=0.774511,posy_max=0.758823;
	float posx_stab=0.562746,posy_stab=0.570588;
	int acc[4];
	struct timespec duree_nanosleep,res;
	int i; 
	FILE * config_file=NULL,*points_file=NULL;
	float eqax,eqapx,eqay,eqapy;

	duree_nanosleep.tv_sec = 1;
        duree_nanosleep.tv_nsec = 0;

	cprints("enter calibre_plateforme \n");
	nanosleep(&duree_nanosleep, &res);

	x_stab=joint_get_serv_by_name((char*)"x_stab");
	y_stab=joint_get_serv_by_name((char*)"y_stab");
	accelero=accelero_get_first_accelero();				

	cprints("utiliser le clavier numerique pour donner l'equilibre sur le servi x_stab et y_stab, et q pour quitter\n");

	joint_servo_command(y_stab, posy_stab, -1);
  	joint_servo_command(x_stab, posx_stab, -1);

	posx=x_stab->position;;
	posy=y_stab->position;
	
	if(scanf("%c",&c)){
		while(c!='q')
		{
			if(c=='2')
			{
				posx=x_stab->position;
				posx=posx+1./255.;
			}			
			if(c=='8')
			{
				posx=x_stab->position;
				posx=posx-1./255.;
			}
			if(c=='6')
			{
				posy=y_stab->position;
				posy=posy+1./255.;
			}			
			if(c=='4')
			{
				posy=y_stab->position;
				posy=posy-1./255.;
			}
			joint_servo_command(y_stab, posy, -1);
			joint_servo_command(x_stab, posx, -1);
			cprints("%f,%f\n",posx,posy);
			if(scanf("%c",&c)) {}
			else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
			}
		}
	else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	posx_stab=posx;
	posy_stab=posy;
	
	points_file=fopen("points_file.txt","w+");
	cprints("valeur centrale:\n");
	for(i=0;i<40;i++)
	{
		accelero_read(accelero,acc);
		cprints("%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);	fprintf(points_file,"%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);

	}

	cprints("\n");

	accelero_read(accelero,acc);

	cprints("equilibre: %d %d %d %d\n",acc[0],acc[1],acc[2],acc[3]);
	
	eqax=acc[3];
	eqapx=acc[1];
	eqay=acc[2];
	eqapy=acc[0];

	cprints("utiliser les touches 1,2 pour donner le max sur le servi x_stab, et q pour quitter\n");
	posx=posx_max;
  	joint_servo_command(x_stab, posx_max, -1);
	if(scanf("%c",&c)){
		}
		else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	while(c!='q')
	{
 		if(c=='2')
 		{
			posx=x_stab->position;
			posx=posx+1./255.;
 		}			
 		if(c=='1')
 		{
			posx=x_stab->position;
			posx=posx-1./255.;
 		}
  		joint_servo_command(x_stab, posx, -1);
		dprints("%f,%f\n",posx,posy);
		if(scanf("%c",&c)){
		}
		else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	}	
	posx_max=posx;
	posx=posx_stab;
	joint_servo_command(x_stab, posx, -1);

	cprints("utiliser les touches 1,2 pour donner le min sur le servi x_stab, et q pour quitter\n");
	nanosleep(&duree_nanosleep, &res);
	posx=posx_min;
  	joint_servo_command(x_stab, posx_min, -1);
	if(scanf("%c",&c)){
		while(c!='q')
		{
			if(c=='2')
			{
				posx=x_stab->position;
				posx=posx+1./255.;
			}			
			if(c=='1')
			{
				posx=x_stab->position;
				posx=posx-1./255.;
			}
			joint_servo_command(x_stab, posx, -1);
			dprints("%f,%f\n",posx,posy);
			if(scanf("%c",&c)){
			}
			else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
		}
	}
	else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	posx_min=posx;
	posx=posx_stab;
	joint_servo_command(x_stab, posx, -1);	

	cprints("utiliser les touches 1,2 pour donner le max sur le servi y_stab, et q pour quitter\n");
	nanosleep(&duree_nanosleep, &res);
	posy=posy_max;
  	joint_servo_command(y_stab, posy_max, -1);
	if(scanf("%c",&c)){
		
		while(c!='q')
		{
			if(c=='2')
			{
				posy=y_stab->position;
				posy=posy+1./255.;
			}			
			if(c=='1')
			{
				posy=y_stab->position;
				posy=posy-1./255.;
			}
			joint_servo_command(y_stab, posy, -1);
			dprints("%f,%f\n",posx,posy);
			if(scanf("%c",&c)){
			}
			else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
		}	
	}
		else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	posy_max=posy;
	posy=posy_stab;
	joint_servo_command(y_stab, posy, -1);

	cprints("utiliser les touches 1,2 pour donner le min sur le servi x_stab, et q pour quitter\n");
	nanosleep(&duree_nanosleep, &res);
  	posy=posy_min;
	joint_servo_command(y_stab, posy_min, -1);
	if(scanf("%c",&c)){
			
		while(c!='q')
		{
			if(c=='2')
			{
				posy=y_stab->position;
				posy=posy+1./255.;
			}			
			if(c=='1')
			{
				posy=y_stab->position;
				posy=posy-1./255.;
			}
			joint_servo_command(y_stab, posy, -1);
			dprints("%f,%f\n",posx,posy);
			if(scanf("%c",&c)){
			}
			else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
		}	
	}
			else {EXIT_ON_ERROR("Erreur de lecture du scanf");}
	posy_min=posy;
	dprints("minmax: %f %f %f %f\n",posx_min,posx_max,posy_min,posy_max);
	posy=posy_stab;
	joint_servo_command(y_stab, posy, -1);
	
	config_file=fopen("config_file.txt","w+");
	fprintf(config_file,"POSXMIN = %f\nPOSXMAX = %f\nPOSYMIN = %f\nPOSYMAX = %f\nAX = \nBX = \nAY = \nBY = \nEQAX = %f\nEQAPX = %f\nEQAY = %f\nEQAPY = %f\n",posx_min,posx_max,posy_min,posy_max,eqax,eqapx,eqay,eqapy);
	fclose(config_file);

	cprints("Attender pendant la calibration\n");
/* 	duree_nanosleep.tv_sec = 1;
         duree_nanosleep.tv_nsec = 0;*/
	nanosleep(&duree_nanosleep, &res);
	for(posx=posx_min;posx<=posx_max;posx=posx+2./255.)
	{
		joint_servo_command(x_stab, posx, -1);
		nanosleep(&duree_nanosleep, &res);
		accelero_read(accelero,acc);
		dprints("%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);
		fprintf(points_file,"%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);
	}	
	posx=posx_stab;
	joint_servo_command(x_stab, posx, -1);

	nanosleep(&duree_nanosleep, &res);
	for(posy=posy_min;posy<=posy_max;posy=posy+2./255.)
	{
		joint_servo_command(y_stab, posy, -1);
		nanosleep(&duree_nanosleep, &res);
		accelero_read(accelero,acc);
		dprints("%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);
		fprintf(points_file,"%f %f %d %d %d %d\n",posx,posy,acc[0],acc[1],acc[2],acc[3]);
	}	
	posy=posy_stab;
	joint_servo_command(y_stab, posy, -1);
	
	fclose(points_file);
	cprints("fin de la calibration\n");
	
	(void) gpe_sortie;
}

void function_correction_plateforme(int gpe_sortie)
{
	Accelero * accelero;
	Joint * x_stab,*y_stab;
	float eqax= 284, eqapx=284;
	float eqay= 278, eqapy=288;	
	float cx;
	float dx;
	float cy;
	float dy;	
	float sax,say,ax,ay,s1,s2;
	float apx,apy;	
	int res[5][4];
	float tau=10;
	int * my_data=NULL;
	float offsetx=0.;
	float offsety=0.;

	float a2;
	float b2;
	float a3;
	float b3;	

	if(def_groupe[gpe_sortie].data==NULL)
	{
		neurone[def_groupe[gpe_sortie].premier_ele].s = neurone[def_groupe[gpe_sortie].premier_ele].s1 = neurone[def_groupe[gpe_sortie].premier_ele].s2 = 0.5;
		neurone[def_groupe[gpe_sortie].premier_ele+1].s = neurone[def_groupe[gpe_sortie].premier_ele+1].s1 = neurone[def_groupe[gpe_sortie].premier_ele+1].s2 = 0.5;
		my_data=(int*)malloc( sizeof(int) ) ;
		*my_data=1;
		def_groupe[gpe_sortie].data=my_data;
	}

	x_stab=joint_get_serv_by_name((char*)"x_stab");
	y_stab=joint_get_serv_by_name((char*)"y_stab");

	if(def_groupe[gpe_sortie].nbre!=2)
	{
		EXIT_ON_ERROR("il faut 2 neurones dans function_correction_plateforme\n");
		exit(0);
	}
	accelero=accelero_get_first_accelero();	
	
	a2=accelero->ax;
	b2=accelero->bx;
	a3=accelero->ay;
	b3=accelero->by;

	cx=1./ a2;
	dx=-b2/a2;

	cy=1./a3;
	dy=-b3/a3;

	eqax= accelero->eqax; /*equilibre de l'accelero x en haut*/ 
	eqapx=accelero->eqapx; /*equilibre de l'accelero x en bas*/
	eqay= accelero->eqay;;
        eqapy=accelero->eqapy;	

	accelero_read(accelero,res[0]);
	/*accelero_read(accelero,res[1]);
	accelero_read(accelero,res[2]);
	accelero_read(accelero,res[3]);
	accelero_read(accelero,res[4]);*/
 	apx=(res[0][1]);
 	apy=(res[0][0]); 	

/* 	accelero_read(accelero,res[0]);
 	apx=(res[0][0]);
 	apy=(res[0][1]);	*/
	
	ax=eqax-(apx-eqapx);
	sax=cx*ax+dx    + offsetx;
	
	ay=eqay-(apy-eqapy);
	say=cy*ay+dy    + offsety  ;

	s1=(sax+tau*neurone[def_groupe[gpe_sortie].premier_ele].s1)/(tau+1.);
	s2=(say+tau*neurone[def_groupe[gpe_sortie].premier_ele+1].s1)/(tau+1.);

/* 	printf("%f %f\n",sax,say);*/

	if(s1<accelero->posx_max && s1>accelero->posx_min)	
		joint_servo_command(x_stab, s1, -1);
	if(s2<accelero->posy_max && s2>accelero->posy_min)	
		joint_servo_command(y_stab, s2, -1);
  	

	neurone[def_groupe[gpe_sortie].premier_ele].s = neurone[def_groupe[gpe_sortie].premier_ele].s1 = neurone[def_groupe[gpe_sortie].premier_ele].s2 = s1;
	neurone[def_groupe[gpe_sortie].premier_ele+1].s = neurone[def_groupe[gpe_sortie].premier_ele+1].s1 = neurone[def_groupe[gpe_sortie].premier_ele+1].s2 = s2;
	
}



