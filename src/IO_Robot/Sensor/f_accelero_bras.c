/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_accelero3D.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author:...
- description: lecture de (2 accel + 1 gyro) x 2 pour le controle de 2 ddl
- date: 16/07/2012


Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 Lit le port série correspondant à l'accelerometre, recupere les accelerations sur x, y, z et les mappe sur 10 neurones

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/IO_Robot/Com_Koala/AngleBoussole

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo: see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
/*#define DEBUG*/


#include <stdlib.h>
#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>


/* lit le port serie de l'accelerometre */
/* et met les trois valeurs d'acceleration */
/* sur les trois neurones du groupe */

/* Basé sur f_accelero (~/simulateur/prom_user/prom_user/user/src/IO_Robot/Sensor) */
/* suppression de : function_calibre_plateforme */
/* et : function_correction_plateforme */

#define NB_DATA 11 /* trame = 255 + 255 + 1 int (tps)+ 5 int + 5 int */

void function_accelero_bras(int gpe_sortie)
{
    int deb_s;
    float result[NB_DATA];
    int i;


    dprints("debut %s\n", __FUNCTION__);


    deb_s = def_groupe[gpe_sortie].premier_ele;
	
    if(def_groupe[gpe_sortie].nbre != NB_DATA-1 )
	{
		kprints("il faut 3 neurones dans function_accelero\n");
		exit(0);
	}
   
     dprints("accelero_read_bras , c'est parti !\n");

	/* Lecture du port serie et recuperation des donnees */
     accelero_read_bras(accelero_get_first_accelero(),result);
 
     dprints("read done, mapping sur neurones ...\n");


	/* premiere donnee = temps non utilisee pour l'instant */
	/* La normalisation par 1000 est arbitraire et ad-hoc pour le HMC6343 qui mesure environ 9800 pour l'influence de la gravité */
	for(i=0;i<NB_DATA-1;i++)
	{
	dprints("%s result[%d] = %d \n",__FUNCTION__,i, result[i]);
	  neurone[def_groupe[gpe_sortie].premier_ele+i].s = neurone[def_groupe[gpe_sortie].premier_ele+i].s1 = neurone[def_groupe[gpe_sortie].premier_ele+i].s2 = result[1+i] / (1024.0 );
	}

}

