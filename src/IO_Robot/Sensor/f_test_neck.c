/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_test_neck.c
\brief

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-none

Global variables:
-int USE_HEAD
-int USE_CAM
-int USE_KATANA
-boolean emission_robot

Internal Tools:
-init_pan()
-init_arm()

External Tools:
-tools/IO_Robot/Pan_Tilt/init_pan_tilt()
-tools/IO_Robot/Com_Koala/GoToLeftRightUncond()
-tools/IO_Robot/Serial/serial_open()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/IO_Robot.h>
#include <libhardware.h>
#include <string.h>
/*#define Obj_Robot*/

void function_test_neck(int gpe)
{

   char commande[15],retour[255];
   char p[10],i;
   Serial * ser = serial_get_serial_by_name((char*)"serial_neck");


   if (def_groupe[gpe].nbre<8)
   {
      EXIT_ON_ERROR("f_test_neck a besoin de 8 neurones au moins\n");
   }

//     printf("send commande S \n");
   memset(retour,0,255);
   sprintf(commande,"S");
   serial_send_and_receive_nchar(ser->name, commande,retour,1,10);
   cprints("--- retour = %c %c %d %d %d %d %d %d %d %d  .... \n",retour[0],retour[1],retour[2],retour[3],retour[4],retour[5],retour[6],retour[7],retour[8],retour[9]);

//     sscanf(retour+2,"%c%c%c%c%c%c%c%c",&(p[0]),&(p[1]),&(p[2]),&(p[3]),&(p[4]),&(p[5]),&(p[6]),&(p[7]));

   for (i=0; i<8; i++)
   {
      neurone[def_groupe[gpe].premier_ele+i].s1=neurone[def_groupe[gpe].premier_ele+i].s2=neurone[def_groupe[gpe].premier_ele+i].s= (float)(retour[i+2]) / 255.;
   }

//     getchar();

}
