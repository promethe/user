/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
//#define DEBUG

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>

#define GPS_SIZE_POS_ONLY 3
#define GPS_SIZE_POS_COMP_SP 5
#define GPS_SIZE_ALL 11

typedef enum enum_Gps_size {
   Gps_size_POS_ONLY	= GPS_SIZE_POS_ONLY ,
   Gps_size_POS_COMP_SP	= GPS_SIZE_POS_COMP_SP ,
   Gps_size_ALL			= GPS_SIZE_ALL
} Gps_size;


typedef struct data_gps {
   Gps *gps ;
   Gps_size gps_gpe_size;
} data_gps;


void new_gps(int gpe) {
   Gps *gps = NULL ;
   char gps_name[256] = "\0";
   char param_link[256];
   int nb_links = 0 ;
   int links_in = -1 ;
   data_gps *my_data ;
   int gpe_size ;

   /* Check group size */
   gpe_size = def_groupe[gpe].nbre ;
   if ( (gpe_size != GPS_SIZE_POS_ONLY) && (gpe_size != GPS_SIZE_POS_COMP_SP) && (gpe_size != GPS_SIZE_ALL) ) {
      EXIT_ON_ERROR("The group must either have GPS_SIZE_POS_ONLY(%d), GPS_SIZE_POS_COMP_SP(%d) or GPS_SIZE_ALL(%d) neurons instead of %d \n", GPS_SIZE_POS_ONLY, GPS_SIZE_POS_COMP_SP, GPS_SIZE_ALL, def_groupe[gpe].nbre);
   }

   /* Parse links in */
   if (def_groupe[gpe].data == NULL) {
      links_in = find_input_link(gpe, nb_links);
      while (links_in != -1) {
         if (prom_getopt(liaison[links_in].nom, "-h", param_link) == 2 || prom_getopt(liaison[links_in].nom, "-H", param_link) == 2){
            strcpy(gps_name, param_link);
         }
         links_in = find_input_link(gpe, ++nb_links);
      }
   }

   if(strlen(gps_name) != 0) {
      gps = gps_get_gps_by_name(gps_name) ;
      if (gps == NULL)
         EXIT_ON_ERROR("No gps named %s was found \n", gps_name) ;
   }
   else {
      gps = gps_get_first_gps() ;
      if (gps == NULL)
         EXIT_ON_ERROR("No gps was found at all\n") ;
   }

   /* Create object */
   my_data = ALLOCATION(data_gps);
   my_data->gps = gps ;
   my_data->gps_gpe_size = gpe_size ;
   def_groupe[gpe].data = (data_gps *) my_data;

   sleep(3); // Wait for GPS to be initialized (TODO: find a better way to do this)
}


void function_gps(int gpe) {
   Gps *gps = NULL ;
   Gps_value *gps_value;
   int debut;
   data_gps *my_data ;
   int gpe_size ;

   my_data = (data_gps *) def_groupe[gpe].data;
   if (my_data == NULL)
      EXIT_ON_ERROR("Cannot retrieve data of group %s \n", def_groupe[gpe].no_name);

   gps = my_data->gps ;
   gpe_size = my_data->gps_gpe_size ;

   gps_read(gps);
   gps_value = gps->gps_value;

   if(gps_value != NULL) {
      /* Enregistrement des neurones */
      debut = def_groupe[gpe].premier_ele;
      neurone[debut].s=neurone[debut].s1=neurone[debut].s2=gps_value->est;
      neurone[debut+1].s=neurone[debut+1].s1=neurone[debut+1].s2=gps_value->nord;
      neurone[debut+2].s=neurone[debut+2].s1=neurone[debut+2].s2=gps_value->altitude;
      if(gpe_size >= GPS_SIZE_POS_COMP_SP) {
         neurone[debut+3].s=neurone[debut+3].s1=neurone[debut+3].s2=gps_value->speed;
         neurone[debut+4].s=neurone[debut+4].s1=neurone[debut+4].s2=gps_value->compass;
      }
      if(gpe_size == GPS_SIZE_ALL) {
         neurone[debut+5].s=neurone[debut+5].s1=neurone[debut+5].s2=gps_value->day;
         neurone[debut+6].s=neurone[debut+6].s1=neurone[debut+6].s2=gps_value->month;
         neurone[debut+7].s=neurone[debut+7].s1=neurone[debut+7].s2=gps_value->year;
         neurone[debut+8].s=neurone[debut+8].s1=neurone[debut+8].s2=gps_value->hour;
         neurone[debut+9].s=neurone[debut+9].s1=neurone[debut+9].s2=gps_value->minute;
         neurone[debut+10].s=neurone[debut+10].s1=neurone[debut+10].s2=gps_value->second;
      }

      dprints("###############\n");
      dprints("UTC date : %f/%f/%f \n",gps_value->day,gps_value->month,gps_value->year);
      dprints("UTC time : %f h %f min %f s \n",gps_value->hour,gps_value->minute,gps_value->second);
      dprints("Nord : %f\n",gps_value->nord);
      dprints("Est : %f\n",gps_value->est);
      dprints("Speed : %f\n",gps_value->speed);
      dprints("Compass : %f\n",gps_value->compass);
      dprints("Altitude : %f\n", gps_value->altitude);

   }
   else
      PRINT_WARNING("Unable to read GPS values !\n");

}


void destroy_gps(int gpe) {
   free(def_groupe[gpe].data) ;
}	

