/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_IR_simul.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
simulation des IR sur 4 neurones repr�entant droite gauche avant arri�e
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-tools/Vision/affiche_pdv()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <Sound/Sound.h>
#include <Kernel_Function/find_input_link.h>

/*#define DEBUG*/

static int tps, tps2;
static int cpt;
void function_IR_simul(int numero)
{

    int i = 0, l = -1;
    int s = 0;

    l = find_input_link(numero, i);
    while (l != -1)
    {

        if (strncmp(liaison[l].nom, "cond", 4) == 0)
        {
            s = 1;
        }

        i++;
        l = find_input_link(numero, i);

    }

    switch (s)
    {

    case 1:
        for (i = 0; i < def_groupe[numero].nbre; i++)
        {

            neurone[def_groupe[numero].premier_ele + i].s = 0;
            neurone[def_groupe[numero].premier_ele + i].s1 = 0;
            neurone[def_groupe[numero].premier_ele + i].s2 = 0;

        }

        cpt++;

        if (tps2 == 3)
            tps2 = 1;
        else
            tps2++;

        if (tps2 == 3)
        {

            neurone[def_groupe[numero].premier_ele].s = 1;
            neurone[def_groupe[numero].premier_ele].s1 = 1;
            neurone[def_groupe[numero].premier_ele].s2 = 1;

        }
        else if (tps2 == 2)
        {

            neurone[def_groupe[numero].premier_ele + 1].s = 1;
            neurone[def_groupe[numero].premier_ele + 1].s1 = 1;
            neurone[def_groupe[numero].premier_ele + 1].s2 = 1;
        }
        else
        {
            neurone[def_groupe[numero].premier_ele + 2].s = 1;
            neurone[def_groupe[numero].premier_ele + 2].s1 = 1;
            neurone[def_groupe[numero].premier_ele + 2].s2 = 1;
        }
        break;




    case 0:
        for (i = 0; i < def_groupe[numero].nbre; i++)
        {

            neurone[def_groupe[numero].premier_ele + i].s = 0;
            neurone[def_groupe[numero].premier_ele + i].s1 = 0;
            neurone[def_groupe[numero].premier_ele + i].s2 = 0;
        }
        if (tps == 3)
            tps = 1;
        else
            tps++;

        if ((global_learn == 1) && (cpt < 50))
        {


            if (tps == 1)
            {
                neurone[def_groupe[numero].premier_ele].s = 1;
                neurone[def_groupe[numero].premier_ele].s1 = 1;
                neurone[def_groupe[numero].premier_ele].s2 = 1;
            }
            else if (tps == 2)
            {

                neurone[def_groupe[numero].premier_ele + 1].s = 1;
                neurone[def_groupe[numero].premier_ele + 1].s1 = 1;
                neurone[def_groupe[numero].premier_ele + 1].s2 = 1;
            }
            else
            {
                neurone[def_groupe[numero].premier_ele + 2].s = 1;
                neurone[def_groupe[numero].premier_ele + 2].s1 = 1;
                neurone[def_groupe[numero].premier_ele + 2].s2 = 1;
            }
        }
        if (cpt > 100)
            usleep(200000);
        break;
    default:
        break;

    }

    return;

}
