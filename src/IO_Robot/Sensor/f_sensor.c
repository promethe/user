/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_sensor.c 
\brief 

Author: LAGARDE Matthieu
Created: 03/07/2006
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-
Macro:
-none 

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>

struct F_SENSOR_STRUCT
{
    int no_neur;
    Sensor *sens;
    float seuil;
    int deb_gpe;
};


void function_sensor(int Gpe)
{
    int lien = -1;
    int inputGpe, deb;
    float pos = 0;
    char *nom_sens;
    struct F_SENSOR_STRUCT *this_struct = NULL;
    float seuil;
    Sensor *sens;
    int deb_gpe;

    if (def_groupe[Gpe].data == NULL)
    {

        lien = find_input_link(Gpe, 0);
        if (lien == -1)
        {
            fprintf(stderr,
                    "f_sensor : Gpe : %d : Absence de  lien entrants...\n",
                    Gpe);
            exit(1);
        }


        sens =
            (Sensor *)
            sensor_get_sens_by_name((char *) (nom_sens = liaison[lien].nom));
        inputGpe = liaison[lien].depart;
        deb = def_groupe[inputGpe].premier_ele;

        this_struct = malloc(sizeof(struct F_SENSOR_STRUCT));
        if (this_struct == NULL)
        {
            fprintf(stderr,
                    "f_sensor : Erreur alloc memeoire dans f_sensor gpe %d\n",
                    Gpe);
            exit(1);
        }
        def_groupe[Gpe].data = (void *) this_struct;
        deb_gpe = this_struct->deb_gpe = def_groupe[Gpe].premier_ele;
        this_struct->no_neur = deb;
        this_struct->sens = sens;
        seuil = this_struct->seuil =
            neurone[def_groupe[Gpe].premier_ele].seuil;
    }
    else
    {
        this_struct = (struct F_SENSOR_STRUCT *) def_groupe[Gpe].data;
        sens = this_struct->sens;
        deb = this_struct->no_neur;
        seuil = this_struct->seuil;
        deb_gpe = this_struct->deb_gpe;
    }

    pos = neurone[deb].s1;
    if (pos >= seuil)
        pos = sensor_sens_command(sens);
    neurone[deb_gpe].s2 = neurone[deb_gpe].s1 = neurone[deb_gpe].s = pos;

    printf("fsensor : position recue : %f\n", pos);
#ifdef DEBUG
    printf("fsensor : position recue : %f\n", pos);
#endif
    return;
}
