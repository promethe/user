/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_IR_localisation f_IR_localisation
   \ingroup libIO_Robot

   \brief Recupere les donnees du hagisonic localisation device.

   \details

   \section Description

   Recupere les donnees du hagisonic localisation device.
   1er neurone: coordonnee x (cm)
   2eme neurone: coordonnee y (cm)
   3eme neurone: angle (degres)

   \file
   \ingroup f_IR_localisation
   \brief

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Hasson
   - description: specific file creation
   - date: 22/02/2010


   Description:
   Recupere les donnees du hagisonic localisation device.
   1er neurone: coordonnee x (cm)
   2eme neurone: coordonnee y (cm)
   3eme neurone: angle (degres)


   http://www.doxygen.org
************************************************************/

/*#define DEBUG*/
#include <string.h>
#include <libx.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <stdlib.h>
#include <net_message_debug_dist.h>


void new_IR_localisation(int gpe)
{
  /*verification taille du groupe*/
  if(def_groupe[gpe].nbre != 3)
  {
    EXIT_ON_ERROR("le groupe f_IR_localisation doit contenir 3 neurones (x, y, angle)");
  }
}


void function_IR_localisation(int gpe)
{
  int deb_gpe;
  int taille_gpe;
  float X;
  float Y;
  float Angle;
  float IR_localisation_value[3];
  IR_localisation *IR_loc = NULL;
  
  deb_gpe = def_groupe[gpe].premier_ele;
  taille_gpe = def_groupe[gpe].nbre;
 
  dprints("IR_localisation read\n");
  
  /*recuperation des donnees du capteur*/
  IR_loc = IR_localisation_get_first_IR_localisation();
  if (IR_loc == NULL)
  {
    EXIT_ON_ERROR("No IR_localisation defined");
  }

  IR_localisation_read(IR_loc, IR_localisation_value);
  
  X = IR_localisation_value[0];
  Y = IR_localisation_value[1];
  Angle = IR_localisation_value[2];

  dprints("%s:\nX:%f\tY:%f\tAngle:%f\n", __FUNCTION__, X, Y, Angle);

  neurone[deb_gpe].s = neurone[deb_gpe].s1 = neurone[deb_gpe].s2 = X;
  neurone[deb_gpe+1].s = neurone[deb_gpe+1].s1 = neurone[deb_gpe+1].s2 = Y;
  neurone[deb_gpe+2].s = neurone[deb_gpe+2].s1 = neurone[deb_gpe+2].s2 = Angle;
}


void destroy_IR_localisation(int gpe)
{
  (void)gpe;
  /** empty - not very useful */
}
