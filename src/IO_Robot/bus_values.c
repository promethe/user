/*
 * bus.c
 *
 *  Created on: Jan 21, 2014
 *      Author: arnablan
 */

#include <libx.h>
#include <stdlib.h>
#include "outils.h"
#include "dev.h"
#include "device.h"

#include <Components/data.h>

#define MAXIMUM_SIZE_OF_OPTION 128
#define BUFFER_MAX 512

typedef struct bus_get_matrix
{
   type_bus *bus;
   float *values;
   int values_nb;
} type_bus_get_matrix;

typedef struct bus_set_values
{
   float *values;
   int values_nb;
   type_bus *bus;
   type_groupe *input_group;
} type_bus_set_values;

void new_bus_get_values(int gpe)
{
   char id_of_device[MAXIMUM_SIZE_OF_OPTION];
   char channel[MAXIMUM_SIZE_OF_OPTION];

   char *link_name;
   Device *device=NULL;
   int link_id;
   int success;
   type_bus_get_matrix *my_data;
   type_groupe *group;

   int nb_links = 0;

   group = &def_groupe[gpe];

   my_data = ALLOCATION(type_bus_get_matrix);

   /* On parcours tous les liens entrant */
   for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, nb_links))
   {
      link_name = liaison[link_id].nom;
      success = prom_getopt(link_name, "-i", id_of_device);

      /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
      if (success == 2) device = dev_get_device("bus", id_of_device);
      else if (success == 0) device = dev_get_device("bus", NULL );
      else if (success == 1) EXIT_ON_GROUP_ERROR(gpe, "The parameter of '-i' is not valid on the link :'%s'.", link_name);

      nb_links++;
   }

// bus->add_channel();
   my_data->values_nb = group->taillex*group->tailley;
   if (my_data->values_nb != device->total_elements_nb) EXIT_ON_ERROR("The total number of elements : %d does not fit the number of neurons %d", device->total_elements_nb, my_data->values_nb  );
   my_data->values = MANY_ALLOCATIONS(my_data->values_nb, float);

   my_data->bus = *(type_bus**)device->components;

   group->data = my_data;
}

void function_bus_get_values(int gpe)
{
   int  i, j, values_nb;
   type_groupe *group;
   type_bus_get_matrix *my_data;
   type_bus *bus;

   group = &def_groupe[gpe];
   my_data = group->data;
   bus = my_data->bus;

   bus->get_values(bus, my_data->values);

   for (j=0; j< group->tailley; j++)
   {
      for (i=0 ; i< group->taillex; i++)
      {
         neurone[group->premier_ele + i + j*group->taillex ].s = neurone[group->premier_ele + i + j*group->taillex ].s1 =neurone[group->premier_ele + i + j*group->taillex ].s2 = my_data->values[j*group->taillex + i];
      }
   }
}

void destroy_bus_get_values(int gpe)
{
   type_bus_get_matrix *my_data;
   my_data = def_groupe[gpe].data;
   free(my_data->values);
   free(my_data);
}


void new_bus_set_values(int gpe)
{
   char id_of_device[MAXIMUM_SIZE_OF_OPTION];
   char channel[MAXIMUM_SIZE_OF_OPTION];

   char *link_name;
   Device *device=NULL;
   int link_id;
   int success;
   type_bus_set_values *my_data;
   type_groupe *group;

   int nb_links = 0;

   group = &def_groupe[gpe];

   my_data = ALLOCATION(type_bus_set_values);

   /* On parcours tous les liens entrant */
   for (link_id = find_input_link(gpe, 0); link_id != -1; link_id = find_input_link(gpe, nb_links))
   {
      link_name = liaison[link_id].nom;
      success = prom_getopt(link_name, "-i", id_of_device);

      /** S'il existe on associe l'appareil de la boite a l'appareil ayant l'id. Sinon on l'associe au seul appareil de meme type. */
      if (success == 2) device = dev_get_device("data", id_of_device);
      else if (success == 0) device = dev_get_device("data", NULL );
      else if (success == 1) EXIT_ON_GROUP_ERROR(gpe, "The parameter of 'i' is not valid on the link :'%s'.", link_name);

      my_data->input_group = &def_groupe[liaison[link_id].depart];
      nb_links++;
   }

   if (nb_links != 1) EXIT_ON_ERROR("You must have exactly one input link");

// bus->add_channel();
   my_data->values_nb = my_data->input_group->taillex*my_data->input_group->tailley;
   my_data->values = MANY_ALLOCATIONS(my_data->values_nb, float);

   my_data->bus = *(type_bus**)device->components;

   group->data = my_data;
}

void function_bus_set_values(int gpe)
{
   char buffer[BUFFER_MAX];
   int ret, i, j, values_nb;
   type_groupe *group, *input_group;
   type_bus_set_values *my_data;
   type_bus *bus;

   group = &def_groupe[gpe];
   my_data = group->data;
   bus = my_data->bus;
   input_group = my_data->input_group;

   for (j=0; j< input_group->tailley; j++)
   {
      for (i=0 ; i< input_group->taillex; i++)
      {
         my_data->values[j*input_group->taillex + i] = neurone[input_group->premier_ele + i + j*input_group->taillex ].s1;
      }
   }
   bus->set_values(bus, my_data->values);
}

void destroy_bus_set_values(int gpe)
{
   type_bus_set_values *my_data;
   my_data = def_groupe[gpe].data;
   free(my_data->values);
   free(my_data);
}

