/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  braittenberg_koala_follow_detect_goal.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 deplacement du robot en ligne droite
 avec contournement d'obstacle
 renvoit 1 ou 2 si but detecte

 Macro:
 -COM_D
 -COM_G
 -COM_H
 -ANGLE90

 Local variables:
 -float Orientation

 Global variables:
 -none

 Internal Tools:
 -AngleTrigo()
 -MotivationStopb()
 -MotivationStop()
 -fct_IR()
 -integre_chemin()


 External Tools:
 -tools/IO_Robot/Com_Koala/commande_koala()
 -tools/IO_Robot/Com_Koala/koala_ir()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <stdio.h>
#include <sys/times.h>

#include <Macro/IO_Robot/macro.h>
#include <libhardware.h>
/*#include "include/local_var.h"
 */
#include <Global_Var/IO_Robot.h>    /*for orientation */
#include <libhardware.h>
#include <IO_Robot/GenericTools.h>

#include "tools/include/MotivationStopb.h"
#include "tools/include/integre_chemin.h"

int braittenberg_koala_follow_detect_goal(float distance, double *norme, double *beta_tot, double *teta)
{
  /*   static int        first=1; */
  int parm[2];
  /* char                *nothing=0; */
  /*   int               taille_groupe_e;        */
  /*   int               taille_groupe_s;                */
  /*   int               deb_e, deb_s; */
  /*   int               increment; */
  /*   char              nom_liaison[32]; */
  /*   static int        first_koala=1; */
  /*   int               i, j, n; */
  int j;
  /*   double    max; */
  /*   int               reponse; */
  /*   int               tailleX_e, tailleY_e; */
  /*   static float      *retard; */
  float IR[16];
  /*   int               IR_aff[16]; */
  float mot[2];
  /*   int               offset; */
  /* char                data[80]; */
  int param[2];
  int d[2];
  float Angle;
  float somme = 0.0;
  int dist[2];
  int dsav[2] =
    { 0, 0 };
  int dinst[2] =
    { 0, 0 };
  /*   double        largeur_robot=9500.0; */
  /*   double    new_norme; */

  /* Coefficients du braittenberg */
  /* static float               W[2][4]={{1.5, 1., 1., 1.5},{1.5, 1., 1., 1.5}}; */

  /* Valeur des poids */
  static float
  /*    W[2][14]={
   {-0.5, -0.5, -0.5, -0.5,   1., -0.5, -0.75,            -0.5, -0.5, -0.5, -0.5,    -1., 0.5, 2.},
   {0.5, 0.5, 0.5, 0.5,      -1., 0.5, 2.,                  0.5, 0.5, 0.5, 0.5,    1., -0.5, -0.75}
   }; */
  W[2][14] =
    {
      { -0.7, -0.7, -0.7, -0.7, 1., -0.7, -0.9, -0.7, -0.7, -0.7, -0.7, -1.2, 0.5, 2. },
      { 0.5, 0.5, 0.5, 0.5, -1.2, 0.5, 2., 0.5, 0.5, 0.5, 0.5, 1., -0.7, -0.95 } };

  /*   float             val, hasard0,hasard1; */
  float sum_drte, sum_gche, signe_devant;
  int /*start, */stop, compteur;
  struct tms buffer;
  int trouve = 0;

  printf("\n\nbraittenberg_koala_follow_detect_goal\n");

  /*val = sec * (float)CLK_TCK;
   diff = (int)val;
   start = times(&buffer);
   stop = times(&buffer); */

  printf("-----------  braittenberg koala detect goal -----------\n");

  /* Initialise position at 0, 0 */
  /* en prendra les infos d'odometrie pour mettre a jours l'orientation */
  parm[0] = 0;
  parm[1] = 0;

  robot_init_wheels_counter(robot_get_first_robot());
  compteur = 0;

  printf("~~~~~~~~~~~~~~~ Norme=%f  Distance=%f\n", *norme, distance);

  while (*norme < distance)
  {
    compteur++;
    printf("compteur de lecture capteur: %d\n", compteur);

    robot_get_ir(robot_get_first_robot(), IR);

    mot[0] = mot[1] = 0.;

    sum_gche = 0.;
    sum_drte = 0.;

    for (j = 0; j < 6; j++)
    {
      sum_gche += j * IR[j];
      sum_drte += j * IR[j + 8];
    }
    if (sum_gche > sum_drte) signe_devant = -1;
    else signe_devant = 1;

    for (j = 0; j < 4; j++) /* 4 capteurs gauche et droite */
    {
      mot[0] += signe_devant * W[0][j] * fct_IR(IR[j]) + signe_devant * W[0][j + 7] * fct_IR(IR[j + 8]);
      mot[1] += signe_devant * W[1][j] * fct_IR(IR[j]) + signe_devant * W[1][j + 7] * fct_IR(IR[j + 8]);
    }
    for (j = 4; j < 6; j++) /* 3 capteurs gauche et droite suivants */
    {
      mot[0] += W[0][j] * fct_IR(IR[j]) + W[0][j + 7] * fct_IR(IR[j + 8]);
      mot[1] += W[1][j] * fct_IR(IR[j]) + W[1][j + 7] * fct_IR(IR[j + 8]);
    }

    mot[0] += 0.3 + 1. * (float) ((fct_IR(IR[0]) + fct_IR(IR[1]) + fct_IR(IR[2]) + fct_IR(IR[3]) + fct_IR(IR[8]) + fct_IR(IR[9]) + fct_IR(IR[10]) + fct_IR(IR[11])) < 0.1);
    mot[1] += 0.3 + 1. * (float) ((fct_IR(IR[0]) + fct_IR(IR[1]) + fct_IR(IR[2]) + fct_IR(IR[3]) + fct_IR(IR[8]) + fct_IR(IR[9]) + fct_IR(IR[10]) + fct_IR(IR[11])) < 0.1);

    somme = somme + mot[0] * 30. + mot[1] * 30.;

    param[0] = (int) (mot[0] * 30.);
    param[1] = (int) (mot[1] * 30.);

    printf("V0=%d  V1=%d\n", param[0], param[1]);

    robot_go_by_speed(robot_get_first_robot(), param[0], param[1]);

    stop = times(&buffer);
    trouve = MotivationStop();

    robot_read_wheels_counter(robot_get_first_robot(), dist, dist + 1);

    /*printf("Distances    D1 = %d  et D2 = %d\n",dist[0],dist[1]); */
    dinst[0] = dist[0] - dsav[0];
    dinst[1] = dist[1] - dsav[1];
    /*printf("Distances instantanees    D1inst = %d  et D2inst = %d\n",dinst[0],dinst[1]); */
    dsav[0] = dist[0];
    dsav[1] = dist[1];

    integre_chemin(dinst[0], dinst[1], teta, norme, beta_tot);

    printf("La norme est %f soit %f centimetres\n", *norme, *norme * 0.003);
    printf("Le beta_tot est= %f soit %f degre\n", *beta_tot, *beta_tot * (180 / 3.14259265));
    printf("Le theta est %f soit %f degre\n", *teta, *teta * (180 / 3.14259265));
  }
  param[0] = param[1] = 0;
  robot_go_by_speed(robot_get_first_robot(), param[0], param[1]);
  /*    3-2-1-0-----8-9-10-11   */
  /*    |                   |   */
  /*    4       Avant      12   */
  /*    |                   |   */
  /*    |                   |   */
  /*    5                  13   */
  /*    |                   |   */
  /*    |                   |   */

  /*    6-7----------15----14   */

  /* Mise a jour de l'orientation */
  robot_read_wheels_counter(robot_get_first_robot(), d, d + 1);

  Angle = ((float) d[0] - d[1]) / 2.;
  Angle = Angle / (float) (robot_get_first_robot()->angle90) * 90.;

  Orientation = AngleTrigo(Orientation + Angle);

  printf("trouve = %d \n", trouve);
  printf("somme activite motrice = %f \n", somme);
  (void) stop;
  (void) parm;
  return trouve;
}
