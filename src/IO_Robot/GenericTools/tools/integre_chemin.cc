/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  integre_chemin.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Modified:
- author: N.Cuperlier
- description: Modification du calcul de Beta pour le rendre compatible avec la nouveau de l'odom�trie.
- date: 06/12/2004


Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
N.C: Cette fonction est appel�e dans integr_deplacement_elementaire pour la fonction move_via_speed.
Elle ne realise plus exactement le meme calcul qu� l'epoque de VB.
 Cette fonction utilise uniquement la norme et le beta_tot...
 D�sormais la direction par defaut (teta � l'initialisation) = 0 (au lieu de PI/2 avec VB)
 Ajout d'info dans le cas du DEBUG

 Historiquement:
    Fonction V B :    On calcul les coordonnees polaires du vecteur deplacement "instantane" 
                   grace a la distance parcourue par chacune des 2 roues (gauche&droite)  
 Le vecteur deplacement instantane (mini_norme,beta) est ensuite aggrege au vecteur       
 deplacement total (norme,beta_tot).  Teta est l 'orientation courante du robot par 	    
 rapport a sa direction initiale sur le cercle trigonometrique: Si pas de changement de   
 direction alors teta=pi/2, si le robot a tourne de 90deg a droite alors teta=0,...	    
 les distances peuvent etre (+,+), (+,-), (-,+)


Macro:
-PI
-PI_2

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <math.h>
#include <stdio.h>
#include "include/macro.h"
#define DEBUG

void integre_chemin(int d0, int d1,double *teta,double *norme,double *beta_tot)
{
  double d2,alpha,beta;
  double dd;
  double largeur_robot=6100;/*8500*;/*9500.0;*/
  /*double mini_norme,new_norme;*/
  int reverse;

  reverse=0;
  if (d0!=d1)
	{ printf("d0:%d,d1:%d\n",d0,d1);
	  if(d0<d1) {dd=d0; d0=d1; d1=dd; reverse=1;} /*inverse D0 et D1 -> D0>d1*/
	  /*calcul du rayon d2 pour obtenir alpha. Il faut resoner*/
	  if (d1==0)
	    {
	      d2=(double)( (largeur_robot*0.01)/(d0-0.01) );
	      alpha=(float)(0.01/d2);
	    }
	  else
	    {
	      d2=(double)( (largeur_robot*d1)/(d0-d1) );
	      alpha=(float)(d1/d2);
	    }
#ifdef DEBUG
	  printf("Alpha :%f\n",alpha);
#endif
	  beta=alpha;/*(PI-alpha)/2.0;*/
#ifdef DEBUG
	  printf("reverse:%d\n",reverse);
#endif
	  if(reverse) beta=/*PI*/-beta;
#ifdef DEBUG
	  printf("beta avant mise a jour reverse :%f\n",beta);
#endif
	/*  beta= **teta/PI_2-beta;*/
#ifdef DEBUG
	  printf("beta apres mise a jour brute :%f\n",beta);
#endif
	  if (beta<-PI)  beta+=2*PI;
	  else if (beta>=PI)  beta-=2*PI;
	  *norme=(2*d2+largeur_robot)*(sin(alpha/2.0));

	  *beta_tot=beta;
	}
     else
       *norme=*norme+d0;

}



