/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\ingroup move_via_speed
\ingroup move_via_mvector_given_speed
\brief Retourne les informations d'odometries correpondant au veritable deplacement effectue.

\details

\section Description
  Retourne les informations d'odometries correpondant au veritable deplacement effectue.

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.cuperlier
- description: specific file creation
- date: 01/09/2004




External Tools:
- integre_chemin() le chemin va changer... a mettre a jour
- tools/IO_Robot/Com_Koala/Read_Robot_Position_Counter(...)

************************************************************/
#include "tools/include/integre_chemin.h"
#include <libhardware.h>
#include <stdio.h>


/*#define DEBUG*/
void integr_deplacement_elementaire(int *dsav, double *norme,
                                    double *beta_tot, double *teta)
{
    int dist[2];
    int dinst[2] = { 0, 0 };

    /*N.C on relie la distance reellement parcourue par les deux roues */
    printf("avant read counter\n");
    robot_read_wheels_counter(robot_get_first_robot(), &dist[0], &dist[1]);
#ifdef DEBUG
    printf("Distances cumulees   D1 = %d  et D2 = %d\n", dist[0], dist[1]);
#endif
    dinst[0] = dist[0] - dsav[0];
    dinst[1] = dist[1] - dsav[1];

#ifdef DEBUG
    printf("Distances instantanees    D1inst = %d  et D2inst = %d\n",
           dinst[0], dinst[1]);
#endif


    integre_chemin(dinst[0], dinst[1], teta, norme, beta_tot);
    dsav[0] = dist[0];
    dsav[1] = dist[1];
#ifdef DEBUG
    printf("-------------------------------------------------");
    printf("La norme est %f soit %f centimetres\n", *norme, *norme * 0.003);
    printf("Le beta_tot est= %f soit %f degre\n", *beta_tot,
           *beta_tot * (180 / 3.14259265));
    printf("-------------------------------------------------");
#endif
}
