/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>
#include <sys/time.h>

/* #include "../../../include/ben_defines.h" */

/** ***********************************************************
\file  f_servo_tete.c 
\brief 

Author: B.Fouque
Created: 01/12/08

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
- Activation des servos de la tete a partir d'un codage neuronal analogique en entree

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

#define SERVOS_SPEED_MAX      5

#define SERVO_TETE_FILENAME "servo_tete.param"

#define SERVOS_NB_MAX 13 /*!< nombre maximal de servos */
#define STR_NAME_SIZE           100 /*!< Taille maximale d'un nom d'expression faciale (une categorie) */
#define SERVOS_POSITIONS_NB_MAX 10 /*!< nombre maximal de positions des servo-moteurs (correspondent à des intensités différentes) */

typedef struct
{
  char           servos_names[SERVOS_NB_MAX][STR_NAME_SIZE];
  int            servos_nb;
  int            servos_speed;
  int            input_gpe;
  int            input_first;
  int            input_taillex;
  int            input_tailley;
} t_servo_tete;

void servo_tete_check_integrity (t_servo_tete* data)
{
  if (data->input_taillex != 1 || data->input_tailley != data->servos_nb)
    {
      printf ("servo_tete_check_integrity(): pas le bon nombre de neurones alloues dans leto\n");
      printf ("disponibles sur x:%i  attendu:%i\n", data->input_taillex, 1);
      printf ("disponibles sur y:%i  attendu:%i\n", data->input_tailley, data->servos_nb);
      exit (1);
    }
}

void servo_tete_check_fscanf_return (int code, int lieu, int i)
{
  if (code <= 0)
    {
      printf ("servo_tete_check_fscanf_return(): erreur de lecture du fichier de params\n");
      printf ("code:%i  lieu:%i  position fichier:%i\n", code, lieu, i);
      exit (1);
    }
}

/*!
 * \brief Chargement des noms des servos (on actionne un servo par son nom).
 */
void load_servo_names (t_servo_tete* data)
{
  FILE*    fp = NULL;
  char     servo_name[STR_NAME_SIZE] = "rien";
  int      nb_servos = 0;
  int      servos_speed = 0;
  int      code = 0;
  int      i = 0;
 
  /* chargement de l'en-tete du fichier */
  if (data == NULL /* || filename == NULL */)
    {
      printf ("servo_tete: load_servo_names(): un des parametres est nul\n");
      exit (1);
    }
  fp = fopen (SERVO_TETE_FILENAME, "r");
  if (fp == NULL)
    {
      printf ("servo_tete: load_servo_names(): impossible d'ouvrir le fichier: %s\n",
	      SERVO_TETE_FILENAME/* filename */);
      exit (1);
    }

  code = fscanf (fp, "nb_servos=%i\n", &nb_servos);
  servo_tete_check_fscanf_return (code, 1, i);
  printf ("nb_servos=%i\n", nb_servos);
  if (nb_servos == 0 || nb_servos != nb_servos)
    {
      printf ("servo_tete: load_servo_names(): pb de lecture du nbre de servos:%i\n", nb_servos);
      exit (1);
    }
  if (nb_servos > SERVOS_NB_MAX)
    {
      printf ("servo_tete: load_servo_names(): pas assez de memoire allouee statiquement pour stocker les donnnes des servos\n");
      printf ("disponible:%i  necessaire:%i\n", SERVOS_NB_MAX, nb_servos);
      exit (1);
    }
  data->servos_nb = nb_servos;
  code = fscanf (fp, "vitesse=%i\n", &servos_speed);
  servo_tete_check_fscanf_return (code, 2, i);
  if (servos_speed <= 0 || servos_speed > SERVOS_SPEED_MAX || servos_speed != servos_speed)
    {
      printf ("servo_tete: load_servo_names(): parametre de vitesse de servo incorrect\n");
      exit (1);
    }
  data->servos_speed = servos_speed;

  for (i = 0; i < nb_servos; i++)
    {
      code = fscanf (fp, "%s", servo_name);
      servo_tete_check_fscanf_return (code, 3, i);
      if (strlen (servo_name) == 0)
	{
	  printf ("servo_tete: load_servo_names(): nom du servo %i mal lu\n", i);
	  exit (1);
	}
      strcpy (data->servos_names[i], servo_name);
    }

  fclose (fp);
}

void new_servo_tete (int gpe)
{
  t_servo_tete*   data = NULL;
  int             input_link = -1;
/*   int             test1 = SERVOS_POSITIONS_NB_MAX; */
/*   int             test2 = STR_NAME_SIZE; */

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = (t_servo_tete *) malloc (sizeof (t_servo_tete));
  if (data == NULL)
    {
      printf("new_servo_tete(): error in memory allocation in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  def_groupe[gpe].data = data;
  if (def_groupe[gpe].data != data || def_groupe[gpe].data == NULL
      || def_groupe[gpe].data != def_groupe[gpe].data)
    {
      printf ("new_servo_tete(): pb d'allocation du champs data\n");
      exit (1);
    }

  /* Recuperation des donnees du groupe d'entree */
  input_link = find_input_link (gpe, 0);
  if (input_link == -1)
    {
      printf("new_servo_tete(): Error, %s group %i doesn't have an input\n",
	     __FUNCTION__, gpe);
      exit(EXIT_FAILURE);
    }
  data->input_gpe = liaison[input_link].depart;
  data->input_first = def_groupe[data->input_gpe].premier_ele;
  data->input_taillex = def_groupe[data->input_gpe].taillex;
  data->input_tailley = def_groupe[data->input_gpe].tailley;

  /* initialisations */
  data->servos_nb = 0;
  data->servos_speed = 0;

  /* Chargement des positions des servos a partir d'un fichier */
  load_servo_names (data);
  
  servo_tete_check_integrity (data);
}

/*!
 * \brief Activation des servos de la tete a partir d'un codage neuronal analogique en entree.
 */
void function_servo_tete (int gpe)
{
  t_servo_tete       *data = NULL;
  Joint              *joint_servo = NULL; /* structure de promethe pour commander un servo */
  float               servo_pos = 0.0;
  int                 servos_nb = 0;
  int                 servos_speed = 0;
  int                 input_first = 0;
  int                 i = 0;

  /* Chargement des donnees du groupe */
  data = (t_servo_tete *)def_groupe[gpe].data;
  if (data == NULL)
    {
      printf("servo_tete(): error while loading data in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  
  /* Deplacement des servos */
  servos_nb = data->servos_nb;
  servos_speed = data->servos_speed;
  input_first = data->input_first;
  printf ("name:%s pos:%f\n", data->servos_names[i], servo_pos);
  for (i = 0; i < servos_nb; i++)
    {
       /* if (i < 8) */
/* 	 { */
/* 	   joint_servo = joint_get_serv_by_name(data->servos_names[i]); */
/* 	   if (i == 0) */
/* 	     joint_servo_command (joint_servo, 0.2, servos_speed); */
/* 	   else if (i >= 1 && i <= 4) */
/* 	     joint_servo_command (joint_servo, 0.45, servos_speed); */
/* 	   else if (i >= 5 && i <= 6) */
/* 	     joint_servo_command (joint_servo, 0.5, servos_speed); */
/* 	   else if (i == 7) */
/* 	     joint_servo_command (joint_servo, 0.65, servos_speed); */
/* 	   continue ; */
/* 	 } */
      
      /* recuperation de la future position du servo dans le groupe d'entree */
      servo_pos = neurone[input_first + i].s2;
      if (servo_pos < 0.0 || servo_pos > 1.0 || !isequal(servo_pos,servo_pos))
	{
	  printf ("servo_tete(): Attention: une valeur envoyee a un servo sort de l'interval autorise\n");
	  printf ("servo:%s  valeur:%f\n", data->servos_names[i], servo_pos);
	  exit (1);
	}

      /* activation des servos */
      if (!isequal(servo_pos,0.0))
	{
	  printf ("name:%s pos:%f\n", data->servos_names[i], servo_pos);
	  joint_servo = joint_get_serv_by_name(data->servos_names[i]);
	  joint_servo_command (joint_servo, servo_pos, servos_speed);
	  printf ("name:%s pos:%f\n", data->servos_names[i], servo_pos); 
	}
    }
/*   printf ("\n"); */
}
