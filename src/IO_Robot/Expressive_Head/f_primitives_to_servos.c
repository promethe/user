/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_primitives_to_servos.c
\brief

Author: VILLEMIN Jean-Baptiste
Created: 2009

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Convertit un codage intensité en valeur analogique pour chaque primitive en un codage "servo-moteur/position".
	Permet à partir d'une valeur analogique pour une primitive de retrouver et d'envoyer la bonne commande servo-moteur.
	On utilise le fichier primitives.param pour obtenir les commandes servo-moteurs adéquates en fonction de la valeur analogique de l'intensite.

	[Exemple de fichiers param :]
		tilt_front		0	0.04	0.08	0.12	0.16	0.2	0.3	0.4	0.5	0.6	0.7	5	0
		sourcil_left_int	0.3	0.33	0.36	0.39	0.42	0.45	0.48	0.51	0.54	0.57	0.6	5	0
		sourcil_right_int	0.55	0.58	0.61	0.64	0.67	0.70	0.73	0.76	0.79	0.82	0.85	5	0
		sourcil_left_ext	0.6	0.63	0.66	0.69	0.72	0.75	0.78	0.81	0.84	0.87	0.9	5	0
		sourcil_right_ext	0.6	0.63	0.66	0.69	0.72	0.75	0.78	0.81	0.84	0.87	0.9	5	0
		tilt_mouth_center	0.05	0.07	0.09	0.11	0.13	0.15	0.25	0.35	0.45	0.60	0.84	5	1
		tilt_mouth_left		0.4	0.48	0.56	0.64	0.72	0.8	0.84	0.88	0.92	0.96	1.0	5	2
		tilt_mouth_right	0.4	0.48	0.56	0.64	0.72	0.8	0.84	0.88	0.92	0.96	1.0	5	2
		pan_mouth_left		0.3	0.33	0.36	0.39	0.42	0.45	0.48	0.51	0.54	0.57	0.6	5	2
		pan_mouth_right		0.3	0.33	0.36	0.39	0.42	0.45	0.48	0.51	0.54	0.57	0.6	5	2
		tilt_eyes		0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	5	3
		left_eye		0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	5	3
		right_eye		0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	0.5	5	3

		----------------------------------------------------------------------------------------------------------------------------------------------------
		Nom			Pos	Pos	Pos	Pos	Pos	PosN	Pos	Pos	Pos	Pos	Pos	Vitesse	Prim
	[Fin exemple]

	La boite doit recevoir 2 liens :
		"learn" pour la condition d'apprentissage
		et un autre lien donnant les valeurs analogiques des intensites des primitives


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <libhardware.h>
#include <sys/time.h>

#define NB_SERVOS 13			/* Nombre de servos */
#define NB_VAL_SERVO 11			/* Nombre de valeurs de commandes possibles pour chaque servo */
#define FILENAME "primitives.param"	/* Nom du fichier a ouvrir contenant les valeurs de commandes possibles */
#define SIZENAME_SERVOS 100		/* Taille max du nom des servo-moteurs */


typedef struct
{
   char name[SIZENAME_SERVOS];		/* Nom du servo-moteur */
   float servos_command[NB_VAL_SERVO]; 	/* Tableau de valeurs de commandes possibles recupere et genere a partir du fichier primitives.param */
   int vitesse;				/* Vitesse du servo-moteur */
   int primitive;				/* Appartient à quelle primitive ? */
} servo;

typedef struct
{
   int flag;		/*neurone indiquant si le reseau apprend ou pas */
   int gpe_data;		/* gpe contenant les valeurs analogiques */
   servo mes_servos[NB_SERVOS];
} data;




void new_primitives_to_servos(int Gpe)
{

   data *my_data=NULL;
   int i,j,l;
   FILE*   fp = NULL;

   dprints(" ~~~~~~~ Constructeur %s ~~~~~~\n", __FUNCTION__);

   /**
   * Creation de la structure qui contiendra les donnees
   */

   my_data = (data *) malloc(sizeof(data));
   if (my_data == NULL)
   {
      printf("error in memory allocation in group %d %s\n", Gpe,__FUNCTION__);
      exit(EXIT_FAILURE);
   }

   /**
   * Recherche des liens d'entrees pour obtenir les bons neurones
   */
   l = 0;
   i = find_input_link(Gpe, l);
   while (i != -1)
   {
      if (strcmp(liaison[i].nom, "learn") == 0)
         my_data->flag = liaison[i].depart;
      else
         my_data->gpe_data = liaison[i].depart;
      i = find_input_link(Gpe, l);
      l++;
   }

   my_data->flag = def_groupe[my_data->flag].premier_ele;
   my_data->gpe_data = def_groupe[my_data->gpe_data].premier_ele;

   dprints("Flag = %d\n",my_data->flag);
   dprints("Gpedata = %d\n",my_data->gpe_data);

   /**
   * Recuperation des valeurs depuis le fichier FILENAME pour remplir le tableau des servos
   */

   fp = fopen(FILENAME,"r");
   if (fp == NULL)
   {
      printf ("Probleme d'ouverture fichier: %s\n", FILENAME);
      exit (1);
   }

   for (i = 0; i < NB_SERVOS; i++)
   {
      /* Recuperation du nom */
      TRY_FSCANF(1,fp,"%s", my_data->mes_servos[i].name);


      /* Recuperation des positions */
      for (j = 0; j < NB_VAL_SERVO; j++)
      {
         TRY_FSCANF(1,fp,"%f", &(my_data->mes_servos[i].servos_command[j]));
      }

      /* Recuperation de la vitesse */
      TRY_FSCANF(1,fp,"%d", &(my_data->mes_servos[i].vitesse));

      /* Recuperation de la primitive associee */
      TRY_FSCANF(1,fp,"%d", &(my_data->mes_servos[i].primitive));
   }

   fclose (fp);


#ifdef DEBUG
   printf("Tableau recupere :\n");
   for (i = 0; i < NB_SERVOS; i++)
   {
      /* Recuperation du nom */
      printf("%s\n", my_data->mes_servos[i].name);


      /* Recuperation des positions */
      for (j = 0; j < NB_VAL_SERVO; j++)
      {
         printf("%0.3f ", my_data->mes_servos[i].servos_command[j]);
      }

      /* Recuperation de la vitesse */
      printf("\t%d\t", my_data->mes_servos[i].vitesse);

      /* Recuperation de la primitive associee */
      printf("%d\n\n", my_data->mes_servos[i].primitive);
   }
#endif


   /**
   * Passe les donnees a la fonction
   */
   def_groupe[Gpe].data = my_data;

   dprints(" ~~~~~~~ FIN constructeur %s ~~~~~~\n", __FUNCTION__);

}


/**
*	------------------- FIN DU CONSTRUCTEUR ------------------------
*/

void function_primitives_to_servos(int Gpe)
{
   data *my_data = NULL;
   int i;
   float val_ana;
   float position;
   Joint *joint_servo = NULL; /* structure de promethe pour commander un servo */

   dprints("---------------------Begin of %s -------------------\n", __FUNCTION__);

   /**
   * Recuperation donnees data
   */

   my_data = (data *) def_groupe[Gpe].data;
   if (my_data == NULL)
   {
      EXIT_ON_ERROR("error while loading data in group %d %s\n", Gpe,__FUNCTION__);
   }

   /**
   * En fonction flag, choix voie a prendre
   */

   if (neurone[my_data->flag].s1 < 0.5)
   {

      for (i = 0; i < NB_SERVOS; i++)
      {
         /* Calcul position en fonction valeur analogique */
         val_ana = neurone[my_data->gpe_data + my_data->mes_servos[i].primitive].s;
         position = my_data->mes_servos[i].servos_command[(int)(round(10 * val_ana))];

         dprints("Val_ana: %0.3f\t Pos: %0.3f\n",val_ana,position);

         /* Envoie des commandes aux servos-moteur */
         joint_servo = joint_get_serv_by_name(my_data->mes_servos[i].name);
         joint_servo_command (joint_servo, position, my_data->mes_servos[i].vitesse);

      }


   }

   dprints("---------------------End of %s -------------------\n", __FUNCTION__);
}

