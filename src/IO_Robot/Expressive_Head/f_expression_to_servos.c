/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #include "../../../include/ben_defines.h" */

/** ***********************************************************
\file  f_expression_to_servos.c 
\brief 

Author: B.Fouque
Created: 01/12/08

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
- Convertit une expression faciale d'un codage type (emotion,intensite) a un codage type (servo, pos).

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

/*!
 * Fichier de description des neurones moteurs a activer pour chaque emotion (=muscles).
 */
#define EMOTIONS_FILENAME "emotions_to_servos.param"

/* #define EMOTIONTOSERVO_ACTIVITY_THRESHOLD 0.1 */
#define EMOTIONTOSERVO_ACTIVITY_THRESHOLD 0.0

#define EMOTION_NB_MAX 5 /*!< nombre maximal d'emotions */
#define EMOTION_INTENSITY_NB_MAX 3 /*!< nombre maximal d'intensites d'emotions */
#define SERVOS_NB_MAX 13 /*!< nombre maximal de servos */
#define STR_NAME_SIZE           100 /*!< Taille maximale d'un nom d'expression faciale (une categorie) */

typedef struct
{
  float          motor_neuron[EMOTION_NB_MAX][SERVOS_NB_MAX][EMOTION_INTENSITY_NB_MAX]; /*!< indique le numero du neurone moteur a activer pour un triplet (emotion, servo, intensite)*/
  int            nb_emotions;
  int            nb_servos;
  int            nb_intensities;
  int            nb_positions_max;
  int            input_gpe;
  int            input_neurons_first;
  int            input_neurons_size;
  int            input_taillex;
  int            input_tailley;
  int            neurons_first;
  int            neurons_size;
  int            taillex;
  int            tailley;
} t_emotion_servo;

void emotiontoservo_check_fscanf_return (int no, int lieu, int i, int j, int k)
{
  if (no <= 0)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): erreur de lecture du fichier. code:%i lieu:%i\n", no, lieu);
      printf ("Avencement boucle: emotion:%i servo:%i intensite:%i\n", i, j, k);
      exit (1);
    }
}


/*! brief Fonction de chargement des parametres des expressions faciales prototypiques.
 *
 * Attention: les emotions, servos et positions sont pris dans l'ordre ou ils viennent:
 *            l'algorithme n'essai pas d'assurer une correspondance au niveau des noms attribues dans les fichiers.
 *            Ces noms sont uniquement la pour clarifier la lecture des fichiers parametres.
 */
void load_emotion_to_motor_neurons_weights (t_emotion_servo* data, char* filename)
{
  FILE*   fp = NULL;
  char    emotion_name[STR_NAME_SIZE] = "rien";
  char    servo_name[STR_NAME_SIZE] = "rien";
  int     nb_emotions = 0;
  int     nb_servos = 0;
  int     nb_intensities = 0;
  int     servo_pos = 0;
  int     max = 0;
  int     i = 0;
  int     j = 0;
  int     k = 0;
  int     code = 0;

  if (data == NULL || filename == NULL)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): un parametre est nul\n");
      exit (1);
    }
  fp = fopen (filename, "r");
  if (fp == NULL)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): Pb d'ouverture fichier: %s\n", filename);
      exit (1);
    }
  
  code = fscanf (fp, "nb_emotions=%i\n", &nb_emotions);
  emotiontoservo_check_fscanf_return (code, 1, 0, 0, 0);
  code = fscanf (fp, "nb_servos=%i\n", &nb_servos);
  emotiontoservo_check_fscanf_return (code, 2, 0, 0, 0);
  code = fscanf (fp, "nb_intensities=%i\n", &nb_intensities);
  emotiontoservo_check_fscanf_return (code, 3, 0, 0, 0);
  if (nb_emotions == 0 || nb_servos == 0 || nb_intensities == 0)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): erreurs sur les quantites\n");
      exit (1);
    }
  if (nb_emotions > EMOTION_NB_MAX)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): pas assez d'emotions allouees statiquement\n");
      printf ("disponibles:%i  necessaires:%i", EMOTION_NB_MAX, nb_emotions);
      exit (1);
    }
  if (nb_servos > SERVOS_NB_MAX)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): pas assez de servos alloues statiquement\n");
      printf ("disponibles:%i  necessaires:%i\n", SERVOS_NB_MAX, nb_servos);
      exit (1);
    }
  if (nb_intensities > EMOTION_INTENSITY_NB_MAX)
    {
      printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): pas assez d'intensites allouees statiquement\n");
      printf ("disponibles:%i  souhaitees:%i\n", EMOTION_INTENSITY_NB_MAX, nb_intensities);
      exit (1);
    }
  data->nb_emotions = nb_emotions;
  data->nb_servos = nb_servos;
  data->nb_intensities = nb_intensities;

  for (i = 0; i < nb_emotions; i++)
    {
      code = fscanf (fp, "emotion : %s\n", emotion_name);
      emotiontoservo_check_fscanf_return (code, 4, i, j, k);
      for (j = 0; j < nb_servos; j++)
	{
	  code = fscanf (fp, "%s=", servo_name);
	  emotiontoservo_check_fscanf_return (code, 5, i, j, k);
	  for (k = 0; k < nb_intensities; k++)
	    {
	      code = fscanf (fp, "%i", &servo_pos);
	      emotiontoservo_check_fscanf_return (code, 6, i, j, k);
	      if (servo_pos > max)
		max = servo_pos;
	      data->motor_neuron[i][j][k] = servo_pos;
	    }
	}
      code = fscanf (fp, "\n");
      if (code < 0)
	{
	  printf ("emotiontoservo: load_emotion_to_motor_neurons_weights(): erreur de lecture du fichier. code:%i\n", code);
	  exit (1);
	}
    }
    
  data->nb_positions_max = max + 1;
  fclose (fp);
}

/*!
 * \brief Fonction de verification d'allocation.
 *
 * Verifie que suffisament de neurones sont alloues avec leto dans le groupe d'entree ET dans le groupe courant.
 */
void emotiontoservo_check_integrity (t_emotion_servo* data)
{
  /* check du groupe d'entree */
  if (data->input_taillex < data->nb_intensities
      || data->input_tailley != data->nb_emotions)
    {
      printf ("emotiontoservo: emotiontoservo_check_integrity(): pas assez de neurones dans le groupe d'entree:\n");
      printf ("nb de neurones sur x:%i  necessaires:%i\n", data->input_taillex, data->nb_intensities);
      printf ("nb de neurones sur y:%i  attendu:%i\n", data->input_tailley, data->nb_emotions);
      exit (1);
    }

  /* check du groupe courant */
  if (data->tailley != data->nb_servos
      || data->taillex < data->nb_positions_max)
    {
      printf ("emotiontoservo: emotiontoservo_check_integrity(): pas assez de neurones dans le groupe courant:\n");
      printf ("nb de neurones sur x:%i  necessaires:%i\n", data->taillex, data->nb_positions_max);
      printf ("nb de neurones sur y:%i  attendu:%i\n", data->tailley, data->nb_servos);
      exit (1);
    }
}

void new_expression_to_servos (int gpe)
{
  t_emotion_servo*     data = NULL;
  int                  input_link = -1;
  int                  nb_servos = 0;
  int                  first = 0;
  int                  neurone_pos = 0;
  int                  i = 0;
  int                  j = 0;

  /* Allocation de la structure */
  data = (t_emotion_servo *)malloc (sizeof (t_emotion_servo));
  if (data == NULL)
    {
      printf("new_emotionservo(): error in memory allocation in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  def_groupe[gpe].data = data;

  /* Recuperation des donnees du groupe d'entree */
  input_link = find_input_link (gpe, 0);
  if (input_link == -1)
    {
      printf("new_emotionservo(): Error, %s group %i doesn't have an input\n",
	     __FUNCTION__, gpe);
      exit(EXIT_FAILURE);
    }
  data->input_gpe = liaison[input_link].depart;
  data->input_neurons_first = def_groupe[data->input_gpe].premier_ele;
  data->input_neurons_size = def_groupe[data->input_gpe].nbre;
  data->input_taillex = def_groupe[data->input_gpe].taillex;
  data->input_tailley = def_groupe[data->input_gpe].tailley;

  /* initialisations */
  data->neurons_first = def_groupe[gpe].premier_ele;
  data->neurons_size = def_groupe[gpe].nbre,
  data->taillex = def_groupe[gpe].taillex;
  data->tailley = def_groupe[gpe].tailley;
  data->nb_emotions = 0;
  data->nb_intensities = 0;
  data->nb_servos = 0;
  data->nb_positions_max = 0;

  /* chargement de la matrice: emotions->neurones moteurs */
  load_emotion_to_motor_neurons_weights (data, (char *)EMOTIONS_FILENAME);

  /* initialisation des neurones */
  nb_servos = data->nb_servos;
  first = data->neurons_first;
  for (i = 0; i < nb_servos; i++)
    {
      for (j = 0; j < data->taillex; j++)
	{
	  neurone_pos = i * data->taillex + j;
	  neurone[first + neurone_pos].s
	    = neurone[first + neurone_pos].s1
	    = neurone[first + neurone_pos].s2
	    = 0.0;
	}
    }

  emotiontoservo_check_integrity (data);
}

/*!
 * Entree: Activitees neuronales des intensites de chaque emotion
 * Sortie: Activitees neuronales des positions de chaque servo
 * Effet: RAS
 * neurones: neurones des intensites de chaque servos
 *
 * Notes: Ajouter la modif de nb_positions_max: super important pour controler que
 *        la taille allouee est suffisante
 */
void function_expression_to_servos (int gpe)
{
  t_emotion_servo    *data = NULL;
  int                 emotion_no = 0;
  int                 emotion_intensity = 0;
  float               max = 0.0;
  float               output = 0.0;
  int                 argmax_i = 0;
  int                 argmax_j = 0;
  int                 first = 0;
  int                 neurone_pos = 0;
  int                 nb_emotions = 0;
  int                 nb_intensities = 0;
  int                 nb_servos = 0;
  int                 nb_positions = 0;
  int                 motor_neuron_no = 0;
  int                 i = 0;
  int                 j = 0;

  /* Chargement des donnees du groupe */
  data = (t_emotion_servo *)def_groupe[gpe].data;
  if (data == NULL)
    {
      printf("emotiontoservo(): error while loading data in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }

  /* recherche de l'emotion en entree (celle obtenant l'activite la plus forte) */
  first = data->input_neurons_first;
  nb_emotions = data->nb_emotions;
  nb_intensities = data->nb_intensities;
  for (i = 0; i < nb_emotions; i++)
    {
      for (j = 0; j < nb_intensities; j++)
	{
	  neurone_pos = i * data->input_taillex + j;
	  output = neurone[first + neurone_pos].s2;
	  if (output >= max)
	    {
	      max = output;
	      argmax_i = i;
	      argmax_j = j;
	    }
	}
    }
  emotion_no = argmax_i;
  emotion_intensity = argmax_j;
  
  /* Conversion de l'emotion en entree en activations des neurones moteurs */
  nb_servos = data->nb_servos;
  nb_positions = data->nb_positions_max;
  first = data->neurons_first;
  for (i = 0; i < nb_servos; i++)
    {
      for (j = 0; j < nb_positions; j++)
	{
	  neurone_pos = i * data->taillex + j;
	  neurone[first + neurone_pos].s
	    = neurone[first + neurone_pos].s1
	    = neurone[first + neurone_pos].s2
	    = 0.0;
	}
      
      if (max > EMOTIONTOSERVO_ACTIVITY_THRESHOLD)
	{
	  motor_neuron_no = data->motor_neuron[emotion_no][i][emotion_intensity];
	  neurone_pos = i * data->taillex + motor_neuron_no;
	  neurone[first + neurone_pos].s
	    = neurone[first + neurone_pos].s1
	    = neurone[first + neurone_pos].s2
	    = 1.0;
	}
    }
}
