/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_test_expression.c 
\brief 

Author: Rafael Shirakawa
Created: 06/07/2006
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-test the robotic face. it receives the command by the file
of the input link. Each line of the file must contains only
the name of the servo to be activate followed by the position
and the speed of the servo. The file must not has blank lines.
it can have as many servos (lines) as the user wants.


Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <Kernel_Function/prom_getopt.h>


#define FILE_NAME_SIZE 255
#define SERVO_NAME 255

void function_test_expression(int Gpe)
{
    /* initialisation des variables */
    FILE *f=NULL;
    int gpe_amounte;
    char param_link[FILE_NAME_SIZE];
    int read;
    int ret = -1;
    Joint *servo;
    float vitesse;
    float position;
    char name[SERVO_NAME];

    printf("~~~~~~~~~~~enter in %s\t group %d\n", __FUNCTION__, Gpe);

    gpe_amounte = find_input_link(Gpe, 0);

    if (def_groupe[Gpe].data == NULL)
    {
        if (gpe_amounte == -1)
        {
            printf(" Error, %s group %i don't have an input\n", __FUNCTION__,
                   Gpe);
            exit(EXIT_FAILURE);
        }

        /*s'il y a un lien en entree */
        if (gpe_amounte > -1)
        {
            /*debut initialisation */
            /*recherche si le lien contient un nom de fichier */
            if (prom_getopt(liaison[gpe_amounte].nom, "-f", param_link) == 2)
            {
                /*si oui, tente de l'ouvrir */
                f = fopen(param_link, "r");
                if (f == NULL)
                {
                    printf("Cannot open input file: %s\n", param_link);
                    exit(EXIT_FAILURE);
                }
                else
                    printf("file %s open with success\n", param_link);
            }
        }
        else
        {
            /*s'il n'y a pas un lien en entree */
            printf("No input file found on %d\n", Gpe);
            exit(EXIT_FAILURE);
        }
    }
    /* donne des commandes a chaque servo */
    do
    {
        /* recuperer les donnes de la ligne lu */
        if (fscanf(f, "%s %g %g\n", name, &position, &vitesse) != 3) EXIT_ON_ERROR("Impossible to parse the file.");

        servo = joint_get_serv_by_name(name);
        printf("servo %s\t\t position %f\t\t speed %f \n", servo->name,        position, vitesse);

        joint_servo_command(servo, position, vitesse);

    }
    while (!feof(f));

    printf(" tapez un nombre entier et apres Entree pour sortir \n");

    ret = scanf("%d", &read);
    if (ret != 1) EXIT_ON_ERROR(" la commande tapée n'a pas le bon format");
}
