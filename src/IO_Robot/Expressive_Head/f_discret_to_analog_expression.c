/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

/* #include "../../../include/ben_defines.h" */

/** ***********************************************************
\file  f_discret_to_analog_expression.c 
\brief 

Author: B.Fouque
Created: 01/12/08

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
- Convertit le codage discret de chaque servo-moteur en codage analogique

Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:


Known bugs: none (yet!)

http://www.doxygen.org
************************************************************/

/* /\* temps d'attend minimal (en secondes) entre une expression et l'autre *\/ */
/* #define DTA_SECONDS_TO_WAIT 0 */

/*!
 * \brief fichier de description des differentes positions pour chaque servo-moteur
 *
 * position d'un servo = intensite de contraction d'un muscle
 */
#define DTA_FILENAME "discret_to_analog.param"

#define DTA_MOTONEURON_ACTIVITY_MIN 0.00
/* #define DTA_MOTONEURON_ACTIVITY_MIN 0.1 */

#define SERVOS_POSITIONS_NB_MAX 10 /*!< nombre maximal de positions des servo-moteurs (correspondent à des intensités différentes) */
#define STR_NAME_SIZE           100 /*!< Taille maximale d'un nom d'expression faciale (une categorie) */
#define SERVOS_NB_MAX           13 /*!< nombre maximal de servos */


typedef struct
{
  float          pos[SERVOS_POSITIONS_NB_MAX];
  char           name[STR_NAME_SIZE];
/*   int            vitesse; */
  float          neutre_pos;
  int            nb_pos;
} t_dta_servo;

typedef struct
{
  t_dta_servo    servos[SERVOS_NB_MAX];
  int            nb_servos;
  int            input_gpe;
  int            input_neurons_first;
/*   int            input_neurons_size; */
  int            input_taillex;
  int            input_tailley;
  int            neurons_first;
  int            taillex;
  int            tailley;
  int            servo_nbpos_max;
/*   struct timeval timer;                 /\* variable pour faire le temps d'attente entre expressions *\/ */
/*   struct timeval actual_time;           /\* idem *\/ */
} t_dta;

/*!
 * \brief Convertit le codage discret d'un servo en codage analogique.
 *
 * Entree: Neurones d'intensite du servo i.
 * Sortie: Valeur neuronale pour la position du servo.
 * Les poids des connexions entre les entrees et la sortie sont les differents parametres de positions des servo-moteurs.
 * Particularite: pour obtenir une moyenne on divise par la somme des entrees.
 */
float discrete_to_analog (t_dta* data, int servo_i)
{
  int    j = 0;
  float  output = 0.0;
  float  activity = 0.0;
  float  weight = 0.0;
  float  input = 0.0;
  int    first = 0;
  int    nb_pos = 0;
  int    neuron_pos = 0;
  int    neuron_line = 0;
  float  input_sum = 0.0;

  first = data->input_neurons_first;
  nb_pos = data->servos[servo_i].nb_pos;
  neuron_line = servo_i * data->input_taillex;
  for (j = 0; j < nb_pos; j++)
    {
      weight = data->servos[servo_i].pos[j];
      neuron_pos = neuron_line + j;
      input = neurone[first + neuron_pos].s2;
      activity += input * weight;
      input_sum += input;
    }
  output = activity / input_sum;

  return output;
}

void dta_check_fscanf_return (int code, int lieu, int i, int j)
{
  if (code <= 0)
    {
      printf ("discret_to_analog: load_servo_positions(): erreur de lecture du fichier\n");
      printf ("code:%i  lieu:%i  i=%i  j=%i\n", code, lieu, i, j);
      exit (1);
    }
}

void print_servos_pos (t_dta* data)
{
  int i = 0;
  int j = 0;

  printf ("print_servos_pos()\n");
  for (i = 0; i < data->nb_servos; i++)
    {
      printf ("%s : nb=%i neutre=%f positions= ",
	      data->servos[i].name,
	      data->servos[i].nb_pos,
	      data->servos[i].neutre_pos);
      for (j = 0; j < data->servos[i].nb_pos; j++)
	{
	  printf ("%f ", data->servos[i].pos[j]);
	}
      printf ("\n");
    }
}

/*!
 * \brief Fonction de chargement des parametres des servos.
 *
 * Verifie que suffisament de memoire est allouee en statique dans le code.
 */
void load_servo_positions (t_dta* data, char* filename)
{
  FILE*  fp = NULL;
  char   servo_name[STR_NAME_SIZE] = "rien";
/*   int    servo_vitesse = 0; */
  float  servo_pos = 0.0;
  float  neutre_pos = 0.0;
  int    max = 0.0;
  int    nb_servos = 0;
  int    nb_pos = 0;
  int    code = 0;
  int    i = 0;
  int    j = 0;

  if (data == NULL || filename == NULL)
    {
      printf ("discret_to_analog: load_servo_positions(): un des parametres est nul\n");
      exit (1);
    }
  fp = fopen (filename, "r");
  if (fp == NULL)
    {
      printf ("discret_to_analog: load_servo_positions(): impossible d'ouvrir le fichier: %s\n", filename);
      exit (1);
    }

  code = fscanf (fp, "nb_servos=%i\n", &nb_servos);
  dta_check_fscanf_return (code, 1, i, j);
  if (nb_servos > SERVOS_NB_MAX)
    {
      printf ("discret_to_analog: load_servo_positions(): pas assez de memoire allouee statiquement pour stocker les donnnes des servos\n");
      printf ("disponible:%i  necessaire:%i\n", SERVOS_NB_MAX, nb_servos);
      exit (1);
    }
  data->nb_servos = nb_servos;
  
  for (i = 0; i < nb_servos; i++)
    {
      code = fscanf (fp, "%s : nb=%i neutre=%f positions=", servo_name, &nb_pos, &neutre_pos);
      dta_check_fscanf_return (code, 2, i, j);
      if (nb_pos > SERVOS_POSITIONS_NB_MAX)
	{
	  printf ("discret_to_analog: load_servo_positions(): pas assez de memoire allouee statiquement pour stocker les positions d'un servo\n");
	  printf ("disponible:%i  necessaire pour le servo %i: %i\n", SERVOS_POSITIONS_NB_MAX, i, nb_pos);
	  exit (1);
	}
      strcpy (data->servos[i].name, servo_name);
/*       data->servos[i].vitesse = servo_vitesse; */
      data->servos[i].neutre_pos = neutre_pos;
      data->servos[i].nb_pos = nb_pos;

      if (nb_pos > max)
	max = nb_pos;

      for (j = 0; j < nb_pos; j++)
	{
	  code = fscanf (fp, "%f", &servo_pos);
	  dta_check_fscanf_return (code, 3, i, j);
	  data->servos[i].pos[j] = servo_pos;
	}
    }

  data->servo_nbpos_max = max;
  fclose (fp);
}

/*!
 * \brief Fonction de verification d'allocation.
 *
 * Verifie que suffisament de neurones sont alloues avec leto dans les groupes d'entree et courant.
 */
void dta_check_integrity (t_dta* data)
{
  if (data->input_taillex < data->servo_nbpos_max || data->input_tailley != data->nb_servos)
    {
      printf ("discret_to_analog: dta_check_integrity(): pas assez de neurones dans le groupe d'entree\n");
      printf ("nb de neurones sur x:%i  necessaires:%i\n", data->input_taillex, data->servo_nbpos_max);
      printf ("nb de neurones sur y:%i  attendu:%i\n", data->input_tailley, data->nb_servos);
      exit (1);
    }

  if (data->taillex != 1 || data->tailley != data->nb_servos)
    {
      printf ("discret_to_analog: dta_check_integrity(): le nombre de neurones defini dans leto n'est pas bon\n");
      printf ("nb de neurones sur x:%i  attendu:%i\n", data->taillex, 1);
      printf ("nb de neurones sur y:%i  attendu:%i\n", data->tailley, data->nb_servos);
      exit (1);
    }
}

void new_discret_to_analog_expression (int gpe)
{
  t_dta*          data = NULL;
  int             input_link = -1;
  int             i = 0;

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = (t_dta *) malloc (sizeof (t_dta));
  if (data == NULL)
    {
      printf("new_discret_to_analog(): error in memory allocation in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }
  def_groupe[gpe].data = data;
  
  /* Recuperation des donnees du groupe d'entree */
  input_link = find_input_link (gpe, 0);
  if (input_link == -1)
    {
      printf("new_discret_to_analog(): Error, %s group %i doesn't have an input\n",
	     __FUNCTION__, gpe);
      exit(EXIT_FAILURE);
    }
  data->input_gpe = liaison[input_link].depart;
  data->input_neurons_first = def_groupe[data->input_gpe].premier_ele;
/*   data->input_neurons_size = def_groupe[data->input_gpe].nbre; */
  data->input_taillex = def_groupe[data->input_gpe].taillex;
  data->input_tailley = def_groupe[data->input_gpe].tailley;

  /* Recuperation des donnees du groupe courant */
  data->neurons_first = def_groupe[gpe].premier_ele;
  data->taillex = def_groupe[gpe].taillex;
  data->tailley = def_groupe[gpe].tailley;

  /* initialisations */
  data->nb_servos = 0;
  data->servo_nbpos_max = 0;

  /* Chargement des positions des servos a partir d'un fichier */
  load_servo_positions (data, (char *)DTA_FILENAME);
  
/*   /\* ajuster chronometre *\/ */
/*   gettimeofday(&data->timer, (void *) NULL); */
  
  dta_check_integrity (data);

  /* initialisations neurones du groupe */
  for (i = 0; i < data->nb_servos; i++)
    {
      neurone[data->neurons_first + i].s
	= neurone[data->neurons_first + i].s1
	= neurone[data->neurons_first + i].s2
	= 0.0;
    }

/*   print_servos_pos (data); */
}

/*!
 * \brief Convertit le codage discret de chaque servo-moteur en codage analogique.
 *
 * Entree: Activitees neuronales discretes des positions de chaque servo.
 * Sortie: Activitees analogiques des neurones moteurs.
 * Effet:  RAS
 * Neurones: Neurones moteurs
 **/
void function_discret_to_analog_expression (int gpe)
{
  t_dta          *data = NULL;
  float           max = 0.0;
  int             argmax = 0;
  float           output = 0.0;
/*   Joint          *joint_servo = NULL; /\* structure de promethe pour commander un servo *\/ */
  float           servo_pos = 0.0;
/*   float           servo_vitesse = 0.0; */
  int             first = 0;
  int             input_first = 0;
  int             neurone_pos = 0;
  int             nb_pos = 0;
  int             nb_servos = 0;
  int             input_taillex = 0;
  int             i = 0;
  int             j = 0;

  /* Chargement des donnees du groupe */
  data = (t_dta *)def_groupe[gpe].data;
  if (data == NULL)
    {
      printf("discret_to_analog(): error while loading data in group %d %s\n",
	     gpe, __FUNCTION__);
      exit(EXIT_FAILURE);
    }

/*   /\* Temps minimale pour changer l'expression faciale. *\/ */
/*   gettimeofday(&data->actual_time, (void *) NULL); */
  
/*   if ((int) (data->timer.tv_sec + DTA_SECONDS_TO_WAIT) > */
/*       (int) (data->actual_time.tv_sec)) */
/*     return; */
  
  /* Modification des valeurs du neurone analogique de chaque servo  */
  input_first = data->input_neurons_first;
  first = data->neurons_first;
  nb_servos = data->nb_servos;
  input_taillex = data->input_taillex;
  for (i = 0; i < nb_servos; i++)
    {
      /* Recherche du neurone max pour le servo i */
      nb_pos = data->servos[i].nb_pos;
      max = 0.0;
      for (j = 0; j < nb_pos; j++)
	{
	  neurone_pos = i * input_taillex + j;
	  output = neurone[input_first + neurone_pos].s2;
	  if (output >= max)
	    {
	      max = output;
	      argmax = j;
	    }
	}
      /* Calcule de l'activite des neurones analogiques (codent les positions des servos) */
      if (max > DTA_MOTONEURON_ACTIVITY_MIN)
/* 	servo_pos = discrete_to_analog (data, i); */
	servo_pos = data->servos[i].pos[argmax];
      else /* position a prendre par defaut: correspond a la position neutre */
	servo_pos = data->servos[i].neutre_pos;
      neurone[first + i].s
	= neurone[first + i].s1
	= neurone[first + i].s2
	= servo_pos;

      /* activation des servos */
/*       joint_servo = joint_get_serv_by_name(data->servos[i].name); */
/*       servo_vitesse = data->servos[i].vitesse; */
/* /\*       printf ("servo_name:%s servo_vitesse:%i servo_pos:%f\n\n", data->servos[i].name, data->servos[i].vitesse, servo_pos); *\/ */
/*       joint_servo_command (joint_servo, servo_pos, servo_vitesse); */
    }
}
