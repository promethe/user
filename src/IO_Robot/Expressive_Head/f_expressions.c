/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*************************************************************
 \file  f_expressions.c
 \brief

 Author: Rafael Shirakawa
 Created: 04/07/2006
 Modified:
 - author: David BAILLY
 - description: modification ... à peu près tout. Le fichier est maintenant écrit en xml et less structure ont été réécrites
 - date: 21/06/2011

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 -control the robotic face. it receives the command by activation des neurones du groupe precedant and sends the command to the servos that control the robotic face. It opens "expressions.param", that gives the position and the speed for each servo for each expression.

 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments: "expressions.param" must be in the form:
 The initial identification of every expression is [ name ] with one space before and after the name
 The final identification of every expression is [/ name ]
 The file must not have blanc lines
 The parameters in each line for the servos must be:
 servo_name position speed
 The position must be between 0. and 1
 The speed must be between the parametres values of the file .hws
 The expressions must be in the same order of the neurones array of the input group

 example for two expressions:
 <expression name="NEUTRE">
 <motor name="sourcil_left_ext" pos="0.45" speed="5"/>
 <motor name="sourcil_left_int" pos="0.45" speed="5"/>
 <motor name="sourcil_right_ext" pos="0.45" speed="5"/>
 <motor name="sourcil_right_int" pos="0.45" speed="5"/>
 <motor name="tilt_front" pos="0.2" speed="5"/>
 <motor name="pan_mouth_left" pos="0.3" speed="5"/>
 <motor name="pan_mouth_right" pos="0.38" speed="5"/>
 <motor name="tilt_mouth_left" pos="0.76" speed="5"/>
 <motor name="tilt_mouth_right" pos="0.76" speed="5"/>
 <motor name="tilt_mouth_center" pos="0.2" speed="5"/>
 </expression>
 <expression name="TRISTESSE">
 <motor name="pan_mouth_left" pos="0.5" speed="5"/>
 <motor name="pan_mouth_right" pos="0.5" speed="5"/>
 <motor name="sourcil_left_ext" pos="0.3" speed="5"/>
 <motor name="sourcil_left_int" pos="0.61" speed="5"/>
 <motor name="sourcil_right_ext" pos="0.4" speed="5"/>
 <motor name="sourcil_right_int" pos="0.6" speed="5"/>
 <motor name="tilt_front" pos="0.1" speed="5"/>
 <motor name="tilt_mouth_left" pos="0.3" speed="5"/>
 <motor name="tilt_mouth_right" pos="0.4" speed="5"/>
 <motor name="tilt_mouth_center" pos="0.1" speed="5"/>
 </expression>

 Known bugs: the new function cannot be used if there are more than one f_expressions in a script. If so they will try to read the .param file at the same file which is not possible.

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <sys/time.h>
#include <Kernel_Function/prom_getopt.h>
#include <limits.h>
#include <xml_tools.h>
#include <unistd.h>
#include <errno.h>

/* temps d'attend minimal (en secondes) entre une expression et l'autre */
#define SECONDS_TO_WAIT 0

/* numero total de servos */
#define NB_MAX_SERVOS 32

/* numero total des expressions */
#define NB_MAX_EXPRESSIONS 10

typedef struct {
  float position; /* position des servos */
  float vitesse; /* vitesse des servos */
  char nom_servo[32]; /* structure de nom de servos */
} Servo_datas;

typedef struct {
  int nb_servos;
  char name[32];
  Servo_datas servos[NB_MAX_SERVOS]; /* chaque expression a une configuration de tous les servos */
} Expression;

typedef struct {
  int expression_actuelle;
  int nb_expression;
  int gpe_amounte; /* groupe de liaision precedant */
  int inputGpe; /* groupe d'entree pour la liaisoin en arriere */
  int deb; /* debut de la chaine de neurones de cette fonction dans les neurones de promethe */
  int longueur; /* longueur de la chaine de neurones (chaque neurone est une expression */
  struct timeval timer; /* variable pour faire le temps d'attend minimum entre une e autre expression */
  struct timeval actual_time; /* idem */
  int flag;
  int gpe_learn;
  Expression expressions[NB_MAX_EXPRESSIONS];
} Data_expressions;

/*#define DEBUG*/
void new_expressions(int Gpe)
{
  Gpe = Gpe;
  /* à remplacer par le contenu de init_expressions*/

}

void init_expressions(int Gpe)
{
  Data_expressions *data = NULL; /* structure de cette fonction */
  /*FILE *f = NULL;*//*ficher des configurations des expressions, Commente car remplace par la lecture du fichier xml. */
  int i = 0, j = 0; /* variable d'iterations */
  char filename[20]; /* nom du ficher f (expressions.param) */
  int l;
  int gpe_amounte = -1;
  int flag = 0; /* structure de promethe */

  Node *tree = NULL;
  Node *expression = NULL;
  Node *motor = NULL;

#ifdef DEBUG
  printf("~~~~~~~~~~~enter in %s\n", __FUNCTION__);
#endif

  /*on cree la structure qui contiendra le pointeur vers ce fichier et on l'initialize */
  data = (Data_expressions *) malloc(sizeof(Data_expressions));
  if (data == NULL)
  {
    EXIT_ON_ERROR("error in memory allocation in group %d %s\n");
  }

  /* on charge le ficher avec la description de vitesse et position de chaque servo */
  l = find_input_link(Gpe, i);
  while (l != -1)
  {
    if (strcmp(liaison[l].nom, "servo") == 0)
    {
      gpe_amounte = data->gpe_amounte = l;
    }
    if (strcmp(liaison[l].nom, "learn") == 0)
    {
      data->gpe_learn = liaison[l].depart;
      flag = 1;
    }

    l = find_input_link(Gpe, i);
    i++;
  }

  /*data->gpe_amounte = find_input_link(Gpe, 0);*/
#ifdef DEBUG
  printf("data groupe precedent= %d\n",data->gpe_amounte);
#endif
  if (gpe_amounte == -1)
  {
    EXIT_ON_ERROR(" Error, %s group %i doesn't have an input\n");
  }

  strcpy(filename, "expressions.param");

#ifdef DEBUG
  printf("on est avant le do\n");
#endif

  /*getting the xml tree*/
  tree = xml_load_file(filename);
  if (tree == NULL)
  {
    EXIT_ON_ERROR("unable to load from %s", filename);
  }

  /*copying the infos for each expression*/
  for (j = 0, expression = xml_get_first_child_with_node_name(tree, "expression"); expression != NULL; j++, expression = xml_get_next_homonymous_sibling(expression))
  {
    strncpy(data->expressions[j].name, xml_try_to_get_string(expression, "name"), 32 * sizeof(char));
    dprints("%s\n", data->expressions[j].name);
    for (i = 0, motor = xml_get_first_child_with_node_name(expression, "motor"); motor != NULL; i++, motor = xml_get_next_homonymous_sibling(motor))
    {
      strncpy(data->expressions[j].servos[i].nom_servo, xml_try_to_get_string(motor, "name"), 32 * sizeof(char));
      data->expressions[j].servos[i].position = atof(xml_try_to_get_string(motor, "pos"));
      data->expressions[j].servos[i].vitesse = atof(xml_try_to_get_string(motor, "speed"));
      dprints("expression %d \t servo %s (number %d) \t position %f \t speed %f \n", j, data->expressions[j].servos[i].nom_servo, i, data->expressions[j].servos[i].position, data->expressions[j].servos[i].vitesse);
    }
    data->expressions[j].nb_servos = i;
  }
  data->nb_expression = j;
  mxmlDelete(tree);

  dprints("~~~~~~~~~~~data->expression = %d\n", data->nb_expression);

  /* prendre les infos necessaires pour acces aux neurones de ce groupe */
  data->inputGpe = liaison[data->gpe_amounte].depart;
  data->deb = def_groupe[data->inputGpe].premier_ele;
  data->longueur = def_groupe[data->inputGpe].nbre;
  data->flag = flag;
  /* ajuster chronometre */
  gettimeofday(&data->timer, (void *) NULL);

  /* passer les donnes a la fonction suivant */
  def_groupe[Gpe].data = data;

  dprints("~~~~~~~~~~~end of %s\n", __FUNCTION__);

}
/*-----------------------------------------------------*/
/*----FIN DU CONSTRUCTEUR------------------------------*/
/*-----------------------------------------------------*/

void function_expressions(int Gpe)
{
  /* initialisation des variables */
  int i; /* variable de iterations */
  Data_expressions *my_data = NULL; /* la structure de cette fonction */
  int expression = 0;
  /*float activity = 0;*/
  Joint *joint_servo;
  /* fin de l'initialisation des variables */

  dprints("~~~~~~~~~~~enter in %s, group %s (%d)\n", __FUNCTION__, def_groupe[Gpe].no_name, Gpe);

  if (def_groupe[Gpe].data == NULL)
  {
    init_expressions(Gpe);
    if (def_groupe[Gpe].data == NULL)
    EXIT_ON_ERROR("error while loading data in group %s (%d) %s\n", def_groupe[Gpe].no_name, Gpe, __FUNCTION__);
  }
  my_data = (Data_expressions *) def_groupe[Gpe].data; /* recuperer les donnes du constructeur */

  if (my_data->flag == 1)
  {
    if (neurone[def_groupe[my_data->gpe_learn].premier_ele].s1 < 0.5)
    {
      expression = 1;
      /* temps minimale pour changer l'expression faciale. */
      gettimeofday(&my_data->actual_time, (void *) NULL);

      if ((int) (my_data->timer.tv_sec + SECONDS_TO_WAIT) > (int) (my_data->actual_time.tv_sec)) return;

      gettimeofday(&my_data->timer, (void *) NULL);

      /* recupere l'expression gagnant */
      for (i = 0; i <= my_data->longueur; i++)
      {
        if (neurone[my_data->deb + i].s2 >= 1.)
        {
          expression = i;
          break;
        }
      }

      /* donner des commandes a chaque servo */
      for (i = 0; i < my_data->expressions[expression].nb_servos; i++)
      {
        dprints("expression %i servo %s \t position %.3f \t speed %.3f \n", expression, my_data->expressions[expression].servos[i].nom_servo, my_data->expressions[expression].servos[i].position, my_data->expressions[expression].servos[i].vitesse);

        /* mis en forme (structure Joint) pour appeller la fonction joint_servo_command */
        joint_servo = joint_get_serv_by_name(my_data->expressions[expression].servos[i].nom_servo);
        /* commander chaque servo */
        joint_servo_command(joint_servo, my_data->expressions[expression].servos[i].position, my_data->expressions[expression].servos[i].vitesse);
      }
    }
  }

#ifdef DEBUG
  printf("~~~~~~~~~~~end of %s, group %s (%d)\n", __FUNCTION__, def_groupe[Gpe].no_name, Gpe);
#endif

}
