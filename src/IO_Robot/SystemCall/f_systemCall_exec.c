/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_systemCall_exec.c
 \brief

 Author: de RENGERVE Antoine
 Created: 26/04/2011
 Modified:
 - author: -
 - description: -
 - date: -

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description: Trigger systemCall defined in .dev to be executed
 If no input link is defined, commands are always
 executed.  If (only) one input link is defined, the commands are
 conditionally executed depending on the neurons in the input group.
 First neuron manages first command.

 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/

#include <libx.h>
#include <net_message_debug_dist.h>
#include <dev.h>
#include <device.h>
#include <Components/systemCall.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#define MAXIMUM_SIZE_OF_OPTION 128
 
typedef struct box_motor_get_torque
{
   Device *device;
   SystemCall **systemCalls;
   int number_of_systemCalls;
   type_neurone* neurons;
   type_groupe *input_gpe;
} Box_systemCall_exec;

void new_systemCall_exec(int index_of_group)
{
   Box_systemCall_exec *box;
   char id_of_device[MAXIMUM_SIZE_OF_OPTION];
   Device *device=NULL;
   int index_of_link, success;

   type_groupe *group;
   type_liaison *link;

   /** On retrouve le groupe de neurones */
   group = &def_groupe[index_of_group];

   /** On creer la boite et l'associe au groupe */
   box = ALLOCATION(Box_systemCall_exec);
   group->data = box;

   /** On recherche le lien entrant s'il existe. */
   index_of_link = find_input_link(index_of_group, 0);
   if (index_of_link == -1)
   {
      link = NULL;
      box->input_gpe = NULL;
      box->neurons = NULL;
   }
   else
   {
      link = &liaison[index_of_link];
      box->input_gpe = &def_groupe[link->depart];
      /** On definit les neurones de la boite d'entree. */
      box->neurons = &neurone[box->input_gpe->premier_ele];
   }
   if (find_input_link(index_of_group, 1) != -1) EXIT_ON_ERROR("Group: %d, has more than one incoming link !", index_of_group);

   /** On recherche le parametre id sur le lien  s'il y en a un.*/
   if (link != NULL)
   {
      success = prom_getopt(link->nom, "id=", id_of_device);
      if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);
   }
   else success = 0;

   /** S'il existe on associe l'appareil de la boite a l'appareil ayant
    * l'id. Sinon on l'associe au seul appareil de meme type. */
   if (success == 2) device = dev_get_device("systemCall", id_of_device);
   else if (success == 0) device = dev_get_device("systemCall", NULL);
   else if (success == 1) EXIT_ON_ERROR("Group %d, the parameter of 'id=' is not valid on the link :'%s'.", index_of_group, link->nom);

   /** On verifie que le nombre de moteurs de l'appareil equivaut a
    * celui du nombre de neurones du groupe d'entree*/
   if (box->input_gpe != NULL && device->number_of_components != box->input_gpe->nbre) EXIT_ON_ERROR("In group (%s) : Number of systemCalls (%d) different from number of neurons in input groupe(%d).", def_groupe[index_of_group].no_name, device->number_of_components, box->input_gpe->nbre);
   else box->number_of_systemCalls = device->number_of_components;

   /** On definit les systemCalls de la boite*/
   box->systemCalls = (SystemCall**) device->components;
}

void function_systemCall_exec(int index_of_group)
{
   Box_systemCall_exec *box;
   void (*exec_command)(SystemCall *);
   int i;
   SystemCall **systemCalls;
   type_neurone *neurons_of_the_box;

   /** On recupere la boite a partir du groupe*/
   box = (Box_systemCall_exec*) def_groupe[index_of_group].data;

   /** On recupere les neurones de la boite et les moteurs*/
   neurons_of_the_box = box->neurons;
   systemCalls = box->systemCalls;
   exec_command = systemCalls[0]->exec_command; /** meme fonction pour tous les moteurs */

   /** On execute la commande pour chaque systemCall*/
   for (i = 0; i < box->number_of_systemCalls; i++)
      if (neurons_of_the_box == NULL || neurons_of_the_box[i].s1 > 0.5) exec_command(systemCalls[i]);
}

void destroy_systemCall_exec(int index_of_group)
{
   free(def_groupe[index_of_group].data);
}

