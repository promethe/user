/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include "tools/include/macro.h"
#include <Global_Var/IO_Robot.h>
#include <IO_Robot/Complex_Action.h>
#include "tools/include/local_var.h"

#include <IO_Robot/Complex_Action.h>
#include <IO_Robot/GenericTools.h>

#include "tools/include/function_robot_explore.h"
void function_manage_phase_1(int *fois,int gpe_sortie,float *Ms);

void function_manage_phase_1(int *fois,int gpe_sortie,float *Ms)
{
    char *s = (char*)"f_robot_limbic";
    int gpe_entree = 0;
    int taille_groupe_e = 0;
    int taille_groupe_e2;
    int deb_e = 0, i;
    int increment = 0, n;

    printf("\nNONODEBUG : alealearn : %d\n", alealearn);

    if ((*fois) == 1)
    {
        /*      Calibre_boussole(); */
        if (fois == &compte_fois[0])
        {
            for (i = 0; i < nb_place_prevus; i++)
                n2pl[i] = -1;

            Niveau_ressources[0] = (float) motivations[0].niveau_min;
            for (i = 1; i < nb_motivations; i++)
                Niveau_ressources[i] = (float) motivations[i].seuil_haut - 1.;

            for (i = 0; i < nbre_liaison; i++)
                if ((liaison[i].arrivee == gpe_sortie)
                    && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
                {
                    gpe_entree = liaison[i].depart;
                    taille_groupe_e = def_groupe[gpe_entree].nbre;
                    taille_groupe_e2 =
                        def_groupe[gpe_entree].taillex *
                        def_groupe[gpe_entree].tailley;
                    increment = taille_groupe_e / taille_groupe_e2;
                    deb_e = def_groupe[gpe_entree].premier_ele;
                    break;
                }
            function_robot_limbic(gpe_entree);

        }
        else
        {
            phase_apprend = 0;

            for (i = 0; (i < nb_motivations) && (&compte_fois[i] != fois);
                 i++)
                Niveau_ressources[i] = (float) motivations[i].seuil_haut - 1;

            for (i = 0; i < nbre_liaison; i++)
                if ((liaison[i].arrivee == gpe_sortie)
                    && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
                {
                    gpe_entree = liaison[i].depart;
                    taille_groupe_e = def_groupe[gpe_entree].nbre;
                    taille_groupe_e2 =
                        def_groupe[gpe_entree].taillex *
                        def_groupe[gpe_entree].tailley;
                    increment = taille_groupe_e / taille_groupe_e2;
                    deb_e = def_groupe[gpe_entree].premier_ele;
                    break;
                }
            function_robot_limbic(gpe_entree);
        }
        vigilence = 1.;
        alealearn = 0;
        EMISSION_ROBOT = 1;
    }

    while (!trouve)
    {
        /* braittenberg_koala_detect_goal(10.);
           braittenberg_koala_follow_detect_goal(10.); */
        function_robot_explore();
    }
    if (trouve)
    {
        printf("\nNONODEBUG apprentissage lance\n");
        if (!sortie_neurones)
        {
            printf
                ("\nNONODEBUG : AAAAAAAAH pas possible de creer sortie_neurones.txt!!!!\n");
            exit(1);
        }
        fprintf(sortie_neurones, "Apprentissage au lieu N° %d \n", *fois);
        fflush(sortie_neurones);
        for (i = 0; i < nbre_liaison; i++)
            if ((liaison[i].arrivee == gpe_sortie)
                && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
            {
                gpe_entree = liaison[i].depart;
                taille_groupe_e = def_groupe[gpe_entree].nbre;
                taille_groupe_e2 =
                    def_groupe[gpe_entree].taillex *
                    def_groupe[gpe_entree].tailley;
                increment = taille_groupe_e / taille_groupe_e2;
                deb_e = def_groupe[gpe_entree].premier_ele;
                break;
            }
        *Ms = 0.;
        for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e; n = n + increment)
        {
            *Ms = *Ms + fabs(neurone[n].s1);
            fprintf(sortie_neurones,
                    "YO MAN DEBUGGING STUFF : Motivational_state = %f, fabs(neur) = %f, neur = %f\n",
                    *Ms, fabs(neurone[n].s2), neurone[n].s2);
            fflush(sortie_neurones);
        }
        vigilence = 1.;
        alealearn = 0;
        (*fois)++;
    }
}

void function_robot_manage_exploration(int gpe_sortie)
{
    int i;
    int gpe_entree = 0;
    int taille_groupe_e = 0;
    int taille_groupe_e2;
    int deb_e = 0, f, drap_fois;
    int increment = 0, n;
    char *s = (char*)"f_robot_limbic";
    float Motivational_state = 0.;

    if (compte_fois[0] == nb_place_reflex + 1)
    {
        compte_fois[0] = 0;
        compte_fois[1] = 1;
        Niveau_ressources[0] = (float) motivations[0].seuil_haut - 1;
        Niveau_ressources[1] = (float) motivations[1].niveau_min;
        trouve = 0;
        Motivational_state = 0.;

        for (i = 0; i < nb_motivations; i++)
        {
            Flag_ingestion[i] = 0;
        }
        for (i = 0; i < nbre_liaison; i++)
            if ((liaison[i].arrivee == gpe_sortie)
                && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
            {
                gpe_entree = liaison[i].depart;
                taille_groupe_e = def_groupe[gpe_entree].nbre;
                taille_groupe_e2 =
                    def_groupe[gpe_entree].taillex *
                    def_groupe[gpe_entree].tailley;
                increment = taille_groupe_e / taille_groupe_e2;
                deb_e = def_groupe[gpe_entree].premier_ele;
                break;
            }
        function_robot_limbic(gpe_entree);

    }
    for (f = 1; f < nb_motivations; f++)
        if (compte_fois[f] == nb_place_reflex + 1)
        {
            compte_fois[f] = 0;
            if (f < nb_motivations - 1)
                compte_fois[f + 1] = 1;
            trouve = 0;
            Motivational_state = 0.;

            Niveau_ressources[f] = (float) motivations[f].seuil_haut - 1.;
            if (f < nb_motivations - 1)
                Niveau_ressources[f + 1] =
                    (float) motivations[f + 1].niveau_min;


            for (i = 0; i < nb_motivations; i++)
            {
                Flag_ingestion[i] = 0;
            }
            for (i = 0; i < nbre_liaison; i++)
                if ((liaison[i].arrivee == gpe_sortie)
                    && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
                {
                    gpe_entree = liaison[i].depart;
                    taille_groupe_e = def_groupe[gpe_entree].nbre;
                    taille_groupe_e2 =
                        def_groupe[gpe_entree].taillex *
                        def_groupe[gpe_entree].tailley;
                    increment = taille_groupe_e / taille_groupe_e2;
                    deb_e = def_groupe[gpe_entree].premier_ele;
                    break;
                }
            function_robot_limbic(gpe_entree);
            break;              /* pour sortir du for (f=1; f<nb_motivations;f++) je sais c'est tres sale */
        }
    drap_fois = 0;
    for (f = 0; f < nb_motivations; f++)
    {
        if (compte_fois[f] > 0)
        {
            function_manage_phase_1(&compte_fois[f], gpe_sortie,
                                    &Motivational_state);
            drap_fois = 1;
            break;
        }

    }
    if (drap_fois == 0)
    {
        /*      emission_robot=0; */

        vigilence = 0.;
        alealearn = 1;
        printf("\nNONODEBUG :  alealearn : %d\n", alealearn);
/*
 fflush(stdin);
   printf("\nPlease press a key.\n");
   pipo = getchar();
   fflush(stdin);*/

        for (i = 0; i < nbre_liaison; i++)
            if ((liaison[i].arrivee == gpe_sortie)
                && (strcmp(def_groupe[liaison[i].depart].nom, s) == 0))
            {
                gpe_entree = liaison[i].depart;
                taille_groupe_e = def_groupe[gpe_entree].nbre;
                taille_groupe_e2 =
                    def_groupe[gpe_entree].taillex *
                    def_groupe[gpe_entree].tailley;
                increment = taille_groupe_e / taille_groupe_e2;
                deb_e = def_groupe[gpe_entree].premier_ele;
                break;
            }
        Motivational_state = 0.;

        for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e; n = n + increment)
        {
            Motivational_state = Motivational_state + fabs(neurone[n].s1);
            fprintf(sortie_neurones,
                    "YO MAN DEBUGGING STUFF : Motivational_state = %f, fabs(neur) = %f, neur = %f\n",
                    Motivational_state, fabs(neurone[n].s2), neurone[n].s2);
            fflush(sortie_neurones);

        }

        while (isequal(Motivational_state, 0.))
        {
            function_gestion_motivations();
            function_robot_limbic(gpe_entree);
            for (i = 0; i < nb_motivations; i++)
            {
                Flag_ingestion[i] = 0;
            }
            function_robot_explore();

            Motivational_state = 0.;
            for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e;
                 n = n + increment)
            {
                Motivational_state = Motivational_state + fabs(neurone[n].s1);
                fprintf(sortie_neurones,
                        "YO MAN DEBUGGING STUFF : Motivational_state = %f, fabs(neur) = %f, neur = %f\n",
                        Motivational_state, fabs(neurone[n].s2),
                        neurone[n].s2);
                fflush(sortie_neurones);

            }
        }
        for (i = 0; i < nb_motivations; i++)
        {
            Flag_ingestion[i] = 0;
        }
    }
}
