/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: Raoul
Created: 20/04/2000
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 N.C: le nombre de neurone du groupe determine la discretisation angulaire
   Raoul: Cette fonction a ete modifiee pour accepter
 l angle donne dans l extension de f_angle_abs_cam,
 qui est un angle donne dans le meme format que
 f_compass: 0 a 1, avec 0.5 pour le nord
 De plus, la recherche des liens a ete amelioree de
 facon a permettre divers formats de compass, par
 exemple:
 f_compass: On va aller chercher dans le 1er neurone
          valeur de 0 a 1 (-180 a +179 degres)
 f_angle_abs_cam: On va aller chercher dans
          l extension une valeur de 0 a 1 (-180 a -179)
 Modif VB : De plus, le groupe relie par le lien
 niv_act peut permettre de regler la diffusion sur
 le theta adapt (partie en commentaire au
 04/20/00)

Macro:
-none

Local variables:
-int rg_pano
-int rg_diff
-int position_courante_x
-int xmax_carac
-float angle

Global variables:
-int NbrFeaturesPoints

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>

#include <Global_Var/IO_Robot.h>
#include "tools/include/local_var.h"

void function_theta_adapt(int numero)
{
    int r_diff, field;
    int gpe_pano_focus = 0, l1 = -1, gpe_compass = 0, l2 = -1, gpe_spice =
        0, l3 = -1, nofind_pano_focus, nofind_compass, nofind_spice;
    int i, mvt_x /*N.C: entre 0 et le nb de neurone */ , d;
    int deb, size /*, view_size*/;
    int xmax2, ymax2;
    int pos;
    float diff/*, a1*/;
    float AngleVue, AngleVrai, Bouss = 0.;  /*N.C: En degre entre -180 et 179 */

    int taille_groupe_place = 0, taille_groupe_place2, increment_place = 0, deb_place = 0;
    float sum_place_act, max_plc/*, max_act*/;
    int compt_plc;


    /* RAOUL: Nouvelles Variables */
    float *r_var;
    float r_angle_norm;
    int r_compas = 0;

#ifndef AVEUGLE
    printf("-------------------------------------------------\n");

    printf("Fabrication du reflexe position focalisation \n");
    fflush(stdout);
#endif


    /* On fait la recherche d'infos */
    nofind_pano_focus = 0;
    nofind_compass = 0;
    nofind_spice = 0;
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == numero)
        {
            /* Recherche de f_pano_focus ou get_pano_alt ou f_get_pano ou load_image_from_disk ou f_load_baccon pour recuperer r_diff ou/et field */
            if ((strcmp(def_groupe[liaison[i].depart].nom, "f_pano_focus") ==
                 0)
                ||
                (strcmp
                 (def_groupe[liaison[i].depart].nom,
                  "f_load_image_from_disk") == 0)
                ||
                (strcmp(def_groupe[liaison[i].depart].nom, "f_get_pano_alt")
                 == 0)
                || (strcmp(def_groupe[liaison[i].depart].nom, "f_get_pano") ==
                    0)
                || (strcmp(def_groupe[liaison[i].depart].nom, "f_load_baccon")
                    == 0))
            {
                gpe_pano_focus = liaison[i].depart;
                l1 = i;
                nofind_pano_focus += 1;
                if (liaison[l1].nom[0] == 'a')
                    sscanf(liaison[l1].nom, "a%d", &r_diff);
                if (liaison[l1].nom[0] == 'b')
                    sscanf(liaison[l1].nom, "b%d,%d", &r_diff, &field);
                if (nofind_pano_focus > 1)
                    printf
                        ("f_theta_adapt : ATTENTION : Plusieurs f_pano_focus relies en meme temps\n");
            }
            /* Recherche de la boussole de type f_compass */
            if ((strcmp(liaison[i].nom, "compass") == 0)
                && (liaison[i].arrivee == numero))
            {
                gpe_compass = liaison[i].depart;
                r_compas = 1;
                l2 = i;
                nofind_compass += 1;
                if (nofind_compass > 1)
                    printf
                        ("f_theta_adapt : ATTENTION : Plusieurs compas relies en meme temps(1)\n");
            }
            /* Recherche de la boussole de type f_angle_abs_cam */
            if (strcmp(liaison[i].nom, "compassext") == 0)
            {
                gpe_compass = liaison[i].depart;
                r_compas = 2;
                l2 = i;
                nofind_compass += 1;
                if (nofind_compass > 1)
                    printf
                        ("f_theta_adapt : ATTENTION : Plusieurs compas relies en meme temps(2)\n");
            }
            /* Recherche du lien niv_act : permet de moduler la diffusion en fonction du niveau
               de la reconnaissance. Si reconnaissance est elevee alors diffusion faible
               Si reconnaissance faible, alors forte diffusion    */
            if (strcmp(liaison[i].nom, "niv_act") == 0)
            {
                /*max_act =
                    neurone[def_groupe[liaison[i].depart].premier_ele].s2;*/
                /*if (max_act==0)
                   r_diff=35;
                   else
                   {
                   r_diff=(int)(-54*max_act+59);
                   if (r_diff>35)
                   r_diff=35;
                   } */
                r_diff = 10;
            }



        }

        /* Recherche de spice
           NC: ne dvrait plus etre utilise.... */
        if (strcmp(def_groupe[liaison[i].depart].nom, "spice") == 0)
        {
            gpe_spice = liaison[i].depart;
            l3 = i;
            nofind_spice += 1;
            if (nofind_pano_focus > 1)
                printf
                    ("f_theta_adapt : ATTENTION : plusieurs SPICEs trouves\n");
        }
    }
    if (nofind_pano_focus == 0)
        printf("f_theta_adapt : Pas trouve f_pano_focus \n");
    if (nofind_compass == 0)
        printf("f_theta_adapt : Pas trouve la fonction de compas \n");
    if (nofind_spice == 0)
        printf("f_theta_adapt : Pas trouve spice \n");





    /* Verification */
     /*RAOULG*/ if ((rg_pano != -1) && (rg_pano != -2))
         /*RAOULG*/ field = rg_pano;
     /*RAOULG*/ if ((rg_diff != -1) && (rg_diff != -2))
         /*RAOULG*/ r_diff = rg_diff;
    printf("\tr_diff: %d\tfield: %d\n", r_diff, field);




#ifndef AVEUGLE
    if (l3 != -1)
        printf
            ("3 liens diff=%d field=%d block boussole:%d block diffusion:%d\n",
             r_diff, field, gpe_compass, gpe_spice);
    if (l2 != -1)
        printf("2 liens diff=%d field=%d block boussole:%d\n", r_diff, field,
               gpe_compass);
    else
        printf("1 lien diff=%d field=%d block entree:%d\n", r_diff, field,
               gpe_pano_focus);
#endif


    if (l3 != -1)
    {
        taille_groupe_place = def_groupe[gpe_spice].nbre;
        taille_groupe_place2 =
            def_groupe[gpe_spice].taillex * def_groupe[gpe_spice].tailley;
        increment_place = taille_groupe_place / taille_groupe_place2;
        deb_place = def_groupe[gpe_spice].premier_ele;
    }

    deb = def_groupe[numero].premier_ele;
    xmax2 = def_groupe[numero].taillex;
    ymax2 = def_groupe[numero].tailley;
    size = xmax2 * ymax2;
    /*view_size = size * field / 360;*/


    /* tous les neurones de sortie a 0 ... */
    for (i = 0; i < size; i++)
        neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

    /* ...sauf celui correspondant a l'abcisse du pt de focus */
    /* le mouvement est entre 0 et le max de neurone */
    /* on tient compte de la taille de l image des   */
    /* points caracteristiques, ici xmax_carac       */

    /*N.C: interet de ce calcul vu que la variable mvt_x  est ecrase sans etre lu entre temps...?????
     * if(field)  test si group sur 360�
     mvt_x = (position_courante_x * view_size)/xmax_carac + ((size-view_size)/2);
     else
     mvt_x = (position_courante_x * size)/xmax_carac;
     */
    if (field)
        AngleVue =
            ((((float) position_courante_x) / ((float) xmax_carac)) *
             (((float) field) / 360.) - 0.5) * 360.;
    else
        AngleVue =
            ((((float) position_courante_x) / ((float) xmax_carac)) -
             0.5) * 360.;

    if (l2 != -1)               /* test si direction absolue boussole */
    {
        if (r_compas == 1)      /* direction donnee par f_compas */
            Bouss =
                (neurone[def_groupe[gpe_compass].premier_ele].s - 0.5) * 360.;
        if (r_compas == 2)      /* Direction absolue de la camera donnee par f_angle_abs_cam */
        {
            r_var = (float *) def_groupe[gpe_compass].ext;
            if ((r_var) == NULL)
            {
                printf
                    ("function_theta_adapt: L angle n a pas ete trouve dans l extention.\n\t\t Angle = 0.5 assume ...! \n");
                r_angle_norm = 0.5;
            }
            if ((r_var) != NULL)
            {
                r_angle_norm = *r_var;
                printf
                    ("function_theta_adapt: L angle a ete trouve. Angle = %f\n",
                     r_angle_norm);
            }
            Bouss = (r_angle_norm - 0.5) * 360.;
        }
    }
    else
        Bouss = 0.;

    AngleVrai = AngleVue + Bouss;
    while (AngleVrai > 180.)
        AngleVrai = AngleVrai - 360.;
    while (AngleVrai < -180.)
        AngleVrai = AngleVrai + 360.;

    angles[(NbrFeaturesPoints - 1) % 20] = AngleVrai;   /* NONODEBUG pour connaitre les angles des amers vus pour le debuggage */

    /*mise a l'echelle */
    mvt_x = (int) ((AngleVrai + 180.) / 360. * ((float) size));
    if (mvt_x < 0)
        mvt_x += size;
    if (mvt_x >= size)
        mvt_x -= size;

#ifndef AVEUGLE
    printf("neurone No:%d  sur:%d\n", mvt_x, size);
#endif

    if (r_diff && l3 != -1)     /* diffusion et adaptation */
    {
        sum_place_act = 0.;
        compt_plc = 0;
        max_plc = -9999.;
        for (i = deb_place + increment_place - 1;
             i < deb_place + taille_groupe_place; i = i + increment_place)
        {
            if (max_plc < neurone[i].s1)
                max_plc = neurone[i].s1;
            sum_place_act = sum_place_act + neurone[i].s1;
            compt_plc++;
        }
        if (compt_plc != 0)
            sum_place_act = (float) (sum_place_act / compt_plc);

        r_diff =
            (int) (((float) r_diff) / ((max_plc + 0.5) * (max_plc + 0.5)));

    }


    printf("THETA ADAPT :    r_diff=%f \n", (float) r_diff);

    if ((r_diff) /*&& (vigilence < 0.5) */ )    /*diffusion */
    {
        /*a1 = -((float) (r_diff * r_diff)) / log(0.5);*/
        for (d = 0; d < r_diff; d++)
        {
            /*diff = (exp(-(float)d*d/a1)-0.5)*2.; diffusion gaussienne */
            diff = (((float) r_diff) - (float) d) / ((float) r_diff);   /*diffusion linaire */
            /*diff = diff*diff; */

            pos = mvt_x + d;
            /* dans le cas 360� on boucle */
            if ((field) && (pos >= size))
                pos -= size;

            if (pos < size)
                neurone[deb + pos].s = neurone[deb + pos].s1 =
                    neurone[deb + pos].s2 = diff;

            pos = mvt_x - d;
            /* dans le cas 360� on boucle */
            if ((field) && (pos < 0))
                pos += size;

            if (pos < size)
                neurone[deb + pos].s = neurone[deb + pos].s1 =
                    neurone[deb + pos].s2 = diff;
             /*NONODEBUG*/ if (diff > 1.0)
            {
                printf("ATTENTION : diff = %f\n", diff);
                exit(1);
            }
        }
    }

    neurone[deb + mvt_x].s = neurone[deb + mvt_x].s1 =
        neurone[deb + mvt_x].s2 = 1.0;
    printf("Theta adapt !!!!    Neurone[%d]=1 !!! \n", mvt_x);
}
