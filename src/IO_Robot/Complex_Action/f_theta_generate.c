/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_theta_generate.c
\brief

Author: Raoul
Created: 20/04/2000
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:

Macro:
-none

Local variables:
-int xmax_carac
-float angles[20]

Global variables:
-int NbrFeaturesPoints

Internal Tools:
-none

External Tools:
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>

#include <Global_Var/IO_Robot.h>

#include "tools/include/local_var.h"
/*#define DEBUG*/
/*
    typedef struct MyData_f_theta_generate{
    char type;
    int field;
    int gpe_compass;
    int gpe_diffusion;
    float r_diff;
    float r_sec;

    } MyData_f_theta_generate; */


void function_theta_generate(int numero)
{
   int r_diff = 45, field = 360;
   int i, l, mvt_x /*N.C: entre 0 et le nb de neurone */ , d;
   int deb, size ; //, view_size; /* M. Belkaid : Je commente la variable view_size qui genere un warning apres que N.C. ait commente la partie ou elle est utilisee */
   int xmax2, ymax2;
   int pos;
   float diff = 0., a1, r_diff_f = 0., r_sec = 0.;
   float AngleVue, AngleVrai, Bouss = 0.;  /*N.C: En degr�entre -180 et 179 */
   int milieu = 0;
   int gpe_X=-1, max_x, j, gpe_compass = 0, pos_x = 0; //, gpe_diffusion ; /* M. Belkaid : Je commente la variable gpe_diffusion qui genere un warning parce qu'elle n'est jamais utilisee */
   float input_max;
   char type = 'd';
   int double_pano = 0;
   char param_link[255];
#ifdef DEBUG
#ifndef AVEUGLE
   printf("-------------------------------------------------\n");

   printf("Fabrication du reflexe position focalisation \n");
   fflush(stdout);
#endif
#endif

   l = 0;
   i = find_input_link(numero, l);
   while (i != -1)
   {                           /* Recherche de f_pano_focus ou get_pano_alt ou f_get_pano ou load_image_from_disk ou f_load_baccon pour recuperer r_diff ou/et field */

      if (prom_getopt(liaison[i].nom, "f", param_link) == 2)
      {
         field = atoi(param_link);
      }
      if (prom_getopt(liaison[i].nom, "t", param_link) == 2)
      {
         type = param_link[0];
      }

      if (liaison[i].nom[0] == 'f')
      {
         sscanf(liaison[i].nom, "f%c%d", &type, &field);

      }

      /* Recherche de la boussole de type f_compass */
      if ((strcmp(liaison[i].nom, "compass") == 0)
            && (liaison[i].arrivee == numero))
      {
         gpe_compass = liaison[i].depart;
      }
      /* Recherche de la boussole de type f_angle_abs_cam */
      if (strcmp(liaison[i].nom, "diffusion") == 0)
      {
         //gpe_diffusion = liaison[i].depart; /* M. Belkaid : Je commente la variable gpe_diffusion qui genere un warning parce qu'elle n'est jamais utilisee */
         r_diff_f =
               (float) neurone[def_groupe[liaison[i].depart].premier_ele].s;
         /*0=pas de diffusion 1= diffusion total donc sur la moiti�des cellules, �doite et �gauche */
         r_diff = (int) (r_diff_f * def_groupe[numero].nbre / 2);
         r_sec = (int) (0.1 * def_groupe[numero].nbre / 2);
#ifdef DEBUG
         printf("PASSAGE:  %f\n", r_diff_f);
#endif
      }

      if (strcmp(liaison[i].nom, "x_focalisation") == 0)
      {
         gpe_X = liaison[i].depart;
         xmax_carac = def_groupe[gpe_X].nbre;
         max_x = 0;
         input_max = 0;
         for (j = def_groupe[gpe_X].premier_ele;
               j < def_groupe[gpe_X].nbre + def_groupe[gpe_X].premier_ele;
               j++)
            if (neurone[j].s2 > input_max)
            {
               input_max = neurone[j].s2;
               max_x = j;
            }

         pos_x = max_x - def_groupe[gpe_X].premier_ele /*+1 */ ;
      }

      if (strcmp(liaison[i].nom, "x_foc_double_pano") == 0)
      {
         double_pano = 1;
         gpe_X = liaison[i].depart;
         xmax_carac = def_groupe[gpe_X].taillex;
         max_x = 0;
         input_max = 0;
         for (j = def_groupe[gpe_X].premier_ele;
               j <
               def_groupe[gpe_X].taillex + def_groupe[gpe_X].premier_ele;
               j++)
            if (neurone[j].s2 > input_max)
            {
               input_max = neurone[j].s2;
               max_x = j;
            }

         pos_x = max_x - def_groupe[gpe_X].premier_ele + 1;
      }


      l++;
      i = find_input_link(numero, l);

   }

#ifdef DEBUG
   printf("field=%d,mode=%c,diffusion= %d\tx_foc=%d\tx_max=%d\tbouss=%f\n",
         field, type, r_diff, pos_x, xmax_carac,
         neurone[def_groupe[gpe_compass].premier_ele].s);
#endif
   deb = def_groupe[numero].premier_ele;
   xmax2 = def_groupe[numero].taillex;
   ymax2 = def_groupe[numero].tailley;
   size = xmax2 * ymax2;
   //view_size = size * field / 360; /* M. Belkaid : Je commente la variable view_size qui genere un warning apres que N.C. ait commente la partie ou elle est utilisee */


   /* tous les neurones de sortie a 0 ... */
   for (i = 0; i < size; i++)
      neurone[deb + i].s = neurone[deb + i].s1 = neurone[deb + i].s2 = 0.;

   /* ...sauf celui correspondant a l'abcisse du pt de focus */
   /* le mouvement est entre 0 et le max de neurone */
   /* on tient compte de la taille de l image des   */
   /* points caracteristiques, ici xmax_carac       */

   /*N.C: interet de ce calcul vu que la variable mvt_x  est ecras�sans etre lu entre temps...?????
    * if(field)  test si group sur 360
     mvt_x = (position_courante_x * view_size)/xmax_carac + ((size-view_size)/2);
     else
     mvt_x = (position_courante_x * size)/xmax_carac;
    */
   /* if(field)
       AngleVue = (( ((float)pos_x) /((float)xmax_carac)) *(((float)field)/360.)-0.5)*360.;
       else
       AngleVue = (( ((float)pos_x)/ ((float)xmax_carac))-0.5)*360.;


    */

   if (gpe_X == -1) // if no gpe_X then default value for AngleVue is 0.5 (center), we have to set max value, representing the image width when there is gpe_X
   {
      AngleVue = 0.5 ;
   }
   else if (double_pano == 1)
   {
      milieu = xmax_carac / 2;
      if (pos_x <= milieu)
         AngleVue =
               ((float) pos_x -
                     (float) xmax_carac / 4) / ((float) xmax_carac / 2) *
                     (float) field;
      else
         AngleVue =
               ((float) pos_x - milieu -
                     (float) xmax_carac / 4) / ((float) xmax_carac / 2) *
                     (float) field;
   }
   else
   {
      /*Angle Vue entre -field/2 et field/2 */
      AngleVue = ((((float) pos_x) / ((float) xmax_carac)) - 0.5) * field;
   }
#ifdef DEBUG
   printf("_____\n angle vue: %f\n", AngleVue);
#endif
   /*Bouss entre -180 et 180*/
   Bouss = (neurone[def_groupe[gpe_compass].premier_ele].s - 0.5) * 360.;
#ifdef DEBUG
   printf("bouss : %f\n", Bouss);
#endif
   if (double_pano == 0)
      AngleVrai = Bouss + AngleVue;
   else if (pos_x <= milieu)
      AngleVrai = Bouss - 90 + AngleVue;
   else
      AngleVrai = Bouss + 90 + AngleVue;

   while (AngleVrai >= 180.)
      AngleVrai = AngleVrai - 360.;
   while (AngleVrai < -180.)
      AngleVrai = AngleVrai + 360.;

   angles[(NbrFeaturesPoints - 1) % 20] = AngleVrai;   /* NONODEBUG pour connaitre les angles des amers vus pour le debuggage */
#ifdef DEBUG
   printf("anlge vrai: %f\n", AngleVrai);
#endif
   AngleVrai = AngleVrai + 180.;
#ifdef DEBUG
   printf("anlge vrai: %f\n", AngleVrai);
#endif


   /*mise �l'echelle */
   mvt_x = (int) (AngleVrai / 360. * ((float) size));
   if (mvt_x < 0)
      mvt_x += size;
   if (mvt_x >= size)
      mvt_x -= size;
#ifdef DEBUG
#ifndef AVEUGLE
   printf("neurone No:%d  sur:%d , angle = %f\n", mvt_x, size, AngleVrai);
   printf("THETA ADAPT :    r_diff=%f \n", (float) r_diff);
#endif
#endif

   if ((r_diff))               /*diffusion */
   {
      a1 = -((float) (r_diff * r_diff)) / log(0.5);
      for (d = 0; d < r_diff; d++)
      {
         /*diff = (exp(-(float)d*d/a1)-0.5)*2.; **diffusion gaussienne */
         if (type == 'a')
            diff = (((float) r_diff) - (float) d) / ((float) r_diff);   /*diffusion linaire */
         /*diff = diff*diff; */
         else if (type == 'c')
         {
            diff =
                  (d / (r_sec - r_diff) + 1 - (r_sec) / (r_sec - r_diff));
            if (diff > 1)
               diff = 1;
         }
         else if (type == 'd')
         {
            diff = (exp(-(float) d * d / a1) - 0.5) * 2.;   /*diffusion gaussienne */

         }
         pos = mvt_x + d;
         /* dans le cas 360 on boucle */
         if ((field) && (pos >= size))
            pos -= size;

         if (pos < size)
         {
            if (type == 'b')
               neurone[deb + pos].s = neurone[deb + pos].s1 =
                     neurone[deb + pos].s2 = 1;
            else
               neurone[deb + pos].s = neurone[deb + pos].s1 =
                     neurone[deb + pos].s2 = diff;

         }
         pos = mvt_x - d;
         /* dans le cas 360 on boucle */
         if ((field) && (pos < 0))
            pos += size;

         if (pos < size)
         {
            if (type == 'b')
               neurone[deb + pos].s = neurone[deb + pos].s1 =
                     neurone[deb + pos].s2 = 1;
            else
               neurone[deb + pos].s = neurone[deb + pos].s1 =
                     neurone[deb + pos].s2 = diff;
         }
         /*NONODEBUG*/ if (diff > 1.0)
         {
            printf("ATTENTION : diff = %f\n", diff);
            exit(1);
         }
      }
   }


   neurone[deb + mvt_x].s = neurone[deb + mvt_x].s1 =
         neurone[deb + mvt_x].s2 = 1.0;
#ifdef DEBUG
   printf("Theta adapt !!!!    Neurone[%d]=1 !!! \n", mvt_x);
#endif
}
