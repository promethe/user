/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  f_reach_destination.c
    \brief 

    Author: J. Hirel
    Created: 18/02/2008
    Modified:
    - author: 
    - description: 
    - date: 

    Theoritical description:
    - \f$  LaTeX equation: none \f$

    Description:
    Gives the angle and distance to a particular destination (given with an absolute position) based on the
    current robot position and orientation

    Macro:
    -none

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools:
    -Kernel_Function/find_input_link()
    -Kernel_Function/prom_getopt()

    Links:
    - pos: Current robot location and orientation
    - destination: Absolute coordinates of the destination

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*#define DEBUG*/

#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>

typedef struct mydata_reach_destination
{
      int gpe_pos;
      int gpe_destination;
} mydata_reach_destination;

void new_reach_destination(int gpe)
{
   mydata_reach_destination *my_data = NULL;
   int i, l;
   int gpe_pos = -1;
   int gpe_destination = -1;

   if (def_groupe[gpe].nbre != 2)
   {
      fprintf(stderr, "ERROR in new_reach_destination(%s): Group size must be 2 neurons (angle, distance)\n", def_groupe[gpe].no_name);
      exit(1);
   }


   if (def_groupe[gpe].data == NULL)
   {
      i = 0;
      l = find_input_link(gpe, i);
      while (l != -1)
      {
	 if (strcmp(liaison[l].nom, "pos") == 0)
	    gpe_pos = liaison[l].depart;
	 if (strcmp(liaison[l].nom, "destination") == 0)
	    gpe_destination = liaison[l].depart;

	 i++;
	 l = find_input_link(gpe, i);
      }

      if (gpe_pos == -1 || gpe_destination == -1)
      {
	 fprintf(stderr, "ERROR in new_reach_destination(%s): Missing input link 'pos' or 'destination'\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      if (def_groupe[gpe].nbre != 2)
      {
	 fprintf(stderr, "ERROR in new_reach_destination(%s): Group size must be 2 (angle, distance)\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      if (def_groupe[gpe_pos].nbre < 2)
      {
	 fprintf(stderr, "ERROR in new_reach_destination(%s): Group size for 'pos' input group must be at least 2 (x, y)\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      if (def_groupe[gpe_destination].nbre < 2)
      {
	 fprintf(stderr, "ERROR in new_reach_destination(%s): Group size for 'destination' input group must be at least 2 (x, y)\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data = (mydata_reach_destination *) malloc(sizeof(mydata_reach_destination));
      if (my_data == NULL)
      {
	 fprintf(stderr, "ERROR in new_reach_destination(%s): malloc failed for data\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data->gpe_pos = gpe_pos;
      my_data->gpe_destination = gpe_destination;
      def_groupe[gpe].data = (mydata_reach_destination *) my_data;
   }
}

void function_reach_destination(int gpe)
{
   int gpe_destination, gpe_pos;
   mydata_reach_destination *my_data = NULL;
   float dest_posx, dest_posy, robot_posx, robot_posy, /*robot_orientation, */distance, angle, dx, dy;
   int first = def_groupe[gpe].premier_ele;
   
   my_data = (mydata_reach_destination *) def_groupe[gpe].data;
   if (my_data == NULL)
   {
      fprintf(stderr, "ERROR in f_reach_destination(%s): Cannot retrieve data\n", def_groupe[gpe].no_name);
      exit(1);
   }
   
   gpe_pos = my_data->gpe_pos;
   gpe_destination = my_data->gpe_destination;

   robot_posx = neurone[def_groupe[gpe_pos].premier_ele].s1;
   robot_posy = neurone[def_groupe[gpe_pos].premier_ele+1].s1;

   dest_posx = neurone[def_groupe[gpe_destination].premier_ele].s1;
   dest_posy = neurone[def_groupe[gpe_destination].premier_ele+1].s1;

   dx = robot_posx - dest_posx;
   dy = robot_posy - dest_posy;

   distance = sqrt(dx * dx + dy * dy);
   angle = atan2f(dy, dx) + M_PI;

   dprints("f_reach_destination(%s): angle = %f, distance = %f\n", def_groupe[gpe].no_name, angle, distance);

   neurone[first].s = neurone[first].s1 = neurone[first].s2 = angle;
   neurone[first+1].s = neurone[first+1].s1 = neurone[first+1].s2 = distance;
}

void destroy_reach_destination(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((mydata_reach_destination*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_reach_destination(%s): Leaving function\n", def_groupe[gpe].no_name);
}
