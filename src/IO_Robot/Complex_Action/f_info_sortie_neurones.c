/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 ___NO_COMMENT___
 ___NO_SVN___

 \file  f_info_sortie_neurones.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include "tools/include/local_var.h"
#include "tools/include/reste_a_apprendre.h"
#include <stdlib.h>
#include <libhardware.h>
void tri_bulle_f(float *t, int nb);

void tri_bulle_f(float *t, int nb)
{
  int i;
  int chngt = 1;
  float prov;

  while (chngt != 0)
  {
    chngt = 0;
    for (i = 0; i < nb - 1; i++)
    {
      if ((*(t + i)) < (*(t + i + 1)))
      {
        prov = *(t + i);
        *(t + i) = *(t + i + 1);
        *(t + i + 1) = prov;
        chngt = 1;
      }
    }
  }
}

void function_info_sortie_neurones(gpe_sortie)
  int gpe_sortie;
{

  int taille_groupe_e2;
  int i, j, n;
  char *s = (char*) "Map", *s2 = (char*) "spice";
  int gpe_entree = 0, taille_groupe_e = 0, increment = 0, deb_e = 0;
  float max_autres, moy_autres, celui_la = 0.; /* variables pour infos relatives entre place cells */

  fprintf(stdout, "%s\n", (char*) "-----------  Function info sorties -----------\n");

  /* localisation des neurones          */
  /* recherche des deux liaisons d'entree */

  for (i = 0; i < nbre_liaison; i++)
    if (liaison[i].arrivee == gpe_sortie)
    {
      gpe_entree = liaison[i].depart;
      taille_groupe_e = def_groupe[gpe_entree].nbre;
      taille_groupe_e2 = def_groupe[gpe_entree].taillex * def_groupe[gpe_entree].tailley;
      increment = taille_groupe_e / taille_groupe_e2;
      deb_e = def_groupe[gpe_entree].premier_ele;
      break;
    }

  /*printf ("\nNONODEBUG : gpe_entree = %d gpe_sortie = %d taille = %d increment = %d\n",gpe_entree,gpe_sortie,taille_groupe_e,increment);
   */

  if ((strncmp(def_groupe[gpe_entree].nom, s, 3) == 0) || (strncmp(def_groupe[gpe_entree].nom, s2, 5) == 0))
  {
    tri_bulle_f((float *) &angles[0], 20);
    for (j = 0; j < 20; j++)
      fprintf(Fichier_positions_amers, "%f, ", angles[j]);
    fprintf(Fichier_positions_amers, "\n");
    fflush(Fichier_positions_amers);

    if (reste_a_apprendre() > 0)
    {
      for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e; n = n + increment)
      {
        if (isequal(neurone[n].s2, 1.))
        {
          printf("\nNONODEBUG : Trouve\n");
          break;
        }
      }
      /*
       if (premfois > 0) n2pl[n-deb_e+increment-1] =  premfois-1; else if (secfois > 0) n2pl[n-deb_e+increment-1] =  secfois-1+nb_place_reflex;
       */
      if (reste_a_apprendre() > 0) n2pl[n - deb_e + increment - 1] = compte_fois[reste_a_apprendre() - 1] - 1 + (reste_a_apprendre() - 1) * nb_place_reflex;
      /*     printf("\nNONODEBUG : n2pl[%d] = %d\n",n-deb_e+increment-1,premfois-1); */
      fprintf(sortie_neurones, "Le neurone : %d code pour la place %d avec l'orientation : %f\n", n - deb_e + increment, n2pl[n - deb_e + increment - 1], compass_read(compass_get_first_compass()));
    }
    else
    {
      /*    printf("\nNONODEBUG : Phase utilisation\n"); */
      fprintf(sortie_neurones, "\nOrientation : %f\n", compass_read(compass_get_first_compass()));
      max_autres = -1000.;
      moy_autres = 0.;

      for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e; n = n + increment)
      {
        fprintf(sortie_neurones, "place %d s:%.4f s1:%.4f s2:%.4f\n", n2pl[n - deb_e + increment - 1], neurone[n].s, neurone[n].s1, neurone[n].s2);
        if ((n2pl[n - deb_e + increment - 1] > -1) && (n2pl[n - deb_e + increment - 1] != nb_place_reflex))
        {
          if (max_autres < neurone[n].s1) max_autres = neurone[n].s1;
          moy_autres = moy_autres + neurone[n].s1;
        }
        if (n2pl[n - deb_e + increment - 1] == nb_place_reflex) celui_la = neurone[n].s1;
      }
      moy_autres = moy_autres / (nb_place_reflex - 1);
      fprintf(sortie_neurones, "\nactivite correcte / moy_autres = %f, act corr / max_autres = %f\n", celui_la / moy_autres, celui_la / max_autres);
      fflush(sortie_neurones);

    }
  }
  else
  {
    if (!Fichier_sortie4)
    {
      printf("\nUtilisation de Fichier_sortie4 impossible, exiting...\n");
      abort();
    }
    for (n = deb_e + increment - 1; n < deb_e + taille_groupe_e; n = n + increment)
    {
      fprintf(Fichier_sortie4, "%f ", neurone[n].s2);
    }
    fprintf(Fichier_sortie4, "\n");
    fflush(Fichier_sortie4);
  }
}
