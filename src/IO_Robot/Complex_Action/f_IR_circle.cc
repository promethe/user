/**
\defgroup f_IR_circle f_IR_circle
\ingroup libIO_Robot

\brief Utilisation des IR du robot Koala

\section Modified
- author: N.Cuperlier
- description: specific file creation
- date: 01/09/2004



\section Description
function puting the 16 koala IR sensors on a 32 neurons cercle -PI Pi
This function is usefull if obstacle avoidance should be preformed by a Neural field.
\section Macro
-none 

\section Local variables
-none

\section Global variables
-none

\section Internal Tools
-none

\section External Tools
-none

\section Links
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

\section Comments

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org

\file f_IR_circle.c



Doc similaire a celle du groupe ...
PLus de details...
**/


#include <libx.h>
#include <libhardware.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <public_tools/IO_Robot/GenericTools.h>
#include <Kernel_Function/find_input_link.h>
/*angles des capteurs pour le moment en constante, finira dans le .dev*/
#define PI 3.14159265

/*#define DEBUG*/
void function_IR_circle(int gpe)
{
  int deb,nbre,nbre2,i,middle=-1;

  float res=-1,angle;
  float /*sigma,*/ IR[16];//tableau des valeur des ir

 /*recuperation des orientation des ir*/
float R0_angle;
float R1_angle;
float R2_angle;
float R3_angle;
float R4_angle ;
float R5_angle ;
float L0_angle ;
float L1_angle ;
float L2_angle ;
float L3_angle ;
float L4_angle ;
float L5_angle ;
float c;
int dl=1,dli=-1;
char * st;
 /* float j, psy;*/

  deb=def_groupe[gpe].premier_ele;
  nbre=def_groupe[gpe].nbre;
  nbre2=def_groupe[gpe].taillex*def_groupe[gpe].tailley;

  if(nbre2!=nbre){
  	 cprints("Neurones of this group should be on one line!\n");
	  exit(0);
     }

   if(nbre2 == 1)
     {
       cprints("This group should have more than 1 neuron!!\n");
       exit(0);
     }

     /*printf("debut IR_circle\n");*/
/*resolution*/
res = 2*PI/nbre;
middle=(int)nbre/2;
/*printf("Res: %f, middle:%d\n",res,middle);*/
/* sensors reading */
Robot * robot = robot_get_first_robot();
robot_get_ir(robot,IR);
/*printf("apres get_ir\n");*/
		dli=find_input_link(gpe,0); /* recherche le 1er lien */
  if(dli==-1) {printf("error no input group for group, dl=1 %d\n",gpe);dl=1;c=1.;}
  else {dl=(int)liaison[dli].norme;
     st = strstr ( liaison[dli].nom, "-c");
      if (st != NULL)
	c = atof (&st[2]);
		printf("c: %f, dl: %d\n",c,dl);
  }
if(robot_get_robot_type(robot)==1)
{
	 R0_angle=robot->angle_ir[0] ;
	 R1_angle= robot->angle_ir[1];
	 R2_angle= robot->angle_ir[2] ;	
	 R3_angle = robot->angle_ir[3];
	R4_angle = robot->angle_ir[4];
	R5_angle = robot->angle_ir[5];	
	 L0_angle = robot->angle_ir[8];
	 L1_angle = robot->angle_ir[9];
	L2_angle = robot->angle_ir[10];
	 L3_angle = robot->angle_ir[11];
	 L4_angle = robot->angle_ir[12];
	 L5_angle = robot->angle_ir[13];
	 
	 for(i=0;i<robot->nb_ir;i++)
	 {
	 	if(IR[i]<robot->seuil_ir)
			IR[i]=0.;
	 }
	for (i=-middle; i<middle;i++)
	{
		angle=res*i;
		if( angle<=(L5_angle *PI/2)/90&&  angle>((L5_angle *PI/2)/90)-res)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =   IR[13];
		
		}
		else if( angle<=(L4_angle *PI/2)/90 && angle>((L4_angle *PI/2)/90)-res )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =   IR[12];
		
		}
		else if( angle<=(L3_angle *PI/2)/90 && angle>((L3_angle *PI/2)/90)-res)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 = IR[11];
		
		}
		
		else if( angle<=(L2_angle *PI/2)/90  && angle>((L2_angle *PI/2)/90)-res)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[10];
		
		}
		else if( angle<=(L1_angle *PI/2)/90 && angle>((L1_angle *PI/2)/90)-res)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[9];
		
		}
		else if( angle<=(L0_angle *PI/2)/90 && angle>((L0_angle *PI/2)/90)-res)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[8];
		
		}
		else if( angle<=(R0_angle*PI/2)/90&&angle>((R0_angle *PI/2)/90)-res) {
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 = IR[0];
		
		}
		else if( angle<=(R1_angle*PI/2)/90&&angle>((R1_angle *PI/2)/90)-res )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[1];
		
		}
		else if( angle<=(R2_angle*PI/2)/90&&angle>((R2_angle *PI/2)/90)-res  )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 = IR[2];
		}
		else if( angle<=(R3_angle*PI/2)/90 && angle>((R3_angle *PI/2)/90)-res )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[3];
		
		}
		else if( angle<=(R4_angle *PI/2)/90&&angle>((R4_angle *PI/2)/90)-res )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[4];
		
		}
		else if( angle>(((R5_angle*PI/2)/90)-res) && res*i<=(R5_angle*PI/2)/90)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 = IR[5];
		
		}
		else{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =0;
		#ifdef DEBUG
		dprints("Neuron not affected on pos:%d, seuil:%f ,value: %f\n",i+middle,(res*i*90/(PI/2)),neurone[deb+i+middle].s1);
		#endif
		}
	}
}
else if(robot_get_robot_type(robot)==2)
{ 
	R0_angle=robot->angle_ir[5]; 
	R1_angle= robot->angle_ir[6];
	R2_angle= robot->angle_ir[7] ;	
	R3_angle = robot->angle_ir[8];
	R4_angle = robot->angle_ir[9];	
	L0_angle = robot->angle_ir[4];
	L1_angle = robot->angle_ir[3];
	L2_angle = robot->angle_ir[2];
	L3_angle = robot->angle_ir[1];
	L4_angle = robot->angle_ir[0];
	cprints("R0:%f,L0:%f\n",R0_angle,L0_angle);
	for(i=0;i<robot->nb_ir;i++)
	 {
	 	if(IR[i]<robot->seuil_ir)
			IR[i]=0.;
	 }
	for (i=-middle; i<middle;i++)
	{
		angle=res*i;
		if( angle<=(R2_angle *PI/2)/90&&  angle>-PI/2-PI/8)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[7];
		
		}
		else if( angle<=(R1_angle *PI/2)/90 && angle>((R2_angle *PI/2)/90) )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[6];
		
		}
		else if( angle>=(R0_angle *PI/2)/90 && angle<=(6*PI/2)/90/*((R0_angle *PI/2)/90)*/)
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  c*IR[5];
		
		}
		else if( angle>=-(6*PI/2)/90/*((L0_angle*PI/2)/90)*/ &&angle<=((L0_angle *PI/2)/90)) {
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =c*IR[4];
		
		}
		else if( angle<((L2_angle*PI/2)/90) &&angle>((L1_angle *PI/2)/90) )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[3];
		
		}
		else if( angle<=PI/2+PI/8&&angle>=((L2_angle *PI/2)/90)/*-res*/  )
		{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =  IR[2];
		}
		else{
		neurone[deb+i+middle].s = neurone[deb+i+middle].s1 = neurone[deb+i+middle].s2 =0;
		/* printf("Neuron not affected on pos:%d, seuil:%f ,value: %f\n",i+middle,(res*i*90/(PI/2)),neurone[deb+i+middle].s1);*/
		}
	}
	#ifdef DEBUG
	if(neurone[deb+i+middle].s1>0)
	dprints("Neuron pos:%d , seuil %f  ,value: %f\n",i+middle,(res*i*90/(PI/2)),neurone[deb+i+middle].s1);
	#endif
}

#ifdef DEBUG
dprints("---------------------------End-------------------------\n");
#endif
}
