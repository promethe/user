/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_get_panorama_alternate.c
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 01/09/2004
 Modified:
 - author: N.Cuperlier
 - description: blue robot compatibility
 - date: 07/09/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:
 Prend une image panoramique, les bibliotheques Xil doivent etre initialises (function_start_capture)

 Macro:
 -none

 Local variables:
 -int rg_pano
 -int rg_diff

 Global variables:
 -none

 Internal Tools:
 -InitPano()
 -GetPano()

 External Tools:
 -USE_ROBOT

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <libhardware.h>
#include <Typedef/boolean.h>
#include <Struct/prom_images_struct.h>

#include "tools/include/local_var.h"
#include <Global_Var/IO_Robot.h>

#include "tools/include/GetPano.h"
void InitPano(float Pano_AngleTot, float Pano_AngleVue, int Pano_NbrIm, prom_images_struct * image_in);

void InitPano(float Pano_AngleTot, float Pano_AngleVue, int Pano_NbrIm, prom_images_struct * image_in)
{
  int xmax;
  int ymax;
  int xpanomax;
  int ypanomax;
  float XmaxPano;


  /*Pret a recevoir les donnees */
  if (image_in == NULL)
  {
    printf("image_in == NULL erreur dans InitPano\n");
    exit(EXIT_FAILURE);
  }
  printf("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ", __FUNCTION__);
  exit(0);

  /* Normalement: 384 et 288 */
  /* xmax = images_capture.sx;
   ymax = images_capture.sy; */

  if (USE_ROBOT == 2)
  {
    xmax = 320;
    ymax = 240;
  }
  else if (USE_ROBOT == 1)
  {
    /*xmax =get_width(); *//* Normalement: 384 et 288 */
    /*ymax=get_height(); */
  }
  else
  {
    printf("USE_ROBOT not set in the .dev file!\n Abording...\n");
    exit(-1);
  }
  /* calcul de la taille de l'image panoramique */
  XmaxPano = (float) xmax * Pano_AngleTot / Pano_AngleVue;
  xpanomax = (int) XmaxPano;
  ypanomax = ymax;
  /* Initialisation de image_in panoramique */
  (*image_in).image_number = 2;
  /*(*image_in).nb_band=get_nbands();*/
  (*image_in).sx = xpanomax;
  (*image_in).sy = ypanomax;
  (*image_in).images_table[0] = (unsigned char *) malloc(xpanomax * ypanomax); /* image panoramique */
  (*image_in).images_table[1] = (unsigned char *) malloc(xmax * ymax * 3); /* image temporaire */

  if ((*image_in).images_table[1] == NULL || (*image_in).images_table[0] == NULL)
  {
    printf("InitPano: ALLOCATION IMPOSSIBLE ...! \n");
    exit(-1);
  }
  /* printf("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
   printf("RAOUL: xmax:%d ymax:%d xpanomax:%d ypanomax:%d\n", xmax, ymax, xpanomax, ypanomax);
   printf("RAOUL: Pano_AngleTot:%f   Pano_AngleVue:%f   Pano_NbrIm:%d\n", Pano_AngleTot, Pano_AngleVue, Pano_NbrIm); */
  (void) Pano_NbrIm;
}

void function_get_panorama_alternate(int gpe_sortie)
{
  int first;
  int nofind;
  int i, gpe_entree, l = -1;
  int r_diff, field;
  int rrrpanofoc;
  boolean take_photo = false;
  int ret;

  prom_images_struct *p_image;

  int r_temp;

  first = 0;

  p_image = (prom_images_struct *) def_groupe[gpe_sortie].ext;

  if (p_image == NULL)
  {
    /*printf("RAOUL: p_image est defini une fois\n"); */
    p_image = (prom_images_struct *) malloc(sizeof(prom_images_struct)); /*allocation du pointeur */
    if ((p_image) == NULL)
    {
      printf("function_get_panorama_alternate: ALLOCATION IMPOSSIBLE ...! \n");
      exit(-1);
    }
    first = 1;
  }

  /**/printf("function_get_panorama_alternate\n");

  /*RAOULG*/if (rg_pano != -2)
  /*RAOULG*/
  {
    /*RAOULG*/r_temp = -1000;
    /*RAOULG*/while ((r_temp != -2) && (r_temp != -1) && (r_temp != 77) && (r_temp != 300))
    /*RAOULG*/
    {
      /*RAOULG*/
      printf("Entrez l amplitude du panorama (-2, -1, 77 ou 300 -- old: %d) :\n", rg_pano);
      /*RAOULG*/ret = scanf("%d", &r_temp);
      /*RAOULG*/}
    /*RAOULG*/rg_pano = r_temp;
    /*RAOULG*/}
  /*RAOULG*/if (rg_diff != -2)
  /*RAOULG*/
  {
    /*RAOULG*/r_temp = -1000;
    /*RAOULG*/while ((r_temp == -1000))
    /*RAOULG*/
    {
      /*RAOULG*/
      printf("Entrez la diffusion (-2, -1, ou 0 a 70 -- old: %d) :\n", rg_diff);
      /*RAOULG*/ret = scanf("%d", &r_temp);
      /*RAOULG*/}
    /*RAOULG*/rg_diff = r_temp;
    /*RAOULG*/}
  nofind = 1;

  for (i = 0; i < nbre_liaison; i++)
  {
    if (liaison[i].arrivee == gpe_sortie)
    {
      gpe_entree = liaison[i].depart;
      l = i;
      nofind = 0;
    }
  }

  if (nofind)
  {
    printf("\nRAOULDEBUG : Pas trouve function_get_panorama_alternate (!!!), exiting...\n");
    abort();
  }
  rrrpanofoc = 0;
  if (liaison[l].nom[0] == 'p')
  {
    rrrpanofoc = 1;
    if (liaison[l].nom[1] == 'h') take_photo = true;
  }
  if (liaison[l].nom[0] == 'r') rrrpanofoc = 2;

  printf("\nVINCENTDEBUG : rrrpanofoc=%d\n", rrrpanofoc);

  if (rrrpanofoc == 0)
  {
    printf("\nRAOULDEBUG : Pas trouve d infos sur le lien menant a function_get_panorama_alternate (!!!), exiting...\n");
    abort();
  }

  /* Ici, on recherche les infos contenus dans le lien devant Pano_focus (la procedure normale) */
  if (rrrpanofoc == 1)
  {
    /**/printf("\tA la recherche de Pano_Focus...\n");
    nofind = 1;
    for (i = 0; i < nbre_liaison; i++)
    {
      if ((strcmp(def_groupe[liaison[i].depart].nom, "f_pano_focus") == 0) && (liaison[i].nom[0] == 'b'))
      {
        if (nofind)
        {
          gpe_entree = liaison[i].depart;
          l = i;
          nofind = 0;
        }
      }
    }
    if (nofind)
    {
      printf("\nNONODEBUG : Pas trouve f_pano_focus dans function_get_panorama_alternate, exiting...\n");
      abort();
    }
    if (liaison[l].nom[0] == 'b')
    {
      sscanf(liaison[l].nom, "b%d,%d", &r_diff, &field);
      /*printf("RAOUL: r_diff:%d   field:%d\n", r_diff, field); */
    }
    else
    {
      printf("\nNONODEBUG : Probleme de nom de liaison dans function_get_panorama_alternate, exiting... \n");
      abort();
    }
  }

  /* Ici, on recherche les infos contenus dans le lien devant Get_Pano_Alt (pour effectuer un simple panorama) */
  if (rrrpanofoc == 2)
  {
    /**/printf("\tA la recherche des infos sur le Lien...\n");
    if (liaison[l].nom[0] == 'r')
    {
      sscanf(liaison[l].nom, "r%d,%d", &r_diff, &field);
      /**/printf("RAOUL: r_diff:%d   field:%d\n", r_diff, field);
    }
    else
    {
      printf("\nNONODEBUG : Probleme de nom de liaison dans function_get_panorama_alternate, exiting... \n");
      abort();
    }
  }

  /*RAOULG*/if ((rg_pano != -1) && (rg_pano != -2))
  /*RAOULG*/field = rg_pano;
  /*  field=273; 250 */
  /*RAOULG*/printf("field (Cyril): %d\n", field);

  /*Raoul: Maintenant, je fait l'appel a InitPano et GetPano** 77: angle de vue   32: Nombre de sous-images */
  /* first=1; */
  if (first)
  { /*InitPanorama(); */
    if (USE_ROBOT == 2)
    {
      field = 273; /*NC:Pour le moment coder en dure (a modifier...) */
      InitPano((float) field, 68.0, 24, p_image);
    }
    else if (USE_ROBOT == 1)
    {
      InitPano((float) field, 77, 32, p_image); /*InitPano((float)field, 77, 32, p_image); */
    }
    else
    {
      printf("Vous devez definir la variable USE_ROBOT dans le fichier .dev !!!!");
      abort();
    }
    first = 0;
  }

  /* Raoul: Si l on doit refaire un nouveau get_pano, on le fera
   ici. Philippe a dit que ce n'etait pas d'actualite: C'est
   donc une copie corrigee de GetPanoramaLarge  */
  if (USE_ROBOT == 2)
  {
    field = 273; /*NC:Pour le moment coder en dure (a modifier...) */

    /*GetPano((float)field, 68.0, 24, p_image,take_photo); */
  }
  else if (USE_ROBOT == 1)
  {

    /*                     GetPano((float)field, 77, 32, p_image,take_photo); */

  }
  else
  {
    printf("Vous devez definir la variable USE_ROBOT dans le fichier .dev !!!!");
    abort();
  }
  /*GetPanorma(); */
  /* DEBUT Precedent: Ce qui suit etait le programme original */
  /*  if(first)
   {
   if (field==250) InitPanorama();
   else if (field==300) InitPanorama_large();
   else {printf("\nNONODEBUG : Probleme: field de vue ni 250 ni 300 dans function_get_panorama_alternate, exiting...\n");abort();}
   first = 0;
   }
   if (field==250)   GetPanorama();
   else if (field==300)   GetPanorama_large();
   else {printf("\nNONODEBUG : Probleme: field de vue ni 250 ni 300 dans function_get_panorama_alternate, exiting...\n");abort();}
   */
  /* FIN Precedent*/

  /* Copie de l'image de sortie */
  def_groupe[gpe_sortie].ext = (void *) (p_image);
  (void) take_photo;
  (void) gpe_entree;
  (void) ret;
}
