/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_landmarks_sequential.c 
\brief 

Author: Julien Hirel
Created: 30/01/2009
Modified:
- author: 
- description: 
- date: 

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description:
Gets landmarks information from the hardware library.
Neuron activity corresponds to the identity of the landmark, position, and wether it is the last in the panorama.
This function outputs landmark information sequentially, one landmark by one.

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

#include <Kernel_Function/find_input_link.h>
#include <net_message_debug_dist.h>
#include <libhardware.h>

typedef struct mydata_landmarks_sequential
{
      int gpe_reset;
}mydata_landmarks_sequential;

void new_landmarks_sequential(int gpe)
{
   int i, l;
   int gpe_reset = -1;
   mydata_landmarks_sequential *my_data = NULL;

   if (def_groupe[gpe].nbre != 4)
   {
      fprintf(stderr, "ERROR in new_landmarks_sequential(%s): Group size must be 4 neurons\n", def_groupe[gpe].no_name);
      exit(1);
   }   

   if (def_groupe[gpe].data == NULL)
   {
      l = 0;
      i = find_input_link(gpe, l);    /* recherche les liens en entree */
      while (i != -1)
      {
	 if (strcmp(liaison[i].nom, "reset") == 0)
	    gpe_reset = liaison[i].depart;

	 l++;
	 i = find_input_link(gpe, l);
      }

      my_data = (mydata_landmarks_sequential*) malloc(sizeof(mydata_landmarks_sequential));
      if (my_data == NULL)
      {
	 fprintf(stderr, "ERROR in new_landmarks_sequential(%s): malloc failed for data\n", def_groupe[gpe].no_name);
	 exit(1);
      }

      my_data->gpe_reset = gpe_reset;
      def_groupe[gpe].data = my_data;
   }

}

void function_landmarks_sequential(int gpe)
{
   int landmark_id = -1;
   float azimuth = 0., elevation = 0.;
   int deb = def_groupe[gpe].premier_ele;
   Robot *robot = robot_get_first_robot();
   int gpe_reset = ((mydata_landmarks_sequential *) def_groupe[gpe].data)->gpe_reset;

   if (robot == NULL)
   {
      fprintf(stderr, "ERROR in f_landmarks_sequential(%s): No robot defined\n", def_groupe[gpe].no_name);
      exit(1);
   }

   /* Resets landmarks */
   if (gpe_reset >= 0 && neurone[def_groupe[gpe_reset].premier_ele].s1 > 0.5)
      robot_reset_landmarks(robot);


   robot_get_one_landmark(robot, &landmark_id, &azimuth, &elevation);

   neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = landmark_id;
   neurone[deb+1].s = neurone[deb+1].s1 = neurone[deb+1].s2 = azimuth;
   neurone[deb+2].s = neurone[deb+2].s1 = neurone[deb+2].s2 = elevation;

   if (landmark_id == -1)
      neurone[deb+3].s = neurone[deb+3].s1 = neurone[deb+3].s2 = 1.;
   else
      neurone[deb+3].s = neurone[deb+3].s1 = neurone[deb+3].s2 = 0.;
}
