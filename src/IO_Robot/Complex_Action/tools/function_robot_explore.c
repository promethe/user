/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <libhardware.h>
#include "include/local_var.h"

#include <libx.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <Global_Var/IO_Robot.h>
#include <IO_Robot/GenericTools.h>

#include <public_tools/Vision.h>

void robot_content_montre()
{
    char data[80];
    char *noth = 0;
    int param[2], position[2], i, signe = -1;

    param[0] = 0;
    param[1] = 0;
    position[0] = 256;
    position[1] = 652;
    while ((position[0] != 0) || (position[1] != 0))
    {
        robot_commande_koala(robot_get_first_robot(), COM_G, param, data);
        robot_commande_koala(robot_get_first_robot(), COM_H, (int *) noth,
                             data);
        sscanf(data, "%d,%d", &(position[0]), &(position[1]));
    }
    param[0] = 500;
    param[1] = 500;
    for (i = 0; i < 10; i++)
    {
        param[0] = signe * param[0];
        param[1] = signe * param[1];
        signe = -signe;
        robot_commande_koala(robot_get_first_robot(), COM_C, param, data);
        while (!robot_precise_last_target_reached(robot_get_first_robot())) ;
    }
}

/*==========================================*/
/*==========================================*/
void braittenberg_koala_detect_goal(sec)
     float sec;
{
    /*int                 parm[2]; */
    int *nothing = 0;
    int i, j;
    float IR[16];
    float mot[2];
    char data[80];
    int param[2];
    int d[2];
    float Angle;
    /* Coefficients du braittenberg */
    /* static float               W[2][4]={{1.5, 1., 1., 1.5},{1.5, 1., 1., 1.5}}; */
    /* Valeur des poids */
    static float W[2][12] = {
        {-1., -1., 1., 1., 1., 1., -1., -1., -1., -1., -1., -1.},
        {-1., -1., -1., -1., -1., -1., -1., -1., 1., 1., 1., 1.}
    };
    float val;
    int start, stop, diff;
    struct tms buffer;

    val = sec * (float) CLOCKS_PER_SEC / 10000;
    diff = (int) val;
    start = times(&buffer);
    stop = times(&buffer);

   /*   message((char*)"-----------  braittenberg koala detect goal -----------\n"); fonction inexistance / inutile */
    /* Initialise position at 0, 0 */
    /* en prendra les infos d'odometrie pour mettre a jours l'orientation */
    /*N.C:parm[0]=0;
       parm[1]=0;
       commande_koala(COM_G,parm,(char*)nothing); */
    robot_init_wheels_counter(robot_get_first_robot());
    while ((stop - start) < diff)
    {

        robot_get_ir(robot_get_first_robot(), IR);
        mot[0] = mot[1] = 0.;
        for (j = 0; j < 6; j++) /* 6 capteurs gauche et droite */
        {
            mot[0] += W[0][j] * IR[j] + W[0][j + 6] * IR[j + 8];
            mot[1] += W[1][j] * IR[j] + W[1][j + 6] * IR[j + 8];
        }

/*      for(i=0;i<16;i++)
	{
	        printf("Capteur[%d]=%f\n",i,IR[i]);

	  if (IR[i]>0.5)
	    IR_aff[i]=1;
	  else
	    IR_aff[i]=0;
	}
           printf("%d-%d-%d-%d----%d-%d-%d-%d\n",IR_aff[3],IR_aff[2],IR_aff[1],IR_aff[0],IR_aff[8],IR_aff[9],IR_aff[10],IR_aff[11]);
	      printf("|                |\n");
	      printf("|                |\n");
	      printf("%d                %d\n",IR_aff[4],IR_aff[12]);
	      printf("|                |\n");
	      printf("%d                %d\n",IR_aff[5],IR_aff[13]);
	      printf("|                |\n");
	      printf("|                |\n");
	      printf("%d---%d--------%d---%d\n",IR_aff[6],IR_aff[7],IR_aff[15],IR_aff[14]); */

        mot[0] += 1.;
        mot[1] += 1.;
/*      if (mot[0]<0.)
	{
	  mot[0]=0.;
	  mot[1]-=mot[0];
	}

      if (mot[1]<0.)
	{
	  mot[1]=0.;
	  mot[0]-=mot[1];
	}*/
        /*             printf("G=%f ### D=%f\n",mot[0],mot[1]); */

        param[0] = (int) (mot[0] * 60.);
        param[1] = (int) (mot[1] * 60.);
        robot_go_by_speed(robot_get_first_robot(), param[0], param[1]);
        /* commande_koala(COM_D, param, data); */
        stop = times(&buffer);

        if (MotivationStop())
        {
            trouve = 1;
            if (isequal(vigilence, 1.))
            {
                /*param[0]=param[1]=0;
                   commande_koala(COM_D, param, data); */
                robot_go_by_speed(robot_get_first_robot(), 0, 0);
                stop = start + diff + 10;
            }
            else
            {
                for (i = 0; i < nb_motivations; i++)
                    if (Niveau_ressources[i] < motivations[i].seuil_bas)
                    {
                        /*param[0]=param[1]=0;
                           commande_koala(COM_D, param, data); */
                        robot_go_by_speed(robot_get_first_robot(), 0, 0);
                        stop = start + diff + 10;

                        robot_content_montre();
                        break;
                    }
            }
        }
    }
/* param[0]=param[1]=0;
   commande_koala(COM_D, param, data);*/
    robot_go_by_speed(robot_get_first_robot(), 0, 0);
    if (trouve)
        printf("\nMIAM MIAM!\n");
    /*   fflush(stdin);	getchar(); */
    /*    3-2-1-0-----8-9-10-11   */
    /*    |                   |   */
    /*    4       Avant      12   */
    /*    |                   |   */
    /*    |                   |   */
    /*    5                  13   */
    /*    |                   |   */
    /*    |                   |   */
    /*    6-7----------15----14   */

    /* Mise a jour de l'orientation */
    robot_commande_koala(robot_get_first_robot(), COM_H, nothing, data);

    sscanf(data, "%d,%d", d, d + 1);

    Angle = ((float) d[0] - d[1]) / 2.;
    Angle = Angle / (float) robot_get_first_robot()->angle90 * 90.;

    Orientation = AngleTrigo(Orientation + Angle);
}

/*====================================================*/
/* function_robot_explore :                           */
/*____________________________________________________*/
/*   Tire une direction et une longueur aleatoires de */
/* deplacement du robot et effectue ce deplacement.   */
/* Les gros changements de direction sont privilegies */
/* et les longueurs de deplacement y sont asservies.  */
/* Un changement de direction grand entraine une long */
/* ueur plutot courte, et inversement.                */
/* Les parametres de reglage sont :                   */
/* ORIENTATION_RANGE : Etendue du changmnt de directn */
/* SIGMA_ORIENT : + c'est grand, + on tirera de grands*/
/*                changements de direction            */
/* SIGMA_LONG : + c'est gd,+ on tire de gds deplcmnts */
/* Longueur_mini : duree en secondes du pas minimum   */
/* pasLong : duree en secondes du deplacement maximal */
/*====================================================*/
void function_robot_explore()
{
    float hasard;
    float dOrientation = 0.;
    float Long = 0.;
    float prov;

    do
    {
        hasard = (float) drand48();
        dOrientation = hasard * 2. - 1.;
        dOrientation = ORIENTATION_RANGE * dOrientation;
        hasard = (float) drand48();
    }
    while (!
           ((float) exp(-(dOrientation * dOrientation) / SIGMA_ORIENT) <
            hasard));

    printf("dOrientation=%f\n", dOrientation);
    if (dOrientation < 0.)
    {
        dOrientation = -dOrientation;
        /*if (!trouve) */
        robot_turn_angle(robot_get_first_robot(), -1. * dOrientation);
    }
    else
    {
        /* if (!trouve) */
        robot_turn_angle(robot_get_first_robot(), dOrientation);
    }
    fprintf(sortie_neurones, "\nExploration du robot...Nouvelle orientation : %f", compass_read(compass_get_first_compass()));

    do
    {
        hasard = (float) drand48();
        Long = pasLong * hasard;
        hasard = (float) drand48();
        prov = -(Long * Long) * dOrientation / SIGMA_LONG;
        if (prov < -600)
            prov = 0.;
        else
            prov = (float) exp(prov);   /* depassement de capacite de la fonction exp() */
        /*printf("exp=%f ?>? hasard=%f\n",(float)exp(-(Long*Long)*dOrientation/SIGMA_LONG), hasard); */
    }
    while (prov <= hasard);

    printf("Long=%f\n\n", Long);
    fprintf(sortie_neurones, " Longueur du deplacement : %f secondes\n", Long + Longueur_mini);
    braittenberg_koala_detect_goal(Long + Longueur_mini);
/* braittenberg_koala_follow_detect_goal(Long+Longueur_mini);*/
}
