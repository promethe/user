/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
___NO_COMMENT___
___NO_SVN___

\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description: 

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
void function_spice(numero)
     int numero;
{
    int deb_s, gpe_entree1 = 0, gpe_entree2 = 0;
    int l1 = -1, l2 = -1, i, n, nofind, inter;
    int taille_groupe_s, taille_groupe_s2, increment_s;
    type_coeff *coeff;
    int Ei;
    float prov, prod, max;
    int posmax;


    printf("\nFonction spice\n--------------------\n");

    nofind = 1;
    for (i = 0; i < nbre_liaison; i++)
    {
        if (liaison[i].arrivee == numero)
        {
            if (nofind)
            {
                gpe_entree1 = liaison[i].depart;
                l1 = i;
                nofind = 0;
            }
            else
            {
                if (gpe_entree1 != liaison[i].depart)
                {
                    gpe_entree2 = liaison[i].depart;
                    l2 = i;
                }
            }
        }
    }


    if (l2 != -1)
    {
        /* si les liaisons 1 et 2 sont inversees */
        if (strcmp(def_groupe[liaison[l1].depart].nom, "f_robot_limbic") == 0)
        {
            inter = gpe_entree1;
            gpe_entree1 = gpe_entree2;
            gpe_entree2 = inter;
            inter = l1;
            l1 = l2;
            l2 = inter;
        }
    }
    printf("\nspicedebug : gpe1: %d gpe2: %d \n", gpe_entree1, gpe_entree2);

    taille_groupe_s = def_groupe[numero].nbre;
    taille_groupe_s2 =  def_groupe[numero].taillex * def_groupe[numero].tailley;
    increment_s = taille_groupe_s / taille_groupe_s2;
    deb_s = def_groupe[numero].premier_ele;

    n = deb_s;

    if (vigilence > 0.5)        /* Apprentissage des liens motivationnels */
    {
        while (n < (deb_s + taille_groupe_s2 * increment_s))
        {
            printf("\nneurone[%d].groupe=%d\n", n, neurone[n].groupe);
            coeff = neurone[n].coeff;
            Ei = 0;

            printf("\n");

            while (coeff != NULL)
            {
                Ei += (neurone[coeff->entree].s2 > 0.9)
                    && (neurone[coeff->entree].groupe == gpe_entree1);

                printf("Ei=%d | n.s %f n.s1 %f n.s2 %f", Ei,
                       neurone[coeff->entree].s, neurone[coeff->entree].s1,
                       neurone[coeff->entree].s2);

                coeff = coeff->s;
            }
            printf("\n");


            coeff = neurone[n].coeff;
            while (coeff != NULL)
            {
                if (Ei && (coeff->type % 2 == 0)
                    && (neurone[coeff->entree].s2 > 0.9)
                    && (neurone[coeff->entree].groupe == gpe_entree2))
                {
                    coeff->val = 1.0;
                    printf("NONODEBUG : et un avec le neurone[%d]\n",
                           coeff->entree);
                }
                else if ((coeff->type % 2 == 0) && isdiff(coeff->val, 1.0)
                         && (neurone[coeff->entree].groupe == gpe_entree2))
                    coeff->val = 0.;
                coeff = coeff->s;
            }
            n += increment_s;
        }
    }

    max = -9999;
    posmax = -1;
    for (i = deb_s; i < deb_s + increment_s * taille_groupe_s2;
         i += increment_s)
    {
        printf("\nneur[%d]\n", i);
        coeff = neurone[i].coeff;
        prod = 0.;
        prov = 0.;
        while (coeff != NULL)
        {
            if (gpe_entree1 == neurone[coeff->entree].groupe)
            {
                prod = neurone[coeff->entree].s * coeff->val;
            }
            else
                prov += coeff->val * neurone[coeff->entree].s;
            coeff = coeff->s;
        }
        neurone[i].s = neurone[i].s1 = neurone[i].s2 = prov * prod;
        printf("neur[%d].s=%f", i, neurone[i].s);
        if (max < neurone[i].s)
        {
            max = neurone[i].s;
            posmax = i;
        }
    }
    if (posmax == -1)
    {
        printf("\nERREUR dans function_spice, exiting...\n");
        abort();
    }
    for (i = deb_s; i < deb_s + increment_s * taille_groupe_s2;
         i += increment_s)
    {
        if (i != posmax)
            neurone[i].s2 = 0.;
        else
            neurone[i].s2 = 1.0;
    }

}
