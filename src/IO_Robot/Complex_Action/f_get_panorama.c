/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup f_get_panorama f_get_panorama
   \ingroup libIO_Robot

   \brief This fonction take a panorama without moving with the koala only, and a pan-camera

   \deprecated
   Do not seem to ne usable anymore due to changes in the libhardware.

   \file
   \ingroup f_get_panorama
   \brief This fonction take a panorama without moving with the koala only, and a pan-camera

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 01/09/2004


   Description:
   This fonction take a panorama without moving with the koala only,
   and a pan-camera


   Local variables:
   -int flag_aff_pano


   Internal Tools:
   -InitPanorama()
   -InitPanoramaInTest()
   -GetPanorama()
   -GetPanoramaInTest()

   External Tools:
   -tools/IO_Robot/Com_Koala/robot_go_by_speed(robot_get_first_robot(),)


   http://www.doxygen.org
************************************************************/
/*#define DEBUG*/
#include <stdio.h>
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <Typedef/boolean.h>
#include <Struct/prom_images_struct.h>
#include <Macro/IO_Robot/macro.h>
#include "tools/include/local_var.h"
#include "tools/include/macro.h"
#include <libhardware.h>
#include <public_tools/Vision.h>

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>

#include "tools/include/InitPanoramaInTest.h"
#include "tools/include/GetPanoramaInTest.h"

/** ***********************************************************
\file  f_read_sequence_conditionnal.c
\brief

Author: ????
Created: 28/01/2004
Modified:
- author: P. Gaussier

Description: Fonction qui devait etre utilisee avec le Koala pour prendre un panorama en faisant tourner le robot. Cette fonction pourrait encore servir avec le roomba mais elle est vraiment tres ad hoc ! Attention la fonction ne marche plus en l'état.


Liens:


Macro:

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:

Links:

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

void GetPanorama(prom_images_struct * image_in, boolean take_photo);
void InitPanoramaInTest(prom_images_struct * image_in);
void InitPanorama(prom_images_struct * image_in);

void InitPanorama(prom_images_struct * image_in)
{
   int xmax=-1, ymax=-1;
   float XmaxPano;

   EXIT_ON_ERROR("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ", __FUNCTION__);


   /*xmax=get_width();
     ymax=get_height();
   */
   AnglePano = 250;
   VueAngle = 77;
   XmaxPano = (float) xmax *AnglePano / VueAngle;
   xmax_ng = (int) XmaxPano;
   ymax_ng = ymax;

   /*im_ng = (unsigned char*) malloc(xmax_ng*ymax_ng); */

   (*image_in).image_number = 2;
   /*(*image_in).nb_band=get_nbands(); */
   (*image_in).sx = xmax_ng;
   (*image_in).sy = ymax_ng;
   (*image_in).images_table[0] = (unsigned char *) malloc(xmax_ng * ymax_ng * 3);  /* image panoramique */
   (*image_in).images_table[1] = (unsigned char *) malloc(xmax * ymax * 3);    /* image temporaire */

   if ((*image_in).images_table[1] == NULL || (*image_in).images_table[0] == NULL)
   {
      EXIT_ON_ERROR("InitPano: ALLOCATION IMPOSSIBLE ...! \n");
   }
   cprints("xmax:%d ymax:%d xmax_ng:%d ymax_ng:%d, nb_band:  %d \n", xmax, ymax, xmax_ng, ymax_ng, (*image_in).nb_band);
}

void InitPanoramaInTest(prom_images_struct * image_in)
{
   int xmax=-1, ymax=-1;
   float XmaxPano;
   EXIT_ON_ERROR("\n %s ne marche plus: revoir pour la faire marcher avec la nouvelle libhardware !\n ", __FUNCTION__);

   /*  xmax=get_width();
       ymax=get_height();
   */
   AnglePano = 300;
   VueAngle = 77;
   XmaxPano = (float) xmax *AnglePano / VueAngle;
   xmax_ng = (int) XmaxPano;
   ymax_ng = ymax;
   /*im_ng = (unsigned char*) malloc(xmax_ng*ymax_ng); */
   (*image_in).image_number = 3;
   /*(*image_in).nb_band=get_nbands(); */
   (*image_in).sx = xmax_ng;
   (*image_in).sy = ymax_ng;
   (*image_in).images_table[0] = (unsigned char *) malloc(xmax_ng * ymax_ng * 3);  /* image panoramique */
   (*image_in).images_table[1] = (unsigned char *) malloc(xmax_ng * ymax_ng * 3);  /* image panoramique */
   (*image_in).images_table[2] = (unsigned char *) malloc(xmax * ymax * 3);    /* image temporaire */
   if ((*image_in).images_table[1] == NULL  || (*image_in).images_table[0] == NULL || (*image_in).images_table[2] == NULL)
   {
      EXIT_ON_ERROR("InitPano: ALLOCATION IMPOSSIBLE ...! \n");
   }
   cprints("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
   cprints("xmax:%d ymax:%d xmax_ng:%d ymax_ng:%d, nb_band:  %d \n", xmax, ymax, xmax_ng, ymax_ng, (*image_in).nb_band);
}

void GetPanorama(prom_images_struct * image_in, boolean take_photo)
{
   int xmax = 384, ymax = 288, startx;
   int x, y, val, pause, k = 0;
   int decx = 0;
   char preffix[32];
   /*  unsigned char	*retine;*/
   float recouvr;
   float XmaxPano;

   prom_images_struct arf;
   recouvr = (float) (NbrIm - AnglePano / VueAngle) * xmax / (NbrIm - 1);
   XmaxPano = (float) xmax *AnglePano / VueAngle;
   startx = (int) (recouvr / 2 + 0.5);
   if (image_in == NULL)
   {
      EXIT_ON_ERROR("NULL erreur dans ImageIn - GetPano\n");
   }
   /*pan(get_camstart()); */
   for (pause = 0; pause < (IMAGE_WAIT * 4); pause++)
   {
      /*CaptureImage( (*image_in).images_table[1]); */
   }
   if (take_photo)
   {
      arf.sx = xmax;
      arf.sy = ymax;
      arf.nb_band = 1;
      arf.image_number = 1;
      arf.images_table[0] = (*image_in).images_table[1];
      save_png_to_disk((char*)"pano0.png", arf, 0);
   }
   /*construction de l'image panoramique */
   for (y = 0; y < ymax; y++)
      for (x = 0; x < xmax; x++)
         (*image_in).images_table[0][y * xmax_ng + x] =
            (*image_in).images_table[1][y * xmax + x];

   for (k = 1, decx = xmax - (int) (recouvr + 0.5); k < NbrIm;
         k++, decx = (int) ((float) k * ((float) xmax - recouvr + 0.5)))
   {
      /*      pan(get_camstart()+ (k*get_camstop())/(NbrIm-1));*/
      for (pause = 0; pause < IMAGE_WAIT; pause++)
      {
         /*CaptureImage( (*image_in).images_table[1]); */
      }
      if (take_photo)
      {
         strcpy(preffix, "pano");
         sprintf(preffix + 4, "%d.png", k);
         arf.images_table[0] = (*image_in).images_table[1];
         save_png_to_disk(preffix, arf, 0);
      }
      /* construction de l'image panoramique */
      for (y = 0; y < (ymax - 1); y++)
      {
         /* moyennage des deux images sur les bords */
         val =(223 * (*image_in).images_table[0][y * xmax_ng + startx - 4 + decx] + 32 * (*image_in).images_table[1][y * xmax + startx - 4]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx - 4 + decx] = val;
         val =(191 * (*image_in).images_table[0][y * xmax_ng + startx - 3 + decx] + 64 * (*image_in).images_table[1][y * xmax + startx - 3]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx - 3 + decx] = val;
         val = (160 * (*image_in).images_table[0][y * xmax_ng + startx - 2 + decx] + 95 * (*image_in).images_table[1][y * xmax + startx -  2]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx - 2 + decx] = val;
         val =(128 *(*image_in).images_table[0][y * xmax_ng + startx - 1 + decx] + 127 * (*image_in).images_table[1][y * xmax + startx - 1]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx - 1 + decx] = val;

         val = (127 * (*image_in).images_table[0][y * xmax_ng + startx + decx] + 128 * (*image_in).images_table[1][y * xmax + startx]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx + decx] = val;
         val = (95 * (*image_in).images_table[0][y * xmax_ng + startx + 1 + decx] + 160 * (*image_in).images_table[1][y * xmax + startx + 1]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx + 1 + decx] = val;
         val = (64 * (*image_in).images_table[0][y * xmax_ng + startx + 2 + decx] + 191 * (*image_in).images_table[1][y * xmax + startx +  2]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx + 2 + decx] = val;
         val = (32 * (*image_in).images_table[0][y * xmax_ng + startx + 3 + decx] + 223 * (*image_in).images_table[1][y * xmax + startx +  3]) / 255;
         (*image_in).images_table[0][y * xmax_ng + startx + 3 + decx] = val;

         for (x = startx + 4; (x < xmax) && (x < (xmax_ng - decx)); x++)
            (*image_in).images_table[0][y * xmax_ng + x + decx] =
               (*image_in).images_table[1][y * xmax + x];
      }
   }

   /*pan((get_camstart()+get_camstop())/2); */

   if (take_photo)
   {
      arf.sx = xmax_ng;
      arf.sy = ymax;
      arf.nb_band = 1;
      arf.image_number = 1;
      arf.images_table[0] = (*image_in).images_table[0];
      save_png_to_disk((char*)"pano.png", arf, 0);
   }
}

void function_get_panorama(int gpe_sortie)
{
   float azim = 0, azim_for_pano = 0.71;
   int gpe_compass = 0;
   static int first = 1;
   prom_images_struct *p_image;
   int l /*,i,j */ ;
   boolean take_photo = false;
   boolean execution_in_test = false;
   boolean avance_after_pano = false;
   boolean use_compass = false;
   boolean rotate = false;
   char param_link[32];
   static int pair = 0;


   static int parm[2];
   char *nothing = 0;          /* no data returned */
   dprints("~~~~~~~~~~~debut %s\n", __FUNCTION__);

   p_image = (prom_images_struct *) def_groupe[gpe_sortie].ext;
   if (p_image == NULL)
   {
      /*printf("RAOUL: p_image est defini une fois\n"); */
      p_image = (prom_images_struct *) malloc(sizeof(prom_images_struct));    /*allocation du pointeur */
      if ((p_image) == NULL)
      {
         EXIT_ON_ERROR("function_get_panorama_alternate: ALLOCATION IMPOSSIBLE ...! \n");
      }
      first = 1;
   }

   l = find_input_link(gpe_sortie, 0);
   if (l == 0)
   {
      cprints("\nRAOULDEBUG : Pas trouve %s (!!!), exiting...\n", __FUNCTION__);
      abort();
   }


   if (prom_getopt(liaison[l].nom, "-r", param_link) >= 1)
   {
      rotate = true;
   }
   if (prom_getopt(liaison[l].nom, "-c", param_link) >= 1)
   {
      gpe_compass = liaison[l].depart;
      use_compass = true;
   }
   if (prom_getopt(liaison[l].nom, "-d", param_link) >= 1)
   {
      avance_after_pano = true;
   }
   if (prom_getopt(liaison[l].nom, "-p", param_link) >= 1)
   {
      take_photo = true;
   }
   if (prom_getopt(liaison[l].nom, "-x", param_link) >= 1)
   {
      execution_in_test = true;
   }

   cprints("option de %s:compass=%d, take_photo=%d, avance after pano= %d, test=%d\n", __FUNCTION__, use_compass, take_photo, avance_after_pano,execution_in_test);

   if (first)
   {
      if (!execution_in_test)
         InitPanorama(p_image);
      first = 0;
   }

   /*if(avance_after_pano)
     {robot_turn_angle(robot_get_first_robot(),60.);while(robot_precise_last_target_reached(robot_get_first_robot())==0){}} */


   if (rotate)
   {
      if (pair == 1)      robot_turn_angle(robot_get_first_robot(), 180);
      else if (pair == 0) robot_turn_angle(robot_get_first_robot(), -180);
      pair = pair + 1;
      if (pair == 2) pair = 0;

   }
   if (use_compass)
   {
      azim = neurone[def_groupe[gpe_compass].premier_ele].s;

      cprints("azim avant pano= %f->%f degres\n", azim, azim * 360 - 180);
      cprints("azim du pano= %f->%f\n", azim_for_pano,  azim_for_pano * 360 - 180);

      if (azim - azim_for_pano > 0.5)         azim = azim - 1;
      else if (azim - azim_for_pano < -0.5)   azim = azim + 1;

      robot_turn_angle(robot_get_first_robot(), azim - azim_for_pano * 360);

      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
      }
   }

   if (!execution_in_test)
      GetPanorama(p_image, take_photo);
   else
   {
      robot_turn_angle(robot_get_first_robot(), -90.);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }
      parm[0] = 0;
      parm[1] = 0;
      robot_commande_koala(robot_get_first_robot(), COM_G, parm, nothing);
      /*wait_stop(); */
      parm[0] = (int) -2000;
      parm[1] = (int) -2000;
      robot_commande_koala(robot_get_first_robot(), COM_C, parm, nothing);

      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }

      GetPanorama(p_image, take_photo);

      parm[0] = 0;
      parm[1] = 0;
      robot_commande_koala(robot_get_first_robot(), COM_G, parm, nothing);


      parm[0] = (int) 2000;
      parm[1] = (int) 2000;
      robot_commande_koala(robot_get_first_robot(), COM_C, parm, nothing);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }
      robot_turn_angle(robot_get_first_robot(), 90.);
      while (robot_precise_last_target_reached(robot_get_first_robot()) ==
             0)
      {
         ;
      }
      robot_turn_angle(robot_get_first_robot(), 90.);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }
      parm[0] = 0;
      parm[1] = 0;
      robot_commande_koala(robot_get_first_robot(), COM_G, parm, nothing);
      parm[0] = (int) -2000;
      parm[1] = (int) -2000;
      robot_commande_koala(robot_get_first_robot(), COM_C, parm, nothing);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }

      GetPanorama(p_image, take_photo);

      parm[0] = 0;
      parm[1] = 0;
      robot_commande_koala(robot_get_first_robot(), COM_G, parm, nothing);
      parm[0] = (int) 2000;
      parm[1] = (int) 2000;
      robot_commande_koala(robot_get_first_robot(), COM_C, parm, nothing);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }

      robot_turn_angle(robot_get_first_robot(), -90.);
      while (robot_precise_last_target_reached(robot_get_first_robot()) == 0)
      {
         ;
      }
   }


   if (avance_after_pano)  robot_go_by_speed(robot_get_first_robot(), 10, 10);

   flag_aff_pano = 1;
   def_groupe[gpe_sortie].ext = (void *) (p_image);

   dprints("=============== fin %s\n", __FUNCTION__);
}



void function_paste_image(int gpe)
{
   int l, i, j;
   prom_images_struct *pt = NULL, *pt_ext_entree0 = NULL, *pt_ext_entree1 =
                               NULL;
   pt = (prom_images_struct *) def_groupe[gpe].ext;
   if (pt == NULL)
   {
      l = find_input_link(gpe, 0);
      /*if(strcmp (def_groupe[liaison[l].depart].nom,"f_get_panorama")==0)
        {
        printf("le groupe d'netre de f_paste_panorama n'est f_get_panorama\n");
        exit(0);
        } */
      pt = (prom_images_struct *) malloc(sizeof(prom_images_struct)); /*allocation du pointeur */
      if ((pt) == NULL)
      {
         EXIT_ON_ERROR("function_get_panorama_alternate: ALLOCATION IMPOSSIBLE ...! \n");
      }
      pt_ext_entree1 = (prom_images_struct *) def_groupe[liaison[l].depart].ext;
      l = find_input_link(gpe, 1);
      pt_ext_entree0 = (prom_images_struct *) def_groupe[liaison[l].depart].ext;
      pt = calloc_prom_image(1, 2 * pt_ext_entree0->sx - 120, pt_ext_entree0->sy, pt_ext_entree0->nb_band);
      def_groupe[gpe].ext = (prom_images_struct *) pt;
   }
   else
   {
      pt = (prom_images_struct *) def_groupe[gpe].ext;
      l = find_input_link(gpe, 0);
      pt_ext_entree1 = (prom_images_struct *) def_groupe[liaison[l].depart].ext;
      l = find_input_link(gpe, 1);
      pt_ext_entree0 =  (prom_images_struct *) def_groupe[liaison[l].depart].ext;

   }
   /*
     for(i=0;i<pt_ext_entree->sy;i++)
     {
     for(j=0; j<pt_ext_entree->sx-50;j++)
     {
     pt->images_table[0][j+i*pt->sx]=pt_ext_entree->images_table[0][j+i*xmax_ng];
     }
     }

     for(i=0;i<pt_ext_entree->sy;i++)
     {
     for(j=0; j<pt_ext_entree->sx-50;j++)
     {
     pt->images_table[0][j+pt_ext_entree->sx-50+i*pt->sx]=pt_ext_entree->images_table[1][j+50+i*xmax_ng];
     }
     } */
   for (i = 0; i < pt_ext_entree0->sy; i++)
   {
      for (j = 60; j < 1536; j++)
      {
         pt->images_table[0][(j - 60 + i * pt->sx) * 3]     =  pt_ext_entree0->images_table[0][(j + i * pt_ext_entree0->sx) * 3];
         pt->images_table[0][(j - 60 + i * pt->sx) * 3 + 1] =  pt_ext_entree0->images_table[0][(j + i * pt_ext_entree0->sx) *	3 + 1];
         pt->images_table[0][(j - 60 + i * pt->sx) * 3 + 2] =  pt_ext_entree0->images_table[0][(j + i * pt_ext_entree0->sx) *	3 + 2];
      }
   }

   for (i = 0; i < pt_ext_entree1->sy; i++)
   {
      for (j = 60; j < 1536; j++)
      {

         pt->images_table[0][(j + 1536 - 120 + i * pt->sx) * 3]     = pt_ext_entree1->images_table[0][(j + i * pt_ext_entree1->sx) * 3];
         pt->images_table[0][(j + 1536 - 120 + i * pt->sx) * 3 + 1] = pt_ext_entree1->images_table[0][(j + i * pt_ext_entree1->sx) *	3 + 1];
         pt->images_table[0][(j + 1536 - 120 + i * pt->sx) * 3 + 2] = pt_ext_entree1->images_table[0][(j + i * pt_ext_entree1->sx) *	3 + 2];
      }
   }

   /*for(i=0;i<pt_ext_entree0->sy;i++)
     {
     for(j=175; j<1503;j++)
     {
     pt->images_table[0][(j-175+i*pt->sx)*3]=pt_ext_entree0->images_table[0][(j+i*pt_ext_entree0->sx)*3];
     pt->images_table[0][(j-175+i*pt->sx)*3+1]=pt_ext_entree0->images_table[0][(j+i*pt_ext_entree0->sx)*3+1];
     pt->images_table[0][(j-175+i*pt->sx)*3+2]=pt_ext_entree0->images_table[0][(j+i*pt_ext_entree0->sx)*3+2];
     }
     }

     for(i=0;i<pt_ext_entree0->sy;i++)
     {
     for(j=159; j<1474;j++)
     {

     pt->images_table[0][(j-159+1503-175+i*pt->sx)*3]=pt_ext_entree1->images_table[0][(j+i*pt_ext_entree0->sx )*3];
     pt->images_table[0][(j-159+1503-175+i*pt->sx)*3+1]=pt_ext_entree1->images_table[0][(j+i*pt_ext_entree0->sx)*3+1];
     pt->images_table[0][(j-159+1503-175+i*pt->sx)*3+2]=pt_ext_entree1->images_table[0][(j+i*pt_ext_entree0->sx)*3+2];
     }
     } */
}
