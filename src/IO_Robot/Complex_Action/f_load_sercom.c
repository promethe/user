/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/**
 \file
 \brief

 Author: xxxxxxxx
 Created: XX/XX/XXXX
 Modified:
 - author: C.Giovannangeli
 - description: specific file creation
 - date: 11/08/2004

 Theoritical description:
 - \f$  LaTeX equation: none \f$

 Description:

 Macro:
 -none

 Local variables:
 -none

 Global variables:
 -int USE_HEAD
 -int USE_CAM
 -int USE_KATANA
 -boolean emission_robot

 Internal Tools:
 -init_pan()
 -init_arm()

 External Tools:
 -tools/IO_Robot/Pan_Tilt/init_pan_tilt()
 -tools/IO_Robot/Com_Koala/GoToLeftRightUncond()
 -tools/IO_Robot/Serial/serial_open()

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Global_Var/IO_Robot.h>
#include <Kernel_Function/find_input_link.h>
#include <libhardware.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

void init_pid(void);
void load_sercom( /*char *name */);

void function_load_sercom(int numero)
{
   /*serial_open(PORT); */
   int gpe_entree1 = -1, deb_e = 1;
   gpe_entree1 = liaison[find_input_link(numero, 0)].depart;
   deb_e = def_groupe[gpe_entree1].premier_ele;
   if (isequal(neurone[deb_e].s2, 1.)) load_sercom(robot_get_first_robot());
   else printf("no sercom_loaded\n");
   init_pid();
}

void read_serial(char *retour)
{
   int serialport = serial_get_serial_by_name(robot_get_first_robot()->port)->desc;
   int n, l, m;
   char lu;
   l = m = 0;
   do
   {
      n = read(serialport, &lu, 1);
      if (n)
      {
         retour[l] = lu;
         l++;
      }
      else
      {
         lu = '\0';
         m++;
      }
   }
   while ((lu != 10) && (l < 100) && (m < 10000));
   printf("retour:%s\n", retour);
}

#define STRING_LENGHT 256
void load_sercom( /*char *name */)
{
   int n, j = 0, l = 0;
   int ret;
   char retour[STRING_LENGHT];
   char cmd[STRING_LENGHT];
   int serialport = serial_get_serial_by_name(robot_get_first_robot()->port)->desc;

   strcpy(cmd, "run sloader  ");

   printf("Power up!\n");
   ret = scanf("%d", &j);
   tcflush(serialport, TCIOFLUSH);
   read_serial(retour);
   /* scanf("%d",&j);*/
   printf("c!\n");
   read_serial(retour);
   /*scanf("%d",&j);*/
   printf("ca!\n");
   read_serial(retour);
   /*scanf("%d",&j);*/
   printf("RUN!\n");

   n = strlen(cmd);
   cmd[n] = 0x0D;
   /* cmd[n + 1] = 0;*/
   read_serial(retour);
   /*scanf("%d",&j);*/
   printf("RUN0!\n");
   /*read_serial(retour);
    scanf("%d",&j);
    */
   /*tcdrain(serialport);*/
   read_serial(retour);
   /*scanf("%d",&j);*/
   printf("RUN!\n");
   printf("cmd!\n");
   tcflush(serialport, TCIOFLUSH);
   ret = write(serialport, cmd, n);
   printf("cmdok!\n");
   /*read_serial(retour);*/
   /*scanf("%d",&j);*/
   printf("RUN!\n");
   printf("cat!\n");
   ret = system("cat sercom_fred.s37 > /dev/ttyS0");
   printf("catok!\n");
   /*  scanf("%d",&j);*/
   tcdrain(serialport);
   read_serial(retour);
   /*scanf("%d",&j);*/
   printf("RUN!\n");

   n = l;

   printf("Sercom successfully loaded!\n");
   (void) ret;
}

void init_pid(void)
{
   static char *nothing = 0;
   static int data[4];
   static int dataa[3];
   static int datab[3];

   data[0] = 10;
   data[1] = 32;
   data[2] = 10;
   data[3] = 32;
   dataa[0] = 200;
   dataa[1] = 2;
   dataa[2] = 200;
   datab[0] = 500;
   datab[1] = 400;
   datab[2] = 50;
   
   dprints("enter pid J \n");
   robot_commande_koala(robot_get_first_robot(), COM_J, data, nothing);
   dprints("pid J ok\n");
   dprints("pid A ok\n");
   robot_commande_koala(robot_get_first_robot(), COM_A, datab, nothing);
   robot_commande_koala(robot_get_first_robot(), COM_F, dataa, nothing);
   dprints("profil F ok\n");
}
