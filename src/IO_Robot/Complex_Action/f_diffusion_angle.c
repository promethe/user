/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_diffusion_angle.c
\brief

Author: Raoul
Created: 20/04/2000
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 01/09/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
	Cette fonction impose la diffusion de l'angle de theta_generate
	entre 0 et 1 soit entre 0 et 180 degres.

	Option et valeur par defaut
	-d1  -dvaleur_de_diffusion_statique
	-c   option sur le lien qui vient d'un groupe de neuone (si on veut gerer la diffusion va l'activite d'un groupe)

	en general, on met soit -dx, soit -c (si on passe les deux, -c est prioritaire).
Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-Kernel_Function/prom_getopt()
-Kernel_Function/find_input_link()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <Kernel_Function/prom_getopt.h>
#include <Kernel_Function/find_input_link.h>
 /*#define DEBUG */
typedef struct MyData_f_diffusion_angle
{
    float diffusion_angle;
    int gpeCell;
    int gpeVigilence;
    int option_gpeCell;
    float *mem_actmax;
} MyData_f_diffusion_angle;

void function_diffusion_angle(int i)
{
    int j = 0, l, gpeCell = -2,gpeVigilence = -2;
    float diffusion_angle = 0,  actmax, variation = 0,vigilence_local=0.;
    int posmax;
    float myeps = 0.05, seuil_max_haut = 0.8, seuil_max_bas = 0.7;
    MyData_f_diffusion_angle *my_data;
    char param_diffusion[20], param_gpeCell[1];
    int dt = 4;
    float *mem_actmax;
    int option_gpeCell;
    int first_tour = 0;

    if (def_groupe[i].data == NULL)
    {
        l = find_input_link(i, j);
	while(l!=-1)
	{	/*-d option pour diffusion statique ex: -d45 par defaut*/
		if ((prom_getopt(liaison[l].nom, "d", param_diffusion) == 2))
			diffusion_angle = atof(param_diffusion);
		else if ((prom_getopt(liaison[l + 1].nom, "d", param_diffusion) == 2))
			diffusion_angle = atof(param_diffusion);
		/*-c option pour regler la diffusion en fonction de l'activite des cellules du groupe d'entree*/
		if (prom_getopt(liaison[l].nom, "c", param_gpeCell) == 2)
			gpeCell = liaison[l].depart;
		if (prom_getopt(liaison[l].nom, "v", param_gpeCell) >= 1)
			gpeVigilence = liaison[l].depart;
		if (prom_getopt(liaison[l + 1].nom, "c", param_gpeCell) == 2)
			gpeCell = liaison[l + 1].depart;
		j++;
		l = find_input_link(i, j);
	}
        mem_actmax = malloc(sizeof(float) * dt);
        for (j = 0; j < dt; j++)
            mem_actmax[j] = 2.0;

        my_data = malloc(sizeof(MyData_f_diffusion_angle));

        my_data->diffusion_angle = diffusion_angle;
        my_data->gpeCell = gpeCell;
        my_data->gpeVigilence = gpeVigilence;
        my_data->option_gpeCell = option_gpeCell = atoi(param_gpeCell);
        my_data->mem_actmax = mem_actmax;
        def_groupe[i].data = (void *) my_data;

        first_tour = 1;
    }
    else
    {
        my_data = (MyData_f_diffusion_angle *) def_groupe[i].data;
        gpeCell = my_data->gpeCell;
        gpeVigilence = my_data->gpeVigilence;
        diffusion_angle = my_data->diffusion_angle;
        mem_actmax = my_data->mem_actmax;
        option_gpeCell = my_data->option_gpeCell;
     }

    if (gpeCell != -2)
    {


        posmax = -1;
        actmax = -1;
        /*si la diffusion est gere par l'activite du groupe de cell d'entree */
        /*on recherche le max */
        for (j = def_groupe[gpeCell].premier_ele;
             j < def_groupe[gpeCell].premier_ele + def_groupe[gpeCell].nbre;
             j++)
        {
            if (neurone[j].s > actmax)
            {
                actmax = neurone[j].s;
                posmax = j;
            }
        }
        if (posmax == -1)
        {
            printf("error dans %s, aucun neurone d'entree actif\n",
                   __FUNCTION__);
            exit(0);
        }



        if (option_gpeCell == 0)
        {
            diffusion_angle = 1. - actmax;

        }
        else if (option_gpeCell == 1)   /*option_seuil */
        {
            if (actmax >= seuil_max_haut)
                diffusion_angle =
                    neurone[def_groupe[i].premier_ele].s - myeps;
            if (actmax <= seuil_max_bas)
                diffusion_angle =
                    neurone[def_groupe[i].premier_ele].s + myeps;
        }
        else if (option_gpeCell == 5)
        {
            posmax = -1;
            actmax = -1;
            /*si la diffusion est gere par l'activite du groupe de cell d'entree */
            /*on recherche le max */
            for (j = def_groupe[gpeCell].premier_ele;
                 j <
                 def_groupe[gpeCell].premier_ele + def_groupe[gpeCell].nbre;
                 j++)
            {
                if (neurone[j].s > actmax)
                {
                    actmax = neurone[j].s;
                    posmax = j;
                }
            }
            if (posmax == -1)
            {
                printf("error dans %s, aucun neurone d'entree actif\n",
                       __FUNCTION__);
                exit(0);
            }

            for (j = 0; j < dt - 1; j++)
            {
                mem_actmax[j] = mem_actmax[j + 1];
            }
            mem_actmax[dt - 1] = actmax;

            variation = mem_actmax[dt - 1] - mem_actmax[0];
            variation = variation / (dt - 2);
            if (actmax > 0.65)
                diffusion_angle = diffusion_angle - 0.1;
            else if (actmax < 0.35)
                diffusion_angle = diffusion_angle + 0.1;
            else
                diffusion_angle = diffusion_angle - variation * eps;

        }
        if (first_tour == 1)
            diffusion_angle = 1.0;

        if (diffusion_angle >= 1)
            diffusion_angle = 1;
        else if (diffusion_angle <= 0.05)
            diffusion_angle = 0.05;

    }
    if(gpeVigilence!=-2)
		vigilence_local=neurone[def_groupe[gpeVigilence].premier_ele].s1;
    else
		vigilence_local=vigilence;
/*    printf("vigilence local %f par %d\n",vigilence_local,gpeVigilence);*/

    if (vigilence_local >= 0.99)
        diffusion_angle = 0.;

#ifdef DEBUG
    printf("param diffusion %s=%f, var = %f\n", __FUNCTION__, diffusion_angle,
           variation);
#endif
    neurone[def_groupe[i].premier_ele].s1 =
        neurone[def_groupe[i].premier_ele].s2 =
        neurone[def_groupe[i].premier_ele].s = diffusion_angle;

}
