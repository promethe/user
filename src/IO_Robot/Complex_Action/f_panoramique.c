/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief

Author: Olivier LEDOUX
Created: Juin 2002
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: nov 2004

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
 *  \brief this group converted panoramic (360�) images into pan images
 *
 * This file contains only the code of this group for Promethee.
 * This one receives panoramic (circular view) images in the ext
 * extension of the previous group and returns pan images (classical
 * view) into its ext extension.
 * The images have the same number of color bands.

*!function_panoramique
 *
 * The box code
 * imperative: only one groupe in input
 * in: a group with a prom_images_struct structure in its .ext
 *     extension
 * out: the .ext structure of our group is filled with a new
 *      prom_images_struct structure
 * link: commands
 *         -xcmin corresponds to the xCircleMin variable
 *         -xcmax corresponds to the xCircleMax variable
 *         -ycmin corresponds to the yCircleMin variable
 *         -ycmax corresponds to the yCircleMax variable
 *         -cpl corresponds to the circleProjL variable
 *         -cph corresponds to the circleProjH variable
 *         -ca corresponds to the 'A' coefficient
 *         -cb corresponds to the 'B' coefficient
 *         -cc corresponds to the 'C' coefficient
 * note: if the expected parameters aren't on the link, the box
 *       ask user to print them manually
 *
 * update September 2003 Olivier Ledoux
 * new struct to save/restore need parameters between different calls

Macro:
-none

Local variables:
-int *panoramique_look_up

Global variables:
-none

Internal Tools:
-noneinit_look_up_table()
-init_rayon_table
-panoramique
-delete_rayon_table

External Tools:
-tools/Vision/calloc_prom_image()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <libx.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <Struct/prom_images_struct.h>

#include "tools/include/local_var.h"

#include <public_tools/Vision.h>
int *init_look_up_table(prom_images_struct * img_acq, prom_images_struct * img_out, float *rayon_table, int centerX, int centerY);
float *init_rayon_table(int hauteur, int rayon);
void panoramique(prom_images_struct * img_in, prom_images_struct * img_out, int numero, int *look_up);
void delete_rayon_table(float **rayon_table);

/*! creation of the look up table
 *
 * in:
 * img_acq=a prom_images_struct structure with acquired images
 * img_out=the output prom_images_struct structure
 * rayon_table=table of correspondence between the ray on the image and the real ray
 * centerX=abscissa of the center of the circle in the input image
 * centerY=orderly of the center of the circle in the input image
 *
 * out:
 * pointer on the look up table*
  */
int *init_look_up_table(prom_images_struct * img_acq, prom_images_struct * img_out, float *rayon_table, int centerX, int centerY)
{
   int acq_x, acq_y;           /*coordinated in the circular image */
   int out_x, out_y;           /* coordinated in the output image */
   float rayon;                /* distance from the center in the panoramic view C [0..r[ */
   float theta;                /* angle of rotation C [0..2Pi[ */
   int index;                  /* index in the look up table */
   int adresse;                /* address in the circular image (from its beginning) */
   float acq_xf, acq_yf, out_xf/*, out_yf*/, centerXf, centerYf;   /* Floating version of coordinates */
   float out_sxf/*, out_syf*/;     /* Floating version of dimensions */
   int *look_up_table;

   printf("start of init_look_up_table function\n");

   /* reserve the memory space for the look up table */
   look_up_table = (int *) malloc(img_out->sx * img_out->sy * sizeof(int));
   if (look_up_table == NULL)
      return (NULL);

   /*
    * completion of our look up table
    */

   /* Pass by floating for calculate */
   out_sxf = (float) img_out->sx;
   /*out_syf = (float) img_out->sy;*/
   centerXf = (float) centerX;
   centerYf = (float) centerY;

   /* calculation for every point of the output image, its address in the circular image */
   for (out_x = 0; out_x < img_out->sx; out_x++)
   {
      for (out_y = 0; out_y < img_out->sy; out_y++)
      {
         out_xf = (float) out_x;
         /*out_yf = (float) out_y;*/

         rayon = rayon_table[img_out->sy - out_y];   /* we take the ray */

         theta = out_xf * 2 * M_PI / out_sxf;
         acq_xf = centerXf - rayon * cos(theta);
         acq_yf = centerYf + rayon * sin(theta);

         /* go back to integer */
         acq_x = (int) (acq_xf + 0.5);   /* add 0.5 for the whole part */
         acq_y = (int) (acq_yf + 0.5);
         if (acq_x > img_acq->sx)
            acq_x = img_acq->sx;
         if (acq_y > img_acq->sy)
            acq_y = img_acq->sy;
         /* write in the look up table */
         index = out_y * img_out->sx + out_x;    /* index in the look up table */
         adresse = (acq_y * img_acq->sx + acq_x) * img_acq->nb_band; /* address int the circular image */
         look_up_table[index] = adresse;
      }
   }
   printf("end of init_look_up_table function\n");
   return (look_up_table);
}

float *init_rayon_table(int hauteur, int rayon)
{
   int i;
   float hauteurF = (float) hauteur;
   float rayonF = (float) rayon;
   float rayonEval;
   float *rayon_table;

   printf("start of init_rayon_table function\n");

   /* reserve the memory space for the look up table */
   rayon_table = (float *) malloc(hauteur * sizeof(float));
   if (rayon_table == NULL)  return (NULL);

   /*
    *  completion of our ray table
    */
   for (i = 0; i < hauteur; i++)
   {
      rayonEval = (float) i;
      rayonEval = (rayonEval / hauteurF); /* passe in an unitarian mark */
      rayonEval = rayonEval;  /*(coeffA*rayonEval+coeffB)+coeffC;  ax�+bx+c=x(ax+b)+c */
      /* if ( (rayonEval<0) || (rayonEval>1) ){
         rayonEval=0; * anti-bug !!! *
         printf ("init_rayon_table: rayonEval: %f \n",rayonEval);
         } */
      rayonEval = rayonEval * rayonF; /* Hand on to the scale in the acquired image */
      rayon_table[i] = rayonEval; /* store the ray */
   }
   printf("end of init_ray_table function\n");
   return (rayon_table);       /* return address of our ray table */
}

void panoramique(prom_images_struct * img_in, prom_images_struct * img_out, int numero, int *look_up)
{
   /*
    * Attention: normally here the output image and the look up table have the same size
    */
   int i;
   int index;                  /* index in the look up table and in the output image */
   int adresse_in;             /* adress in the input image */
   int adresse_out;            /* adress in the output image */
   int size;

   printf("start of panoramique function\n");

   size = img_out->sx * img_out->sy;   /* calculate the size of the output image */
   printf("size: %d\n", size);
   /*
    * Treatment
    */
   if (img_in->nb_band > 1)    /* in case of color image */
      /* make the transformation */
      for (index = 0; index < size; index++)
      {
         adresse_in = look_up[index];
         adresse_out = index * img_out->nb_band;
         for (i = 0; i < img_in->nb_band; i++)
            img_out->images_table[numero][adresse_out++] = img_in->images_table[numero][adresse_in++];
      }
   else                        /* in case of black and white image */
      /* make the transformation */
      for (index = 0; index < size; index++)
         img_out->images_table[numero][index] = img_in->images_table[numero][look_up[index]];

   printf("end of panoramique function\n");
}

void delete_rayon_table(float **rayon_table)
{
   if ((*rayon_table) != NULL)
   {
      free(*rayon_table);
      *rayon_table = NULL;
   }
   else printf("ERRROR: Attempt of deletion of a ray table already empty\n");
}

void function_panoramique(int gpe_sortie)
{
   int i;
   int nb_gpe;                 /*< number of groups connected with the input */
   int gpe_entree = 0;         /*< input number of the first group on the input */
   int num_liaison = 0;        /*< input number of the link between the input group and our group */
   int largeur, hauteur;       /*< dimension of the circular zone to be used */
   int centerX, centerY;       /*< coordinates of the center of the circulaire zone */

   int rayon;                  /*< ray of the circular zone */

   char *parametre;            /*< parameters on the link */
   char *commande;             /*< command of the link */
   float *rayon_table = NULL;  /*< pointer on our ray table */
   int ret;

   /* -1 is important to detected if a commande is on the link */
   int xCircleMin = -1;
   int xCircleMax = -1;
   int yCircleMin = -1;
   int yCircleMax = -1;
   int circleProjL = 1400;
   int circleProjH = 240;

   float paramA = -1;
   float paramB = -1;
   float paramC = -1;
   float temp;

   prom_images_struct *image_src;  /*< pointer on the .ext structure of the input group */
   prom_images_struct *image_out;  /*< pointer on the .ext structure of our group */

   /* update September 2003 Olivier Ledoux */
   typedef struct
   {
      prom_images_struct *image_src;
      prom_images_struct *image_out;
      int *panoramique_look_up;   /* pointer on the look_up table */
   } DataStruct;

   /*
    * initialization in case of first execution of our group
    */

   if (def_groupe[gpe_sortie].ext == NULL) /* in case of first call */
   {
      /*
       * find the input group
       */
      nb_gpe = 0;             /* by default there is no link in input */
      for (i = 0; i < nbre_liaison; i++)  /* all the links are analysed */
      {
         if (liaison[i].arrivee == gpe_sortie)   /* if we find a link to our group */
         {
            if (!nb_gpe)    /* is it the first ? */
            {
               gpe_entree = liaison[i].depart; /* read the input group */
               num_liaison = i;
            }
            nb_gpe++;
         }
      }

      /*
       * we check that we have got ONE AND ONLY ONE group on the input
       */
      if (!nb_gpe)            /* nothing on the input */
      {
         EXIT_ON_ERROR("This group must have got one input group... There isn't one!\n");
      }
      if (nb_gpe > 1)         /* more than a group on the input */
      {
         EXIT_ON_ERROR("This group must have got one input group... There are too many!\n");
      }

      /*
       * we check that the input group containts at less a picture
       */
      image_src = (prom_images_struct *) def_groupe[gpe_entree].ext;
      if (image_src == NULL)
      {
         EXIT_ON_ERROR("Problem (function_panoramique) : there is no image in the group %d\n",gpe_entree);
      }

      /*
       * reading of the link
       */
      parametre = liaison[num_liaison].nom;
      i = 0;
      while ((parametre[i] = tolower(parametre[i])))
         i++;                /* our string of parameter is converted into small letter */

      /* xCircleMin */
      commande = strstr(parametre, "-xcmin");
      if (commande != NULL)
         xCircleMin = atoi(&commande[6]);
      /* xCircleMax */
      commande = strstr(parametre, "-xcmax");
      if (commande != NULL)
         xCircleMax = atoi(&commande[6]);
      /* yCircleMin */
      commande = strstr(parametre, "-ycmin");
      if (commande != NULL)
         yCircleMin = atoi(&commande[6]);
      /* yCircleMax */
      commande = strstr(parametre, "-ycmax");
      if (commande != NULL)
         yCircleMax = atoi(&commande[6]);
      /* circleProjL */
      commande = strstr(parametre, "-cpl");
      if (commande != NULL)
         circleProjL = atoi(&commande[4]);
      /* circleProjH */
      commande = strstr(parametre, "-cph");
      if (commande != NULL)
         circleProjH = atoi(&commande[4]);
      /* A */
      commande = strstr(parametre, "-ca");
      if (commande != NULL)
         paramA = (float) atof(&commande[2]);
      /* B */
      commande = strstr(parametre, "-cb");
      if (commande != NULL)
         paramB = (float) atof(&commande[2]);
      /* C */
      commande = strstr(parametre, "-cc");
      if (commande != NULL)
         paramC = (float) atof(&commande[2]);

      /*
       * ask to the user, the missing parameters
       */
      if (xCircleMin == -1)
      {
         printf("\nPlease print xCircleMin : ");
         fflush(stdout);
         ret=scanf("%d", &xCircleMin);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      if (xCircleMax == -1)
      {
         printf("\nPlease print xCircleMax : ");
         fflush(stdout);
         ret=scanf("%d", &xCircleMax);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      if (yCircleMin == -1)
      {
         printf("\nPlease print yCircleMin : ");
         fflush(stdout);
         ret=scanf("%d", &yCircleMin);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      if (yCircleMax == -1)
      {
         printf("\nPlease print yCircleMax : ");
         fflush(stdout);
         ret=scanf("%d", &yCircleMax);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      if (circleProjL == -1)
      {
         printf("\nPlease print circleProjL : ");
         fflush(stdout);
         ret=scanf("%d", &circleProjL);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      if (circleProjH == -1)
      {
         printf("\nPlease print circleProjH : ");
         fflush(stdout);
         ret=scanf("%d", &circleProjH);
         if (ret!=1) EXIT_ON_ERROR("Function scanf return with wrong value");
         fflush(stdin);
      }
      /*    if (paramA==-1)
         {printf ("\nPlease print 'a' of ax�+bx+c : ");fflush(stdout);scanf("%f",&paramA);fflush(stdin);}
         if (paramB==-1)
         {printf ("\nPlease print 'b' of ax�+bx+c : ");fflush(stdout);scanf("%f",&paramB);fflush(stdin);}
         if (paramC==-1)
         {printf ("\nPlease print 'c' of ax�+bx+c : ");fflush(stdout);scanf("%f",&paramC);fflush(stdin);}
       */
      /*
       * the equation of deformation f(x)=ax�+b�+c is checked
       */

      /* f(0)=c */
      if ((paramC < 0) || (paramC > 1))
      {
         printf("\nAttention the deformation puts problem!!!: C= %f\n",paramC);
         fflush(stdout);
      }
      /* f(1)=a+b+c */
      else
      {
         temp = paramA + paramB + paramC;
         if ((temp < 0) || (temp > 1))
         {
            printf("\nAttention the deformation puts problem!! A+B+C= %f\n",paramC + paramA + paramB);
            fflush(stdout);
         }
         /* maxima f(-b/2a)=c-b�/4a */
         else
         {
            temp = paramC - paramB * paramB / (4 * paramA);
            if ((temp < 0) || (temp > 1))
            {
               printf("\nAttention the deformation puts problem!!, Maxima= %f\n",temp);
               fflush(stdout);
            }
         }
      }

      /* evaluation of important datas for the output images */
      printf("Circular zone xCircleMon=%d, xCircleMax=%d\n", xCircleMin, xCircleMax);
      printf("Circular zone yCircleMon=%d, yCircleMax=%d\n", yCircleMin, yCircleMax);
      largeur = xCircleMax - xCircleMin;  /* width of the circular zone to be treated */
      hauteur = yCircleMax - yCircleMin;  /* height of the circular zone to be treated */
      printf("Circular zone width=%d, heigth=%d\n", largeur, hauteur);
      centerX = 323;          /*320;(xCircleMin+xCircleMax)/2;  abscissa of the center of this zone */
      centerY = 267;          /*277;(yCircleMin+yCircleMax)/2;  orderly of the center of this zone */

      /*center pour image extern 323 367 */

      printf("Centre trouve x=%d,y=%d\n", centerX, centerY);
      rayon = 240;            /*241;if (largeur>hauteur) rayon=largeur/2; else rayon=hauteur/2;  we take the biggest dimension as ray */
      printf("Rayon trouve x=%d\n", rayon);

      /* memory allocation */
      image_out = calloc_prom_image(image_src->image_number, circleProjL, circleProjH, image_src->nb_band);   /* both have the same number of images and colors */
      def_groupe[gpe_sortie].ext = image_out;

      /* creation of the ray table */
      rayon_table = init_rayon_table(circleProjH, rayon);
      /* creation of the look up table */
      panoramique_look_up = init_look_up_table(image_src, image_out, rayon_table, centerX,  centerY);
      /* deletion of the ray table */
      delete_rayon_table(&rayon_table);


      /* update september 2003 Olivier Ledoux */
      def_groupe[gpe_sortie].data = (void *) malloc(sizeof(DataStruct));
      if ((prom_images_struct *) def_groupe[gpe_sortie].data == NULL)
      {
         EXIT_ON_ERROR("%s line:%d : ALLOCATION IMPOSSIBLE ...! \n", __FUNCTION__, __LINE__);
      }
      ((DataStruct *) def_groupe[gpe_sortie].data)->image_src = image_src;
      ((DataStruct *) def_groupe[gpe_sortie].data)->image_out = image_out;
      ((DataStruct *) def_groupe[gpe_sortie].data)->panoramique_look_up = panoramique_look_up;
   }
   else                        /* if this is NOT the first call */
   {
      /* update september 2003 Olivier Ledoux */
      image_src = ((DataStruct *) def_groupe[gpe_sortie].data)->image_src;
      image_out = ((DataStruct *) def_groupe[gpe_sortie].data)->image_out;
      panoramique_look_up = ((DataStruct *) def_groupe[gpe_sortie].data)->panoramique_look_up;
      /* here we could check if parameters are the same as the first call */
   }

   /* here we can make the treatment */
   for (i = 0; i < image_src->image_number; i++)   /* for all the images */
      panoramique(image_src, image_out, i, panoramique_look_up);
}
