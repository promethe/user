/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_init_joystick.c 
\brief 

Author: M. Maillard
Created: XX/XX/XXXX
Modified:
- author: M. Maillard
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  Fonction devant etre appele une fois pour initialise le joystick avant utilisation

Macro:
-NAME_LENGTH
-JOY_PORT

Local variables:
-int joy_port;
-joy_struct s_joy;

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>

/*#define DEBUG*/

#include <libhardware.h>
#include <net_message_debug_dist.h>

#define JOYSTICK_TMP_FILENAME "joystick.hwj"

void function_init_joystick(int gpe)
{
   FILE *fh = NULL;
   Joystick **joystick_table = NULL;

   (void) gpe; // (unused)

   PRINT_WARNING("Function f_init_joystick is deprecated: joysticks should be congifured on the hardware side, using the Joystick object");

   if (joystick_get_nb_joystick() > 0)
   {
      dprints("f_init_joystick(%s): Joystick already created => Doing nothing\n", def_groupe[gpe].no_name);
   }
   else
   {
      fh = fopen(JOYSTICK_TMP_FILENAME, "r");

      if (fh == NULL)
      {
	 fh = fopen(JOYSTICK_TMP_FILENAME, "w");
	 fprintf(fh, "port = /dev/input/js0\n");
	 fprintf(fh, "type = auto\n");
      }
      fclose(fh);

      joystick_table = ALLOCATION(Joystick *);
      joystick_table[0] = NULL;
      joystick_create_joystick(joystick_table, (char *) "joy", (char *) JOYSTICK_TMP_FILENAME);
      joystick_create_joystick_table(joystick_table, 1);
   }      
}
