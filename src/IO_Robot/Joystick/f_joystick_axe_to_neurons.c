/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  f_joystick_axe_to_neurons.c 
\brief 

Author: M. MAILLARD
Created: xx/xx/xx
Modified:
- author: M. Maillard
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Met 1 sur le neurone du groupe correspondant a la position de l'axe du joystick
les autres neurones sont a 0
un axe est passe sur le lien en parametre
fonction utile pour par exemple une interface Joystick/Neural Field
(codage en population)

Macro:
-none 

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define DEBUG

#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>
#include <libhardware.h>

typedef struct data_joystick_axe_to_neurons
{
      int axis_nb;
      float range;
      char joystick_name[256];
   float init_pos;
} data_joystick_axe_to_neurons;


void new_joystick_axe_to_neurons(int gpe)
{
   int index, link, ret;
   int axis_nb = -1;
   float init_pos = 0.5;
   float range = 1.;
   char retour[256] = "\0";  
   char joystick_name[256] = "\0";
   data_joystick_axe_to_neurons *my_data = NULL;

   dprints("new_joystick_axe_to_neurons(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {
      index = 0;
      link = find_input_link(gpe, index);
      while (link != -1)
      {
	 if (prom_getopt(liaison[link].nom, "AXE", retour) == 2)	 
	 {
            ret = sscanf(retour, "%i", &axis_nb);
	    if (ret != 1)
	    {
	       EXIT_ON_ERROR("Incorrect format for option -AXE (should be followed by an integer)");
	    }
	 }

	 if (prom_getopt(liaison[link].nom, "D", retour) == 2)	 
	 {
            ret = sscanf(retour, "%f", &range);
	    if (ret != 1)
	    {
	       EXIT_ON_ERROR("Incorrect format for option -AXE (should be followed by an integer)");
	    }
	 }

	 if (prom_getopt(liaison[link].nom, "j", retour) == 2)	 
	 {
            strcpy(joystick_name, retour);
	 }
	 
	 if (prom_getopt(liaison[link].nom, "i", retour) == 2)	 
	 {
            init_pos = atof(retour);
	    if (init_pos < 0 || init_pos > 1)
	    { 
	       EXIT_ON_ERROR("init position (-i option) for the joystick must be between 0 and 1");
	    } 
	 }
	 
	 index++;
	 link = find_input_link(gpe, index);	 
      }

      if (range > 1.)
      {
	 range = 1.;
      }
      else if (range < 0.)
      {
	 range = 0.;
      }
      
      if (axis_nb == -1)
      {
	 EXIT_ON_ERROR("Missing input link option -AXE[axis_nb]");
      }

      my_data = ALLOCATION(data_joystick_axe_to_neurons);
      memset(my_data, 0, sizeof(data_joystick_axe_to_neurons));

      strcpy(my_data->joystick_name, joystick_name);
      my_data->axis_nb = axis_nb;
      my_data->range = range;
      my_data->init_pos = init_pos;
      def_groupe[gpe].data = my_data;
   }   
}

void function_joystick_axe_to_neurons(int gpe)
{
   int i, neuron_index;
   float pos;
   Joystick *joystick = NULL;
   data_joystick_axe_to_neurons *my_data = (data_joystick_axe_to_neurons *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retrieve data");
   }

   if (strlen(my_data->joystick_name) == 0)
   {
      joystick = joystick_get_first_joystick();
      if (joystick == NULL)
      {
	 EXIT_ON_ERROR("No joystick defined");
      }
   }
   else
   {
      joystick = joystick_get_joystick_by_name(my_data->joystick_name);
      if (joystick == NULL)
      {
	 EXIT_ON_ERROR("No joystick with name %s", my_data->joystick_name);
      }
   }

   pos = joystick_get_axis_position(joystick, my_data->axis_nb);
   if (pos < 0)
   {
      pos = my_data->init_pos;
   }

   neuron_index = def_groupe[gpe].premier_ele + lrint((pos * my_data->range + 0.5 - my_data->range / 2.) * (def_groupe[gpe].nbre - 1));

   dprints("f_joystick_axe_to_neurons(%s): Axis %i has value %f, setting neuron %i\n", def_groupe[gpe].no_name, my_data->axis_nb, pos, neuron_index);
   
   for (i = def_groupe[gpe].premier_ele; i < def_groupe[gpe].premier_ele + def_groupe[gpe].nbre; i++)
   {
      neurone[i].s = neurone[i].s1 = neurone[i].s2 = 0;
   }

   neurone[neuron_index].s = neurone[neuron_index].s1 = neurone[neuron_index].s2 = 1;
}

void destroy_joystick_axe_to_neurons(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_joystick_axe_to_neurons*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_joystick_axe_to_neurons(%s): Leaving function\n", def_groupe[gpe].no_name);
}

