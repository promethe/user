/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   defgroup f_control_robot_by_joystick f_control_robot_by_joystick
   ingroup libIO_Robot

   CHECK DOC !

   \details

   \section Description
   le groupe extrait les point de l'image d'entree (groupe liaison algo 'image') dont l'activite est au dessus du seuil du groupe  
   a chaque debut de boucle (groupe f_trasition_detect liaison algo 'transition_detect'), et dispense l'absisce et l'ordonne
   du max sur ces deux premiers neurone. Lorsque tout les point sont traite, le troisieme neurone s'active et peut erte connecte a un break;


   \file 
   \ingroup f_control_robot_by_joystick
   \brief 

   Author: xxxxxxxx
   Created: XX/XX/XXXX
   Modified:
   - author: C.Giovannangeli
   - description: specific file creation
   - date: 02/05

************************************************************/
/*#define DEBUG*/
#include <libx.h>

#include <Typedef/liste_chaine.h>
#include <Struct/prom_images_struct.h>
#include <stdlib.h>
#include <Kernel_Function/find_input_link.h>
#include <string.h>
#include "../Robot_Mvt/tools/include/move_via_speed.h"

typedef struct MyData_f_control_robot_by_joystick
{
  int gpe_stop;
  int gpe_direction;
  int gpe_vitesse;

} MyData_f_control_robot_by_joystick;

void function_control_robot_by_joystick(int gpe)
{
  MyData_f_control_robot_by_joystick *my_data;
  int gpe_stop = -1, gpe_direction = -1, gpe_vitesse = -1;
  int l = -1, i = 0, neur_direction, neur_vitesse;
  float stop, vitesse, direction;
  double beta_tot = 0, norme = 0;
  float max;
  FILE *fp;

  (void) stop; // (unused)

  if (def_groupe[gpe].data == NULL)
  {

    i = 0;
    l = find_input_link(gpe, i);
    while (l != -1)
    {
      if (strcmp(liaison[l].nom, "vitesse") == 0)
      {
	gpe_vitesse = liaison[l].depart;
      }
      if (strcmp(liaison[l].nom, "direction") == 0)
      {
	gpe_direction = liaison[l].depart;
      }

      if (strcmp(liaison[l].nom, "stop") == 0)
      {
	gpe_stop = liaison[l].depart;
      }
      i++;
      l = find_input_link(gpe, i);

    }

    if (gpe_vitesse == -1 || gpe_direction == -1 || gpe_stop == -1)
    {
      EXIT_ON_ERROR("il manque un groupe en entree de f_control_robot_by_joystick(%s)\n",def_groupe[gpe].no_name);
    }

    my_data =
      (MyData_f_control_robot_by_joystick *)
      malloc(sizeof(MyData_f_control_robot_by_joystick));
    my_data->gpe_vitesse = gpe_vitesse;
    my_data->gpe_stop = gpe_stop;
    my_data->gpe_direction = gpe_direction;
    def_groupe[gpe].data = (MyData_f_control_robot_by_joystick *) my_data;
    /*Init_Robot_Position_Counter(); */
  }
  else
  {
    my_data = (MyData_f_control_robot_by_joystick *) def_groupe[gpe].data;
    gpe_stop = my_data->gpe_stop;
    gpe_vitesse = my_data->gpe_vitesse;
    gpe_direction = my_data->gpe_direction;
  }
  /*code effectif */
  /* printf("PASSSSSSSSSSSSSSSAAAAAAAAAAAAAGGGGGGGGGGGGGGEEEEEEEEEEEEE\n"); */
  /*Calcul Vitesse */

  max = -100;
  neur_vitesse = -1;
  for (i = 0; i < def_groupe[gpe_vitesse].nbre; i++)
  {
    if (neurone[i + def_groupe[gpe_vitesse].premier_ele].s2 >= max)
    {
      neur_vitesse = i;
      max = neurone[i + def_groupe[gpe_vitesse].premier_ele].s2;
    }
  }
  vitesse =
    (float) (def_groupe[gpe_vitesse].nbre - 1 -
	     neur_vitesse) / (float) (def_groupe[gpe_vitesse].nbre) *
    200.;
  cprints("gagnat=%d   vitesse:%f\n", neur_vitesse, vitesse);
  /*calcul direction */
  max = -100;
  neur_direction = -1;
  for (i = 0; i < def_groupe[gpe_direction].nbre; i++)
  {
    if (neurone[i + def_groupe[gpe_direction].premier_ele].s2 >= max)
    {
      neur_direction = i;
      max = neurone[i + def_groupe[gpe_direction].premier_ele].s2;
    }
  }
  direction =
    ((float) neur_direction / (float) (def_groupe[gpe_direction].nbre) -
     0.5);
  cprints("gagnant: %d   direction:%f\n", neur_direction, direction);



  if (direction < 0)
    move_via_speed((int) vitesse + 2 * direction * vitesse, (int) vitesse, &norme, &beta_tot, 0); /*robot_go_by_speed(robot_get_first_robot(),4,4); */
  else
    move_via_speed((int) vitesse, (int) vitesse - 2 * direction * vitesse, &norme, &beta_tot, 0);


/*verifie stop*/
  stop = 0;
  for (i = 0; i < def_groupe[gpe_stop].nbre; i++)
  {
    if (isequal(neurone[i + def_groupe[gpe_stop].premier_ele].s2, 1))
    {
      move_via_speed(0, 0, &norme, &beta_tot, 0);   /*robot_go_by_speed(robot_get_first_robot(),4,4); */
      cprints("SSSTTTOOOPPPPP\n");
    }
  }


  fp = fopen("odometry.SAVE", "a");
  fprintf(fp, "%f %f\n ", beta_tot, norme);
  fclose(fp);
}
