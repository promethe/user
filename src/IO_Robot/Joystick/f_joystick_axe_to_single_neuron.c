/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/** ***********************************************************
\file  f_joystick_axe_to_single_neuron.c 
\brief 

Author: M. Maillard
Created: XX/XX/XXXX
Modified:
- author: M. Maillard
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
Met la position d'un axe sur le neurone du groupe 
un axe est passe sur le lien en parametre
Le codage de la position de l'axe du joystick est une valeur entre 0 et 1
Macro:
-none 

Local variables:
-int joy_port
-joy_struct s_joy

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <libx.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG*/
#define MAX_NB_JOYSTICK_AXES 10

#include <libhardware.h>
#include <Kernel_Function/find_input_link.h>
#include <Kernel_Function/prom_getopt.h>
#include <net_message_debug_dist.h>


typedef struct data_joystick_axe_to_single_neuron
{
      int nb_axes;
      int axes_index[MAX_NB_JOYSTICK_AXES];
      char joystick_name[256];
   float init_pos;
} data_joystick_axe_to_single_neuron;


void new_joystick_axe_to_single_neuron(int gpe)
{
   int index, link, ret;
   int nb_axes = 0;
   char retour[256] = "\0";  
   char joystick_name[256] = "\0";
   char *link_parse = NULL;
   float init_pos = 0.5;
   data_joystick_axe_to_single_neuron *my_data = NULL;

   dprints("new_joystick_axe_to_single_neuron(%s): Entering function\n", def_groupe[gpe].no_name);

   if (def_groupe[gpe].data == NULL)
   {
      my_data = ALLOCATION(data_joystick_axe_to_single_neuron);
      memset(my_data, 0, sizeof(data_joystick_axe_to_single_neuron));

      index = 0;
      link = find_input_link(gpe, index);
      while (link != -1)
      {
	 link_parse = liaison[link].nom;

	 while (prom_getopt(link_parse, "AXE", retour) == 2)	 
	 {
            ret = sscanf(retour, "%i", my_data->axes_index + nb_axes);
	    if (ret != 1)
	    {
	       EXIT_ON_ERROR("Incorrect format for option -AXE (should be followed by an integer)");
	    }

	    nb_axes++;
	    link_parse = strstr(link_parse, "-AXE") + 4 + strlen(retour);
	 }

	 if (prom_getopt(liaison[link].nom, "j", retour) == 2)	 
	 {
            strcpy(joystick_name, retour);
	 }
	 
	 if (prom_getopt(liaison[link].nom, "i", retour) == 2)	 
	 {
            init_pos = atof(retour);
	    if (init_pos < 0 || init_pos > 1)
	    { 
	       EXIT_ON_ERROR("init position (-i option) for the joystick must be between 0 and 1");
	    } 
	 }
	 
	 index++;
	 link = find_input_link(gpe, index);	 
      }

      if (nb_axes != def_groupe[gpe].nbre)
      {
	 EXIT_ON_ERROR("Number of axes (options -AXE) is different from group size");
      }

      strcpy(my_data->joystick_name, joystick_name);
      my_data->nb_axes = nb_axes;
      my_data->init_pos = init_pos;
      def_groupe[gpe].data = my_data;
   }   
}

void function_joystick_axe_to_single_neuron(int gpe)
{
   int i, first;
   float pos;
   Joystick *joystick = NULL;
   data_joystick_axe_to_single_neuron *my_data = (data_joystick_axe_to_single_neuron *) def_groupe[gpe].data;

   if (my_data == NULL)
   {
      EXIT_ON_ERROR("Cannot retrieve data");
   }

   if (strlen(my_data->joystick_name) == 0)
   {
      joystick = joystick_get_first_joystick();
      if (joystick == NULL)
      {
	 EXIT_ON_ERROR("No joystick defined");
      }
   }
   else
   {
      joystick = joystick_get_joystick_by_name(my_data->joystick_name);
      if (joystick == NULL)
      {
	 EXIT_ON_ERROR("No joystick with name %s", my_data->joystick_name);
      }
   }

   first = def_groupe[gpe].premier_ele;

   for (i = 0; i < my_data->nb_axes; i++)
   {      
      pos = joystick_get_axis_position(joystick, my_data->axes_index[i]);
      if (pos < 0)
      {
	 pos = my_data->init_pos;
      }

      neurone[first + i].s = neurone[first + i].s1 = neurone[first + i].s2 = pos;

      dprints("f_joystick_axe_to_single_neuron(%s): Axis %i has value %f\n", def_groupe[gpe].no_name, my_data->axes_index[i], pos);
   }
}

void destroy_joystick_axe_to_single_neuron(int gpe)
{
   if (def_groupe[gpe].data != NULL)
   {
      free(((data_joystick_axe_to_single_neuron*) def_groupe[gpe].data));
      def_groupe[gpe].data = NULL;
   }
   dprints("destroy_joystick_axe_to_single_neuron(%s): Leaving function\n", def_groupe[gpe].no_name);
}
