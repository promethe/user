/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 @ingroup plugin_list
 @defgroup libSensors libSensors
 @brief Groups to acquire data from sensors (sound, vision, ...).
 */

#include <string.h>

#include <libx.h>

#include <Sound/Sound.h>

#include <group_function_pointers.h>

#include <Vision/Image_Processing.h>
#include <Vision/Image_Convert.h>
#include <Vision/QuadTree.h>
#include <Vision/Contours.h>
#include <Vision/Robot_Vision.h>
#include <Vision/Vision_Tools.h>
#include <Vision/Flow.h>
#include <Vision/Contraste.h>
#include <Vision/Head.h>
#include <Vision/Image_IO.h>
#include <Vision/Color.h>
#include <Vision/Sampling.h>
#include <Vision/Filtrage.h>
#include <Vision/Scale_Space.h>
#include <Vision/popout.h>
#include <Vision/Vision.h>

/* networks of sensors */
#include <Sensors/Network.h>


#include <libSensors.h>

type_group_function_pointers group_function_pointers[] =
  {
  /* Sound */
    { "f_assoc_timing", function_assoc_timing, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_close_sound", function_close_sound, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_compute_cr", function_compute_cr, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_context", function_context, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_dsp_audio", function_dsp_audio, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_fft_audio_execute", function_fft_audio_execute, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_get_sound", function_get_sound, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_init_fft_audio", function_init_fft_audio, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_init_sound", function_init_sound, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_mean_sound", function_mean_sound, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_traite_context", function_traite_context, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sound_get_signal", function_sound_get_signal, new_sound_get_signal, destroy_sound_get_signal, NULL, NULL, -1, -1 },
    { "f_play_sound", function_play_sound, new_play_sound, destroy_play_sound, NULL, NULL, -1, -1 },
    {"f_read_audio", function_read_audio, new_read_audio, destroy_read_audio, NULL, NULL, -1, -1},

  /* Vision */
    { "f_check_visual_compass", function_check_visual_compass, NULL, NULL, NULL, NULL, -1, -1 },

  /* Color */
    { "f_couleur_selection", function_couleur_selection, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_bleu_selection", function_img_bleu_selection, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_couleur_selection", function_img_couleur_selection, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_gris_selection", function_img_gris_selection, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_select_color", function_img_select_color, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_select_color_channel", function_img_select_color_channel, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_selection_image_couleur", function_selection_image_couleur, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_generator", function_img_generator, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_img_test", function_img_test, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_color_histogram", function_color_histogram, new_color_histogram, destroy_color_histogram, NULL, NULL, -1, -1 },

  /* Contours */
    { "f_calgradD", function_calgradD, new_calgradD, NULL, NULL, NULL, -1, -1 },
    { "f_calgradDS", function_calgradDS, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ptcg", function_ptcg, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_rosten", function_rosten, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_orient", function_orient, new_orient, destroy_orient, NULL, NULL, -1, -1 },
    { "f_paquerette", function_paquerette, new_paquerette, destroy_paquerette, NULL, NULL, -1, -1 },
	{ "f_vanish_point", function_vanish_point, new_vanish_point, destroy_vanish_point, NULL, NULL, -1, -1 },
    { "f_seuil_dynamic", function_seuil_dynamic, new_seuil_dynamic, destroy_seuil_dynamic, NULL, NULL, -1, -1 },
    { "f_compet_ptc", function_compet_ptc, new_compet_ptc, destroy_compet_ptc, NULL, NULL, -1, -1 },
    { "f_conv_dog", function_conv_dog, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_gradient_vectoriel", function_gradient_vectoriel, new_gradient_vectoriel, destroy_gradient_vectoriel, NULL, NULL, -1, -1 },

    
  /* Contraste */
    { "f_rehaussement", function_rehaussement, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ext_instar", function_ext_instar, NULL, destroy_ext_instar, NULL, NULL, -1, -1 },
    { "f_log_ext", function_log_ext, NULL, destroy_log_ext, NULL, NULL, -1, -1 },
    /* { "f_retinex", function_retinex, new_retinex, destroy_retinex, NULL, NULL, -1, -1 }, */

  /* FFT */
    { "f_fft", function_fft_real_to_complex, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_fft_signal", function_fft_signal, new_fft_signal, NULL, NULL, NULL, -1, -1 },
    { "f_fft_inv", function_fft_inv_complex_to_real, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_filtre_FFT_complex", function_filtre_FFT_complex, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_init_fftw", function_init_fftw, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_init_fftw_complex", function_init_fftw_complex, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_multi_inputs_fft", function_multi_inputs_fft, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_wta_ext_float", function_wta_ext_float, NULL, NULL, NULL, NULL, -1, -1 },

  /* filtrage */
    { "f_passebas", function_passebas, NULL, NULL, NULL, NULL, -1, -1 },

  /* Flow */
    { "f_optical_flow", function_optical_flow, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_conversion_90_to_180", function_conversion_90_to_180, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_delta_input", function_delta_input, new_delta_input, NULL, NULL, NULL, -1, -1 },
    { "f_detect_mvt_a", function_detect_mvt_a, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_head_direction", function_head_direction, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_turn_body", function_turn_body, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_turn_head", function_turn_head, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_turn_head_and_body", function_turn_head_and_body, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_turn_head_reset", function_turn_head_reset, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_turn_servo_2", function_turn_servo_2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_interpolation_2D", function_interpolation_2D, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_optical_flow_directions", function_optical_flow_directions, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_optical_flow_comp", function_optical_flow_comp, NULL, NULL, NULL, NULL, -1, -1 },

  /* Head */
    { "f_cadrer_depuis_projections", function_cadrer_depuis_projections, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cadre_RN", function_cadre_RN, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_hough_for_head", function_circular_hough_transform_for_head, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_hough_circular", function_circular_hough_transform_for_head, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_image", function_decouper_image, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_image_tracking", function_decouper_image_tracking, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_visage", function_decouper_visage, new_decouper_visage, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_visage_inf_tracking", function_decouper_visage_inf_tracking, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_visage_sup", function_decouper_visage_sup, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_decouper_visage_sup_tracking", function_decouper_visage_sup_tracking, NULL, NULL, NULL, NULL, -1, -1 },
  /*{"f_decouper_visage_tracking", function_decouper_visage_tracking, NULL, NULL, NULL, NULL, -1, -1},inutilisee */
    { "f_extraction_yeux_bouche", function_extraction_yeux_bouche, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_eye_analog_segment", function_eye_analog_segment, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_filtre_teinte", function_filtre_teinte, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_histogramme_vertical", function_histogramme_vertical, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_histogramme", function_histogramme, new_histogramme, destroy_histogramme, NULL, NULL, -1, -1 },
    { "f_hough_neurone", function_hough_neurone, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_produit_tensoriel", function_produit_tensoriel, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_projection_horizontale", function_projection_horizontale, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_projection_verticale", function_projection_verticale, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_macro_data", function_save_macro_data, new_save_macro_data, NULL, NULL, NULL, -1, -1 },

  /* Image Convert  */
    { "f_convert_image_float_to_uchar", function_convert_image_float_to_uchar, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_convert_image_uchar_to_float", function_convert_image_uchar_to_float, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_convert_RGB_LAB", function_convert_RGB_LAB, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_convert_RGB_NB", function_convert_RGB_NB, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_convert_YCbCr_NB", function_convert_YCbCr_NB, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_convert_RGB_HSV", function_convert_RGB_HSV, NULL, NULL, NULL, NULL, -1, -1 },

    { "f_convert_image_complex_to_norm", function_convert_image_complex_to_norm, NULL, NULL, NULL, NULL, -1, -1 },

    { "f_convert_image_2floats_color", function_convert_image_2floats_color, NULL, NULL, NULL, NULL, -1, -1 },

  /* Image IO */
    { "f_get_one_image", function_get_one_image,new_get_one_image , NULL, NULL, NULL, -1, -1 },
    { "f_grabimages", function_grabimages, new_grabimages, destroy_grabimages, NULL, NULL, -1, -1 },
    { "f_grabimages_wait", function_grabimages, new_grabimages, destroy_grabimages, NULL, NULL, -1, -1 },
    { "f_grabimages_autoexposure", function_grabimages_autoexposure, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_grabimages_autoexposure_waitcond", function_grabimages_autoexposure_waitcond, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_grabimages_waitcond", function_grabimages_waitcond, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_baccon", function_load_baccon, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_image_from_disk", function_load_image_from_disk, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_image_png", function_load_image_png, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_image_sequential", function_load_image_sequential, NULL, destroy_load_image_sequential, NULL, NULL, -1, -1 },
    { "f_load_image_to_prom_window", function_load_image_to_prom_window, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_load_list", function_load_list, new_load_list, NULL, NULL, NULL, -1, -1 },
    { "f_load_listimage_intensity", function_load_listimage_intensity, new_load_listimage_intensity, NULL, NULL, NULL, -1, -1 },
    { "f_save_images_local", function_save_images_local, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_images_to_disk_expression", function_save_images_to_disk_expression, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_groupe_to_disk_expression", function_save_groupe_to_disk_expression, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_images_to_disk_expression_intensity", function_save_images_to_disk_expression_intensity, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_images_to_disk_intensity", function_save_images_to_disk_intensity, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_image_to_disk", function_save_image_to_disk, new_save_image_to_disk, destroy_save_image_to_disk, NULL, NULL, -1, -1 },
    { "f_save_im_to_disk", function_save_image_to_disk, new_save_image_to_disk, destroy_save_image_to_disk, NULL, NULL, -1, -1 },
    { "f_save_images_to_disk", function_save_image_to_disk, new_save_images_to_disk, destroy_save_image_to_disk, NULL, NULL, -1, -1 },
    { "f_track_robot_with_mouse", function_track_robot_with_mouse, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_copy_window_to_ext", function_copy_window_to_ext, new_copy_window_to_ext, destroy_copy_window_to_ext, NULL, NULL, -1, -1 },
    { "f_nios_grabimages", function_nios_grabimages, new_NIOS_grabimages, NULL, NULL, NULL, -1, -1 },
    { "f_set_camera_param", function_set_camera_param , new_set_camera_param, NULL, NULL, NULL, -1, -1 },
    { "f_save_image_with_carac", function_save_image_with_carac, new_save_image_with_carac, destroy_save_image_with_carac, NULL, NULL, -1, -1 },
  //  { "f_compress_image", function_compress_image, new_compress_image, destroy_compress_image, NULL, NULL, -1, -1 },
    { "f_uncompress_image", function_uncompress_image, new_uncompress_image, destroy_uncompress_image, NULL, NULL, -1, -1 },

  /* Image_Processing/Movement_Tracking */
    { "f_detect_flow_fast_signed_x", function_detect_flow_fast_signed_x, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_detect_mvt_fast", function_detect_mvt_fast, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_image_difference", function_image_difference, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_quantite_mvt", function_quantite_mvt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_eye_static", function_tracking_eye_static, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_filter_dyn", function_tracking_filter_dyn, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_filter_static", function_tracking_filter_static, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_filter_static_graphic", function_tracking_filter_static_graphic, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_head_static", function_tracking_head_static, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_tracking_head_static_neurone", function_tracking_head_static_neurone, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_visu_orient_scale_key", function_visu_orient_scale_key, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_wta_special", f_wta_special, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_image_multiply", function_image_multiply, new_image_multiply, NULL, NULL, NULL, -1, -1 },
    { "f_nios_give_focus_point", function_nios_give_focus_point, new_give_nios_focus_point, NULL, NULL, NULL, -1, -1 },
    { "f_nios_rho_theta", function_NIOS_rho_theta, NULL, NULL, NULL, NULL, -1, -1 },
		{ "f_tracking_diff", function_tracking_diff, new_tracking_diff, destroy_tracking_diff, NULL, NULL, -1, -1 },
		{ "f_give_focus_point_from_saved_data", function_give_focus_point_from_saved_data, new_give_focus_point_from_saved_data, destroy_give_focus_point_from_saved_data, NULL, NULL, -1, -1 },

  /* Image_Processing/Rho_Theta */
    { "f_EV_ext", function_EV_ext, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sequential_winner", function_give_focus_point, new_give_focus_point, destroy_give_focus_point, NULL, NULL, -1, -1 },
    { "f_give_focus_point", function_give_focus_point, new_give_focus_point, destroy_give_focus_point, NULL, NULL, -1, -1 },
    { "f_memory_focus_point", function_memory_focus_point, new_memory_focus_point, destroy_memory_focus_point, NULL, NULL, -1, -1 },
    { "f_rho_theta", function_rho_theta, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sequ_winn_combinen", function_sequ_winn_combine, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_image_multiplyn", function_image_multiply, NULL, NULL, NULL, NULL, -1, -1 },

  /* popout */
    { "f_add_ext_float", function_add_ext_float, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cordonnee_plaque", function_cordonnee_plaque, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_intensite_expression", function_demande_intensite_expression, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demande_numero", function_demande_numero, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_demultiplex_image_filename", function_demultiplex_image_filename, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ecriture_des_focalisations", function_ecriture_des_focalisations, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_extension_png_to_txt", function_extension_png_to_txt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_modulation2", function_modulation2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_multi_ext_float", function_multi_ext_float, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_biais2", function_popout_biais2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_compptcar", function_popout_compptcar, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_creation_master", function_popout_creation_master, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_focus2", function_popout_focus2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_master2", function_popout_master2, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_popout_ptcar", function_popout_ptcar, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_test_appart_point", function_test_appart_point, NULL, NULL, NULL, NULL, -1, -1 },

  /* Quad Tree */
    { "f_fusion_rectangles", function_fusion_rectangles, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_image_des_teintes", function_image_des_teintes, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_moyennage", function_moyennage, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_multiplication_images", function_multiplication_images, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_soustraction_images", function_soustraction_images, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_diff_images", function_diff_images, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_teinte_jaune", function_teinte_jaune, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_visage", function_visage, NULL, NULL, NULL, NULL, -1, -1 },

  /* Robot Vision */
    { "f_ctr_sp", function_ctr_sp, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_entree_vision_sp", function_entree_vision_sp, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_EV_sp", function_entree_vision_sp, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_gradH", function_gradH, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_pano_focus", function_pano_focus, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_ptc_sp", function_ptc_sp, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_shift_ext_non_circular", function_shift_ext_non_circular, NULL, NULL, NULL, NULL, -1, -1 },
    {"f_winner_image",function_winner_image,new_winner_image,destroy_winner_image, NULL, NULL, -1, -1},


  /* Sampling */
    { "f_over_sample_two", function_over_sample_two, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sub_sample", function_sub_sample, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_sub_sample_two", function_sub_sample, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_image_zoom2", function_images_zoom2, new_image_zoom, destroy_image_zoom, NULL, NULL, -1, -1 },

  /* Scale Space */
    { "f_add_local_to_global", function_add_local_to_global, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_add_neural_Gpe", function_add_neural_Gpe, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_argmax", function_argmax, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_coarse_to_fine", function_coarse_to_fine, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_dispatch_caract", function_dispatch_caract, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_gather_scale_desc", function_gather_scale_desc, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_integration", function_integration, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_integr_sensation_action", function_integr_sensation_action, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_keypoint_descriptor", function_keypoint_descriptor, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_merge_neuro_mod", function_merge_neuro_mod, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_merge_servo_flow", function_merge_servo_flow, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_nav_population", function_nav_population, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_nav_population_avant_arriere", function_nav_population_avant_arriere, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_nav_vit_orient", function_nav_vit_orient, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_save_ext_float", function_save_ext_float, NULL, NULL, NULL, NULL, -1, -1 },

  /* Vision Tools */
    { "f_affiche_mvt", function_affiche_mvt, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_average_position", function_average_position, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_bipano_to_pano", function_bipano_to_pano, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_compare_trajectory", function_compare_trajectory, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_compute_grad_orient", function_compute_grad_orient, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_conversion_coordonnee_neurone", function_conversion_coordonnee_neurone, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_cut_image_ext", function_cut_image_ext, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_difference_images", function_difference_images, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_extend_image_panoramic", function_extend_image_panoramic, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_imagette", function_imagette, new_imagette, NULL, NULL, NULL, -1, -1 },
    { "f_image_zoom", function_image_zoom, new_image_zoom, destroy_image_zoom, NULL, NULL, -1, -1 },
    { "f_insert_imagette", function_insert_imagette, new_insert_imagette, destroy_insert_imagette, NULL, NULL, -1, -1 },
    { "f_keypoint_orient", function_keypoint_orient, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_moy_ecart_img", function_moy_ecart_img, new_moy_ecart_img, NULL, NULL, NULL, -1, -1 },
    { "f_moyenne_image", function_moyenne_image, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_multi_where", function_multi_where, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_paste_panorama", function_paste_panorama, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_seuil_image_char", function_seuil_image_char, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_seuil_image", function_seuil_image, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_shift_to_north_image_panoramic", function_shift_to_north_image_panoramic, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_simulation_object", function_simulation_object, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_split_image_panoramic", function_split_image_panoramic, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_unsplit_image_panoramic", function_unsplit_image_panoramic, NULL, NULL, NULL, NULL, -1, -1 },
    { "f_quad_average", function_quad_average, NULL, NULL, NULL, NULL, -1, -1},
    { "f_sort_directions", function_sort_directions, NULL, NULL, NULL, NULL, -1, -1},
	{ "f_sort_directions_only", function_sort_directions_only, NULL, NULL, NULL, NULL, -1, -1},
    { "f_running_average", function_running_average, new_running_average, destroy_running_average, NULL, NULL, -1, -1 },
    { "f_vergence", function_vergence, new_vergence, destroy_vergence, NULL, NULL, -1, -1},
    { "f_patch_normalization", function_patch_normalization, new_patch_normalization, destroy_patch_normalization, NULL, NULL, -1, -1},
    { "f_img_normalization", function_img_normalization, new_img_normalization, destroy_img_normalization, NULL, NULL, -1, -1},
   /* Networks sensors */ 
    { "f_net_sensor_get_sensor", function_net_sensor_get_sensor, new_net_sensor_get_sensor, destroy_net_sensor_get_sensor, NULL, NULL, -1, -1},
    { "f_get_sensor_value", function_get_sensor_value, new_get_sensor_value, destroy_get_sensor_value, NULL, NULL, -1, -1},
    
    /* Other*/
    
    { "f_energy", function_energy, new_energy, destroy_energy, NULL, NULL, -1, -1},
    
 /* pour indiquer la fin du tableau*/
    { NULL, NULL, NULL, NULL, NULL, NULL, -1, -1 } };

void read_help();

void modify_help();

int main()
{
  /*printf("main de la librairie, ne serat a rien...mais Mac OS le demande... \n");*/
  return 1;
}
