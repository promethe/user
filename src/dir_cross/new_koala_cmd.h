/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
extern void koala_ir(int *);
extern void wait_stop();

/*extern int	vIDProcess[3];*/

/* Angle camera par rapport a 0� ; var globale*/
/*extern float	AngleCam;*/

extern void turn_right(int);
extern void turn_left(int);
extern void turn_right2(int);
extern void turn_left2(int);
extern void TurnRightCond(int);
extern void TurnLeftCond(int);

extern void progress(int);
extern void backwards(int);

extern void function_init_koala();
extern void Init_Koala();

extern boolean commande_koala_2(char *, char *);
extern boolean macro_choice(char *, char *);



extern void GoToLeftRight(int, int);
extern void GoToLeftRightUncond(int, int);
extern void GoToLeftRightCond(int, int);
extern int GoToLeftRightIRUncond(int, int);
extern int GoToLeftRightIRCond(int, int);

extern void turn_left_2(int);   /* Z6  */
extern void turn_right_2(int);  /* Z7  */
extern int AngleTrigo(int);     /* Z29 */
extern int AngleAbsoluCam();    /* Z32 */
extern int AngleRelatifCam();   /* Z33 */
extern int MotivationStop();    /* Z34 */
extern int AngleCompassProcess();   /* Z35 */
extern void move(int);          /* Z36 */
extern void turn_2(int);        /* Z37 */
extern void TurnCond(int);      /* Z38 */
extern void turn(int);          /* Z39 */
extern int turn_stop(int);      /* Z40 */
extern void ServoFloat(int);    /* Z28 */


extern int LastTargetReached();
extern int IRKoalaStop();
extern int frontal_shock_detect();
extern int left_shock_detect();
extern int right_shock_detect();
extern int straight_ahead_stop(int);
extern int turn_left_stop(int);
extern int turn_right_stop(int);

extern void ServoInt(int);
extern void Boussole(int *, int *); /*int32 */
extern int ReadCaptor(int);
extern int AngleBoussole();
extern int AngleBoussole_one();

extern float HeadDirection;

/* modif Cyril 3/06/02, je place ces definitions CAM dans panorama.h */
/* psotion -90 et +90 degree pour la camera */
/*#define CAMSTART 10*/
/*#define CAMSTOP 215   Cyril 21/05/02 au lieu de 245-> en butee*/

/* ::: Valeur pour l'odometrie correspondant a un angle de 90 degree ::: */
/* ::: si les valeurs sont : -ANGLE90, ANGLE90 le robot a tourne de 90 sur la gauche */
#define ANGLE90 	7300
#define ANGLE90ENTIER	81      /*   approximation 7300/90=81,attention valeur a modifie dans new_koala.c            */

#define SHOCK_THRESH  205       /* changement de valeur 1023* 0.2 = 204.6 = 205 */

/* valeur de calibrage pour la boussole */
/* encore utilise avec la fonction de Jose ?? */
#define CMAX 780.0
#define SMAX 780.0
#define CMIN 465.0
#define SMIN 465.0


/* Seuil de detection du but blanc par 1e capteur 2 (oeil d'huitre) */
/* #define	SEUIL_DETECT_GOAL	0.187 */

/* Seuil de detection du but rouge par 1e capteur 2 (oeil d'huitre) */
/* #define	SEUIL_DETECT_GOAL2	0.10 */

/* Seuil de detection du but blanc par 1e capteur 2 (oeil d'huitre)  */
#define	SEUIL_DETECT_GOAL	191

/* Seuil de detection du but rouge par 1e capteur 2 (oeil d'huitre) */
#define	SEUIL_DETECT_GOAL2	102



            /*-------------------------------------------*/
#define COM_A 'A'               /* Configure                                 */
            /*-------------------------------------------*/
#define COM_B 'B'               /* Read software version                     */
            /*-------------------------------------------*/
#define COM_C 'C'               /* Set a position to be reached              */
            /*-------------------------------------------*/
#define COM_D 'D'               /* Set speed                                 */
            /*-------------------------------------------*/
#define	COM_E 'E'               /* Read speed                                */
            /*-------------------------------------------*/
#define COM_F 'F'               /* Configure the position PID controller     */
            /*-------------------------------------------*/
#define COM_G 'G'               /* Set position to the position counter      */
            /*-------------------------------------------*/
#define COM_H 'H'               /* Read position                             */
            /*-------------------------------------------*/
#define COM_I 'I'               /* Read I/A input                            */
            /*-------------------------------------------*/
#define COM_J 'J'               /* Configure the speed profile controller    */
            /*-------------------------------------------*/
#define COM_K 'K'               /* Read the status of the mottion controller */
            /*-------------------------------------------*/
#define COM_L 'L'               /* Change LED state                          */
            /*-------------------------------------------*/
#define COM_N 'N'               /* Read the proximity sensors                */
            /*-------------------------------------------*/
#define COM_O 'O'               /* Read ambient light sensors                */
            /*-------------------------------------------*/
#define COM_S 'S'               /* Read battery charge level                 */
            /*-------------------------------------------*/
#define COM_T 'T'               /* Send a message to an additional module    */
            /*-------------------------------------------*/
#define COM_R 'R'               /* Read a byte on the extensiion bus         */
            /*-------------------------------------------*/
#define COM_W 'W'               /* Write a byte on the extensiion bus        */
            /*-------------------------------------------*/
#define COM_Q 'Q'               /* Write a bit on a output                   */
            /*-------------------------------------------*/
#define COM_Y 'Y'               /* Read general digital input state          */
            /*-------------------------------------------*/
#define COM_Z 'Z'               /* Fonctions supplementaires                 */
            /*-------------------------------------------*/
#define COM_P 'P'
