#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################


##############################################################################################
# Script faisant appel aux scripts de creation des Makefile des differents programmes du projet
# Genere aussi un makefile pour pouvoir gerer les differentes compilations
##############################################################################################

source ../scripts/COMPILE_FLAG

ALL_CONFIGURATIONS=(debug release)
ALL_MODES=(gui blind)
ALL_PROJECTS=(SigProc IHM IO_Robot Parallel_Comm NN_IO Sensors promethe)
ALL_ACTIONS=(clean reset)

# Resets the previous Makefile
echo "" > Makefile


#default rule: compile all projects
echo -e "default: | $LOGDIR " >> Makefile
echo -e "\t\$(MAKE) all --jobs=$NUMBER_OF_CORES > $LOGDIR/prom_user_make.log\n" >> Makefile
echo -e "include ../scripts/variables.mk\n" >> Makefile

echo -e "all:\c" >> Makefile #\c pour ne pas aller à la ligne
for CONFIGURATION in ${ALL_CONFIGURATIONS[@]}
do 
	echo -e "${ALL_MODES[@]/%/_${CONFIGURATION}} \c" >> Makefile
done

echo -e "\n\trm -f $DIR_BIN_LETO_PROM/simulator" >> Makefile
echo -e "\tln -sf $SIMULATOR_PATH $DIR_BIN_LETO_PROM/simulator" >> Makefile

echo "" >> Makefile
echo "Creating dynamic lib makefiles ..."
./Create_dynamic_lib_Makefile.sh -enable-threads -enable-sim2d $* > $LOGDIR/Create_dynamic_lib_Makefile.log
echo "Creating promethe makefiles ..."
./Create_Promethe_Makefile.sh -enable-threads -enable-sim2d $* > $LOGDIR/Create_Promethe_Makefile.log

for CONFIGURATION in ${ALL_CONFIGURATIONS[@]}
do
	for MODE in ${ALL_MODES[@]}
	do
		echo "${MODE}_${CONFIGURATION}:${ALL_PROJECTS[@]/%/_${MODE}_${CONFIGURATION}}" >> Makefile #Pour chaque élement du tableau [@] on ajoute à la fin (/%) la chaine '_debug' ou '_release' (/_$CONFIGURATION).
		echo "" >> Makefile
	
		for PROJECT in ${ALL_PROJECTS[@]}
		do
			echo "${PROJECT}_${MODE}_${CONFIGURATION}:" >> Makefile
				if [ "$PROJECT" != "promethe" ]
				then
					echo -e "\t\$(MAKE) --file=src/$PROJECT/Makefile.lib${PROJECT}_${MODE}_${CONFIGURATION}" >> Makefile
				else
					echo -e "\t\$(MAKE) --file=Makefile.${PROJECT}_${MODE}_${CONFIGURATION}" >> Makefile
				fi 
				echo "" >> Makefile
		done
	done
done

for ACTION in ${ALL_ACTIONS[@]}
do
	echo "$ACTION:" >> Makefile
	for CONFIGURATION in ${ALL_CONFIGURATIONS[@]}
	do
		for MODE in ${ALL_MODES[@]}
		do
			for PROJECT in ${ALL_PROJECTS[@]}
			do
				if [ "$PROJECT" != "promethe" ]
				then
					echo -e "\t\$(MAKE) --file=src/$PROJECT/Makefile.lib${PROJECT}_${MODE}_${CONFIGURATION} $ACTION" >> Makefile
			else
					echo -e "\t\$(MAKE) --file=Makefile.${PROJECT}_${MODE}_${CONFIGURATION} $ACTION" >> Makefile
				fi 
			done
			echo "" >> Makefile
		done
	done
done

