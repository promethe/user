#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################

####################################################
#script permettant de generer un Makefile pour compiler promethe
#v2.0 Maillard M. & Baccon J.C.
#v3.0 Hirel J.
#v3.1 PG Simplification avec plus d'infos dans COMPILE_FLAG et meilleur support MACOS
###################################################

####################################################
#Definition des chemins d'acces, options de compile etc...
####################################################


ALL_CONFIGURATIONS=(debug release)
ALL_MODES=(gui blind)

for MODE in ${ALL_MODES[@]}
do
	for CONFIGURATION in ${ALL_CONFIGURATIONS[@]}
	do
		
	source ../scripts/COMPILE_FLAG

# Initialisation des libs, includes et flags
LIBS="$GTKLIB -L$LIBDIR $PNGLIB $JPEGLIB $ZLIB $FFTWLIB $SOUND $MXMLLIB -ldl $SIMULATOR_PATH/lib/$SYSTEM/enet/lib/libenet.a"
INCLUDES="$GTKINCLUDES -I$SIMULATOR_PATH/shared/include/ -I$PWD/include -I. -I$INCLUDE2 -I$SIMULATOR_PATH" 
#CFLAGS definis dans le COMPILE_FLAGS

# Gestion des parametres passes au Create_Makefile
CFLAGS="$CFLAGS -DFLAG_ENET"

echo "$*" | grep -q "-enable-firewire" 
if [ $? -eq 0 ]
then echo "compile $PROG_NAME with firewire"
    CFLAGS="$CFLAGS -DFIREWIRE"
    LIBS="$LIBS $DC1394LIB $RAW1394LIB "		
fi

echo "$*" | grep -q "-enable-player" 
if [ $? -eq 0 ]
then echo "compile $PROG_NAME with player"
    CFLAGS="$CFLAGS -DPLAYER"
    INCLUDES="$INCLUDES -I/usr/local/include/player-2.1/"
    LIBS="$LIBS -L/usr/local/lib/ $PLAYERLIB"
fi	

echo "$*" | grep -q "-enable-sim2d" 
if [ $? -eq 0 ]
then echo "compile $PROG_NAME with sim2d"
    CFLAGS="$CFLAGS -DSIM2D"
fi	

	if [ "$MODE" == "blind" ]
	then
		PROG_NAME="bpromethe"
		KERNELLIB="${KERNELLIB}_blind"
		IVYLIB="-livy"
		CFLAGS="$CFLAGS -DAVEUGLE"
		BLIND=1
	else
		PROG_NAME="promethe"
		IVYLIB="-lglibivy"
		BLIND=0
	fi
	
#	echo "$*" | grep -q "-enable-threads"
#	if [ $? -eq 0 ]
#	then echo "compile enable-threads..."
#   	KERNELLIB="${KERNELLIB}_threads"
#    	CFLAGS="$CFLAGS -DUSE_THREADS -DUSE_THREADS_2"
#	else echo "compile disable-threads..."    
#	fi


#	echo "$*" | grep -q "-disable-threads" 
	echo "$*" |  grep -q -e "-disable-threads" 	
	if [ $? -eq 0 ]
	then 
		echo "compile disable-threads..." 
	else 
		echo "compile enable-threads..."
    	KERNELLIB="${KERNELLIB}_threads"
    	CFLAGS="$CFLAGS -DUSE_THREADS -DUSE_THREADS_2"
	fi	

	if [ "$CONFIGURATION" == "debug" ]
	then echo "compile debug..."
		HARDWARELIB="${HARDWARELIB}_debug"
		KERNELLIB="${KERNELLIB}_debug"
		PROG_NAME="${PROG_NAME}_debug"
		CFLAGS="$CFLAGS $FLAGS_DEBUG"
		OSCILLOKERNELLIB="${OSCILLOKERNELLIB}_debug"
	else echo "compile release..."
		PROG_NAME="${PROG_NAME}"
		CFLAGS="$CFLAGS $FLAGS_OPTIM"
	fi	

		
		#PG: il faudrait ramener ce test dans le COMPILE_FLAG pour eviter que le fichier ne depende de la machine
		echo "$SYSTEM" | grep -q "Darwin"
		if [ $? -eq 0 ]
		then
			FINALLIBS="-force_load $KERNELLIBPATH/lib${KERNELLIB}.a -force_load $HARDWARELIBPATH/lib${HARDWARELIB}.a  $BLCLIBPATH/lib${BLCLIB}.a -framework CoreServices"	
		    LIBDYNFLAGS=""

			if [[ $BLIND = 0 ]]
			then
				FINALLIBS="$FINALLIBS -force_load $GRAPHICLIBPATH/lib${GRAPHICLIB}.a"
			fi
		else		
			FINALLIBS="-Wl,--whole-archive -L$KERNELLIBPATH -l$KERNELLIB -L$HARDWARELIBPATH -l$HARDWARELIB"
			if [[ $BLIND = 0 ]]
			then
				FINALLIBS="$FINALLIBS -L$GRAPHICLIBPATH -l$GRAPHICLIB -lX11"
			fi
			FINALLIBS="$FINALLIBS -Wl,--no-whole-archive"
		fi 
	
#		FINALLIBS="$FINALLIBS -L$SCRIPTLIBPATH $SCRIPTLIB -L$IVYLIBPATH $IVYLIB -lpcre $LIBS $MXMLLIB -lm -L $HOME/.local/lib -lblc"
		FINALLIBS="$FINALLIBS -L$SCRIPTLIBPATH $SCRIPTLIB -L$IVYLIBPATH $IVYLIB -lpcre $LIBS $MXMLLIB -lm -L $BLCLIBPATH -l${BLCLIB}  -Wl,-rpath,\\$\$ORIGIN/Libraries" #the -Wl,rpath,... allow to specify where to find the dynamic lib at runtine. $ORIGIN is a specific keyword for rpath and we need to protect $ from replacement by bash and make !!
		

		if [ "$MODE" == "gui" ] 
		then 
			FINALLIBS="$FINALLIBS -L$OSCILLOKERNELLIBPATH $OSCILLOKERNELLIB "			
		fi
	
		FINALCFLAGS="$CFLAGS -DUSE_FLOAT_PRECISION_FFT"
		LIBDYNFLAGS=" -rdynamic"
		
		#Les repertoires de destination des fichiers compiles
		OBJDIR="$OBJPATH/$PROG_NAME"
		
		#Gestion des fichiers a compiler
		#SOURCES=`find src/ -depth -type f -name '*.c' | grep -e 'main.c' -e 'src/public_tools' -e 'src/NN_Core'`
		SOURCES=`find src/ -depth -type f -name '*.c' | grep -e 'main.c'`
		SOURCES2=`find src/public_tools -depth -type f -name '*.c' | grep -e 'src/public_tools'`
		SOURCES3=`find src/NN_Core -depth -type f -name '*.c' | grep -e 'src/NN_Core'`
		SOURCES="$SOURCES $SOURCES2 $SOURCES3"
		
		OBJECTS=""
		
		####################################################
		#Creation du Makefile
		####################################################
		
		MAKEFILE="Makefile.promethe_${MODE}_${CONFIGURATION}"
		
		#ecrasement du Makefile precedent
		echo "" > $MAKEFILE
		echo -e "default:all\n" >> 	$MAKEFILE
		echo -e "include $SIMULATOR_PATH/scripts/variables.mk\n" >> $MAKEFILE
		echo -e ".PHONY:clean reset default all FORCE\n" >> 	$MAKEFILE
		
		echo "lib${KERNELLIB}:" >> 	$MAKEFILE
		echo -e "\tcd ${SIMULATOR_PATH}/prom_kernel; \$(MAKE) \$@\n" >> $MAKEFILE
		
		#Mettre la version SVN a chaque compilation
		echo -e 'SVN_REVISION:=-DSVN_REVISION="$(shell svnversion -n .)"' >> $MAKEFILE
		echo -e "FORCE:" >> $MAKEFILE
		echo -e "$OBJDIR/main.o:FORCE" >> 	$MAKEFILE
				
		#regle par defaut
		echo "all: $PROG_NAME" >> $MAKEFILE
		echo "" >> $MAKEFILE
		
		echo "$OBJDIR:" >> $MAKEFILE
		echo -e "\tmkdir -p \$@\n" >> $MAKEFILE
		
		echo "$BINDIR:" >> $MAKEFILE
		echo -e "\tmkdir -p \$@\n" >> $MAKEFILE
		
		# creer les regles
		#pour chaque  .o
		for i in $SOURCES
		do
		    echo "processing '$i'"
		    FICHIER=`basename $i .c`
		    CHEMIN=`echo $i | sed -e s@$FICHIER.c@@`
		    echo "$OBJDIR/$FICHIER.o:$i | $OBJDIR" >> $MAKEFILE
		    echo -e "\techo \"[processing $i ...]\"">> $MAKEFILE
		    echo -e "\t($CC \$(SVN_REVISION) $FINALCFLAGS $INCLUDES -c -o $OBJDIR/$FICHIER.o $i)">> $MAKEFILE
		    echo "" >> $MAKEFILE
		    OBJECTS="$OBJECTS $OBJDIR/$FICHIER.o"
		done
		

		echo "$PROG_NAME: ${BINDIR} ${OBJECTS} lib${KERNELLIB} | $BINDIR" >> $MAKEFILE
		echo -e "\techo \"[Link objects...]\"" >> $MAKEFILE
		echo -e "\t$CC $FLAG_LINK $OBJECTS -o $BINDIR/$PROG_NAME  $FINALLIBS $LIBDYNFLAGS "  >> $MAKEFILE
		echo -e "\tcp -f $BINDIR/$PROG_NAME ${DIR_BIN_LETO_PROM}/$PROG_NAME " >> $MAKEFILE
		
		echo "" >> $MAKEFILE
		
		
		#règles additionnelles
		echo "clean:" >> $MAKEFILE
		echo -e "\trm -f  $OBJDIR/*.o $BINDIR/$PROG_NAME " >> $MAKEFILE
		echo "" >> $MAKEFILE
		
		echo "reset: clean" >> $MAKEFILE
		echo -e "\trm -f  $DIR_BIN_LETO_PROM/$PROG_NAME" >> $MAKEFILE
		echo "" >> $MAKEFILE

	done
done

