/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _PROM_SEQ_H
#define _PROM_SEQ_H

#include <sys/types.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <libx.h>
#include <errno.h>

#include <Typedef/liste_chaine.h>


extern int max_ech_temps_main;

/*Code pour verifier si les threads tourne bien sur les proc desire*/
/* #define DEBUG_CPU*/

/*Choix du proc au debut du prog en fonction du num ou de l'echelle*/
 #define SEQ_INIT  

/*Choix du proc alterne, toutes nouvelles exec est 1 fois vers l'un, une fois vers l'aure proc: pb du nombre de migration*/
/* #define SEQ_ONLINE */

/*Choix du processeur dernierement libere: pas terrible*/
/* #define SEQ_ONLINE_FIN*/

/*Choix processeur moins charge*/
/* #define SEQ_LOAD*/

double waste_time(long n);


/*extern int cpt;
extern int moy;*/

#ifdef SEQ_ONLINE
extern struct PromJeton *  proc_jeton;
#endif

#ifndef AVEUGLE

int boite_x_min[255];
int boite_y_min[255];
int boite_x_max[255];
int boite_y_max[255];
int x_ech_start[255];
int y_ech_start[255];
int x_ech_stop[255];
int y_ech_stop[255];

#endif

void init_sequenceur_autonome(void);
void init_debug(void);

typedef struct PromJeton
{
 pthread_mutex_t mutex;
 int depart;
 int arrivee;
} PromJeton;

PromJeton * new_prom_jeton(int depart,int arrivee);
void prom_jeton_lock(PromJeton*);
void prom_jeton_trylock(PromJeton*);
void prom_jeton_unlock(PromJeton*);
void debug_jeton(PromJeton*jeton,int mode);
void prom_jeton_destroy(PromJeton*);



typedef struct PromFifo
{
/*Contient les tableaux des groupe, des groupe devant etre initialised et des fin_de_boucle*/
    liste_chaine * to_execute;
    liste_chaine ** waiting;
    PromJeton * locker_modify;
    PromJeton * locker_empty;
    int size;
    int nb_ele;
    int nb_gpes;
} PromFifo;

typedef struct PromFinBoucle
{
    int proc;
    int echelle;
 
    int breaking;

    int nb_jeton_in;
    int nb_jeton_blocked;

    int nb_gpes_out;
    struct PromGroupe ** gpes_out;

    int nb_gpes_extra;
    struct PromGroupe ** gpes_extra;

    int nb_gpes_intra;
    struct PromGroupe ** gpes_intra;

    int nb_gpes_perfin;
    struct PromGroupe ** gpes_perfin;

    struct PromFinBoucle * pfb;
    int    nb_pfb;

    int cpt_echelle;

     PromJeton * want_to_unlock;

    PromFifo * fifo;

} PromFinBoucle;

void prom_finboucle_go_autonome(PromFinBoucle * pfb);
void prom_finboucle_go(PromFinBoucle * pfb);
PromFinBoucle * new_prom_finboucle (int ec);
void prom_fb_unlock(PromFinBoucle * pfb);
void prom_fb_lock(PromFinBoucle * pfb);
void pfb_display(PromFinBoucle*pfb);

void prom_fb_execute ( PromFinBoucle* pfb ) ;
void prom_fb_break ( PromFinBoucle* pfb ) ;
void prom_fb_set_fifo(PromFinBoucle*pfb,PromFifo*fifo);


typedef struct PromGroupe
{
 int proc;
 
 int num_groupe;
 int type_groupe;
 int nb_jeton_in;
 int nb_jeton_blocked;
 int nb_gpes_out;

/*Est ce groupe n'ayant pas de fin ou allant vers la sortie*/
 int nb_pfb;

/*Le type est promGroupe normalement*/
 struct PromGroupe ** gpes_out;

 PromFinBoucle * pfb;
/*  PromFinBoucle * pfb_break;*/
 
 /*Jeton pour se declarer a un groupe*/
  PromJeton * want_to_unlock;

/*Jeton pour l'execution*/

/*Jeton de demarrage*/


  PromFifo * fifo;

} PromGroupe;

void prom_gpe_go_autonome(  PromGroupe * prom_gpe);
void prom_gpe_go(PromGroupe *  prom_gpe);
PromGroupe * new_prom_groupe(int i,PromFinBoucle*);
void prom_gpe_trylock(PromGroupe *  prom_gpe,int i);
void pg_display(PromGroupe*pg);

void prom_gpe_lock(PromGroupe *  prom_gpe);
void prom_gpe_unlock(PromGroupe *  prom_gpe);
void prom_groupe_execute(PromGroupe*);
int  prom_groupe_break(int gpe);
void prom_gpe_set_fifo(PromGroupe*pgpe,PromFifo*fifo);

typedef struct PromSequenceur
{
/*Contient les tableaux des groupe, des groupe devant etre initialised et des fin_de_boucle*/
    int nb_prom_gpe;
    PromGroupe ** prom_gpes;

    int nb_gpes_init;
    PromGroupe ** prom_gpes_init;

    int nb_fin_boucle;
    PromFinBoucle ** prom_finboucle;

    PromFifo * fifo;

} PromSequenceur;

PromSequenceur *  new_prom_sequenceur(void);
void  prom_sequenceur_init_tab_jeton(PromSequenceur*);
void prom_sequenceur_init_jeton(PromSequenceur * prom_seq);
void prom_sequenceur_go(PromSequenceur * prom_sequenceur);

typedef struct SeqArg
{
	int proc;
	PromSequenceur * prom_seq;
} SeqArg;

void prom_seq_thread(SeqArg*seqarg);

typedef struct PromThread
{
/*Contient les tableaux des groupe, des groupe devant etre initialised et des fin_de_boucle*/
    int proc;
    PromSequenceur * prom_seq;
    PromJeton * started; /*bloque au demarrage, il se rebloque pour tous les thread avant les run et se debloque qd le thread es pret a run*/
    PromJeton * run; /*bloque au demarage, il se debloque pour run qd tous les thread sont pret*/
    PromJeton * waiting; /*bloque au demarrage, il se rebloque qd un thread arrive sur la file vide. Il est relance par le write dans la fifo*/
	

} PromThread;

PromThread * new_prom_thread(PromSequenceur * ps,int proc);
void prom_thread_init(PromThread*pth);
void prom_thread_run(PromThread*pth);


PromFifo *  new_prom_fifo(int size,int nb_gpes);
int  prom_fifo_read(PromFifo*pfifo,PromThread*);
void prom_fifo_write(PromFifo*pfifo,int);












#endif
