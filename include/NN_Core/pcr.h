/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _NN_CODE_PCR_H
#define _NN_CORE_PCR_H

/* Structure contenant les donnees specifiques a PCR.
 */
typedef struct t_PCR_Data {
  int active_input;

  float s_temps;
  /* Matrices des activites intégrees                                         */
  float *I;
  float *O;
  float *IO;
  /* Dimension des matrices I, O                                              */
  int I_first_elem;
  int I_nb_elem;
  int I_width;
  int I_height;
  int O_first_elem;
  int O_nb_elem;
  int O_width;
  int O_height;
  /* Liens de neuromodulation                                                 */
  noeud_modulation *modulation_activation;
  noeud_modulation *modulation_choc;
  noeud_modulation *modulation_satisfaction;
  /* Dernier renforcement recu                                                */
  float last_reinf;
  /* L'apprentissage et le calcul des correlations doit-il être inversé ?     */
  int correlation_computing_after;
} PCR_Data;


/* Fonctions relatives à PCR
 */
void new_PCR(int index_of_group);
void update_neural_activity_PCR(int index_of_neuron, int gestion_STM, int learn);
void update_group_PCR(int index_of_group);
void learn_PCR(int index_of_group);


/* Implémentation de l'algorithme P.C.R. [Revel 97]
 * @param int le numero de groupe concerne.
 */
void function_PCR(int);

/* Fonction permettant d'obtenir une valeur ???
 * @param float une valeur de poids synaptique
 * @return float une valeur ??
 */
float PCR_Fn(float);

/** Fonction de seuillage
 * @param init la valeur initiale
 * @return 1 si value > 1, 0 si value < 0, sinon value
 */
float PCR_H(float init);

#endif
