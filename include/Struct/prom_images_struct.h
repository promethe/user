/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _PROM_IMAGES_STRUCT_H
#define _PROM_IMAGES_STRUCT_H

#ifndef NB_MAX_IMAGES_IN_PROM_IMAGES
#define NB_MAX_IMAGES_IN_PROM_IMAGES 2048
#endif

/* The structure for stroring many images (16) */
#define PROM_IMAGES_STRUCT_HEAD_SIZE (sizeof(prom_images_struct) -  sizeof(unsigned char *[NB_MAX_IMAGES_IN_PROM_IMAGES]))

/* The structure for stroring many images (16) */
#define PROM_IMAGES_STRUCT_COMPRESSED_HEAD_SIZE (sizeof(prom_images_struct) -  sizeof(unsigned char *[NB_MAX_IMAGES_IN_PROM_IMAGES]) - sizeof(unsigned int[NB_MAX_IMAGES_IN_PROM_IMAGES]))


typedef struct prom_images_struct_def
{
    int image_number;  /*How many images in the structure */
    int sx;            /*Images size */
    int sy;
    int nb_band;       /*=3 for color(3*8bit) ; =1 for grayscale(8bit) ; 4 for grayscale in float !!! */
    unsigned char *images_table[NB_MAX_IMAGES_IN_PROM_IMAGES];  /*The table stores the images. This is not send. It is not in the head*/
} prom_images_struct;

typedef struct prom_images_struct_compressed_def{
	prom_images_struct super;

	unsigned int images_size[NB_MAX_IMAGES_IN_PROM_IMAGES];
}prom_images_struct_compressed;

prom_images_struct *calloc_prom_image(int im_number, int x, int y, int b);


#endif
