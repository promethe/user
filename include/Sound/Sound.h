/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _SOUND_H
#define _SOUND_H

#ifndef _f_audio
#define _f_audio
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef NO_FFT
#include "fftw3.h"
#endif

#include <math.h>
#include <stdio.h>

#ifdef Linux
#include <malloc.h>
#endif

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#ifndef NO_ALSA
#include <alsa/asoundlib.h>
#endif

#include <assert.h>
#include <sys/poll.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/signal.h>
#include "formats.h"
#include "version.h"
#ifndef _SQ
#define _SQ
/* Macro pour calculer X au carre */
#define sq(X) pow(X,2)
#endif

#ifndef PR
#define PR
#define pr(X) printf(X)

#define endl printf("\n")
#endif
#define DEFAULT_SPEED 		8000

#define FORMAT_DEFAULT		-1
#define FORMAT_RAW		0
#define FORMAT_VOC		1
#define FORMAT_WAVE		2
#define FORMAT_AU		3

/* global data */

#ifndef NO_ALSA
snd_pcm_sframes_t(*readi_func) (snd_pcm_t * handle, void *buffer,
                                snd_pcm_uframes_t size)
    __attribute__ ((unused));
snd_pcm_sframes_t(*writei_func) (snd_pcm_t * handle, const void *buffer,
                                 snd_pcm_uframes_t size)
    __attribute__ ((unused));
snd_pcm_sframes_t(*readn_func) (snd_pcm_t * handle, void **bufs,
                                snd_pcm_uframes_t size)
    __attribute__ ((unused));
snd_pcm_sframes_t(*writen_func) (snd_pcm_t * handle, void **bufs,
                                 snd_pcm_uframes_t size)
    __attribute__ ((unused));
#else
#define snd_pcm_stream_t void *
#define snd_pcm_uframes_t void *
#define snd_output_t void *
#define off64_t void *
#define snd_pcm_t void *
#define snd_pcm_format_t void *
#define SND_PCM_STREAM_CAPTURE 0
#endif

char *command __attribute__ ((unused));
snd_pcm_t *handle __attribute__ ((unused));
struct
{
    snd_pcm_format_t format;
    unsigned int channels;
    unsigned int rate;
} __attribute__ ((unused)) hwparams, __attribute__ ((unused)) rhwparams;
/*Time limit 
*/

/*Structure audio	*/
typedef struct type_audio
{

    char *trame;                /*declare en char type par defaut de l'acquisition */
    int taille;                 /*taille des trames */
    int sample_rate;            /*taux d'echantillonnage par defaut 8kHz; non usite pour le moment */
    int sample_size;            /*taille de lm'echantillon** */
    int sample_format;
          /**  Format des echantillons**/
    int channels;
         /***Nombre de canaux utilisees  mono ou stereo**/
} prom_audio_struct;

#ifndef NO_FFT

typedef struct type_fft_1d_audio
{

    float *in;                  /*trame en flottant pour la fft */
    fftwf_complex *out;         /*sortie de la fft */
    fftwf_plan *fft_plan;       /*plan de traitement de la fft */
    int taille;
} fft_1d_audio;

#endif

/* needed prototypes */
void getbuf(char *buf);
void capture_end();

int saveto(FILE * fd, char *c) __attribute__ ((unused));
int f_audio_capture(char *buf) __attribute__ ((unused));
int f_audio_close() __attribute__ ((unused));
int f_audio_init() __attribute__ ((unused));
int f_audio_close() __attribute__ ((unused));
void function_close_sound(int numero);
void function_get_sound(int numero);
void function_init_sound(int numero);
void function_spectro_1d(int numero);
void function_assoc_timing(int numero);
void function_mean_sound(int numero);
void function_compute_cr(int numero);
void function_traite_context(int numero);
void function_context(int numero);
void function_init_fft_audio(int numero);
void function_fft_audio_execute(int numero);
void function_dsp_audio(int numero);
void function_play_sound(int gpe);
void function_read_audio(int Gpe);
void new_read_audio(int Gpe);
void destroy_read_audio(int gpe);

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
#define error(...) do {\
	fprintf(stderr, "%s: %s:%d: ", command, __FUNCTION__, __LINE__); \
	fprintf(stderr, __VA_ARGS__); \
	putc('\n', stderr); \
} while (0)
#else
#define error(args...) do {\
	fprintf(stderr, "%s: %s:%d: ", command, __FUNCTION__, __LINE__); \
	fprintf(stderr, ##args); \
	putc('\n', stderr); \
} while (0)
#endif
#endif


#ifndef f_audio_fft
#define f_audio_fft
#ifndef PR
#define PR
#define pr(X) printf(X);printf("\n");

#define endl printf("\n")

#endif
int audiofft(char *buf, int l, int num) __attribute__ ((unused));


char *itoa(int val, int base);

#endif

void new_sound_get_signal(int gpe);
void function_sound_get_signal(int gpe);
void destroy_sound_get_signal(int gpe);

void function_fft_signal(int Gpe);
void new_fft_signal(int Gpe);
void new_energy(int Gpe);
void destroy_energy(int gpe);
void function_energy(int Gpe);

#endif
