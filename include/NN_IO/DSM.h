#ifndef _NN_IO_DSM_H
#define _NN_IO_DSM_H



//#define DEBUG_GRAPH


#include <float.h>
#include <limits.h>


#ifdef DEBUG_GRAPH
   #include <graphviz/gvc.h>
#endif


#define KCYN  "\x1B[36m "	/* cyan */
#define KGRN  "\x1B[32m "	/* green */
#define KWHT  "\x1B[37m "	/* white : NORMAL */
#define KRED  "\x1B[31m "	/* red */
#define KMAG  "\x1B[35m "	/* magenta */
#define KYEL  "\x1B[33m "	/* yellow */

#define COLOR_THRES  KCYN
#define COLOR_CATEGO  KMAG
#define COLOR_ASSO  KRED
#define COLOR_OUTPUT  KWHT
#define COLOR_MAIN  KMAG
#define COLOR_PRESC  KRED

/* SLE output */
#define DEB_SUM_PRED 0
#define DEB_FORCE_NEW_S_POS 1 /* s:force presc ok     s1:force new s     s2:sign recruit bab */
#define DEB_PRES_ACHIEVED 2
#define DEB_EFF 3
#define DEB_M_PRESC 4
#define DEB_S_PRESC 5

#define VMAX 0.01  /* Debug! */

#define DEF_VAL_PRESC -0.42  /* Debug! */

// SHM
#define CLEF 1111 /* random value */

#define COUNT_ITERATIONS 1000 /* Count when no mvt?  */
#define COUNT_NO_RECRUIT 100 /* Count when no mvt?  */
#define COUNT_NO_EP 100 /* Timeout : no EP -> could stop every presc! */

#define TH_ACT_VAL_0 0.25

#define RECRUIT_PRIORITY 1000
#define PRIORITY_EP 1      /* //RECRUIT_PRIORITY, to detect external motivation prescriptions */

#define M_EPS 0.0001

#define SLE_TOLERANCE 2
#define FACT_THRES_MVT 0.25
#define FACT_DELT_VAL_PRESC 3

#define NB_NO_SLE 4;




typedef struct type_recruit_bab
{
	int is_recruiting;
	int sensor;
	int motor;
	int sign;
} type_recruit_bab;




void new_SLE_evaluate_threshold(int gpe);
void function_SLE_evaluate_threshold(int gpe);
void destroy_SLE_evaluate_threshold(int gpe);

/* OLD VERSION */
//void new_DSM_catego(int gpe);
//void function_DSM_catego(int gpe);
//void destroy_DSM_catego(int gpe);

void new_SLE_catego_DSM(int gpe);
void function_SLE_catego_DSM(int gpe);
void destroy_SLE_catego_DSM(int gpe);

void new_SLE_catego_SW(int gpe);
void function_SLE_catego_SW(int gpe);
void destroy_SLE_catego_SW(int gpe);


void new_SLE_asso(int gpe);
void function_SLE_asso(int gpe);
void destroy_SLE_asso(int gpe);

void new_DSM_output(int gpe);
void function_DSM_output(int gpe);
void destroy_DSM_output(int gpe);

void new_DSM_main(int gpe);
void function_DSM_main(int gpe);
void destroy_DSM_main(int gpe);

void new_DSM_motor_learn(int gpe);
void function_DSM_motor_learn(int gpe);
void destroy_DSM_motor_learn(int gpe);


#endif
