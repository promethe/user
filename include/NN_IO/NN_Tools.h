/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _NN_IO_NN_TOOLs_H
#define _NN_IO_NN_TOOLs_H

void new_ext_to_gpe(int gpe);
void function_ext_to_gpe(int gpe);
void destroy_ext_to_gpe(int gpe);
void function_gpe_to_ext(int Gpe);
void function_normalisation_c(int gpe_sortie);
void function_baricentre(int);
void function_pruning_input_group(int gpe_sortie);
void function_max_ana(int gpe_sortie);
void function_contraste_max(int gpe_sortie);
void function_product(int numero);
void function_from_ana(int gpe_sortie);
void function_extract_neuron(int numero);
void function_extract_neurons(int Gpe);
void function_extract_population(int numero);
void function_extract_coeffs(int gpe);

void new_extract_val_to_vector(int gpe);
void function_extract_val_to_vector(int gpe);
void destroy_extract_val_to_vector(int gpe);

void new_extract_multi_vals_to_one_vector(int gpe);
void function_extract_multi_vals_to_one_vector(int gpe);
void destroy_extract_multi_vals_to_one_vector(int gpe);

void new_extract_coeffs(int gpe);
void destroy_extract_coeffs(int gpe);
void function_analogize(int gpe);
void new_analogize(int gpe);
void destroy_analogize(int gpe);
void function_discretize(int gpe);
void new_discretize(int gpe);
void destroy_discretize(int gpe);
void function_switch_input(int gpe);
void function_bruit(int numero);
void function_bruit_sur_signal(int numero);
void function_concatenation(int Gpe);
void function_instar_gpe(int Gpe);
void new_instar_dynamic_gpe(int Gpe);
void function_instar_dynamic_gpe(int Gpe);
void destroy_instar_dynamic_gpe(int Gpe);
void function_sigmo_norm_instar(int Gpe);
void function_normalize_(int Gpe);
void function_inverse_gpe_population(int Gpe);
void function_im_to_gpe(int Gpe);
void function_seqN_WTA(int Gpe);
void function_x_1(int gpe);
void function_s_to_s1(int gpe);
void function_s_winner_to_s1 (int gpe);
void new_z_1(int gpe_sortie);
void function_z_1(int gpe_sortie);
void destroy_z_1(int gpe_sortie);
void new_z_n(int gpe_sortie);
void function_z_n(int gpe_sortie);
void destroy_z_n(int gpe_sortie);
void function_switch_input(int gpe);
void function_switch_2groups(int gpe);
void function_write_conditionnal(int gpe);
void f_pondere_puissance(int gpe);
void function_normalize(int gpe);
void function_normalize_PC(int gpe);
void function_norm_ana(int gpe_sortie);
void function_ask_planification(int gpe_sortie);
void function_neurone_aleatoire(int Gpe);
void function_test_results(int numero);
void new_random_variable(int gpe);
void function_random_variable(int gpe);
void destroy_random_variable(int gpe);
void function_hebb_seuil(int numero);
void f_transpose(int numero);
void function_neurone_seq(int gpe);
void function_neurone_seq2(int gpe);
void new_neurone_seq_AR(int gpe);
void function_neurone_seq_AR(int gpe);
void new_neurone_seq_param(int Gpe);
void function_neurone_seq_param(int Gpe);
void destroy_neurone_seq_param(int Gpe);
void function_erreur(int gpe);
void function_pos_offset(int num);
void function_ext_plus_neurone(int numero);
void function_demande_intensite_expression(int numero);
void function_ambigous_states_learning(int numero);
void function_1D_TO_2D(int numero);
void function_grad_to_max(int num);
void function_telecommande(int gpe);
void function_valeur_emotionnelle(int gpe);
void function_get_potential(int num); 
void function_neutre(int gpe);
void function_get_orientation(int numero);
void f_get_angle_from_orientation(int numero);
void function_supervision(int Gpe);
void new_supervision(int Gpe);
void function_population_to_neuron(int numero);
void function_matrice_alea_entre_0_1(int numGroupe);
void function_reset_integr(int gpe_sortie);
void function_reset_but(int gpe_sortie);
void new_nb_expressions(int gpe);
void function_nb_expressions(int gpe);
void function_simulation_status(int numero);
void function_set_zero(int numero);
void function_read_and_map_angle_ana(int numero);
void function_moyenne_oneshot(int);
void function_moyenne_etat(int);
void f_previous_ext_to_neurone_at_this_position(int num);
void f_shift_vector(int numero);
void function_moyenneur(int gpe);
void function_detection_nouveaute(int gpe_sortie);
void function_all_n_link(int numero);
void function_sum_angle(int numero);
void f_keep_last_active_neuron(int numero);
void new_esn(int gpe);
void function_esn(int gpe);
void function_sort(int gpe);
void new_sort(int gpe);
void destroy_sort(int gpe);
void function_instar_multi_gpe(int gpe);
void function_norm_euclidienne(int gpe);
void function_qsort(int gpe);
void new_qsort(int gpe);
void destroy_qsort(int gpe);
void function_exponential(int gpe);
void function_merge_potentials(int num); 
void function_choose_input(int gpe);
void new_choose_input(int gpe);
void destroy_choose_input(int gpe);

//void function_nelder_f1(int Gpe);
//void new_nelder_f1(int Gpe);
//void destroy_nelder_f1(int Gpe);

void function_nelder_mead(int Gpe);
void new_nelder_mead(int Gpe);
void destroy_nelder_mead(int Gpe);

void function_calcul_distance(int Gpe);
void new_calcul_distance(int Gpe);
void destroy_calcul_distance(int Gpe);

void function_points_Of_Interest(int Gpe);
void new_points_Of_Interest(int Gpe);
void destroy_points_Of_Interest(int Gpe);


void new_compet_inter_cartes(int gpe);
void function_compet_inter_cartes(int gpe);
void destroy_compet_inter_cartes(int gpe);
void function_alpha_cell(int gpe);
void destroy_alpha_cell(int gpe);
void new_alpha_cell(int gpe);

void new_random_bytes(int Gpe);
void destroy_random_bytes(int gpe);
void function_random_bytes(int Gpe);

void new_concatenation_iter(int gpe);
void function_concatenation_iter(int gpe);
void destroy_concatenation_iter(int gpe);



#endif
