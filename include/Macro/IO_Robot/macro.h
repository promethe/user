/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _MACRO_IO_ROBOT_MACRO_H
#define _MACRO_IO_ROBOT_MACRO_H

/*
#define MAX_DOF_KATANA_ARM 	5
#define DATA_MAX		100
#define Freezed			0
#define Free			1*/

/*
#define CAMSTART 10
#define CAMSTOP 215
*/
#define COM_A 'A'               /* Configure                            */
            /*--------------------------------------*/
#define COM_B 'B'               /* Read software version                */
            /*--------------------------------------*/
#define COM_C 'C'               /* Set a position to be reached         */
            /*--------------------------------------*/
#define COM_D 'D'               /* Set speed                            */
            /*--------------------------------------*/
#define	COM_E 'E'               /* Read speed                           */
            /*--------------------------------------*/
#define COM_F 'F'               /* Configure the position PID controller */
            /*--------------------------------------*/
#define COM_G 'G'               /* Set position to the position counter */
            /*--------------------------------------*/
#define COM_H 'H'               /* Read position                        */
            /*--------------------------------------*/
#define COM_I 'I'               /* Read I/A input                       */
            /*--------------------------------------*/
#define COM_J 'J'               /* Configure the speed profile controller */
            /*--------------------------------------*/
#define COM_K 'K'               /* Read the status of the mottion controller */
            /*--------------------------------------*/
#define COM_L 'L'               /* Change LED state                     */
            /*--------------------------------------*/
#define COM_N 'N'               /* Read the proximity sensors           */
            /*--------------------------------------*/
#define COM_O 'O'               /* Read ambient light sensors           */
            /*--------------------------------------*/
#define COM_T 'T'               /* Send a message to an additional module */
            /*--------------------------------------*/
#define COM_R 'R'               /* Read a byte on the extension bus     */
            /*--------------------------------------*/
#define COM_W 'W'               /* Write a byte on the extension bus    */
            /*--------------------------------------*/
#define COM_Q 'Q'               /* Write a bit on a output              */
            /*--------------------------------------*/
#define COM_P 'P'               /* Set the desired PWM amplitude        */
            /*--------------------------------------*/
#define COM_Y 'Y'               /* Read the digital inputs              */
            /*--------------------------------------*/
#define COM_Z 'Z'               /* User Defined     */
            /*--------------------------------------*/

#define ANGLE90 5350            /*N.C:New value for CMD_C with grey robot, Old value:7300 */

#define nb_motivations 2
#endif
