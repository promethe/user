/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _SIGPROC_MATH_H_
#define _SIGPROC_MATH_H_

void function_minimum(int gpe);
void new_interpolation(int gpe);
void function_interpolation(int gpe);
void function_interpolation_lineaire(int gpe);
void new_interpolation_lineaire(int gpe);
void destroy_interpolation_lineaire(int gpe);
void function_moyenne(int);
void function_derivation_t(int gpe_sortie);
void function_runge_kutta(int gpe);
void function_lissage_temp(int num);

void function_shift_colonne(int Gpe);
void function_find_scale_space_keypoints(int Gpe);
void function_learn_sensori_motor(int Gpe);

void function_statistics_one(int Gpe);
void new_statistics_one(int Gpe);
void new_statistics(int Gpe);
void function_statistics(int Gpe);

void function_calibre_perspective(int);
void function_calibre_perspective_wl(int);
void function_calibre_traj_optimal(int gpe);

void function_ecart_type(int);
void new_ecart_type(int);
void destroy_ecart_type(int gpe);

void function_perspective_to_2D(int);
void function_perspective_to_2D_wl(int);

void function_erreur_moyenne(int numero);
void function_deriv_erreur(int num);
void function_erreur_prediction(int numero);


void new_stats(int Gpe);
void function_stats(int Gpe);
void destroy_stats(int Gpe);


void new_stats_distance(int Gpe);
void function_stats_distance(int Gpe);
void destroy_stats_distance(int Gpe);


void new_non(int Gpe);
void function_non(int Gpe);
void destroy_non(int Gpe);

void new_threshold(int Gpe);
void function_threshold(int Gpe);
void destroy_threshold(int Gpe);

void new_gaussian2D(int Gpe);
void function_gaussian2D(int Gpe);
void destroy_gaussian2D(int Gpe);

void function_norm_vecteur(int gpe);

#endif
