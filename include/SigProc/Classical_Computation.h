/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _SIGPROC_CL_COMP_H
#define _SIGPROC_CL_COMP_H

void function_sum_no_seuil(int gpe);
void new_double_threshold(int gpe);
void function_double_threshold(int gpe);
void destroy_double_threshold(int gpe);

void function_double_threshold2(int gpe);
void function_digital_freq_divider(int gpe);
void function_band_limit(int gpe);
void function_saturation(int gpe);
void function_minimum_limit(int gpe);
void function_product_term_to_term(int gpe);
void function_sum_bearing(int gpe);
void function_multiply(int gpe);
void function_valeur_absolue(int gpe);
void function_angular_distance(int gpe);
void function_distance(int gpe);
void function_modulo(int gpe);
void function_distance_euclidienne(int gpe);
void function_inverse_neuron(int gpe);
void function_divide(int gpe);
void new_divide(int gpe);
void destroy_divide(int gpe);
void function_diff_time(int gpe);
void new_diff_time(int gpe);
void destroy_diff_time(int gpe);
void new_random_time(int gpe);
void function_random_time(int gpe);
void destroy_random_time(int gpe);

void function_sqrt(int gpe);

void function_bias(int numero);
void function_sum(int numero);
void function_NONOU(int numero);

void new_sinus(int numero);
void function_sinus(int numero);
void destroy_sinus(int numero);

void function_bille2D(int numero);
void destroy_bille2D(int numero);
void new_bille2D(int numero);

void function_init_direction(int numero);
void destroy_init_direction(int numero);
void new_init_direction(int numero);

void new_cosinus(int numero);
void function_cosinus(int numero);
void destroy_cosinus(int numero);
void new_wave(int numero);
void function_wave(int numero);
void destroy_wave(int numero);

void new_shift(int numero);
void function_shift(int numero);
void function_shift_compass(int numero);
void function_neural_shift_compass(int numero);
void function_neural_shift_compass_inv(int numero);

void function_shift_bords(int Gpe);

void function_d_phi(int Gpe);
void function_d_phi2(int Gpe);

void new_appraise(int gpe);
void function_appraise(int gpe);
void destroy_appraise(int gPe);

void new_matricial_product(int gpe);
void function_matricial_product(int gpe);
void destroy_matricial_product(int gpe);

void new_perceive(int gpe);
void function_perceive(int gpe);
void destroy_perceive(int gpe);

void new_test_correlation(int index_of_group);
void function_test_correlation(int index_of_group);
void destroy_test_correlation(int index_of_group);

void new_low_pass_multi_tab (int index_of_group);
void function_low_pass_multi_tab (int index_of_group);
void destroy_low_pass_multi_tab (int index_of_group);
void function_selverstone(int numero);

void new_mem(int gpe);
void function_mem(int gpe);
void destroy_mem(int gpe);

#endif
