/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _VISION_H
#define _VISION_H

#include <Struct/prom_images_struct.h>
#include <libx.h>

#define STRING_SIZE 255

typedef struct MyData_f_load_image_sequential
{
	char listeim[STRING_SIZE];
	char type[STRING_SIZE];
	FILE *jice_fichier_liste;
	char  jice_nom_image[STRING_SIZE];
	int reload;
} MyData_f_load_image_sequential;

void load_image_from_disk(const char *im_format, char *im_name, unsigned char **image_table   /*image table */
                          , int *width, int *height /*image size */
                          , int *nbands /**/);

void free_prom_image(prom_images_struct * prom_image);
prom_images_struct *calloc_prom_image(int im_number, int x, int y, int b);
void save_png_to_disk(char *im_name, prom_images_struct prom_image_input,
                      int Z_PNG_COMPRESSION_CHOICE /*0 to 9 */ );
void save_ppm_to_disk(char *im_name, prom_images_struct prom_image);
#ifndef AVEUGLE
void affiche_pdv(TxDonneesFenetre * fenetre, int dmin, int dmax, int couleur,
                 int posx, int posy, char *texte);
#endif
void affichage2(int n, float *image, int sx, int sy, int x, int y);
int LastTargetReached(void);
#endif
