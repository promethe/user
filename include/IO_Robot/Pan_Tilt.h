/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IO_ROBOT_PAN_TILT_H
#define _IO_ROBOT_PAN_TILT_H

void function_pos_pano(int num);
void function_init_pan_tilt(int gpe);
void function_serial_close(int num);
void function_global_x(int num);
void function_global_y(int num);
void function_phi_pan_tilt(int num);
void function_merge(int num);
void function_phi_pan(int num);

void new_joint(int num);
void function_joint(int num);
void destroy_joint(int num);

void function_joint_population(int num);
void function_dphi_to_joint(int num);

void function_joint_get_proprio(int Gpe);
void function_speedo_joint(int num);
void function_project_on_global_space(int Gpe);
void function_readout(int num);

void new_pantilt_move_position(int gpe);
void function_pantilt_move_position(int gpe);
void destroy_pantilt_move_position(int gpe);

void new_pantilt_get_proprio(int gpe);
void function_pantilt_get_proprio(int gpe);
void destroy_pantilt_get_proprio(int gpe);

void new_motor_is_moving(int gpe);
void function_motor_is_moving(int gpe);
void destroy_motor_is_moving(int gpe);

void new_motor_position(int gpe);
void function_motor_position(int gpe);
void destroy_motor_position(int gpe);

void function_motor_get_proprio(int Gpe);

void new_activ_joint(int gpe);
void function_activ_joint(int gpe);

void new_motor_get_position(int gpe);
void function_motor_get_position(int gpe);
void destroy_motor_get_position(int gpe);

void new_motor_get_3D_pos_of_joint(int gpe);
void function_motor_get_3D_pos_of_joint(int gpe);
void destroy_motor_get_3D_pos_of_joint(int gpe);

void new_motor_get_torque(int gpe);
void function_motor_get_torque(int gpe);
void destroy_motor_get_torque(int gpe);

void new_motor_set_position(int gpe);
void function_motor_set_position(int gpe);
void destroy_motor_set_position(int gpe);

void new_motor_set_speed(int gpe);
void function_motor_set_speed(int pge);
void destroy_motor_set_speed(int gpe);

void new_motor_set_activation(int gpe);
void function_motor_set_activation(int pge);
void destroy_motor_set_activation(int gpe);

void new_motor_set_torque(int gpe);
void function_motor_set_torque(int gpe);
void destroy_motor_set_torque(int gpe);

void new_handle_panorama(int gpe);
void function_handle_panorama(int gpe);
void destroy_handle_panorama(int gpe);

void new_detect_end_panorama(int gpe);
void function_detect_end_panorama(int gpe);
void destroy_detect_end_panorama(int gpe);


#endif
