/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _ROBOT_MVT_H
#define _ROBOT_MVT_H

void function_test_robot(int numero);
void function_stop_robot(int numero);
void function_init_robot(int numero);
void function_init_robot_s(int Gpe);
void function_orientation(int numero);
void function_navigation_movement(int numero);
void function_calibre_compass(int gpe);
void function_generate_movement(int numero);
void function_stop_movement(int numero);
void function_move_alea(int numero);
void function_navigation_movement_robot(int numero);
void function_navigation_movement_simule(int numero);
void function_generate_rotation(int numero);
void function_explo_alea_ou_dirige(int gpe);
void function_navigation_movement_simule2(int numero);
void function_movement_simule(int numero);
void function_get_h(int numero);

void function_navigation_movement_simple(int numero);
void function_readout_nms(int numero);
void function_set_velocity(int numero);

void function_pseudo_movement_simple(int numero);
void new_odometrie(int gpe);
void function_odometrie(int gpe);
void new_odo_roburoc(int gpe);
void function_odo_roburoc(int gpe);
void function_true_orientation(int numero);
void function_turn_angle_robot(int numero);

void function_test_robubox(int gpe);
void function_laser_LMS(int gpe);

void function_speedo_angular_robot(int gpe);

void new_location_odo(int gpe);
void function_location_odo(int gpe);
void destroy_location_odo(int gpe);

void new_set_robot_location(int gpe);
void function_set_robot_location(int gpe);
void destroy_set_robot_location(int gpe);

void new_location_abs(int gpe);
void function_location_abs(int gpe);

void function_random_mvt(int numero);
void function_algowinner(int gpe);
void function_action_selection(int numero);
void function_store(int numero);
void function_positionX(int numero);
void function_epsilon_greedy_action(int numero);

void destroy_mrpt_send_odo(int gpe);
void function_mrpt_send_odo(int gpe);
void new_mrpt_send_odo(int gpe);

void new_get_motor_power(int gpe);
void function_get_motor_power(int gpe);

#endif
