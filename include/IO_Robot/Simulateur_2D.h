/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IO_ROBOT_SIM2D_H
#define _IO_ROBOT_SIM2D_H

#ifdef SIM2D

#define SIZE_TRACE  1000
#ifndef AVEUGLE
#include <graphic_Tx.h>
#endif

/*Le simulateur a une taille de 1000*1000*/
/*Mais sa representation peut changer, en gerant le parametre scale_display*/
typedef struct Landmark_sim2d
{
   int x;
   int y;
   int always_visible;
    
} Landmark_sim2d;

typedef struct Obstacle_sim2d
{
   int xa;
   int ya;
   int xb;
   int yb;
   int high;
    
} Obstacle_sim2d;

typedef struct Agent_sim2d
{
   float x;
   float y;
   float theta;

   float trace_x[SIZE_TRACE];
   float trace_y[SIZE_TRACE];
   float trace_theta[SIZE_TRACE];
    
   /*Masse mobile en kg*/
   float masse;
   
   /*vitesse max en m/s */
   float speed_max;
   
   /*Force centrifuge max*/
   float FC_max;
   
   /*rayon de vision du capteur de proximite en m*/
   float radius_proximity;
    
} Agent_sim2d;

typedef struct Sim2d
{

   int width;
   int height;  
   int step;      
         
   /*duree d'1 pas de temps en s*/
   float cste_temps;
         
   /*longueur d'un pixel en m*/
   float cste_spatial;      
    
   int nb_landmarks;
   Landmark_sim2d ** landmarks;

   int nb_obstacles;    
   Obstacle_sim2d ** obstacles;

   int nb_agents;
   Agent_sim2d ** agents;

} Sim2d;


extern Sim2d * the_sim2d;


/*Initilisation de l'environnement*/
void new_function_sim2d_init(int numero);
void function_sim2d_init(int numero);

/*Dessine l'environnement (efface tout et affiche)*/
void function_sim2d_display(int numero);

/*Perception d'un agent: necessite d'etre cable a un break en sortie et a un reset en entree*/
void function_sim2d_sequential_landmarks_perception(int numero);

/*Perception d'un agent: necessite d'etre cable a un break en sortie et a un reset en entree*/
void function_sim2d_landmarks_perception(int numero);
void function_sim2d_place_cell_simul(int gpe);

/*Perception d'un agent: necessite d'etre cable a un break en sortie et a un reset en entree*/
void function_sim2d_agent_proximity(int numero);

/*Deplace un agent: necessite un groupe mvt en entree*/
void function_sim2d_move_agent_abs(int numero);
void function_sim2d_move_agent_rel(int numero);

/*Dessine un agent et efvemtuellement sa trajectoire*/
void function_sim2d_display_agent(int numero);

/*Capteur de proximity*/
void function_sim2d_agent_proximity(int gpe);

void function_sim2d_agent_position(int gpe);

void function_sim2d_flushimage(int gpe);

void function_sim2d_otheragents_perception(int gpe);

#endif
#endif
