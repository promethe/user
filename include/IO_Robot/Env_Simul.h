/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IO_ROBOT_ENV_SIMUL_H
#define _IO_ROBOT_ENV_SIMUL_H

void function_env_init(int numero);
void function_env_init2(int numero);

extern void function_print_place(int gpe);
extern void new_print_one_place(int gpe);
extern void fin_print_one_place(int gpe);

extern void new_print_place_world(int gpe);
extern void function_print_place_world(int gpe);
extern void destroy_print_place_world(int gpe);

extern void function_stat_on_position_error(int gpe);
extern void new_stat_on_position_error(int gpe);
extern void destroy_stat_on_position_error(int gpe);

void function_print_one_place(int gpe);
void function_print_place_elegant(int gpe);
void function_simul_robot_perception(int numero);
void function_move_robot(int numero);
void function_show_env_and_robot(int numero);
void function_sequential_perception(int gpe);
void function_sequential_perception_break(int gpe);
void function_extract_val_to_vector(int gpe);
void function_extract_val_to_vector_circular(int gpe);
void function_extract_val_to_vector_non_circular(int gpe);

void new_extract_vector_to_val(int gpe);
void function_extract_vector_to_val(int gpe);

void function_fin_simul(int numero);
void function_vigilance(int numero);
void function_vigilance_up_down(int numero);
void function_explo_alea(int numero);
void function_move_robot_alea(int numero);
void function_move_robot_info(int gpe);
void function_drive_detected_simule(int gpe_sortie);
void function_print_infos(int gpe);
void function_simul_drive_expected(int numero);
void function_rebond(int numero);
void function_nav_a_vue(int numero);
void function_print_pos(int gpe);
void function_renorm_vect(int gpe_sortie);
void function_print_place_from_file(int gpe);
void function_reco_info(int numero);
void function_print_vector_explo_from_print_info(int gpe);
void function_trajectory_XY_teacher(int);
void function_trajectory_XY_teacher_2(int);
void function_trajectory_XY_teacher_3(int);
void function_trajectory_XY_teacher_4(int);

void new_monitor_planif(int);
void new_monitor_planif_real(int);
void function_monitor_planif(int);
void destroy_monitor_planif(int);
void save_monitor_planif(int);

void function_pres_obstacle(int);
void function_explo(int);
void function_sonar_simule(int);
void function_sonar_simule2(int);
void function_fin_planif(int);
void function_fin_planif_real(int);
void function_fin_simul_trajrecord(int);
void function_envie_et_decouverte(int);
void function_mesure_planif(int numero);

void new_monitor_planif_real(int);
void function_monitor_planif_real(int);
void destroy_monitor_planif_real(int);

void new_timing_reward(int);
void function_timing_reward(int);
void destroy_timing_reward(int);

void function_simulation_lumiere(int numero);
void function_simul_real(int gpe_sortie);

void new_variables_essentielles(int Gpe);
void function_variables_essentielles(int Gpe);

void function_extract_multi_vectors_to_vals(int gpe);
void function_extract_multi_vals_to_vectors(int gpe);
void function_extract_multi_vals_to_vectors_circular(int gpe);
void function_extract_multi_vals_to_vectors_non_circular(int gpe);
#endif
