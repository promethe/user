/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _NN_IO_H
#define _NN_IO_H

/* NN_IO */
void function_affiche_pdv_xy(int numero);
void function_affiche_xy(int numero);
void new_affiche_xy(int numero);
void destroy_affiche_xy(int numero);
void function_affiche_coeff2D(int numero);
void new_affiche_coeff2D(int numero);
void destroy_affiche_coeff2D(int numero);
void function_affiche_pdv_info(int numero);
void function_display_group(int numero);
void function_display_group_txt(int numero);
void function_display_group_perso(int numero);
void function_save_pdv (int Gpe);
void function_oscillo(int numero);
void function_oscillo_c(int numero);
void function_oscillo2(int numero);
void function_oscillo_group(int numero);
void new_demande_numero0(int gpe);
void function_demande_numero0(int gpe);
void new_display_image_activity(int numero);
void function_display_image_activity(int numero);
void destroy_display_image_activity(int numero);
void new_display_group_activity(int gpe);
void function_display_group_activity(int gpe);
void destroy_display_group_activity(int gpe);
void function_save_image_activity(int numero);
void function_write_image_activity_split(int numero);
void function_save_activity(int numero);
void function_save_weight(int numero);

void new_save_probas(int numero);
void function_save_probas(int numero);
void destroy_save_probas(int numero);

void new_save_weights(int numero);
void function_save_weights(int numero);
void destroy_save_weights(int numero);

void function_init_var_from_config(int numero);
void function_record_image_activity(int numero);

void new_speak(int gpe);
void function_speak(int gpe);
void destroy_speak(int gpe);
#endif
