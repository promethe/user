/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IO_ROBOT_GLOBAL_VAR_H
#define _IO_ROBOT_GLOBAL_VAR_H

#include <net_message_debug_dist.h> /* pour la def de MAXBUF; bofff.... */
#include <Struct/cam_pos.h>
#include <Typedef/boolean.h>
#include <Struct/motivation.h>
#include <Macro/IO_Robot/macro.h>

 /* NC boolean constante for the variable set by the ?.dev file */
#define UNUSED 0
 /*extern const int SET ; */
#define SET 1

/*extern cam_pos PTposition;        Variable used in tools/IO_Robot/Pan_Tilt et IO_Robot/Pan_Tilt*/
/*
extern int OFFSET_PAN;
extern int CAM_MODE;
extern int PAN_MAX_DELTA ;
extern int TILT_MAX_DELTA;
*/
extern int USE_ROBOT;           /* NC:  define the robot's type used:- */
#define USE_ROBOT_RESERVED 0    /*reserved until now. */
#define USE_KOALA 1
#define USE_LABO3 2
#define USE_SIMULATION 4

/*extern int USE_CAM;  NC: define the camera's type used:*/
#define USE_PAN_TILT_CAM 2
#define USE_PAN_CAM 1
#define USE_PANORAMIC_CAM 3     /*real 360 panoramic camera */

extern int USE_KATANA;
extern int USE_HEAD;

/*extern int MOVE_PAN ;
extern int MOVE_TILT ;
*/
/*extern char VIDEO_DEV[50];*/

extern boolean EMISSION_ROBOT;  /*NC: if set to 1 order are sent to the robot, else not. */

extern char PORT[MAXBUF];           /*NC: first serial port */
extern char SPORT[MAXBUF];          /*NC: second serial port */
/*
extern int serialport;
extern int serialport1;*/


extern int MOTOR_LOGICAL_STATUS[5];

extern float MOTOR_1, MOTOR_2, MOTOR_3, MOTOR_4, MOTOR_5;

/*extern int logical_DOF [MAX_DOF_KATANA_ARM];
/Attenetion : Variable Static dans arm.c*/

/*extern float Range[MAX_DOF_KATANA_ARM];/<array of the range of each motor encoder*/
/*Attenetion : Variable Static dans arm.c*/
/*extern float Min[MAX_DOF_KATANA_ARM]; /< array of the minimal value of each motor encoder*/
/*Attenetion : Variable Static dans arm.c*/


/*extern int AngleServoInt;*/

extern int NbrFeaturesPoints;


extern int FIELD;
/*
extern int USE_COMPASS;
extern int bc, bs;
extern int bcmin,bcmax,bsmin,bsmax;
extern int BS_crhaut;
extern int BS_crbas;*/

extern int alealearn;

extern motivation motivations[nb_motivations];
extern float Niveau_ressources[nb_motivations];

extern float Orientation;

#endif
