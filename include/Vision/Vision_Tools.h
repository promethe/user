/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _VISION_VISION_TOOLS_H
#define _VISION_VISION_TOOLS_H

void new_cut_image_ext(int gpe);
void function_cut_image_ext(int gpe);
void destroy_cut_image_ext(int gpe);

void function_extend_image_panoramic(int Gpe);
void function_shift_to_north_image_panoramic(int Gpe);
void function_split_image_panoramic(int Gpe);
void function_unsplit_image_panoramic(int Gpe);
void function_simulation_object(int gpe);
void function_conversion_coordonnee_neurone(int gpe);
void function_moyenne_image(int gpe);
void function_average_position(int gpe);
void new_imagette(int gpe);
void function_imagette(int gpe);
void new_insert_imagette(int gpe);
void function_insert_imagette(int gpe);
void destroy_insert_imagette(int gpe);
void new_moy_ecart_img(int gpe);
void function_moy_ecart_img(int gpe);
void function_quad_average(int gpe);
void function_sort_directions(int gpe);
void function_sort_directions_only(int gpe);

void function_affiche_mvt(int numero);

void new_image_zoom(int gpe);
void function_image_zoom(int gpe);
void destroy_image_zoom(int gpe);

void function_multi_where(int Gpe);
void function_keypoint_orient(int Gpe);
void function_compute_grad_orient(int Gpe);
void function_seuil_image(int Gpe);
void function_seuil_image_char(int Gpe);

void function_bipano_to_pano(int gpe);
void function_paste_panorama(int gpe);

void function_compare_trajectory(int gpe_sortie);

void new_running_average(int gpe);
void function_running_average(int gpe);
void destroy_running_average(int gpe);

void new_patch_normalization(int gpe);
void function_patch_normalization(int gpe);
void destroy_patch_normalization(int gpe);
void new_img_normalization(int gpe);
void function_img_normalization(int gpe);
void destroy_img_normalization(int gpe);


#endif
