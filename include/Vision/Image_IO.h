/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _VISION_IMAGE_IO_H
#define _VISION_IMAGE_IO_H


void function_load_baccon(int Gpe);
void function_load_listimage_intensity (int Gpe);
void new_load_listimage_intensity (int Gpe);
void function_load_image_from_disk(int Gpe);

void function_load_image_sequential(int Gpe);
void destroy_load_image_sequential(int Gpe);

void function_end_capture(int gpe_sortie);
void function_start_capture(int gpe_sortie);
void function_grabimages_autoexposure(int gpe_sortie);
void function_grabimages_autoexposure_waitcond(int gpe_sortie);
void function_grabimages_waitcond(int gpe_sortie);

void function_load_image_png(int gpe);

void new_grabimages(int gpe);
void function_grabimages(int gpe);
void destroy_grabimages(int gpe);

void new_save_images_to_disk(int gpe);
void new_save_image_to_disk(int gpe);
void function_save_image_to_disk(int gpe);
void destroy_save_image_to_disk(int gpe);

void new_copy_window_to_ext(int gpe);
void function_copy_window_to_ext(int gpe);
void destroy_copy_window_to_ext(int gpe);

void function_save_images_to_disk_expression(int gpe_sortie);
void function_save_groupe_to_disk_expression(int gpe_sortie);
void function_save_images_to_disk_expression_intensity(int gpe_sortie);
/* TODO combiner avec function_save_images_to_disk_expression */
void function_save_images_to_disk_intensity(int gpe_sortie);
void function_save_images_local(int gpe_sortie);
void function_get_one_image(int gpe);
void new_get_one_image(int gpe);
void function_load_image_to_prom_window(int numero);

void new_load_list(int Gpe);
void function_load_list(int Gpe);

void new_supervision(int Gpe);
void function_supervision(int Gpe);

void function_track_robot_with_mouse(int gpe);


void function_nios_grabimages(int gpe);
void new_NIOS_grabimages(int gpe);

void function_set_camera_param(int gpe);
void new_set_camera_param(int gpe);

void function_save_image_with_carac(int gpe);
void new_save_image_with_carac(int gpe);
void destroy_save_image_with_carac(int gpe);

void function_compress_image(int gpe);
void new_compress_image(int gpe);
void destroy_compress_image(int gpe);

void new_uncompress_image(int gpe);
void function_uncompress_image(int gpe);
void destroy_uncompress_image(int gpe);

#endif
