/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _IMAGE_PROCESSING_H
#define _IMAGE_PROCESSING_H

void function_multi_inputs_fft(int Gpe);
void function_init_fftw(int Gpe);
void function_init_fftw_complex(int Gpe);
void function_fft_inv_complex_to_real(int Gpe);
void function_fft_real_to_complex(int Gpe);
void function_wta_ext_float(int Gpe);
void function_tracking_filter_dyn(int Gpe);
void function_tracking_filter_static_graphic(int Gpe);
void function_tracking_filter_static(int Gpe);

void function_EV_ext(int gpe_sortie);
void function_rho_theta(int gpe_sortie);

void new_memory_focus_point(int gpe_sortie);
void function_memory_focus_point(int gpe_sortie);
void destroy_memory_focus_point(int gpe_sortie);

void new_give_focus_point(int gpe_sortie);
void function_give_focus_point(int gpe_sortie);
void destroy_give_focus_point(int gpe_sortie);

void function_sequ_winn_combine(int gpe_sortie);

void function_filtre_FFT_complex(int gpe_sortie);

void function_tracking_head_static(int Gpe);
void function_tracking_eye_static(int Gpe);
void function_tracking_bouche_static(int Gpe);

void function_tracking_head_static_neurone(int Gpe);

void f_wta_special(int numero);

void function_quantite_mvt(int gpe);
void function_visu_orient_scale_key(int Gpe);
void function_image_difference(int Gpe);
void function_detect_mvt_fast(int Gpe);
void function_detect_flow_fast_signed_x(int Gpe);
void new_image_multiply(int gpe);
void function_image_multiply(int gpe);

void new_give_nios_focus_point(int gpe);
void function_nios_give_focus_point(int gpe);
void function_NIOS_rho_theta(int gpe_sortie);
/* void function_learn_position(int Gpe);*/

void destroy_tracking_diff(int Gpe);
void new_tracking_diff(int Gpe);
void function_tracking_diff(int Gpe);

void new_give_focus_point_from_saved_data(int gpe);
void function_give_focus_point_from_saved_data(int gpe);
void destroy_give_focus_point_from_saved_data(int gpe);

#endif
