/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _LIBPARALLEL_COMM_H
#define _LIBPARALLEL_COMM_H

#include "blc_channel.h"
#include "libx.h"
#include <Struct/prom_images_struct.h>


/* pour demarrage_taches_transmissions (arguments args) */
#ifndef ARGUMENTS_H
#include <Struct/arguments.h>
#endif

/* fonction de la librairie dynamic */
void read_help();
void modify_help();
void initialisation_des_groupes();

/* fonction de Parallel_Comm */
void new_receive_PVM_block_ack(int num);
void function_receive_PVM_block_ack(int num);
void destroy_receive_PVM_block_ack(int num);

void new_receive_PVM_non_block_ack(int num);
void function_receive_PVM_non_block_ack(int num);
void destroy_receive_PVM_non_block_ack(int num);


void new_send_PVM_ack_sync(int num);
void function_send_PVM_ack_sync(int num);
void destroy_send_PVM_ack_sync(int num);

void new_send_PVM_ack(int num);
void function_send_PVM_ack(int num);
void destroy_send_PVM_ack(int num);

void new_receive_PVM_non_block(int num);
void function_receive_PVM_non_block(int num);
void destroy_receive_PVM_non_block(int num);

void new_send_PVM(int num);
void function_send_PVM(int num);
void destroy_send_PVM(int num);

void new_receive_PVM_ack_block(int num);
void function_receive_PVM_ack_block(int num);
void destroy_receive_PVM_ack_block(int num);

void new_receive_PVM_block(int num);
void function_receive_PVM_block(int num);
void destroy_receive_PVM_block(int num);

void new_receive_SOCKET_ack_block(int num);
void function_receive_SOCKET_ack_block(int num);
void destroy_receive_SOCKET_ack_block(int num);

void new_receive_SOCKET_block_ack(int num);
void function_receive_SOCKET_block_ack(int num);
void destroy_receive_SOCKET_block_ack(int num);

void new_receive_SOCKET_block(int num);
void function_receive_SOCKET_block(int num);
void destroy_receive_SOCKET_block(int num);

void new_receive_SOCKET_non_block_ack(int num);
void function_receive_SOCKET_non_block_ack(int num);
void destroy_receive_SOCKET_non_block_ack(int num);

void new_receive_SOCKET_non_block(int num);
void function_receive_SOCKET_non_block(int num);
void destroy_receive_SOCKET_non_block(int num);

void new_send_SOCKET_ack(int num);
void function_send_SOCKET_ack(int num);
void destroy_send_SOCKET_ack(int num);

void new_send_SOCKET_ack_sync(int num);
void function_send_SOCKET_ack_sync(int num);
void destroy_send_SOCKET_ack_sync(int num);

void new_send_SOCKET(int num);
void function_send_SOCKET(int num);
void destroy_send_SOCKET(int num);



/* libcomm */
void new_send(int num_groupe);
void function_send(int num_groupe);
void destroy_send(int num_groupe);

void new_recv(int num_groupe);
void function_recv(int num_groupe);
void destroy_recv(int num_groupe);


void new_network_id(int num_groupe);
void function_network_id(int num_groupe);
void destroy_network_id(int num_groupe);

void new_publish(int num_groupe);
void function_publish(int num_groupe);
void destroy_publish(int num_groupe);

void new_send_udp(int num_groupe);
void function_send_udp(int num_groupe);
void destroy_send_udp(int num_groupe);

void new_recv_udp(int num_groupe);
void function_recv_udp(int num_groupe);
void destroy_recv_udp(int num_groupe);

void new_in(int gpe);
void function_in(int gpe);
void destroy_in(int gpe);

void new_out(int gpe);
void function_out(int gpe);
void destroy_out(int gpe);


#endif
