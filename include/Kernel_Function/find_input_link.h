/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _FIND_INPUT_LINK_H
#define _FIND_INPUT_LINK_H
/* cherche dans une table le numero du xeme groupe de lien connecte en entree du groupe considere
   si c'est la premiere fois que cette requete est lance alors la fonction recherche et enregistre
   tous les liens du groupe de maniere a accelerer les recherches suivantes.
   retourne -1 s'il n'y a pas de numero de lien correspondant  */

extern int find_input_link(int group, int link_number);
/*{
    int val, nb_inputs, i;
    val = input_link_number[group][link_number];
    if (val != -2)
	return val;
     recherche pour la premiere fois la liaison correspondante 
   nb_inputs = 0;
   for (i = 0; i < nbre_liaison; i++) {
	if (liaison[i].arrivee == group) {
	    input_link_number[group][nb_inputs] = i;
	    nb_inputs = nb_inputs + 1;
	}
    }
    for (i = nb_inputs; i < nbre_liaison; i++)
	input_link_number[group][nb_inputs] = -1;

    return input_link_number[group][link_number];

    }*/
#endif
