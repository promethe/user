/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _ECRITURE_LENA_H
#define _ECRITURE_LENA_H

extern void ecriture_lena(unsigned char *im3, int xmax, int ymax, int gpe);
 /*{ */
 /*    int p, i, j, dx, dy, p2, deb, taillex,tailley,tailleg, nbre, increment; */
 /*    float echx,echy,echg; */
 /*    float taille_max, taille_min; */
 /*    int x_offset=10,y_offset=40; */
 /*    TxPoint point; */
 /*    static char chaine[255]; */
 /**/
    /*if (rapide == 1) return; */
    /*    TxEffacerAireDessin(&fenetre2); */
    /*    sprintf(chaine,"Debug .s activity of group %d, named : %s \n",gpe,def_groupe[gpe].nom); */
    /*point.x=10;point.y=20; */
    /*    TxEcrireChaine(&fenetre2, bleu, point, chaine, NULL); */
     /**/
    /*    dx = def_groupe[gpe].taillex; */
    /*   dy = def_groupe[gpe].tailley; */
    /*    if (dx < 1 || dy < 1) { */
    /*     printf("ERREUR: le groupe courant a une taille nulle \n"); */
    /*     exit(0); */
    /*    } */
    /*    nbre = def_groupe[gpe].nbre; */
    /*    increment = nbre / (dx * dy); */
    /*    echx = ((float) xmax) / ((float) dx); */
    /*    echy = ((float) ymax) / ((float) dy); */
    /*   deb = def_groupe[gpe].premier_ele; */
    /*    taille_max = 0.; */
    /*   taille_min = 10000.; */
    /*   for (i = deb + increment - 1; i < nbre; i = i + increment) { */
    /*     if (neurone[i].s > taille_max) */
    /*         taille_max = (neurone[i].s); */
    /*     else if (neurone[i].s < taille_min) */
    /*         taille_min = neurone[i].s; */
    /*    } */
     /**/
    /*    if (taille_max <= 0.) { */
    /*      printf("toutes les sorties de la couche courante a 0\n");return; */
    /*     taille_max = 1000000.; */
    /*    } */
    /*    if (p_trace > 0) { */
    /*     printf("plus grande valeur a afficher = %f \n", taille_max); */
    /*     printf("plus petite valeur a afficher = %f \n", taille_min); */
    /*     printf("echelle echx= %f echy=%f\n", echx,echy); */
    /*     printf("xmax = %d \n", xmax); */
    /*    } */
     /**/
/*   if (rapide == 0) */
    /*     for (j = 0; j < dy; j++) */
    /*         for (i = 0; i < dx; i++) */
    /*            { */
    /*             p = (int) (((float) i) * echx) + */
    /*                 (int) (((float) j) * echy * ((float) xmax)); */
    /*             p2 = (1 + i + j * dx) * increment - 1; */
    /*             point.x = x_offset + (int) (((float) i) * echx); */
    /*             point.y = y_offset + (int) (((float) j) * echy); */
    /*             if (neurone[p2 + deb].s < 0.) { */
    /*                 taillex = (int) (-neurone[p2 + deb].s * echx); */
    /*                 tailley = (int) (-neurone[p2 + deb].s * echy); */
    /*                 if (taillex < 1)    taillex = 1; */
    /*                 if (tailley < 1)    tailley = 1; */
    /*                 TxDessinerRectangle(&fenetre2, rouge, TxVide, point, */
    /*                                     taillex, tailley, 1); */
    /*             } else { */
    /*                 taillex = (int) (neurone[p2 + deb].s * echx); */
    /*                 tailley = (int) (neurone[p2 + deb].s * echy); */
    /*                 if (taillex < 1)    taillex = 1; */
    /*                 if (tailley < 1)    tailley = 1; */
    /*                 TxDessinerRectangle(&fenetre2, noir, TxPlein, point, */
    /*                                     taillex, tailley, 1); */
    /*             } */
    /*              if(taillex<tailley) {tailleg=tailley; echg=echy;} else {tailleg=taillex; echg=echx;} */
    /*             if (tailleg > (int) echg) */
    /*                 tailleg = (int) echg; */
    /*             if (tailleg <= 0) */
    /*                 tailleg = 1; */
    /*             im3[p2] = (int) (((float) (tailleg - 1)) * 256. / echg); */
    /*         } */
     /**/
    /*    TxFlush(&fenetre2); */
 /**   printf("appuyez sur une touche ");*/
    /*   scanf("%c",&c); */
     /**/
/*   printf("ecriture de l'image\n");
   decri.ny=xmax;
   decri.nx=xmax;
   p_im=ouvre('e',&decri,fsortie);
   ecri(&p_im,im3,decri.ny); */
    /*} */
#endif
